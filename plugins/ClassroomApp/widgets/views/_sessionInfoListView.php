<style>
    a.noClickEvent{
        vertical-align: middle;
    }
</style>
<?php /* @var $data array  */
$idCourse = intval($_GET['course_id']);
$idSession = intval($_GET['session_id']);
$course_associated = isset(Yii::app()->session['PUIsCourseAssociated'][$idCourse]) ? Yii::app()->session['PUIsCourseAssociated'][$idCourse] : false;
$isSubscribed = LtCourseuserSession::isSubscribed(Yii::app()->user->id, $idSession);
$isInstructor = LearningCourseuser::isUserLevel(Yii::app()->user->id, $idCourse, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR);
?>
<div class="item">
    <div class="primary row-fluid">
        <div class="item-col col-status">
            <?php
            $css = '';
            $_today = strtotime(Yii::app()->localtime->getLocalNow('Y-m-d H:i:s'));
			$_day_begin = strtotime($data->day.' '.$data->time_begin.' '.$date->timezone);
            $_day_end = strtotime($data->day.' '.$data->time_end.' '.$date->timezone);
            if ($_today > $_day_end) {
                $css = 'green';
            }
            else if ($_day_begin <= $_today && $_day_end >= $_today) {
                $css = 'orange';
            }
            else {
                $css = 'grey';
            }
            $html = '<span class="i-sprite is-circle-check '.$css.'"></span>';
            echo $html;
            ?>
        </div>
        <div class="item-col col-name text-colored">
            <?= $data->name ?>
        </div>
        <div class="item-col col-toggle_handle">
            <div style="display: inline-block;margin-right: 20px">
                <?php if (Yii::app()->user->getIsGodadmin() //godadmin
                    //|| isset(Yii::app()->session['PUIsInstructorForCourse'][$idCourse][$idSession]) //Warning: not set only when PU is Instructor but when is Learner too
                    //PU with "view" permissions
                    //|| (Yii::app()->user->getIsPu() && $course_associated && Yii::app()->user->checkAccess('/lms/admin/classroomsessions/view'))  //This check not working for PU Profile permissions
                    || (Yii::app()->user->getIsPu() && $course_associated && CoreRoleMembers::checkPUAccess(CoreGroupMembers::model()->getPowerUserProfile(Yii::app()->user->id)->idst, '/lms/admin/classroomsessions/view'))
                    //instructor subscribed to the session
                    || ($isInstructor && $isSubscribed)
                ) { ?>
                    <a href="<?= Docebo::createLmsUrl('ClassroomApp/instructor/axAttendanceSheet', array(
                        'course_id' => $idCourse,
                        'id_session' => $idSession
                    )) ?>" class="open-dialog noClickEvent" data-dialog-class="attendance-sheet-print-dialog" rel="tooltip" title="<?=Yii::t('classroom', 'Print attendance sheet');?>"><i class="fa fa-print"></i></a>
                <?php }
                if (Yii::app()->user->getIsGodadmin() //godadmin
                    //|| isset(Yii::app()->session['PUIsInstructorForCourse'][$idCourse][$idSession]) //Warning: not set only when PU is Instructor but when is Learner too
                    //PU with "mod" permissions OR with "add" permissions and the course is created by him
                    //|| (Yii::app()->user->getIsPu() && $course_associated && (Yii::app()->user->checkAccess('/lms/admin/course/evaluation')))  //This check not working for PU Profile permissions
                    || (Yii::app()->user->getIsPu() && $course_associated && CoreRoleMembers::checkPUAccess(CoreGroupMembers::model()->getPowerUserProfile(Yii::app()->user->id)->idst, '/lms/admin/course/evaluation'))
                    //instructor subscribed to the session
                    || ($isInstructor && $isSubscribed)
                ) { ?>
                    <a href="<?= Docebo::createLmsUrl('ClassroomApp/instructor/evaluation', array(
                        'course_id' => $idCourse,
                        'id_session' => $idSession
                    )); ?>" class="noClickEvent" rel="tooltip" title="<?=Yii::t('classroom', 'Evaluation & attendance');?>"><i class="fa fa-book"></i></a>
                <?php } ?>
            </div>
            <?= CHtml::tag('span', array('class'=>'fa fa-chevron-down', 'data-expand-class'=>'ico-arrow-top', 'data-retract-class'=>'ico-arrow-bottom'), '') ?>
        </div>
        <div class="item-col col-location">
            <?= $data->ltLocation->name ?>
        </div>
        <div class="item-col col-time">
            <?= Yii::app()->localtime->toLocalDateTime($data->day . ' ' . $data->time_end, 'short', 'short', null, $data->timezone); ?>
        </div>
        <div class="item-col col-date">
            <?= Yii::app()->localtime->toLocalDateTime($data->day . ' ' . $data->time_begin, 'short', 'short', null, $data->timezone); ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row-fluid secondary hide">

        <div class="span6">
            <h4 class="info-section-title"><?= Yii::t('classroom', 'Location information') ?></h4>
            <div class="row-fluid">
                <div class="span6">
                    <div class="info-section">
                        <div class="info-section-icon">
                            <span class="classroom-sprite marker-tiny"></span>
                        </div>
                        <div class="info-section-content has-icon">
                            <?= $data->location->name ?><br/>
                            <?= $data->location->renderAddress() ?>
                        </div>
                    </div>
                </div>
                <div class="span6">
                    <?php if (!empty($data->location->telephone)) : ?>
                    <div class="info-section">
                        <div class="info-section-icon">
                            <span class="classroom-sprite phone"></span>
                        </div>
                        <div class="info-section-content has-icon" style="padding-top: 3px;"><?= $data->location->telephone ?><br/></div>
                    </div>
                    <?php endif; ?>
                    <?php if (!empty($data->location->email)) : ?>
                    <div class="info-section">
                        <div class="info-section-icon">
                            <span class="classroom-sprite mail"></span>
                        </div>
                        <div class="info-section-content has-icon text-colored"><?= $data->location->email ?></div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>

            <?php if ($data->ltClassroom) : ?>
            <h4 class="info-section-title"><?= Yii::t('classroom', 'Classroom information') ?></h4>
            <div class="row-fluid">
                <div class="span6">
                    <div class="info-section">
                        <div class="info-section-icon">
                            <span class="classroom-sprite classroom"></span>
                        </div>
                        <div class="info-section-content has-icon">
                            <?= $data->ltClassroom->name ?><br/>
                            <?= DoceboHtml::readmore($data->ltClassroom->details, 200) ?>
                        </div>
                    </div>
                    <?php if (isset($data->ltClassroom->seats)) : ?>
                    <div class="info-section">
                        <div class="info-section-icon">
                            <span class="classroom-sprite users"></span>
                        </div>
                        <div class="info-section-content has-icon" style="padding-top: 2px;"><?= Yii::t('classroom', '{count} available seats', array('{count}'=>$data->ltClassroom->seats)) ?><br/></div>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="span6">
                    <?php if (!empty($data->ltClassroom->equipment)) : ?>
                    <div class="info-section">
                        <div class="info-section-icon">
                            <span class="classroom-sprite settings"></span>
                        </div>
                        <div class="info-section-content has-icon"><?= $data->ltClassroom->equipment ?></div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php endif; ?>

            <?php if ($data->ltLocation->hasExtraInfo() && !empty($data->ltLocation->address)) : ?>
            <h4 class="info-section-title"><?= Yii::t('classroom', 'Location map') ?></h4>
            <div class="info-section">
                <div class="map-preview-container">
                    <?= CHtml::hiddenField('address', $data->ltLocation->renderAddress()) ?>
                    <span class="classroom-sprite map-placeholder"></span>
                    <div class="location-map-canvas"></div>
                </div>
            </div>
            <?php endif; ?>
        </div>

        <div class="span6">
            <?php if ($data->ltLocation->hasExtraInfo()) : ?>
                <?php if (!empty($data->ltLocation->reaching_info)) : ?>
                <h4 class="info-section-title"><?= Yii::t('classroom', 'How to reach the location') ?></h4>
                <div class="info-section">
                    <div class="info-section-content"><?= Yii::app()->htmlpurifier->purify($data->ltLocation->reaching_info) ?></div>
                </div>
                <?php endif; ?>
                <?php if (!empty($data->ltLocation->accomodations)) : ?>
                <h4 class="info-section-title"><?= Yii::t('classroom', 'Suggested accomodations') ?></h4>
                <div class="info-section">
                    <div class="info-section-content"><?= Yii::app()->htmlpurifier->purify($data->ltLocation->accomodations) ?></div>
                </div>
                <?php endif; ?>
                <?php if (!empty($data->ltLocation->other_info)) : ?>
                <h4 class="info-section-title"><?= Yii::t('classroom', 'Other info') ?></h4>
                <div class="info-section">
                    <div class="info-section-content"><?= Yii::app()->htmlpurifier->purify($data->ltLocation->other_info) ?></div>
                </div>
                <?php endif; ?>
                <?php if (!empty($data->ltLocation->photos)) : ?>
                <h4 class="info-section-title"><?= Yii::t('classroom', 'Location photos') ?></h4>
                <div class="info-section">
                    <div class="info-section-content">
                        <?php foreach ($data->ltLocation->photos as $_photo) : ?>
                            <a href="<?= $_photo->getUrl() ?>" class="thumbnail fancybox" rel="gallery">
                                <img src="<?= $_photo->getUrl() ?>" alt=""/>
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>
                <?php endif; ?>
            <?php else: ?>

                <?php if (!empty($data->ltLocation->address)) : ?>
                    <h4 class="info-section-title"><?= Yii::t('classroom', 'Location map') ?></h4>
                    <div class="info-section">
                        <div class="map-preview-container">
                            <?= CHtml::hiddenField('address', $data->ltLocation->renderAddress()) ?>
                            <span class="classroom-sprite map-placeholder"></span>
                            <div class="location-map-canvas"></div>
                        </div>
                    </div>
                <?php endif; ?>

            <?php endif; ?>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('a.noClickEvent').click(function(e){
            e.stopPropagation();
        })
    })
</script>