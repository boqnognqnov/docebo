<?php
	/* @var $model LtCourseSession */
	$course_associated = isset(Yii::app()->session['PUIsCourseAssociated'][$idCourse]) ? Yii::app()->session['PUIsCourseAssociated'][$idCourse] : false;
	$isSubscribed = LtCourseuserSession::isSubscribed(Yii::app()->user->id, $idSession);
	$isInstructor = LearningCourseuser::isUserLevel(Yii::app()->user->id, $idCourse, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR);
?>

<div class="node-actions">
    <ul>
	    <?php if(Yii::app()->user->getIsGodadmin() //godadmin
			//|| isset(Yii::app()->session['PUIsInstructorForCourse'][$idCourse][$idSession]) //Warning: not set only when PU is Instructor but when is Learner too
			//PU with "mod" permissions OR with "add" permissions and the course is created by him
			//|| (Yii::app()->user->getIsPu() && $course_associated && (Yii::app()->user->checkAccess('/lms/admin/course/evaluation'))) //This check not working for PU Profile permissions
            || (Yii::app()->user->getIsPu() && $course_associated && CoreRoleMembers::checkPUAccess(CoreGroupMembers::model()->getPowerUserProfile(Yii::app()->user->id)->idst, '/lms/admin/course/evaluation'))
			//instructor subscribed to the session
			|| ($isInstructor && $isSubscribed)
		): ?>
        <li class="node-action">
            <i class="classroom-sprite attendance"></i>
            <a style="padding: 0px 0px 0px 8px; background: none;" href="<?= Docebo::createLmsUrl('ClassroomApp/instructor/evaluation', array(
                'course_id' => $idCourse,
                'id_session' => $idSession
            )); ?>">
                <?php echo Yii::t('classroom', 'Evaluation & attendance'); ?>
            </a>
        </li>
	    <?php endif; ?>


	    <?php if(Yii::app()->user->getIsGodadmin() //godadmin
			//|| isset(Yii::app()->session['PUIsInstructorForCourse'][$idCourse][$idSession]) //Warning: not set only when PU is Instructor but when is Learner too
			//PU with "view" permissions
			//|| (Yii::app()->user->getIsPu() && $course_associated && Yii::app()->user->checkAccess('/lms/admin/classroomsessions/view'))  //This check not working for PU Profile permissions
            || (Yii::app()->user->getIsPu() && $course_associated && CoreRoleMembers::checkPUAccess(CoreGroupMembers::model()->getPowerUserProfile(Yii::app()->user->id)->idst, '/lms/admin/classroomsessions/view'))
			//instructor subscribed to the session
			|| ($isInstructor && $isSubscribed)
		): ?>
        <li class="node-action">
            <i class="classroom-sprite print"></i>
            <a style="padding: 0px 0px 0px 4px; background: none;" href="<?= Docebo::createLmsUrl('ClassroomApp/instructor/axAttendanceSheet', array(
                'course_id' => $idCourse,
                'id_session' => $idSession
            )) ?>" data-dialog-class="attendance-sheet-print-dialog" class="open-dialog">
                <?php echo Yii::t('classroom', 'Print attendance sheet'); ?>
            </a>
        </li>
	    <?php endif; ?>


	    <?php if(Yii::app()->user->getIsGodadmin() //godadmin
			|| isset(Yii::app()->session['PUIsInstructorForCourse'][$idCourse][$idSession])
			//PU with "view" permissions
			|| (Yii::app()->user->getIsPu() && $course_associated && Yii::app()->user->checkAccess('/lms/admin/classroomsessions/view'))
			//instructor, subscribed to the session
			|| ($isInstructor && $isSubscribed)
		): ?>
        <li class="node-action">
            <i class="classroom-sprite details"></i>
            <a style="padding: 0px 0px 0px 8px; background: none;" href="<?= Docebo::createLmsUrl('player/training/session', array('session_id' => $idSession, 'course_id' => $idCourse)) ?>">
                <?php echo Yii::t('player', 'Student view'); ?>
            </a>
        </li>
	    <?php endif; ?>

        <?php
        // let plugins add menu items
        $_extraActions = '';
        Yii::app()->event->raise('OnCourseSessionListActionsRender', new DEvent($this, array(
            'extraActions' => &$_extraActions,
            'courseId' => $idCourse,
            'sessionId' => $idSession
        )));
        echo $_extraActions;
        ?>

        <?php if (Yii::app()->user->getIsGodadmin() || (Yii::app()->user->getIsPu() && $course_associated && (Yii::app()->user->checkAccess('/lms/admin/classroomsessions/mod') ||
					(Yii::app()->user->checkAccess('/lms/admin/classroomsessions/add') && $model->created_by==Yii::app()->user->getIdst())))): ?>
		<li class="node-action"><hr/></li>
        <li class="node-action">
            <a class="open-dialog course-copy"
               href="<?= Docebo::createLmsUrl('ClassroomApp/session/axCopySession', array('id_session' => $idSession, 'course_id' => $idCourse)) ?>"
               data-modal-class="modal-copy-session"
               data-dialog-class="modal-copy-session">
                <?php echo Yii::t('standard', '_MAKE_A_COPY'); ?>
            </a>
        </li>
        <?php endif; ?>

	    <?php if (Yii::app()->user->getIsGodadmin() || (Yii::app()->user->getIsPu() && $course_associated && (Yii::app()->user->checkAccess('/lms/admin/classroomsessions/mod')||
					(Yii::app()->user->checkAccess('/lms/admin/classroomsessions/add') && $model->created_by==Yii::app()->user->getIdst())))): ?>
        <li class="node-action">
            <a class="open-dialog node-edit"
               href="<?=Docebo::createLmsUrl('ClassroomApp/session/edit', array('course_id' => $idCourse, 'id_session' => $idSession))?>"
               class="open-dialog"
               data-dialog-class="modal-create-session"
               data-before-send="beforeSessionSave(data)">
                <?php echo Yii::t('standard', '_MOD'); ?>
            </a>
        </li>
	    <?php endif; ?>
	    <?php if(Yii::app()->user->getIsGodadmin() ||  (Yii::app()->user->checkAccess('/lms/admin/classroomsessions/del') && Yii::app()->user->checkAccess('/lms/admin/classroomsessions/mod')) || (Yii::app()->user->checkAccess('/lms/admin/classroomsessions/del') && !Yii::app()->user->checkAccess('/lms/admin/classroomsessions/add') && !Yii::app()->user->checkAccess('/lms/admin/classroomsessions/mod')) ||  ( Yii::app()->user->checkAccess('/lms/admin/classroomsessions/del') && Yii::app()->user->checkAccess('/lms/admin/classroomsessions/add') && !Yii::app()->user->checkAccess('/lms/admin/classroomsessions/mod')  && $model->created_by==Yii::app()->user->getIdst())): ?>
		    <li class="node-action">
			    <a class="open-dialog delete-node"
			       data-dialog-class="modal-delete-session"
			       href="<?= Docebo::createLmsUrl('ClassroomApp/session/axDeleteSession', array('id_session' => $idSession, 'course_id' => $idCourse)) ?>"
			       rel="delete-session">
				    <?php echo Yii::t('standard', '_DEL'); ?>
			    </a>
		    </li>
        <?php endif; ?>
    </ul>
</div>