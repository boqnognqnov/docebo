<?php
/* @var $form CActiveForm */
/* @var $settings TeleskillAppSettingsForm */
?>

<?php DoceboUI::printFlashMessages(); ?>

<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'app-settings-form',
	'htmlOptions'=>array('class' => 'form-horizontal docebo-form'),
));
?>

<a href="<?= ('it' == Yii::app()->getLanguage()) ? 'https://www.docebo.com/it/knowledge-base/elearning-docebo-per-teleskill/' : 'https://www.docebo.com/knowledge-base/elearning-docebo-for-teleskill/' ?>" target="_blank" class="app-link-read-manual"><?= Yii::t('apps', 'Read Manual'); ?></a>

<h2><?=Yii::t('standard','Settings') ?>: <?= Yii::t('standard', '_TELESKILL') ?></h2>
<br>

<div class="control-container odd">
	<div class="control-group">
		<?=CHtml::label(Yii::t('standard', 'Website'), false, array('class'=>'control-label'));?>
		<div class="controls">
			<a class="app-settings-url" href="http://www.teleskill.it/" target="_blank">http://www.teleskill.it/</a>
		</div>
	</div>
</div>

<div class="control-container even">
	<div class="control-group">
		<?=CHtml::label(Yii::t('webinar', 'Accounts'), false, array('class'=>'control-label'));?>
		<div class="controls">
			<?php
			$this->widget('common.widgets.WebinarAccountsEditor', array(
				'dataProvider' => WebinarToolAccount::model()->dataProvider('teleskill'),
				'editAccountUrl' => $this->createUrl('editAccount'),
				'enableMultiAccount' => WebinarTool::getById('teleskill')->hasMultiAccountSupport()
			));
			?>
		</div>
	</div>
</div>

<?php $this->endWidget(); ?>