<?php

/**
 * Plugin for TeleskillApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class TeleskillApp extends DoceboPlugin {


	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'TeleskillApp';

	/**
	 * Returns the settings page (admin app)
	 */
	public static function settingsUrl() {
		return Docebo::createAdminUrl('TeleskillApp/teleskillApp/settings');
	}

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {
		parent::activate();
		// create missing table
		$sql = "CREATE TABLE IF NOT EXISTS `conference_teleskill_meeting` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `id_room` int(11) NOT NULL COMMENT 'Id of the LMS room (local)',
		  `meeting_id` bigint(20) unsigned NOT NULL COMMENT 'Teleskill room id',
		  PRIMARY KEY (`id`),
		  KEY `id_room` (`id_room`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
		$command = Yii::app()->db->createCommand($sql);
		$command->execute();
	}


	public function deactivate() {
		parent::deactivate();
	}


}
