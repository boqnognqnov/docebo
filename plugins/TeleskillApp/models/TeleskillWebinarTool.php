<?php

/**
 * Class TeleskillWebinarTool
 * Implemenents webinar tool fo teleskill
 */
class TeleskillWebinarTool extends WebinarTool {

	/**
	 * Main constructor
	 */
	protected function __construct() {
		$this->id = 'teleskill';
		$this->name = 'Teleskill';
	}

	/**
	 * Creates a new room using the Teleskill api
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param string $roomDescription The description of the room
	 * @param integer $idAccount The id of the account to use to create the room
	 * @param $startDateTime string The start date&time in UTC + MYSQL/ISO format
	 * @param $endDateTime string The end date&time in UTC + MYSQL/ISO format
	 * @param $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 *
	 * @return array "tool specific" params returned by the api
	 */
	public function createRoom($roomName, $roomDescription, $idAccount, $startDateTime, $endDateTime, $maxParticipants, $api_params = array()) {
		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new TeleskillApiClient($accountSettings->clientcode);
		$meetingId = $apiClient->createMeeting($roomName, $startDateTime, $endDateTime, $maxParticipants);
		if (!$meetingId)
			throw new CHttpException(500, ConferenceManager::$MSG_INVALID_DATA_RETURNED);

		$result = array(
			"meeting_id" => $meetingId,
		);

		return $result;
	}

	/**
	 * Updates an existing room with the provided params
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param string $roomDescription The description of the room
	 * @param integer $idAccount The id of the account to use to update the room
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @param $startDateTime string The start date&time in UTC + MYSQL/ISO format
	 * @param $endDateTime string The end date&time in UTC + MYSQL/ISO format
	 * @param $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 *
	 * @return array "tool specific" params returned by the api
	 */
	public function updateRoom($roomName, $roomDescription, $idAccount, $api_params = array(), $startDateTime, $endDateTime, $maxParticipants) {
		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new TeleskillApiClient($accountSettings->clientcode);
		$apiClient->updateMeeting($api_params['meeting_id'], $roomName, $startDateTime, $endDateTime, $maxParticipants);
		return $api_params;
	}

	/**
	 * Deletes an existing room
	 *
	 * @param integer $idAccount The id of the account to use to delete the room
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 */
	public function deleteRoom($idAccount, $api_params = array()) {
		$meetingId = $api_params['meeting_id'];
		$account = $this->getAccountById($idAccount, true);
		if($account !== false)
		{
			$accountSettings = $account->settings;
			$apiClient = new TeleskillApiClient($accountSettings->clientcode);
			$apiClient->deleteMeeting($meetingId);
		}
	}

	/**
	 * Returns the URL to join a room
	 *
	 * @param string         $roomName         The name of the room to create in the videconference platform
	 * @param integer        $maxParticipants  integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param integer        $idAccount        The id of the account to use
	 * @param array          $api_params       An array of "tool specific" params to pass to the api
	 * @param CoreUser       $userModel        The id of the user for whom the current URL is returned
	 * @param WebinarSession $sessionDateModel The current session object
	 */
	public function getJoinRoomUrl($roomName, $maxParticipants, $idAccount, $api_params, $userModel, $sessionDateModel, $widgetContext = false) {
		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new TeleskillApiClient($accountSettings->clientcode);
		return $apiClient->getJoinMeetingUrl($api_params['meeting_id'], $userModel->idst, $this->getUserDisplayName($userModel), $userModel->email, TeleskillApiClient::ROLE_ATTENDEE);
	}

	/**
	 * Returns the URL to start a room
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param integer $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param integer $idAccount The id of the account to use
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @param CoreUser $userModel The id of the user for whom the current URL is returned
	 * @param WebinarSession $sessionModel The current session object
	 */
	public function getStartRoomUrl($roomName, $maxParticipants, $idAccount, $api_params, $userModel, $sessionModel) {
		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new TeleskillApiClient($accountSettings->clientcode);
		return $apiClient->getJoinMeetingUrl($api_params['meeting_id'], $userModel->idst, $this->getUserDisplayName($userModel), $userModel->email, TeleskillApiClient::ROLE_HOST);
	}

}