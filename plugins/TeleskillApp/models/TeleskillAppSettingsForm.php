<?php

/**
 * TeleskillAppSettingsForm class.
 *
 * @property string $checkinurl
 * @property string $clientcode
 * @property string $account_name
 * @property string $additional_info
 * @property string $max_rooms
 * @property string $max_rooms_per_course
 * @property string $max_concurrent_rooms
 */
class TeleskillAppSettingsForm extends CFormModel {

	public $checkinurl;
	public $clientcode;
	public $account_name;
	public $additional_info;
	public $max_rooms;
	public $max_rooms_per_course;
	public $max_concurrent_rooms;


	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			array('account_name, checkinurl, clientcode, additional_info, max_rooms, max_rooms_per_course, max_concurrent_rooms', 'safe'),
			array('account_name, checkinurl, clientcode, max_rooms, max_rooms_per_course, max_concurrent_rooms', 'required'),
            array('max_concurrent_rooms, max_rooms, max_rooms_per_course', 'numerical'),
		);
	}


	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array(
            'checkinurl' => 'Checkin url',
            'clientcode' => 'Client code',
			'account_name' => Yii::t('webinar', 'Account Name'),
			'additional_info' => Yii::t('webinar', 'Additional information'),
			'max_rooms' => Yii::t('configuration','Max total rooms'),
			'max_rooms_per_course' => Yii::t('configuration','Max rooms per course'),
			'max_concurrent_rooms' => Yii::t('configuration','Max concurrent rooms'),
		);
	}
}
