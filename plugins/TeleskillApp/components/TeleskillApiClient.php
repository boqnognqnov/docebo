<?php
/**
 *
 * @author Plamen Petkov
 *
 * Copyright (c) 2013 Docebo
 * http://www.docebo.com
 *
 *
 */
class TeleskillApiClient extends CComponent {

    const ACTION_CREATE         = 1;
    const ACTION_DELETE         = 2;
    const ACTION_GETLOGIN       = 3;
    const ACTION_CHECK          = 5;
    const ACTION_UPDATE         = 6;
    const ACTION_GETROOMINFO    = 10;

    const ROLE_ATTENDEE         = 1;
    const ROLE_HOST             = 2;

    public $proxyHost = false;
    public $proxyPort = false;


    // From settings
    private $_checkInUrl = "";
    private $_clientCode = "";


    private $_xmlTmpl = "<?xml version='1.0' encoding='utf-8'?><ews></ews>";

    private $_client = null;



    /**
     * Constructor
     */
    public function __construct($client_code = null) {
		// If the new parameters are not passed, try to load the default webinar tools account
		if(!$client_code) {
			$account = WebinarToolAccount::model()->findByAttributes(array('tool' => 'teleskill'));
			if($account)
				$client_code = $account->settings->clientcode;
		}

        $this->init($client_code);
    }


    /**
     * Initialize component
     *
     */
    public function init($client_code) {

        Yii::import('common.extensions.httpclient.*');

        // Get these from settings
        $this->_checkInUrl = "http://asp.teleskill.it/tvclive/server-1-1.asp";
        //$this->_clientCode = "M730PZ-M441SZ-GOTMA4-6NM91E-OA7R3U-1APAAI-733RVD";
		$this->_clientCode = $client_code;
        //$this->proxyHost = '127.0.0.1';
        //$this->proxyPort = '8888';

    }



    public function canCreateMeeting($startDateTime, $endDateTime, $maxParticipants = false) {
        $xml = new SimpleXMLElement($this->_xmlTmpl);
        $xml->addAttribute('type', self::ACTION_CHECK);
        $xml->addChild('startdate', $startDateTime);
        $xml->addChild('enddate', $endDateTime);
        if ($maxParticipants !== false) {
            $xml->addChild('users', $maxParticipants);
        }

        //CVarDumper::dump($xml,20,true); die;

        // This will throw an exception if there is an error
        $result = $this->call($xml);

        // .. So, return true if ever get to this point
        return true;
    }


    public function createMeeting($roomName, $startDateTime, $endDateTime, $maxParticipants=false) {

//         $canCreate = $this->canCreateMeeting($startDateTime, $endDateTime, $maxParticipants);
//         if ($canCreate !== true) {
//             throw new Exception('Can not create meeting room');
//         }

        $xml = new SimpleXMLElement($this->_xmlTmpl);
        $xml->addAttribute('type', self::ACTION_CREATE);
        $xml->addChild('startdate', $startDateTime);
        $xml->addChild('enddate', $endDateTime);
        $xml->addChild('title', $roomName);

        $callbackUrl = Yii::app()->controller->createAbsoluteUrl("videoconference/teleskillCallback");
        $xml->addChild('callbackurl', $callbackUrl);

        if ($maxParticipants !== false) {
            $xml->addChild('users', $maxParticipants);
        }

        //CVarDumper::dump($xml,20,true); die;

        $result = $this->call($xml);

        //CVarDumper::dump($result,20,true); die;

        if ($result['data']['errorcode'] > 0) {
            throw new Exception($result['errormessage'][''],$result['data']['errorcode']);
        }

        $meetingId = $result['data']['roomid'];

        return $meetingId;
    }




    public function updateMeeting($meetingId, $roomName, $startDateTime, $endDateTime, $maxParticipants=false) {

        $xml = new SimpleXMLElement($this->_xmlTmpl);
        $xml->addAttribute('type', self::ACTION_UPDATE);
        $xml->addChild('roomid', $meetingId);
        $xml->addChild('startdate', $startDateTime);
        $xml->addChild('enddate', $endDateTime);
        $xml->addChild('title', $roomName);

        $callbackUrl = Yii::app()->controller->createAbsoluteUrl("videoconference/teleskillCallback");
        $xml->addChild('callbackurl', $callbackUrl);

        if ($maxParticipants !== false) {
            $xml->addChild('users', $maxParticipants);
        }

        //CVarDumper::dump($xml,20,true); die;

        $result = $this->call($xml);

        if ($result['data']['errorcode'] > 0) {
            throw new Exception($result['errormessage'][''],$result['data']['errorcode']);
        }

        return true;


    }



    public function deleteMeeting($meetingId) {

        $xml = new SimpleXMLElement($this->_xmlTmpl);
        $xml->addAttribute('type', self::ACTION_DELETE);
        $xml->addChild('roomid', $meetingId);

        $result = $this->call($xml);

        //CVarDumper::dump($result,20,true); die;

        if ($result['data']['errorcode'] > 0) {
            throw new Exception($result['errormessage'][''],$result['data']['errorcode']);
        }

        return true;

    }



	public function getJoinMeetingUrl($meetingId, $idUser, $displayName, $email, $role)  {

	    $xml = new SimpleXMLElement($this->_xmlTmpl);
	    $xml->addAttribute('type', self::ACTION_GETLOGIN);

	    $xml->addChild('roomid', $meetingId);
	    $xml->addChild('name', $displayName);
	    $xml->addChild('lang', 'EN');
	    $xml->addChild('email', $email);
	    $xml->addChild('role', $role);
	    $xml->addChild('lmsuserid', $idUser);

	    //CVarDumper::dump($xml,20,true); die;

	    $result = $this->call($xml);

	    if ($result['data']['errorcode'] > 0) {
	        throw new Exception($result['errormessage'][''],$result['data']['errorcode']);
	    }

	    //CVarDumper::dump($result,20,true); die;

	    $url = $result['data']['url'];

	    return $url;



    }


    /**
     * Get user activity (sessions) for a given meeting/room Id
     *
     * @param string $meetingId
     * @throws Exception
     * @return unknown
     */
    public function getUserSessions($meetingId) {
        $xml = new SimpleXMLElement($this->_xmlTmpl);
        $xml->addAttribute('type', self::ACTION_GETROOMINFO);
        $xml->addChild('roomid', $meetingId);
        $xml->addChild('complete', 1);

        $result = $this->call($xml);

        //CVarDumper::dump($result,20,true); die;

        if ($result['data']['errorcode'] > 0) {
            throw new Exception($result['errormessage'][''],$result['data']['errorcode']);
        }

        $sessions = $result['data']['sessions'];

        return $sessions;


    }



    /**
     * Execute the API call, get the response and return an array of data to use/analyze
     *
     * @param object $xml
     * @throws Exception
     * @return array
     */
    private function call($xml) {

        // Add client code and Language
        $xml->addChild('clientcode', $this->_clientCode);
        $xml->addAttribute('lang', 'en');

        $url = $this->_checkInUrl;

        $parsed = parse_url($remote_url);
        $host = $parsed['host'];

        $this->_client = new EHttpClient($url, array(
                'adapter'      => 'EHttpClientAdapterCurl',
                'proxy_host'   => $this->proxyHost,
                'proxy_port'   => $this->proxyPort,
        ));

        $data = urlencode('message').'='.urlencode($xml->asXML());

        //CVarDumper::dump($xml->asXML(),20,true); die;

        $this->_client->setRawData($data);

        $response = $this->_client->request(EHttpClient::POST);

        //CVarDumper::dump($response,20,true); die;

        // Check if we've got an error HTTP status; throw exception with error message
        if ($this->_client->getLastResponse()->isError()) {
            throw new Exception($this->_client->getLastResponse()->getMessage(), $this->_client->getLastResponse()->getStatus());
        }

        if (!$response) {
            throw new Exception('Invalid response from server');
        }

        // Decode response to an array
        $body = $response->getBody();
        $xmlAnswer = null;
        if ($body) {
            // STOP XML runtime errors
            libxml_use_internal_errors(true);

            // Load XML object from request body
            $xmlAnswer = new SimpleXMLElement($body);

            //CVarDumper::dump($xmlAnswer,20,true); die;

            // Collect errors (if result is not valid) and throw exception
            if (!$xmlAnswer){
                $errors = '';
                foreach(libxml_get_errors() as $error) {
                    $errors .= ",\t" . $error->message;
                }
                $error = "XML parsing error(s): $errors";
                throw new Exception($error);
            }
        }

        // Convert to array
        $json = json_encode($xmlAnswer);
        $data = json_decode($json, TRUE);

        //CVarDumper::dump($data,20,true); die;

        // Check if we've got error code from server.
        // NOTE: HTTP status CAN be 200 (OK), but still errocode can be not zero
        if (isset($data["errorcode"]) && (int) $data["errorcode"] > 0) {
            throw new Exception($data["errormessage"], $data["errorcode"]);
        }

        // Always add response status/message
        $result["response_status"] = $this->_client->getLastResponse()->getStatus();
        $result["response_message"] = $this->_client->getLastResponse()->getMessage();
        $result["data"] = $data;

        // Return array
        return $result;

    }


    public function test() {

        //$r = $this->createMeeting('3455', '2013-04-09 18:00:00', '2013-04-09 22:00:00');

        //$r = $this->updateMeeting(21030, 'TEST ME UPDATED', '2013-05-01 11:00:00', '2014-05-01 12:00:00',20);

        //$r = $this->deleteMeeting(21029);

//         $r = $this->getJoinMeetingUrl(21062, Yii::app()->user->id, 2);
//         $link = '<a href="'.$r.'"
// 										onclick="window.open(\''.$r.'\', \'TeleSkill\', \'location=0,status=1,menubar=0,toolbar=0,resizable=1,scrollbars=1,width=1000,height=700\'); return false;"
// 										onkeypress="window.open(\''.$r.'\', \'TeleSkill\', \'location=0,status=1,menubar=0,toolbar=0,resizable=1,scrollbars=1,width=1000,height=700\'); return false;">'
// 									. 'ENTER'
// 									.'</a>';
//         echo $link;
//         CVarDumper::dump($r, 20, true);

//         for ($i = 21070; $i <= 21100; $i++) {
//             try {
//                 $r = $this->getUserSessions($i);
//                 echo "ROOM: $i <br>";
//                 CVarDumper::dump($r, 20, true);
//                 echo "<br>";
//                 echo "<br>";
//             }
//             catch (Exception $e) {

//             }
//         }





    }




}
