<?php
/**
 * AdobeConnect 8 api client
 * @see https://github.com/sc0rp10/AdobeConnect-php-api-client
 * @see http://help.adobe.com/en_US/connect/8.0/webservices/index.html
 * @version 0.1a
 *
 * Copyright 2012, sc0rp10
 * https://weblab.pro
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 *
 * Modified by Docebo
 *
 */
class AdobeConnectApiClient extends CComponent {
    
    public static $MEETINGS_FOLDER_NAME = 'meetings';
    public static $MEETINGS_FOLDER_ID = null;

    private $_basedomain = NULL;
    private $_api_url = NULL;
    private $_folder_id = 0;
    
    private $_username = NULL;
    private $_password = NULL;

    /**
     * @const
     * your personally root-folder id
     * @see http://forums.adobe.com/message/2620180#2620180
     */
    const ROOT_FOLDER_ID = 0; //root folder id

	/**
	 * time-zone-id UTC
	 */
	const TIME_ZONE_UTC = 85;

    /**
     * @var string filepath to cookie-jar file
     */
    private $cookie;

    /**
     * @var resource
     */
    private $curl;

    /**
     * @var bool
     */
    private $is_authorized = false;
    
    /**
     * Constructor
     */
    public function __construct($server_url = null, $admin_user = null, $admin_password = null, $account_id = null) {
		// If the new parameters are not passed, try to load the default webinar tools account
		if(!$server_url && !$admin_user && !$admin_password) {
			if($account_id)
				$account = WebinarToolAccount::model()->findByPk($account_id);
			if(!$account)
				$account = WebinarToolAccount::model()->findByAttributes(array('tool' => 'adobeconnect'));

			if($account) {
				$server_url = $account->settings->server_url;
				$admin_user = $account->settings->admin_username;
				$admin_password = $account->settings->admin_password;
			}
		}

        $this->init($server_url, $admin_user, $admin_password);
    }
    

    /**
     * Init component
     */
    public function init($server_url, $admin_user, $admin_password) {
        $this->_docebo_initialize($server_url, $admin_user, $admin_password);
        $this->cookie = sys_get_temp_dir().DIRECTORY_SEPARATOR.'cookie_'.time().'.txt';
        $ch = curl_init();
        if ( (!ini_get('safe_mode')) && (!ini_get('open_basedir'))) {
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        }
        curl_setopt($ch, CURLOPT_REFERER, $this->_api_url);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookie);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookie);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $this->curl = $ch;
        $this->makeAuth();
    }

    /**
     * Getter: return selected user attribute
     * Selected user: user 
     * @see CComponent::__get()
     */
    public function __get($name) {
        $output = NULL;
        switch (strtolower($name)) {
            case 'username': 
                $output = $this->_username;
                 break;
            case 'password': 
                $output = $this->_password;
                break;
            case 'basedomain':
                $output = $this->_basedomain;
                break;
            case 'api_url': 
                $output = $this->_api_url;
                break;
        }
        return $output;
    }

    /**
     * Setter: Set selected user attribute
     * 
     * @see CComponent::__set()
     */
    public function __set($name, $value) {
        switch (strtolower($name)) {
            case 'username': break; //not settable
            case 'password': break; //not settable
            case 'basedomain':
                $this->_basedomain = $value;
                break;
            case 'api_url': 
                $this->_api_url = $value;
                break;
        }
    }

    /**
     * Initialize 
     * 
     */
    private function _docebo_initialize($server_url, $admin_user, $admin_password) {

        // Get Adobe URL; add "/api/"; Note ending slash!
        $this->_basedomain = $server_url;
        $this->_api_url= rtrim($this->_basedomain, "/") . "/api/";
        
        // Set Client username and password (we always authenticate as admin)
        $this->_username = $admin_user;
        $this->_password = $admin_password;
        
        // Set folder (to root folder... but maybe a parameter)
        $this->_folder_id = self::ROOT_FOLDER_ID;
        
    }

    /**
     * Get all available folders
     * @return array
     */
    public function getAllFolders() {
        return $this->makeRequest('sco-shortcuts');
    }


    /**
     * Make auth-request with stored username and password
     *
     * @param bool|string $username
     * @param bool|string $password
     * @return AdobeConnectApiClient
     */
    public function makeAuth($username = false, $password = false) {
        
        if ($username === false) { 
            $username = $this->_username; 
        }
        if ($password === false) { 
            $password = $this->_password; 
        }
        
        $this->makeRequest('login', array(
                'login'    => $username,
                'password' => $password,
        ));
        $this->is_authorized = true;
        return $this;
    }

    /**
     * Get common info about current authenticated user
     *
     * @return array
     */
    public function getCommonInfo() {
        return $this->makeRequest('common-info');
    }

    /**
     * Create new user
     *
     * @param string $email
     * @param string $password
     * @param string $first_name
     * @param string $last_name
     * @param string $type
     *
     * @return array
     */
    public function createUser ($email, $password, $first_name, $last_name, $type = 'user') {
        $result = $this->makeRequest('principal-update',
                array(
                        'first-name'   => $first_name,
                        'last-name'    => $last_name,
                        'email'        => $email,
                        'password'     => $password,
                        'type'         => $type,
                        'has-children' => 0
                )
        );
        return $result;
    }


    /**
     * Get user by his/her login
     *
     * @param string $login
     * @param bool|string $only_id
     * @throws Exception
     * @return array
     */
    public function getUserByLogin($login, $only_id = false) {
        $result = $this->makeRequest('principal-list',array('filter-login' => $login));
        if (empty($result['principal-list'])) {
            throw new Exception('Cannot find user');
        }
        if ($only_id) {
            return $result['principal-list']['principal']['@attributes']['principal-id'];
        }
        return $result;
    }

    /**
     * Update user fields
     * 
     * @param string $login
     * @param array $data
     * @return array
     */    
    public function updateUser($login, array $data = array()) {
        $principal_id = $this->getUserByLogin($login, true);
        $data['principal-id'] = $principal_id;
        return $this->makeRequest('principal-update', $data);
    }

    /**
     * Retrieve users list
     * 
     * @return array
     */
    public function getUsersList() {
        $users = $this->makeRequest('principal-list');
        $result = array();
        foreach($users['principal-list']['principal'] as $key => $value) {
            $result[$key] = $value['@attributes'] + $value;
        };
        unset($result[$key]['@attributes']);
        return $result;
    }

    
    

    /**
     * get a particulrat meeting, identified by SCO ID
     * 
     * @param string $scoId
     * @return mixed
     */
    public function getMeeting($scoId) {
        return $this->makeRequest('report-my-meetings', array('filter-sco-id' => $scoId));
    }
    
    
    /**
     * get all meetings
     *
     * @return array
     */
    public function getAllMeetings() {
        return $this->makeRequest('report-my-meetings');
    }

    /**
     * create meeting-folder
     *
     * @param string $name
     * @param string $url
     *
     * @return array
     */
    public function createFolder($name, $url) {
        $result = $this->makeRequest('sco-update',array(
                'type'       => 'folder',
                'name'       => $name,
                'folder-id'  => $this->_folder_id,
                'depth'      => 1,
                'url-path'   => $url,
        ));
        return $result['sco']['@attributes']['sco-id'];
    }

    /**
     * create meeting
     *
     * @param int    $folder_id
     * @param string $name
     * @param string $date_begin
     * @param string $date_end
     * @param string $urlPath
     * @internal param string $url
     *
     * @return array
     */
    public function createMeeting($folder_id, $name, $date_begin, $date_end, $urlPath = "") {
        $params = array(
            'type'       => 'meeting',
            'name'       => $name,
            'folder-id'  => $folder_id,
            'date-begin' => $this->formatDateString($date_begin),
            'date-end'   => $this->formatDateString($date_end),
			'time-zone-id' => self::TIME_ZONE_UTC
            );
        
        if (!empty($urlPath)) {
            $params['url-path'] = $urlPath;    
        }

        $meeting = $this->makeRequest('sco-update', $params);
        
        $result = array();
        $result["id"] = $meeting['sco']['@attributes']['sco-id'];
        $result["url-path"] = $meeting['sco']['url-path'];
        $result["meetingurl"] = rtrim($this->_basedomain, "/") . $result["url-path"];
        
        return $result;
    }

    
    /**
     * 
     * @param unknown $sco_id
     * @param unknown $name
     * @param unknown $date_begin
     * @param unknown $date_end
     * @param string $urlPath
     * @return multitype:NULL string
     */
    public function updateMeeting($sco_id, $name, $date_begin, $date_end, $urlPath = "") {
        $params = array(
                'type'       => 'meeting',
                'name'       => $name,
                'sco-id'     => $sco_id,
                'date-begin' => $this->formatDateString($date_begin),
                'date-end'   => $this->formatDateString($date_end),
				'time-zone-id' => self::TIME_ZONE_UTC
        );
    
        if (!empty($urlPath)) {
            $params['url-path'] = $urlPath;
        }
    
        $meeting = $this->makeRequest('sco-update', $params);
    
        $result = array();
        $result["id"] = $meeting['sco']['@attributes']['sco-id'];
        $result["url-path"] = $meeting['sco']['url-path'];
        $result["meetingurl"] = rtrim($this->_basedomain, "/") . $result["url-path"];
    
        return $result;
    }


    /**
     * format date for adobe connect
     * @param string $date
     * @return string
     */
    private function formatDateString($date){
        return str_replace(
            array(':', '+'),
            array('%3A', '%2b'),
            $date . '+00:00'
        );
    }
    
    
    /**
     * invite user to meeting
     *
     * @param int    $meeting_id
     * @param string $login
     *
     * @return mixed
     */
    public function inviteUserToMeeting($meeting_id, $login) {
        $user_id = $this->getUserByLogin($login, true);
        $result = $this->makeRequest('permissions-update', array(
                'principal-id'  => $user_id,
                'acl-id'        => $meeting_id,
                'permission-id' => 'view'
        ));
        return $result;
    }


	/**
	 * Make a request
	 *
	 * @param string $action
	 * @param array $params
	 * @return mixed
	 * @throws Exception
	 */
	private function makeRequest($action, array $params = array())
	{
		$url = $this->_api_url;
		$url .= 'xml?action=' . $action;
        $url .= '&' . urldecode(http_build_query($params));
        $url = str_replace(
            array('&amp;', '%40'),
            array('&', '@'),
            $url
        );
		curl_setopt($this->curl, CURLOPT_URL, $url);
		$result = curl_exec($this->curl);

		libxml_use_internal_errors(true); //let the user handle xml errors, if they occurs
		$xml = simplexml_load_string($result);
		if (!$xml) {
			$log = 'AdobeConnect API error(s) in action "'.$action.'":';
			$errors = '<br />Errors in API response:<br /><ul>';
			foreach (libxml_get_errors() as $error) {
				//print all xml errors as a list
				$log .= "\n".'- (#'.$error->code.') '.$error->message;
				$errors .= '<li>- '.$error->message.'</li>';
			}
			$errors .= '</ul>';
			libxml_clear_errors();
			$log = str_replace("\n\n", "\n", $log); //this is just for human readability in the log message
			$log .= "\nAPI response:\n".(strlen($result) > 200 ? substr($result, 0, 197).'...' : $result); //add API response, but truncate it if it's too long in order not to clog log files
			Yii::log($log, 'error');
			throw new Exception('Coulnd\'t perform the action: ' . $action /*. (!empty($errors) ? $errors : '')*/ );
		}

		// Convert XML into an array! Thank you JSON
		$json = json_encode($xml);
		$data = json_decode($json, TRUE);
		if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') {
			throw new Exception('Coulnd\'t perform the action: ' . $action);
		}

		return $data;
	}



    /**
     * 
     */
    public function __destruct() {
        @curl_close($this->curl);
    }



    /**
     * Set meeting host (person who is hosting the meeting)
     *
     * @param int    $meeting_id
     * @param string $login
     *
     * @return mixed
     */
    public function setMeetingHost($meeting_id, $login) {
        $user_id = $this->getUserByLogin($login, true);

        $result = $this->makeRequest('permissions-update',
                array(
                        'principal-id'  => $user_id,
                        'acl-id'        => $meeting_id,
                        'permission-id' => 'host'
                )
        );
        return $result;
    }


    /**
     * Set a meeting to be public
     *
     * @param int $meeting_id
     * @return mixed
     */
    public function setMeetingPublic($meeting_id) {
        $result = $this->makeRequest('permissions-update',
                array(
                        'principal-id'  => 'public-access',
                        'acl-id'        => $meeting_id,
                        'permission-id' => 'view-hidden'
                )
        );
        return $result;
    }


    /**
     * Return meeting attendance for a given period (optional)
     *
     * @param id $meeting_id
     * @param bool|string $date_start
     * @param bool|string $date_end
     * @return
     */
    public function reportMeetingAttendance($meeting_id, $date_start = false, $date_end = false) {
        $params = array(
                'sco-id' => $meeting_id
        );
        if ($date_start) {
            $params[] = $date_start;
        }
        if ($date_end) {
            $params[] = $date_end;
        }
        $result = $this->makeRequest('report-meeting-attendance', $params);
        return $result['report-meeting-attendance']['row'];
    }


    /**
     * Logout from server
     *
     * @return AdobeConnectApiClient
     */
    public function unmakeAuth() {
        $this->makeRequest('logout');
        $this->is_authorized = false;
        return $this;
    }


    /**
     * Make a test of a given user
     *
     * @param string $username
     * @param string $password
     * @return boolean
     */
    public function testLogin($username, $password) {
        // Logout first if we are authorized
        if ($this->is_authorized) {
            $this->unmakeAuth();
        }

        $output = false;
        $res = $this->makeRequest('login', array(
                'login'    => $username,
                'password' => $password
        ));

        if (!empty($res) && isset($res['status']['@attributes']['code']) && $res['status']['@attributes']['code'] == 'ok') {
            $output = true;
        }
        return $output;
    }
    
    
    /**
     * 
     * @return boolean
     */
    public function isAuthorized() {
        return $this->is_authorized;
    }


    /**
     * Get the AC ID of the folder where we are going to place our meetings/rooms
     *
     * @param string $name
     * @throws Exception
     * @return mixed
     */
    public function getMeetingsFolderId($name = false) {
        $folders = $this->getAllFolders();
        $count = count($folders['shortcuts']['sco']);
        
        if ($count <= 0) {
            throw new Exception('Unable to get list of folders.'); 
        }

        // Search for predefined folder name if not provided as a parameter
        if (empty($name)) {
            $name = self::$MEETINGS_FOLDER_NAME;
        }
        
        for ($i=0; $i < $count; $i++ ) {
            $folder = $folders['shortcuts']['sco'][$i];
            $folderName = $folder['@attributes']['type'];
            if ($folderName == $name) {
                return $folder['@attributes']['sco-id'];
            }
        }
        
        // If we are here.. that means we didn't find the folder
        throw new Exception('Unable to find the requested folder (' . self::$MEETINGS_FOLDER_NAME . ')');
        
    }
    
    

    /**
     * Check if Adobe Connect user exists (by email)
     * 
     * @param string $email
     * @return boolean
     */
    public function userExistsByEmail($email) {
        $result = $this->makeRequest('principal-list',array('filter-email' => $email));
        if (empty($result['principal-list'])) {
            return false;
        }
        return true;
    }
    

    
    /**
     * 
     * @param int $sco_id
     * @return mixed
     */
    public function deleteMeeting($sco_id) {
		try {
        	$result = $this->makeRequest('sco-delete',array('sco-id' => $sco_id));
		} catch(Exception $e) {
			Yii::log('AdobeConnect API error (sco-id = '.$sco_id.'): '.$e->getMessage());
			return true;
		}
        return $result;
    }

    /**
     * get the recording for a specified meeting
     *
     * @param string $scoId
     * @return mixed
     */
    public function retrieveRecordingLink($scoId){
        return $this->makeRequest('list-recordings',array('folder-id' => $scoId));
    }
}

?>