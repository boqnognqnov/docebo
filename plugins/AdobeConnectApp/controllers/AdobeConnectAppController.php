<?php

class AdobeConnectAppController extends Controller {


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		$admin_only_actions = array('settings', 'editAccount');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}

	/**
	 * Renders the edit webinar account form for adobe connect
	 */
	public function actionEditAccount($id_account = null) {
		// Load the webinar account
		if(!$id_account) {
			$account = new WebinarToolAccount();
			$account->tool = 'adobeconnect';
		} else
			$account = WebinarToolAccount::model()->findByPk($id_account);

		// Load the settings form
		$settings = new AdobeConnectAppSettingsForm();
		if (isset($_POST['AdobeConnectAppSettingsForm'])) {
			$settings->setAttributes($_POST['AdobeConnectAppSettingsForm']);
			if ($settings->validate()) {
				// Fix the URL
				$urlRequiredScheme = 'http://';
				$urlRequiredSchemeSecure = 'https://';
				if (0 !== strncmp($settings->server_url, $urlRequiredScheme, strlen($urlRequiredScheme)) && 0 !== strncmp($settings->server_url, $urlRequiredSchemeSecure, strlen($urlRequiredSchemeSecure)))
					$settings->server_url = $urlRequiredScheme . $settings->server_url;

				// Save the settings in the webinar account
				$accountSettings = array();
				$accountSettings['server_url'] = $settings->server_url;
				$accountSettings['admin_username'] = $settings->admin_username;
				$accountSettings['admin_password'] = $settings->admin_password;
				$accountSettings['max_rooms'] = $settings->max_rooms;
				$accountSettings['max_rooms_per_course'] = $settings->max_rooms_per_course;
				$accountSettings['max_concurrent_rooms'] = $settings->max_concurrent_rooms;
				$account->name = $settings->account_name;
				$account->additional_info = $settings->additional_info;
				$account->settings = $accountSettings;
				$account->save();

				// Close the modal
				echo '<a class="auto-close"></a>';
				Yii::app()->end();
			}
		}

		// Load account settings in form
		$accountSettings = $account->settings;
		$settings->account_name = $account->name;
		$settings->server_url = $accountSettings->server_url;
		$settings->admin_username = $accountSettings->admin_username;
		$settings->admin_password = $accountSettings->admin_password;
		$settings->max_rooms = $accountSettings->max_rooms;
		if (empty($settings->max_rooms) && !$id_account)
			$settings->max_rooms = intval(Yii::app()->params['videoconf_max_rooms_default']);
		$settings->max_rooms_per_course = $accountSettings->max_rooms_per_course;
		if (empty($settings->max_rooms_per_course) && !$id_account)
			$settings->max_rooms_per_course = intval(Yii::app()->params['videoconf_max_rooms_per_course_default']);
        // Manage max concurrent rooms limit
        $settings->max_concurrent_rooms = $accountSettings->max_concurrent_rooms;
        if (empty($settings->max_concurrent_rooms) && !$id_account)
            $settings->max_concurrent_rooms = intval(Yii::app()->params['videoconf_max_concurrent_rooms_default']);
		$settings->additional_info = $account->additional_info;

		// Render the edit account view in the modal
		$this->renderPartial('_account_settings', array(
			'modalTitle' => $account->id_account ? Yii::t('webinar', 'Edit webinar account') : Yii::t('webinar', 'New webinar account'),
			'settings' => $settings
		));
	}

	/**
	 * Renders the main adobe settings page
	 */
	public function actionSettings() {
		Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl . '/css/webinar-setup.css');

		$settings = new AdobeConnectAppSettingsForm();
		$this->render('settings', array('settings' => $settings));
	}

	public function actionTest(){
		$apiClient = new AdobeConnectApiClient('https://meet24596238.adobeconnect.com', 'daniele.minoia@docebo.com', 'UyCgX66', 52);
		$sessionDate = WebinarSessionDate::model()->findByAttributes(array(
			'id_session' => 204,
			'day' => '2016-05-17'
		));
		var_dump($sessionDate);
	}

}
