<?php

class AdobeConnectAppModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'AdobeConnectApp.models.*',
			'AdobeConnectApp.components.*',
		));

		Yii::app()->event->on('WebinarCollectTools', array($this, 'installWebinarTool'));
	}

	/**
	 * Installs the webinar tool for adobe connect
	 * @param $event DEvent
	 */
	public function installWebinarTool($event) {
		$event->return_value['tools']['adobeconnect'] = 'AdobeConnectWebinarTool';
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
            /**
             * Check if the user is trying to access this module's endpoint trough the LMS route
             * and if so redirect him to use the ADMIN route, because we need JS and CSS that are already loaded there
             */
            if (strpos(Yii::app()->request->requestUri,'/lms/index.php') === 0) {
                $path = array($controller->module->id, $controller->id, $action->id);
                Yii::app()->controller->redirect(Docebo::createAdminUrl(implode('/', $path)));
            } else {
                return true;
            }
		}
		else
			return false;
	}

	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu() {
		//compose the entire menu
		Yii::app()->mainmenu->addAppMenu('adobe_connect', array(
			'app_initials' => 'AC',
			'settings' => Docebo::createAdminUrl('AdobeConnectApp/adobeConnectApp/settings'),
			'label' => Yii::t('app', 'Adobe Connect'),
			),
			array()
		);
	}
}
