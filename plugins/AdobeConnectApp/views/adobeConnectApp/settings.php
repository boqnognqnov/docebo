<?php
/* @var $form CActiveForm */
/* @var $settings AdobeConnectAppSettingsForm */
?>

<?php DoceboUI::printFlashMessages(); ?>

<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'app-settings-form',
	'htmlOptions'=>array('class' => 'form-horizontal docebo-form'),
));
?>

<a href="<?= ('it' == Yii::app()->getLanguage()) ? 'https://www.docebo.com/it/knowledge-base/elearning-docebo-per-adobe-connect/' : 'https://www.docebo.com/knowledge-base/elearning-docebo-for-adobe-connect/' ?>" target="_blank" class="app-link-read-manual"><?= Yii::t('apps', 'Read Manual'); ?></a>

<h2><?=Yii::t('standard','Settings') ?>: Adobe Connect</h2>
<br>

<div class="control-container odd">
	<div class="control-group">
		<?=CHtml::label(Yii::t('standard', 'Website'), false, array('class'=>'control-label'));?>
		<div class="controls">
			<a class="app-settings-url" href="http://www.adobe.com/it/products/adobeconnect.html" target="_blank">http://www.adobe.com/it/products/adobeconnect.html</a>
		</div>
	</div>
</div>

<div class="control-container even">
	<div class="control-group">
		<?=CHtml::label(Yii::t('webinar', 'Accounts'), false, array('class'=>'control-label'));?>
		<div class="controls">
			<?php
				$this->widget('common.widgets.WebinarAccountsEditor', array(
					'dataProvider' => WebinarToolAccount::model()->dataProvider('adobeconnect'),
					'editAccountUrl' => $this->createUrl('editAccount'),
					'enableMultiAccount' => WebinarTool::getById('adobeconnect')->hasMultiAccountSupport()
				));
			?>
		</div>
	</div>
</div>

<?php $this->endWidget(); ?>