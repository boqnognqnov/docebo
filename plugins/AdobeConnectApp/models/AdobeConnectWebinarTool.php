<?php

/**
 * Class AdobeConnectWebinarTool
 * Implemenents webinar tool for adobe connect
 */
class AdobeConnectWebinarTool extends WebinarTool {
	/**
	 * @var bool does the current webinar tool supports grabbing a recording via API call or automatically identify a viewing URL?
	 */
	protected $apiFindRecording = true;

	/**
	 * Main constructor
	 */
	protected function __construct() {
		$this->id = 'adobeconnect';
		$this->name = 'Adobe Connect';

		$this->playRecordingType = self::PLAY_RECORDING_TYPE_EXTERNAL;
	}

	/**
	 * Returns true if the current webinar tools has support for multi account
	 * @return bool
	 */
	public function hasMultiAccountSupport() {
		return true;
	}

	/**
	 * Creates a new room using the Adobe Connect api
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param string $roomDescription The description of the room
	 * @param integer $idAccount The id of the account to use to create the room
	 * @param $startDateTime string The start date&time in UTC + MYSQL/ISO format
	 * @param $endDateTime string The end date&time in UTC + MYSQL/ISO format
	 * @param $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 *
	 * @return array "tool specific" params returned by the api
	 */
	public function createRoom($roomName, $roomDescription, $idAccount, $startDateTime, $endDateTime, $maxParticipants, $api_params = array()) {
		// Convert date begin and end
		$formatDateTime = new DateTime($startDateTime);
		$dateTimeBegin = $formatDateTime->format('Y-m-d\TH:i');
		$formatDateTime = new DateTime($endDateTime);
		$dateTimeEnd = $formatDateTime->format('Y-m-d\TH:i');

		// Compose and Slugify meeting name
		$meetingName = $this->slugify('Docebo_' . trim(strtolower($roomName)) . '_' . time());

		// Get LMS user who runs this procedure, we gonna create and set him as Host of this meeting (later)
		$userModel = Yii::app()->user->loadUserModel();

		// Get the API client; it will authenticate, internally using Admin credentials
		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new AdobeConnectApiClient($accountSettings->server_url, $accountSettings->admin_username, $accountSettings->admin_password);

		// Get folder id
		$folderId = $apiClient->getMeetingsFolderId();
		if (!$folderId)
			throw new CHttpException(500, "Cannot get meeting folder id");

		// Create meeting/room
		$meeting = $apiClient->createMeeting($folderId, $meetingName, $dateTimeBegin, $dateTimeEnd);
		$meetingId = $meeting["id"];
		$meetingUrlPath = $meeting["url-path"];
		$hostLogin = $accountSettings->admin_username;
		$apiClient->setMeetingHost($meetingId, $hostLogin);
		$apiClient->setMeetingPublic($meetingId);

		// Return full information RE: this meeting for further processing
		$result = array(
			"folder_id" => $folderId,
			"created_by" => $userModel->email,
			"server_url" => $apiClient->basedomain,
			"url_path" => $meetingUrlPath,
			"sco_id" => $meetingId,
			"name" => $meetingName,
		);

		return $result;
	}

	/**
	 * Updates an existing room with the provided params
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param string $roomDescription The description of the room
	 * @param integer $idAccount The id of the account to use to update the room
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @param $startDateTime string The start date&time in UTC + MYSQL/ISO format
	 * @param $endDateTime string The end date&time in UTC + MYSQL/ISO format
	 * @param $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 *
	 * @return array "tool specific" params returned by the api
	 */
	public function updateRoom($roomName, $roomDescription, $idAccount, $api_params = array(), $startDateTime, $endDateTime, $maxParticipants) {
		// Slugify meeting name
		$meetingName = $this->slugify('Docebo_' . trim(strtolower($roomName)) . '_' . time());

		// date time parameters
		$formatDateTime = new DateTime($startDateTime);
		$dateTimeBegin = $formatDateTime->format('Y-m-d\TH:i');
		$formatDateTime = new DateTime($endDateTime);
		$dateTimeEnd = $formatDateTime->format('Y-m-d\TH:i');

		// Get the API client; it will authenticate, internally using Admin credentials
		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new AdobeConnectApiClient($accountSettings->server_url, $accountSettings->admin_username, $accountSettings->admin_password);
		$apiClient->updateMeeting($api_params['sco_id'], $meetingName, $dateTimeBegin, $dateTimeEnd);

		$apiClient->setMeetingHost($api_params['sco_id'], $accountSettings->admin_username);

		// Update local meeting table
		$api_params['name'] = $meetingName;

		return $api_params;
	}

	/**
	 * Deletes an existing room
	 *
	 * @param integer $idAccount The id of the account to use to delete the room
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 */
	public function deleteRoom($idAccount, $api_params = array()) {
		$sco_id = $api_params['sco_id'];

		// Delete remotely
		$account = $this->getAccountById($idAccount, true);
		if($account !== false)
		{
			$accountSettings = $account->settings;
			$apiClient = new AdobeConnectApiClient($accountSettings->server_url, $accountSettings->admin_username, $accountSettings->admin_password);
			$apiClient->deleteMeeting($sco_id);
		}
	}

	/**
	 * Returns the URL to join a room
	 *
	 * @param string         $roomName         The name of the room to create in the videconference platform
	 * @param integer        $maxParticipants  integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param integer        $idAccount        The id of the account to use
	 * @param array          $api_params       An array of "tool specific" params to pass to the api
	 * @param CoreUser       $userModel        The id of the user for whom the current URL is returned
	 * @param WebinarSession $sessionDateModel The current session object
	 */
	public function getJoinRoomUrl($roomName, $maxParticipants, $idAccount, $api_params, $userModel, $sessionDateModel, $widgetContext = false) {
		// Get the previously created/saved server url and url path to the meeting
		$url = rtrim($api_params['server_url'], "/") . $api_params['url_path'];
		$url .= "?guestname=" . urlencode($this->getUserDisplayName($userModel));
		return $url;
	}

	/**
	 * Returns the URL to start a room
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param integer $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param integer $idAccount The id of the account to use
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @param CoreUser $userModel The id of the user for whom the current URL is returned
	 * @param WebinarSessionDate $sessionModel The current session object
	 */
	public function getStartRoomUrl($roomName, $maxParticipants, $idAccount, $api_params, $userModel, $sessionModel) {
		// Get the API client; it will authenticate, internally using Admin credentials
		$accountSettings = $this->getAccountById($idAccount)->settings;
		$url = rtrim($api_params['server_url'], "/") . $api_params['url_path'];
		$url .= "?login=" . $accountSettings->admin_username . "&password=" . $accountSettings->admin_password;
		return $url;
	}

	/**
	 * @param int $idAccount $idAccount The id of the account to use
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @return array
	 */
	public function retrieveRecordingLink($idAccount, $api_params){
		$return = array(
			'error' => '',
			'recordingUrl' => ''
		);
		$account = WebinarToolAccount::model()->findByPk($idAccount);
		$apiClient = new AdobeConnectApiClient($account->settings->server_url, $account->settings->admin_username, $account->settings->admin_password);
		$result = $apiClient->retrieveRecordingLink($api_params['sco_id']);
		if(!empty($result['recordings']['sco'])){
			//$return['recordingUrl'] = rtrim($api_params['server_url'], '/').$result['recordings']['sco']['url-path']."launcher=false&fcsContent=true&pbMode=normal";
			$return['recordingUrl'] = rtrim($api_params['server_url'], '/').$result['recordings']['sco']['url-path'];
		} else {
			$return['error'] = Yii::t('webinar', 'Could not find a recording for this session.');
		}
		return $return;
	}
}