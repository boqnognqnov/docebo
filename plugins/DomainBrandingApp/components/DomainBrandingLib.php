<?php

/**
 * Domain branding event handler
 * Class DomainBrandingLib
 */
class DomainBrandingLib extends CComponent {


    /**
     * Set the logo, login image, player bg AND color scheme for the current organization
     * @param DEvent $event
     */
    public function manageDomainBranding($event) {

		$event->preventDefault();

        // Compute the host to use
        $host = $this->getHost();

        // Check if we need to invalidate the domainbranding session
        if (Yii::app()->session['domainbranding']) {
            if((Yii::app()->user->getIsGuest() && Yii::app()->session['domainbranding']['logged_in']) ||
                (!Yii::app()->user->getIsGuest() && !Yii::app()->session['domainbranding']['logged_in']) ||
                (strcasecmp(Yii::app()->session['domainbranding']['host'], $host) !== 0))
                Yii::app()->session['domainbranding'] = null;
        }

        // get the branding settings and save them in session
        if (!Yii::app()->session['domainbranding']) {
            $branding = null;

            // Load the domain branding settings based on the custom url, if it is set
            $domainBranding = CoreOrgChartBranding::model()->findByAttributes(array('custom_url'=>$host));

            // If we're logged in, override domain-based settings and take user's node settings
            if (!Yii::app()->user->getIsGuest()) {
							$orderCriteria = new CDbCriteria();
							$orderCriteria->order = "t.lev DESC, t.iLeft ASC"; //this ordering is needed by search loop below
                /* @var $orgNodesList CoreOrgChartTree[] */
                $orgNodesList = CoreOrgChartTree::model()->getOrgNodesByUserId( Yii::app()->user->getIdst(), $orderCriteria);
                if (!empty($orgNodesList)) {
                    // Loop through nodes to find the first branding template set
                    foreach ($orgNodesList as $orgChartTree) {
                        /* @var $branding CoreOrgChartBranding */
                        $branding = CoreOrgChartBranding::model()->findByPk($orgChartTree->idOrg);
                        if(!$branding && ($orgChartTree->lev > 1)) {
                            // Search for a template up the hierarchy
                            $ancestors = CoreOrgChartTree::model()->getAncestorsNodes($orgChartTree);
                            foreach($ancestors as $ancestor) {
                                $branding = CoreOrgChartBranding::model()->findByPk($ancestor->idOrg);
                                if ($branding) // Stop at the first valid branding template
                                    break;
                            }
                        }

                        if ($branding) // Stop at the first valid branding template
                            break;
                    }
                }
            }

            // apply domain branding if no custom branding is set for the user's org node
            if (!$branding)
                $branding = $domainBranding;

            if ($branding) {

                // get also the home page title/caption
                $signinForm = new DomainBrandingSigninForm($branding->id_org);
                $signinForm->loadHomepageTextTranslations();

                $domainBrandingSettings = array(
                    'id_org' => $branding->id_org,
                    'host' => $host,
                    'logged_in' => !Yii::app()->user->getIsGuest(),
                    'id_template' => $branding->id_template,
                    'custom_url' => $branding->custom_url,
                    'logo' => $branding->getLogoUrl(),
                    'favicon' => $branding->getFaviconUrl(),
                    'login_img' => $branding->getLoginImgUrl(),
                    'login_img_aspect' => $branding->login_img_aspect,
                    'player_bg' => $branding->getPlayerBgUrl(),
                    'player_bg_aspect' => $branding->player_bg_aspect,
                    'signin_translation_keys' => array(
                        'title' => $signinForm->getTitleTextKey(),
                        'caption' => $signinForm->getCaptionTextKey()
                    ),
                    'signin_translations' => $signinForm->getTranslations()
                );

                // add the white label settings for the current node
                if (PluginManager::isPluginActive('WhitelabelApp')) {
                    $whiteLabelForm = new DomainBrandingWhiteLabelForm($branding->id_org);
                    $domainBrandingSettings['white_label'] = $whiteLabelForm->attributes;
                }

                Yii::app()->session['selected_color_scheme'] = $branding->id_template;
            } else
				$domainBrandingSettings = array(
					'host' => $host,
					'logged_in' => !Yii::app()->user->getIsGuest(),
				);

			Yii::app()->session['domainbranding'] = $domainBrandingSettings;
        }

        if (Yii::app()->session['domainbranding']) {
            $settings = Yii::app()->session['domainbranding'];

            $event->params['logo_image_url'] = ($settings['logo'] && !$event->params['master_logo']) ? $settings['logo'] : $event->params['logo_image_url'];

			$event->params['login_image'] = array (
				'src' => $settings['login_img'] ? $settings['login_img'] : $event->params['login_image']['src'],
				'aspect' => in_array($settings['login_img_aspect'], array('fill', 'tile')) ? $settings['login_img_aspect'] : in_array($event->params['login_image']['aspect'], array('fill', 'tile')) ? $event->params['login_image']['aspect'] : 'fill'
			);

            $event->params['player_bg_image'] = array (
                'src' => $settings['player_bg'] ? $settings['player_bg'] : $event->params['player_bg_image']['src'],
                'aspect' => in_array($settings['player_bg_aspect'], array('fill', 'tile')) ? $settings['player_bg_aspect'] : 'fill'
            );

            $event->params['favicon_url'] = ($settings['favicon'] && !$event->params['master_favicon']) ? $settings['favicon'] : $event->params['favicon_url'];

            if (isset($settings['white_label'])) {
                foreach ($settings['white_label'] as $name => $value) {
                    $event->params[$name] = $value ? $value : $event->params[$name];
                }
            }

            // set the home page title/caption
            $signinTranslationKeys = $settings['signin_translation_keys'];
            $signinTranslations = $settings['signin_translations'];


            if (!empty($signinTranslations)) {
                $targetLang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
                $titleTextKey = $signinTranslationKeys['title'];
                $captionTextKey = $signinTranslationKeys['caption'];
                foreach ($signinTranslations as $_langCode => $_translation) {
                    if ($_langCode == $targetLang) {
                        if ($_translation[$titleTextKey] && !empty($_translation[$titleTextKey]->translation_text))
                            $event->params['signin_title'] = $_translation[$titleTextKey]->translation_text;
                        if ($_translation[$captionTextKey] && !empty($_translation[$captionTextKey]->translation_text))
                            $event->params['signin_caption'] = $_translation[$captionTextKey]->translation_text;
                        break;
                    }
                }
            }

            // filter the home user pages
            if (!empty($event->params['signin_pages'])) {
                $signinPages = $event->params['signin_pages'];
                foreach ($signinPages as $k => $learningWebpageTranslation) {
                    $page = $learningWebpageTranslation->page;
                    // remove the page if it not assigned to the current node
                    if ($page->visible_nodes === 'selection' && !in_array($settings['id_org'],CHtml::listData($page->brandingSettings, 'id_org', 'id_org'))) {
                        unset($signinPages[$k]);
                    }
                }
                $event->params['signin_pages'] = $signinPages;
            }
        }

    }

	/**
	 * Installs the custom item on the orgchart node context menu
	 * @param $event
	 */
	public function renderOrgChartTreeNodeMenu($event) {

        if(Yii::app()->user->getIsGodadmin()) {
            $nodeId = $event->params['id'];
            $html = '<li class="node-action">' .
                CHtml::link(Yii::t('domainbranding', 'Edit branding template'), Docebo::createAppUrl('admin:DomainBrandingApp/DomainBrandingApp/edit', array('id' => $nodeId)), array(
                    'class' => 'edit-branding',
                    'onClick' => 'window.location = this.href'
                )) . '</li>'
				.'<li class="node-action"><hr/></li>';
        } else
            $html = '';

        echo $html;
		return true;
    }

	/**
	 * Checks if we can assign this newly registered node to the orgchart node
	 * @param $event DEvent
	 */
	public function assignUserToBrandingNode($event) {
		$userTemp = $event->params['coreUserTemp']; /* @var $userTemp CoreUserTemp */

		// Check if the user use a reg_code and is already assigned to a node
		$regcode = Yii::app()->request->getParam('reg_code');
		if(!$regcode && isset(Yii::app()->session['domainbranding']['id_org'])) {
			$orgChart = CoreOrgChartTree::model()->findByPk(Yii::app()->session['domainbranding']['id_org']);
			if($orgChart) {
				// Org chart exists, get groups associated
				// with it and subscribe the temp user to them
				$c = new CDbCriteria();
				$c->addInCondition('idst', array($orgChart->idst_oc, $orgChart->idst_ocd));
				$groups = CoreGroup::model()->findAll($c);
				if($groups){
					foreach($groups as $group){
						// Remember to use the 'self-register' scenario
						// to avoid triggering enrollment rules
						// which won't work on temporary user IDs (the
						// user is still at core_user_temp until he is approved
						// by admin or confirms his email (on "free" self-registration mode)
						$exists = CoreGroupMembers::model()->exists('idst = :idst AND idstMember = :idstMember', array(
							'idst' => $group->idst,
							'idstMember' => $userTemp->idst
						));
						if(!$exists) {
							$addMember = new CoreGroupMembers('self-register');
							$addMember->idst = $group->idst;
							$addMember->idstMember = $userTemp->idst;
							$addMember->save();
						}
					}
				}
			}
		}
	}

    public function getHost(){
        $host = false;
        // Compute the host to use
        if(!empty($_GET['domain'])) {
            $eventSubfolder = new DEvent($this, array('subfolder' => $_GET['domain']));
            Yii::app()->event->raise("BeforeSubfolderProcessing", $eventSubfolder);
            if($eventSubfolder->shouldPerformAsDefault()) {
                $host = $_SERVER['HTTP_HOST'];
                $host = $_GET['domain'].".".$host;
            }
            else
                $host = $eventSubfolder->return_value['branding_host'];
        } else {
            // This is needed in case the domainbranding is used with the full url
            $host = $_SERVER['HTTP_HOST'];
        }

        return $host;
    }
}