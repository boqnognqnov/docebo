<?php

/**
 * Plugin for DomainBrandingApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class DomainBrandingApp extends DoceboPlugin {


	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'DomainBrandingApp';

	public $is_mobile_capable = true;

    /**
     * This app has no settings to configure, so hide the configure button in apps marketplace
     * @var bool
     */
    public static $HAS_SETTINGS = false;

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {
		parent::activate();

        try {
            $sql = "CREATE TABLE IF NOT EXISTS `core_org_chart_branding` (
              `id_org` int(11) NOT NULL,
              `id_template` int(11) DEFAULT NULL,
              `logo` varchar(255) DEFAULT NULL,
              `login_img` varchar(255) DEFAULT NULL,
              `player_bg` varchar(255) DEFAULT NULL,
              `custom_url` varchar(255) DEFAULT NULL,
              PRIMARY KEY (`id_org`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            ";

            $command = Yii::app()->db->createCommand($sql);
            $command->execute();

            $sql = "
                ALTER TABLE `core_org_chart_branding` ADD `favicon` VARCHAR(255)  NULL  DEFAULT NULL  AFTER `logo`;
                ALTER TABLE `core_org_chart_branding` ADD `login_img_aspect` VARCHAR(255)  NULL  DEFAULT 'fill'  COMMENT 'tile,fill';
                ALTER TABLE `core_org_chart_branding` ADD `player_bg_aspect` VARCHAR(255)  NULL  DEFAULT 'fill'  COMMENT 'tile,fill';
                ALTER TABLE `core_org_chart_branding`
                  ADD FOREIGN KEY (`id_org`) REFERENCES `core_org_chart_tree` (`idOrg`) ON DELETE CASCADE ON UPDATE CASCADE;
            ";
            $command = Yii::app()->db->createCommand($sql);
            $command->execute();

            $sql = "CREATE TABLE IF NOT EXISTS `learning_webpage_domainbranding` (
              `id_branding` int(11) NOT NULL,
              `id_page` int(11) NOT NULL,
              PRIMARY KEY (`id_branding`,`id_page`),
              constraint `fk_learning_webpage_domainbranding_branding` FOREIGN KEY (`id_branding`) REFERENCES `core_org_chart_branding` (`id_org`) ON DELETE CASCADE ON UPDATE CASCADE,
              constraint `fk_learning_webpage_domainbranding_webpage` FOREIGN KEY (`id_page`) REFERENCES `learning_webpage` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            $command = Yii::app()->db->createCommand($sql);
            $command->execute();

            $sql = "ALTER TABLE `learning_webpage` ADD `visible_nodes` VARCHAR(10)  NOT NULL  DEFAULT 'all'  COMMENT 'all | selection'  AFTER `in_home`";
            $command = Yii::app()->db->createCommand($sql);
            $command->execute();

			// Load all tables of the application in the schema
			Yii::app()->db->schema->getTables();

			// clear the cache of all loaded tables
			Yii::app()->db->schema->refresh();

        } catch(Exception $e) {
            Yii::log("Error activating app: ".$e->getMessage(), CLogger::LEVEL_ERROR);
        }
	}


	public function deactivate() {
		parent::deactivate();
	}


}
