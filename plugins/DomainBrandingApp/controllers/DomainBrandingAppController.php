<?php

class DomainBrandingAppController extends Controller {

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		$admin_only_actions = array('edit', 'axDelete');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}

    /**
     * Action: Edit domain branding settings
     */
    public function actionEdit() {

        Yii::app()->tinymce->init();

        // Check node id
        $nodeId = intval(Yii::app()->request->getParam('id'));
        /* @var $node CoreOrgChart */
        $node = CoreOrgChart::model()->with(array('coreOrgChartTree', 'coreOrgChartBranding'))->findByAttributes(array('id_dir'=>$nodeId, 'lang_code'=>'english'));
        if (!$node)
            $this->redirect(Docebo::createAppUrl('admin:userManagement/index'));


        // Load current branding settings for this node (if any)
        $branding = $node->coreOrgChartBranding;
        if (!$branding) {
            $branding = new CoreOrgChartBranding();
            $branding->id_org = $node->id_dir;
        }

        $brandingForm = new DomainBrandingSettingsForm($branding);
        $signinModel = new DomainBrandingSigninForm($nodeId);
        $pageModel = LearningWebpage::model();

        // Handle form submission
        if (isset($_POST['DomainBrandingSettingsForm'])) {
            $brandingForm->setAttributes($_POST['DomainBrandingSettingsForm']);
            $brandingForm->logo = CUploadedFile::getInstance($brandingForm, 'logo');
            $brandingForm->favicon = CUploadedFile::getInstance($brandingForm, 'favicon');
            $brandingForm->loginImg = CUploadedFile::getInstance($brandingForm, 'loginImg');
            $brandingForm->playerBg = CUploadedFile::getInstance($brandingForm, 'playerBg');

            if (!$brandingForm->validate() || !$brandingForm->save()) {
                Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
                // Reload the previous form values
                $brandingForm = new DomainBrandingSettingsForm($branding);
            }
            else {
                // Raise event to let plugins check the submitted data
                Yii::app()->event->raise('DomainBrandingSettingsFormSubmit', new DEvent($this, array(
                    'id_org' => $nodeId
                )));

                $signinModel->save();
                Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));
            }
        }

        $whiteLabelForm = null;
        if (PluginManager::isPluginActive('WhitelabelApp')) {

            $whiteLabelForm = new DomainBrandingWhiteLabelForm($nodeId);
            if (isset($_POST['DomainBrandingWhiteLabelForm'])) {
                $whiteLabelForm->attributes = $_POST['DomainBrandingWhiteLabelForm'];
                if(!$whiteLabelForm->validate() || !$whiteLabelForm->save()) {
                    // Reload the previous form values
                    $whiteLabelForm = new DomainBrandingWhiteLabelForm($nodeId);
                }
            }
        }

        $signinModel->loadHomepageTextTranslations();

        $this->render('edit', array(
			'nodeId' => $nodeId,
            'brandingForm' => $brandingForm,
            'whiteLabelForm' => $whiteLabelForm,
            'signinModel' => $signinModel,
            'pageModel' => $pageModel,
            'nodeName' => $node->translation(),
            'selectedTab' => isset($_POST['selectedTab']) ? $_POST['selectedTab'] : null
        ));
    }

	public function actionAxDelete() {

		$idOrg = intval( Yii::app()->request->getParam('id_org') );
		$model = CoreOrgChartBranding::model()->findByPk($idOrg);

		if (isset($_POST['CoreOrgChartBranding'])) {
			if ($model->delete()) {

				$this->redirect(Docebo::createAppUrl('admin:userManagement/index'));

				//echo '<a href="" class="auto-close"></a>';
				//return;
			}
		}

		$this->renderPartial('_delete', array(
			'model' => $model
		));
	}
}