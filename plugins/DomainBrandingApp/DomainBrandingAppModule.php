<?php

class DomainBrandingAppModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

        Yii::app()->event->on('GetLogoPath', array(new DomainBrandingLib(), 'manageDomainBranding'));
        Yii::app()->event->on('GetFaviconPath', array(new DomainBrandingLib(), 'manageDomainBranding'));
        Yii::app()->event->on('GetLoginImage', array(new DomainBrandingLib(), 'manageDomainBranding'));
        Yii::app()->event->on('GetPlayerBgImage', array(new DomainBrandingLib(), 'manageDomainBranding'));
        Yii::app()->event->on('GetWhiteLabelSettings', array(new DomainBrandingLib(), 'manageDomainBranding'));
        Yii::app()->event->on('RenderLoginPage', array(new DomainBrandingLib(), 'manageDomainBranding'));
        Yii::app()->event->on('GetSigninTitle', array(new DomainBrandingLib(), 'manageDomainBranding'));
        Yii::app()->event->on('GetSigninCaption', array(new DomainBrandingLib(), 'manageDomainBranding'));
        Yii::app()->event->on('FilterSigninPages', array(new DomainBrandingLib(), 'manageDomainBranding'));
        Yii::app()->event->on('RenderOrgChartTreeNodeMenu', array(new DomainBrandingLib(), 'renderOrgChartTreeNodeMenu'));
		Yii::app()->event->on('AfterUserSelfRegisterSave', array(new DomainBrandingLib(), 'assignUserToBrandingNode'));

		// import the module-level models and components
		$this->setImport(array(
            'DomainBrandingApp.models.*',
            'DomainBrandingApp.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
