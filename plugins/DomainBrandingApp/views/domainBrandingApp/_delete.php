<?php
/* @var $this DomainBrandingAppController */
/* @var $model CoreOrgChartBranding */
/* @var $form CActiveForm */
?>
<h1><?= Yii::t('standard', '_AREYOUSURE') ?></h1>


<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'delete-branding-form',
	'method' => 'POST',
	'htmlOptions' => array(
		'class' => ''
	),
));
echo $form->hiddenField($model, 'id_org');
?>

<?= Yii::t('standard', "_AREYOUSURE") ?>


<div class="form-actions">
	<input class="btn-docebo green big" type="submit" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" />
	<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">

	$(document).delegate(".modal-delete-branding", "dialog2.content-update", function() {
		var e = $(this);

		var autoclose = e.find("a.auto-close");

		if (autoclose.length > 0) {
			e.find('.modal-body').dialog2("close");

			var href = autoclose.attr('href');
			if (href) {
				window.location.href = href;
			}
		}
	});

</script>

