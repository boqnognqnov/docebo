<div class="row" data-page-id="<?php echo $data->id; ?>">
    <div class="title">
        <?php
        $currentLanguageTitle = trim($data->translation->title);
        if (!empty($currentLanguageTitle)) {
            echo $currentLanguageTitle;
        }
        else {
            echo '-';
        }
        ?>
    </div>
    <div class="actions">
        <ul>
            <li class="visibility">
                <?= CHtml::link(
                    '',
                    Docebo::createAbsoluteUrl('admin:branding/index#signin'),
                    array('class'=>'branding-page-visibility '.($data->visible_nodes=='selection' && !in_array(Yii::app()->request->getParam('id'), CHtml::listData($data->brandingSettings, 'id_org', 'id_org')) ? 'grayed' : ''))
                ) ?>
            </li>
        </ul>
    </div>
</div>