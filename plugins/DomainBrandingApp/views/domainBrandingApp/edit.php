<?php
/* @var $brandingForm DomainBrandingSettingsForm */
/* @var $signinModel DomainBrandingSigninForm */
/* @var $pageModel LearningPage */
/* @var $nodeId int */
?>

<style type="text/css">
        /* White Label styles */
    .white-label-preview {
        background: url(<?= Yii::app()->theme->baseUrl ?>/images/branding_white_label.png) no-repeat;
        width: 496px;
        height: 310px;
        position: relative;
    }

    .wlabel-preview {
        position: absolute;
        color: #0465ac;
        font-family: 'Handlee', cursive;
        font-size: 19px;
        line-height: 22px;
        font-weight: bold;
    }

    .wlabel-preview.wl-menu {
        left: -22px;
        top: 85px;
    }
    .wlabel-preview.wl-user-counter {
        left: 429px;
        top: 3px;
        min-width: 120px;
    }
    .wlabel-preview.wl-footer {
        left: 500px;
        top: 288px;
    }

    .white-label-inactive-title {
        font-family: 'Handlee', cursive;
        font-size: 26px;
        font-weight: bold;
        color: #5ABF57;
        margin-top: 20px;
    }

    .white-label-inactive-text li {
        line-height: 20px;
        margin-top: 10px;
        float: left;
    }

    .white-label-inactive-text li div {
        font-family: 'Handlee', cursive;
        font-size: 20px;
        color: #303030;
        float: left;
    }
    /* fix web pages rows margin */
    .list-view .items > .row {
        margin-left: 0;
    }
</style>

<?php
/* @var $this DomainBrandingAppController */
/* @var $form CActiveForm */
/* @var $brandingForm DomainBrandingSettingsForm */
/* @var $whiteLabelForm DomainBrandingWhiteLabelForm */
/* @var $nodeName string */
$this->breadcrumbs = array(
    Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
    Yii::t('menu', '_USER_MANAGMENT') => Docebo::createAppUrl('admin:userManagement/index'),
    Yii::t('domainbranding', 'Edit branding template')
);
?>

    <h4>
        <a class="docebo-back" href="<?=Docebo::createAppUrl('admin:userManagement/index')?>">
            <span><?=Yii::t('standard', '_BACK')?></span>
        </a>
        <?= Yii::t('domainbranding', 'Branding scheme for node "{node_name}"', array('{node_name}'=>$nodeName)) ?>
    </h4>
    <hr>

<?php DoceboUI::printFlashMessages(); ?>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'branding-assigntemplate-form',
    'method' => 'POST',
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
        'class' => 'form-horizontal docebo-form'
    )
));
?>


<div class="docebo-modal-tabs clearfix">
    <ul class="nav nav-tabs" id="whitelabel-tab">
        <li class="active">
            <a href="#" data-toggle="tab" data-target="#domainbranding-branding">
                <span style="margin-right: 10px;" class="docebo-sprite ico-branding"></span>
                <span style="margin-right: 10px;" class="docebo-sprite ico-branding active"></span>
                <?= Yii::t('domainbranding', 'Branding template') ?>
            </a>
        </li>
        <?php if ($whiteLabelForm) : ?>
        <li>
            <a href="#" data-toggle="tab" data-target="#domainbranding-whitelabel">
                <span style="margin-left: 3px;" class="docebo-sprite ico-bookmark"></span>
                <span style="margin-left: 3px;" class="docebo-sprite ico-bookmark active"></span>
                <?= Yii::t('templatemanager', 'White Label') ?>
            </a>
        </li>
        <?php endif; ?>
        <li>
            <a href="#" data-toggle="tab" data-target="#domainbranding-signin">
                <span style="margin-right: 13px;" class="docebo-sprite ico-home"></span>
                <span style="margin-right: 13px;" class="docebo-sprite ico-home active"></span>
                <?= Yii::t('templatemanager', 'Login page layout') ?>
            </a>
        </li>
		<?php
		// Raise event to let plugins add new tabs
		Yii::app()->event->raise('RenderDomainBrandingTabs', new DEvent($this, array()));
		?>
    </ul>

    <div class="tab-content">

        <div class="tab-pane active" id="domainbranding-branding">


            <div class="control-container">
                <div class="control-group">
                    <?= $form->labelEx($brandingForm, 'templateId', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <p class="help-block"><?= Yii::t('setup', 'Choose the color scheme for your new Docebo') ?></p>
                        <?= $form->dropDownList($brandingForm, 'templateId', $brandingForm->getTemplateDropdownList(), array(
                            'class' => ''
                        )) ?>
                    </div>
                </div>
            </div>

            <div class="control-container odd">
                <div class="control-group">
                    <?= $form->labelEx($brandingForm, 'customUrl', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?= $form->textField($brandingForm, 'customUrl', array(
                            'class' => '',
                            'placeholder' => ''
                        )) ?>
                    </div>
                </div>
            </div>

            <div class="control-container">
                <div class="control-group">
                    <div class="control-label">
                        <?= $form->labelEx($brandingForm, 'logo', array('class' => '')) ?>
                        <p class="help-block"><?= Yii::t('branding', '_COMPANY_LOGO_DESCRIPTION') ?></p>
                    </div>
                    <div class="controls">
                        <?php if (is_string($brandingForm->logo) && strlen($brandingForm->logo)) : ?>
                            <p><strong><?= Yii::t('domainbranding', 'Current logo') ?></strong></p>
                            <div class="row-fluid">
                                <div class="thumbnail span4">
                                    <img src="<?= $brandingForm->branding->getLogoUrl() ?>" alt=""/>
                                </div>
                            </div>
                        <?php endif; ?>
                        <br>
                        <?= $form->fileField($brandingForm, 'logo', array(
                            'class' => ''
                        )) ?>
                    </div>
                </div>
            </div>

            <div class="control-container odd">
                <div class="control-group">
                    <div class="control-label">
                        <?= $form->labelEx($brandingForm, 'favicon', array('class' => '')) ?>
                        <p class="help-block"><?= Yii::t('branding', 'Upload a 16x16 png/ico image that will represent your website\'s favicon.') ?></p>
                    </div>
                    <div class="controls">
                        <?php if (is_string($brandingForm->favicon) && strlen($brandingForm->favicon)) : ?>
                            <div class="row-fluid">
                                <div class="thumbnail span4">
                                    <img src="<?= $brandingForm->branding->getFaviconUrl() ?>" alt=""/>
                                </div>
                            </div>
                        <?php endif; ?>
                        <br>
                        <?= $form->fileField($brandingForm, 'favicon', array(
                            'class' => ''
                        )) ?>
                    </div>
                </div>
            </div>

            <div class="control-container">
                <div class="control-group">
                    <div class="control-label">
                        <?= $form->labelEx($brandingForm, 'loginImg', array('class' => '')) ?>
                        <p class="help-block"><?= Yii::t('branding', '_HOME_LOGIN_IMG_DESCRIPTION') ?></p>
                    </div>
                    <div class="controls">
                        <?php if (is_string($brandingForm->loginImg) && strlen($brandingForm->loginImg)) : ?>
                            <p><strong><?= Yii::t('templatemanager', 'Current background image') ?></strong></p>
                            <div class="row-fluid">
                                <div class="thumbnail span4">
                                    <img src="<?= $brandingForm->branding->getLoginImgUrl() ?>" alt=""/>
                                </div>
                                <div class="span8">
                                    <?= $form->labelEx($brandingForm, 'loginImgAspect', array(
                                        'label' => $form->radioButton($brandingForm, 'loginImgAspect', array('id'=>'loginImgAspect_opt_a', 'value'=>'fill', 'uncheckValue'=>NULL)) . ' ' . Yii::t('branding', '_HOME_IMG_POSITION_FILL', array('{w}'=>1100, '{h}'=>410)),
                                        'for' => 'loginImgAspect_opt_a',
                                        'class' => 'radio'
                                    )) ?>
                                    <br>
                                    <?= $form->labelEx($brandingForm, 'loginImgAspect', array(
                                        'label' => $form->radioButton($brandingForm, 'loginImgAspect', array('id'=>'loginImgAspect_opt_b', 'value'=>'tile', 'uncheckValue'=>NULL)) . ' ' . Yii::t('branding', '_HOME_IMG_POSITION_TILE'),
                                        'for' => 'loginImgAspect_opt_b',
                                        'class' => 'radio'
                                    )) ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <br>
                        <?= $form->fileField($brandingForm, 'loginImg', array(
                            'class' => ''
                        )) ?>
                    </div>
                </div>
            </div>

            <div class="control-container odd">
                <div class="control-group">
                    <?= $form->labelEx($brandingForm, 'playerBg', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php if (is_string($brandingForm->playerBg) && strlen($brandingForm->playerBg)) : ?>
                            <p><strong><?= Yii::t('templatemanager', 'Current background image') ?></strong></p>
                            <div class="row-fluid">
                                <div class="thumbnail span4">
                                    <img src="<?= $brandingForm->branding->getPlayerBgUrl() ?>" alt=""/>
                                </div>
                                <div class="span8">
                                    <?= $form->labelEx($brandingForm, 'playerBgAspect', array(
                                        'label' => $form->radioButton($brandingForm, 'playerBgAspect', array('id'=>'playerBgAspect_opt_a', 'value'=>'fill', 'uncheckValue'=>NULL)) . ' ' . Yii::t('branding', '_HOME_IMG_POSITION_FILL', array('{w}'=>1100, '{h}'=>410)),
                                        'for' => 'playerBgAspect_opt_a',
                                        'class' => 'radio'
                                    )) ?>
                                    <br>
                                    <?= $form->labelEx($brandingForm, 'playerBgAspect', array(
                                        'label' => $form->radioButton($brandingForm, 'playerBgAspect', array('id'=>'playerBgAspect_opt_b', 'value'=>'tile', 'uncheckValue'=>NULL)) . ' ' . Yii::t('branding', '_HOME_IMG_POSITION_TILE'),
                                        'for' => 'playerBgAspect_opt_b',
                                        'class' => 'radio'
                                    )) ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <br>
                        <?= $form->fileField($brandingForm, 'playerBg', array(
                            'class' => ''
                        )) ?>
                    </div>
                </div>
            </div>

        </div>

        <?php if ($whiteLabelForm) : ?>
        <div class="tab-pane" id="domainbranding-whitelabel">

            <div class="control-container odd">
                <div class="control-group">
                    <div class="control-label">
                        <p class="help-block"><?php echo Yii::t('branding', 'Hide some Docebo\'s custom branding elements from your Docebo E-Learning Platform'); ?></p>
                    </div>
                    <div class="controls">
                        <div class="white-label-preview">
                            <span class="wlabel-preview wl-menu"><?=Yii::t('adminrules', '_ADMIN_MENU')?></span>
                            <span class="wlabel-preview wl-user-counter"><?=Yii::t('templatemanager', 'User Counter')?></span>
                            <span class="wlabel-preview wl-footer"><?=Yii::t('templatemanager', 'Foooter')?></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="control-container">
                <div class="control-group">
                    <div class="control-label">
                        <?php echo Yii::t('templatemanager', 'Foooter'); ?>
                    </div>
                    <div class="controls">
                        <?= CHtml::label(
                            $form->radioButton($whiteLabelForm, 'whitelabel_footer', array('value' => 'show_docebo', 'id' => 'whitelabel_footer_1', 'uncheckValue'=>null)). ' ' . Yii::t('branding', 'Show "Powered by Docebo" in the footer', array('{year}'=>date('Y'))),
                            'whitelabel_footer_1',
                            array('class' => 'radio')
                        ) ?>
                        <?= CHtml::label(
                            $form->radioButton($whiteLabelForm, 'whitelabel_footer', array('value' => 'hide_docebo', 'id' => 'whitelabel_footer_2', 'uncheckValue'=>null)) . ' ' . Yii::t('branding', 'Hide "Powered by Docebo" in the footer', array('{year}'=>date('Y'))),
                            'whitelabel_footer_2',
                            array('class' => 'radio')
                        ) ?>
                        <?= CHtml::label(
                            $form->radioButton($whiteLabelForm, 'whitelabel_footer', array('value' => 'custom_text', 'id' => 'whitelabel_footer_3', 'uncheckValue'=>null)) . ' ' . Yii::t('branding', 'Use my own custom text'),
                            'whitelabel_footer_3',
                            array('class' => 'radio')
                        ) ?>
                        <?php echo $form->textField($whiteLabelForm, 'whitelabel_footer_text', array('class' => 'input-block-level')) ?>
                    </div>
                </div>
            </div>

            <div class="control-container odd">
                <div class="control-group">
                    <div class="control-label">
                        <?php echo Yii::t('templatemanager', 'Naming'); ?>
                        <?/*= $form->labelEx($model, 'loginImg', array('class' => '')) */?>
                    </div>
                    <div class="controls">
                        <?= CHtml::label(
                            $form->checkbox($whiteLabelForm, 'whitelabel_naming', array('id' => 'whitelabel_naming')) . ' ' . Yii::t('branding', 'Replace, in every text, the word Docebo with the word'),
                            'whitelabel_naming',
                            array('class' => 'checkbox')
                        ) ?>
                        <?php echo $form->textField($whiteLabelForm, 'whitelabel_naming_text', array('class' => 'input-block-level')) ?>
                        <br/>
                        <br/>

                        <label for="BrandingWhiteLabelForm_whitelabel_naming_site" class="no-space">
                            <?php echo Yii::t('branding', 'Replace, where used, the website link www.docebo.com with'); ?>
                        </label>
                        <?php echo $form->textField($whiteLabelForm, 'whitelabel_naming_site', array('class' => 'input-block-level')) ?>
                    </div>
                </div>
            </div>

        </div>
        <?php endif; ?>

        <div class="tab-pane" id="domainbranding-signin">

            <div class="control-container">
                <div class="control-group">
                    <div class="control-label">
                        <?php echo Yii::t('branding', '_TITLE_TEXT_SECTION'); ?>
                        <p class="help-block"><?php echo Yii::t('branding', '_TITLE_TEXT_SECTION_DESCRIPTION'); ?></p>
                    </div>
                    <div class="controls">
                        <div class="languages">
                            <p><?php echo Yii::t('branding', '_PAGE_LANGUAGE_LABEL'); ?></p>
                            <div class="lang-wrapper">
                                <?php echo CHtml::listBox('languages', '0', array('0' => Yii::t('adminrules', '_SELECT_LANG_TO_ASSIGN')) + $signinModel->getLanguages(), array('size' => 1, 'id' => 'login-pages-lang-select', 'class' => 'language-selector')); ?>
                                <div class="stats">
                                    <p class="available"><?php echo Yii::t('admin_lang', '_LANG_ALL'); ?>: <span><?php echo count($signinModel->getLanguages()); ?></span></p>
                                    <p class="assigned"><?php echo Yii::t('standard', 'Filled languages'); ?>: <span></span></p>
                                </div>
                            </div>
                            <div class="input-fields">
                                <p><?php echo Yii::t('standard', '_TITLE'); ?></p>
                                <div class="max-input">
                                    <?php echo CHtml::textfield(Yii::t('standard', '_TITLE'), '', array('class' => 'title')); ?>
                                </div>
                                <p><?php echo Yii::t('standard', '_TEXTOF'); ?></p>
                                <?php echo CHtml::textarea(Yii::t('branding', '_TITLE_TEXT_SECTION'), '', array('class' => 'text')); ?>
                            </div>
                            <div class="translations" style="display: none;">
                                <?php
                                    $_translations = $signinModel->getTranslations();
                                    $_titleTextKey = $signinModel->getTitleTextKey();
                                    $_captionTextKey = $signinModel->getCaptionTextKey();
                                ?>
                                <?php foreach ($_translations as $_langCode => $_translation): ?>
                                    <div class="<?php echo $_langCode ?>">
                                        <?php echo $form->textfield($_translation[$_titleTextKey], 'translation_text', array('name' => "CoreLangTranslation[$_langCode][{$_translation[$_titleTextKey]->id_text}][translation_text]")); ?>
                                        <?php echo $form->textarea($_translation[$_captionTextKey], 'translation_text', array('name' => "CoreLangTranslation[$_langCode][{$_translation[$_captionTextKey]->id_text}][translation_text]")); ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="control-container odd">
                <div class="control-group">
                    <div class="control-label">
                        <?php echo Yii::t('admin_webpages', '_TITLE_WEBPAGES'); ?>
                        <p class="help-block"><?php echo Yii::t('branding', 'Shortcut to set the visibility of your pages for this node. To manage your pages {url}click here{/url}', array('{url}'=>'<a href="'.Docebo::createAbsoluteUrl('admin:branding/index#signin').'">','{/url}'=>'</a>')); ?></p>
                    </div>
                    <div class="controls">
                        <div class="items-sortable-wrapper">
                            <?= CHtml::link(
                            Yii::t('standard', '_MANAGE'),
                            Docebo::createAbsoluteUrl('admin:branding/index#signin'),
                            array('class'=>'btn btn-docebo green big pull-right')
                            ) ?>
                            <h6><?php echo Yii::t('configuration', '_PAGE_TITLE'); ?></h6>
                            <?php $this->widget('zii.widgets.CListView', array(
                                'id' => 'external-pages-management-list',
                                'htmlOptions' => array('class' => 'list-view clearfix'),
                                'dataProvider' => $pageModel->dataProvider(),
                                'itemView' => '_view_webpages',
                                // 'itemsCssClass' => 'items clearfix',
                                'template' => '{items}',
                                'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
                                'ajaxUpdate' => true,
                            )); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

		<?php
		// Raise event to let plugins add new tabs
		Yii::app()->event->raise('RenderDomainBrandingTabsContent', new DEvent($this, array('id_org'=>$nodeId)));
		?>
    </div>

</div>

<div class="form-actions">
    <button type="submit" class="btn btn-docebo green big"><?= Yii::t('standard', '_SAVE') ?></button>
	<?php if ( ! $brandingForm->branding->getIsNewRecord() ) : ?>
	<a href="<?= $this->createUrl('axDelete', array('id_org'=>$brandingForm->branding->id_org)) ?>" class="btn btn-docebo red big open-dialog" data-dialog-class="modal-delete-branding"><?= Yii::t('standard', '_DEL') ?></a>
	<?php endif; ?>
</div>

<?php echo CHtml::hiddenField('selectedTab', $selectedTab, array('id' => 'selectedTab')); ?>
<?php $this->endWidget(); ?>


<script type="text/javascript">

    function populateAssignedLanguages() {
        var count = 0;

        var assigned = $('.assigned span');
        var set = $('.translations > div');

        set.each(function() {
            if ($(this).find('input').val() != "" || $(this).find('textarea').val() != "") {
                count++;
            }
        });

        assigned.html(count);
    }

    $(function(){

        $('#<?= CHtml::activeId($brandingForm, 'logo') ?>').styler({browseText:'<?=Yii::t('setup', 'Upload your logo')?>'});
        $('#<?= CHtml::activeId($brandingForm, 'favicon') ?>').styler({browseText:'<?=Yii::t('standard', '_UPLOAD')?>'});
        $('#<?= CHtml::activeId($brandingForm, 'loginImg') ?>').styler({browseText:'<?=Yii::t('templatemanager', 'Login background image')?>'});
        $('#<?= CHtml::activeId($brandingForm, 'playerBg') ?>').styler({browseText:'<?=Yii::t('setup', 'Upload your background')?>'});

        $('#domainbranding-branding input:not(:file), #domainbranding-whitelabel input:not(:file)').styler({});

        // Intercept tab toggle event to save the current tab
        $('#branding-assigntemplate-form a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $('#selectedTab').val($(e.target).data('target'));
        });

        // Open the currently selected tab (if any)
        var initialTab = '#whitelabel-tab a[data-target="<?= ($selectedTab ? $selectedTab : '#domainbranding-branding') ?>"]';
        $(initialTab).tab('show');

        initTinyMCE('.input-fields textarea', 150);

        $('.language-selector').live('change', function() {
            var container = $(this).closest('.languages');

            var prevSelection = $(this).data('prevSelection');
            var selected = $(this).val();
            var inputTitle = container.find('.input-fields input[type="text"]');
            var inputText = container.find('.input-fields textarea');

            var tinyMCEContent = tinymce.activeEditor.getContent();

            if (prevSelection) {
                container.find('.translations .' + prevSelection).find('input[type="text"]').val(inputTitle.val());
                container.find('.translations .' + prevSelection).find('textarea').val(tinyMCEContent);
            }

            if (selected != 0) {
                var selectedLanguage = container.find('.translations .' + selected);

                inputTitle.val(selectedLanguage.find('input[type="text"]').val());
                tinymce.activeEditor.setContent(selectedLanguage.find('textarea').val());
            } else {
                inputTitle.val('');
                tinymce.activeEditor.setContent('');
            }

            $(this).data('prevSelection', $(this).val());
        });

        $('#branding-assigntemplate-form').live('submit', function() {
            $('.language-selector').trigger('change');
            return true;
        });

        $('.input-fields .title, .input-fields .text').live('blur', function() {
            $(this).closest('.languages').find('select.language-selector').trigger('change');
            populateAssignedLanguages();
        });
    });

</script>