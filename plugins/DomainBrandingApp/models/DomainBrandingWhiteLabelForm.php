<?php

class DomainBrandingWhiteLabelForm extends CFormModel {

    protected $_orgNodeId;

    public $whitelabel_footer;
    public $whitelabel_footer_text;
    public $whitelabel_naming;
    public $whitelabel_naming_text;
    public $whitelabel_naming_site;

    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            array('whitelabel_footer, whitelabel_footer_text, whitelabel_naming, whitelabel_naming_text, whitelabel_naming_site', 'safe')
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'whitelabel_footer' => Yii::t('templatemanager', 'Foooter'),
            'whitelabel_naming' => Yii::t('templatemanager', 'Naming'),
            'whitelabel_naming_text' => Yii::t('configuration', '_URL'),
        );
    }

    public function __construct($orgNodeId) {
        $this->_orgNodeId = $orgNodeId;

        $this->getOrgNodeSettings();
    }

    protected function getDefaultWhiteLabelFields() {
        $fields = array(
            'whitelabel_footer',
            'whitelabel_footer_text',
            'whitelabel_naming',
            'whitelabel_naming_text',
            'whitelabel_naming_site',
        );
        return $fields;
    }

    /**
     * Returns the current org node's specific white label settings,
     * or the system wide white label settings if there aren't any specific settings
     * @return CActiveRecord[]
     */
    protected function getOrgNodeSettings() {
        $settings = array();

        $defaultFields = $this->getDefaultWhiteLabelFields();
        $specificFields = array();
        $specificToDefaultFieldsMap = array();

        // build the specific fields array
        foreach ($defaultFields as $field) {
            $name = $field . '_' . $this->_orgNodeId;
            $specificFields[] = $name;
            $specificToDefaultFieldsMap[$name] = $field;
        }

        // find the whitelabel settings specific for the current org node id
        $criteria = new CDbCriteria();
        $criteria->addInCondition('param_name', $specificFields);
        $results = CoreSetting::model()->findAll($criteria);

        if (!empty($results))
        {
            // load the current form's attributes with these settings' values
            foreach ($results as $setting) {
                $settingName = $setting->param_name;
                $settingValue = $setting->param_value;

                // find the default field name
                $field = $specificToDefaultFieldsMap[$settingName];
                $this->$field = $settingValue;
            }
            return;
        }


        // if no specific settings for the current org node id, get the default settings
        $criteria = new CDbCriteria();
        $criteria->addInCondition('param_name', $defaultFields);
        $results = CoreSetting::model()->findAll($criteria);
        if (!empty($results))
        {
            foreach ($results as $setting) {
                $settingName = $setting->param_name;
                $settingValue = $setting->param_value;
                $this->$settingName = $settingValue;
            }
        }
    }

    public function save()
    {
        foreach ($this->attributes as $param => $value) {
            // create a new set of white label settings specific for the current org node
            $settingName = $param . '_' . $this->_orgNodeId;
            Settings::save($settingName, $value);
        }
    }
}