<?php

class DomainBrandingSigninForm extends CFormModel {

    protected $_orgChartNodeId;
    protected $_params;
    protected $_languages;
    protected $_translations;

    public function __construct($orgChartNodeId) {
        parent::__construct();
        $this->_orgChartNodeId = $orgChartNodeId;

    }

    public function save() {
        if (!empty($_POST['CoreLangTranslation'])) {
            foreach ($_POST['CoreLangTranslation'] as $langCode => $translations) {
                foreach ($translations as $idText => $translation) {
                    $t = CoreLangTranslation::model()->findByAttributes(array('lang_code' => $langCode, 'id_text' => $idText));

                    if (empty($t)) {
                        $t = new CoreLangTranslation;
                        $t->id_text = $idText;
                        $t->lang_code = $langCode;
                        $t->translation_status = 'new';
                    }

                    $t->translation_text = $translation['translation_text'];
                    $t->save_date = Yii::app()->localtime->toLocalDateTime();
                    $t->save(false); //false to allow empty text, i.e. to clear the values
                }
            }
            $this->clearLangCache();
        }
    }

    protected function clearLangCache() {

        //analyze input
        $arrIdText = array();
        if (!empty($_POST['CoreLangTranslation'])) {
            foreach ($_POST['CoreLangTranslation'] as $langCode => $translations) {
                foreach ($translations as $idText => $translation) {
                    if (!in_array((int)$idText, $arrIdText)) {
                        $arrIdText[] = (int)$idText;
                    }
                }
            }
        }

        if (empty($arrIdText)) { return true; } //no cache to upgrade

        //retrieve all input's text modules
        $categories = array();
        $criteria = new CDbCriteria();
        $criteria->addInCondition('id_text', $arrIdText);
        $list = CoreLangText::model()->findAll($criteria);
        foreach ($list as $item) {
            if (!in_array($item->text_module, $categories)) {
                $categories[] = $item->text_module;
            }
        }

        if (empty($categories)) { return true; } //no cache to upgrade

        //clean text modules in cache
        foreach ($categories as $category) {
            Yii::app()->messages->resetCache($category);
        }

        return true;
    }

    public function getTitleTextKey() {
        return '_LOGIN_MAIN_TITLE_'.$this->_orgChartNodeId;
    }

    public function getCaptionTextKey() {
        return '_LOGIN_MAIN_CAPTION_'.$this->_orgChartNodeId;
    }

    public function loadHomepageTextTranslations() {

        // make sure that these keys exist: _LOGIN_MAIN_TITLE and _LOGIN_MAIN_CAPTION
        $titleTextKey = $this->getTitleTextKey();
        $textKeyTitle = CoreLangText::model()->findByAttributes(array('text_key'=>$titleTextKey));
        if (!$textKeyTitle) {
            $textKeyTitle = new CoreLangText();
            $textKeyTitle->text_key = $titleTextKey;
            $textKeyTitle->text_module = 'login';
            $textKeyTitle->context = 'login';
            $textKeyTitle->save();
        }
        $captionTextKey = $this->getCaptionTextKey();
        $textKeyCaption = CoreLangText::model()->findByAttributes(array('text_key'=>$captionTextKey));
        if (!$textKeyCaption) {
            $textKeyCaption = new CoreLangText();
            $textKeyCaption->text_key = $captionTextKey;
            $textKeyCaption->text_module = 'login';
            $textKeyCaption->context = 'login';
            $textKeyCaption->save();
        }

        // Sign in section
        $languages = CoreLangLanguage::getActiveLanguages();

        $textKeys = array($textKeyTitle, $textKeyCaption);
        $textsIds = CHtml::listData($textKeys, 'id_text', '');
        $textsByKey = array();
        foreach ($textKeys as $text) {
            $textsByKey[$text->id_text] = $text;
        }

        $criteria = new CDbCriteria();
        $criteria->addInCondition('id_text', array_keys($textsIds));
        $foundTranslations = CoreLangTranslation::model()->findAll($criteria);
        $translationsByIdText = array();

        // here we group the found translations into an array['id_text']['lang_code'] = $translation
        if (!empty($foundTranslations)) {
            foreach ($foundTranslations as $translation) {
                if (!isset($translationsByIdText[$translation->id_text]))
                    $translationsByIdText[$translation->id_text] = array();
                $translationsByIdText[$translation->id_text][$translation->lang_code] = $translation;
            }
        }

        // There MIGHT be NO translations in DB at all!
        $arrTranslations = array();

        // fill in all languages translations
        foreach ($languages as $langCode => $langName) {
            foreach ($textKeys as $text) {

                // did we find the translation ?

                if (!empty($translationsByIdText[$text->id_text][$langCode])) {

                    $arrTranslations[$langCode][$text->text_key] = $translationsByIdText[$text->id_text][$langCode];
                }

                else {

                    $newLang = new CoreLangTranslation();
                    $newLang->id_text = $text->id_text;

                    $arrTranslations[$langCode][$text->text_key] = $newLang;
                }
            }

            // add * for each language in the languages dropdown to indicate that a translation exists
            $translation = $arrTranslations[$langCode][$titleTextKey];
            $caption = $arrTranslations[$langCode][$captionTextKey];
            if (!empty($translation->translation_text) || !empty($caption->translation_text)) {
                $languages[$langCode] = "$langName *";
            }
        }

        $this->_languages = $languages;
        $this->_translations = $arrTranslations;
    }

    public function getLanguages() {
        return $this->_languages;
    }

    public function getTranslations() {
        return $this->_translations;
    }
} 