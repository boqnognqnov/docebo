<?php
/**
 * Class DomainBrandingSettingsForm
 * Handles form subnmission
 */
class DomainBrandingSettingsForm extends CFormModel {
    /**
     * @var string
     */
    public $title;
    /**
     * @var CUploadedFile
     */
    public $logo;
    /**
     * @var CUploadedFile
     */
    public $favicon;
    /**
     * @var CUploadedFile
     */
    public $loginImg;
    /**
     * @var string
     */
    public $loginImgAspect;
    /**
     * @var CUploadedFile
     */
    public $playerBg;
    /**
     * @var string
     */
    public $playerBgAspect;
    /**
     * @var int
     */
    public $templateId;
    /**
     * @var string
     */
    public $customUrl;
    /**
     * @var CoreOrgChartBranding
     */
    public $branding;

    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            array('title, templateId, customUrl, loginImgAspect, playerBgAspect', 'safe'),
            array('templateId', 'required'),
            array('logo, loginImg, playerBg', 'file', 'types'=>'jpg, jpeg, gif, png', 'allowEmpty'=>true) ,
            array('favicon', 'file', 'types'=>'ico, png', 'allowEmpty'=>true)
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels() {
        return array(
            'title' => Yii::t('standard','_TITLE'),
            'logo' => Yii::t('templatemanager','Logo'),
            'favicon' => Yii::t('templatemanager','Favicon'),
            'loginImg' => Yii::t('login','_HOMEPAGE'),
            'playerBg' => Yii::t('templatemanager','Course Player'),
            'templateId' => Yii::t('branding','_COLORS_SCHEME'),
            'customUrl' => Yii::t('domainbranding','Custom URL'),

        );
    }

    /**
     * Form constructor
     * @param CoreOrgChartBranding $branding
     */
    public function __construct($branding) {
        parent::__construct();
        $this->branding = $branding;
        $this->_initForm();
    }

    /**
     * Init form values
     */
    protected function _initForm() {

        // set default form values
        $branding =& $this->branding;

        $this->templateId   = $branding->id_template;
        $this->customUrl    = $branding->custom_url;
        $this->logo         = $branding->getLogoUrl();
        $this->favicon      = $branding->getFaviconUrl();
        $this->loginImg     = $branding->getLoginImgUrl();
        $this->loginImgAspect = $branding->login_img_aspect;
        $this->playerBg     = $branding->getPlayerBgUrl();
        $this->playerBgAspect = $branding->player_bg_aspect;
    }

    /**
     * Return array of templates
     * @return array
     */
    public function getTemplateDropdownList() {
        $data = array();

        $templates = CoreScheme::model()->findAll();
        if (!empty($templates)) {
            foreach ($templates as $template) {
                $data[$template->id] = $template->title;
            }
        }

        return $data;
    }

    /**
     * Saves the branding scheme for an organization node
     * @return bool
     */
    public function save() {
        $branding =& $this->branding;
        $branding->id_template = $this->templateId;

        // normalize url
        $url = '';
        if (!empty($this->customUrl)) {
            $url = $this->customUrl;
            if(strpos($url,'http://')===0)
                $url=substr($url, strlen('http://'));
            if (substr($url, -1) === '/')
                $url=substr($url, 0, strlen($url)-1);

            if ($url != $branding->custom_url) {
                // check that the custom domain is unique
                $isUrlInUse = CoreOrgChartBranding::model()->findByAttributes(array('custom_url' => $url));
                if ($isUrlInUse) {
                    Yii::app()->user->setFlash('error', Yii::t('domainbranding', 'Custom URL already in use. Please choose a unique url.'));
                }
            }
        }
        $branding->custom_url = $url;


        // Prepare the storage manager
        $storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_COMMON);
        
        // Save logo
        if ($this->logo instanceof CUploadedFile) {
        	$filename = Docebo::randomHash() . '.' . CFileHelper::getExtension($this->logo->getName()); 
            $storageManager->storeAs($this->logo->tempName, $filename);
            $branding->logo = $filename;
        }

        // Save favicon
        if ($this->favicon instanceof CUploadedFile) {
        	$filename = Docebo::randomHash() . '.' . CFileHelper::getExtension($this->favicon->getName());
        	$storageManager->storeAs($this->favicon->tempName, $filename);
        	$branding->favicon = $filename;
        }

        // Save loginImg
        if ($this->loginImg instanceof CUploadedFile) {
        	$filename = Docebo::randomHash() . '.' . CFileHelper::getExtension($this->loginImg->getName());
        	$storageManager->storeAs($this->loginImg->tempName, $filename);
        	$branding->login_img = $filename;
        }
        $branding->login_img_aspect = $this->loginImgAspect;

        // Save playerBg
        if ($this->playerBg instanceof CUploadedFile) {
        	$filename = Docebo::randomHash() . '.' . CFileHelper::getExtension($this->playerBg->getName());
        	$storageManager->storeAs($this->playerBg->tempName, $filename);
        	$branding->player_bg = $filename;
        }
        $branding->player_bg_aspect = $this->playerBgAspect;
        

        if (!$branding->save())
            return false;

        $this->_initForm();
        return true;
    }
}