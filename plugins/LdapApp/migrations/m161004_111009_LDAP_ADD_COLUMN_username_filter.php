<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 4.10.2016 г.
 * Time: 14:11
 */
class m161004_111009_LDAP_ADD_COLUMN_username_filter extends DoceboDbMigration {
    public function safeUp()
    {
        $this->addColumn(LdapAppAccount::model()->tableName(), 'ldap_username_filter', 'varchar(255) DEFAULT NULL');
    }

    public function safeDown()
    {
        $this->dropColumn(LdapAppAccount::model()->tableName(), 'ldap_username_filter');
    }
}