<?php
/**
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */

class LdapSettings extends CWidget {

	public $id_multidomain;

	/**
	 * @var LdapAppSettingsForm
	 */
	public $settingsForm;

	public function init () {
		parent::init();
	}

	public function run () {
		if(!$this->settingsForm){
			$this->settingsForm = new LdapAppSettingsForm();
		}

		$this->render('ldap_settings', array(
			'settings'=>$this->settingsForm
		));
	}


}