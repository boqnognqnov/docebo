<?
/* @var $this LdapSettings */
/* @var $settings LdapAppSettingsForm */
?>

<?
$assetsPath = Yii::app()->assetManager->publish(Yii::getPathOfAlias('LdapApp.assets'));
Yii::app()->clientScript->registerCssFile($assetsPath.'/ldap_app.css');
?>

<?php DoceboUI::printFlashMessages(); ?>

<?php
$form = $this->beginWidget( 'CActiveForm', array(
	'id'          => 'app-settings-form',
	'htmlOptions' => array( 'class' => 'form-horizontal docebo-form' ),
) );
?>

	<div class="control-container">
		<div class="control-group">
			<?= $form->labelEx( $settings, 'ldap_used', array( 'class' => 'control-label' ) ); ?>
			<div class="controls">
				<?= $form->checkBox( $settings, 'ldap_used' ); ?>
				<?=Yii::t('ldap','_LDAP_USED')?>
				<?= $form->error( $settings, 'ldap_used' ); ?>
			</div>
		</div>
	</div>

	<div class="control-container odd">
		<div class="control-group">
			<label class="control-label">
				<?=Yii::t('ldap', 'LDAP Authentication Configuration')?>
			</label>
			<div class="controls">
				<div class="row-fluid">
					<div class="span6">
						<p>
							<?=Yii::t('ldap', '_LDAP_SERVER')?>
						</p>
						<?= $form->textField( $settings, 'ldap_server'); ?>
						<?= $form->error( $settings, 'ldap_server' ); ?>
					</div>
					<div class="span6">
						<p>
							<?=Yii::t('ldap', 'LDAP port')?>
						</p>
						<?= $form->textField( $settings, 'ldap_port' ); ?>
						<?= $form->error( $settings, 'ldap_port' ); ?>

						<p class="context">
							<?=Yii::t('ldap', '(usually 389)')?>
						</p>
					</div>
				</div>

				<br/>

				<div class="row-fluid">
					<div class="span6">
						<p>
							<?=Yii::t('ldap', 'Username for LDAP users')?>
						</p>
						<?= $form->textField( $settings, 'ldap_user_string'); ?>
						<?= $form->error( $settings, 'ldap_user_string' ); ?>

						<p class="context">
							<?=Yii::t('ldap', 'Use $user for the username. Ex. $user@domain2.domain1 (for: W2k3)')?>
						</p>
					</div>
					<div class="span6">
						<p>
							<?=Yii::t('ldap','Base DN')?>
						</p>
						<?= $form->textField( $settings, 'ldap_base_dn' ); ?>
						<?= $form->error( $settings, 'ldap_base_dn' ); ?>

						<p class="context">
							<?=Yii::t('ldap', 'E.g.: ou=users,ou=system')?>
						</p>
					</div>
				</div>

				<br/>

				<div>
					<label>
						<?= $form->checkBox( $settings, 'ldap_use_ssl' ); ?>
						<?=Yii::t('ldap','Use LDAPS protocol')?>
					</label>
					<?= $form->error( $settings, 'ldap_use_ssl' ); ?>
				</div>

				<br/>

				<div>
					<label>
						<?= $form->checkBox( $settings, 'ldap_check_lms_first' ); ?>
						<?=Yii::t('ldap','Check LMS login before LDAP')?>
					</label>
					<?= $form->error( $settings, 'ldap_check_lms_first' ); ?>
				</div>

			</div>
		</div>
	</div>

	<div class="control-container">
		<div class="control-group">
			<label class="control-label">
				<?=Yii::t('ldap', 'LDAP administrative functionality')?>
			</label>
			<div class="controls">
				<label>
					<?= $form->checkBox( $settings, 'ldap_admin_enabled' ); ?>
					<?=Yii::t('ldap', 'Enable LDAP Administrative functionality')?>
				</label>
				<p>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<?=Yii::t('ldap', '(listing, creating, deleting entries)')?>
				</p>
				<?= $form->error( $settings, 'ldap_admin_enabled' ); ?>
			</div>
		</div>
	</div>

	<div class="control-container odd">
		<div class="control-group">
			<label class="control-label">
				<?=Yii::t('ldap', 'LDAP administrative functionality configuration')?>
			</label>

			<div class="controls">
				<div class="row-fluid">
					<div class="span6">
						<p>
							<?=Yii::t('ldap','LDAP admin username')?>
						</p>
						<?= $form->textField( $settings, 'ldap_admin_username' ); ?>
						<?= $form->error( $settings, 'ldap_admin_username' ); ?>
					</div>
					<div class="span6">
						<p>
							<?=Yii::t('ldap','LDAP admin password')?>
						</p>
						<?= $form->passwordField( $settings, 'ldap_admin_password' ); ?>
						<?= $form->error( $settings, 'ldap_admin_password' ); ?>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span6">
						<p>
							<?=Yii::t('ldap','Username LDAP field')?>
						</p>
						<?= $form->textField( $settings, 'ldap_username_field' ); ?>
						<?= $form->error( $settings, 'ldap_username_field' ); ?>
					</div>
					<div class="span6">
						<p>
							<?=Yii::t('ldap','Username filter')?>
						</p>
						<?= $form->textField( $settings, 'ldap_username_filter' ); ?>
						<?= $form->error( $settings, 'ldap_username_filter' ); ?>
						<p class="context" style="margin-bottom: 5px;">
							<?=Yii::t('ldap', 'If provided, this filter is applied only when importing users from LDAP')?>
						</p>
					</div>
				</div>

				<div class="row-fluid">
					<div class="span6">
						<p>
							<?=Yii::t('ldap','Lastname LDAP field')?>
						</p>
						<?= $form->textField( $settings, 'ldap_lastname_field' ); ?>
						<?= $form->error( $settings, 'ldap_lastname_field' ); ?>
					</div>
					<div class="span6">
						<p>
							<?=Yii::t('ldap','Firstname LDAP field')?>
						</p>
						<?= $form->textField( $settings, 'ldap_firstname_field' ); ?>
						<?= $form->error( $settings, 'ldap_firstname_field' ); ?>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span6">
						<p>
							<?=Yii::t('ldap','Email LDAP field')?>
						</p>
						<?= $form->textField( $settings, 'ldap_email_field' ); ?>
						<?= $form->error( $settings, 'ldap_email_field' ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="form-actions">
		<?= CHtml::submitButton( Yii::t( 'standard', '_SAVE' ), array( 'class' => 'btn-docebo green big' ) ); ?>
		&nbsp;
		<a href="<?= Docebo::createAbsoluteUrl( 'lms:app/index' ); ?>"
		   class="btn-docebo black big"><?= Yii::t( 'standard', '_CANCEL' ); ?></a>
	</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(function(){
		$('input,select', '#app-settings-form').styler();
	})
</script>