
<style>
	<!--

	.modal.progressive-dialog .failed-lines {
		color: red;
	}

	.modal.progressive-dialog .failed-lines a {
		text-decoration: underline;
	}

	-->
</style>
<?php
echo CHtml::beginForm('', 'POST', array(
	'class'=>'ajax',
));

?>
<div id="ldap-sync-container" class="ldap-sync-container">


	<div class="row-fluid">
		<div class="span12">

			<div class="pull-left sub-title" ><?= Yii::t('ldap', 'Syncing {users} users', array('{users}'=>$totalUsers)) ?></div>
			<div class="pull-right percentage-gauge"><?=$completedPercent?>%</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="docebo-progress progress progress-success">
				<div style="width: <?=$completedPercent?>%;" class="bar full-height" id="uploader-progress-bar"></div>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span12 action-undergoing">
			<?php if ($stop) : ?>
				<div class="row-fluid">
					<div class="span12">
						<?= Yii::t('ldap', '{N} item(s) successfully synchronised', array('{N}' => $synchronised)); ?>
					</div>
				</div>
				<?php if ($failed > 0) : ?>
					<div class="row-fluid">
						<div class="span12 failed-lines">
							<?php
							echo Yii::t('ldap', '{N} item(s) failed to import, saved in a separate CSV file and available for <strong><a href="{url}">download</a></strong>',array(
								'{N}' 	=> $failed,
								'{url}'	=> Docebo::createAbsoluteLmsUrl('stream/file') . '&fn=' . $csvErrorFileName . '&col=uploads',
							));
							?>
						</div>
					</div>
				<?php  endif; ?>
				<div class="form-actions">
					<input class="btn btn-docebo black big close-dialog" type="submit" value="<?= Yii::t('standard', '_CLOSE') ?>" />
				</div>
			<?php endif; ?>
		</div>
	</div>

</div>

<?= CHtml::endForm() ?>


<script type="text/javascript">

	$(function(){

		var $modal = $('#ldap-sync-container').parents('.modal');
		// Set dialog title, depending on export phase we are in
		var dialogTitle = '<span></span><?=Yii::t('ldap', 'Sync LDAP users')?>';
		$modal.find('.modal-header h3').html(dialogTitle);

		$('#ldap-sync-container').closest('.modal-body').on('dialog2.closed', function(){
			window.location.reload();
		});
	});

</script>