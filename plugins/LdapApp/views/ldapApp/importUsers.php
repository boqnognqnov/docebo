<div class="content lead" style="border-top: 1px solid #e4e6e5; border-bottom: 1px solid #e4e6e5; background: #f1f3f2; padding: 18px 12px;">
	<div class="row-fluid text-center">
		<div class="span6">
			<?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/student-status.png');?>
			<div class="clearfix" style="margin-bottom: 0">
				<span style="color: #0465ac; font-size: 40px; font-weight: 700;"><?=$ldapUsersCount?></span> Users in your LDAP
			</div>
		</div>
		<div class="span6">
			<div class="lead" style="color: #0465ac; font-size: 40px; font-weight: 700; margin-bottom: 10px;">
				<?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/student-status.png');?>

			</div>
			<div class="lead clearfix" style="margin-bottom: 0">
				<span style="color: #0465ac; font-size: 40px; font-weight: 700;"><?=$usersCountDocebo?></span> Users in your Docebo platform
			</div>
		</div>
	</div>
</div>


<a href="<?=Docebo::createAbsoluteLmsUrl('progress/run', array(
	'type'		=> Progress::JOBTYPE_LDAP_IMPORT,
))?>" class="btn confirm-btn sync-users-btn open-dialog <?=$ldapUsers->getTotalItemCount()==0 ? 'disabled' : null; ?>"><i class="i-sprite is-assign white"></i> <?=Yii::t('ldap', 'Import LDAP users')?></a>

<? if($ldapUsers): ?>
<div class="bottom-section clearfix">
	<div id="grid-wrapper">
		<?php $this->widget('DoceboCGridView', array(
			//'id' => 'group-management-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			'dataProvider' => $ldapUsers,
			'template' => '{items}{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'pager' => array(
				'class' => 'adminExt.local.pagers.DoceboLinkPager',
			),
			'ajaxUpdate' => 'all-items',
			'columns' => array(
				array(
					'header'=>'Username',
					'value'=>'$data[\'username\']'
				),
				'firstname',
				'lastname',
				array(
					'header'=>'Email',
					'value'=>'$data[\'email\']'
				),
				array(
					'header' => 'Synched',
					'type' => 'raw',
					'value' => '$data[\'exists_in_lms\'] ? "<i class=\'i-sprite is-check green\'/>" : "<i class=\'i-sprite is-remove\'/>"',
					'htmlOptions' => array('class' => 'button-column-single')
				),
			),
		));?>
	</div>
</div>
<? endif; ?>

<script type="text/javascript">
	(function($){
		$('.sync-users-btn').click(function(e){

			if($(this).hasClass('disabled')) return;
		});
	})(jQuery);
</script>