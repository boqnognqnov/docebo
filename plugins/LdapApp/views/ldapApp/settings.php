<?php
/* @var $form CActiveForm */
/* @var $settings LdapAppSettingsForm */
?>

<a href="<?= ('it' == Yii::app()->getLanguage()) ? 'https://www.docebo.com/it/knowledge-base/ldap-2/' : 'https://www.docebo.com/knowledge-base/ldap/' ?>" target="_blank" class="app-link-read-manual"><?= Yii::t('apps', 'Read Manual'); ?></a>

<div class="clearfix"></div>

<h2 class="bottom-bordered">LDAP <?= Yii::t('standard', 'Settings') ?></h2>

<?php $this->widget( 'LdapSettings', array(
	'settingsForm'=>$settings,
	'id_multidomain' => null, // This is global settings page, not multidomain specific
) ); ?>
