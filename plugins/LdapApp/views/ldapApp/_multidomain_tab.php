<?php /* @var $client CoreMultidomain */ ?>
<?php /* @var $this SimpleSamlAppController */ ?>
<? /* @var $multiDomainModel CoreMultidomain */ ?>
<? /* @var $settingsForm LdapAppSettingsForm */ ?>

<div class="tab-pane" id="ldap-tab">
	<h1>LDAP - <?= Yii::t( 'standard', 'Settings' ); ?></h1>
	<?php $this->widget( 'LdapSettings', array(
		'id_multidomain' => $client->id,
		'settingsForm'=>$settingsForm,
	) ); ?>
</div>