<?php

class LdapAppModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'LdapApp.models.*',
			'LdapApp.components.*',
			'LdapApp.widgets.*',
			'LdapApp.extensions.adLDAP.YiiLDAP',
		));

		// Capture the event that is raised when the user initiates a login.
		// Note that up to this event we only know that the username and password
		// are provided/valid strings. No other check is done before the event is raised.
		// All needed checks must be done in the callback/event listener which is the code below
		// in this case.
		if(LdapSettingsHelper::get('ldap_used')){
			Yii::app()->event->on('UserManagementTiles', array($this, 'addImportButton'));
			Yii::app()->event->on('UserLoginPreProcess', array($this, 'handleLdapBindRequest'));
			Yii::app()->event->on('UserLoginFailed', array($this, 'handleLdapBindRequestAfterLmsCheck'));
		}

		Yii::app()->event->on("RenderDomainBrandingTabs", array($this, 'renderDomainBrandingTabs'));
		Yii::app()->event->on("RenderDomainBrandingTabsContent", array($this, 'renderDomainBrandingTabsContent'));

		// This is not called for some reason
		//Yii::app()->event->on("DomainBrandingSettingsFormSubmit", array($this, 'saveLdapSettings'));
	}

	/**
	 * Adds the import button
	 * @param $event DEvent
	 */
	public function addImportButton($event) {
		// Render one more tile in User management page to allow importing LDAP users
		if (Yii::app()->user->getIsGodadmin() && LdapSettingsHelper::get('ldap_admin_enabled')) {
			echo '
					<li>
						<div>
							<a href="'.Docebo::createAdminUrl('LdapApp/LdapApp/importUsers').'" class="import" alt="'.Yii::t('ldap', 'Import LDAP users').'">
								<span></span>'.Yii::t('ldap', 'Import LDAP users').'
							</a>
						</div>
					</li>';
		}
	}

	/**
	 * This is used if the check LDAP/LMS is swapped
	 * @param $event DEvent
	 */
	public function handleLdapBindRequestAfterLmsCheck($event) {
		if(LdapSettingsHelper::get('ldap_check_lms_first')) {
			Yii::log('LMS check failed... Let us ask LDAP now', CLogger::LEVEL_ERROR);
			$event->params['after_lms_check'] = true;
			$this->handleLdapBindRequest($event);
		}
	}

	/**
	 * Adds the LDAP specific binding logic
	 * @param $event DEvent
	 * @return bool
	 */
	public function handleLdapBindRequest($event) {

		// Check if the ldap_check_lms_first flag is on and this is the second check
		if(LdapSettingsHelper::get('ldap_check_lms_first') && !isset($event->params['after_lms_check'])) {
			Yii::log('Skipping first check.. Let the LMS check first', CLogger::LEVEL_ERROR);
			return false;
		}

		if(!empty($event->params)){
			$username = isset($event->params['username']) ? $event->params['username'] : null;
			$password = isset($event->params['password']) ? $event->params['password'] : null; // Ignored for now

			$user = CoreUser::model()->find('userid = :userid AND valid=1', array(':userid' => Yii::app()->user->getAbsoluteUsername($username)));

			if(!$user){
				// This username MUST exist in our DB
				return FALSE;
			}

			$ldapUserString = LdapSettingsHelper::get('ldap_user_string');

			// YiiLDAP is just a wrapper for adLDAP
			$ldap = new YiiLDAP();
			$baseDn = LdapSettingsHelper::get('ldap_base_dn', null);
			$ldap->options = array(
				'domain_controllers' => array(LdapSettingsHelper::get('ldap_server')),
				'ad_port'=>LdapSettingsHelper::get('ldap_port', 389),
				'use_ssl' => LdapSettingsHelper::get('ldap_use_ssl'),
				'account_suffix' => null, // force it to blank (by default it has some value)
				'base_dn' => $baseDn ? $baseDn : 'ou=users,ou=system', // Use ApacheDS default value as default
			);

			try{
				$ldap->init(); /* @var $ldap adLDAP */
			}catch(Exception $e){
				Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
				return FALSE;
			}

			$ldap_user = preg_replace('#\$user#Ui', $username, $ldapUserString);

			// Check if the provided username/password will authenticate the user
			// in the LDAP server
			if($ldap->authenticate($ldap_user, $password)){

				$ldap->close();

				Yii::log('Ldap authentication success for user '.$ldap_user, CLogger::LEVEL_INFO);

				$ui = new DoceboUserIdentity($username, $password);
				// Since we authenticated the user at LDAP, don't check his
				// LMS password for correctness
				$ui->authenticateWithNoPassword();

				Yii::app()->user->login($ui);

				// if the authentication goes well we need to check for a suspended installation process
				// we can identify this thing trough a setting
				Docebo::resumeSaasInstallation();

				// Check if we are trying to use oauth2
				$controllerId = Yii::app()->getController()->getId();
				if(strtolower($controllerId) !== 'oauth2') {
					$afterloginRedirectUrl = isset(Yii::app()->session['lasturl']) ? Yii::app()->session['lasturl'] : false;

					// Redirect user to destination Url
					if ($afterloginRedirectUrl !== false) {
						unset(Yii::app()->session['lasturl']);
						Yii::app()->getController()->redirect($afterloginRedirectUrl);
					}

					Yii::app()->getController()->redirect(Docebo::getUserHomePageUrl());

					$event->return_value['logged_in'] = false;
				} else {
					// This is needed because LDAP should not redirect to anything in Mobile App
					$event->return_value['logged_in'] = true;
				}

				return TRUE;
			}else{
				// LDAP authentication failed
				Yii::log('Ldap error check for user '.$ldap_user.': '.$ldap->getLastError(), CLogger::LEVEL_ERROR);
				$ldap->close();
				return FALSE;
			}
		}

		return false;
	}

	/**
	 * Adds the tab to the multidomain app
	 * @param $event DEvent
	 */
	public function renderDomainBrandingTabs($event) {
		echo "<li>
				<a href='#' data-toggle='tab' data-target='#ldap-tab'>
						 <i class='fa fa-lg fa-key' style='width: 22px;height: 22px;padding-left: 7px;'></i>
						 Ldap App - ".Yii::t('standard','Settings')."
			</a>
			</li>";
	}

	/**
	 * Renders the saml tab in multidomain client config
	 * @param $event DEvent
	 */
	public function renderDomainBrandingTabsContent($event) {
		$multiDomainModel = $event->params['multidomain_client_model'];

		$account = LdapAppAccount::model()->findByAttributes(array(
			'id_multidomain'=>$multiDomainModel ? $multiDomainModel->id : null,
		));

        /**
         * @var $account LdapAppAccount
         */

		if(!$account){
			$account = new LdapAppAccount();
			if($multiDomainModel)
				$account->id_multidomain = $multiDomainModel->id;
		}

		$settings = new LdapAppSettingsForm();
		$settings->ldap_server = $account->ldap_server;
		$settings->ldap_port = $account->ldap_port;
		$settings->ldap_user_string = $account->ldap_user_string;
		$settings->ldap_used = $account->ldap_used;
		$settings->ldap_use_ssl = $account->ldap_use_ssl;
		$settings->ldap_base_dn = $account->ldap_base_dn;
		$settings->ldap_admin_enabled = $account->ldap_admin_enabled;
		$settings->ldap_admin_username = $account->ldap_admin_username;
		$settings->ldap_admin_password = $account->ldap_admin_password;
		$settings->ldap_username_field = $account->ldap_username_field;
		$settings->ldap_firstname_field = $account->ldap_firstname_field;
		$settings->ldap_lastname_field = $account->ldap_lastname_field;
		$settings->ldap_email_field = $account->ldap_email_field;
		$settings->ldap_check_lms_first = $account->ldap_check_lms_first;
        $settings->ldap_username_filter = $account->ldap_username_filter;

		if (isset($_POST['LdapAppSettingsForm'])) {
			$settings->setAttributes($_POST['LdapAppSettingsForm']);

			if ($settings->validate()) {
				$account->ldap_server = $settings->ldap_server;
				$account->ldap_port = $settings->ldap_port;
				$account->ldap_user_string = $settings->ldap_user_string;
				$account->ldap_used = $settings->ldap_used;
				$account->ldap_use_ssl = $settings->ldap_use_ssl;
				$account->ldap_base_dn = $settings->ldap_base_dn;
				$account->ldap_admin_enabled = $settings->ldap_admin_enabled;
				$account->ldap_admin_username = $settings->ldap_admin_username;
				$account->ldap_admin_password = $settings->ldap_admin_password;
				$account->ldap_username_field = $settings->ldap_username_field;
				$account->ldap_firstname_field = $settings->ldap_firstname_field;
				$account->ldap_lastname_field = $settings->ldap_lastname_field;
				$account->ldap_email_field = $settings->ldap_email_field;
				$account->ldap_check_lms_first = $settings->ldap_check_lms_first;
                $account->ldap_username_filter = $settings->ldap_username_filter;

				if($account->save()){
					Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));
				}else{
					Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
					Yii::log(print_r($account->getErrors(), 1), CLogger::LEVEL_ERROR);
				}
			}
			else {
				Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
				Yii::log(print_r($settings->getErrors(), 1), CLogger::LEVEL_ERROR);
			}
		}

		Yii::app()->getController()->renderPartial('LdapApp.views.ldapApp._multidomain_tab', array(
			'multiDomainModel'=>$multiDomainModel,
			'settingsForm'=>$settings,
		));
	}

	/**
	 * Saves multidomain client settings for LDAP
	 * @param $event
	 *
	 * @deprecated
	 */
	public function saveLdapSettings($event) {
		var_dump($_POST);exit;
		$controller = new SimpleSamlAppController('simpleSamlApp', $this);
		$controller->saveMultidomainSettings($event->params['multidomain_client_model']);
	}

    /**
     * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
     */
    public function menu() {
        Yii::app()->mainmenu->addAppMenu('ldap', array(
            'icon' => 'home-ico ldap',
			'app_icon' => 'fa-lock', // icon used in the new menu
            'link' => Docebo::createAppUrl('admin:LdapApp/LdapApp/settings'),
            'label' => 'LDAP '.Yii::t('standard', 'Settings'),
        ), array());
    }
}
