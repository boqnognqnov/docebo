<?php
/**
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */

class LdapSettingsHelper {

	/**
	 * Get the "local" LDAP setting named $paramName
	 * based on the current Multidomain
	 * (if the MultidomainApp is active). Otherwise
	 * get the global LDAP that corresponds to the $paramName
	 *
	 * @param      $paramName
	 * @param null $defaultValue
	 *
	 * @return mixed|null
	 */
	static public function get($paramName, $defaultValue = null){
	    if(Yii::app() instanceof CWebApplication) {
            $account = LdapAppAccount::resolveAccount();
        }

		if($account){
			$attr = $account->getAttribute($paramName);
			if($attr) return $attr;
		}

		return $defaultValue;
	}
}