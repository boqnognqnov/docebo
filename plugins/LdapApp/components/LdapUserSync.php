<?php
Yii::import('LdapApp.extensions.adLDAP.YiiLDAP');

class LdapUserSync {
	static public function getLdapUsers($server, $port = null, $baseDn = null, $username = null, $password = null, $pageSize = null) {
		// YiiLDAP is just a wrapper for adLDAP
		$ldap = new YiiLDAP();
		$ldap->options = array(
			// Use multiple servers for load balancing if needed, one random will be picked
			'domain_controllers' => is_array($server) ? $server : array($server),
			'ad_port'=>$port ? $port : 389,
			'account_suffix' => null, // force it to blank (by default it has some value)
			'use_ssl' => (bool) LdapSettingsHelper::get('ldap_use_ssl'),
			'base_dn' => $baseDn ? $baseDn : 'ou=users,ou=system', // Use ApacheDS default value as default
		);
		try{
			$ldap->init(); /* @var $ldap adLDAP */
		}catch(Exception $e){
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			return FALSE;
		}

		if(!LdapSettingsHelper::get('ldap_admin_enabled'))
			throw new CHttpException(500, 'LDAP admininstrative functionality not enabled in the LDAP app');

		if(!$username && !$password){
			// If they are passed as parameters, use those,
			// otherwise use admin username/password from the LDAP
			// app settings
			$username = LdapSettingsHelper::get('ldap_admin_username');
			$password = LdapSettingsHelper::get('ldap_admin_password');
		}

		// Try to bind the LDAP user to the directory (make the connection)
		try{
			$bind = $ldap->authenticate($username, $password);
			if(!$bind){
				Yii::log('Can not bind to LDAP due to: '.$ldap->getLastError(), CLogger::LEVEL_ERROR);
				$ldap->close();
				return FALSE;
			}
		}catch (adLDAPException $e){
			$ldap->close();
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			return FALSE;
		}

		$filter = "(objectClass=person)";
		$fields = array("*");
		$entries = array();
		if($pageSize && version_compare(phpversion(), '5.4.0', '>=')) {
			$cookie = '';
			do {
				// Pagination can only work during a single PHP request.
				// LDAP support in PHP is quite crappy. More information:
				// https://sgehrig.wordpress.com/2009/11/06/reading-paged-ldap-results-with-php-is-a-show-stopper/
				// or ask Raffaele or Dzhuneyt
				ldap_control_paged_result($ldap->getLdapConnection(), $pageSize, true, $cookie);
				$result  = ldap_search($ldap->getLdapConnection(), $baseDn, $filter, $fields);
				if(!$result)
					break;
				$entries = array_merge($entries, ldap_get_entries($ldap->getLdapConnection(), $result));
				ldap_control_paged_result_response($ldap->getLdapConnection(), $result, $cookie);
			} while($cookie !== null && $cookie != '');
		} else {
			// No 'perPage' set, disable pagination and retrieve all results
			$sr = ldap_search($ldap->getLdapConnection(), $ldap->getBaseDn(), $filter, $fields);
			if(!$sr){ // No results
				Yii::log('ldap_search() returned no results based on the criteria', CLogger::LEVEL_ERROR);
				$ldap->close();
				return FALSE;
			}
			$entries = ldap_get_entries($ldap->getLdapConnection(), $sr);
		}

		// Always remember to close the connection
		$ldap->close();

		return $entries;
	}
} 