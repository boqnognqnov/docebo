<?php

/**
 * ApiAppSettingsForm class.
 *
 * @property string $catalog_type
 */

class LdapAppSettingsForm extends CFormModel {

	public $ldap_server;

	public $ldap_port;

	public $ldap_user_string;

	public $ldap_used;

	public $ldap_admin_username;

	public $ldap_admin_password;

	public $ldap_admin_enabled;

	public $ldap_use_ssl;

	public $ldap_base_dn;

	public $ldap_username_field;

	public $ldap_firstname_field;

	public $ldap_lastname_field;

	public $ldap_email_field;

	public $ldap_check_lms_first;

    public $ldap_username_filter;

	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			array('ldap_server, ldap_port, ldap_user_string, ldap_used, ldap_use_ssl, ldap_admin_username, ldap_admin_password,
			ldap_admin_enabled, ldap_base_dn, ldap_username_field, ldap_firstname_field, ldap_lastname_field, ldap_email_field,
			ldap_check_lms_first, ldap_username_filter', 'safe'),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array(
			'ldap_server' => Yii::t('ldap', '_LDAP_SERVER'),
			'ldap_port' => Yii::t('ldap','_LDAP_PORT'),
			'ldap_user_string' => Yii::t('ldap','_LDAP_USER_STRING'),
			'ldap_used' => Yii::t('ldap','LDAP authentication'),
			'ldap_use_ssl' => Yii::t('ldap','Use LDAPS protocol'),
			'ldap_base_dn' => Yii::t('ldap','Base DN'),
			'ldap_admin_enabled' => Yii::t('ldap', 'Enable LDAP administrative functionality (listing, creating, deleting entries)'),
			'ldap_admin_username' => Yii::t('ldap','LDAP admin username'),
			'ldap_admin_password' => Yii::t('ldap','LDAP admin password'),
			'ldap_username_field' => Yii::t('ldap','Username LDAP field'),
			'ldap_firstname_field' => Yii::t('ldap','Firstname LDAP field'),
			'ldap_lastname_field' => Yii::t('ldap','Lastname LDAP field'),
			'ldap_email_field' => Yii::t('ldap','Email LDAP field'),
			'ldap_check_lms_first' => Yii::t('ldap','Check LMS login before LDAP'),
            'ldap_username_filter' => Yii::t('ldap', 'Username filter')
		);
	}
}
