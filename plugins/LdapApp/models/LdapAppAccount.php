<?php

/**
 * This is the model class for table "ldap_app_account".
 *
 * The followings are the available columns in table 'ldap_app_account':
 * @property integer $id
 * @property integer $id_multidomain
 * @property string $ldap_server
 * @property string $ldap_port
 * @property string $ldap_user_string
 * @property integer $ldap_used
 * @property integer $ldap_use_ssl
 * @property string $ldap_base_dn
 * @property integer $ldap_admin_enabled
 * @property string $ldap_admin_username
 * @property string $ldap_admin_password
 * @property string $ldap_username_field
 * @property string $ldap_firstname_field
 * @property string $ldap_lastname_field
 * @property string $ldap_email_field
 * @property integer $ldap_check_lms_first
 * @property string $ldap_username_filter
 */
class LdapAppAccount extends CActiveRecord
{
	private static $activeAccount;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ldap_app_account';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_multidomain, ldap_used, ldap_use_ssl, ldap_admin_enabled, ldap_check_lms_first', 'numerical', 'integerOnly'=>true),
			array('ldap_server, ldap_port, ldap_user_string, ldap_base_dn, ldap_admin_username, ldap_admin_password, ldap_username_field, ldap_firstname_field, ldap_lastname_field, ldap_email_field, ldap_username_filter', 'length', 'max'=>255),
			array('id, id_multidomain, ldap_server, ldap_port, ldap_user_string, ldap_used, ldap_use_ssl, ldap_base_dn, ldap_admin_enabled, ldap_admin_username, ldap_admin_password, ldap_username_field, ldap_firstname_field, ldap_lastname_field, ldap_email_field, ldap_check_lms_first, ldap_username_filter', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_multidomain' => 'Id Multidomain',
			'ldap_server' => 'Ldap Server',
			'ldap_port' => 'Ldap Port',
			'ldap_user_string' => 'Ldap User String',
			'ldap_used' => 'Ldap Used',
			'ldap_use_ssl' => 'Ldap Use Ssl',
			'ldap_base_dn' => 'Ldap Base Dn',
			'ldap_admin_enabled' => 'Ldap Admin Enabled',
			'ldap_admin_username' => 'Ldap Admin Username',
			'ldap_admin_password' => 'Ldap Admin Password',
			'ldap_username_field' => 'Ldap Username Field',
			'ldap_firstname_field' => 'Ldap Firstname Field',
			'ldap_lastname_field' => 'Ldap Lastname Field',
			'ldap_email_field' => 'Ldap Email Field',
			'ldap_check_lms_first' => 'Ldap Check Lms First',
            'ldap_username_filter' => 'Username Filter'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_multidomain',$this->id_multidomain);
		$criteria->compare('ldap_server',$this->ldap_server,true);
		$criteria->compare('ldap_port',$this->ldap_port,true);
		$criteria->compare('ldap_user_string',$this->ldap_user_string,true);
		$criteria->compare('ldap_used',$this->ldap_used);
		$criteria->compare('ldap_use_ssl',$this->ldap_use_ssl);
		$criteria->compare('ldap_base_dn',$this->ldap_base_dn,true);
		$criteria->compare('ldap_admin_enabled',$this->ldap_admin_enabled);
		$criteria->compare('ldap_admin_username',$this->ldap_admin_username,true);
		$criteria->compare('ldap_admin_password',$this->ldap_admin_password,true);
		$criteria->compare('ldap_username_field',$this->ldap_username_field,true);
		$criteria->compare('ldap_firstname_field',$this->ldap_firstname_field,true);
		$criteria->compare('ldap_lastname_field',$this->ldap_lastname_field,true);
		$criteria->compare('ldap_email_field',$this->ldap_email_field,true);
		$criteria->compare('ldap_check_lms_first',$this->ldap_check_lms_first);
        $criteria->compare('ldap_username_filter',$this->username_filter,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LdapAppAccount the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Returns an instance of this class, resolving the client and then finding the best account to use
	 *
	 * @return LdapAppAccount
	 */
	public static function resolveAccount() {
		static $_resolvedAccountLogged = false;

		if(!self::$activeAccount) {
			// Trigger client resolution
			if (PluginManager::isPluginActive('MultidomainApp')) {
				$client = CoreMultidomain::resolveClient();
				if ($client)
					self::$activeAccount = self::model()->findByAttributes(array('id_multidomain' => $client->id));
			}

			// If we are here, not account has been loaded. Return the default account
			if(!self::$activeAccount)
				self::$activeAccount = self::model()->findByAttributes(array('id_multidomain' => null));

			if(self::$activeAccount && !$_resolvedAccountLogged){
				//Yii::log("Using account ".self::$activeAccount->id." for LDAP", CLogger::LEVEL_INFO);
				$_resolvedAccountLogged = true;
			}
		}

		return self::$activeAccount;
	}
}
