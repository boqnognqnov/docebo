<?php

class LdapAppController extends Controller {


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		$admin_only_actions = array('settings', 'importUsers');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}


	public function actionSettings() {

		if(!isset(Yii::app()->session['ldap_admin_password_fake']))
			Yii::app()->session['ldap_admin_password_fake'] = '**********';

		$settings = new LdapAppSettingsForm();

		$account = LdapAppAccount::resolveAccount();

		if(!$account){
			$account = new LdapAppAccount();
		}

		$settings->ldap_server = $account->ldap_server;
		$settings->ldap_port = $account->ldap_port;
		$settings->ldap_user_string = $account->ldap_user_string;
		$settings->ldap_used = $account->ldap_used;
		$settings->ldap_use_ssl = $account->ldap_use_ssl;
		$settings->ldap_base_dn = $account->ldap_base_dn;
		$settings->ldap_admin_enabled = $account->ldap_admin_enabled;
		$settings->ldap_admin_username = $account->ldap_admin_username;
		$settings->ldap_admin_password = Yii::app()->session['ldap_admin_password_fake'];
		Yii::app()->session['ldap_admin_password'] = $account->ldap_admin_password;
		$settings->ldap_username_field = strtolower($account->ldap_username_field);
		$settings->ldap_firstname_field = strtolower($account->ldap_firstname_field);
		$settings->ldap_lastname_field = strtolower($account->ldap_lastname_field);
		$settings->ldap_email_field = strtolower($account->ldap_email_field);
		$settings->ldap_check_lms_first = $account->ldap_check_lms_first;
        $settings->ldap_username_filter = $account->ldap_username_filter;

		if (isset($_POST['LdapAppSettingsForm'])) {
			$settings->setAttributes($_POST['LdapAppSettingsForm']);

			$settings->ldap_username_field = strtolower($settings->ldap_username_field);
			$settings->ldap_firstname_field = strtolower($settings->ldap_firstname_field);
			$settings->ldap_lastname_field = strtolower($settings->ldap_lastname_field);
			$settings->ldap_email_field = strtolower($settings->ldap_email_field);

			if ($settings->validate()) {
				$account->ldap_server = $settings->ldap_server;
				$account->ldap_port = $settings->ldap_port;
				$account->ldap_user_string = $settings->ldap_user_string;
				$account->ldap_used = $settings->ldap_used;
				$account->ldap_use_ssl = $settings->ldap_use_ssl;
				$account->ldap_base_dn = $settings->ldap_base_dn;
				$account->ldap_admin_enabled = $settings->ldap_admin_enabled;
				$account->ldap_admin_username = $settings->ldap_admin_username;

				if($settings->ldap_admin_password == Yii::app()->session['ldap_admin_password_fake'] && isset(Yii::app()->session['ldap_admin_password']))
					$account->ldap_admin_password = Yii::app()->session['ldap_admin_password'];
				else
					$account->ldap_admin_password = $settings->ldap_admin_password;

				$account->ldap_username_field = $settings->ldap_username_field;
				$account->ldap_firstname_field = $settings->ldap_firstname_field;
				$account->ldap_lastname_field = $settings->ldap_lastname_field;
				$account->ldap_email_field = $settings->ldap_email_field;
				$account->ldap_check_lms_first = $settings->ldap_check_lms_first;
                $account->ldap_username_filter = $settings->ldap_username_filter;

				if($account->save()){
				{
					Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));
					Yii::app()->session['ldap_admin_password'] = $account->ldap_admin_password;
				}
				}else{
					Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
				}
			}
			else {
				Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
			}
		}

		$settings->ldap_admin_password = Yii::app()->session['ldap_admin_password_fake'];

		$this->render('settings', array('settings' => $settings));
	}

	private function getLdapUsers($perPage = 500) {
		$ldap_server = LdapSettingsHelper::get('ldap_server');
		$ldap_port = LdapSettingsHelper::get('ldap_port');
		$ldap_user_string = LdapSettingsHelper::get('ldap_user_string');
		$ldap_used = LdapSettingsHelper::get('ldap_used');
        $ldap_username_filter = LdapSettingsHelper::get('ldap_username_filter');

		// We ignore pagination for now and get the full list
		// of LDAP users always, because based on testing and
		// example 2 on this page: http://php.net/manual/en/function.ldap-control-paged-result.php
		// we can only do pagination of LDAP results during a single
		// PHP request. (E.g. we can't get page 2 of results
		// in a subsequent AJAX call, we have to get all results
		// during the current request, be it paginated or not)

		if(!$ldap_used){
			Yii::log('Ldap is not enabled in the app settings', CLogger::LEVEL_WARNING);
			$this->redirect(Docebo::createLmsUrl('LdapApp/LdapApp/settings'));
		}

		if(!$ldap_server){
			Yii::log('LDAP server not set', CLogger::LEVEL_WARNING);
			$this->redirect(Docebo::createLmsUrl('LdapApp/LdapApp/settings'));
		}

		$ldapUsers = LdapUserSync::getLdapUsers($ldap_server, $ldap_port, LdapSettingsHelper::get('ldap_base_dn', null), null, null, $perPage);
		if(!$ldapUsers)
			Yii::log('No LDAP users returned from server', CLogger::LEVEL_WARNING);

		// Leave only the clean users array
		unset($ldapUsers['count']);

		$res = array();
		if($ldapUsers){
			foreach($ldapUsers as $ldapUserArr){

				// Initialize the blank array for better readability
				$singleLdapUser = array(
					'username' => null,
					'firstname' => null,
					'lastname' => null,
					'email' => null, // must also be used as array key
					'force_pwd_change' => 0,
					'suspended' => 0,
					'is_admin' => 0,
					'admin' => 0,
					'exists_in_lms' => false, // a check is performed later
					'lmsModel' => null,
				);

				$singleLdapUser['username'] = $this->getAttributeValue($ldapUserArr, LdapSettingsHelper::get('ldap_username_field', 'userprincipalname'));
				if(!$singleLdapUser['username'])
					continue;

                if($ldap_username_filter && !empty($ldap_username_filter)){
                    $ldap_user_string = $ldap_username_filter;
                }

				$matches = array();
				preg_match('/'.str_replace('$user', '(.+)', $ldap_user_string).'/i', $singleLdapUser['username'], $matches);
				if(!$matches[1])
					continue;
				else
					$singleLdapUser['username'] = $matches[1];

				$singleLdapUser['email'] = $this->getAttributeValue($ldapUserArr, LdapSettingsHelper::get('ldap_email_field', 'mail'));
				$singleLdapUser['firstname'] = $this->getAttributeValue($ldapUserArr, LdapSettingsHelper::get('ldap_firstname_field', 'givenname'));
				$singleLdapUser['lastname'] = $this->getAttributeValue($ldapUserArr, LdapSettingsHelper::get('ldap_lastname_field', 'sn'));

				$res[$singleLdapUser['username']] = $singleLdapUser;
			}
		}

		// Now check which ones from LDAP exist in the LMS (email based check)
		$ldapUsernames = array_keys($res);

		// We store the usernames in the LMS as "/username",
		// so duplicate the usernames array with entries with that slash before them
		foreach($ldapUsernames as $username){
			$ldapUsernames[] = '/'.$username;
		}

		if($ldapUsernames)
		{
			$existingUsers = Yii::app()->db->createCommand()
				->select('idst, userid')
				->from(CoreUser::model()->tableName())->queryAll();

			foreach($existingUsers as $row)
			{
				$lmsUsernameClean = ltrim($row['userid'], '/');
				if(isset($res[$lmsUsernameClean]))
				{
					$res[$lmsUsernameClean]['exists_in_lms'] = true;
					$res[$lmsUsernameClean]['exists_idst'] = $row['idst'];
				}
			}

			unset($existingUsers);
		}

		return $res;
	}

	/**
	 * Shows the sync users from LDAP page
	 *
	 * @throws CException
	 * @see ldap_get_entries() for the format returned to $ldapUsers
	 */
	public function actionImportUsers() {
		try {
			// Get the users array from the cache
			$cacheKey = $this->_getCacheKey();
			$page = Yii::app()->request->getParam('page');
			if(!$page) {
				// Trigger LDAP call to read users from LDAP and save them in cache
				$res = $this->getLdapUsers();
				Yii::app()->cache_ldap_users->set($cacheKey, $res);
			} else
				$res = Yii::app()->cache_ldap_users->get($cacheKey);

			$ldapUsersDataProvider = new CArrayDataProvider(array_values($res), array(
				'pagination' => array('pageSize' => 50),
			));

			$this->render('importUsers', array(
				'usersCountDocebo' => (int) CoreUser::model()->getUsersCount(),
				'ldapUsersCount' => (int) count($res), //$this->countLdapUsers(),
				'ldapUsers' => $ldapUsersDataProvider,
			));

			Yii::app()->end();

		} catch(CHttpException $e) {
			Yii::app()->user->setFlash('error', Yii::t('ldap', $e->getMessage()));
		} catch(Exception $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
		}

		$this->redirect(Docebo::createAppUrl('admin:userManagement/index'));
	}

	/**
	 * Returns the cache key for the ldap user import
	 */
	private function _getCacheKey() {
		$cacheKeyActors = array(
			Yii::app()->db->connectionString
		);

		$cacheKey = md5(json_encode($cacheKeyActors));
		return $cacheKey;
	}

	/**
	 * Progressive imported action
	 */
	public function actionProgressiveImportUsers() {

		// Get the users array from the cache
		$cacheKey = $this->_getCacheKey();
		$res = Yii::app()->cache_ldap_users->get($cacheKey);
		if(!$res)
			$res = array();

		// Get current offset and calculate stop flag
		$offset = isset($_REQUEST['offset']) ? $_REQUEST['offset'] : 0;
		$totalCount = count($res);
		$stop = ($offset < $totalCount) ? false : true;
		$newOffset = $offset + 100;
		$failedImportsArray = Yii::app()->request->getParam('failedImportsArray', array());


		if(!isset($_REQUEST['result']) || !$_REQUEST['result']){
			// Preparing result for importer
			$result = array (
				'usersCreated'=>0,
				'usersUpdated'=>0,
				'usersFailed'=>0,
				'synchronised'=>0,
			);
		}else{
			$result = $_REQUEST['result'];
		}

		if(!isset($_REQUEST['csvErrorFileName']) || !$_REQUEST['csvErrorFileName']){
			$csvErrorFileName =  Docebo::randomHash().'.csv';
		}else{
			$csvErrorFileName = $_REQUEST['csvErrorFileName'];
		}


		if(!isset($_REQUEST['csvErrorFilePath'])){
			$csvErrorFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $csvErrorFileName;
		}else{
			$csvErrorFilePath = $_REQUEST['csvErrorFilePath'];
		}


		$index = $offset;
		$res = array_slice($res, $offset, 100);
		foreach($res as $ldapUser) {
			try {
				if(!$ldapUser['exists_in_lms']) {
					// Create that LDAP user in the LMS
					$newLmsUser = new CoreUser('create');
					$newLmsUser->unsetAttributes();
					$newLmsUser->userid = '/'.$ldapUser['username'];
					$newLmsUser->email = trim($ldapUser['email']);
					$newLmsUser->new_password = $newLmsUser->new_password_repeat = CoreUser::model()->encryptPassword(uniqid(substr($newLmsUser->email, 0, 10) . "%", true));
					$newLmsUser->firstname = $ldapUser['firstname'];
					$newLmsUser->lastname = $ldapUser['lastname'];
					if(!$newLmsUser->save()){
						foreach($newLmsUser->getErrors() as $error){
							if($ldapUser['username']){
								$failedImportsArray[$ldapUser['username']] = $error;
							}else{
								$failedImportsArray[] = $error;
							}
						}

						throw new CHttpException(500, CHtml::errorSummary($newLmsUser));
					}
					else {
						// Assign this user to the student level group
						$auth = Yii::app()->authManager;
						$auth->assign(Yii::app()->user->level_user, $newLmsUser->idst);
						$newLmsUser->saveChartGroups();
					}
					$result['usersCreated']++;
					$result['synchronised']++;
				} else {
					// Update existing LMS user
					$userToUpdate = CoreUser::model()->findByPk($ldapUser['exists_idst']);
					if($userToUpdate)
					{
						/* @var $userToUpdate CoreUser */
						$userToUpdate->setScenario('update');
						$userToUpdate->firstname = $ldapUser['firstname'];
						$userToUpdate->lastname = $ldapUser['lastname'];
						$userToUpdate->email = trim($ldapUser['email']);
						if(!$userToUpdate->save()){
							foreach($userToUpdate->getErrors() as $error){
								if($userToUpdate->userid){
									$failedImportsArray[$userToUpdate->userid] = $error;
								}else{
									$failedImportsArray[] = $error;
								}
							}
							throw new CHttpException(500, CHtml::errorSummary($userToUpdate));
						}

						$result['usersUpdated']++;
						$result['synchronised']++;
					}
				}
			} catch(Exception $e) {
				Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);

				$result['usersFailed']++;
			}

			$index++;
		}

		$completedPercent = $totalCount==0 ? 0 : (round($index/$totalCount * 100, 0));
		if($completedPercent>100) $completedPercent = 100;
		if($completedPercent<0) $completedPercent = 0;
		if($stop){
			$nextPhase = 'finish';
			if (!empty($failedImportsArray) && $csvErrorFilePath) {
				$fh = fopen($csvErrorFilePath, 'w');
				foreach ($failedImportsArray as $key => $failedLine) {
					$failedLine = array_merge(array($key=>$key), $failedLine);
					$failedLine =  CSVParser::arrayToCsv($failedLine);
					fputs($fh, $failedLine . "\n");
				}
				fclose($fh);
			}
		}else{
			$nextPhase = 'synch';
		}

		$html = $this->renderPartial('progressive_dialog', array(
			'offset' => $offset,
			'created' => $result['usersCreated'],
			'updated' => $result['usersUpdated'],
			'synchronised' => $result['synchronised'],
			'failed' => $result['usersFailed'],
			'totalUsers' => $totalCount,
			'nextPhase' => $nextPhase,
			'completedPercent' => $completedPercent,
			'stop' => $stop,
			'csvErrorFileName' => $csvErrorFileName,
		), true, true);

		// Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
		// These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stoppping when $stop == true)
		$data = array(
			'offset'	=> $newOffset,
			'stop'		=> $stop,
			'csvErrorFilePath' => $csvErrorFilePath,
			'failedImportsArray' => $failedImportsArray,
			'result'     => $result,
			'csvErrorFileName' => $csvErrorFileName,
		);
		$response = new AjaxResult();
		$response->setStatus(true);
		$response->setHtml($html)->setData($data)->toJSON();
		Yii::app()->end();
	}

	/**
	 * Reads an attribute from the ldap entry
	 * @param $ldapEntry
	 * @param $attributeName
	 * @return bool
	 */
	private function getAttributeValue($ldapEntry, $attributeName){
		if(isset($ldapEntry[$attributeName])){
			if(is_array($ldapEntry[$attributeName])){
				return $ldapEntry[$attributeName][0];
			}
		}

		return FALSE;
	}
}
