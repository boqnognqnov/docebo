<?php

/**
 * Plugin for LDAP login
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */
class LdapApp extends DoceboPlugin {

	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'LdapApp';

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 *
	 * @param string $class_name
	 *
	 * @return Plugin Object
	 */
	public static function plugin ( $class_name = __CLASS__ ) {
		return parent::plugin( $class_name );
	}

	public function activate () {
		// sso_token, don't touch it
		// sso_secret, don't touch it on activation

        Yii::import('plugin.LdapApp.models.*');

		try {
			Yii::app()->getDb()->createCommand()->createTable( 'ldap_app_account', array(
				'id'                   => 'pk',
				'id_multidomain'       => 'integer',
				'ldap_server'          => 'string',
				'ldap_port'            => 'string',
				'ldap_user_string'     => 'string',
				'ldap_used'            => 'boolean',
				'ldap_use_ssl'         => 'boolean',
				'ldap_base_dn'         => 'string',
				'ldap_admin_enabled'   => 'boolean',
				'ldap_admin_username'  => 'string',
				'ldap_admin_password'  => 'string',
				'ldap_username_field'  => 'string',
				'ldap_firstname_field' => 'string',
				'ldap_lastname_field'  => 'string',
				'ldap_email_field'     => 'string',
				'ldap_check_lms_first' => 'boolean'
			) );
		} catch ( Exception $e ) {
		}

        parent::activate();
	}

	public function deactivate () {
		parent::deactivate();
	}

}
