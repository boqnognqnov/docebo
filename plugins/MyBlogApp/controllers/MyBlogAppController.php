<?php

class MyBlogAppController extends Controller {


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		$admin_only_actions = array('settings');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}

    public function init()
    {
        parent::init();
        $this->registerResources();
    }

    /**
     * Register module specific assets
     *
     * (non-PHPdoc)
     * @see LoBaseController::registerResources()
     */
    public function registerResources()
    {
        if (!Yii::app()->request->isAjaxRequest)
        {
            $cs = Yii::app()->getClientScript();
            $cs->registerCssFile(Yii::app()->baseUrl.'/../plugins/MyBlogApp/assets/css/myblog.css');
            Yii::app()->tinymce->init();
        }
    }

	/**
	 * Step1: settings dialog where the user can set the custom domain name
	 */
	public function actionSettings() {

		$settings = new MyBlogAppSettingsForm();

		if (isset($_POST['MyBlogAppSettingsForm'])) {
			$settings->setAttributes($_POST['MyBlogAppSettingsForm']);

			if ($settings->validate() ) {
                Settings::save('myblog_show_set_tone', $settings->myblog_show_set_tone);
                Settings::save('myblog_set_tone_icons', $settings->myblog_set_tone_icons);
                Settings::save('myblog_show_helpful', $settings->myblog_show_helpful);

                Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));
			}
			else {
				Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
			}
		}

        $settings->myblog_show_set_tone = Settings::get('myblog_show_set_tone', 1);
        $settings->myblog_set_tone_icons = Settings::get('myblog_set_tone_icons', 'smiley');
        $settings->myblog_show_helpful = Settings::get('myblog_show_helpful', 1);

		$this->render('settings', array('settings' => $settings));
	}

    public function actionIndex() {

        /* @var $cs CClientScript */
        $cs = Yii::app()->getClientScript();

        // Bootcamp menu
        $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/docebo_bootcamp.js');
        $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/docebo_bootcamp.css');

        $this->render('blog', array(
            'isBlogAdmin' => true,
        ));
    }

    public function actionBlog() {

        $author = CoreUser::model()->findByPk(Yii::app()->request->getParam('idUser'));
        if (!$author)
            $this->redirect(Docebo::createAppUrl('lms:site/index'));

        /* @var $cs CClientScript */
        $cs = Yii::app()->getClientScript();
	    // Bootcamp menu
	    $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/docebo_bootcamp.js');
	    $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/docebo_bootcamp.css');

        $this->render('blog', array(
            'isBlogAdmin' => ($author->idst == Yii::app()->user->id),
            'author' => $author
        ));
    }

    public function actionAxEdit()
    {
        $model = null;
        $id = intval(Yii::app()->request->getParam('idPost'));
        $idCourse = Yii::app()->request->getParam('idCourse', null);

        if ($id > 0) {
            $model = MyblogPost::model()->findByPk($id);
        }
        if (!$model) {
            $model = new MyblogPost();
            $model->id_user = Yii::app()->user->id;
            $model->id_course = $idCourse;
        }

        if (isset($_POST['MyblogPost'])) {
            $postData = $_POST['MyblogPost'];
			$postData['title'] = trim($postData['title']);
			$postData['content'] = Yii::app()->htmlpurifier->purify($postData['content'], 'allowFrameAndTarget');

            $model->attributes = $postData;
            $model->posted_on = Yii::app()->localtime->toLocalDateTime();

            if ($model->save()) {
                // close dialog and reload
                echo '<a href="" class="auto-close"></a>';
                return;
            }
        }

        $this->renderPartial('_edit', array(
            'model' => $model
        ));
    }

    public function actionAxLoadPosts() {

        $response = new AjaxResult();

        $page = Yii::app()->request->getParam('page');
        $idUser = Yii::app()->request->getParam('idUser');
        $idCourse = Yii::app()->request->getParam('idCourse');

        $data = array(
            "html" => $this->renderPartial('_blog_posts', array(
                'page' => $page,
                'idUser' => $idUser,
                'idCourse' => $idCourse
            ))
        );

        $response->setStatus(true)->setData($data)->toJSON();
        Yii::app()->end();
    }

    public function actionAxLoadComments() {

        $page = intval(Yii::app()->request->getParam('page'));

        $options = array(
            'pageSize' => 3,
            'page' => $page
        );

        $model = new MyblogComment('search');
        $model->id_post = Yii::app()->request->getParam('idPost');
        $dataProvider = $model->dataProvider($options);


        $myblog_show_helpful = Settings::get('myblog_show_helpful', 0);
        $showHelpfulInComments = in_array($myblog_show_helpful, array(3, 4));

        $myblog_show_set_tone = Settings::get('myblog_show_set_tone', 0);
        $showRatingInComments = in_array($myblog_show_set_tone, array(3, 4));

        $ratingType = Settings::get('myblog_set_tone_icons', 'star');

        $this->renderPartial('_blog_comments', array(
            'dataProvider' => $dataProvider,
            'showHelpful' => $showHelpfulInComments,
            'showRating' => $showRatingInComments,
            'ratingType' => $ratingType
        ));
    }

    public function actionAxVoteHelpful() {

        if (Yii::app()->request->isAjaxRequest)
        {
            $idUser = Yii::app()->user->id;
            $idPost = intval( Yii::app()->request->getParam('idPost') );
            $idComment = intval( Yii::app()->request->getParam('idComment') );
            $value = intval( Yii::app()->request->getParam('value') );

            // vote post or comment
            if ($idPost > 0) {
				try {
					$model = new MyblogPostVote();
					$myblogPostVote = $model->tableName();
					Yii::app()->db->createCommand("
DELETE FROM $myblogPostVote
WHERE id_post=$idPost AND id_user=$idUser AND type='helpful'")->query();
					$model->id_user = $idUser;
					$model->id_post = $idPost;
					$model->type = 'helpful';
					$model->value = ($value > 0) ? 1 : -1;
					$model->save();
				}
				catch (CException $ex) {}

				// return the number of likes/dislikes
				$myblogPost = new MyblogPost();
				$myblogPost->id_post = $idPost;
				$helpful = $myblogPost->getHelpful();
				echo CJSON::encode($helpful);
            }
            else if ($idComment > 0) {
				try {
					$model = new MyblogCommentVote();
					$myblogCommentVote = $model->tableName();
					Yii::app()->db->createCommand("
	DELETE FROM $myblogCommentVote
	WHERE id_comment=$idComment AND id_user=$idUser AND type='helpful'")->query();
					$model->id_user = $idUser;
					$model->id_comment = $idComment;
					$model->type = 'helpful';
					$model->value = ($value > 0) ? 1 : -1;
					$model->save();
				}
				catch (CException $ex) {}

				// return the number of likes/dislikes
				$myblogComment = new MyblogComment();
				$myblogComment->id_comment = $idComment;
				$helpful = $myblogComment->getHelpful();
				echo CJSON::encode($helpful);
            }
        }

    }

    public function actionAxVoteTone() {

        if (Yii::app()->request->isAjaxRequest)
        {
            $idUser = Yii::app()->user->id;
            $idPost = intval( Yii::app()->request->getParam('idPost') );
            $idComment = intval( Yii::app()->request->getParam('idComment') );
            $value = intval( Yii::app()->request->getParam('value') );

            // vote post or comment
            if ($idPost > 0) {
                $model = MyblogPost::model()->findByPk($idPost);
            }
            else if ($idComment > 0) {
                $model = MyblogComment::model()->findByPk($idComment);
            }

            if ($model) {
                try {
                    $model->saveRating($idUser, $value);
                }
                catch (CDbException $ex) {

                }
            }
        }

    }

    public function actionAxDeletePost() {

        if (Yii::app()->request->isPostRequest) {

            $idPost = Yii::app()->request->getParam('idPost');

            $post = MyblogPost::model()->findByPk($idPost);

            if ($post && $post->canUserEditDelete()) {
                $status = $post->delete();

                // close dialog and reload locations
                echo CHtml::tag('a', array('class'=>'auto-close', 'href'=>$this->createUrl('index')));
                Yii::app()->end();
            }
        }

        $this->renderPartial('_delete_comment');
    }

    public function actionAxDeleteComment() {

        if (Yii::app()->request->isPostRequest) {

            $idComment = Yii::app()->request->getParam('idComment');
            $block = Yii::app()->request->getParam('block', null);

            $comment = MyblogComment::model()->with('post')->findByPk($idComment);

            // only post authors may delete comments
            if ($comment && $comment->canUserEditDelete()) {
                $status = $comment->delete();

                // close dialog and reload locations
                echo CHtml::tag('a', array('class'=>'auto-close', 'data-remove-comment'=>($status ? $idComment : ''), 'data-post' => $comment->id_post, 'data-block'=> $block));
                Yii::app()->end();
            }
        }

        $this->renderPartial('_delete_comment');
    }

    public function actionAxEditComment() {

        if (isset($_POST['MyblogComment'])) {

            $postData = $_POST['MyblogComment'];
            $comment = MyblogComment::model()->findByPk($postData['id_comment']);

            // only post authors may delete comments
            if ($comment && $comment->canUserEditDelete()) {
                $comment->content = trim($postData['content']);
                if ($comment->save())
                    echo CHtml::encode($comment->content);
            }
        }
    }

	public function actionCoursePost()
	{
		$this->actionPost(Yii::app()->request->getParam('idCourse'));
	}

    public function actionPost($from_course = false) {

        /* @var $post MyblogPost */
        $post = MyblogPost::model()->with(array(
            'user',
            'comments' => array(
                'order' => 'id_comment DESC'
            )
        ))->findByPk(Yii::app()->request->getParam('idPost'));

        if (!$post)
            $this->redirect(Docebo::createAppUrl('lms:site/index'));

        if (isset($_POST['MyblogComment'])) {

            $comment = new MyblogComment();
            $comment->content = trim($_POST['MyblogComment']['content']);
            $comment->id_post = $post->id_post;
            $comment->id_user = Yii::app()->user->id;
            $comment->posted_on = Yii::app()->localtime->toLocalDateTime();
            $comment->save();
            //Yii::app()->end();
        }

        $this->render('post', array(
            'post' => $post,
            'author' => ($post->id_user != Yii::app()->user->id) ? $post->user : null,
		  'from_course' => $from_course
        ));
    }

    public function actionAxUsersAutocomplete(){
        $query = $_REQUEST['tag'];

        $command = Yii::app()->getDb()->createCommand();
        $command->select('u.idst, u.userid, u.firstname, u.lastname, u.email')->from(CoreUser::model()->tableName().' u');
        $command->where('u.userid LIKE :query OR u.firstname LIKE :query OR u.lastname LIKE :query', array(':query'=>'%'.$query.'%'));
        $command->limit(100);

        if(Yii::app()->user->getIsPu()){
            // PU users filter
            $command->join(CoreUserPU::model()->tableName().' pu', 'pu.user_id=u.idst AND puser_id=:puId', array(':puId'=>Yii::app()->user->getIdst()));
        }

        $results = $command->queryAll();

        $res = array();
        foreach($results as $row){
            $firstname = $row['firstname'];
            $lastname = $row['lastname'];
            $email = $row['email'];
            $userid = $row['userid']; // username

            $name = $firstname . " " . $lastname;
            $label = ltrim($userid, '/') . (trim($name) ? ' - ' . trim($name) : '');

            $res[] = array(
                'key'=>$row['idst'],
                'value'=>$label
            );
        }

        echo CJSON::encode($res);
    }

    public function actionAxLoadDashletComments(){
        $id = Yii::app()->request->getParam('idPost', null);
        $currentPage = Yii::app()->request->getParam('page', 0);
        $block = Yii::app()->request->getParam('block', null);
        $criteria = new CDbCriteria(array(
            'condition' => 'id_post = ' . $id,
            'order' => 'posted_on DESC',
            'limit' => 3,
            'offset' => 3 * $currentPage
        ));
        $comments = MyblogComment::model()->findAll($criteria);
        if(!empty($comments)){

            $myblog_show_helpful = Settings::get('myblog_show_helpful', 0);
            $showHelpfulInComments = in_array($myblog_show_helpful, array(3, 4));

            $myblog_show_set_tone = Settings::get('myblog_show_set_tone', 0);
            $showRatingInComments = in_array($myblog_show_set_tone, array(3, 4));

            $ratingType = Settings::get('myblog_set_tone_icons', 'star');

            $html = $this->renderPartial('_axDashletComments', array(
                'comments'      => $comments,
                'showHelpful'   => $showHelpfulInComments,
                'showRating'    => $showRatingInComments,
                'ratingType'    => $ratingType,
                'block'         => $block
            ), true, true);

            echo json_encode(array('hasComments' => true, 'html' => $html));

        }else{
            echo json_encode(array('hasComments' => false));
        }

    }

    public function actionAxNewPostReply(){
        $id = Yii::app()->request->getParam('idPost', null);
        $comment = Yii::app()->request->getParam('reply', null);
        $post = MyblogPost::model()->findByPk($id);
        if($post !== null){
            $reply = new MyblogComment();
            $reply->id_post = $id;
            $reply->id_user = Yii::app()->user->id;
            $reply->content = $comment;
            $reply->posted_on = Yii::app()->localtime->toLocalDateTime();
            if($reply->save()){
                echo json_encode(array('saved' => true));
                Yii::app()->end();
            }else{
                Yii::log(json_encode($reply->getErrors()));
                Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
                echo json_encode(array('saved' => false));
                Yii::app()->end();
            }
        }
        Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
        echo json_encode(array('saved' => false));
    }

    public function actionAxEditPostReply(){
        $id = Yii::app()->request->getParam('idComment', null);
        $content = Yii::app()->request->getParam('reply', null);
        $comment = MyblogComment::model()->findByPk($id);
        if($comment !== null){
            $comment->content = $content;
            if($comment->save()){
                echo json_encode(array('saved' => true));
                Yii::app()->end();
            }else{
                Yii::log(json_encode($comment->getErrors()));
                Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
                echo json_encode(array('saved' => false));
                Yii::app()->end();
            }
        }
        Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
        echo json_encode(array('saved' => false));
    }
}