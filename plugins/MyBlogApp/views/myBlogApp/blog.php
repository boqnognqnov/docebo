<?php
/* @var $this MyBlogAppController */
/* @var $author CoreUser */
/* @var $isBlogAdmin bool */

if ($author) {
    $this->breadcrumbs[] = $author->getFullName();
}
$this->breadcrumbs[] = Yii::t('myblog', 'My Blog');
?>

<div class="row-fluid">

    <div class="span8">

        <?php if ($isBlogAdmin) : ?>
        <h2><?= Yii::t('myblog', 'My Blog') ?></h2>
        <br>

        <ul id="docebo-bootcamp-locations" class="docebo-bootcamp clearfix">
            <li>
                <a id="add-new-location"
                   href="<?= $this->createUrl('axEdit') ?>"
                   class="open-dialog"
                   closeOnOverlayClick="false"
                   closeOnEscape="false"
                   data-dialog-class="modal-edit-blogpost"
                   data-helper-title="<?= Yii::t('myblog', 'New Blog Post') ?>"
                   data-helper-text="<?= Yii::t('helper', 'New blog post - blog') ?>">
                    <span class="myblog-sprite chat-bubble-large"></span>
                    <span class="myblog-sprite chat-bubble-large white"></span>
                    <h4><?= Yii::t('myblog', 'New Blog Post') ?></h4>
                </a>
            </li>

            <li class="helper">
                <a href="#">
                    <span class="i-sprite is-circle-quest large"></span>
                    <h4 class="helper-title"></h4>
                    <p class="helper-text"></p>
                </a>
            </li>

        </ul>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#docebo-bootcamp-locations').doceboBootcamp();
                });
            </script>
        <?php endif; ?>
        <br/>

        <?php
        $this->widget('BlogPostList', array(
            'idUser'=> ($author ? $author->idst : Yii::app()->user->id),
            'isAdminMode'=>$isBlogAdmin
        ));
        ?>
    </div>


    <div class="span4">
        <?php $this->widget('BlogSidebar', array(
            'idUser' => ($author ? $author->idst : Yii::app()->user->id),
            'searchUrl' => ($author
                ? Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/blog', array('idUser'=>$author->idst))
                : Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/index')
            )
        )); ?>
    </div>
</div>