<?php
/* @var $this MyBlogAppController */
/* @var $dataProvider CActiveDataProvider */
/* @var $showHelpful bool */
/* @var $showRating bool */
/* @var $ratingType string */

/* @var $form CActiveForm */
/* @var $_comment MyblogComment */

?>

<?php foreach ($dataProvider->getData() as $_comment) : ?>
    <div class="blogcomment" id="blogcomment-<?= $_comment->id_comment ?>">
        <div class="blogcomment-header clearfix">
            <span class="author-picture"><?= $_comment->user->getAvatarImage() ?></span>

            <div class="comment-details">
                <div class="author">
                    <span class="myblog-sprite user"></span>
                    <?= $_comment->user->getFullName() ?>
                </div>
                <div class="date">
                    <span class="myblog-sprite calendar"></span>
                    <?= $_comment->posted_on ?>
                </div>
            </div>
        </div>

        <div class="blogcomment-content">
            <div class="inner">
                <div class="comment-preview"><?= CHtml::encode($_comment->content) ?></div>
                <div class="comment-edit">
                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'method' => 'POST',
                        'action' => Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/axEditComment')
                    ));
                        echo $form->hiddenField($_comment, 'id_comment');
                        echo $form->textArea($_comment, 'content');
                    ?>
                        <input class="btn-docebo green big" type="submit" value="<?php echo Yii::t('standard', '_SAVE'); ?>" />
                    <?php $this->endWidget(); ?>
                </div>

                <?php if ($_comment->canUserEditDelete()) : ?>
                <div class="actions">
                    <a class="blogcomment-edit" href="#"><i class="myblog-sprite edit"></i></a>
                    <a class="blogcomment-delete open-dialog" data-dialog-class="myblog-delete-confirm-dialog" href="<?= Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/axDeleteComment', array('idComment'=>$_comment->id_comment)) ?>"><i class="myblog-sprite delete"></i></a>
                </div>
                <?php endif; ?>
            </div>
        </div>

        <div class="blogcomment-footer clearfix">

            <?php if ($showRating) : ?>
                <div class="blog-rating">
                    <label><?= Yii::t('myblog', 'Set tone:') ?></label>
                    <?php
					// if user is creator of the comment, can't give evaluation!
					$isReadOnly = $_comment->id_user == Yii::app()->user->id ? true : false;
					$this->widget('common.widgets.DoceboStarRating', array(
						'id' => 'comment-rating-' . $_comment->id_comment,
						'name' => 'comment_rating_' . $_comment->id_comment,
						'type' => $ratingType,

						// possible bug - not working evaluation! FIXED only once, we need for every user!
						'readOnly' => $isReadOnly, // ($_comment->id_user == Yii::app()->user->id),
						'value' => $_comment->getRating(),
						'urlParams' => array(
							'idComment' => $_comment->id_comment
						)
					));
                    ?>
                </div>
            <?php endif; ?>

            <?php if ($showHelpful) : ?>

                <?php
                $_commentHelpful = $_comment->getHelpful();
                $this->widget('common.widgets.DoceboHelpful',array(
                    'id'=>'comment-helpful-'.$_comment->id_comment,
                    'likes' => $_commentHelpful['likes'],
                    'dislikes' => $_commentHelpful['dislikes'],
                    'vote' => ($_comment->id_user == Yii::app()->user->id) ? 'readonly' : $_comment->getUserHelpfulVote(Yii::app()->user->id),
                    'ratingUrl' => Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/axVoteHelpful', array('idComment' => $_comment->id_comment))
                ));
                ?>

            <?php endif; ?>

        </div>
    </div>
<?php endforeach; ?>