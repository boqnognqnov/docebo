<?php
/* @var $this MyBlogAppController */
/* @var $model MyblogPost */
/* @var $form CActiveForm */
?>
<h1><?= (true===$model->getIsNewRecord()) ? Yii::t('myblog', 'New Blog Post') : Yii::t('myblog', 'Edit Blog Post') ?></h1>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'edit-blogpost-form',
    'method' => 'POST',
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
        'class' => 'ajax docebo-form'
    ),
));
    echo $form->hiddenField($model, 'id_post');

?>

    <?= $form->labelEx($model, 'title') ?>
    <?= $form->textField($model, 'title', array('class'=>'input-block-level')) ?>

    <?= $form->labelEx($model, 'content') ?>
    <?= $form->textArea($model, 'content', array('class'=>'')) ?>

<div class="form-actions">
    <input class="btn-docebo green big" type="submit" value="<?= Yii::t('standard', '_PUBLISH') ?>" />
    <input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">

    (function() {

        $(document).delegate(".modal-edit-blogpost", "dialog2.content-update", function() {
            var e = $(this);
            var $modal = e.find('.modal-body');

            var autoclose = e.find("a.auto-close");

            if (autoclose.length > 0) {
                e.find('.modal-body').dialog2("close");

                <?php if (intval($model->id_course) > 0) : ?>
                    var options = {};
                    $('.player-block-blog').loadOneBlockContent(options);
                <?php else: ?>
                    var href = autoclose.attr('href');
                    if (href) {
                        window.location.href = href;
                    } else {
                        window.location.reload();
                    }
                <?php endif; ?>
            }
            else {

                var $icon = $('<span/>').attr('class','myblog-sprite chat-bubble-large white');

                var $modalTitle = $modal.parents('.modal').eq(0).find('.modal-header h3');
                $modalTitle.find('span').remove();
                $modalTitle.prepend($icon);

                // init tinymce
                TinyMce.attach('#<?= CHtml::activeId($model, 'content') ?>', 'standard_embed', {height: 200});

            }

            $('#edit-blogpost-form').bind('form-pre-serialize', function() {
                tinyMCE.triggerSave();
            });
        });

        // On closing the dialog we must destroy TinyMce editors;
        $(document).delegate(".modal-edit-blogpost", "dialog2.closed", function() {
            TinyMce.removeEditorById('<?= CHtml::activeId($model, 'content') ?>');
        });

    })();
</script>

