<?php foreach ($comments as $_comment) : ?>
    <div class="dashletComment" id="dashletComment-<?= $_comment->id_comment ?>" data-edit-url="<?= Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/AxEditPostReply', array('idComment' => $_comment->id_comment));?>">

        <div class="dashletComment-header">
            <div class="span1">
                <span class="author-picture"><?= $_comment->user->getAvatarImage() ?></span>
            </div>
            <div class="span11">
                <div class="dashletcomment-details">
                    <div class="dashletcomment-details-author-date">
                        <div class="author">
                            <?= $_comment->user->getFullName() ?>
                        </div>
                        <div class="date">
                            <?= $_comment->posted_on ?>
                        </div>
                    </div>

                    <?php if ($_comment->canUserEditDelete()) : ?>
                        <div class="actions">
                            <a class="blogcomment-edit" href="#" editPost="<?= $_comment->id_post ?>"><i class="myblog-sprite edit"></i></a>
                            <a class="blogcomment-delete open-dialog" data-idComment="<?= $_comment->id_comment ?>" data-dialog-class="myblog-ax-delete-confirm-dialog" href="<?= Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/axDeleteComment', array('idComment'=>$_comment->id_comment, 'block' => $block)) ?>"><i class="myblog-sprite delete"></i></a>
                        </div>
                    <?php endif; ?>

                </div>
                <div class="dashletcomment-content">
                    <div class="inner">
                        <div class="comment-preview"><?= CHtml::encode($_comment->content) ?></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="dashletComment-footer clearfix">

            <?php if ($showRating) : ?>
                <div class="span6">
                    <div class="blog-rating">
                        <label><?= Yii::t('myblog', 'Set tone:') ?></label>
                        <?php
                        // if user is creator of the comment, can't give evaluation!
                        $isReadOnly = $_comment->id_user == Yii::app()->user->id ? true : false;
                        $this->widget('common.widgets.DoceboStarRating', array(
                            'id' => 'comment-rating-' . $_comment->id_comment . '-' . Docebo::randomHash(),
                            'name' => 'comment_rating_' . $_comment->id_comment . '-' . Docebo::randomHash(),
                            'type' => $ratingType,
                            'readOnly' => $isReadOnly,
                            'value' => $_comment->getRating(),
                            'ratingUrl' => Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/axVoteTone'),
                            'urlParams' => array(
                                'idComment' => $_comment->id_comment
                            )
                        ));
                        ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($showHelpful) : ?>
                <div class="span6">
                    <?php
                    $_commentHelpful = $_comment->getHelpful();
                    $this->widget('common.widgets.DoceboHelpful',array(
                        'id'=>'comment-helpful-'.$_comment->id_comment,
                        'likes' => $_commentHelpful['likes'],
                        'dislikes' => $_commentHelpful['dislikes'],
                        'vote' => ($_comment->id_user == Yii::app()->user->id) ? 'readonly' : $_comment->getUserHelpfulVote(Yii::app()->user->id),
                        'ratingUrl' => Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/axVoteHelpful', array('idComment' => $_comment->id_comment))
                    ));
                    ?>
                </div>
            <?php endif; ?>

        </div>
    </div>
<?php endforeach; ?>
<script>
    $(function () {
        $(document).controls();

        // Edit existing reply
        $('.dashletcomment-details .actions .blogcomment-edit').click(function(){
            // IF you are not already in edit more (escaping the case when the user will click twice on the edit button)
            if(!$(this).hasClass('inEditMode')){
                var reply = $(this).closest('.dashletComment-header').find('.comment-preview').html();
                var html = '<input type="hidden" class="oldComment" value="' + reply + '" />';
                html += '<textarea class="editCommentContent">'+reply+'</textarea> ';
                html += '<?php echo CHtml::button(Yii::t('standard', '_CANCEL'), array('class'=>'btn-docebo red big closeEditMode')); ?>';
                html += '<?php echo CHtml::button(Yii::t('standard', '_SAVE'), array('class'=>'btn-docebo blue big updateComment')); ?>';
                $(this).closest('.dashletComment-header').find('.comment-preview').html(html);
                $(this).addClass('inEditMode');
            }
            return false;
        });
    });

</script>
