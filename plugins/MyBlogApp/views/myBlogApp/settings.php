<?php
/* @var $form CActiveForm */
/* @var $settings MyBlogAppSettingsForm */
?>

<a href="<?= ('it' == Yii::app()->getLanguage()) ? 'https://www.docebo.com/it/knowledge-base/elearning-come-attivare-e-gestire-lapp-blog/' : 'https://www.docebo.com/knowledge-base/elearning-how-to-activate-and-manage-the-blog-app/' ?>" target="_blank" class="app-link-read-manual"><?= Yii::t('apps', 'Read Manual'); ?></a>
<h2><?=Yii::t('myblog','My Blog'); ?> - <?=Yii::t('standard','Settings'); ?></h2>
<br>

<?php DoceboUI::printFlashMessages(); ?>

<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'myblogapp-settings-form',
	'htmlOptions'=>array('class' => 'form-horizontal docebo-form'),
));
?>

<div class="error-summary"><?php echo $form->errorSummary($settings); ?></div>

<div class="control-container odd">
    <div class="control-group">
        <?=$form->labelEx($settings, 'myblog_show_set_tone', array('class'=>'control-label'));?>
        <div class="controls">
            <?= $form->radioButtonList($settings, 'myblog_show_set_tone',
                array(
                    1 => Yii::t('myblog', 'Do not show'),
                    2 => Yii::t('myblog', 'Show in blog posts only'),
                    3 => Yii::t('myblog', 'Show in blog comments only'),
                    4 => Yii::t('myblog', 'Show both in blog posts and comments'),
                )); ?>
        </div>
    </div>
</div>
<div class="control-container">
    <div class="control-group">
        <?=$form->labelEx($settings, 'myblog_set_tone_icons', array('class'=>'control-label'));?>
        <div class="controls tone-icons-list">

            <div>
                <div class="rating-container">
                    <?php
                    $this->widget('common.widgets.DoceboStarRating',array(
                        'name'=>'smiley_rating',
                        'type' => 'smiley'
                    ));
                    ?>
                </div>
                <?= $form->radioButton($settings, 'myblog_set_tone_icons', array('value'=>'smiley', 'uncheckValue'=>null)) ?>
            </div>

            <div>
                <div class="rating-container">
                    <?php
                    $this->widget('common.widgets.DoceboStarRating',array(
                        'name'=>'star_rating',
                        'type' => 'star'
                    ));
                    ?>
                </div>
                <?= $form->radioButton($settings, 'myblog_set_tone_icons', array('value'=>'star', 'uncheckValue'=>null)) ?>
            </div>

            <div>
                <div class="rating-container">
                    <?php
                    $this->widget('common.widgets.DoceboStarRating',array(
                        'name'=>'circle_rating',
                        'type' => 'circle'
                    ));
                    ?>
                </div>
                <?= $form->radioButton($settings, 'myblog_set_tone_icons', array('value'=>'circle', 'uncheckValue'=>null)) ?>
            </div>

            <div>
                <div class="rating-container">
                    <?php
                    $this->widget('common.widgets.DoceboStarRating',array(
                        'name'=>'heart_rating',
                        'type' => 'heart'
                    ));
                    ?>
                </div>
                <?= $form->radioButton($settings, 'myblog_set_tone_icons', array('value'=>'heart', 'uncheckValue'=>null)) ?>
            </div>
        </div>
    </div>
</div>
<div class="control-container odd">
    <div class="control-group">
        <?=$form->labelEx($settings, 'myblog_show_helpful', array('class'=>'control-label'));?>
        <div class="controls">
            <?= $form->radioButtonList($settings, 'myblog_show_helpful',
                array(
                    1 => Yii::t('myblog', 'Do not show'),
                    2 => Yii::t('myblog', 'Show in blog posts only'),
                    3 => Yii::t('myblog', 'Show in blog comments only'),
                    4 => Yii::t('myblog', 'Show both in blog posts and comments'),
                )); ?>
        </div>
    </div>
</div>


<div class="form-actions">

	<?=CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
		'class'=>'btn-docebo green big'
	)); ?>
	&nbsp;
	<a href="<?=Docebo::createLmsUrl("app/index");?>" class="btn-docebo black big"><?=Yii::t('standard', '_CANCEL');?></a>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        var $form = $('#myblogapp-settings-form');

        $form.find('.control-container.odd :radio:visible')
            .add('.tone-icons-list > div > :radio')
            .styler();
    });
</script>