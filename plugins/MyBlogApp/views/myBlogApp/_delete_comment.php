<?php
/* @var $this MyBlogAppController */
/* @var $form CActiveForm */
?>
<h1><?= Yii::t('standard', '_CONFIRM') ?></h1>


<?php $form = $this->beginWidget('CActiveForm', array(
    'method' => 'POST',
    'htmlOptions' => array(
        'class' => 'ajax'
    ),
));
?>

<?= Yii::t('standard', "_AREYOUSURE") ?>


<div class="form-actions">
    <input class="btn-docebo green big" type="submit" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" />
    <input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">

    $(document).delegate(".myblog-delete-confirm-dialog", "dialog2.content-update", function() {
        var e = $(this);

        var autoclose = e.find("a.auto-close");

        if (autoclose.length > 0) {
            e.find('.modal-body').dialog2("close");

            var idComment = parseInt(autoclose.data('remove-comment'));
            if (idComment > 0) {
                $('#blogcomment-'+idComment).remove();
            }

            var href = autoclose.attr('href');
            if (href) {
                window.location.href = href;
            }
        }
    });

</script>

