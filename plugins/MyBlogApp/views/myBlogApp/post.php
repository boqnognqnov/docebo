<?php
/* @var $this MyBlogAppController */
/* @var $author CoreUser */
/* @var $post MyblogPost */

if($from_course)
{
	$_backUrl = Docebo::createAbsoluteUrl('player', array('course_id' => $from_course));
	$course_model = LearningCourse::model()->findByPk($from_course);
	$this->breadcrumbs[$course_model->name] = $_backUrl;
	$this->breadcrumbs[] = Yii::t('myblog', 'Course specific post');
}
elseif ($author) {
	$_backUrl = $this->createUrl('blog', array('idUser' => $author->idst));
	$this->breadcrumbs[] = $author->getFullName();
	$this->breadcrumbs[] = Yii::t('myblog', 'My Blog');
} else {
	$_backUrl = $this->createUrl('index');
	$this->breadcrumbs[] = Yii::t('myblog', 'My Blog');
}
?>


<div class="row-fluid">
    <div class="span8">
        <h3 class="title-bold back-button">
            <?php echo CHtml::link(Yii::t('standard', '_BACK'), $_backUrl); ?>
            <span><?= CHtml::encode($post->title) ?></span>
        </h3>
        <br/>


        <?php
        $this->widget('BlogPost', array(
            'post' => $post,
            'isAdminMode' => (!$author)
        ));
        ?>
    </div>
    <div class="span4">
        <?php $this->widget('BlogSidebar', array(
            'idUser' => ($author ? $author->idst : Yii::app()->user->id),
            'searchUrl' => ($author
                    ? Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/blog', array('idUser'=>$author->idst))
                    : Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/index')
                )
        )); ?>
    </div>
</div>