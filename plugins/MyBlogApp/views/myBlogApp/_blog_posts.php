<?php
/* @var $this MyBlogAppController */
/* @var $page int */
/* @var $idUser int */
/* @var $idCourse int */

$this->widget('BlogPostList', array(
    'idUser' => $idUser,
    'idCourse' => $idCourse,
    'page' => $page
));
