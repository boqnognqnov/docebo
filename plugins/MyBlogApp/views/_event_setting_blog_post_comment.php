<div id="comment-type-selector">
	<div>
		<?= CHtml::radioButton('params[comment_type]', (!isset($params['comment_type']) || (isset($params['comment_type']) && $params['comment_type'] == 'any_blog') ? true : false), array('id' => 'comment_type_0', 'class' => 'radio-comment-type-selection', 'value' => 'any_blog')); ?>
		<label for="comment_type_0" class="label-course-selection"><?= Yii::t('myblog', 'Commented on any blog'); ?></label>
        <?= CHtml::radioButton('params[comment_type]', (isset($params['comment_type']) && ($params['comment_type'] == 'own_blog') ? true : false), array('id' => 'comment_type_1', 'class' => 'radio-comment-type-selection', 'value' => 'own_blog')); ?>
        <label for="comment_type_1" class="label-course-selection"><?= Yii::t('myblog', "Commented on user's own blog"); ?></label>
        <?= CHtml::radioButton('params[comment_type]', (isset($params['comment_type']) && ($params['comment_type'] == 'others_blog') ? true : false), array('id' => 'comment_type_2', 'class' => 'radio-comment-type-selection', 'value' => 'others_blog')); ?>
        <label for="comment_type_2" class="label-course-selection"><?= Yii::t('myblog', "Commented on other user's blogs"); ?></label>
	</div>
	<div class="clearfix"></div>
</div>
<script type="text/javascript">
	$('.radio-comment-type-selection').styler();
</script>