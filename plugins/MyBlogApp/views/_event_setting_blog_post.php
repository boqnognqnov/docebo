<div id="post-type-selector">
	<div>
		<?= CHtml::radioButton('params[post_type]', (!isset($params['post_type']) || (isset($params['post_type']) && $params['post_type'] == 'generic') ? true : false), array('id' => 'post_type_0', 'class' => 'radio-course-selection', 'value' => 'generic')); ?>
		<label for="post_type_0" class="label-course-selection"><?= Yii::t('myblog', 'Generic post'); ?></label>
		<?= CHtml::radioButton('params[post_type]', (isset($params['post_type']) && $params['post_type'] == 'course' ? true : false), array('id' => 'post_type_1', 'class' => 'radio-course-selection', 'value' => 'course')); ?>
		<label for="post_type_1" class="label-course-selection"><?= Yii::t('myblog', 'Course specific post'); ?></label>
	</div>
	<div class="clearfix"></div>
	<div id="course_selector"<?= (isset($params['post_type']) && $params['post_type'] == 'course' ? ' class="display-block"' : ''); ?>>
		<select id="courses" name="params[courses]">
			<?php foreach($courses as $info) : ?>
				<option value="<?= $info['idCourse']; ?>" class="selected"><?= ($info['code'] != '' ? $info['code'].' - ' : '').$info['name']; ?></option>
			<?php endforeach; ?>
		</select>
        <div class="errorMessage"></div>
	</div>
    <div class="clearfix"></div>
    <div id="post-counter" style="margin-top: 15px;">
        <label for="post-number"><?= Yii::t('myblog', 'Assign badge only when the amount of blog posts reaches') ?>:</label>
        <input style="width: 60px;" type="text" name="params[post_number]" id="post-number" value="<?= isset($params['post_number']) ? $params['post_number'] : '' ?>"/>
        <div class="errorMessage"></div>
    </div>
</div>
<script type="text/javascript">
	$('.radio-course-selection').styler();

	$('.radio-course-selection').change(function(e)
	{
        $('#course_selector').find('.errorMessage').html('');
        $('#post-counter').find('.errorMessage').html('');
		if($('#post_type_0:checked').length > 0)
			$('#course_selector').removeClass('display-block');
		else
			$('#course_selector').addClass('display-block');
	});

	$(document).ready(function(){
		$("#courses").fcbkcomplete({
			json_url: "./index.php?r=GamificationApp/GamificationApp/getCourseList",
			addontab: true,
			input_min_size: 0,
            width: '98.5%',
			cache: true,
			complete_text: '',
			maxitems: '<?php echo $totalCourses?>',
			filter_selected: true
		});

        function isNormalInteger(str) {
            var n = ~~Number(str);
            return String(n) === str && n > 0;
        }

        $('#badge-create-form').submit(function(e){
			if($('#association_mode_0:checked').length == 0) {
				if($('#post-type-selector').length == 0)
					return true;

				if(($('input:radio[name="params[post_type]"]:checked').val() == 'course') && !$("#courses").val()) {
					$('#course_selector').find('.errorMessage').html(<?=CJSON::encode(Yii::t('gamification', 'Please select at least one course'))?>);
					e.preventDefault();
                    $('input#save').removeClass('disabled');
				} else {
                    $('#course_selector').find('.errorMessage').html('');
                }

                if(!$("#post-number").val() || !isNormalInteger($("#post-number").val())) {
                    $('#post-counter').find('.errorMessage').html(<?=CJSON::encode(Yii::t('gamification', 'Please enter a number higher than') . ' ' . 0)?>);
                    e.preventDefault();
                    $('input#save').removeClass('disabled');
                } else {
                    $('#post-counter').find('.errorMessage').html('');
                }
			}
        });
	});
</script>