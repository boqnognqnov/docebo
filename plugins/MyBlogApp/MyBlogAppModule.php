<?php

class MyBlogAppModule extends CWebModule {


	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'MyBlogApp.models.*',
			'MyBlogApp.components.*',
			'MyBlogApp.widgets.*',
		));

		Yii::app()->event->on('GamificationEventsFetching', array($this, 'addGamificationEvent'));
		Yii::app()->event->on('BeforeRenderUserAvatar', array($this, 'renderUserBlogLink'));
		Yii::app()->event->on('CollectMyDashboardDashletWidgets', array($this, 'provideBlogDashlet'));
	}

	/**
	 * Installs My Blog gamification events
	 * @param $event
	 */
	public function addGamificationEvent($event)
	{
		$event->params['event_list']['StudentPostedOnBlog'] = array(
			'event_name' => Yii::t('myblog', 'Student posted on his own blog'),
			'group'=>'Blog badges',
			'event_description' => Yii::t('myblog', 'This event is triggered when a user creates a new post in his personal blog'),
			'handler' => 'MyBlogApp.components.StudentPostedOnBlogHandler'
		);

		$event->params['event_list']['StudentCommentedOnBlog'] = array(
			'event_name' => Yii::t('myblog', 'Student commented on a blog post'),
			'group'=>'Blog badges',
			'event_description' => Yii::t('myblog', 'This event is triggered when a user comments on a blog post'),
			'handler' => 'MyBlogApp.components.StudentCommentedOnBlogPostHandler'
		);
	}

	/**
	 * Installs the avatar link for the user's blog
	 * @param $event DEvent
	 */
	public function renderUserBlogLink($event)
	{
		$src = $event->params['src'];
		$className = $event->params['class'];
		$alt = $event->params['alt'];
		$user = $event->params['user'];
		$style = $event->params['style'];
		/* @var $user CoreUser */

		if ($className == 'pull-left')
			return;

		$event->preventDefault();
		$event->return_value['html'] = CHtml::link(CHtml::image($src, $alt, array('class' => $className, 'style' => $style)), Docebo::createAbsoluteUrl('lms:MyBlogApp/MyBlogApp/blog', array('idUser' => $user->idst)));
	}

	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu()
	{
		Yii::app()->mainmenu->addAppMenu('myBlog', array(
			'icon' => 'home-ico my-blog',
			'app_icon' => 'fa-pencil', // icon used in the new menu
			'link' => Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/settings'),
			'label' => Yii::t('myblog', 'Blog'),
			//'settings' => Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/settings'),
		), array());
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}

	public function userMenu() {
		return array(
			/*array(
				'href' => Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/index', array()),
				'icon' => 'my-blog',
				'label' => Yii::t('myblog', 'My Blog'),
			)*/
		);
	}

	/**
	 * Respond to the dashboard dashlet collector and include
	 * the MyBlogApp specific "Blog" dashlet
	 * @param DEvent $event
	 */
	public function provideBlogDashlet(DEvent $event){
		if(!isset($event->params['descriptors'])){
			return;
		}

		$myblogappDescriptor = DashletBlog::descriptor();

		$collectedSoFarHandlers = array();
		foreach($event->params['descriptors'] as $descriptorObj){
			if ($descriptorObj instanceof DashletDescriptor) {
				$collectedSoFarHandlers[] = $descriptorObj->handler;
			}
		}

		if(!in_array($myblogappDescriptor->handler, $collectedSoFarHandlers)){
			// Add the MyBlogApp dashlet to the event params
			// so the Blog dashlet is available everywhere
			$event->params['descriptors'][] = $myblogappDescriptor;
		}

	}

}
