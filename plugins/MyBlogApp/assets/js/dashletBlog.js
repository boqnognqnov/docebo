$(function(){
    // Show hide Post Replies actions
    $('.dashletcomment-details .actions').live('mouseenter', function () {
        $(this).find(".blogcomment-edit").show();
        $(this).find(".blogcomment-delete").show();
    });
    $('.dashletcomment-details .actions').live('mouseleave', function () {
        $(this).find(".blogcomment-edit").hide();
        $(this).find(".blogcomment-delete").hide();
    });

    // Show the comments if they are not already shown
    $(document).on('click', '.show-comments', function(event){
        event.preventDefault();
        //debugger;
        // Get Click post id
        var post = $(this).attr('post');
        var block = $(this).closest('.dashlet');
        if(!($(this).hasClass('opened') || $(this).hasClass('closed'))){
            // Load more post comments
            loadFirstPosts(block, post);
        }

        if($(this).hasClass('opened')){
            block.find('.postid-' + post).hide();
            $(this).removeClass('opened').addClass('closed');
        }else{
            // Show post's comments
            block.find('.postid-' + post).show();
            $(this).removeClass('closed').addClass('opened');
        }

        return false;
    });

    // Add new Reply
    $(document).on('click', '.sendReply', function(event){
        event.preventDefault();
        var post = $(this).attr('id');
        var block = $(this).closest('.dashlet');
        var reply = block.find('#post_' + post).val();
        if(reply.replace(/\s/g, '')){
            var url = $(this).attr('url');
            AddEditReply(block, url, post, reply);
        }
        return false;
    });

    // Update certain reply
    $(document).on('click', '.updateComment', function(event){
        event.preventDefault();
        var reply = $(this).closest('.dashletComment-header').find('textarea').val();
        if(reply.replace(/\s/g, '')){
            var block = $(this).closest('.dashlet');
            var url = $(this).closest('.dashletComment').data('edit-url');
            var post = $(this).closest('.blog-comments-dashlet').attr('class').replace(/\D/g,'');
            AddEditReply(block, url, post, reply);
        }
        return false;
    });

    // Close Edit mode
    $(document).on('click', '.closeEditMode', function(event){
        event.preventDefault();
        // Remove inEditMode
        $(this).closest('.dashletComment-header').find('.blogcomment-edit').removeClass('inEditMode');
        // Clear the EditMode content
        var reply = $(this).closest('.dashletComment-header').find('.oldComment').val();
        $(this).closest('.dashletComment-header').find('.comment-preview').html(reply);
        return false;
    });

    // Load more comments if any
    $(document).on('click', '.load-more-comments', function(event){
        event.preventDefault();
        var post = $(this).closest('.blog-comments-dashlet').attr('class').replace(/\D/g,'');
        var block = $(this).closest('.dashlet');
        loadFirstPosts(block, post);
        return false;
    });

    // Delete reply
    $(document).delegate('.myblog-ax-delete-confirm-dialog', "dialog2.content-update", function() {
        var autoclose = $(this).find("a.auto-close");
        if (autoclose.length > 0) {
            var post = parseInt(autoclose.data('post'));
            var block = $('#' + autoclose.data('block'));
            if (block && post) {
                block.find('.postid-' + post + ' .load-more-comments').data('page', 0);
                block.find('.postid-' + post + ' .blog-commnets-load-more').show();
                block.find('.postid-' + post + ' .blog-comments-older-comments').html('');
                loadFirstPosts(block, post);
            }else{
                location.reload();
            }
            $(this).find('.modal-body').dialog2("close");
        }
    });
});

function loadFirstPosts(block, post){
    // Set loader screen
    var loader = '<div class="ajaxloader" style="display: block;"></div>';
    block.find('.postid-' + post + ' .blog-comments-older-comments').append(loader).addClass('loaderFix').closest('.postid-' + post).show();
    // Get Url from which we will get the new replies
    var url = block.find('a[post="'+post+'"]').attr('href');
    // Get current Page
    var currentPage = block.find('.postid-' + post + ' .load-more-comments').data('page');
    $.ajax({
        type: 'POST',
        url: url,
        data: {page: currentPage, block: block.attr('id')},
        success: function(data){
            var response = JSON.parse(data);
            block.find('.postid-' + post + ' .ajaxloader').remove();
            block.find('.postid-' + post + ' .blog-comments-older-comments').removeClass('loaderFix');
            if(response.hasComments){
                // Set Current Page number
                block.find('.postid-' + post + ' .load-more-comments').data('page', currentPage + 1);
                block.find('.postid-' + post + ' .blog-comments-older-comments').append(response.html);
            }else{
                block.find('.postid-' + post + ' .blog-commnets-load-more').hide();
            }
        }
    });
}

// Add or Edit Existing post
function AddEditReply(block, url, post, reply){

    block.find('.postid-' + post + ' .load-more-comments').data('page', 0);
    block.find('.postid-' + post + ' .blog-commnets-load-more').show();

    // Set loading screen while waiting the ajax to get back
    var loader = '<div class="ajaxloader" style="display: block;"></div>';
    block.find('.postid-' + post + ' .blog-comments-older-comments').html(loader).addClass('loaderFix');
    block.find('#post_' + post).val('');

    $.ajax({
        type: 'POST',
        url: url,
        data: {reply: reply},
        success: function(data){
            var response = JSON.parse(data);
            if(response.saved){
                block.find('.postid-' + post + ' .ajaxloader').remove();
                block.find('.postid-' + post + ' .blog-comments-older-comments').removeClass('loaderFix');
                loadFirstPosts(block, post);
            }else{
                location.reload();
            }
        }
    });
}