$(function(){
    // Show hide Post Replies actions
    $('.dashletcomment-details .actions').live('mouseenter', function () {
        $(this).find(".blogcomment-edit").show();
        $(this).find(".blogcomment-delete").show();
    });
    $('.dashletcomment-details .actions').live('mouseleave', function () {
        $(this).find(".blogcomment-edit").hide();
        $(this).find(".blogcomment-delete").hide();
    });

    // Show comments if they are not already shown
    $(document).on('click', '.show-comments', function(){
        // Get Click post id
        var post = $(this).attr('post');
        if(!($(this).hasClass('opened') || $(this).hasClass('closed'))){
            // Load more post comments
            loadFirstPosts(post);
        }

        if($(this).hasClass('opened')){
            $(this).closest('.blogpost').find('.postid-' + post).hide()
            $(this).removeClass('opened');
            $(this).addClass('closed');
        }else{
            $(this).closest('.blogpost').find('.postid-' + post).show()
            // Show post's comments
            $(this).removeClass('closed');
            $(this).addClass('opened');
        }

        return false;
    });

    // Send a new reply to the server
    $(document).on('click', '.sendReply', function(event){
        event.preventDefault();
        var post = $(this).attr('id');
        var reply = $('#post_' + post).val();
        if(reply.replace(/\s/g, '')){
            var url = $(this).attr('url');
            AddEditReply(url, post, reply);
        }
        return false;
    });

    // Close reply edit mode
    $(document).on('click', ".closeEditMode", function(event) {
        event.preventDefault();
        // Remove inEditMode
        $(this).closest('.dashletComment-header').find('.blogcomment-edit').removeClass('inEditMode');
        // Clear the EditMode content
        var reply = $(this).closest('.dashletComment-header').find('.oldComment').val();
        $(this).closest('.dashletComment-header').find('.comment-preview').html(reply);
        return false;
    });

    // Update reply content
    $(document).on('click', ".updateComment", function(event) {
        event.preventDefault();
        var reply = $(this).closest('.dashletComment-header').find('textarea').val();
        if(reply.replace(/\s/g, '')){
            var url = $(this).closest('.dashletComment').data('edit-url');
            var post = $(this).closest('.blog-comments-dashlet').attr('class').replace(/\D/g,'');
            AddEditReply(url, post, reply);
        }
        return false;
    });

    // Load more comments
    $(document).on('click', ".load-more-comments", function(event) {
        event.preventDefault();
        var post = $(this).closest('.blog-comments-dashlet').attr('class').replace(/\D/g,'');
        loadFirstPosts(post);
        return false;
    });

    // Delete existing comment
    $(document).delegate('.myblog-ax-delete-confirm-dialog', "dialog2.content-update", function() {
        var e = $(this);
        var autoclose = e.find("a.auto-close");
        if (autoclose.length > 0) {
            e.find('.modal-body').dialog2("close");
            var post = parseInt(autoclose.data('post'));
            if (post) {
                $('.postid-' + post).find('.load-more-comments').data('page', 0);
                $('.postid-' + post).find('.blog-commnets-load-more').show();
                $('.postid-' + post).find('.blog-comments-older-comments').html('');
                loadFirstPosts(post)
            }else{
                location.reload();
            }
        }
    });
});

function loadFirstPosts(post){
    // Set loader screen
    var loader = '<div class="ajaxloader" style="display: block;"></div>';
    $('.postid-' + post + ' .blog-comments-older-comments').append(loader).addClass('loaderFix');
    // Get Url from which we will get the new replies
    var url = $('a[post="'+post+'"]').attr('href');
    // Get current Page
    var currentPage = $('.postid-' + post).find('.load-more-comments').data('page');
    $.ajax({
        type: 'POST',
        url: url,
        data: {page: currentPage},
        success: function(data){
            var response = JSON.parse(data);
            $('.postid-' + post + ' .blog-comments-older-comments').removeClass('loaderFix');
            $('.postid-' + post).find('.ajaxloader').remove();
            if(response.hasComments){
                // Set Current Page number
                $('.postid-' + post).find('.load-more-comments').data('page', currentPage + 1);
                $('.postid-' + post + ' .blog-comments-older-comments').append(response.html);
            }else{
                $('.postid-' + post).find('.blog-commnets-load-more').hide();
            }
        },
    });
}

function AddEditReply(url, post, reply){
    $('.postid-' + post).find('.load-more-comments').data('page', 0);
    // Set loading screen while waiting the ajax to get back
    var loader = '<div class="ajaxloader" style="display: block;"></div>';
    $('.postid-' + post + ' .blog-comments-older-comments').html(loader).addClass('loaderFix');
    $('#post_' + post).val('');

    $.ajax({
        type: 'POST',
        url: url,
        data: {reply: reply},
        success: function(data){
            var response = JSON.parse(data);
            if(response.saved){
                $('.postid-' + post + ' .blog-comments-older-comments').removeClass('loaderFix');
                $('.postid-' + post).find('.ajaxloader').remove();
                loadFirstPosts(post);
                $('.postid-' + post).find('.blog-commnets-load-more').show();
            }else{
                location.reload();
            }
        },
    });
}