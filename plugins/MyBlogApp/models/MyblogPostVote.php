<?php

/**
 * This is the model class for table "myblog_post_vote".
 *
 * The followings are the available columns in table 'myblog_post_vote':
 * @property integer $id_post
 * @property integer $id_user
 * @property integer $value
 * @property string $type
 *
 * The followings are the available model relations:
 * @property MyblogPost $idPost
 * @property CoreUser $idUser
 */
class MyblogPostVote extends CActiveRecord
{

	//voting types
	const TYPE_TONE = 'tone';
	const TYPE_HELPFUL = 'helpful';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'myblog_post_vote';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_post, id_user', 'required'),
			array('id_post, id_user, value', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>7),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_post, id_user, value, type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPost' => array(self::BELONGS_TO, 'MyblogPost', 'id_post'),
			'idUser' => array(self::BELONGS_TO, 'CoreUser', 'id_user'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_post' => 'Id Post',
			'id_user' => 'Id User',
			'value' => 'Value',
			'type' => 'Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_post',$this->id_post);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('value',$this->value);
		$criteria->compare('type',$this->type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MyblogPostVote the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * (non-PHPdoc)
     * @see CActiveRecord::afterSave()
     */
    public function afterSave() {
        parent::afterSave();
        if ($this->isNewRecord) {
            Yii::app()->event->raise(($this->type == 'tone') ? 'NewToneFeedback' : 'NewHelpfulFeedback', new DEvent($this, array(
                'evaluator_user' => $this->idUser,
                'evaluated_user' => $this->idPost->user,
                'value' => $this->value,
                'target_type' => 'post',
                'target_element' => $this->idPost
            )));
        }
    }
}
