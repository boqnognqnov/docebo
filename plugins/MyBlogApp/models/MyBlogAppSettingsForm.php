<?php

/**
 * MyBlogAppSettingsForm class.
 *
 * @property string $myblog_show_set_tone
 * @property string $myblog_set_tone_icons
 * @property string $myblog_show_helpful
 */
class MyBlogAppSettingsForm extends CFormModel {
	public $myblog_show_set_tone;
	public $myblog_set_tone_icons;
	public $myblog_show_helpful;

	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
            array('myblog_show_set_tone, myblog_set_tone_icons, myblog_show_helpful', 'safe')
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array(
			'myblog_show_set_tone' => Yii::t('myblog', 'Show "Set tone"'),
			'myblog_set_tone_icons' => Yii::t('myblog', '"Set tone" icons'),
			'myblog_show_helpful' => Yii::t('myblog', 'Show "helpful"'),
		);
	}
}
