<?php

/**
 * This is the model class for table "myblog_post".
 *
 * The followings are the available columns in table 'myblog_post':
 * @property integer $id_post
 * @property string $title
 * @property string $content
 * @property string $posted_on
 * @property integer $id_user
 * @property integer $id_course
 * @property integer $tone_sum
 * @property integer $tone_count
 *
 * The followings are the available model relations:
 * @property MyblogComment[] $comments
 * @property CoreUser $user
 * @property LearningCourse $course
 * @property MyblogPostVote[] $postVotes
 */
class MyblogPost extends CActiveRecord
{
    public $search;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'myblog_post';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, content, posted_on, id_user', 'required'),
			array('id_user, id_course, tone_sum, tone_count', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
            array('title, content', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_post, title, content, posted_on, id_user, id_course, tone_sum, tone_count', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'comments' => array(self::HAS_MANY, 'MyblogComment', 'id_post'),
			'user' => array(self::BELONGS_TO, 'CoreUser', 'id_user'),
			'course' => array(self::BELONGS_TO, 'LearningCourse', 'id_course'),
			'postVotes' => array(self::HAS_MANY, 'MyblogPostVote', 'id_post'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_post' => 'Id Post',
			'title' => Yii::t('standard', '_TITLE'),
			'content' => Yii::t('myblog', 'Post Content'),
			'posted_on' => 'Posted On',
			'id_user' => 'Id User',
			'id_course' => 'Id Course',
			'tone_sum' => 'Tone Sum',
			'tone_count' => 'Tone Count',
		);
	}

    public function behaviors()
    {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
                'timestampAttributes' => array('posted_on medium')
            )
        );
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_post',$this->id_post);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('posted_on',$this->posted_on,true);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('id_course',$this->id_course);
		$criteria->compare('tone_sum',$this->tone_sum);
		$criteria->compare('tone_count',$this->tone_count);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MyblogPost the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            // delete comments and votes
            $comments = $this->comments;
            foreach ($comments as $comment) {
                $comment->delete();
            }
            MyblogPostVote::model()->deleteAllByAttributes(array(
                'id_post' => $this->id_post
            ));
            return true;
        }
        return false;
    }

    public function afterSave() {
        if($this->getIsNewRecord()) {
            // Raised when a new blog post is created
            Yii::app()->event->raise('StudentPostedOnBlog', new DEvent($this, array(
                'user' => Yii::app()->user,
                'post' => $this,
                'course' => $this->course,
            )));
        }

        parent::afterSave();
    }

    /**
     * @param array $options
     * @return CActiveDataProvider
     */
    public function dataProvider($options = array()) {
        $config = array();
        $criteria = new CDbCriteria();
        $month = $year = 0;

        if (intval($options['page']) > 0) {
            // get search keywords from session
            $this->search = Yii::app()->session[CHtml::activeName($this, 'search')];
            $month = Yii::app()->session[CHtml::activeName($this, 'month')];
            $year = Yii::app()->session[CHtml::activeName($this, 'year')];
        } else {
            // clear search keywords
            Yii::app()->session[CHtml::activeName($this, 'search')] = '';
            Yii::app()->session[CHtml::activeName($this, 'month')] = 0;
            Yii::app()->session[CHtml::activeName($this, 'year')] = 0;
        }

        if (!empty($this->search)) {
            // save in session
            Yii::app()->session[CHtml::activeName($this, 'search')] = $this->search;
            $criteria->addCondition('t.title LIKE :search OR t.content LIKE :search');
            $criteria->params[':search'] = "%$this->search%";
        }

        $month = intval( Yii::app()->request->getParam('month', $month) );
        $year = intval( Yii::app()->request->getParam('year', $year) );

        // check 'month' and 'year' params
        if ($month > 0 && $year > 0) {
            $criteria->addCondition('YEAR(posted_on)=:year');
            $criteria->addCondition('MONTH(posted_on)=:month');
            $criteria->params[':year'] = $year;
            $criteria->params[':month'] = $month;

            Yii::app()->session[CHtml::activeName($this, 'month')] = $month;
            Yii::app()->session[CHtml::activeName($this, 'year')] = $year;
        }

        $criteria->compare('id_user',$this->id_user);
        $criteria->compare('id_course',$this->id_course);

        $criteria->with = array('user', 'course');

        $sortAttributes = array();
        foreach ($this->attributeNames() as $attributeName) {
            $sortAttributes[$attributeName] = 't.' . $attributeName;
        }

        $config['criteria'] = $criteria;
        $config['sort']['attributes'] = $sortAttributes;
        $config['sort']['defaultOrder'] = 'posted_on DESC';
		$config['pagination'] = array(
			'pageSize' => isset($options['pageSize'])
					? $options['pageSize']
					: Settings::get('elements_per_page', 10),
			'currentPage' => intval($options['page']),
			'validateCurrentPage' => false
		);

        return new CActiveDataProvider(get_class($this), $config);
    }

    /**
     * @return string
     */
    public function renderCommentActionLabel() {
        $numComments = $this->getNumComments();
        if ($numComments) {
            return Yii::t('standard', '_COMMENTS') . ' ('.$numComments.')';
        }
        return Yii::t('forum', 'Write here your comment');
    }

    /**
     * @return int
     */
    public function getNumComments() {
        return intval(count($this->comments));
    }

    /**
     * @return array
     */
    public function getHelpful() {
		$myblogPostVote = MyblogPostVote::model()->tableName();
		$idPost = $this->id_post;

		$rowLikes = Yii::app()->db->createCommand("
SELECT COUNT(*) AS count
FROM $myblogPostVote
WHERE type='helpful' and id_post='$idPost' and value=1")->queryRow();

		$rowDislikes = Yii::app()->db->createCommand("
SELECT COUNT(*) AS count
FROM $myblogPostVote
WHERE type='helpful' and id_post='$idPost' and value=-1")->queryRow();

		$helpful = array(
			'likes' => intval($rowLikes['count']),
			'dislikes' => intval($rowDislikes['count'])
		);

		return $helpful;
    }

    /**
     * @param $idUser
     * @return string|mixed
     */
    public function getUserHelpfulVote($idUser) {
        $model = new MyblogPostVote('search');
        $model->id_post = $this->id_post;
        $model->id_user = $idUser;
        $model->value = null;
        $model->type = 'helpful';
        $dataProvider = $model->search();

        if ($dataProvider->totalItemCount > 0) {
            $data = $dataProvider->getData();
            return ($data[0]->value == 1) ? 'like' : 'dislike';
        }

        return null;
    }

    /**
     * @return float
     */
    public function getRating() {
        $sum = intval($this->tone_sum);
        $count = intval($this->tone_count);
        $avg = (!$count) ? 0 : doubleval($sum / $count);
        return round($avg);
    }

    /**
     * @param $idUser
     * @param $value
     */
    public function saveRating($idUser, $value) {
        $model = new MyblogPostVote();

		$myBlogPost = $this->tableName();
		$myblogPostVote = $model->tableName();
		$idPost = $this->id_post;

		// check if the user has previously voted
		$row = Yii::app()->db->createCommand("
SELECT *
FROM $myblogPostVote
WHERE id_post=$idPost AND id_user=$idUser AND type='tone'")->queryRow();

		if ($row) {
			// delete the previous rating
			$rowValue = intval($row['value']);
			Yii::app()->db->createCommand("
DELETE FROM $myblogPostVote
WHERE id_post=$idPost AND id_user=$idUser AND type='tone'")->query();
			// update the tone counters for the message
			$newToneSum = $this->tone_sum - $rowValue;
			$this->tone_sum = $newToneSum;
			$newToneCount = $this->tone_count - 1;
			$this->tone_count = $newToneCount;
			Yii::app()->db->createCommand("
UPDATE $myBlogPost
SET tone_sum=$newToneSum, tone_count=$newToneCount
WHERE id_post=$idPost")->query();
		}

		// save the new rating
		/*Yii::app()->db->createCommand("
INSERT INTO $myblogPostVote
SET id_post=$idPost, id_user=$idUser, type='tone', value=$value")->query();*/
        /* Using the model to save because we need from its afterSave() method for another events */
        $model->id_post = $idPost;
        $model->id_user = $idUser;
        $model->type = 'tone';
        $model->value = $value;
        $model->save();

		// update the forum message
		$toneSum = intval($this->tone_sum) + $value;
		$toneCount = intval($this->tone_count) + 1;
		Yii::app()->db->createCommand("
UPDATE $myBlogPost
SET tone_sum=$toneSum, tone_count=$toneCount
WHERE id_post=$idPost")->query();
    }

    /**
     * Returns an array containing the number of blog posts, month and year.
     * @param $idUser
     * @return mixed
     */
    public static function getBlogArchive($idUser) {
        $rows = Yii::app()->db->createCommand()
            ->select('
                YEAR(posted_on) AS year,
                MONTH(posted_on) AS month,
                COUNT(id_post) AS total')
            ->from(self::model()->tableName())
            ->where('id_user=:id_user', array(':id_user'=>$idUser))
            ->order('year DESC, month DESC')
            ->group('year,month')
            ->queryAll();

        return $rows;
    }

    /**
     * @return bool
     */
    public function canUserEditDelete() {
        return (Yii::app()->user->getIsGodadmin() || $this->id_user == Yii::app()->user->id);
    }


	/**
	 * Generic method to calculate total blog posts of a specified user
	 * @param $idUser the idst of the user
	 * @return int the number of user blog posts
	 */
	public function getCountOfTotalUserPosts($idUser) {
		$count = Yii::app()->db->createCommand()
			->select("COUNT(*)")
			->from(self::model()->tableName())
			->where("id_user = :id_user", array(':id_user' => $idUser))
			->queryScalar();
		return (int)$count;
	}

	/**
	 * Generic method to calculate blog posts related to the courses of a specified user
	 * @param $idUser the idst of the user
	 * @param array $courses
	 * @return int the number of user blog posts
	 */
	public function getCountOfUserPostsRelatedToTheCourses($idUser, $courses) {
		if(!is_array($courses))
			$courses = array();

		$count = Yii::app()->db->createCommand()
			->select("COUNT(*)")
			->from(self::model()->tableName())
			->where("id_user = :id_user AND id_course IN (".implode(',', $courses).")", array(':id_user' => $idUser))
			->queryScalar();
		return (int)$count;
	}


	/**
	 * Calculate the number of helpful votes (either positive and negative) for a specific user
	 * @param $idUser the idst of the user who has been voted
	 * @return array 'yes' the number of positive helful votes, 'not' the number of negative helpful votes
	 */
	public function getUserTotalHelpfulVotes($idUser) {

		//prepare output
		$output = array(
			'yes' => 0,
			'not' => 0
		);

		//create query
		$command = Yii::app()->db->createCommand()
			->select("COUNT(*) AS total")
			->from(self::model()->tableName()." bp")
			->join(MyblogPostVote::model()->tableName()." bpv", "bp.id_post = bpv.id_post AND bp.id_user = :id_user")
			->where("bpv.type = :type AND bpv.value = :value");
		/* @var $command CDbCommand */
		$command->bindValue(':id_user', $idUser);
		$command->bindValue(':type', MyblogPostVote::TYPE_HELPFUL);

		//calculate positive helpful votes
		$command->bindValue(':value', 1);
		$output['yes'] = $command->queryScalar();

		//calculate negative helpful votes
		$command->bindValue(':value', -1);
		$output['not'] = $command->queryScalar();

		return $output;
	}


	/**
	 * Calculate average tone vote for all posts of a  specified user
	 * @param $idUser the idst of the user who has been voted
	 * @return bool|float|int the average tone vote for posts of the specified user
	 */
	public function getUserAverageToneVotes($idUser) {
		$totals = Yii::app()->db->createCommand()
			->select("SUM(tone_sum) AS tot_tone_sum, SUM(tone_count) AS tot_tone_count")
			->from(self::model()->tableName())
			->where("id_user = :id_user", array(':id_user' => $idUser))
			->queryRow();
		if (!$totals) return false;
		if ($totals['tot_tone_count'] <= 0) return 0;
		return ($totals['tot_tone_sum'] / $totals['tot_tone_count']);
	}


	/**
	 * Count how many votes of value $vote the specified user has received
	 * @param $idUser the idst of the specified user
	 * @param $vote the target vote to be counted
	 * @return int
	 */
	public function countUserToneVotesByVote($idUser, $vote) {
		$count = Yii::app()->db->createCommand()
			->select("COUNT(*) AS total")
			->from(self::model()->tableName()." bp")
			->join(MyblogPostVote::model()->tableName()." bpv", "bp.id_post = bpv.id_post AND bp.id_user = :id_user", array(':id_user' => $idUser))
			->where("bpv.type = :type AND bpv.value = :value", array(':type' => MyblogPostVote::TYPE_TONE, ':value' => $vote))
			->queryScalar();
		return (int)$count;
	}
}
