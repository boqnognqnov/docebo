<?php

/**
 * This is the model class for table "myblog_comment".
 *
 * The followings are the available columns in table 'myblog_comment':
 * @property integer $id_comment
 * @property integer $id_post
 * @property integer $id_user
 * @property string $content
 * @property string $posted_on
 * @property integer $tone_sum
 * @property integer $tone_count
 *
 * The followings are the available model relations:
 * @property MyblogPost $post
 * @property CoreUser $user
 * @property MyblogCommentVote[] $commentVotes
 */
class MyblogComment extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'myblog_comment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_post, id_user, content, posted_on', 'required'),
			array('id_post, id_user, tone_sum, tone_count', 'numerical', 'integerOnly'=>true),
			array('content', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_comment, id_post, id_user, content, posted_on, tone_sum, tone_count', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'post' => array(self::BELONGS_TO, 'MyblogPost', 'id_post'),
			'user' => array(self::BELONGS_TO, 'CoreUser', 'id_user'),
			'commentVotes' => array(self::HAS_MANY, 'MyblogCommentVote', 'id_comment'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_comment' => 'Id Comment',
			'id_post' => 'Id Post',
			'id_user' => 'Id User',
			'content' => 'Content',
			'posted_on' => 'Posted On',
			'tone_sum' => 'Tone Sum',
			'tone_count' => 'Tone Count',
		);
	}

    public function behaviors()
    {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
                'timestampAttributes' => array('posted_on medium')
            )
        );
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_comment',$this->id_comment);
		$criteria->compare('id_post',$this->id_post);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('posted_on',$this->posted_on,true);
		$criteria->compare('tone_sum',$this->tone_sum);
		$criteria->compare('tone_count',$this->tone_count);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * @param array $options
     * @return CActiveDataProvider
     */
    public function dataProvider($options = array()) {
        $config = array();
        $criteria = new CDbCriteria();

        /*if (!empty($this->name)) {
            $criteria->addCondition('t.name LIKE :search');
            $criteria->params[':search'] = "%$this->name%";
        }*/

        $criteria->compare('id_post',$this->id_post);

        $criteria->with = array('user');

        $sortAttributes = array();
        foreach ($this->attributeNames() as $attributeName) {
            $sortAttributes[$attributeName] = 't.' . $attributeName;
        }

        $config['criteria'] = $criteria;
        $config['sort']['attributes'] = $sortAttributes;
        $config['sort']['defaultOrder'] = 'posted_on DESC';
        $config['pagination'] = array(
            'pageSize' => isset($options['pageSize'])
                    ? $options['pageSize']
                    : Settings::get('elements_per_page', 10),
            'currentPage' => intval($options['page']),
            'validateCurrentPage' => false
        );

        return new CActiveDataProvider(get_class($this), $config);
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MyblogComment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeSave(){

        $this->content = Yii::app()->htmlpurifier->purify($this->content);

        return parent::beforeSave();
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            // delete comment votes
            MyblogCommentVote::model()->deleteAllByAttributes(array(
                'id_comment' => $this->id_comment
            ));
            return true;
        }
        return false;
    }

    public function afterSave() {
        if($this->getIsNewRecord()) {
            // Raised when a new blog post comment is created
            Yii::app()->event->raise('StudentCommentedOnBlog', new DEvent($this, array(
                'user' => Yii::app()->user,
                'post' => $this->post,
                'comment' => $this
            )));

            if($this->post->id_course){
                Yii::app()->event->raise(CoreNotification::NTYPE_BLOG_COMMENT_IN_COURSE, new DEvent($this, array(
                    'user' => Yii::app()->user,
                    'post' => $this->post,
                    'course' => $this->post->course,
                    'comment' => $this
                )));
            }
        }

        parent::afterSave();
    }

    /**
     * @return array
     */
    public function getHelpful() {
        $model = new MyblogCommentVote('search');
        $model->id_comment = $this->id_comment;
        $model->type = 'helpful';
        $model->value = null;
        $dataProvider = $model->search();

        $helpful = array(
            'likes' => 0,
            'dislikes' => 0
        );

        foreach ($dataProvider->getData() as $row) {
            if ($row->value > 0)
                $helpful['likes']++;
            else
                $helpful['dislikes']++;
        }

        return $helpful;
    }

    /**
     * @param $idUser
     * @return string|mixed
     */
    public function getUserHelpfulVote($idUser) {
        $model = new MyblogCommentVote('search');
        $model->id_comment = $this->id_comment;
        $model->id_user = $idUser;
        $model->value = null;
        $model->type = 'helpful';
        $dataProvider = $model->search();

        if ($dataProvider->totalItemCount > 0) {
            $data = $dataProvider->getData();
            return ($data[0]->value == 1) ? 'like' : 'dislike';
        }

        return null;
    }

    /**
     * @return float
     */
    public function getRating() {

		$user_id = Yii::app()->user->id;
		$userVoteOld = MyblogCommentVote::model()->find('id_user = :user_id',array(':user_id'=>$user_id));

		// if user is not already voted AND is not the author of the comment, we give him right to vote by set rating=0
		if (!$userVoteOld && $this->id_user != $user_id) {
			return 0;
		}
		// calculate average rating
        $sum = intval($this->tone_sum);
        $count = intval($this->tone_count);
        $avg = (!$count) ? 0 : doubleval($sum / $count);
        return round($avg);
    }

    /**
     * @param $idUser
     * @param $value
     * @return bool
     */
    public function saveRating($idUser, $value) {
        $model = new MyblogCommentVote();

		$myBlogComment = $this->tableName();
		$myblogCommentVote = $model->tableName();
		$idComment = $this->id_comment;

		// check if the user has previously voted
		$row = Yii::app()->db->createCommand("
SELECT *
FROM $myblogCommentVote
WHERE id_comment=$idComment AND id_user=$idUser AND type='tone'")->queryRow();

		if ($row) {
			// delete the previous rating
			$rowValue = intval($row['value']);
			Yii::app()->db->createCommand("
DELETE FROM $myblogCommentVote
WHERE id_comment=$idComment AND id_user=$idUser AND type='tone'")->query();
			// update the tone counters for the message
			$newToneSum = $this->tone_sum - $rowValue;
			$this->tone_sum = $newToneSum;
			$newToneCount = $this->tone_count - 1;
			$this->tone_count = $newToneCount;
			Yii::app()->db->createCommand("
UPDATE $myBlogComment
SET tone_sum=$newToneSum, tone_count=$newToneCount
WHERE id_comment=$idComment")->query();
		}

		// save the new rating
		/*Yii::app()->db->createCommand("
INSERT INTO $myblogCommentVote
SET id_comment=$idComment, id_user=$idUser, type='tone', value=$value")->query();*/
        /* Using the model to save because we need from its afterSave() method for another events */
        $model->id_comment = $idComment;
        $model->id_user = $idUser;
        $model->type = 'tone';
        $model->value = $value;
        $model->save();

		// update the forum message
		$toneSum = intval($this->tone_sum) + $value;
		$toneCount = intval($this->tone_count) + 1;
		Yii::app()->db->createCommand("
UPDATE $myBlogComment
SET tone_sum=$toneSum, tone_count=$toneCount
WHERE id_comment=$idComment")->query();
    }

    /**
     * @return bool
     */
    public function canUserEditDelete() {
        return (Yii::app()->user->getIsGodadmin() || $this->id_user == Yii::app()->user->id);
    }
}
