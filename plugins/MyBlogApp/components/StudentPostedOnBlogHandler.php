<?php

/**
 * Class StudentPostedOnBlogHandler
 * Handles the student posted on blog course event
 */
class StudentPostedOnBlogHandler extends CComponent implements GamificationEventHandler
{
    /**
     * Renders the view for the settings page
     * @param $params
     */
    public function renderEventSettingsView($params)
    {
        if(isset($params['params']['courses']) && !empty($params['params']['courses']))
        {
            $criteria = new CDbCriteria();
            $criteria->select = 'idCourse, code, name';
            $criteria->addInCondition('idCourse', $params['params']['courses']);
            $params['courses'] = LearningCourse::model()->findAll($criteria);
        }
        else
            $params['courses'] = array();

		$totalCourses = Yii::app()->db->createCommand('SELECT COUNT(*) FROM '.LearningCourse::model()->tableName())->queryScalar();
		$params['totalCourses'] = $totalCourses;

        // Render the _event_setting_blog_post view
        return Yii::app()->controller->renderPartial('MyBlogApp.views._event_setting_blog_post', $params, true);
    }

    /**
     * Checks whether this badge is to be assigned and returns event logging info
     * @param $badge GamificationBadge
     * @param $event_config array
     * @param $event_params array
     */
    public function shouldAssignBadge($badge, $event_config, $event_params) {
        $result = array('action' => false);

        // Fetch event params
        $post = $event_params['post']; /* @var $post MyblogPost */
        $course = $post->course;
        $user = $event_params['user']; /* @var $user CoreUser */

		if($event_config['post_type'] == 'course') {
			$selected_courses = isset($event_config['courses']) ? $event_config['courses'] : array();
		}

        // Check how many posts have been created
        $postNumberThreashold = (int) $event_config['post_number'];
        if($postNumberThreashold > 0) {
			if($event_config['post_type'] == 'generic')
				$currentPostNumber = MyblogPost::model()->getCountOfTotalUserPosts($user->idst);
			else if($event_config['post_type'] == 'course') {
				$currentPostNumber = MyblogPost::model()->getCountOfUserPostsRelatedToTheCourses($user->idst, $selected_courses);
			}

            if(isset($currentPostNumber) && $currentPostNumber == $postNumberThreashold)
                $result['action'] = true;
        } elseif($event_config['post_type'] == 'generic' || ($event_config['post_type'] == 'course' && $course && in_array($course->idCourse, $selected_courses)))
				$result['action'] = true;

        if($result['action']) {
            // If badge is to be assigned, fill in logging info
            $result['id_user'] = $user->idst;
            $result['event_module'] = 'myblog';
            $result['event_key'] = 'Student has created post on his blog';
            $result['event_params'] = array(0 => array());

            if($course) {
                $result['event_key'] .= ' inside course "{course_name}"';
                $result['event_params'][0]['{course_name}'] = $course->name;
            }
        }

        return $result;
    }
}