<?php

/**
 * Class StudentCommentedOnBlogPostHandler
 * Handles the student commented on blog post event
 */
class StudentCommentedOnBlogPostHandler extends CComponent implements GamificationEventHandler
{
    /**
     * Renders the view for the settings page
     * @param $params
     */
    public function renderEventSettingsView($params)
    {
        // Render the _event_setting_blog_post view
        return Yii::app()->controller->renderPartial('MyBlogApp.views._event_setting_blog_post_comment', $params, true);
    }

    /**
     * Checks whether this badge is to be assigned and returns event logging info
     * @param $badge GamificationBadge
     * @param $event_config array
     * @param $event_params array
     */
    public function shouldAssignBadge($badge, $event_config, $event_params) {
        $result = array('action' => false);

        // Fetch event params
        $post = $event_params['post']; /* @var $post MyblogPost */
        $user = $event_params['user']; /* @var $user CoreUser */
        $comment = $event_params['comment']; /* @var $user MyblogComment */

        // Check type of blog
        $result['action'] = ($event_config['comment_type'] == 'any_blog') ||
            (($event_config['comment_type'] == 'own_blog') && ($post->user->idst == Yii::app()->user->idst)) ||
            (($event_config['comment_type'] == 'others_blog') && ($post->user->idst != Yii::app()->user->idst));

        if($result['action']) {
            // If badge is to be assigned, fill in logging info
            $result['id_user'] = $user->idst;
            $result['event_module'] = 'myblog';
            $result['event_key'] = 'Student has commented on blog post "{post_name}" on ';
            if($post->user->idst == Yii::app()->user->idst)
                $result['event_key'] .= 'his own blog';
            else
                $result['event_key'] .= "another user's blog";

            $result['event_params'] = array(0 => array(
                '{post_name}' => $post->title
            ));
        }

        return $result;
    }
}