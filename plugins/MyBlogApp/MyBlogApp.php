<?php

/**
 * Plugin for MyBlogApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class MyBlogApp extends DoceboPlugin {

    public static $SET_TONE_HIDE = 1;
    public static $SET_TONE_POSTS_ONLY = 2;
    public static $SET_TONE_COMMENTS_ONLY = 3;
    public static $SET_TONE_EVERYWHERE = 4;

	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'MyBlogApp';


	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {
		parent::activate();

        // create default settings
        Settings::save('myblog_show_set_tone', 1); // do not show "set tone"
        Settings::save('myblog_set_tone_icons', 'smiley');
        Settings::save('myblog_show_helpful', '1'); // do not show "helpful"

        // create missing table(s)
        $sql = "
-- -----------------------------------------------------
-- Table `myblog_post`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `myblog_post` (
  `id_post` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `content` TEXT NOT NULL,
  `posted_on` TIMESTAMP NOT NULL,
  `id_user` INT(11) NOT NULL,
  `id_course` INT(11) NULL,
  `tone_sum` INT(11) NOT NULL DEFAULT 0,
  `tone_count` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_post`),
  INDEX `fk_myblog_post_core_user_idx` (`id_user` ASC),
  INDEX `fk_myblog_post_learning_course1_idx` (`id_course` ASC),
  CONSTRAINT `fk_myblog_post_core_user`
    FOREIGN KEY (`id_user`)
    REFERENCES `core_user` (`idst`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_myblog_post_learning_course1`
    FOREIGN KEY (`id_course`)
    REFERENCES `learning_course` (`idCourse`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB
COLLATE='utf8_general_ci';


-- -----------------------------------------------------
-- Table `myblog_comment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `myblog_comment` (
  `id_comment` INT(11) NOT NULL AUTO_INCREMENT,
  `id_post` INT(11) NOT NULL,
  `id_user` INT(11) NOT NULL,
  `content` TEXT NOT NULL,
  `posted_on` TIMESTAMP NOT NULL,
  `tone_sum` INT(11) NOT NULL DEFAULT 0,
  `tone_count` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_comment`),
  INDEX `fk_myblog_comment_myblog_post1_idx` (`id_post` ASC),
  INDEX `fk_myblog_comment_core_user1_idx` (`id_user` ASC),
  CONSTRAINT `fk_myblog_comment_myblog_post1`
    FOREIGN KEY (`id_post`)
    REFERENCES `myblog_post` (`id_post`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_myblog_comment_core_user1`
    FOREIGN KEY (`id_user`)
    REFERENCES `core_user` (`idst`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COLLATE='utf8_general_ci';


-- -----------------------------------------------------
-- Table `myblog_post_vote`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `myblog_post_vote` (
  `id_post` INT(11) NOT NULL,
  `id_user` INT(11) NOT NULL,
  `value` INT(4) NOT NULL DEFAULT 0,
  `type` ENUM('tone','helpful') NOT NULL DEFAULT 'tone',
  PRIMARY KEY (`id_post`, `id_user`, `type`),
  INDEX `fk_myblog_post_vote_core_user1_idx` (`id_user` ASC),
  CONSTRAINT `fk_myblog_post_vote_myblog_post1`
    FOREIGN KEY (`id_post`)
    REFERENCES `myblog_post` (`id_post`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_myblog_post_vote_core_user1`
    FOREIGN KEY (`id_user`)
    REFERENCES `core_user` (`idst`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COLLATE='utf8_general_ci';


-- -----------------------------------------------------
-- Table `myblog_comment_vote`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `myblog_comment_vote` (
  `id_comment` INT(11) NOT NULL,
  `id_user` INT(11) NOT NULL,
  `value` INT(4) NOT NULL DEFAULT 0,
  `type` ENUM('tone','helpful') NOT NULL DEFAULT 'tone',
  PRIMARY KEY (`id_comment`, `id_user`, `type`),
  INDEX `fk_myblog_post_vote_core_user1_idx` (`id_user` ASC),
  CONSTRAINT `fk_myblog_post_vote_core_user10`
    FOREIGN KEY (`id_user`)
    REFERENCES `core_user` (`idst`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_myblog_comment_vote_myblog_comment1`
    FOREIGN KEY (`id_comment`)
    REFERENCES `myblog_comment` (`id_comment`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COLLATE='utf8_general_ci';
        ";
        $command = Yii::app()->db->createCommand($sql);
        $command->execute();

	}

	public function deactivate() {
		parent::deactivate();
		//Removing widgets from dashboards
		DashboardDashlet::model()->deleteAllByAttributes(array('handler' => 'plugin.MyBlogApp.widgets.DashletBlog'));
	}

}
