<?php
/* @var $this BlogPostList */
/* @var $dataProvider CActiveDataProvider */
/* @var $_post MyblogPost */
?>

<div class="blogpost-list">

    <?php if ($dataProvider->getTotalItemCount() == 0) : ?>
        <div class="well empty">
            <?= Yii::t('myblog', 'No blog posts available.') ?>
        </div>
    <?php else: ?>

        <div class="container">
        <?php foreach ($dataProvider->getData() as $_post) : ?>

            <div class="blogpost">
                <div class="blogpost-header">

                    <div class="post-title">
                        <span><?= $_post->user->getAvatarImage() ?></span>
					<?php
						if($idCourse !== '' && $idCourse > 0)
							$url = Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/CoursePost',  array('idPost' => $_post->id_post, 'idCourse' => $idCourse));
						else
							$url = Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/post', array('idPost'=>$_post->id_post));
					?>
                        <h3><a href="<?= $url ?>"><?= CHtml::encode($_post->title) ?></a></h3>
                    </div>

                    <div class="post-details">
                        <div class="inner">
                            <div class="date pull-right">
                                <span class="myblog-sprite calendar"></span>
                                <?= $_post->posted_on ?>
                            </div>
                            <div class="author">
                                <span class="myblog-sprite user"></span>
                                <a href="<?= Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/blog', array('idUser'=>$_post->id_user)) ?>"><?= $_post->user->getFullName() ?></a>
                            </div>
                        </div>
                    </div>

                    <?php if ($_post->course && !$this->idCourse) : ?>
                    <div class="post-tags">
                        <?= Yii::t('myblog', 'Related to:') ?>
                        <ul>
                            <li><a href="<?= Docebo::createAppUrl('lms:player', array('course_id'=>$_post->course->idCourse)) ?>"><?= $_post->course->name ?></a></li>
                        </ul>
                    </div>
                    <?php endif; ?>
                </div>

                <div class="blogpost-content">
                    <?= Yii::app()->htmlpurifier->purify($_post->content, 'allowFrameAndTarget'); ?>
                </div>

                <div class="blogpost-footer">

                    <?php if ($this->showRating) : ?>
                    <div class="blog-rating">
                        <label><?= Yii::t('myblog', 'Set tone:') ?></label>
                        <?php
                        $this->widget('common.widgets.DoceboStarRating',array(
                            'id'=>'post-rating-'.$_post->id_post . '-' . Docebo::randomHash(),
                            'name'=>'post_rating_'.$_post->id_post . '-' . Docebo::randomHash(),
                            'type' => $this->ratingType,
                            'readOnly' => ($_post->id_user == Yii::app()->user->id),
                            'value' => $_post->getRating(),
                            'ratingUrl' => Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/axVoteTone'),
                            'urlParams' => array(
                                'idPost' => $_post->id_post
                            )
                        ));
                        ?>
                    </div>
                    <?php endif; ?>

                    <?php if ($this->showHelpful) : ?>

                        <?php
                        $_postHelpful = $_post->getHelpful();
                        $this->widget('common.widgets.DoceboHelpful',array(
                            'id'=>'post-helpful-'.$_post->id_post,
                            'likes' => $_postHelpful['likes'],
                            'dislikes' => $_postHelpful['dislikes'],
                            'vote' => ($_post->id_user == Yii::app()->user->id) ? 'readonly' : $_post->getUserHelpfulVote(Yii::app()->user->id),
                            'ratingUrl' => Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/axVoteHelpful', array('idPost' => $_post->id_post))
                        ));
                        ?>

                    <?php endif; ?>

                    <div class="post-actions">
                        <div class="post-action-comment">
                            <span class="myblog-sprite comments"></span>
                            <a class="text-colored show-comments" href="<?=Docebo::createLmsUrl('MyBlogApp/MyBlogApp/AxLoadDashletComments', array('idPost'=>$_post->id_post))?>" post="<?= $_post->id_post ?>"><?= $_post->renderCommentActionLabel() ?></a>
                            |&nbsp;&nbsp;&nbsp;&nbsp;
                            <a class="full-article-link text-colored" href="<?=Docebo::createLmsUrl('MyBlogApp/MyBlogApp/post', array('idPost'=>$_post->id_post))?>"><?=Yii::t('blog', 'Read full article')?></a>
                        </div>
                        <?php if ($this->isAdminMode) : ?>
                        <div class="post-action-edit">
                            <span class="i-sprite is-edit"></span>
                            <a
                                class="text-colored open-dialog"
                                href="<?= Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/axEdit', array('idPost'=>$_post->id_post)) ?>"
                                closeOnOverlayClick="false"
                                closeOnEscape="false"
                                data-dialog-class="modal-edit-blogpost"><?= Yii::t('standard', '_MOD') ?></a>
                        </div>
                        <div class="post-action-delete">
                            <span class="i-sprite is-remove red"></span>
                            <a class="text-colored open-dialog" data-dialog-class="myblog-delete-confirm-dialog" href="<?= Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/axDeletePost', array('idPost'=>$_post->id_post)) ?>"><?= Yii::t('standard', '_DEL') ?></a>
                        </div>
                        <?php endif; ?>
                    </div>

                    <div class="blog-comments-dashlet postid-<?= $_post->id_post ?>">
                        <div class="blog-comments-leave-comment">
                            <div class="span1">
                                <?= Yii::app()->user->getAvatar()?>
                            </div>
                            <div class="span11">
                                <?php
                                echo CHtml::textArea('post_' . $_post->id_post, null, array('post_id' => $_post->id_post ));
                                echo CHtml::button(Yii::t('standard', '_PUBLISH'), array(
                                    'class'=>'btn-docebo green big sendReply',
                                    'id' => $_post->id_post,
                                    'url' => Docebo::createLmsUrl('MyBlogApp/MyBlogApp/axNewPostReply', array('idPost'=>$_post->id_post))
                                ));
                                ?>
                            </div>
                        </div>
                        <div class="blog-comments-older-comments"></div>
                        <div class="blog-commnets-load-more">
                            <a class="text-colored load-more-comments" href="#" data-page="0" ><?=Yii::t('myblog', 'Older comments...')?></a>
                        </div>
                    </div>


				</div>
				<?php if($dataProvider->pagination->pageCount == ($dataProvider->pagination->currentPage + 1)) : ?>
					<input type="hidden" id="no_more_posts_hidden_field" name="no_more_posts_hidden_field" value="1" />
				<?php endif; ?>
            </div>

        <?php endforeach; ?>
        </div>

		<a href="#" class="loadmore" data-page="<?= intval($this->page) + 1 ?>"><?= Yii::t('myblog', 'Older posts...') ?></a>

    <?php endif; ?>

</div>

<script type="text/javascript">

    var oBlog = {
        $list: null,
        initPosts: function() {
            oBlog.$list.find('.blog-rating > span').each(function(i, el) {
                var $el = $(el),
                    options = {};
                var value = parseInt($el.data('value')),
                    readOnly = parseInt($el.data('readonly'));
                options.readOnly = (value!=0 || readOnly==1);
                options.callback = function(value, link) {
                    var data = {"idPost":$(this).parent().data('idpost')};
                    data.value = value;
                    $.post('<?= Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/axVoteTone') ?>', data);
                };

                $el.find('input').rating(options);
            });
            $('.docebo-helpful').helpful();
        },
        loadPosts: function(iPage) {
            var url = '<?= Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/axLoadPosts') ?>';
            var data = {idUser: '<?= $this->idUser ?>', idCourse: '<?= $this->idCourse ?>', page:iPage};
            $('<div/>').load(url + ' .blogpost-list .blogpost', data, function(response) {
                var html = $(this).html();
                if ($.trim(html).length ) {
                    oBlog.$list.find('.container').eq(0).append(html);
                    oBlog.$list.find('a.loadmore').data('page', iPage+1);
                    oBlog.initPosts();
					if(oBlog.$list.find('.container').eq(0).find('#no_more_posts_hidden_field').length > 0)
						oBlog.$list.find('a.loadmore').hide();
                }
				else
					oBlog.$list.find('a.loadmore').hide();
            });
        },
        init: function() {
            oBlog.$list = $('.blogpost-list');
            oBlog.$list.find('a.loadmore').click(function(e) {
                e.preventDefault();
                var iPage = parseInt($(this).data('page'));
                oBlog.loadPosts(iPage);
            });
        }
    };

    $(document).ready(oBlog.init);

</script>