<?php
/* @var $this BlogSidebar */
/* @var $form CActiveForm */
/* @var $model MyblogPost */
/* @var $archive array */
?>

<div class="blog-sidebar-search">

    <h4><?= Yii::t('myblog', 'Search the Blog') ?></h4>

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'blog-search-form',
        'action' => $this->searchUrl,
      //  'method' => 'GET',
        'htmlOptions'=>array('class' => 'docebo-form grid-search-form'),
    ));
    ?>

    <div class="control-container odd">
        <div class="control-group">
            <div class="input-wrapper">
                <?= $form->textField($model, 'search', array(
                    'autocomplete' => 'off',
                    'data-autocomplete' => 'true',
                    'placeholder' => Yii::t('standard', '_SEARCH'),
                    'name' => 'search'
                )) ?>
                <span class="i-sprite is-search"></span>
            </div>
        </div>
    </div>

    <?php $this->endWidget(); ?>

    <br/>
    <h4><?= Yii::t('myblog', 'My Blog Archive') ?></h4>
    <?php if (!empty($archive)) : ?>
    <ul class="blog-archive">
        <?php foreach ($archive as $archiveRow) : ?>
        <li>
            <?php $_url = $this->searchUrl . '&' . implode('&', array(
                    'month='.$archiveRow['month'],
                    'year='.$archiveRow['year']
            )); ?>
            <a href="<?= $_url ?>" class="text-colored"><?= Yii::t('standard', '_MONTH_' . str_pad($archiveRow['month'], 2, '0', STR_PAD_LEFT)) . ' ' . $archiveRow['year'] ?></a>
            <a href="<?= $_url ?>" class="pull-right text-colored text-bold"><?= $archiveRow['total'] ?></a>
        </li>
        <?php endforeach; ?>
    </ul>
    <?php endif; ?>
</div>

<script type="text/javascript">

    var oBlogSearch = {
        $form: null,
        init: function() {
            oBlogSearch.$form = $('#blog-search-form');

            oBlogSearch.$form.find('.is-search').click(oBlogSearch.search);

            // on enter key press
            oBlogSearch.$form.find('input').bind('keypress', function(e) {
                var code = e.keyCode || e.which;
                // enter was pressed
                if(code == 13) {
                    e.preventDefault();
                    oBlogSearch.$form.submit();
                    return false;
                }
            });
        }
    };

    $(document).ready(oBlogSearch.init);
</script>