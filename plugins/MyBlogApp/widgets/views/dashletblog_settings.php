<style type="text/css">
	div#list_only_from_these_user_ids {
		padding-left: 30px;
	}
</style>

<br>

<div class="row-fluid">
	<div class="control-group">
		<label class="control-label"></label>
		<div class="controls">

				<p>
					<?=CHtml::radioButton('post_listing_type', $this->getParam('post_listing_type', DashletBlog::POST_LISTING_TYPE_MINE)===DashletBlog::POST_LISTING_TYPE_MINE, array(
						'value'=>DashletBlog::POST_LISTING_TYPE_MINE,
						'uncheckValue'=>null,
					))?> <?=Yii::t('blog', 'Show posts from <b>my blog</b>')?>
				</p>

				<br/>

				<p>
					<?=CHtml::radioButton('post_listing_type', $this->getParam('post_listing_type', DashletBlog::POST_LISTING_TYPE_MINE)===DashletBlog::POST_LISTING_TYPE_SPECIFIC_USERS, array(
						'value'=>DashletBlog::POST_LISTING_TYPE_SPECIFIC_USERS,
						'uncheckValue'=>null,
					))?> <?=Yii::t('blog', 'Show posts from <b>the following</b> users')?>:
				</p>
		</div>
	</div>
</div>

<div class="blog-posters-filter">
<div class="row-fluid">
	<div class="control-group">
		<label class="control-label"></label>
		<div class="controls">
			<div id="list_only_from_these_user_ids">
				<select name="list_only_from_these_user_ids">
					<?php
					if (count($currentUsers) > 0) {
						foreach ($currentUsers as $id=>$label) {
							echo "<option class=\"selected\" value=\"$id\" selected=\"selected\">$label</options>";
						}
					}
					?>
				</select>
			</div>
		</div>
	</div>
</div>
</div>

<br>

<div class="row-fluid">
	<div class="control-group">
		<label class="control-label"></label>
		<div class="controls">
			<div class="row-fluid">
				<div class="span6">
					<div>
						&nbsp;&nbsp;<?=CHtml::checkBox('show_post_date', $this->getParam('show_post_date'))?>
						<?=Yii::t('blog', 'Show post date')?>
					</div>
					<br/>
					<div>
						&nbsp;&nbsp;<?=CHtml::checkBox('show_author', $this->getParam('show_author'))?>
						<?=Yii::t('blog', 'Show post author')?>
					</div>
				</div>
				<div class="span6">
					<p><?=Yii::t('blog', 'Show max')?></p>
					<?=CHtml::textField('max_posts', $this->getParam('max_posts', 10), array(
						'class'=>'span5'
					))?>
					<?=Yii::t('blog', 'blog posts')?>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

					
	$(function(){

		var showPostersFilter = <?= json_encode($this->getParam('post_listing_type', DashletBlog::POST_LISTING_TYPE_MINE)===DashletBlog::POST_LISTING_TYPE_SPECIFIC_USERS) ?>;

		if (!showPostersFilter) {
			$('.blog-posters-filter').hide();
		}

		$("#list_only_from_these_user_ids select").fcbkcomplete({
			json_url: '<?=Docebo::createAbsoluteLmsUrl('MyBlogApp/MyBlogApp/axUsersAutocomplete')?>',
			addontab: true,
			width: '98.5%',
			cache: true,
			complete_text: '',
			newel: true,
			maxshownitems: 5,
			input_name: 'maininput-name',
			filter_selected: true
		});

		$(document).off('change', 'input[name="post_listing_type"]')
		.on('change', 'input[name="post_listing_type"]', function(){
			$('.blog-posters-filter').show();
			if ($(this).val() == '<?= DashletBlog::POST_LISTING_TYPE_MINE ?>') {
				$('.blog-posters-filter').hide();	
			} 
		});
		
		
	});

	
	
	
</script>