<?php
/* @var $this BlogPost */
/* @var $dataProvider CActiveDataProvider */
/* @var $post MyblogPost */
/* @var $comment MyblogComment */
/* @var $user CoreUser */
/* @var $_comment MyblogComment */
/* @var $form CActiveForm */
?>



<div class="blogpost">
    <div class="blogpost-header">

        <div class="post-details">
            <div class="inner">
                <div class="date pull-right">
                    <span class="myblog-sprite calendar"></span>
                    <?= $post->posted_on ?>
                </div>
                <div class="author-inline">
                    <span class="author-inline-image"><?= $post->user->getAvatarImage() ?></span>
                    <h4 class="author-inline-name"><?= $post->user->getFullName() ?></h4>
                </div>
            </div>
        </div>

        <?php if ($post->course) : ?>
        <div class="post-tags">
            <?= Yii::t('myblog', 'Related to:') ?>
            <ul>
                <li><a href="<?= Docebo::createAppUrl('lms:player', array('course_id'=>$post->course->idCourse)) ?>"><?= $post->course->name ?></a></li>
            </ul>
        </div>
        <?php endif; ?>
    </div>

    <div class="blogpost-content">
        <?= Yii::app()->htmlpurifier->purify($post->content, 'allowFrameAndTarget'); ?>
    </div>

    <div class="blogpost-footer">

        <?php if ($this->showRatingInPost) : ?>
        <div class="blog-rating">
            <label><?= Yii::t('myblog', 'Set tone:') ?></label>
            <?php
            $this->widget('common.widgets.DoceboStarRating',array(
                'id'=>'post-rating-'.$post->id_post,
                'name'=>'post_rating_'.$post->id_post,
                'type' => $this->ratingType,
                'readOnly' => ($post->id_user == Yii::app()->user->id),
                'value' => $post->getRating(),
                'ratingUrl' => Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/axVoteTone'),
                'urlParams' => array(
                    'idPost' => $post->id_post
                )
            ));
            ?>
        </div>
        <?php endif; ?>

        <?php if ($this->showHelpfulInPost) : ?>

            <?php
            $_postHelpful = $post->getHelpful();
            $this->widget('common.widgets.DoceboHelpful',array(
                'id'=>'post-helpful-'.$post->id_post,
                'likes' => $_postHelpful['likes'],
                'dislikes' => $_postHelpful['dislikes'],
                'vote' => ($post->id_user == Yii::app()->user->id) ? 'readonly' : $post->getUserHelpfulVote(Yii::app()->user->id),
                'ratingUrl' => Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/axVoteHelpful', array('idPost' => $post->id_post))
            ));
            ?>

        <?php endif; ?>

        <div class="post-actions">
            <div class="post-action-comment">
                <span class="myblog-sprite comments"></span>
                <a class="text-colored" href="#comment"><?= $post->renderCommentActionLabel() ?></a>
            </div>
            <?php if ($this->isAdminMode) : ?>
            <div class="post-action-edit">
                <span class="i-sprite is-edit"></span>
                <a class="text-colored open-dialog"
                   href="<?= Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/axEdit', array('idPost'=>$post->id_post)) ?>"
                   closeOnOverlayClick="false"
                   closeOnEscape="false"
                   data-dialog-class="modal-edit-blogpost"><?= Yii::t('standard', '_MOD') ?></a>
            </div>
            <div class="post-action-delete">
                <span class="i-sprite is-remove red"></span>
                <a class="text-colored open-dialog" data-dialog-class="myblog-delete-confirm-dialog" href="<?= Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/axDeletePost', array('idPost'=>$post->id_post)) ?>"><?= Yii::t('standard', '_DEL') ?></a>
            </div>
            <?php endif; ?>
        </div>

    </div>
</div>


<div class="blogcomment-list">
    <h3 class="title-bold" id="comment"><?= Yii::t('standard', '_COMMENTS') ?></h3>

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'myblogapp-comment-form',
        'htmlOptions'=>array('class' => 'form-horizontal docebo-form blogcomment-form'),
    ));
    ?>

    <div class="clearfix">
        <span class="comment-author-picture"><?= $user->getAvatarImage() ?></span>

        <div class="well">
            <?= $form->textArea($comment, 'content') ?>
        </div>

    </div>

    <div class="form-actions">
        <?=CHtml::submitButton(Yii::t('standard', '_PUBLISH'), array(
            'class'=>'btn-docebo green big disabled'
        )); ?>
    </div>

    <?php $this->endWidget(); ?>

    <!--Container for the post comments-->
    <div class="container"></div>

    <a href="#" class="loadmore" data-page="0"><?= Yii::t('myblog', 'Older comments...') ?></a>

</div>

<script type="text/javascript">

    var oComments = {
        $list: null,
        $form: null,
        initComments: function() {
            $(document).controls();

            oComments.$list.find('.blog-rating > span').each(function(i, el) {
                var $el = $(el),
                    options = {};
                var value = parseInt($el.data('value')),
                    readOnly = parseInt($el.data('readonly'));
                options.readOnly = (value!=0 || readOnly==1);
                options.callback = function(value, link) {
                    var data = {"idComment":$(this).parent().data('idcomment')};
                    data.value = value;
                    $.post('<?= Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/axVoteTone') ?>', data);
                };

                $el.find('input').rating(options);
            });
            $('.docebo-helpful').helpful();

            oComments.$list.find('.comment-edit form')
                .not('.ajax-form')
                    .addClass('ajax-form')
                    .each(function() {
                        var $form = $(this);

                        $form.ajaxForm(function(html, statusText, xhr, $form) {
                            // update comment
                            var $comment = $form.closest('.blogcomment');
                            $comment.find('.comment-edit').hide().find('textarea').val(html);
                            $comment.find('.comment-preview').html(html).show();
                        });

                        $form.find('textarea')
                            .keyup(function(e) {
                                var len = $.trim($(this).val()).length;
                                if (len) {
                                    $form.find(':submit').removeClass('disabled');
                                } else {
                                    $form.find(':submit').addClass('disabled');
                                }
                            });

                        $form.submit(function() {
                            if ( $form.find(':submit').hasClass('disabled') )
                                return false;
                        });
                    });
        },
        loadComments: function(iPage) {
            var url = '<?= Docebo::createAppUrl('lms:MyBlogApp/MyBlogApp/axLoadComments') ?>'
            var data = {idPost:'<?= intval($post->id_post) ?>', page:iPage};

            $('<div/>').load(url, data, function(response) {
                var html = $(this).html();
                if ($.trim(html).length) {
                    var $container = oComments.$list.find('.container').eq(0);
                    if (iPage == 0) {
                        $container.html(html);
                    } else {
                        $container.append(html);
                    }
                    oComments.$list.find('a.loadmore').data('page', iPage+1);
                    oComments.initComments();
                }
                else {
                    oComments.$list.find('a.loadmore').hide();
                }
            });
        },
        init: function() {
            oComments.$list = $('.blogcomment-list');

            oComments.$form = $('#myblogapp-comment-form');


            oComments.$form.find('textarea')
            .keyup(function(e) {
                var len = $.trim($(this).val()).length;
                if (len) {
                    oComments.$form.find(':submit').removeClass('disabled');
                } else {
                    oComments.$form.find(':submit').addClass('disabled');
                }
            });

            oComments.$form.submit(function() {
                if ( oComments.$form.find(':submit').hasClass('disabled') )
                    return false;
            });
            oComments.$form.ajaxForm(function(response) {
                oComments.$form.find('textarea').val('');
                oComments.$form.find(':submit').addClass('disabled');
                oComments.loadComments(0);
            });

            oComments.$list.find('a.loadmore').click(function(e) {
                e.preventDefault();
                var iPage = parseInt($(this).data('page'));
                oComments.loadComments(iPage);
            });

            oComments.loadComments(0);

            $('body').on('click', 'a.blogcomment-edit', function(e) {
                e.preventDefault();
                var that = $(this);
                var $comment = that.closest('.blogcomment');

                $comment.find('.comment-preview').hide();
                $comment.find('.comment-edit').show();
            });
        }
    };

    $(document).ready(oComments.init);
</script>