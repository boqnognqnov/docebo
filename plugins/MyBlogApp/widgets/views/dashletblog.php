<style type="text/css">
	.blog-posts-dashlet .single-post{
		margin-bottom: 25px;
	}
	.blog-posts-dashlet .meta{
		font-size: 10px;
		color: #a4a4a4;
		margin: 0;
	}
	.blog-posts-dashlet .post-title{
		font-weight: bold;
		color: #0765AB;
	}
	.blog-posts-dashlet .post-content,
	.blog-posts-dashlet .post-content p{
		margin: 0;
	}
	.blog-posts-dashlet .blog-post-comments{
		background: url('<?=Yii::app()->theme->getBaseUrl()?>/images/my_blog_sprite.png') no-repeat -34px -29px;
		display: inline-block;
		zoom: 1;
		vertical-align: middle;
		width: 19px;
		height: 16px;
	}
	.blog-posts-dashlet a,
	.blog-posts-dashlet a:visited{
		color: #0465AC;
	}
</style>
<div class="blog-posts-dashlet">
<?php foreach($posts as $key => $post): ?>
	<div class="single-post">
		<?php if($this->getParam('show_post_date') || $this->getParam('show_author')): ?>
		<p class="meta">
			<?php if(!$this->getParam('show_post_date') && $this->getParam('show_author')): ?>
				<?=Yii::t('blog', 'Posted by {author}', array('{author}'=>$post['authorName']))?>
			<?php elseif($this->getParam('show_post_date') && !$this->getParam('show_author')): ?>
				<?=Yii::t('blog', 'Posted on {date}', array('{date}'=>Yii::app()->localtime->toLocalDate($post['posted_on'])))?>
			<?php else: ?>
				<?=Yii::t('blog', 'Posted on {date} by {author}', array('{date}'=>Yii::app()->localtime->toLocalDate($post['posted_on']), '{author}'=>$post['authorName']))?>
			<?php endif; ?>
		</p>
		<?php endif; ?>

		<?php if($post['image']): ?>
			<div class="pull-left dashlet-blog-post-image">
				<img src="<?=$post['image']?>" alt="<?=$post['title']?>"/>
			</div>
		<?php endif; ?><p class="post-title">
			<?=CHtml::encode($post['title'])?>
		</p>

		<div class="post-content">
			<?=$post['content']?>
		</div>

		<br/>

		<p>
            <span class="blog-post-comments"></span>
            <?php $label = $post['comments'] ? (Yii::t('standard', '_COMMENTS') . ' ('.$post['comments'].')') : Yii::t('forum', 'Write here your comment') ?>
            <a class="text-colored show-comments" href="<?=Docebo::createLmsUrl('MyBlogApp/MyBlogApp/AxLoadDashletComments', array('idPost'=>$post['id']))?>" post="<?= $post['id'] ?>"><?=$label?></a>
            &nbsp;&nbsp;|&nbsp;&nbsp;
			<a class="full-article-link text-colored" href="<?=Docebo::createLmsUrl('MyBlogApp/MyBlogApp/post', array('idPost'=>$post['id']))?>"><?=Yii::t('blog', 'Read full article')?></a>
		</p>

        <div class="blog-comments-dashlet postid-<?php echo $post['id']?>">
            <div class="blog-comments-leave-comment">
                <div class="span1">
                    <?= Yii::app()->user->getAvatar()?>
                </div>
                <div class="span11">
                    <?php
                        echo CHtml::textArea('post_' . $post['id'], null, array('post_id' => $post['id']));
                        echo CHtml::button(Yii::t('standard', '_PUBLISH'), array(
                            'class'=>'btn-docebo green big sendReply',
                            'id' => $post['id'],
                            'url' => Docebo::createLmsUrl('MyBlogApp/MyBlogApp/axNewPostReply', array('idPost'=>$post['id'])),
                        ));
                    ?>
                </div>
            </div>
            <div class="blog-comments-older-comments"></div>
            <div class="blog-commnets-load-more">
                <a class="text-colored load-more-comments" href="#" data-page="0" ><?=Yii::t('myblog', 'Older comments...')?></a>
            </div>
        </div>
	</div>
    <?php if($key != count($posts)-1) echo "<hr>"; ?>

<?php endforeach; ?>
</div>