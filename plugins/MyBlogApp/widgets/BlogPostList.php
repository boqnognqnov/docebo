<?php

/**
 * Class BlogPostList
 * Renders a list of blog posts, followed by a link to load more
 * The sort order is newest first
 * @package widgets
 */
class BlogPostList extends CWidget {

    public $isAdminMode;
    public $idUser = '';
    public $idCourse = '';
    public $count = 3;
    public $page;

    public $showHelpful;
    public $showRating;
    public $ratingType;

    public function init() {

        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile(Yii::app()->baseUrl.'/../plugins/MyBlogApp/assets/css/replies.css');
        $cs->registerScriptFile(Yii::app()->baseUrl.'/../plugins/MyBlogApp/assets/js/courseBlog.js');
        $cs->registerCssFile(Yii::app()->baseUrl.'/../plugins/MyBlogApp/assets/css/myblog.css');

        $myblog_show_helpful = Settings::get('myblog_show_helpful', 0);
        $this->showHelpful = in_array($myblog_show_helpful, array(2, 4));

        $myblog_show_set_tone = Settings::get('myblog_show_set_tone', 0);
        $this->showRating = in_array($myblog_show_set_tone, array(2, 4));

        $this->ratingType = Settings::get('myblog_set_tone_icons', 'star');
    }
    public function run() {
		/* @var $model MyblogPost */
		$model = new MyblogPost('search');
		$model->id_user = $this->idUser;
		$model->id_course = $this->idCourse;
		$model->search = Yii::app()->request->getParam('search');

		$options = array(
			'pageSize' => $this->count,
			'page' => intval($this->page)
		);

		$this->render('_blog_post_list', array(
			'dataProvider' => $model->dataProvider($options),
			'idCourse' => $this->idCourse
		));
    }

}