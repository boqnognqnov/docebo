<?php

/**
 * Class BlogPost
 * Renders a detailed blog post view
 * @package widgets
 */
class BlogPost extends CWidget {

    /**
     * @var bool
     */
    public $isAdminMode;
    /**
     * @var MyblogPost
     */
    public $post;

    public $showHelpfulInPost;
    public $showHelpfulInComments;
    public $showRatingInPost;
    public $showRatingInComments;
    public $ratingType;

    public function init() {

        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile(Yii::app()->baseUrl.'/../plugins/MyBlogApp/assets/css/myblog.css');

        $myblog_show_helpful = Settings::get('myblog_show_helpful', 0);
        $this->showHelpfulInPost = in_array($myblog_show_helpful, array(2, 4));
        $this->showHelpfulInComments = in_array($myblog_show_helpful, array(3, 4));

        $myblog_show_set_tone = Settings::get('myblog_show_set_tone', 0);
        $this->showRatingInPost = in_array($myblog_show_set_tone, array(2, 4));
        $this->showRatingInComments = in_array($myblog_show_set_tone, array(3, 4));

        $this->ratingType = Settings::get('myblog_set_tone_icons', 'star');
    }
    public function run() {

        $user = CoreUser::model()->findByPk(Yii::app()->user->id);

        $comment = new MyblogComment();

        $this->render('_blog_post', array(
            'post' => $this->post,
            'comment' => $comment,
            'user' => $user
        ));

    }

} 