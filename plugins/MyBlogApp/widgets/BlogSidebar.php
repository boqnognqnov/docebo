<?php

class BlogSidebar extends CWidget {

    public $idUser;
    public $searchUrl;

    public function init() {}

    public function run() {

        $model = new MyblogPost('search');
        $model->search = Yii::app()->request->getParam('search');

        $archive = $model::getBlogArchive($this->idUser);

        $this->render('_blog_sidebar', array(
            'model' => $model,
            'archive' => $archive
        ));

    }

} 