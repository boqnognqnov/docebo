<?php

class DashletBlog extends DashletWidget implements IDashletWidget{

	const POST_LISTING_TYPE_MINE = 'only_mine';
	const POST_LISTING_TYPE_SPECIFIC_USERS = 'specific_users';

	public function renderFrontEnd() {
		$postListingType = $this->getParam('post_listing_type', self::POST_LISTING_TYPE_MINE); // only_mine|specific_users

		if(!in_array($postListingType, array(self::POST_LISTING_TYPE_MINE, self::POST_LISTING_TYPE_SPECIFIC_USERS))){
			throw new CException('Invalid blog post listing type');
		}

		$showPostDate = (bool) $this->getParam('show_post_date', true);
		$showAuthor = (bool) $this->getParam('show_author', true);
		$maxNumPostsToShow = (int) $this->getParam('max_posts', 10);

		// Show posts from those user IDs only
		$userIdsFilter = array();

		switch($postListingType){
			case self::POST_LISTING_TYPE_SPECIFIC_USERS:
				$specificUserIds = $this->getParam('list_only_from_these_user_ids', array());
				if(!$specificUserIds) $specificUserIds = array();

				$userIdsFilter = (array) $specificUserIds;
				break;
			case self::POST_LISTING_TYPE_MINE:
			default:
				$userIdsFilter = array(Yii::app()->user->getIdst());
				break;
		}

		$postsArr = array();
		if(is_array($userIdsFilter) && !empty($userIdsFilter)){
			$command = Yii::app()->getDb()->createCommand()
						->select('p.id_post, p.title, p.content, p.id_user, u.firstname, u.lastname, p.posted_on, COUNT(cm.id_post) AS comments')
						->from(MyblogPost::model()->tableName().' p')
						->join(CoreUser::model()->tableName().' u', 'u.idst=p.id_user')
						->leftJoin(MyblogComment::model()->tableName().' cm', 'cm.id_post=p.id_post')
						->limit($maxNumPostsToShow)
						->group('p.id_post')
						->order('p.posted_on DESC')
						->where(array('IN', 'p.id_user', $userIdsFilter));

			//if(Yii::app()->user->getIsPu()){
			//		$command->join(CoreUserPU::model()->tableName().' pu', 'pu.user_id=u.idst AND pu.puser_id=:currentUserId', array(':currentUserId'=>Yii::app()->user->getIdst()));
			//	}

			$query = $command->queryAll();

			foreach($query as $singlePost){
				$body = $singlePost['content'];
				$body = Yii::app()->htmlpurifier->purify($body);
				$body = strip_tags($body, '<ul><li><ol><br><p><a>');

				if(strlen($body)>250)
					$body = $this->truncateHtml($body, 247);

				// Extract the first image from the blog post if found
				$dom = new DOMDocument();

				libxml_use_internal_errors(true);
				$dom->loadHTML($singlePost['content']);
				$xml = simplexml_import_dom($dom);
				libxml_use_internal_errors(false);

				$images = $xml -> xpath('//img/@src');
				$image = false;
				if(is_array($images) && count($images)){
					$image = reset($images);
					unset($images);
				}

				$postsArr[] = array(
					'title'=>$singlePost['title'],
					'content'=>$body,
					'image'=>$image,
					'authorName'=>$singlePost['firstname'].' '.$singlePost['lastname'],
					'posted_on'=>$singlePost['posted_on'],
					'comments'=>$singlePost['comments'],
					'id'=>$singlePost['id_post'],
				);
			}
		}


		$this->render(strtolower(__CLASS__), array(
			'posts'=>$postsArr
		));
	}

	/**
	* truncateHtml can truncate a string up to a number of characters while preserving whole words and HTML tags
	*
	* @param string $text String to truncate.
	* @param integer $length Length of returned string, including ellipsis.
	* @param string $ending Ending to be appended to the trimmed string.
	* @param boolean $exact If false, $text will not be cut mid-word
	* @param boolean $considerHtml If true, HTML tags would be handled correctly
	*
	* @return string Trimmed string.
	*/
	public function truncateHtml($text, $length = 100, $ending = '...', $exact = false, $considerHtml = true)
	{
		if ($considerHtml)
		{
			// if the plain text is shorter than the maximum length, return the whole text
			if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length)
				return $text;
			// splits all html-tags to scanable lines
			preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);
			$total_length = strlen($ending);
			$open_tags = array();
			$truncate = '';
			foreach ($lines as $line_matchings)
			{
				// if there is any html-tag in this line, handle it and add it (uncounted) to the output
				if (!empty($line_matchings[1]))
				{
					// if it's an "empty element" with or without xhtml-conform closing slash
					if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1]))
					{
					// do nothing
					// if tag is a closing tag
					}
					elseif(preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings))
					{
						// delete tag from $open_tags list
						$pos = array_search($tag_matchings[1], $open_tags);
						if ($pos !== false)
							unset($open_tags[$pos]);
						// if tag is an opening tag
					}
					elseif(preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings))
						// add tag to the beginning of $open_tags list
						array_unshift($open_tags, strtolower($tag_matchings[1]));
					// add html-tag to $truncate'd text
					$truncate .= $line_matchings[1];
				}
				// calculate the length of the plain text part of the line; handle entities as one character
				$content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
				if ($total_length+$content_length> $length)
				{
					// the number of characters which are left
					$left = $length - $total_length;
					$entities_length = 0;
					// search for html entities
					if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE))
					{
						// calculate the real length of all entities in the legal range
						foreach ($entities[0] as $entity)
						{
							if($entity[1]+1-$entities_length <= $left)
							{
								$left--;
								$entities_length += strlen($entity[0]);
							}
							else
								break; // no more characters left
						}
					}
					$truncate .= substr($line_matchings[2], 0, $left+$entities_length);
					// maximum lenght is reached, so get off the loop
					break;
				}
				else
				{
					$truncate .= $line_matchings[2];
					$total_length += $content_length;
				}
				// if the maximum length is reached, get off the loop
				if($total_length>= $length)
					break;
			}
		}
		else
		{
			if (strlen($text) <= $length)
				return $text;
			else
				$truncate = substr($text, 0, $length - strlen($ending));
		}
		// if the words shouldn't be cut in the middle...
		if (!$exact)
		{
			// ...search the last occurance of a space...
			$spacepos = strrpos($truncate, ' ');
			if (isset($spacepos))
				$truncate = substr($truncate, 0, $spacepos);// ...and cut the text in this position
		}
		// add the defined ending to the text
		$truncate .= $ending;
		if($considerHtml)
		{
			// close all unclosed html-tags
			foreach ($open_tags as $tag)
				$truncate .= '</' . $tag . '>';
		}
		return $truncate;
	}

	/**
	 * Render dashlet settings, used in Admin Dashlets UI
	 */
	public function renderSettings() {
		$cs = Yii::app()->getClientScript();
		$themeUrl = Yii::app()->theme->baseUrl;

		// FCBK
		$cs->registerCssFile($themeUrl . '/css/fcbkcomplete.css');
		$cs->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js');

		$currentListOfUserIds = $this->getParam('list_only_from_these_user_ids', array());
		$currentSelectedUsers = array(); // passed to the view later
		if(!empty($currentListOfUserIds)){
			// When editing an existing dashlet, pre-populate some fields with the old values
			$command = Yii::app()->getDb()->createCommand();
			$command->select('u.idst, u.userid, u.firstname, u.lastname, u.email')->from(CoreUser::model()->tableName().' u');
			$command->where(array('IN', 'u.idst', $currentListOfUserIds));
			$command->limit(1000);

			if(Yii::app()->user->getIsPu()){
				// PU users filter
				$command->join(CoreUserPU::model()->tableName().' pu', 'pu.user_id=u.idst AND puser_id=:puId', array(':puId'=>Yii::app()->user->getIdst()));
			}

			$results = $command->queryAll();

			foreach($results as $row){
				$firstname = $row['firstname'];
				$lastname = $row['lastname'];
				$email = $row['email'];
				$userid = $row['userid']; // username
				$idst = $row['idst'];

				$label = trim("&raquo;{$firstname} {$lastname}> {$email}");

				$name = $firstname . " " . $lastname;
				$label = ltrim($userid, '/') . (trim($name) ? ' - ' . trim($name) : '');

				$currentSelectedUsers[$row['idst']] = $label;
			}
		}

		$this->render(strtolower(__CLASS__).'_settings', array(
			'currentUsers'=>$currentSelectedUsers,
		));
	}


	/**
	 * Dashlet descriptor
	 *
	 * @return DashletDescriptor Object, describing the dashlet to the outside world
	 */
	public static function descriptor( $params = FALSE ) {
		$sampleImageUrl = Yii::app()->mydashboard->getModule()->getAssetsUrl() . "/images/dashlet_sample.png";

		$descriptor = new DashletDescriptor();

		$descriptor->name 					= 'core_dashlet_blog';
		$descriptor->handler				= 'plugin.MyBlogApp.widgets.DashletBlog';
		$descriptor->title					= Yii::t('myblog', 'Blog');
		$descriptor->description			= Yii::t('dashlets', 'Shows the latest posts from the blog of a selected user');
		$descriptor->sampleImageUrl 		= $sampleImageUrl;
		$descriptor->settingsFieldNames		= array('post_listing_type', 'show_post_date', 'show_author', 'max_posts', 'list_only_from_these_user_ids');
		$descriptor->hasHeight				= 1;

		return $descriptor;
	}

	/**
	 * @see IDashletWidget::bootstrap()
	 */
	public function bootstrap($params=false) {
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile(Yii::app()->baseUrl.'/../plugins/MyBlogApp/assets/js/dashletBlog.js');
        $cs->registerCssFile(Yii::app()->baseUrl.'/../plugins/MyBlogApp/assets/css/replies.css');
	}

}