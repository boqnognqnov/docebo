<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */

class WhitelabelAppModule extends CWebModule {


	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}


	public function init() {
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		$this->setImport(array(
		));

		// Change the header/footer is it is set in the Whitelabel config
		Yii::app()->event->on("BeforeHeaderRender", array($this, 'renderCustomHeader'));
		Yii::app()->event->on("BeforeFooterRender", array($this, 'renderCustomFooter'));
	}


	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu() {

	}

	/**
	 * Adds security params to iframe URL
	 * @param $src
	 */
	private function getIframeUrl($src) {

		// - username
		$username = Yii::app()->user->getUsername();
		if (!strstr($src, '?')) {
			$src .= '?username='.urlencode($username);
		} else {
			$src .= '&username='.urlencode($username);
		}

		// - LMS custom domain
		$url = Settings::get('custom_domain_url');
		if(!$url)
			$url = parse_url(Settings::get('url'), PHP_URL_HOST);
		$src .= '&domain='.urlencode($url);

		// - timestamp
		$time = time();
		$src .= '&timestamp='.$time;

		// - SHA 1 of all params with SSO token
		$secret = Settings::get('sso_secret', '8ca0f69afeacc7022d1e589221072d6bcf87e39c');
		$sha1 = sha1($username.",".$url.",".$time.",".$secret);
		$src .= '&hash='.$sha1;

		// make sure there is http::// in the iframe source
		if(!preg_match('/^([http::\/\/]|[https::\/\/])+/', $src)) {
			$src = 'http:://' . $src;
		}

		return $src;
	}

	/**
	 * @param $event DEvent
	 * @return bool
	 */
	public function renderCustomHeader($event) {
		$headerType = Settings::get('whitelabel_header', null, false, false);
		$headerUrlHeight = Settings::get('whitelabel_header_ext_height', '50', false, false);
		$currentLang = $_SESSION['current_lang'];
		$defaultLanguage = CoreLangLanguage::getDefaultLanguage();
		// get main domain header url
		$headerUrl = Yii::app()->db->createCommand()
			->select('value')
			->from(CoreLangWhiteLabelSetting::model()->tableName())
			->where('type=:type AND language=:lang', array(
				':type' => BrandingWhiteLabelForm::POWEREDBY_CUSTOM_URL_HEADER,
				':lang' => $currentLang
			))->queryScalar();

		if(empty($headerUrl) || $headerUrl == 'http://'){
			// get main domain default language setting
			$headerUrl = Yii::app()->db->createCommand()
				->select('value')
				->from(CoreLangWhiteLabelSetting::model()->tableName())
				->where('type=:type AND language=:lang', array(
					':type' => BrandingWhiteLabelForm::POWEREDBY_CUSTOM_URL_HEADER,
					':lang' => $defaultLanguage
				))->queryScalar();
		}

		Yii::app()->event->raise('GetWhiteLabelSettings', new DEvent(self, array(
			'whitelabel_header' => &$headerType,
			'whitelabel_header_ext_url' => &$headerUrl,
			'whitelabel_header_ext_height' => &$headerUrlHeight,
		)));



		if($headerType != 'external_header' ||  !$headerUrl){
			if(PluginManager::isPluginActive('ApchqApp')){
				return false;
			}else{
				return;
			}
		}


		$src = $this->getIframeUrl($headerUrl);

		echo '<iframe style="height: '.$headerUrlHeight.'px; width: 100%; border: 0px; margin-bottom: 10px; overflow-y: hidden;"
		 src="'.$src.'" scrolling="no" seamless="seamless"></iframe>';
		return false;
	}

	/**
	 * @param $event DEvent
	 * @return bool
	 */
	public function renderCustomFooter($event) {
		$footerUrl = Settings::get('whitelabel_footer_ext_url', null, false, false);
		$footerUrlHeight = Settings::get('whitelabel_footer_ext_height', '30', false, false);
		$modelSettingByLanguage = CoreLangWhiteLabelSetting::model()->findByAttributes(
				array(
						'language' => Lang::getCodeByBrowserCode(Yii::app()->getLanguage()),
						'type' => BrandingWhiteLabelForm::POWEREDBY_CUSTOM_URL
				)
		);
		if (!$modelSettingByLanguage || $modelSettingByLanguage->value == 'http://'){
			$modelSettingByLanguage = CoreLangWhiteLabelSetting::model()->findByAttributes(
					array(
							'language' => CoreLangLanguage::getDefaultLanguage(),
							'type' => BrandingWhiteLabelForm::POWEREDBY_CUSTOM_URL
					)
			);
		}

		$footerUrl = $modelSettingByLanguage->value;
		$footerUrlHeight = Settings::get('whitelabel_footer_ext_height', '30');
		$footerType = Settings::get('whitelabel_footer', '30', false, false);

		Yii::app()->event->raise('GetWhiteLabelSettings', new DEvent(self, array(
			'whitelabel_footer_ext_url' => &$footerUrl,
			'whitelabel_footer_ext_height' => &$footerUrlHeight,
			'whitelabel_footer' => &$footerType,
		)));


		if($footerType != 'custom_url' || !$footerUrl)
			if(PluginManager::isPluginActive('ApchqApp')){
				return false;
			}else{
				return;
			}

		$src = $this->getIframeUrl($footerUrl);

		echo '<iframe style="height: '.$footerUrlHeight.'px; width: 100%; border: 0px; margin-top: 20px; overflow-y: hidden;"
		 src="'.$src.'" scrolling="no" seamless="seamless"></iframe>';
		return false;
	}


}
