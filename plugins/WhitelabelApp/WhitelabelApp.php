<?php

/**
 * Plugin for EnrollmentrulesApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class WhitelabelApp extends DoceboPlugin {


	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'WhitelabelApp';
	public static $coreParams = array(
		'whitelabel_footer' => array('default' => 'show_docebo'),
		'whitelabel_footer_text' => array('default' => ''),
		'whitelabel_menu' => array('default' => ''),
		'whitelabel_menu_userid' => array('default' => ''),
		'whitelabel_helpdesk' => array('default' => 'to_docebo'),
		'whitelabel_helpdesk_email' => array('default' => ''),
		'whitelabel_naming' => array('default' => ''),
		'whitelabel_naming_text' => array('default' => ''),
		'whitelabel_naming_site' => array('default' => ''),
	);

	public static function settingsUrl()
	{
		return Docebo::createAdminUrl('branding/index', array('#'=>'whiteLabel'));
	}

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {
		foreach (self::$coreParams as $param => $properties)
		{
			if (Settings::get($param, null) === null)
				Settings::save($param, $properties['default']);
		}
		parent::activate();
	}


	public function deactivate() {
		parent::deactivate();
	}

}
