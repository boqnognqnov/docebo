<?php
/**
 * Yii DB Migration template.
 * 
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 * 
 */
class m160816_121649_ALTER_TABLE_simplesaml_account_ADD_COLUMN_signature_algorithm extends DoceboDbMigration {

	public function safeUp()
	{

		$this->addColumn('simplesaml_account', 'signature_algorithm', 'boolean NOT NULL DEFAULT 1');

		$this->getDbConnection()->createCommand()
			->update('simplesaml_account', array(
				'signature_algorithm' => 0
			));


		return true;
	}

	public function safeDown()
	{
		$this->dropColumn('simplesaml_account', 'signature_algorithm');
		return true;
	}
	
	
}
