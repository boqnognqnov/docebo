<?php

/**
 * Displays the saml settings page (shared between the global settings page and the multidomain page)
 */
class SamlSettings extends CWidget {

	/**
	 * @var SimplesamlAccount Instance of the saml account to use (passed at widget initialization)
	 */
	private $account;

	/**
	 * Id of the multidomain client
	 */
	public $id_multidomain;

	/**
	 * True if the widget should be rendered for OKTA (instead of standard SAML)
	 * @var boolean
	 */
	public $okta;

	/**
	 * Initialization method
	 *
	 * @throws CHttpException
	 * @throws Exception
	 */
	public function init() {
		// Check if we have a valid account
		if (isset($_POST['SimpleSamlAppSettingsForm']))
			$account = SimplesamlAccount::model()->findByPk($_POST['SimpleSamlAppSettingsForm']['simplesaml_id_account']);
		else
			$account = SimplesamlAccount::model()->findByAttributes(array('id_multidomain' => $this->id_multidomain));

		// If not, let's create it
		if(!$account) {
			$account = new SimplesamlAccount();
			$account->id_multidomain = $this->id_multidomain;
		}

		$this->account = $account;

		// If this widget is initialized after a form submit, let's trigger settings save
		if($_POST['SimpleSamlAppSettingsForm'])
			$this->saveSettings();

		/* @var $cs CClientScript */
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('SimpleSamlApp.assets')) .'/saml_settings.css');
	}


	/**
	 * Returns the URL to get the Service Provider metadata
	 */
	protected function getSPMetadataURL() {
		$serviceProviderId = 'default-sp';
		if($this->id_multidomain) {
			$client = CoreMultidomain::model()->findByPk($this->id_multidomain);
			if($client) {
				$account = SimplesamlAccount::model()->findByAttributes(array('id_multidomain' => $this->id_multidomain));
				if(!$account) {
					$account = new SimplesamlAccount();
					$account->id_multidomain = $this->id_multidomain;
				}

				$serviceProviderId = $account->getServiceProviderId();
				return (Yii::app()->request->getIsSecureConnection() ? 'https' : 'http') . "://" . $client->getClientUri(false) . '/lms/index.php?r=SimpleSamlApp/SimpleSamlApp/modules/saml/sp/metadata.php/'.$serviceProviderId;
			}
		}

		return Docebo::createAbsoluteUrl('lms:SimpleSamlApp/SimpleSamlApp/modules/saml/sp/metadata.php/'.$serviceProviderId);
	}

	/**
	 * Returns the parsed array of IdP metadata from the WS-Federation XML
	 * @param $xmlMetadata
	 */
	private function getParsedMetadata($xmlMetadata, $idpEntityId)
	{
		SimpleSAML_Utilities::validateXMLDocument($xmlMetadata, 'saml-meta');
		$entities = SimpleSAML_Metadata_SAMLParser::parseDescriptorsString($xmlMetadata);

		// Get all metadata for the entities.
		foreach ($entities as &$entity) {
			$entity = array(
				'shib13-sp-remote' => $entity->getMetadata1xSP(),
				'shib13-idp-remote' => $entity->getMetadata1xIdP(),
				'saml20-sp-remote' => $entity->getMetadata20SP(),
				'saml20-idp-remote' => $entity->getMetadata20IdP(),
			);
		}

		/* Transpose from $entities[entityid][type] to $output[type][entityid]. */
		$output = SimpleSAML_Utilities::transposeArray($entities);

		/* Merge all metadata of each type to a single string which should be
		 * added to the corresponding file. */
		foreach ($output as $type => &$entities) {
			foreach ($entities as $entityId => $entityMetadata) {
				if ($entityMetadata === NULL)
					continue;

				/* Remove the entityDescriptor element because it is unused, and only
				 * makes the output harder to read.
				 */
				unset($entityMetadata['entityDescriptor']);

				if (($type == 'saml20-idp-remote') && ($entityId == $idpEntityId))
					return $entityMetadata;
			}
		}

		return null;
	}

	/**
	 * Uploads the certificate and private key file using the storage system
	 * @return array The array of private key and certificate filename
	 */
	private function uploadCertificateFiles()
	{
		$result = array();

		// Save uploaded certiticate and pk files
		$privateKey = CUploadedFile::getInstanceByName('SimpleSamlAppSettingsForm[simplesaml_privatekey_file]');
		$certificate = CUploadedFile::getInstanceByName('SimpleSamlAppSettingsForm[simplesaml_certificate_file]');
		if ($privateKey && $certificate) {
			$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_SIMPLESAML);

			// Save private key to tmp dir
			$hash = Docebo::randomHash();
			$uploadTmpFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $hash.'.pem';
			if ($privateKey->saveAs($uploadTmpFilePath)) {
				// Now save this tmp file to storage
				if ($storageManager->store($uploadTmpFilePath))
					$result[] = $hash.'.pem';
				else
					throw new Exception("Unable to save private key in S3");
			} else
				throw new Exception("Error while uploading private key file");

			// Save certificate to tmp dir
			$uploadTmpFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $hash.'.crt';
			if ($certificate->saveAs($uploadTmpFilePath)) {
				// Now save this tmp file to storage
				if ($storageManager->store($uploadTmpFilePath))
					$result[] = $hash.'.crt';
				else
					throw new Exception("Unable to save certificate in S3");
			} else
				throw new Exception("Error while uploading certificate file");
		}

		return $result;
	}

	/**
	 * Removes the certificate files from S3
	 */
	private function resetCertificateFiles() {
		try {
			/* @var $storageManager CS3Storage */
			$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_SIMPLESAML);
			$storageManager->remove($this->account->privatekey_file);
			$storageManager->remove($this->account->certificate_file);
		} catch (Exception $e) {
			Yii::log($e->getMessage(), (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));
		}
	}

	/**
	 * Receives the HTTP post form and save them into an account object
	 */
	public function saveSettings() {
		try {
			$settings = new SimpleSamlAppSettingsForm();
			$settings->setAttributes($_POST['SimpleSamlAppSettingsForm']);
			if ($settings->validate()) {
				// Process xml federation metadata and transform it in json
				$metadata = $this->getParsedMetadata($settings->simplesaml_federation_metadata, $settings->simplesaml_idp);
				if (!$metadata)
					throw new CHttpException(500, "No valid metadata found for the passed IdP. Please check that Identity Provider ID and Entity ID in XML Metadata match.");

				// Should we user service provider (self-signed) certificate?
				if($this->okta)
					$settings->simplesaml_use_certificate = 1;

				// Check if metadata ask for redirect.signin
				if($metadata['redirect.sign'] && !$settings->simplesaml_use_certificate)
					throw new CHttpException(500, "Invalid metatada configuration. Your IdP asks for signing of authentication request, but you are not providing a private key and certificate to the configuration.");

				$this->account->idp = $settings->simplesaml_idp;
				$this->account->username_attribute = $settings->simplesaml_username_attribute;
				$this->account->idp_metadata = CJSON::encode($metadata);
				$this->account->federation_metadata = $settings->simplesaml_federation_metadata;
				$this->account->logout = $this->okta ? 0 : $settings->simplesaml_logout;
				$this->account->login_behavior = $settings->simplesaml_login_behavior;
				$this->account->sso_login_button = $settings->simplesaml_sso_login_button;
				$this->account->logout_url = $settings->simplesaml_logout_url;
				$this->account->user_provisioning = $settings->user_provisioning;
				$this->account->user_provisioning_update_if_exists = $settings->user_provisioning_update_if_exists;
				$this->account->signature_algorithm = $settings->signature_algorithm;

				$this->account->use_certificate = $settings->simplesaml_use_certificate;
				if ($settings->simplesaml_use_certificate) {
					list($private_key, $certificate) = $this->uploadCertificateFiles();
					if ($private_key && $certificate) {
						$this->account->privatekey_file = $private_key;
						$this->account->certificate_file = $certificate;
					}
				} else {
					$this->resetCertificateFiles();
					$this->account->privatekey_file = null;
					$this->account->certificate_file = null;
				}

				if(!empty($settings->saml_statements)){
					$this->account->saml_statements = $settings->saml_statements;
				}

				// Save model
				if(!$this->account->save()) {
					Yii::log(var_export($this->account->getErrors(),true), (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));
					throw new CHttpException( 500, CHtml::errorSummary( $this->account, NULL, NULL, array( 'class' => '' ) ) );
				}
				Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));

			} else
				throw new CHttpException(500, CHtml::errorSummary($settings, null, null, array('class' => '')));

		} catch (CHttpException $e) {
			Yii::log($e->getMessage(), (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));
			Yii::app()->user->setFlash('error', $e->getMessage());
		} catch (Exception $e) {
			Yii::log($e->getMessage(), (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));
			Yii::app()->user->setFlash('error', $e->getMessage());
		}
	}

	/**
	 * Render settings widget content
	 * @throws CException
	 */
	public function run() {
		// Prepare the settings form
		$settings = new SimpleSamlAppSettingsForm();
		$settings->is_okta = $this->okta;
		$settings->simplesaml_id_account = $this->account->id_account;
		$settings->simplesaml_idp = $this->account->idp;
		$settings->simplesaml_federation_metadata = $this->account->federation_metadata;
		$settings->simplesaml_privatekey_file = $this->account->privatekey_file;
		$settings->simplesaml_certificate_file = $this->account->certificate_file;
		$settings->simplesaml_use_certificate = $this->account->use_certificate || $this->okta;
		$settings->simplesaml_username_attribute = $this->account->username_attribute;
		$settings->simplesaml_logout = $this->account->logout;
		$settings->simplesaml_login_behavior = $this->account->login_behavior;
		$settings->simplesaml_sso_login_button = $this->account->sso_login_button;
		$settings->simplesaml_logout_url = $this->account->logout_url;
		$settings->user_provisioning = $this->account->user_provisioning;
		$settings->user_provisioning_update_if_exists = $this->account->user_provisioning_update_if_exists;
		$settings->saml_statements = $this->account->saml_statements;
		$settings->signature_algorithm = $this->account->signature_algorithm;

		// When the form is submitted/saved the SAML fields are still
		// in JSON string format. Decode them to array in this specific scenario
		if(!is_array($settings->saml_statements))
			$settings->saml_statements = CJSON::decode($settings->saml_statements);

		if(!$settings->saml_statements)
			$settings->saml_statements = array();

		if(!$settings->simplesaml_login_behavior){
			$settings->simplesaml_login_behavior = SimplesamlAccount::LOGIN_BEHAVIOR_STANDARD;
		}

		// Render the settings page
		$this->render('settings', array(
			'settings' => $settings
		));
	}
}