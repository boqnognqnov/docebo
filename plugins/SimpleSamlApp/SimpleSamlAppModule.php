<?php

class SimpleSamlAppModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'SimpleSamlApp.models.*',
			'SimpleSamlApp.components.*',
			'SimpleSamlApp.controllers.*',
			'SimpleSamlApp.widgets.*',
		));

        if(Yii::app() instanceof CWebApplication){
            // Add no CSRF route
            Yii::app()->request->noCsrfValidationRoutes[] = 'SimpleSamlApp/SimpleSamlApp/modules/saml/sp/metadata.php/[\S]*';
            Yii::app()->request->noCsrfValidationRoutes[] = 'SimpleSamlApp/SimpleSamlApp/modules/saml/sp/saml2-acs.php/[\S]*';
            Yii::app()->request->noCsrfValidationRoutes[] = 'site/sso'; // Used if someone tries to start SSO via HTTP POST
			Yii::app()->request->noCsrfValidationRoutes[] = 'SimpleSamlApp/SimpleSamlApp/modules/saml/sp/saml1-acs.php/[\S]*';
        }

        // temporary disable Yii autoloader
        spl_autoload_unregister(array('YiiBase','autoload'));

        // Load simplesaml API classes
        require_once(dirname(__FILE__).'/lib/_autoload.php');

        // enable Yii autoloader
        spl_autoload_register(array('YiiBase','autoload'));

        // Register the token based SSO handler
        Yii::app()->event->on('RegisterSSOHandlers', array($this, 'registerSsoHandler'));

        // Register the logout handler
        Yii::app()->event->on('UserLogout', array($this, 'logout'));

		// Register notification tag course_saml_link
		Yii::app()->event->on('NotificationShortcodesGet', array($this, 'addSSONotificationTags'));
		Yii::app()->event->on('ResolveNotificationTags', array($this, 'resolveCourseAndSessionSamlLink'));
		Yii::app()->event->on("RenderDomainBrandingTabs", array($this, 'renderDomainBrandingTabs'));
		Yii::app()->event->on("RenderDomainBrandingTabsContent", array($this, 'renderDomainBrandingTabsContent'));
		Yii::app()->event->on("DomainBrandingSettingsFormSubmit", array($this, 'saveSamlSettings'));

        if(Yii::app() instanceof CWebApplication){
            $account = SimplesamlAccount::resolveAccount();
        }

		if($account){
			// Raised after a SAML SSO succeeds and user's attributes
			// are retrieved from the SAML provider. Shortly after this event
			// the user is logged in to the LMS, if he exists, so it is safe
			// to assume that this is a "after SAML SSO + before LMS login" event
			Yii::app()->event->on('SimpleSamlParseUsername', array($this, 'onUserSamlSSOLoginSuccess'));

			// Override some core LMS functionalities (mainly on the login screen)
			// if the default login behavior is overriden in the SAML settings
			// screen (single domain SAML settings or multidomain settings).
			switch($account->getLoginBehavior()){
				case SimplesamlAccount::LOGIN_BEHAVIOR_REDIRECT:
					Yii::app()->event->on( 'onBeforeHomepage', array( $this, 'onBeforeHomepageRedirectToSAML' ) );
					break;
				case SimplesamlAccount::LOGIN_BEHAVIOR_STANDARD:default:
					if($account->sso_login_button){
						// Add the SSO button to all login pages (mobile, desktop)

						if( Yii::app()->name === 'mobile' ) {
							// Mobile version

							Yii::app()->event->on('afterMobileLoginPageSocialIcons', function(){
							    if(Yii::app() instanceof CWebApplication){
                                    Yii::app()->clientScript->registerCss('simplesaml-mobile-login', '
									.simplesaml-mobile-login{
										box-sizing: border-box;
										text-align: center;
										border: 0px none;
										position: relative;
										background-attachment: scroll;
										background-clip: border-box;
										font: bold 11px Arial;
										text-decoration: none;
										background-color: #fff;
										color: #333333;
										padding: 2px 6px 2px 6px;
										width: 100%;
										display: inline-block;
										margin-bottom: 7px;
									}
									.simplesaml-text{
										display: inline-block;
										color: #F5AF41;
										font-size: 15px;
										font-weight: 200;
										line-height: 41px;
										text-shadow: none;
									}
								');
                                    echo "
									<div class='row'>
										<a class='ui-link simplesaml-mobile-login'
											href='".SimpleSamlSsoHandler::getSamlSSOlink()."'>

											<span class='span-apps-logo'>
												<img src='/themes/spt/images/loginexternal/google-25.png' alt=''/>
											</span>
											<span class='simplesaml-text'>
												".Yii::t('simplesaml', 'Sign in with SAML')."
											</span>
										</a>
									</div>
								";
                                }
							});
						}else{
							// Desktop version
							// Layout 1, 2 or 3
							Yii::app()->event->on('afterSocialLoginIcons', function(){
							    if(Yii::app() instanceof CWebApplication){
                                    Yii::app()->clientScript->registerCss('saml-login-icon', '
								.header-login .saml-login-icon,
								#loginLayout .saml-login-icon{
									width: 24px;
									height: 24px;
									background-color: #F5AF41;
									color: #555 !important;
									margin-right: 10px;
									border-radius: 4px;
									text-align: center;
								}
								.saml-login-icon .fa-key{
									margin: 0;
									padding: 0;
									width: auto;
									height: auto;
									margin-top: 5px;
								}
								.header-login .saml-login-icon{
									margin-top: 20px;
								}
								
								/* Special overrides per-login layout */
								#loginFormBottom .saml-login-icon{
									margin-top: 4px;
								}
								#loginFormBottom .saml-login-icon a{
									color: #555 !important;
								}
								');

                                    echo '<div class="pull-left">
											<div class="saml-login-icon">
											<a title="SAML" class="" href="' . SimpleSamlSsoHandler::getSamlSSOlink() . '">
												<i class="fa fa-key"></i>
											</a>
											</div>
											&nbsp;&nbsp;&nbsp;
									</div>';
                                }
							});

							// Layout 4 (minimal layout)
							Yii::app()->event->on('onAfterMinimalLayoutSocialIcons', function(){
							    if(Yii::app() instanceof CWebApplication){
                                    echo "
									<div class='sso-login-btn google-btn'>
					                    <a title='Google Apps' href='".SimpleSamlSsoHandler::getSamlSSOlink()."'>
					                        <i class=\"fa fa-key\"></i>
					                        <span>
					                        ".Yii::t('simplesaml','Sign in with SAML SSO')."
					                        </span>
					                    </a>
					                </div>
				                ";
                                }
							});
						}
					}else{
						// nothing, no change from standard LMS behavior
					}
					break;

			}
		}

	}

	/**
	 * Adds the tab to the multidomain app
	 * @param $event DEvent
	 */
	public function renderDomainBrandingTabs($event) {
		$controller = new SimpleSamlAppController('simpleSamlApp', $this);
		$controller->renderMultidomainTab();
	}

	/**
	 * Renders the saml tab in multidomain client config
	 * @param $event DEvent
	 */
	public function renderDomainBrandingTabsContent($event) {
		$controller = new SimpleSamlAppController('simpleSamlApp', $this);
		$controller->renderMultidomainTabContent($event->params['multidomain_client_model']);
	}

	/**
	 * Saves multidomain client settings for saml
	 * @param $event
	 */
	public function saveSamlSettings($event) {
		$controller = new SimpleSamlAppController('simpleSamlApp', $this);
		$controller->saveMultidomainSettings($event->params['multidomain_client_model']);
	}

	/**
	 * Resolves the [course_sso_link] notification tag
	 * @param $event Devent
	 */
	public function resolveCourseAndSessionSamlLink($event) {
		$result = & $event->params['result'];

		/* @var $course LearningCourse */
		$course = $event->params['models']['courseModel'];

		/* @var $ltSession WebinarSession|LtCourseSession */
		$ltSession = $event->params['models']['sessionModel'];

		if($course) {
			$urlParams = array(
				'sso_type'  => 'saml',
				'id_course' => $course->idCourse
			);

			// Replace the value of the [course_saml_link] shortcode
			$result['[course_saml_link]'] = Yii::app()->createAbsoluteUrl( 'site/sso', $urlParams );

			if($ltSession){
				$urlParams['session_id'] = $ltSession->id_session;

				// Replace the value of the [session_saml_link] shortcode
				$result['[session_saml_link]'] = Yii::app()->createAbsoluteUrl( 'site/sso', $urlParams );
			}
		}
	}

	/**
	 * Register the [course_sso_link] notification tag
	 * @param $event Devent
	 */
	public function addSSONotificationTags($event) {
		$shortcodes = & $event->params['shortcodes'];
		$excludedNotifications = array(
			CoreNotification::NTYPE_COURSE_NOT_YET_COMPLETE,
			CoreNotification::NTYPE_WEBINAR_SESSION_DELETED
		);
		foreach ($shortcodes as $type => &$codeArr) {
			if (isset($codeArr['[course_link]'])) {
				$codeArr['[course_saml_link]'] = '[course_saml_link]';

				if(!in_array($type, $excludedNotifications)) {
					$codeArr['[session_saml_link]'] = '[session_saml_link]';
				}
			}
		}
	}

    /**
     * Register the standard token based SSO handler
     * @param $event
     */
    public function registerSsoHandler($event) {
        $event->params['map']['saml'] = new SimpleSamlSsoHandler();
    }

    /**
     * Logs the user out (if the flag is set)
     */
    public function logout($event) {
		$account = SimplesamlAccount::resolveAccount();


	    if($account){

		    // URL to redirect to after logout
		    $customAfterLogoutURL = false;

		    if($account->getLoginBehavior()==SimplesamlAccount::LOGIN_BEHAVIOR_REDIRECT){

			    if($account->logout_url) {
				    $customAfterLogoutURL = $account->logout_url;
				    if(stripos($customAfterLogoutURL, 'http')===false){
					    $customAfterLogoutURL = 'http://'.$customAfterLogoutURL;
				    }
			    }else{
				    $customAfterLogoutURL = Docebo::createLmsUrl('SimpleSamlApp/SimpleSamlApp/logoutThanks');
			    }
		    }

		    if($account->logout){
			    // When the setting is checked:
			    // "When the user clicks on logout, he will be logged out from the Identity Provider as well."
			    $handler = new SimpleSamlSsoHandler();
			    $handler->logout($customAfterLogoutURL);

			    if($customAfterLogoutURL){
				    Yii::app()->getController()->redirect($customAfterLogoutURL);
			    }
		    }else{
			    if($customAfterLogoutURL){
				    Yii::app()->getController()->redirect($customAfterLogoutURL);
			    }
		    }
	    }
    }

	/**
	 * Renders a SSO login link (only called when this login behavior
	 * is selected/enabled in the SAML settings screen) and some styles
	 * to make it better fit the layout
	 *
	 *@param \DEvent $event
	 *
	 * @throws \CException
	 */
	public function BeforeLoginFormRender ( DEvent $event ) {

		$samlSsoUrl = SimpleSamlSsoHandler::getSamlSSOlink();

		if( Yii::app()->name === 'mobile' ) {
			Yii::app()->clientScript->registerCss( 'saml-login-btn-mobile', '#saml-login-btn-mobile{ margin-bottom: 20px; }' );

			echo "<a id='saml-login-btn-mobile' href='{$samlSsoUrl}' class='btn-login ui-btn ui-shadow ui-corner-all'>" . Yii::t( 'gym', 'SAML SSO login' ) . "</a>";
		} else {

			// Is desktop layout

			if(Settings::get('catalog_external')=='on'){
				// External (anonymous users) catalog enabled
			}else{
				// Regular login layout (one of the three)

				$layout = Settings::get( 'login_layout' );

				if( $layout == 'layout1' ) { // side/right login layout
					Yii::app()->clientScript->registerCss( 'saml-login-btn', '
							#saml-login-btn{
								margin-right: 20px;
							}
							#rightLoginWrapper #mainCaptionRight{
								max-height: 150px;
							}
				' );
				}elseif($layout == 'layout2'){ // Top login layout
					Yii::app()->clientScript->registerCss( 'saml-login-btn', '
							#topLayout #loginFormTopContent{
								margin-right: 10px;
							}
							#saml-login-btn{
								margin-right: 10px;
							}
							#loginFormTop .btn#saml-login-btn{
								margin-top: 1px;
							}
				' );
				}elseif($layout=='layout3'){ // Bottom login layout
					Yii::app()->clientScript->registerCss( 'saml-login-btn', '
							#saml-login-btn{
								margin-left: 10px;
							}
							#bottomLayout #login_userid,
							#bottomLayout #login_pwd{
								max-width: 160px;
							}
				' );
				}

				echo "<a id='saml-login-btn' href='{$samlSsoUrl}' class='{$layout} btn btn btn-docebo green big pull-right saml-login-btn'>" . Yii::t( 'gym', 'SAML SSO login' ) . "</a>";
			}
		}
	}

	/**
	 * Similar to @see BeforeLoginFormRender(), but only valid for the
	 * "external catalog" (CatalogApp enabled and
	 * "Catalog visible for non-logged users" enabled)
	 *
	 * @param \DEvent $event
	 */
	public function ExternalCatalogAfterLoginForm(DEvent $event){
		Yii::app()->clientScript->registerCss( 'saml-login-btn', '
							#saml-login-btn{
								margin-top: 13px;
								margin-right: 6px;
								height: 32px;
								line-height: 32px;
							}
							.header-login-form #login_userid,
							.header-login-form #login_pwd{
								max-width: 90px;
							}
		' );

		$url = SimpleSamlSsoHandler::getSamlSSOlink();
		echo "<a id='saml-login-btn' href='{$url}' class='btn btn btn-docebo green big pull-right saml-login-btn'>" . Yii::t( 'gym', 'SAML SSO login' ) . "</a>";
	}

	public function onBeforeHomepageRedirectToSAML ( DEvent $event ) {
		if( Yii::app()->user->isGuest ) {
			if( isset( $_GET['staff.docebo'] ) || isset( $_GET['staff_docebo'] ) ) {
				// Do nothing, force stop of the external SAML SSO request
			} else {
				$samlConfig = SimplesamlAccount::resolveAccount();
				if( $samlConfig && $samlConfig->getLoginBehavior() == SimplesamlAccount::LOGIN_BEHAVIOR_REDIRECT ) {
					if(isset(Yii::app()->session['in_saml_sso_process']) && Yii::app()->session['in_saml_sso_process']){
						// We have already redirected to SAML SSO and
						// it redirected us back here without (not to ?r=site/sso)
						// which means it's probably badly configured
						// Stop redirecting again to SAML provider SSO page
						// to avoid infinite redirect
						Yii::app()->session['in_saml_sso_process'] = 0;
						Yii::app()->getController()->redirect(Yii::app()->createAbsoluteUrl('site/index', array('error' => 1)), true, 302, true);
					}

					if( ! isset( $_GET['error'] ) ) { // avoid infinite redirect in case of wrong configured SAML
						Yii::app()->session['in_saml_sso_process'] = 1;
						Yii::app()->getController()->redirect( SimpleSamlSsoHandler::getSamlSSOlink() );
					}
				}
			}
		}
	}

	/**
	 * This event handler is triggered after successful SSO from SAML
	 * and SAML returns a user ID (right before checking if this user exists
	 * in the LMS and before logging in that user to the LMS)
	 *
	 * @param \DEvent $event
	 */
	public function onUserSamlSSOLoginSuccess(DEvent $event){
		$username = $event->getParamValue('userid');

		$criteria = new CDbCriteria();
		$criteria->addCondition('userid=:userRel OR userid=:userAbsl');
		$criteria->params[':userRel'] = Yii::app()->user->getRelativeUsername($username);
		$criteria->params[':userAbsl'] = Yii::app()->user->getAbsoluteUsername($username);
		$user = CoreUser::model()->find($criteria);

		if($user){
			// If a super admin successfully logs in via SAML SSO,
			// allow him to modify the LMS login behavior settings afterwards
			// Otherwise, he may cause himself to be locked out if he enables
			// "Direct login" without configuring the SAML settings correctly first
			if(Yii::app()->user->getHighestLevel($user->idst) == Yii::app()->user->getGodadminLevelLabel()){
				$appName = '';
				if(PluginManager::isPluginActive('OktaApp')){
					$appName = 'OktaApp';
				}elseif(PluginManager::isPluginActive('SimpleSamlApp')){
					$appName = 'SimpleSamlApp';
				}

				if(PluginSettings::save('saml_godadmin_logged_in', 1, $appName)) {
					Yii::log('A godadmin logged in via SAML', CLogger::LEVEL_INFO);
				}
			}

			Yii::app()->session['in_saml_sso_process'] = 0;
		}
	}

	/**
	 * Checks that this app is correctly configured
	 */
	public function checkCorrectAppConfiguration() {
		try {
			$ssoHandler = new SimpleSamlSsoHandler();
			$ssoHandler->checkCorrectAppConfiguration();
			return true;
		} catch(Exception $e) {
			return false;
		}
	}

    /**
     * Returns the currently logged in user in ADFS (if any). Otherwise null.
     * @return CoreUser
     */
    public function getUser($return_url = null) {
        $user = null;

        $ssoHandler = new SimpleSamlSsoHandler();
        $userAttributes = $ssoHandler->getUserAttributes($return_url);
        if($userAttributes && !empty($userAttributes)) {
            $user = CoreUser::model()->find('userid = :userid', array(
                ':userid' => Yii::app()->user->getAbsoluteUsername($userAttributes['docebo_user']),
            ));
        }

        return $user;
    }

    /**
     * Gets the value of a specific attribute of the user (SAML claim)
     * @param $attributeName The name of attribute to search
     * @param null $return_url The return url after login (if session is expired)
     * @return attribute value or null
     */
    public function getUserAttribute($attributeName, $return_url = null) {
        $value = null;

        $ssoHandler = new SimpleSamlSsoHandler();
        $userAttributes = $ssoHandler->getUserAttributes($return_url);
        if(!empty($userAttributes) && isset($userAttributes[$attributeName]))
            $value = $userAttributes[$attributeName];

        return $value;
    }

    /**
     * Gets the array of attribute of the logged in user (SAML claims)
     * @param null $return_url The return url after login (if session is expired)
     * @return array attribute values
     */
    public function getUserAttributes($return_url = null) {
        $values = array();

        $ssoHandler = new SimpleSamlSsoHandler();
        $userAttributes = $ssoHandler->getUserAttributes($return_url);
        if(!empty($userAttributes))
            $values = $userAttributes;

        return $values;
    }

    /**
     * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
     */
    public function menu() {
		if(!PluginManager::isPluginActive("OktaApp")) {
			Yii::app()->mainmenu->addAppMenu('simpleSaml', array(
				'icon' => 'home-ico simplesaml',
				'app_icon' => 'fa-exchange', // icon used in the new menu
				'link' => Docebo::createAppUrl('admin:SimpleSamlApp/SimpleSamlApp/settings'),
				'label' => 'SAML '.Yii::t('standard', 'Settings'),
			), array());
		}
    }
}
