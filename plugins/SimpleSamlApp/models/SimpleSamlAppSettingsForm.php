<?php

/**
 * SimpleSamlAppSettingsForm class.
 *
 * @property string $simplesaml_idp The name of the Identity Provider to connect to
 */
class SimpleSamlAppSettingsForm extends CFormModel {

	/**
	 * True if current form is for OKTA
	 * @var boolean
	 */
	public $is_okta;

	/**
	 * @var integer The saml account to use
	 */
	public $simplesaml_id_account;

    /**
     * @var string The entity ID of the IdP this should SP should contact
     */
    public $simplesaml_idp;

    /**
     * @var boolean Should we use a certificate?
     */
    public $simplesaml_use_certificate;

    /**
     * @var string Private key file
     */
    public $simplesaml_privatekey_file;

    /**
     * @var string Certificate file (PEM)
     */
    public $simplesaml_certificate_file;

    /**
     * @var string  Federation metadata XML
     */
    public $simplesaml_federation_metadata;

    /**
     * @var string SAML attribute to be used as username in LMS login
     */
    public $simplesaml_username_attribute;

    /**
     * @var boolean Should we also logout from the Idp?
     */
    public $simplesaml_logout;

	/**
	 * @var
	 */
	public $simplesaml_login_behavior;

	/**
	 * @var
	 */
	public $simplesaml_sso_login_button;

	/**
	 * @var
	 */
	public $simplesaml_logout_url;

	/**
	 * @var boolean Should we enable "Just in time" user provisioning
	 */
	public $user_provisioning;

	/**
	 * @var boolean Only valid if $user_provisioning=true. Updates the existing
	 *              user during "Just in time" user provisioning/login
	 * @see SimpleSamlAppSettingsForm::$user_provisioning
	 */
	public $user_provisioning_update_if_exists;

	/**
	 * @var
	 * Encryption algorithm (SHA-1 vs SHA-256)
	 * SHA-256 is used by default for new configurations
	 */
	public $signature_algorithm;

	public $saml_statements = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
            array('simplesaml_idp, simplesaml_use_certificate, simplesaml_federation_metadata, simplesaml_username_attribute, simplesaml_logout, simplesaml_login_behavior, simplesaml_sso_login_button, simplesaml_logout_url, user_provisioning, user_provisioning_update_if_exists, saml_statements, signature_algorithm', 'safe'),
            array('simplesaml_idp, simplesaml_federation_metadata, simplesaml_username_attribute', 'required')
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array(
			'simplesaml_id_account' => Yii::t('webinar', 'Account'),
			'simplesaml_idp' => $this->is_okta ? Yii::t('okta', 'External key') : Yii::t('simplesaml', 'Identity Provider ID'),
            'simplesaml_use_certificate' => $this->is_okta ? Yii::t('okta', 'Service Provider Signing') : Yii::t('simplesaml', 'Enable Service Provider Certificate'),
            'simplesaml_privatekey_file' => Yii::t('simplesaml', 'Private Key file (PEM)'),
            'simplesaml_certificate_file' => Yii::t('simplesaml', 'Certificate file (CRT)'),
            'simplesaml_federation_metadata' => $this->is_okta ? Yii::t('okta', 'IDP Metadata') : Yii::t('simplesaml', 'XML Metadata'),
            'simplesaml_username_attribute' => Yii::t('simplesaml', 'Username attribute'),
            'simplesaml_logout' => Yii::t('simplesaml', 'Logout behaviour'),
			'simplesaml_login_behavior' => Yii::t('simplesaml', 'SSO behavior'),
			'simplesaml_logout_url'=>Yii::t('simplesaml', 'Logout URL'),
			'signature_algorithm'=>Yii::t('simplesaml', 'Signature Algorithm'),
		);
	}


	/**
	 * Returns the help text for Entity ID
	 * @return string
	 */
	public function getIdpHelpText() {
		if($this->is_okta)
			return "E.g.: http://www.okta.com/k1qwww5OZDAAERFVUQFQ";
		else
			return "E.g.: http://myadfs.domain.com/adfs/services/trust";
	}

	/**
	 * Returns the help text for Signature Algorithm
	 * @return string
	 */
	public function getSignatureAlgorithmHelpText() {
		return Yii::t('simplesaml', 'This algorithm has been deprecated since 2011');
	}

	/**
	 * Certificate help text
	 * @return string
	 */
	public function getCertificateHelpText() {
		if($this->is_okta)
			return Yii::t('okta', 'Upload a self-generated couple of private key/certificate PEM files');
		else
			return Yii::t('simplesaml', 'Some Identity Providers/Federations may require that Service Providers hold a certificate. If you enable a certificate for your Service Provider, it may be able to sign requests and response sent to the Identity Provider, as well as receiving encrypted re');
	}

	/**
	 * Returns metadata help text
	 */
	public function getMetadataHelpText() {
		if($this->is_okta)
			return "E.g.: email";
		else
			return "E.g.: http://schemas.microsoft.com/ws/2008/06/identity/claims/windowsaccountname";
	}
}
