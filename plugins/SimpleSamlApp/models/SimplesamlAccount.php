<?php

/**
 * This is the model class for table "simplesaml_account".
 *
 * The followings are the available columns in table 'simplesaml_account':
 * @property integer $id_account
 * @property integer $id_multidomain
 * @property string $federation_metadata
 * @property string $idp
 * @property string $idp_metadata
 * @property boolean $logout
 * @property string $login_behavior
 * @property boolean $sso_login_button
 * @property string $username_attribute
 * @property boolean $use_certificate
 * @property string $privatekey_file
 * @property string $certificate_file
 * @property boolean $user_provisioning
 * @property boolean $user_provisioning_update_if_exists
 * @property string $logout_url
 * @property array $saml_statements
 *
 * The followings are the available model relations:
 * @property CoreMultidomain $idMultidomain
 */
class SimplesamlAccount extends CActiveRecord
{
	/**
	 * The currently active account
	 * @var SimplesamlAccount
	 */
	private static $activeAccount;

	/**
	 * Possible values for: @see SimplesamlAccount::$login_behavior
	 */
	const LOGIN_BEHAVIOR_STANDARD = 'standard';
	const LOGIN_BEHAVIOR_REDIRECT = 'redirect';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'simplesaml_account';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('id_multidomain, sso_login_button', 'numerical', 'integerOnly'=>true),
			array('login_behavior', 'in', 'range'=>array(
				self::LOGIN_BEHAVIOR_REDIRECT,
				self::LOGIN_BEHAVIOR_STANDARD
			)),
			array('logout_url, sso_login_button', 'safe'),
			array('id_account, id_multidomain, federation_metadata, idp, logout, login_behavior, username_attribute, use_certificate, user_provisioning, user_provisioning_update_if_exists, saml_statements', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'idMultidomain' => array(self::BELONGS_TO, 'CoreMultidomain', 'id_multidomain'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_account' => 'Id Account',
			'id_multidomain' => 'Id Multidomain',
			'logout_url'=>'Logout URL',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('id_account',$this->id_account);
		$criteria->compare('id_multidomain',$this->id_multidomain);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SimplesamlAccount the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Returns the entity ID for the config file
	 */
	public function getEntityId() {
		if($this->id_multidomain) {
			$client = $this->idMultidomain;
			return (Yii::app()->request->getIsSecureConnection() ? 'https' : 'http')."://".$client->getClientUri(false).Yii::app()->request->getScriptUrl();
		} else
			return Yii::app()->request->getHostInfo().Yii::app()->request->getScriptUrl();
	}

	public function afterFind(){
		$this->saml_statements = CJSON::decode($this->saml_statements);
		return parent::afterFind();
	}
	public function beforeSave(){
		$this->saml_statements = CJSON::encode($this->saml_statements);
		return parent::beforeSave();
	}

	/**
	 * Returns an instance of this class, resolving the client and then finding the best account to use
	 *
	 * @return SimplesamlAccount
	 */
	public static function resolveAccount() {
		if(!self::$activeAccount) {

			// Check if we've been requested a SP explictly (i.e. when downloading SP metadata)
			$requestedResource = Yii::app()->request->getParam('r');
			if(strpos($requestedResource, "SimpleSamlApp/SimpleSamlApp/modules/saml/sp/metadata.php/default-sp") !== FALSE) {
				// Ok... We got a request to download metadata for a specifc SP
				if(strpos($requestedResource, "SimpleSamlApp/SimpleSamlApp/modules/saml/sp/metadata.php/default-sp-") !== FALSE) {
					$idMultidomain = substr($requestedResource, strlen("SimpleSamlApp/SimpleSamlApp/modules/saml/sp/metadata.php/default-sp-"));
					self::$activeAccount = self::model()->findByAttributes(array('id_multidomain' => $idMultidomain));
				}
			} else {
				// Trigger client resolution
				if (PluginManager::isPluginActive('MultidomainApp')) {
					$client = CoreMultidomain::resolveClient();
					if ($client)
						self::$activeAccount = self::model()->findByAttributes(array('id_multidomain' => $client->id));
				}
			}

			// If we are here, not account has been loaded. Return the default account
			if(!self::$activeAccount)
				self::$activeAccount = self::model()->findByAttributes(array('id_multidomain' => null));

			if(!isset(Yii::app()->session['saml_current_id_account']) || empty(Yii::app()->session['saml_current_id_account'])) {
				Yii::app()->session['saml_current_id_account'] = self::$activeAccount->id_account;
			}

			if(Yii::app()->session['saml_current_id_account'] != self::$activeAccount->id_account) {
				Yii::log("Using account " . self::$activeAccount->id_account . " for SAML SSO", (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));
				Yii::app()->session['saml_current_id_account'] = self::$activeAccount->id_account;
			}
		}

		return self::$activeAccount;
	}

	/**
	 * Returns the SP identifier
	 */
	public function getServiceProviderId() {
		if($this->id_multidomain)
			return "default-sp-".$this->id_multidomain;
		else
			return "default-sp";
	}

	public function getLoginBehavior(){
		if($this->login_behavior)
			return $this->login_behavior;
		else
			return self::LOGIN_BEHAVIOR_STANDARD;
	}
}