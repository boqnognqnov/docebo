<?php

class SimpleSamlAppController extends Controller
{


    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }


    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        $res = array();

        // keep it in the following order:

        // http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
        // Allow following actions to admin only:
        $admin_only_actions = array('settings', 'ldapFieldsAutocomplete', 'getHtmlForSamlField');
        $res[] = array(
            'allow',
            'roles' => array(Yii::app()->user->level_godadmin),
            'actions' => $admin_only_actions,
        );

        // deny admin only actions to all other users:
        $res[] = array(
            'deny',
            'actions' => $admin_only_actions,
        );

        $res[] = array(
            'allow',
            'users' => array('*'),
            'actions' => array('modules', 'logoutThanks')
        );

        // Allow access to other actions only to logged-in users:
        $res[] = array(
            'allow',
            'users' => array('@'),
        );

        $res[] = array(
            'deny', // deny all
            'users' => array('*'),
        );

        return $res;
    }

	public function actionLogoutThanks(){

		/**
		 * Let's not reinvent the wheel :-)
		 * We already have a layout that almost does the full job
		 */
		$this->layout = '//layouts/login_minimal';

		$this->render('logoutThanks');
	}

	/**
	 * Renders multidomain tab for saml
	 */
	public function renderMultidomainTab() {
		$this->renderPartial('_tab');
	}

	/**
	 * Renders multidomain client tab for saml
	 * @param $client CoreMultidomain
	 */
	public function renderMultidomainTabContent($client) {
		$this->renderPartial('_multidomain_tab', array('client' => $client));
	}

	/**
	 * @param $client CoreMultidomain
	 */
	public function saveMultidomainSettings($client) {
		$this->widget('SamlSettings', array('id_multidomain' => $client->id), true);
	}

    /**
     * Handler for module requests.
     *
     * This web page receives requests for web-pages hosted by modules, and directs them to
     * the RequestHandler in the module.
     *
     * @author Olav Morken, UNINETT AS.
     * @package simpleSAMLphp
     *
     * @version $Id$
     */
    public function actionModules()
    {
        require_once(dirname(__FILE__) . '/../www/_include.php');

        /* Index pages - filenames to attempt when accessing directories. */
        $indexFiles = array('index.php', 'index.html', 'index.htm', 'index.txt');

        /* MIME types - key is file extension, value is MIME type. */
        $mimeTypes = array(
            'bmp' => 'image/x-ms-bmp',
            'css' => 'text/css',
            'gif' => 'image/gif',
            'htm' => 'text/html',
            'html' => 'text/html',
            'shtml' => 'text/html',
            'ico' => 'image/vnd.microsoft.icon',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'js' => 'text/javascript',
            'pdf' => 'application/pdf',
            'png' => 'image/png',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',
            'swf' => 'application/x-shockwave-flash',
            'swfl' => 'application/x-shockwave-flash',
            'txt' => 'text/plain',
            'xht' => 'application/xhtml+xml',
            'xhtml' => 'application/xhtml+xml',
        );

        try {

            $url = Yii::app()->request->getParam('resource');
			if(!$url) {
				$r = Yii::app()->request->getParam('r');
				$url = substr($r, strlen('SimpleSamlApp/SimpleSamlApp/modules/'));
			}

            $modEnd = strpos($url, '/', 1);
            if ($modEnd === FALSE)
                /* The path must always be on the form /module/. */
                throw new SimpleSAML_Error_NotFound('The URL must at least contain a module name followed by a slash.');

            $module = substr($url, 0, $modEnd);
            $url = substr($url, $modEnd);
            if ($url === FALSE)
                $url = '';

            if (!SimpleSAML_Module::isModuleEnabled($module))
                throw new SimpleSAML_Error_NotFound('The module \''.$module.'\' was either not found, or wasn\'t enabled.');

            /* Make sure that the request isn't suspicious (contains references to current
             * directory or parent directory or anything like that. Searching for './' in the
             * URL will detect both '../' and './'. Searching for '\' will detect attempts to
             * use Windows-style paths.
             */
            if (strpos($url, '\\') !== FALSE)
                throw new SimpleSAML_Error_BadRequest('Requested URL contained a backslash.');
            elseif (strpos($url, './') !== FALSE)
                throw new SimpleSAML_Error_BadRequest('Requested URL contained \'./\'.');

            $moduleDir = SimpleSAML_Module::getModuleDir($module) . '/www/';

            /* Check for '.php/' in the path, the presence of which indicates that another php-script
             * should handle the request.
             */
            for ($phpPos = strpos($url, '.php/'); $phpPos !== FALSE; $phpPos = strpos($url, '.php/', $phpPos + 1)) {
                $newURL = substr($url, 0, $phpPos + 4);
                $param = substr($url, $phpPos + 4);
                if (is_file($moduleDir . $newURL)) {
                    /* $newPath points to a normal file. Point execution to that file, and
                     * save the remainder of the path in PATH_INFO.
                     */
                    $url = $newURL;
                    $_SERVER['PATH_INFO'] = $param;
                    break;
                }
            }

            $path = $moduleDir . $url;
            if ($path[strlen($path) - 1] === '/') {
                /* Path ends with a slash - directory reference. Attempt to find index file
                 * in directory.
                 */
                foreach ($indexFiles as $if) {
                    if (file_exists($path . $if)) {
                        $path .= $if;
                        break;
                    }
                }
            }

            if (is_dir($path)) {
                /* Path is a directory - maybe no index file was found in the previous step, or
                 * maybe the path didn't end with a slash. Either way, we don't do directory
                 * listings.
                 */
                throw new SimpleSAML_Error_NotFound('Directory listing not available.');
            }

            if (!file_exists($path)) {
                /* File not found. */
                SimpleSAML_Logger::info('Could not find file \'' . $path . '\'.');
                throw new SimpleSAML_Error_NotFound('The URL wasn\'t found in the module.');
            }

            if (preg_match('#\.php$#D', $path)) {
                /* PHP file - attempt to run it. */
                $_SERVER['SCRIPT_NAME'] .= '?r=SimpleSamlApp/SimpleSamlApp/modules/' . $module . $url;
                require($path);
                Yii::app()->end();
            }

            /* Some other file type - attempt to serve it. */

            /* Find MIME type for file, based on extension. */
            $contentType = NULL;
            if (preg_match('#\.([^/\.]+)$#D', $path, $type)) {
                $type = strtolower($type[1]);
                if (array_key_exists($type, $mimeTypes)) {
                    $contentType = $mimeTypes[$type];
                }
            }

            if ($contentType === NULL) {
                /* We were unable to determine the MIME type from the file extension. Fall back
                 * to mime_content_type (if it exists).
                 */
                if (function_exists('mime_content_type')) {
                    $contentType = mime_content_type($path);
                } else {
                    /* mime_content_type doesn't exist. Return a default MIME type. */
                    SimpleSAML_Logger::warning('Unable to determine mime content type of file: ' . $path);
                    $contentType = 'application/octet-stream';
                }
            }

            $contentLength = sprintf('%u', filesize($path)); /* Force filesize to an unsigned number. */

            header('Content-Type: ' . $contentType);
            header('Content-Length: ' . $contentLength);
            header('Cache-Control: public,max-age=86400');
            header('Expires: ' . gmdate('D, j M Y H:i:s \G\M\T', time() + 10 * 60));
            header('Last-Modified: ' . gmdate('D, j M Y H:i:s \G\M\T', filemtime($path)));

            readfile($path);
            Yii::app()->end();

        } catch (Exception $e) {
            Yii::log($e->getMessage(), (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));
        }

        // If we get here, something wrong happened and we can't throw an exception!!
        // Just redirect to the homepage
        $this->redirect(Docebo::createAbsoluteUrl("lms:site/index", array('error' => 1)));
    }

    /**
     * Settings action (configures params needed for the SAML connection
     */
    public function actionSettings()
    {
		$cs = Yii::app()->getClientScript();
		$themeUrl = Yii::app()->theme->baseUrl;

		// FCBK
		$cs->registerCssFile($themeUrl . '/css/fcbkcomplete.css');
		$cs->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js');

		// Render the view with the widget settings form
		$this->render('index');
    }



	public function actionLdapFieldsAutocomplete($tag = null){
		$arr = SimpleSamlFieldsHelper::getLdapFields();

		echo CJSON::encode(array(
			'options'=>$arr
		));

	}

	public function actionGetHtmlForSamlField($field = null){
		echo SimpleSamlFieldsHelper::getHtmlForSamlField($field);
	}



}