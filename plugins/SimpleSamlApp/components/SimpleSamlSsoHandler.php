<?php

class SimpleSamlSsoHandler extends CComponent implements ISsoHandler {

    /**
     * Checks that simplesaml has been correctly configured
     */
    public function checkCorrectAppConfiguration() {
		// Load saml account
		$account = SimplesamlAccount::resolveAccount();
		if(!$account)
			throw new Exception("Invalid SAML configuration");

        $idpEntity = $account->idp;
        $useCertificate = $account->use_certificate;
        $privateKey = $account->privatekey_file;
        $certificate = $account->certificate_file;
        $metadata = $account->idp_metadata;
        $usernameAttribute = $account->username_attribute;

        if(!$idpEntity || ($useCertificate && (!$privateKey || !$certificate)) || !$metadata || !$usernameAttribute)
            throw new Exception("Invalid SAML configuration");
    }

	static public function afterLogout(){

	}

    /**
     * Logs out the user from the IdP
     */
    public function logout($logoutUrl = null) {
        // Load simplesaml API classes
		$account = SimplesamlAccount::resolveAccount();
        $auth = new SimpleSAML_Auth_Simple($account->getServiceProviderId());
        if($auth->isAuthenticated()) {
	        if(!$logoutUrl){
		        $logoutUrl = Docebo::getRelativeUrl('.');
	        }
            $auth->logout($logoutUrl);
	        Yii::app()->getController()->redirect($logoutUrl);
            // This function never returns
        }
    }

    /**
     * Checks with the SAML IdP if there is already a user logged in
     * @return string Userid of the logged in user
     * @throws Exception in case something is wrong and SSO cannot be performed
     */
    public function login() {

		Yii::log('Initiating SAML SSO', (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));

        $userid = null;

        // Check app configuration
        $this->checkCorrectAppConfiguration();

        // Load simplesaml API classes
		$account = SimplesamlAccount::resolveAccount();
        $auth = new SimpleSAML_Auth_Simple($account->getServiceProviderId());

        // Is this an error?
        if (array_key_exists(SimpleSAML_Auth_State::EXCEPTION_PARAM, $_REQUEST)) {
            $state = SimpleSAML_Auth_State::loadExceptionState();
            $e = $state[SimpleSAML_Auth_State::EXCEPTION_DATA];
            $error_message = "Exception during login:\n";
            $error_message .= implode("\n", $e->format());
            throw new Exception($error_message);
        }

        // Check if we have a user currently authenticated
        if (!$auth->isAuthenticated()) {

			Yii::log('SAML user not authenticated yet. Authenticating', (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));

			$id_course      = Yii::app()->request->getParam('id_course', 0);
			$destination    = Yii::app()->request->getParam('destination', '');

			$idSession = Yii::app()->request->getParam('session_id', 0);

            // Not yet authenticated -> prepare return to URL and start authentication process
			$params = array('sso_type' => 'saml');
			if($id_course)
				$params['id_course'] = $id_course;
			if($destination)
				$params['destination'] = $destination;
			if($idSession){
				$params['session_id'] = $idSession;
			}

			// Some SAML providers (e.g. OKTA) don't automatically
			// pass back the additional parameters we gave them but
			// instead always redirect to a fixed ReturnTo URL defined
			// in the provider's dashboard. So preserve the dynamic $_GET
			// parameters in session for reading later
			Yii::app()->session['saml_redirect_params'] = $params;

            $return_url = Docebo::createAbsoluteUrl('site/sso', $params);

			Yii::log('Authenticating and setting redirect URL to '.$return_url, (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));

			Yii::app()->session['lasturl'] = $this->getLandingURL();

            $auth->login(array('ReturnTo' => $return_url, 'ErrorURL' => $return_url, 'RelayState'=>$return_url));
        } else {

			Yii::log('User is authenticated in SAML. Getting attributes...', (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));

            // User is authenticated -> we need to get his information and return the corresponding userid in Docebo
			$account = SimplesamlAccount::resolveAccount();
            $usernameAttribute = $account->username_attribute;
            $samlUserAttributes = $auth->getAttributes();
			Yii::log("SAML returned these claims: ".print_r($samlUserAttributes, true), (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));
            if (!isset($samlUserAttributes[$usernameAttribute][0]))
                throw new Exception('Username attribute missing from SAML user record.');

            $userid = $samlUserAttributes[$usernameAttribute][0];

            // Trigger event to let customer apps parse the userid and (possibly) change it
            Yii::app()->event->raise('SimpleSamlParseUsername', new DEvent($this, array('userid' => &$userid)));

			if($account->user_provisioning){
				// "Just in time" user provisioning enabled

				$criteria = new CDbCriteria();
				$criteria->addCondition('userid=:userRel OR userid=:userAbsl');
				$criteria->params[':userRel'] = Yii::app()->user->getRelativeUsername($userid);
				$criteria->params[':userAbsl'] = Yii::app()->user->getAbsoluteUsername($userid);
				$user = CoreUser::model()->find($criteria);

				if($user){
					if($account->user_provisioning_update_if_exists){
						$this->updateExistingUser($user, $account, $samlUserAttributes);
					}
				}else{
					$this->createNewUser($userid, $account, $samlUserAttributes);
				}
			}
        }

        return $userid;
    }

	protected function createNewUser($userid, $account, $samlUserAttributes){

		/**
		 * Associative array with:
		 * [field key]=>[custom saml attribute statement from
		 * the SAML app settings page]
		 *
		 * @see SimpleSamlFieldsHelper::getLdapFields() for the possible [field key]s
		 */
		$samlStatementsMap = $account->saml_statements;

		Yii::log('SAML user provisioning enabled. Creating user "just in time". Importing '.count($samlStatementsMap). ' SAML statements', (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));

		$user = new CoreUser('create');
		$user->unsetAttributes();

		$data = $this->_populateUserModelWithStatements($user, $samlStatementsMap, $samlUserAttributes);

		// This is a special field.
		// It may have been "post processed" by an event so use the
		// $userid variable instead of the incoming SAML statement value
		$user->userid = Yii::app()->user->getAbsoluteUsername($userid);

		// Set default values for model
		$user->valid = 1;
		$user->force_change = 0;
		$generatedPassword = 	chr(mt_rand(65, 90))
								. chr(mt_rand(97, 122))
								. mt_rand(0, 9)
								. chr(mt_rand(97, 122))
								. chr(mt_rand(97, 122))
								. chr(mt_rand(97, 122))
								. mt_rand(0, 9)
								. chr(mt_rand(97, 122));
		$user->new_password = $generatedPassword;
		$user->new_password_repeat = $generatedPassword;

		$user->runGroupAutoassign = true;

		Yii::log('Saving SAML provisioned user with attributes: '.print_r($user->attributes, 1), CLogger::LEVEL_ERROR);

		if($user->save()){
			// Set the user's language to the default one
			$user->resetUserLanguage();

			// Assign this user to the student level group
			try {
				$auth = Yii::app()->authManager;
				$auth->assign( Yii::app()->user->level_user, $user->idst );
			}catch(Exception $e){
				// Already a user level? Nothing to do
			}

			$user->saveChartGroups();

			if(isset($data['additionalFields'])){
				$user->new_password = null;
				$user->setScenario('selfEditProfile');
				$user->setAdditionalFieldValues($data['additionalFields']);

				if(!$user->save(false)){
					Yii::log('Can not save additional fields: '. print_r($data['additionalFields'], 1), (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));
					Yii::log(print_r($user->getErrors(), 1), (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));
				}
			}

			Yii::app()->event->raise('NewUserCreated', new DEvent($this, array('user' => $user)));
			return TRUE;
		}else{
			Yii::log('Error creating SAML user provisioned user' . print_r($user->getErrors(),1), CLogger::LEVEL_ERROR);
		}

		return FALSE;
	}

	protected function updateExistingUser(CoreUser $user, SimplesamlAccount $account, $samlUserAttributes){
		/**
		 * Associative array with:
		 * [field key]=>[custom saml attribute statement from
		 * the SAML app settings page]
		 *
		 * @see SimpleSamlFieldsHelper::getLdapFields() for the possible [field key]s
		 */
		$samlStatementsMap = $account->saml_statements;

		Yii::log('Updating SAML user with '.count($samlStatementsMap).' provisioned fields', (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));

		$user->scenario = 'update';
		$data = $this->_populateUserModelWithStatements($user, $samlStatementsMap, $samlUserAttributes);

		if($user->save(false)){
			$user->saveChartGroups();

			if(isset($data['additionalFields'])){
				$user->new_password = null;
				$user->setScenario('selfEditProfile');
				$user->setAdditionalFieldValues($data['additionalFields']);

				if(!$user->save(false)){
					Yii::log('Can not save additional fields: '. print_r($data['additionalFields'], 1), (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));
					Yii::log(print_r($user->getErrors(), 1), (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));
				}
			}

			return TRUE;
		}else{
			Yii::log('Error updating SAML user provisioned user' . print_r($user->getErrors(),1), (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));
		}
	}

	/**
	 * A simple reusable method for both user creation and updating
	 *
	 * @param \CoreUser $user
	 * @param           $statements array Incoming SAML statements and values
	 * @param           $attributes array
	 */
	protected function _populateUserModelWithStatements(CoreUser &$user, $statements, $attributes){
		$additionalFieldValues = array();

		$baseRootBranch = $rootBranch = CoreOrgChartTree::model()->getOrgRootNode();

		$userBranchProvided = false;


		// Loop all SAML statements/fields from the SAML settings page
		// and populate a new user's fields and then create him in the platform
		if(is_array($statements) && !empty($statements))
		{
			foreach($statements as $fieldName=>$statementName){

				// If that SAML statement/field is provided by the endpoint
				if(isset($attributes[$statementName][0])){

					$incomingValue = $attributes[$statementName][0];

					Yii::log('Statement name: '.$statementName.' Value is:'.print_r($incomingValue,1), (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));

					// Validate if the incomping value is supported by Docebo
					$found = false;
					foreach(SimpleSamlFieldsHelper::getLdapFields() as $field){
						if($field['key']==$fieldName){
							$found = true;
						}
					}
					if(!$found){
						// Unsupported
						Yii::log('Statement '.$statementName.' not found in SAML', (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));
						continue;
					}

					if($user->hasAttribute($fieldName)){
						Yii::log('Populating user field '.$fieldName.' with value '.$incomingValue, (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));

						// Check if this is a standard (CoreUser) field
						// and populate it in the newly created user model
						$user->$fieldName = $incomingValue;
					}else{
						Yii::log('Processing non-user field named: '.$fieldName, (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));

						switch($fieldName){
							case 'branch_code':
								Yii::log('Provided branch code: '.$incomingValue, (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));

								// Check if branch name is valid, in current LMS language
								$branch = CoreOrgChartTree::model()->findByAttributes(array('code' => $incomingValue));

								if (!$branch) {
									Yii::log('Branch with this code does not exist: ' . $incomingValue, CLogger::LEVEL_ERROR);
									continue;
								}else {
									// Ok, we found the branch, ask the code below to use it
									$user->chartGroups = array((int)$branch->idOrg);
									$userBranchProvided = true;
								}
								break;
							case 'branch_name':
								Yii::log('Provided branch name: '.$incomingValue, (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));

								// Check if branch name is valid, in current LMS language
								$branch = CoreOrgChart::model()->findByAttributes(array(
									'translation' => $incomingValue,
									'lang_code'=>Settings::get('default_language', 'english')
								));

								if (!$branch) {
									Yii::log('Branch with this name does not exist: ' . $incomingValue, CLogger::LEVEL_ERROR);
									continue;
								}else {
									// Ok, we found the branch, ask the code below to use it
									$user->chartGroups = array((int)$branch->id_dir);
									$userBranchProvided = true;
								}
								break;
							default:
								if(is_numeric($fieldName)){
									// This is most likely an additional field
                                    $idField = (int) $fieldName;
									$additionalFieldValues[$idField] = array('value' => $incomingValue);
								}else{
									Yii::log($fieldName.' is an unknown field for SAML import', CLogger::LEVEL_ERROR);
								}
						}
					}

				}
			}
		}

		Yii::log('Setting additional fields: '.print_r($additionalFieldValues,1), (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));

		// If no branch provided for user, associate him with Root
		if(!$userBranchProvided){
			$user->chartGroups = $user->getOrgChartGroupsList();

			if (PluginManager::isPluginActive('MultidomainApp')) {
				// Associate with root branch of current Multidomain environment
				$multidomainClientModel = CoreMultidomain::resolveClient();

				if($multidomainClientModel){
					$multidomainClientOrgChart = $multidomainClientModel->org_chart;

					if($multidomainClientOrgChart){
						$rootBranch = CoreOrgChartTree::model()->findByPk($multidomainClientOrgChart);
					}
				}

				if(!$rootBranch){
					throw new CException('Multidomain root branch not found');
				}
			}

			if(!$rootBranch){
				throw new CException('No root branch found to associate user with');
			}
		}

		$user->chartGroups[] = (int)$baseRootBranch->idOrg;
		if((int)$baseRootBranch->idOrg != (int)$rootBranch->idOrg)
			$user->chartGroups[] = (int)$rootBranch->idOrg;

		return array(
			'additionalFields'=>$additionalFieldValues,
		);
	}

    /**
     * Returns the array of user attributes (or null) for the currently logged in user in ADFS
     * @param $return_url Return url to pass to the Idp, after renewing the token
     * @return array
     */
    public function getUserAttributes($return_url = null) {
        $user = null;

        try {
            // Check app configuration
            $this->checkCorrectAppConfiguration();

            // Load simplesaml API classes
			$account = SimplesamlAccount::resolveAccount();
            $auth = new SimpleSAML_Auth_Simple($account->getServiceProviderId());

            // Check if we have a user currently authenticated
            if ($auth->isAuthenticated()) {
				$account = SimplesamlAccount::resolveAccount();
                $usernameAttribute = $account->username_attribute;
                $attributes = $auth->getAttributes();
				Yii::log("SAML returned these claims: ".print_r($attributes, true), (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));
                if (!isset($attributes[$usernameAttribute][0]))
                    throw new Exception('Username attribute missing from SAML user record.');

                $userid = $attributes[$usernameAttribute][0];
                // Trigger event to let customer apps parse the userid and (possibly) change it
                Yii::app()->event->raise('SimpleSamlParseUsername', new DEvent($this, array('userid' => &$userid)));

                $attributes['docebo_user'] = $userid;
                $user = $attributes;
            } else if($return_url) {
                // Is this an error?
                if (array_key_exists(SimpleSAML_Auth_State::EXCEPTION_PARAM, $_REQUEST)) {
                    $state = SimpleSAML_Auth_State::loadExceptionState();
                    $e = $state[SimpleSAML_Auth_State::EXCEPTION_DATA];
                    $error_message = "Exception during login:\n";
                    $error_message .= implode("\n", $e->format());
                    throw new Exception($error_message);
                }

                // Not yet authenticated -> prepare return to URL and start authentication process
                $auth->login(array('ReturnTo' => $return_url, 'ErrorURL' => $return_url));
            }

        } catch(Exception $e) {
            Yii::log($e->getMessage(), (Settings::get('simplesaml_logging', 'off') === 'on' ? CLogger::LEVEL_WARNING : CLogger::LEVEL_INFO));
        }

        return $user;
    }

    /**
     * Returns the URL to redirect to, after a successful LMS login
     * @return string URL to redirect to
     */
    public function getLandingURL() {
        // Check passed GET params to calculate the landing page after sso
        $id_course      = Yii::app()->request->getParam('id_course', 0);
        $destination    = Yii::app()->request->getParam('destination', '');

		// Optional
		$idSession = Yii::app()->request->getParam('session_id', 0);

		// Restore stuff from session if they were set before the redirect to the SAML provider
		$sessionParams = Yii::app()->session['saml_redirect_params'];
		if(is_array($sessionParams)){
			if(!$id_course && isset($sessionParams['id_course'])){
				$id_course = $sessionParams['id_course'];
			}
			if(!$destination && isset($sessionParams['destination'])){
				$destination = $sessionParams['destination'];
			}
			if(!$idSession && isset($sessionParams['session_id'])){
				$idSession = $sessionParams['session_id'];
			}
		}

        $redirect_url = null;

        // *** HOOK FOR PLUGINS ***
        $loginEvent = new DEvent($this, array('redirect_url' => &$redirect_url));
        Yii::app()->event->raise('ActionSsoLogin', $loginEvent);
        if(!$redirect_url) {
            $redirect_url = Docebo::getUserAfetrLoginRedirectUrl();
            if($id_course) {
				if($idSession){

					$courseType = LearningCourse::model()->findByAttributes(array(
						'idCourse' => $id_course
					));

					switch($courseType->course_type){
						case LearningCourse::TYPE_CLASSROOM:
							$redirect_url = Docebo::createAbsoluteLmsUrl('player/training/session', array('course_id' => $id_course, 'session_id' => $idSession));
							break;
						case LearningCourse::TYPE_WEBINAR:
							$redirect_url = Docebo::createAbsoluteLmsUrl('player/training/webinarSession', array('course_id' => $id_course, 'session_id' => $idSession));
							break;
					}
					
				}else{
					$redirect_url = Docebo::createAbsoluteUrl( 'player', array( 'course_id' => $id_course ) );
				}
			}else {
				switch($destination) {
					case 'mycourses':
						$redirect_url = Docebo::createAbsoluteUrl('site/index', array('opt' => 'fullmycourses'));
						break;
					case 'catalog':
						$redirect_url = Docebo::createAbsoluteUrl('site/index', array('opt' => 'catalog'));
						break;
					case 'learningplan':
						$redirect_url = Docebo::createAbsoluteUrl('curricula/show', array('sop' => 'unregistercourse'));
						break;
					case 'myactivities':
						$redirect_url = Docebo::createAbsoluteUrl('myActivities/index');
						break;
					case 'mycalendar':
						$redirect_url = Docebo::createAbsoluteUrl('mycalendar/show');
						break;
				}
			}
        }

        return $redirect_url;
    }

	static public function getSamlSSOlink(){
		return Docebo::createLmsUrl( 'site/sso', array( 'sso_type' => 'saml' ) );
	}
}