<div class="row-fluid saml-fields-row" data-saml-field="<?= $field['key'] ?>">
	<div class="span3">
		<?= $field['value'] ?>
	</div>
	<div class="span3">
		<? if( $field['is_standard'] ): ?>
			<?= Yii::t( 'standard', 'Text field' ) ?>
		<? else: ?>
			<?=Yii::t('field', '_' . strtoupper($field['type']))?>
		<? endif; ?>
	</div>
	<div class="span6">
		<div class="pull-right" style="margin-top:3px;">
			<a href="#" class="remove-saml-field">
				<span class="i-sprite is-remove red"></span>
			</a>
		</div>
		<? $value = NULL;
		// Try to retrieve the statement value from the existing form and populate the input
		foreach ( $model->saml_statements as $key => $fieldValue ) {
			if( $key == $field['key'] ) {
				$value = $fieldValue;
			}
		} ?>
		<input type="text" style="width:90%;"
			   value="<?= $value ?>"
			   name="SimpleSamlAppSettingsForm[saml_statements][<?= $field['key'] ?>]"/>
	</div>
</div>