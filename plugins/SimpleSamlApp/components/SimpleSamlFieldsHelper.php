<?php

/**
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */
class SimpleSamlFieldsHelper {

	/**
	 * This has been abstracted in its own function because
	 * it's used by multiple places with and without the $formModel parameter
	 *
	 * @param      $field
	 *
	 * @param null $formModel
	 *
	 * @return string
	 * @throws \CException
	 */
	static public function getHtmlForSamlField ( $field, $formModel = NULL ) {
		$fields = self::getLdapFields();

		$fieldObject = NULL;

		foreach ( $fields as $f ) {
			if( $f['key'] == $field ) {
				$fieldObject = $f;
				break;
			}
		}

		if( ! $formModel ) {
			$formModel = new SimpleSamlAppSettingsForm();
		}

		return Yii::app()->getController()->renderPartial( 'SimpleSamlApp.components.views._single_saml_field', array(
			'field' => $fieldObject,
			'model' => $formModel,
		), TRUE );
	}

	static public function getLdapFields () {
		$arr = array();

		$allowedStandardFields = array(
			'userid',
			'firstname',
			'lastname',
			'email',
			'signature',
			'facebook_id',
			'twitter_id',
			'linkedin_id',
			'google_id',
		);

		$standardModel = CoreUser::model();
		foreach ( $standardModel->attributeLabels() as $attr => $label ) {
			if(in_array($attr, $allowedStandardFields)) {
				$arr[] = array(
					'is_standard' => TRUE,
					'key'         => $attr,
					'value'       => $label,
				);
			}
		}

		$arr[] = array(
			'is_standard'=>true,
			'key'=>'branch_code',
			'value'=>ucfirst(Yii::t('standard', '_ORGCHART')) . ' ' . Yii::t('standard', '_CODE'),
		);
		$arr[] = array(
			'is_standard'=>true,
			'key'=>'branch_name',
			'value'=>ucfirst(Yii::t('standard', '_ORGCHART')) . ' ' . Yii::t('standard', '_NAME'),
		);

        $fieldsCollection = CoreUserField::model()->language()->findAll();

		foreach ($fieldsCollection as $additionalField) {
            /** @var CoreUserField $additionalField */
            $arr[] = [
				'is_standard' => false,
				'key'   => $additionalField->getFieldId(),
				'value' => $additionalField->getTranslation(Lang::getCodeByBrowserCode(Yii::app()->getLanguage())),
				'type'  => $additionalField->getFieldType(),
            ];
		}

		// Could raise an event to collect more fields here

		return $arr;
	}
}