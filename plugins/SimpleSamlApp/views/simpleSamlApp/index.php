<?php
/* @var $this SamlSettings */
?>

<a href="<?= ('it' == Yii::app()->getLanguage()) ? 'https://www.docebo.com/it/knowledge-base/saml/' : 'https://www.docebo.com/knowledge-base/saml/' ?>" target="_blank" class="app-link-read-manual"><?= Yii::t('apps', 'Read Manual'); ?></a>
<h2>SAML 2.0 - <?=Yii::t('standard','Settings'); ?></h2>
<br>

<?php $this->widget('SamlSettings') ?>