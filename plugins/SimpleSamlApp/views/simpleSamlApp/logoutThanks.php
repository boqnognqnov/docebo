<?
$img = Yii::app()->assetManager->publish(Yii::getPathOfAlias('SimpleSamlApp.assets')) . '/img/background.jpg';
?>
<style>
	#register-modal-btn{
		display: none;
	}
	#after-logout-message {
		width: 320px;
		max-height: 180px;
		position: absolute;
		top: 200px;
		left: 0;
		right: 0;
		margin: auto;
		padding: 25px 50px;
		color: #555555;
		background: #fcfcfc;
		background: rgba(252, 252, 252, 0.9);
	}

	#after-logout-message h2 {
		text-align: center;
		padding: 0;
		margin: 0 0 15px;
		color: #555555;
		font-weight: normal;
	}

	#after-logout-message a,
	#after-logout-message a:hover {
		line-height: 27px;
		font-weight: 600;
		font-size: 14px;
		text-transform: uppercase;
		color: #FFFFFF;
		background: #5EBE5E;
		border-radius: 0;
		border-color: #5EBE5E;
	}
</style>
<div style="width:100%; height:100%; position: relative;">

	<img src="<?= $img ?>" style="position: absolute; width: 100%; height: 100%;"/>

	<div class="content-wrapper">
		<div id="after-logout-message">
			<br/>
			<h2><?= Yii::t('simplesaml', 'Your logout was successful') ?></h2>

			<p class="text-centers">
				<?= Yii::t('simplesaml', 'You can now re-enter inside the LMS or close this window') ?>
			</p>

			<div>
				<a class="btn btn-primary btn-lg btn-block btn-docebo green" href="<?=SimpleSamlSsoHandler::getSamlSSOlink()?>"><?=Yii::t('simplesaml', 'Re-login')?></a>
			</div>

			<div class="clearfix"></div>

			<br/>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		var d = new Date();
		$("a[href*='site/sso']").each(function()
		{
			var $this = $(this);
			var _href = $this.attr("href");
			$this.attr("href", _href + '&offset='+(-d.getTimezoneOffset()*60));
		});
	});
</script>