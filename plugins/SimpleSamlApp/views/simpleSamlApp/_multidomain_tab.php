<?php /* @var $client CoreMultidomain */ ?>
<?php /* @var $this SimpleSamlAppController */ ?>

<div class="tab-pane" id="simplesaml-tab">
	<h1>SAML 2.0 - <?=Yii::t('standard','Settings'); ?></h1>
	<?php $this->widget('SamlSettings', array('id_multidomain' => $client->id)); ?>
</div>