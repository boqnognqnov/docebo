<?php

class ApiAppModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'ApiApp.models.*',
			'ApiApp.components.*',
		));

        // Register the token based SSO handler
        Yii::app()->event->on('RegisterSSOHandlers', array($this, 'registerTokenSsoHandler'));
	}

    /**
     * Register the standard token based SSO handler
     * @param $event
     */
    public function registerTokenSsoHandler($event) {
        $event->params['map']['token'] = new TokenSsoHandler();
    }


	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu() {
		//compose the entire menu
		Yii::app()->mainmenu->addAppMenu('api', array(
				'icon' => 'admin-ico settings',
				'app_icon' => 'fa-code', // icon used in the new menu
				'link' => Docebo::createLmsUrl('//ApiApp/ApiApp/settings'),
				'label' => Yii::t('app', 'api_sso'),
				'settings' => ''
			),
			array()
		);
	}
}
