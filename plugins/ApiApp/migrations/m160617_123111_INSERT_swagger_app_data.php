<?php
/**
 * Yii DB Migration template.
 *
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 *
 */
class m160617_123111_INSERT_swagger_app_data extends DoceboDbMigration {

    //table name
    private $oauthClients_table = "oauth_clients";


	public function safeUp()
	{
        $this->insert($this->oauthClients_table, array(
            'client_id' => OauthClients::SWAGGER_CLIENT_ID,
            'client_secret' => Docebo::randomHash(),
            //left empty/null ON PURPOSE. Otherwise there will be redirect_uri mismatches
            //'redirect_uri' => null,
            'grant_types' => 'authorization_code refresh_token implicit',
            'app_name' => 'Swagger UI',
            'app_description' => 'Used by the API docs',
            'enabled' => true,
            'hidden' => true,
        ));
		return true;
	}

	public function safeDown()
	{
        $this->delete($this->oauthClients_table, "client_id = '" . OauthClients::SWAGGER_CLIENT_ID . "'");
		return true;
	}
}