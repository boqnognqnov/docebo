<?php

/**
 * ApiAppSettingsForm class.
 *
 * @property string $catalog_type
 */

class ApiAppSettingsForm extends CFormModel {

	public $sso_token;

	public $sso_secret;

	public $legacy_api_authentication;

	public $external_sso_force;
	public $external_sso_url;

	public $documentationUrl = 'https://www.docebo.com/lms-docebo-api-third-party-integration/';

	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			array('sso_token,sso_secret, legacy_api_authentication, external_sso_force, external_sso_url', 'safe'),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array(
			'sso_token' => Yii::t('configuration', 'Enable <abbr title="Single Sign On">SSO</abbr>'),
			'sso_secret' => Yii::t('configuration','_SSO_SECRET'),
			'legacy_api_authentication' => Yii::t('app',''),
			'external_sso_force' => Yii::t('app', 'Force external SSO'),
			'external_sso_url' => Yii::t('app', 'External SSO URL'),
		);
	}

	public function beforeValidate() {
		$this->sso_token = ($this->sso_token == 'on' ? 'on' : 'off');
		$this->legacy_api_authentication = ($this->legacy_api_authentication == 'on' ? 'on' : 'off');
		return parent::beforeValidate();
	}

}
