<?php

/* @var $form CActiveForm */

$form = $this->beginWidget('CActiveForm', array(
	'id' 	  => 'sso-client-edit-form',
	'method' => 'POST',
	'htmlOptions' => array(
		'name' 	  => 'sso-client-edit-form',
		'class' => 'docebo-form ajax',
		'enctype' => 'multipart/form-data'
	),
));


?>

<div class="row-fluid">

	<div class="clearfix">
		<?php
			echo $form->labelEx($model, 'app_name');
			echo $form->textField($model, 'app_name', array(
				'class' => 'sso-app-name input-block-level',
			));
			echo $form->error($model, 'app_name');
		?>
	</div>
	<br>

	<div class="clearfix">
		<?php
		echo $form->labelEx($model, 'app_description');
		echo $form->textArea($model, 'app_description', array(
			'class' => 'sso-app-description input-block-level',
		));
		echo $form->error($model, 'app_description');
		?>
	</div>

	<div id="app-icon-container" class="sso-client-edit-input-container">
		<div class="app-icon-label">
			<div>
				<?= Yii::t('oauth', 'App icon') ?>
				<br />
				<span style="font-size: 12px">
					<?= Yii::t('oauth', 'Your image will be resized to a square ({size})', array('{size}' => '60x60px')); ?>
				</span>
			</div>
		</div>
		<?php echo CHtml::fileField(
			'app_icon',
			($client_id) ? $model->app_icon : '',
			array(
				'class'             => 'sso-app-icon input-block-level',
				'id'                => CHtml::activeId(OauthClients::model(), 'app_icon'),
				'accept'            => 'image/*'
			)); ?>
	</div>


	<div class="clearfix">
		<?php
		echo $form->labelEx($model, 'client_id');
		echo $form->textField($model, 'client_id', array(
			'class' => 'sso-client-id input-block-level'
		));
		echo $form->error($model, 'client_id');
		?>
	</div>
	<br>

	<div class="clearfix">
		<?= $form->labelEx($model, 'client_secret'); ?>
		<?php echo CHtml::textField(
			CHtml::activeName(OauthClients::model(), 'client_secret'),
			($client_id) ? $model->client_secret : Docebo::randomHash(),
			array(
				'class'     => 'sso-client-secret input-block-level',
				'id'        => CHtml::activeId(OauthClients::model(), 'client_secret'),
				'readonly'  => 'readonly'
			)); ?>
	</div>
	<br>

	<div class="clearfix">
		<?php
		echo $form->labelEx($model, 'redirect_uri');
		echo $form->textField($model, 'redirect_uri', array(
			'class' => 'redirect-uri-id input-block-level'
		));
		echo $form->error($model, 'redirect_uri');
		?>
	</div>

	<div id="show-advanced-options-container" class="sso-client-edit-input-container">
		<div class="row-fluid">
			<div class="span12">
				<?php echo CHtml::checkBox('show-advanced-options',
					false,
					array(
						'class' => 'show-advanced-option',
						'id' => 'show-advanced-option',
//						'value' => ''
					));
				echo CHtml::label(Yii::t('standard', 'Show advanced settings'), 'show-advanced-option');
				?>
			</div>
		</div>
	</div>
	<div id="grant-types-container" class="sso-client-edit-input-container">
		<div class="grant-types-label">
			<div><?= Yii::t('oauth', 'Grant types') ?></div>
		</div>

		<div class="row-fluid">
			<div class="span12">
				<?php echo CHtml::checkBox(
					CHtml::activeName(OauthClients::model(), 'grant_types') . '[]',
					($client_id) ? (is_array($grantTypes) && in_array('authorization_code', $grantTypes) ? true : false) : true,
					array(
						'class' => 'grant-types-id',
						'id' => CHtml::activeId(OauthClients::model(), 'grant-types') . '-authorization-code',
						'value' => 'authorization_code'
					));
					echo CHtml::label(Yii::t('oauth', 'Authorization code + Implicit Grant'), CHtml::activeId(OauthClients::model(), 'grant-types') . '-authorization-code');
				?>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span12">
				<?php echo CHtml::checkBox(
					CHtml::activeName(OauthClients::model(), 'grant_types') . '[]',
					($client_id) ? (is_array($grantTypes) && in_array('password', $grantTypes) ? true : false) : false,
					array(
						'class' => 'grant-types-id',
						'id' => CHtml::activeId(OauthClients::model(), 'grant-types') . '-password',
						'value' => 'password'
					));
				echo CHtml::label(Yii::t('oauth', 'Resource Owner Password Credentials'), CHtml::activeId(OauthClients::model(), 'grant-types') . '-password');
				?>
			</div>
		</div>


		<div class="row-fluid">
			<div class="span12">
				<?php echo CHtml::checkBox(
					CHtml::activeName(OauthClients::model(), 'grant_types') . '[]',
					($client_id) ? (is_array($grantTypes) && in_array('client_credentials', $grantTypes) ? true : false) : false,
					array(
						'class' => 'grant-types-id',
						'id' => CHtml::activeId(OauthClients::model(), 'grant-types') . '-client-credentials',
						'value' => 'client_credentials'
					));
				echo CHtml::label(Yii::t('oauth', 'Client Credentials'), CHtml::activeId(OauthClients::model(), 'grant-types') . '-client-credentials');
				?>
			</div>
		</div>

	</div>


</div>

<div class="form-actions">
	<?= CHtml::submitButton(Yii::t('standard', '_CONFIRM'), 		array('id'=>'coaching-session-managment-confirm','name'  => 'confirm_button','class' => 'confirm-save btn btn-docebo green big')); ?>
	<?= CHtml::button(		Yii::t('standard', '_CLOSE'), 			array('class' => 'btn btn-docebo black big close-dialog')); ?>
	<?= CHtml::hiddenField('hidden', 0); ?>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	//<![CDATA[
	$(function(){
		$('#grant-types-container').hide();
		$('input').styler({
			browseText: '<?php echo Yii::t('standard', 'UPLOAD ICON') ?>'
		});

		// trigger file input click event on click upload icon
		$('.jq-file__browse').on('click', function() {
			$('#OauthClients_app_icon').trigger('click');
		});

		<?php
			if ($iconURL != '') {
		?>
		$('div.jq-file__name').prepend('<?= Yii::t("oauth", "Preview"); ?><br /><span class="sso-client-icon" style="background-image: url(\'<?=$iconURL; ?>\');"></span>');
		<?php
			} else {
		?>
		$('div.jq-file__name').prepend('<?= Yii::t("oauth", "Preview"); ?><br /><span class="sso-client-icon"><i class="fa fa-picture-o fa-lg"></i></span>');
		<?php
			}
		?>
		$('.show-advanced-option').on('click', function() {
			if ($('.show-advanced-option:checked').length) {
				$('#grant-types-container').show();
			} else {
				$('#grant-types-container').hide();
			}
		});

        var pattern = new RegExp(/^(http(s)?:\/\/)?(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i);
        var redirectUri = $('input[id*="redirect_uri"]');
        redirectUri.on('keyup', function(){
            if ( !pattern.test($(this).val()) ) {
                $(this).css("border-color",'red');
                $('.confirm-save.btn-docebo.green.big').addClass('disabled');
                $("span.error").show();
            } else if ( $(this).val().length < 1) {
                $(this).css("border-color",'#ccc');
                $('.confirm-save.btn-docebo.green.big').removeClass('disabled');
                $("span.error").hide();
            } else if (pattern.test($(this).val()) && $(this).val().length > 0) {
                $(this).css("border-color",'#ccc');
                $('.confirm-save.btn-docebo.green.big').removeClass('disabled');
                $("span.error").hide();
            }
        });
        redirectUri.on('blur', function(){
            if ( $(this).val() == '' || pattern.test($(this).val())) {
                $(this).css("border-color",'#ccc');
                $('.confirm-save.btn-docebo.green.big').removeClass('disabled');
                $("span.error").hide();
            }
        });
        $('.confirm-save.btn-docebo.green.big').on('click', function(){
            if ( $(this).hasClass('disabled') ) {
                redirectUri.append("<span class='error'>"+ "<?= Yii::t('app', 'Please enter valid Redirect URI') ?>"+"</span>");
                return false;
            }
        });

	});
	//]]>
</script>
