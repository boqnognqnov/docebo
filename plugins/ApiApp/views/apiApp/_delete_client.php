<?php
/* @var $model OauthClients */
?>

<?= CHtml::beginForm('', 'POST', array('class'	=> 'ajax')); ?>
<?= CHtml::hiddenField('client_id', $model->client_id) ?>

<div id="proceed-check" class="coaching-mode-radio-btn">
	<?php echo CHtml::checkBox("agree_deletion", false, array(
		'id'    =>  'proceed-check-btn',
		'class' =>  'delete-check ',
	)); ?>
	<?php echo CHtml::label(Yii::t('standard','Yes, I confirm I want to proceed'), 'proceed-check-btn',array('class'=>'coaching-inline-label')); ?>
</div>

<!-- For Dialog2 - they go in footer -->
<div class="form-actions">
	<?= CHtml::submitButton(Yii::t('standard', '_CONFIRM'), array(
			'name'  => 'confirm_button',
			'class' => 'sso-client-delete-confirm btn-docebo green big disabled' )
	); ?>
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' 	=> 'btn-docebo black big close-dialog')); ?>
</div>
<?php echo CHtml::endForm(); ?>

<script type="text/javascript">
	$(function(){
		$('.sso-client-delete-modal .delete-check').styler();

		$('#proceed-check-btn').on('change', function() {
			if($(this).is(':checked'))
				$('.sso-client-delete-confirm').removeClass('disabled');
			else
				$('.sso-client-delete-confirm').addClass('disabled');
		});

//		$('.coaching-session-delete-confirm').on('click', function() {
//			if($(this).hasClass('disabled'))
//				return false;
//		});
	});
</script>