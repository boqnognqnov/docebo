<?php
/* @var $form CActiveForm */
/* @var $settings ApiAppSettingsForm */
?>

<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('apps', 'My<br> apps') => Docebo::createAppUrl('lms:app/index#myapps'),
	Yii::t('app', 'api_sso') => Docebo::createAppUrl('lms:ApiApp/ApiApp/settings'),
	Yii::t('standard', 'Settings'),
); ?>

<?php $sidebarLinks = array(
	'settings' => array(
		'class' => 'advanced',
		'title' => '<i class="fa fa-lock fa-lg"></i> ' . Yii::t('app', 'SSO'),
		'data-tab' => 'settings',
		'data-url' => Docebo::createAppUrl('lms:ApiApp/ApiApp/settings'),
	),
	'oauth2' => array(
		'class' => 'https',
		'title' => '<span class="secret-key"></span> ' . Yii::t('app', 'API Credentials'),
		'data-tab' => 'oauth2',
		'data-url' => Docebo::createAppUrl('lms:ApiApp/ApiApp/oauth2'),
	),
	'legacy-api' => array(
		'class' => 'https',
		'title' => '<span class="secret-key"></span> ' . Yii::t('app', 'Legacy API'),
		'data-tab' => 'legacy-api',
		'data-url' => Docebo::createAppUrl('lms:ApiApp/ApiApp/oauth2'),
	)
);

$_manualUrl = ('it' == Yii::app()->getLanguage()
	? 'https://www.docebo.com/it/lms-docebo-api-third-party-integration/'
	: 'https://www.docebo.com/lms-docebo-api-third-party-integration/');
?>

	<div class="row-fluid">
		<div class="span12">
			<a href="<?= $_manualUrl ?>" target="_blank" class="app-link-read-manual">
				<?= Yii::t('apps', 'Read Manual'); ?>
			</a>
		</div>
	</div>

	<br/>
	<?php DoceboUI::printFlashMessages(); ?>

	<div id="admin-apiapp-settings" class="admin-advanced-wrapper">
		<div id="sidebar" class="advanced-sidebar">
			<h3><?= Yii::t('app', 'api_sso') ?></h3>
			<ul>
				<?php foreach ($sidebarLinks as $link) {
					$title = $link['title'];
					unset($link['title']);
					echo '<li class="'.$link['data-tab'].'-tab">' . CHtml::link($title, 'javascript:;', $link + array('data-loaded' => 0)) . '</li>';
				} ?>
			</ul>
		</div>
		<div id="main" class="advanced-main">
		<div class="content tab-content">
			<div id="settings">
				<h3><?= Yii::t('app', 'SSO') ?></h3>

				<?php
				$form = $this->beginWidget('CActiveForm', array(
					'id' => 'app-settings-form',
					'htmlOptions'=>array('class' => 'form-horizontal'),
				));
				?>

				<div class="control-group row odd hide">

					<label class="span1"><?= Yii::t('apps', 'API Key'); ?></label>

					<div class="controls">
						<?=$GLOBALS['cfg']['api_key'];?>
					</div>

				</div>

				<div class="control-group row hide">

					<label class="span1"><?= Yii::t('apps', 'API Secret'); ?></label>

					<div class="controls">
						<?=$GLOBALS['cfg']['api_secret'];?>
					</div>

				</div>

				<div class="row odd">

					<div class="control-group row">
						<div class="setting-name">
							<?=$form->labelEx($settings, 'sso_token', array('class'=>'row-field-label'));?>
						</div>

						<div class="values">
							<?=$form->checkBox($settings, 'sso_token', array('value' => 'on'));?>
							<?='&nbsp;&nbsp;'.Yii::t('configuration', '_SSO_TOKEN'); ?>
							<?=$form->error($settings, 'sso_token'); ?>
						</div>

					</div>
				</div>

				<div class="control-group row">
					<div class="setting-name">
						<?=$form->labelEx($settings, 'sso_secret', array('class'=>'row-field-label'));?>
					</div>
					<div class="values">
						<?=$form->textField($settings, 'sso_secret');?>
						<?=$form->error($settings, 'sso_secret'); ?>
					</div>
				</div>

				<div class="row odd">
					<div class="control-group row">
						<div class="setting-name">
							<?=$form->labelEx($settings, 'external_sso_force', array('class'=>'row-field-label'));?>
						</div>
						<div class="values">
							<?=$form->checkBox($settings, 'external_sso_force', array('value' => 'on'));?>
							<?='&nbsp;&nbsp;'.Yii::t('configuration', 'Force non logged in users to an external URL'); ?>
							<?=$form->error($settings, 'external_sso_force'); ?>
						</div>
					</div>
				</div>

				<div class="control-group row">
					<div class="setting-name">
						<?=$form->labelEx($settings, 'external_sso_url', array('class'=>'row-field-label'));?>
					</div>
					<div class="values">
						<?=$form->textField($settings, 'external_sso_url');?>
						<?=$form->error($settings, 'external_sso_url'); ?>
					</div>
				</div>

				<div class="form-actions">
					<?=CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class'=>'btn-docebo green big')); ?>
					&nbsp;
					<a href="<?=Docebo::createLmsUrl("app/index");?>" class="btn-docebo black big"><?=Yii::t('standard', '_CANCEL');?></a>
					<?= CHtml::hiddenField('ApiAppSettingsForm[legacy_api_authentication]', $settings->legacy_api_authentication); ?>
				</div>

				<?php $this->endWidget(); ?>
				</div>
				<div id="oauth2">
					<h3><?= Yii::t('app', 'API Credentials'); ?></h3>
					<hr />
					<div class="oauth2-help-container">
						<?= Yii::t('app', 'Manage credentials to access Docebo Restful APIs. Please <a href="{doc_url}" target="_blank">refer to the APIs documentation</a> for details',
							array('{doc_url}' => $settings->documentationUrl)); ?>.
					</div>
					<hr />
					<div class="main-actions clearfix">
						<ul class="clearfix" data-bootstro-id="bootstroNewCourse">
							<li>
								<div>
									<?= CHtml::link('<span style="background-image: none!important;"><i class="fa fa-lock fa-3x"></i></span>' . Yii::t('app', 'Add OAuth2 App'),
									Docebo::createAdminUrl('ApiApp/ApiApp/axEditSsoClient', array()),
									array(
										'class' => "open-dialog new-course_",
										'data-dialog-id'    => "sso-edit-client-dialog",
										'data-dialog-class' => "sso-edit-client-dialog",
										'data-dialog-title'	=> Yii::t('app', 'Add OAuth2 App'),
										'style' => "padding-top: 13.75px; height: 70.25px;"
									)
									); ?>
								</div>
							</li>
						</ul>
					</div>



					<div class="controls">
						<?php $this->renderPartial('_list'); ?>
					</div>
				</div>
				<div id="legacy-api">
					<h3><?= Yii::t('app', 'Legacy API'); ?></h3>
					<hr />
					<span class="legacy-explanation-text">
					<?= Yii::t('app', 'You can still use our legacy authentication method. However, we strongly recommend that you update your clients to OAuth2, as this method is going to be dismissed in future releases.'); ?>
					</span>

					<?php
					$form = $this->beginWidget('CActiveForm', array(
						'id' => 'app-settings-form2',
						'htmlOptions'=>array('class' => 'form-horizontal'),
					));
					?>

					<div class="row odd">
						<div class="control-group row">
							<div class="setting-name">
								<?=CHtml::label(Yii::t('app', 'Legacy API authentication'), 'sso-legacy-api-authentication', array('class'=>'row-field-label'));?>
							</div>
							<div class="values">
								<?=$form->checkBox($settings, 'legacy_api_authentication', array('value' => 'on', 'id' => 'sso-legacy-api-authentication'));?>
								<?='&nbsp;&nbsp;'.Yii::t('app', 'Enable legacy API authentication'); ?>
								<?=$form->error($settings, 'legacy_api_authentication'); ?>
								<div>
									<br />
									<label class="span1"><?= Yii::t('apps', 'API Key'); ?></label>: <?=$GLOBALS['cfg']['api_key'];?><br />
									<label class="span1"><?= Yii::t('apps', 'API Secret'); ?></label>: <?=$GLOBALS['cfg']['api_secret'];?>
									<?= CHtml::hiddenField('ApiAppSettingsForm[sso_token]', $settings->sso_token); ?>
									<?= CHtml::hiddenField('ApiAppSettingsForm[sso_secret]', $settings->sso_secret); ?>
								</div>
							</div>
						</div>
					</div>


					<div class="form-actions">
						<?=CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class'=>'btn-docebo green big')); ?>
						&nbsp;
						<a href="<?=Docebo::createLmsUrl("app/index");?>" class="btn-docebo black big"><?=Yii::t('standard', '_CANCEL');?></a>
					</div>

					<?php $this->endWidget(); ?>
				</div>
			</div>
		</div>
	</div>
<?php echo CHtml::hiddenField('selectedTab', '', array('id' => 'selectedTab')); ?>
<script type="text/javascript">
	$(function(){
		$('#oauth2, #legacy-api').hide();
		$('#settings').show();
		$('.settings-tab a').addClass('active');
		$('input').styler();

		$('#sidebar li a').off();
		$('#sidebar li a').on('click', function () {
			$('#sidebar li a').removeClass('active');
			$(this).addClass('active');

			$('#settings, #oauth2, #legacy-api').hide();
			$('#'+$(this).data('tab')).show();
		});

        if ( $(".alert-success").length) {
            setInterval(function(){
                $(".alert-success").fadeOut(1500);
            }, 3000);
        }
	});
</script>
