
<h3 class="oauth2-gridview-title">
	<?= Yii::t('app', 'Applications allowed to use OAuth2'); ?>
</h3>

<?php

$filter = new stdClass();
$filter->search_input = Yii::app()->request->getParam('term', Yii::app()->request->getParam('search_input', ''));

$columns = array(
	array(
		'value' => '$data["client_id"]',
		'headerHtmlOptions' => array('style' => 'display:none'),
		'htmlOptions' => array('style' => 'display:none'),
	),
	array(
		'name' => 'app_icon',
		'type' => 'raw',
		'header' => Yii::t('label', 'Icon'),
		'value' => function($data, $index) {
			$currAppIcon = '';
			if (!empty($data["app_icon"])) {
				$currAppIconAsset = CoreAsset::model()->findByPk($data['app_icon']);

				if ($currAppIconAsset) {
					$currAppIcon = '<span class="sso-client-icon" style="background-image: url('. $currAppIconAsset->getUrl(CoreAsset::VARIANT_MICRO) . ');"></span>';
				}
			}

			echo !empty($currAppIcon) ? $currAppIcon : '<span class="sso-client-icon"><i class="fa fa-picture-o fa-lg"></i></span>';
		},
		'htmlOptions' => array(
			'class' => 'td-oauth2-api-icon'
		)
	),
	array(
		'name' => 'app_name',
		'value' => 'CHtml::encode($data["app_name"])',
		'type' => 'raw',
		'header' => Yii::t('standard', '_NAME'),
		'htmlOptions' => array(
			'class' => 'td-oauth2-api-name'
		)
	),
	array(
		'name' => 'app_description',
        'htmlOptions'   => array(
            'class' => 'description-column td-oauth2-api-description'
        ),
		'value' => function($data) {
            echo $data["app_description"];
        },
		'type' => 'raw',
		'header' => Yii::t('standard', '_DESCRIPTION'),
	),
	array(
		'type'	=> 'html',
		'value'	=> array($this, 'renderOperationsColumn'),
		'htmlOptions' => array(
			'class' => 'text-right td-oauth2-api-operation-column'
		)
	)
);

$dataProvider = OauthClients::sqlDataProvider($filter, Settings::get('elements_per_page', 10));

$gridParams = array(
	'gridId' 				=> 'sso-clients-list',
	'dataProvider'			=> $dataProvider,
	'columns'				=> $columns,
	'disableMassSelection' => true,
	'disableMassActions' => true,
	'autocompleteRoute'		=> '//ApiApp/ApiApp/Autocomplete',
	'hiddenFields' => array(
	),
);
$this->widget('common.widgets.ComboGridView', $gridParams);

?>
<div class="clearfix"></div>
<script type="text/javascript">
	//<![CDATA[
	$(function () {
		//
		$("#show_available_only").on('change', function(){
			// Prepare the form data to be sent with gridView update
			var options = {
				data: $('#combo-grid-search-input-sso-clients-list').serialize()
			};

			// Do the yiiGridView update here
			$.fn.yiiGridView.update("sso-clients-list", options);
		});

		$('#show_available_only').styler();

		/**
		 * GLOBAL LIVE event to listen for ToggleStatus clicks in a GRID [column]
		 * data-url : controller/action to call
		 * data-id  : ID of the item to toggle
		 *
		 */
		$('#content').on('click', '.grid-view .toggle-active-state', function(event) {
			var _this = $(this);
			var gridId = $(this).parents('.grid-view').attr('id');

			// Check for available URL
			var url = $(this).data('url');
			if (url) {
				$.get(HTTP_HOST + '?r=' + url, { 'id': $(this).data('id') })
					.done(function(data) {
						if (data.success) {
							if (_this.hasClass('disabled')) {
								_this.removeClass('disabled').addClass('enabled');
							} else {
								_this.removeClass('enabled').addClass('disabled');
							}
							_this = null;

							$.fn.yiiGridView.update(gridId);
						}
					});
			}
			event.preventDefault();
		});

		// Do the ComboGridView update on modal close event
		$(document).on("dialog2.closed","#delete-sso-client-dialog, #sso-edit-client-dialog", function(){
			// Prepare the form data to be sent with gridView update
			var options = {
				data: $('#combo-grid-search-input-sso-clients-list').serialize()
			};
			// Do the yiiGridView update here
			$.fn.yiiGridView.update("sso-clients-list", options);
		});
	});
	//]]>
</script>