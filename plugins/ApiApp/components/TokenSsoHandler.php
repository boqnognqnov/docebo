<?php

class TokenSsoHandler extends CComponent implements ISsoHandler {

    /**
     * Validate the token used by the standard Docebo authentication
     * @return string Userid of the logged in user
     * @throws Exception in case something is wrong and SSO cannot be performed
     *
     * Example URL: /lms/?r=site/sso&login_user=XXXX&time=12321321321&token=AERQWETWERTR
     * [optional] &id_course=1234
     * [optional] &destination=[mycourse|catalog|learningplan|myactivities|mycalendar]
     */
    public function login() {

        // Get request params
        $login_user = Yii::app()->request->getParam('login_user', '');

        // Check if user is already logged in
        if(!Yii::app()->user->getIsGuest()) {
            $loggedInUser = Yii::app()->user->getUsername();
            if($loggedInUser == $login_user) {
                // Determine which URL to jump to
                $redirect_url = $this->getLandingURL();

                // Finally, redirect!!
                Yii::app()->controller->redirect($redirect_url);
            } else {
                LearningTracksession::model()->updateAll(array('active' => 0), 'active = 1 AND idUser = '.Yii::app()->user->id);
                Yii::app()->user->logout();
				Yii::app()->controller->refresh();
            }
        }

        $auth_regen     = (Yii::app()->request->getParam('auth_regen', 0) == 1) ? true : false;
        $time		    = Yii::app()->request->getParam('time', '');
        $secret		    = Settings::get('sso_secret', '8ca0f69afeacc7022d1e589221072d6bcf87e39c');
        $token		    = strtoupper(Yii::app()->request->getParam('token', ''));
        $id_course      = Yii::app()->request->getParam('id_course', 0);
        $destination    = Yii::app()->request->getParam('destination', '');
        $lifetime       = Settings::get('rest_auth_lifetime', 1);

        // Get settings
        $is_sso_enabled = Settings::get('sso_token', 'off') == 'on';

        // Should we accept sso requests?
        if(($login_user == '') || !$is_sso_enabled)
            throw new Exception("Empty login_user param or SSO not available", 1);

        // Recalculate the token internally
        $recalc_token	= strtoupper(md5($login_user.','.$time.','.$secret));

        // Should we authoregenerate the token ?
        if($auth_regen && ($recalc_token==$token)) {
            // We've landed on a page from a SSO link from a third-party app like Joomla or WordPress.
            // Since SSO links have an expire option (they are usually only valid for 60 seconds),
            // we use this landing page to rebuild a valid and unexpired SSO link and redirect to it.
            // Essentially, if we are inside this code block, it will rebuild the SSO link
            // using the current time and redirect to this SSO link so it is always valid/non expired.
            $new_time = time();
            $new_token = md5($login_user.','.$new_time.','.$secret);
            $redirect_url = 'index.php?r=site/sso&modname=login&op=confirm&login_user='.$login_user.'&time='.$new_time.'&token='.$new_token;

            // Preserve the optional params to be viewed on redirect
            if($id_course)
                $redirect_url .= '&id_course='.$id_course;
            else if($destination)
                $redirect_url .= '&destination='.$destination;

            // Redirect to the same URL but with a recalculated token
	        // so the second request has a valid SSO token and it should
	        // theoretically result in a successful login
	        if($controller = Yii::app()->getController())
                $controller->redirect($redirect_url);
	        else
		        throw new CException('TokenSsoHandler must be called from a Controller context for redirect to work');
        } else {
            // Validate the token here and check for time expiration
            if(($recalc_token != $token) || ($time+$lifetime < time()))
                throw new Exception("Invalid or expired token", 1);

            return $login_user;
        }
    }

    /**
     * Returns the URL to redirect to, after a successful LMS login
     * @return string URL to redirect to
     */
    public function getLandingURL() {

        // Check passed GET params to calculate the landing page after sso
        $id_course      = Yii::app()->request->getParam('id_course', 0);
        $destination    = Yii::app()->request->getParam('destination', '');

        $redirect_url = null;

        // *** HOOK FOR PLUGINS ***
        $loginEvent = new DEvent($this, array('redirect_url' => &$redirect_url));
        Yii::app()->event->raise('ActionSsoLogin', $loginEvent);
        if(!$redirect_url) {

			$isSmartphone = !isset($_SESSION['not_smartphone']) || !$_SESSION['not_smartphone'];
			$urlFormatter = 'createAbsoluteLmsUrl';
			if (Docebo::isMobile() && $isSmartphone)
				$urlFormatter = 'createAbsoluteMobileUrl';

            $redirect_url = Docebo::getUserAfetrLoginRedirectUrl();
            if($id_course)
                $redirect_url = Docebo::$urlFormatter((Docebo::isMobile() && $isSmartphone) ? 'mplayer' :'player', array('course_id' => $id_course));
            else {
				switch($destination) {
					case 'mycourses':
						$redirect_url = Docebo::$urlFormatter('site/index', array('opt' => 'fullmycourses'));
						break;
					case 'catalog':
						$redirect_url = Docebo::$urlFormatter('site/index', array('opt' => 'catalog'));
						break;
					case 'learningplan':
						$redirect_url = Docebo::$urlFormatter('curricula/show', array('sop' => 'unregistercourse'));
						break;
					case 'myactivities':
						$redirect_url = Docebo::$urlFormatter((Docebo::isMobile() && $isSmartphone) ? 'mreport/index' : 'myActivities/index');
						break;
					case 'mycalendar':
						$redirect_url = Docebo::$urlFormatter('mycalendar/show');
						break;
				}
			}
        }

        return $redirect_url;
    }
}