<?php

class ApiAppController extends Controller {


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		$admin_only_actions = array('settings', 'toggleSsoClientState');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}


	public function actionSettings() {
		// Add the admin.css
		Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/admin.css');

		$settings = new ApiAppSettingsForm();
		$settings->sso_token = Settings::get('sso_token');
		$settings->sso_secret = Settings::get('sso_secret');
		$settings->legacy_api_authentication = Settings::get('legacy_api_authentication', 'on');

		$settings->external_sso_force = PluginSettings::get('external_sso_force');
		$settings->external_sso_url = PluginSettings::get('external_sso_url');

		if (isset($_POST['ApiAppSettingsForm'])) {
			$settings->setAttributes($_POST['ApiAppSettingsForm']);

			if ($settings->validate()) {
				if ($settings->legacy_api_authentication != 'on')
					$settings->legacy_api_authentication = 'off';

				Settings::save('sso_token', $settings->sso_token);
				Settings::save('sso_secret', $settings->sso_secret);
				Settings::save('legacy_api_authentication', $settings->legacy_api_authentication);

				PluginSettings::save('external_sso_force', $settings->external_sso_force);
				PluginSettings::save('external_sso_url', $settings->external_sso_url);
				Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));
                $this->redirect(Yii::app()->createUrl('ApiApp/ApiApp/settings'));
			}
			else {
				Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
			}
		}

		$this->render('settings', array('settings' => $settings));
	}

	/**
	 * The backend counterpart of the "sso clients search autocomplete" (JuiAutocomplete)
	 */
	public function actionAutocomplete() {
		$maxNumber 		= Yii::app()->request->getParam('max_number', false);
		$term 			= Yii::app()->request->getParam('term', false);

		$filter         = new StdClass;
		$filter->search_input = $term ? $term : null;
		$dataProvider   = OauthClients::sqlDataProvider($filter);
		$dataProvider->setPagination(array(
			'pageSize' => $maxNumber,
		));
		$data = $dataProvider->getData();
		$result = array();
		if ($data) {
			foreach ($data as $item) {

				$result[] = array(
					'label' => $item["app_name"],
					'value' => $item["app_name"]//$item["client_id"],
				);
			}
		}
		echo CJSON::encode($result);
	}

	/**HTML for the last column of the main gridView of sso-clients/index
	 *
	 * @param mixed $data
	 * @param int $index
	 */
	protected function renderOperationsColumn($data, $index) {

		$html  = "<div class='operation-column'>";
		$html .= CHtml::link("", "javascript:void(0);",
			array(
				"class" => "docebo-sprite ico-circle-check tooltipize toggle-active-state " . ($data['enabled'] == 1 ? "green" : ""),
				"data-url" => "ApiApp/ApiApp/toggleSsoClientState",
				"data-id" => $data['client_id'],
				"title" => Yii::t("standard", "_ACTIVATE") . "/" . Yii::t("standard", "_DEACTIVATE"),
			));

		$html .= "&nbsp;&nbsp;&nbsp;&nbsp;". CHtml::link('<span class="i-sprite is-edit"></span>',
			Docebo::createAdminUrl('ApiApp/ApiApp/axEditSsoClient', array(
				'client_id'		=> $data['client_id'],
			)),
			array(
				'class' => "open-dialog",
				'data-dialog-id'    => "sso-edit-client-dialog",
				'data-dialog-class' => "sso-edit-client-dialog",
				'data-dialog-title'	=> Yii::t('app', 'Edit OAuth2 App'),
			)
		);
		$html .= "&nbsp;&nbsp;". CHtml::link('<span class="i-sprite is-remove red"></span>',
				Docebo::createAdminUrl('ApiApp/ApiApp/axDeleteSsoClient', array(
					'client_id'             => $data['client_id'],
				)),
				array(
					'class'                 => "open-dialog",
					'data-dialog-id'        => "delete-sso-client-dialog",
					'data-dialog-class'     => "delete-item-modal sso-client-delete-modal",
					'closeOnEscape'         => "true",
					'closeOnOverlayClick'   => "true",
					'data-dialog-title'	    => Yii::t('app', 'Delete client'),
				)
			);
        $html  .= "</div>";
		echo $html;

	}

	public function actionToggleSsoClientState($id) {
		$model = OauthClients::model()->findByPk($id);

		if ($model) {
			$model->enabled = $model->enabled == 1 ? 0 : 1;
			$model->save();

			$result['success'] = true;

			Yii::app()->event->raise('ToggleSSOClientState', new DEvent($this, array(
				'client_id' => $model->client_id,
				'model' => $model
			)));
		} else {
			$result['success'] = false;
		}

		$this->sendJSON($result);
	}

	/**
	 * Deletes the sso client
	 */
	public function actionAxDeleteSsoClient() {
		$idClient  = Yii::app()->request->getParam('client_id', false);
		$confirm = Yii::app()->request->getParam('confirm_button', false);
		$agreed = Yii::app()->request->getParam('agree_deletion', false);
		$transaction = null;

		try {
			// Load session id
			$clientModel = OauthClients::model()->findByPk($idClient);
			if (!$clientModel)
				throw new Exception('Invalid client');

			if($confirm && $agreed) {
				if (Yii::app()->db->getCurrentTransaction() === NULL)
					$transaction = Yii::app()->db->beginTransaction();

				if(!$clientModel->delete())
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));

				if ($transaction)
					$transaction->commit();

				echo '<a class="auto-close"></a>';
				Yii::app()->end();

			} else {
				$this->renderPartial('_delete_client', array(
					'model' => $clientModel,
				));
			}
		}
		catch (Exception $e) {
			if ($transaction)
				$transaction->rollback();

			$this->renderPartial('admin.protected.views.common/_dialog2_error', array(
				'message' 	=> $e->getMessage(),
				'type'		=> 'error',
			));
			Yii::app()->end();
		}
	}


	/**
	 * Render and handle Creaion and Editing of a sso client
	 */
	public function actionAxEditSsoClient() {
		$idClient   = Yii::app()->request->getParam('client_id', false);
		$confirm 	= Yii::app()->request->getParam("confirm_button", FALSE);
		$clientModel = false;

		if ($confirm) {
			$clientData = Yii::app()->request->getParam('OauthClients',array());

			// purify the input data
			foreach($clientData as $postKey => $postValue)
				$clientData[$postKey] = Yii::app()->htmlpurifier->purify($clientData[$postKey]);

			if (!empty($clientData['grant_types']) && is_array($clientData['grant_types']) && count($clientData['grant_types'])) {
				$clientData['grant_types'][] = 'refresh_token';
				if(array_search('authorization_code',$clientData['grant_types']) !== false)
					$clientData['grant_types'][] = 'implicit';

				$clientData['grant_types'] = implode(' ', $clientData['grant_types']);
			}

			if($idClient)
				$clientModel = OauthClients::model()->findByPk($idClient);
			else
				$clientModel = new OauthClients();

			try {
				$clientModel->setAttributes($clientData);

				if ($clientModel->hidden != 1) {
					$clientModel->hidden = 0;
				}

				$modelErrors = '';
				if(!$clientModel->save()) {
					throw new CException($modelErrors);
				}


				if (!empty($_FILES["app_icon"]) && $confirm) {

					// Get the uploaded file
					$uploadedFile = CUploadedFile::getInstanceByName("app_icon");
					$filePath = $uploadedFile->getTempName();
					$tmpFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $uploadedFile->getName();
					copy($filePath, $tmpFilePath);

					if(!$clientModel->app_icon) {
						$asset = new CoreAsset();
					} else {
						$asset = CoreAsset::model()->findByPk($clientModel->app_icon);
					}

					$asset->sourceFile = $tmpFilePath;
					$asset->type = CoreAsset::TYPE_APP_ICON;

					// Validate!! && Save
					$ok = $asset->save();
					if (!$ok) {
						throw new CException(Yii::t("standard", '_OPERATION_FAILURE'));
					} else {
						// Set the asset as client app
						//$clientModel->app_icon = $uploadedFile->getName();
						//$clientModel->app_icon = $asset->getUrl(CoreAsset::VARIANT_MICRO);//$asset->id;
						$clientModel->app_icon = $asset->id;

						$clientModel->save();

						// Clean up
						FileHelper::removeFile($tmpFilePath);
					}
				}

				echo '<a class="auto-close"></a>';
				Yii::app()->end();


				// commit

			} catch(Exception $e) {
				// rollback
				Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			}

		}

		$grantTypes = array();
		$iconURL = '';
		if($idClient && !$clientModel) {
			$clientModel = OauthClients::model()->findByPk($idClient);
			$grantTypes = explode(' ', $clientModel->grant_types);

			if ($clientModel->app_icon != '') {
				$asset = CoreAsset::model()->findByPk($clientModel->app_icon);

				if ($asset) {
					$iconURL = $asset->getUrl(CoreAsset::VARIANT_SMALL);
				}
			}
		}


		if (!$clientModel) {
			$clientModel = new OauthClients();
			$clientModel->client_secret = Docebo::randomHash();
		}

		$params = array(
			'client_id'     => $idClient,
			'model'         => $clientModel,
			'grantTypes'    => $grantTypes,
			'iconURL'       => $iconURL
		);

		echo $this->renderPartial('_edit_client', $params, true, true);
		Yii::app()->end();
	}

}
