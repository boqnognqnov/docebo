<?php
/* @var $form CActiveForm */
/* @var $settings OktaAppSettingsForm  */
?>

<a href="<?= ('it' == Yii::app()->getLanguage()) ? 'https://www.docebo.com/it/knowledge-base/docebo-per-okta/' : 'https://www.docebo.com/knowledge-base/docebo-okta/' ?>" target="_blank" class="app-link-read-manual"><?= Yii::t('apps', 'Read Manual'); ?></a>
<h2>OKTA - <?=Yii::t('standard','Settings'); ?></h2>
<br>

<?php DoceboUI::printFlashMessages(); ?>

<?php $this->widget('SamlSettings', array('okta' => true)) ?>