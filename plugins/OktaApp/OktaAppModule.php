<?php

class OktaAppModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'plugin.SimpleSamlApp.widgets.*',
			'plugin.SimpleSamlApp.models.*',
		));
	}

    /**
     * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
     */
    public function menu() {
        Yii::app()->mainmenu->addAppMenu('okta', array(
            'icon' => 'home-ico okta',
			'app_icon' => 'fa-lock', // icon used in the new menu
            'link' => Docebo::createAppUrl('admin:OktaApp/OktaApp/settings'),
            'label' => 'OKTA '.Yii::t('standard', 'Settings'),
        ), array());
    }
}
