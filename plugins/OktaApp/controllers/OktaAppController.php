<?php

class OktaAppController extends Controller
{


    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }


    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        $res = array();

        // keep it in the following order:
        // Allow following actions to admin only:
        $admin_only_actions = array('settings');
        $res[] = array(
            'allow',
            'roles' => array(Yii::app()->user->level_godadmin),
            'actions' => $admin_only_actions,
        );

        // deny admin only actions to all other users:
        $res[] = array(
            'deny',
            'actions' => $admin_only_actions,
        );

        $res[] = array(
            'deny', // deny all
            'users' => array('*'),
        );

        return $res;
    }

	/**
     * Settings action (configures params needed for the SAML connection
     */
    public function actionSettings()
    {
		// Render the view with the widget settings form
		$this->render('index');
    }
}