<div id="<?= isset($containerId) ? 'player-centralrepo-uploader-panel' : 'player-arena-lti-editor-panel' ?>" class="player-arena-editor-panel" xmlns="http://www.w3.org/1999/html">
	<div class="header">LTI</div>
	<div class="content">
		<?php
		/* @var $htmlPageObj LearningLti */
		/* @var $form CActiveForm */
		?>

		<?php
		$form = $this->beginWidget('CActiveForm', array(
			'id' => $centralRepoContext?'central-lo-lti-editor-form':'lti-editor-form',
			'method' => 'post',
			'action' => isset($saveUrl) ? $saveUrl : Docebo::createAbsoluteLmsUrl("//LtiApp/LtiApp/axSaveLo"),
			'htmlOptions' => array(
				'class' => 'form-horizontal',
				'enctype' => 'multipart/form-data',
			)
		));
		?>

		<?=(!empty($courseModel))?$form->hiddenField($courseModel, 'idCourse'):'';?>
		<?=$form->hiddenField($ltiObj, 'id');?>

		<div id="<?= isset($containerId) ? $containerId : 'player-arena-uploader' ?>">
			<div>
				<div class="tabbable"> <!-- Only required for left/right tabs -->
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab1" data-toggle="tab"><?=LearningOrganization::objectTypeValue($lo_type)?></a></li>
						<li><a href="#lo-additional-information" data-toggle="tab"><?=Yii::t('myactivities', 'Additional info')?></a></li>

						<?php
						// Raise event to let plugins add new tabs
						if(!$centralRepoContext){
							Yii::app()->event->raise('RenderEditLoTabs', new DEvent($this, array(
								'courseModel' => $courseModel,
								'loModel' => isset($loModel) ? $loModel : null
							)));
						}
						?>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab1">
							<div class="player-arena-uploader">

								<div class="control-group">
									<?php echo CHtml::label(Yii::t('standard', '_TITLE'), 'LearningLti_title', array(
										'class' => 'control-label'
									)); ?>
									<div class="controls">
										<?= $form->textField($ltiObj, 'title', array('class' => 'input-block-level')) ?>
									</div>
								</div>

								<div class="control-group">
									<?php echo CHtml::label(Yii::t('standard', '_DESCRIPTION'), 'LearningLti_description', array(
										'class' => 'control-label'
									)); ?>
									<div class="controls">
										<?php echo $form->error($ltiObj,'description'); ?>
										<?= $form->textArea($ltiObj, 'description', array('id' => 'player-arena-lti-editor-textarea')); ?>
									</div>
								</div>

								<div class="control-group">
									<?php echo CHtml::label(Yii::t('lti', 'Launch URL'), 'LearningLti_url', array(
										'class' => 'control-label'
									)); ?>
									<div class="controls">
										<?= $form->textField($ltiObj, 'url', array('class' => 'input-block-level')) ?>
										<span class="url-valudator-info"></span>
										<span class="lti_title_info"><?=Yii::t('lti', 'Paste the launch URL of the LTI resource')?></span>
									</div>
								</div>

								<div class="control-group">
									<?php echo CHtml::label(Yii::t('lti', 'Consumer key'), 'LearningLti_consumer_key', array(
										'class' => 'control-label'
									)); ?>
									<div class="controls">
										<?= $form->textField($ltiObj, 'consumer_key', array('class' => 'input-block-level')) ?>
									</div>
								</div>

								<div class="control-group">
									<?php echo CHtml::label(Yii::t('lti', 'Shared secret'), 'LearningLti_shared_secret', array(
										'class' => 'control-label'
									)); ?>
									<div class="controls">
										<?= $form->textField($ltiObj, 'shared_secret', array('class' => 'input-block-level')) ?>
									</div>
								</div>

								<div class="control-group lti_checkbox_group">
									<?php echo CHtml::label(Yii::t('lti', 'Privacy'), '', array(
										'class' => 'control-label'
									)); ?>
									<div class="controls">
										<div class="lti_checkbox_item">
											<?= $form->checkbox($ltiObj, 'share_name', array(
												'value' => 1,
												'uncheckValue' => 0
											)) ?>
											<?= CHtml::label(Yii::t('lti', "Share launcher's name with the tool"), 'LearningLti_share_name') ?>
										</div>
										<div class="lti_checkbox_item">
											<?= $form->checkbox($ltiObj, 'share_email', array(
												'value' => 1,
												'uncheckValue' => 0
											)) ?>
											<?= CHtml::label(Yii::t('lti', "Share launcher's email with the tool"), 'LearningLti_share_email') ?>
										</div>
									</div>
								</div>

								<div class="control-group lti_radio_group">
									<?php echo CHtml::label(Yii::t('lti', 'Completion'), '', array(
										'class' => 'control-label'
									)); ?>
									<div class="controls">
										<?= $form->radioButtonList($ltiObj, 'completion_type', array(
											LearningLti::LTI_COMPLETE_ON_OPEN => Yii::t('lti', 'On opening the Learning Object'),
											LearningLti::LTI_COMPLETE_ON_RETURN => Yii::t('lti', 'After receiving feedback from the external tool'). " *"
										),array(
											'separator' => '',
											'template' => '<span class="lti_radio_item">{input}{label}</span>'
										)) ?>
										<div class="radio_comment">* <?=Yii::t('lti', 'Please make sure this tool or the selected content can send back feedback, or this Learning Object can never be marked as completed')?></div>

										<div class="lti_completion_extension" style="display: <?=($ltiObj->completion_type == LearningLti::LTI_COMPLETE_ON_RETURN)?'block':'none'?>;">
											<?php echo CHtml::label(Yii::t('lti', 'Set minimum score to pass'), 'LearningLti_minimum_score', array()); ?>
											<?= $form->textField($ltiObj, 'minimum_score', array('class' => 'input-block-level')) ?>
										</div>

										<div class="lti_completion_extension block_direct_completion" style="display: <?=($ltiObj->completion_type == LearningLti::LTI_COMPLETE_ON_RETURN)?'block':'none'?>;">
											<?= $form->checkbox($ltiObj, 'direct_completion', array(
												'value' => 1,
												'uncheckValue' => 0
											)) ?>
											<?= CHtml::label(Yii::t('lti', "No score reporting (only mark as complete when the above score is reached)"), 'LearningLti_direct_completion') ?>
										</div>
									</div>
								</div>

								<div class="control-group lti_radio_group">
									<?php echo CHtml::label(Yii::t('lti', 'View mode'), '', array(
										'class' => 'control-label'
									)); ?>
									<div class="controls">
										<?= $form->radioButtonList($ltiObj, 'target', array(
											LearningLti::LTI_TARGET_IFRAME => Yii::t('player', 'Inline'),
											LearningLti::LTI_TARGET_LBOX => Yii::t('player', 'Lightbox'),
											LearningLti::LTI_TARGET_FSCREEN => Yii::t('standard', 'Fullscreen'),
											LearningLti::LTI_TARGET_WINDOW => Yii::t('lti', 'New window (use only when the tool does not support I-frames)')
										),array(
											'separator' => '',
											'template' => '<span class="lti_radio_item">{input}{label}</span>'
										)) ?>
									</div>
								</div>
								<br>

							</div>
						</div>
						<div class="tab-pane" id="lo-additional-information">
							<?php
							$additionalInfoWidgetParams = array(
								'loModel'=> $loModel,
								'idCourse' => isset($courseModel) ? $courseModel->idCourse : null,
							);

							if($centralRepoContext){
								$additionalInfoWidgetParams['refreshUrl'] = Docebo::createAbsoluteLmsUrl('centralrepo/centralLo/getSlidersContent');
								$additionalInfoWidgetParams['deleteUrl'] = Docebo::createAbsoluteLmsUrl('centralrepo/centralLo/deleteImage');
							}
							?>
							<? $this->widget('common.widgets.PlayerLayoutAdditionalInfo', $additionalInfoWidgetParams); ?>
						</div>

						<?php
						// Raise event to let plugins add new tab contents
						if(!$centralRepoContext){
							Yii::app()->event->raise('RenderEditLoTabsContent', new DEvent($this, array(
								'courseModel' => isset($courseModel) ? $courseModel : null,
								'loModel' => isset($loModel) ? $loModel : null
							)));
						}
						?>
					</div>
				</div>

			</div>

		</div>

		<div class="text-right">
			<input type="submit" name="confirm_save_lti" class="btn-docebo green big"
				   value="<?php echo Yii::t('standard', '_SAVE'); ?>">&nbsp;&nbsp;
			<a id="cancel_save_lti"
			   class="btn-docebo black big" <?= isset($backButtonUrl) ? "href='" . $backButtonUrl . "'" : "" ?>><?php echo Yii::t('standard', '_CANCEL'); ?></a>
		</div>

		<?php if($idObject): ?>
			<input type="hidden" name="id_object" value="<?=$idObject ?>">
		<?php endif; ?>

		<?php $this->endWidget(); ?>

	</div>
</div>
<script type="text/javascript">
	$('input[type="checkbox"], .controls input[type="radio"]').styler();

	/*<![CDATA[*/
	(function ($) {
		$(function () {
			$(document).controls();

			$('input[name="LearningLti[completion_type]"]').on('change', function(){
				var completionType = $('input[name="LearningLti[completion_type]"]:checked').val();
				if(completionType == 1){
					$('.lti_completion_extension').css('display', 'none');
				} else {
					$('.lti_completion_extension').css('display', 'block');
				}
			})

			<?php if(isset($containerId)): ?>
			TinyMce.attach('#player-arena-lti-editor-textarea', 'standard_embed', {height: 180});
			var editLTI = new CentralLoLtiEdit({
				editLtiUrl: <?= json_encode(Docebo::createAbsoluteAdminUrl('LtiApp/LtiApp/editCentralRepo')) ?>
			});
			editLTI.formListeners();
			<?php endif; ?>

		});
	})(jQuery);

	/*]]>*/
</script>