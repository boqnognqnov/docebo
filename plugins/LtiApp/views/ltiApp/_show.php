<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, target-densitydpi=160dpi, minimal-ui">
	<script src="<?=Yii::app()->theme->baseUrl . '/js/jquery.js'?>"></script>
	<style type="text/css">
		body{
			margin: 0;
			padding: 0;
		}

		.lti-container{
			position: absolute;
			width: 100%;
			height: 100%;
		}

		iframe{
			position: absolute;
			height: 100%;
			width: 100%;
			border: none;
		}

		input#basiclti_submit{
			background-color: #55AB54;
			padding:6px 12px;
			font-weight: 600;
			font-size: 13px;
			vertical-align: middle;
			display: inline-block;
			color: #ffffff;
			text-shadow: 0 1px 0 #4EAE4F;
			line-height: normal;
			cursor: pointer;
			-webkit-appearance: button;
			border: 0px none;
			text-transform: uppercase;
			border-radius: 0px;
			font-family: 'Open Sans', sans-serif;
			margin: 15px;
		}

	</style>
</head>
<body>
	<div class="lti-container">
		<form id="ltiLaunchForm" name="ltiLaunchForm"
			  action="<?=(isset($returnLaunchUrl) && !empty($returnLaunchUrl))?rtrim($returnLaunchUrl, '/'):rtrim($resource->url, '/')?>"
			  <?=($launchLinkParams['launch_presentation_document_target'] == LearningLti::LTI_TARGET_WINDOW)?'target="_blank"':''?>
			  method="post" enctype="application/x-www-form-urlencoded">
		<?php
		unset($launchLinkParams['basiclti_submit']);
		foreach($launchLinkParams as $key => $val){
			print '<input type="hidden" name="'.$key.'" value="'.$val.'"/>';
		}
		?>
		<input type="submit" name="basiclti_submit" id="basiclti_submit" value="<?=Yii::t('lti', 'Launch')?>" />
		</form>
	</div>
	<script type="text/javascript">
		(function($){
			$(function(){
//				if($('input[name="launch_presentation_document_target"]').val() == "iframe"){
					$('#basiclti_submit').css('display', 'none').trigger('click');
					return false;
//				}
			});
		})(jQuery);
	</script>
</body>
</html>