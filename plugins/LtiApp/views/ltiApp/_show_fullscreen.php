<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, target-densitydpi=160dpi, minimal-ui">
    <?php
    $assetsUrl = Yii::app()->getModule('LtiApp')->getAssetsUrl();
    $bootstrapUrl = Yii::app()->getModule('mobileapp')->getAssetsUrl();
    ?>
    <script src="<?=Yii::app()->theme->baseUrl . '/js/jquery.js'?>"></script>
    <link rel="stylesheet" href="<?=$bootstrapUrl . '/css/bootstrap.min.css'?>" media="all">
    <link rel="stylesheet" href="<?=$assetsUrl . '/css/lti_fullscreen.css'?>" media="all">

</head>
<body>
    <div class="mynavbar">
        <h1><?=$object->title?></h1>
        <a id="player_close" href="<?=Docebo::createLmsUrl('player', array('course_id' => $object->idCourse))?>">
            <span class="glyphicon glyphicon-remove glyphicon-white"></span>
        </a>
    </div>
    <iframe  allowfullscreen="true" id="ltiFullscreenIframe" name="ltiFullscreenIframe" src="<?=Docebo::createLmsUrl('LtiApp/LtiApp/show', array(
        'id_object' => $object->idOrg,
        'course_id' => $object->idCourse,
        'display' => LearningLti::LTI_TARGET_FSCREEN
    ))?>" frameborder="0"></iframe>
</body>
</html>