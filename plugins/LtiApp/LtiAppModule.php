<?php

class LtiAppModule extends CWebModule
{

	private $_assetsUrl;

	public function init()
	{
		// import the module-level models and components
		$this->setImport(array(
			'LtiApp.components.*'
		));

		$this->defaultController = 'LtiApp';

        if (Yii::app() instanceof CWebApplication) { //make sure this is not a console application

            // Register CSS ONLY if this is NOT an ajax call. Plugins' init() is always called during their loading
            if (!Yii::app()->request->isAjaxRequest) {
                Yii::app()->getClientScript()->registerCssFile($this->getAssetsUrl() . "/css/lti.css");
            }

        }

//		Yii::app()->request->noCsrfValidationRoutes[] = 'LtiApp/LtiApp/return';
//		Yii::app()->request->noCsrfValidationRoutes[] = 'LtiApp/LtiApp/outcome';

		Yii::app()->event->on(EventManager::EVENT_ON_PLUGIN_DEACTIVATE, array($this, 'onPluginDeactivate'));

	}


	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(
				Yii::getPathOfAlias('LtiApp.assets'),
				false,
				-1,
				Yii::app()->params['forceCopyAssets']
			);
		}
		return $this->_assetsUrl;
	}

	public function onPluginDeactivate(DEvent $event){

		// We don't care about Plugins anyway, we only do this check for LtiApp!
		if (!isset($event->params['pluginName']) || $event->params['pluginName'] !== $this->name) {
			$event->return_value['canProceed'] = true;
			return;
		}

		$canProceed = true;

		$itemsStandalone = Yii::app()->db->createCommand()
			->select("COUNT(idOrg)")
			->from(LearningOrganization::model()->tableName())
			->where('objectType = :objectType', array(
				':objectType' => LearningOrganization::OBJECT_TYPE_LTI
			))
			->queryScalar();
		if($itemsStandalone > 0) $canProceed = false;

		$itemsCentralLO = Yii::app()->db->createCommand()
			->select("COUNT(id_object)")
			->from(LearningRepositoryObject::model()->tableName())
			->where('object_type = :objectType', array(
				':objectType' => LearningOrganization::OBJECT_TYPE_LTI
			))
			->queryScalar();
		if($itemsCentralLO > 0) $canProceed = false;

		if($canProceed === false){
			Yii::app()->user->setFlash('error', Yii::t('lti', 'In order to deactivate this app, you should delete all LTI Learning Objects from your courses'));
		}

		$event->return_value['canProceed'] = $canProceed;
	}

}
