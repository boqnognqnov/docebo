<?php

class LtiApp extends DoceboPlugin {

	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'LtiApp';
	public static $HAS_SETTINGS = false;

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name = __CLASS__) {
		return parent::plugin($class_name);
	}

	public function activate() {
		Yii::import('plugin.LtiApp.components.*');
		parent::activate();

		$sql = <<< SQL

SET FOREIGN_KEY_CHECKS=0;

--
-- Table structure for table `learning_lti`
--

CREATE TABLE IF NOT EXISTS `learning_lti` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `url` varchar(450) NOT NULL,
  `description` text,
  `consumer_key` varchar(255) NULL,
  `shared_secret` varchar(255) NULL,
  `share_name` tinyint(4) NOT NULL DEFAULT '0',
  `share_email` tinyint(4) NOT NULL DEFAULT '0',
  `accept_grades` tinyint(4) NOT NULL DEFAULT '0',
  `completion_type` tinyint(4) NOT NULL DEFAULT '1',
  `minimum_score` int(11) NOT NULL DEFAULT '0',
  `direct_completion` tinyint(4) NOT NULL DEFAULT '0',
  `target` varchar(20) NOT NULL,
  `author` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

SET FOREIGN_KEY_CHECKS=1;

SQL;

		$command = Yii::app()->db->createCommand($sql);
		$command->execute();
	}

	public function deactivate() {
		parent::deactivate();
	}

}
