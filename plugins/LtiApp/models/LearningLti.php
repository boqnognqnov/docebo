<?php

/**
 * This is the model class for table "learning_lti".
 *
 * The followings are the available columns in table 'learning_lti':
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property string $description
 * @property string $consumer_key
 * @property string $shared_secret
 * @property integer $share_name
 * @property integer $share_email
 * @property integer $accept_grades
 * @property integer $completion_type
 * @property integer $minimum_score
 * @property integer $direct_completion
 * @property string $target
 * @property integer $author
 */
class LearningLti extends CActiveRecord
{

	// Options to define what target the LTI resource will open in
	const LTI_TARGET_IFRAME = 'iframe';
	const LTI_TARGET_LBOX = 'lightbox';
	const LTI_TARGET_FSCREEN = 'fullscreen';
	const LTI_TARGET_WINDOW = 'window';

	const LTI_MSG_TYPE_BASIC = 'basic-lti-launch-request';
	const LTI_MSG_TYPE_RESULT = 'basic-lis-readresult';
	const LTI_VERSION_1_0 = 'LTI-1p0';

	const LTI_ROLE_LEARNER = 'Learner';
	const LTI_ROLE_INSTRUCTOR = 'Instructor';
	const LTI_ROLE_ADMIN = 'Administrator';

	const LTI_OAUTH_VERSION = '1.0';
	const LTI_OAUTH_SIGNATURE_METHOD = 'HMAC-SHA1';

	const LTI_EXT_INTEDED_USE_EMBED = 'embed';
	const LTI_EXT_RESULT_VALUE = 'decimal,passfail';

	const LTI_COMPLETE_ON_OPEN = 1;   // mark the LO as completed when it's opened
	const LTI_COMPLETE_ON_RETURN = 2; // mark the LO as completed when we get a return url (and LO is passed)

	const LTI_MAX_SCORE = 100; // For the moment, we'll just recalculate all LTI scores in percentages

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_lti';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, url, target, author', 'required'),
			array('share_name, share_email, accept_grades, completion_type, minimum_score, direct_completion, author', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>150),
			array('url', 'length', 'max'=>450),
			array('consumer_key, shared_secret', 'length', 'max'=>255),
			array('target', 'length', 'max'=>20),
			array('description', 'safe'),
			// The following rule is used by search().
			array('id, title, url, description, consumer_key, shared_secret, share_name, share_email, accept_grades, completion_type, minimum_score, direct_completion, target, author', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'url' => 'Url',
			'description' => 'Description',
			'consumer_key' => 'Consumer Key',
			'shared_secret' => 'Shared Secret',
			'share_name' => 'Share Name',
			'share_email' => 'Share Email',
			'accept_grades' => 'Accept Grades',
			'completion_type' => 'Completion Type',
			'minimum_score' => 'Minimum score',
			'direct_completion' => 'Direct completion',
			'target' => 'Target',
			'author' => 'Author',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('consumer_key',$this->consumer_key,true);
		$criteria->compare('shared_secret',$this->shared_secret,true);
		$criteria->compare('share_name',$this->share_name);
		$criteria->compare('share_email',$this->share_email);
		$criteria->compare('accept_grades',$this->accept_grades);
		$criteria->compare('completion_type',$this->completion_type);
		$criteria->compare('minimum_score',$this->minimum_score);
		$criteria->compare('direct_completion',$this->direct_completion);
		$criteria->compare('target',$this->target,true);
		$criteria->compare('author',$this->author);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningLti the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Generates a unique inter-LMS Resource Link ID
	 *
	 * @param LearningOrganization|LearningRepositoryObjectVersion $loModel
	 * @param boolean $centralRepo
	 * @return string
	 */
	public static function generateResourceLinkId($loModel, $centralRepo){
		if(!$centralRepo) {
			$idParts = array(
				0 => str_pad(Docebo::getErpInstallationId(), 6, '0', STR_PAD_LEFT),   // LMS ID
				1 => str_pad($loModel->idCourse, 4, '0', STR_PAD_LEFT),               // Course ID
				2 => str_pad($loModel->idResource, 5, '0', STR_PAD_LEFT)              // LO ID
			);
		} else {
			$idParts = array(
				0 => str_pad(Docebo::getErpInstallationId(), 6, '0', STR_PAD_LEFT),   // LMS ID
				1 => str_pad($loModel->id_object, 4, '0', STR_PAD_LEFT),              // Course ID
				2 => str_pad($loModel->id_resource, 5, '0', STR_PAD_LEFT)             // LO ID
			);
		}
		if(empty($idParts[0])) $idParts[0] = '000000';
		return implode('-', $idParts);
	}

	/**
	 * Generates a unique inter-LMS enrollment ID
	 *
	 * @param LearningOrganization|LearningRepositoryObjectVersion $loModel
	 * @param boolean $centralRepo
	 * @return string
	 */
	public static function generateEnrollmentId($loModel, $centralRepo){
		if(!$centralRepo) {
			$idParts = array(
				0 => Docebo::getErpInstallationId(),   // LMS ID
				1 => $loModel->idCourse,               // Course ID
				2 => Yii::app()->user->id,             // User ID
				3 => $loModel->idOrg                   // LO ID
			);
		} else {
			$idParts = array(
				0 => Docebo::getErpInstallationId(),   // LMS ID
				1 => $loModel->id_object,              // Course ID
				2 => Yii::app()->user->id,             // User ID
				3 => 'preview'                         // LO ID
			);
		}
		return implode('-', $idParts);
	}

	/**
	 * Generates a unique inter-LMS User ID
	 *
	 * @return string
	 */
	public static function generateLtiUserId(){
		$idParts = array(
			0 => str_pad(Docebo::getErpInstallationId(), 6, '0', STR_PAD_LEFT),   // LMS ID
			1 => str_pad(Yii::app()->user->id, 7, '0', STR_PAD_LEFT),             // User ID
		);
		if(empty($idParts[0])) $idParts[0] = '000000';
		return implode('-', $idParts);
	}

	/**
	 * Generates a unique MD5 encoded string to be used as OAuth nonce
	 *
	 * @param integer $timestamp
	 * @return string
	 */
	public static function generateOAuthNonce($timestamp){
		$nonceParts = array(
			0 => Docebo::getErpInstallationId(),
			1 => Yii::app()->user->id,
			2 => $timestamp
		);
		return md5(implode('-', $nonceParts));
	}

	/**
	 * Generates the OAuth signature for an LTI launch URL
	 *
	 * @param array $params
	 * @param string $launchUrl
	 * @param string $shared_secret
	 * @param string $method (POST / GET)
	 * @return string
	 */
	public static function generateOAuthSignature($params, $launchUrl, $shared_secret, $method = "POST") {
		// STEP 1: urlencode every key/value that will be signed
		$paramsEncoded = array();
		foreach($params as $key => $val){
			$keyEncoded = rawurlencode(trim($key));
			$valEncoded = rawurlencode(trim($val));
			$paramsEncoded[$keyEncoded] = $valEncoded;
		}

		// STEP 2: sort the array by key ASC
		ksort($paramsEncoded);

		// STEP 3: concatenate the params:
		$paramsString = "";
		foreach($paramsEncoded as $key => $val){
			$paramsString .= "&".$key."=".$val;
		}
		$paramsString = ltrim($paramsString, "&");

		// STEP 4: prepare the shared secret
		$keyParts = array(
			str_replace('+', ' ', str_replace('%7E', '~', rawurlencode($shared_secret))),
			"" //Token (unsure if required so far)
		);
		$keyString = implode('&', $keyParts);

		// STEP 5: create the signature:
		$method = strtoupper($method);
		$baseString = $method."&".rawurlencode(rtrim($launchUrl, '/'))."&".rawurlencode($paramsString);
		return base64_encode(hash_hmac('sha1', $baseString, $keyString, true));
	}

	/**
	 * Prepares the variables used for the POST request to launch an LTI app
	 *
	 * @param LearningOrganization $learningObject
	 * @param LearningLti $ltiResource
	 * @param LearningCourseuser $enrollment
	 * @param array $additionalParams
	 * @return array
	 */
	public static function getLaunchParams($learningObject, $ltiResource, $enrollment, $additionalParams = array()){
		$customLink = ($additionalParams['customLink'])?$additionalParams['customLink']:null;
		$centralRepo = ($additionalParams['centralRepo'])?$additionalParams['centralRepo']:false;

		$user = CoreUser::model()->findByPk(Yii::app()->user->id);
		$timestamp = strtotime(Yii::app()->localtime->getUTCNow());
		$roles = array(self::LTI_ROLE_LEARNER);
		if(!empty($enrollment) && $enrollment->level > LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT){
			$roles[] = self::LTI_ROLE_INSTRUCTOR;
		}
		if(!empty($enrollment) && $enrollment->level >= LearningCourseuser::$USER_SUBSCR_LEVEL_ADMIN){
			$roles[] = self::LTI_ROLE_ADMIN;
		}
		if($centralRepo){
			$roles[] = self::LTI_ROLE_INSTRUCTOR;
		}
		$target = '';
		switch($ltiResource->target){
			case self::LTI_TARGET_IFRAME:
			case self::LTI_TARGET_LBOX:
			case self::LTI_TARGET_FSCREEN:
				$target = self::LTI_TARGET_IFRAME;
				break;
			case self::LTI_TARGET_WINDOW:
				$target = self::LTI_TARGET_WINDOW;
				break;
		}
		$erpClient = new ErpApiClient2();
		$erpInfo = $erpClient->getInstallationCrmData();
		$resource_link_id = self::generateResourceLinkId($learningObject, $centralRepo);
		$enrollment_id = self::generateEnrollmentId($learningObject, $centralRepo);
		$user_id = self::generateLtiUserId();

		$launchLinkParams = array(
			'context_id' => $resource_link_id,
			'context_title' => $ltiResource->title,
			'resource_link_id' => $resource_link_id,
			'resource_link_title' => $ltiResource->title,
			'lti_version' => self::LTI_VERSION_1_0,
			'lti_message_type' => self::LTI_MSG_TYPE_BASIC,
			'oauth_consumer_key' => $ltiResource->consumer_key,
			'oauth_signature_method' => self::LTI_OAUTH_SIGNATURE_METHOD,
			'oauth_timestamp' => $timestamp,
			'oauth_nonce' => self::generateOAuthNonce($timestamp),
			'oauth_version' => self::LTI_OAUTH_VERSION,
			'oauth_callback' => 'about:blank',
			'user_id' => $user_id,
			'roles' => implode(',', $roles),
			'launch_presentation_document_target' => $target,
			'launch_presentation_return_url' => Docebo::createAbsoluteUrl('LtiApp/LtiApp/return', array(
				'return_type' => self::LTI_TARGET_IFRAME,
				'enrollment' => $enrollment_id
			)),
			'tool_consumer_instance_name' => $erpInfo['company_name'],
			'tool_consumer_instance_contact_email' => $erpInfo['contact_email'],
			'tool_consumer_instance_guid' => $_SERVER['SERVER_NAME'],
			'basiclti_submit' => Yii::t('lti', 'Launch')
		);

		// Add i-frame specific params
		if($ltiResource->target == self::LTI_TARGET_IFRAME){
			$launchLinkParams['launch_presentation_width'] = '1130';
			$launchLinkParams['launch_presentation_height'] = '680';
		}

		if($ltiResource->share_name == 1){
			$launchLinkParams['lis_person_name_given'] = $user->firstname;
			$launchLinkParams['lis_person_name_family'] = $user->lastname;
			$launchLinkParams['lis_person_name_full'] = implode(' ', array($user->firstname, $user->lastname));
		}

		if($ltiResource->share_email == 1){
			$launchLinkParams['lis_person_contact_email_primary'] = $user->email;
		}

		if($ltiResource->completion_type == self::LTI_COMPLETE_ON_RETURN){
			$launchLinkParams['lis_result_sourcedid'] = $enrollment_id;
			$launchLinkParams['lis_outcome_service_url'] = Docebo::createAbsoluteUrl('LtiApp/LtiApp/outcome', array(), "https");
			$launchLinkParams['lis_person_sourcedid'] = $user_id;
		}

		// Add OAuth Signature
		$launchLinkParams['oauth_signature'] = self::generateOAuthSignature($launchLinkParams, (!empty($customLink))?$customLink:$ltiResource->url, $ltiResource->shared_secret);

		return $launchLinkParams;
	}

	public static function verifyEnrollment($userId, $courseId){
		$model = LearningCourseuser::model()->findByAttributes(array(
			'idUser' => $userId,
			'idCourse' => $courseId
		));
		return !empty($model)?$model:false;
	}

	/**
	 * Parses a retrieved OAuth header string into an associative array
	 *
	 * @param string $headers
	 * @return array
	 */
	public static function parseOAuthHeaders($headers){
		$headers = str_ireplace('OAuth ', '', $headers);
		$headers = str_ireplace('Authorization: ', '', $headers);
		$return = array();
		$parts = explode(',', $headers);
		foreach($parts as $part){
			$parts2 = explode('=', $part);
			$return[trim($parts2[0])] = trim($parts2[1], '"');
		}
		return $return;
	}

	/**
	 * Retrieves the LTI's Shared Secret for a given Learning Object
	 *
	 * @param int $loId
	 * @return string
	 */
	public static function fetchSharedSecret($loId){
		return Yii::app()->db->createCommand()
			->select('ll.shared_secret')
			->from(LearningOrganization::model()->tableName(). " lo")
			->join(LearningLti::model()->tableName()." ll", "lo.idResource = ll.id")
			->where('idOrg = :idOrg', array(
				':idOrg' => $loId
			))
			->queryScalar();
	}
}
