<?php
class m161124_115300_UPDATE_learning_LTI_ADD_COLUMN_direct_completion extends DoceboDbMigration {

	public function safeUp()
	{
		$order66 = "ALTER TABLE `learning_lti` ADD `direct_completion` tinyint(4) NOT NULL DEFAULT '1' AFTER `minimum_score`;";
		$this->execute($order66);
		return true;
	}

	public function safeDown()
	{
		$order66 = "ALTER TABLE `learning_lti` DROP `direct_completion`;";
		$this->execute($order66);
		return true;
	}
}
