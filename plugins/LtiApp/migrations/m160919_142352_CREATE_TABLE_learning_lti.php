<?php
class m160919_142352_CREATE_TABLE_learning_lti extends DoceboDbMigration {

	public function safeUp()
	{
		$order66 = "
			CREATE TABLE IF NOT EXISTS `learning_lti` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `title` varchar(150) NOT NULL,
			  `url` varchar(450) NOT NULL,
			  `description` text,
			  `consumer_key` varchar(255) NOT NULL,
			  `shared_secret` varchar(255) NOT NULL,
			  `share_name` tinyint(4) NOT NULL DEFAULT '0',
			  `share_email` tinyint(4) NOT NULL DEFAULT '0',
			  `accept_grades` tinyint(4) NOT NULL DEFAULT '0',
			  `target` varchar(20) NOT NULL,
			  `author` int(11) NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
		$this->execute($order66);
		return true;
	}

	public function safeDown()
	{
		$order66 = "DROP TABLE IF EXISTS `learning_lti`;";
		$this->execute($order66);
		return true;
	}
	
	
}
