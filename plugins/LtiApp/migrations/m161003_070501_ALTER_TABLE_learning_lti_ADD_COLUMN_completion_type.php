<?php
class m161003_070501_ALTER_TABLE_learning_lti_ADD_COLUMN_completion_type extends DoceboDbMigration {

	public function safeUp()
	{
		$order66 = "
			ALTER TABLE `learning_lti` ADD `completion_type` TINYINT NOT NULL DEFAULT '1' AFTER `accept_grades`;
			ALTER TABLE `learning_lti` CHANGE `consumer_key` `consumer_key` VARCHAR(255) NULL;
			ALTER TABLE `learning_lti` CHANGE `shared_secret` `shared_secret` VARCHAR(255) NULL;
		";
		$this->execute($order66);
		return true;
	}

	public function safeDown()
	{
		$order66 = "
			ALTER TABLE `learning_lti` DROP `completion_type`;
			ALTER TABLE `learning_lti` CHANGE `consumer_key` `consumer_key` VARCHAR(255) NOT NULL;
			ALTER TABLE `learning_lti` CHANGE `shared_secret` `shared_secret` VARCHAR(255) NOT NULL;
		";
		$this->execute($order66);
		return true;
	}
	
	
}
