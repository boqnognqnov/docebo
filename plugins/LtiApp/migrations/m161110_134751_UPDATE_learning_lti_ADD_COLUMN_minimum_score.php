<?php
class m161110_134751_UPDATE_learning_lti_ADD_COLUMN_minimum_score extends DoceboDbMigration {

	public function safeUp()
	{
		$order66 = "ALTER TABLE `learning_lti` ADD `minimum_score` INT(11) NOT NULL DEFAULT '0' AFTER `completion_type`;";
		$this->execute($order66);
		return true;
	}

	public function safeDown()
	{
		$order66 = "ALTER TABLE `learning_lti` DROP `minimum_score`;";
		$this->execute($order66);
		return true;
	}

}
