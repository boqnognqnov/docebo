<?php

class LtiAppController extends LoBaseController {

	/**
	 * Regular expression used to check if the URL is a valid https link
	 */
	const LTI_URL_REGEX_PATTERN = "/^(https:\/\/)?(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/";

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// Opening up LtiApp/LtiApp/outcome for callbacks from Tool Providers
		$res[] = array(
			'allow',
			'users' => array('*'),
			'actions' => array('outcome'),
		);

		$admin_only_actions = array('settings');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'actions' => array('webhook'),
			'users' => array('*'),
		);


		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}



	public function beforeAction($action){
		return parent::beforeAction($action);
	}


	public function actionAxEditLTI(){
		$ltiObj = new LearningLti();

		$idResource = intval(Yii::app()->request->getParam("idResource", null));
		$idCourse = Yii::app()->request->getParam('course_id', false);
		$courseModel = LearningCourse::model()->findByPk($idCourse);

		$learningObject = null;
		if ($idResource > 0) {
			$learningObject = LearningOrganization::model()->findByAttributes(array('idOrg' => $idResource, 'objectType' => LearningOrganization::OBJECT_TYPE_LTI));
			if ($learningObject) {
				// get object
				$ltiObj = LearningLti::model()->findByPk($learningObject->idResource);
			}
		}

		//preset some values:
		if(empty($ltiObj->url)) $ltiObj->url = "https://";
		if(empty($ltiObj->target)) $ltiObj->target = LearningLti::LTI_TARGET_IFRAME;

		$html = $this->renderPartial('_edit_lti_lo', array(
			'centralRepoContext' => false,
			'ltiObj' => $ltiObj,
			'loModel'=>$learningObject,
			'courseModel'=>$courseModel,
			'lo_type'=>LearningOrganization::OBJECT_TYPE_LTI,
		), true, true);

		$response = new AjaxResult();
		$response->setStatus(true)->setHtml($html)->toJSON();
		Yii::app()->end(); // End !!!
	}

	public function actionEditCentralRepo(){
		$ltiObj = new LearningLti();

		$idResource = intval(Yii::app()->request->getParam("id_object", null));

		Yii::app()->tinymce->init();

		Yii::app()->getModule('centralrepo')->registerResources();
		$cs = Yii::app()->getClientScript();
		$playerAssetsUrl = Yii::app()->findModule('player')->assetsUrl;
		$cs->registerCssFile ($playerAssetsUrl.'/css/base.css');
		$cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/admin.css');

		$centralLoObject = null;
		$loModel = null;
		if ($idResource > 0) {
			$centralLoObject = LearningRepositoryObjectVersion::model()->findByAttributes(array('id_object' => $idResource, 'object_type' => LearningOrganization::OBJECT_TYPE_LTI));
			$loModel = LearningRepositoryObject::model()->findByAttributes(array('id_object' => $idResource, 'object_type' => LearningOrganization::OBJECT_TYPE_LTI));
			if ($centralLoObject) {
				// get object
				$ltiObj = LearningLti::model()->findByPk($centralLoObject->id_resource);
			}
		}

		//preset some values:
		if(empty($ltiObj->url)) $ltiObj->url = "https://";
		if(empty($ltiObj->target)) $ltiObj->target = LearningLti::LTI_TARGET_IFRAME;

		$this->render('_edit_lti_lo', array(
			'centralRepoContext' => true,
			'ltiObj' => $ltiObj,
			'lo_type'=>LearningOrganization::OBJECT_TYPE_LTI,
			'loModel' => $loModel,
			'containerId' => 'player-centralrepo-uploader',
			'saveUrl' => Docebo::createAbsoluteLmsUrl('LtiApp/LtiApp/saveCentralRepo'),
			'backButtonUrl' => Docebo::createAbsoluteLmsUrl('centralrepo/centralRepo/index'),
		), false);
	}

	public function actionAxSaveLo(){
		$thumb = Yii::app()->request->getParam("thumb", null);
		$shortDescription = urldecode(Yii::app()->request->getParam("short_description", ''));

		$response = new AjaxResult();
		$transaction = Yii::app()->db->beginTransaction();

		try {
			$postData = Yii::app()->request->getParam('LearningLti', null);
			$postData['title'] = trim($postData['title']);
			$postData['url'] = trim($postData['url']);
			$postData['description'] = Yii::app()->htmlpurifier->purify($postData['description'], 'allowFrameAndTargetLo');
			$postData['id'] = intval($postData['id']);
			$postData['consumer_key'] = trim($postData['consumer_key']);
			$postData['shared_secret'] = trim($postData['shared_secret']);

			$courseData = Yii::app()->request->getParam('LearningCourse', null);
			$courseId = $courseData['idCourse'];

			// Validate input parameters
			if (!$courseId) { throw new CException("Invalid specified course."); }
			if (empty($postData['title'])) { throw new CException(Yii::t('error', 'Enter a title!')); }
			if (!$this->validateLtiUrl($postData['url'])) { throw new CException(Yii::t('lti', 'Invalid LTI resource URL!')); }

			$learningObject = null;

			if ($postData['id'] > 0) { // existing record --> edit
				$learningObject = LearningOrganization::model()->findByAttributes(array(
					'objectType' => LearningOrganization::OBJECT_TYPE_LTI,
					'idResource' => $postData['id'],
					'idCourse' => $courseId
				));
				$ltiObj = LearningLti::model()->findByPk($postData['id']);
			} else { // new record --> create
				$ltiObj = new LearningLti();
				$ltiObj->author = Yii::app()->user->getIdst();
			}

			$ltiObj->title             = $postData['title'];
			$ltiObj->url               = $postData['url'];
			$ltiObj->description       = $postData['description'];
			$ltiObj->consumer_key      = $postData['consumer_key'];
			$ltiObj->shared_secret     = $postData['shared_secret'];
			$ltiObj->share_name        = $postData['share_name'];
			$ltiObj->share_email       = $postData['share_email'];
			$ltiObj->completion_type   = $postData['completion_type'];
			$ltiObj->minimum_score     = $postData['minimum_score'];
			$ltiObj->direct_completion = $postData['direct_completion'];
			$ltiObj->target            = $postData['target'];

			//do actual saving
			if (!$ltiObj->save()) {
				$errorsLTI = $ltiObj->getErrors();

				$returnErrors = Yii::t('lti', 'Error while saving LTI Resource'). ': <br /><ul class="customErrorSummary">';
				foreach($errorsLTI as $errorsLTIField) {
					if (is_array($errorsLTIField))  {
						foreach($errorsLTIField as $errorLTI) {
							$returnErrors .= '<li>' . $errorLTI . '</li>';
						}
					}
				}

				$returnErrors .= '</ul>';

				throw new CException($returnErrors);
			}

			if($learningObject){
				$learningObject->title = $ltiObj->title;
				$learningObject->short_description = Yii::app()->htmlpurifier->purify($shortDescription);
				if($thumb) $learningObject->resource = intval($thumb);
				$learningObject->saveNode();
			} else { // no record exists yet, so we've got to push a new one
				$loParams = array(
					'short_description' => $shortDescription
				);
				if($thumb) $loParams['resource'] = $thumb;
				$parentModel = LearningOrganization::model()->findByAttributes(array(
					'idCourse' => $ltiObj->id,
					'lev' => 1
				));

				$loManager = new LearningObjectsManager($courseId);
				$learningObject = $loManager->addLearningObject(LearningOrganization::OBJECT_TYPE_LTI, $ltiObj->id, $postData['title'], $parentModel->idOrg, $loParams);
			}

			$learningObject = LearningOrganization::model()->findByAttributes(array(
				'idResource' => $ltiObj->id,
				'objectType' => LearningOrganization::OBJECT_TYPE_LTI,
				'idCourse' => $courseId
			));
			if (!$learningObject) { throw new CException(Yii::t('course', 'Invalid learning object'). "(".$ltiObj->id.")"); }

			// DB Commit
			$transaction->commit();

			// Trigger event to let plugins save their own data from POST
			Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
				'loModel' => $learningObject
			)));

			$data = array(
				'resourceId' => $learningObject->idResource,
				'organizationId' => (int)$learningObject->getPrimaryKey(),
				'parentId' => (int)$learningObject->idParent,
				'title' => $ltiObj->title,
				'message' => Yii::t('standard', '_OPERATION_SUCCESSFUL')
			);
			$response->setData($data)->setStatus(true);

		} catch (CException $e) {
			$transaction->rollback();
			$response->setMessage($e->getMessage())->setStatus(false);
		}

		$response->toJSON();
		Yii::app()->end();
	}

	public function actionSaveCentralRepo(){
		$thumb = Yii::app()->request->getParam("thumb", null);
		$shortDescription = urldecode(Yii::app()->request->getParam("short_description", ''));

		$response = new AjaxResult();
		$transaction = Yii::app()->db->beginTransaction();

		try {
			$postData = Yii::app()->request->getParam('LearningLti', null);
			$postData['title'] = trim($postData['title']);
			$postData['url'] = trim($postData['url']);
			$postData['description'] = Yii::app()->htmlpurifier->purify($postData['description'], 'allowFrameAndTargetLo');
			$postData['id'] = intval($postData['id']);
			$postData['consumer_key'] = trim($postData['consumer_key']);
			$postData['shared_secret'] = trim($postData['shared_secret']);

			// Validate input parameters
			if (empty($postData['title'])) { throw new CException(Yii::t('error', 'Enter a title!')); }
			if (!$this->validateLtiUrl($postData['url'])) { throw new CException(Yii::t('lti', 'Invalid LTI resource URL!')); }

			$repoVersion = null;

			if ($postData['id'] > 0) { // existing record --> edit
				$ltiObj = LearningLti::model()->findByPk($postData['id']);
				$repoVersion = LearningRepositoryObjectVersion::model()->findByAttributes(array(
					'id_resource' => $postData['id'],
					'object_type' => LearningOrganization::OBJECT_TYPE_LTI
				));
			} else { // new record --> create
				$ltiObj = new LearningLti();
				$ltiObj->author = Yii::app()->user->getIdst();
			}

			$ltiObj->detachBehavior('learningObjects');

			$ltiObj->title           = $postData['title'];
			$ltiObj->url             = $postData['url'];
			$ltiObj->description     = $postData['description'];
			$ltiObj->consumer_key    = $postData['consumer_key'];
			$ltiObj->shared_secret   = $postData['shared_secret'];
			$ltiObj->share_name      = $postData['share_name'];
			$ltiObj->share_email     = $postData['share_email'];
			$ltiObj->completion_type = $postData['completion_type'];
			$ltiObj->minimum_score   = $postData['minimum_score'];
			$ltiObj->direct_completion = $postData['direct_completion'];
			$ltiObj->target          = $postData['target'];

			//do actual saving
			if (!$ltiObj->save()) {
				$errorsLTI = $ltiObj->getErrors();

				$returnErrors = Yii::t('lti', 'Error while saving LTI Resource'). ': <br /><ul class="customErrorSummary">';
				foreach($errorsLTI as $errorsLTIField) {
					if (is_array($errorsLTIField))  {
						foreach($errorsLTIField as $errorLTI) {
							$returnErrors .= '<li>' . $errorLTI . '</li>';
						}
					}
				}

				$returnErrors .= '</ul>';

				throw new CException($returnErrors);
			}

			if(!empty($repoVersion)){
				$repoObject = LearningRepositoryObject::model()->findByPk($repoVersion->id_object);
				$repoObject->title = $ltiObj->title;
				$repoObject->short_description = Yii::app()->htmlpurifier->purify($shortDescription);
				$repoObject->resource = intval($thumb);

				if(!$repoObject->save()){
					Log::_($repoObject->getErrors());
				}
			} else{
				$repoObject = new LearningRepositoryObject();
				$repoObject->title = $postData['title'];
				$repoObject->short_description = Yii::app()->htmlpurifier->purify($shortDescription);
				$repoObject->resource = intval($thumb);
				$categoryNodeId = LearningCourseCategoryTree::getRootNodeId();
				if(isset(Yii::app()->session['currentCentralRepoNodeId'])){
					$categoryNodeId = Yii::app()->session['currentCentralRepoNodeId'];
				}
				$repoObject->id_category = $categoryNodeId;
				$repoObject->params_json = CJSON::encode(array());
				$repoObject->object_type = LearningOrganization::OBJECT_TYPE_LTI;

				if($repoObject->save()){
					$versionModel = new LearningRepositoryObjectVersion();
					$versionModel->id_resource = $ltiObj->id;
					$versionModel->object_type = LearningOrganization::OBJECT_TYPE_LTI;
					$versionModel->version = 1;
					$versionModel->id_object = $repoObject->id_object;
					$versionModel->version_name = 'V1';
					$versionModel->version_description = '';
					$userModel = Yii::app()->user->loadUserModel();
					/**
					 * @var $userModel CoreUser
					 */
					$authorData = array(
						'idUser' => $userModel->idst,
						'username' => $userModel->getUserNameFormatted()
					);
					$versionModel->author = CJSON::encode($authorData);
					$versionModel->create_date = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');

					if(!$versionModel->save()){
						Log::_($versionModel->getErrors());
					}

				} else{
					Log::_($repoObject->getErrors());
				}
			}

			// DB Commit
			$transaction->commit();

			$data = array(
				'message' => Yii::t('standard', '_OPERATION_SUCCESSFUL')
			);

			$response->setData($data)->setStatus(true);
		} catch (CException $e) {
			Log::_($e->getMessage());
			$transaction->rollback();
			$response->setMessage($e->getMessage())->setStatus(false);
		}

		$response->toJSON();
		Yii::app()->end();
	}

	public function actionShow(){
		$id_object = Yii::app()->request->getParam("id_object", null);
		$id_course = Yii::app()->request->getParam("course_id", null);
		$display = Yii::app()->request->getParam("display", null);

		$transaction = Yii::app()->db->beginTransaction();

		try {
			$object = LearningOrganization::model()->findByPk($id_object);
			if (!$object || $object->objectType != LearningOrganization::OBJECT_TYPE_LTI) {
				throw new CException('Invalid object ID: '.$id_object);
			}

			$enrollment = LearningCourseuser::model()->findByAttributes(array(
				'idUser' => Yii::app()->user->id,
				'idCourse' => $id_course
			));

			// Learning Object ID must match the course ($this->course_id is an ID set by PlayerBaseController... always)
			if ($object->idCourse != $id_course) {
				throw new CException('Learning Object does not match the course. Are you a hacker or something.... ?');
			}

			// Now fetch LTI specific information
			$idDocument = (int)$object->idResource;
			$ltiResource = LearningLti::model()->findByPk($idDocument);
			if (!$ltiResource) {
				throw new CException('Invalid page ID: '.$idDocument);
			}

			// Preparing the LTI Launch link parameters:
			$launchLinkParams = LearningLti::getLaunchParams($object, $ltiResource, $enrollment);

			// Track the LO
			// Please note that an LTI can be either completed on load, or after the LTI redirects back to the LMS.
			$idUser = Yii::app()->user->id;
			$object = $object->getMasterForCentralLo($idUser);

			$track = LearningMaterialsTrack::model()->findByAttributes(array('idReference' => $object->getPrimaryKey(), 'idUser' => $idUser));
			if (!$track) {
				//the object has not been tracked yet for this user: create the record now
				$track = new LearningMaterialsTrack();
				$track->idUser = (int)$idUser;
				$track->idReference = (int)$object->getPrimaryKey();
				$track->idResource = (int)$ltiResource->getPrimaryKey();
			}
			switch($ltiResource->completion_type){
				case LearningLti::LTI_COMPLETE_ON_OPEN:
					$track->setStatus(LearningCommontrack::STATUS_COMPLETED);
					break;
				case LearningLti::LTI_COMPLETE_ON_RETURN:
					$track->setStatus(LearningCommontrack::STATUS_ATTEMPTED);
					break;
				default:
					throw new CException('Invalid completion type selected.');
			}
			$track->setObjectType(LearningOrganization::OBJECT_TYPE_LTI);
			if (!$track->save()) {
				throw new CException("Error while saving track data");
			}
			UserLimit::logActiveUserAccess($idUser);

			$renderParams = array(
				'launchLinkParams' => $launchLinkParams,
				'resource' => $ltiResource,
				'idReference' => $id_object,
				'object' => $object
			);

			//load content
			switch($ltiResource->target){
				case LearningLti::LTI_TARGET_IFRAME:
				case LearningLti::LTI_TARGET_LBOX:
				case LearningLti::LTI_TARGET_WINDOW:
					$html = $this->renderPartial("_show", $renderParams, true);
					break;
				case LearningLti::LTI_TARGET_FSCREEN:
					if(!empty($display) && $display == LearningLti::LTI_TARGET_FSCREEN) {
						$html = $this->renderPartial('_show', $renderParams, true);
					} else {
						$html = $this->renderPartial('_show_fullscreen', $renderParams, true);
					}
					break;
			}

			//finish action
			$transaction->commit();

			echo $html;
			Yii::app()->end();
		} catch (CException $e) {
			$transaction->rollback();
			echo $e->getMessage();
		}
	}

	public function actionReturn(){
		$enrollmentParams = Yii::app()->request->getParam("enrollment", null);
		$returnLaunchUrl = rawurldecode(Yii::app()->request->getParam("url", null));
		$enrollmentParts = explode("-", $enrollmentParams);

		$transaction = Yii::app()->db->beginTransaction();

		try {
			// Run series of test to check if the enrollment params are returned cleanly. Keeping the error message vague to discourage manipulation attempts...
			// Test 1: check if the enrollment param contains the required 4 parts: LMS ID, Course ID, User ID and LO ID
			// Test 2: check if the LMS ID hasn't been manipulated
			// Test 3: check if the User ID hasn't been manipulated
			if(
				count($enrollmentParts) != 4
				|| $enrollmentParts[0] != Docebo::getErpInstallationId()
				|| $enrollmentParts[2] != Yii::app()->user->id
			){
				throw new CException(Yii::t('lti', 'This link does not seem to work properly.'));
			}

			$id_object = $enrollmentParts[3];
			$id_course = $enrollmentParts[1];

			$object = LearningOrganization::model()->findByPk($id_object);
			if (!$object || $object->objectType != LearningOrganization::OBJECT_TYPE_LTI) {
				throw new CException('Invalid object ID: '.$id_object);
			}

			$enrollment = LearningCourseuser::model()->findByAttributes(array(
				'idUser' => Yii::app()->user->id,
				'idCourse' => $id_course
			));

			// Learning Object ID must match the course ($this->course_id is an ID set by PlayerBaseController... always)
			if ($object->idCourse != $id_course) {
				throw new CException('Learning Object does not match the course. Are you a hacker or something.... ?');
			}

			// Now fetch LTI specific information
			$idDocument = (int)$object->idResource;
			$ltiResource = LearningLti::model()->findByPk($idDocument);
			if (!$ltiResource) {
				throw new CException('Invalid page ID: '.$idDocument);
			}

			// Preparing the LTI Launch link parameters:
			$launchLinkParams = LearningLti::getLaunchParams($object, $ltiResource, $enrollment, array('customLink' => $returnLaunchUrl));

			//load content
			$html = $this->renderPartial("_show", array(
				'launchLinkParams' => $launchLinkParams,
				'resource' => $ltiResource,
				'idReference' => $id_object,
				'object' => $object,
				'returnLaunchUrl' => $returnLaunchUrl
			), true);

			//finish action
			$transaction->commit();

			echo $html;
			Yii::app()->end();
		} catch (CException $e) {
			$transaction->rollback();
			echo $e->getMessage();
			Yii::log($e->getMessage());
		}
	}

	/**
	 * Callback function from the LTI Tool
	 * The callback's body is XML
	 * The callback's headers contain the necessary OAuth validation params
	 */
	public function actionOutcome(){
		try {
			// Step 1: Fetch external content
			$headers = apache_request_headers();
			$dataPOST = trim(file_get_contents('php://input'));
			Yii::log($dataPOST, CLogger::LEVEL_WARNING);

			// Step 2: Prepare headers for authentication
			$receivedHeaders = $headers['Authorization'];
			if(empty($receivedHeaders)){
				throw new CException("No OAuth headers found.");
			}
			Yii::log($receivedHeaders, CLogger::LEVEL_WARNING);
			$receivedHeaders = LearningLti::parseOAuthHeaders($receivedHeaders);


			// Step 3: Prepare the body for authentication
			libxml_use_internal_errors(true);
			$xml = simplexml_load_string($dataPOST);
			if($xml !== false){
				$errors = libxml_get_errors();
				if(!empty($errors)){
					// We're supposed to expect a well-formed XML-file from the Tool Provider, so errors shouldn't occur
					// If needed, we can expand on the XML error logging here.
					throw new CException("There seems to be an error in the XML.");
				}
			} else {
				throw new CException("The retrieved content is no valid XML.");
			}

			// Step 4: Check the enrollment & result parameters
			if(!isset($xml->imsx_POXBody->replaceResultRequest->resultRecord->result->resultScore->textString)){
				throw new CException("Could not find result Score in XML.");
			}
			if(!isset($xml->imsx_POXBody->replaceResultRequest->resultRecord->sourcedGUID->sourcedId)){
				throw new CException("Could not find sourced ID in XML.");
			}
			$enrollment = $xml->imsx_POXBody->replaceResultRequest->resultRecord->sourcedGUID->sourcedId;
			$newScore = floatval($xml->imsx_POXBody->replaceResultRequest->resultRecord->result->resultScore->textString);
			$newScore = round($newScore * 100);
			$maxScore = LearningLti::LTI_MAX_SCORE;

			$enrollmentParts = explode('-', $enrollment);
			if(count($enrollmentParts) != 4){ // the enrollment string should be made up of 4 parts!!!
				throw new CException("Incorrect value set for sourced ID in XML.");
			}

			$lmsId = $enrollmentParts[0];
			$courseId = $enrollmentParts[1];
			$userId = $enrollmentParts[2];
			$loId = $enrollmentParts[3];

			// Step 5: Verify the authenticity of the call
			$bodyHash = base64_encode(sha1($dataPOST,true));
			$shared_secret = LearningLti::fetchSharedSecret($loId);
			$endpoint = Docebo::createAbsoluteUrl('LtiApp/LtiApp/outcome');
			$consumer = new OAuthConsumer($receivedHeaders['oauth_consumer_key'], $shared_secret);

			$request = OAuthRequest::from_consumer_and_token($consumer, '', 'POST', $endpoint, array(
				'oauth_body_hash' => $bodyHash,
				'oauth_nonce' => $receivedHeaders['oauth_nonce'],
				'oauth_version' => $receivedHeaders['oauth_version'],
				'oauth_signature_method' => $receivedHeaders['oauth_signature_method'],
				'oauth_timestamp' => $receivedHeaders['oauth_timestamp']
			));
			$request->sign_request(new OAuthSignatureMethod_HMAC_SHA1(), $consumer, '');
			$calculatedHeaders = $request->to_header();
			$calculatedHeaders = LearningLti::parseOAuthHeaders($calculatedHeaders);

			if($receivedHeaders['oauth_signature'] != $calculatedHeaders['oauth_signature']){
				// If first check doesn't match, let's retry with trimmed endpoint URL
				$endpoint = Docebo::createAbsoluteUrl('');
				$request = OAuthRequest::from_consumer_and_token($consumer, '', 'POST', $endpoint, array(
					'oauth_body_hash' => $bodyHash,
					'oauth_nonce' => $receivedHeaders['oauth_nonce'],
					'oauth_version' => $receivedHeaders['oauth_version'],
					'oauth_signature_method' => $receivedHeaders['oauth_signature_method'],
					'oauth_timestamp' => $receivedHeaders['oauth_timestamp']
				));
				$request->sign_request(new OAuthSignatureMethod_HMAC_SHA1(), $consumer, '');

				$calculatedHeaders = $request->to_header();
				$calculatedHeaders = LearningLti::parseOAuthHeaders($calculatedHeaders);

				if($receivedHeaders['oauth_signature'] != $calculatedHeaders['oauth_signature']) {
					throw new CException("OAuth Signature does not match.");
				}
			}

			// Step 6: Verify the integrity of the enrollment parts
			if($lmsId != Docebo::getErpInstallationId()){
				throw new CException("LMS IDs does not match.");
			}

			if(!$enrollmentModel = LearningLti::verifyEnrollment($userId, $courseId)){
				throw new CException("There is no enrollment found for userId {$userId} and courseId {$courseId}.");
			}

			$object = LearningOrganization::model()->findByAttributes(array(
				'idOrg' => $loId,
				'objectType' => LearningOrganization::OBJECT_TYPE_LTI
			));
			if(empty($object)){
				throw new CException("No LTI Learning Object could be found for LO ID {$loId}.");
			}

			$ltiResource = LearningLti::model()->findByPk($object->idResource);
			if(empty($ltiResource)){
				throw new CException("No matching LTI Resource could be found for LO ID {$loId}.");
			}

			// Step 7: Add the tracking
			$object = $object->getMasterForCentralLo($userId);

			$track = LearningMaterialsTrack::model()->findByAttributes(array('idReference' => $loId, 'idUser' => $userId));
			if (!$track) {
				//the object has not been tracked yet for this user: create the record now
				$track = new LearningMaterialsTrack();
				$track->idUser = (int)$userId;
				$track->idReference = (int)$object->getPrimaryKey();
				$track->idResource = (int)$ltiResource->getPrimaryKey();
			}

			$track->setObjectType(LearningOrganization::OBJECT_TYPE_LTI);

			$track->updateCommontrackScore($newScore, $maxScore, $ltiResource->direct_completion, LearningCommontrack::UPDATE_STATUS_IMPROVE_ONLY, $ltiResource->minimum_score);

			if (!$track->save()) {
				throw new CException("Error while saving track data");
			}
			UserLimit::logActiveUserAccess($userId);

			Yii::log("LTI enrollment successfully updated: courseId {$courseId} / userId {$userId} / loId {$loId}", CLogger::LEVEL_INFO, 'LTI App');
		} catch(CException $e){
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR, 'LTI App');
			var_dump($e->getMessage());
		}
	}

	public function validateLtiUrl($url){
		return preg_match(self::LTI_URL_REGEX_PATTERN, $url);
	}
}