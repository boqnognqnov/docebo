<?php
/**
 * Blue Jeans Video communications - http://bluejeans.com/
 * API Client
 *
 * @author Kurt De Ridder
 *
 * Copyright (c) 2016 Docebo
 * http://www.docebo.com
 *
 */
class BlueJeansApiClient extends CComponent {

	// Can be used to debug the traffic
	public $proxyHost = false;
	public $proxyPort = false;

	// will hold the EHttpClient
	private $_client = null;

	// MUST BE HTTPS: !!
	private $_baseDomain = 'https://api.bluejeans.com';

	private $_joinUrlPrefix = 'https://bluejeans.com/';

	// Will be taken from settings
	private $_app_key = '';
	private $_app_secret = '';
	private $_user_name = '';
	private $_user_pass = '';
	private $_user_token = '';
	private $_meeting_token = '';
	private $_enterprise_token = '';
	private $_user_id = '';
	private $_enterprise = '';


	public function __construct($idAccount = null) {
		// If the new parameters are not passed, try to load the default webinar tools account
		if(!$idAccount) {
			$account = WebinarToolAccount::model()->findByAttributes(array('tool' => 'bluejeans'));
			if($account) {
				$idAccount = $account->id_account;
//				$app_secret = $account->settings->app_secret;
			}
		}
		$this->init($idAccount);
	}

	/**
	 *
	 * @throws Exception
	 */
	public function init($idAccount = null) {

		Yii::import('common.extensions.httpclient.*');

		if(empty($idAccount)){
			throw new Exception("App key not set");
		}

		$account = WebinarToolAccount::model()->findByPk($idAccount);

		$this->_app_key = $account->settings->app_key;
		$this->_app_secret = $account->settings->app_secret;
		$this->_user_name = $account->settings->user_name;
		$this->_user_pass = $account->settings->user_pass;
		if (empty($this->_user_name)) {
			throw new Exception("Credentials not set");
		}
		if(empty($account->settings->user_token) || time() > $account->settings->user_token_exp){
			$newAccessToken = $this->getAccessToken('user');
			$account['settings']->user_token = $newAccessToken['data']['access_token'];
			$account['settings']->user_token_exp = time() + $newAccessToken['data']['expires_in'] - 1;
			$account['settings']->user_id = $newAccessToken['data']['scope']['user'];
			$account->save();
			$this->_user_token = $newAccessToken['data']['access_token'];
			$this->_user_id = $newAccessToken['data']['scope']['user'];
		} else {
			$this->_user_token = $account->settings->user_token;
			$this->_user_id = $account->settings->user_id;
		}
	}

	/**
	 * Acquires an access token from Blue Jeans OAuth server
	 *
	 * @param string $scope user|meeting|enterprise
	 * @param array $params: Only used for meeting scope
	 * @throws Exception
	 * @return array
	 */
	public function getAccessToken($scope = 'user', $params = array()) {
		$url = $this->_baseDomain . "/oauth2/token";

		$this->_client = new EHttpClient($url, array(
			'adapter'      => 'EHttpClientAdapterCurl',
			'proxy_host'   => $this->proxyHost,
			'proxy_port'   => $this->proxyPort,
			'disable_ssl_verifier' => true,
		));

		switch($scope) {
			case 'meeting':
				if(empty($params['meetingId'])){
					throw new Exception('No meeting ID submitted for token request.');
				}
				$params = array(
					'grant_type' => 'meeting_passcode',
					'meetingNumericId' => $params['meetingId']
				);
				if(!empty($params['passcode'])){
					$params['meetingPasscode'] = $params['passcode'];
				}
				break;
			case 'enterprise':
				$params = array(
					'grant_type' => 'client_credentials',
					'client_id' => $this->_app_key,
					'client_secret' => $this->_app_secret
				);
				break;
			case 'user':
			default:
				$params = array(
					'grant_type' => 'password',
					'username' => $this->_user_name,
					'password' => $this->_user_pass
				);
				break;
		}

		$this->_client->setRawData(json_encode($params), 'application/json');
		$response = $this->_client->request(EHttpClient::POST);

		// Check if we've got an error; throw exception with error message
		if ($this->_client->getLastResponse()->isError()) {
			throw new Exception($this->_client->getLastResponse()->getMessage(), $this->_client->getLastResponse()->getStatus());
		}

		if (!$response) {
			throw new Exception('Invalid response from BlueJeans OAuth server');
		}

		// Decode response to an array
		$body = $response->getBody();

		// If we get body, it must be JSON and be an array
		$response_data = array();
		if ($body) {
			$response_data = CJSON::decode($body);
			if ( ($response_data == null) || !is_array($response_data) ) {
				throw new Exception('Invalid data format received from OAuth server');
			}
		}

		// Always add response status/message
		$result["response_status"] = $this->_client->getLastResponse()->getStatus();
		$result["response_message"] = $this->_client->getLastResponse()->getMessage();
		$result["data"] = $response_data;

		return $result;
	}

	/**
	 * @param string $scope user|meeting|enterprise
	 * @return string
	 */
	public function getToken($scope = 'user'){
		switch($scope){
			case 'enterprise':
				return $this->_enterprise_token;
				break;
			case 'meeting':
				return $this->_meeting_token;
				break;
			case 'user':
				return $this->_user_token;
				break;
		}
	}

	/**
	 * @param string $actionPath
	 * @param string $method
	 * @param string $scope user|meeting|enterprise
	 * @param null|array $params
	 * @param null|array $getString optional additional GET params (besides access_token)
	 * @throws Exception
	 * @return array
	 */
	private function call($actionPath, $method, $scope, $params=null, $getParams=null) {
		switch($scope){
			case 'enterprise':
				$restPath = '/v1/enterprise/';
				$scopeId = $this->_enterprise;
				$token = $this->_enterprise_token;
				break;
			case 'meeting':
				$restPath = '/v1/meeting/';
				$scopeId = '';
				$token = $this->_meeting_token;
				break;
			case 'user':
				$restPath = '/v1/user/';
				$scopeId = $this->_user_id;
				$token = $this->_user_token;
				break;
		}

		$getString = "";
		if(!empty($getParams) && is_array($getParams)){
			foreach($getParams as $k => $v){
				$getString .= "&".$k."=".$v;
			}
		}
		// Build URL
		$url = $this->_baseDomain . $restPath . $scopeId . $actionPath . "?access_token=" . $token . $getString;
//		var_dump($url);die;

		$this->_client = new EHttpClient($url, array(
			'adapter'      => 'EHttpClientAdapterCurl',
			'proxy_host'   => $this->proxyHost,
			'proxy_port'   => $this->proxyPort,
			'disable_ssl_verifier' => true,
		));

		// Add Content length header
		$json_data = "";
		if (is_array($params)) {
			$json_data = CJSON::encode($params);
			$this->_client->setHeaders('Content-Length', strlen($json_data));
		}

		// Add other headers
		$this->_client->setHeaders('Accept', "application/json");
		$this->_client->setHeaders('Content-Type', "application/json");

		// Add raw data, if any
		if (!empty($json_data)) {
			$this->_client->setRawData($json_data);
		}

		// Make the call, get the response
		$response = $this->_client->request($method);

		if($response){
			Yii::log('Sending params to BlueJeans API: '.print_r($params,true), 'debug');
		}

		// Check if we've got an error; throw exception with error message
		if ($this->_client->getLastResponse()->isError()) {
			throw new Exception($this->_client->getLastResponse()->getMessage(), $this->_client->getLastResponse()->getStatus());
		}

		if (!$response) {
			throw new Exception('Invalid response from server');
		}

		// Decode response to an array
		$body = $response->getBody();
		// If we get body, it must be JSON and be an array
		$response_data = array();
		if ($body) {
			$response_data = CJSON::decode($body);
			if ( ($response_data === null) || !is_array($response_data) ) {
				throw new Exception('Invalid data format received from server');
			}
		}

		// Always add response status/message
		$result["response_status"] = $this->_client->getLastResponse()->getStatus();
		$result["response_message"] = $this->_client->getLastResponse()->getMessage();
		$result["data"] = $response_data;

		// Return array
		return $result;

	}

	/**
	 * Create a meeting
	 *
	 * @param string $roomName
	 * @param string $roomDescription
	 * @param string $startDateTime
	 * @param string $endDateTime
	 * @return array
	 */
	public function createMeeting($roomName, $roomDescription, $startDateTime, $endDateTime) {
		$startDateTime = strtotime($startDateTime)*1000;
		$endDateTime = strtotime($endDateTime)*1000;

		$params = array(
			'title' => $roomName,
			'description' => $roomDescription,
			'start' => $startDateTime,
			'end' => $endDateTime,
			'timezone' => CoreSetting::getEntity('timezone_default')->param_value,
			'endPointType' => 'Browser',
			'endPointVersion' => ''
		);

		$result = $this->call('/scheduled_meeting', EHttpClient::POST, 'user', $params);
		//todo: add join url to the call
//		$joinUrlResponse = $this->getStartTrainingUrl($result['data']);
//		$result['join_url'] = $joinUrlResponse['data'];
		$result['join_url'] = $this->_joinUrlPrefix.$result['data']['numericMeetingId'];
		return $result;
	}

	/**
	 * Update a meeting
	 *
	 * @param string $roomId
	 * @param string $roomName
	 * @param string $roomDescription
	 * @param string $startDateTime
	 * @param string $endDateTime
	 * @throws Exception
	 * @return array
	 */
	public function updateMeeting($roomId, $roomName, $roomDescription, $startDateTime, $endDateTime) {
		$roomId = trim($roomId);

		if (empty($roomId)) {
			throw new Exception('Invalid meeting Id provided');
		}

		$startDateTime = strtotime($startDateTime)*1000;
		$endDateTime = strtotime($endDateTime)*1000;

		$params = array(
			'title' => $roomName,
			'start' => $startDateTime,
			'end' => $endDateTime,
		);

		$result = $this->call('/scheduled_meeting/'.$roomId, EHttpClient::PUT, 'user', $params);
		return $result;
	}

	/**
	 * Deletes a meeting by ID
	 *
	 * @param string $roomId
	 * @throws Exception
	 * @return array
	 */
	public function deleteMeeting($roomId) {
		$roomId = trim($roomId);
		if (empty($roomId)) {
			throw new Exception('Invalid meeting Id provided');
		}
		try {
			$result = $this->call('/scheduled_meeting/'.$roomId, EHttpClient::DELETE, 'user');
		} catch(Exception $e) {
			if($e->getCode() == 404) { //already deleted
				Yii::log('BlueJeans API error: '.$e->getMessage());
				return true;
			}else
				throw new Exception($e->getMessage(), $e->getCode());
		}
		return $result;
	}

	/**
	 * Returns the "join url" for the passed user. The API first register the user to the training.
	 * @param $roomId
	 * @return string
	 */
	public function getJoinMeetingUrl($roomId) {
		try {
			$roomId = trim($roomId);
			if (empty($roomId))
				throw new Exception('Invalid Meeting Id provided');

			$result = $this->call('/scheduled_meeting/'.$roomId, EHttpClient::GET, 'user');
			if($result){
				return $this->_joinUrlPrefix.$result['data']['numericMeetingId'];
			} else {
				throw new Exception('Could not retrieve meeting information');
			}
		} catch(Exception $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			return null;
		}
	}

	public function getMeetingRecording($meetingId){
		$errors = array();
		try{
			if(empty($meetingId)){
				throw new Exception('Invalid meeting Id provided');
			}

			$result1 = $this->call('/meeting_history/'.$meetingId.'/recordings', EHttpClient::GET, 'user');
			$contentId = null;

			if(!empty($result1['data'][0]['recordingSessions'])){ // there is recording content to be found, now let's try to fetch the right one and get the contentId
				foreach($result1['data'][0]['recordingSessions'] as $recordingSession){
					if(($recordingSession['contentStatus'] == 'PROCESSED') && ($recordingSession['recordingType'] == 'MEDIA')){
						$contentId = $recordingSession['contentId'];
					}
				}
				if(!empty($contentId)){
					$getParams = array(
						'isDownloadable' => 'true'
					);
					$result2 = $this->call('/cms/'.$contentId, EHttpClient::GET, 'user', null, $getParams);
					if(empty($result2['data']['contentUrl'])){
						throw new Exception('Could not retrieve download link for this session');
					}
					return $result2;
				} else {
					throw new Exception('No finished recordings found for this session. Please verify if your video is finished converting.');
				}
			} else {
				throw new Exception('No recordings found for this session');
			}
			return $result;
		} catch(Exception $e) {
			$errors[] = $e->getMessage();
			return $errors;
		}
	}
}

?>