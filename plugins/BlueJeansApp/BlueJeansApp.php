<?php

/**
 * Plugin for BlueJeansApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class BlueJeansApp extends DoceboPlugin {


	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'BlueJeansApp';

	/**
	 * Returns the settings page (admin app)
	 */
	public static function settingsUrl() {
		return Docebo::createAdminUrl('BlueJeansApp/blueJeansApp/settings');
	}

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {
		parent::activate();
	}


	public function deactivate() {
		parent::deactivate();
	}
}
