<?php

class BlueJeansAppController extends Controller {


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		$admin_only_actions = array('settings', 'editAccount');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}

	/**
	 * Renders the edit webinar account form for blue jeans
	 */
	public function actionEditAccount($id_account = null) {
		// Load the webinar account
		if(!$id_account) {
			$account = new WebinarToolAccount();
			$account->tool = 'bluejeans';
		} else
			$account = WebinarToolAccount::model()->findByPk($id_account);

		// Load the settings form
		$settings = new BlueJeansAppSettingsForm();
		if (isset($_POST['BlueJeansAppSettingsForm'])) {
			$settings->setAttributes($_POST['BlueJeansAppSettingsForm']);
			if ($settings->validate()) {
				$accountSettings = array();
				$accountSettings['app_key'] = $settings->app_key;
				$accountSettings['app_secret'] = $settings->app_secret;
				$accountSettings['user_name'] = $settings->user_name;
				$accountSettings['user_pass'] = $settings->user_pass;
				$accountSettings['max_rooms'] = $settings->max_rooms;
				$accountSettings['max_rooms_per_course'] = $settings->max_rooms_per_course;
				$accountSettings['max_concurrent_rooms'] = $settings->max_concurrent_rooms;
				$account->name = $settings->account_name;
				$account->additional_info = $settings->additional_info;
				$account->settings = $accountSettings;
				$account->save();

				// Close the modal
				echo '<a class="auto-close"></a>';
				Yii::app()->end();
			}
		}

		// Load account settings in form
		$accountSettings = $account->settings;
		$settings->account_name = $account->name;
		$settings->app_key = $accountSettings->app_key;
		$settings->app_secret = $accountSettings->app_secret;
		$settings->user_name = $accountSettings->user_name;
		$settings->user_pass = $accountSettings->user_pass;
		$settings->max_rooms = $accountSettings->max_rooms;
		if (empty($settings->max_rooms) && !$id_account)
			$settings->max_rooms = intval(Yii::app()->params['videoconf_max_rooms_default']);
		$settings->max_rooms_per_course = $accountSettings->max_rooms_per_course;
		if (empty($settings->max_rooms_per_course) && !$id_account)
			$settings->max_rooms_per_course = intval(Yii::app()->params['videoconf_max_rooms_per_course_default']);
		$settings->max_concurrent_rooms = $accountSettings->max_concurrent_rooms;
		if (empty($settings->max_concurrent_rooms) && !$id_account)
			$settings->max_concurrent_rooms = intval(Yii::app()->params['videoconf_max_concurrent_rooms_default']);
		$settings->additional_info = $account->additional_info;

		// Render the edit account view in the modal
		$this->renderPartial('_account_settings', array(
			'modalTitle' => $account->id_account ? Yii::t('webinar', 'Edit webinar account') : Yii::t('webinar', 'New webinar account'),
			'settings' => $settings
		));
	}

	/**
	 * Renders the main settings page
	 */
	public function actionSettings() {
		Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl . '/css/webinar-setup.css');

		$settings = new BlueJeansAppSettingsForm();
		$this->render('settings', array('settings' => $settings));
	}
}
