<?php

/**
 * Class BlueJeansWebinarTool
 * Implemenents webinar tool for Blue Jeans
 */
class BlueJeansWebinarTool extends WebinarTool {
	/**
	 * @var bool does the current webinar tool supports grabbing a recording via API call?
	 */
	protected $apiGrabSupported = true;

	/**
	 * Main constructor
	 */
	protected function __construct() {
		$this->id = 'bluejeans';
		$this->name = 'BlueJeans';
	}

	/**
	 * Returns true if the current webinar tools has support for multi account
	 * @return bool
	 */
	public function hasMultiAccountSupport()
	{
		return true;
	}

	public function createRoom($roomName, $roomDescription, $idAccount, $startDateTime, $endDateTime, $maxParticipants, $api_params = array()) {
		$apiClient = new BlueJeansApiClient($idAccount);
		$meeting = $apiClient->createMeeting($roomName, $roomDescription, $startDateTime, $endDateTime);

		if (!isset($meeting['data']))
			throw new CHttpException(500, ConferenceManager::$MSG_INVALID_DATA_RETURNED);

		$result = array(
			'name' => $roomName,
			'meeting_id' => $meeting['data']['id'],
			'numeric_meeting_id' => $meeting['data']['numericMeetingId'],
			'attendee_passcode' => $meeting['data']['attendeePasscode'],
			'join_url' => urlencode($meeting['join_url'])
		);

		return $result;
	}

	public function updateRoom($roomName, $roomDescription, $idAccount, $api_params = array(), $startDateTime, $endDateTime, $maxParticipants) {
		$apiClient = new BlueJeansApiClient($idAccount);
		$apiClient->updateMeeting($api_params['meeting_id'], $roomName, $roomDescription, $startDateTime, $endDateTime);
		$api_params['name'] = $roomName;
		return $api_params;
	}

	public function deleteRoom($idAccount, $api_params = array()) {
		$meetingId = $api_params['meeting_id'];
		$account = $this->getAccountById($idAccount, true);
		if($account !== false) {
			$apiClient = new BlueJeansApiClient($idAccount);
			$apiClient->deleteMeeting($meetingId);
		}
	}

	public function getJoinRoomUrl($roomName, $maxParticipants, $idAccount, $api_params, $userModel, $sessionModel, $widgetContext = false) {
		$meetingId = $api_params['meeting_id'];
		$apiClient = new BlueJeansApiClient($idAccount);
		$result = $apiClient->getJoinMeetingUrl($meetingId);
		return $result;
	}

	public function getStartRoomUrl($roomName, $maxParticipants, $idAccount, $api_params, $userModel, $sessionModel) {
		$meetingId = $api_params['meeting_id'];
		$apiClient = new BlueJeansApiClient($idAccount);
		$result = $apiClient->getJoinMeetingUrl($meetingId);
		return $result;
	}

	/**
	 * @param int $idAccount $idAccount The id of the account to use
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @return array
	 */
	public function getApiRecordings($idAccount, $api_params){
		$return = array(
			'error' => '',
			'recordingUrl' => ''
		);

		$apiClient = new BlueJeansApiClient($idAccount);
		$result = $apiClient->getMeetingRecording($api_params['numeric_meeting_id']);

		if(!empty($result['data']['contentUrl'])){
			$return['recordingUrl'] = $result['data']['contentUrl'];
		} else {
			$return['error'] = Yii::t('webinar', 'Could not find a recording for this session');
		}
		return $return;
	}
}