<?php

/**
 * BlueJeansAppSettingsForm class.
 *
 * @property string $account_name
 * @property string $max_rooms
 * @property string $max_rooms_per_course
 * @property string $app_key
 * @property string $app_secret
 * @property string $user_name
 * @property string $user_pass
 * @property string $max_concurrent_rooms
 * @property string $additional_info
 */
class BlueJeansAppSettingsForm extends CFormModel {

	public $account_name;
	public $max_rooms;
	public $max_rooms_per_course;
	public $app_key;
	public $app_secret;
	public $user_name;
	public $user_pass;
	public $max_concurrent_rooms;
	public $additional_info;


	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			array('account_name, max_rooms, max_rooms_per_course, additional_info, app_key, app_secret, user_name, user_pass, max_concurrent_rooms', 'safe'),
			array('account_name, max_rooms, max_rooms_per_course, app_key, app_secret, user_name, user_pass, max_concurrent_rooms', 'required'),
			array('max_concurrent_rooms, max_rooms, max_rooms_per_course', 'numerical'),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array(
			'max_rooms' => Yii::t('configuration','Max total rooms'),
			'max_rooms_per_course' => Yii::t('configuration','Max rooms per course'),
			'max_concurrent_rooms' => Yii::t('configuration','Max concurrent rooms'),
			'app_key' => Yii::t('configuration','App key'),
			'app_secret' => Yii::t('configuration','App secret'),
			'user_name' => Yii::t('configuration','Webinar tool username'),
			'user_pass' => Yii::t('configuration','Webinar tool password'),
			'additional_info' => Yii::t('webinar', 'Additional information'),
			'account_name' => Yii::t('webinar', 'Account Name')
		);
	}
}
