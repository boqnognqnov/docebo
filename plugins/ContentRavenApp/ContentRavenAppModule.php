<?php

class ContentRavenAppModule extends CWebModule
{
    /**
     * @see CModule::init()
     */
    public function init()
    {
    }

    /**
     * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
     */
    public function menu()
    {
        // Add menu voices !
        Yii::app()->mainmenu->addAppMenu('contentRaven', array(
            'app_initials' => 'CR',
            'settings' => Docebo::createAdminUrl('ContentRavenApp/Settings'),
            'label' => 'Content Raven',
        ), array());
    }

}