<?php

class ContentRavenApp extends DoceboPlugin {

    // Name of the plugin; needed to identify plugin folder and entry script
    public $plugin_name = 'ContentRavenApp';
    public static $HAS_SETTINGS = false;

    /**
     * This will allow us to access "statically" (singleton) to plugin methods
     * @param string $class_name
     * @return Plugin Object
     */
    public static function plugin($class_name = __CLASS__) {
        return parent::plugin($class_name);
    }

    public function activate() {
        parent::activate();
    }

    public function deactivate() {
        parent::deactivate();
    }

}
