<?php
$_breadcrumbs = array(
    Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
    'Content Raven',
    Yii::t('standard','Settings')
);

$this->breadcrumbs = $_breadcrumbs;
?>

<div class="row-fluid">
    <div class="span6">
        <h3 class="advanced-elucidat-settings-form-title">Content Raven</h3>
    </div>
    <div class="span6">
        <a href="<?= ('it' == Yii::app()->getLanguage()) ? 'https://www.docebo.com/it/knowledge-base' : 'https://www.docebo.com/knowledge-base' ?>" target="_blank" class="app-link-read-manual">
            <?= Yii::t('apps', 'Read Manual'); ?>
        </a>
    </div>
</div>
<br>

<?php DoceboUI::printFlashMessages(); ?>

<div class="advanced-main" id="content-raven-settings-form">
    <?php
    $form = $this->beginWidget( 'CActiveForm', array(
        'id'          => 'app-settings-form',
        'htmlOptions' => array( 'class' => 'form-horizontal' ),
    ) );
    ?>

    <div class="section">

        <h3 class="advanced-catalog-settings-form-title">Content Raven</h3>
        <br/>

        <div class="row odd">
            <div class="row">
                <div class="setting-name">
                    <?= $form->labelEx( $settings, 'content_raven_url' ); ?>
                </div>
                <div class="values">
                    <?= $form->textField( $settings, 'content_raven_url' ); ?>
                    <?= $form->error( $settings, 'content_raven_url' ); ?>
                </div>
            </div>
        </div>

        <div class="row even">
            <div class="row">
                <div class="setting-name">
                    <?= $form->labelEx( $settings, 'token' ); ?>
                </div>
                <div class="values">
                    <?= $form->textField( $settings, 'token', array('class' => 'token')); ?>
                    <?= $form->error( $settings, 'token' ); ?>
                </div>
            </div>
        </div>

        <div class="row odd">
            <div class="row">
                <div class="setting-name">
                    <?= $form->labelEx( $settings, 'sender_id' ); ?>
                </div>
                <div class="values">
                    <?= $form->textField( $settings, 'sender_id' ); ?>
                    <?= $form->error( $settings, 'sender_id' ); ?>
                </div>
            </div>
        </div>

        <div class="form-actions">
            <?= CHtml::submitButton( Yii::t( 'standard', '_SAVE' ), array( 'class' => 'btn-docebo green big' ) ); ?>
            &nbsp;
            <a href="<?= Docebo::getUserHomePageUrl(); ?>" class="btn btn-docebo black big"><?= Yii::t( 'standard', '_CANCEL' ); ?></a>
        </div>

    </div>

    <?php $this->endWidget(); ?>

</div>