<?php

class SettingsController extends  Controller
{
    /**
     * @return array action filters
     */
    public function filters () {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }


    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules () {
        $res = array();

        // keep it in the following order:
        $admin_only_actions = array( 'index' );
        $res[]              = array(
            'allow',
            'roles'   => array( Yii::app()->user->level_godadmin ),
            'actions' => $admin_only_actions,
        );

        // deny admin only actions to all other users:
        $res[] = array(
            'deny',
            'actions' => $admin_only_actions,
        );

        // Allow access to other actions only to logged-in users:
        $res[] = array(
            'allow',
            'users' => array( '@' ),
        );

        $res[] = array(
            'deny', // deny all
            'users' => array( '*' ),
        );

        return $res;
    }

    public function actionIndex(){
        $settings = new ContentRavenSettingsForm();
        // Extract some data from the DB
        $settings->content_raven_url    = PluginSettings::get('content_raven_url');
        $settings->token                = PluginSettings::get('token');
        $settings->sender_id            = PluginSettings::get('sender_id');

        if (isset($_POST['ContentRavenSettingsForm'])) {
            $settings->setAttributes($_POST['ContentRavenSettingsForm']);

            if ($settings->validate()) {

                // If the data is validated then let's save it
                PluginSettings::save('content_raven_url',   $settings->content_raven_url);
                PluginSettings::save('token',               $settings->token);
                PluginSettings::save('sender_id',           $settings->sender_id);

                Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));
            }
            else {
                Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
            }
        }

        Yii::app()->clientScript->registerCss('zendesk-settings-form', '
			#content-raven-settings-form.advanced-main {
				margin-left: 0;
			}
			#content-raven-settings-form input[type="text"]:not(.token) {
			    width: 98%;
			}
			#content-raven-settings-form input.token {
			    width: 96%;
			}
			#content-raven-settings-form .form-actions {
			    border: none;
			    background-color: white;
			    text-align: right;
			    padding-right: 0px;
			}
		');
        Yii::app()->clientScript->registerScript('zendesk-settings-form', '
			$("input,select").styler();
		');

        Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/admin.css');

        $this->render('index', array(
            'settings'=>$settings,
        ));
    }
}