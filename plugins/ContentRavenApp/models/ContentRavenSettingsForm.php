<?php

class ContentRavenSettingsForm extends CFormModel
{
    public $content_raven_url;
    public $token;
    public $sender_id;

    /**
     * Declares the validation rules.
     */
    public function rules () {
        return array(
            array( 'content_raven_url, token, sender_id', 'required' ),
            array( 'content_raven_url, token, sender_id', 'safe' ),
        );
    }


    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels () {
        return array(
            'content_raven_url' => 'Content Raven' . " " . Yii::t('standard', '_URL'),
            'token' => Yii::t( 'salesforce', 'Token' ),
            'sender_id' => Yii::t( 'standard', '_SENDER' ) . " " . strtoupper(Yii::t("admin_directory", "_DIRECTORY_GROUPID")),
        );
    }
}