<?php

class SubscriptionCodesAppController extends Controller {

/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		$admin_only_actions = array(
			'index', 'editSet', 'toggleSetStatus', 'deleteSet',
			'showLogs', 'addCodes', 'addCodesDialog', 'deleteCodesFromSet',
			'assignCourses', 'unassignCourses', 'assignCoursesToSet',
			'AxCodeLogsAutocomplete',  'AxAddCodesAutocomplete',  'axAssignCoursesAutocomplete',
		);
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller::init()
	 */
    public function init()
    {
        parent::init();
        $this->registerResources();
    }

    /**
     * Register module specific assets
     */
    public function registerResources()
    {
    	if (!Yii::app()->request->isAjaxRequest) {
    		$cs = Yii::app()->getClientScript();
    		$cs->registerCssFile($this->getModule()->getAssetsUrl().'/css/subscriptioncodes.css');
    		$cs->registerScriptFile($this->getModule()->getAssetsUrl().'/js/subscriptioncodes.js');
    		Yii::app()->bootstrap->registerAssetCss('bootstrap-datepicker.css');
    		Yii::app()->bootstrap->registerAssetJs('bootstrap.datepicker.js');
    	}
    }




    /**
     * Main page (Sets)
     */
    public function actionIndex() {

    	$params = array(
    		'setModel' => new SubscriptioncodesSet(),
    	);

    	$this->render('index', $params);
    }


    /**
     * Create/Edit a Set
     */
    public function actionEditSet() {

    	$idSet 		= Yii::app()->request->getParam('idSet', false);
    	$confirm 	= Yii::app()->request->getParam('confirm', false);

    	if (!$idSet) {
    		$setModel = new SubscriptioncodesSet();
    	}
    	else {
    		$setModel = SubscriptioncodesSet::model()->findByPk($idSet);
    	}

    	// Confirm buttong clicked
    	if ($confirm) {
    		$setModel->attributes = $_REQUEST['SubscriptioncodesSet'];
    		if ($setModel->validate()) {
    			if ($setModel->save(false)) {
    				echo '<a class="auto-close success">';
    				Yii::app()->end();
    			}
    		}
    	}

		// Otherwise...
    	$params = array(
    		'setModel' => $setModel,
    	);

    	$html = $this->renderPartial('_editSet', $params);

    	echo $html;
    	Yii::app()->end();

    }



    /**
     * Activate/Deactivate a single Set. No dialogs.
     * Toogles the SET status and returns the newly acquired one.
     */
    public function actionToggleSetStatus() {

    	// Get Model of the calling Set (by idSet), check it, then change status, save and return new one
    	$idSet = Yii::app()->request->getParam('idSet', false);
    	$model = SubscriptioncodesSet::model()->findByPk($idSet);
    	$res = new AjaxResult();
    	if (!$model) {
    		$res->setStatus(false)->toJSON();
    		Yii::app()->end();
    	}

    	$model->is_active = $model->is_active == 1 ? 0 : 1;
    	$model->save(false);

    	$data = array(
    		'status' => $model->is_active,
    	);
    	$res->setStatus(true)->setData($data);
    	$res->toJSON();

    }

    /**
     * Delete single set
     */
    public function actionDeleteSet() {

    	$idSet = Yii::app()->request->getParam('idSet', false);
    	$confirm = Yii::app()->request->getParam('confirm', false);

    	if ($confirm) {
    		SubscriptioncodesSet::model()->deleteByPk($idSet);
    		echo '<a class="auto-close success">';
    		Yii::app()->end();
    	}

    	$setModel = SubscriptioncodesSet::model()->findByPk($idSet);
    	$this->renderPartial('_delete_set_dialog', array(
    			'setModel' => $setModel,
    	));

    	Yii::app()->end();

    }

    /**
     * Show "Confirm" dialog2 to delete one or more codes from a set
     */
    public function actionDeleteCodesFromSet() {

    	$idSet = Yii::app()->request->getParam('idSet', false);
    	$selectedItems = Yii::app()->request->getParam('selectedItems', false);
    	$confirm = Yii::app()->request->getParam('confirm', false);

    	if ($confirm) {
    		$criteria = new CDbCriteria();
    		$criteria->addCondition('id_set=:idSet');
    		$criteria->addInCondition('code', explode(',', $selectedItems));
    		$criteria->params[':idSet'] = $idSet;
    		SubscriptioncodesCode::model()->deleteAll($criteria);
    		echo '<a class="auto-close success">';
    		Yii::app()->end();
    	}

    	$setModel = SubscriptioncodesSet::model()->findByPk($idSet);
    	$this->renderPartial('_delete_codes_dialog', array(
    		'setModel' => $setModel,
    		'selectedItems' => $selectedItems,
    	));

    	Yii::app()->end();

    }


    /**
     * Show Codes "consumption" logs
     */
    public function actionShowLogs() {
    	$codeModel = new SubscriptioncodesCode();
    	$codeModel->searchText = Yii::app()->request->getParam('filter_search_input', false);
    	$this->render('show_logs', array(
    		'codeModel' => $codeModel,
    	));

    	Yii::app()->end();
    }


    /**
     * Show Set's Codes management page
     */
    public function actionAddCodes() {

    	$idSet = Yii::app()->request->getParam('idSet', false);
    	$setModel   = SubscriptioncodesSet::model()->findByPk($idSet);
    	$codeModel  = new SubscriptioncodesCode();
    	$codeModel->id_set = $idSet;

    	$codeModel->searchText 			= Yii::app()->request->getParam('filter_search_input', false);
    	$codeModel->codeStatusFilter 	= Yii::app()->request->getParam('filter_code_status', SubscriptioncodesCode::STATUS_ALL);

    	Yii::app()->session['selectedItems'] = explode(',', $_REQUEST['selectedItems']);

    	$params = array(
    		'setModel' 		=> $setModel,
    		'codeModel'		=> $codeModel,
    	);

    	$this->render('add_codes', $params);

    	Yii::app()->end();
    }


    /**
     * Show and handle "Add codes" dialog
     */
    public function actionAddCodesDialog() {

    	$idSet = Yii::app()->request->getParam('idSet', false);
    	$confirm = Yii::app()->request->getParam('confirm', false);
		$illegalCharsInCodes = 0;

    	if ($confirm) {

    		$codesList = Yii::app()->request->getParam('codes_list', false);
    		$codesArray = explode("\n", $codesList);


			foreach ($codesArray as $code) {
				$code = trim($code);
				if (!ctype_alnum($code) && !empty($code)) {
				$illegalCharsInCodes = 1;
				}
			}

			if ($illegalCharsInCodes != 0) {
				$html = $this->renderPartial('_add_codes_dialog', array('badCodesList' => $codesList), true);
				echo $html;
				Yii::app()->end();
			}

    		SubscriptioncodesCode::addCodesToSet($idSet, $codesArray);
    		echo '<a class="auto-close success">';
    		Yii::app()->end();
    	}


    	$html = $this->renderPartial('_add_codes_dialog', $params, true);
    	echo $html;

    	Yii::app()->end();

    }

    /**
     * Show "Assign Courses to Subscription Codes SET" Selector (see UsersSelector for predefined types). Handled by a separate action.
     */
    public function actionAssignCourses() {

    	$idSet = Yii::app()->request->getParam('idSet', false);
    	$setModel   = SubscriptioncodesSet::model()->findByPk($idSet);

    	$courseModel  = new SubscriptioncodesCourse();
    	$courseModel->id_set = $idSet;
    	$courseModel->searchText 		= Yii::app()->request->getParam('filter_search_input', false);

    	Yii::app()->session['selectedItems'] = explode(',', $_REQUEST['selectedItems']);

    	$params = array(
    			'setModel' 		=> $setModel,
    			'courseModel'	=> $courseModel,
    	);

    	UsersSelector::registerClientScripts();

    	$this->render('assign_courses', $params);

    	Yii::app()->end();
    }


    /**
     * Collects data from Assign-Courses Selector. Assign courses to the set.
     *
     */
    public function actionAssignCoursesToSet() {

    	$idSet = Yii::app()->request->getParam("idGeneral", false);
    	$setModel = SubscriptioncodesSet::model()->findByPk($idSet);
    	$selectorType = Yii::app()->request->getParam("type", false);

    	try {

    		if (!$setModel) {
    			throw new CException('Invalid set');
    		}

    		if (isset($_REQUEST['confirm'])) {
    			$selectedCourses = UsersSelector::getCoursesListOnly($_REQUEST);
    			foreach ($selectedCourses as $idCourse) {
					if(SubscriptioncodesCourse::model()->findByPk(array('id_set' => $idSet, 'course_id' => $idCourse)))
						continue;
    				$setCourseModel = new SubscriptioncodesCourse();
    				$setCourseModel->id_set = $idSet;
    				$setCourseModel->course_id = $idCourse;
    				$setCourseModel->save();
    			}
    		}


    	}
    	catch (CException $e) {
    		$this->renderPartial('admin.protected.views.common._dialog2_error', array(
    				'type' => 'error',
    				'message' => $e->getMessage(),
    		));
    		Yii::app()->end();
    	}


    	echo '<a class="auto-close success">';
    	Yii::app()->end();



    }


    /**
     * Show "Confirm" dialog2 to unassogn one or more courses from a set
     */
    public function actionUnassignCourses() {

    	$idSet = Yii::app()->request->getParam('idSet', false);
    	$selectedItems = Yii::app()->request->getParam('selectedItems', false);
    	$confirm = Yii::app()->request->getParam('confirm', false);

    	if ($confirm) {
    		$courses = explode(',', $selectedItems);
    		$criteria = new CDbCriteria();
    		$criteria->addCondition('id_set=:idSet');
    		$criteria->addInCondition('course_id', explode(',', $selectedItems));
    		$criteria->params[':idSet'] = $idSet;
    		SubscriptioncodesCourse::model()->deleteAll($criteria);
    		echo '<a class="auto-close success">';
    		Yii::app()->end();
    	}

    	$setModel = SubscriptioncodesSet::model()->findByPk($idSet);
    	$this->renderPartial('_unassign_courses_dialog', array(
    		'setModel' => $setModel,
    		'selectedItems' => $selectedItems,
    	));

    	Yii::app()->end();

    }


    /**
     * Probably the most important action, called by normal user.
     * Enroll current user to all courses, assigned to the SET, which member is the incoming "code" in the request
     */
    public function actionSelfEnrollOLD() {

    	$code = Yii::app()->request->getParam('code', false);

    	$response = new AjaxResult();
    	$success = false;
    	$nCourses = 0;

    	// Get CODE model (case insensitive search)
    	$criteria = new CDbCriteria();
    	$criteria->addCondition('LOWER(code) = :code');
    	$criteria->params = array(':code' => strtolower($code));
    	$codeModel = SubscriptioncodesCode::model()->find($criteria);

    	$message = false;

    	// Code must be NOT null/NOT empty, model must exist and Code NOT used yet (used_by)
    	if ($code && $codeModel && is_null($codeModel->used_by)) {
    		$setModel = $codeModel->subscriptionSet;

    		// SET must be ACTIVE (enabled) && today must be a valid date, based on SET valid from/to dates
    		$todayDate = Yii::app()->localtime->toLocalDate();
    		if ($setModel->is_active && $setModel->isValidTime($todayDate)) {

    			$idUser =  Yii::app()->user->id;
    			$alreadyUsed = SubscriptioncodesCode::model()->exists('(id_set=:idSet) AND (used_by=:idUser)', array(
    				':idSet' => $setModel->id_set,
    				':idUser' => $idUser,
    			));

    			// User must not have been used ANY code from the same set so far
    			if (!$alreadyUsed) {
	    			// Number of courses in the SET must be positive
    				$nCourses = $setModel->countCourses;
    				if ($nCourses > 0) {
    					// Good!! Good! Looks like we have something to do: ENROLLLLLL!
    					foreach ($setModel->courses as $course) {
    						$enroll = new LearningCourseuser();
   							$enroll->subscribeUser($idUser, $idUser, $course->idCourse, 0, LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
    					}

	    				// Update CODEs table and set the code as USED/Consumed!
    					$codeModel->used_by = $idUser;
    					$codeModel->used_on = Yii::app()->localtime->toLocalDateTime();
    					$codeModel->save(false);
    					$success = true;
    				}
    			}
    			else {
    				$message = Yii::t('standard', 'You already have used a code for the same set of courses');
    				$success = false;
    			}
    		}

    	} elseif($code) {
			$message = Yii::t('course_autoregistration', 'The code you typed is invalid or unknown. Please try again!');
			$success = false;
		}

    	$response->setStatus($success);

    	if ($success) {
    		$message = Yii::t('standard', 'You have been successfully subscribed to {n} courses', array('{n}' => $nCourses));
    	}
    	else {
    		if (!$message)
    			$message = Yii::t('standard', '_OPERATION_FAILURE');
    	}

    	$response->setMessage($message);
    	$response->setData($data)->toJSON();
    	Yii::app()->end();
    }




    /**
     * Probably the most important action, called by normal user.
     * Enroll current user to all courses, assigned to the SET, which member is the incoming "code" in the request
     */
    public function actionSelfEnroll() {

    	$code = Yii::app()->request->getParam('code', false);

    	$response = new AjaxResult();
    	$success = false;
    	$nCourses = 0;

    	// Try to subscribe, also try to use the OLD course codes (last true parameter)
    	$result = SubscriptionCodesManager::enrollUser($code, Yii::app()->user->id, true);
    	$success = $result['success'];
    	$message = $result['message'];
    	$nCourses= $result['ncourses'];


    	// Handle the result
    	$response->setStatus($success);
    	if ($success) {
    		$message = Yii::t('standard', 'You have been successfully subscribed to {n} courses', array('{n}' => $nCourses));
    	}
    	else {
    		if (!$message)
    			$message = Yii::t('standard', '_OPERATION_FAILURE');
    	}

    	$response->setMessage($message);
    	$response->setData($data)->toJSON();
    	Yii::app()->end();
    }



    /**
     * Autocomplete: Logs page
     */
    public function actionAxCodeLogsAutocomplete() {

    	$maxNumber = Yii::app()->request->getParam('max_number', 10);
    	$term = Yii::app()->request->getParam('term', '');

    	$codeModel = new SubscriptioncodesCode();
    	$codeModel->searchText = $term;

    	$dataProvider = $codeModel->dataProviderLogsGrid();

    	$dataProvider->setPagination(array(
    			'pageSize' => $maxNumber,
    	));

    	$data = $dataProvider ->getData();

    	$result = array();
    	if ($data) {
    		foreach ($data as $item) {
    			$label = $item['codeName'];
    			$value = $label;
    			$result[] = array(
    					'label' => $label,
    					'value' => $value,
    			);
    		}
    	}

    	echo CJSON::encode($result);


    }



    /**
     * Autocomplete: Set's Codes page
     */
    public function actionAxAddCodesAutocomplete() {

    	$idSet = Yii::app()->request->getParam('idSet', false);
    	$maxNumber = Yii::app()->request->getParam('max_number', 10);
    	$term = Yii::app()->request->getParam('term', '');

    	$codeModel = new SubscriptioncodesCode();
    	$codeModel->id_set = $idSet;
    	$codeModel->searchText = $term;
    	$codeModel->codeStatusFilter = Yii::app()->request->getParam('filter_code_status', SubscriptioncodesCode::STATUS_ALL);

    	$dataProvider = $codeModel->dataProviderAddCodesGrid();

    	$dataProvider->setPagination(array(
    			'pageSize' => $maxNumber,
    	));

    	$data = $dataProvider ->getData();

    	$result = array();
    	if ($data) {
    		foreach ($data as $item) {
    			$label = $item['code'];
    			$value = $label;
    			$result[] = array(
    					'label' => $label,
    					'value' => $value,
    			);
    		}
    	}

    	echo CJSON::encode($result);


    }



    /**
     * Autocomplete: Set's Courses page
     */
    public function actionAxAssignCoursesAutocomplete() {

    	$idSet = Yii::app()->request->getParam('idSet', false);
    	$maxNumber = Yii::app()->request->getParam('max_number', 10);
    	$term = Yii::app()->request->getParam('term', '');

    	$model = new SubscriptioncodesCourse();
    	$model->id_set = $idSet;
    	$model->searchText = $term;

    	$dataProvider = $model->dataProviderAssignCoursesGrid();

    	$dataProvider->setPagination(array(
    			'pageSize' => $maxNumber,
    	));

    	$data = $dataProvider ->getData();

    	$result = array();
    	if ($data) {
    		foreach ($data as $item) {
    			$label = $item->course->name;
    			$value = $label;
    			$result[] = array(
    					'label' => $label,
    					'value' => $value,
    			);
    		}
    	}

    	echo CJSON::encode($result);


    }



    /**
     * Render Edit Set management grid column
     * @param unknown $row
     * @param unknown $index
     * @return string
     */
    public function gridRenderEditSet($row, $index) {
    	$html = CHtml::link('<span class="i-sprite is-edit"></span>', $this->createUrl('editSet', array('idSet' => $row->id_set)), array(
    		'class' => 'edit-action open-dialog',
    		'rel' 	=> 'edit-set-dialog',
    		'title' => Yii::t('standard', '_MOD'),
    		'data-dialog-class' => 'edit-set-dialog',
    		'data-dialog-title' => Yii::t('standard', '_MOD'),
    		'removeOnClose' => 'true',
    		'closeOnOverlayClick' => 'true',
    		'closeOnEscape' => 'true',
    	));
    	return $html;
    }


    /**
     * Render Toggle Status management grid column
     * @param unknown $row
     * @param unknown $index
     * @return string
     */
    public function gridRenderSetStatus($row, $index) {
    	$model = SubscriptioncodesSet::model()->findByPk($row->id_set);
    	if ($model) {
    		$currentStatusClass = $model->is_active == 1 ? 'suspend-action' : 'activate-action';
    	}
    	$html = CHtml::link('<span class="i-sprite"></span>', $this->createUrl('toggleSetStatus', array('idSet' => $row->id_set)), array(
			'class' => "$currentStatusClass toggle-set-status",
    		'title' => Yii::t('standard', 'Change status'),
		));
		return $html;
    }

    /**
     * Render Delete Set management grid column
     * @param unknown $row
     * @param unknown $index
     * @return string
     */
    public function gridRenderDeleteSet($row, $index) {
    	$html = CHtml::link('<span class="i-sprite"></span>', $this->createUrl('deleteSet', array('idSet' => $row->id_set)), array(
    		'class' => 'delete-action open-dialog',
    		'rel' 	=> 'delete-set-dialog',
   			'title' => Yii::t('standard', '_DEL'),
    		'data-dialog-class' => 'delete-set-dialog',
    		'data-dialog-title' => Yii::t('standard', '_DEL'),
    		'removeOnClose' => 'true',
    		'closeOnOverlayClick' => 'true',
    		'closeOnEscape' => 'true',
    	));
    	return $html;
    }


    public function gridRenderDeleteCodeFromSet($row, $index) {
    	$html = CHtml::link('<span class="i-sprite"></span>', $this->createUrl('deleteCodesFromSet',
    		array(
    			'selectedItems' => $row->code,
    			'idSet'			=> $row->id_set,
    		)),
    		array(
   				'class' => 'delete-action open-dialog',
   				'rel' 	=> 'delete-codes-from-set-dialog',
   				'title' => Yii::t('standard', '_DEL'),
   				'data-dialog-class' => 'delete-codes-from-set-dialog',
   				'data-dialog-title' => Yii::t('standard', '_DEL'),
   				'removeOnClose' => 'true',
   				'closeOnOverlayClick' => 'true',
   				'closeOnEscape' => 'true',
    		)
    	);
    	return $html;
    }


    /**
     *
     * @param unknown $row
     * @param unknown $index
     * @return string
     */
    public function gridRenderAddCodesLink($row, $index) {
    	$url = $this->createUrl('addCodes', array('idSet' => $row->id_set));
    	$html = "<a href=\"$url\"><i class=\"icon-code-tag-small\"></i> " . $row->countCodes . "</a>";
    	return $html;
    }

    /**
     *
     * @param unknown $row
     * @param unknown $index
     * @return string
     */
    public function gridRenderAssignCoursesLink($row, $index) {
    	$url = $this->createUrl('assignCourses', array('idSet' => $row->id_set));
    	$html = "<a href=\"$url\"><i class=\"icon-courses-list\"></i> " . $row->countCourses . "</a>";
    	return $html;
    }



    /**
     *
     */
	public function getLocalDateFormat() {
		return Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short'));
	}



}
