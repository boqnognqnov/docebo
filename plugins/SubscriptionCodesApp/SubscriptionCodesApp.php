<?php

/**
 * Plugin for SubscriptionCodesApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class SubscriptionCodesApp extends DoceboPlugin {

	// Override parent
	public static $HAS_SETTINGS = false;

	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'SubscriptionCodesApp';

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {
		parent::activate();

		$sql = "
-- -----------------------------------------------------
-- Table `subscriptioncodes_set`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `subscriptioncodes_set` (
  `id_set` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `valid_from` DATE NULL,
  `valid_to` DATE NULL,
  `is_active` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_set`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Table `subscriptioncodes_code`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `subscriptioncodes_code` (
  `code` VARCHAR(50) NOT NULL,
  `id_set` INT(11) NOT NULL,
  `used_on` TIMESTAMP NULL,
  `used_by` INT(11) NULL,
  PRIMARY KEY (`code`),
  INDEX `fk_subscriptioncodes_code_core_user_idx` (`used_by` ASC),
  INDEX `fk_subscriptioncodes_code_set_idx` (`id_set` ASC),
  CONSTRAINT `fk_subscriptioncodes_code_core_user`
    FOREIGN KEY (`used_by`)
    REFERENCES `core_user` (`idst`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_subscriptioncodes_code_set`
    FOREIGN KEY (`id_set`)
    REFERENCES `subscriptioncodes_set` (`id_set`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Table `subscriptioncodes_course`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `subscriptioncodes_course` (
  `id_set` INT(11) NOT NULL,
  `course_id` INT(11) NOT NULL,
  PRIMARY KEY (`id_set`, `course_id`),
  INDEX `fk_subscriptioncodes_course_learning_course_idx` (`course_id` ASC),
  CONSTRAINT `fk_subscriptioncodes_course_learning_course`
    FOREIGN KEY (`course_id`)
    REFERENCES `learning_course` (`idCourse`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_subscriptioncodes_course_set`
    FOREIGN KEY (`id_set`)
    REFERENCES `subscriptioncodes_set` (`id_set`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

";
		$command = Yii::app()->db->createCommand($sql);
		$command->execute();
	}


	public function deactivate() {
		parent::deactivate();
		//Removing widgets from dashboards
		DashboardDashlet::model()->deleteAllByAttributes(array('handler' => 'plugin.SubscriptionCodesApp.widgets.SubscriptionCodesDashlet'));
		$this->removeFieldsFromCustomReports();
	}

	private function removeFieldsFromCustomReports() {
		$allFilters = LearningReportFilter::model()->findAll();
		foreach($allFilters as $filter) 
		{
			$data = json_decode($filter->filter_data);
			if($enr = $data->fields->enrollment)
			{
				$changes = array();
				if ($changes[] = isset($enr->subscription_code)) unset($enr->subscription_code);
				if ($changes[] = isset($enr->subscription_code_set)) unset($enr->subscription_code_set);

				if (in_array(true, $changes))
				{
					$filter->filter_data = json_encode($data);
					$filter->save(false);
				}
			}
		}
	}
}
