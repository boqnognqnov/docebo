/**
 * SubscriptionCodes App JS compagnion.
 * Load/Register it on every render() (not renderPartial()). Use it like:
 *  
 *  	var options = {};
 *  	var SubsCodes = new SubscriptionCodes(options);
 *  
 *  "options" depends on particular action/view. See views/assign_courses.php, for example
 *  
 *  As of this writing, used options are:
 *
 *		options.idSet 							 
 *  	options.massDeleteCodesDialogUrl
 *  	options.massDeleteCodesDialogTitle
 *  	options.massUnassignCoursesDialogUrl
 *  	options.massUnassignCoursesDialogTitle
 *  	options.defaultCodeLength
 *  
 *  However it may evolve and more to be added. Please check this file for ".options".
 *  
 */
var SubscriptionCodes  = false;

(function($){

	/**
	 * Class constructor
	 */	
	SubscriptionCodes = function(options) {
		if (typeof options === "undefined" ) {
			var options = {};
		}
		
		this.options = {};
		this.defaultOptions = {
			defaultCodeLength: 12	
		};		
		
		this.init(options);
	}
	
	/**
	 * Class prototype
	 */
	SubscriptionCodes.prototype = {

		/**
		 * Initialize object
		 * @param options
		 */
		init: function(options) {
			this.options = $.extend({}, this.defaultOptions, options); 
			this.addListeners();
			
			// The StudentSelfEnrollment widget, shown in front end (My Courses, Courses Catalog, etc.)
			// Has a form which must be "ajaxified", to make it AJAX submitable.
			this.ajaxifySelfEnrollmentForm();
			
			// Tooltip-ize all links in button columns
			$('.grid-view .button-column-single a').tooltip();
			
			// We need to add a class to top-most level to control some DOM elements poping OUTSIDE all containers (like Autocomplete's Jquery UI)
			$('body').addClass('subscriptioncodes-body');
		},	
			
		/**
		 * Listeners for All
		 * 
		 * Note: Not all pages require ALL listeners, but it doesn't harm to set them ALL for the sake of simplicity: Single JS, Single init()
		 * 
		 */
		addListeners: function() {
			var that = this;
			
			// Add selectedItems to Mass Action dialog opening AJAX calls: Delete codes from Set
			$(document).on('dialog2.before-send', '#delete-codes-from-set-dialog', function(e, xhr, settings) {
				var selectedItems = $('input#subscriptioncodes-add-codes-grid-selected-items-list').val();
				settings.data = settings.data + '&selectedItems=' + selectedItems;
			});
			
			// Add selectedItems to Mass Action dialog opening AJAX calls: Unassign courses from Set
			$(document).on('dialog2.before-send', '#unassign-courses-from-set-dialog', function(e, xhr, settings) {
				var selectedItems = $('input#subscriptioncodes-assign-courses-list-selected-items-list').val();
				settings.data = settings.data + '&selectedItems=' + selectedItems;
			});
			
			// Add Codes UI: Mass DELETE selected 
			$('select#subscriptioncodes-add-codes-massive-action').on('change', function(){
				if (!$('input#subscriptioncodes-add-codes-grid-selected-items-list').val().trim()) return;
				if ($(this).val() == 'delete') {
					var url = that.options.massDeleteCodesDialogUrl + '&idSet=' + that.options.idSet;
					// Open dialog2 using POST
					openDialog(null, that.options.massDeleteCodesDialogTitle, url, 'delete-codes-from-set-dialog', 'delete-codes-from-set-dialog', 'POST');
				}
			});
			
			// Assign Courses UI: Mass DELETE selected 
			$('select#subscriptioncodes-assign-courses-massive-action').on('change', function(){
				if (!$('input#subscriptioncodes-assign-courses-list-selected-items-list').val().trim()) return;
				if ($(this).val() == 'delete') {
					var url = that.options.massUnassignCoursesDialogUrl + '&idSet=' + that.options.idSet;
					// Open dialog2 using POST
					openDialog(null, that.options.massUnassignCoursesDialogTitle, url, 'unassign-courses-from-set-dialog', 'unassign-courses-from-set-dialog', 'POST');
				}
			});
			
			// Listen for ASSIGN COURSES **selector** dialog close+success  (Confirm)
			// This event is standard and is fired by "Users Selector" (used here for courses selection)
			// And update the List View of assigned courses
			$(document).on('userselector.closed.success', '.modal.assign-courses-to-set-dialog', function(e,data) {
				$.fn.yiiListView.update('subscriptioncodes-assign-courses-list');
			});

			
			// Auto-close listeners for various dialog2's 
			var dialogSelectors = 
				[
				 '#unassign-courses-from-set-dialog',
				 '#delete-codes-from-set-dialog', 
				 '#edit-set-dialog', 
				 '#delete-set-dialog',
				 '#add-codes-dialog'
				];
			$(document).on("dialog2.content-update", dialogSelectors.join(','), function (e) {
				if ($(this).find("a.auto-close").length > 0) {
					// Close the dialog
					$(this).dialog2("close");
					
					// Reset selections after deleting SOME codes or unassigning SOME courses
					var id = $(this).attr('id');
					if (id == 'delete-codes-from-set-dialog') {
						$('input[name="subscriptioncodes-add-codes-grid-selected-items-list"]').val('');
						$('#subscriptioncodes-add-codes-grid-selected-items-count').html('0');
					}
					if (id == 'unassign-courses-from-set-dialog') {
						$('input[name="subscriptioncodes-assign-courses-list-selected-items-list"]').val('');
						$('#subscriptioncodes-assign-courses-list-selected-items-count').html('0');
					}
					
					// Update Grids and List views, if any
					that.updateGridAndListViews();	
				}
			});
			
			
			// Random Codes generation listener
			$(document).on('click','#generate-subscodes', function(e){
				var numberOfCodes = $('input[name="number_of_codes"]').val();
				if (numberOfCodes <= 0) {
					return;
				}
				for (var i=1; i<=numberOfCodes; i++) {
					$('textarea[name="codes_list"]').append(that.randomString(that.options.defaultCodeLength) + "\n");
				}

				// Not used yet
				var numberOfCodes = $('textarea[name="codes_list"]').val().split("\n").length - 1;
				
			});
			
			
			/**
			 * Do ajax calls on Toggle Set Status clicks (activate/deactivate)
			 * 
			 */
			$(document).on('click', '.toggle-set-status', function () { 
				var set = $(this);
				$.ajax({
					url:     $(this).attr('href'),
					dataType: 'json',
					success: function (res) {
						if (res.success) {
							var data = res.data;
							if (data.status == 1) {
								set.addClass('suspend-action');
								set.removeClass('activate-action');
							}
							else {
								set.addClass('activate-action');
								set.removeClass('suspend-action');
							}
						}
					}
				});
				return false;
			});
			
			
			/**
			 * Any #grid-filter-form form submition is prohibited.
			 * Instead, Grid update is initiated
			 */
			$('#grid-filter-form').on('submit', function(e){
				e.preventDefault();
				that.updateGridAndListViews();
			});
			
			
			/**
			 * On Clear-Seacrh icon click (x in the search form field)
			 */
			$('.search-input-wrapper .clear-search').on('click', function(){
				$('.search-input-wrapper #filter-search-input').val('');
				that.updateGridAndListViews();
			});
			
			/**
			 * On change of any Radio of any form initiate a grid/list view update
			 */
			$('#grid-filter-form input:radio').on('change', function(){
				that.updateGridAndListViews();
			});
			
			
			/**
			 * On Autocomplete item selected
			 */
			$('#filter-search-input').on('autocompleteselect', function(e, data){
				var $target = $(e.currentTarget);
				$target.val(data.item.value);
				that.updateGridAndListViews();
			});

			
			
		},
		
		/**
		 * Make the Self Enrollment widget's form AJAX submitable and attach callback
		 */
		ajaxifySelfEnrollmentForm: function() {
			// Self-enrollment widget's form (ajaxForm), visible in My Courses/Catalog area, where user is typing a code and try to subscribe
			var formOptions = {
				dataType: 'json',
				success: function(response) {
					if (response.success) {
						// Refresh the courses grid by emulating "search button" click
						Docebo.Feedback.show('success', response.message);
						$('#gsf-btn-search').trigger('click');
                        if($('.mydashboard\\.widgets\\.DashletMyCourses') && $('#mydashboard')){
                            $('#mydashboard').dashboard('loadContent', '.mydashboard\\.widgets\\.DashletMyCourses')
                        }
					}
					else {
						Docebo.Feedback.show('error', response.message);
					}
				}
			};
			$('#subscodes-selfenrollment-form').ajaxForm(formOptions);
		},
		
		
		/**
		 * Generate random string (numbers and characters)
		 * 
		 * @param string_length
		 * @returns {String}
		 */
		randomString: function(string_length) {
			if (typeof string_length == "undefined")
				string_length = 8;
			var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ";
			var randomstring = '';
			for (var i=0; i < string_length; i++) {
				var rnum = Math.floor(Math.random() * chars.length);
				randomstring += chars.substring(rnum,rnum+1);
			}
			return randomstring;
		},
		
		

		/**
		 * Update all available grids at the moment.
		 * Usually, only one is available though, but it doesn't harm to TRY to update a non-existent grid/list view.
		 */
		updateGridAndListViews: function() {
			if ($.fn.yiiListView)
				$.fn.yiiListView.update('subscriptioncodes-assign-courses-list');
			if ($.fn.yiiGridView) {
				$.fn.yiiGridView.update('subscriptioncodes-add-codes-grid');
				$.fn.yiiGridView.update('subscriptioncodes-management-grid');
				$.fn.yiiGridView.update('subscriptioncodes-logs-grid');
			}
		}
		
			
			
	}
	
	
	
})(jQuery);