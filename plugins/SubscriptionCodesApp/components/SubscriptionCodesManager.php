<?php

class SubscriptionCodesManager extends CComponent {

	/**
	 * Enroll user to courses according to the passed Subscription Code.
	 * Provides cross-acceptance of codes between Old Codes and New  Subscription codes plugin (see $tryOldCourseCodes)
	 *
	 * @param string $code
	 * @param integer $idUser
	 * @param string $tryOldCourseCodes Should we try (on failure) to use Old Course Codes approach to subscribe the user?
	 *
	 * @return array
	 */
	public static function enrollUser($code, $idUser=false, $tryOldCourseCodes=false) {

		// Get CODE model (case insensitive search)
		$criteria = new CDbCriteria();
		$criteria->addCondition('LOWER(code) = :code');
		$criteria->params = array(':code' => strtolower($code));
		$codeModel = SubscriptioncodesCode::model()->find($criteria);

		$result = array();
		$success 	= false;
		$message 	= Yii::t('standard', '_OPERATION_FAILURE');
		$errorcode 	= 0;

		// Code must be NOT null/NOT empty, model must exist and Code NOT used yet (used_by) by ANYONE
		if ($code && $codeModel && is_null($codeModel->used_by)) {

			$setModel = $codeModel->subscriptionSet;

			// SET must be ACTIVE (enabled) && today must be a valid date, based on SET valid from/to dates
			$todayDate = Yii::app()->localtime->toLocalDate();
			if ($setModel->is_active && $setModel->isValidTime($todayDate)) {

				$idUser =  $idUser ? $idUser : Yii::app()->user->id;
				$alreadyUsed = SubscriptioncodesCode::model()->exists('(id_set=:idSet) AND (used_by=:idUser)', array(
					':idSet' => $setModel->id_set,
					':idUser' => $idUser,
				));

				// User must not have been used ANY code from the same set so far
				if (!$alreadyUsed) {
					// Number of courses in the SET must be positive
					$nCourses = $setModel->countCourses;
					if ($nCourses > 0) {
						// Good!! Good! Looks like we have something to do: ENROLLLLLL!
						foreach ($setModel->courses as $course) {
							$enroll = new LearningCourseuser();
							$enroll->scenario = 'self-subscribe';
							$enroll->subscribeUser($idUser, $idUser, $course->idCourse, 0, LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
						}

						// Update CODEs table and set the code as USED/Consumed!
						$codeModel->used_by = $idUser;
						$codeModel->used_on = Yii::app()->localtime->toLocalDateTime();
						$codeModel->save(false);
						$success = true;
					}
				}
				else {
					$message = Yii::t('standard', 'You already have used a code for the same set of courses');
					$success = false;
				}
			}
    	}
    	else if ($code) {
			$message = Yii::t('course_autoregistration', 'The code you typed is invalid or unknown. Please try again!');
			$success = false;
		}


		// Try OLD Codes ?
		if (!$success && $tryOldCourseCodes) {
			$nCourses = false;
			// Call the Old Codes enrollment, but avoid circular calls (false et the end)
			$errorMessage = LearningCourseuser::subscribeUserByOldCode($code, Yii::app()->user->id, false);
			$success = !$errorMessage;
			// ! Message remains the one from Plugin: $message
		}


		$result['success'] 		= $success;
		$result['message'] 		= $message;
		$result['ncourses'] 	= $nCourses;
		$result['errorcode'] 	= 0;


		return $result;

	}



}