<?php
	$backUrl = Docebo::createAdminUrl('//SubscriptionCodesApp/SubscriptionCodesApp/index');
	$pageTitle = Yii::t('standard', 'Subscription Codes Logs');
	$backPageTitle = Yii::t('standard', 'Subscription codes sets');
	$this->breadcrumbs = array(
		Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
		$backPageTitle => $backUrl,
		$pageTitle => '',		
	); 
?>


<h3 class="title-bold">
	<a class="docebo-back" href="<?= $backUrl ?>">
		<span><?= Yii::t('standard', '_BACK') ?></span>
	</a>
	<?= $pageTitle ?>
</h3>

<br />


<!-- FILTER -->
<?php 
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'grid-filter-form',
		'htmlOptions' => array(
       		'class' => 'form-inline',
   		),
	)); 
?>

<div class="filters-wrapper">
	<table class="filters">
		<tbody>
			<tr>
				<td class="table-padding">
					<table class="table-border">
						<tr>
							<td class="group">
								<table class="pull-right">
									<tr>
										<td>
											<div class="search-input-wrapper">
												<?php 
													$params = array();
													$params['max_number'] = 10;
													$params['autocomplete'] = 1;
													$autoCompleteUrl = Docebo::createAbsoluteAdminUrl('SubscriptionCodesApp/SubscriptionCodesApp/axCodeLogsAutocomplete', $params);
													$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
														'id' 				=> 'filter-search-input',	
														'name' 				=> 'filter_search_input',
														'value' 			=> '',
														'options' => array(
															'minLength' => 1,
														),
														'source' => $autoCompleteUrl,
														'htmlOptions' => array('class' => 'search-query', 'placeholder' => Yii::t('standard', '_SEARCH')),
													));
												?>
												<button type="button" class="close clear-search">&times;</button>
												<span 	class="perform-search search-icon"></span>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</div>


<?php 
	echo CHtml::hiddenField($selectedUsersFieldName, '');
	$this->endWidget();
?>
<!-- FILTER -->



<?php 

	$columns = array();
	$columns[] = array(
			'type' => 'raw',
			'header' => Yii::t('standard', '_CODE'),
			'value' => '$data[codeName]',
	);
	$columns[] = array(
			'type' => 'raw',
			'header' => Yii::t('standard', '_SET'),
			'value' => '$data[setName]',
	);
	$columns[] = array(
			'type' => 'raw',
			'header' => Yii::t('standard', 'Used on'),
			'value' => 'Yii::app()->localtime->toLocalDateTime($data[usedOnDate])',
	);
	$columns[] = array(
			'type' => 'raw',
			'header' => Yii::t('standard', 'Used by'),
			'value' => 'trim($data[userId],"/")',
	);
	

	$this->widget('zii.widgets.grid.CGridView', array(
		'updateSelector'=>'{page}, {sort}, .search-input-wrapper .perform-search',
		'columns' => $columns,	
		'ajaxType' => 'POST',
		'id' => 'subscriptioncodes-logs-grid',
		'htmlOptions' => array('class' => 'grid-view clearfix'),
		'dataProvider' => $codeModel->dataProviderLogsGrid(),
		'template' => '{items}{summary}{pager}',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
		'pager' => array(
			'class' => 'common.components.DoceboCLinkPager',
		),
		'beforeAjaxUpdate' => 'function(id, options) {
			options.data = $("#grid-filter-form").serialize();
		}',
		'afterAjaxUpdate' => 'function(id, data) {
				$(\'a.tooltip\').tooltip();
				$(document).controls();
		}',
	));

?>

<script type="text/javascript">

$(function(){

	// Create SubsCodes JS class for THIS PAGE
	var SubsCodes = new SubscriptionCodes({});

});


</script>