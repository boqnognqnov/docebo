<div class="main-actions clearfix">

	<ul class="clearfix">
	
		<li>
			<div>
				<?php echo CHtml::link('<span></span>'.Yii::t('standard', 'New set'),
					Yii::app()->createUrl('//SubscriptionCodesApp/SubscriptionCodesApp/editSet'),
					array(
						'class' => 'new-subs-code open-dialog',
						'rel' 	=> 'edit-set-dialog',
						'data-dialog-class' => 'edit-set-dialog',	
						'data-dialog-title' => Yii::t('standard', 'New set'),
						'alt' => Yii::t('standard', 'Create new set of subscription codes'),
					)); ?>
			</div>
		</li>
		
		<li>
			<div>
				<?php echo CHtml::link('<span></span>'.Yii::t('helper', 'Show logs'),
					Yii::app()->createUrl('//SubscriptionCodesApp/SubscriptionCodesApp/showLogs'),
					array(
						'class'	=> 'additional-fields',	
						'alt' => Yii::t('standard', 'Show codes activation log'),
					)); ?>
			</div>
		</li>
		
		
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>

			<p></p>
		</div>
	</div>
	
	
</div>
