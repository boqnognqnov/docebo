<?php 

	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'subscriptioncodes-set-form',
		'htmlOptions' => array(
			'class' => 'ajax',	
		),
	));
	
?>


<div class="row-fluid">
	<div class="span12">
		<?= Yii::t('standard', 'Randomly generate') ?>
		&nbsp;<input type="text" class="" name="number_of_codes" value="60">&nbsp;
		<?= Yii::t('standard', 'Subscription codes') ?>
		&nbsp;<a href="javascript:;" class="btn btn-docebo green big" id="generate-subscodes"><?= Yii::t('certificate', '_GENERATE') ?></a>&nbsp;
	</div>
</div>

<br />

<div class="row-fluid">
	<div class="span12">
		<?= Yii::t('standard', 'or manually write/paste here your codes, one per line.') ?>
		<?= CHtml::textArea('codes_list', ($badCodesList ? $badCodesList : ''), array('class' => 'span12', 'rows' => 20))?>
		<?= Yii::t('standard', 'You can copy the random generated codes from this text area') ?>
		<?php if ($badCodesList) { ?> <div class="errorMessage"><?= Yii::t('error', 'Only letters (a-z,A-Z) and numbers (0-9) are allowed.') ?></div> <?php } ?>
	</div>
</div>


<!-- For Dialog2 - they go in footer -->
<div class="form-actions">
	<?php echo CHtml::submitButton(Yii::t('standard','_CONFIRM'), array('class' => 'btn btn-submit', 'name' => 'confirm')); ?>
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
</div>


<?php 
	$this->endWidget();
?>

