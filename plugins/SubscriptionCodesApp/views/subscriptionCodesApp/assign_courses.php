<?php
	$massUnassignUrl 	= Docebo::createAdminUrl('//SubscriptionCodesApp/SubscriptionCodesApp/unassignCourses');
	$massUnassignTitle 	= "<span class='icons-elements check-user white'></span>&nbsp;&nbsp;" . Yii::t('standard', '_UNASSIGN');
	
	$backUrl = Docebo::createAdminUrl('//SubscriptionCodesApp/SubscriptionCodesApp/index');
	$backPageTitle = Yii::t('standard', 'Subscription codes sets');
	$pageTitle = Yii::t('standard', 'Assign course(s) to a codes set');
	$this->breadcrumbs = array(
		Yii::t('menu', 'Admin') => Docebo::createAdminUrl('dashboard/index'),
		$backPageTitle => $backUrl,
		$pageTitle => '',		
	); 
?>


<h3 class="title-bold">
	<a class="docebo-back" href="<?= $backUrl ?>">
		<span><?= Yii::t('standard', '_BACK') ?></span>
	</a>
	<?= $pageTitle ?>
</h3>
<br>

<?php 
	$this->renderPartial('_mainActions_AssignCourses', array(
		'setModel' => $setModel,	
	)); 
?>

<h3 class="title-bold"><?php echo Yii::t('standard', 'Courses assigned to ') . $setModel->name; ?></h3>
<br />

<!-- FILTER -->
<?php 
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'grid-filter-form',
		'htmlOptions' => array(
       		'class' => 'form-inline',
   		),
	)); 
?>

<div class="filters-wrapper">
	<table class="filters">
		<tbody>
			<tr>
				<td class="table-padding">
					<table class="table-border">
						<tr>
							<td class="group">
								<table class="pull-right">
									<tr>
										<td>
											<div class="search-input-wrapper">
												<?php 
													$params = array();
													$params['max_number'] = 10;
													$params['autocomplete'] = 1;
													$params['idSet'] = $setModel->id_set;
													$autoCompleteUrl = Docebo::createAbsoluteAdminUrl('SubscriptionCodesApp/SubscriptionCodesApp/axAssignCoursesAutocomplete', $params);
													$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
														'id' 				=> 'filter-search-input',	
														'name' 				=> 'filter_search_input',
														'value' 			=> '',
														'options' => array(
															'minLength' => 1,
														),
														'source' => $autoCompleteUrl,
														'htmlOptions' => array('class' => 'search-query', 'placeholder' => Yii::t('standard', '_SEARCH')),
													));
												?>
												<button type="button" class="close clear-search">&times;</button>
												<span 	class="perform-search search-icon"></span>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<?php 
	echo CHtml::hiddenField($selectedUsersFieldName, '');
	$this->endWidget();
?>
<!-- FILTER -->




<div class="selections clearfix">
	<?php $this->widget('ext.local.widgets.ListViewSelectAll', array(
		'gridId' => 'subscriptioncodes-assign-courses-list',
		'class' => 'left-selections clearfix',
		'dataProvider' => $courseModel->dataProviderAssignCoursesGrid(),
		'itemValue' => '$data->course_id',
	)); ?>

	<div class="right-selections clearfix">
		<select name="massive_action" id="subscriptioncodes-assign-courses-massive-action">
			<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
			<option value="delete"><?php echo Yii::t('standard', '_UNASSIGN'); ?></option>
		</select>
	</div>
	
</div>

<h3 class="title-bold title-with-border"><?php echo Yii::t('standard', 'Assigned courses'); ?></h3>


<?php
 
	$this->widget('ext.local.widgets.CXListView', array(
		'ajaxUpdate' => 'subscriptioncodes-assign-courses-list-all-items',	
		'updateSelector'=>'.pager a, .sort  a, .search-input-wrapper .perform-search',
		'ajaxType' => 'POST',	
		'id' => 'subscriptioncodes-assign-courses-list',
		'htmlOptions' => array('class' => 'list-view clearfix'),
		'dataProvider' => $courseModel->dataProviderAssignCoursesGrid(),
		'itemView' => '_viewOneCourse',
		'itemsCssClass' => 'items clearfix',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'pager' => array(
			'class' => 'common.components.DoceboCLinkPager',
		),
		'template' => '{items}{pager}{summary}',
		'beforeAjaxUpdate' => 'function(id, options) {
			options.type = \'POST\';
			options.data = $("#grid-filter-form").serialize();
			options.data += "&selectedItems=" + $(\'#\'+id+\'-selected-items-list\').val();
		}',
		'afterAjaxUpdate' => 'function(id, data) { 
			$(\'#subscriptioncodes-assign-courses-list input\').styler();
			$(document).controls();
			$.fn.updateListViewSelectPage();
		}',
	));
	 
?>



<script type="text/javascript">
	$(function(){
		$('#subscriptioncodes-assign-courses-list input').styler();

		// Create SubsCodes JS class for THIS PAGE
		var options = {
			idSet								: <?= (int) $setModel->id_set ?>,	
			massUnassignCoursesDialogUrl 		: "<?= $massUnassignUrl ?>",
			massUnassignCoursesDialogTitle		: "<?= $massUnassignTitle ?>"
		};
		var SubsCodes = new SubscriptionCodes(options);

		
	});
</script>
