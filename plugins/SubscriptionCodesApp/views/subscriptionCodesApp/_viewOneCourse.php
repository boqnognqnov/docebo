<?php
	$checked = in_array($data->course_id, Yii::app()->session['selectedItems']);
	$unassignUrl = Docebo::createAdminUrl('//SubscriptionCodesApp/SubscriptionCodesApp/unassignCourses', array('idSet' => $data->id_set, 'selectedItems' => $data->course_id));
	 
?>
<div class="item  <?php echo (($index % 2) ? 'odd' : 'even'); ?>">
	<div class="checkbox-column">
		<?php echo '<input id="subscriptioncodes-assign-courses-list-checkboxes_'. $data->course_id .'" type="checkbox" name="subscriptioncodes-assign-courses-list-checkboxes[]" value="'.$data->course_id.'" '. ($checked ? 'checked="checked"':'') .'>'; ?>
	</div>

	<div class="group-member-username"><?php echo $data->course->name;?></div>
		<div class="delete-button">
			<?php
				$html = CHtml::link('', $unassignUrl, array(
					'rel'	=> 'unassign-courses-from-set-dialog',	
					'class' => 'delete-node open-dialog',
					'data-dialog-class' => 'unassign-courses-from-set-dialog',
					'data-dialog-title' => Yii::t('standard', '_UNASSIGN'),
					'removeOnClose' => 'true',
					'closeOnOverlayClick' => 'true',
					'closeOnEscape' => 'true',
				));
				echo $html;
			?>
		</div>
</div>