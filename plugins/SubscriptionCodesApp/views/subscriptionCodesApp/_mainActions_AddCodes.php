<div class="main-actions clearfix">

	<ul class="clearfix">
	
		<li>
			<div>
				<?php echo CHtml::link('<span></span>'.Yii::t('standard', 'Add codes'),
					Yii::app()->createUrl('//SubscriptionCodesApp/SubscriptionCodesApp/addCodesDialog', array(
						'idSet' => $setModel->id_set	
					)),
					array(
						'class' => 'open-dialog import',
						'rel' 	=> 'add-codes-dialog',
						'data-dialog-class' => 'add-codes-dialog',	
						'data-dialog-title' => Yii::t('standard', 'Add codes'),
						'data-ajaxType' => 'PosT',	
						'alt' => Yii::t('standard', 'Add codes'),
					)); ?>
			</div>
		</li>
		
		
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>

			<p></p>
		</div>
	</div>
	
	
</div>
