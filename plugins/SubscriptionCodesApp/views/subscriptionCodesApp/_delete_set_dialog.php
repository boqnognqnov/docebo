<?php 
	$form = $this->beginWidget('CActiveForm', array(
		'htmlOptions' => array(
			'class'		=> 'ajax',	
		),
	));
?>

<p>
	<?= Yii::t('standard', 'Please confirm you want to delete {set}', array(
		'{set}' => '<strong>' . $setModel->name . '</strong>'
	)) ?>
</p>

<!-- For Dialog2 - they go in footer -->
<div class="form-actions">
	<?= CHtml::submitButton(Yii::t('standard','_CONFIRM'), array('class' => 'btn btn-submit', 'name' => 'confirm')) ?>
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')) ?>
</div>

<?php
	$this->endWidget();
?>
