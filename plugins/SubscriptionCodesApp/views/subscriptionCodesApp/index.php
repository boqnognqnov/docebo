<?php 
	$pageTitle = Yii::t('standard', 'Subscription codes sets');
	$this->breadcrumbs = array(
		Yii::t('menu', 'Admin') => Docebo::createAdminUrl('dashboard/index'),
		$pageTitle => '',
	); 
?>

<h3 class="title-bold"><?php echo $pageTitle; ?></h3>
<br />

<?php $this->renderPartial('_mainActions_Index', array('showWaiting' => true)); ?>

<?php 
	$columns = array();
	$columns[] = array(
		'type' => 'raw',
		'header' => Yii::t('standard', '_NAME'),
		'value' => '$data->name',
	);
	$columns[] = array(
		'type' => 'raw',
		'header' => Yii::t('subscribe', '_DATE_BEGIN_VALIDITY'),
		'value' => '($data->valid_from == "0000-00-00" ? "" : $data->valid_from) ',
	);
	$columns[] = array(
		'type' => 'raw',
		'header' => Yii::t('subscribe', '_DATE_EXPIRE_VALIDITY'),
		'value' => '($data->valid_to == "0000-00-00" ? "" : $data->valid_to) ',
	);
	$columns[] = array(
		'type' => 'raw',
		'header' =>  Yii::t('standard', 'Codes'),
		'value' => array($this, 'gridRenderAddCodesLink'),
	);
	$columns[] = array(
		'type' => 'raw',
		'header' => Yii::t('standard', '_COURSES'),
		'value' => array($this, 'gridRenderAssignCoursesLink'),
	); 
	$columns[] = array(
			'type' => 'raw',
			'value' => array($this, 'gridRenderSetStatus'),
			'htmlOptions' => array(
					'class' => 'button-column-single',
			)
	);
	
	$columns[] = array(
		'type' => 'raw',
		'value' => array($this, 'gridRenderEditSet'),
		'htmlOptions' => array(
			'class' => 'button-column-single',
		)
	);
	$columns[] = array(
			'type' => 'raw',
			'value' => array($this, 'gridRenderDeleteSet'),
			'htmlOptions' => array(
					'class' => 'button-column-single',
			)
	);
	

	?>
	
	<h3 class="title-bold"><?php echo Yii::t('standard', 'Subscription codes sets'); ?></h3>
	<br />
	
	<?php 	

	$this->widget('zii.widgets.grid.CGridView', array(
		'columns' => $columns,
		'ajaxType' => 'POST',	
		'id' => 'subscriptioncodes-management-grid',
		'htmlOptions' => array('class' => 'grid-view clearfix'),
		'dataProvider' => $setModel->dataProviderManagementGrid(),
		'template' => '{items}{summary}{pager}',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
		'pager' => array(
				'class' => 'common.components.DoceboCLinkPager',
		),
		'beforeAjaxUpdate' => 'function(id, options) {
		}',
		'afterAjaxUpdate' => 'function(id, data) {
				$(\'a.tooltip\').tooltip();
				$(document).controls();
		}',
	));

?>




<script type="text/javascript">

$(function(){

	// Create SubsCodes JS class for THIS PAGE
	var SubsCodes = new SubscriptionCodes({});

});


</script>