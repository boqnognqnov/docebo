<?php 
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'subscriptioncodes-set-form',
		'htmlOptions' => array(
			'class'		=> 'ajax',	
		),
	));
	
	echo CHtml::hiddenField('selectedItems', $selectedItems);
	
?>

<p>
	<?= Yii::t('standard', 'Please confirm you want to unassign {n} course(s) from {set}', array(
		'{n}' => count(explode(',', $selectedItems)),
		'{set}' => '<strong>' . $setModel->name . '</strong>'
	)) ?>
</p>

<!-- For Dialog2 - they go in footer -->
<div class="form-actions">
	<?= CHtml::submitButton(Yii::t('standard','_CONFIRM'), array('class' => 'btn btn-submit', 'name' => 'confirm')) ?>
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')) ?>
</div>

<?php
	$this->endWidget();
?>
