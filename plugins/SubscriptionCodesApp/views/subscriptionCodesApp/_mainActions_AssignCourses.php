<?php
	$assignCoursesUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
		'type' 	=> UsersSelector::TYPE_SUBSCRIPTIONCODES_COURSES,
		'idGeneral'	=> $setModel->id_set,
	));
?>
<div class="main-actions clearfix">

	<ul class="clearfix">
	
		<li>
			<div>
				<?php echo CHtml::link('<span></span>'.Yii::t('standard', 'Assign courses'),
					$assignCoursesUrl,
					array(
						'class' => 'open-dialog new-course',
						'rel' 	=> 'assign-courses-to-set-dialog',
						'data-dialog-class' => 'assign-courses-to-set-dialog',	
						'data-dialog-title' => Yii::t('standard', 'Assign courses'),
						'alt' => Yii::t('standard', 'Assign courses'),
					)); ?>
			</div>
		</li>
		
		
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>

			<p></p>
		</div>
	</div>
	
	
</div>
