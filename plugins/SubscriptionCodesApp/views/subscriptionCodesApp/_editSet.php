<?php 

$form = $this->beginWidget('CActiveForm', array(
	'id' => 'subscriptioncodes-set-form',
	'htmlOptions' => array(
		'class' => 'ajax',
	),
));

$setModel->valid_from = ($setModel->valid_from == '0000-00-00' ? '' : $setModel->valid_from);
$setModel->valid_to = ($setModel->valid_to == '0000-00-00' ? '' : $setModel->valid_to);

$dpFormat = Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat());

?>

<div class="row-fluid">
	<?php echo $form->labelEx($setModel,'name'); ?>
    <?php echo $form->textField($setModel,'name'); ?>
    <?php echo $form->error($setModel,'name'); ?>
</div>

<br />
<div class="row-fluid">

	<div class="span6">
		<?php echo $form->labelEx($setModel,'valid_from'); ?>
    	<?php echo $form->textField($setModel,'valid_from', array('class'=>'datepicker edit-day-input', 'id' => 'from-date-input', 'readonly' => 'readonly')); ?>
    	<?php echo $form->error($setModel,'valid_from'); ?>
    	<i class="p-sprite calendar-black-small date-icon"></i>
	</div>

	<div class="span6">
		<?php echo $form->labelEx($setModel,'valid_to'); ?>
    	<?php echo $form->textField($setModel,'valid_to', array('class'=>'datepicker edit-day-input', 'id' => 'to-date-input', 'readonly' => 'readonly')); ?>
    	<?php echo $form->error($setModel,'valid_to'); ?>
    	<i class="p-sprite calendar-black-small date-icon"></i>
	</div>
	
</div>




<!-- For Dialog2 - they go in footer -->
<div class="form-actions">
	<?php echo CHtml::submitButton(Yii::t('standard','_CONFIRM'), array('class' => 'btn btn-submit', 'name' => 'confirm')); ?>
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
</div>


<?php 
	$this->endWidget();
?>

<script type="text/javascript">

	$(function(){

		/* Init Datepickers */
		var fromPicker = $('#from-date-input').bdatepicker({format: '<?= $this->getLocalDateFormat() ?>', language: '<?= Yii::app()->getLanguage() ?>'}).data('datepicker');
		var toPicker = $('#to-date-input').bdatepicker({format: '<?= $this->getLocalDateFormat() ?>', language: '<?= Yii::app()->getLanguage() ?>'}).data('datepicker');
		$('#from-date-input').bdatepicker().on('show', function(ev){ toPicker.hide(); });
		$('#to-date-input').bdatepicker().on('show', function(ev){ fromPicker.hide(); });
		/* Hide date panel on change/click */
		$('#from-date-input, #to-date-input').on('changeDate', function(ev){
		    $(this).bdatepicker('hide');
		});
		/*  Install click on calendar icon */
		$(".date-icon").on('click', function (ev) {
			if (!$(this).prev('input').attr('disabled'))
				$(this).prev('input').bdatepicker().data('datepicker').show();
		});
	
	});
</script>