<?php
	$massDeleteUrl 	= Docebo::createAdminUrl('//SubscriptionCodesApp/SubscriptionCodesApp/deleteCodesFromSet');
	$massDeleteTitle 	= "<span class='icons-elements check-user white'></span>&nbsp;&nbsp;" . Yii::t('standard', '_DEL');

	$backUrl = Docebo::createAdminUrl('//SubscriptionCodesApp/SubscriptionCodesApp/index');
	$backPageTitle = Yii::t('standard', 'Subscription codes sets');
	$pageTitle = Yii::t('standard', 'Add Subscriptions codes');
	$this->breadcrumbs = array(
		Yii::t('menu', 'Admin') => Docebo::createAdminUrl('dashboard/index'),
		$backPageTitle => $backUrl,
		$pageTitle => '',		
	); 
?>


<h3 class="title-bold">
	<a class="docebo-back" href="<?= $backUrl ?>">
		<span><?= Yii::t('standard', '_BACK') ?></span>
	</a>
	<?= $pageTitle ?>
</h3>
<br>

<?php 
	$this->renderPartial('_mainActions_AddCodes', array(
		'setModel' => $setModel,
	));
?>


<h3 class="title-bold"><?php echo Yii::t('standard', 'Subscriptions codes list') . ": " . $setModel->name; ?></h3>
<br />



<!-- FILTER -->
<?php 
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'grid-filter-form',
		'htmlOptions' => array(
       		'class' => 'form-inline',
   		),
	)); 
?>

<div class="filters-wrapper">
	<table class="filters">
		<tbody>
			<tr>
				<td class="table-padding">
					<table class="table-border">
						<tr>
							<td class="group">
								
								<table>
									<tr>
										<td>
										<?php 
											echo CHtml::radioButtonList('filter_code_status', SubscriptioncodesCode::STATUS_ALL, array(
												SubscriptioncodesCode::STATUS_ALL 		=> Yii::t('standard', '_ALL'),
												SubscriptioncodesCode::STATUS_USED 		=> Yii::t('standard', 'Used codes'),
												SubscriptioncodesCode::STATUS_UNUSED 	=> Yii::t('standard', 'Unused codes'),
											), 
											array(
												'template' => "&nbsp;&nbsp;&nbsp;{input}&nbsp;{label}&nbsp;",
												'container' => '', 
												'separator' => ''
											));
										?>
										</td>
									</tr>
								</table>
														
							</td>
						
							<td class="group">
								<table class="pull-right">
									<tr>
										<td>
											<div class="search-input-wrapper">
												<?php 
													$params = array();
													$params['max_number'] = 10;
													$params['autocomplete'] = 1;
													$params['idSet'] = $setModel->id_set;
													$autoCompleteUrl = Docebo::createAbsoluteAdminUrl('SubscriptionCodesApp/SubscriptionCodesApp/axAddCodesAutocomplete', $params);
													$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
														'id' 				=> 'filter-search-input',	
														'name' 				=> 'filter_search_input',
														'value' 			=> '',
														'options' => array(
															'minLength' => 1,
														),
														'source' => $autoCompleteUrl,
														'htmlOptions' => array('class' => 'search-query', 'placeholder' => Yii::t('standard', '_SEARCH')),
													));
												?>
												<button type="button" class="close clear-search">&times;</button>
												<span 	class="perform-search search-icon"></span>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</div>


<?php 
	echo CHtml::hiddenField($selectedUsersFieldName, '');
	$this->endWidget();
?>
<!-- FILTER -->






<div class="selections clearfix">
	<?php $this->widget('ext.local.widgets.GridViewSelectAll', array(
		'gridId' => 'subscriptioncodes-add-codes-grid',
		'class' => 'left-selections clearfix',
		'dataProvider' => $codeModel->dataProviderAddCodesGrid(),
		'itemValue' => 'code',
	)); ?>
	<div class="right-selections clearfix">
		<select name="massive_action" id="subscriptioncodes-add-codes-massive-action">
			<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
			<option value="delete"><?php echo Yii::t('standard', '_DEL'); ?></option>
		</select>
	</div>
</div>


<?php

	$columns = array();
	
	$columns[] = array(
		'class' => 'CCheckBoxColumn',
		'selectableRows' => 2,
		'id' => 'subscriptioncodes-add-codes-grid-checkboxes',
		'value' => '$data->code',
		'checked' => 'in_array($data->code, Yii::app()->session[\'selectedItems\'])',
	);
	
	$columns[] = array(
		'type' => 'raw',
		'header' => Yii::t('standard', '_CODE'),
		'value' => '$data->code',
	);
	
	$columns[] = array(
		'type' => 'raw',
		'header' => Yii::t('standard', 'Used on'),
		'value' => '$data->used_on',
	);
	
	$columns[] = array(
		'type' => 'raw',
		'header' => Yii::t('standard', 'Used by'),
		'value' => 'trim($data->user->userid,"/")',
	);
	
	$columns[] = array(
			'type' => 'raw',
			'header' => Yii::t('standard', '_FIRSTNAME'),
			'value' => '$data->user->firstname',
	);
	
	$columns[] = array(
			'type' => 'raw',
			'header' => Yii::t('standard', '_LASTNAME'),
			'value' => '$data->user->lastname',
	);
	
	
	
	$columns[] = array(
		'type' => 'raw',
		'value' => array($this, 'gridRenderDeleteCodeFromSet'),
		'htmlOptions' => array(
			'class' => 'button-column-single',
		)
	);
	
	
	
	
	$this->widget('zii.widgets.grid.CGridView', array(
		'ajaxUpdate' => 'subscriptioncodes-add-codes-grid-all-items',
		'updateSelector'=>'{page}, {sort}, .search-input-wrapper .perform-search',
		'columns' => $columns,
		'ajaxType' => 'POST',	
		'id' => 'subscriptioncodes-add-codes-grid',
		'htmlOptions' => array('class' => 'grid-view clearfix'),
		'dataProvider' => $codeModel->dataProviderAddCodesGrid(),
		'template' => '{items}{summary}{pager}',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
		'pager' => array(
			'class' => 'common.components.DoceboCLinkPager',
		),
				
		'beforeAjaxUpdate' => 'function(id, options) {
			options.data = $("#grid-filter-form").serialize();
			options.data += "&selectedItems=" + $(\'#\'+id+\'-selected-items-list\').val();
		}',
		'afterAjaxUpdate' => 'function(id, data) {
				$(\'a.tooltip\').tooltip();
				$(\'.grid-view#subscriptioncodes-add-codes-grid input\').styler();
				$(document).controls();
		}',
	));

?>


<script type="text/javascript">

	$(function(){
		$('#subscriptioncodes-add-codes-grid input').styler();

		// Create SubsCodes JS class for THIS PAGE
		var options = {
			idSet							: <?= (int) $setModel->id_set ?>,	
			massDeleteCodesDialogUrl 		: "<?= $massDeleteUrl ?>",
			massDeleteCodesDialogTitle		: "<?= $massDeleteTitle ?>",
			defaultCodeLength				: <?= isset(Yii::app()->params['subscription_code_length']) ? (int) Yii::app()->params['subscription_code_length'] : 12 ?>,
		};
		var SubsCodes = new SubscriptionCodes(options);
	});
	

</script>
