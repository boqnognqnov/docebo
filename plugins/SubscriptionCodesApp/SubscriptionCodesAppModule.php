<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */

class SubscriptionCodesAppModule extends CWebModule {
	
	public $_assetsUrl;

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}


	public function init() {
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		$this->setImport(array(
			'SubscriptionCodesApp.models.*',
			'SubscriptionCodesApp.widgets.*'
		));

		Yii::app()->event->on('CollectMyDashboardDashletWidgets', array($this, 'installSubscriptionCodesDashlet'));
	}

	/**
	 * Respond to the dashboard dashlet collector and include
	 * the SubscriptionCodes special dashlet
	 * @param DEvent $event
	 */
	public function installSubscriptionCodesDashlet(DEvent $event){
		if(!isset($event->params['descriptors']))
			return;

		$subscriptionCodesDashletDescriptor = SubscriptionCodesDashlet::descriptor();

		$collectedSoFarHandlers = array();
		foreach($event->params['descriptors'] as $descriptorObj){
			if ($descriptorObj instanceof DashletDescriptor)
				$collectedSoFarHandlers[] = $descriptorObj->handler;
		}

		if(!in_array($subscriptionCodesDashletDescriptor->handler, $collectedSoFarHandlers)){
			// Add the SubscriptionCodesDashlet dashlet to the event params
			$event->params['descriptors'][] = $subscriptionCodesDashletDescriptor;
		}
	}

	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu() {
	
		Yii::app()->mainmenu->addAppMenu('subscriptionCodes', array(
			'icon' => 'home-ico subscodes',
			'app_icon' => 'fa-barcode', // icon used in the new menu
			'link' => Docebo::createAdminUrl('//SubscriptionCodesApp/SubscriptionCodesApp/index'),
			'label' => Yii::t('standard', 'Subscription codes'),
			), array()
		);
	
	}
	

	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public function getAssetsUrl() {
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('SubscriptionCodesApp.assets'));
		}
		return $this->_assetsUrl;
	}
	
	
}
