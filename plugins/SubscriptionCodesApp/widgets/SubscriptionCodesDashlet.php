<?php

class SubscriptionCodesDashlet extends DashletWidget implements IDashletWidget{

	public function renderFrontEnd() {
		$this->render('studentSelfEnrollment');
	}

	/**
	 * Render dashlet settings, used in Admin Dashlets UI
	 */
	public function renderSettings() {

	}


	/**
	 * Dashlet descriptor
	 *
	 * @return DashletDescriptor Object, describing the dashlet to the outside world
	 */
	public static function descriptor( $params = FALSE ) {
		$sampleImageUrl = Yii::app()->mydashboard->getModule()->getAssetsUrl() . "/images/dashlet_sample.png";

		$descriptor = new DashletDescriptor();
		$descriptor->name 					= 'core_dashlet_subscription_codes';
		$descriptor->handler				= 'plugin.SubscriptionCodesApp.widgets.SubscriptionCodesDashlet';
		$descriptor->title					= Yii::t('standard', 'Subscription codes');
		$descriptor->description			= Yii::t('dashlets', 'Subscription codes widget');
		$descriptor->sampleImageUrl 		= $sampleImageUrl;
		$descriptor->settingsFieldNames		= array();
		$descriptor->disallowMultiInstance	= 1;

		return $descriptor;
	}

	/**
	 * @see IDashletWidget::bootstrap()
	 */
	public function bootstrap($params=false) {
		$cs = Yii::app()->getClientScript();
		$pluginAssetsUrl = Yii::app()->getModule('SubscriptionCodesApp')->getAssetsUrl();
		$cs->registerCssFile($pluginAssetsUrl.'/css/subscriptioncodes.css');
		$cs->registerScriptFile($pluginAssetsUrl.'/js/subscriptioncodes.js');
	}
	
}