<?php 

class StudentSelfEnrollment extends CWidget {
	
	protected $pluginAssetsUrl;
	
	public function init() {
		$this->pluginAssetsUrl = Yii::app()->getModule('SubscriptionCodesApp')->getAssetsUrl();
		
	}
	
	public function run() {
		
		// We will need the plugin JS class
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile($this->pluginAssetsUrl.'/css/subscriptioncodes.css');
		$cs->registerScriptFile($this->pluginAssetsUrl.'/js/subscriptioncodes.js');
		
		$this->render('studentSelfEnrollment');
	}
	
	
}
