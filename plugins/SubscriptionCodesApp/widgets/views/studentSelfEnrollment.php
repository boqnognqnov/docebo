<style>
<!--

.subscodes-selfenrollment-widget {
	height: 30px;
}

.subscodes-selfenrollment-widget input[name="code"], 
.subscodes-selfenrollment-widget input[name="code"]:focus {
 	height: 20px; 
	line-height: 20px;
}

.dashlets-cell[data-cell-text-width="onethird"] .subscodes-selfenrollment-widget input[name="code"],
.dashlets-cell[data-cell-text-width="twothird"] .subscodes-selfenrollment-widget input[name="code"],
.dashlets-cell[data-cell-text-width="half"] .subscodes-selfenrollment-widget input[name="code"] {
	width: 98%;
	padding:4px 0px;
	margin-bottom: 5px;
}

.dashlets-cell[data-cell-text-width="onethird"] .subscodes-selfenrollment-widget #subscode-confirm,
.dashlets-cell[data-cell-text-width="twothird"] .subscodes-selfenrollment-widget #subscode-confirm,
.dashlets-cell[data-cell-text-width="half"] .subscodes-selfenrollment-widget #subscode-confirm {
	width: 100%;
}

.subscodes-selfenrollment-widget #subscode-confirm {
	font-size: 12px;
	float: right;
}

.subscodes-selfenrollment-widget .subs-codes-text-info {
	padding: 3px;	
}

.subscodes-selfenrollment-widget .subscodes-selfenrollment-widget-icon-tag {
	padding-top: 1px;
	float: left;	
}


-->
</style>

<?php
	// AJAX FORM (see subscriptioncodes.js)
	$action = Docebo::createAdminUrl('SubscriptionCodesApp/SubscriptionCodesApp/selfEnroll', array());
	echo CHtml::beginForm($action, 'POST', array(
		'id' => 'subscodes-selfenrollment-form',	
	)); 
?>

<div class="subscodes-selfenrollment-widget">
	<div class="row-fluid">
		<div class="span12">
		<table width="100%">
			<tr>
				<td>
					<i style="display: block;" class="icon-code-tag"></i>
				</td>
				<td class="subs-codes-text-info">
					<strong><?= Yii::t('standard', 'Do you have a subscription code?') ?></strong>&nbsp;
					<?= Yii::t('standard', 'Type it here to subscribe to your courses!') ?>
				</td>
				<td align="right">
					<input type="text" class="search-txt" id="" name="code"><button href="#" class="btn btn-docebo green big" id="subscode-confirm"><span><?= Yii::t('standard', '_CONFIRM') ?></span></button>
				</td>
			</tr>
		</table>
		</div>
	</div>

</div>


<?php 
	echo Chtml::endForm();
?>

<script type="text/javascript">
	$(function(){
		// We will need the plugin's JS class, where all JS related to SubscriptionCodes App is located
		var options = {};
		var SubsCodes = new SubscriptionCodes(options);
	});
</script>

