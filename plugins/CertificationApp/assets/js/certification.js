var CertificationManClass = null; 
	
(function ( $ ) {

	/**
	 * Constructor
	 */
	CertificationManClass = function (options) {
		if (typeof options === "undefined" ) {
			var options = {};
		}
		this.init(options);
	};


	/**
	 * Prototype
	 */
	CertificationManClass.prototype = {
		
		defaultOptions: {
			editCertDialogId			: 'edit-certification-dialog',
			certsGridId					: 'combo-grid',
			certItemsListId				: 'combo-list',
			massActionUrl				: ''
		},
		options: {},
		
		init: function(options) {
			var that = this;
			
			// Merge incoming options to default ones
			this.options = $.extend({}, this.defaultOptions, options);

			
			// On closing the dialog we must destroy TinyMce editors; 
			$(document).on('dialog2.closed', '.modal-body', function (e) {
				if ($(this).attr('id') === that.options.editCertDialogId) {
					TinyMce.removeEditorById('certification-description');
				}
			});

			
			/**
			 * Dialog update listeners for various Dialog2's
			 */
			$(document).on("dialog2.content-update", '.modal-body', function (e) {
				
				// If a dialog has been closed with "success"
				if ($(this).find("a.auto-close").length > 0) {
					$(this).dialog2("close");
					
					// Update Certifications grid, if any
					if ($('#' + that.options.certsGridId).length > 0) {
						$('#' + that.options.certsGridId).yiiGridView('update');
					}

					// If it is the "assign items to certification" selector, update the items list view
					if ($(this).attr('id') === 'certifications-assign-items-dialog') {
						$.fn.yiiListView.update(that.options.certItemsListId);
					}

					// Mass action dialog has been closed, update appropriate grids or views, if any
					if ($(this).attr('id') === 'certifications-mass-action-dialog') {
						// Certification Items List view?
						if (that.options.certItemsListId && $('#'+that.options.certItemsListId).length > 0) {
							$.fn.yiiListView.update(that.options.certItemsListId);
							$('#' + that.options.certItemsListId).comboListView('setSelection', []);
						}
						// Certifications grid view ?
						if (that.options.certsGridId && $('#'+that.options.certsGridId).length > 0) {
							$('#' + that.options.certsGridId).yiiGridView('update');
							$('#' + that.options.certsGridId).comboGridView('setSelection', []);
						}
					}
					
					// When certification ITEM deletion is confirmed
					if ($(this).attr('id') === 'delete-certification-item-dialog') {
						$.fn.yiiListView.update(that.options.certItemsListId);
					}
					

				}
				
				/**
				 * Edit Certification dialog2 loaded, do some stuff, if any
				 */
				if ($(this).attr('id') === that.options.editCertDialogId) {
					that.editCertificationReady();
				}
				
			});
			

			/**
			 * When some massive action is selected in some Combo Grid/List views
			 */
			$(document).on("combogridview.massive-action-selected combolistview.massive-action-selected", function(e, data) {
				
				if (data.selection === null || typeof data.selection === "undefined" || data.selection.length <= 0) {
					return;
				}
				
				var url = that.options.massActionUrl;
				var title = '';
				var id = 'certifications-mass-action-dialog';
				var modalClass = 'certifications-mass-action-dialog';
				var ajaxType = 'POST';
				
				// Determine the type of items we rae going to delete: certifications or certification items
				// depending on where the trigger originates from
				var type = false;
				if (data.gridId && data.gridId == that.options.certsGridId) 
					type = "certifications";
				else if (data.listId && data.listId == that.options.certItemsListId)
					type = "certitems";
				if (type === false)
					return;

				data.type = type;
				
				switch (data.action) {
					case 'delete' :
						title = "<span></span>" + Yii.t('standard', '_DEL');
						break;
						
					default:
						// Unknown action? Get out!
						return;
				}
				
				var settings = {
					title: title,
				    content: url,
			        ajaxType: ajaxType,  // POST, GET, null
			        id: id,
			        modalClass: modalClass,
			        data: data
				};
			    $('<div/>').dialog2(settings);
			    
			});
			
			
			$(document).on('click', '.combo-list-item .close', function(){
				var key = $(this).closest('.combo-list-item').data('key');
			});

			
			
		},
		

		/**
		 * CREATE/EDIT dialog has been loaded
		 */
		editCertificationReady: function() {
			TinyMce.attach('#certification-description', 'standard', {height: 170});
		}
		
		
		
		
	};
	
	
	
	
	
	
	
}( jQuery ));
