
<?php

class CertificationAppController extends AdminController {
	
	
	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
				'accessControl', // perform access control for CRUD operations
		);
	}
	
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();
	
		$admin_only_actions = array(
			'index', 
			'edit', 
			'delete', 
			'settings', 
			'certAutocomplete', 
			'certItemsAutocomplete', 
			'massAction', 
			'assignItems', 
			'deleteCertItem'
		);
		
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);
	
		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);
	
		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);
	
		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);
		
		return $res;
	}

	
	public function init() {
		parent::init();
		JsTrans::addCategories('certification');
	}
	
	public function actionSettings() {
	
		$settings = new CertificationAppSettingsForm();
	
		if (isset($_POST['CertificationAppSettingsForm'])) {
			$settings->setAttributes($_POST['CertificationAppSettingsForm']);
	
			if ($settings->validate()) {
				Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));
			}
			else {
				Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
			}
		}
	
		$this->render('settings', array('settings' => $settings));
	}

	

	/**
	 * ADMIN HOME
	 */
	public function actionIndex() {
		
		$this->registerResources();
		
		$searchInput = Yii::app()->request->getParam('search_input', false);
		$selectAll = Yii::app()->request->getParam('comboGridViewSelectAll', false);

		$model = new Certification();
		$model->search_input = $searchInput ? $searchInput : null;
		
		if ($selectAll) {
			$dataProvider = $model->sqlDataProvider();
			$dataProvider->setPagination(false);
			$keys = $dataProvider->keys;
			foreach ($keys as $index => $key) {
				$keys[$index] = (int) $key;
			}
			echo json_encode($keys);
			Yii::app()->end();
		}

		$certsGridId = 'certifications-grid';

		// Define array of available massive actions to show: <value> => <label>
		$massiveActions = array(
			'delete' => Yii::t('standard', '_DEL'),
		);
		
		$dataProvider = $model->sqlDataProvider();
		//$dataProvider->pagination = array('pageSize' => 3);
		
		$certsGridParams = array(
			'gridId' 			=> $certsGridId,
			'dataProvider'		=> $dataProvider,
			'columns'			=> $this->getCertsGridColumns(),
			'autocompleteRoute'	=> '//CertificationApp/CertificationApp/certsAutocomplete',
			'massiveActions'	=> $massiveActions,
			//'disableMassSelection' => true,	
			//'disableMassActions' => true,
		);
		
		$params = array(
			'certsGridParams'			=> $certsGridParams,
			'model'						=> $model,
			'editCertDialogId' 			=> 'edit-certification-dialog',
		);
		
		// If this is an Yii Grid Update request (detected by ajax=<grid-ID> in the Request)
		// just render-partial the grid. This hugely improves the speed!!!
		if (isset($_REQUEST['ajax']) && ($_REQUEST['ajax'] == $certsGridId)) {
			$this->widget('common.widgets.ComboGridView', $certsGridParams);
			Yii::app()->end();
		}
		
		
		$this->render('index', $params);
	}
	
	
	/**
	 * Edit/Create Certification MVC
	 */
	public function actionEdit() {
		
		$idCert 		= Yii::app()->request->getParam('id_cert', false);
		$certification 	= Yii::app()->request->getParam('Certification', false);
		
		try {
			
			$model = Certification::model()->findByPk($idCert);
			if (!$model) {
				$model = new Certification();
			}
				
			// POSTed form ?
			if ($certification) {
				$model->attributes = $certification;
				$ok = $model->save();
				if ($ok) {
					echo '<a class="auto-close"></a>';
					Yii::app()->end();
				}
			}
			
			$html = $this->renderPartial('_edit', array(
				'model'			=> $model,
			), true, true);
			echo $html;
			
		}
		catch (Exception $e) {
			$this->renderPartial('admin.protected.views.common._dialog2_error', array(
				'type' => 'error',
				'message' => $e->getMessage(),
			));
			Yii::app()->end();
		}
		
	}
	
	/**
	 * Delete single certification MVC
	 * 
	 * @throws Exception
	 */
	public function actionDelete() {
		
		$idCert 		= Yii::app()->request->getParam('id_cert', false);
		$confirmDelete	= Yii::app()->request->getParam('confirm_delete', false);
		$model 			= Certification::model()->findByPk($idCert);
		
		try {
			if (!$model) {
				throw new Exception('Invalid certification');					
			}
			
			// Is already issued to ANYONE?
			$isIssued = Certification::isIssuedTo(array($idCert));
			
			if ($confirmDelete) {
				
				// ASK user to AGREE the deletion if the certification is already issued to someone
				if (!$isIssued || ($isIssued && (Yii::app()->request->getParam('agree_delete', false) === 'agree'))) {
					$model->deleted = Certification::$CERTIFICATION_SOFT_DELETED;
					$model->save(false);
//					$model->delete();
				}
					
				echo '<a href="" class="auto-close"></a>';
				Yii::app()->end();
			}
			
			$html = $this->renderPartial('_delete', array(
				'model'		=> $model,
				'isIssued' 	=> $isIssued,	
			), true, true);
			echo $html;
		}
		catch (Exception $e) {
			$this->renderPartial('admin.protected.views.common._dialog2_error', array(
				'type' => 'error',
				'message' => $e->getMessage(),
			));
			Yii::app()->end();
		}
		
		
		
		
	}
	

	/**
	 * Register client resources (CSS, JS,..)
	 */
	public function registerResources() {
		if (!Yii::app()->request->isAjaxRequest) {
			
			$cs = Yii::app()->getClientScript();
			
			$assetsUrl = $this->module->getAssetsUrl();
			Yii::app()->tinymce->init();
			
			$cs->registerScriptFile($assetsUrl 	. '/js/certification.js');
			$cs->registerCssFile($assetsUrl 	. '/css/certification.css');
			
		}
		
		
		
	}
	

	/**
	 * Render the "expiration" column 
	 * 
	 * @param array $data
	 * @param integer $index
	 * @return string
	 */
	protected function renderExpirationColumn($data, $index) {
		if ($data['duration'] > 0) { 
			$unitLabels = Certification::expirationDurationUnits();
			$html = $data['duration'] . ' ' . $unitLabels[$data['duration_unit']];
		} 
		else {
			$html = Yii::t('standard', '_NEVER');
		}
		return $html;
	}
	

	/**
	 * Render the "assign items" column 
	 * 
	 * @param array $data
	 * @param integer $index
	 * @return string
	 */
	protected function renderAssignItemsColumn($data, $index) {
		$html = $this->renderPartial('_assign_items_column', array(
			'data' => $data,
		));
		return $html;
	}

	
	/**
	 * Render the "certification operations" column
	 * 
	 * @param array $data
	 * @param integer $index
	 * @return string
	 */
	protected function renderCertActions($data, $index) {
		$html = $this->renderPartial('_operations', array(
			'data' => $data,	
		)); 
		return $html;
	}
	
	
	/**
	 * Certifications grid columns
	 * 
	 * @return array
	 */
	protected function getCertsGridColumns() {
        $truncate = 75;
		$columns = array(
			array(
				'class' => 'CCheckBoxColumn',
				'id' 	=> 'items-grid-checkboxes',
				'value' => '$data["id_cert"]',
				'htmlOptions'=>array('style'=>'max-width: 300px; word-wrap: break-word; vertical-align:top; padding: 6px 15px;'),
			),
					
			array(
				'name' => 'name',
				'value' => 'strip_tags($data["title"])', // Strip tags LB-1092
				/*'type' => 'raw',*/
				'header' => Yii::t('standard', '_NAME'),    
				'htmlOptions'=>array('style'=>'max-width: 300px; word-wrap: break-word; vertical-align:top'),
				'headerHtmlOptions' => array(
				),
			),
					
			array(
				'name' => 'description',
				'value' => 'substr(strip_tags($data["description"]), 0, ' . $truncate . ') . (strlen($data["description"]) > ' . $truncate . ' ? "..." : "")', // truncating and strip the HTML tags LB-1092
				'type' => 'raw',
				'header' => Yii::t('standard', '_DESCRIPTION'),
				'htmlOptions'=>array('style'=>'max-width: 300px; word-wrap: break-word; vertical-align:top'),
				'headerHtmlOptions' => array(
				),
			),
					
			array(
				'name' => 'expiration',
				'value' => array($this, 'renderExpirationColumn'),
				'type' => 'raw',
				'header' => Yii::t('standard', 'Expiration'),
				'htmlOptions'=>array('style'=>'max-width: 300px; word-wrap: break-word; vertical-align:top'),
				'headerHtmlOptions' => array(
				),
			),
					
			array(
				'name' => 'assign_item',
				'value' => array($this, 'renderAssignItemsColumn'),
				'type' => 'raw',
				'header' => Yii::t('standard', '_ASSIGN'),
				'htmlOptions'=>array('style'=>'max-width: 300px; word-wrap: break-word; vertical-align:top; text-align:right;'),
				'headerHtmlOptions' => array(
				),
			),
					
			array(
				'name' => 'edit_cert',
				'value' => array($this, 'renderCertActions'),
				'type' => 'raw',
				'header' => '',
				'htmlOptions' => array(
					'style'=>'max-width: 300px; word-wrap: break-word; vertical-align:top',
					'class' => 'text-right'	
				),
				'headerHtmlOptions' => array(
				),
			)
		);
		
		
		return $columns;
		
	}
	
	/**
	 * The backend counterpart of the "certifications search autocomplete" (JuiAutocomplete)
	 */
	public function actionCertsAutocomplete() {
		
		$maxNumber 		= Yii::app()->request->getParam('max_number', 10);
		$term 			= Yii::app()->request->getParam('term', false);
		
		$model = new Certification();

		$model->search_input = $term ? $term : null; 
		
		$dataProvider = $model->sqlDataProvider();
		$dataProvider->setPagination(array(
			'pageSize' => $maxNumber,
		));
		$data = $dataProvider->getData();
		$result = array();
		if ($data) {
			foreach ($data as $item) {
				$result[] = array(
					'label' => $item['title'],
					'value' => $item['title'],
				);
			}
		}
		echo CJSON::encode($result);
	}
	
	
	/**
	 * The backend counterpart of the "certification items search autocomplete" (JuiAutocomplete)
	 * 
	 */
	public function actionCertItemsAutocomplete() {
	
		$maxNumber 		= Yii::app()->request->getParam('max_number', 10);
		$term 			= Yii::app()->request->getParam('term', false);
		$idCert			= Yii::app()->request->getParam('id_cert', false);
	
		$model = new CertificationItem();
	
		$model->search_input = $term ? $term : null;
		$model->id_cert = $idCert;
	
		$dataProvider = $model->sqlDataProvider();
		$dataProvider->setPagination(array(
			'pageSize' => $maxNumber,
		));
		$data = $dataProvider->getData();
		
		$result = array();
		if ($data) {
			foreach ($data as $item) {
				if ($item['item_type'] == CertificationItem::TYPE_COURSE) {
					$field = 'name';
				}
				else if ($item['item_type'] == CertificationItem::TYPE_LEARNING_PLAN) {
					$field = 'path_name';
				}
				else if ($item['item_type'] == CertificationItem::TYPE_TRANSCRIPT) {
					$field = 'course_name';
				}
				
				$result[] = array(
						'label' => $item[$field],
						'value' => $item[$field],
				);
			}
		}
		echo CJSON::encode($result);
	}
	
	
	/**
	 * Get list of certifications, usually used in "select all" logic
	 * 
	 * @param Certification $model
	 */
	protected function getCertsList(Certification $model) {
		$dataProvider = $model->sqlDataProvider();
		$dataProvider->setPagination(false);
		$data = $dataProvider->keys;
		$response = new AjaxResult();
		$response->setStatus(true)->setData($data)->toJSON();
		Yii::app()->end();
	}
	
	
	
	/**
	 * Mass operations logic
	 *  
	 */
	public function actionMassAction() {

		$action		= Yii::app()->request->getParam('action', false);
		$selection 	= Yii::app()->request->getParam('selection', false);
		$type		= Yii::app()->request->getParam('type', false);
		
		if ($action === 'delete' && $type === 'certifications') {
			$this->massDeleteCertifications($selection);
			Yii::app()->end();	
		}
		else if ($action === 'delete' && $type === 'certitems') {
			$this->massDeleteCertificationItems($selection);
			Yii::app()->end();
		}
		
		
		$this->renderPartial('admin.protected.views.common._dialog2_error', array(
			'type' => 'error',
			'message' => 'Invalid mass action',
		));
		Yii::app()->end();
		
	}
	
	
	/**
	 * Assign Items to certification MVC
	 * 
	 * @throws Exception
	 */
	public function actionAssignItems() {
		
		$this->registerResources();
		
		$idCert			= Yii::app()->request->getParam('id_cert', false);
		$selectAll 		= Yii::app()->request->getParam('comboListViewSelectAll', false);
		$searchInput 	= Yii::app()->request->getParam('search_input', false);
		
		try {
			
			$model = Certification::model()->findByPk($idCert);
			$certItemModel = new CertificationItem();
			$certItemModel->id_cert = $idCert;
			$certItemModel->search_input = $searchInput;
			
			if ($selectAll) {
				$dataProvider = $certItemModel->sqlDataProvider();
				$dataProvider->setPagination(false);
				$keys = $dataProvider->keys;
				foreach ($keys as $index => $key) {
					$keys[$index] = (int) $key;
				}
				echo json_encode($keys);
				Yii::app()->end();
			}
				
			
			if (!$model) {
				throw new Exception(Yii::t('certification', 'Invalid certification'));
			}
		
			if (isset($_REQUEST['confirm'])) {
				$selectedCourses 	= UsersSelector::getCoursesListOnly($_REQUEST);
				$selectedPlans 		= UsersSelector::getPlansListOnly($_REQUEST);
				
				$this->saveAssignedItems($idCert, $selectedCourses, $selectedPlans);
				echo '<a class="auto-close success">';
				Yii::app()->end();
			}
		
			if (!Yii::app()->request->isAjaxRequest) {
				// Register UserSelector JS
				UsersSelector::registerClientScripts();
			}
		
			$listViewId = 'certification-items-list';
			$dataProvider = $certItemModel->sqlDataProvider();
			//$dataProvider->pagination = array('pageSize' => 3); 
			$listViewParams = array(
				'autocompleteRoute' => '//CertificationApp/CertificationApp/certItemsAutocomplete',
				'massiveActions'	=> array('delete' => Yii::t('standard', '_DEL')),
				'listId'			=> $listViewId,
				'dataProvider'		=> $dataProvider,
				'listItemView'		=> 'plugin.CertificationApp.views.certificationApp._cert_item_list_view',
				'hiddenFields'		=> array('id_cert' => $idCert),
				//'disableMassSelection' => true,
				//'disableMassActions' => true,
				//'disableCheckBox' => true,
			);
			
			
			if (isset($_REQUEST['ajax']) && ($_REQUEST['ajax'] == $listViewId)) {
				$this->widget('common.widgets.ComboListView', $listViewParams);
				Yii::app()->end();
			}
				
		
			$this->render('assign_items', array(
				'model'				=>	$model,
				'certItemModel'	 	=>	$certItemModel,
				'listViewParams'	=>  $listViewParams,	
			));
		}
		catch (Exception $e) {
			$this->renderPartial('admin.protected.views.common._dialog2_error', array(
				'type' => 'error',
				'message' => 'Invalid mass action',
			));
			Yii::app()->end();
		}
		
		
	}
	
	/**
	 * Delete single certification item
	 * 
	 * @throws Exception
	 */
	public function actionDeleteCertItem() {
		
		$id 			= Yii::app()->request->getParam('id', false);
		$confirmDelete	= Yii::app()->request->getParam('confirm_delete', false);
		$model 			= CertificationItem::model()->findByPk($id);
		
		try {
			if (!$model) {
				throw new Exception('Invalid certification item');					
			}
			
			$isIssued = CertificationUser::model()->findByAttributes(array('id_cert_item' => $id)); 
			
			if ($confirmDelete) {
				
				// ASK user to AGREE the deletion if the certification is already issued to someone
				if (!$isIssued || ($isIssued && (Yii::app()->request->getParam('agree_delete', false) === 'agree'))) {
					$model->delete();
				}
				
				echo '<a href="" class="auto-close"></a>';
				Yii::app()->end();
			}
			
			$html = $this->renderPartial('_delete_cert_item', array(
				'model'			=> $model,
				'isIssued'		=> $isIssued,
			), true, true);
			echo $html;
		}
		catch (Exception $e) {
			$this->renderPartial('admin.protected.views.common._dialog2_error', array(
				'type' => 'error',
				'message' => $e->getMessage(),
			));
			Yii::app()->end();
		}
				
		
	}
	
	
	
	/**
	* Mass-Remove certifications
	*
	* @param array|string $selection
	* 
	* @throws Exception
	 */
	protected function massDeleteCertifications($selection) {
		
		if (is_string($selection)) {
			$selection = explode(',', $selection);
		}
		
		try {
			
			$confirmDelete	= Yii::app()->request->getParam('confirm_delete', false);
			
			$isIssued = Certification::isIssuedTo($selection);
			
			if (count($selection) <= 0 || empty($selection)) {
				throw new Exception(Yii::t('certification', 'Invalid selection'));
			}
			
			if ($confirmDelete) {
				$model = new Certification();
				// ASK user to AGREE the deletion if there is some issued certifications among those being deleted
				if (!$isIssued || ($isIssued && (Yii::app()->request->getParam('agree_delete', false) === 'agree'))) {
					foreach($selection as $selected){
						$selectedModel = Certification::model()->findByPk($selected);
						if($selectedModel){
							$selectedModel->deleted = Certification::$CERTIFICATION_SOFT_DELETED;
							$selectedModel->save(false);
						}
					}
//					$model->deleteAllByAttributes(array('id_cert' => $selection));
				}
				echo '<a href="" class="auto-close"></a>';
				Yii::app()->end();
			}
				
			$html = $this->renderPartial('_mass_delete', array(
				'selection' => $selection,
				'isIssued' 	=> $isIssued,	
			), true, true);
			echo $html;
		}
		catch (Exception $e) {
			$this->renderPartial('admin.protected.views.common._dialog2_error', array(
				'type' => 'error',
				'message' => $e->getMessage(),
			));
			Yii::app()->end();
		}
		
		
		
	}
	
	/**
	 * Mass-Remove items from a certification
	 * 
	 * @param array|string $selection
	 * @throws Exception
	 */
	protected function massDeleteCertificationItems($selection) {
		
		if (is_string($selection)) {
			$selection = explode(',', $selection);
		}
	
		try {
			
			$isIssued = CertificationUser::model()->findByAttributes(array('id_cert_item' => $selection));
				
			$confirmDelete	= Yii::app()->request->getParam('confirm_delete', false);
				
			if (count($selection) <= 0) {
				throw new Exception(Yii::t('certification', 'Invalid selection'));
			}
				
			// Confirm button clicked?
			if ($confirmDelete) {
				$model = new CertificationItem();
				// ASK user to AGREE the deletion if there is some issued certifications related to items among those being deleted
				if (!$isIssued || ($isIssued && (Yii::app()->request->getParam('agree_delete', false) === 'agree'))) {
					$model->deleteAllByAttributes(array('id' => $selection));
				}
				echo '<a href="" class="auto-close"></a>';
				Yii::app()->end();
			}
	
			$html = $this->renderPartial('_mass_delete_cert_items', array(
				'selection' => $selection,
				'isIssued' 	=> $isIssued,
			), true, true);
			
			echo $html;
		}
		catch (Exception $e) {
			$this->renderPartial('admin.protected.views.common._dialog2_error', array(
					'type' => 'error',
					'message' => $e->getMessage(),
			));
			Yii::app()->end();
		}
	
	
	
	}
	
	

	
	/**
	 * Save/Assign selected items to a certification
	 * 
	 * @param integer $idCert
	 * @param array $courses
	 * @param array $plans
	 * 
	 * @throws CException
	 */
	protected function saveAssignedItems($idCert, $courses=array(), $plans=array()) {

		foreach ($courses as $idItem) {
			$model = new CertificationItem();
			$model->id_cert = $idCert;
			$model->id_item = $idItem;
			$model->item_type = CertificationItem::TYPE_COURSE;
			if (!$model->save()) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));				
			}
		}

		foreach ($plans as $idItem) {
			$model = new CertificationItem();
			$model->id_cert = $idCert;
			$model->id_item = $idItem;
			$model->item_type = CertificationItem::TYPE_LEARNING_PLAN;
			if (!$model->save()) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}
		}
		
		
	}
	

	public function actionRenew($cu_id){
	
		if(!PluginManager::isPluginActive('CertificationApp')) throw new CException('Certification app inactive');
	
		try {
			$cuModel = CertificationUser::model()->findByPk($cu_id);
			if (!$cuModel) {
				throw new CException(Yii::t("certification", "User certification not found"));
			}
				
			if ($cuModel->expire_at == '0000-00-00 00:00:00' || is_null($cuModel->expire_at) || !$cuModel->expire_at) {
				throw new CException(Yii::t("certification", "Cannot renew a certification which never expires!"));
			}
				
			// Get assignment information about the certification related to this particular user certification
			$assignedItems = $cuModel->certificationItem->certification->getAssignmentInfo();
			$certModel = $cuModel->certificationItem->certification;
				
			$items = array();
			$itemsToReset = array();
			$idUser = Yii::app()->user->id;
			foreach ($assignedItems['courses'] as $assignedItemId) {
				// If this item is already used in ANY user's certification in the past?
				$isAlreadyUsed = Certification::certificationsUsedLearningItemToCertifyUser($assignedItemId, CertificationItem::TYPE_COURSE, $idUser);
	
				// Check if are allowed to re-train using the same item
				// If not, we must exclude the item from the list
				$allowRetrain = $certModel->allow_same_item || $isAlreadyUsed === false;
				if ($allowRetrain) {
					if ($isAlreadyUsed !== false) {
						$itemsToReset[] = CertificationItem::TYPE_COURSE . "_" . $assignedItemId;
					}
					$items[] = LearningCourse::model()->findByPk($assignedItemId);
				}
			}
			foreach ($assignedItems['plans'] as $assignedItemId) {
				$isAlreadyUsed = Certification::certificationsUsedLearningItemToCertifyUser($assignedItemId, CertificationItem::TYPE_LEARNING_PLAN, $idUser);
				$allowRetrain = $certModel->allow_same_item || $isAlreadyUsed === false;
				if ($allowRetrain) {
					if ($isAlreadyUsed !== false) {
						$itemsToReset[] = CertificationItem::TYPE_LEARNING_PLAN . "_" . $assignedItemId;
					}
					$items[] = LearningCoursepath::model()->findByPk($assignedItemId);
				}
			}
				
			// Create and save a one time reset token which will be observed by Player,
			// effectively asking the Player to RESET user tracking before actually play the course
			// See PlayerBaseController
			$resetToken = Docebo::randomHash();
			$cuModel->reset_token = $resetToken;
			$cuModel->save();
				
			$this->render('_renew_grid', array(
					'model'			=> $certModel,
					'cuModel'		=> $cuModel,
					'items'			=> $items,
					'itemsToReset'	=> $itemsToReset,
					'resetToken'	=> $resetToken,
			));
	
		}
		catch (Exception $e) {
			Yii::app()->user->setFlash('error', $e->getMessage());
			$this->redirect(Docebo::createLmsUrl('site/index'), true);
		}
	
	}
	
	
}