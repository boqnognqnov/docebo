<?php
/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */
class CertificationAppModule extends CWebModule {
	
	
	/**
	 * URL to this module's assets (web accessible)
	 * 
	 * @var string
	 */
	protected $_assetsUrl = null;
	

	/**
	 * Initialization method
	 */
	public function init() {
		// this method is called when the module is being created
		// you may place code here to customize the module or the application
		$this->setImport(array(
			'CertificationApp.components.*',
		));
		
		Yii::app()->event->on(EventManager::EVENT_STUDENT_COMPLETED_COURSE, array($this, 'studentCompletedCourse'));
		Yii::app()->event->on(EventManager::EVENT_STUDENT_COMPLETED_LEARNING_PLAN, array($this, 'studentCompletedLearningPlan'));
		Yii::app()->event->on('ExternalTrainingApproved', array($this, 'externalTrainingApproved'));
		Yii::app()->event->on('CollectCustomNotifications', array($this, 'collectCustomNotifications'));
		Yii::app()->event->on(EventManager::EVENT_COURSE_DELETED, array($this, 'deleteCourseFromCertifications'));
		
	}

	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu() {
		// Add menu voices !
		Yii::app()->mainmenu->addAppMenu('certification', array(
			'icon' 		=> 'admin-ico certification',
			'app_icon' => 'fa-certificate', // icon used in the new menu
			'link' 		=> Docebo::createAdminUrl('//CertificationApp/certificationApp/index'),
			'label' 	=> Yii::t('certification', 'Certifications & Retraining'),
			//'settings' 	=> Docebo::createAdminUrl('//CertificationApp/certificationApp/settings'),
		), array());
	}
	
	
	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('CertificationApp.assets'));
		}
		return $this->_assetsUrl;
	}

	/**
	 * Catches the Course Deleted event and deletes the item from all certifications
	 * @param DEvent $event
	 */
	public function deleteCourseFromCertifications($event) {
		$course = $event->params['course'];
		CertificationItem::model()->deleteAllByAttributes(array(
			'id_item'	=> $course->idCourse,
			'item_type' => CertificationItem::TYPE_COURSE
		));
	}

	/**
	 * Issue a certification to the user completing a Course, IF the Course is associated to a certification
	 *
	 * @param DEvent $event
	 */
	public function studentCompletedCourse($event) {
		
		$course = $event->params['course'];
		$user 	= $event->params['user'];
		
		$certItem = CertificationItem::model()->findByAttributes(array(
			'id_item'	=> $course->idCourse,		
			'item_type' => CertificationItem::TYPE_COURSE
		));
		
		if ($certItem) {
			$extraData = array(
				'score' => LearningCourseuser::getLastScoreByUserAndCourse($course->idCourse, $user->idst),	
			);
			CertificationUser::issueCertification($certItem->id, $user->idst, $extraData);
		}
		
	}
	

	/**
	 * Issue a certification to the user completing a Learning Plan, IF the plan is associated to a certification
	 *  
	 * @param DEvent $event
	 */
	public function studentCompletedLearningPlan($event) {
		
		$learningPlan 	= $event->params['learningPlan'];
		$user 			= $event->params['user'];
	
		$certItem = CertificationItem::model()->findByAttributes(array(
			'id_item'	=> $learningPlan->id_path,
			'item_type' => CertificationItem::TYPE_LEARNING_PLAN
		));
	
		if ($certItem) {
			CertificationUser::issueCertification($certItem->id, $user->idst);
		}
	
	}

	
	/**
	 * Issue a certification to the user upon extrenal training aproval 
	 *
	 * @param DEvent $event
	 */
	public function externalTrainingApproved($event) {
	
		$transcript 	= $event->params['transcript'];
		$user 			= $event->params['user'];
	
		$certItem = CertificationItem::model()->findByAttributes(array(
				'id_item'	=> $transcript->id_record,
				'item_type' => CertificationItem::TYPE_TRANSCRIPT,
		));
	
		if ($certItem) {
			CertificationUser::issueCertification($certItem->id, $user->idst, array(), $transcript);
		}
	
	}
	
	
	
	/**
	 * Event handler.
	 * Add Notification descriptor(s) to event raiser's supplied array of notification
	 * 
	 * @param DEvent $event
	 */
	public function collectCustomNotifications($event) {
		if (isset($event->params['notifications']) && is_array($event->params['notifications'])) {
			
			// This is passed by reference,  s we can modify it
			$notificationsArray = $event->params['notifications'];
			
			// List classes known to THIS plugin
			// BE SURE THIS IS A VALID, loadable class name!!!! 
			$myHandlers = array('CertificationIssued', 'CertificationExpired');
			//$myHandlers = array('CertificationIssued');
			
			// Enumerate all classes and call their descriptor() methods
			foreach ($myHandlers as $handlerName) {
				$descriptor = $handlerName::descriptor();
				$descriptor->group = Yii::t('certification','Certification');
				$event->params['notifications'][] = $descriptor;
			}
			
		} 
	}
	
	
	
	
	
	
}
