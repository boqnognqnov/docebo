<?php
	/* @var $model Certification */
	$this->breadcrumbs = array(
		Yii::t('certification', 'Certifications & Retraining'),
	);
	
	$massActionUrl = Docebo::createAdminUrl('//CertificationApp/CertificationApp/massAction');
	
?>

<h2 class="title-bold"><?= Yii::t('certification', 'Certifications & Retraining') ?></h2>
<br/>

<?php
	$this->renderPartial('_mainActions', array(
		'editCertDialogId' => $editCertDialogId,
	));
?>


<div class="clearfix">

	<?php $this->widget('common.widgets.ComboGridView', $certsGridParams); ?>
	
</div>


<script type="text/javascript">
/*<![CDATA[*/
           
	var options = {
		editCertDialogId				: <?= json_encode($editCertDialogId) ?>,
		certsGridId						: <?= json_encode($certsGridParams['gridId']) ?>,
		massActionUrl					: <?= json_encode($massActionUrl) ?>		
	};           
	var CertificationMan = new CertificationManClass(options);

	//restore default selected value after change
	$(function(){
		$('.combo-grid-selection-massive-action').change(function(){
			$(this).val('');
		});
		$('#certifications-grid table tr td').css('max-width','300px');
		$('#certifications-grid table tr td').css('word-wrap','break-word');
		$('#certifications-grid table tr td').css('vertical-align','top');
	});
	
/*]]>*/
</script>

