<?php 
/* @var $model Certification */
/* @var $items LearningCourse[]|LearningCoursepath[] */

	$this->breadcrumbs = array(
		Yii::t('certification', 'Renew your certification'),
	);

?>

<h3 class="title-bold"><?=Yii::t('certification', 'Certifications & Retraining')?></h3>

<div style="border: 1px solid #D3D3D3; padding: 10px 15px; margin-bottom: 25px;">
	<div class="pull-left" style="min-height: 40px;">
		<span class="fa fa-exclamation-circle certifications-expiring-warning"></span>
	</div>
	<div>
		<h4>
		<?php
			if ($cuModel->isExpired()) {
				$header1 = Yii::t('certification', 'Your certification {name} has expired at {date}.', array(
					'{name}'=>  strip_tags($model->title),
					'{date}'=>$cuModel->expire_at,
				)) ;	
			} 
			else {
				$header1 = Yii::t('certification', 'Your certification {name} is going to expire at {date}.', array(
					'{name}'=>  strip_tags($model->title),
					'{date}'=>$cuModel->expire_at,
				)) ;
			}
			
			echo $header1;
			
		?>
		</h4>
		<?=Yii::t('certification', 'Please subscribe and complete one of the following courses or learning plans in order to renew it.')?>
		<br>
		<?=Yii::t('certification', 'Selecting an item you\'ve already used to obtain this certification in the past, will result in a complete tracking data reset for that item!')?>
	</div>
</div>

<ul class="thumbnails fixed tiles">
<?php foreach($items as $item): ?>
	<?php
	
		$tmpResetToken = $resetToken;
	
		if($item instanceof LearningCourse){
			$key = CertificationItem::TYPE_COURSE . "_" . $item->idCourse;
			if (!in_array($key, $itemsToReset)) {
				$tmpResetToken = false;
			}
			$portlet = $this->widget('common.widgets.CoursePortlet', array(
				'courseModel'     => $item,
				'courseDetailUrl' => Yii::app()->createUrl('course/axDetails', array('id' => $item->idCourse)),
				'resetToken' 	  => $tmpResetToken,	
				'gridType'        => CoursesGrid::$GRIDTYPE_INTERNAL_MYCOURSES, // fake it, no need to reinvent the wheel
				'idUser'          => Yii::app()->user->id,
			));
		}
		else if ($item instanceof LearningCoursepath) {
			$key = CertificationItem::TYPE_LEARNING_PLAN . "_" . $item->id_path;
			if (!in_array($key, $itemsToReset)) {
				$tmpResetToken = false;
			}
			$portlet = $this->widget('common.widgets.CoursepathPortlet', array(
				'coursepathModel'     => $item,
				'coursepathDetailUrl' => Yii::app()->createUrl('coursepath/details', array('id_path' => $item->id_path)),
				'resetToken' 	  => $tmpResetToken,
				'gridType'        => CoursesGrid::$GRIDTYPE_INTERNAL_MYCOURSES, // fake it, no need to reinvent the wheel
				'idUser'          => Yii::app()->user->id,
			));
		}
	?>
<? endforeach; ?>
</ul>

<p class="clearfix"></p>

<? if(PluginManager::isPluginActive('TranscriptsApp') && Settings::get('transcripts_allow_users')): ?>
<div class="certification-renew-alternative">
	<span class="icon-my-activities pull-left"></span>
	<a class="btn-docebo green big pull-right my-activities" href="<?=Docebo::createLmsUrl('TranscriptsApp/TranscriptsUser/index')?>"><?=Yii::t('certification', 'Go to my external activities')?></a>
	<?=Yii::t('certifications', 'Alternatively, you can submit a new external activity valid for this certification. It will be avaluated by your instructor and eventually approved or rejected')?>
</div>
<? endif; ?>
<script>
	$(function(){
		$(document).on('mouseenter touchstart', '.tile.course', function(){
			var $this = $(this);
			var courseInfo = $this.find('.course-hover-details');
			if (courseInfo.hasClass('empty')) {
				courseInfo.load($(this).attr('data-details-url'), function () {
					courseInfo.removeClass('empty');
					$(document).controls();
				});
			}
		});
	})
</script>