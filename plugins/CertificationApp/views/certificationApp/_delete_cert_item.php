<style>
<!--
	.modal.delete-certification-item-dialog  {
		width: 500px;
		margin-left: -250px;	
	}
	
	.delete-item-name {
		font-size: 14px;
	}
		
-->
</style>

<?php 
	$form = $this->beginWidget('CActiveForm', array(
    	'id' => 'delete-certification-item-form',
    	'method' => 'POST',
    	'htmlOptions' => array(
        	'class' => 'ajax'
    	),
	));
	
	// In tis case model->id is already set and the data provider should return a single row of data, hence [0]
	$data = $model->sqlDataProvider()->getData();
	$name = $model->item_type == CertificationItem::TYPE_COURSE ? $data[0]['name'] : $data[0]['path_name'];
	
?>

<?php
	  
?>

<div class="row-fluid">
	<div class="span12">
		<?= Yii::t('certification', "Are you sure you want to remove this item from the certification?") ?>
	</div>
</div>


<?php if ($isIssued) : ?>
	<div class="row-fluid">
		<div class="span12">
			<div class="alert alert-warning alert-compact">
				<?= Yii::t('certification', 'This certification item is part of a certification, already issued to one or more users. Deleting it will also revoke the certification from users. Please confirm the deletion.') ?>
			</div>
			<br>
		</div>
	</div>
	
	<div class="row-fluid">
		<div class="span12">
		<label class="checkbox">
			<?= CHtml::checkBox('agree_delete', false, array('value' => 'agree')) ?>
			<?= Yii::t('certification', 'I understand that the certification related to this item will be revoked from users and this operation <strong>cannot be undone!</strong>') ?>
		</label>
		</div>
	</div>
<?php endif; ?>


<div class="form-actions">
    <input class="btn-docebo green big" type="submit" name="confirm_delete" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" />
    <input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>

<?php $this->endWidget(); ?>


<script type="text/javascript">

	$('input[name="agree_delete"]').styler();

</script>