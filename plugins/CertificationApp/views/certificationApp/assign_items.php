<?php
	/* @var $model Certification */
	/* @var $certItemModel CertificationItem */

	$this->breadcrumbs = array(
		Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
		Yii::t('certification', 'Certifications & Retraining'),
		strip_tags($model->title),
		Yii::t('certification', 'Assign items')
	);
	
	$assignItemsSelectorUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
		'type'  		=> UsersSelector::TYPE_CERTIFICATIONS_ASSIGN_ITEMS,
		'idGeneral'		=> $model->id_cert, 
		'idForm' 		=> 'selector-courses',
	));
	
	
	$massActionUrl = Docebo::createAdminUrl('//CertificationApp/CertificationApp/massAction');

?>


<div>

	<h3 class="title-bold back-button">
		<?php echo CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('//CertificationApp/CertificationApp/index')); ?>
        <span><?= Yii::t('certification', 'Assign items') . ": " . strip_tags($model->title) ?></span>
	</h3>
	
	<br>
	<div class="main-actions clearfix">
		<ul class="clearfix">
			<li>
				<div>
					<a href="<?= $assignItemsSelectorUrl ?>"
						alt="<?= Yii::t('certification', 'Assign items') ?>"
						class="open-dialog new-node"
						data-dialog-id = "certifications-assign-items-dialog"
						data-dialog-class = "certifications-assign-items-dialog"
						data-dialog-title="<?= Yii::t('certification', 'Assign items') ?>"
						>
						<span></span> <?= Yii::t('certification', 'Assign items') ?>
					</a>
				</div>
			</li>
		</ul>
		<div class="info">
			<div>
				<h4 class="clearfix"></h4>
				<p></p>
			</div>
		</div>
	</div>
	
	
</div>


<div class="clearfix">
	<?php 
		$this->widget('common.widgets.ComboListView', $listViewParams);
	?>
</div>


<script type="text/javascript">
/*<![CDATA[*/
           
	var options = {
		certItemsListId					: <?= json_encode($listViewParams['listId']) ?>,
		massActionUrl					: <?= json_encode($massActionUrl) ?>		
	};           
	var CertificationMan = new CertificationManClass(options);

	$(function(){
		$('input, select').styler();
		$('.combo-list-selection-massive-action').change(function(){
			$(this).val('');
		});
	});
	

/*]]>*/
</script>

