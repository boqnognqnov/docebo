<?php
/* @var $this CertificationAppController */


$assignmentInfo = Certification::model()->getAssignmentInfo($data['id_cert']);

if (!PluginManager::isPluginActive('TranscriptsApp')) {
	$subText = Yii::t('certification', '{n1} courses, {n2} paths', array(
		'{n1}' => count($assignmentInfo['courses']),
		'{n2}' => count($assignmentInfo['plans']),
	));
}
else {
	$subText = Yii::t('certification', '{n1} courses, {n2} paths, {n3} transcripts', array(
		'{n1}' => count($assignmentInfo['courses']),
		'{n2}' => count($assignmentInfo['plans']),
		'{n3}' => count($assignmentInfo['transcripts']),
	));
}


$infoText = '<span class="underline">' . $subText . ' </span>&nbsp;&nbsp;<span class="i-sprite is-forum"></span>';

$url = Docebo::createAdminUrl('//CertificationApp/CertificationApp/assignItems', array('id_cert' => $data['id_cert']));
echo  CHtml::link($infoText, $url, array(
	'class' => '',
	'rel' => 'tooltip',
	'data-html' => true,
));


?>




