<?php
	$editCertificationUrl = Docebo::createAdminUrl('CertificationApp/CertificationApp/edit'); 
?>
<div class="main-actions clearfix">
	<ul class="clearfix">
		<li>
			<div>
				<a href="<?= $editCertificationUrl ?>"
				    alt="<?= Yii::t('certification', 'Create new certification') ?>"
					class="open-dialog certification"
					data-dialog-id 		= '<?= $editCertDialogId ?>'
					data-dialog-class 	= '<?= $editCertDialogId ?>'
					data-dialog-title	= "<?= Yii::t('certification', 'New certification') ?>"
					>
					<span></span> <?= Yii::t('certification', 'New certification') ?>
				</a>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>
			<p></p>
		</div>
	</div>
</div>
