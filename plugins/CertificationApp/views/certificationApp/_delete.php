<style>
	<!--
	#delete-certification-dialog label {
		white-space: initial;
		max-width: 100%;
		padding-left: 20px;
		text-align: left;
	}

	-->
</style>
<?php
	$form = $this->beginWidget('CActiveForm', array(
    	'id' => 'delete-certification-form',
    	'method' => 'POST',
    	'htmlOptions' => array(
        	'class' => 'ajax form-horizontal'
    	),
	));
?>

<div class="row-fluid">
    <div class="span12" style="word-wrap: break-word;">
		<?= Yii::t('player', "Are you sure you want to delete:") . " <strong>".  strip_tags($model->title)."</strong> ?" ?>
	</div>
</div>

<?php if ($isIssued) : ?>

	<div class="row-fluid">
		<div class="span12">
			<div class="alert alert-warning alert-compact">
				<?= Yii::t('certification', 'This certification is already issued to one or more users. Deleting it will also revoke the certification from users. Please confirm the deletion.') ?>
			</div>
			<br>
		</div>
	</div>
	
	<div class="row-fluid">
		<div class="span12">
		<label class="checkbox">
			<?= CHtml::checkBox('agree_delete', false, array('value' => 'agree')) ?>
			<?= Yii::t('certification', 'I understand that all issued certifications of this type will be revoked from users and this operation <strong>cannot be undone!</strong>') ?>
		</label>
		</div>
	</div>
	

<?php endif; ?>

<div class="form-actions">
    <input class="btn-docebo green big" type="submit" name="confirm_delete" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" />
    <input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">

	$('input[name="agree_delete"]').styler();

</script>