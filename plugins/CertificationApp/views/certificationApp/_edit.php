<?php
	/* @var $model Certification */
	/* @var $form CActiveForm */

	$form = $this->beginWidget('CActiveForm', array(
    	'id' => 'edit-certification-form',
    	'method' => 'POST',
    	'htmlOptions' => array(
        	'class' => 'ajax docebo-form'
    	),
	));
?>




<div class="row-fluid">
	<div class="control-group">
        <?= $form->labelEx($model, 'title', array('class'=>'control-label')) ?>
        <div class="controls">
            <div class="row-fluid">
                <?= $form->textField($model, 'title', array('class' => 'span12')) ?>
                <?= $form->error($model, 'title') ?>
            </div>
        </div>
    </div>
</div>


<div class="row-fluid">
	<div class="control-group">
        <?= $form->labelEx($model, 'description', array('class'=>'control-label')) ?>
        <div class="controls">
            <div class="row-fluid">
                <?= $form->textArea($model, 'description', array('class' => 'span12', 'id' => 'certification-description')) ?>
                <?= $form->error($model, 'description') ?>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid">
	<div class="span12">
	
		<table>
			<tr>
				<td colspan="3">
					<?= $form->labelEx($model, 'duration', array('class'=>'control-label')) ?>
				</td>
			</tr>
						
			<tr>
				<td>
					<?= $form->textField($model, 'duration', array('style' => 'width: 50px;'));?>&nbsp;
				</td>
				<td>
					&nbsp;
					<?php
						echo $form->dropDownList(
							$model,
							'duration_unit', 
							Certification::expirationDurationUnits(),
							array(
								'style' => 'width: 150px;'
							)
						);
					?>
					&nbsp;
				</td>
			</tr>
			<tr><td colspan=2><?= $form->error($model, 'duration') ?></td></tr>
		</table>
	</div>
</div>

<br>
<div class="row-fluid">
   	<label class="checkbox"><?= $form->checkBox($model, 'allow_same_item', array('id' => 'allow_same_item')) ?><?= $model->getAttributeLabel('allow_same_item') ?></label>
</div>



<div class="form-actions">
	<input class="btn-docebo green big" type="submit" value="<?= Yii::t('standard', '_CONFIRM') ?>" />
	<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>


<?php 
	$this->endWidget();	
?>

<script type="text/javascript">
/*<![CDATA[*/
	$(function(){
     
     // Fix for Tiny MCE if browser is Firefox.
     //  Without this fix there is a error NS_ERROR_UNEXPECTED when you click Cancel in modal and you can NOT edit certificate anymore.
        try {
            tinymce.remove();
        } catch (e) {}
        
		$('#edit-certification-form :input[type="checkbox"]').styler();
		           
	});
/*]]>*/
</script>
