<?php
	$deleteUrl = Docebo::createAdminUrl('//CertificationApp/CertificationApp/deleteCertItem', array(
		'id' => $data['id'],
	));
?>

<?php if ( $data['item_type'] == CertificationItem::TYPE_LEARNING_PLAN ) : ?>
	<div class="span8">
		<?= $data['path_name'] ?>
	</div>
	<div class="span4 text-right">
		<?= Yii::t('standard', '_COURSEPATH') ?>
		<i class="docebo-sprite ico-elearning"></i>
		<a
			class="open-dialog"
			href="<?= $deleteUrl ?>"
			data-dialog-id="delete-certification-item-dialog"
			data-dialog-class="delete-certification-item-dialog"
			data-dialog-title="<?= Yii::t('standard', '_DEL') ?>"
		>
			<div class="close">&#10006;</div>
		</a>

	</div>

<?php elseif ( $data['item_type'] == CertificationItem::TYPE_COURSE ): ?>
	<div class="span8">
		<?= $data['name'] ?>
	</div>
	<div class="span4 text-right">
		<?
		if ($data['course_type'] == LearningCourse::TYPE_CLASSROOM) {
			echo Yii::t('standard', '_CLASSROOM')."&nbsp;";
			echo '<i class="docebo-sprite ico-classroom"></i>';
		}
		else if ($data['course_type'] == LearningCourse::TYPE_WEBINAR) {
			echo Yii::t('webinar', 'Webinar')."&nbsp;";
			echo '&nbsp;<i class="icon webinar-icon-small-inline" style="vertical-align: middle; margin-left: 3px;"></i>';
		}
		else {
			echo Yii::t('course', '_COURSE_TYPE_ELEARNING')."&nbsp;";
			echo '&nbsp;<i class="docebo-sprite ico-elearning"></i>';
		}
		?>
		<a
			class="open-dialog"
			href="<?= $deleteUrl ?>"
			data-dialog-id="delete-certification-item-dialog"
			data-dialog-class="delete-certification-item-dialog"
			data-dialog-title="<?= "<i class='i-sprite is-remove white'></i> " . Yii::t('standard', '_DEL') ?>"
		>
			<div class="close">&#10006;</div>
		</a>
	</div>

<?php elseif ( PluginManager::isPluginActive('TranscriptsApp') && ($data['item_type'] == CertificationItem::TYPE_TRANSCRIPT) ): ?>
	<div class="span8">
		<?= $data['course_name'] ?>
	</div>
	<div class="span4 text-right">
		<?= Yii::t('transcripts', 'External activities') ?>
		&nbsp;<i class="docebo-sprite ico-elearning"></i>
		<a
			class="open-dialog"
			href="<?= $deleteUrl ?>"
			data-dialog-id="delete-certification-item-dialog"
			data-dialog-class="delete-certification-item-dialog"
			data-dialog-title="<?= Yii::t('standard', '_DEL') ?>"
		>
			<div class="close">&#10006;</div>
		</a>
	</div>

<?php endif; ?>


