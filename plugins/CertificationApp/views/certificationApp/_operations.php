<?php
	/* @var $data Certification */
	$editUrl = Docebo::createAdminUrl("//CertificationApp/CertificationApp/edit", array('id_cert' => $data['id_cert']));
	$deleteUrl = Docebo::createAdminUrl("//CertificationApp/CertificationApp/delete", array('id_cert' => $data['id_cert']));
?>

<a 
	href="<?= $editUrl ?>" 
	class="open-dialog"
	data-dialog-id 		= "edit-certification-dialog" 
	data-dialog-class	= "edit-certification-dialog"
	data-dialog-title	= "<?= Yii::t('standard', '_MOD') ?>"
	>
	<span class="i-sprite is-edit"></span>
</a>

<a href="<?php echo $deleteUrl; ?>"
	class="open-dialog"
	data-dialog-class="delete-item-modal"
	closeOnEscape="false"
	closeOnOverlayClick="false"
	data-dialog-id="delete-certification-dialog"
	data-dialog-title	= "<?= Yii::t('standard', '_DEL') ?>"	
	>
	
	<span class="i-sprite is-remove red"></span>
</a>