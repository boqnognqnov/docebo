<style>
<!--

	.modal.certifications-mass-action-dialog {
		width: 600px;
		margin-left: -300px;
	} 

-->
</style>


<?php 
	$form = $this->beginWidget('CActiveForm', array(
    	'id' => 'delete-certification-form',
    	'method' => 'POST',
    	'htmlOptions' => array(
        	'class' => 'ajax'
    	),
	));
?>

<?= Chtml::hiddenField('selection', implode(',', $selection)) ?>
<?= Chtml::hiddenField('action', 'delete') ?>
<?= Chtml::hiddenField('type', 'certifications') ?>


<div class="row-fluid">
	<div class="span12">
		<?= Yii::t('certification', 'Are you sure you want to delete {count} certification(s)?', array('{count}' => count($selection))) ?>
	</div>
</div>

<?php if ($isIssued) : ?>

	<div class="row-fluid">
		<div class="span12">
			<div class="alert alert-warning alert-compact">
				<?= Yii::t('certification', 'Some certifications are already issued to one or more users. Deleting them will also revoke the certifications from users. Please confirm the deletion.') ?>
			</div>
			<br>
		</div>
	</div>
	
	<div class="row-fluid">
		<div class="span12">
		<label class="checkbox">
			<?= CHtml::checkBox('agree_delete', false, array('value' => 'agree')) ?>
			<?= Yii::t('certification', 'I understand that all issued certifications of selected types will be revoked from users and this operation <strong>cannot be undone!</strong>') ?>
		</label>
		</div>
	</div>
	

<?php endif; ?>


<div class="form-actions">
    <input class="btn-docebo green big" type="submit" name="confirm_delete" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" />
    <input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>

<?php $this->endWidget(); ?>


<script type="text/javascript">

	$('input[name="agree_delete"]').styler();

</script>