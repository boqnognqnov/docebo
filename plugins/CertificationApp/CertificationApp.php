<?php

/**
 * Plugin for CertificationApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class CertificationApp extends DoceboPlugin {
	
	// Override parent 
	public static $HAS_SETTINGS = false;

	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'CertificationApp';

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {
		
		try {
			
			$tables = Yii::app()->db->schema->tableNames;
			
			if(!in_array('certification', $tables)) {
				$sql = "
				CREATE TABLE IF NOT EXISTS `certification` (
	  				`id_cert` int(11) NOT NULL AUTO_INCREMENT,
	  				`title` varchar(255) NOT NULL,
	  				`description` text,
	  				`duration` int(11) NOT NULL DEFAULT '365',
	  				`allow_same_item` tinyint(1) NOT NULL DEFAULT '0',
	  				`duration_unit` varchar(16) NOT NULL COMMENT 'day,month,year',
	  				`deleted` tinyint(1) NOT NULL DEFAULT '0',
	  				PRIMARY KEY (`id_cert`)
				) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
				";
					
				$command = Yii::app()->db->createCommand($sql);
				$command->execute();
			}
				
			if(!in_array('certification_item', $tables)) {
				$sql = "
				CREATE TABLE IF NOT EXISTS `certification_item` (
	  				`id` int(11) NOT NULL AUTO_INCREMENT,
	  				`id_cert` int(11) NOT NULL,
	  				`id_item` int(11) NOT NULL COMMENT 'Course,Plan or Transcript ID',
	  				`item_type` varchar(32) NOT NULL DEFAULT 'course' COMMENT 'Type of the id_item field',
	  				PRIMARY KEY (`id`),
	  				UNIQUE KEY `unique_key` (`id_cert`,`id_item`,`item_type`),
	  				KEY `id_cert` (`id_cert`),
	  				CONSTRAINT `certification_item_certification1` 
						FOREIGN KEY (`id_cert`) 
						REFERENCES `certification` (`id_cert`) ON DELETE CASCADE ON UPDATE CASCADE
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;
				";
			
				$command = Yii::app()->db->createCommand($sql);
				$command->execute();
			}
	
			if(!in_array('certification_user', $tables)) {
				$sql = "
				CREATE TABLE IF NOT EXISTS `certification_user` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `id_user` int(11) NOT NULL,
				  `on_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
				  `expire_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
				  `id_cert_item` int(11) NOT NULL COMMENT 'PK/ID from the certification_item table',
				  `archived` int(1) DEFAULT '0',
				  `extra_data` text COMMENT 'JSON',
				  `in_renew` int(1) DEFAULT '0',
				  `reset_token` varchar(64) DEFAULT NULL COMMENT 'A one-time token to secure a course reset process',
				  PRIMARY KEY (`id`),
				  KEY `id_user` (`id_user`),
				  KEY `id_cert_item` (`id_cert_item`),
				  CONSTRAINT `certification_user_certification_item1` 
						FOREIGN KEY (`id_cert_item`) REFERENCES `certification_item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
				  CONSTRAINT `certification_user_core_user1` 
						FOREIGN KEY (`id_user`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;
				";
			
				$command = Yii::app()->db->createCommand($sql);
				$command->execute();
			}
			
			parent::activate();
			
				
		
		}
		catch (Exception $e) {
			Yii:log('Error while creating Certification tables', CLogger::LEVEL_ERROR);	
		}
	}
	

	public function deactivate() {
		parent::deactivate();
	}

}
