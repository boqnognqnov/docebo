<?php



class CertificationIssued extends CertificationNotificationHandler    {

	
	public function getRecipientsData($params=false) {
		
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());
				
		// "AT" ?  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			
			$certUser = isset($this->eventParams['certificationUser']) ? $this->eventParams['certificationUser'] : null;
			if (!$certUser)
				return $recipCollection ;
			
			$certificationItemModel = $certUser->certificationItem;
			$certificationModel 	= $certificationItemModel->certification;
			$userModel 				= $certUser->user;

			$targetUsers 	= $this->maskItems(self::MASK_USERS, array($userModel->idst));
			$targetCourses = false;
			
			$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $targetCourses, self::PU_OF_USERSONLY);

			// ADD RELATED INFO
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					$recipCollection['users'][$index]['user'] 				= $userModel->idst;
					$recipCollection['users'][$index]['certificationUser'] 	= $certUser->id;
				}
			}
			
		}
		
		// "AFTER" ?  
		else  if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AFTER) {
			
			// ALL users/courses are candidates (flase, false)
			$targetUsers 	= false;	// all users are legit
				
			// AFTER INFO
			$shiftNumber = $this->notification->schedule_shift_number;
			$shiftPeriod = $this->notification->schedule_shift_period;
			$shiftType	 = $this->notification->schedule_type;
				
			$usersHavingEvent = $this->getCertifiedUsersWithEventAfterBefore($targetUsers, self::EVENT_CERT_ISSUED, $shiftType, $shiftNumber, $shiftPeriod);
				
			// Specific related data
			$relatedFields = array('user', 'certificationUser');
			$recipCollection = $this->helperGetRecipientsForListOfUsers($usersHavingEvent, $this->notification->recipient, $relatedFields);
			
		}
		
		$response->recipients 	= $recipCollection['users'];  
		$response->languages 	= $recipCollection['languages'];
		
		return $response;
		
	}
	

	/**
	 * 
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		
		$descriptor = new NotificationDescriptor();
		$descriptor->type 					= __CLASS__;
		$descriptor->title 					= Yii::t('notification', 'Certification has been issued');
		$descriptor->description 			= 'A certification has been issued to a user for completing a course or learning plan';
		$descriptor->allowedProperties 		= CoreNotification::$PROP_NO_COURSE;
		$descriptor->allowedRecipientTypes 	= CoreNotification::$RECIP_NO_INSTRUCTOR;
		$descriptor->allowedScheduleTypes 	= CoreNotification::$SCHEDULE_PRESET_NO_BEFORE;
		$descriptor->shortcodes = array(
			self::SC_CERT_TITLE,
			self::SC_CERT_DESCR,
			self::SC_CERT_EXPIRE_AT,
			self::SC_CERT_ISSUED_ON,
			self::SC_CERT_ITEM_NAME,				
			self::SC_CERT_ITEM_TYPE,
			CoreNotification::SC_USERNAME,
			CoreNotification::SC_FIRSTNAME,
			CoreNotification::SC_LASTNAME,
			CoreNotification::SC_USER_EMAIL,
		);
		$descriptor->jobHandler = NotificationJobHandler::HANDLER_ID;
		
		return $descriptor;
		
	}
	
		
}
