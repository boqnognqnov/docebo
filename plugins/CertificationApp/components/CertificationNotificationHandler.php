<?php

/**
 * Base class for Certification related notification handlers
 * 
 *
 */
class CertificationNotificationHandler extends NotificationHandler implements INotificationHandler    {
	
	const SC_CERT_TITLE 		= '[cert_title]';
	const SC_CERT_DESCR 		= '[cert_description]';
	const SC_CERT_EXPIRE_AT		= '[cert_expire_at]';
	const SC_CERT_ISSUED_ON		= '[cert_issued_on]';
	const SC_CERT_ITEM_TYPE		= '[cert_item_type]';  // course, plan, transcript
	const SC_CERT_ITEM_NAME		= '[cert_item_name]';  // name/title of the item (course, plan,...)
	const SC_CERT_RENEW_LINK	= '[renew_link]';   
	
	const EVENT_CERT_EXPIRED	= 'certexpired';
	const EVENT_CERT_ISSUED		= 'certissued';
	
	/**
	 * MUST BE overriden by descendant classes
	 * @see INotificationHandler::getRecipientsData()
	 */
	public function getRecipientsData($params=false) {
	}

	/**
	 * MUST BE overriden by descendant classes
	 * @param string $params
	 */
	public static function descriptor($params=false) {
	}
	
	
	/**
	 * Gte list of users (and their data, not all but some), which has experienced some event in a given period or at all
	 *
	 * @param array $usersList Filter by this list of users
	 * @param string $certEvent Check for this event (e.g. certification expired, certification issued, ...)
	 * @param string $shiftType  at/after/before
	 * @param integer$shiftNumber	number of hours/days/...
	 * @param string $shiftPeriod hour/day/..
	 *
	 * @return array of DATA staructure
	 */
	protected function getCertifiedUsersWithEventAfterBefore($usersList=false, $certEvent=false, $shiftType=false, $shiftNumber=false, $shiftPeriod=false) {
	
		// ----------
		$params = array();
		$select = array(
				'user.idst							AS idst',
				'user.userid 						AS username',
				'user.email							AS email',
				'user.register_date					AS register_date',
				'cu.id								AS certificationUser',
		);
	
		// ----------
	
		$command = Yii::app()->db->createCommand()->from('core_user user');
	
		$command->join('certification_user cu', 'user.idst=cu.id_user');
	
		// Filter by list of users
		if ($usersList !== false && is_array($usersList)) {
			$command->andWhere('user.idst IN (' . implode(',',$usersList) . ')');
		}
	
		// Valid users only
		$command->andWhere('user.valid=1');
	
		// ----------
	
		// Ignore anonymous user
		$command->andWhere("user.userid <> '/Anonymous'");
	
		// -----------
	
		// Event specific FILTERING/QUERIES
		switch ($certEvent) {
			case self::EVENT_CERT_EXPIRED:
				break;
	
			case self::EVENT_CERT_ISSUED:
				break;
		}
	
	
		// --------------
	
		// If "after/before event" filtering is requested,
		// Get target period of time to observe and apply the filtering
		// Allow open periods:  [FROM,infinity] and [infinity, TO]
		if ($shiftNumber && $shiftPeriod && $shiftType) {
				
			list($targetTimeFrom, $targetTimeTo) = $this->getTargetPeriodForShifted($shiftType, $shiftNumber, $shiftPeriod);
			Yii::log('Looking for time period: ' . $targetTimeFrom . '--' . $targetTimeTo, CLogger::LEVEL_INFO);
	
			if ($targetTimeFrom) {
				switch ($certEvent) {
					case self::EVENT_CERT_EXPIRED:
						$command->andWhere('cu.expire_at >= :timeFrom');
						$params[':timeFrom'] = $targetTimeFrom;
						break;
	
					case self::EVENT_CERT_ISSUED:
						$command->andWhere('cu.on_datetime >= :timeFrom');
						$params[':timeFrom'] = $targetTimeFrom;
						break;
				}
			}
			if ($targetTimeTo) {
				switch ($certEvent) {
					case self::EVENT_CERT_EXPIRED:
						$command->andWhere('cu.expire_at <= :timeTo');
						$params[':timeTo'] = $targetTimeTo;
						break;
	
					case self::EVENT_CERT_ISSUED:
						$command->andWhere('cu.on_datetime <= :timeTo');
						$params[':timeTo'] = $targetTimeTo;
						break;
				}
			}
		}
	
		// -----------
	
		$command->select(implode(',', $select));
	
		// ----------
	
		$reader = $command->query($params);
	
		// ----------
	
		$list = array();
		$languages = array();
		foreach ($reader as $row) {
			$language = isset($row['language']) ? $row['language'] : $this->defaultLanguage;
			$user = array(
					'idst' 					=> $row['idst'],
					'username'				=> trim($row['username'],'/'),
					'email'					=> $row['email'],
					'language'				=> $language,
					'timezone'				=> $row['timezone'],
					'date_format_locale'	=> $row['date_format_locale'],
					// --- YES!! (below)
					'user'					=> $row['idst'],
					'certificationUser'		=> $row['certificationUser'], // ID of the issued certification
			);
			$languages[$language] = 1;
			$list[] = $user;
		}
	
	
		// ----------
	
	
		return array(
				'users' 			=> $list,
				'languages'			=> array_keys($languages),
		);
	
	
	}
	

	
	/**
	 * @see NotificationHandler::getShortcodesData($recipient, $metadata = array(), $textFormat = false)
	 */
	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
	
		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);
	
		$certUser = CertificationUser::model()->findByPk($recipient['certificationUser']);
		$certItem = $certUser->certificationItem;
		$cert = $certItem->certification;
	
		$result[self::SC_CERT_TITLE] 		= $cert->title;
		$result[self::SC_CERT_DESCR] 		= $cert->description;
		$result[self::SC_CERT_EXPIRE_AT] 	= $certUser->expire_at;
		$result[self::SC_CERT_ISSUED_ON] 	= $certUser->on_datetime;
			
		if ($certItem->item_type == CertificationItem::TYPE_COURSE) {
			$item = LearningCourse::model()->findByPk($certItem->id_item);
			$itemName = $item->name;
			$itemType = Yii::t('standard', '_COURSE');
		}
		else if ($certItem->item_type == CertificationItem::TYPE_LEARNING_PLAN) {
			$item = LearningCoursepath::model()->findByPk($certItem->id_item);
			$itemName = $item->path_name;
			$itemType = Yii::t('standard', '_COURSEPATH');
		}
		else if ($certItem->item_type == CertificationItem::TYPE_TRANSCRIPT) {
			$item = TranscriptsRecord::model()->findByPk($certItem->id_item);
			$itemName = $item->course_name;
			$itemType = Yii::t('transcripts', 'External activities');
		}
	
			
		$result[self::SC_CERT_ITEM_NAME] = $itemName;
		$result[self::SC_CERT_ITEM_TYPE] = $itemType;
		$result[self::SC_CERT_RENEW_LINK] = Docebo::createAbsoluteLmsUrl('CertificationApp/certificationApp/renew', array('cu_id' => $certUser->id));
		
	
		return $result;
	
	}
	
	
}