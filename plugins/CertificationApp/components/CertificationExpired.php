<?php



class CertificationExpired extends CertificationNotificationHandler    {
	

	public function getRecipientsData($params=false) {
		
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());
		
		// Restrict running for allowed only (in this case it is after and before the event)
		if (in_array($this->notification->schedule_type, CoreNotification::$SCHEDULE_PRESET_NO_AT)) {
			
			// Filter users by selection (assigned users)
			$targetUsers 	= $this->maskItems(self::MASK_USERS, false);
			
			// AFTER INFO
			$shiftNumber = $this->notification->schedule_shift_number;
			$shiftPeriod = $this->notification->schedule_shift_period;
			$shiftType	 = $this->notification->schedule_type;
			
			// Get list of users (and their personal data) having the "certification expired" event in the given period 
			$usersHavingEvent = $this->getCertifiedUsersWithEventAfterBefore($targetUsers, self::EVENT_CERT_EXPIRED, $shiftType, $shiftNumber, $shiftPeriod);
			
			// Specific related data
			$relatedFields = array('user', 'certificationUser');
			$recipCollection = $this->helperGetRecipientsForListOfUsers($usersHavingEvent, $this->notification->recipient, $relatedFields);
				
			
		}
		
		$response->recipients 	= $recipCollection['users'];  
		$response->languages 	= $recipCollection['languages'];
		
		return $response;
		
	}
	
	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
	
		$descriptor = new NotificationDescriptor();
		$descriptor->type 					= __CLASS__;
		$descriptor->title 					= Yii::t('notification', 'Certification has expired');
		$descriptor->description 			= 'A certification is expired or is going to expire';
		$descriptor->allowedProperties 		= CoreNotification::$PROP_NO_COURSE;
		$descriptor->allowedRecipientTypes 	= CoreNotification::$RECIP_NO_INSTRUCTOR;
		$descriptor->allowedScheduleTypes 	= CoreNotification::$SCHEDULE_PRESET_NO_AT;
		$descriptor->shortcodes = array(
				self::SC_CERT_TITLE,
				self::SC_CERT_DESCR,
				self::SC_CERT_EXPIRE_AT,
				self::SC_CERT_ISSUED_ON,
				self::SC_CERT_ITEM_NAME,
				self::SC_CERT_ITEM_TYPE,
				self::SC_CERT_RENEW_LINK,
				CoreNotification::SC_USERNAME,
				CoreNotification::SC_FIRSTNAME,
				CoreNotification::SC_LASTNAME,
				CoreNotification::SC_USER_EMAIL,
		);
		$descriptor->jobHandler = NotificationJobHandler::HANDLER_ID;
	
		return $descriptor;
	
	}
	
	
	
	
}
