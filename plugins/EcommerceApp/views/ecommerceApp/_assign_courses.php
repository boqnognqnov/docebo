<?php
/* @var $this EcommerceAppController */
/* @var $model EcommerceCoupon */
?>

<h4>
    <a class="docebo-back" href="<?= $this->createUrl('coupons') ?>">
        <span><?= Yii::t('standard', '_BACK') ?></span>
    </a>
    <?= Yii::t('coupon', 'Assign courses to a coupon') ?></h4>

<ul id="docebo-bootcamp-coupons" class="docebo-bootcamp clearfix">
    <li>
        <a id=""
           href="<?= $this->createUrl('axAssignSelectCourses', array('id_coupon' => $model->id_coupon)) ?>"
           class="popup-handler bootcamp-coupon-assigncourses"
           data-modal-class="modal-assign-coupon-courses"
           data-modal-title="<?= Yii::t('standard', '_ASSIGN_COURSES') ?>"
           data-helper-text="<?= Yii::t('helper', 'Coupon assign courses - coupon') ?>">
            <span class=""></span>
            <span class="white"></span>
            <h4><?= Yii::t('standard', '_ASSIGN_COURSES') ?></h4>
        </a>
    </li>

    <li class="helper">
        <a href="#">
            <span class="i-sprite is-circle-quest large"></span>
            <h4 class="helper-title"></h4>
            <p class="helper-text"></p>
        </a>
    </li>

</ul>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'assign-coupon-courses-form',
    'htmlOptions' => array(
        'class' => 'ajax-list-form',
        'data-grid' => '#coupon-assigned-courses-list'
    ),
)); ?>

<h2><?= Yii::t('coupon', 'Filters & Search') ?></h2>
<div class="main-section group-management">
    <div class="filters-wrapper">
        <div class="filters clearfix">
            <div class="input-wrapper">
                <?php echo CHtml::textField('LearningCourse[name]', '', array(
                    'id' => 'advanced-search-group',
                    'class' => 'typeahead',
                    'autocomplete' => 'off',
                    'data-url' => Docebo::createAppUrl('admin:courseManagement/courseAutocomplete'),
                    'data-coupon' => $model->id_coupon,
                    'placeholder' => Yii::t('standard', '_SEARCH'),
                )); ?>
                <span class="search-icon"></span>
            </div>
        </div>
    </div>
</div>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<div class="selections clearfix">
    <?php $this->widget('ext.local.widgets.ListViewSelectAll', array(
        'gridId' => 'coupon-assigned-courses-list',
        'class' => 'left-selections clearfix',
        'dataProvider' => $model->assignedCoursesDataProvider(),
        'itemValue' => 'idCourse',
    )); ?>

    <div class="right-selections clearfix">
        <select name="massive_action" id="coupon-assigned-courses-massive-action">
            <option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
            <option value="delete"><?php echo Yii::t('standard', '_DEL'); ?></option>
        </select>
        <label for="coupon-assigned-courses-massive-action"><?php echo Yii::t('standard', '_ON_SELECTED'); ?></label>
    </div>
</div>

<div class="bottom-section clearfix">
    <h3 class="title-bold title-with-border">
        <?= Yii::t('coupon', 'Courses assigned to: {coupon}', array('{coupon}'=>$model->code)) ?>
    </h3>
    <div id="grid-wrapper">
        <?php $this->widget('adminExt.local.widgets.CXListView', array(
            'id' => 'coupon-assigned-courses-list',
            'htmlOptions' => array(
                'class' => 'list-view',
                'data-coupon-id' => $model->id_coupon
            ),
            'dataProvider' => $model->assignedCoursesDataProvider(),
            'itemView' => '_item_assigned_course',
            'itemsCssClass' => 'items clearfix',
            'summaryText' => Yii::t('standard', '_TOTAL'),
            'pager' => array(
                'class' => 'adminExt.local.pagers.DoceboLinkPager',
            ),
            'template' => '{items}{pager}{summary}',
            'ajaxUpdate' => 'coupon-assigned-courses-list-all-items',
            'beforeAjaxUpdate' => 'function(id, options) {
				options.type = "POST";
				if (options.data == undefined) {
					options.data = {
						selectedItems: $("#"+id+"-selected-items-list").val()
					}
				}
			}',
            'afterAjaxUpdate' => 'function(id, data) {
                $("#coupon-assigned-courses-list input").styler();
                $(document).controls();
                $.fn.updateListViewSelectPage();
            }',
        )); ?>
    </div>
</div>

<script type="text/javascript">
    (function ($) {
        $(function () {
            $('input, select').styler();
            replacePlaceholder();
        });
    })(jQuery);
</script>