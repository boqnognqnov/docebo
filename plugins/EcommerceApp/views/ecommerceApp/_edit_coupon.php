<?php
/* @var $this EcommerceAppController */
/* @var $model EcommerceCouponForm */
/* @var $form CActiveForm */

$currencySymbol = Settings::get('currency_symbol', null, false, false);
?>

<h1><?= $model->coupon->isNewRecord ? Yii::t('coupon', 'New coupon') : Yii::t('coupon', 'Edit coupon') ?></h1>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'edit-coupon-form',
    'method' => 'POST',
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
        'class' => 'ajax docebo-form form-horizontal'
    ),
));
?>

    <div class="control-group">
        <?= $form->labelEx($model->coupon, 'code', array('class'=>'control-label')) ?>
        <div class="controls">
            <div class="row-fluid">
                <?= $form->error($model->coupon, 'code') ?>
                <?= $form->textField($model->coupon, 'code', array('class'=>'span8')) ?>
                <button id="generateCouponCode" class="btn btn-docebo green big pull-right span4"><?= Yii::t('certificate', '_GENERATE') ?></button>
            </div>
            <p class="help-block" style="margin: 5px 0 0;">
                <?= Yii::t('coupon', 'You can define your own coupon code or you can generate a random code') ?>
            </p>
        </div>
    </div>

    <div class="control-group">
        <?= $form->labelEx($model->coupon, 'description', array('class'=>'control-label')) ?>
        <div class="controls">
            <?= $form->error($model->coupon, 'description') ?>
            <?= $form->textField($model->coupon, 'description', array('class'=>'input-block-level')) ?>
        </div>
    </div>

    <hr/>

    <div class="control-group">
        <?= CHtml::label(Yii::t('coupon', 'Coupon validity'), '', array('class'=>'control-label')) ?>
        <div class="controls">

            <?= $form->error($model->coupon, 'valid_from') ?>
            <?= $form->error($model->coupon, 'valid_to') ?>

            <div class="validity-period">
                <?= $form->radioButton($model, 'validityType', array(
                        'value'=>EcommerceCoupon::VALIDITY_SPECIFIED,
                        'uncheckValue' => null
                    )) ?>

                <div class="valid-from">
                    <?= $form->labelEx($model->coupon, 'valid_from', array()) ?>
                    <?= $form->textField($model->coupon, 'valid_from', array(
						'class'=>'datepicker edit-day-input',
						'value'=>Yii::app()->localtime->toLocalDate(Yii::app()->localtime->fromLocalDateTime($model->coupon->valid_from))
					)) ?>
                    <i class="p-sprite calendar-black-small date-icon"></i>
                </div>

                <div class="valid-to">
                    <?= $form->labelEx($model->coupon, 'valid_to', array()) ?>
                    <?= $form->textField($model->coupon, 'valid_to', array(
						'class'=>'datepicker edit-day-input',
						'value'=>Yii::app()->localtime->toLocalDate(Yii::app()->localtime->fromLocalDateTime($model->coupon->valid_to))
					)) ?>
                    <i class="p-sprite calendar-black-small date-icon"></i>
                </div>
            </div>
            <?= CHtml::label(
                $form->radioButton($model, 'validityType', array(
                    'uncheckValue' => null,
                    'value' => EcommerceCoupon::VALIDITY_ALWAYS,
                    'checked'=>(!$model->validityType || $model->validityType===EcommerceCoupon::VALIDITY_ALWAYS),
                    'id'=>'validity_always'
                )) . ' '
                . Yii::t('coupon', 'Always valid'),
                'validity_always',
                array('class'=>'radio', 'style'=>'padding-left:25px;')
            ) ?>
        </div>
    </div>

    <hr/>

    <div class="control-group">
        <?= CHtml::label(Yii::t('coupon', 'Coupon usage'), '', array('class'=>'control-label')) ?>
        <div class="controls">
            <?= CHtml::label($form->checkBox($model, 'useOnAllCoursesChecked', array(
                    'uncheckValue' => null
                )) .' ' . Yii::t('coupon', 'This coupon can be used on all courses'),
                CHtml::activeId($model, 'useOnAllCoursesChecked'),
                array('class'=>'checkbox')
            ) ?>
            <p class="help-block">
                <?= Yii::t('coupon', 'Each user can use this coupon once to discount the entire cart') ?>
            </p>
            <br/>

            <?= CHtml::label($form->radioButton($model, 'usageType', array(
                    'uncheckValue'=>null,
					'id' => EcommerceCoupon::USAGE_UNLIMITED_USERS,
                    'value' => EcommerceCoupon::USAGE_UNLIMITED_USERS
                )) . ' ' . Yii::t('coupon', 'Single-use coupon'),
				EcommerceCoupon::USAGE_UNLIMITED_USERS,
                array('class'=>'radio')
            ) ?>
            <p class="help-block">
                <?= Yii::t('coupon', 'A single user can use this coupon once to discount the entire cart') ?>
            </p>

            <?= CHtml::label($form->radioButton($model, 'usageType', array(
                    'uncheckValue'=>null,
					'id' => EcommerceCoupon::USAGE_LIMITED_USERS,
                    'value' => EcommerceCoupon::USAGE_LIMITED_USERS
                )) . ' ' . Yii::t('coupon', 'Limit the usage of this coupon to {count} users', array(
                    '{count}' => $form->textField($model->coupon, 'usage_count', array('class'=>'usage-count'))
                )), EcommerceCoupon::USAGE_LIMITED_USERS,
                array('class'=>'radio')
            ) ?>
            <p class="help-block">
                <?= Yii::t('coupon', 'The coupon can be used once by the number of users set') ?>
            </p>
        </div>
    </div>

    <hr/>

    <div class="control-group">
        <?= CHtml::label(Yii::t('coupon', 'Coupon discount'), '', array('class'=>'control-label')) ?>
        <div class="controls">
            <?= CHtml::label($form->radioButton($model->coupon, 'discount_type', array(
                    'value' => EcommerceCoupon::DISCOUNT_TYPE_PERCENT,
                    'uncheckValue' => null,
                )).' ' . $form->textField($model->coupon, 'discount', array(
                    'value' => ( ($model->coupon->discount_type == EcommerceCoupon::DISCOUNT_TYPE_PERCENT && floatval($model->coupon->discount) >= 0)
                            ? $model->coupon->discount : ''
                    ),
                    'disabled' => ($model->coupon->discount_type != EcommerceCoupon::DISCOUNT_TYPE_PERCENT),
                    'class' => '',
                    'id' => CHtml::activeId($model->coupon, 'discount') . '_percent'
                ))
                . ' %',
                CHtml::activeId($model->coupon, 'discount') . '_percent',
                array('class'=>'radio discount-percent'))
            ?>
            <?= CHtml::label(
                $form->radioButton($model->coupon, 'discount_type', array(
                    'value' => EcommerceCoupon::DISCOUNT_TYPE_AMOUNT,
                    'uncheckValue' => null,
                )).' '
                . $form->textField($model->coupon, 'discount', array(
                    'value' => (
                        ($model->coupon->discount_type == EcommerceCoupon::DISCOUNT_TYPE_AMOUNT && floatval($model->coupon->discount) >= 0)
                            ? $model->coupon->discount
                            : ''
                        ),
                    'disabled' => ($model->coupon->discount_type != EcommerceCoupon::DISCOUNT_TYPE_AMOUNT),
                    'class' => '',
                    'id' => CHtml::activeId($model->coupon, 'discount') . '_amount'
                ))
                . ' ' . $currencySymbol,
                CHtml::activeId($model->coupon, 'discount') . '_amount',
                array('class'=>'radio discount-amount'))
            ?>
        </div>
    </div>

    <div class="control-group" style="margin-bottom:0px;">
        <?= CHtml::label(Yii::t('coupon', 'Minimum order'), '', array('class'=>'control-label')) ?>
        <div class="controls">
            <?= CHtml::label($form->checkBox($model, 'minimumOrderChecked', array(
                    'uncheckValue'=>null,
                )) . ' ' . $form->textField($model->coupon, 'minimum_order', array(
                    'class' => 'span1',
                    'disabled' => !$model->minimumOrderChecked,
                    'value' => (floatval($model->coupon->minimum_order) > 0) ? $model->coupon->minimum_order : ''
                )) . ' ' . $currencySymbol,
                CHtml::activeId($model->coupon, 'minimum_order'),
                array('class'=>'checkbox min-order')
            ) ?>
            <p class="help-block">
                <?= Yii::t('coupon', 'To use the discount coupon, the order total must reach the minimum amount set') ?>
            </p>
        </div>
    </div>

    <div class="form-actions">
        <input class="btn-docebo green big" type="submit" value="<?= Yii::t('standard', '_CONFIRM') ?>" />
        <input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
    </div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    (function() {
        $('input, select').styler();

        $(document).delegate(".modal-edit-coupon", "dialog2.content-update", function() {
            var e = $(this);

            // Autoclosing modal code
            var autoclose = e.find("a.auto-close");
            if (autoclose.length > 0) {
                e.find('.modal-body').dialog2("close");

                // reload grid
                $("#coupon-management-grid").yiiGridView('update');

                var href = autoclose.attr('href');
                if (href)
                    window.location.href = href;

                return;
            }

            // Install validity type radio button check
            function updateValidityType() {
                var selectedValidity = $('input:radio[name="EcommerceCouponForm[validityType]"]:checked').val();
                if(selectedValidity == '<?= EcommerceCoupon::VALIDITY_SPECIFIED ?>') {
                    $('input[name="EcommerceCoupon[valid_from]"]').attr('disabled', false);
                    $('input[name="EcommerceCoupon[valid_to]"]').attr('disabled', false);
                } else {
                    $('input[name="EcommerceCoupon[valid_from]"]').attr('disabled', true);
                    $('input[name="EcommerceCoupon[valid_to]"]').attr('disabled', true);
                }

            }

            $('input:radio[name="EcommerceCouponForm[validityType]"]').change(updateValidityType);
            updateValidityType();

            // Install generate coupon function
            $('#generateCouponCode').click(function(e) {
                e.preventDefault();
                var code = (Math.random()+1).toString(36).substring(2,10).toUpperCase();
                $(this).parent().find('input:text').val(code);
                return false;
            });

            // Install date picker
            var fromPicker = $('input[name="EcommerceCoupon[valid_from]"]').bdatepicker({format: '<?= Yii::app()->localtime->dateStringToBDatepickerFormat() ?>'}).data('datepicker');
            var toPicker = $('input[name="EcommerceCoupon[valid_to]"]').bdatepicker({format: '<?= Yii::app()->localtime->dateStringToBDatepickerFormat() ?>'}).data('datepicker');
            $('input[name="EcommerceCoupon[valid_from]"]').bdatepicker().on('show', function(ev){
                toPicker.hide();
            });

            $('input[name="EcommerceCoupon[valid_to]"]').bdatepicker().on('show', function(ev){
                fromPicker.hide();
            });

            // Install click on calendar icon
            $(".date-icon").on('click', function (ev) {
                if(!$(this).prev('input').attr('disabled'))
                    $(this).prev('input').bdatepicker().data('datepicker').show();
            });

            // Install other checks
            $('.discount-percent input:radio').change(function() {
                if ($(this).is(':checked')) {
                    $(this).parent().find('input:text').attr('disabled', false).val('');
                    $('.discount-amount').find('input:text').attr('disabled', true).val('');
                }
            });
            $('.discount-amount input:radio').change(function() {
                if ($(this).is(':checked')) {
                    $(this).parent().find('input:text').attr('disabled', false).val('');
                    $('.discount-percent').find('input:text').attr('disabled', true).val('');
                }
            });
            $('.min-order input:checkbox').change(function() {
                if ($(this).is(':checked')) {
                    $(this).parent().find('input:text').attr('disabled', false).focus();
                } else {
                    $(this).parent().find('input:text').attr('disabled', true);
                }
            });
            $('.min-order input[type="text"]').click(function(e) {
				e.preventDefault();
				return false;
            });
        });
    })();
</script>