<?php
/* @var $form CActiveForm */
/* @var $settings EcommerceAppSettingsForm */
?>

<h4><?= Yii::t('standard', 'Settings') ?></h4>

<div class="form-wrapper">
    <?php $form = $this->beginWidget('CActiveForm',
        array(
            'id' => 'app-settings-form',
            'htmlOptions'=>array('class' => 'form-horizontal docebo-form')
        )
    ); ?>

    <div class="control-container odd">
        <div class="control-group">
            <?=$form->labelEx($settings, 'authorize_enabled', array('class'=>'control-label'));?>
            <div class="controls">
                <?=$form->checkBox($settings, 'authorize_enabled');?>
                <span style="margin-left: 7px"><?=Yii::t('ecommerce', 'Enable Authorize.net payment gateway')?></span>
                <?=$form->error($settings, 'authorize_enabled'); ?>
            </div>
        </div>
    </div>

    <div class="control-container">
        <div class="control-group">
            <?=$form->labelEx($settings, 'authorize_loginid', array('class'=>'control-label'));?>
            <div class="controls input-wrapper">
                <?=$form->passwordField($settings, 'authorize_loginid', array('style' => ''));?>
                <?=$form->error($settings, 'authorize_loginid'); ?>
            </div>
        </div>
    </div>

    <div class="control-container odd">
        <div class="control-group">
            <?=$form->labelEx($settings, 'authorize_key', array('class'=>'control-label'));?>
            <div class="controls input-wrapper">
                <?=$form->passwordField($settings, 'authorize_key', array('style' => ''));?>
                <?=$form->error($settings, 'authorize_key'); ?>
            </div>
        </div>
    </div>

    <div class="control-container">
        <div class="control-group">
            <?=$form->labelEx($settings, 'authorize_hash', array('class'=>'control-label'));?>
            <div class="controls input-wrapper">
                <?=$form->passwordField($settings, 'authorize_hash', array('style' => ''));?>
                <?=$form->error($settings, 'authorize_hash'); ?>
            </div>
        </div>
    </div>

    <div class="control-container odd">
        <div class="control-group">
            <?=$form->labelEx($settings, 'authorize_sandbox', array('class'=>'control-label'));?>
            <div class="controls">
                <span style="float: left"><?=$form->checkBox($settings, 'authorize_sandbox');?></span>
                <span class="controls-infobox">
                    <span><?=Yii::t('EcommerceApp', 'Use sandbox instead of the real environment')?></span>
                    <span class="muted">(<?=Yii::t('EcommerceApp', 'this is a debug and testing option')?>)</span>
                </span>
                <?=$form->error($settings, 'authorize_sandbox'); ?>
            </div>
        </div>
    </div>

    <br/>

    <div class="control-container text-right">
        <?=CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class'=>'btn-docebo green big')); ?>
        &nbsp;
        <a href="<?=Docebo::createLmsUrl("app/index");?>" class="btn-docebo black big"><?=Yii::t('standard', '_CANCEL');?></a>
    </div>

    <?php $this->endWidget(); ?>
</div>

<script type="text/javascript">
    //$('input,select').styler();
    $('#EcommerceAppSettingsForm_authorize_enabled').change(function() {
        var attrReadonly = ( ! $(this).is(':checked') );
        $('#EcommerceAppSettingsForm_authorize_loginid, #EcommerceAppSettingsForm_authorize_key, #EcommerceAppSettingsForm_authorize_hash, #EcommerceAppSettingsForm_authorize_sandbox').attr('readonly', attrReadonly);
    }).trigger('change');
</script>