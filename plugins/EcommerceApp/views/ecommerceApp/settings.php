<?php
/* @var $form CActiveForm */
/* @var $settings EcommerceAppSettingsForm */

/*$this->breadcrumbs = array(
    Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
    Yii::t('menu', '_CONFIGURATION'),
);*/
?>

<a href="<?= ('it' == Yii::app()->getLanguage()) ?	'https://www.docebo.com/it/knowledge-base/elearning-come-attivare-e-gestire-lapp-e-commerce/'
		: 'https://www.docebo.com/knowledge-base/docebo-elearning-how-to-sell-online-courses/' ?>" target="_blank" class="app-link-read-manual"><?= Yii::t('apps', 'Read Manual'); ?></a>
<h2>&nbsp;</h2>
<br>

<?php DoceboUI::printFlashMessages(); ?>


<div class="ecommerce-app-settings">
    <?php $this->widget('common.widgets.Sidebarred', array(
        'sidebarTitle' => Yii::t('configuration', '_ECOMMERCE'),
        'sidebarItems' => array(
            array(
                'title' => Yii::t('standard', 'Settings'),
                'icon' => 'icon-settings',
                'data-tab' => 'settings',
                'data-url' => 'EcommerceApp/EcommerceApp/axSettings',
            ),
            array(
                'title'=>Yii::t('coupon', 'Coupons'),
                'icon' => false,
                'data-tab' => 'coupons',
                'data-url' => 'EcommerceApp/EcommerceApp/axCoupons',
                'data-callback' => 'doceboCGridViewInitCouponManagementGrid'
            )
        ),
    )); ?>
</div>


<script type="text/javascript">

    $(function(){

        $('.sidebarred .ajaxloader').show();

        $(window).hashchange(function(){
            var hash = location.hash,
                hasStr = "",
                link = null;

            hashStr = hash.substr(1);

            if(hashStr.length==0)
                hashStr = 'settings';

            if($('.sidebarred .sidebar a').length==0)
                return;

            link = $('.sidebarred .sidebar a.' + hashStr);

            $('.sidebarred .sidebar a').removeClass('active');
            link.addClass('active');

            if (link.data('loaded') == 0) {
                loadTabContent(link, link.data('callback'));
            }

            $('.tab-content .single-tab-content').hide();
            $('.tab-content').find(hash).show();
            $('#selectedTab').val(hashStr);

            $(window).trigger('resize');
        });

        $(window).hashchange();

    });

    function loadTabContent(link, callback) {
        $('.sidebarred .tab-content .ajaxloader').css('display', 'block').show();

        $.ajax({
            url: yii.urls.base + '/?r=' + link.data('url'),
            cache: false,
            timeout: 4000,
            error: function(){
                console.log('Error with getting tab contents');
                console.log(arguments);
                return true;
            },
            success: function (result) {
                $('.sidebarred .tab-content .ajaxloader').hide();

                // Prevent double loading on page load or if quickly switching between tabs
                $('.tab-content .single-tab-content').hide();

                link.data('loaded', 1);

                var res;
                try{
                    res = $.parseJSON(result);
                }catch(e){
                    console.log(e);
                    return;
                }

                var html = res.html;

                // If the tab exists previously (although it shouldn't) - remove it
                $('.tab-content #'+link.data('tab')).remove();

                var div = $('<div/>', { 'id': link.data('tab'), 'class': 'single-tab-content' }).html(html);
                $('.tab-content').append(div);

                // if (link.data('tab') != 'logo')
                $('.tab-content input, .tab-content select').styler();

                $('a[rel=tooltip]').tooltip();

                // Suck Dialog2 controls
                $(document).controls();

                // Some elements inside the loaded tab content will need to know this,
                // since triggering this event from inside the tab content doesn't work
                $(window).trigger('resize');

                var fn = window[callback];
                if (typeof fn === 'function')
                    fn();

                $('#docebo-bootcamp-coupons').doceboBootcamp();
            }
        });
    }

</script>