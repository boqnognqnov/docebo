<?php
/* @var $form CActiveForm */
/* @var $settings EcommerceAppSettingsForm */
?>

<h4><?= Yii::t('standard', 'Settings') ?></h4>

<div class="form-wrapper">
	<?php
	$form = $this->beginWidget('CActiveForm',
		array(
			'id' => 'app-settings-form',
			'htmlOptions' => array('class' => 'form-horizontal docebo-form')
		)
	);
	$_oddCounter = 1;
	?>

    <div class="control-container<?= ($_oddCounter++ % 2) > 0 ? ' odd' : '' ?>">
        <div class="control-group">
            <?=$form->labelEx($settings, 'stripe_enabled', array('class'=>'control-label'));?>
            <div class="controls">
                <?=$form->checkBox($settings, 'stripe_enabled');?>
                <span style="margin-left: 7px"><?=Yii::t('ecommerce', 'Enable Stripe payment gateway')?></span>
                <?=$form->error($settings, 'stripe_enabled'); ?>
            </div>
        </div>
    </div>
    <div class="control-container<?= ($_oddCounter++ % 2) > 0 ? ' odd' : '' ?>">
        <div class="control-group">
            <?=$form->labelEx($settings, 'stripe_type', array('class'=>'control-label'));?>
            <div class="controls">
                <?=$form->radioButtonList($settings, 'stripe_type', array(
                    'simple' => Yii::t('configuration', 'Simple charge'),
                    'advanced' => Yii::t('configuration', 'Stripe order')
                ), array(
                    'template' => '<div class="stripe_type_row">{input}<span class="controls-infobox">{label}</span></div>',
                    'separator' => '',
                    'class' => 'stripe_payment_type'
                ))?>
                <span class="stripe_advanced_text"><?=Yii::t('configuration', 'Allows you to automatically collect taxes. For more information, {LS}click here{LE}.', array(
                        '{LS}' => '<a href="https://stripe.com/docs/orders/tax-integration" target="_blank">',
                        '{LE}' => '</a>'
                    ))?></span>
                <div id="stripe_tax_code_container" style="display: <?=($settings->stripe_type == "advanced")?"block":"none"?>;">
                    <?=$form->labelEx($settings, 'stripe_tax_code');?>
                    <div class="input-wrapper">
                        <?=$form->textField($settings, 'stripe_tax_code');?>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<div class="control-container<?= ($_oddCounter++ % 2) > 0 ? ' odd' : '' ?>">
		<div class="control-group">
			<?=$form->labelEx($settings, 'stripe_alipay_enabled', array('class'=>'control-label'));?>
			<div class="controls">
				<?=$form->checkBox($settings, 'stripe_alipay_enabled');?>
				<span class="controls-infobox">
					<span><?=Yii::t('configuration', 'Enable Alipay stripe integration')?></span>
				</span>
				<?=$form->error($settings, 'stripe_alipay_enabled'); ?>
			</div>
		</div>
	</div>

    <div class="control-container<?= ($_oddCounter++ % 2) > 0 ? ' odd' : '' ?>">
        <div class="control-group">
            <?=$form->labelEx($settings, 'stripe_account_email', array('class'=>'control-label'));?>
            <div class="controls input-wrapper">
                <?=$form->textField($settings, 'stripe_account_email', array('style' => ''));?>
                <?=$form->error($settings, 'stripe_account_email'); ?>
            </div>
        </div>
    </div>

    <div class="control-container<?= ($_oddCounter++ % 2) > 0 ? ' odd' : '' ?>">
        <div class="control-group">
            <?=$form->labelEx($settings, 'stripe_private_key', array('class'=>'control-label'));?>
            <div class="controls input-wrapper">
                <?=$form->passwordField($settings, 'stripe_private_key', array('style' => ''));?>
                <?=$form->error($settings, 'stripe_private_key'); ?>
            </div>
        </div>
    </div>

    <div class="control-container<?= ($_oddCounter++ % 2) > 0 ? ' odd' : '' ?>">
        <div class="control-group">
            <?=$form->labelEx($settings, 'stripe_public_key', array('class'=>'control-label'));?>
            <div class="controls input-wrapper">
                <?=$form->textField($settings, 'stripe_public_key', array('style' => ''));?>
                <?=$form->error($settings, 'stripe_public_key'); ?>
            </div>
        </div>
    </div>

    <div class="control-container<?= ($_oddCounter++ % 2) > 0 ? ' odd' : '' ?>">
        <div class="control-group">
            <?=$form->labelEx($settings, 'stripe_sandbox', array('class'=>'control-label'));?>
            <div class="controls">
                <?=$form->checkBox($settings, 'stripe_sandbox');?>
                <span class="controls-infobox">
                    <span><?=Yii::t('EcommerceApp', 'Use stripe sandbox instead of the real environment')?></span>
                    <span class="muted">(<?=Yii::t('EcommerceApp', 'this is a debug and testing option')?>)</span>
                </span>
                <?=$form->error($settings, 'stripe_sandbox'); ?>
            </div>
            <div class="secondary-controls">
                <div class="secondary-item">
                    <?=$form->labelEx($settings, 'stripe_private_key_test', array('class'=>'control-label'));?>
                    <?=$form->passwordField($settings, 'stripe_private_key_test', array('style' => ''));?>
                    <?=$form->error($settings, 'stripe_private_key_test'); ?>
                </div>
                <div class="secondary-item">
                    <?=$form->labelEx($settings, 'stripe_public_key_test', array('class'=>'control-label'));?>
                    <?=$form->textField($settings, 'stripe_public_key_test', array('style' => ''));?>
                    <?=$form->error($settings, 'stripe_public_key_test'); ?>
                </div>
            </div>
        </div>
    </div>

    <br/>

    <div class="control-container text-right">
        <?=CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class'=>'btn-docebo green big')); ?>
        &nbsp;
        <a href="<?=Docebo::createLmsUrl("app/index");?>" class="btn-docebo black big"><?=Yii::t('standard', '_CANCEL');?></a>
    </div>

    <?php $this->endWidget(); ?>
</div>

<script type="text/javascript">
    $(document).on('ready', function(){
        var sandboxActive = $('#EcommerceAppSettingsForm_stripe_sandbox-styler').hasClass('checked');
        if(sandboxActive){
            $('.secondary-controls').css('display', 'block');
        } else {
            $('.secondary-controls').css('display', 'none');
        }
    });
    $('#EcommerceAppSettingsForm_stripe_sandbox').on('change', function(){
        var sandboxActive = $('#EcommerceAppSettingsForm_stripe_sandbox-styler').hasClass('checked');
        if(sandboxActive){
            $('.secondary-controls').css('display', 'block');
        } else {
            $('.secondary-controls').css('display', 'none');
        }
    });
    $('.stripe_payment_type').on('change', function(){
        var paymentType = $('.stripe_payment_type:checked').val();
        if(paymentType == "advanced"){
            $('#stripe_tax_code_container').css('display', 'block');
        } else {
            $('#stripe_tax_code_container').css('display', 'none');
        }
    });

    function toggleEnableCheckStripe(){
        var enableCheck = $('#EcommerceAppSettingsForm_stripe_enabled');
        if (enableCheck.is(':checked')) {
            $('#EcommerceAppSettingsForm_stripe_account_email').prop('readonly', false);
            $('#EcommerceAppSettingsForm_stripe_private_key').prop('readonly', false);
            $('#EcommerceAppSettingsForm_stripe_public_key').prop('readonly', false);
        } else {
            $('#EcommerceAppSettingsForm_stripe_account_email').prop('readonly', true);
            $('#EcommerceAppSettingsForm_stripe_private_key').prop('readonly', true);
            $('#EcommerceAppSettingsForm_stripe_public_key').prop('readonly', true);
        }
    }
    $(function(){
        toggleEnableCheckStripe();
        $(document).on('change', '#EcommerceAppSettingsForm_stripe_enabled', toggleEnableCheckStripe);
    })
</script>