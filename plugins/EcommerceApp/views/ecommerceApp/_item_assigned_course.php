<?php
/* @var $data LearningCourse */
?>

<div class="item  <?php echo (($index % 2) ? 'odd' : 'even'); ?>">
    <div class="checkbox-column">
        <?php echo '<input id="assign-coupon-courses-checkboxes_'. $data->idCourse .'" type="checkbox" name="assign-coupon-courses-checkboxes[]" value="'.$data->idCourse.'"'.(isset(Yii::app()->session['selectedItems']) && in_array($data->idCourse, Yii::app()->session['selectedItems']) ? ' checked="checked"' : '').'>'; ?>
    </div>
    <div class="list-view-item-content"><?= $data->name ?></div>
    <div class="delete-button">
        <?= CHtml::tag(
            'a',
            array(
                'href' => Yii::app()->controller->createUrl('axDeleteAssignedCourse', array(
                    'id_coupon' => Yii::app()->request->getParam('id_coupon'),
                    'ids[]' => $data->idCourse
                )),
                'class' => 'open-dialog',
                'data-dialog-class' => 'modal-delete-coupon-assigned-courses',
            ),
            Yii::t('standard', '_UNASSIGN')
        ) ?>
    </div>
</div>