<?php
/* @var $model EcommerceCoupon */

/* @var $form CActiveForm */
?>

<h4><?= Yii::t('coupon', 'Coupons') ?></h4>

<ul id="docebo-bootcamp-coupons" class="docebo-bootcamp clearfix">
    <li>
        <a id="add-new-coupon"
           href="<?= $this->createUrl('axEditCoupon') ?>"
           class="open-dialog"
           data-dialog-class="modal-edit-coupon"
           data-helper-title="<?= Yii::t('coupon', 'New coupon') ?>"
           data-helper-text="<?= Yii::t('helper', 'New coupon - coupon') ?>">
            <span class="coupons-sprite icon-bigass-dollartag"></span>
            <span class="coupons-sprite icon-bigass-dollartag white"></span>
            <h4><?= Yii::t('coupon', 'New coupon') ?></h4>
        </a>
    </li>

    <li class="helper">
        <a href="#">
            <span class="i-sprite is-circle-quest large"></span>
            <h4 class="helper-title"></h4>
            <p class="helper-text"></p>
        </a>
    </li>

</ul>



<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'coupons-search-form',
    'htmlOptions' => array(
        'class' => 'ajax-grid-form',
        'data-grid' => '#coupon-management-grid'
    ),
)); ?>

<h2><?= Yii::t('coupon', 'Filters & Search') ?></h2>
<div class="main-section search-form">
    <div class="filters-wrapper">
        <div class="filters clearfix">
            <div class="row-fluid">
                <div class="input-wrapper pull-right">
                    <?php echo $form->textField($model, 'search_input', array(
                        'id' => 'advanced-search-coupons',
                        'class' => 'typeahead',
                        'autocomplete' => 'off',
                        'data-type' => 'core',
                        'data-autocomplete' => 'true',
                        'data-url' => $this->createUrl('coupons'),
                        'placeholder' => Yii::t('standard', '_SEARCH'),
                    )); ?>
                    <span>
					    <button class="search-btn-icon"></button>
					</span>
                </div>

                <label class="validity-filter-label"><?= Yii::t('coupon', 'Filter by validity') ?>:</label>

                <div class="input-wrapper date dates" id="search-coupons-date-from">
                    <?= Yii::t('standard', '_FROM') ?>
                    <?= CHtml::activeTextField($model, 'search_date_from', array('style' => 'margin-left: 5px;')) ?>
                    <i class="add-on i-sprite is-calendar-date"></i>
                </div>
                <div class="input-wrapper date dates" id="search-coupons-date-to">
                    &nbsp;&nbsp;&nbsp;<?= Yii::t('standard', '_TO') ?>&nbsp;&nbsp;
                    <?= CHtml::activeTextField($model, 'search_date_to', array()) ?>
                    <i class="add-on i-sprite is-calendar-date"></i>
                </div>

                <?= CHtml::label(
                        $form->checkBox($model,'hide_expired').' '.Yii::t('coupon', 'Hide expired'),
                        '',
                        array('class' => 'checkbox hide-expired-label')
                ) ?>

            </div>
        </div>
    </div>
</div>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>


<!--locations grid-->
<div id="coupons-grid-wrapper" class="">
    <?php

    $_columns = array(
        array(
            'type' => 'raw',
            'value' => '$data["code"]',
            'name' => 'code',
            'header' => Yii::t('standard', '_CODE'),
            'sortable' => true,
        ),
        array(
            'type' => 'raw',
            'name' => 'description',
            'value' => '$data["description"]',
            'header' => Yii::t('standard', '_DESCRIPTION')
        ),
        array(
            'type' => 'raw',
            'name' => 'discount',
            'value' => '$data->renderDiscount()',
            'header' => Yii::t('coupon', 'Discount')
        ),
        array(
            'type' => 'raw',
            'name' => 'validity',
            'value' => '$data->renderValidity()',
            'header' => Yii::t('coupon', 'Validity')
        ),
        array(
            'type' => 'raw',
            'name' => 'assigned_to',
            'value' => '$data->renderAssignedCourses()',
            'header' => Yii::t('coupon', 'Assigned to'),
        ),
        array(
            'type' => 'raw',
            'value' => array($this, 'gridRenderLinkCouponDetails'),
            'htmlOptions' => array(
                'class' => 'td-button-container',
            )
        ),
        array(
            'type' => 'raw',
            'value' => array($this, 'gridRenderLinkEditCoupon'),
            'htmlOptions' => array(
                'class' => 'td-button-container',
            )
        ),
        array(
            'type' => 'raw',
            'value' => array($this, 'gridRenderLinkDeleteCoupon'),
            'htmlOptions' => array(
                'class' => 'td-button-container',
            )
        )
    );


    $this->widget('DoceboCGridView', array(
        'id' => 'coupon-management-grid',
        'htmlOptions' => array('class' => 'grid-view clearfix'),
        'dataProvider' => $model->dataProvider(),
        'columns' => $_columns,
        'template' => '{items}{summary}{pager}',
        'summaryText' => Yii::t('standard', '_TOTAL'),
        'pager' => array(
            'class' => 'admin.protected.extensions.local.pagers.DoceboLinkPager',
        ),
        'ajaxUrl' => $this->createUrl('coupons', array('update'=>1)),
        'beforeAjaxUpdate' => 'function(id, options) {
            // Reset placeholder text if default one
            //resetSearchPlaceholder();
            options.type = "POST";
            //options.data = $("#location-grid-search-form").serialize()
        }',
        'afterAjaxUpdate' => 'function(id, data){
            $(\'a[rel="tooltip"]\').tooltip();
            $(document).controls();
        }'
    ));
    ?>
</div>

<script type="text/javascript">
    <!--
    $(function(){
        // Attach Date Pickers
        $('#search-coupons-date-from').bdatepicker({format: '<?= Yii::app()->localtime->dateStringToBDatepickerFormat() ?>'});
        $('#search-coupons-date-to').bdatepicker({format: '<?= Yii::app()->localtime->dateStringToBDatepickerFormat() ?>'});

        $('input, select').styler();

		$('#<?= CHtml::activeId($model, 'hide_expired') ?>').change(function() {
			$(this).closest('form').submit();
		});
    });
    //-->
</script>