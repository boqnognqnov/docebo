<?php
/* @var $this EcommerceAppController */
/* @var $models EcommerceCouponCourses[] */

/* @var $form CActiveForm */
?>
<h1><?= Yii::t('coupon', 'Delete assigned courses') ?></h1>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'delete-coupon-courses-form',
    'method' => 'POST',
    'htmlOptions' => array(
        'class' => 'ajax'
    ),
));
    echo CHtml::hiddenField('id_coupon', $models[0]->id_coupon);
?>

    <p><?php echo Yii::t('standard', '_UNASSIGN').': "'.count($models).'" '.Yii::t('standard', '_COURSES'); ?></p>

    <br/>

    <div class="clearfix">
        <?php echo CHtml::label(
            CHtml::checkBox('confirm_unassign', false, array('id' => 'confirm_delete_assigned_courses')) . ' ' . Yii::t('standard', 'Yes, I confirm I want to proceed'),
            'confirm_delete_assigned_courses',
            array('class' => 'checkbox')
        ); ?>
    </div>

    <?php foreach ($models as $model) {
        echo CHtml::hiddenField('ids[]', $model->id_course, array('id' => false));
    } ?>

    <div class="form-actions">
        <input class="submit btn-docebo green big disabled" type="submit" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" />
        <input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
    </div>

<?php $this->endWidget(); ?>


<script type="text/javascript">

    $(document).delegate(".modal-delete-coupon-assigned-courses", "dialog2.content-update", function() {
        var e = $(this);

        var autoclose = e.find("a.auto-close");

        if (autoclose.length > 0) {
            e.find('.modal-body').dialog2("close");

            // reload grid
            $.fn.yiiListView.update('coupon-assigned-courses-list');

            var href = autoclose.attr('href');
            if (href) {
                window.location.href = href;
            }
            return;
        }

        e.find(':checkbox').change(function() {
            if ($(this).is(':checked')) {
                e.find('.modal-footer .submit').removeClass('disabled');
            } else {
                e.find('.modal-footer .submit').addClass('disabled');
            }
        });

        e.find('input').styler({});
    });

</script>