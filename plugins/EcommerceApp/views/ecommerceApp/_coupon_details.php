<?php
/* @var $this EcommerceAppController */
/* @var $model EcommerceCoupon */

/* @var $form CActiveForm */

$timeConversion = new LocalTime();
$fromDateFormat = $timeConversion->getPHPLocalDateFormat();
?>

<h1><?= Yii::t('coupon', 'Coupon details') ?></h1>


<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'coupon-transactions-grid-search-form',
    'htmlOptions' => array('class' => 'ajax-grid-form', 'data-grid' => '#coupon-transactions-grid'),
)); ?>

<div class="main-section">

    <div class="filters-wrapper">
        <div class="selections" data-grid="">
            <div class="input-wrapper">
                <?php echo $form->textField(CoreUser::model(), 'search_input', array(
                    'id' => 'advanced-search-coupon-transaction',
                    'class' => 'typeahead',
                    'autocomplete' => 'off',
                    'data-type' => 'core',
                    'data-source-desc' => 'true',
                    'data-autocomplete' => 'true',
                    'data-url' => Docebo::createAppUrl('admin:userManagement/index'),
                    'placeholder' => Yii::t('standard', '_SEARCH'),
                )); ?>
                <span class="search-icon"></span>
            </div>
            <div class="dd-filter-wrapper">
                <label for=""><?= Yii::t('standard', 'Filter') ?></label>
                <?= CHtml::dropDownList(
                    'coupon_filter_dd',
                    $model->id_coupon,
                    CHtml::listData(EcommerceCoupon::model()->findAll(),'id_coupon',function($coupon) {
                        return CHtml::encode($coupon->code);
                    }),
                    array(
                        'prompt' => Yii::t('coupon', 'View all coupons')
                    )
                ) ?>
            </div>
        </div>
    </div>
</div>

<input type="hidden" name="contentType" value="html"/>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>


<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'coupon-details-form',
    'htmlOptions' => array(
        'data-grid-items' => 'true',
        'class' => 'ajax'
    ),
)); ?>

    <div class="coupon-stats-container single-coupon-view clearfix">
        <div class="coupon-stats">
            <?php if (intval($model->usage_count) > 0) : ?>
            <div class="usage">
                <i class="coupons-sprite icon-dollartag-multiple"></i>
                <?= Yii::t('coupon', '{count_usage} time(s)', array('{count_usage}'=>CHtml::tag('strong',array(),$model->renderUsage()))) ?>
            </div>
            <?php endif; ?>

            <?php if ($model->can_have_courses) : ?>
            <div class="assigned-courses">
                <i class="coupons-sprite icon-assigned-courses"></i>
                <?= Yii::t('coupon', '{count} courses', array('{count}'=>CHtml::tag('strong',array(),count($model->assignedCourses)))) ?>
            </div>
            <?php endif; ?>

            <?php if ($model->valid_from && $model->valid_to) : ?>
            <div class="validity">
                <i class="p-sprite calendar-black-small date-icon"></i>
                <?= CHtml::tag('strong',array(),Yii::t('standard', '_FROM')) . ' ' ?><?= Yii::app()->localtime->toLocalDate(Yii::app()->localtime->fromLocalDateTime($model->valid_from)) ?>
                <br/>
                <?= CHtml::tag('strong',array(),Yii::t('standard', '_TO')) . ' '?><?= Yii::app()->localtime->toLocalDate(Yii::app()->localtime->fromLocalDateTime($model->valid_to))?>
            </div>
            <?php endif; ?>

            <div class="discount">
                <i class="coupons-sprite icon-scissors"></i>
                <strong><?= $model->renderDiscount() ?></strong>
            </div>
        </div>

        <div class="coupon-description">
            <span class="coupons-sprite icon-bigass-dollartag"></span>
            <h2><?= $model->code ?></h2>
            <p><?= $model->description ?></p>
        </div>
    </div>

    <div id="grid-wrapper" class="">
			<?php

			$_columns = array();

			$_columns[] = array(
				'name' => Yii::t('coupon', 'Coupon code'),
				'value' => '$data->ecommerceCoupon->code',
				'htmlOptions' => array(
					'class' => 'all-coupons-view'
				),
				'headerHtmlOptions' => array(
					'class' => 'all-coupons-view'
				),
			);

			$_columns[] = array(
				'name' => Yii::t('standard', '_USERNAME'),
				'value' => '$data->user->clearUserId'
			);

			$_columns[] = array(
				'name' => Yii::t('standard', '_FIRSTNAME'),
				'value' => '$data->user->firstname'
			);

			$_columns[] = array(
				'name' => Yii::t('standard', '_LASTNAME'),
				'value' => '$data->user->lastname'
			);

			$_columns[] = array(
				'type' => 'raw',
				'name' => 'totalPrice',
				'value' => '$data->renderTotalAmount()',
				'header' => Yii::t('coupon', 'Order amount')
			);

			$_columns[] = array(
				'type' => 'raw',
				'name' => 'totalPrice',
				'value' => '$data->renderTotalPaid()',
				'header' => Yii::t('coupon', 'Total paid')
			);

			$_columns[] = array(
				'name' => 'date_creation',
				'value' => '$data->renderDateCreation()',
				'header' => Yii::t('coupon', 'Used on')
			);

			$_columns[] = array(
				'type' => 'raw',
				'name' => 'details',
				'value' => array($this, 'gridRenderSingleCouponDetailsLink'),
				'header' => Yii::t('standard', '_DETAILS'),
			);

			$this->widget('zii.widgets.grid.CGridView', array(
				'id' => 'coupon-transactions-grid',
				'htmlOptions' => array('class' => 'grid-view clearfix'),
				'dataProvider' => EcommerceTransaction::model()->couponsDataProvider($model->id_coupon),
				'cssFile' => false,
				'columns' => $_columns,
				'template' => '{items}{summary}{pager}',
				'summaryText' => Yii::t('standard', '_TOTAL'),
				'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
				'pager' => array(
					'class' => 'adminExt.local.pagers.DoceboLinkPager',
					'cssFile' => false,
				),
				'beforeAjaxUpdate' => 'function(id, options) {
                options.type = "POST";
                if (options.data == undefined) {
                    options.data = {
                        contentType: "html"
                    };
                }
                options.data.id_coupon = $("#coupon_filter_dd").val();
            }',
				'ajaxUrl' => $this->createUrl('axCouponDetails'),
				'ajaxUpdate' => 'coupon-transactions-grid-all-items',
				'afterAjaxUpdate' => 'function(id, data) {
                var $data = $("<div>" + data + "</div>");
                $(".coupon-stats-container").replaceWith($(".coupon-stats-container", $data));
                $("#coupon-transactions-grid input").styler();
                afterGridViewUpdate(id);
                $("#coupon_filter_dd").trigger("toggle-ui");
            }',
			));

			?>
    </div>

    <div class="form-actions">
        <input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CLOSE'); ?>"/>
    </div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    (function() {
        $(document).delegate(".modal-coupon-details", "dialog2.content-update", function() {

            applyTypeahead($('.typeahead'));

            var e = $(this);
            var autoclose = e.find("a.auto-close");

            if (autoclose.length > 0) {
                e.find('.modal-body').dialog2("close");

                var href = autoclose.attr('href');
                if (href) {
                    window.location.href = href;
                }

                return;
            }

            $('#coupon_filter_dd')
                .change(function() {
                    $.fn.yiiGridView.update('coupon-transactions-grid');
                })
                .on('toggle-ui', function() {
                    var couponId = $(this).val();
                    if (couponId) {
                        // show single coupon view
                        e.find('.all-coupons-view').hide();
                        e.find(".single-coupon-view").show();
                    }
                    else {
                        // show all coupons view
                        e.find('.all-coupons-view').show();
                        e.find(".single-coupon-view").hide();
                    }
                })
                .trigger('toggle-ui');
        });
    })();
</script>