<?php
/* @var $this EcommerceAppController */
/* @var $models EcommerceCouponCourses[] */

/* @var $form CActiveForm */
?>

<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'delete-coupon-courses-form',
    'method' => 'POST',
    'htmlOptions' => array(
        'class' => 'ajax'
    ),
));
    echo CHtml::hiddenField('id_coupon', $models[0]->id_coupon);
?>

    <p><?php echo Yii::t('standard', '_UNASSIGN').': "'.count($models).'" '.Yii::t('standard', '_COURSES'); ?></p>

    <div class="clearfix">
        <?php echo CHtml::label(
            CHtml::checkBox('confirm_unassign', false, array('id' => 'confirm_delete_assigned_courses_'.$checkboxIdKey, 'onchange' => $onChange)) . ' ' . Yii::t('standard', 'Yes, I confirm I want to proceed'),
            'confirm_delete_assigned_courses',
            array('class' => 'checkbox', 'style'=>'margin:0;')
        ); ?>
    </div>

    <?php foreach ($models as $model) {
        echo CHtml::hiddenField('ids[]', $model->id_course, array('id' => false));
    } ?>

<?php $this->endWidget(); ?>