<?php
/* @var $form CActiveForm */
/* @var $settings EcommerceAppSettingsForm */
?>

<h4><?= Yii::t('standard', 'Settings') ?></h4>

<div class="form-wrapper">
    <?php $form = $this->beginWidget('CActiveForm',
        array(
            'id' => 'app-settings-form',
            'htmlOptions'=>array('class' => 'form-horizontal docebo-form')
        )
    ); ?>

    <div class="control-container odd">
        <div class="control-group">
            <?=$form->labelEx($settings, 'currency_symbol', array('class'=>'control-label'));?>
            <div class="controls input-wrapper">
                <?=$form->textField($settings, 'currency_symbol', array('style' => ''));?>
                <?=$form->error($settings, 'currency_symbol'); ?>
            </div>
        </div>
    </div>


	<div class="control-container">
		<div class="control-group">
			<?=$form->labelEx($settings, 'currency_code', array('class'=>'control-label'));?>
			<div class="controls">
				<?=$form->dropDownList($settings, 'currency_code', array_change_key_case(LocaleManager::getCurrencyArr(), CASE_UPPER)); ?>
				<?=$form->error($settings, 'currency_code'); ?>
				<span class="settings">
					<p class="description" style="margin-left: 0px;margin-top: 5px;"><?=Yii::t('configuration', 'Authorize.net account currency setting must match the currency selected from this dropdown.');?></p>
				</span>
			</div>
		</div>
	</div>

    <br/>

    <div class="control-container text-right">
        <?=CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class'=>'btn-docebo green big')); ?>
        &nbsp;
        <a href="<?=Docebo::createLmsUrl("app/index");?>" class="btn-docebo black big"><?=Yii::t('standard', '_CANCEL');?></a>
    </div>

    <?php $this->endWidget(); ?>
</div>

<script type="text/javascript">
    //$('input,select').styler();
</script>