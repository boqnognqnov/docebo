<?php
/* @var $form CActiveForm */
/* @var $settings EcommerceAppSettingsForm */
?>

<h4><?= Yii::t('standard', 'Settings') ?></h4>

<div class="form-wrapper">
    <?php $form = $this->beginWidget('CActiveForm',
        array(
            'id' => 'app-settings-form',
            'htmlOptions'=>array('class' => 'form-horizontal docebo-form')
        )
    ); ?>

    <div class="control-container odd">
        <div class="control-group">
            <?=$form->labelEx($settings, 'adyen_enabled', array('class'=>'control-label'));?>
            <div class="controls">
                <?=$form->checkBox($settings, 'adyen_enabled');?>
                <span style="margin-left: 7px"><?=Yii::t('ecommerce', 'Enable Adyen payment gateway')?></span>
                <?=$form->error($settings, 'adyen_enabled'); ?>
            </div>
        </div>
    </div>

    <div class="control-container">
        <div class="control-group">
            <?=$form->labelEx($settings, 'adyen_merchant_account', array('class'=>'control-label'));?>
            <div class="controls input-wrapper">
                <?=$form->passwordField($settings, 'adyen_merchant_account', array('style' => ''));?>
                <?=$form->error($settings, 'adyen_merchant_account'); ?>
            </div>
        </div>
    </div>

    <div class="control-container odd">
        <div class="control-group">
            <?=$form->labelEx($settings, 'adyen_hmac_key', array('class'=>'control-label'));?>
            <div class="controls input-wrapper">
                <?=$form->passwordField($settings, 'adyen_hmac_key', array('style' => ''));?>
                <?=$form->error($settings, 'adyen_hmac_key'); ?>
            </div>
        </div>
    </div>

    <div class="control-container">
        <div class="control-group">
            <?=$form->labelEx($settings, 'adyen_skin_code', array('class'=>'control-label'));?>
            <div class="controls input-wrapper">
                <?=$form->passwordField($settings, 'adyen_skin_code', array('style' => ''));?>
                <?=$form->error($settings, 'adyen_skin_code'); ?>
            </div>
        </div>
    </div>

	<div class="control-container odd">
		<div class="control-group">
			<?=$form->labelEx($settings, 'adyen_single_page_hpp', array('class'=>'control-label'));?>
			<div class="controls">
				<?=$form->checkBox($settings, 'adyen_single_page_hpp');?>
				<?=$form->error($settings, 'adyen_single_page_hpp'); ?>
			</div>
		</div>
	</div>

    <div class="control-container">
        <div class="control-group">
            <?=$form->labelEx($settings, 'adyen_sandbox', array('class'=>'control-label'));?>
            <div class="controls">
                <span style="float: left"><?=$form->checkBox($settings, 'adyen_sandbox');?></span>
                <span class="controls-infobox">
                    <span><?=Yii::t('EcommerceApp', 'Use sandbox instead of the real environment')?></span>
                    <span class="muted">(<?=Yii::t('EcommerceApp', 'this is a debug and testing option')?>)</span>
                </span>
                <?=$form->error($settings, 'adyen_sandbox'); ?>
            </div>
        </div>
    </div>

    <br/>

    <div class="control-container text-right">
        <?=CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class'=>'btn-docebo green big')); ?>
        &nbsp;
        <a href="<?=Docebo::createLmsUrl("app/index");?>" class="btn-docebo black big"><?=Yii::t('standard', '_CANCEL');?></a>
    </div>

    <?php $this->endWidget(); ?>
</div>

<script type="text/javascript">
    //$('input,select').styler();
    $('#EcommerceAppSettingsForm_adyen_enabled').change(function() {
        var attrReadonly = ( ! $(this).is(':checked') );
        $('#EcommerceAppSettingsForm_adyen_merchant_account, #EcommerceAppSettingsForm_adyen_hmac_key, #EcommerceAppSettingsForm_adyen_skin_code, #EcommerceAppSettingsForm_adyen_sandbox').attr('readonly', attrReadonly);
        if(attrReadonly)
			$('#EcommerceAppSettingsForm_adyen_single_page_hpp-styler').addClass('disabled');
		else
			$('#EcommerceAppSettingsForm_adyen_single_page_hpp-styler').removeClass('disabled');
    }).trigger('change');

	//TODO: investigate, why on page load, if the plugin is disabled the checkbox is not set attributes "disabled"

</script>