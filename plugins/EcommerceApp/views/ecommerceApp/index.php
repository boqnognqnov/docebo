<?php
/* @var $this EcommerceAppController */
/* @var $breadcrumbs string|array */
/* @var $activeTab string */
/* @var $tabContent string */

/* @var $form CActiveForm */

// Bootcamp menu
Yii::app()->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl . '/js/docebo_bootcamp.js');
Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl . '/css/docebo_bootcamp.css');

$sidebarItems = array(
    'settings' => array(
        'title' => Yii::t('standard', 'Settings'),
        'icon' => 'icon-settings',
        'url' => $this->createUrl('settings'),
        'data-tab' => 'settings',
    ),
    'coupons' => array(
        'title'=>Yii::t('coupon', 'Coupons'),
        'icon' => false,
        'url' => $this->createUrl('coupons'),
        'data-tab' => 'coupons',
        'data-callback' => 'doceboCGridViewInitCouponManagementGrid',
    ),
    'paypal' => array(
        'title'=> 'Paypal',
        'icon' => false,
        'url' => $this->createUrl('paypal'),
        'data-tab' => 'paypal',
        'data-callback' => '',
    ),
    'authorizedotnet' => array(
        'title'=> 'Authorize.net',
        'icon' => false,
        'url' => $this->createUrl('authorizedotnet'),
        'data-tab' => 'authorizedotnet',
        'data-callback' => '',
    ),
    'adyen' => array(
        'title'=> 'Adyen',
        'icon' => false,
        'url' => $this->createUrl('adyen'),
        'data-tab' => 'adyen',
        'data-callback' => '',
    ),
	'stripe' => array(
        'title'=> 'Stripe',
        'icon' => false,
        'url' => $this->createUrl('stripe'),
        'data-tab' => 'stripe',
        'data-callback' => '',
    ),
    'cybersource' => array(
        'title'=> 'Cybersource',
        'icon' => false,
        'url' => $this->createUrl('cybersource'),
        'data-tab' => 'cybersource',
        'data-callback' => '',
    ),
);

$_breadcrumbs = array(
    Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
    Yii::t('configuration', '_ECOMMERCE')
);

if (is_array($breadcrumbs)) {
    $_breadcrumbs = array_merge($_breadcrumbs, $breadcrumbs);
} else {
    $_breadcrumbs[] = $breadcrumbs;
}

$this->breadcrumbs = $_breadcrumbs;
?>

<a href="<?= ('it' == Yii::app()->getLanguage()) ?	'http://www.docebo.com/it/knowledge-base/elearning-come-attivare-e-gestire-lapp-e-commerce/'
		: 'http://www.docebo.com/knowledge-base/docebo-elearning-how-to-sell-online-courses/' ?>" target="_blank" class="app-link-read-manual"><?= Yii::t('apps', 'Read Manual'); ?></a>
<h2>&nbsp;</h2>
<br>

<?php DoceboUI::printFlashMessages(); ?>


<div class="ecommerce-app-settings">

    <div class="sidebarred">
        <div class="sidebar">
            <h3><?= Yii::t('configuration', '_ECOMMERCE') ?></h3>

            <?php if($sidebarItems): ?>
                <ul>
                    <?php foreach($sidebarItems as $sidebarItem): ?>
                        <li>
                            <?= CHtml::link(
                                ( $sidebarItem['title'] ? '<span></span>' . $sidebarItem['title'] : null ),
                                $sidebarItem['url'],
                                array_merge(
                                    $sidebarItem,
                                    array(
                                        'data-loaded' => 0,
                                        'class'=>$sidebarItem['data-tab'] . ($sidebarItem['data-tab']==$activeTab ? ' active' : '')
                                    )
                                )
                            ) ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>
        <div class="main">
            <div class="tab-content">
                <?= $tabContent ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('#docebo-bootcamp-coupons').doceboBootcamp();
        $('input').styler({});
    });
</script>