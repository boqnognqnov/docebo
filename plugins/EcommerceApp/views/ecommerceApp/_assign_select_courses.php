<?php
/* @var $this EcommerceAppController */
/* @var $model LearningCourse */

/* @var $form CActiveForm */
?>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'coupon-courses-assign-form',
    'htmlOptions' => array('class' => 'ajax-grid-form', 'data-grid' => '#coupon-courses-assign-grid'),
)); ?>
<div class="main-section">

    <div class="filters-wrapper">
        <div class="selections" data-grid="coupon-courses-assign-grid">
            <div class="input-wrapper">
                <?php echo $form->textField($model, 'name', array(
                    'id' => 'advanced-search-course',
                    'class' => 'typeahead',
                    'autocomplete' => 'off',
                    'data-url' => Docebo::createAppUrl('admin:courseManagement/courseAutocomplete'),
                    'placeholder' => Yii::t('standard', '_SEARCH'),
                )); ?>
                <span class="search-icon"></span>
            </div>
            <?php $this->widget('adminExt.local.widgets.GridViewSelectAll', array(
                'gridId' => 'coupon-courses-assign-grid',
                'class' => 'left-selections',
                'dataProvider' => $model->dataProviderCatalogue($excludedCourses),
                'itemValue' => 'idCourse',
            )); ?>
        </div>
    </div>
</div>

<input type="hidden" name="contentType" value="html"/>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>



<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'select-course-form',
    'htmlOptions' => array(
        'data-grid-items' => 'true'
    ),
)); ?>

    <div id="grid-wrapper" class="courseEnroll-course-table">
        <h4><?php echo Yii::t('report', '_REPORT_COURSE_SELECTION'); ?></h4>
			<?php

			$_columns = array();

			$_columns[] = array(
				'class' => 'CCheckBoxColumn',
				'selectableRows' => 2,
				'id' => 'coupon-courses-assign-grid-checkboxes',
				'value' => '$data->idCourse',
                'checked' => 'in_array($data->idCourse, Yii::app()->session[\'selectedItems\'])',
			);

			$_columns[] = 'code';

			$_columns[] = 'name';

			if (PluginManager::isPluginActive('ClassroomApp')) {
				$_columns[] = array(
					'id' => 'course_type',
					'value' => '$data->getCourseTypeTranslation()',
					'header' => Yii::t('course', '_COURSE_TYPE')
				);
			}

			$this->widget('zii.widgets.grid.CGridView', array(
				'id' => 'coupon-courses-assign-grid',
				'htmlOptions' => array('class' => 'grid-view clearfix'),
				'dataProvider' => $model->dataProviderCatalogue($excludedCourses),
				'cssFile' => false,
				'columns' => $_columns,
				'template' => '{items}{summary}{pager}',
				'summaryText' => Yii::t('standard', '_TOTAL'),
				'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
				'pager' => array(
					'class' => 'adminExt.local.pagers.DoceboLinkPager',
					'cssFile' => false,
				),
				'beforeAjaxUpdate' => 'function(id, options) {
                    options.type = "POST";
                    if (options.data == undefined) {
                        options.data = {
                            contentType: "html",
                            selectedItems: $(\'#\'+id+\'-selected-items-list\').val()
                        }
                    }
                }',
				'ajaxUpdate' => 'coupon-courses-assign-grid-all-items',
				'afterAjaxUpdate' => 'function(id, data){$("#coupon-courses-assign-grid input").styler(); afterGridViewUpdate(id);}',
			));

			?>
    </div>

<?php $this->endWidget(); ?>


<div class="modal-footer">
    <input class="btn confirm-btn" type="button" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" name="submit" data-submit-form="select-course-form"/>
    <input class="btn close-btn" type="button" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>


<script type="text/javascript">
    (function() {
        $("#coupon-courses-assign-form").submit(function () {
            $("#coupon-courses-assign-grid-all-items-list").val("");
        });

        $('.modal-assign-coupon-courses').on('hidden.bs.modal', function () {
			$.ajax({
				url: '<?= Docebo::createAppUrl('admin:EcommerceApp/ecommerceApp/clearCoursesSelection'); ?>',
				cache: false,
				timeout: 4000,
				error: function(){
					$.fn.yiiListView.update('coupon-assigned-courses-list');
				},
				success: function (result) {
					$.fn.yiiListView.update('coupon-assigned-courses-list');
				}
			});
        })
    })();
</script>