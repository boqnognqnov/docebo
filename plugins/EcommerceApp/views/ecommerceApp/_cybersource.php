<?php
/**
 *
 * @var EcommerceAppController $this
 */
?>

<h4><?= Yii::t('standard', 'Settings') ?></h4>
<div class="form-wrapper">
	<?php $form = $this->beginWidget('CActiveForm',
		array(
			'id' => 'app-settings-form',
			'htmlOptions'=>array('class' => 'form-horizontal docebo-form')
		)
	); ?>

	<div class="control-container odd">
		<div class="control-group">
			<?=$form->labelEx($settings, 'cybersource_enabled', array('class'=>'control-label'));?>
			<div class="controls">
				<?=$form->checkBox($settings, 'cybersource_enabled');?>
				<span style="margin-left: 7px"><?=Yii::t('ecommerce', 'Enable cybersource payment gateway')?></span>
				<?=$form->error($settings, 'cybersource_enabled'); ?>
			</div>
		</div>
	</div>

	<div class="control-container">
		<div class="control-group">
			<?=$form->labelEx($settings, 'cybersource_access_key', array('class'=>'control-label'));?>
			<div class="controls input-wrapper">
				<?=$form->passwordField($settings, 'cybersource_access_key', array('readonly' => true));?>
				<?=$form->error($settings, 'cybersource_access_key'); ?>
			</div>
		</div>
	</div>

	<div class="control-container odd">
		<div class="control-group">
			<?=$form->labelEx($settings, 'cybersource_secret_key', array('class'=>'control-label'));?>
			<div class="controls input-wrapper">
				<?=$form->passwordField($settings, 'cybersource_secret_key', array('readonly' => true));?>
				<?=$form->error($settings, 'cybersource_secret_key'); ?>
			</div>
		</div>
	</div>

	<div class="control-container">
		<div class="control-group">
			<?=$form->labelEx($settings, 'cybersource_profile_id', array('class'=>'control-label'));?>
			<div class="controls input-wrapper">
				<?= $form->textField($settings, 'cybersource_profile_id', array('readonly' => true));?>
				<?=$form->error($settings, 'cybersource_profile_id'); ?>
			</div>
		</div>
	</div>

	<div class="control-container odd">
		<div class="control-group">
			<?=$form->labelEx($settings, 'cybersource_sandbox_mode', array('class'=>'control-label'));?>
			<div class="controls">
				<span style="float: left"><?=$form->checkBox($settings, 'cybersource_sandbox_mode');?></span>
				<span class="controls-infobox">
                    <span><?=Yii::t('EcommerceApp', 'Use sandbox instead of the real environment')?></span>
                    <span class="muted">(<?=Yii::t('EcommerceApp', 'this is a debug and testing option')?>)</span>
                </span>
				<?=$form->error($settings, 'cybersource_sandbox_mode'); ?>
			</div>
		</div>
	</div>

	<br/>

	<div class="control-container text-right">
		<?=CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class'=>'btn-docebo green big')); ?>
		&nbsp;
		<a href=<?=Docebo::createLmsUrl("app/index");?>" class="btn-docebo black big"><?=Yii::t('standard', '_CANCEL');?></a>
	</div>

	<?php $this->endWidget(); ?>
</div>
<script>
	function toggleEnableCheck(){
		var enableCheck = $('#EcommerceAppSettingsForm_cybersource_enabled');
		if (enableCheck.is(':checked')) {
			$('#EcommerceAppSettingsForm_cybersource_access_key').prop('readonly', false);
			$('#EcommerceAppSettingsForm_cybersource_secret_key').prop('readonly', false);
			$('#EcommerceAppSettingsForm_cybersource_profile_id').prop('readonly', false);
		} else {
			$('#EcommerceAppSettingsForm_cybersource_access_key').prop('readonly', true);
			$('#EcommerceAppSettingsForm_cybersource_secret_key').prop('readonly', true);
			$('#EcommerceAppSettingsForm_cybersource_profile_id').prop('readonly', true);
		}
	}
	$(function(){
		toggleEnableCheck();
		$(document).on('change', '#EcommerceAppSettingsForm_cybersource_enabled', toggleEnableCheck);
	})
</script>