<?php

/**
 * Plugin for EcommerceApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class EcommerceApp extends DoceboPlugin {


	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'EcommerceApp';


	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {
		parent::activate();
		Settings::save('module_ecommerce', 'on', 'enum', 3); // enable the feature
		// moved into catalog // Settings::save('catalog_external', 'on', 'enum', 3);
		Settings::save('catalog_enabled', '1', 'int', 1); // enable the catalog
		$_SESSION['onShopifyDeactivation_dontMarkCoursesUnsold'] = true;
		PluginManager::deactivateAppByCodename('Shopify');
	}


	public function deactivate() {
		parent::deactivate();
		Settings::save('module_ecommerce', 'off', 'enum', 3); // disable the feature
		// moved into catalog // Settings::save('catalog_external', 'off', 'enum', 3);
		Settings::save('catalog_enabled', '0', 'int', 1); // disable the homepage catalog

		if ($_SESSION['onECommerceDeactivation_dontMarkCoursesUnsold'] !== true) {
			// Turn all paid courses and learning plans into free and admin subscribe only
			$paidCourses = LearningCourse::model()->findAllByAttributes(array('selling' => 1));
			foreach ($paidCourses as $paidCourse) {
				$paidCourse->selling = 0;
				$paidCourse->subscribe_method = 0;
				$paidCourse->save();
			}
			$paidLearningPlans = LearningCoursepath::model()->findAllByAttributes(array('is_selling' => 1));
			foreach ($paidLearningPlans as $paidLearningPlan) {
				$paidLearningPlan->is_selling = 0;
				$paidLearningPlan->subscribe_method = 0;
				$paidLearningPlan->save();
			}
		}
		else
		{
			// on switch to Shopify
			$params = array(
				'installation_id' => Docebo::getErpInstallationId(),
				'filter' => 'all',
				'lang' => Yii::app()->getLanguage(),
			);
			$appId = 0;
			$appLst = CJSON::decode(AppsMpApiClient::apiListAvailableApps($params));
			foreach ($appLst['data'] as $appDatum) {
				if ($appDatum['lang']['en']['title'] == 'E-Commerce') {
					$appId = $appDatum['lang']['en']['app_id'];
					break;
				}
			}
			// turn off Shopify from ERP
			$apiParams = array(
				'installation_id' => Docebo::getErpInstallationId(),
				'app_id' => $appId
			);
			$apiRes = AppsMpApiClient::apiAppCancel($apiParams);
		}

		unset($_SESSION['onECommerceDeactivation_dontMarkCoursesUnsold']);
	}


}
