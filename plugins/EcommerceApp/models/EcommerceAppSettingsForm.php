<?php

/**
 * EcommerceAppSettingsForm class.
 *
 * @property string $currency_symbol
 * @property string $currency_code
 * @property string $catalog_external
 *
 * @property string $paypal_enabled
 * @property string $paypal_mail
 * @property string $paypal_sandbox
 *
 * @property string $authorize_enabled
 * @property string $authorize_loginid
 * @property string $authorize_key
 * @property string $authorize_hash
 * @property string $authorize_sandbox
 *
 * @property string $stripe_enabled;
 * @property string $stripe_type;
 * @property string $stripe_tax_code;
 * @property string $stripe_alipay_enabled;
 * @property string $stripe_public_key_test;
 * @property string $stripe_private_key_test;
 * @property string $stripe_public_key;
 * @property string $stripe_private_key;
 * @property string $stripe_account_email;
 * @property string $stripe_sandbox;
 */
class EcommerceAppSettingsForm extends CFormModel {

    public $currency_symbol;
	public $currency_code;
    // moved into catalog // public $catalog_external;

	//PayPal
    public $paypal_enabled;
    public $paypal_mail;
    public $paypal_sandbox;

	//Authorize
    public $authorize_enabled;
    public $authorize_loginid;
    public $authorize_key;
    public $authorize_hash;
    public $authorize_sandbox;

	//Adyen
	public $adyen_enabled;
	public $adyen_merchant_account;
	public $adyen_hmac_key;
	public $adyen_skin_code;
	public $adyen_single_page_hpp;
	public $adyen_sandbox;

	//Stripe
	public $stripe_enabled;
	public $stripe_type;
	public $stripe_tax_code;
	public $stripe_alipay_enabled;
	public $stripe_public_key_test;
	public $stripe_private_key_test;
	public $stripe_public_key;
	public $stripe_private_key;
	public $stripe_account_email;
	public $stripe_sandbox;

	//cybersource
	public $cybersource_enabled;
	public $cybersource_access_key;
	public $cybersource_secret_key;
	public $cybersource_profile_id;
	public $cybersource_sandbox_mode;

	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			array('currency_symbol, currency_code,
				paypal_enabled, paypal_mail, paypal_sandbox,
				authorize_enabled, authorize_loginid, authorize_key, authorize_hash, authorize_sandbox,
				adyen_enabled, adyen_merchant_account, adyen_hmac_key, adyen_skin_code, adyen_single_page_hpp, adyen_sandbox,
				stripe_enabled, stripe_type, stripe_tax_code, stripe_alipay_enabled, stripe_public_key_test, stripe_private_key_test, stripe_public_key, stripe_private_key, stripe_sandbox, stripe_account_email,
				cybersource_enabled, cybersource_access_key, cybersource_secret_key, cybersource_profile_id, cybersource_sandbox_mode',
				'safe'),
			array('paypal_mail', 'email'),
			array('stripe_account_email', 'email'),
		);
	}


	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array(
			'currency_symbol' => Yii::t('configuration', '_CURRENCY_SYMBOL'),
			'currency_code' => Yii::t('standard', 'Currency'),
			
            'paypal_enabled' => Yii::t('standard', 'Enable'),
			'paypal_mail' => Yii::t('configuration', '_PAYPAL_MAIL'),
			'paypal_sandbox' => Yii::t('configuration', 'Sandbox mode'),

            'authorize_enabled' => Yii::t('configuration', 'Enable'),
            'authorize_loginid' => Yii::t('configuration', 'Login ID'),
            'authorize_key' => Yii::t('configuration', 'Transaction key'),
            'authorize_hash' => Yii::t('configuration', 'MD5 Hash'),
            'authorize_sandbox' => Yii::t('configuration', 'Sandbox mode'),

			'adyen_enabled' => Yii::t('configuration', 'Enable'),
			'adyen_merchant_account' => Yii::t('configuration', 'Merchant account'),
			'adyen_hmac_key' => Yii::t('configuration', 'HMAC key'),
			'adyen_skin_code' => Yii::t('configuration', 'Skin Code'),
			'adyen_single_page_hpp' => Yii::t('configuration', 'Single page HPP'),
			'adyen_sandbox' => Yii::t('configuration', 'Sandbox mode'),

			'stripe_enabled' => Yii::t('configuration', 'Enable'),
			'stripe_type' => Yii::t('configuration', 'Payment type'),
			'stripe_tax_code' => Yii::t('configuration', 'Tax code'),
			'stripe_alipay_enabled' => Yii::t('configuration', 'Alipay'),
			'stripe_public_key_test' => Yii::t('configuration', 'Test Publishable Key'),
			'stripe_private_key_test' => Yii::t('configuration', 'Test Secret Key'),
			'stripe_public_key' => Yii::t('configuration', 'Live Publishable Key'),
			'stripe_private_key' => Yii::t('configuration', 'Live Secret Key'),
			'stripe_account_email' => Yii::t('configuration', 'Stripe Account Email'),
			'stripe_sandbox' => Yii::t('configuration', 'Sandbox'),

			'cybersource_enabled' => Yii::t('configuration', 'Enable'),
			'cybersource_access_key' => Yii::t('organization', '_ORG_ACCESS'). ' ' . Yii::t('configuration', 'Key'),
			'cybersource_secret_key' => Yii::t('manmanu', 'Secret key'),
			'cybersource_profile_id' => Yii::t('standard', 'Profile ID'),
			'cybersource_sandbox_mode' => Yii::t('configuration', 'Sandbox mode'),
		);
	}


	public function beforeValidate() {

        // $this->catalog_external = ($this->catalog_external ? 'on' : 'off');

        $this->paypal_enabled = ($this->paypal_enabled ? 'on' : 'off');
        $this->paypal_sandbox = ($this->paypal_sandbox ? 'on' : 'off');

        $this->authorize_enabled = ($this->authorize_enabled ? 'on' : 'off');
        $this->authorize_sandbox = ($this->authorize_sandbox ? 'on' : 'off');

		$this->adyen_enabled = ($this->adyen_enabled ? 'on' : 'off');
		$this->adyen_sandbox = ($this->adyen_sandbox ? 'on' : 'off');

        return parent::beforeValidate();
	}


}
