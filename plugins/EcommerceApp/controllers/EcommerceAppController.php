<?php

class EcommerceAppController extends Controller {


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		$admin_only_actions = array('settings', 'coupons', 'cybersource');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}

    public function beforeAction($action) {
        if (
            isset($_POST['EcommerceAppSettingsForm']) ||
            ($action->id === 'axEditCoupon' && Yii::app()->request->isPostRequest) || //check add, edit and delete coupon
            ($action->id === 'axDeleteCoupon' && isset($_POST['EcommerceCoupon']))
        ) {
            Yii::app()->event->raise(CoreNotification::NTYPE_ECOMMERCE_SETTINGS_CHANGED, new DEvent($this, array('user'=>Yii::app()->user->id)));
        }

        return parent::beforeAction($action);
    }

	public function actionIndex() {
        $this->forward('settings');
	}


	public function actionSettings() {

		$settings = new EcommerceAppSettingsForm();

		if (isset($_POST['EcommerceAppSettingsForm'])) {
			$settings->setAttributes($_POST['EcommerceAppSettingsForm']);

			if ($settings->validate()) {
				Settings::save('currency_symbol', $settings->currency_symbol, 'string', 10, '0', 5, 2);
				Settings::save('currency_code', $settings->currency_code, 'string', 255, '0', 5, 2);
				// Settings::save('catalog_external', $settings->catalog_external, 'enum', 3, '0', 5, 4);

				Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));
			}
			else {
				Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
			}
		}

		$settings->currency_symbol = Settings::get('currency_symbol', null, false, false);
		$settings->currency_code = Settings::get('currency_code', null, false, false);
		// $settings->catalog_external = (Settings::get('catalog_external', null, false, false) == 'on' ? true : false);

        $this->render('index', array(
            'breadcrumbs' => Yii::t('standard', 'Settings'),
            'activeTab' => 'settings',
            'tabContent' => $this->renderPartial('_settings', array('settings' => $settings), true)
        ));
	}

    /**
     * Renders the coupons grid
     */
	public function actionCoupons() {

        // Load the datepicker (used in the new/edit coupon modal)
        Yii::app()->bootstrap->registerAssetCss('bootstrap-datepicker.css');
        Yii::app()->bootstrap->registerAssetJs('bootstrap.datepicker.js');

        $doUpdate = Yii::app()->request->getParam('update');

        $couponModel = new EcommerceCoupon('search');
        $couponModel->attributes = $_REQUEST['EcommerceCoupon'];

        // is used for autocomplete
        if (isset($_REQUEST['data']['autocomplete'])) {
            $couponModel->search_input = addcslashes($_REQUEST['data']['query'], '%_');
            $dataProvider = $couponModel->dataProvider();
            $dataProvider->pagination = false;
            $result = array('options' => array());
            foreach ($dataProvider->data as $coupon) {
                $result['options'][$coupon->code] = $coupon->code;
            }
            $this->sendJSON($result);
        }

        if ($doUpdate) {
            $this->renderPartial('_coupons', array(
                'model' => $couponModel
            ));
            Yii::app()->end();
        }

        $this->render('index', array(
            'breadcrumbs' => Yii::t('coupon', 'Coupons'),
            'activeTab' => 'coupons',
            'tabContent' => $this->renderPartial('_coupons', array('model' => $couponModel), true)
        ));
	}

    /**
     * Renders the Paypal settings
     */
	public function actionPaypal() {

        $settings = new EcommerceAppSettingsForm();

        if (isset($_POST['EcommerceAppSettingsForm'])) {
            $settings->setAttributes($_POST['EcommerceAppSettingsForm']);

            if ($settings->validate()) {
                Settings::save('paypal_enabled', $settings->paypal_enabled, 'enum', 3, '0', 5, 4);
                Settings::save('paypal_mail', $settings->paypal_mail, 'string', 255, '0', 5, 1);
                Settings::save('paypal_sandbox', $settings->paypal_sandbox, 'enum', 3, '0', 5, 4);

                Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));
            }
            else {
                Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
            }
        }

        $settings->paypal_enabled   = Settings::get('paypal_enabled', null, false, false) == 'on' ? true : false;
        $settings->paypal_mail      = Settings::get('paypal_mail', null, false, false);
        $settings->paypal_sandbox   = Settings::get('paypal_sandbox', null, false, false) == 'on' ? true : false;

        $this->render('index', array(
            'breadcrumbs' => 'Paypal',
            'activeTab' => 'paypal',
            'tabContent' => $this->renderPartial('_paypal', array('settings' => $settings), true)
        ));
	}

    /**
     * Renders the Authorize.net settings
     */
	public function actionAuthorizedotnet() {

        $settings = new EcommerceAppSettingsForm();

        if (isset($_POST['EcommerceAppSettingsForm'])) {
            $settings->setAttributes($_POST['EcommerceAppSettingsForm']);

            if ($settings->validate()) {
                if ($settings->authorize_enabled === 'on' && (
						(empty($settings->authorize_loginid)) || (empty($settings->authorize_key)) || (empty($settings->authorize_hash)))) {
                    Yii::app()->user->setFlash('error', Yii::t('configuration', 'Incomplete configuration.'));
                }
                else {
                    Settings::save('authorize_enabled', $settings->authorize_enabled, 'enum', 3, '0', 5, 4);
					if($settings->authorize_loginid !== 'xxxxxx')
                    	Settings::save('authorize_loginid', $settings->authorize_loginid, 'string', 255, '0', 5, 2);
					if($settings->authorize_key !== 'xxxxxx')
                    	Settings::save('authorize_key', $settings->authorize_key, 'string', 255, '0', 5, 1);
					if($settings->authorize_hash !== 'xxxxxx')
                    	Settings::save('authorize_hash', $settings->authorize_hash, 'string', 255, '0', 5, 1);
                    Settings::save('authorize_sandbox', $settings->authorize_sandbox, 'enum', 3, '0', 5, 4);

                    Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));
                }
            }
            else {
                Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
            }
        }

        $settings->authorize_enabled = Settings::get('authorize_enabled', null, false, false) == 'on' ? true : false;
        $settings->authorize_loginid = Settings::get('authorize_loginid', null, false, false) ? 'xxxxxx' : '';
        $settings->authorize_key = Settings::get('authorize_key', null, false, false) ? 'xxxxxx' : '';
        $settings->authorize_hash = Settings::get('authorize_hash', null, false, false) ? 'xxxxxx' : '';
        $settings->authorize_sandbox = Settings::get('authorize_sandbox', null, false, false) == 'on' ? true : false;

        $this->render('index', array(
            'breadcrumbs' => 'Authorize.net',
            'activeTab' => 'authorizedotnet',
            'tabContent' => $this->renderPartial('_authorizedotnet', array('settings' => $settings), true)
        ));
	}

    public function actionAdyen()
    {
        $settings = new EcommerceAppSettingsForm();

        if (isset($_POST['EcommerceAppSettingsForm'])) {
            $settings->setAttributes($_POST['EcommerceAppSettingsForm']);

            if ($settings->validate()) {
                if ($settings->adyen_enabled === 'on' && (
                        (empty($settings->adyen_merchant_account)) ||
                        (empty($settings->adyen_hmac_key)) ||
                        (empty($settings->adyen_skin_code)))
                ) {
                    Yii::app()->user->setFlash('error', Yii::t('configuration', 'Incomplete configuration.'));
                }
                else {
                    Settings::save('adyen_enabled', $settings->adyen_enabled, 'enum', 3, '0', 5, 4);
                    if($settings->adyen_merchant_account !== 'xxxxxx')
                        Settings::save('adyen_merchant_account', $settings->adyen_merchant_account, 'string', 255, '0', 5, 2);
                    if($settings->adyen_hmac_key !== 'xxxxxx')
                        Settings::save('adyen_hmac_key', $settings->adyen_hmac_key, 'string', 255, '0', 5, 1);
                    if($settings->adyen_skin_code !== 'xxxxxx')
                        Settings::save('adyen_skin_code', $settings->adyen_skin_code, 'string', 255, '0', 5, 1);

                    Settings::save('adyen_single_page_hpp', $settings->adyen_single_page_hpp, 'enum', 3, '0', 5, 4);
                    Settings::save('adyen_sandbox', $settings->adyen_sandbox, 'enum', 3, '0', 5, 4);

                    Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));
                }
            }
            else {
                Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
            }
        }

        //TODO: continue with fixing all the attributes needed for adyen and then, the form in the view
        $settings->adyen_enabled 			= Settings::get('adyen_enabled', null, false, false) == 'on' ? true : false;
        $settings->adyen_merchant_account 	= Settings::get('adyen_merchant_account', null, false, false) ? 'xxxxxx' : '';
        $settings->adyen_hmac_key 			= Settings::get('adyen_hmac_key', null, false, false) ? 'xxxxxx' : '';
        $settings->adyen_skin_code 			= Settings::get('adyen_skin_code', null, false, false) ? 'xxxxxx' : '';
        $settings->adyen_single_page_hpp 	= Settings::get('adyen_single_page_hpp', null, false, false) ? true : false;
        $settings->adyen_sandbox 			= Settings::get('adyen_sandbox', null, false, false) == 'on' ? true : false;

        $this->render('index', array(
            'breadcrumbs' => 'Adyen',
            'activeTab' => 'adyen',
            'tabContent' => $this->renderPartial('_adyen', array('settings' => $settings), true)
        ));
    }

    public function actionStripe()
    {
        $settings = new EcommerceAppSettingsForm();

        $ec = new EcommerceAppModule();
        Yii::app()->getClientScript()->registerCssFile($ec->getAssetsUrl().'/css/stripe.css');

        if (isset($_POST['EcommerceAppSettingsForm'])) {
            $settings->setAttributes($_POST['EcommerceAppSettingsForm']);
            Settings::save('stripe_sandbox', $settings->stripe_sandbox, 'int', 255, '0', 5, 1);

            if ($settings->validate()) {
                if (
                    (empty($settings->stripe_public_key)) ||
                    (empty($settings->stripe_private_key)) ||
                    (empty($settings->stripe_account_email))
                ) {
                    Yii::app()->user->setFlash('error', Yii::t('configuration', 'Incomplete configuration.'));
                }
                elseif($settings->stripe_sandbox && (empty($settings->stripe_public_key_test) || empty($settings->stripe_private_key_test))){
                    Yii::app()->user->setFlash('error', Yii::t('configuration', 'Incomplete configuration.'));
                }
                else {
                    Settings::save('stripe_enabled', $settings->stripe_enabled, 'int', 255, '0', 5, 1);
                    Settings::save('stripe_type', $settings->stripe_type, 'string', 255, '0', 5, 1);
                    Settings::save('stripe_tax_code', $settings->stripe_tax_code, 'string', 255, '0', 5, 1);
                    Settings::save('stripe_public_key_test', $settings->stripe_public_key_test, 'string', 255, '0', 5, 2);
                    Settings::save('stripe_private_key_test', $settings->stripe_private_key_test, 'string', 255, '0', 5, 1);
                    Settings::save('stripe_public_key', $settings->stripe_public_key, 'string', 255, '0', 5, 2);
                    Settings::save('stripe_private_key', $settings->stripe_private_key, 'string', 255, '0', 5, 1);
                    Settings::save('stripe_account_email', $settings->stripe_account_email, 'string', 255, '0', 5, 1);
                    Settings::save('stripe_alipay_enabled', $settings->stripe_alipay_enabled, 'int', 255, '0', 5, 1);

                    Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));
                }
            }
            else {
                Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
            }
        }

        $settings->stripe_enabled = Settings::get('stripe_enabled', null, false, false);
        $settings->stripe_type = Settings::get('stripe_type', 'simple', false, false);
        $settings->stripe_tax_code = Settings::get('stripe_tax_code', null, false, false);
        $settings->stripe_alipay_enabled = Settings::get('stripe_alipay_enabled', null, false, false);
        $settings->stripe_public_key_test = Settings::get('stripe_public_key_test', null, false, false);
        $settings->stripe_private_key_test = Settings::get('stripe_private_key_test', null, false, false);
        $settings->stripe_public_key = Settings::get('stripe_public_key', null, false, false);
        $settings->stripe_private_key = Settings::get('stripe_private_key', null, false, false);
        $settings->stripe_account_email = Settings::get('stripe_account_email', null, false, false);
        $settings->stripe_sandbox = Settings::get('stripe_sandbox', null, false, false);

        $this->render('index', array(
            'breadcrumbs' => 'Stripe',
            'activeTab' => 'stripe',
            'tabContent' => $this->renderPartial('_stripe', array('settings' => $settings), true)
        ));
    }

    /**
     * Render the edit coupon modal
     */
    public function actionAxEditCoupon() {
        $model = new EcommerceCouponForm();

        if (Yii::app()->request->isPostRequest) {
            if ($model->save()) {
                // close dialog and reload grid
                echo '<a href="" class="auto-close"></a>';
                return;
            }
        }

        // Load the datepicker (used in the new/edit coupon modal)
        Yii::app()->bootstrap->registerAssetCss('bootstrap-datepicker.css');
        Yii::app()->bootstrap->registerAssetJs('bootstrap.datepicker.js');

        $this->renderPartial('_edit_coupon', array('model' => $model), false, true);
    }

    /**
     * Deletes a coupon
     */
    public function actionAxDeleteCoupon() {
        $model = EcommerceCoupon::model()->findByPk(Yii::app()->request->getParam('id_coupon'));

        if (isset($_POST['EcommerceCoupon'])) {
            if ($model->delete()) {
                // close dialog and reload locations
                echo '<a href="" class="auto-close"></a>';
                return;
            }
        }

        $this->renderPartial('_delete_coupon', array(
            'model' => $model
        ));
    }

    public function actionAssignCourses() {

        $model = EcommerceCoupon::model()->findByPk(Yii::app()->request->getParam('id_coupon'));

		if ((isset($_REQUEST['selectedItems']) && ($_REQUEST['selectedItems'] != ''))) {
            $selectedItems = explode(',', $_REQUEST['selectedItems']);
            if(!empty($selectedItems) && is_array($selectedItems)){
				Yii::app()->session['selectedItems'] = $selectedItems;
            }
        }

        $this->render('index', array(
            'breadcrumbs' => array(
                Yii::t('coupon', 'Coupons') => $this->createUrl('coupons'),
                Yii::t('standard', '_ASSIGN_COURSES'),
            ),
            'activeTab' => 'coupons',
            'tabContent' => $this->renderPartial('_assign_courses', array('model' => $model), true)
        ));
    }

    public function actionAxAssignSelectCourses() {

        $courseModel = new LearningCourse();
        $courseModel->unsetAttributes();
        $courseModel->attributes = Yii::app()->request->getParam('LearningCourse');

        $idCoupon = Yii::app()->request->getParam('id_coupon');

        if ((isset($_REQUEST['selectedItems']) && ($_REQUEST['selectedItems'] != ''))) {
            $selectedItems = explode(',', $_REQUEST['selectedItems']);
            if(!empty($selectedItems) && is_array($selectedItems)){
                Yii::app()->session['selectedItems'] = $selectedItems;
            }
        }
		else
			Yii::app()->session['selectedItems'] = array();

        if ( ! empty($_POST['coupon-courses-assign-grid-selected-items']) ) {
            $selectedCourses = explode(',', $_POST['coupon-courses-assign-grid-selected-items']);
            foreach ($selectedCourses as $assignedCourseId) {
                try {
                    $model = new EcommerceCouponCourses();
                    $model->id_coupon = $idCoupon;
                    $model->id_course = $assignedCourseId;
                    $model->save();
                }
                catch (CDbException $ex) {

                }
            }
			Yii::app()->session['selectedItems'] = array();
            $this->sendJSON(array('status' => 'saved'));
        }

		$excludedCourses =	Yii::app()->db->createCommand()->select('id_course')
							->from(EcommerceCouponCourses::model()->tableName())
							->where('id_coupon = :id_coupon', array(':id_coupon' => $idCoupon))
							->queryColumn();

        $result = array();

        $processOutput = false;

        if (isset($_REQUEST['popupFirstShow']) && $_REQUEST['popupFirstShow'] === '1') {
            $processOutput = true;
        }

        $result['html'] = $this->renderPartial('_assign_select_courses', array(
            'model' => $courseModel,
            'excludedCourses' => $excludedCourses
        ), true, $processOutput);

        if (isset($_REQUEST['contentType']) && ($_REQUEST['contentType'] == 'html')) {
            echo $result['html'];
        } else {
            $this->sendJSON($result);
        }

    }

	public function actionClearCoursesSelection()
	{
		Yii::app()->session['selectedItems'] = array();
	}

    public function actionAxDeleteAssignedCourse() {

        $idCoupon = Yii::app()->request->getParam('id_coupon');
        $ids = Yii::app()->request->getParam('ids', array());

        $criteria = new CDbCriteria();
        $criteria->addCondition('id_coupon = :id_coupon');
        $criteria->params[':id_coupon'] = $idCoupon;
        $criteria->addInCondition('id_course', $ids);

        $models = EcommerceCouponCourses::model()->findAll($criteria);

        if (isset($_POST['confirm_unassign'])) {
            foreach ($models as $model)
                $model->delete();

            // close dialog and reload grid
            echo '<a href="" class="auto-close"></a>';
            Yii::app()->end();
        }

        $this->renderPartial('_delete_assigned_course', array(
            'models' => $models
        ));
    }

    public function actionAxMassiveDeleteAssignedCourse() {

        $idCoupon = Yii::app()->request->getParam('id_coupon');
        $ids = Yii::app()->request->getParam('ids');

        $criteria = new CDbCriteria();
        $criteria->addCondition('id_coupon = :id_coupon');
        $criteria->params[':id_coupon'] = $idCoupon;
        $criteria->addInCondition('id_course', $ids);

        $models = EcommerceCouponCourses::model()->findAll($criteria);

        if (isset($_POST['confirm_unassign'])) {
            foreach ($models as $model)
                $model->delete();

            // close dialog and reload grid
            $this->sendJSON(array());
        }

        if (Yii::app()->request->isAjaxRequest) {
            $content = $this->renderPartial('_massive_delete_assigned_course', array(
                'models' => $models
            ), true);
            $this->sendJSON(array(
                'html' => $content,
            ));
        }
        Yii::app()->end();
    }

    public function actionAxCouponDetails() {
        $model = EcommerceCoupon::model()->findByPk(Yii::app()->request->getParam('id_coupon'));
        if (!$model)
            $model = new EcommerceCoupon();

        echo $this->renderPartial('_coupon_details', array(
            'model' => $model
        ), true, true);
    }

    public function actionCybersource(){
        $settings = new EcommerceAppSettingsForm();

        if (isset($_POST['EcommerceAppSettingsForm']) && !empty($_POST['EcommerceAppSettingsForm'])) {
            $settings->setAttributes($_POST['EcommerceAppSettingsForm']);
            Settings::save('cybersource_enabled', $settings->cybersource_enabled);
            Settings::save('cybersource_access_key', $settings->cybersource_access_key);
            Settings::save('cybersource_profile_id', $settings->cybersource_profile_id);
            Settings::save('cybersource_sandbox_mode', $settings->cybersource_sandbox_mode);
            Settings::save('cybersource_secret_key', $settings->cybersource_secret_key);
        }

        $settings->cybersource_enabled = Settings::get('cybersource_enabled');
        $settings->cybersource_access_key = Settings::get('cybersource_access_key');
        $settings->cybersource_profile_id = Settings::get('cybersource_profile_id');
        $settings->cybersource_sandbox_mode = Settings::get('cybersource_sandbox_mode');
        $settings->cybersource_secret_key = Settings::get('cybersource_secret_key');

        $this->render('index', array(
            'breadcrumbs' => 'Cybersource',
            'activeTab' => 'cybersource',
            'tabContent' => $this->renderPartial('_cybersource', array('settings' => $settings), true)
        ));
    }

    protected function gridRenderLinkCouponDetails($row, $index) {
        $html = CHtml::link('<span class="i-sprite is-poll"></span>', $this->createUrl('axCouponDetails', array('id_coupon' => $row['id_coupon'])), array(
            'class' => 'open-dialog link-coupon-details',
            'data-dialog-class' => 'modal-coupon-details',
            'title' => Yii::t('standard', '_DETAILS'),
            'closeOnOverlayClick' => 'false',
            'closeOnEscape' => 'false',
            'rel' => 'tooltip',
        ));
        return $html;
    }

    protected function gridRenderSingleCouponDetailsLink($row, $index) {
        $html = CHtml::link('<span class="i-sprite is-poll"></span>', Docebo::createAdminUrl('transaction/index', array(
            CHtml::decode( CHtml::activeName(EcommerceTransaction::model(), 'search_input') ) => $row['id_trans']
        )), array(
            'class' => 'link-coupon-details',
            'title' => Yii::t('standard', '_DETAILS'),
            'data-coupon-id' => $row['id_coupon']
        ));
        return $html;
    }

    protected function gridRenderLinkEditCoupon($row, $index) {
        $html = CHtml::link('<span class="i-sprite is-edit"></span>', $this->createUrl('axEditCoupon', array('id_coupon' => $row['id_coupon'])), array(
            'class' => 'open-dialog link-edit-coupon',
            'data-dialog-class' => 'modal-edit-coupon',
            'title' => Yii::t('standard', '_MOD'),
            'closeOnOverlayClick' => 'false',
            'closeOnEscape' => 'false',
            'rel' => 'tooltip',
        ));
        return $html;
    }

    protected function gridRenderLinkDeleteCoupon($row, $index) {
        $html = CHtml::link('<span class="i-sprite is-remove red"></span>', $this->createUrl('axDeleteCoupon', array('id_coupon' => $row['id_coupon'])), array(
            'class' => 'open-dialog link-delete-coupon',
            'data-dialog-class' => 'modal-delete-coupon',
            'title' => Yii::t('standard', '_DEL'),
            'rel' => 'tooltip',
        ));
        return $html;
    }

}
