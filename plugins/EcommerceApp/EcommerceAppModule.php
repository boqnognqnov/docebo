<?php

class EcommerceAppModule extends CWebModule
{
	/**
	 * URL to this module's assets (web accessible)
	 *
	 * @var string
	 */
	protected $_assetsUrl = null;

	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

	}

	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu() {

		Yii::app()->mainmenu->addAppMenu('ecommerce', array(
			'icon' => 'admin-ico ecommerce',
			'app_icon' => 'fa-shopping-cart', // icon used in the new menu
			'link' => 'javascript:void(0);',
			'label' => Yii::t('app', 'ecommerce'),
			'settings' => Docebo::createAdminUrl('//EcommerceApp/EcommerceApp/settings'),
		), array(
			Yii::t('standard', '_TRANSACTION') => Docebo::createAdminUrl('transaction/index'),
			Yii::t('coupon', 'Coupons') => Docebo::createAdminUrl('//EcommerceApp/EcommerceApp/coupons')
		));
	}

	/**
	 *
	 */
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('EcommerceApp.assets'));
		}
		return $this->_assetsUrl;
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
/*

DELETE FROM `core_setting` WHERE `param_name` IN ('ecommerce_authorize_enabled', 'ecommerce_authorize_login_id', 'ecommerce_authorize_md5_hash', 'ecommerce_authorize_sandbox', 'ecommerce_authorize_transaction_key', 'ecommerce_paypal_enabled', 'paypal_enabled', 'authorize_enabled', 'authorize_loginid', 'authorize_key', 'authorize_hash', 'authorize_sandbox');


INSERT INTO `core_setting` (`param_name`, `param_value`, `value_type`, `max_size`, `pack`, `regroup`, `sequence`, `param_load`, `hide_in_modify`, `extra_info`)
VALUES
    ('paypal_enabled', 'on', 'enum', 3, '0', 5, 3, 1, 1, ''),
    ('authorize_enabled', 'off', 'enum', 3, '0', 5, 3, 1, 1, '');

*/