<?php

class GoogleDriveApp extends DoceboPlugin {

	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'GoogleDriveApp';
	public static $HAS_SETTINGS = true;

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name = __CLASS__) {
		return parent::plugin($class_name);
	}

	public static function settingsUrl()
	{
		return Docebo::createAdminUrl('//GoogleDriveApp/googleDriveApp/settings');
	}

	public function activate() {
		Yii::import('plugin.GoogleDriveApp.components.*');
		parent::activate();

		$sql = <<< SQL

SET FOREIGN_KEY_CHECKS=0;

--
-- Table structure for table `learning_googledoc`
--

CREATE TABLE IF NOT EXISTS `learning_googledoc` (
  `idPage` int(11) NOT NULL AUTO_INCREMENT,
  `import_type` varchar(10) NOT NULL DEFAULT 'embed',
  `title` varchar(150) NOT NULL,
  `url` varchar(450) NOT NULL,
  `description` text,
  `author` int(11) NOT NULL,
  PRIMARY KEY (`idPage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

SET FOREIGN_KEY_CHECKS=1;

SQL;

		$command = Yii::app()->db->createCommand($sql);
		$command->execute();
	}

	public function deactivate() {
		parent::deactivate();
	}

}
