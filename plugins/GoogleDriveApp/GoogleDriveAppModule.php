<?php

class GoogleDriveAppModule extends CWebModule
{

	private $_assetsUrl;

	public function init()
	{
		// import the module-level models and components
		$this->setImport(array(
			'GoogleDriveApp.components.*'
		));

		$this->defaultController = 'googleDriveApp';

		// Register CSS ONLY if this is NOT an ajax call. Plugins' init() is always called during their loading
		if (Yii::app() instanceof CWebApplication) {
			if (!Yii::app()->request->isAjaxRequest) {
				Yii::app()->getClientScript()->registerCssFile($this->getAssetsUrl() . "/css/googleDrive.css");
			}
		}

//		Yii::app()->event->on(EventManager::EVENT_ON_RENDER_USERMAN_USER_ACTIONS, array($this, 'onUsersActionsRender'));

		Yii::app()->event->on(EventManager::EVENT_ON_PLUGIN_DEACTIVATE, array($this, 'onPluginDeactivate'));

	}


	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(
				Yii::getPathOfAlias('GoogleDriveApp.assets'),
				false,
				-1,
				Yii::app()->params['forceCopyAssets']
			);
		}
		return $this->_assetsUrl;
	}

	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu()
	{
		Yii::app()->mainmenu->addAppMenu(
			'googledrive',
			array(
				'app_initials' => 'GD',
				'settings' => Docebo::createAdminUrl('GoogleDriveApp/googleDriveApp/settings'),
				'label' => 'Google Drive',
			), ''
		);
	}

	public function onPluginDeactivate(DEvent $event){

		// We don't care about Plugins anyway, we only do this check for GoogleDriveApp!
		if (!isset($event->params['pluginName']) || $event->params['pluginName'] !== $this->name) {
			$event->return_value['canProceed'] = true;
			return;
		}

		$canProceed = true;

		$itemsStandalone = Yii::app()->db->createCommand()
			->select("COUNT(idOrg)")
			->from(LearningOrganization::model()->tableName())
			->where('objectType = :objectType', array(
				':objectType' => LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE
			))
			->queryScalar();
		if($itemsStandalone > 0) $canProceed = false;

		$itemsCentralLO = Yii::app()->db->createCommand()
			->select("COUNT(id_object)")
			->from(LearningRepositoryObject::model()->tableName())
			->where('object_type = :objectType', array(
				':objectType' => LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE
			))
			->queryScalar();
		if($itemsCentralLO > 0) $canProceed = false;

		if($canProceed === false){
			Yii::app()->user->setFlash('error', Yii::t('googledrive', 'In order to deactivate this app, you should delete all Google Drive training materials from your courses'));
		}

		$event->return_value['canProceed'] = $canProceed;
	}

}
