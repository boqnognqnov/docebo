<?php
class m160719_074254_CREATE_TABLE_learning_googledoc extends DoceboDbMigration {

	public function safeUp()
	{
		$order66 = "CREATE TABLE IF NOT EXISTS `learning_googledoc` (
		  `idPage` int(11) NOT NULL AUTO_INCREMENT,
		  `title` varchar(150) NOT NULL,
		  `url` varchar(450) NOT NULL,
		  `description` text,
		  `author` int(11) NOT NULL,
		  PRIMARY KEY (`idPage`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
		$this->execute($order66);
		return true;
	}

	public function safeDown()
	{
		$order66 = "DROP TABLE IF EXISTS `learning_googledoc`;";
		$this->execute($order66);
		return true;
	}
	
	
}
