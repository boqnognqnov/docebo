<?php
class m160928_070236_ALTER_TABLE_learning_googledoc_ADD_COLUMN_import_type extends DoceboDbMigration {

	public function safeUp()
	{
		$order66 = "ALTER TABLE `learning_googledoc` ADD `import_type` VARCHAR(10) NOT NULL DEFAULT 'embed' AFTER `idPage`;";
		$this->execute($order66);
		return true;
	}

	public function safeDown()
	{
		$order66 = "ALTER TABLE `learning_googledoc` DROP `import_type`;";
		$this->execute($order66);
		return true;
	}
}
