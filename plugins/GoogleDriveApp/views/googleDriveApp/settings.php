<?php
$_breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	'Google Drive',
	Yii::t('standard', 'Settings')
);

if (is_array($breadcrumbs)) {
	$_breadcrumbs = array_merge($_breadcrumbs, $breadcrumbs);
} else {
	$_breadcrumbs[] = $breadcrumbs;
}

$this->breadcrumbs = $_breadcrumbs;
?>

<div class="row-fluid">
	<div class="span6">
		<h3 class="advanced-googledrive-settings-form-title"><?=Yii::t('googledrive', 'Google Drive Settings')?></h3>
	</div>
	<a href="<?=Docebo::createAdminUrl('GoogleDriveApp/googleDriveApp/resetConfig')?>"
	   style="margin-left: 15px;"
	   class="open-dialog btn btn-docebo red big open-dialog pull-right"
	   data-dialog-class="googledrive-dialog-reset-config"
	   title="<?= Yii::t('googledrive', 'Reset configuration') ?>"
		>
		<?=Yii::t('googledrive', 'Reset configuration');?>
	</a>
</div>
<br>

<?php DoceboUI::printFlashMessages(); ?>

<?= CHtml::beginForm(); ?>

<div class="advanced-main googledrive-settings">

	<div class="section">

		<div class="row odd">
			<div class="row">
				<div class="setting-name">
					<?= Yii::t('googledrive', 'Google Picker') ?>
				</div>
				<div class="values">
					<?= CHtml::checkBox('use_picker', $usePicker, array(
						'value' => 1,
						'uncheckValue' => 0
					)) ?>
					<?=CHtml::label(Yii::t('googledrive', 'Enable Google Picker'), 'use_picker', array('id' => 'label-use-picker'))?>
				</div>
			</div>
		</div>

		<div class="row even google-setting">
			<div class="row">
				<div class="setting-name">
					<?= Yii::t('apps', 'API Key') ?>
				</div>
				<div class="values">
					<?= CHtml::textField('api_key', $apiKey) ?>
				</div>
			</div>
		</div>

		<div class="row odd google-setting">
			<div class="row">

				<div class="setting-name google-setting">
					<?= Yii::t('app', 'Client id') ?>
				</div>
				<div class="values">
					<?= CHtml::textField('client_id', $clientId) ?>
				</div>

			</div>
		</div>

		<div class="row even row-fluid googledrive-align-right">
			<div class="row">
				<?= CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
					'class' => 'btn btn-docebo green big',
					'name' => 'submit'
				)); ?>
				<?= CHtml::submitButton(Yii::t('standard', '_CANCEL'), array(
					'class' => 'btn btn-docebo black big',
					'name' => 'cancel'
				)); ?>
			</div>
		</div>

	</div>
</div>

<?= CHtml::endForm() ?>

<script type="text/javascript">
	$(function(){
		if ( $(".alert-success").length) {
			setInterval(function(){
				$(".alert-success").fadeOut(1500);
			}, 3000);
		}
		if ( $(".alert-error").length) {
			setInterval(function(){
				$(".alert-error").fadeOut(1500);
			}, 3000);
		}

		function styleApiKeyAndClientId(){
			var pickerEnabled = $('#use_picker').prop('checked');
			if(pickerEnabled){
				$('.google-setting').css('opacity', '1');
				$('.google-setting input[type=text]').attr('disabled', false);
			} else {
				$('.google-setting').css('opacity', '.65');
				$('.google-setting input[type=text]').attr('disabled', true);
			}
			console.log(pickerEnabled);
		}

		styleApiKeyAndClientId();

		$('#use_picker').on('change', function(){
			styleApiKeyAndClientId();
		});

		$("#use_picker").styler();
	});
</script>