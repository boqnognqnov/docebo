<div id="<?= isset($containerId) ? 'player-centralrepo-uploader-panel' : 'player-arena-googledrive-editor-panel' ?>" class="player-arena-editor-panel" xmlns="http://www.w3.org/1999/html">
	<div class="header"><?php echo Yii::t('googledrive', 'Google Drive'); ?></div>
	<div class="content">
		<?php
		/* @var $htmlPageObj LearningGoogledoc */
		/* @var $form CActiveForm */
		?>

		<?php
		$form = $this->beginWidget('CActiveForm', array(
			'id' => $centralRepoContext?'central-lo-gdrive-editor-form':'googledrive-editor-form',
			'method' => 'post',
			'action' => isset($saveUrl) ? $saveUrl : Docebo::createAbsoluteLmsUrl("//GoogleDriveApp/GoogleDriveApp/axSaveLo"),
			'htmlOptions' => array(
				'class' => 'form-horizontal googledrive-form',
				'enctype' => 'multipart/form-data',
			)
		));
		?>

		<?=(!empty($courseModel))?$form->hiddenField($courseModel, 'idCourse'):'';?>
		<?=$form->hiddenField($googleDriveObj, 'idPage');?>
		<?=CHtml::hiddenField('wrong_type', 0)?>

		<div id="<?= isset($containerId) ? $containerId : 'player-arena-uploader' ?>">
			<div>
				<div class="tabbable"> <!-- Only required for left/right tabs -->
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab1" data-toggle="tab"><?=LearningOrganization::objectTypeValue($lo_type)?></a></li>
						<li><a href="#lo-additional-information" data-toggle="tab"><?=Yii::t('myactivities', 'Additional info')?></a></li>

						<?php
						// Raise event to let plugins add new tabs
						if(!$centralRepoContext){
							Yii::app()->event->raise('RenderEditLoTabs', new DEvent($this, array(
								'courseModel' => $courseModel,
								'loModel' => isset($loModel) ? $loModel : null
							)));
						}
						?>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab1">
							<div class="player-arena-uploader">
								<?php if(!$usePicker){ ?>
								<div class="control-group" id="googledrive_warning">
									<?php
									$this->widget('common.widgets.warningStrip.WarningStrip', array(
										'message'	=> Yii::t('googledrive', "Importing files is not possible until you have enabled the Google Picker in your Google Drive App and have set up your credentials. Click {XX_here_XX} to configure your app.", array('{XX_here_XX}' => CHtml::link(Yii::t('googledrive', 'here'), Docebo::createAdminUrl('//GoogleDriveApp/googleDriveApp/settings')))),
										'type'		=>'warning',
									));
									?>
								</div>
								<?php } ?>
								<div class="control-group">
									<?php echo CHtml::label(Yii::t('googledrive', 'Google file source'), 'import_types', array(
										'class' => 'control-label'
									)); ?>
									<div class="controls">
										<div class="googledrive-radiolist">
											<?=$form->radioButtonList($googleDriveObj, 'import_type', array(
												LearningGoogledoc::GOOGLEDRIVE_IMPORTTYPE_IMPORT => Yii::t('googledrive', 'Import file'),
												LearningGoogledoc::GOOGLEDRIVE_IMPORTTYPE_EMBED => Yii::t('googledrive', 'Embed URL')
											), array(
												'separator' => '',
												'template' => '<span>{input}{label}</span>'
											))?>
										</div>
										<div id="googledrive_container_import" class="googledrive_container" style="display: <?=($googleDriveObj->import_type==LearningGoogledoc::GOOGLEDRIVE_IMPORTTYPE_IMPORT)?'block':'none'?>;">
											<div class="googledrive_container_import2">
												<button type="button" id="pick" class="btn-docebo green big"><?=Yii::t('googledrive', 'Load from Google Drive')?></button>
												<span class="googledrive_preview_url"><?=($googleDriveObj->import_type == LearningGoogledoc::GOOGLEDRIVE_IMPORTTYPE_IMPORT)?$googleDriveObj->url:''?></span>
												<input type="hidden" value="<?=$googleDriveObj->url?>" name="LearningGoogledoc[url2]" id="LearningGoogledoc_url2" />
											</div>
										</div>
										<div id="googledrive_container_embed" class="googledrive_container" style="display: <?=($googleDriveObj->import_type==LearningGoogledoc::GOOGLEDRIVE_IMPORTTYPE_EMBED)?'block':'none'?>;">
											<?= $form->textField($googleDriveObj, 'url', array('class' => 'input-block-level')) ?>
											<span class="url-valudator-info"></span>
											<span class="googledrive_title_info"><?=Yii::t('googledrive', 'Paste URL of Google drive file (the available formats are Google Docs, Google Sheets, Google Slides, Google Drawings)')?></span>
										</div>
									</div>
								</div>

								<div class="control-group">
									<?php echo CHtml::label(Yii::t('standard', '_TITLE'), 'LearningGoogleDrive_title', array(
										'class' => 'control-label'
									)); ?>
									<div class="controls">
										<?= $form->textField($googleDriveObj, 'title', array('class' => 'input-block-level')) ?>
									</div>
								</div>

								<div class="control-group">
									<?php echo CHtml::label(Yii::t('standard', '_DESCRIPTION'), 'LearningGoogleDrive_description', array(
										'class' => 'control-label'
									)); ?>
									<div class="controls">
										<?php echo $form->error($googleDriveObj,'description'); ?>
										<?= $form->textArea($googleDriveObj, 'description', array('id' => 'player-arena-googledrive-editor-textarea')); ?>
									</div>
								</div>
								<br>

							</div>
						</div>
						<div class="tab-pane" id="lo-additional-information">
							<?php
							$additionalInfoWidgetParams = array(
								'loModel'=> $loModel,
								'idCourse' => isset($courseId) ? $courseId : null,
							);

							if($centralRepoContext){
								$additionalInfoWidgetParams['refreshUrl'] = Docebo::createAbsoluteLmsUrl('centralrepo/centralLo/getSlidersContent');
								$additionalInfoWidgetParams['deleteUrl'] = Docebo::createAbsoluteLmsUrl('centralrepo/centralLo/deleteImage');
							}
							?>
							<? $this->widget('common.widgets.PlayerLayoutAdditionalInfo', $additionalInfoWidgetParams); ?>
						</div>

						<?php
						// Raise event to let plugins add new tab contents
						if(!$centralRepoContext){
							Yii::app()->event->raise('RenderEditLoTabsContent', new DEvent($this, array(
								'courseModel' => isset($courseModel) ? $courseModel : null,
								'loModel' => isset($loModel) ? $loModel : null
							)));
						}
						?>
					</div>
				</div>

			</div>

		</div>

		<div class="text-right">
			<input type="submit" name="confirm_save_googledrive" id="confirm_save_googledrive" class="btn-docebo green big"
				   value="<?php echo Yii::t('standard', '_SAVE'); ?>">&nbsp;&nbsp;
			<a id="cancel_save_googledrive"
			   class="btn-docebo black big" <?= isset($backButtonUrl) ? "href='" . $backButtonUrl . "'" : "" ?>><?php echo Yii::t('standard', '_CANCEL'); ?></a>
		</div>

		<?php if($idObject): ?>
			<input type="hidden" name="id_object" value="<?=$idObject ?>">
		<?php endif; ?>

		<?php $this->endWidget(); ?>

	</div>
</div>
<script type="text/javascript">
	/*<![CDATA[*/
	(function ($) {
		$(function () {
			$(".googledrive-radiolist input").on('change', function(){
				var selectedType = $(".googledrive-radiolist input:checked").val();
				$('.googledrive_container').css('display', 'none');
				$('#googledrive_container_'+selectedType).css('display', 'block');
			});

			<?php if(!$usePicker){ ?>
			$('#LearningGoogledoc_import_type_0').attr('disabled', true);
			$('label[for=LearningGoogledoc_import_type_0]').addClass('disabledLabel');
			<?php } ?>
			$(".googledrive-radiolist input").styler();

			$('#confirm_save_googledrive').on('click', function(){
				$('#wrong_type').val(0);
			});

			$(document).controls();

			<?php if(isset($containerId)): ?>
			TinyMce.attach('#player-arena-googledrive-editor-textarea', 'standard_embed', {height: 300});
			var editGD = new CentralLoGoogleDriveEdit({
				editGoogleDriveUrl: <?= json_encode(Docebo::createAbsoluteAdminUrl('GoogleDriveApp/googleDriveApp/editCentralRepo')) ?>
			});
			editGD.formListeners();
			<?php endif; ?>

		});
	})(jQuery);

	function initPicker() {
		var picker = new FilePicker({
			apiKey: '<?=$apiKey?>',
			clientId: '<?=$clientId?>',
			buttonEl: document.getElementById('pick'),
			onSelect: function(file) {
				// Only take care of files having an embed link:
				if(file.hasOwnProperty('embedLink')) {
					$('#wrong_type').val(0);
					$('#LearningGoogledoc_url2').val(file.embedLink);
					$('.googledrive_preview_url').text(file.embedLink);
					$('#LearningGoogledoc_title').val(file.title);
					if (file.hasOwnProperty('description')) {
						var editor = tinymce.get('player-arena-googledrive-editor-textarea');
						editor.setContent(file.description);
					}
					if (file.hasOwnProperty(file.thumbnailLink)) {
						// todo: add thumbnail functionality:
						// 1: import the file into the LMS
						// 2: add the file to online storage
						// 3: log new thumbnail in the resource table
						// 4: update thumb field
					}
				} else {
					$('#wrong_type').val(1);
					$('.googledrive-form').submit();
				}
			}
		});
	}

	/*]]>*/
</script>

<script src="https://www.google.com/jsapi?key=<?=$apiKey?>"></script>
<script src="https://apis.google.com/js/client.js?onload=initPicker&<?=time()?>"></script>