<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, target-densitydpi=160dpi, minimal-ui">
	<style type="text/css">
		body{
			margin: 0;
			padding: 0;
		}

		.googledrive-container{
			position: absolute;
			width: 100%;
			height: 100%;
		}

		iframe{
			position: absolute;
			height: 100%;
			width: 100%;
			border: none;
		}
	</style>
</head>
<body>
<?php if(empty($error)){ ?>
<div class="googledrive-container">
	<iframe src="<?=$document->url?>">Sorry, your browser doesn't support I-frames.</iframe>
</div>
<?php } else { ?>
	<p><?=$error['error']?></p>
<?php } ?>
</body>
</html>