<?php

/**
 * This is the model class for table "learning_googledoc".
 *
 * The followings are the available columns in table 'learning_googledoc':
 * @property integer $idPage
 * @property string $import_type
 * @property string $title
 * @property string $url
 * @property string $description
 * @property integer $author
 */
class LearningGoogledoc extends CActiveRecord
{
	const GOOGLEDRIVE_IMPORTTYPE_EMBED = 'embed';
	const GOOGLEDRIVE_IMPORTTYPE_IMPORT = 'import';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_googledoc';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('import_type, title, url, author', 'required'),
			array('author', 'numerical', 'integerOnly'=>true),
			array('import_type', 'length', 'max'=>10),
			array('title', 'length', 'max'=>150),
			array('url', 'length', 'max'=>450),
			array('description', 'safe'),
			// The following rule is used by search().
			array('idPage, import_type, title, url, description, author', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idPage' => 'Id Page',
			'import_type' => 'Import type',
			'title' => 'Title',
			'url' => 'Url',
			'description' => 'Description',
			'author' => 'Author',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idPage',$this->idPage);
		$criteria->compare('import_type',$this->import_type,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('author',$this->author);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningGoogledoc the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @param string $url link to Google Drive document
	 * @return array
	 */
	public static function validateGoogleDriveUrl($url){
		$result = array();
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_NOBODY, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_exec($ch);
		$info = curl_getinfo($ch);
		curl_close($ch);

		if($info['http_code'] == 404){
			$result['error'] = Yii::t('googledrive', 'The supplied URL is not a valid Google Drive document.');
		} elseif($info['download_content_length'] == 0){
			$result['error'] = Yii::t('googledrive', "This google drive document has not yet been published or can't be read otherwise.");
		}
		return $result;
	}
}
