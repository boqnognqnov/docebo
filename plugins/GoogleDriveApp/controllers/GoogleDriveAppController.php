<?php

class GoogleDriveAppController extends LoBaseController {

	/**
	 * Regular expression used to check if the URL matches the requirements
	 */
	const GOOGLE_DRIVE_URL_REGEX_PATTERN = "/^(https:\/\/docs.google.com\/)(a\/.{3,}\/)?(document|spreadsheets|presentation|drawings)(\/d\/).{10,}(\/pub|\/pubhtml|\/embed|\/preview|\/htmlembed)/";

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		$admin_only_actions = array('settings');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'actions' => array('webhook'),
			'users' => array('*'),
		);


		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}

	public function actionSettings(){
		$usePicker = Settings::get('googledrive_use_picker', 0);
		$apiKey = Settings::get('googledrive_api_key', false);
		$clientId = Settings::get('googledrive_client_id', false);

		if($_POST){
			if(isset($_POST['cancel'])){
				$this->redirect(Docebo::createLmsUrl('app/index'));
			}

			if(isset($_POST['submit'])){
				$usePicker = Yii::app()->request->getParam('use_picker', 0);
				$apiKey = Yii::app()->request->getParam('api_key', false);
				$clientId = Yii::app()->request->getParam('client_id', false);

				if(!$usePicker || (($apiKey && $clientId) && (strlen($apiKey) > 0 && strlen($clientId) > 0))){
					//Save settings
					//todo: further validation of the credentials...
					Settings::save('googledrive_use_picker', $usePicker);
					if($usePicker) {
						Settings::save('googledrive_api_key', $apiKey);
						Settings::save('googledrive_client_id', $clientId);
					}
					Yii::app()->user->setFlash('success', Yii::t('standard', '_OPERATION_SUCCESSFUL'));
				} else{
					Yii::app()->user->setFlash('error', Yii::t('googledrive', 'Please provide both your API key and Client id.'));
					//$this->redirect(Docebo::createAdminUrl('GoogleDriveApp/googleDriveApp/settings'));
				}
			}
		}

		$this->render('settings', array(
			'usePicker' => $usePicker,
			'apiKey'    => $apiKey,
			'clientId'  => $clientId
		));
	}

	public function actionResetConfig(){
		$confirmYes = Yii::app()->request->getParam('confirm_yes', null);

		if ( $confirmYes === 'Yes') {
			Settings::save('googledrive_use_picker', 0);
			Settings::save('googledrive_api_key', '');
			Settings::save('googledrive_client_id', '');

			echo '<a class="auto-close"></a>';

			$redirectUrl = Docebo::createAdminUrl('GoogleDriveApp/googleDriveApp/settings');
			$this->renderPartial('lms.protected.views.site._js_redirect', array(
				'url'           => $redirectUrl,
				'showMessage'   => false
			));

			Yii::app()->end();
		}

		$this->renderPartial('_reset_config', array(),false, true);
		Yii::app()->end();
	}

	public function actionAxEditGoogleDrive(){
		$googleDriveObj = new LearningGoogledoc();

		$idResource = intval(Yii::app()->request->getParam("idResource", null));
		$idCourse = Yii::app()->request->getParam('course_id', false);
		$courseModel = LearningCourse::model()->findByPk($idCourse);

		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile(Yii::app()->baseUrl.'/../plugins/GoogleDriveApp/assets/js/filepicker.js');

		$learningObject = null;
		if ($idResource > 0) {
			$learningObject = LearningOrganization::model()->findByAttributes(array('idOrg' => $idResource, 'objectType' => LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE));
			if ($learningObject) {
				// get object
				$googleDriveObj = LearningGoogledoc::model()->findByPk($learningObject->idResource);
			}
		}

		$usePicker = Settings::get('googledrive_use_picker', 0);

		if(empty($googleDriveObj->idPage) && $usePicker){
			$googleDriveObj->import_type = LearningGoogledoc::GOOGLEDRIVE_IMPORTTYPE_IMPORT;
		}

		$html = $this->renderPartial('_edit_googledrive_lo', array(
			'centralRepoContext' => false,
			'googleDriveObj' => $googleDriveObj,
			'loModel'=>$learningObject,
			'courseModel'=>$courseModel,
			'lo_type'=>LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE,
			'usePicker' => $usePicker,
			'apiKey' => Settings::get('googledrive_api_key', false),
			'clientId' => Settings::get('googledrive_client_id', false),
		), true, true);

		$response = new AjaxResult();
		$response->setStatus(true)->setHtml($html)->toJSON();
		Yii::app()->end(); // End !!!
	}

	public function actionEditCentralRepo(){
		$googleDriveObj = new LearningGoogledoc();

		$idResource = intval(Yii::app()->request->getParam("id_object", null));

		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile(Yii::app()->baseUrl.'/../plugins/GoogleDriveApp/assets/js/filepicker.js');

		Yii::app()->tinymce->init();

		Yii::app()->getModule('centralrepo')->registerResources();
		$cs = Yii::app()->getClientScript();
		$playerAssetsUrl = Yii::app()->findModule('player')->assetsUrl;
		$cs->registerCssFile ($playerAssetsUrl.'/css/base.css');
		$cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/admin.css');

		$centralLoObject = null;
		$loModel = null;
		if ($idResource > 0) {
			$centralLoObject = LearningRepositoryObjectVersion::model()->findByAttributes(array('id_object' => $idResource, 'object_type' => LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE));
			$loModel = LearningRepositoryObject::model()->findByAttributes(array('id_object' => $idResource, 'object_type' => LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE));
			if ($centralLoObject) {
				// get object
				$googleDriveObj = LearningGoogledoc::model()->findByPk($centralLoObject->id_resource);
			}
		}

		$usePicker = Settings::get('googledrive_use_picker', 0);

		if(empty($googleDriveObj->idPage) && $usePicker){
			$googleDriveObj->import_type = LearningGoogledoc::GOOGLEDRIVE_IMPORTTYPE_IMPORT;
		}

		$this->render('_edit_googledrive_lo', array(
			'centralRepoContext' => true,
			'googleDriveObj' => $googleDriveObj,
			'lo_type'=>LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE,
			'loModel' => $loModel,
			'containerId' => 'player-centralrepo-uploader',
			'saveUrl' => Docebo::createAbsoluteLmsUrl('GoogleDriveApp/GoogleDriveApp/saveCentralRepo'),
			'backButtonUrl' => Docebo::createAbsoluteLmsUrl('centralrepo/centralRepo/index'),
			'usePicker' => $usePicker,
			'apiKey' => Settings::get('googledrive_api_key', false),
			'clientId' => Settings::get('googledrive_client_id', false),
		), false);
	}

	public function actionAxSaveLo(){
		$thumb = Yii::app()->request->getParam("thumb", null);
		$shortDescription = urldecode(Yii::app()->request->getParam("short_description", ''));
		$wrongType = intval(Yii::app()->request->getParam("wrong_type", null));

		$response = new AjaxResult();
		$transaction = Yii::app()->db->beginTransaction();

		try {
			$postData = Yii::app()->request->getParam('LearningGoogledoc', null);
			$postData['title'] = trim($postData['title']);
			$postData['url'] = $this->normalizeGoogleDriveURL($postData['url']);
			$postData['url2'] = $this->normalizeGoogleDriveURL($postData['url2']);
			$postData['description'] = Yii::app()->htmlpurifier->purify($postData['description'], 'allowFrameAndTargetLo');
			$postData['idPage'] = intval($postData['idPage']);

			$courseData = Yii::app()->request->getParam('LearningCourse', null);
			$courseId = $courseData['idCourse'];

			// Throw an error when a wrong filetype has been chosen with the google picker
			if($postData['import_type'] == LearningGoogledoc::GOOGLEDRIVE_IMPORTTYPE_IMPORT && $wrongType == 1){
				throw new CException(Yii::t('googledrive', 'Invalid Google Drive document type! Please select only documents, spreadsheets or presentations.'));
			}

			// Validate input parameters
			if (!$courseId) { throw new CException("Invalid specified course."); }
			if (empty($postData['title'])) { throw new CException(Yii::t('error', 'Enter a title!')); }
			if (!$this->validateGoogleDriveURL($postData['url']) && !$this->validateGoogleDriveURL($postData['url2'])) { throw new CException(Yii::t('googledrive', 'Invalid Google Drive URL!')); }

			$learningObject = null;

			if ($postData['idPage'] > 0) { // existing record --> edit
				$learningObject = LearningOrganization::model()->findByAttributes(array(
					'objectType' => LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE,
					'idResource' => $postData['idPage'],
					'idCourse' => $courseId
				));
				$googleDriveObj = LearningGoogledoc::model()->findByPk($postData['idPage']);
			} else { // new record --> create
				$googleDriveObj = new LearningGoogledoc();
				$googleDriveObj->author = Yii::app()->user->getIdst();
			}

			$googleDriveObj->import_type = $postData['import_type'];
			$googleDriveObj->title = $postData['title'];
			$googleDriveObj->url = ($googleDriveObj->import_type == LearningGoogledoc::GOOGLEDRIVE_IMPORTTYPE_EMBED)?$postData['url']:$postData['url2'];
			$googleDriveObj->description = $postData['description'];

			//do actual saving
			if (!$googleDriveObj->save()) {
				$errorsGoogleDrive = $googleDriveObj->getErrors();

				$returnErrors = Yii::t('googledrive', 'Error while saving Google Drive object'). ': <br /><ul class="customErrorSummary">';
				foreach($errorsGoogleDrive as $errorsGoogleDriveField) {
					if (is_array($errorsGoogleDriveField))  {
						foreach($errorsGoogleDriveField as $errorGoogleDrive) {
							$returnErrors .= '<li>' . $errorGoogleDrive . '</li>';
						}
					}
				}

				$returnErrors .= '</ul>';

				throw new CException($returnErrors);
			}

			if($learningObject){
				$learningObject->title = $googleDriveObj->title;
				$learningObject->short_description = Yii::app()->htmlpurifier->purify($shortDescription);
				if($thumb) $learningObject->resource = intval($thumb);
				$learningObject->saveNode();
			} else { // no record exists yet, so we've got to push a new one
				$loParams = array(
					'short_description' => $shortDescription
				);
				if($thumb) $loParams['resource'] = $thumb;
				$parentModel = LearningOrganization::model()->getRoot($courseId);

				$loManager = new LearningObjectsManager($courseId);
				$learningObject = $loManager->addLearningObject(LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE, $googleDriveObj->idPage, $postData['title'], $parentModel->idOrg, $loParams);
			}

			$learningObject = LearningOrganization::model()->findByAttributes(array(
				'idResource' => $googleDriveObj->idPage,
				'objectType' => LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE,
				'idCourse' => $courseId
			));
			if (!$learningObject) { throw new CException(Yii::t('course', 'Invalid learning object'). "(".$googleDriveObj->idPage.")"); }

			// DB Commit
			$transaction->commit();

			// Trigger event to let plugins save their own data from POST
			Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
				'loModel' => $learningObject
			)));

			$data = array(
				'loModel' => $learningObject,
				'courseId' => $courseId,
				'resourceId' => $learningObject->idResource,
				'organizationId' => (int)$learningObject->getPrimaryKey(),
				'parentId' => (int)$learningObject->idParent,
				'title' => $googleDriveObj->title,
				'message' => Yii::t('standard', '_OPERATION_SUCCESSFUL')
			);
			$response->setData($data)->setStatus(true);

		} catch (CException $e) {
			$transaction->rollback();
			$response->setMessage($e->getMessage())->setStatus(false);
		}

		$response->toJSON();
		Yii::app()->end();
	}

	public function actionSaveCentralRepo(){
		$thumb = Yii::app()->request->getParam("thumb", null);
		$shortDescription = urldecode(Yii::app()->request->getParam("short_description", ''));
		$wrongType = intval(Yii::app()->request->getParam("wrong_type", null));

		$response = new AjaxResult();
		$transaction = Yii::app()->db->beginTransaction();

		try {
			$postData = Yii::app()->request->getParam('LearningGoogledoc', null);
			$postData['title'] = trim($postData['title']);
			$postData['url'] = $this->normalizeGoogleDriveURL($postData['url']);
			$postData['url2'] = $this->normalizeGoogleDriveURL($postData['url2']);
			$postData['description'] = Yii::app()->htmlpurifier->purify($postData['description'], 'allowFrameAndTargetLo');
			$postData['idPage'] = intval($postData['idPage']);

			// Throw an error when a wrong filetype has been chosen with the google picker
			if($postData['import_type'] == LearningGoogledoc::GOOGLEDRIVE_IMPORTTYPE_IMPORT && $wrongType == 1){
				throw new CException(Yii::t('googledrive', 'Invalid Google Drive document type! Please select only documents, spreadsheets or presentations.'));
			}

			// Validate input parameters
			if (empty($postData['title'])) { throw new CException(Yii::t('error', 'Enter a title!')); }
			if (!$this->validateGoogleDriveURL($postData['url']) && !$this->validateGoogleDriveURL($postData['url2'])) { throw new CException(Yii::t('googledrive', 'Invalid Google Drive URL!')); }

			$repoVersion = null;

			if ($postData['idPage'] > 0) { // existing record --> edit
				$googleDriveObj = LearningGoogledoc::model()->findByPk($postData['idPage']);
				$repoVersion = LearningRepositoryObjectVersion::model()->findByAttributes(array(
					'id_resource' => $postData['idPage'],
					'object_type' => LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE
				));
			} else { // new record --> create
				$googleDriveObj = new LearningGoogledoc();
				$googleDriveObj->author = Yii::app()->user->getIdst();
			}

			$googleDriveObj->detachBehavior('learningObjects');

			$googleDriveObj->import_type = $postData['import_type'];
			$googleDriveObj->title = $postData['title'];
			$googleDriveObj->url = ($googleDriveObj->import_type == LearningGoogledoc::GOOGLEDRIVE_IMPORTTYPE_EMBED)?$postData['url']:$postData['url2'];
			$googleDriveObj->description = $postData['description'];

			//do actual saving
			if (!$googleDriveObj->save()) {
				$errorsGoogleDrive = $googleDriveObj->getErrors();

				$returnErrors = Yii::t('googledrive', 'Error while saving Google Drive object'). ': <br /><ul class="customErrorSummary">';
				foreach($errorsGoogleDrive as $errorsGoogleDriveField) {
					if (is_array($errorsGoogleDriveField))  {
						foreach($errorsGoogleDriveField as $errorGoogleDrive) {
							$returnErrors .= '<li>' . $errorGoogleDrive . '</li>';
						}
					}
				}

				$returnErrors .= '</ul>';

				throw new CException($returnErrors);
			}

			if(!empty($repoVersion)){
				$repoObject = LearningRepositoryObject::model()->findByPk($repoVersion->id_object);
				$repoObject->title = $googleDriveObj->title;
				$repoObject->short_description = Yii::app()->htmlpurifier->purify($shortDescription);
				$repoObject->resource = intval($thumb);

				if(!$repoObject->save()){
					Log::_($repoObject->getErrors());
				}
			} else{
				$repoObject = new LearningRepositoryObject();
				$repoObject->title = $postData['title'];
				$repoObject->short_description = Yii::app()->htmlpurifier->purify($shortDescription);
				$repoObject->resource = intval($thumb);
				$categoryNodeId = LearningCourseCategoryTree::getRootNodeId();
				if(isset(Yii::app()->session['currentCentralRepoNodeId'])){
					$categoryNodeId = Yii::app()->session['currentCentralRepoNodeId'];
				}
				$repoObject->id_category = $categoryNodeId;
				$repoObject->params_json = CJSON::encode(array());
				$repoObject->object_type = LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE;

				if($repoObject->save()){
					$versionModel = new LearningRepositoryObjectVersion();
					$versionModel->id_resource = $googleDriveObj->idPage;
					$versionModel->object_type = LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE;
					$versionModel->version = 1;
					$versionModel->id_object = $repoObject->id_object;
					$versionModel->version_name = 'V1';
					$versionModel->version_description = '';
					$userModel = Yii::app()->user->loadUserModel();
					/**
					 * @var $userModel CoreUser
					 */
					$authorData = array(
						'idUser' => $userModel->idst,
						'username' => $userModel->getUserNameFormatted()
					);
					$versionModel->author = CJSON::encode($authorData);
					$versionModel->create_date = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');

					if(!$versionModel->save()){
						Log::_($versionModel->getErrors());
					}

				} else{
					Log::_($repoObject->getErrors());
				}
			}

			// DB Commit
			$transaction->commit();

			$data = array(
				'message' => Yii::t('standard', '_OPERATION_SUCCESSFUL')
			);

			$response->setData($data)->setStatus(true);
		} catch (CException $e) {
			Log::_($e->getMessage());
			$transaction->rollback();
			$response->setMessage($e->getMessage())->setStatus(false);
		}

		$response->toJSON();
		Yii::app()->end();
	}

	public function actionShow(){
		$id_object = Yii::app()->request->getParam("id_object", null);
		$id_course = Yii::app()->request->getParam("course_id", null);

		$transaction = Yii::app()->db->beginTransaction();

		try {
			$idObject = $id_object;
			$object = LearningOrganization::model()->findByPk($idObject);
			if (!$object || $object->objectType != LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE) {
				throw new CException('Invalid object ID: '.$idObject);
			}

			// Learning Object ID must match the course ($this->course_id is an ID set by PlayerBaseController... always)
			if ($object->idCourse != $id_course) {
				throw new CException('Learning Object does not match the course. Are you a hacker or something.... ?');
			}

			//now load GoogleDrive specific information
			$idDocument = (int)$object->idResource;
			$googleDocument = LearningGoogledoc::model()->findByPk($idDocument);
			if (!$googleDocument) {
				throw new CException('Invalid page ID: '.$idDocument);
			}

			// Track the page: GoogleDrive document is considered "completed" when it is just loaded
			$idUser = Yii::app()->user->id;
			$object = $object->getMasterForCentralLo($idUser);

			$track = LearningMaterialsTrack::model()->findByAttributes(array('idReference' => $object->getPrimaryKey(), 'idUser' => $idUser));
			if (!$track) {
				//the object has not been tracked yet for this user: create the record now
				$track = new LearningMaterialsTrack();
				$track->idUser = (int)$idUser;
				$track->idReference = (int)$object->getPrimaryKey();
				$track->idResource = (int)$googleDocument->getPrimaryKey();
			}
			$track->setStatus(LearningCommontrack::STATUS_COMPLETED);
			$track->setObjectType(LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE);
			if (!$track->save()) {
				throw new CException("Error while saving track data");
			}
			UserLimit::logActiveUserAccess($idUser);

			$validation = LearningGoogledoc::validateGoogleDriveUrl($googleDocument->url);

			//load content
			$html = $this->renderPartial("_show", array(
				'document' => $googleDocument,
				'idReference' => $idObject,
				'object' => $object,
				'error' => $validation
			), true);

			//finish action
			$transaction->commit();

			echo $html;
			Yii::app()->end();


		} catch (CException $e) {
			$transaction->rollback();
			echo $e->getMessage();
		}
	}

	/**
	 * @param string $url
	 * @return string
	 */
	public function normalizeGoogleDriveURL($url){
		$url = trim($url);
		preg_match(self::GOOGLE_DRIVE_URL_REGEX_PATTERN, $url, $matches);
		switch($matches[3]){
			case 'presentation':
				if($matches[5] == '/pub'){ // Google drive spreadsheets won't show in an I-frame when it's only published, we need the 'embed' url
					$url = str_replace('/pub', '/embed', $url);
				}
				return $url;
			case 'document':
			case 'spreadsheets':
			default:
				return $url;
		}
	}

	/**
	 * @param string $url eg: https://docs.google.com/document/d/aBc123dEf456GhI789/pub
	 * @return bool
	 */
	public function validateGoogleDriveURL($url){
		return preg_match(self::GOOGLE_DRIVE_URL_REGEX_PATTERN, $url);
	}
}