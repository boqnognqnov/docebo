<?php

/**
 * Class ZendeskAppModule
 *
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */
class ZendeskAppModule extends CWebModule {
	public function init () {

		Yii::app()->event->on( 'BeforeBodyTagClose', array( $this, 'beforeBodyClose' ) );
	}

	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu () {

		Yii::app()->mainmenu->addAppMenu( 'zendesk', array(
			'icon'     => 'admin-ico ecommerce',
			'app_icon' => 'fa-comments-o', // icon used in the new menu
			'link'     => Docebo::createAdminUrl( '//ZendeskApp/ZendeskApp/settings' ),
			'label'    => 'Zendesk'
		), array() );
	}

	public function beforeBodyClose ( DEvent $event ) {

		// Only logged in users see the widget
		if(Yii::app()->user->isGuest) return;

		// Widget should only be echoed on full-page requests, not ajax
		if(Yii::app()->getRequest()->getIsAjaxRequest()) return;

		if( Settings::get( 'zendesk_enabled' ) ) {

			$_controller = Yii::app()->getController()->getId();
			$_action = Yii::app()->getController()->getAction()->getId();

			$isDashboard = $_controller==='dash';
			$isPlayer = Yii::app()->getController() && Yii::app()->getController()->getModule() && Yii::app()->getController()->getModule()->getName() == 'player';
			$isCourseDetailsPage = $_controller=='course' && $_action=='details';

			if(!$isDashboard && !$isCourseDetailsPage) return;

			$script = Settings::get( 'zendesk_js_code' );

			if( stripos( $script, '<script' ) === FALSE ) {
				echo '<script type="text/javascript">/*<![CDATA[*/' . PHP_EOL;
				echo '/* Zendesk JS code */' . PHP_EOL;
				echo $script;
				echo PHP_EOL . '/*]]>*/</script>';
			} else {
				echo $script;
			}

		}
	}
}
