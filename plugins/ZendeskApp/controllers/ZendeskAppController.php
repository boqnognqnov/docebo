<?php

/**
 * Class ZendeskAppController
 *
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */
class ZendeskAppController extends Controller {
	/**
	 * @return array action filters
	 */
	public function filters () {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules () {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		$admin_only_actions = array( 'settings' );
		$res[]              = array(
			'allow',
			'roles'   => array( Yii::app()->user->level_godadmin ),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array( '@' ),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array( '*' ),
		);

		return $res;
	}

	public function actionSettings(){
		$settings = new ZendeskAppSettingsForm();

		$settings->js_code = Settings::get('zendesk_js_code');
		$settings->enabled = Settings::get('zendesk_enabled');

		if (isset($_POST['ZendeskAppSettingsForm'])) {
			$settings->setAttributes($_POST['ZendeskAppSettingsForm']);

			if ($settings->validate()) {

				Settings::save('zendesk_js_code', $settings->js_code);
				Settings::save('zendesk_enabled', (bool) $settings->enabled);

				Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));
			}
			else {
				Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
			}
		}

		Yii::app()->clientScript->registerCss('zendesk-settings-form', '
			#zendesk-settings-form.advanced-main {
				margin-left: 0;
			}
			#zendesk-settings-form .form-actions .btn-docebo{
				float: right;
				margin-left: 20px;
			}
		');
		Yii::app()->clientScript->registerScript('zendesk-settings-form', '
			$("input,select", "#zendesk-settings-form").styler();
		');
		$this->render('settings', array(
			'settings'=>$settings,
		));
	}
}