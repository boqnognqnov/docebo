<?php

/**
 * Plugin for ZendeskApp
 *
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 *
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */

class ZendeskApp extends DoceboPlugin {


	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'ZendeskApp';


	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {
		Settings::save('zendesk_enabled', '1');
		parent::activate();
	}


	public function deactivate() {
		Settings::save('zendesk_enabled', '0');
		parent::deactivate();
	}

}
