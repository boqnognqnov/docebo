<?php

/**
 * ZendeskAppSettingsForm class.
 * @property $js_code
 * @property $enabled boolean
 *
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */
class ZendeskAppSettingsForm extends CFormModel {
	public $js_code;
	public $enabled = FALSE;

	/**
	 * Declares the validation rules.
	 */
	public function rules () {
		return array(
			array( 'js_code, enabled', 'safe' ),
		);
	}


	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels () {
		return array(
			'js_code' => Yii::t( 'ZendeskApp', 'Zendesk JS code' ),
			'enabled' => Yii::t( 'configuration', 'Enabled' ),
		);
	}
}
