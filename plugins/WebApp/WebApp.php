<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 25.1.2016 г.
 * Time: 11:16 ч.
 */

class WebApp extends DoceboPlugin {

    // Name of the plugin; needed to identify plugin folder and entry script
    public $plugin_name = 'WebApp';

    /**
     * This will allow us to access "statically" (singleton) to plugin methods
     * @param string $class_name
     * @return Plugin Object
     */
    public static function plugin($class_name = __CLASS__) {
        return parent::plugin($class_name);
    }

    /**
     * a URL for the settings page of the plugin
     * @return mixed
     */
    public static function settingsUrl()
    {
        return Docebo::createAdminUrl('mobile/settings');
    }

    /**
     * Activate the plugin with SQL /if needed/
     */
    public function activate() {
        parent::activate();
        // nothing to do

    }

    /**
     * Deactivate the plugin
     */
    public function deactivate() {
        parent::deactivate();
        // nothing to do
    }
}
