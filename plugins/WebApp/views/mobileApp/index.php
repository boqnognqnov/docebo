<?php
$widgetIsMultiDomain = false;
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 25.1.2016 г.
 * Time: 17:42 ч.
 */
/*** Create the breadcrumbs */
if(!isset($isMultidomain)){
	$this->breadcrumbs = array(
		Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
		Yii::t('mobile', 'Mobile App Settings')
	);
}else{
?>
	<div class="tab-pane" id="mobileAppSettings">
<?php 
	$widgetIsMultiDomain = true;
}
/*** Call The MobileAppSettings widget which holds the content */
echo $this->widget('common.widgets.MobileAppSettings', array(
		'action'    => $action,
		'primaryColor' => $mobileAppSettings['primaryColor'],
		'loginColor' => $mobileAppSettings['loginColor'],
		'logo' => $mobileAppSettings['logo'],
		'isMultidomain' => $widgetIsMultiDomain,
        'isEnabled'     => $isEnabled
	), true);
if(isset($isMultidomain)){
?>
	</div>
<?php
}