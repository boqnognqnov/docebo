<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 25.1.2016 г.
 * Time: 11:17 ч.
 */

class WebAppModule extends CWebModule
{
    /**
     * URL to this module's assets (web accessible)
     *
     * @var string
     */
    protected $_assetsUrl = null;

    public function init()
    {
        // import the module-level models and components
        $this->setImport(array(
            'WebApp.models.*',
            'WebApp.components.*',
        ));

        /** Load Assets **/
        if(Yii::app() instanceof CWebApplication){
            if(Yii::app()->name !== 'api') {
                Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl . '/../plugins/WebApp/assets/css/mobile.css');
            }
        }

		if(PluginManager::isPluginActive('MultidomainApp')){
			Yii::app()->event->on("RenderDomainBrandingTabs", array($this, 'renderDomainBrandingTabs'));
			Yii::app()->event->on("RenderDomainBrandingTabsContent", array($this, 'renderDomainBrandingTabsContent'));
			Yii::app()->event->on("DomainBrandingSettingsFormSubmit", array($this, 'saveMultidomainBranding'));
		}
    }


    /**
     * @return string
     */
    public function getAssetsUrl()
    {
        if ($this->_assetsUrl === null) {
            $this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('WebApp.assets'));
        }
        return $this->_assetsUrl;
    }

    /**
     * @param CController $controller
     * @param CAction $action
     * @return bool
     *
     * @see CWebModule::beforeControllerAction()
     */
    public function beforeControllerAction($controller, $action)
    {
        if(parent::beforeControllerAction($controller, $action))
        {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        }
        else
            return false;
    }
	
	/**
	 * Adds the tab to the multidomain app
	 * @param $event DEvent
	 */
	public function renderDomainBrandingTabs($event) {
		Yii::app()->getController()->renderPartial('WebApp.views.mobileApp.partial._multidomain_tab');
	}

	/**
	 * Renders the saml tab in multidomain client config
	 * @param $event DEvent
	 */
	public function renderDomainBrandingTabsContent($event) {
		$idMultidomain = $event->params['multidomain_client_id'];
		$settings = MobileAppMultiDomain::model()->findByAttributes(array('idMultidomain' => $idMultidomain));
		if( is_null($settings) ){
            $isEnabled      = false;
			$loginColor     = PluginSettings::get('login_page_background', 'WebApp');
			$primaryColor   = PluginSettings::get('main_app_color', 'WebApp');
			$logo           = PluginSettings::get('app_logo', 'WebApp');
		} else {
            $isEnabled      = true;
			$loginColor     = $settings->login_color;
			$primaryColor   = $settings->primary_color;
			$logo           = $settings->logo;
			if($logo == ""){
				$logo = PluginSettings::get('app_logo', 'WebApp');
			}
		}
		Yii::app()->clientScript->registerCssFile('../webapp/css/webapp-font.css');
        Yii::app()->clientScript->registerCssFile('js/farbtastic/farbtastic.css');
        Yii::app()->clientScript->registerScriptFile('js/farbtastic/farbtastic.min.js');
		Yii::app()->getController()->renderPartial('WebApp.views.mobileApp.index',array(
			'mobileAppSettings' => array(
				'loginColor' => $loginColor,
				'primaryColor' => $primaryColor,
				'logo' => $logo
			),
			'isMultidomain' => true,
			'action' => Docebo::createAdminUrl('MultidomainApp/main/settings', array('id' => $idMultidomain)),
            'isEnabled' => $isEnabled
		));
	}
	
	public function saveMultidomainBranding($event){
		$coreMultidomainParams = Yii::app()->request->getParam('CoreMultidomain');
		if((bool)$coreMultidomainParams['isMobileAppSettings'] != true){
			return;
		}
		$idMultidomain = $event->params['multidomain_client_id'];
		$payload = Yii::app()->request;
		$enabled = $payload->getParam('isEnabled');
		$model = MobileAppMultiDomain::model()->findByAttributes(array('idMultidomain' => $idMultidomain));
		$message = true;
		$deleteCondition = is_null($enabled) && !is_null($model);
		// @TODO Move most of the logic in model before save!!!!
		if($deleteCondition){
			$model->delete();
			return;
		}
		if(is_null($model)){
			$model = new MobileAppMultiDomain();
			$model->idMultidomain = $idMultidomain;
		}
		$logo = $payload->getParam('uploadedFile');
		$model->logo = $payload->getParam('currentFile');
		if($logo != ""){
			$message = MobileAppModel::validateImage($logo);
			if($message === true){
				MobileAppModel::moveLogoToS3($logo, $oldLogo);
				$model->logo = $logo;
			}
		}
		$type = 'error';
		$responseMessage = $message;
		if($message == true){
			$responseMessage = Yii::t('mobile', 'Settings saved successfully');
			$type = 'success';
			if(!is_null($enabled)){
				$model->login_color = $payload->getParam('login_page_color');
				$model->primary_color = $payload->getParam('primary_color');
				$model->save();
			}
		}
		Yii::app()->user->setFlash($type, $responseMessage);
	}
}