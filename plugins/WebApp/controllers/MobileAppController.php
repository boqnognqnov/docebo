<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 25.1.2016 г.
 * Time: 12:05 ч.
 */

class MobileAppController extends Controller
{
    /**
     * @param CAction $action
     * @return bool
     */
    public function beforeAction( $action ) {

        JsTrans::addCategories('mobile');

        return parent::beforeAction( $action );
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }



    public function actionIndex() {
        Yii::app()->clientScript->registerCssFile('../webapp/css/webapp-font.css');
        Yii::app()->clientScript->registerCssFile('js/farbtastic/farbtastic.css');
        Yii::app()->clientScript->registerScriptFile('js/farbtastic/farbtastic.min.js');
        $this->render('index', array('mobileAppSettings' => array(
			'loginColor' => PluginSettings::get('login_page_background', 'WebApp'),
			'primaryColor' => PluginSettings::get('main_app_color', 'WebApp'),
			'logo' => PluginSettings::get('app_logo', 'WebApp')
		), 'action' => '/admin/index.php?r=WebApp/MobileApp/saveSettings'));
    }
	
	public function actionSaveSettings(){
		$appName = MobileAppModel::PLUGIN_APP_NAME;
		$request = Yii::app()->request;
		$loginColor = $request->getParam('login_page_color');
		$primaryColor = $request->getParam('primary_color');
		$logo = $request->getParam('uploadedFile');
		$oldLogo = $request->getParam('currentFile');
		$message = true;
		if($logo != ""){
			$message = MobileAppModel::validateImage($logo);
			if($message === true){
				MobileAppModel::moveLogoToS3($logo, $oldLogo);
				PluginSettings::save('app_logo', $logo, $appName);
			}
		}
		if($message === true){
			$status = true;
			$responseMessage = Yii::t('mobile', 'Settings saved successfully');
			PluginSettings::save('login_page_background', $loginColor, $appName);
			PluginSettings::save('main_app_color', $primaryColor, $appName);
		}else{
			$status = false;
			$responseMessage = $message;
		}
		$result = new AjaxResult();
		$result->setStatus($status)->setMessage($responseMessage)->toJSON();
	}



}
