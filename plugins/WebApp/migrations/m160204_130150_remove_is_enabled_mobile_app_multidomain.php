<?php
/**
 * Yii DB Migration template.
 * 
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 * 
 */
class m160204_130150_remove_is_enabled_mobile_app_multidomain extends DoceboDbMigration {

	public function safeUp()
	{
		// PUT YOUR MIGRATION-UP CODE HERE
		// DO NOT USE try/catch or DB transactions!
		
		// Internally, this method call is wrapped in a try/catch block and DB transaction.
		// You can throw exceptions here to be catched internally.
		// If you wish to cancel the migration, return false
		$this->dropColumn('mobileapp_multidomain', 'isEnabled');
		return true;
	}

	public function safeDown()
	{
		// PUT YOUR MIGRATION-DOWN CODE HERE
		// DO NOT USE try/catch or DB transactions!
		
		// Internally, this method call is wrapped in a try/catch block and DB transaction
		// You can throw exceptions here to be catched internally.
		// If you wish to cancel the migration, return false
		return true;
	}
	
	
}
