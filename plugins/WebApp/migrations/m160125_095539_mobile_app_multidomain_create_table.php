<?php
/**
 * Yii DB Migration template.
 * 
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 * 
 */
class m160125_095539_mobile_app_multidomain_create_table extends DoceboDbMigration {

	public function safeUp()
	{
		// PUT YOUR MIGRATION-UP CODE HERE
		// DO NOT USE try/catch or DB transactions!
		$this->createTable('mobileapp_multidomain', array(
			'id' => 'pk',
			'idMultidomain' => 'int NOT NULL',
			'login_color' => 'string NOT NULL',
			'primary_color' => 'string NOT NULL',
			'logo' => 'string NOT NULL'
		));
		$this->addForeignKey('fk_mobile_app_multi_domain', 'mobileapp_multidomain', 'idMultidomain', 'core_multidomain', 'id', 'CASCADE', 'CASCADE');
		// Internally, this method call is wrapped in a try/catch block and DB transaction.
		// You can throw exceptions here to be catched internally.
		// If you wish to cancel the migration, return false
		return true;
	}

	public function safeDown()
	{
		// PUT YOUR MIGRATION-DOWN CODE HERE
		// DO NOT USE try/catch or DB transactions!
		
		// Internally, this method call is wrapped in a try/catch block and DB transaction
		// You can throw exceptions here to be catched internally.
		// If you wish to cancel the migration, return false
		return true;
	}
	
	
}
