/* global MultidomainManClass */

angular.module('ngSettingsApp', [
  'ngSettingsApp.controllers'
]);
angular.module('ngSettingsApp.controllers', []).
		controller('settingsController', function($scope) {
		/* 
			* To change this license header, choose License Headers in Project Properties.
			* To change this template file, choose Tools | Templates
			* and open the template in the editor.
		*/
	   $(function() {
		   var allFilesSize = 0; //Current files size;
		   var maxFilesSize = 1024 * 1024; //10MB
		   var upUrl = '/lms/index.php?r=site/axUploadFile';
		   
		   var uploader = new plupload.Uploader({
				chunk_size: '1mb',
				multi_selection:  false,
				runtimes: 'html5,flash',
				browse_button: 'pickfiles',
				container: 'container',
				max_file_size: '1024mb',
				url: upUrl,
				multipart: true,
				unique_names: true,
				flash_swf_url: yii.urls.base + '/../common/extensions/plupload/assets/plupload.flash.swf',
				silverlight_xap_url: '/plupload/js/plupload.silverlight.xap',
				multipart_params: {
                   YII_CSRF_TOKEN: angular.element('input[name=YII_CSRF_TOKEN]').val(),
                   fileId: ''
				}
		   });

           uploader.bind('Init', function (up, params) {
               //$('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
           });

           uploader.init();

           // Just before uploading file
           uploader.bind('BeforeUpload', function (up, file) {
               Docebo.log('BeforeUpload');
           });

           // Handle 'files added' event
           uploader.bind('FilesAdded', function (up, files) {
               this.files = [files[0]];
			    var file = jQuery('#'+uploader.id+'_html5').get(0).files[0]
				var allowedExtensions = ['jpg', 'jpeg', 'png', 'bmp'];
				var extension = file.name.replace(/^.*\./, '').toLowerCase();
				if(allowedExtensions.indexOf(extension) == -1){
					Docebo.Feedback.show('error', 'Invalid file type allowed are '+allowedExtensions.join(', '), true, true);
					return;
				}
				var reader = new FileReader();
				reader.onload = function(){
					document.getElementById('logo-preview').src = reader.result
					document.getElementById('logo_preview_img').src = reader.result
				};
				reader.readAsDataURL(file);
				$('#ngAppSettings #saveButton').attr('disabled', true);
               uploader.start();
           });
		   
		   
		   uploader.bind('FileUploaded', function (up, file, info) {
				var uploaded = document.getElementById('uploadedFile');
				uploaded.value = file.target_name;
				$('#ngAppSettings #saveButton').attr('disabled', false);
			});
			if(typeof MultidomainManClass != 'undefined'){
				$('#isEnabled-styler').click(function(){
					$scope.enableEdit();
				});
				$scope.enableEdit();
			}
	   });




	   /***
		* Color Picker
		*/
	   // Init the color picker tooltip
	   $('.color-tooltip').tooltip({
		   title: function() {
			   return $(this).closest('.wrap').find('.farbtastic-tooltip').html();
		   },
		   trigger:  'focus',
		   html: true
	   });

	   // Init farbtastic picker
	   $('.color-tooltip').on('shown.bs.tooltip', function() {
		   // Add color picker
		   $('.colors .color-item input').each(function() {
			   var _this = $(this);
			   var _preview = $(this).parents('.wrap').find('.color-preview');

			   var f = $.farbtastic($(this).parents('.wrap').find('.colorpicker'), function(data) {
				   _this.val(data);
				   $scope.$apply(function(){
					  $scope[_this.attr('ng-model')] = data;
				   });
				   _preview.css('background-color', data);
			   });

			   // Initializes colorpicker color
			   f.setColor($(this).val());

			   $(this).on('keyup', farbtasticUpdateCallback(f, $(this)));
		   });
	   });

	   $('.colors').on('click', '.color-preview', function() {
		   $(this).parents('.wrap').find('input').focus();
	   });

		$scope.contrastColor = function( hex ) {

			hex = hex.substr(1);
			return (parseInt(hex, 16) > 0xffffff/2) ? '#000000' : '#ffffff';
		};

		$scope.loginPageColor = mobileAppSettings.loginColor;
		$scope.primaryColor = mobileAppSettings.primaryColor;

		$scope.$watch('loginPageColor', function(nv){
			document.getElementById('login-preview').style.backgroundColor = nv;
			angular.element(".preview-username, .preview-password").css('border-color', $scope.contrastColor( nv ));
			angular.element(".icon-user-mobile, .icon-lock-mobile").css('color', $scope.contrastColor( nv ));
		});
		$scope.$watch('primaryColor', function(nv){
			document.getElementById('colored-header').style.backgroundColor = nv;
			angular.element("#hamburger-menu, #search").css('color', $scope.contrastColor( nv ));
		});
		if($('.multidomain-client-settings').length === 0){
			$('#mobileAppSaveSettings').on('submit', function(e){
				e.preventDefault();
				var formData = $('form#mobileAppSaveSettings').serializeArray();
				var cleanFormData = {};
				for(var i=0;i<formData.length;i++){
					var toPush = formData[i];
					cleanFormData[toPush.name] = toPush.value;
					delete toPush.name;
					delete formData[i];
				}
				formData = null;
				$.ajax({
					url: $('#mobileAppSaveSettings').attr('action'),
					data: cleanFormData,
					method: 'POST',
					dataType: 'json',
					success: function(data){
						if(data.success === true){
							Docebo.Feedback.show('success', data.message, true, true);
							var uploaded = document.getElementById('uploadedFile');
							uploaded.value = "";
						}else{
							Docebo.Feedback.show('error', data.message, true, true);
						}
					}
				});
			});
		}else{
			$('#mobileAppSaveSettings').append('<input type="hidden" name="save_multidomain_settings" value="true" />');
			$('#mobileAppSaveSettings').append('<input type="hidden" name="CoreMultidomain[isMobileAppSettings]" value="true" />');
		}
		if(typeof window['multidomainman'] != 'undefined'){
			$scope.enableEdit = function(){
				if ($('input[name="isEnabled"]').is(':checked')) {
					$('#mobileAppSaveSettings .blockable').unblock();
				} else {
					window['multidomainman'].blockBlockable($('#mobileAppSaveSettings .blockable'));
				}
			};
		}
});