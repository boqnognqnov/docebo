<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2014 Docebo
 */

class HDSilverApp extends DoceboPlugin {

	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'HDSilverApp';

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name = __CLASS__) {
		return parent::plugin($class_name);
	}

	public function activate() {
		Settings::save("helpdesk_level", "silver");
		parent::activate();
	}

	public function deactivate() {
		Settings::save("helpdesk_level", "silver");
		parent::deactivate();
	}

}
