<?php

const FRAMEWORK_LEVEL_USER = '/framework/level/user';

//include '../plugins/ShopifyApp/models/ShopifyProductProxy.php';
//include '../plugins/ShopifyApp/models/ShopifyCustomerProxy.php';

include '../plugins/ShopifyApp/components/ShopifyProductProxy.php';
include '../plugins/ShopifyApp/components/ShopifyCustomerProxy.php';


include '../lms/protected/controllers/CartController.php';

class ShopifyAppController extends Controller {


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		$res[] =
			array('allow',
				'actions' =>
					array(
						'webhookOnCheckoutCreated', 'webhookOnCheckoutUpdated',
						'webhookOnOrderPaid',
						'webhookOnCustomerCreated', 'webhookOnCustomerDeleted',
						'webhookOnProductUpdated'
					),
				'users' => array('*'),
			);

		$admin_only_actions = array('settings', 'axBeginCoursesSynchronization', 'axCheckCoursesSynchronizationStatus');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);
		
		// Allow access to other actions only to logged-in users:
		/*
		$res[] = array(
		    'allow',
		    'actions' => array(''),
		    'users' => array('*'),
		);
		*/

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}



	public function beforeAction($action) {

		//just some log activity
		switch ($action->id) {
			case 'webhookOnCheckoutCreated':
			case 'webhookOnCheckoutUpdated':
			case 'webhookOnOrderPaid':
			case 'webhookOnCustomerCreated':
			case 'webhookOnCustomerDeleted':
			case 'webhookOnProductUpdated':
				Yii::log("Shopify Webhook called: ".$action->id, CLogger::LEVEL_INFO);
				break;
		}

		return parent::beforeAction($action);
	}



	public function actionIndex() {
		$this->forward('settings');
	}

	public function actionSettings() {
//		$publicKey = Settings::get('shopify_public_key', false);
//		$secretKey = Settings::get('shopify_private_key', false);
		$shopifySubdomain = Settings::get('shopify_subdomain', false);
		$shopifyPrivateApiKey = Settings::get('shopify_private_api_key', false);
		$shopifyPrivatePassword = Settings::get('shopify_private_password', false);
		$shopifySharedSecret = Settings::get('shopify_shared_secret', false);
		$currencySymbol = Settings::get('currency_symbol', false);
		$currencyCode = Settings::get('currency_code', false);
		$shopifyEnabled = Settings::get('shopify_enabled', false);

		$lastCoursesSyncDate = Settings::get('shopify_last_courses_replication_timestamp', 'never');
		$lastCoursesSyncAffectedCount = Settings::get('shopify_last_courses_replication_count', 0);

		$isConfigured = $shopifySubdomain && $shopifyPrivateApiKey && $shopifyPrivatePassword /*&& $shopifySharedSecret*/;
		$shopifySettingsURL = Docebo::createAdminUrl('ShopifyApp/ShopifyApp/settings');

		/*
		//set currency
		if ($isConfigured && $shopifySubdomain != '') {
			if (!ShopifyProductProxy::shopifyCurrencyIs($currencyCode)) {
				ShopifyRegistrationProxy::alignLmsCurrencyToShopifyCurrency();
			}
		}
		*/

		if($_POST){

			if(isset($_POST['cancel'])){
				$this->redirect(Docebo::createLmsUrl('app/index'));
			}

			if(isset($_POST['submit'])){
				$shopifySubdomain = Yii::app()->request->getParam('shopify_subdomain', false);
				$shopifyPrivateApiKey = Yii::app()->request->getParam('shopify_private_api_key', false);
				$shopifyPrivatePassword = Yii::app()->request->getParam('shopify_private_password', false);
				$shopifySharedSecret = Yii::app()->request->getParam('shopify_shared_secret', false);
				$currencySymbol = Yii::app()->request->getParam('currency_symbol', false);
				$currencyCode = Yii::app()->request->getParam('currency_code', false);

				$continue = true;
				if(!$shopifySubdomain){
					$continue = false;
					Yii::app()->user->setFlash('error', Yii::t('shopify', 'Please, enter site domain'));
				}
				if(!$shopifyPrivateApiKey){
					$continue = false;
					Yii::app()->user->setFlash('error', Yii::t('shopify', 'Please, enter private API key'));
				}
				if(!$shopifyPrivatePassword){
					$continue = false;
					Yii::app()->user->setFlash('error', Yii::t('shopify', 'Please, enter private password'));
				}
				if(!$shopifySharedSecret){
					$continue = false;
					Yii::app()->user->setFlash('error', Yii::t('shopify', 'Please, enter shared secret'));
				}

				if ($continue)
				{
					$shopifyBranch = ShopifyCustomerProxy::retrieveShopifyBranch();
					if (empty($shopifyBranch)) {
						$shopifyBranch = ShopifyCustomerProxy::createShopifyBranch();
					}

//					$shopifyDomainIsChanged = (Settings::get('shopify_subdomain') != $shopifySubdomain);

					//Save settings
					Settings::save('shopify_subdomain', $shopifySubdomain);
					Settings::save('shopify_private_api_key', $shopifyPrivateApiKey);
					Settings::save('shopify_private_password', $shopifyPrivatePassword);
					Settings::save('shopify_shared_secret', $shopifySharedSecret);
					Settings::save('currency_code', ShopifyProductProxy::getShopifyCurrency());
					Settings::save('currency_symbol', '');

					ShopifyCustomerProxy::registerShopifyHooks();
					ShopifyRegistrationProxy::addCollections();

//					AppsMpApiClient::apiSetShopifyDomain(array('shopify_domain' => $shopifySubdomain . '.myshopify.com', 'installation_id' => Docebo::getErpInstallationId()));

					Yii::app()->user->setFlash('success', Yii::t("standard", '_OPERATION_SUCCESSFUL'));
//					if ($shopifyDomainIsChanged)
//						$this->redirect(Yii::app()->params['installer_url'] . 'shopify/authenticate?shop=' . $shopifySubdomain . '.myshopify.com');
				}
				$this->redirect($shopifySettingsURL);
			}
		}

		$this->render('settings', array(
			'shopifySubdomain' => $shopifySubdomain,
			'shopifyPrivateApiKey' => $shopifyPrivateApiKey,
			'shopifyPrivatePassword' => $shopifyPrivatePassword,
			'shopifySharedSecret' => $shopifySharedSecret,
			'shopifyEnabled' => $shopifyEnabled,
			'currencySymbol' => $currencySymbol,
			'currencyCode' => $currencyCode,
			'isConfigured' => $isConfigured,
			'shopify_last_courses_replication_timestamp' => $lastCoursesSyncDate,
			'shopify_last_courses_replication_count' => $lastCoursesSyncAffectedCount
		));
	}

	public function actionAxSynchronizeCourses()
	{
		Settings::save('shopifyCoursesSyncLog', '');
		$doceboCurrencyCode = Settings::get('currency_code');
		if (!ShopifyProductProxy::shopifyCurrencyIs($doceboCurrencyCode))
		{
			ShopifyRegistrationProxy::alignLmsCurrencyToShopifyCurrency();
		}

		$affectedItemsCount = ShopifyProductProxy::replicateCoursesAndPlansFromLmsToShopify();
		Settings::save('shopify_last_courses_replication_timestamp', time());
		Settings::save('shopify_last_courses_replication_count', $affectedItemsCount);
		$startDate = Yii::app()->localtime->getUTCNow('Y-m-d h:i:s A');
		echo '{"success": "true", "affectedCount": ' . $affectedItemsCount . ', "lastUpdate": "' . $startDate . '", "currency_code_lms": "' . Settings::get('currency_code') . '"}';
	}

	public function actionReportShopifyCoursesSynch()
	{
		$reportType = $_GET['type'];

		ob_end_clean();
		$filename =
			"shopifyCourseReplicationReport_" . date('Ymd') .
			($reportType == 'csv' ? ".csv" : '.xls');
		header("Content-Disposition: attachment; filename=\"$filename\"");
		if ($reportType == 'csv')
			header("Content-Type: text/csv");
		elseif ($reportType == 'xls')
				header("Content-Type: application/vnd.ms-excel");

		$logItems = explode('|', Settings::get('shopifyCoursesSyncLog'));

		if ($reportType == 'xls')
		{
			Yii::import('common.extensions.array2excel.Array2Excel');

			$putInArrayFunc = function ($item)
			{
				return [$item];
			};

			$xls = new Array2Excel(array_map($putInArrayFunc, $logItems), ['Error']);
			$xls->exportType = 'Excel5';
			$xls->filename = Docebo::getUploadTmpPath().DIRECTORY_SEPARATOR . Sanitize::fileName('xlsReportShopifyCoursesSyncLog' . rand(100000, 999999) . '.xls', true);

			// Save the file locally first (due to memory issues with direct stream of data)
			$xls->stream = false;
			$xls->run();

			readfile($xls->filename);

			unlink($xls->filename);

			Yii::app()->end();
		}

		if ($reportType == 'csv') {
			$out = fopen("php://output", 'w');
			fputcsv($out, ['Error'], ',', '"');
			foreach ($logItems as $logItem)
				if ($logItem != '') {
					fputcsv($out, [$logItem], ',', '"');
				}
			fclose($out);
			exit;
		}
	}

//	public function actionAuthenticateCallback() {
//		$apiClient =
//			ShopifyAPIClient::create(false);
//
//		$token = $apiClient->getAccessToken($_GET['code']);
//		if ($token)
//		{
//			Settings::save('shopify_authentication_token', $token);
//			Yii::app()->user->setFlash('success', Yii::t("standard", '_OPERATION_SUCCESSFUL'));
//
//			ShopifyRegistrationProxy::afterTokenReceivedActions();
//		}
//		else
//			Yii::app()->user->setFlash('error', Yii::t("shopify", 'An error occured with authentication token retrieval.'));
//		$this->redirect(Docebo::createAdminUrl('ShopifyApp/ShopifyApp/settings'));
//	}

	public function actionWebhookOnProductUpdated()
	{
		$postdata = file_get_contents("php://input");

		$this->verifyWebhookCall($postdata);
		$postdata = json_decode($postdata);

		if (isset($postdata->variants) && is_array($postdata->variants))
		{
			$course = null;
			$productId = $postdata->id;

			$product = [
				'date' => null,
				'price' => 0.00
			];
			foreach ($postdata->variants as $variant)
			{
				if ($product['date'] === null || $product['date'] < $variant->updated_at)
				{
					$product['date'] = $variant->updated_at;
					$product['price'] = $variant->price;

					//if this is a course variant (ILT or webinar) make sure to update its price in the LMS database
					Yii::log("Shopify Webhook onProductUpdate: updating variant #".$variant->id." with price: ".number_format($variant->price, 2, '.', ''));
					$query = "UPDATE ".LtCourseSessionShopifyVariant::model()->tableName()." SET price = :price WHERE shopifyVariantId = :vid";
					$rs = Yii::app()->db->createCommand($query)->execute(array(':price' => floatval($variant->price), ':vid' => $variant->id));
					$query = "UPDATE ".WebinarSessionShopifyVariant::model()->tableName()." SET price = :price WHERE shopifyVariantId = :vid";
					$rs = Yii::app()->db->createCommand($query)->execute(array(':price' => floatval($variant->price), ':vid' => $variant->id));
				}
			}

			if (ShopifyWebhookHistory::isWehbookUsedAlreadyAndTrack($postdata->id, $variant->updated_at)) {
				Yii::app()->end();
			}

			$model = LearningCourseShopifyProduct::model()->findByAttributes(['shopifyProductId' => $productId]);
			if ($model)
			{
				$course = LearningCourse::model()->findByPk($model->idCourse);
				if ($course)
				{
					//$course->prize = $product['price'];
					//$course->save(false);
					//NOTE: avoid the AR model, which will raise an event calling shopify API again which will then callback again the LMS and so on in a loop
					$query = "UPDATE ".LearningCourse::model()->tableName()." SET prize = :price WHERE idCourse = :id_course";
					$rs = Yii::app()->db->createCommand($query)->execute(array(':price' => $product['price'], ':id_course' => $course->idCourse));
				}
			}
			else
			{
				$model = LearningCoursepathShopifyProduct::model()->findByAttributes(['shopifyProductId' => $productId]);
				$coursePath = LearningCoursepath::model()->findByPk($model->id_path);
				if ($coursePath)
				{
					//$coursePath->price = $product['price'];
					//$coursePath->save(false);
					//NOTE: avoid the AR model, which will raise an event calling shopify API again which will then callback again the LMS and so on in a loop
					$query = "UPDATE ".LearningCoursepath::model()->tableName()." SET price = :price WHERE id_path = :id_path";
					$rs = Yii::app()->db->createCommand($query)->execute(array(':price' => $product['price'], ':id_path' => $coursePath->id_path));
				}
			}
		}
		Yii::app()->end();
	}

	public function actionWebhookOnCheckoutCreated()
	{

	}

	public function actionWebhookOnCheckoutUpdated()
	{

	}

	public function actionWebhookOnOrderPaid()
	{
		$postdata = file_get_contents("php://input");
		$orderJSON = json_decode($postdata, true);
		$this->verifyWebhookCall($postdata);
		if ($this->orderContainsAnyCourseOrCoursepath($orderJSON['line_items'])) {
			ShopifyCustomerProxy::addShopifyUser($orderJSON['customer'], true);
			if (!$this->transactionExists($orderJSON['id'])) {
				$userIdst = $this->subscribeUser($orderJSON['email'], $orderJSON['line_items']);
				$this->storeTransaction($userIdst, $orderJSON);
			}
			//sometimes it may happen that some enrolled/imported users have a invalid enrollment level. Trying to fix it.
			LearningCourseuser::fixEnrollmentsLevel();
		}
	}

	private function transactionExists($id)
	{
		return EcommerceTransaction::model()->find('payment_txn_id = :payment_txn_id', array(':payment_txn_id' => $id)) != null;
	}

	private function orderContainsAnyCourseOrCoursepath($productList)
	{
		foreach ($productList as $productVO) {
			$shopifyProductId = $productVO['product_id'];

			$courseShopifyProduct =
				LearningCourseShopifyProduct::model()->find('shopifyProductId = :shopifyProductId', [':shopifyProductId' => $shopifyProductId]);
			if ($courseShopifyProduct != null) {
				return true;
			} else {
				$coursePathShopifyProduct =
					LearningCoursepathShopifyProduct::model()->find('shopifyProductId = :shopifyProductId', [':shopifyProductId' => $shopifyProductId]);
				if ($coursePathShopifyProduct != null) {
					return true;
				}
			}
		}
		return false;
	}

	private function enableSession($userIdst, $chosenVariantShopifyId)
	{
		$ltCourseSessionShopifyVariant =
			LtCourseSessionShopifyVariant::model()->find('shopifyVariantId = :shopifyVariantId', [':shopifyVariantId' => $chosenVariantShopifyId]);
		$webinarSessionShopifyVariant =
			WebinarSessionShopifyVariant::model()->find('shopifyVariantId = :shopifyVariantId', [':shopifyVariantId' => $chosenVariantShopifyId]);

		$sessionEnrollment = null;
		$idSession = null;
		if ($ltCourseSessionShopifyVariant != null)
		{
			$idSession = $ltCourseSessionShopifyVariant->id_session;
			$sql = "insert into lt_courseuser_session (id_session, id_user, date_subscribed) values (:id_session, :id_user, :date_subscribed)";
			$parameters = array(':id_session' => $idSession, ':id_user' => $userIdst, ':date_subscribed' => Yii::app()->localtime->getLocalNow());
			Yii::app()->db->createCommand($sql)->execute($parameters);

		}
		elseif ($webinarSessionShopifyVariant != null)
		{
			$idSession = $webinarSessionShopifyVariant->id_session;
			$sql = "insert into webinar_session_user (id_session, id_user, date_subscribed) values (:id_session, :id_user, :date_subscribed)";
			$parameters = array(':id_session' => $idSession, ':id_user' => $userIdst, ':date_subscribed' => Yii::app()->localtime->getLocalNow());
			Yii::app()->db->createCommand($sql)->execute($parameters);
		}

	}

	private function subscribeUser($userEmail, $productList)
	{
		$userIdst = 0;

		$userVO = CoreUser::model()->find('email = :email', [':email' => $userEmail]);
		if ($userVO)
		{
			$userIdst = $userVO->idst;
			foreach ($productList as $productVO)
			{
				$shopifyProductId = $productVO['product_id'];

				$chosenVariantShopifyId = $productVO['variant_id'];

				$courseShopifyProduct =
					LearningCourseShopifyProduct::model()->find('shopifyProductId = :shopifyProductId', [':shopifyProductId' => $shopifyProductId]);
				if ($courseShopifyProduct != null)
				{
					//enroll to course
					$idCourse = $courseShopifyProduct->idCourse;
					CartController::enableCourse($userIdst, $idCourse);
					$this->enableSession($userIdst, $chosenVariantShopifyId);
				}
				else
				{
					//no course could be find, let's retry with learning plans
					$coursePathShopifyProduct =
						LearningCoursepathShopifyProduct::model()->find('shopifyProductId = :shopifyProductId', [':shopifyProductId' => $shopifyProductId]);
					if ($coursePathShopifyProduct != null)
					{
						// enroll to learning path
						$path_id = $coursePathShopifyProduct->id_path;
						CartController::enableLearningPath($userIdst, $path_id);
						$planVO = LearningCoursepath::model()->find('id_path = :id_path', [':id_path' => $path_id]);
						$containedCourses = LearningCoursepathCourses::model()->findAll('id_path = :id_path', [':id_path' => $planVO->id_path]);
						foreach ($containedCourses as $containedCourseLinkage)
						{
							CartController::enableCourse($userIdst, $containedCourseLinkage->id_item);
						}
					} else {
						Yii::log('Shopify method '.__METHOD__.' error: could not find course or learning plan with pruduct id #'.$shopifyProductId.' for user enrollment "'.$userEmail.'"', CLogger::LEVEL_WARNING);
					}
				}
			}
		} else {
			Yii::log('Shopify method '.__METHOD__.' error: could not find user "'.$userEmail.'"', CLogger::LEVEL_WARNING);
		}

		return $userIdst;
	}

	private function storeTransaction($userIdst, $orderData)
	{
		$billingInfo = new CoreUserBilling();
		$billingInfo->bill_address1 = $orderData['billing_address']['address1'];
		$billingInfo->bill_address2 = $orderData['billing_address']['address2'];
		$billingInfo->bill_city = $orderData['billing_address']['city'];
		$billingInfo->bill_country_code = $orderData['billing_address']['country_code'];
		$billingInfo->bill_company_name = $orderData['billing_address']['company'];
		$billingInfo->bill_state = $orderData['billing_address']['country'];
		$billingInfo->bill_zip = $orderData['billing_address']['zip'];
		$billingInfo->idst = $userIdst;
		$billingInfo->save();

		$transactionModel = new EcommerceTransaction();
		$transactionModel->id_user = $userIdst;
		$transactionModel->paid = 1;
		$transactionModel->date_activated = Yii::app()->localtime->localNow;
		$transactionModel->location = 'lms';
		$transactionModel->date_creation = Yii::app()->localtime->localNow;
		$transactionModel->billing_info_id = $billingInfo->id;
		$transactionModel->payment_currency = CoreSetting::get('currency_code');
		$transactionModel->payment_type = 'shopify';
		$transactionModel->payment_txn_id = $orderData['id'];

		$transactionSaved = $transactionModel->save();
		if ($transactionSaved) {

			$transaction_id = $transactionModel->id_trans;

			foreach ($orderData['line_items'] as $orderProductVO)
			{
				$transactionItemModel = new EcommerceTransactionInfo();
				$transactionItemModel->id_trans = $transaction_id;

				$transactionItemModel->id_date = '';
				$transactionItemModel->id_edition = '';
				$transactionItemModel->code = '';
				$transactionItemModel->activated = 0;
				$transactionItemModel->name = $orderProductVO['name'];

				$foundLmsCourseOrPath = false;

				$shopifyProductId = $orderProductVO['product_id'];
				$courseShopifyProduct =
					LearningCourseShopifyProduct::model()->find('shopifyProductId = :shopifyProductId', [':shopifyProductId' => $shopifyProductId]);
				if ($courseShopifyProduct != null)
				{
					//course
					$idCourse = $courseShopifyProduct->idCourse;
					$courseVO = LearningCourse::model()->findByPk($idCourse);

					$transactionItemModel->id_course = $idCourse;
					$transactionItemModel->price = $courseVO->prize == '' ? 0 : $courseVO->prize;
					$transactionItemModel->code = $courseVO->code;
					$transactionItemModel->item_type = EcommerceTransactionInfo::TYPE_COURSE;

					if ($courseVO->course_type == LearningCourse::TYPE_CLASSROOM && !empty($orderProductVO['variant_id'])) {
						$chosenVariantShopifyId = $orderProductVO['variant_id'];
						$variant = LtCourseSessionShopifyVariant::model()->findByAttributes(array('shopifyVariantId' => $chosenVariantShopifyId));
						if (!empty($variant)) {
							$transactionItemModel->id_date = $variant->id_session;
						}
					}

					if ($courseVO->course_type == LearningCourse::TYPE_WEBINAR && !empty($orderProductVO['variant_id'])) {
						$chosenVariantShopifyId = $orderProductVO['variant_id'];
						$variant = WebinarSessionShopifyVariant::model()->findByAttributes(array('shopifyVariantId' => $chosenVariantShopifyId));
						if (!empty($variant)) {
							$transactionItemModel->id_date = $variant->id_session;
						}
					}

					$foundLmsCourseOrPath = true;
				}
				else
				{
					$coursePathShopifyProduct =
						LearningCoursepathShopifyProduct::model()->find('shopifyProductId = :shopifyProductId', [':shopifyProductId' => $shopifyProductId]);
					if ($coursePathShopifyProduct != null)
					{
						//path
						$path_id = $coursePathShopifyProduct->id_path;
						$planVO = LearningCoursepath::model()->find('id_path = :id_path', [':id_path' => $path_id]);

						$transactionItemModel->id_path = $planVO->id_path;
						$transactionItemModel->price = $planVO->price == '' ? 0 : $planVO->price;
						$transactionItemModel->code = $planVO->path_code;
						$transactionItemModel->item_type = EcommerceTransactionInfo::TYPE_COURSEPATH;

						$foundLmsCourseOrPath = true;
					}
				}

				if ($foundLmsCourseOrPath) {
					$transactionItemModel->save();
				}
			}

			//now finalize transaction and handle events
			$this->sendEmailNotifications($transactionModel);
		}
	}


	/**
	 * NOTE: this internal method is very similar to CartController::sendEmailNotifications, althoug hused in a different context
	 * @param EcommerceTransaction $transactionModel
	 * @return bool
	 */
	private function sendEmailNotifications($transactionModel) {

		$userModel = CoreUser::model()->findByPk($transactionModel->id_user);

		Yii::app()->event->raise('UserBoughtCourse', new DEvent($this, array(
			'user' => $userModel,
			'transaction' => $transactionModel
		)));

		$trans_items = $transactionModel->transaction_items;
		if (is_array($trans_items)) {
			foreach ($trans_items as $transaction_item) {

				if ($transaction_item->item_type == EcommerceTransactionInfo::TYPE_COURSE) {

					$courseModel = LearningCourse::model()->findByPk($transaction_item->id_course);

					Yii::app()->event->raise(EventManager::EVENT_USER_SUBSCRIBED_COURSE, new DEvent($this, array(
						'user' => $userModel,
						'course' => $courseModel
					)));

					if ($courseModel->course_type == LearningCourse::TYPE_CLASSROOM && $transaction_item->id_date) {
						Yii::app()->event->raise(EventManager::EVENT_USER_ENROLLED_IN_CLASSROOM_SESSION, new DEvent($this, array(
							'session_id' => $transaction_item->id_date,
							'user' => $transactionModel->id_user,
						)));
					}

					if ($courseModel->course_type == LearningCourse::TYPE_WEBINAR && $transaction_item->id_date) {
						Yii::app()->event->raise(EventManager::EVENT_USER_ENROLLED_IN_WEBINAR_SESSION, new DEvent($this, array(
							'session_id' => $transaction_item->id_date,
							'user' => $transactionModel->id_user,
						)));
					}
				}

				if ($transaction_item->item_type == EcommerceTransactionInfo::TYPE_COURSEPATH) {
					$model = LearningCoursepath::model()->findByPk($transaction_item->id_path);
					Yii::app()->event->raise(EventManager::EVENT_USER_ENROLLED_IN_LEARNING_PLAN, new DEvent($this, array(
						'user_id' => $transactionModel->id_user,
						'learning_plan' => $model
					)));
				}
			}
		}

		return true;
	}


	private function verifyWebhookCall($input) {

		$hmac = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
		$calculatedHmac =
			base64_encode(hash_hmac('sha256', $input, Settings::get('shopify_shared_secret'), true));

		if ($hmac != $calculatedHmac)
			throw new Exception("wrong validation for webhook: $hmac $calculatedHmac");
	}


	public function actionWebhookOnCustomerCreated() {}

	public function actionWebhookOnCustomerDeleted() {}


	public function actionClearSettings(){
		//clear all shopify-related settings
		Settings::remove('shopify_public_key');
		Settings::remove('shopify_private_key');
		Settings::remove('shopify_subdomain');
		Settings::remove('shopify_private_api_key');
		Settings::remove('shopify_private_password');
		Settings::remove('shopify_shared_secret');
		Settings::remove('shopify_last_courses_replication_timestamp');
		Settings::remove('shopify_last_courses_replication_count');
		Settings::remove('currency_code');

		Settings::remove('shopify_sync_process_status');
		Settings::remove('shopify_last_api_call_time');
		//===
		//clear pending/ruined existing jobs
		CoreJob::model()->deleteAllByAttributes(array(
			'name' => ShopifyJobHandler::JOB_NAME
		));
		//===

		//===
		//clear all LMS products/variants IDs
		LearningCourseShopifyProduct::model()->deleteAll();
		if (PluginManager::isPluginActive('CurriculaApp')) { LearningCoursepathShopifyProduct::model()->deleteAll(); }
		if (PluginManager::isPluginActive('ClassroomApp')) { LtCourseSessionShopifyVariant::model()->deleteAll(); }
		WebinarSessionShopifyVariant::model()->deleteAll();
		//===

		Yii::app()->user->setFlash('success', Yii::t("standard", '_OPERATION_SUCCESSFUL'));
		$this->redirect(Docebo::createAdminUrl('ShopifyApp/ShopifyApp/settings'));
	}

	private function authenticate($publicKey, $secretKey, $shopifySubdomain) {
		$apiClient = ShopifyAPIClient::create(false);
		$callbackUrl =
			(
				($_SERVER['SERVER_PORT'] == 443)
					? "https://"
					: "http://"
			) .
			$_SERVER['SERVER_NAME'] .
			Docebo::createAdminUrl('ShopifyApp/ShopifyApp/authenticateCallback');

		$installationUrl =
			$apiClient->getAuthorizeUrl(
				'read_customers,write_products,read_orders',
				$callbackUrl
			);

		$this->redirect($installationUrl);
	}

	public function actionAxLogsPopup() {

		$content = $this->renderPartial('_logs', [], true);
		echo $content;
		Yii::app()->end();
	}



	protected function _getActiveSyncCoursesJob() {
		//search for job involved in synch process
		$activeJobs = CoreJob::model()->findAllByAttributes(array(
			'name' => ShopifyJobHandler::JOB_NAME,
			'type' => CoreJob::TYPE_ONETIME
		));

		$foundJob = false;
		if (is_array($activeJobs) && !empty($activeJobs)) {
			foreach ($activeJobs as $activeJob) {
				$jobParams = CJSON::decode($activeJob->params);
				if (is_array($jobParams) && isset($jobParams['type']) && $jobParams['type'] == 'sync_courses') {
					$foundJob = $activeJob;
				}
			}
		}

		return $foundJob;
	}


	public function actionAxBeginCoursesSynchronization() {

		$alreadyActiveJob = $this->_getActiveSyncCoursesJob();
		if (!empty($alreadyActiveJob)) {
			echo CJSON::encode(array(
				'success' => false,
				'message' => Yii::t('shopify', 'A synchronization job is already running, retry in a while')
			));
			Yii::app()->end();
		}

		//check currency
		Settings::save('shopifyCoursesSyncLog', '');
		$doceboCurrencyCode = Settings::get('currency_code');
		if (!ShopifyProductProxy::shopifyCurrencyIs($doceboCurrencyCode)) {
			ShopifyRegistrationProxy::alignLmsCurrencyToShopifyCurrency();
		}

		//clean old sync status infos
		Settings::remove('shopify_sync_process_status');

		//check courses to be synchronized
		$toBeSync = array(
			'courses' => array(),
			'plans' => array()
		);

		//check courses
		$sql = "SELECT * FROM ".LearningCourse::model()->tableName()." WHERE selling = 1";
		$cmd = Yii::app()->db->createCommand($sql);
		$reader = $cmd->query();
		while ($record = $reader->read()) {
			$toBeSync['courses'][] = (int)$record['idCourse'];
		}

		//check learning plans
		if (PluginManager::isPluginActive('CurriculaApp')) {
			$sql = "SELECT * FROM ".LearningCoursepath::model()->tableName()." WHERE is_selling = 1";
			$cmd = Yii::app()->db->createCommand($sql);
			$reader = $cmd->query();
			while ($record = $reader->read()) {
				$toBeSync['plans'][] = (int)$record['id_path'];
			}
		}

		//check if there are effectively items to be synchronized
		if ((count($toBeSync['courses']) + count($toBeSync['plans'])) <= 0) {
			/* @var $lt LocalTime */
			$lt = Yii::app()->localtime;
			$utcNow = $lt->getUTCNow('Y-m-d H:i:s');
			$now = $lt->toLocalDateTime($utcNow);
			Settings::save('shopify_sync_process_status', CJSON::encode(array(
				'start_time' => $now,
				'end_time' => $now,
				'status' => 'done',
				'total' => 0,
				'completed' => 0,
				'errors' => 0
			)));

			Settings::save('shopify_last_courses_replication_timestamp', $utcNow);
			Settings::save('shopify_last_courses_replication_count', 0);

			echo CJSON::encode(array(
				'success' => true,
				'num_courses' => 0,
				'num_plans' => 0,
				'id_job' => null
			));
			//in this case no more action are needed, no job is needed, just return information to the client
			YiI::app()->end();
		}

		//prepare the job
		$job = Yii::app()->scheduler->createImmediateJob(ShopifyJobHandler::JOB_NAME, ShopifyJobHandler::id(), array(
			'type' => 'sync_courses',
			'courses' => $toBeSync['courses'],
			'plans' => $toBeSync['plans']
		));
		if (empty($job)) {
			echo CJSON::encode(array(
				'success' => false,
				'message' => Yii::t('standard', '_OPERATION_FAILURE')
			));
			Yii::app()->end();
		} else {
			/* @var $lt LocalTime */
			$lt = Yii::app()->localtime;
			$now = $lt->toLocalDateTime($lt->getUTCNow('Y-m-d H:i:s'));
			Settings::save('shopify_sync_process_status', CJSON::encode(array(
				'start_time' => $now,
				'end_time' => null,
				'status' => 'not started',
				'total' => (count($toBeSync['courses']) + count($toBeSync['plans'])),
				'completed' => 0,
				'errors' => 0
			)));
		}

		echo CJSON::encode(array(
			'success' => true,
			'id_job' => $job->id_job,
			'num_courses' => count($toBeSync['courses']),
			'num_plans' => count($toBeSync['plans'])
		));
		YiI::app()->end();
	}


	public function actionAxCheckCoursesSynchronizationStatus() {

		//read client status
		$clientStatus = Yii::app()->request->getParam('current_status', 'not started');
		$clientCompleted = Yii::app()->request->getParam('current_completed', 0);
		$clientErrors = Yii::app()->request->getParam('current_errors', 0);

		//retrieve current status descriptor
		$status = Settings::get('shopify_sync_process_status', null);
		if ($status === null) {
			//info were not set yet, init them now
			/* @var $lt LocalTime */
			$lt = Yii::app()->localtime;
			$now = $lt->toLocalDateTime($lt->getUTCNow('Y-m-d H:i:s'));
			$status = CJSON::encode(array(
				'start_time' => $now,
				'end_time' => null,
				'status' => 'not started',
				'total' => 0,
				'completed' => 0,
				'errors' => 0
			));
			Settings::save('shopify_sync_process_status', $status);
		}

		//check continuously the status of the job until it has effectively changed or the timeout limit has been reached
		$timeBegin = microtime(true);
		$timeLimit = 5.0; //5 seconds
		$timeLimitPassed = false;
		$statusHasChanged = false;
		while (!$statusHasChanged && !$timeLimitPassed) {
			$decoded = CJSON::decode($status);
			if (empty($decoded) || !is_array($decoded) || !isset($decoded['status'])) {
				echo CJSON::encode(array('status' => 'error', 'message' => 'Invalid status'));
				Yii::app()->end();
			}

			//first check time
			$timeNow = microtime(true);
			if (($timeNow - $timeBegin) > $timeLimit) {
				$timeLimitPassed = true;
			} else {
				//then check status
				if ($decoded['status'] != $clientStatus || $decoded['completed'] != $clientCompleted || $decoded['errors'] != $clientErrors) {
					$statusHasChanged = true;
				} else {
					usleep(10000); //wait a bit and then re-read the job status
					$status = Settings::get('shopify_sync_process_status', null);
				}
			}
		}

		echo $status;

		Yii::app()->end();
	}

}
