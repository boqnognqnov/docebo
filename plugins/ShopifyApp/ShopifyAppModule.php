<?php

class ShopifyAppModule extends CWebModule
{

	private $_assetsUrl;

	public function init()
	{
		// import the module-level models and components
		$this->setImport(array(
			'ShopifyApp.components.*'
		));

		$this->defaultController = 'shopifyApp';

		if (Yii::app() instanceof CWebApplication) {
			// Register CSS ONLY if this is NOT an ajax call. Plugins' init() is always called during their loading
			if (!Yii::app()->request->isAjaxRequest) {
				Yii::app()->getClientScript()->registerCssFile($this->getAssetsUrl() . "/css/shopify.css");
			}

			Yii::app()->request->noCsrfValidationRoutes[] = 'ShopifyApp/ShopifyApp/webhookOnCheckoutCreated';
			Yii::app()->request->noCsrfValidationRoutes[] = 'ShopifyApp/ShopifyApp/webhookOnCheckoutUpdated';
			Yii::app()->request->noCsrfValidationRoutes[] = 'ShopifyApp/ShopifyApp/webhookOnOrderPaid';
			Yii::app()->request->noCsrfValidationRoutes[] = 'ShopifyApp/ShopifyApp/webhookOnCustomerCreated';
			Yii::app()->request->noCsrfValidationRoutes[] = 'ShopifyApp/ShopifyApp/webhookOnCustomerDeleted';
			Yii::app()->request->noCsrfValidationRoutes[] = 'ShopifyApp/ShopifyApp/webhookOnProductUpdated';
		}

		Yii::app()->event->on('BeforeCreateOrgchartTree', array(new ShopifyListeners(), 'onTreeNodeRender'));

		Yii::app()->event->on(EventManager::EVENT_COURSE_PROP_CHANGED, array($this, 'eventCourseUpdate')); //course category may have been changed
		Yii::app()->event->on(EventManager::EVENT_COURSE_DELETED, array($this, 'eventCourseDeleted'));
		Yii::app()->event->on('SaveCustomSettingsFields', array($this, 'eventCourseSettingsUpdate'));

		Yii::app()->event->on(EventManager::EVENT_LEARNING_PLAN_AFTER_SAVE, array($this, 'eventLearningPlanUpdate'));
		Yii::app()->event->on('AfterLearningPlanDeleteAction', array($this, 'eventLearningPlanDelete'));

		Yii::app()->event->on('WebinarSessionCreated', array($this, 'eventWebinarSessionSave'));
		Yii::app()->event->on(WebinarSession::EVENT_AFTER_DELETE, array($this, 'eventWebinarSessionDelete'));
		Yii::app()->event->on('ILTSessionChanged', array($this, 'eventLtCourseSessionSave'));
		Yii::app()->event->on('NewILTSessionCreated', array($this, 'eventLtCourseSessionSave'));

		Yii::app()->event->on(LtCourseSession::EVENT_AFTER_DELETE, array($this, 'eventLtCourseSessionDelete'));

		Yii::app()->event->on(EventManager::EVENT_ENTRY_ASSIGNED_TO_CATALOG, array($this, 'eventEntryAssignedToCatalog'));
		Yii::app()->event->on(EventManager::EVENT_ENTRIES_ASSIGNED_TO_CATALOG, array($this, 'eventEntriesAssignedToCatalog'));
		Yii::app()->event->on(EventManager::EVENT_ENTRY_UNASSIGNED_FROM_CATALOG, array($this, 'eventEntryUnassignedFromCatalog'));
		Yii::app()->event->on(EventManager::EVENT_ENTRIES_UNASSIGNED_FROM_CATALOG, array($this, 'eventEntriesUnassignedFromCatalog'));
	}



	public function eventLearningPlanUpdate($event)
	{
		if (!PluginManager::isPluginActive('ShopifyApp'))
			return;

		$coursePathVO = $event->params['model'];

		if (get_class($coursePathVO) != 'LearningCoursepath') {
			return false;
		}

		if ($coursePathVO->is_selling == 1)
			ShopifyProductProxy::createOrUpdateProductOnShopify($coursePathVO);
		else
			ShopifyProductProxy::deleteProductOnShopify($coursePathVO, true);

	}

	public function eventLearningPlanDelete($event)
	{
		if (!PluginManager::isPluginActive('ShopifyApp'))
			return;

		$coursePathVO = $event->params['model'];

		if (get_class($coursePathVO) != 'LearningCoursepath') {
			return false;
		}

		ShopifyProductProxy::deleteProductOnShopify($coursePathVO, true);
	}

	public function eventCourseUpdate($event) {

		if (!PluginManager::isPluginActive('ShopifyApp'))
			return;

		$course = $event->params['course'];

		if (get_class($course) != 'LearningCourse') {
			return false;
		}

		if ($course->selling == 1 && $course->status == LearningCourse::$COURSE_STATUS_EFFECTIVE)
			ShopifyProductProxy::createOrUpdateProductOnShopify($course);
		else
			ShopifyProductProxy::deleteProductOnShopify($course, true);
	}

	public function eventCourseSettingsUpdate($event)
	{
		if (!PluginManager::isPluginActive('ShopifyApp'))
			return;

		$course = $event->params['course'];

		if ($course->selling == 1 && $course->status == LearningCourse::$COURSE_STATUS_EFFECTIVE)
			$this->eventCourseUpdate($event);
		else
			ShopifyProductProxy::deleteProductOnShopify($course, true);
	}

	public function eventCourseDeleted($event) {

		if (!PluginManager::isPluginActive('ShopifyApp'))
			return;

		$course = $event->params['course'];

		if (get_class($course) != 'LearningCourse') {
			return false;
		}

		ShopifyProductProxy::deleteProductOnShopify($course);
	}

	private function retrieveCourseFromSession($sessionVO)
	{
		return LearningCourse::model()->findByPk($sessionVO->course_id);
	}

	public function eventWebinarSessionSave(DEvent $event) {
		if (!PluginManager::isPluginActive('ShopifyApp'))
			return;
		$webinarSessionModel = $event->params['session'];
		//$webinarSessionDateModels = $event->params['session_dates'];

		$course = $this->retrieveCourseFromSession($webinarSessionModel);
		if (empty($course) || $course->selling == 0)
			return;
		ShopifyProductProxy::createOrUpdateProductVariantOnShopify($webinarSessionModel);
	}

	public function eventWebinarSessionDelete(DEvent $event) {
		if (!PluginManager::isPluginActive('ShopifyApp'))
			return;
		$webinarSessionModel = $event->sender;
		ShopifyProductProxy::deleteProductVariantOnShopify($webinarSessionModel);
	}

	public function eventLtCourseSessionSave(DEvent $event) {
		if (!PluginManager::isPluginActive('ShopifyApp'))
			return;
		$ltCourseSessionModel = $event->params['session'];
		if ($this->retrieveCourseFromSession($ltCourseSessionModel)->selling == 0)
			return;
		ShopifyProductProxy::createOrUpdateProductVariantOnShopify($ltCourseSessionModel);
	}

	public function eventLtCourseSessionDelete(DEvent $event) {
		if (!PluginManager::isPluginActive('ShopifyApp'))
			return;
		$ltCourseSessionModel = $event->sender;
		ShopifyProductProxy::deleteProductVariantOnShopify($ltCourseSessionModel);
	}



	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu()
	{
		// Add menu voices !

		Yii::app()->mainmenu->addAppMenu('shopify', array(
				'icon' => 'admin_ico shopify-icon',
				'link' => 'javascript:void(0);',
				'settings' => Docebo::createAdminUrl('//ShopifyApp/ShopifyApp/settings'),
				'label' => 'Shopify',
			),
			array(
				Yii::t('standard', '_TRANSACTION') => Docebo::createAdminUrl('transaction/index')
			)
		);

	}




	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('ShopifyApp.assets'));
		}
		return $this->_assetsUrl;
	}



	protected function _setCatalogMetafieldUpdateJob($idCatalog, $courses, $plans) {
		if (!is_array($courses)) { $courses = array(); }
		if (!is_array($plans)) { $plans = array(); }
		Yii::app()->scheduler->createImmediateJob(ShopifyJobHandler::JOB_NAME, ShopifyJobHandler::id(), array(
			'type' => 'update_catalog_metafield',
			'catalog_id' => intval($idCatalog),
			'courses' => $courses,
			'plans' => $plans
		));
	}

	public function eventEntryAssignedToCatalog($event) {
		$model = $event->params['model'];
		if ($model instanceof LearningCatalogueEntry) {
			switch ($model->type_of_entry) {
				case LearningCatalogueEntry::ENTRY_COURSE:
					$this->_setCatalogMetafieldUpdateJob($model->idCatalogue, array($model->idEntry), array());
					break;
				case LearningCatalogueEntry::ENTRY_COURSEPATH:
					$this->_setCatalogMetafieldUpdateJob($model->idCatalogue, array(), array($model->idEntry));
					break;
			}
		}
	}

	public function eventEntryUnassignedFromCatalog($event) {
		$model = $event->params['model'];
		if ($model instanceof LearningCatalogueEntry) {
			switch ($model->type_of_entry) {
				case LearningCatalogueEntry::ENTRY_COURSE:
					$this->_setCatalogMetafieldUpdateJob($model->idCatalogue, array($model->idEntry), array());
					break;
				case LearningCatalogueEntry::ENTRY_COURSEPATH:
					$this->_setCatalogMetafieldUpdateJob($model->idCatalogue, array(), array($model->idEntry));
					break;
			}
		}
	}

	public function eventEntriesAssignedToCatalog($event) {
		$idCatalog = $event->params['idCatalogue'];
		$courses = $event->params['courses'];
		$plans = $event->params['plans'];
		$this->_setCatalogMetafieldUpdateJob($idCatalog, $courses, $plans);
	}

	public function eventEntriesUnassignedFromCatalog($event) {
		$idCatalog = $event->params['idCatalogue'];
		$courses = $event->params['courses'];
		$plans = $event->params['plans'];
		$this->_setCatalogMetafieldUpdateJob($idCatalog, $courses, $plans);
	}





}
