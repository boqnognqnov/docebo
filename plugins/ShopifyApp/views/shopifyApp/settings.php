<?php

$_breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	'Shopify',
);

if (isset($breadcrumbs) && !empty($breadcrumbs)) {
	if (is_array($breadcrumbs)) {
		$_breadcrumbs = array_merge($_breadcrumbs, $breadcrumbs);
	} else {
		$_breadcrumbs[] = $breadcrumbs;
	}
}

$this->breadcrumbs = $_breadcrumbs;
?>

<?= CHtml::beginForm(); ?>

<div class="row-fluid">
	<div class="span6">
		<h3 class="advanced-shopify-settings-form-title">
			<?= Yii::t('shopify', 'Shopify') ?>
		</h3>
	</div>
	<?php if ($shopifySubdomain != ''): ?>
	<div class="span6" style="text-align: right">
		<?= CHtml::button(Yii::t('shopify', 'Reset configuration'), array(
			'class' => 'btn btn-docebo red big',
			'id' => 'shopify-clear'
		)) ?>
	</div>
	<?php endif; ?>
</div>
<br>

<?php DoceboUI::printFlashMessages(); ?>

<div class="advanced-main shopify-settings">

	<div class="section">

		<div class="row odd">
			<div class="row">
				<div class="setting-name public-key-setting-name">
					<?= Yii::t('shopify', 'Shopify address') ?>
					<p class="setting-description"><?= Yii::t('shopify', 'Insert here your shopify store address') ?></p>
				</div>
				<div class="values" style="color:#333333;">
					https:// <?= CHtml::textField('shopify_subdomain', $shopifySubdomain) ?> .myshopify.com
				</div>
			</div>
		</div>

		<div class="row even">
			<div class="row">
				<div class="setting-name public-key-setting-name">
					<?= Yii::t('shopify', 'API Key') ?>
					<p class="setting-description"><?= Yii::t('shopify', 'The API key for your private Shopify app') ?></p>
				</div>
				<div class="values">
					<?= CHtml::textField('shopify_private_api_key', $shopifyPrivateApiKey, array('class' => 'inputLarge')) ?>
				</div>
			</div>
		</div>

		<div class="row odd">
			<div class="row">
				<div class="setting-name public-key-setting-name">
					<?= Yii::t('shopify', 'Password') ?>
					<p class="setting-description"><?= Yii::t('shopify', 'The Password for your private Shopify app') ?></p>
				</div>
				<div class="values">
					<?= CHtml::textField('shopify_private_password', $shopifyPrivatePassword, array('class' => 'inputLarge')) ?>
				</div>
			</div>
		</div>

		<div class="row even">
			<div class="row">
				<div class="setting-name public-key-setting-name">
					<?= Yii::t('shopify', 'Shared Secret') ?>
					<p class="setting-description"><?= Yii::t('shopify', 'The shared secret to link your private Shopify app to your LMS') ?></p>
				</div>
				<div class="values">
					<?= CHtml::textField('shopify_shared_secret', $shopifySharedSecret, array('class' => 'inputLarge')) ?>
				</div>
			</div>
		</div>

		<div class="row odd" style="display: none;">
			<div class="row">
				<div class="setting-name public-key-setting-name">
					<?= Yii::t('configuration', 'Currency symbol') ?>
					<p class="setting-description"><?php Yii::t('configuration', 'This symbol will be displayed to end users') ?></p>
				</div>
				<div class="values">
					<?= CHtml::textField('currency_symbol', $currencySymbol, array('class' => 'span12', 'readonly' => true)) ?>
				</div>
			</div>
		</div>

		<div class="row even" style="display:none;">
			<div class="row">

				<div class="setting-name public-key-setting-name">
					<?= Yii::t('apps', 'Enabled') ?>
				</div>
				<div class="values">
					<?= CHtml::checkBox('shopify_enabled', $shopifyEnabled) ?>
				</div>

			</div>
		</div>

		<div class="row odd" style="display: none">
			<div class="row">
				<div class="setting-name public-key-setting-name">
					<?= Yii::t('shopify', 'API Key') ?>
				</div>
				<div class="values">
					<?= CHtml::textField('public_key', $publicKey) ?>
				</div>
			</div>
		</div>

		<div class="row even" style="display: none">
			<div class="row">

				<div class="setting-name public-key-setting-name">
					<?= Yii::t('apps', 'Credential sets (API secret)') ?>
				</div>
				<div class="values">
					<?= CHtml::textField('secret_key', $secretKey) ?>
				</div>

			</div>
		</div>

		<div class="row odd" style="display: none">
			<div class="row">

				<div class="setting-name public-key-setting-name">
					<?= Yii::t('shopify', 'Authentication Callback') ?>
					<p class="setting-description"><?= Yii::t('shopify', 'Copy and paste this URL into your Shopify store settings') ?></p>
				</div>
				<div class="values">
					<br/>
					<?= (
					($_SERVER['SERVER_PORT'] == 443)
						? "https://"
						: "http://"
					) .
					$_SERVER['SERVER_NAME'] .
					Docebo::createAdminUrl('ShopifyApp/ShopifyApp/authenticateCallback')
					?>
				</div>

			</div>
		</div>


		<?
		if ($shopifySubdomain != '') {
			?>

			<div class="row odd">
				<div class="row">

					<div class="setting-name public-key-setting-name">
						<?= Yii::t('standard', 'Currency') ?>
					</div>
					<div class="values">
						<div id="currency_code_lms"><?= $currencyCode ?></div>
						<?php
						echo CHtml::textField('currency_code', $currencyCode, array('readonly' => true, 'disabled' => 'disabled', 'style' => 'display: none'));
						?>
					</div>

				</div>
			</div>

			<div class="row even">
				<div class="row">
					<div class="setting-name public-key-setting-name">
						<?= Yii::t('apps', 'Courses synchronization') ?>
						<p class="setting-description"><?= Yii::t('apps', 'Click here for the first synchronization') ?></p>
					</div>
					<div class="values">
						<br/>

						<div class="row-fluid">
							<div id="sync-courses-holder" class="span3" style="display: none; ">
								<div>
									<div class="span2 text-uppercase">
										<img src="../../../../themes/spt/images/ajax-loader10.gif">
									</div>
									<div class="span10">
										<p class="lead text-uppercase"><strong><?= Yii::t('shopify', 'Syncing<br>Courses...') ?></strong></p>
									</div>
								</div>
								<div id="sync-courses-progress">
									<?= Yii::t('standard', '_PROGRESS') ?>:&nbsp;<strong></strong>
								</div>
							</div>
							<div id="buttonSynchNow" class="span2">
								<?= CHtml::button(Yii::t('apps', 'SYNC NOW'), array(
									'class' => 'btn btn-docebo blue big',
									'name' => 'replicateCourses',
									'id' => 'replicateCourses'
								)); ?>
								<div id="sync-courses-error">

								</div>
							</div>
							<div id="syncDetailsField" class="span3" style="width: 400px;">
								<div class="row">
									<div>
										<?= Yii::t('shopify', 'Last synchronization: ') ?>
										<strong>
											<span id="courses_last_date" class="courses_last_date" style="font-weight: bold;">
												<?php
												/* @var $lt LocalTime */
												$lt = Yii::app()->localtime;
												echo $shopify_last_courses_replication_timestamp == 'never'
													? Yii::t('shopify', 'Never')
													: $lt->toLocalDateTime($shopify_last_courses_replication_timestamp);
												?>
											</span>
										</strong>
										<span id="linkToLogs"<?php if ($shopify_last_courses_replication_timestamp == 'never') echo ' style="display: none;"'; ?>>
											<a class="underline blue-text open-dialog" style="color: #0465ac" data-toggle="modal" href="index.php?r=ShopifyApp/ShopifyApp/axLogsPopup">
												<?= Yii::t('shopify', 'View logs') ?>
											</a>
										</span>
									</div>
								</div>
								<div class="row">
									<span style="margin-bottom: 0px;">
										<?= Yii::t('shopify', 'Synchronized items:') ?>
										<strong>
											<span id="courses_items_count" class="courses_items_count" style="font-weight: bold;">
												<?= $shopify_last_courses_replication_count == '' ? 0 : $shopify_last_courses_replication_count ?>
											</span>
										</strong>
									</span>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>

		<?php
		}
		?>

		<div class="row odd">
			<div class="row">
				<div class="row-fluid right" style="text-align: right">
					<?= CHtml::submitButton(Yii::t('shopify', 'Save Changes'), array(
						'class' => 'btn btn-docebo green big',
						'name' => 'submit'
					)); ?>
					<?= CHtml::submitButton(Yii::t('standard', '_CANCEL'), array(
						'class' => 'btn btn-docebo black big',
						'name' => 'cancel'
					)); ?>
				</div>
			</div>
		</div>

	</div>
</div>

<?= CHtml::endForm() ?>

<script type="text/javascript">
	$(function () {
		if ($(".alert-success").length) {
			setInterval(function () {
				$(".alert-success").fadeOut(1500);
			}, 3000);
		}
		if ($(".alert-error").length) {
			setInterval(function () {
				$(".alert-error").fadeOut(1500);
			}, 3000);
		}

		//handle reset button
		$(document).on('click', '#shopify-clear', function (event) {
			//if not then create and open a confirm dialog
			var dialogId = 'reset-settings-dialog';
			var dialog = $('<div/>').dialog2({
				autoOpen: true,
				id: dialogId,
				title: <?= CJSON::encode(Yii::t('standard', '_AREYOUSURE')) ?>,
				modalClass: 'modal-reset-settings-dialog',
				showCloseHandle: true,
				removeOnClose: true,
				closeOnEscape: true,
				closeOnOverlayClick: true
			});
			$('.modal.modal-reset-settings-dialog').find('.modal-body').html(<?= CJSON::encode(Yii::t('shopify', 'Are you sure you want to reset your settings?')) ?>);
			$('.modal.modal-reset-settings-dialog').find('.modal-footer').html(
				'<input class="btn confirm-btn" type="button" value=<?= CJSON::encode(Yii::t('standard', '_CONFIRM')) ?> id="reset-settings-dialog-confirm-button"/>'+
				'<input class="btn cancel-btn" type="button" value=<?= CJSON::encode(Yii::t('standard', '_CANCEL')) ?> id="reset-settings-dialog-cancel-button" />'
			);
			//buttons actions
			$('.modal.modal-reset-settings-dialog #reset-settings-dialog-confirm-button').off('click');
			$('.modal.modal-reset-settings-dialog #reset-settings-dialog-confirm-button').on('click', function() {
				window.location.href = HTTP_HOST + "index.php?r=ShopifyApp/ShopifyApp/clearSettings";
			});
			$('.modal.modal-reset-settings-dialog #reset-settings-dialog-cancel-button').off('click');
			$('.modal.modal-reset-settings-dialog #reset-settings-dialog-cancel-button').on('click', function() {
				$('#' + dialogId).dialog2('close');
			});
		});


		$('#sync-courses-error').hide();

		var currentPollStatus = {
			current_status: 'not started',
			current_completed: 0,
			current_errors: 0
		};

		var poll = function() {
			$.ajax({
				url: "<?= Docebo::createAdminUrl('ShopifyApp/ShopifyApp/axCheckCoursesSynchronizationStatus') ?>",
				type: 'POST',
				dataType: 'json',
				data: currentPollStatus,
				beforeSend: function () {}
			}).done(function(o) {
				if (!o.status) {}
				switch (o.status) {
					case 'error':
						var errorMessage = o.message;
						//toggle sync. spinner
						$('#sync-courses-holder').hide();
						$('#replicateCourses').show();
						$('#linkToLogs').show();
						//show error message to the user
						var err = $('#sync-courses-error');
						if (err.length > 0) {
							err.html(/*errorMessage ? errorMessage : */<?= CJSON::encode(Yii::t('standard', '_OPERATION_FAILURE')) ?>)
							err.show();
						}
						//reset polling status
						currentPollStatus = {
							current_status: 'not started',
							current_completed: 0,
							current_errors: 0
						};
						break;
					case 'not started':
					case 'running':
						//update client interface
						var t = (o.completed + o.errors) + '/' + o.total;
						$('#sync-courses-progress').find('strong').html(t);
						//update current poll status
						currentPollStatus.current_status = o.status;
						currentPollStatus.current_completed = o.completed;
						currentPollStatus.current_errors = o.errors;
						//set the next polling check
						setTimeout(poll, 100);
						break;
					case 'done':
						//update client interface
						$('.courses_items_count').text(o.total);
						$('.courses_last_date').text(o.end_time);
						//toggle sync. spinner
						$('#sync-courses-holder').hide();
						$('#replicateCourses').show();
						$('#linkToLogs').show();
						//reset polling status
						currentPollStatus = {
							current_status: 'not started',
							current_completed: 0,
							current_errors: 0
						};
						break;
				}

			}).fail(function() {
				//reset polling status
				currentPollStatus = {
					current_status: 'not started',
					current_completed: 0,
					current_errors: 0
				};
				//show a generic error message
				var err = $('#sync-courses-error');
				if (err.length > 0) {
					err.html(<?= CJSON::encode(Yii::t('standard', '_OPERATION_FAILURE')) ?>)
					err.show();
				}
				//toggle sync. spinner
				$('#sync-courses-holder').hide();
				$('#replicateCourses').show();
				$('#linkToLogs').show();
			}).always(function() {

			});
		}

		$('#replicateCourses').on('click', function () {
				$(this).hide();
				$('#sync-courses-holder').show();
				$('#sync-courses-error').hide();
				$('#sync-courses-error').html('');

				$.ajax({
					url: "<?= Docebo::createAdminUrl('ShopifyApp/ShopifyApp/axBeginCoursesSynchronization') ?>",
					type: 'POST',
					dataType: 'json',
					data: {},
					beforeSend: function () {}
				}).done(function(o) {

					//check if request output is ok
					if (!o.success) {
						$('#sync-courses-error').show();
						if (o.message) {
							$('#sync-courses-error').html(o.message);
						} else {
							$('#sync-courses-error').html(<?= CJSON::encode(Yii::t('standard', '_OPERATION_FAILURE')) ?>);
						}
						return;
					}

					//set currency if requested
					if (o.currency_code_lms) {
						$('#currency_code_lms').html(o.currency_code_lms);
					}

					//check if there are effectively some items to be synchronized
					if (o.num_courses <= 0 && o.num_plans <= 0) {
						//update client interface
						$('.courses_items_count').text('0');
						$('.courses_last_date').text('');
						//toggle sync. spinner
						$('#sync-courses-holder').hide();
						$('#replicateCourses').show();
						$('#linkToLogs').show();
						//reset polling status
						currentPollStatus = {
							current_status: 'not started',
							current_completed: 0,
							current_errors: 0
						};
						return;
					}

					//start polling
					poll();

				}).fail(function(jqXHR, textStatus, errorThrown) {

				}).always(function() {

				});
			}
		);

	});
</script>