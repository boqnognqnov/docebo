<h1><?= Yii::t('shopify', 'Synchronization logs') ?></h1>

<p>
    <div class="advanced-main elucidat-settings" style="margin-left: 0px">
        <div class="section">
            <div class="row">
                <div class="row odd"
                    >
                    <div style="margin-bottom: 1px; margin-top: 1px; background-color: rgba(241, 243, 242, 1); padding: 10px; font-size: 13px; height: 40px; padding-bottom: 0px">
                        <div id="dialogueLastSyncDetailsField" class="setting-name dialogueLastSyncDetailsField" style="width: 400px; line-height: 13px; font-size: 13px; font-weight: 300;">
                            &nbsp;
                        </div>
                        <div class="value" style="text-align: right;">
                            <?= Yii::t('shopify', 'Export:') ?>
                            <?php
                                echo
                                    CHtml::dropDownList(
                                        'export_type',
                                        0,
                                        [Yii::t('shopify', 'Choose an action'), Yii::t('shopify', 'Export as XLS'), Yii::t('shopify', 'Export as CSV')],
                                        ['style' => 'width: 150px', 'onchange' => 'exportLog(this.value)']
                                    );
                            ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <b>
        <?= Yii::t('shopify', 'DESCRIPTION') ?>
    </b>
    <hr style="margin-top: 10px; margin-bottom: 10px"/>

    <?php
        $logItems = explode('|', Settings::get('shopifyCoursesSyncLog'));
        foreach ($logItems as $logItem)
            if ($logItem != '') {
                ?><div><?= $logItem ?></div><?
            }
        if (count($logItems) == 0 || $logItems[0] == '') {
            ?>
                <i style="margin-left: 10px;"><?= Yii::t('shopify', 'No errors found') ?></i>
            <?
        }
    ?>
    <hr style="margin-top: 10px; margin-bottom: 10px"/>
</p>

<div style="width: 100%; text-align: right">
    <a class="close-dialog btn btn-docebo blue big" href="#" onclick="javascript: $('.modal').hide(); $('.modal-backdrop').hide();">
        <?= Yii::t('shopify', 'Close') ?>
    </a>
</div>

<script>
    $('.modal-footer').hide();
    $('.dialogueLastSyncDetailsField').html($('#syncDetailsField').html().replace('linkToLogs', 'linkToLogs2'));
    $('.dialogueLastSyncDetailsField #linkToLogs2').hide();

    function exportLog(type)
    {
        if (type == 1)
        {
            type = 'xls';
        }
        else if (type == 2)
        {
            type = 'csv';
        }
        window.open('<?=
                (
                    ($_SERVER['SERVER_PORT'] == 443)
                        ? "https://"
                        : "http://"
                ) .
                $_SERVER['SERVER_NAME'] .
                Docebo::createAdminUrl('ShopifyApp/ShopifyApp/reportShopifyCoursesSynch&type=')
                ?>' + type,'_blank');
    }
</script>