<?php

class ShopifyListeners extends CComponent{

    /**
     * Listen for an event and act accordingly:
     *
     * @param DEvent $event
     */
    public function onTreeNodeRender(DEvent $event) {
        $sourceData = $event->params['sourceData'];
        foreach ($sourceData as $key => $sourceDatum)
        {
            if ($sourceDatum['title'] == 'Shopify')
                $event->params['sourceData'][$key]['customCssClasses'] = 'multidomain';
        }
    }

}