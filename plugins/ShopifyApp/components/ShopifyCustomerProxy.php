<?php

class ShopifyCustomerProxy
{

	private static function removeAllShopifyHooks($apiClient)
	{
		$getHooksResult = $apiClient->call(
			'GET',
			'/admin/webhooks.json'
		);

		foreach ($getHooksResult as $hookVO) {
			$webhookId = $hookVO['id'];
			$hookUnregisterResult = $apiClient->call(
				'DELETE',
				"/admin/webhooks/$webhookId.json"
			);
		}
	}



	public static function registerShopifyHooks()
	{
		$apiClient = ShopifyAPIClient::create();

		self::removeAllShopifyHooks($apiClient);

		$userCreationHookRegisterResult =
			$apiClient->call(
				'POST',
				'/admin/webhooks.json',
				[
					'webhook' =>
						[
							'topic' => 'customers/create',
							'address' => Docebo::createAbsoluteUrl('ShopifyApp/ShopifyApp/webhookOnCustomerCreated'),
							'format' => 'json',
						]
				]
			);

		$userRemovalHookRegisterResult =
			$apiClient->call(
				'POST',
				'/admin/webhooks.json',
				[
					'webhook' =>
						[
							'topic' => 'customers/delete',
							'address' => Docebo::createAbsoluteUrl('ShopifyApp/ShopifyApp/webhookOnCustomerDeleted'),
							'format' => 'json',
						]
				]
			);

		$orderPaymentHookRegisterResult =
			$apiClient->call(
				'POST',
				'/admin/webhooks.json',
				[
					'webhook' =>
						[
							'topic' => 'orders/paid',
							'address' => Docebo::createAbsoluteUrl('ShopifyApp/ShopifyApp/webhookOnOrderPaid'),
							'format' => 'json',
						]
				]
			);

		$checkoutCreateRegisterResult =
			$apiClient->call(
				'POST',
				'/admin/webhooks.json',
				[
					'webhook' =>
						[
							'topic' => 'checkouts/create',
							'address' => Docebo::createAbsoluteUrl('ShopifyApp/ShopifyApp/webhookOnCheckoutCreated'),
							'format' => 'json',
						]
				]
			);

		$checkoutUpdateRegisterResult =
			$apiClient->call(
				'POST',
				'/admin/webhooks.json',
				[
					'webhook' =>
						[
							'topic' => 'checkouts/update',
							'address' => Docebo::createAbsoluteUrl('ShopifyApp/ShopifyApp/webhookOnCheckoutUpdated'),
							'format' => 'json',
						]
				]
			);

		$productUpdateRegisterResult =
			$apiClient->call(
				'POST',
				'/admin/webhooks.json',
				[
					'webhook' =>
						[
							'topic' => 'products/update',
							'address' => Docebo::createAbsoluteUrl('ShopifyApp/ShopifyApp/webhookOnProductUpdated'),
							'format' => 'json',
						]
				]
			);
	}



	public static function replicateUsersFromShopify()
	{
		$apiClient = ShopifyAPIClient::create();

		$customersResult = $apiClient->call(
			'GET',
			'/admin/customers.json',
			array(
				'fields' => 'id,email,first_name,last_name',
				'since_id' => 0
			)
		);

		foreach ($customersResult as $customerVO) {
			self::addShopifyUser($customerVO);
		}
	}



	public static function addShopifyUser($customerVO, $createPassword = false)
	{
		$email = $customerVO['email'];
		$firstName = $customerVO['first_name'];
		$lastName = $customerVO['last_name'];

		if ($email == '' || $firstName == '' || $lastName == '')
			return;

		$userInDb = CoreUser::model()->find('email = :email', array(':email' => $email));
		if (!$userInDb) {
			$newUser = new CoreUser();
			$newUser->email = $email;
			$newUser->firstname = $firstName;
			$newUser->lastname = $lastName;
			$newUser->userid = '/' . $email;
			$newUser->valid = 1;
			$plainPassword = '';
			if ($createPassword) {
				$plainPassword = 'p' . rand(100000, 999999) . rand(100000, 999999) . rand(100000, 999999);
				$newUser->pass = md5($plainPassword);
				//ShopifyAppController::outputRequestToFile('plainPassword.html', $plainPassword);
			}
			if (Settings::get('pass_change_first_login', 'off') == 'on') {
				$newUser->force_change = 1;
			}

			$userSaved = $newUser->save();

			if ($userSaved) {
				$userIdst = CoreUser::model()->find('email = :email', array(':email' => $email))->idst;

				//add the new user in orgbranch
				self::insertUserInBranch($userIdst, CoreOrgChartTree::getOrgRootNode()->idst_oc); // root branch
				self::insertUserInBranch($userIdst, CoreOrgChartTree::getOrgRootNode()->idst_ocd); // root branch
				self::insertUserInBranch($userIdst, Yii::app()->user->getUserLevelIds(true)[FRAMEWORK_LEVEL_USER]); // users

				//do other Shopify stuff for the user
				self::createShopifyBranchAndInsertUser($userIdst);

				//raise user creation event
				$newUser->passwordPlainText = $plainPassword;
				Yii::app()->event->raise('NewUserCreated', new DEvent(Yii::app()->controller, array(
					'user' => $newUser,
				)));

				if ($createPassword) {
					try {
						$mailResult = mail($newUser->email, 'LMS Password', 'Password: ' . $plainPassword);
					} catch (Exception $ex) {
					}
				}
			} else {
				Yii::log('Shopify method '.__METHOD__.' error: user "'.$email.'" ('.$firstName.' '.$lastName.') could not be created', CLogger::LEVEL_WARNING);
			}
		}
	}



	private static function insertUserInBranch($userIdst, $groupId)
	{
		$coreGroupMember = new CoreGroupMembers();
		$coreGroupMember->idstMember = $userIdst;
		$coreGroupMember->idst = $groupId;
		return $coreGroupMember->save();
	}



	public static function createShopifyBranchAndInsertUser($userIdst)
	{
		$shopifyBranch = self::retrieveShopifyBranch();
		if (!$shopifyBranch) {
			$shopifyBranch = self::createShopifyBranch();
		}

		self::insertUserInBranch(
			$userIdst,
			CoreOrgChartTree::model()->findByPk($shopifyBranch->id_dir)->idst_oc
		);
		self::insertUserInBranch(
			$userIdst,
			CoreOrgChartTree::model()->findByPk($shopifyBranch->id_dir)->idst_ocd
		);
	}



	public static function retrieveShopifyBranch()
	{
		$shopifyBranch =
			CoreOrgChart::model()->find("lang_code = 'english' and translation = 'Shopify'");
		return $shopifyBranch;
	}



	public static function createShopifyBranch()
	{
		$rootBranch = CoreOrgChartTree::getOrgRootNode();

		$shopifyBranch = new CoreOrgChartTree();
		$shopifyBranch->appendTo($rootBranch, true, null, false);
		$shopifyBranchId = $shopifyBranch->getPrimaryKey();

		$activeLanguages = CoreLangLanguage::getActiveLanguages();
		$shopifyBranchTranslation = null;
		foreach ($activeLanguages as $langKey => $lang) {
			$shopifyBranchTranslation = new CoreOrgChart();
			$shopifyBranchTranslation->id_dir = $shopifyBranchId;
			$shopifyBranchTranslation->lang_code = $langKey;
			$shopifyBranchTranslation->translation = "Shopify";
			$shopifyBranchTranslation->save();
		}

		return $shopifyBranchTranslation;
	}

}