<?php

class ShopifyJobHandler extends JobHandler implements IJobHandler
{


	/**
	 * Job name
	 * @var string
	 */
	const JOB_NAME = 'ShopifyJob';


	/**
	 * THIS handler identifier
	 * @var string
	 */
	const HANDLER_ID = 'ShopifyApp.components.ShopifyJobHandler';


	/**
	 * @see IJobHandler::init()
	 */
	public function init($params = false) {
		// do some initialization stuff here, specific for this job handler
	}



	/**
	 * @see IJobHandler::id()
	 */
	public static function id($params = false) {
		return self::HANDLER_ID;
	}


	/**
	 * @see IJobHandler::run()
	 */
	public function run($params = false) {
		// Already loaded at upper level

		/* @var $job CoreJob */
		$job = $this->job;
		if (!$job) {
			Yii::log('Invalid job: ' . __METHOD__, CLogger::LEVEL_ERROR);
			return;
		}
		//---


		try {

			$params = CJSON::decode($job->params);
			if (empty($params) || !is_array($params)) {
				throw new CException('Invalid job parameters');
			}

			//=======================
			//=== executing stuff ===
			//=======================

			$apiClient = ShopifyAPIClient::create();

			$jobType = (isset($params['type']) ? $params['type'] : false);
			switch ($jobType) {

				case 'sync_courses':

					$curriculaAppActive = PluginManager::isPluginActive('CurriculaApp');
					/* @var $lt LocalTime */
					$lt = Yii::app()->localtime;

					$_courses = array_unique($params['courses']);
					$_plans = ($curriculaAppActive ? array_unique($params['plans']) : array());

					//making an unique list of courses and learning plans to be synchronized
					$allSyncItems = array();
					foreach ($_courses as $idCourse) {
						$allSyncItems[] = 'c'.'|'.$idCourse;
					}
					foreach ($_plans as $idPath) {
						$allSyncItems[] = 'p'.'|'.$idPath;
					}

					//checking all sync items one by one, regardless of item type
					if (is_array($allSyncItems) && !empty($allSyncItems)) {

						$count = count($allSyncItems);
						$offset = 0;//(isset($params['offset']) ? intval($params['offset']) : 0);
						$limit = 0;//(isset($params['limit']) ? intval($params['limit']) : 0); //0 = no limit

						//retrieve current status descriptor
						$status = Settings::get('shopify_sync_process_status', null);
						if ($status === null) {
							//info were not set yet, init them now (but it should have been set before)
							$now = $lt->toLocalDateTime($lt->getUTCNow('Y-m-d H:i:s'));
							$status = array(
								'start_time' => $now,
								'end_time' => null,
								'status' => 'running',
								'total' => $count,
								'completed' => 0,
								'errors' => 0
							);
						} else {
							$status = CJSON::decode($status);
							$status['total'] = $count;
							$status['status'] = 'running'; //just to be sure ...
						}

						if ($offset < $count) {

							for ($i=0/*$offset*/; $i<$count;/*($limit > 0 ? $i<($offset + $limit) && $i < $count : $i < $count);*/ $i++) {
								if ($i >= $count) { break; }
								$item = $allSyncItems[$i];
								$result = false;
								if (!empty($item) && is_string($item) && strpos($item, '|') !== false) {
									list($type, $id) = explode('|', $item);
									if (intval($id) > 0) {
										switch ($type) {
											case 'c': //'c' as "course"
												$model = LearningCourse::model()->findByPk($id);
												if (!empty($model)) {
													ShopifyProductProxy::createOrUpdateProductOnShopify($model);
													$result = true;
												} else {
													$result = false;
												}
												break;
											case 'p': //'p' as "plan" (learning plan)
												$model = LearningCoursepath::model()->findByPk($id);
												if (!empty($model)) {
													ShopifyProductProxy::createOrUpdateProductOnShopify($model);
													$result = true;
												} else {
													$result = false;
												}
												break;
										}
									}
								} else {
									//invalid item, count it as an error
								}
								//update sync process status
								//check operation result and update counters
								if ($result) {
									$status['completed']++;
								} else {
									$status['errors']++;
								}
								//update status variable
								Settings::save('shopify_sync_process_status', CJSON::encode($status));
							}
						}

						//update process status descriptor
						$utcNow = $lt->getUTCNow('Y-m-d H:i:s');
						$status['status'] = 'done';
						$status['end_time'] = $lt->toLocalDateTime($utcNow);
						Settings::save('shopify_sync_process_status', CJSON::encode($status));

						Settings::save('shopify_last_courses_replication_timestamp', $utcNow);
						Settings::save('shopify_last_courses_replication_count', $count);

					} else {

						//something went wrong
						Settings::save('shopify_sync_process_status', CJSON::encode(array(
							'status' => 'error',
							'message' => 'invalid job parameters'
						)));

					}
					break;

				case 'update_catalog_metafield':

					//retrieve job parameters related to the task
					$courses = (isset($params['courses']) && is_array($params['courses']) ? $params['courses'] : array());
					$plans = (isset($params['plans']) && is_array($params['plans']) ? $params['plans'] : array());

					//=== processing Courses ===
					if (!empty($courses)) {

						$chunks = array_chunk($courses, 100);
						foreach ($chunks as $chunk) {

							/* @var $cmd CDbCommand */
							$cmd = Yii::app()->db->createCommand()
								->select('*')
								->from(LearningCourse::model()->tableName())
								->where(array("IN", "idCourse", $chunk))
								->andWhere("selling = 1");
							$reader = $cmd->query();
							if ($reader) {
								while ($record = $reader->read()) {
									//retrieve shopify product ID
									/* @var $sp LearningCourseShopifyProduct */
									$sp = LearningCourseShopifyProduct::model()->findByAttributes(array('idCourse' => $record['idCourse']));
									if (!empty($sp)) {
										$shopifyProductId = $sp->shopifyProductId;
										if (!empty($shopifyProductId)) {
											$catalogs = Yii::app()->db->createCommand()
												->select("c.idCatalogue, c.name, ce.idEntry")
												->from(LearningCatalogue::model()->tableName()." c")
												->join(LearningCatalogueEntry::model()->tableName()." ce", "c.idCatalogue = ce.idCatalogue")
												->where("ce.idEntry = :id_course AND ce.type_of_entry = :type")
												->queryAll(true, array(':id_course' => $record['idCourse'], ':type' => LearningCatalogueEntry::ENTRY_COURSE));
											$catList = array();
											if (!empty($catalogs) && is_Array($catalogs)) {
												foreach ($catalogs as $catalog) {
													$catList[$catalog['idCatalogue']] = $catalog['name'];
												}
											}
											$catStr = (!empty($catList) ? implode(', ', $catList) : '');
											// 1) retrieve metafield ID
											$metafieldId = NULL;
											$productMetafields = $apiClient->call(
												'GET',
												'/admin/products/'.$shopifyProductId.'/metafields.json',
												array()
											);
											if (isset($productMetafields['metafields']) && is_array($productMetafields['metafields'])) {
												foreach ($productMetafields['metafields'] as $productMetafield) {
													if ($productMetafield['key'] == 'catalogs' && $productMetafield['namespace'] == 'lms') {
														$metafieldId = $productMetafield['id'];
														break;
													}
												}
											}
											// 2) update metafield value
											if (!empty($metafieldId)) {
												$result = $apiClient->call(
													'PUT',
													'/admin/products/' . $shopifyProductId . '/metafields/' . $metafieldId . '.json',
													array(
														"id" => $metafieldId,
														"value" => $catStr,
														"value_type" => "string"
													)
												);
											} else {
												//The product has no "catalogs" metafield assigned yet. Create it now.
												$result = $apiClient->call(
													'POST',
													'/admin/products/' . $shopifyProductId . '/metafields.json',
													array(
														"metafield" => array(
															"namespace" => "lms",
															"key" => "catalogs",
															"value" => $catStr,
															"value_type" => "string"
														)
													)
												);
											}
										}
									}
								}
							}

						}
					}
					//===


					//=== processing Learning Plans ===
					if (!empty($plans) && PluginManager::isPluginActive('CurriculaApp')) {

						$chunks = array_chunk($plans, 100);
						foreach ($chunks as $chunk) {

							/* @var $cmd CDbCommand */
							$cmd = Yii::app()->db->createCommand()
								->select('*')
								->from(LearningCoursepath::model()->tableName())
								->where(array("IN", "id_path", $chunk))
								->andWhere("is_selling = 1");
							$reader = $cmd->query();
							if ($reader) {
								while ($record = $reader->read()) {
									//retrieve shopify product ID
									/* @var $sp LearningCourseShopifyProduct */
									$sp = LearningCoursepathShopifyProduct::model()->findByAttributes(array('idCourse' => $record['id_path']));
									if (!empty($sp)) {
										$shopifyProductId = $sp->shopifyProductId;
										if (!empty($shopifyProductId)) {
											$catalogs = Yii::app()->db->createCommand()
												->select("c.idCatalogue, c.name, ce.idEntry")
												->from(LearningCatalogue::model()->tableName()." c")
												->join(LearningCatalogueEntry::model()->tableName()." ce", "c.idCatalogue = ce.idCatalogue")
												->where("ce.idEntry = :id_course AND ce.type_of_entry = :type")
												->queryAll(true, array(':id_course' => $record['idCourse'], ':type' => LearningCatalogueEntry::ENTRY_COURSEPATH));
											$catList = array();
											if (!empty($catalogs) && is_Array($catalogs)) {
												foreach ($catalogs as $catalog) {
													$catList[$catalog['idCatalogue']] = $catalog['name'];
													break;
												}
											}
											$catStr = (!empty($catList) ? implode(', ', $catList) : '');
											// 1) retrieve metafield ID
											$metafieldId = NULL;
											$productMetafields = $apiClient->call(
												'GET',
												'/admin/products/'.$shopifyProductId.'/metafields.json',
												array()
											);
											if (isset($productMetafields['metafields']) && is_array($productMetafields['metafields'])) {
												foreach ($productMetafields['metafields'] as $productMetafield) {
													if ($productMetafield['key'] == 'catalogs' && $productMetafield['namespace'] == 'lms') {
														$metafieldId = $productMetafield['id'];
													}
												}
											}
											// 2) update metafield value
											if (!empty($metafieldId)) {
												$result = $apiClient->call(
													'PUT',
													'/admin/products/' . $shopifyProductId . '/metafields/' . $metafieldId . '.json',
													array(
														"id" => $metafieldId,
														"value" => $catStr,
														"value_type" => "string"
													)
												);
											} else {
												//The product has no "catalogs" metafield assigned yet. Create it now.
												$result = $apiClient->call(
													'POST',
													'/admin/products/' . $shopifyProductId . '/metafields.json',
													array(
														"metafield" => array(
															"namespace" => "lms",
															"key" => "catalogs",
															"value" => $catStr,
															"value_type" => "string"
														)
													)
												);
											}
										}
									}
								}
							}

						}
					}
					//===

					break;
			}

			//==========================
			//=== end execution part ===
			//==========================

		} catch(Exception $e) {

			Yii::log("ERROR:\n".$e->getMessage()."\n\nSTACK TRACE:\n".$e->getTraceAsString(), 'error');
		}

	}

}