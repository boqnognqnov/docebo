<?php

class ShopifyProductProxy
{

	public static function replicateCoursesAndPlansFromLmsToShopify()
	{
		//set_time_limit(50000);

		$counter = 0;

		$sql = "SELECT idCourse FROM ".LearningCourse::model()->tableName()." WHERE selling = 1";
		$list = Yii::app()->db->createCommand($sql)->queryColumn();
		if (is_array($list) && !empty($list)) {
			foreach ($list as $idCourse) {
				$courseModel = LearningCourse::model()->findByPk($idCourse);
				if (!empty($courseModel)) {
					self::createOrUpdateProductOnShopify($courseModel);
				} else {
					//TODO: throwing an exception ?!?
				}
				$counter++;
			}
		}

		$sql = "SELECT id_path FROM ".LearningCoursepath::model()->tableName()." WHERE is_selling = 1";
		$list = Yii::app()->db->createCommand($sql)->queryColumn();
		if (is_array($list) && !empty($list)) {
			foreach ($list as $idPath) {
				$pathModel = LearningCoursepath::model()->findByPk($idPath);
				if (!empty($pathModel)) {
					self::createOrUpdateProductOnShopify($pathModel);
				} else {
					//TODO: throwing an exception ?!?
				}
				$counter++;
			}
		}

		return $counter;
	}


	public static function createOrUpdateProductOnShopify($courseOrPathVO)
	{
		$apiClient = ShopifyAPIClient::create();

		$isCourseAndSelling = ($courseOrPathVO instanceof LearningCourse && $courseOrPathVO->selling > 0);
		$isPathAndSelling = ($courseOrPathVO instanceof LearningCoursepath && $courseOrPathVO->is_selling > 0);
		if (!$isCourseAndSelling && !$isPathAndSelling) {
			return;
		}

		$shopifyProductId = self::getReplicatedCourseOrPathShopifyProductId($courseOrPathVO);

		$isNewProduct = false;
		if ($shopifyProductId == null) {
			self::createProduct($apiClient, $courseOrPathVO);
			$shopifyProductId = self::getReplicatedCourseOrPathShopifyProductId($courseOrPathVO);
			$isNewProduct = true;
		}

		try {
			self::updateProduct($apiClient, $courseOrPathVO, $shopifyProductId, $isNewProduct);
		} catch (ShopifyApiException $exception) {
			try {
				if ($exception->getResponse()['errors'] == 'Not Found') {
					//product has been deleted on shopify, but the LMS has not been updated, so try to re-synch here
					self::removeCourseOrPathShopifyProductId($courseOrPathVO);
					self::createOrUpdateProductOnShopify($courseOrPathVO);
				}
			} catch (Exception $ex) {
				Yii::log('ShopifyProductProxy->updateProduct 0.2695 ', CLogger::LEVEL_ERROR);
			}
		}

	}




	public static function deleteProductOnShopify($courseOrPathVO, $deleteEvenNotSold = false)
	{
		$apiClient = ShopifyAPIClient::create();

		// first detect the type of input object
		$isCourse = $courseOrPathVO instanceof LearningCourse;
		$isPath = $courseOrPathVO instanceof LearningCoursepath;

		if (!$isCourse && !$isPath) return; //invalid/unknown object has been passed
		//TODO: should we raise an exception when input is of invalid type?

		//check if course/path is effectively to be deleted
		if (($isCourse && $courseOrPathVO->selling == 0 || $isPath && $courseOrPathVO->is_selling == 0) && !$deleteEvenNotSold) {
			return;
		}

		$shopifyProductId = self::getReplicatedCourseOrPathShopifyProductId($courseOrPathVO);

		if ($shopifyProductId != null) {
			$productDeletionResult = $apiClient->call(
				'DELETE',
				"/admin/products/$shopifyProductId.json",
				array()
			);
		}

		if ($isCourse) {

			$idCourse = $courseOrPathVO->idCourse;
			LearningCourseShopifyProduct::model()->deleteAll('idCourse = :idCourse', array(':idCourse' => $idCourse));
			$courseType = $courseOrPathVO->course_type;
			switch ($courseType) {
				case LearningCourse::TYPE_CLASSROOM:
					$sessionsToRemove = LtCourseSession::model()->findAll('course_id = :course_id', array(':course_id' => $idCourse));
					foreach ($sessionsToRemove as $sessionVO) {
						self::deleteProductVariantOnShopify($sessionVO);
					}
					break;
				case LearningCourse::TYPE_WEBINAR:
					$sessionsToRemove = WebinarSession::model()->findAll('course_id = :course_id', array(':course_id' => $idCourse));
					foreach ($sessionsToRemove as $sessionVO) {
						self::deleteProductVariantOnShopify($sessionVO);
					}
					break;
			}

		} elseif ($isPath) {

			LearningCoursepathShopifyProduct::model()->deleteAll('id_path = :id_path', [':id_path' => $courseOrPathVO->id_path]);
		}
	}


	private static function getReplicatedSessionShopifyProductVariantId($sessionModel)
	{
		$shopifyProductVariant = null;

		if ($sessionModel instanceof WebinarSession) {
			$shopifyProductVariant = WebinarSessionShopifyVariant::model()->find(
				'id_session = :id_session',
				[':id_session' => $sessionModel->id_session]
			);
		} else {
			$shopifyProductVariant = LtCourseSessionShopifyVariant::model()->find(
				'id_session = :id_session',
				[':id_session' => $sessionModel->id_session]
			);
		}

		return ($shopifyProductVariant != null) ? $shopifyProductVariant->shopifyVariantId : null;
	}



	private static function productExists($shopifyProductId, $apiClient)
	{
		try {
			$productDeletionResult =
				$apiClient->call
				(
					'GET',
					"/admin/products/$shopifyProductId.json",
					[]
				);
			return true;
		} catch (Exception $ex) {
		}

		return false;
	}



	public static function createOrUpdateProductVariantOnShopify($sessionModel)
	{
		$apiClient = ShopifyAPIClient::create();

		$courseModel = $sessionModel->course;
		if (empty($courseModel)) {
			// TODO: should we throw an exception here ?
			return;
		}

		$shopifyProduct = LearningCourseShopifyProduct::model()->find('idCourse = :idCourse', array(':idCourse' => $courseModel->idCourse));
		if (empty($shopifyProduct)) {
			ShopifyProductProxy::createOrUpdateProductOnShopify($courseModel); // NOTE: product variants are already handled in that function
			return;
		}

		if (!self::productExists($shopifyProduct->shopifyProductId, $apiClient)) {
			ShopifyProductProxy::createOrUpdateProductOnShopify($courseModel);  // NOTE: product variants are already handled in that function
			return;
		}

		$shopifyProductVariantId = self::getReplicatedSessionShopifyProductVariantId($sessionModel);
		if (empty($shopifyProductVariantId)) {
			self::createProductVariant($apiClient, $sessionModel);
		} else {
			self::updateProductVariant($apiClient, $sessionModel, $shopifyProductVariantId, false);
		}
	}


	public static function deleteProductVariantOnShopify($sessionModel)
	{
		$apiClient =
			ShopifyAPIClient::create();

		$shopifyProductVariantId = self::getReplicatedSessionShopifyProductVariantId($sessionModel);
		if ($shopifyProductVariantId != null) {
			$idCourse = $sessionModel->course_id;
			$shopifyProductId =
				LearningCourseShopifyProduct::model()->find('idCourse = :idCourse', [':idCourse' => $idCourse])->shopifyProductId;
			try {
				$productVariantDeletionResult =
					$apiClient->call
					(
						'DELETE',
						"/admin/products/$shopifyProductId/variants/$shopifyProductVariantId.json",
						[]
					);
			} catch (Exception $ex) {
			}
		}

		if ($sessionModel instanceof WebinarSession)
			WebinarSessionShopifyVariant::model()->deleteAll('id_session = :id_session', [':id_session' => $sessionModel->id_session]);
		else
			LtCourseSessionShopifyVariant::model()->deleteAll('id_session = :id_session', [':id_session' => $sessionModel->id_session]);
	}



	private static function removeCourseOrPathShopifyProductId($courseOrPath)
	{
		if ($courseOrPath instanceof LearningCourse) {
			LearningCourseShopifyProduct::model()->deleteAll('idCourse = :idCourse', [':idCourse' => $courseOrPath->idCourse]);
		}
		if ($courseOrPath instanceof LearningCoursepath) {
			LearningCoursepathShopifyProduct::model()->deleteAll('id_path = :id_path', [':id_path' => $courseOrPath->id_path]);
		}
	}



	private static function getReplicatedCourseOrPathShopifyProductId($courseOrPath)
	{
		$shopifyProduct = null;

		if ($courseOrPath instanceof LearningCourse) {
			$shopifyProduct = LearningCourseShopifyProduct::model()->find('idCourse = :idCourse', [':idCourse' => $courseOrPath->idCourse]);
		}
		if ($courseOrPath instanceof LearningCoursepath) {
			$shopifyProduct = LearningCoursepathShopifyProduct::model()->find('id_path = :id_path', [':id_path' => $courseOrPath->id_path]);
		}

		return (!empty($shopifyProduct) ? $shopifyProduct->shopifyProductId : null);
	}


	private static function createProduct($apiClient, $courseOrPathVO)
	{
		try {
			$productCreationResult = $apiClient->call(
				'POST',
				'/admin/products.json',
				array(
					'product' => self::getProductVO($courseOrPathVO)
				)
			);

			ob_start();
			$resultStr = ob_get_clean();

			if ($courseOrPathVO instanceof LearningCourse) {
				$courseShopifyProduct = new LearningCourseShopifyProduct();
				$courseShopifyProduct->idCourse = $courseOrPathVO->idCourse;
				$courseShopifyProduct->shopifyProductId = $productCreationResult['id'];
				$courseShopifyProduct->shopifyProductMeaningfulId = $productCreationResult['handle'];
				$courseShopifyProduct->save();

				$courseType = $courseOrPathVO->course_type;
				if ($courseType == LearningCourse::TYPE_CLASSROOM || $courseType == LearningCourse::TYPE_WEBINAR) {
					self::replicateCourseSessions($courseOrPathVO);
				}
			}
			if ($courseOrPathVO instanceof LearningCoursepath) {
				$pathShopifyProduct = new LearningCoursepathShopifyProduct();
				$pathShopifyProduct->id_path = $courseOrPathVO->id_path;
				$pathShopifyProduct->shopifyProductId = $productCreationResult['id'];
				$pathShopifyProduct->shopifyProductMeaningfulId = $productCreationResult['handle'];
				$pathShopifyProduct->save();
			}

		} catch (ShopifyApiException $shopifyException) {
			ob_start();

			Yii::log("ShopifyProductProxy->createOrUpdateProductOnShopify 7 " . ob_get_clean(), CLogger::LEVEL_ERROR);
			Yii::log("ShopifyProductProxy->createOrUpdateProductOnShopify 7.4 " . $shopifyException->getMessage(), CLogger::LEVEL_ERROR);

		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
		}
	}




	private static function replicateCourseSessions(LearningCourse $courseVO)
	{
		$apiClient = ShopifyAPIClient::create();
		$courseType = $courseVO->course_type;

		switch ($courseType) {

			case LearningCourse::TYPE_CLASSROOM:
				$sql = "SELECT * FROM ".LtCourseSession::model()->tableName()." WHERE course_id = :course_id";
				$reader = Yii::app()->db->createCommand($sql)->query(array(':course_id' => $courseVO->idCourse));
				if ($reader) {
					while ($record = $reader->read()) {
						$sessionModel = LtCourseSession::model()->findByPk($record['id_session']);
						if (empty($sessionModel)) {
							self::createProductVariant($apiClient, $sessionModel);
						}
					}
				}
				break;

			case LearningCourse::TYPE_WEBINAR:
				$sql = "SELECT * FROM ".WebinarSession::model()->tableName()." WHERE course_id = :course_id";
				$reader = Yii::app()->db->createCommand($sql)->query(array(':course_id' => $courseVO->idCourse));
				if ($reader) {
					while ($record = $reader->read()) {
						$sessionModel = WebinarSession::model()->findByPk($record['id_session']);
						if (empty($sessionModel)) {
							self::createProductVariant($apiClient, $sessionModel);
						}
					}
				}
				break;

		}

	}


	private static function prepareSessionMetafields($sessionVO)
	{
		if ($sessionVO instanceof LtCourseSession) {
			/*
			// NOTE: old logic, deprecated
			$datesStr = '';
			foreach ($sessionVO->learningCourseSessionDates as $date) {
				$datesStr .= $date->day . ' ' . $date->time_begin . '#';
				$datesStr .= $date->ltLocation->name . '#';
				$datesStr .= date('h:i:s', strtotime($date->time_end) - strtotime($date->time_begin)) . '|';
			}

			return
				array
				(
					array
					(
						"key" => "category",
						"value" => LearningCourse::TYPE_CLASSROOM,
						"value_type" => "string",
						"namespace" => "lms"
					),
					array
					(
						"key" => "dates",
						"value" => $datesStr,
						"value_type" => "string",
						"namespace" => "lms"
					),
				);
			*/

			$allDates = [];

			$duration_minutes = 0;
			$sessionDateVO = NULL;
			$startDateUTC = '-';
			foreach ($sessionVO->learningCourseSessionDates as $date) {
				$updateStartDateUTC = false;
				if (empty($sessionDateVO) || !is_object($sessionDateVO) || !($sessionDateVO instanceof LtCourseSessionDate)) {
					//retrieving the first valid webinar session date model available
					$sessionDateVO = $date;
					$updateStartDateUTC = true;
				}
				//calculating the total time amount of all dates in minutes
				$duration_minutes += $date->countMinutes();

				$dt = new DateTime($date->day.' '.$date->time_begin, new DateTimeZone($date->timezone));
				$dt->setTimezone(new DateTimeZone('UTC'));
				$utcStartDate = $dt->format('Y-m-d H:i:s');
				if ($updateStartDateUTC) { $startDateUTC = $utcStartDate; }

				$allDates[] = [
					'start_date' => $date->day.' '.$date->time_begin,
					'timezone' => $date->timezone,
					'start_date_utc' => $utcStartDate,
					'duration' => $date->countMinutes(),
					'name' => $date->name,
					'location' => (!empty($date->getLocation()) ? $date->getLocation()->name : false),
					'classroom' => (!empty($date->getClassroom()) ? $date->getClassroom()->name : false)
				];
			}

			//make sure that minutes duration is in proper format
			$duration_minutes_str = number_format($duration_minutes, 2, '.', '');
			//remove eventual ".00" or simply "0" from decimals
			$duration_minutes_str = rtrim($duration_minutes_str, '0');
			$duration_minutes_str = rtrim($duration_minutes_str, '.');

			$location = (!empty($sessionDateVO) ? $sessionDateVO->getLocation() : null);
			$classroom = (!empty($sessionDateVO) ? $sessionDateVO->getClassroom() : null);

			return
				array
				(
					array
					(
						"key" => "category",
						"value" => LearningCourse::TYPE_CLASSROOM,
						"value_type" => "string",
						"namespace" => "lms"
					),
					array(
						"key" => "session_name",
						"value" => (!empty($sessionVO) ? trim($sessionVO->name) : ''),
						"value_type" => "string",
						"namespace" => "lms"
					),
					array
					(
						"key" => "start_date",
						"value" => (!empty($sessionDateVO) ? $sessionDateVO->day." ".$sessionDateVO->time_begin : '-'),
						"value_type" => "string",
						"namespace" => "lms"
					),
					array
					(
						"key" => "timezone",
						"value" => (!empty($sessionDateVO) ? $sessionDateVO->timezone : ''),
						"value_type" => "string",
						"namespace" => "lms"
					),
					array
					(
						"key" => "start_date_utc",
						"value" => $startDateUTC,
						"value_type" => "string",
						"namespace" => "lms"
					),
					array
					(
						"key" => "duration",
						"value" => $duration_minutes_str."m",
						"value_type" => "string",
						"namespace" => "lms"
					),
					array
					(
						"key" => "location",
						"value" => (!empty($location) ? $location->name : ''),
						"value_type" => "string",
						"namespace" => "lms"
					),
					array
					(
						"key" => "classroom",
						"value" => (!empty($classroom) ? $classroom->name : ''),
						"value_type" => "string",
						"namespace" => "lms"
					),
					array
					(
						"key" => "all_dates_json",
						"value" => CJSON::encode($allDates),
						"value_type" => "string",
						"namespace" => "lms"
					),
				);
		} else
			if ($sessionVO instanceof WebinarSession) {
//				$sql = "SELECT TIMEDIFF(date_end, date_begin) FROM webinar_session WHERE id_session = :id_session";
//
//				$duration = Yii::app()->db->createCommand($sql)->queryScalar(array(':id_session' => $sessionVO->id_session));
				/*
				$hours = substr($duration, 2);
				$days = inval($hours / 24);
				$hours = $hours - $days * 24;
				*/
				$link = json_decode($sessionVO->tool_params, true)['join_url'];
				if ($link == null && json_decode($sessionVO->tool_params, true)['webinar_id'] != null) {
					$link = json_decode($sessionVO->tool_params, true)['webinar_id']['webinarKey'];
				}

				//=== additional checks on URL ===
				// NOTE: this is a temporary patch, a better method should be implemented later
				if (!is_string($link)) {
					if (is_array($link) && isset($link['hostURL'])) {
						$link = $link['hostURL'];
					} else {
						$link = '';
					}
				}
				//===

				$allDates = [];
				$duration_minutes = 0;
				$sessionDateVO = NULL;
				$startDateUTC = '-';
				foreach ($sessionVO->webinarSessionDates as $date) {
					$updateStartDateUTC = false;
					if (empty($sessionDateVO) || !is_object($sessionDateVO) || !($sessionDateVO instanceof WebinarSessionDate)) {
						//retrieving the first valid webinar session date model available
						$sessionDateVO = $date;
						$updateStartDateUTC = true;
					}
					//calculating the total time amount of all dates in minutes
					$duration_minutes += $date->duration_minutes;

					$dt = new DateTime($date->day.' '.$date->time_begin, new DateTimeZone($date->timezone_begin));
					$dt->setTimezone(new DateTimeZone('UTC'));
					$utcStartDate = $dt->format('Y-m-d H:i:s');
					if ($updateStartDateUTC) { $startDateUTC = $utcStartDate; }

					$allDates[] = [
						'start_date' => $date->day.' '.$date->time_begin,
						'timezone' => $date->timezone_begin,
						'start_date_utc' => $utcStartDate,
						'name' => $date->name,
						'duration' => $date->duration_minutes,
						'tool' => $date->webinar_tool
					];
				}

				return
					array
					(
						array
						(
							"key" => "category",
							"value" => LearningCourse::TYPE_WEBINAR,
							"value_type" => "string",
							"namespace" => "lms"
						),
						array(
							"key" => "session_name",
							"value" => (!empty($sessionVO) ? trim($sessionVO->name) : ''),
							"value_type" => "string",
							"namespace" => "lms"
						),
						array
						(
							"key" => "start_date",
							"value" => (!empty($sessionDateVO) ? $sessionDateVO->day." ".$sessionDateVO->time_begin : '-'),
							"value_type" => "string",
							"namespace" => "lms"
						),
						array
						(
							"key" => "timezone",
							"value" => (!empty($sessionDateVO) ? $sessionDateVO->timezone_begin : ''),
							"value_type" => "string",
							"namespace" => "lms"
						),
						array
						(
							"key" => "start_date_utc",
							"value" => $startDateUTC,
							"value_type" => "string",
							"namespace" => "lms"
						),
						array
						(
							"key" => "duration",
							"value" => $duration_minutes."m",
							"value_type" => "string",
							"namespace" => "lms"
						),
						array
						(
							"key" => "webinar_link",
							"value" => $link,
							"value_type" => "string",
							"namespace" => "lms"
						),
						array
						(
							"key" => "tool",
							"value" => (!empty($sessionDateVO) ? $sessionDateVO->webinar_tool : '-'),
							"value_type" => "string",
							"namespace" => "lms"
						),
						array
						(
							"key" => "all_dates_json",
							"value" => CJSON::encode($allDates),
							"value_type" => "string",
							"namespace" => "lms"
						),
					);
			}
	}




	private static function createProductVariant($apiClient, $sessionVO, $price = false)
	{
		$courseVO = LearningCourse::model()->findByPk($sessionVO->course_id);

		$productShopifyModel = LearningCourseShopifyProduct::model()->find('idCourse = :idCourse', array(':idCourse' => $courseVO->idCourse));
		if (!empty($productShopifyModel)) { $productShopifyId = $productShopifyModel->shopifyProductId; }

		$imageShopifyId = 0;
		$productResult = $apiClient->call(
			'GET',
			"/admin/products/$productShopifyId.json?fields=images"
		);

		try {
			if (is_array($productResult) && isset($productResult['images'])) {
				if (is_array($productResult['images']) && !empty($productResult['images'])) {
					if (is_array($productResult['images'][0]) && isset($productResult['images'][0]['id'])) {
						$imageShopifyId = $productResult['images'][0]['id'];
					}
				}
			}
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
		}

		$t = array(
			'variant' => array(
				'title' => self::retrieveSessionTitle($courseVO, $sessionVO),
				'price' => ($price !== false ? $price : $courseVO->prize), //if price has not been specified, then inherit the course price
				'option1' => self::retrieveSessionTitle($courseVO, $sessionVO),
				'product_id' => $productShopifyId,
				'image_id' => $imageShopifyId,
				'inventory_management' => 'shopify',
				'inventory_policy' => 'deny',
				'inventory_quantity' => $sessionVO->max_enroll,
				'requires_shipping' => false,
				'taxable' => true,
				'metafields' => self::prepareSessionMetafields($sessionVO),
			)
		);
		try {
			$productVariantStoreResult = $apiClient->call(
				'POST',
				"/admin/products/$productShopifyId/variants.json",
				$t
			);
		} catch (ShopifyApiException $ex) {

			try {
				$alreadyExists = false;
				$response = $ex->getResponse();
				if (isset($response['errors']['base']) && strpos($response['errors']['base'][0], 'already exists.') !== false) {
					$alreadyExists = true;
				}

				if ($alreadyExists !== false) {
					$shopifyProductVariantId = 0;
					$productVariantListResult = $apiClient->call(
						'GET',
						"/admin/products/$productShopifyId/variants.json"
					);
					$productVariant = null;
					foreach ($productVariantListResult as $productVariantListResultItem) {
						if ($productVariantListResultItem['title'] == self::retrieveSessionTitle($courseVO, $sessionVO)) {
							$shopifyProductVariantId = $productVariantListResultItem['id'];
							$productVariant = $productVariantListResultItem;
							break;
						}
					}

					if ($shopifyProductVariantId != 0) {
						self::removeVariantMetafields($apiClient, $shopifyProductVariantId);
						$productVariantStoreResult = $apiClient->call(
							'PUT',
							"/admin/variants/$shopifyProductVariantId.json",
							array(
								'variant' => array(
									'title' => self::retrieveSessionTitle($courseVO, $sessionVO),
									'price' => $courseVO->prize,
									'option1' => self::retrieveSessionTitle($courseVO, $sessionVO),
									'product_id' => $productShopifyId,
									'image_id' => $imageShopifyId,
									"inventory_management" => "shopify",
									"inventory_policy" => "deny",
									"inventory_quantity" => $sessionVO->max_enroll,
									'requires_shipping' => false,
									'taxable' => true,
									"metafields" => self::prepareSessionMetafields($sessionVO),
								)
							)
						);
					}
				}
			} catch (Exception $ex) {
				Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
				//TODO: return here ?
			}
		}

		// Delete old Shopify variant ID from LMS database (whether it is present or not)
		if ($sessionVO instanceof WebinarSession) {
			WebinarSessionShopifyVariant::model()->deleteAll('id_session = :id_session', array(':id_session' => $sessionVO->id_session));
		} else {
			LtCourseSessionShopifyVariant::model()->deleteAll('id_session = :id_session', array(':id_session' => $sessionVO->id_session));
		}

		//create a new Shopify variant ID record in the LMS database, putting the newly obtained value from API call
		if (isset($productVariantStoreResult) && !empty($productVariantStoreResult) && isset($productVariantStoreResult['id'])) {
			$sessionShopifyVariant = null;
			if ($sessionVO instanceof WebinarSession) {
				$sessionShopifyVariant = new WebinarSessionShopifyVariant();
			} else {
				$sessionShopifyVariant = new LtCourseSessionShopifyVariant();
			}
			$sessionShopifyVariant->id_session = $sessionVO->id_session;
			$sessionShopifyVariant->shopifyVariantId = $productVariantStoreResult['id'];
			$sessionShopifyVariant->price = floatval($productVariantStoreResult['price']);
			$sessionShopifyVariant->save();
		} else {
			//TODO: what to do now ? what about throwing an exception ?
		}

		self::deleteDefaultProductVariantOnShopify($apiClient, $productShopifyId);
	}


	private static function removeVariantMetafields($apiClient, $shopifyProductVariantId)
	{

		try {
			$getProductVariantMetafieldsResult = $apiClient->call(
				'GET',
				"/admin/variants/$shopifyProductVariantId/metafields.json",
				array()
			);
		} catch (ShopifyApiException $ex) {
			ob_start();
			Yii::log('removeVariantMetafields 1 ' . ob_get_clean() . ' ' . $ex->getMessage(), CLogger::LEVEL_ERROR);
			return;
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return;
		}

		foreach ($getProductVariantMetafieldsResult as $metafieldVO) {
			$metafieldId = $metafieldVO['id'];
			try {
				$removeProductMetafieldResult = $apiClient->call(
					'DELETE',
					"/admin/variants/$shopifyProductVariantId/metafields/$metafieldId.json",
					array()
				);
			} catch (Exception $ex) {
				Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			}
		}
	}



	private static function deleteDefaultProductVariantOnShopify($apiClient, $productShopifyId)
	{
		$productVariantsResult = $apiClient->call(
			'GET',
			"/admin/products/$productShopifyId/variants.json?fields=id,option1"
		);
		foreach ($productVariantsResult as $variantVO) {
			if ($variantVO['option1'] == 'Default Title') {
				$productVariantShopifyId = $variantVO['id'];
				$apiClient->call
				(
					'DELETE',
					"/admin/products/$productShopifyId/variants/$productVariantShopifyId.json"
				);
				break;
			}
		}
	}


	private static function removeProductMetafields($apiClient, $shopifyProductId)
	{
		try {
			$getProductMetafieldsResult = $apiClient->call(
				'GET',
				"/admin/products/$shopifyProductId/metafields.json",
				array()
			);
		} catch (ShopifyApiException $ex) {
			ob_start();
			Yii::log('removeProductMetafields 1 ' . ob_get_clean() . ' ' . $ex->getMessage(), CLogger::LEVEL_ERROR);
			return;
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return;
		}

		foreach ($getProductMetafieldsResult as $metafieldVO) {
			$metafieldId = $metafieldVO['id'];
			try {
				$removeProductMetafieldResult = $apiClient->call(
					'DELETE',
					"/admin/products/$shopifyProductId/metafields/$metafieldId.json",
					array()
				);
			} catch (Exception $ex) {
				Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			}
		}
	}

	
	private static function updateProduct($apiClient, $courseOrPathVO, $shopifyProductId, $isNewProduct)
	{
		$productVO = self::getProductVO($courseOrPathVO);
		$productVO['id'] = $shopifyProductId;
		$productVO['metafields'] = [];

		try {
			self::removeProductMetafields($apiClient, $shopifyProductId);
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
		}

		if ($courseOrPathVO instanceof LearningCourse) {
			try {
				$category =
					(string)
					(
					LearningCourseCategory::model()
						->find
						(
							"idCategory = :idCategory and lang_code = 'english'",
							['idCategory' => $courseOrPathVO->idCategory]
						)->translation
					);
				if ($category != '')
					$productVO['metafields'][] =
						[
							'key' => 'category',
							'value' => $category,
							'value_type' => 'string',
							'namespace' => 'lms',
						];
			} catch (Exception $ex) {
				Yii::log('metafields error: 771 ' . $ex->getMessage());
			}

			try {
				$language = (string)($courseOrPathVO->lang_code);
				if ($language != '')
					$productVO['metafields'][] =
						[
							'key' => 'language',
							'value' => $language,
							'value_type' => 'string',
							'namespace' => 'lms',
						];
			} catch (Exception $ex) {
				Yii::log('metafields error: 772 ' . $ex->getMessage());
			}

			try {
				$labels = (string)(self::extractLearningLabelsOfCourse($courseOrPathVO->idCourse));
				if ($labels != '')
					$productVO['metafields'][] =
						[
							'key' => 'labels',
							'value' => $labels,
							'value_type' => 'string',
							'namespace' => 'lms',
						];
			} catch (Exception $ex) {
				Yii::log('metafields error: 773 ' . $ex->getMessage());
			}
		} else {
			try {
				$daysValid = (string)($courseOrPathVO->days_valid);
				if ($daysValid != '')
					$productVO['metafields'][] =
						[
							'key' => 'days_valid',
							'value' => $daysValid,
							'value_type' => 'string',
							'namespace' => 'lms',
						];
			} catch (Exception $ex) {
				Yii::log('metafields error: 774 ' . $ex->getMessage());
			}

			try {
				$certificate = (string)(self::getCertificateFromId($courseOrPathVO->certificate->id_certificate));
				if ($certificate != '')
					$productVO['metafields'][] =
						[
							'key' => 'certificate',
							'value' => $certificate,
							'value_type' => 'string',
							'namespace' => 'lms',
						];
			} catch (Exception $ex) {
				Yii::log('metafields error: 775 ' . $ex->getMessage());
			}
		}

		try {
			$typeOfEntry =
				($courseOrPathVO instanceof LearningCourse)
					? 'course'
					: 'coursepath';
			$catalogs =
				LearningCatalogueEntry::model()->
				findAll(
					"idEntry = :idEntry and type_of_entry = '$typeOfEntry'",
					array(
						':idEntry' =>
							($courseOrPathVO instanceof LearningCourse)
								? $courseOrPathVO->idCourse
								: $courseOrPathVO->id_path
					)
				);
			$catLst = array();
			foreach ($catalogs as $catalog) {
				$id = $catalog->idCatalogue;
				$cat = LearningCatalogue::model()->findByPk($id)->name;
				array_push($catLst, $cat);
			}
			$catalogs = implode(', ', $catLst);

			if ($catalogs != '')
				$productVO['metafields'][] =
					[
						'key' => 'catalogs',
						'value' => $catalogs,
						'value_type' => 'string',
						'namespace' => 'lms',
					];
		} catch (Exception $ex) {
			Yii::log('metafields error: 773 ' . $ex->getMessage());
		}

		try {
			$credits = (string)($courseOrPathVO->credits);
			if ($credits != '')
				$productVO['metafields'][] =
					[
						'key' => 'credits',
						'value' => $credits,
						'value_type' => 'string',
						'namespace' => 'lms',
					];
		} catch (Exception $ex) {
			Yii::log('metafields error: 776 ' . $ex->getMessage());
		}

		try {
			$url = (string)(self::getProductUrl($courseOrPathVO));
			if ($url != '')
				$productVO['metafields'][] =
					[
						'key' => 'lms_url',
						'value' => $url,
						'value_type' => 'string',
						'namespace' => 'lms',
					];
		} catch (Exception $ex) {
			Yii::log('metafields error: 777 ' . $ex->getMessage());
		}

		$productUpdateResult =
			$apiClient->call
			(
				'PUT',
				"/admin/products/$shopifyProductId.json",
				[
					'product' => $productVO
				]
			);

		if ($courseOrPathVO instanceof LearningCourse) {
			//save old variant prices
			//TODO: make a better optimized process here
			$oldVariantPrices = ['ilt' => [], 'webinar' => []];
			$rd = Yii::app()->db->createCommand("SELECT * FROM " . LtCourseSessionShopifyVariant::model()->tableName())->query();
			while ($record = $rd->read()) {
				$oldVariantPrices['ilt'][$record['id_session']] = $record['price'];
			}
			$rd = Yii::app()->db->createCommand("SELECT * FROM " . WebinarSessionShopifyVariant::model()->tableName())->query();
			while ($record = $rd->read()) {
				$oldVariantPrices['webinar'][$record['id_session']] = $record['price'];
			}
		}


		// update price on existing product variants
		for ($i = 0; $i < count($productUpdateResult['variants']); $i++) {
			$productVariantId = $productUpdateResult['variants'][$i]['id'];

			$parameters =
				[
					'variant' =>
						[
							'id' => $productVariantId,
							'requires_shipping' => false,
							'taxable' => true,
						]
				];
			if ($isNewProduct) {
				// Since the control on the price is on Shopify, then prevent the LMS to set this property, unless
				// the product is being created on Shopify by the LMS itself
				$parameters['variant']['price'] =
					(
					$courseOrPathVO instanceof LearningCourse
						? $courseOrPathVO->prize
						: $courseOrPathVO->price
					);
			}

			$productVariantUpdateResult =
				$apiClient->call
				(
					'PUT',
					"/admin/variants/$productVariantId.json",
					$parameters
				);

			try {
				$productVariantUpdateResult =
					$apiClient->call
					(
						'DELETE',
						"/admin/products/$shopifyProductId/variants/$productVariantId.json"
					);
				WebinarSessionShopifyVariant::model()->deleteAll('shopifyVariantId = :shopifyVariantId', [':shopifyVariantId' => $productVariantId]);
				LtCourseSessionShopifyVariant::model()->deleteAll('shopifyVariantId = :shopifyVariantId', [':shopifyVariantId' => $productVariantId]);
			} catch (ShopifyApiException $ex) {
				$ex->getResponse();
			} catch (Exception $ex) {
				$ex->getResponse();
			}

		}

		if ($courseOrPathVO instanceof LearningCourse) {
			if ($courseOrPathVO->course_type == LearningCourse::TYPE_CLASSROOM) {
				$courseSessions = LtCourseSession::model()->findAll('course_id = :course_id', [':course_id' => $courseOrPathVO->idCourse]);
				foreach ($courseSessions as $courseSession) {
					LtCourseSessionShopifyVariant::model()->deleteAll('id_session = :id_session', [':id_session' => $courseSession->id_session]);
					$_price = (isset($oldVariantPrices['ilt'][$courseSession->id_session]) ? $oldVariantPrices['ilt'][$courseSession->id_session] : false);
					self::createProductVariant(ShopifyAPIClient::create(), $courseSession, $_price);
				}
			} else
				if ($courseOrPathVO->course_type == LearningCourse::TYPE_WEBINAR) {
					$courseSessions = WebinarSession::model()->findAll('course_id = :course_id', [':course_id' => $courseOrPathVO->idCourse]);
					foreach ($courseSessions as $courseSession) {
						WebinarSessionShopifyVariant::model()->deleteAll('id_session = :id_session', [':id_session' => $courseSession->id_session]);
						$_price = (isset($oldVariantPrices['webinar'][$courseSession->id_session]) ? $oldVariantPrices['ilt'][$courseSession->id_session] : false);
						self::createProductVariant(ShopifyAPIClient::create(), $courseSession, $_price);
					}
				}
		}
	}


	private static function retrieveSessionTitle($courseVO, $sessionVO)
	{
		if ($courseVO->course_type == LearningCourse::TYPE_CLASSROOM) {
			$title =
				$sessionVO->learningCourseSessionDates[0]->day . ' ' .
				$sessionVO->learningCourseSessionDates[0]->time_begin . ' ' .
				$sessionVO->learningCourseSessionDates[0]->ltLocation->name;
		}
		if ($courseVO->course_type == LearningCourse::TYPE_WEBINAR) {

			/* @var $cmd CDbCommand */
			$cmd = Yii::app()->db->createCommand();
			//retrieve the first session date of the webinar
			$cmd->select("day, time_begin, webinar_tool, timezone_begin")
				->from(WebinarSessionDate::model()->tableName())
				->where("id_session = :id_session")
				->order('day ASC, time_begin ASC')
				->limit(1, 0);
			$row = $cmd->queryRow(true, array(':id_session' => $sessionVO->id_session));
			if (!empty($row)) {
				//do effective datetime conversion in local format, keeping as timezone the same one specified in the webinar session date record
				$ISO_dateBegin = $row['day'].' '.$row['time_begin']; //this should be in format "Y-m-d H:i:s"
				$tmp = new DateTime($ISO_dateBegin, new DateTimeZone($row['timezone_begin']));
				$dateBegin = $tmp->format('Y/m/d H:i:s'); // TODO: the output date format should be defined by parameters
				//prepare output, joining date and webinar tool information
				$title = $dateBegin . ' ' . $row['webinar_tool'];
			} else {
				$title = '';
			}
		}
		return $title;
	}


	private static function updateProductVariant($apiClient, $sessionVO, $shopifyProductVariantId, $isNewVariant = false)
	{
		self::removeVariantMetafields($apiClient, $shopifyProductVariantId);

		$courseVO = LearningCourse::model()->findByPk($sessionVO->course_id);

		$title = self::retrieveSessionTitle($courseVO, $sessionVO);

		$info = [
			'id' => $shopifyProductVariantId,
			'title' => $title,
			'option1' => $title,
			'requires_shipping' => false,
			'taxable' => true,
			"metafields" => self::prepareSessionMetafields($sessionVO),
		];
		if ($isNewVariant) {
			//variant has been newly created, so use the course price as beginning
			$info['price'] = $courseVO->prize;
		} else {
			//we stored the price previously from Shopify API
			if ($sessionVO instanceof LtCourseSession) {
				$t = LtCourseSessionShopifyVariant::model()->findByAttributes(['shopifyVariantId' => $shopifyProductVariantId, 'id_session' => $sessionVO->id_session]);
				if (!empty($t)) {
					if ($t->price > 0) { //security check : avoid to pass a price of 0, if that's the case then "fallback" on the main course price
						$info['price'] = number_format($t->price, 2, '.', '');
					} else {
						$info['price'] = $courseVO->prize;
					}
				}
			} else if ($sessionVO instanceof WebinarSession) {
				$t = WebinarSessionShopifyVariant::model()->findByAttributes(['shopifyVariantId' => $shopifyProductVariantId, 'id_session' => $sessionVO->id_session]);
				if (!empty($t)) {
					if ($t->price > 0) { //security check : avoid to pass a price of 0, if that's the case then "fallback" on the main course price
						$info['price'] = number_format($t->price, 2, '.', '');
					} else {
						$info['price'] = $courseVO->prize;
					}
				}
			}
		}
		$productVariantUpdateResult =
			$apiClient->call
			(
				'PUT',
				"/admin/variants/$shopifyProductVariantId.json",
				[
					'variant' =>
						$info
				]
			);
	}



	private static function getCertificateFromId($certId)
	{
		$certificateModel = LearningCertificate::model()->findByPk($certId);
		return (!empty($certificateModel) ? $certificateModel->name : null);
	}



	public static function getShopifyCurrency()
	{
		$apiClient = ShopifyAPIClient::create();
		$getShopCurrencyCodeResult = $apiClient->call(
			'GET',
			"/admin/shop.json",
			array()
		);
		return $getShopCurrencyCodeResult['currency'];
	}



	public static function shopifyCurrencyIs($currencyCode)
	{
		return (self::getShopifyCurrency() == $currencyCode);
	}




	private static function extractLearningLabelsOfCourse($idCourse)
	{
		$labelsRefsList = LearningLabelCourse::model()->findAll('id_course = :idCourse', [':idCourse' => $idCourse]);
		$labels = '';
		foreach ($labelsRefsList as $labelRef) {
			$labels .=
				', ' .
				LearningLabel::model()->find(
					'id_common_label = :idCommonLabel',
					[':idCommonLabel' => $labelRef->id_common_label]
				)->title;
		}
		$labels = substr($labels, 2);
		return $labels;
	}



	private static function getProductUrl($courseOrPathVO)
	{
		$result =
			(
			($_SERVER['SERVER_PORT'] == 443)
				? "https://"
				: "http://"
			) .
			$_SERVER['SERVER_NAME'] .
			(
			$courseOrPathVO instanceof LearningCourse
				?
				str_replace('admin', 'lms', Docebo::createAppUrl('player')) .
				"&course_id=" . $courseOrPathVO->idCourse
				:
				str_replace('admin', 'lms', Docebo::createAppUrl('curricula/show')) .
				"&id_path=" . $courseOrPathVO->id_path
			);
		return $result;
	}



	private static function getProductVO($courseOrPathVO)
	{
		$isCourse = $courseOrPathVO instanceof LearningCourse;
		$isPath = $courseOrPathVO instanceof LearningCoursepath;

		if ($isCourse) {
			$name = $courseOrPathVO->name;
			$description = $courseOrPathVO->description;
			$tags = $courseOrPathVO->getAllCategoriesTitlesWithoutRoot();
			$type = $courseOrPathVO->course_type;
		}
		if ($isPath) {
			$name = $courseOrPathVO->path_name;
			$description = $courseOrPathVO->path_descr;
			$tags = array();
			$type = 'learning_plan';
		}

		$test = self::getProductImageVO($courseOrPathVO);
		$productVO = array(
			'title' => $name,
			'metafields_global_title_tag' => $name,
			'metafields_global_description_tag' => $description,
			'body_html' => $description . '<br />' . '<a href="' . self::getProductUrl($courseOrPathVO) . '">' . $name . '</a>',
			'format' => 'json',
			'vendor' => Settings::get('title_organigram_chart'),
			'product_type' =>$type,
			'images' => array(self::getProductImageVO($courseOrPathVO)),
			'tags' => implode(',', $tags)
		);
		return $productVO;
	}


	private static function getProductImageVO($courseOrPathVO)
	{
		$isCourse = $courseOrPathVO instanceof LearningCourse;
		$isPath = $courseOrPathVO instanceof LearningCoursepath;

		$protocol = (($_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://");
		if ($isCourse) { $logoUrl = $courseOrPathVO->getCourseLogoUrl(); }
		if ($isPath) {$logoUrl = $courseOrPathVO->getLogo(); }

		$src = $protocol . substr($logoUrl, 2);
		$productImageVO = array(
			'src' => $src
		);

		return $productImageVO;
	}

}