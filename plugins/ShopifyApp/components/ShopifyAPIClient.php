<?php

class ShopifyAPIClient extends CComponent {

	public $name;
	public $shop_domain;
	private $token;
	private $api_key;
	private $secret;
	private $last_response_headers = null;
	private $private_api_key;
	private $private_password;
	private $shared_secret;

	/* Since Shopify has a limit on possible API calls during time, we try to manage it by code */
	protected $max_api_calls_per_second = 2;


	public function __construct($shop_domain, $private_api_key, $private_password, $shared_secret) {
		$this->name = "ShopifyClient";
		$this->shop_domain = $shop_domain;
//		$this->token = $token;
//		$this->api_key = $private_api_key;
//		$this->secret = $private_password;
		$this->private_api_key = $private_api_key;
		$this->private_password = $private_password;
		$this->shared_secret = $shared_secret;
	}


	public static function create($authenticated = true)
	{
		$inst = new ShopifyAPIClient(Settings::get('shopify_subdomain') . '.myshopify.com', Settings::get('shopify_private_api_key'), Settings::get('shopify_private_password'), Settings::get('shopify_shared_secret'));
		return $inst;
	}


	// Get the URL required to request authorization
//	public function getAuthorizeUrl($scope, $redirect_url='') {
//		$url = "http://{$this->shop_domain}/admin/oauth/authorize?client_id={$this->api_key}&scope=" . urlencode($scope);
//		if ($redirect_url != '')
//		{
//			$url .= "&redirect_uri=" . urlencode($redirect_url);
//		}
//		return $url;
//	}


	// Once the User has authorized the app, call this with the code to get the access token
//	public function getAccessToken($code) {
//		// POST to  POST https://SHOP_NAME.myshopify.com/admin/oauth/access_token
//		$url = "https://{$this->shop_domain}/admin/oauth/access_token";
//		$payload = "client_id={$this->api_key}&client_secret={$this->secret}&code=$code";
//		$response = $this->curlHttpApiRequest('POST', $url, '', $payload, array());
//		$response = json_decode($response, true);
//		if (isset($response['access_token']))
//			return $response['access_token'];
//		return '';
//	}


	public function callsMade()
	{
		return $this->shopApiCallLimitParam(0);
	}


	public function callLimit()
	{
		return $this->shopApiCallLimitParam(1);
	}


	public function callsLeft()
	{
		return $this->callLimit() - $this->callsMade();
	}


	public function call($method, $path, $params=array())
	{
		Yii::log('Shopify API call: '.$method.' '.$path.' '.CJSON::encode($params), CLogger::LEVEL_INFO);

		$this->startCheckingLastAPICallTime();

//		$baseurl = "https://{$this->shop_domain}/"; // for OAuth2
		$baseurl = "https://{$this->private_api_key}:{$this->private_password}@{$this->shop_domain}/";
		$url = $baseurl.ltrim($path, '/');
		$query = in_array($method, array('GET','DELETE')) ? $params : array();
		$payload = in_array($method, array('POST','PUT')) ? json_encode($params) : array();
		$request_headers = in_array($method, array('POST','PUT')) ? array("Content-Type: application/json; charset=utf-8", 'Expect:') : array();
		// add auth headers
//		$request_headers[] = 'X-Shopify-Access-Token: ' . $this->token;
		$response = $this->curlHttpApiRequest($method, $url, $query, $payload, $request_headers);

		$this->stopCheckingLastAPICallTime();

		$response = json_decode($response, true);
		if (isset($response['errors']) or ($this->last_response_headers['http_status_code'] >= 400)) {
			throw new ShopifyApiException($method, $path, $params, $this->last_response_headers, $response);
		}
		return (is_array($response) and (count($response) > 0)) ? array_shift($response) : $response;
	}


	public function validateSignature($query)
	{
		if(!is_array($query) || empty($query['signature']) || !is_string($query['signature']))
			return false;
		foreach($query as $k => $v) {
			if($k != 'shop' && $k != 'timestamp') continue;
			$signature[] = $k . '=' . $v;
		}
		sort($signature);

		$hmac = hash_hmac('sha256', implode('&', $signature), $this->secret); // md5($this->secret . implode('&', $signature));

		return $query['hmac'] == $hmac;
	}


	private function curlHttpApiRequest($method, $url, $query='', $payload='', $request_headers=array())
	{
		set_time_limit(50000);

		$url = $this->curlAppendQuery($url, $query);
		$ch = curl_init($url);
		$this->curlSetopts($ch, $method, $payload, $request_headers);
		$response = curl_exec($ch);
		$errno = curl_errno($ch);
		$error = curl_error($ch);
		curl_close($ch);
		if ($errno) throw new ShopifyCurlException($error, $errno);
		list($message_headers, $message_body) = preg_split("/\r\n\r\n|\n\n|\r\r/", $response, 2);
		$this->last_response_headers = $this->curlParseHeaders($message_headers);
		return $message_body;
	}


	private function curlAppendQuery($url, $query)
	{
		if (empty($query)) return $url;
		if (is_array($query)) return "$url?".http_build_query($query);
		else return "$url?$query";
	}


	private function curlSetopts($ch, $method, $payload, $request_headers)
	{
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_USERAGENT, 'ohShopify-php-api-client');
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, $method);
		if (!empty($request_headers)) curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
		
		if ($method != 'GET' && !empty($payload))
		{
			if (is_array($payload)) $payload = http_build_query($payload);
			curl_setopt ($ch, CURLOPT_POSTFIELDS, $payload);
		}
	}


	private function curlParseHeaders($message_headers)
	{
		$header_lines = preg_split("/\r\n|\n|\r/", $message_headers);
		$headers = array();
		list(, $headers['http_status_code'], $headers['http_status_message']) = explode(' ', trim(array_shift($header_lines)), 3);
		foreach ($header_lines as $header_line)
		{
			list($name, $value) = explode(':', $header_line, 2);
			$name = strtolower($name);
			$headers[$name] = trim($value);
		}
		return $headers;
	}


	private function shopApiCallLimitParam($index)
	{
		if ($this->last_response_headers == null)
		{
			throw new Exception('Cannot be called before an API call.');
		}
		$params = explode('/', $this->last_response_headers['http_x_shopify_shop_api_call_limit']);
		return (int) $params[$index];
	}


	/**
	 * This function should ensure that no more than {$this->max_api_calls_per_second} API calls are performed in a second
	 */
	protected function startCheckingLastAPICallTime() {
		//check current user
		$uid = Yii::app()->user->id;
		if (empty($uid) || is_null($uid)) {
			//maybe we are in a background async. job, with no current user itself?
			$uid = -1;
		}
		//start doing checks
		if ($this->max_api_calls_per_second <= 0) return; //no limit of API calls per second
		$time_slice = ceil(1000000 / $this->max_api_calls_per_second); //microseconds, time to wait between each API call
		if ($time_slice <= 0) return; //extreme case
		$last_api_call_time_J = Settings::get('shopify_last_api_call_time', null);
		$current_time_MT = microtime();
		if (empty($last_api_call_time_J)) {
			//this means that there have been no API calls yet on this server, so just set the setting and go on
			Settings::save('shopify_last_api_call_time', CJSON::encode(array(
				'start_time' => $current_time_MT,
				'end_time' => "",
				'user_calling' => $uid
			)));
		} else {
			//some API calls have already been made, check if enough time has passed
			$go_on = false;
			while (!$go_on) {
				//first check if someone else is already performing API calls in this moment
				$previous_value = CJSON::decode($last_api_call_time_J);
				if (empty($previous_value)) { throw new CException('Error while checking API call time'); }
				if ($previous_value['user_calling'] != 0 && empty($previous_value['end_time'])) {
					usleep(100000); //wait an arbitrary 0.1 seconds and then re-check
					$last_api_call_time_J = Settings::get('shopify_last_api_call_time', null);
					$current_time_MT = microtime();
				} else {
					$go_on = true;
				}
			}
			//reserve the right to API-call for the current user
			//(other user accessing the API calling function will wait until we are finished here, see above 'while' cycle)
			Settings::save('shopify_last_api_call_time', CJSON::encode(array(
				'start_time' => $current_time_MT,
				'end_time' => "",
				'user_calling' => $uid
			)));
			//check last API call ending time
			$last_api_call_time_MT = $previous_value['end_time'];
			list($current_time_uS, $current_time_S) = explode(' ', $current_time_MT);
			list($last_api_call_time_uS, $last_api_call_time_S) = explode(' ', $last_api_call_time_MT);
			$diff_S = (doubleval($current_time_S) - doubleval($last_api_call_time_S));
			$diff_uS = (doubleval($current_time_uS) - doubleval($last_api_call_time_uS));
			//NOTE: since max time slice is 1 second, $diff_S (difference in seconds) has just to be less than 1
			if (($diff_S < 1) && (floor($diff_uS * 1000000) < $time_slice)) {
				$waiting_time = (int)($time_slice - floor($diff_uS * 1000000));
				Yii::log('Shopify API call delay: '.number_format($waiting_time, 0, '', '').' microseconds', CLogger::LEVEL_INFO);
				usleep($waiting_time);
			}
		}
	}


	/**
	 * Finalize the API call checks
	 */
	protected function stopCheckingLastAPICallTime() {
		//check current user
		$uid = Yii::app()->user->id;
		if (empty($uid) || is_null($uid)) {
			//maybe we are in a background async. job, with no current user itself?
			$uid = -1;
		}
		//do checks
		$previous_value = Settings::get('shopify_last_api_call_time', null);
		if (empty($previous_value)) { return; } //this should not happen, anyway nothing else to do here
		$value = CJSON::decode($previous_value);
		if (empty($value) || $value['user_calling'] != $uid) { throw new CException('Error while checking API call time'); }
		//API call is finished, so set the end time and remove the user ID from calling parameter
		$value['end_time'] = microtime();
		$value['user_calling'] = 0;
		Settings::save('shopify_last_api_call_time', CJSON::encode($value));
	}
}



class ShopifyCurlException extends Exception {

}



class ShopifyApiException extends Exception
{
	protected $method;
	protected $path;
	protected $params;
	protected $response_headers;
	protected $response;
	
	function __construct($method, $path, $params, $response_headers, $response)
	{
		$this->method = $method;
		$this->path = $path;
		$this->params = $params;
		$this->response_headers = $response_headers;
		$this->response = $response;
		
		parent::__construct($response_headers['http_status_message'], $response_headers['http_status_code']);
	}

	function getMethod() { return $this->method; }

	function getPath() { return $this->path; }

	function getParams() { return $this->params; }

	function getResponseHeaders() { return $this->response_headers; }

	function getResponse() { return $this->response; }

}

