<?php

class ShopifyRegistrationProxy
{

//	public static function afterTokenReceivedActions()
//	{
//		ShopifyCustomerProxy::registerShopifyHooks();
//
//		self::alignLmsCurrencyToShopifyCurrency();
//		self::addCollections();
//	}



	public static function addCollections()
	{
		$apiClient = ShopifyAPIClient::create(true);

		$logoImages = array(
			'classroom' => 'iVBORw0KGgoAAAANSUhEUgAAAPAAAADwCAYAAAA+VemSAAAI1klEQVR4nO3dfWxV9R3H8U8faFN56EAotGOW8WCJQKMMh4CySDI1ZBqnIbiNDP8gm4uJrJo9OJclzixxLkQWM7bFJTM8uCwsJEsQM1xGKSIZTrYoGa0t3eighYb1SWqf790frBX0lnBv77nnfs59v/5qb5tzvzS8e37p+Z1787RlX1wALOWHPQCA1BEwYIyAAWMEDBgjYMAYAQPGCBgwRsCAMQIGjBEwYIyAAWMEDBgjYMAYAQPGCBgwRsCAMQIGjBEwYIyAAWMEDBgjYMAYAQPGCBgwRsCAMQIGjBEwYIyAAWMEDBgjYMAYAQPGCBgwRsCAMQIGjBEwYIyAAWMEDBgjYMAYAQPGCBgwRsCAMQIGjBEwYIyAAWMEDBgjYMAYAQPGCBgwRsCAMQIGjBEwYIyAAWMEDBgjYMAYAQPGCBgwRsCAMQIGjBEwYIyAAWMEDBgjYMAYAQPGCBgwRsCAMQIGjBEwYIyAAWMEDBgjYMAYAQPGCBgwRsCAMQIGjBEwYIyAAWMEDBgjYMAYAQPGCBgwRsCAMQIGjBEwYIyAAWMEDBgj4Ih6bOEs1T+9TvGXv6z6p9fpsYWzwh4JASDgCHqkolS/+M4aVc0vlSRVzb/8+SMVpSFPhnQj4Ah6eM085efnXfVYfn6eHl4zL5yBEJjCsAeIqkcrZ2jD2vkqn1mS8ede8JlpCR+/Z9VcnZib+GtBarvYp711zXrlTEfGnzvq8rRlXzzsIaJm6+LZ2v7U6rDHyDrf3vaWfl5/IewxIoUldACe2LAs7BGyEj+X9GMJHYD5N00d+3j3/ia1XvwwxGnCVTHzBm360kJJV/9ckB4EHLD9x1v0+7busMcIzcby0rGAkX4soQFjnIEDtqqqTBXTM/+X6GxRWcayOUgEHIC29g9VXnaDJGnr15aGPE32aGvP3b8FBIUldAB+88f6sEfISvxc0o8zcAB+dPyMBgZH9JV7FqlqfqkKCy7viurtG9bAwEjI0wWvuLhAk0su/9caHomroblbvzvYqJ/842zIk0UPGzkCdqLmLt12y0xJ0guvvKvvHT0d8kTB++maBfruo9WSpL//86KWv3gk5ImiiyU0YIyAAWMEDBgjYMAYAQPGuIyURR4sm6r1y+dq9ozkd251XRrUmyfP6+Xmi9f8vidvmaM7q8tV8LEb/hM5296r7XVNahyM/qUvVwScJR6/uUzbn1w9ds04FV+/f5EW73pPT9U1Jfz6+hsna1vNquRn+3NDyjMhWCyhs0BRnvTM5tsmFO+oJ766VLdPLkr4tZEUrvgPD8cmOBGCxBk4CywrKRrbOy1J+2tb1HVpMKljjN6yV1iQpyUzp+jt3k++fM2fOnr1+AtHtXJx2SdeMyuRCx192n6sOak5kFkEnAWKPhbTjtfr9XpHb1LHuPKe20kF4y+sdjS2a0dje3IDImuxhAaMcQbOMeUF+bq1dGL3J4/E4/prd5+6Y2yjDxsB55BFRQV64wfrVPnpKRM+1psnzuuuXx5Lw1SYCJbQOWTFrKlpiVeS7lw+R0uL+f0fNgLOIbXne3T83fT8AesPB5t1cmA4LcdC6vgVmkPaRmJa+dJRLZhUoLwJXHLuj8V1luvDWYGAc9DpIbZGRgVLaMAYZ+Ac89zKSq1ZNkfjbcQ62dylZw69zyUiEwScQx4sm6ofbll+ze/5wucr1HC2Uy81sFvLAUvoHNLZP6Th67ijoaN3KAPTIB04A+eQwz392vxcre6oKlNhgv3SsVhc7/27Q3vOdoYwHVJBwDnm1XNdevVcV9hjIE1YQgPGOANnodvnzdDcTwXzhmjVJZO0Yva063pJnfEMDMd0uLVLZ9jMEToCzgKn+4bU1zeskv+/Hcmz31oxoeOd6+5L+Hh1yST95dkv6sbpxRM6viSdfL9Tt/6sVmwJCRdL6CzQPhLTrtcSv45Vsur+1qYD/038YgBLZkxOS7yStPTm6VpYVJCWYyF1BJwlvv9GvbbvPqlzF1J7C86OrgHtea1J3/jt2+N+z4G2bu2vbdHQ0MSWvr19w/rV3lNq4NUqQ8cSOkt0xuKqOdyomsONgT1Hdyyu+/e8I+15J7DnQGZxBgaMETBgjIABYwQMGCNgwBgBA8YIGDBGwIAxNnIELBb/6Ab6xZWlqumcE+I0mbG4snTs4yv//Ug/Ag7YmbZefW7JLEnSA3dX6oG7K0OeKLPOtCX3Jm1IDkvogO061KT+gdzcM9w/MKJdh9JzkwYSy9OWfaxxArahvFTfXF+lz1ZMy9hzFhbm66aKyWOft7T2ZvTNuv/V2qNfH2jQ3rbujD1nLiLgiFo1pVhvvbh+7PPVNQd07NJAiBMhCCyhAWMEDBgjYMAYAUdQUZ609d6qqx7bem+ViibwjoTITgQcMUV50s6HqrXxvgVXPb7xvgXa+VA1EUcMAUfIePGOIuLoIeCISBRvS2uvNv+4Vi2tH+2GIuJoIeAIGC/eTdvqtPM/ndq0rY6II4qAzV0r3iM9/ZKkIz39RBxRBGzseuIdRcTRRMCmkol3FBFHDwEbSiXeUUQcLQRs6Pm1i1KKd9R4ET+/dlHaZ0WwCNjQHUtnjX2cbLyjEkV85XHhgYAN7T7YpJ4PBnWqqSuleEeNRnyqqUs9Hwxq90FuvnfD/cCAMc7AgDECBowRMGCMgAFjBAwYI2DAGAEDxggYMEbAgDECBowRMGCMgAFjBAwYI2DAGAEDxggYMEbAgDECBowRMGCMgAFjBAwYI2DAGAEDxggYMEbAgDECBowRMGCMgAFjBAwYI2DAGAEDxggYMEbAgDECBowRMGCMgAFjBAwYI2DAGAEDxggYMEbAgDECBowRMGCMgAFjBAwYI2DAGAEDxggYMEbAgDECBowRMGCMgAFjBAwYI2DAGAEDxggYMEbAgDECBowRMGCMgAFjBAwYI2DAGAEDxggYMEbAgDECBowRMGCMgAFjBAwYI2DAGAEDxggYMEbAgDECBowRMGCMgAFjBAwYI2DAGAEDxggYMEbAgDECBowRMGDsfwVcHq+86PWVAAAAAElFTkSuQmCC',
			'webinar' => 'iVBORw0KGgoAAAANSUhEUgAAAPAAAADwCAYAAAA+VemSAAAQSUlEQVR4nO3de3SU5YHH8W9mMpNMEnIjQxKSmBCNEMAkKAiRsgVUrBxAXNaWs9CDHqL1srqwuGh7Vm21tZojK1utLArHS1E5PdqDyqKLF6hWAREhERI0chmTQG4k5EIScmP/mA5aqit5E+edB3+fP3OYJ89D8s07817DKPrTKUTESA67JyAi1ilgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYMpYBGDKWARgylgEYOF2z0BGRh3GMz0DuHirESyh8dyfnosGSkxxESFMyTGDUBrWxdt7T1U1rRxoKqFg0da+PhwI5vqW+k6ZfMCZEDCKPqTfoSGcYfB/IxEZk7I4MpJacTHRVga53jzSd7cXs2mnZWsr2xUzAZSwAZJdTooyk+jaG4u6SnRp79e19DBB6W17DvYiK/+BL6mdpq7e2jq7gUgweUkzhVOZkIUmd5oxmQnclleMsOSPKfHqKo5wZoN5awpqeZob1/Q1ybWKGADOIGlY1O5c0E+3qH+6Grr29mw1cfru6t4raHN0rizk2K4elw6c6dmkuyNAqD+WAePPF/Co3uP0jtYC5DvjAIOcdPjPfx6wTgm5icDUHG4hTWvlLG2vIamvsH50SU4wlicm0LRNaPJyYoFYEdJLf/x/G7eOd4xKN9DvhsKOIQtyU3hVzeNJybaRXNLFyvWlfBoaTXtgxTumaIcYSzNS2PZwnziYt20nejmvic/YmV5zXfy/WTgFHAIcofBYzNyKZo3CoC3t1WxfP0e9nR0B+X7F3hcFM8v4PLCdADWvLyf2zeXaydXCHJy8U9+afck5EsJjjCeva6ABbNy6O07RfHTeyjaWMbRnrPfsZTqdPD4rDH84baJ/OLqkYx0udlxoIG2U2dXYE1PHy/sOYK7oZPC/BTGj/EyxhPJm+W1dCrikKKAQ4g7DJ69roBrrxjBya5elv7nBzxceoT+NvP4rDH8dHYOLpcDl8tB/siheE852fBZ3VmPcQp4u+o4DfuPcfmENMbmJJITGcGGshrt3AohOhMrhDw2I/d0vDc99B6rPq+3NM68K0YAcOOD73Ljg+/+zdf6a9Xn9dz00Huc7Orl2itG8NiMXEvjyHdDAYeIJbkpFM0bRW/fKZat3Ma6qia7p3Tauqomlq3cRm/fKYrmjWJJbordU5K/0lvoEDA93sN/L/sBbreT4qf38HDpkQGNN9LlJn/kUOZMyWTOlEwA1r9xoF9voc/0UWM7nmOd/GBcKpPzUvhwRyWHOnsGNE8ZOAVsMyewbvGlZGfE8va2Koo2lvX7M++ZdhxowHvKSc55cXT39LH+jQP8fFPZWe/E+iZbq45zWdIQRmUnMHLYEJ7ZWTngucrA6DCSze4cm8rDd0yiuaWL6fdsDtqhIqsKPC7eeWAGcbFu7vrddh7Ze9TuKX2v6TOwjVKdDu5ckA/AinUlIR8vwJ6OblasKwHgzgX5pDr1K2Qn/e/bqCg/De9QDxWHW3i0tNru6Zy1R0urqTjcgneoh6L8NLun872m64Ft4g6Dorn+QzJrXikblNMj8zwu5o3172S6KGcoSYmRADQ0dvJJxTHeL63h5b1HKR3glr697xRrXinj4X+dRNHcXB7eXamztGyigG0yPyOR9JRoauvbWTvAc40zwx0sn5rDojkX4on8+x9pUmIk0yamMW1iGss6e3j21c8o3lqBrx9nd51pbXkN/1bfTnpKNPMzEnnui8aBLEEsUsA2mTkhA4ANW30DuqpoeryHJ24pJGdEHADbS2p5Y1slOw4d4+CJkwBkR0cwccRQflSYwaT8ZG7+8Wgun5DGrau2Wb7aqKnvFBu2+vjZdbnMnJChgG2ivdA2cIfB0eKZxMdFMPcXb1q+nnd6vIfnl/+QYUkefNVt3Lv2o289AWRhegL3Lx5PZloMdQ0dLCj+s+WIZyfFsOHBKznefJLU5Zv0NtoG2ollg5neIcTHRVDX0GE53sxwB0/cUsiwJA8fltYx66EtrKtq4pIoN7+bfiHlP59O++Nz2L3sH/7mdeuqmpj10BY+LK1jWJJ/650Zbu3X4LWGNuoaOoiPi2Cmd4ilMWRgFLANLs5KBOCD0lrLYyyfmkPOiDh81W0sfmoHZSd7WJKbwtbfXsVt88dw4Yg4ItzOr31t2ckeFj+1A191Gzkj4lg+NcfyPAJrCKxJgksB2yB7uP+uF/sOWvvcmOdxsWjOhQDcu/aj0/GuWFpIlCecd3ceoeg3fyb/jo2MW/Hu145RdrKHe9d+BMCiOReS53FZmktgDYE1SXApYBucn+7/ZffVn7D0+nljU/FEhrO9pPb02+YHbp4AwO/X72PaUzt42tfI3s7//3DRuqomtpfU4okMZ97YVEtzCawhsCYJLgVsg4yUGAB8Te2WXj85z3810BvbKgFYNCnr9Jb3jnc+69dYgTECY/ZXYA2BNUlwKWAbxET5j941d1u7mueinKEA7Dh0DIArJ/pvffPc5op+jxUYIzBmfwXWEFiTBJcCtkHgiQmB+zb3V+AMq8Bx3sw0/9ZvZ21rv8cKjBEYs78CawisSYJLfzbPAZ8eOg7wrZ955dyjgG3Q0tpF7BA3CS4ndPV/K9zQ2ElSYiTZ0RF83tX+jXuaz0Z2dMTpMa1IcPkPVbW0dlmeg1int9A2aD3h31LGuaz9/fykwv+5deIIa59bvyowRmDM/gqsIbAmCS4FbIPqOv+hl8yEKEuvf7/Uf/HDjwozBjyXwBiBMfsrsIbAmiS4FLANPq9sASDTG/0t//Lrvbz3KB2dPUzKT2ZheoLleSxMT2BSfjIdnT28bPHOGoE1BNYkwaWAbXDwSDMAY7KtnX5Y2tHNs6/6j/fev3g8oyP6/1Z8dEQ49y8eD8Czr35m+RrhwBoCa5LgUsA22O3zXzF0WV6y5TGKt1ZQcaiZzLQY1t44sV8Rj44IZ+2NE8lMi6HiUDPFW/t//DggsIbAmiS4FLANNtW30tjUybAkD7OTrJ3B5Ovp49ZV26hr6ODSvGFsvHvaWb2dXpiewMa7p3Fp3jDqGjq4ddU2yxf2z06KYViSh8amTjbV9/8YtAycbitrg15gQnw0o89PoLmxi/852GBpnEOdPezeVU3hBV6yz4vl2h9mcdV5iaT1QmRnD2F9fSQ6HYyP9fDPo1P47T+O5fafjCU+1k3FoWYW/9dfBvT40CWTsxk/xsum975gfZn1K6vEOl3Qb5OfZiTwzD1Tqa1vZ8w9mwd0V45vu6XOV3UM0i11Ehxh7HtgBsneKK5/YCt/qNRbaDvoRA6bvFDZxK+q28hMi2FxbgqP7LN+f2VfTx+3vfUpq98/GJSb2gEszk0h2RuFr7qNFxSvbRSwTXqBp1/bzy9vHk/RNaN5orxmwHemLO3opnTnF7Dzi8GZ5DeIcoRRdM1owL8GPa3QPtqJZaPVJdUcqW0nJyuWpXnm3F95aV4aOVmxHKltZ3WJOfezPhcpYBvV9fbxxEt7AVi2MJ8Ci3fFCKYCj4tlC/1Pk3jipb3U9Vr/HC0Dp4BtVlxSzfsf1xAX66Z4fgFffxer0OAEiucXEBfr5v2PayjW1td2CthmvcB9L+6mvbOHywvTuX9Slt1T+kYPFGZxeWE67Z093Pfibn32DQEKOARsae7knlU7Afj36wu45QKvzTP6e7dc4OXORQUA3LNqJ1uarV1+KINLAYeIleU1PPVSOU5HGCuWFA7oIoXBtjA9gRVLCnE6wnjqpXJWDvBRMDJ4FHAIuePN/by6xUeE28mTd08JiS3xLRd4efLuKUS4nby6xccdb+63e0ryFTqVMoT0ApvLasiPj2bkiHiuKszAc6yTrVXHCfbpck7gN4VZPHDrpbjCHfzvXypZ9OLHnNB5eyFFW+AQkx3pIjvd/6AypyOMu24Yx+s3TAjqIaYCj4vXb5jAXTeMw+kI888rPY7syNA/zPV9oy1wCLkkys3zS6aQkxVLxeEWfv/HvVwyysuo7ATmT84ivK6DXXWtdH9HW8EoRxjL89NZvWQyuRck0Haim4ee2cPwoTHkZMUydVQK23ZVc9Ti3TRl8CngEHFmvAtWvseLvkY+3FHJqORYzj8vlmkT0rhu7HAiGzspP3aCzkEKOcERxr+MTmV10aX805XZREY4+bC0jpse+4BnDh5j265qpo5KUcQhSFcjhYCvi3dX+5d3eXQCyy4azu0/vojhyf57UNXWt7Nhq4/Xd1dZfsLh7KQYrh6XztypmSR7/eMeqW3nsT9+wopPjvzNcd5vm6PYQwHbrD9hDHM6+Fl+GtfPGklW+peP86xr6OCD0lr2HWzEV38CX1M7zd09p2+6nuByEucKJzMhikxvNGOyE7ksL5lhSZ7TYxyuauWZjZ+yuqT6G0+PVMShRwHbyGoQ7jBYmDmUGZekc8WkNBLiIix9/6bmk7y1vZrNu6pY5zt2Vg/oVsShRQHbZLBCiHOEcYV3CBdlxDPyvHiyhg8hMzWGIUPcRP314v72zh5aW7vwHW3j8JFWPv3iOJ9UHuet+laaLVzCqIhDhwK2wbkQwLmwhnOBjgMH2bnyi7+rvYsFK9+j4nALOVmxPL9kCpdE6QFnwaaAg+hciTdAEdtPAQfJuRZvgCK2lwIOkudun3zOxRtwZsTP3T7Z7il9byjgIOnp7WX/gePnXLwBgYj3HzhOT6/O0goW7YUWMZi2wCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBlPAIgZTwCIGU8AiBvs/8KRmB/wYsmQAAAAASUVORK5CYII=',
			'elearning' => 'iVBORw0KGgoAAAANSUhEUgAAAPAAAADwCAYAAAA+VemSAAAFiElEQVR4nO3aQWjWdRzH8Z+pm9NtGajhjIgoZtp0AxH00qI6WXhKGBRSeIx1kZodUgOLoHMIlR06BkMDoYOHvBSFIqWHBAmRMGzGzCJbruzU4llCtT3jt8+z1+v2/58+/NnbfUUXlT2jtwoQ6Y7aA4CZEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEEzAEW1J7QCt7eVNPeW7H+tJ7/521p1Rz/psfy/vHvy5vfnW59pSWtKjsGb1Ve0Qren3bfWXf8wO1Z8wbbxw5U1757GLtGS3HCT0HetsWl+Ghvtoz5pXhob7S27a49oyW44SeA1vWdJcVHX9/2us//Vaujk9UXFTHqrvaS3dXWymllBUdS8qWNd3l/LfjlVe1FgHPgeXTftN8dPJSefbY2Upr6vlgZ1955skHpp6nfxdmzwkNwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwQQMwZbUHtCKJn//o+F5oHdVefuJ9ZXW1DPQu6rh+ea078LsCXgOXJ+YbHje+ODKsvHBlZXWzB/XbtysPaHlOKHnwAtPPVR7wrz04s4NtSe0HAE32b7N68rg1p7aM+alwa09Zd/mdbVntBQndBNt72wvI7v7G94dP3mpfHLmclnevvA+9S8Tk2VwoKfseOTeqXcju/vLyVevlk9/nqi4rHUsvJ+qOXRoqL90d7VNPV8Zu1FGPvyynJv2d+KF5OMLY2XLhtXl7tUdpZRSurvayqGh/vLoO59XXtYanNBNcrvT+cCR0ws63lJKOTcxWQ4cOd3wzindPAJugtudzqMnLpbDF8YqLZpfDl8YK6MnLja8G9ndX7Z3ttcZ1EIE3AS3O533Hz1bcdH8s//o2XJl7MbU81+nNLMj4FlyOv83Tum5IeBZcDr/P07p5hPwLBx8elPD6Tz2w69O53+x/+jZ8v3VxlP6tV2bKy7K5p+RZmjvw2vL49vvaXi3rH1xOfbSYJ1BQTqWNf7YPbZtXdn7xdry1rnvKi3KJeAZGt7V9493XZ1LS1fn0gpr8g3v6hPwDDihZ2hFhz/7mmn5Mt9zJgQ8QwfePVXGr/nvgM0wfm2iHHzvVO0ZkRaVPaO3ao8AZsZvYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAgmYAj2J9X6uAsExIZmAAAAAElFTkSuQmCC',
			'learning_plan' => 'iVBORw0KGgoAAAANSUhEUgAAAPAAAADwCAYAAAA+VemSAAAOzUlEQVR4nO3deXhV9Z3H8U9uCCEECFsCgbDvaxGUfdHyYLEqdpyxKGr1UYSWsTI4MPrUlc5TOwxtxaVgAa1UK0uroNQKkwEsMGGNMuwhLAFCgCQkISQkIcnN/EEJBDj35oZ7yfkO79df5J5zz/2hvHPu2cM08bMKATDJU9sDAFBzBAwYRsCAYQQMGEbAgGEEDBhGwIBhBAwYRsCAYQQMGEbAgGEEDBhGwIBhBAwYRsCAYQQMGEbAgGEEDBhGwIBhBAwYRsCAYQQMGEbAgGEEDBhGwIBhBAwYRsCAYQQMGEbAgGEEDBhGwIBhBAwYRsCAYQQMGEbAgGEEDBhGwIBhBAwYRsCAYQQMGEbAgGEEDBhGwIBhBAwYRsCAYQQMGEbAgGEEDBhGwIBhBAwYRsCAYQQMGEbAgGEEDBhGwIBhBAwYRsCAYQQMGEbAgGEEDBhGwIBhBAwYRsCAYQQMGEbAgGEEDBhGwIBhBAwYRsCAYQQMGEbAgGEEDBhGwIBhBAwYRsCAYQQMGEbAgGEEDBhGwIBhBAwYRsCAYQQMGEbAgGEEDBhGwIBhBAwYRsCAYQQMGFantgcA96obJo1PaKIh3VtoQPdm6tw2Rk0bR0qS8s9d0OH0fO08kKOkfZn6c1q2zpRX1PKIbz1hmvgZ/9VRRX1PmKb1ba2JD/RQ+4QG1XpPdk6xPvjigOZuOaKjZd4QjxCXEDCqGNs0WrOfvkO9uzap0fuPZRTqpQXb9HF6bpBHhuthGxiVJneK1Z9f+26N45Wktq2iteiVUXrt9rZBHBmcsA0MSdKkjs317oxhqhMedsPL8njC9PrkAQr3hOnVrUeDMDo4IeAQ610vQv/YO15dE2JUVubV7rQc/XH/aWW4aDtxbNNo/WbqkKDEe6VXnumvtMwCfZB2JqjLxWVsA4fQ5E6xmjVloGIa1a3y+rGMQj37TpJWZhfU0sguq+8J06ZpI9S3ezO/85aVV+h0dpG83grFNa2nyMhwv+85lVWkO19PVMqF8mAMF1dhDRwiP4hrqDnThqjedf6Rt20VrYX/OlyjZ67R7uLSWhjdZT/tHe833r0H8zT/87368nC2Dv49xLhwj8a1baInx3bVsP4tHd/bMjZKU0d10ZTE/UEdNy5iJ1aITB7b7brxXhLXPEpPDWp/8wbkYOK4Hj6nL/h0v4bO/lpv7T9dGa8kZZZ7tfDIGQ2ft0n/vuAbn8t4/N7O6hThf22NwBFwiIwYEO93nsG9Y2/CSJz9U8sYdW7XyHH6RytTNWnVPp31+t7KenXrUb3x/reO0xtER+gHXeNqPE44I+AQqc72YWTd2l0rDe7i/AvkZOZ5vfLXvdVe1qub07RjX7bzZ/Uk4FAg4BDZk5rjd56UtLM3YSTOenVo7DhtWeLhgM6oKpf08eqDjtP7dPa/kwyBI+AQWZzo/I/5kj8lpYV+ID4ktHA+TXJralbAy0s66vxLq13r6p2SicAQcIjM2pmhxX895Dj9Pz/coeWn8yt/jg/3KMYT3OOw/tSv53wQIvv8hYCXd9DHe3zt0EPNcRgphCYs36mt+zM1/rsd1adrM5WWeZW8J0sfrD6gT07kVc43tXsLzXp2kHLzL+jl+dv0/k068aGwyPkQVsPIwP9pxPt4T2FRWcDLg38EHGJz9p3SnH2nfM4zblg7RUaGq2VslBa+NFIDlu3TS2tSlOtn7++NOnO22HHad9o20aen8h2nX8/g+BjHadk5zp+FmuMrtAus2Jimsiuupf3JD3vo06cGhvxzDxxzDvTBOzuqfoBf6R8Y1t5x2v4jeY7TUHME7ALvpGTq0ZnrlH6ysPK1uwa1Uu8afI0NxJ40551Ovbo01oz+baq9rIkdmun7I53n33nQ/155BI6AXWLZybP6/hvrtPLrY/J6K7Tkq0PaXRLa7cZ1R3OqrPmv9vLE/nquWwu/y3m4VYx+9exgn/MkpWYGPD74x8UMLlTfE6bzId7+vWTtxEG6a1Arn/N88uVBzVtzQBvPlVR5vXe9CE0e2kHPPNjd54kraekF6jAzMSjjRVXsxHKhq+Od3iteXRIa6+NNR7QhP7g7g77YmOY34An3dtaEeztrV0qODqbny+utUJsW0erfK7ZalyD+6b8PB2u4uAoBu9zDrWI0+18ufj0dM6i1xs36Oqhfrf94IFMvZBWpZWyU33n7dGuqPt2aBrT8kpJyLfn2eE2HBz/YBna54itOZ+zQpqHmPNY/qMvP8lZo4ef7grrMKy1dfVjf1OCkEFQPAbvcisxz+mB5SuXPowe31ot9fX/lDdS85OM6cfp8UJcpScUl5frt2gNBXy4uI2ADpq/ap2/3Xr7S575h7YK6/Iwyr+Ys2RnUZUrSgs/2a2sha99QImADcr0VevHDZOXmXdwLvDY5I+if8ebuk9q6M3iHeo6fLNSv1/u/oAM3hsNIhrSPCFeryDpKKijxP3MN3NM0Wst/Prpa1zL7M+mXG7TgsPP1wQiOcPUf/3ptDwL+JdTx6NFe8XpgYBs90ideAxpHqzD3vNKDeLO4g0Wlani2TMP6+T95w5elqw7pZ0lHgjQq+MJhJAPua95Ac58bqjbx0VVe/2lRT706f7t+tftk0D7rjU2HNaJfCw2pYcRHjp/TKyv3BG088I1tYJfrEVlH86cNuyZeSYqKqqPZUwfr0YSaP0nhame9FZq+KFlncgP/ml5SUq4Zv9uqVG4he9MQsMs9MaCN4uPq+5xnip87SwYqqaBE097Z5PM86euZuSBZn54O7BJE3BgCdrnbe/i/c+Xg78SpSZDv5vHR8Vz9YqHv28Ve6cMVB/TL/z0R1DHAPwJ2uercudLjCVO9sODfjuf17cd83hbokg3Jp/T8V9W/gyWCh4Bdbu9h/xfCpx7N18ny0Dxr6Z8/36U1m53XrHsP5unHv98W8ruH4PoI2OWWbklTaanvOBf9JcXn9BuR663QxD8ka9uua+9SeSyjUE+++z/aG+LrluGMgF1ubV6RXnx3i+MOpSVfHdIvdqSHdAxppeV66nebtXP/5Zvtnc4q0tNvbtQ2TpWsVZzIYcCmrALt2JyuemHhahgVoeKSciXvzdabi3dpxoabc61tZrlXG785oeEdm0sVYfrR7PVKzAv+BRAIDKdSulSniHC98/BtGtKvhd5eskuvbTtW20OSJPWOrKO4qAitzSuq7aFAfIV2pbhwjxY8cbvuGdlGjRvV1Y8f7FnbQ6q0u6SMeF2EgF3o3X/oU+U2N4lbOL6K6yNgl5nUsbke+l7Hyp+/3pqh5zm3GA4I2GXqhF/+X7InNU8TP9yuzBAd44V9XI3kMnNTM9VifrKax0Tp/aQjOlTKhQFwxl5oVEtcuEd3NKmviooKbc8r4luBS7AGhk/x4R69fFcX/ei+LmoQHSFJOldQqo++TNXMdamEXMvYBnaBGE+Y3ru7u1Y/eYfGxTas7eFUivWEaekzgzRlfM/KeCWpYYMITRnfU8ueHnjTn2mMqgjYBSZ0bK7JD/XQ3cMS9MJDfWp7OJWm9G+jEQNaOk4fdUe8pvZLuIkjwtUI2AW6tG5c+edwF63R7h/e1u889w0P7i1uERgCdoGoK+4CmZXrngdht2/VyO88bVs2uAkjgRMCdoGC86WVf46Ocs9+xawc/79M3PQL51ZEwC5w5Ir7SJ0tLPUx5831X5v9P5QscSunedYm9/y6v4XNTc1S07nbFN8sWu+76H7Kc5MO694R7dSp7fX3jKcezdfcTTw6tDZxIgd8GtQgUrMe6adRA6s+UG3dlgy9sPhbLuivZQTsQnXDpCc6NFd+UamWnjzrON9jCU00sGusYqLrKiO7QKv2nNLfgvwA8EvuaRqtHi0vron3njqnVTmFIfkcBIaAXejXIzvr+ccvHg9OTErX2yv36S/ZBZXTO9cN13uPD9Dowa2rvK+01Ku3PtmtGRv930nSSXy4R5NvS9CjY7uo4PwFTZm/VZtC9Cwm3Di2gV0oIfbyUxjGDE3QmKEJ+mr9cU1a/I3Sy7x677Fr45WkiAiPpj/RV3kFJQHfJ6tz3XA9P6qLxt/dUU0bR1a+fn+feG3alFbTvwpCjL3QLvTbxBTtSsmp8to9I9vooa5xeqR1Y40ecm28V5o2oa9ir3NCSIwnTP2iIjS8YaSuvtv0z8Z0109+2KNKvMUl5Uo+kiO4F2tgF1qfX6zBv1mv53rH65kHeqpj24YqLilXamaB7uzhfGrjJc2aRGpIswb6IuucekbW0ZwJt6lft+aKbVavcp7VG9M1dtG2yp8rKi5vSZWUlOuzNWmat+aANoRomxrBQcAudd5bof/YmaG3d5/U/S0a6VRhif6WX6yx1XhSgyTVj7j45erBni01Zui15yt/b3iCYj/arqy/35D9jcQUZeUVqehCuZbvOamdRe45Hg1nBOxy570VVfZEp53y//Awr7dCe3Mv3vJ194mzKi31KiLi8tZSSUm5Fi5PqYxXkg6VlutFFx2DRvUQsDErUjM1PatILWKjHOf5fN3RyjXoisxzuvPfVqlD4/o6c/6CThVd0JGSMp3lUSj/L3AYyaDHEppo3vRhVa7RvWTfwTyNf2ujdhXzFfhWwJMZDNqZX6xtm9MVHRau+Gb1FVWvjg4fO6dFK1P13LIdOnCBZxXdKlgDA4ZxHBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMOz/ALfh+6w/ItmLAAAAAElFTkSuQmCC',
			'lms_products' => 'iVBORw0KGgoAAAANSUhEUgAAAPAAAADwCAYAAAA+VemSAAANb0lEQVR4nO3de3SU9Z3H8U9uQO4JCQlMyBXQRAkgLEQoEmRxtXIRrLpnu7BLXbVbu+vZak8Vt9vjWimup/7hKct61nNW67b2aG0EuZQWynINEoQAMZBwzY0Ecr8Qcp/sH1MCKVoRZjLzHd6vc/hjhmHmNzl583ue3zPPMwF6Iq9fAEwK9PYAANw4AgYMI2DAMAIGDCNgwDACBgwjYMAwAgYMI2DAMAIGDCNgwDACBgwjYMAwAgYMI2DAMAIGDCNgwDACBgwjYMAwAgYMI2DAMAIGDCNgwDACBgwjYMAwAgYMI2DAMAIGDCNgwDACBgwjYMAwAgYMI2DAMAIGDCNgwDACBgwjYMAwAgYMI2DAMAIGDCNgwDACBgwjYMAwAgYMI2DAMAIGDCNgwDACBgwjYMAwAgYMI2DAMAIGDCNgwDACBgwjYMAwAgYMI2DAMAIGDCNgwDACBgwjYMAwAgYMI2DAMAIGDCNgwDACBgwjYMAwAgYMI2DAMAIGDCNgwDACBgwjYMAwAgYMI2DAMAIGDCNgwDACBgwjYMAwAgYMI2DAMAIGDCNgwDACBgwjYMCwYG8P4FYwPzZMy2dnaN50hxyJYQoMDPD2kDzC6exX9YVL2n6gWv+754y2NV3y9pD8XoCeyOv39iD82eqZafrBiimS5Lfh/imn0/Ur9do7h7VyX5lXx+LvmIE9aPXMNL3w+F3eHsaQu/wf1eX3TsSewz6wh8yPDRuYea92eXbyR5/33n6wYormx4Z5YTS3BmZgD1k+O2PQbaezX82t3dq4q0Ltnb1eGpVnhY8I1sI5KYqJGjZod2H57Axt2/CZF0fmvwjYQ+ZNdwz6JW5u7dbXX9qmgvZuL47K82ZsP6HfvjRfI2OGS3JtTs+b7pAI2CPYhPYQR+KVzUans18bd1X4fbySVNDu2sq4enP66p8F3IuAPeTq2TcwMMBvN5s/T3tn7zXvH55BwIBhBAwYRsCAYQQMGEbAgGEcB/ZDjuBAPTJulBbNStHsaWO052CNNuRX6MPTdarudXp7eHAjAvYTQZIedURrwfQULZqTouioYert61dRaYPm5iRp/qyxerm1Wxt2VWjTgQr9urpFfd4eNG4aARuXGz1CD01J1pLcVKUnR0qSikobte79Mn1UVK3Cjh7dFRqipdkOLZmTpmULx2vZwvH6SWWb1u0s1/rDldrZ0unld4EbRcAGZQ0P1pLMRD10T5pyJidIkmpqL+m/PjiujYcqtbmhfdDjCzt6VFhQrh8VlOvBuHAtnJqsJXNT9b1lE/W9ZRO1/0it1u8u07qSCzredet84MQfELAR0YEBeiwtTg/mpOj+mUkKDQ1WR0ev8rae1cb9FcqrbFLLdZzptLmhXZu3lmjlH0r1cHKsFuak6Ouzk5UzOUH/1tGr3+07p837K/RBWcN1PR+8i4B93KL4CC2c5poxE+JDJUn5hRe0fneZ8kov6FT3je3Jtjj79XZ5o94ub9T4dUV6+HbXjL5kXqqWzEvVK/UdWrejXBsPVmpD/UV3viW4EQH7qJWTk7Tsgdt0x/gYSdLpija9+/Oj2nj0nHa2unef9VR3n14rqtZrRdXKjRqhBdlJenheup56JFNPPZKpY6ea9YstJ7T6yDm3vi5uHgH7qGe/OUnxI0dIko6WNGj1r44MycrxztZO7dl7WofO1mvl30zWpMw43TE+Rs9+cxIB+yAC9lF/9dI2PZzt0PMrpmhSZpx+9e/ztKqiTR/tKNPHR6q0y82zsCTNiRqhxZPHauncNGWkuFa0i08266MdZ5RXVO3218PNI2AfVdjRo7pDlfrhk1N1sLhOB47Va+ncND33d9l6Ttnad/iCPt5dpt+UXNDJG9wPlqQJw4L0jcxELb4nTTOnJEqSLtR16M1fH9emQ1XayP6vTyNgHzYxyrVodaikQd/5fYle2Faq/1w8UX+7YLxyJiVo5pRE/aijV1vyq7Tpkwp9WNF4XSvH0YEBeiRlpBbcnaIHZo1VaGiwurpc/wn8fm+VHnv3U1agjSBgH5YS4wq4qs41C7Y4+xUc5Do5fsHKrZoyNlqL70nT0r90/VlV16F1O8q06VDV564cL4qP0INTXZvIiaNcz315Jl9fWqv9rz6gxLhQ4jWEgH1YUpzrUjTVjVcukJ6cGKGurj5tb2rXlsZ2vXq0etC+67cfzdK3H80a2HfNP1WvWePjtXRuhu6c4FrRPlPRptffLbpmX/roiQbNyE5QQlCgavv4zLQFBOzDkhNcC0llzR0D96U5IlVe067uqybJXa2d2rX7lL6/+5T+eky0HpyerMW5qfrhk1MHHtPc2q13N5zU5gOVer+m5XNfr/hMs742dbRyYsM49msEAfuw1NHhkqSjf5wlE4IC5UgM0x8++eLDOe/XtOj9j1s0ZtMxfWNcvKZNGKWDJ+v0m9P1qvmSWbW0skmSdIcjioCNIGAfluaIUmVN+8Dm7MRI16VaK8+3/7l/Jkmq6XNqzYla6UTtdb/e8fNtkqTbk2Oloxw2soAT+n3UP6TFKSUpQhfbezTsjxd1TIl2LTxVN3x5wDciv/mSOjp6dWdGjEeeH+5HwD7q9X++W8FBAcoaH6MV6fGSJEesK+BzDZ751r8WZ7+KTzfpzgkjFcalYE0gYB91vv7KwlXtxS5JkiMuQpJUddWilrsVn25SeGiw7onhYuwWsA/sox7/2V4tzB6jknOtWlfr2jdNSnAtap1tu3LoZ1xIkOY4onWbI0pbis9f94kOSxIi9RfpI/VZZbN2XmgbWOAqqXAtZGUlRup3jZ7ZVIf7ELCPyr/Ypfx9ZYPuSx0dro6OXs1KitF3x8Vpxp2jNDkzbuCbD55s6tIzb+TrvXPNX/i8wwKkVV8bp39Zlj3woZDOrj4dOlavA8fq1NHVI0nKTI6Rjp/3xFuDGxGwIamOSIWGBuvNF2ZLkuobO7VlT5UOHK9Vd2+fXvzWVL3zr7lK++9P9ZPD1x5qyokYrp8un6rZU0frRFmr3vjgqFITojQtM0452QmadVfiwGOz0lnIsoCADdmws1yRYSE6eKJOhyqatbWxfdDphUU/btHaZ2Zp1XdnaNxHpXp2y/GBj0X+4/hReuWp6YqLHa73Np3SixuLVd7rlE7WSXtPKzowQPfGR2ha2khNnhCvY2cbvfMm8ZUQsCErNhb/2b/fUH9RVav/T28+MUOPL71dGUmRev69Qj197wT9/UO3qbm1W8/8dK9+VnrtseEWZ7/W1ba59rcLyj31FuBmBOxnCjt6tGjtPq1Zmq1H78/Q3mljFBwUoP1HavX9n3+qPW1d3h4i3IjDSH6ots+pxz48olf/p1BdXX1645ef6f61+cTrh5iB/djKfWVaua/My6OAJzEDA4YRMGAYAQOGETBgGAEDhrEKPUTm5yRp/ahb4wyfrIxYbw/hlkHAQyR9bKTSx0Z6exjwMwQ8RC6f+QO4E/vAgGHMwEOkta1bzW093h7GkIiJDFFU5DBvD+OWQMBD5Je/Pa2nt5Z4exhDYu19mfrOY1neHsYtgU1owDAC9pDevitfneB09isqPMSLoxlaUeEhcl71/UpX/yzgXgTsIVU1Vy4IFxgYoEVzUpQbPcKLIxoaudEjtGhOysB1uqTBPwu4F/vAHrL9wDk9Pvb2gdsR4SHa9PJ92vrJOV285J+LWRFhIbrv7iSFjggadP/2A1/8VTC4OQTsIe/sOaPlCycoJMS1kRMYGKDwsGAtzE3x8sg860+Pd/f0OPXOnjNeGo3/I2AP2d3aqZffOqgfPz190P232gc6Xn7roHZf57Wq8dWxD+xBrxRW6cU1Bers6vvyB/uZzq4+vbimQK8UVnl7KH4tQE/ksUToYTkRw7VsRqrmTnPIkRCmIDd/71B4eMigmb23r1/t7T1f+TE3q8/Zr+raS9pxsFq/KCjX/otcg8vTCNgPHHlujiZlxg3c3llQrblv7R/0mB1P5ih3hmPg9tGSBk1+fdeQjRGewSY0YBgBA4YRMGAYh5GM+afbEpSVEquAgCsLUqPjB1/pI31stNbel3nNfVcbHR826DH9/f06XtGkNSeu/doV+C4WsQx5dVa6nv/WFI++xn+8fVgv5J/16GvAfdiENmTpvRl+8RpwHwI2pLy6ddBZPu7mdParvLrVY88P9yNgQ1blFan4ZJPHnr/4ZJNW5RV57PnhfuwDA4YxAwOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOG/T8mK7ma/5887wAAAABJRU5ErkJggg==',
			'other_courses' => 'iVBORw0KGgoAAAANSUhEUgAAAPAAAADwCAYAAAA+VemSAAAK9klEQVR4nO3dbUyV5x3H8Z+AIFKUggio4BMdaLFKqxZLxaRDpVSxZsuyLNubrVuz1Kxx25uma5s16ZJmW5MmXZp12d40abekay31CXVarVasVtFixRYrFSsioqAiHh7O2YsDtOS+4YByHv7w/SS84TrAJfbb+/LPOTfj9MS7PgEwKSrcGwBw+wgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDCBgwjIABwwgYMIyAAcMIGDCMgAHDYsK9AfT3zo8W6QcrZ9/Wx3o83frlyx/pzfqrI7wrRCquwBEmP2fKbX9sXFy0ZqfeNYK7QaTjChyhfvz8bv2noXXIj39z3QL9dE12EHeESMQVGDCMgAHDCBgwjICNeO37OWp95TE9khQf7q0ggjDECqHs2OiAj4mKGuf6/rw5SZqUGKu5yQna3dI+0lvrpzAxTl+0dajJ6wvq18GdI+AQqfpdkRbmpoR7GwMqmjRBpXkZKls+S/Oyk7Rp99da//bRcG8LARBwiERivEsTYvXY/HSVPpSlxXmp/dYeLZyhnP8e1+mO7jDtDkNBwCF2z1Plqh0kijPPFWtOVmJQ9/BwYpxe+km+ihZn9L3P4+nWh4cbtO3QOf1q3XzNz05S2fem6s/VDUHdC+4MAY9BBTOT++Ldc+iCKj6p16aaxr6rbUbyRM3PXqSSBzMJOMIxhR6D9p5tlrdnQPXsv6v08okL/Y7KFSf90RYtmaYFE8aHZY8YGgIegw63dejAsUZJUum96Y71Pa239OnJJsVEj9Pa3Kmh3h6GgYDHqIrKeklSSUGm6/q2g73rWSHbE4aPgMeoD043qqvbp8V5qSqaNMGxXvH5RUlSYX6a8uM5RkcqAg6TvU8W6OwLK5URHZ6/ghPtndp3+IIkqTQvw7G+/7pHlVWNiooap7J7neuIDAQcJnnZyZo14y7NCuPVbcfh85KkR5e5H6O3H/Kvlzw4I2R7wvAQ8BhWfvqSPJ5u3ZebouK7JzrWt566KK/Xp4JFaVp2V1wYdohACHiUiR0fpezY6CG9dfp8OnjcP40uyZvm+FyBptUIP57IMco8+8T9elb3D/vjSh/K1O8/qnW8v6KyXssfSFdJQaaeO/T1SGwRI4grMCRJ87KTtGaK835agabVCC+uwKPMC68f0YtH64f8+I3z0/XKxmWSpOK8DG3+8Mt+673T6kcKpqs0L0P7Pj47ovvFneEKjD5rls90fX+gaTXCh4DRZ25Woh6f6nwl1Hen1atcptUIHwJGP8ULnNPoU54u7T3if4HDKpdpNcKHgEe5X2en6tZrZXo6N23Qx7Xd7JIkla2YKbcb/1R84v939WOFPDc6khDwKLdgdori4qKVk3n3oI/7uOqirl3vUGZGgtanT3asv/9lk9rbu5Q7d7LrtBrhQcCQJLV7urXjoH9YtXKh85h8prNbuz/xP3e62OW50wgPAkafbT3T5rIVMxXrcnPM7T3H6IGm1Qg9AkafD85d0dUWj9JT4/XDaUmO9U1fXVbbza4Bp9UIPQJGnyavT1sP+K+yxYumO9bPd3n7jtlu02qEHgGjnx2f+gNdW5SlyS43ma/4zjE78G3qEWwEjH7e/6ZFl6/c0pTkCVo33XmMfq+uedBpNUKLgNFPq9enzfvOSZJWPeB8If+lbu+g02qEFgHDYWfVN5Kk0sJMpbocowNNqxE6BAyHdy606GJTu+5OitParGTHeqBpNUKHgOHQ4ZPK9/pfvP/oEucxusnr05b9/mn1SpdpNUKHgOFq53H/s65WLZuhqS53ztx51H+MXjPAtBqhQcBGVH/VomvXO3TmSltIvt57F1tV39CmSYmxWj/L+ZsVA02rERoEbMSG/53W5N9uCfov9+7VrW+P0atdjtGtXp8+GGRajdAgYAxo12ffHqNnxDj/U9kVYFqN4OOeWBHun6Xz9fP1Oa5rbe1d+tlLe/Ve47WgfO1Nl67rzLnr/uc+z5mi17641G/9nQst+mtTu9JT47U2K1n/qmsOyj4wMK7AES4/x/nvz14J8TGalRLcW9xs/sh/jC5Z6rwfVqBpNYKPgI34zV8O6J6nyvvetu0b+p0n78Sunl/w/cjSaZo73vns50DTagQX33EjbnV6VdvR3ffW2e0NydfdfPmGas60Kj4+RuvuSXWsB5pWI7gIGAFtOeCfNq92OUYHmlYjuAgYAW2v9h+TVyzO0Lw459wz0LQawcN3GwHtunpTJ2qaFRcXrbKcqY713ml1wsQYPT5nShh2OHYR8Cj32dlmeTzdOl1/9Y4+z7aD/qHZqgGOyYNNqxE8BDzKvV7bpAkbyvVqTeMdfZ6tPdPooiXTdJ/LLyUPNK1GcBBwmFTXXlHd+Ruqa+8M91aGZN+1WzpS3aSY6HFam+O8Sfzmyzd0qrZlwGk1goOAw2TF3ys1+4871RCiHweNhO2V/mP06gL3Y/LWj3vWOUaHDAFjyLaevChJKsxP05KEWMd6oGk1Rh7fZSPeeGa53hjG45/OTdMrG5cpaoAXGfzhb4f1UtX5Ye3h4A2PKqsaVbAoTaXz0nX4yLl+673T6vtyU1SWM1WnTlwY1ufH8HEFjnDHTg/8AoG29i7VNd8M4W6k7Yf80Zc86D6Nrqj0v0JpoGk1RhZX4Aj3i62f68UdNRrvciVt7vLqqtfn+nGv1jTq1Sc3jfh+yk826HmvTwWL0vRwYpz2X/doRkyU1s+dotVLMlVc4L/FTmF++oh/bTgRcIilxA7+Le898k6MjVZ27OA/jkmJiVLvs49je54BFTs+KuDHfdeEYf7I51h7pw4ca9TyB9K1YVWONkZHaXVhphImfvvnOvr5Zb21o3ZYnxe3Z5yeeNf9f+EYUb5/rA/3FgZVvudrrXvr6JAe+8zC6frThqX93neqtkWb95/TtupvtKf1VjC2CBdcgUPkeE2zFuZG7qt1TtUN/Zla79c06qnGm+rs9Kp8b512VDdoy+UbQdwdBsIVGDCMKTRgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGAYAQOGETBgGAEDhhEwYBgBA4YRMGDY/wEGGqitHFam2AAAAABJRU5ErkJggg=='
		);


		$collectionListResult = $apiClient->call(
			'GET',
			'/admin/smart_collections.json'
		);

		/*
		// NOTE: we can't delete every collection!! we can't assume that the client has not created its own collections
		// and we are going to dleete them all without any filter !! but we are allowed to handle only ours !!
		foreach ($collectionListResult as $collectionVO) {
			$collectionId = $collectionVO['id'];
			$collectionDeletionResult = $apiClient->call(
				'DELETE',
				"/admin/smart_collections/$collectionId.json"
			);
		}
		*/

		//these are our standard collections to be present on client shopify
		$lmsCollections = array(
			'elearning' => array('title' => 'E-Learning', 'found' => false),
			'classroom' => array('title' => 'Classroom', 'found' => false),
			'webinar' => array('title' => 'Webinar', 'found' => false),
			'learning_plan' => array('title' => 'Learning Plan', 'found' => false),
			'lms_products' => array('title' => 'LMS Products', 'found' => false),
			'other_products' => array('title' => 'Other products', 'found' => false)
		);

		//iterate through existing collections list in order to check if LMS collections are already present
		foreach ($collectionListResult as $collectionVO) {
			$title = $collectionVO['title'];
			foreach ($lmsCollections as $key => $lmsCollectionTitle) {
				if (strtolower($title) == strtolower($lmsCollectionTitle['title'])) { //make the check case-insensitive, just to avoid confusion on duplicated titles ...
					$lmsCollections[$key]['found'] = true;
					break; //end loop, we do not need to iterate anymore
				}
			}
		}

		//create missing LMS collections on Shopify

		if (!$lmsCollections['elearning']['found']) {
			$collectionCreationResult = $apiClient->call(
				'POST',
				'/admin/smart_collections.json',
				[
					'smart_collection' =>
						[
							'image' =>
								array(
									'attachment' => $logoImages['elearning']
								),
							'title' => 'E-Learning',
							'rules' =>
								[
									[
										"column" => "type",
										"relation" => "equals",
										"condition" => "elearning"
									]
								],
						]
				]
			);
		}

		if (!$lmsCollections['classroom']['found']) {
			$collectionCreationResult = $apiClient->call(
				'POST',
				'/admin/smart_collections.json',
				[
					'smart_collection' =>
						[
							'image' =>
								array(
									'attachment' => $logoImages['classroom']
								),
							'title' => 'Classroom',
							'rules' =>
								[
									[
										"column" => "type",
										"relation" => "equals",
										"condition" => "classroom"
									]
								],
						]
				]
			);
		}

		if (!$lmsCollections['webinar']['found']) {
			$collectionCreationResult = $apiClient->call(
				'POST',
				'/admin/smart_collections.json',
				[
					'smart_collection' =>
						[
							'image' =>
								array(
									'attachment' => $logoImages['webinar']
								),
							'title' => 'Webinar',
							'rules' =>
								[
									[
										"column" => "type",
										"relation" => "equals",
										"condition" => "webinar"
									]
								],
						]
				]
			);
		}

		if (!$lmsCollections['learning_plan']['found']) {
			$collectionCreationResult = $apiClient->call(
				'POST',
				'/admin/smart_collections.json',
				[
					'smart_collection' =>
						[
							'image' =>
								array(
									'attachment' => $logoImages['learning_plan']
								),
							'title' => 'Learning Plan',
							'rules' =>
								[
									[
										"column" => "type",
										"relation" => "equals",
										"condition" => "learning_plan"
									]
								],
						]
				]
			);
		}

		if (!$lmsCollections['lms_products']['found']) {
			$collectionCreationResult = $apiClient->call(
				'POST',
				'/admin/smart_collections.json',
				[
					'smart_collection' =>
						[
							'image' =>
								array(
									'attachment' => $logoImages['lms_products']
								),
							'title' => 'LMS Products',
							'rules' =>
								[
									[
										"column" => "type",
										"relation" => "equals",
										"condition" => "elearning"
									],
									[
										"column" => "type",
										"relation" => "equals",
										"condition" => "classroom"
									],
									[
										"column" => "type",
										"relation" => "equals",
										"condition" => "webinar"
									],
									[
										"column" => "type",
										"relation" => "equals",
										"condition" => "learning_plan"
									],
								],
							'disjunctive' => true,
						]
				]
			);
		}

		if (!$lmsCollections['other_products']['found']) {
			$collectionCreationResult = $apiClient->call(
				'POST',
				'/admin/smart_collections.json',
				[
					'smart_collection' =>
						[
							'image' =>
								array(
									'attachment' => $logoImages['other_courses']
								),
							'title' => 'Other products',
							'rules' =>
								[
									[
										"column" => "type",
										"relation" => "not_equals",
										"condition" => "elearning"
									],
									[
										"column" => "type",
										"relation" => "not_equals",
										"condition" => "classroom"
									],
									[
										"column" => "type",
										"relation" => "not_equals",
										"condition" => "webinar"
									],
									[
										"column" => "type",
										"relation" => "not_equals",
										"condition" => "learning_plan"
									],
								],
							'disjunctive' => false,
						]
				]
			);
		}

	}



	public static function alignLmsCurrencyToShopifyCurrency()
	{
		$currencyCode = strtoupper(ShopifyProductProxy::getShopifyCurrency());
		Settings::save('currency_symbol', '');
		Settings::save('currency_code', $currencyCode);
	}

}