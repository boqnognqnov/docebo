<?php

class ShopifyApp extends DoceboPlugin {

	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'ShopifyApp';
	public static $HAS_SETTINGS = true;

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name = __CLASS__) {
		return parent::plugin($class_name);
	}



	private function checkExistingShopifyTables() {

		//clear existing tables with the same names required for shopify
		$tables = array(
			'learning_course_shopify_product',
			'learning_coursepath_shopify_product',
			'lt_course_session_shopify_variant',
			'webinar_session_shopify_variant',
			'shopify_webhook_history'
		);

		$output = array(); //this will contain the detected already  existing tables

		/* @var $db CDbConnection */
		$db = Yii::app()->db;
		foreach ($tables as $table) {
			$res = $db->createCommand("SHOW TABLES LIKE '".$table."'")->queryColumn();
			if (!empty($res)) {
				//$db->createCommand()->dropTable($table);
				$output[] = $table;
			}
		}

		return $output;
	}



	private function migrateUp()
	{
		try {

			$existingTables = $this->checkExistingShopifyTables();

			/* @var $db CDbConnection */
			$db = Yii::app()->db;

			if (!in_array('learning_course_shopify_product', $existingTables)) {
				$db->createCommand()->createTable('learning_course_shopify_product', array(
					'idCourse' => 'int NOT NULL DEFAULT 0',
					'shopifyProductId' => "varchar(255) NOT NULL DEFAULT ''",
					'shopifyProductMeaningfulId' => "varchar(255) NOT NULL DEFAULT ''"
				));
			}

			if (!in_array('learning_coursepath_shopify_product', $existingTables)) {
				$db->createCommand()->createTable('learning_coursepath_shopify_product', array(
					'id_path' => 'int NOT NULL DEFAULT 0',
					'shopifyProductId' => "varchar(255) NOT NULL DEFAULT ''",
					'shopifyProductMeaningfulId' => "varchar(255) NOT NULL DEFAULT ''"
				));
			}

			if (!in_array('lt_course_session_shopify_variant', $existingTables)) {
				$db->createCommand()->createTable('lt_course_session_shopify_variant', array(
					'id_session' => 'int NOT NULL DEFAULT 0 PRIMARY KEY',
					'shopifyVariantId' => "varchar(255) NOT NULL DEFAULT ''",
					'price' => "float NOT NULL DEFAULT '0'"
				));
			}

			if (!in_array('webinar_session_shopify_variant', $existingTables)) {
				$db->createCommand()->createTable('webinar_session_shopify_variant', array(
					'id_session' => 'int NOT NULL DEFAULT 0 PRIMARY KEY',
					'shopifyVariantId' => "varchar(255) NOT NULL DEFAULT ''",
					'price' => "float NOT NULL DEFAULT '0'"
				));
			}

			if (!in_array('shopify_webhook_history', $existingTables)) {
				$db->createCommand()->createTable('shopify_webhook_history', array(
					'webhook_id' => 'varchar(50) NOT NULL',
					'date' => 'timestamp NULL'
				));
			}

		} catch (Exception $e) {

			Yii::log(__METHOD__.' exception: '.$e->getMessage(), 'error');
			throw $e;
		}

	}


	public function activate() {
		Yii::import('plugin.ShopifyApp.components.*');
		parent::activate();
		$this->migrateUp();
		$_SESSION['onECommerceDeactivation_dontMarkCoursesUnsold'] = true;
		PluginManager::deactivateAppByCodename('Ecommerce');
		Settings::save('module_ecommerce', 'off');
	}


	public function deactivate() {
		parent::deactivate();

		if ($_SESSION['onShopifyDeactivation_dontMarkCoursesUnsold'] !== true) {
			// Turn all paid courses and learning plans into free and admin subscribe only
			$paidCourses = LearningCourse::model()->findAllByAttributes(array('selling' => 1));
			foreach ($paidCourses as $paidCourse) {
				$paidCourse->selling = 0;
				$paidCourse->subscribe_method = 0;
				$paidCourse->save();
			}
			$paidLearningPlans = LearningCoursepath::model()->findAllByAttributes(array('is_selling' => 1));
			foreach ($paidLearningPlans as $paidLearningPlan) {
				$paidLearningPlan->is_selling = 0;
				$paidLearningPlan->subscribe_method = 0;
				$paidLearningPlan->save();
			}
		}
		else
		{
			// on switch to Ecommerce
			$params = array(
				'installation_id' => Docebo::getErpInstallationId(),
				'filter' => 'all',
				'lang' => Yii::app()->getLanguage(),
			);
			$appId = 0;
			$appLst = CJSON::decode(AppsMpApiClient::apiListAvailableApps($params));
			foreach ($appLst['data'] as $appDatum) {
				if ($appDatum['lang']['en']['title'] == 'Shopify') {
					$appId = $appDatum['lang']['en']['app_id'];
					break;
				}
			}

			// turn off ECommerce from ERP
			$apiParams = array(
				'installation_id' => Docebo::getErpInstallationId(),
				'app_id' => $appId
			);
			$apiRes = AppsMpApiClient::apiAppCancel($apiParams);
		}
		unset($_SESSION['onShopifyDeactivation_dontMarkCoursesUnsold']);
	}


	public static function settingsUrl()
	{
		return Docebo::createAdminUrl('ShopifyApp/ShopifyApp/settings');
	}



}
