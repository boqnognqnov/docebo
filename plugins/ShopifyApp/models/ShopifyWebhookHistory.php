<?php
/**
 * This is the model class for table "shopify_webhook_history".
 *
 * The followings are the available columns in table 'shopify_webhook_history':
 * @property integer $webhook_id
 * @property string $date
 */
class ShopifyWebhookHistory extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ShopifyWebhookHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function primaryKey()
	{
		return 'webhook_id';
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'shopify_webhook_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('webhook_id', 'required'),
			array('date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('webhook_id, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'webhook_id' => 'Webhook',
			'date' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('webhook_id',$this->webhook_id);
		$criteria->compare('date',$this->date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Check if a webhook is already used.
	 * If false - create OR update the webhook date
	 * If true - return false
	 * @param $webhook_id
	 * @param $updated_at
	 * @return bool
	 */
	public static function isWehbookUsedAlreadyAndTrack($webhook_id, $updated_at)
	{
		$updated_at = $updated_at ? $updated_at : date("Y-m-d H:i:s");
		$isUsed = false;
		$date = new DateTime($updated_at);
		$date = $date->format("Y-m-d H:i:s");
		$model = ShopifyWebhookHistory::model()->findByAttributes(['webhook_id' => $webhook_id]);
		if ($model) {
			if ($model->date == $date)
				$isUsed = true;
			else {
				$model->date = $date;
				$model->save(false);
			}
		} else {
			$model = new ShopifyWebhookHistory();
			$model->webhook_id = $webhook_id;
			$model->date = $date;
			$model->save(false);
		}
		return $isUsed;
	}

}