<?php

class m170228_141621_ALTER_shopify_variant_price extends CDbMigration
{
	public function safeUp()
	{
		$query = "SHOW TABLES like '%shopify_variant%'";
		$tables = Yii::app()->db->createCommand($query)->queryColumn();

		$tname = LtCourseSessionShopifyVariant::model()->tableName();
		if (in_array($tname, $tables)) {
			$query = "SHOW COLUMNS FROM `".$tname."` LIKE 'price'";
			$columns = Yii::app()->db->createCommand($query)->queryAll();

			if (empty($columns)) {
				$query = "ALTER TABLE `".$tname."` ADD COLUMN `price` FLOAT NOT NULL DEFAULT 0.0 AFTER `shopifyVariantId`;";
				$rs = Yii::app()->db->createCommand($query)->execute();
			}
		}

		$tname = WebinarSessionShopifyVariant::model()->tableName();
		if (in_array($tname, $tables)) {
			$query = "SHOW COLUMNS FROM `".$tname."` LIKE 'price'";
			$columns = Yii::app()->db->createCommand($query)->queryAll();

			if (empty($columns)) {
				$query = "ALTER TABLE `".$tname."` ADD COLUMN `price` FLOAT NOT NULL DEFAULT 0.0 AFTER `shopifyVariantId`;";
				$rs = Yii::app()->db->createCommand($query)->execute();
			}
		}
	}

	public function safeDown()
	{
		$query = "SHOW TABLES like '%shopify_variant%'";
		$tables = Yii::app()->db->createCommand($query)->queryColumn();

		$tname = LtCourseSessionShopifyVariant::model()->tableName();
		if (in_array($tname, $tables)) {
			$query = "SHOW COLUMNS FROM `" . $tname . "` LIKE 'price'";
			$columns = Yii::app()->db->createCommand($query)->queryAll();

			if (!empty($columns)) {
				$query = "ALTER TABLE `".$tname."` DROP COLUMN `price`;";
				$rs = Yii::app()->db->createCommand($query)->execute();
			}
		}

		$tname = WebinarSessionShopifyVariant::model()->tableName();
		if (in_array($tname, $tables)) {
			$query = "SHOW COLUMNS FROM `" . $tname . "` LIKE 'price'";
			$columns = Yii::app()->db->createCommand($query)->queryAll();

			if (!empty($columns)) {
				$query = "ALTER TABLE `".$tname."` DROP COLUMN `price`;";
				$rs = Yii::app()->db->createCommand($query)->execute();
			}
		}
	}
}