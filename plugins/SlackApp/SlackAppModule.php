<?php

class SlackAppModule extends CWebModule
{

	protected static $_assetsUrl;


	public function init()
	{
		// import the module-level models and components
		$this->setImport(array(
			'SlackApp.assets.*',
			'SlackApp.models.*',
			'SlackApp.components.*',
		));

		if (Yii::app() instanceof CWebApplication) { //make sure this is not a console application
			// Register CSS ONLY if this is NOT an ajax call. Plugins' init() is always called during their loading
			if (!Yii::app()->request->isAjaxRequest) {
				Yii::app()->getClientScript()->registerCssFile(self::getAssetsUrl() . "/css/slack.css");
			}

			Yii::app()->request->noCsrfValidationRoutes[] = 'SlackApp/commands/docebotest';
			Yii::app()->request->noCsrfValidationRoutes[] = 'SlackApp/commands/lmsinfo';
			Yii::app()->request->noCsrfValidationRoutes[] = 'SlackApp/interactiveMessages/index';
			Yii::app()->request->noCsrfValidationRoutes[] = 'SlackApp/slackApp/signin';
		}

		$this->defaultController = 'slackApp';

		//set events
		//Yii::app()->event->on('', array($this, ''));
	}


	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}


	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu() {
		//compose the entire menu
		Yii::app()->mainmenu->addAppMenu('slack', array(
			'app_icon' => 'fa-slack',
			'settings' => Docebo::createAdminUrl('SlackApp/settings/edit'),
			'label' => Yii::t('app', 'Slack'),
		), array());
	}


	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public static function getAssetsUrl()
	{
		if (self::$_assetsUrl === null) {
			self::$_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('SlackApp.assets'));
		}
		return self::$_assetsUrl;
	}


	/**
	 * This method will be called in login page in order to allow plugin to add their custom login buttons
	 * @param string $layout the layout type of the login page
	 * @return string
	 */
	public function customLoginButton($layout = 'standard') {
		$output = '';
		$team = SlackHelper::getCurrentSlackTeam();
		if ($team instanceof SlackTeam && $team->enable_signin) {
			switch ($layout) {
				case 'standard':
					$output .= SlackHelper::getSignInButton();
					break;
				//TODO: other layouts ...
			}
		}
		return $output;
	}

}
