<?php

/**
* Plugin for SlackApp
* Note: for Apps the class name should match this:
* ucfirst($codename).'App';
* where $codename is the App.codename defined in Erp
*/

class SlackApp extends DoceboPlugin {


	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'SlackApp';

	/**
	* Returns the settings page (admin app)
	*/
	public static function settingsUrl() {
		return Docebo::createAdminUrl('SlackApp/settings/edit');
	}

	/**
	* This will allow us to access "statically" (singleton) to plugin methods
	* @param string $class_name
	* @return Plugin Object
	*/
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {
		parent::activate();

		/* @var $db CDbConnection */
		$db = Yii::app()->db;

		$sql = "CREATE TABLE IF NOT EXISTS `slack_team` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `domain` VARCHAR(255) NOT NULL,
  `client_id` VARCHAR(255) NOT NULL,
  `client_secret` VARCHAR(255) NOT NULL,
  `token` VARCHAR(255) NULL,
  `verification_token` VARCHAR(255) NULL,
  `branch_id` INT(11) NULL,
  `synch_info` INT(11) NOT NULL DEFAULT -1,
  `enable_signin` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;";
		$cmd = $db->createCommand($sql);
		$cmd->execute();

		$sql = "CREATE TABLE IF NOT EXISTS `slack_user` (
  `user_id` INT(11) NOT NULL,
  `team_id` INT(11) NOT NULL,
  `slack_id` VARCHAR(255) NOT NULL,
  `slack_name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`user_id`, `team_id`, `slack_id`),
  INDEX `fk_slack_user_team_id_slack_team_id_idx` (`team_id` ASC),
  CONSTRAINT `fk_slack_user_user_id_core_user_idst`
    FOREIGN KEY (`user_id`)
    REFERENCES `core_user` (`idst`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_slack_user_team_id_slack_team_id`
    FOREIGN KEY (`team_id`)
    REFERENCES `slack_team` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;";
		$cmd = $db->createCommand($sql);
		$cmd->execute();

		$sql = "CREATE TABLE IF NOT EXISTS `slack_log` (
  `id` INT(11) NOT NULL  AUTO_INCREMENT,
  `team_id` INT(11) NOT NULL,
  `type` VARCHAR(255) NOT NULL,
  `data` TEXT NULL,
  `datetime` TIMESTAMP NOT NULL,
  `job_id` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_slack_log_team_id_slack_team_id_idx` (`team_id` ASC),
  CONSTRAINT `fk_slack_log_team_id_slack_team_id`
    FOREIGN KEY (`team_id`)
    REFERENCES `slack_team` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;";
		$cmd = $db->createCommand($sql);
		$cmd->execute();

		Settings::save('slack_client_id', '18756162992.53193994325');
		Settings::save('slack_client_secret', 'a59117563224fb6f54d5b092ec987922');

		//Create the transport record for notification.
		//NOTE: this won't be removed when deactivating the app, since transports already need explicitly the plugin to be active.
		if (PluginManager::isPluginActive('NotificationApp')) {
			$slackPlugin = CorePlugin::model()->findByAttributes(array('plugin_name' => 'SlackApp'));
			if (!empty($slackPlugin)) {
				CoreNotificationTransport::addActiveTransport('slack', $slackPlugin->plugin_id, array(
					'manager_class' => 'SlackTransportManager',
					'editor_tool' => 'none',
					'template' => false
				));
			}
		}
	}


	public function deactivate() {

		Settings::remove('slack_client_id');
		Settings::remove('slack_client_secret');

		/* @var $db CDbConnection */
		$db = Yii::app()->db;

		$sql = "DROP TABLE `slack_log`;";
		$cmd = $db->createCommand($sql);
		$cmd->execute();

		$sql = "DROP TABLE `slack_user`;";
		$cmd = $db->createCommand($sql);
		$cmd->execute();

		$sql = "DROP TABLE `slack_team`;";
		$cmd = $db->createCommand($sql);
		$cmd->execute();

		parent::deactivate();
	}

}