<?php

/**
 * This is the model class for table "slack_log".
 *
 * The followings are the available columns in table 'slack_log':
 * @property integer $id
 * @property integer $team_id
 * @property string $type
 * @property string $datetime
 * @property string $data
 * @property string $job_id
 *
 * The followings are the available model relations:
 * @property SlackTeam $team
 */
class SlackLog extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'slack_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('team_id, type, datetime', 'required'),
			array('team_id', 'numerical', 'integerOnly'=>true),
			array('type, job_id', 'length', 'max'=>255),
			array('data', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, team_id, type, datetime, data, job_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'team' => array(self::BELONGS_TO, 'SlackTeam', 'team_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'team_id' => 'Team',
			'type' => 'Type',
			'datetime' => 'Datetime',
			'data' => 'Data',
			'job_id' => 'Job',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('team_id',$this->team_id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('datetime',$this->datetime,true);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('job_id',$this->job_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SlackLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function behaviors()
	{
		return array(
			'LocalTimeConversionBehavior' => array(
				'class' => 'common.components.LocalTimeConversionBehavior',
				'timestampAttributes' => array('datetime'),
				'dateAttributes' => array()
			)
		);
	}


}
