<?php

/**
 * This is the model class for table "slack_team".
 *
 * The followings are the available columns in table 'slack_team':
 * @property integer $id
 * @property string $domain
 * @property string $client_id
 * @property string $client_secret
 * @property string $token
 * @property string $verification_token
 * @property integer $branch_id
 * @property integer $synch_info
 * @property boolean $enable_signin
 *
 * The followings are the available model relations:
 * @property SlackUser[] $slackUsers
 */
class SlackTeam extends CActiveRecord
{

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'slack_team';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('domain', 'required'),
			array('branch_id, synch_info, enable_signin', 'numerical', 'integerOnly'=>true),
			array('domain, client_id, client_secret, token, verification_token', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, domain, client_id, client_secret, token, verification_token, branch_id, synch_info, enable_signin', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'slackUsers' => array(self::HAS_MANY, 'SlackUser', 'team_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'domain' => 'Domain',
			'client_id' => 'Client',
			'client_secret' => 'Client Secret',
			'token' => 'Token',
			'verification_token' => 'Verification Token',
			'branch_id' => 'Branch',
			'synch_info' => 'Synch Info',
			'enable_signin' => 'Enable Signin'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('domain',$this->domain,true);
		$criteria->compare('client_id',$this->client_id,true);
		$criteria->compare('client_secret',$this->client_secret,true);
		$criteria->compare('token',$this->token,true);
		$criteria->compare('verification_token',$this->verification_token,true);
		$criteria->compare('branch_id',$this->branch_id);
		$criteria->compare('synch_info',$this->synch_info,true);
		$criteria->compare('enable_signin',$this->enable_signin,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SlackTeam the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}



	public static function getSynchOptions() {
		return array(
			-1	=> Yii::t('salesforce', 'Manually'),
			 1	=> Yii::t('salesforce', 'Every {hour} hours', array('{hour}' => 1)),
			 4	=> Yii::t('salesforce', 'Every {hour} hours', array('{hour}' => 4)),
			 8	=> Yii::t('salesforce', 'Every {hour} hours', array('{hour}' => 8)),
			16	=> Yii::t('salesforce', 'Every {hour} hours', array('{hour}' => 16)),
			24	=> Yii::t('salesforce', 'Every {hour} hours', array('{hour}' => 24)),
		);
	}



	public function isAuthorized() {
		return (!$this->isNewRecord && !empty($this->domain) && !empty($this->token));
	}


	public function apiCall($method, $params) {

		//check team validity
		if ($this->isNewRecord || empty($this->token)) {
			throw new CException('Error: token not set');
		}

		//do the job
		$apiClient = SlackApiClient::getInstance();
		$result = $apiClient->callExternal($this->token, $method, $params);

		//check the result
		if (empty($result)) {
			//something went wrong while trying to call the API method
			throw new CException('API error: '.$apiClient->getError());
		}
		if (is_array($result) && isset($result['ok']) && !$result['ok']) {
			//the API method has been called, but something was not ok and an error has been returned
			throw new CException('API response error: '.$result['error']);
		}

		return $result;
	}


}
