<?php

class CommandsController extends SlackBaseJsonController {

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}



	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter

		$res[] = array(
			'allow',
			'actions' => array('docebotest', 'lmsinfo'),
			'users' => array('*'),
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}




	protected function _validateCommand() {

		//prepare output value
		$isValid = true;

		/* @var $rq CHttpRequest */
		$rq = Yii::app()->request;

		$token = $rq->getParam('token', '');
		$teamDomain = $rq->getParam('team_domain', '');

		$team = SlackTeam::model()->findByAttributes(array(
			'domain' => $teamDomain
		));

		if (empty($team)) {
			$this->setJSONData(array('text' => 'ERROR: Wrong specified team'));
			$isValid = false;
		} else if ($team->verification_token != $token) {
			$this->setJSONData(array('text' => 'ERROR: Invalid verification token'));
			$isValid = false;
		}

		if (!$isValid) {
			//something was wrong, just go away with an error message previously set
			exit(CJSON::encode($this->_jsonData));
		}
	}



	public function actionDocebotest() {

		$this->_validateCommand();

		/* @var $rq CHttpRequest */
		$rq = Yii::app()->request;

		$output = array();
		//$output['response_type'] = 'ephemeral';
		$output['text'] = 'Docebo core version: '.Settings::get('core_version', '(unknown)').' on PHP '.phpversion();
		$output['attachments'] = array(
			array('text' => 'You have written: '.$rq->getParam('text', ''))
		);
		$this->setJSONData($output);
	}


	public function actionLmsinfo() {

		$this->_validateCommand();

		/* @var $rq CHttpRequest */
		$rq = Yii::app()->request;


		$output = array(
			'text' => "Requesting LMS info",
			'attachments' => array(
				array(
					'text' => "Are you sure?",
					'fallback' => "You cannot get LMS info at the moment",
					'callback_id' => "lms_info",
					'color' =>  "#DE14DA",
					'attachment_type' => "default",
					'actions' => array(
						array(
							'name' => "confirm",
							'text' => "Yes",
							'type' => "button",
							'value' => "yes",
							'style' => "primary"
						),
						array(
							'name' => "confirm",
							'text' => "No",
							'type' => "button",
							'value' => "no"
						),
					)
				)
			)
		);
		$this->setJSONData($output);
	}


}