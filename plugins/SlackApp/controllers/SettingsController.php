<?php

class SettingsController extends Controller {


	protected $_assetsUrl;
	protected $_team;

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter

		$res[] = array(
			'allow',
			'actions' => array('signin'),
			'users' => array('*'),
		);

		// Allow following actions to admin only:
		$admin_only_actions = array('index', 'edit', 'authorize', 'axSynchronizeUsers', 'checkUsersSynchronizationStatus');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}




	public function beforeAction($action) {
		/* @var $rq CHttpRequest */
		$rq = Yii::app()->request;
		$teamId = $rq->getParam('team_id', false);
		if ($teamId !== false) {
			$this->_team = SlackTeam::model()->findByPk($teamId);
		} else {
			$this->_team = SlackHelper::getCurrentSlackTeam();
		}
		if (empty($this->_team)) {
			$this->_team = new SlackTeam();
		}

		//other action-specific preliminary operations
		switch ($action->id) {
			case 'edit':

				//preload some external stuff for org. chart tree selector
				$ft = new FancyTree();
				Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree-all.js', CClientScript::POS_HEAD);
				Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree.docebo.js', CClientScript::POS_HEAD);
				Yii::app()->getClientScript()->registerCssFile($ft->getAssetsUrl() . '/skin-tree-widget/ui.fancytree.css');
				Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl . '/css/DoceboPager.css');

				//make sure not to mess with orgchart selector
				$index = 'slack_selected_branch_'.$this->_team->id;
				Yii::app()->user->setState($index, null);

				break;
		}

		return parent::beforeAction($action);
	}


	public function getAssetsUrl() {
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('SlackApp.assets'));
		}
		return $this->_assetsUrl;
	}




	//========== ACTIONS ==========



	public function actionIndex() {
		/*
		In the future, if multi-team will be implemented, this may be the teams grid page
		*/
		$this->redirect('site/index'); //TEMPORARY FALLBACK
	}



	public function actionEdit() {

		/* @var $rq CHttpRequest */
		$rq = Yii::app()->request;
		$redirectUrl = Docebo::createAdminUrl('SlackApp/settings/edit'); //basically this same action

		//check if we are requesting a saving action
		if ($rq->getParam('save', false) !== false) {
			//'save' button has been pressed
			try {
				$this->saveSettings(array(
					'verification_token' => $rq->getParam('verification_token', ''),
					'enable_signin' => ($rq->getParam('enable_signin', 0) > 0 ? 1 : 0),
					'branch_id' => $rq->getParam('branch_id', false),
					'synch_info' => $rq->getParam('synch_info', -1)
				));

				$this->handleAutoSync();

				Yii::app()->user->setFlash('success', Yii::t('standard', '_OPERATION_SUCCESSFUL'));
			} catch(Exception $e) {
				Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
			}
			$this->redirect($redirectUrl);
		}

		//check if a Slack Job already exists
		$existingJob = CoreJob::model()->find('name = :jobName AND type = :type', array(
			':jobName' => SlackJobHandler::JOB_NAME,
			':type' => CoreJob::TYPE_ONETIME
		));
		if(!empty($existingJob)){
			//Delete the job if it's older than the maximum allowed lifetime
			$dateJob = strtotime(Yii::app()->localtime->fromLocalDateTime($existingJob->created)) + SlackJobHandler::JOB_MAX_LIFETIME;
			$dateNow = strtotime(Yii::app()->localtime->getLocalNow());
			if($dateNow > $dateJob){
				$existingJob->delete();
				$jobExists = false;
			} else {
				$jobExists = true;
			}
		} else {
			$jobExists = false;
		}

		//if no action is requested, just render
		$this->render('edit', array(
			'team' => $this->_team,
			'jobExists' => $jobExists
		));
	}


	public function actionAxSelectOrganizationChartNode()
	{
		/* @var $rq CHttpRequest */
		$rq = Yii::app()->request;

		$index = 'slack_selected_branch_'.$this->_team->id;
		$selectedNode = $rq->getParam('node', false);
		$state = Yii::app()->user->getState($index);
		if ($state) {
			$selectedNode = $state;
		}

		$selectedJson = $rq->getParam('orgcharts_selected-json', false);
		if ($selectedJson !== false) {
			Yii::app()->user->setState($index, $selectedJson);
			echo '<a class="auto-close" data-node="'.$selectedJson.'"></a>';
			echo '<script type="text/javascript">$("#slack_branch_id").val('.$selectedJson.'); updateBranchPath();</script>';
			Yii::app()->end();
		}

		$this->renderPartial('axSelectOrganizationChartNode', array('node' => $selectedNode), false, false);
	}


	public function actionAxGetBranchPath() {
		/* @var $rq CHttpRequest */
		$rq = Yii::app()->request;

		if ($rq->isAjaxRequest) {
			$output = array('path' => false);
			$branch_id = $rq->getParam('branch_id');
			if (!empty($branch_id)) {
				$output['path'] = $this->getBranchPath($branch_id);
			}
			echo CJSON::encode($output);
		}
	}





	public function actionAuthorize() {

		/* @var $rq CHttpRequest */
		/* @var $team SlackTeam */
		$rq = Yii::app()->request;
		$team = $this->_team;
		if (empty($team)) { $team = new SlackTeam(); }

		//check authorization code
		$code = $rq->getParam('code', false);
		if ($code !== false) {

			//we have been authorized by slack oauth and received a code

			try {

				$apiClient = SlackApiClient::getInstance();

				//check state
				$state = $rq->getParam('state', false);
				if (!$this->checkStateAuthorizeParameter($state)) {
					throw new CException('Invalid state parameter');
				}

				// Try to get an access token (using the authorization code grant)
				$callParams = array(
					'client_id' => SlackHelper::getClientId(),
					'client_secret' => SlackHelper::getClientSecret(),
					'code' => $code,
					'redirect_uri' => Docebo::createAbsoluteAdminUrl('SlackApp/settings/authorize')
				);

				$result = $apiClient->call('oauth.access', $callParams);
				if (empty($result)) {
					throw new CException('Unable to obtain authorization');
				}

				$decodedInfo = $result;
				if (!is_array($decodedInfo) || !isset($decodedInfo['ok']) || (isset($decodedInfo['ok']) && !$decodedInfo['ok'])) {
					throw new CException(isset($decodedInfo['error']) ? $decodedInfo['error'] : "An error occurred while retrieving token");
				}
				$this->clearStateAuthorizeParameter();  // make sure not to prevent future authorization requests
				$token = $decodedInfo['access_token'];
				$scope = $decodedInfo['scope'];
				if (empty($token)) {
					throw new CException('Invalid token');
				}

				//save token
				$this->_team->domain = $decodedInfo['team_name'];
				$this->_team->client_id = SlackHelper::getClientId(); // TODO: this filed will be removed from the table
				$this->_team->client_secret = SlackHelper::getClientSecret(); // TODO: this filed will be removed from the table
				$this->_team->token = $token;
				$this->_team->save();

				Yii::app()->user->setFlash('success', Yii::t('standard', '_OPERATION_SUCCESSFUL'));

			} catch (Exception $e) {

				Yii::log(__METHOD__.' EXCEPTION: '.$e->getMessage(), CLogger::LEVEL_ERROR);
				Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
			}
		} else {

			Yii::log(__METHOD__.' : Invalid code', CLogger::LEVEL_ERROR);
			Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
		}

		//go back to setting page (with flash message ready to get print)
		$backUrl = Docebo::createAbsoluteAdminUrl('SlackApp/settings/edit');
		$this->redirect($backUrl);
	}

	public function actionResetConfiguration(){
		$confirmYes = Yii::app()->request->getParam('confirm_yes', null);
		$redirectUrl = Docebo::createAdminUrl('SlackApp/settings/edit');

		if ($confirmYes === 'Yes') {
			$transaction = Yii::app()->db->beginTransaction();
			try {
				if (empty($this->_team)) {
					throw new CException('Invalid team');
				}
				if (!$this->_team->isNewRecord) {
					SlackUser::model()->deleteAllByAttributes(array('team_id' => $this->_team->id));
					$this->_team->delete();
					$this->_team = new SlackTeam(); // NOTE: overwrite the internal team reference in order to correctly recognize it as "not authorized" later
				}
				$this->clearStateAuthorizeParameter(); // make sure not to prevent future authorization requests
				$transaction->commit();
				Yii::app()->user->setFlash('success', Yii::t('standard', '_OPERATION_SUCCESSFUL'));
			} catch (Exception $e) {
				$transaction->rollback();
				Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
			}

			echo '<a class="auto-close"></a>';
			$this->renderPartial('lms.protected.views.site._js_redirect', array(
				'url'           => $redirectUrl,
				'showMessage'   => false
			));
			Yii::app()->end();
		}

		$this->renderPartial('_resetConfig', array(),false, true);
		Yii::app()->end();
	}


	public function actionAxSynchronizeUsers() {

		/* @var $team SlackTeam */
		$team = $this->_team;

		if (empty($team) || $team->isNewRecord) {
			//an already synchronized team is required to run this action
			$job = null;
		} else {
			$existingJob = CoreJob::model()->find('name = :jobName AND type = :type', array(
				':jobName' => SlackJobHandler::JOB_NAME,
				':type' => CoreJob::TYPE_ONETIME
			));

			if(empty($existingJob)){ // new job
				/* @var $job CoreJob */
				$job = Yii::app()->scheduler->createImmediateJob(SlackJobHandler::JOB_NAME, SlackJobHandler::id(), array(
					'job_type' => SlackJobHandler::TYPE_SYNCH_USERS,
					'team_id' => $team->id
				));
			} else { // fetch details for existing job
				$job = $existingJob;
			}
		}

		//check job creation result
		$output = array();
		if (empty($job)) {
			$output['success'] = false;
			$output['message'] = Yii::t('standard', '_OPERATION_FAILURE');
		} else {
			$output['success'] = true;
			$output['job_id'] = $job->id_job;
		}

		//send result to the client
		echo CJSON::encode($output);
		Yii::app()->end();
	}



	public function actionAxCheckUsersSynchronizationStatus() {

		/* @var $rq CHttpRequest */
		$rq = Yii::app()->request;
		$job_id = $rq->getParam('job_id', false);

		//find the synch. job
		$job = CoreJob::model()->findByAttributes(array(
			'id_job' => $job_id
		));

		//analyze job status and prepare output info
		$output = array();

		if (!empty($job)) {
			$output['status'] = 'running';
			$params = CJSON::decode($job->params);
			if (is_array($params) && isset($params['synchronized_users'])) {
				$already = $params['synchronized_users'];
				$total = $params['total_users'];
				$output['synchronized_users'] = $already;
				$output['total_users'] = $total;
				$output['percentage'] = $total <= 0 ? 0 : number_format($already / $total, 1, '.', '');
				$output['percentage'] = str_replace('.0', '', $output['percentage']); //remove unneeded decimal zeros, if present
				$output['message'] = Yii::t('slack', 'Synchronized users: {n}', array('{n}' => $params['synchronized_users'].'/'.$params['total_users']));
			} else {
				$output['percentage'] = 0;
			}
		} else {
			//immediate job is already finished and removed
			$output['status'] = 'done';
			$output['synch_info'] = $this->getLastSynchInfo();
		}


		/*
		if (!empty($job) && $job->is_running <= 0 && $job->last_status == CoreJob::STATUS_FAILED) {
			$output['status'] = 'error';
			$output['message'] = (!empty($job->last_error_message) ? $job->last_error_message : Yii::t('standard', '_OPERATION_FAILURE'));
			$job->delete(); //make sure not to leave "dirt" in the jobs database
		}
		if (!empty($job) && $job->is_running > 0) {
			//job is still doing stuff
			$output['status'] = 'running';
		}
		if (empty($job) || ($job->is_running <= 0 && $job->last_status == CoreJob::STATUS_SUCCESS)) {
			$output['status'] = 'done';
			$params = CJSON::decode($job->params);
			if (is_array($params) && isset($params['synchronized_users'])) {
				$already = $params['synchronized_users'];
				$total = $params['total_users'];
				$output['synchronized_users'] = $already;
				$output['total_users'] = $total;
				$output['percentage'] = $total <= 0 ? 0 : number_format($already / $total, 1, '.', '');
				$output['percentage'] = str_replace('.0', '', $output['percentage']); //remove unneeded decimal zeros, if present
				$output['message'] = Yii::t('slack', 'Synchronized users: {n}', array('{n}' => $params['synchronized_users'].'/'.$params['total_users']));
				$output['synch_info'] = $this->getLastSynchInfo();
			}
		}
		*/

		//send result to the client
		echo CJSON::encode($output);
		Yii::app()->end();
	}



	public function actionLogs() {
		$cmd = Yii::app()->db->createCommand();
		$cmd->select("*");
		$cmd->from(SlackLog::model()->tableName());
		$cmd->where("team_id = :team_id", array(':team_id' => $this->_team->id));
		$cmd->order("datetime DESC");
		$cmd->limit(1, 0);
		$logModel = $cmd->queryRow(true);

		$params = CJSON::decode($logModel['data']);
		$logErrors = (count($params['messages']) >= 1)?$params['messages']:array();

		$dataProvider = new CArrayDataProvider($logErrors, array(
			'keyField' => false,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			)
		));

		$this->renderPartial('_logs', array(
			'logModel' => $logModel,
			'params' => $params,
			'dataProvider' => $dataProvider
		), false, true);
	}



	//========== INTERNAL METHODS ================


	/**
	 * Generates a random state check parameter for authorize requests, then stores it in session
	 * @return string the generated value
	 */
	protected function generateStateAuthorizeParameter() {
		//first check if a value has already been generated and stored in session
		$already = Yii::app()->session['slack.authorize.state'];
		if (!empty($already)) { return $already; }
		//if not, then generate it
		$value = sha1('slack_authorize.'.Yii::app()->user->id.'.'.time());
		Yii::app()->session['slack.authorize.state'] = $value;
		return $value;
	}

	/**
	 * Check if a state value is equal to the one stored in session (empty values always return a false result)
	 * @param $value the value to be checked
	 * @return bool
	 */
	protected function checkStateAuthorizeParameter($value) {
		if (empty($value)) { return false; }
		$checker = Yii::app()->session['slack.authorize.state'];
		return ($value == $checker);
	}

	/**
	 * Clear the state value in order to start new authorization operations
	 */
	protected function clearStateAuthorizeParameter() {
		Yii::app()->session['slack.authorize.state'] = null;
		unset(Yii::app()->session['slack.authorize.state']);
	}



	/**
	 * Renders the full path of an org. chart branch
	 * @param $branch_id the numeric ID of an org. chart branch
	 * @return bool|string
	 */
	protected function getBranchPath($branch_id) {
		if ($branch_id != CoreOrgChartTree::getOrgRootNode()->idOrg) {
			$node = CoreOrgChartTree::model()->findByPk($branch_id);
			if (!empty($node)) {
				$separator = '<span class="branch-path-separator">&rsaquo;</span>';
				return $separator . $node->getNodePath($separator, true)/* . $separator . CoreOrgChartTree::getTranslatedName($branch_id)*/;
			}
		}
		return '';
	}



	protected function saveSettings(array $newSettings) {

		if (empty($newSettings)) { return false; }

		try {
			$this->_team->setAttributes($newSettings);
			if (!$this->_team->validate()) {
				//passed data is invalid, we have to handle the error
				throw new CException('Invalid parameters');
			}

			$this->_team->save();

		} catch (Exception $e) {

			$output = false;
		}

		return $output;
	}



	protected function getLastSynchInfo() {

		$teamId = $this->_team->id;

		$logsCount = SlackLog::model()->count("team_id = :team_id", array(':team_id' => $teamId));
		if ($logsCount > 0) {
			/* @var $cmd CDbCommand */
			$cmd = Yii::app()->db->createCommand();
			$cmd->select("*");
			$cmd->from(SlackLog::model()->tableName());
			$cmd->where("team_id = :team_id", array(':team_id' => $teamId));
			$cmd->order("datetime DESC");
			$cmd->limit(1, 0);
			$lastLog = $cmd->queryRow(true);
			$lastDate = Yii::app()->localtime->toLocalDateTime($lastLog['datetime']);
		} else {
			$lastDate = '';
		}
		$hrefViewLog = Docebo::createAdminUrl('SlackApp/settings/logs');

		return '<p class="sync_info">'
			.Yii::t('salesforce', 'Last sync:')
			.'<span class="users_last_date'.($logsCount > 0 ? '' : ' no-logs').'">'
				.'<span class="usersSyncMsgHasLog">'
					.'<span class="last-date">'.$lastDate.'</span>'
					.'&nbsp;|&nbsp;'
					.'<a href="'.$hrefViewLog.'" class="underline blue-text open-dialog">'.Yii::t('salesforce', 'View logs').'</a>'
				.'</span>'
				.'<span class="usersSyncMsgNoLog">'
					.Yii::t('standard', '_NEVER')
				.'</span>'
			.'</span>'
		.'</p>';
	}

	protected function handleAutoSync(){
		$jobModel = CoreJob::model()->findByAttributes(array(
			'name' => SlackJobHandler::JOB_NAME,
			'type' => CoreJob::TYPE_RECURRING
		));
		if($this->_team->synch_info > 0){ // Enable auto sync
			$params = array(
				'job_type' => SlackJobHandler::TYPE_AUTO_SYNCH,
				'team_id' => $this->_team->id
			);
			if(empty($jobModel)){ // Create a new auto sync job
				$job = Yii::app()->scheduler->createJob(
					SlackJobHandler::JOB_NAME,
					SlackJobHandler::id(),
					CoreJob::TYPE_RECURRING,
					'hour',
					$this->_team->synch_info,
					0,  // minute is always 0
					0,
					'UTC',
					null,
					null,
					null,
					$params
				);
			} else { // Update the existing job
				$jobModel->rec_length = $this->_team->synch_info;
				$jobModel->save();
			}
		} else { // Disable auto sync
			if(!empty($jobModel)){
				$jobModel->delete();
			}
		}
	}
}