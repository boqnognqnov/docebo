<?php

class InteractiveMessagesController extends SlackBaseJsonController {


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}



	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter

		$res[] = array(
			'allow',
			'actions' => array('index'),
			'users' => array('*'),
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}




	public function actionIndex() {

		/* @var $rq CHttpRequest */
		$rq = Yii::app()->request;

Yii::log("DEBUG:\n".print_r($_POST, true));
		$output = array(); //initialize output variable
		try {

			//read JSON payload of the request
			$payload = $rq->getParam('payload', false);
			if (empty($payload)) {
				throw new CException('Invalid input arguments');
			}

			/*
			Example of payload data structure: (see also "https://api.slack.com/docs/message-buttons#how_to_respond_to_message_button_actions")
			{
				"actions": [
					{
						"name": "recommend",
						"value": "yes"
					}
				],
				"callback_id": "comic_1234_xyz",
				"team": {
					"id": "T47563693",
					"domain": "watermelonsugar"
				},
				"channel": {
					"id": "C065W1189",
					"name": "forgotten-works"
				},
				"user": {
					"id": "U045VRZFT",
					"name": "brautigan"
				},
				"action_ts": "1458170917.164398",
				"message_ts": "1458170866.000004",
				"attachment_id": "1",
				"token": "xAB3yVzGS4BQ3O9FACTa8Ho4",
				"original_message": "{\"text\":\"New comic book alert!\",\"attachments\":[{\"title\":\"The Further Adventures of Slackbot\",\"fields\":[{\"title\":\"Volume\",\"value\":\"1\",\"short\":true},{\"title\":\"Issue\",\"value\":\"3\",\"short\":true}],\"author_name\":\"Stanford S. Strickland\",\"author_icon\":\"https://api.slack.com/img/api/homepage_custom_integrations-2x.png\",\"image_url\":\"http://i.imgur.com/OJkaVOI.jpg?1\"},{\"title\":\"Synopsis\",\"text\":\"After @episod pushed exciting changes to a devious new branch back in Issue 1, Slackbot notifies @don about an unexpected deploy...\"},{\"fallback\":\"Would you recommend it to customers?\",\"title\":\"Would you recommend it to customers?\",\"callback_id\":\"comic_1234_xyz\",\"color\":\"#3AA3E3\",\"attachment_type\":\"default\",\"actions\":[{\"name\":\"recommend\",\"text\":\"Recommend\",\"type\":\"button\",\"value\":\"recommend\"},{\"name\":\"no\",\"text\":\"No\",\"type\":\"button\",\"value\":\"bad\"}]}]}",
				"response_url": "https://hooks.slack.com/actions/T47563693/6204672533/x7ZLaiVMoECAW50Gw1ZYAXEM"
			}
			*/

			//decode and validate input data
			$input = CJSON::decode($payload);
			if (empty($input)) {
				throw new CException('Invalid input arguments');
			}
			//check token and team info
			$team = SlackTeam::model()->findByAttributes(array(
				'domain' => ''.$input['team']['domain']
			));
			if (empty($team)) {
				throw new CException('Invalid team');
			}
			if ($team->verification_token != $input['token']) {
				throw new CException('Invalid token');
			}

			//dispatch callback
Yii::log("DEBUG: callback_id = ".$input['callback_id']);
			switch ($input['callback_id']) {
				case 'lms_info':
					$output = $this->getLmsInfo($input);
					break;
				default:
					throw new CException('Invalid specified callback action: '.$input['callback_id']);
					break;
			}

		} catch (Exception $e) {

			//something went wrong, just send back an error message
			$output = array(
				'text' => $e->getMessage()
			);
		}

		$this->setJSONData($output);
	}



	protected function getLmsInfo($params) {

		//validate input
		if (!isset($params['actions']) || !is_array($params['actions'])) {
			throw new CException('Invalid specified action');
		}

		//check pressed button
		$isConfirmed = false;
		foreach ($params['actions'] as $action) {
			if ($action['name'] == 'confirm' && $action['value'] == 'yes') {
				$isConfirmed = true;
			}
		}

		if (!$isConfirmed) {
			return array(
				'text' => "You declined info request"
			);
		}

		$output = array(
			'text' => 'LMS info:',
			'attachments' => array(
				array(
					'text' => "Title: ".Settings::get('page_title', '')
				),
				array(
					'text' => "URL: ".Settings::get('url', '')
				),
				array(
					'text' => "Version: ".Settings::get('core_version', '')
				)
			)
		);
		return $output;
	}

}