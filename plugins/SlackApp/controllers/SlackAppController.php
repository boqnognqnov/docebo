<?php

class SlackAppController extends Controller {

	protected $_assetsUrl;

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter

		$res[] = array(
			'allow',
			'actions' => array('signin'),
			'users' => array('*'),
		);

		/*
		// Allow following actions to admin only:
		$admin_only_actions = array();
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);
		*/

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}



	public function getAssetsUrl() {
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('SlackApp.assets'));
		}
		return $this->_assetsUrl;
	}



	/**
	 * login from Slack sign in button
	 */
	public function actionSignin(){
		//Yii::log("DEBUG ".__METHOD__.":\n".print_r($_REQUEST, true));
		//echo "<pre>TEST:\n".print_r($_REQUEST, true)."</pre>";

		/* @var $rq CHttpRequest */
		$rq = Yii::app()->request;

		$apiClient = SlackApiClient::getInstance();

		//check team
		/* @var $team SlackTeam */
		$team = SlackHelper::getCurrentSlackTeam();
		if (empty($team)) {
			throw new CException('Invalid Slack credentials');
		}

		$mobile = false;
		if ( isset($_SERVER) && is_array($_SERVER) ) {
			if (preg_match("/(?:mobile)/i", $_SERVER['HTTP_REFERER'])) {
				$mobile = true;
			}
		}

		//check authorization code
		$code = $rq->getParam('code', false);
		if ($code !== false) {
			//we have been authorized by slack oauth and received a code

			/*
			//check state
			$state = $rq->getParam('state', false);
			$checkState = md5('test');
			if (empty($state) || $state != $checkState) {
				echo 'ERROR: invalid state ....';
				return;//Yii::app()->end();
			}
			*/

			// Try to get an access token (using the authorization code grant)
			$tokenUrl = 'https://slack.com/api/oauth.access';
			$data = array(
				'client_id' => $team->client_id,
				'client_secret' => $team->client_secret,
				'code' => $code,
				'redirect_uri' => Docebo::createAbsoluteAdminUrl('SlackApp/slackApp/signin'),
			);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $tokenUrl);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$result = curl_exec($ch);
			if ($result === false) {
				echo 'ERROR: cannot obtain token ('. curl_error($ch).')';
				return;//Yii::app()->end();
			}

			$decodedInfo = CJSON::decode($result);
			if (empty($decodedInfo)) {
				echo 'RESULT: '.$result;
				echo 'ERROR: invalid token result ....';
				return;//Yii::app()->end();
			}
			if (isset($decodedInfo['ok']) && !$decodedInfo['ok']) {
				echo 'RESULT: '.$result;
				echo 'ERROR: '.(isset($decodedInfo['error']) ? $decodedInfo['error'] : "error while retrieving token ...");
				return;//Yii::app()->end();
			}
			$token = $decodedInfo['access_token'];
			$scope = $decodedInfo['scope'];
			if (empty($token)) {
				echo 'RESULT: '.$result;
				echo 'ERROR: invalid decoded token ....';
				return;//Yii::app()->end();
			}


			//check user identity now
			//lack.com/api/users.identity?token=awarded_token
			$identity = $apiClient->call('users.identity', array(
				'token' => $token
			));
			/* Example of response from identity:
			{
				"ok": true,
				"user": {
						"name": "Sonny Whether",
						"id": "U0G9QF9C6"
				},
				"team": {
					"id": "T0G9PQBBK"
				}
			}
			*/
			if (empty($identity) || !is_array($identity)) {
				throw new CException("Invalid user identity");
			}
			if (!isset($identity['ok']) || !$identity['ok']) {
				throw new CException("An error occurred while identifying user: ".$identity['error']);
			}

			//check if the user exists in the database
			$localSlackUser = SlackUser::model()->findByAttributes(array(
				'team_id' => $team->id,
				'slack_id' => $identity['user']['id']
			));
			if (empty($localSlackUser)) {
				Yii::app()->user->setFlash('error', Yii::t('login', '_NOACCESS'));
				$this->redirect(Docebo::createLmsUrl('site/index'));
			}

			//find LMS user and try to log in in the system
			$userModel = CoreUser::model()->findByPk($localSlackUser->user_id);
			if ($userModel) {

				// Search for the user in DB and authenticate him without password
				$ui = new DoceboUserIdentity($userModel->userid, null);
				if ($ui->authenticateWithNoPassword()) {
					Yii::app()->user->login($ui);

					// if the authentication goes well we need to check for a suspended installation process
					Docebo::resumeSaasInstallation();

					// Determine which URL to jump to
					if ($mobile) {
						$redirect_url = Yii::app()->createMobileUrl('site/index', array('login' => 1));
					} else {
						$redirect_url = Docebo::getUserAfetrLoginRedirectUrl();
					}

					// *** HOOK FOR PLUGINS ***
					$loginEvent = new DEvent($this, array('redirect_url' => &$redirect_url));
					Yii::app()->event->raise('ActionSsoLogin', $loginEvent);

					// Finally, redirect!!
					$this->redirect($redirect_url);

				} else {

					//TODO: perform slack logout ?
					$ui->logFailedLoginAttempt();
					//throw new Exception("Invalid username or user suspended", 2);
					Yii::app()->user->setFlash('error', "Invalid username or user suspended");
					$this->redirect(Docebo::createLmsUrl('site/index'));
				}

			} else {
				Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
				$this->redirect(Docebo::createLmsUrl('site/index'));
			}

		} else {

			//no code present
			throw new CException("Error while trying to authorize the sign in");

		}


	}






}
