<?php


class SlackTransportManager extends NotificationTransportManager {


	public function getName() {
		return 'slack';
	}


	public function getLabel() {
		return Yii::t('slack', 'Slack');
	}


	public function getTextFormatType() {
		return self::TEXT_FORMAT_MARKDOWN;
	}


	public function getAdditionalFormParameters(CoreNotification $notification) {
		$params = array(
			'recipient_type' => 'user',
			'recipient_channel' => ''
		);
		if (!$notification->isNewRecord) {
			$query = "SELECT ta.* 
				FROM ".CoreNotificationTransportActivation::model()->tableName()." ta
				JOIN ".CoreNotificationTransport::model()->tableName()." t ON (t.id = ta.transport_id AND t.name = :name)
				WHERE ta.notification_id = :notification_id";
			/* @var $cmd CDbCommand */
			$cmd = Yii::app()->db->createCommand($query);
			$record = $cmd->queryRow(true, array(
				':name' => $this->getName(),
				':notification_id' => $notification->id
			));
			if (!empty($record)) {
				$info = CJSON::decode($record['additional_info']);
				if (!empty($info) && is_array($info)) {
					$params['recipient_type'] = isset($info['recipient_type']) ? $info['recipient_type'] : '';
					$params['recipient_channel'] = isset($info['recipient_channel']) ? $info['recipient_channel'] : '';
				}
			}
		}
		//retrieve all slack channels in the list
		$channels = array();
		$apiClient = SlackApiClient::getInstance();
		$result = $apiClient->call('channels.list', array('exclude_archived' => 1));
		if (is_array($result) && isset($result['channels']) && !empty($result['channels'])) {
			foreach ($result['channels'] as $channel) {
				$channels[$channel['id']] = '#'.$channel['name'];
			}
		}
		if (empty($channels)) {
			//if no channels are listed, then do not display options but just go straight for single users recipients
			$html = '';
			$html .= CHtml::hiddenField('slack_transport_additional_info[recipient_type]', 'user');
			$html .= CHtml::hiddenField('slack_transport_additional_info[recipient_channel]', '');
			return $html;
		} else {
			$params['channels'] = $channels;
		}
		return Yii::app()->controller->renderPartial('plugin.SlackApp.views.transports._slack_options', $params, true);
	}


	public function readInputParameters($input) {
		$params = (is_array($input) && isset($input['slack_transport_additional_info']) ?  $input['slack_transport_additional_info'] : array());
		return array(
			'recipient_type' => (is_array($params) && isset($params['recipient_type']) ? $params['recipient_type'] : 'user'),
			'recipient_channel' => (is_array($params) && isset($params['recipient_channel']) ? $params['recipient_channel'] : '')
		);
	}


	public function getEditorHintText() {
		return Yii::t('slack', 'Check out how to add text formatting to your slack message. {link}', array(
			'{link}' => CHtml::link(Yii::t('offlineplayer', 'Click here'), 'https://get.slack.help/hc/en-us/articles/202288908-Formatting-your-messages', array('target' => '_blank'))
		));
	}



	public function deliver($finalDataHash, $notHandler, $notification) {

		$transportId = $this->getTransportId();

		//generic client for API
		$apiClient = SlackApiClient::getInstance();

		/* @var $team SlackTeam */
		$team = SlackHelper::getCurrentSlackTeam();
		if (empty($team)) {
			return false;
		}

		$allTeamUsers = array();
		/* @var $cmd CDbCommand */
		$cmd = Yii::app()->db->createCommand("SELECT * FROM ".SlackUser::model()->tableName()." WHERE team_id = :team_id");
		$reader =$cmd->query(array(':team_id' => $team->id));
		if ($reader) {
			while ($record = $reader->read()) {
				$allTeamUsers[$record['user_id']] = array(
					'id' => $record['slack_id'],
					'name' => $record['slack_name']
				);
			}
		}

		//--- check recipient type (users or single channel ---
		$additionalInfo = CoreNotificationTransportActivation::getAdditionalInfo($notification->id, 'slack');
		if (is_array($additionalInfo) && isset($additionalInfo['recipient_type']) && $additionalInfo['recipient_type'] == 'channel') {
			$channelID = $additionalInfo['recipient_channel'];
			$language = Settings::get('default_language', 'english');
			//create a dummy recipient to be used in the next 'for' cycle instead of the standard recipients list
			$dummyUser = CoreUser::model()->findByAttributes(array('userid' => '/Anonymous'));
			$channelRecipient = array(
				'idst' => $dummyUser->idst, //we need a valid idst for following operations
				'channel_id' => $channelID,
				'username' => '',
				'email' => '',
				'language' => $language,
				'timezone' => 'UTC',
				'date_format_locale' => 'en',
				'courses' => false
			);
			//extract all extra fields of recipient(s) and assign them to our dummy channel recepient
			if (!empty($finalDataHash['recipients'])) {
				$sampleRecipient = array_pop($finalDataHash['recipients']);
				foreach ($sampleRecipient as $s_index => $s_value) {
					if (!array_key_exists($s_index, $channelRecipient)) {
						$channelRecipient[$s_index] = $s_value;
					}
				}
			}
			//create effective recipients list to be processed later
			$recipients = array($channelRecipient);
		} else {
			$recipients = $finalDataHash['recipients'];
		}
		//--- ---


		//send stuff to recipients
		$savedCount = 0;
		foreach ($recipients as $recipient) {

			if (isset($recipient['channel_id']) && !empty($recipient['channel_id'])) {

				$to = true;
				$language = $recipient['language'];

			} else {

				//first check if the user is effectively a slack one
				$isSlack = array_key_exists($recipient['idst'], $allTeamUsers);
				if (!$isSlack) {
					continue;
				}

				//check if user is valid in the LMS
				$user = CoreUser::model()->findByPk(trim($recipient['idst']));
				if ($user && (!$user->valid || $user->isUserExpired())) {
					continue;
				}

				$language = $recipient['language'];
				if (!array_key_exists('user', $recipient)) {
					$recipient['user'] = $recipient['idst'];
				}
			}

			// Get shortcodes data from the particular handler.
			// We pass the handler the recipient data structure that was returned to us from it earlier.
			// That way, the handler KNOWS what to expect from this call and use it on its own discretion.
			// This forth-and-back "dance" is to provide an abstraction and centralized cycle for sending emails.
			// Otherwise we must code email sending into every notification handler class.
			$shortcodesData = $notHandler->getShortcodesData($recipient, $finalDataHash['metadata'], $this->getTextFormatType());

			//get special properties from this handler and see if any of them is usable by Slack
			$specialProperties = $notHandler->getSpecialProperties($recipient, $finalDataHash['metadata']);

			$to = trim($recipient['idst']);
			if ($to) {
				$subject = $finalDataHash['subjects'][$transportId][$language];
				$message = $finalDataHash['messages'][$transportId][$language];

				// Fix some legacy shortcodes
				$subject = str_replace(array("[firstname]", "[lastname]", "[password]"), array("[first_name]", "[last_name]", "[user_password]"), $subject);
				$message = str_replace(array("[firstname]", "[lastname]", "[password]"), array("[first_name]", "[last_name]", "[user_password]"), $message);

				$subject = str_replace(array_keys($shortcodesData), $shortcodesData, $subject);
				$message = str_replace(array_keys($shortcodesData), $shortcodesData, $message);

				// If you want your date tag to use the format functionality, just put it in the array: $FORMATTABLE_DATE_TAGS
				$subject = NotificationJobHandler::manageDateTagsFormat($subject, CoreNotification::$FORMATTABLE_DATE_TAGS, $shortcodesData);
				$message = NotificationJobHandler::manageDateTagsFormat($message, CoreNotification::$FORMATTABLE_DATE_TAGS, $shortcodesData);

				// NOTE: ICS attachments are not allowed at the moment for slack notifications at the moment
				$message = str_replace(CoreNotification::SC_CALENDAR_ATTACHMENT, '', $message);

				// compose slack message
				if (isset($recipient['channel_id']) && !empty($recipient['channel_id'])) {
					$apiParams = array(
						'channel' => $recipient['channel_id'], //direct message to the user in slack
						'as_user' => false
					);
				} else {
					$apiParams = array(
						'channel' => '@' . $allTeamUsers[$recipient['idst']]['name'], //direct message to the user in slack (NOTE: it will be showed in the "slackbot" chat of the recipient)
						'as_user' => false
					);
				}
				$apiParams['text'] = '*'.$subject.'*';
				$apiParams['fallback'] = '';
				$apiParams['username'] = SlackHelper::getDoceboBotName();
				$apiParams['attachments'] = array();

				//start preparing message attachment
				$tmp_attachment = array(
					'mrkdwn_in' => array('text'),
					'color' => '#cccccc',
					'text' => $message,
					'fallback' => $message
				);

				//apply special properties
				if (is_array($specialProperties)) {
					foreach ($specialProperties as $sp_key => $sp_value) {
						if (!empty($sp_value)) {
							switch ($sp_key) {
								case 'author':
									$tmp_attachment['author_name'] = $sp_value;
									break;
								case 'avatar':
									$tmp_attachment['author_icon'] = $sp_value;
									break;
								case 'thumb':
									$tmp_attachment['thumb_url'] = $sp_value;
									break;
								case 'image':
									$tmp_attachment['image_url'] = $sp_value;
									break;
							}
						}
					}
				}

				$apiParams['attachments'][] = $tmp_attachment;

				$result = $apiClient->call('chat.postMessage', $apiParams);
				if ($result) {
					$savedCount++;
					Yii::log("Delivered to ".$to, CLogger::LEVEL_INFO);
				}else{
					$error = $apiClient->getError();
					Yii::log("Unable to send slack message to user ".$to.($error ? " (ERROR:".$error.")" : ""), CLogger::LEVEL_WARNING);
				}

			} else {
				Yii::log("Notification '{$notification->type}', target user #".$recipient['idst']." has no email. Skipped.", CLogger::LEVEL_WARNING);
			}
		}
		Yii::log('DEBUG: Total sent messages to slack users: '.$savedCount, CLogger::LEVEL_INFO);
	}



	public function loadEditorAssets() {
		$assetsPath = SlackAppModule::getAssetsUrl();
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile($assetsPath . '/css/transport.css');
	}


}