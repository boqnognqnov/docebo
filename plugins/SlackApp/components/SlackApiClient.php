<?php

class SlackApiClient {


	private static $_instance = NULL;

	private static $_errorMessage = false;

	private static $_token = false;


	/* Since Slack has a limit on possible API calls during time, we try to manage it by code */
	protected $max_api_calls_per_second = 1;


	private function __construct() {
		//NOTE: this is private in order to make a "factory" class
	}


	public static function getInstance() {
		if (self::$_instance === NULL) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}


	public static function getToken() {
		if (self::$_token === false) {
			$teamModel = SlackHelper::getCurrentSlackTeam();
			if (!empty($teamModel) && !empty($teamModel->token)) {
				self::$_token = $teamModel->token;
			}
		}
		return self::$_token;
	}


	private static function setError($message) {
		$message = trim($message);
		if (!empty($message)) {
			Yii::log(__CLASS__.' ERROR: '.$message, CLogger::LEVEL_ERROR);
			self::$_errorMessage = $message;
		}
	}

	public static function getError() {
		return self::$_errorMessage;
	}

	private static function resetError() {
		self::$_errorMessage = false;
	}


	/**
	 * This function should ensure that no more than {$this->max_api_calls_per_second} API calls are performed in a second
	 */
	protected function startCheckingLastAPICallTime() {
		//check current user
		$uid = Yii::app()->user->id;
		if (empty($uid) || is_null($uid)) {
			//maybe we are in a background async. job, with no current user itself?
			$uid = -1;
		}
		//start doing checks
		if ($this->max_api_calls_per_second <= 0) return; //no limit of API calls per second
		$time_slice = ceil(1000000 / $this->max_api_calls_per_second); //microseconds, time to wait between each API call
		if ($time_slice <= 0) return; //extreme case
		$last_api_call_time_J = Settings::get('slack_last_api_call_time', null);
		$current_time_MT = microtime();
		if (empty($last_api_call_time_J)) {
			//this means that there have been no API calls yet on this server, so just set the setting and go on
			Settings::save('slack_last_api_call_time', CJSON::encode(array(
				'start_time' => $current_time_MT,
				'end_time' => "",
				'user_calling' => $uid
			)));
		} else {
			//some API calls have already been made, check if enough time has passed
			$go_on = false;
			while (!$go_on) {
				//first check if someone else is already performing API calls in this moment
				$previous_value = CJSON::decode($last_api_call_time_J);
				if (empty($previous_value)) { throw new CException('Error while checking API call time'); }
				if ($previous_value['user_calling'] != 0 && empty($previous_value['end_time'])) {
					usleep(100000); //wait an arbitrary 0.1 seconds and then re-check
					$last_api_call_time_J = Settings::get('slack_last_api_call_time', null);
					$current_time_MT = microtime();
				} else {
					$go_on = true;
				}
			}
			//reserve the right to API-call for the current user
			//(other user accessing the API calling function will wait until we are finished here, see above 'while' cycle)
			Settings::save('slack_last_api_call_time', CJSON::encode(array(
				'start_time' => $current_time_MT,
				'end_time' => "",
				'user_calling' => $uid
			)));
			//check last API call ending time
			$last_api_call_time_MT = $previous_value['end_time'];
			list($current_time_uS, $current_time_S) = explode(' ', $current_time_MT);
			list($last_api_call_time_uS, $last_api_call_time_S) = explode(' ', $last_api_call_time_MT);
			$diff_S = (doubleval($current_time_S) - doubleval($last_api_call_time_S));
			$diff_uS = (doubleval($current_time_uS) - doubleval($last_api_call_time_uS));
			//NOTE: since max time slice is 1 second, $diff_S (difference in seconds) has just to be less than 1
			if (($diff_S < 1) && (floor($diff_uS * 1000000) < $time_slice)) {
				$waiting_time = (int)($time_slice - floor($diff_uS * 1000000));
				Yii::log('Slack API call delay: '.number_format($waiting_time, 0, '', '').' microseconds', CLogger::LEVEL_INFO);
				usleep($waiting_time);
			}
		}
	}


	/**
	 * Finalize the API call checks
	 */
	protected function stopCheckingLastAPICallTime() {
		//check current user
		$uid = Yii::app()->user->id;
		if (empty($uid) || is_null($uid)) {
			//maybe we are in a background async. job, with no current user itself?
			$uid = -1;
		}
		//do checks
		$previous_value = Settings::get('slack_last_api_call_time', null);
		if (empty($previous_value)) { return; } //this should not happen, anyway nothing else to do here
		$value = CJSON::decode($previous_value);
		if (empty($value) || $value['user_calling'] != $uid) { throw new CException('Error while checking API call time'); }
		//API call is finished, so set the end time and remove the user ID from calling parameter
		$value['end_time'] = microtime();
		$value['user_calling'] = 0;
		Settings::save('slack_last_api_call_time', CJSON::encode($value));
	}



	private function _makeCall($method, $callParams) {
		$url = $url = 'https://slack.com/api/'.$method;

		$this->startCheckingLastAPICallTime();

		//pre-process parameters list in order to make them acceptable to slack API
		$finalParams = array();
		if (is_array($callParams) && !empty($callParams)) {
			foreach ($callParams as $key => $value) {
				if (is_array($value)) {
					$finalParams[$key] = CJSON::encode($value);
				} else {
					$finalParams[$key] = $value;
				}
			}
		}

		//do effective API call through CURL
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($finalParams));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$result = curl_exec($ch);

		$this->stopCheckingLastAPICallTime();

		if (empty($result)) {
			self::setError(curl_error($ch));
			return false;
		}

		$decoded = CJSON::decode($result);
		if (empty($decoded)) {
			self::setError("Invalid API result");
			return false;
		}

		return $decoded;
	}


	public function call($method, array $params = array()) {

		self::resetError();

		$tokenRequested = ($method != 'oauth.access');

		$callParams = $params;
		if ($tokenRequested && !isset($callParams['token'])) {
			$token = self::getToken();
			if (empty($token)) {
				self::setError("Token has not been set yet");
				return false;
			}
			$callParams['token'] = $token;
		}

		return $this->_makeCall($method, $callParams);
	}


	public function callExternal($tokenInfo, $method, array $params = array()) {

		self::resetError();

		$tokenRequested = ($method != 'oauth.access');

		$callParams = $params;
		if ($tokenRequested) {
			if (is_object($tokenInfo) && $tokenInfo instanceof SlackTeam) {
				$token = $tokenInfo->token;
			} else if (is_string($tokenInfo) && !empty($tokenInfo)) {
				$token = $tokenInfo;
			} else {
				self::setError('Invalid token provided');
				return false;
			}
			$callParams['token'] = $token;
		}

		return $this->_makeCall($method, $callParams);
	}

}