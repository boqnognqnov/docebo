<?php

class SlackBaseJsonController extends Controller {

	/**
	 * This is the data content that will be rendered in the "afterAction" method
	 * @var null/array/object
	 */
	private $_jsonData = NULL;


	protected function setJSONData($data) {
		$this->_jsonData = $data;
	}

	protected function getJSONData() {
		return $this->_jsonData;
	}


	protected function beforeAction($action) {
		ob_clean(); // clear output buffer to avoid rendering anything else
		header('Content-type: application/json'); // set content type header as json
		return parent::beforeAction($action);
	}

	protected function afterAction($action) {
		parent::afterAction($action);
		exit(CJSON::encode($this->_jsonData)); // exit with rendering json data
	}

}