<?php

class SlackHelper extends CComponent {


	public static function getCurrentSlackTeam() {
		//TODO: in the future this should somehow handle multidomain and multiteam
		return SlackTeam::model()->find();
	}


	public static function getSignInButton() {
		//NOTE: see at https://api.slack.com/docs/sign-in-with-slack#generator
		$title = 'Sign in with Slack';
		$imgUrl = 'https://platform.slack-edge.com/img/sign_in_with_slack.png';
		$srcSet = 'https://platform.slack-edge.com/img/sign_in_with_slack.png 1x, https://platform.slack-edge.com/img/sign_in_with_slack@2x.png 2x';
		$redirectUrl = Docebo::createAbsoluteAdminUrl('SlackApp/slackApp/signin');
		$url = 'https://slack.com/oauth/authorize?scope=identity.basic,identity.email,identity.team,identity.avatar&client_id='.self::getClientId().'&redirect_uri='.urlencode($redirectUrl);
		return '<a href="'.$url.'"><img alt="'.$title.'" height="40" width="172" src="'.$imgUrl.'" srcset="'.$srcSet.'" /></a>';
	}



	public static function getClientId() {
		return Settings::get('slack_client_id', '18756162992.53193994325');
	}


	public static function getClientSecret() {
		return Settings::get('slack_client_secret'/*, 'a59117563224fb6f54d5b092ec987922'*/); //NOTE: the secret can be changed manually in the slack app settings
	}


	public static function getDoceboBotName() {
		return 'docebobot';
	}

}