<?php

class SlackJobHandler extends JobHandler implements IJobHandler
{


	/**
	 * Job name
	 * @var string
	 */
	const JOB_NAME = 'SlackJobHandler';


	/**
	 * THIS handler identifier
	 * @var string
	 */
	const HANDLER_ID = 'SlackApp.components.SlackJobHandler';



	const TYPE_SYNCH_USERS = 'synch_users';
	const TYPE_AUTO_SYNCH  = 'slack_auto_sync';

	/**
	 * existing jobs older than the maximum allowed will be deleted when reloading the Slack settings page
	 * @var int maximum lifetime in seconds
	 */
	const JOB_MAX_LIFETIME = 21600; // 6 hours

	/**
	 * @see IJobHandler::init()
	 */
	public function init($params = false) {
		// do some initialization stuff here, specific for this job handler
	}



	/**
	 * @see IJobHandler::id()
	 */
	public static function id($params = false) {
		return self::HANDLER_ID;
	}


	/**
	 * @see IJobHandler::run()
	 */
	public function run($params = false) {
		// Already loaded at upper level

		/* @var $job CoreJob */
		$job = $this->job;
		if (!$job) {
			Yii::log('Invalid job: ' . __METHOD__, CLogger::LEVEL_ERROR);
			return;
		}

		try {

			$jobParams = CJSON::decode($job->params);
			$jobType = $jobParams['job_type'];
			$teamId = $jobParams['team_id'];

			switch ($jobType) {
				case self::TYPE_AUTO_SYNCH:
					Yii::log('Auto sync job starting....', CLogger::LEVEL_INFO);
				case self::TYPE_SYNCH_USERS:
					$this->synchronizeUsers($params);
					break;
				default:
					Yii::log('Invalid job type: '.$jobType, CLogger::LEVEL_ERROR);
					break;
			}

		} catch(Exception $e) {
			Yii::log('Slack job, an error occurred: '.$e->getMessage());
		}

	}




	protected function synchronizeUsers($params) {

		$timeBegin = microtime(true);

		$job = $this->job;
		$jobParams = CJSON::decode($job->params);

		try {

			/* @var $teamModel SlackTeam */
			if (isset($jobParams['team_id']) && !empty($jobParams['team_id'])) {
				$teamModel = SlackTeam::model()->findByPk($jobParams['team_id']);
			}
			if (empty($teamModel)) {
				Yii::log(__METHOD__.' EXCEPTION: Invalid team (passed ID: '.$jobParams['team_id'].')', CLogger::LEVEL_ERROR);
				throw new CException('Invalid team');
			}

			//prepare log record
			$logData = array(
				'status' => 'running',
				'synchronized_users' => 0,
				'total_users' => '-',
				'created' => '-',
				'updated' => '-',
				'errors' => '-',
				'messages' => array(),
				'time_begin' => $timeBegin,
				'time_end' => '-',
				'job_hash_id' => $job->hash_id
			);

			//initializing log for this job
			$log = new SlackLog();
			$log->team_id = $teamModel->id;
			$log->type = 'synch_users';
			$log->data = CJSON::encode($logData);
			$log->datetime = Yii::app()->localtime->toLocalDateTime();
			$log->job_id = $job->id_job;
			$rs = $log->save();
			if (!$rs) {
				Yii::log("Error while creating log:\n".print_r($log->getErrors(), true));
				throw new CException('Error while creating log');
			}

			$apiClient = SlackApiClient::getInstance();

			//read users list from slack
			$apiResponse = $apiClient->call('users.list', array(
				'presence' => 0 //we don't need to know presence info now
			));
			//check response
			if (empty($apiResponse)) {
				throw new CException('Error while retrieving slack users');
			}
			if (!$apiResponse['ok']) {
				throw new CException('Error while retrieving slack users: '.$apiResponse['error']['msg']);
			}

			//start processing users

			$slackUsers = $apiResponse['members'];

			$counter = 0; // keeps track of the currently handled users
			$total = 0;   // total of created, updated and erroneous users
			$created = 0; // counter for created users
			$updated = 0; // counter for updated users
			$errors = 0;  // counter for users having an error
			$messages = array(); // will contain the error logs for the view

			if (is_array($slackUsers)) {
				//retrieve preliminary data
				$userLevelGroup = CoreGroup::model()->findByAttributes(array('groupid' => Yii::app()->user->level_user)); //we need this to retrieve the users group ID

				//first loop just to count total non-bot users
				foreach ($slackUsers as $slackUser) {
					$isBot = ($slackUser['is_bot'] || $slackUser['name'] == 'slackbot' || !isset($slackUser['profile']['email']));
					if (!$isBot) { $total++; }
				}

				//updating operation status in the log
				$logData['total_users'] = $total;
				$log->data = CJSON::encode($logData);
				$log->save();
				//NOTE: some log data has already been retrieved and stored before

				//find users already synchronized in the LMS for the current team
				$existingUsers = self::getExistingUsers($teamModel->id);

				//find out if the Slack users already belong to the Slack branch
				$currentUserBranches = self::getCurrentSlackUsersBranchIds($teamModel);

				// START SYNCHING
				foreach ($slackUsers as $slackUser) {
					$isBot = ($slackUser['is_bot'] || $slackUser['name'] == 'slackbot' || !isset($slackUser['profile']['email']));
					$saveSlackModel = true;
					if (!$isBot) { // continue only if the user isn't a bot
						$slackUserExists = array_key_exists($slackUser['id'], $existingUsers);

						if (!$slackUserExists) { // Slack user doesn't exist yet, let's add him
							$userModel = CoreUser::model()->findByAttributes(array('email' => $slackUser['profile']['email']));
							if(empty($userModel)){ // E-mail address isn't used by any existing LMS user --> create new one
								$userModel = new CoreUser('create');
								$userModel->unsetAttributes();
								$userModel->userid = $slackUser['name'];
								$userModel->firstname = $slackUser['profile']['first_name'];
								$userModel->lastname = $slackUser['profile']['last_name'];
								$userModel->email = $slackUser['profile']['email'];
								$userModel->valid = 1;
								$userModel->force_change = 0;
								$generatedPassword = chr(mt_rand(65, 90))
									. chr(mt_rand(97, 122))
									. mt_rand(0, 9)
									. chr(mt_rand(97, 122))
									. chr(mt_rand(97, 122))
									. chr(mt_rand(97, 122))
									. mt_rand(0, 9)
									. chr(mt_rand(97, 122));
								$userModel->new_password = $generatedPassword;
								$userModel->new_password_repeat = $generatedPassword;

								if (!empty($teamModel->branch_id)) {
									$userModel->chartGroups[] = $teamModel->branch_id;
								}

								if ($userModel->save()) {
									$userModel->resetUserLanguage(); // Set the user's language to the default one
									$userModel->saveChartGroups();
									//assign user level (default: "user")
									if (!empty($userLevelGroup)) {
										$levelAssign = new CoreGroupMembers();
										$levelAssign->idst = $userLevelGroup->idst;
										$levelAssign->idstMember = $userModel->idst;
										$levelAssign->save();
									}

									//raise user creation event
									Yii::app()->event->raise('NewUserCreated', new DEvent($this, array('user' => $userModel)));
									$created++;
								} else {
									$errors = $userModel->getErrors();
									$messages[] = Yii::t('notification', 'Error while saving user {USERNAME}:', array('{USERNAME}' => $slackUser['profile']['email']))." ".$errors['userid'][0];
									$errors++;
									$saveSlackModel = false;
								}
							} else { // E-mail address already exists --> register the Slack user and add existing user to Slack branch
								if (!empty($teamModel->branch_id)) {
									$userModel->chartGroups = self::getCurrentLmsUserBranchIds($userModel->idst);
									$userModel->chartGroups[] = $teamModel->branch_id;
								}

								if ($userModel->saveChartGroups()) {
									$created++;
								} else {
									$messages[] = Yii::t('notification', 'Could not assign user {USERNAME} to the Slack user branch.', array('{USERNAME}' => $slackUser['profile']['email']));
									$errors++;
									$saveSlackModel = false;
								}
							}

							if($saveSlackModel){ // Only create slack user model when there's no error while creating the core user
								$slackUserModel = new SlackUser();
								$slackUserModel->user_id = $userModel->idst;
								$slackUserModel->slack_id = $slackUser['id'];
								$slackUserModel->team_id = $teamModel->id;
								$slackUserModel->slack_name = $slackUser['name'];
								$slackUserModel->save();
							}
						} else { // Slack user already exists
							$updates = array();

							// Setting up some otherwise very long variables
							$newFirstName = $slackUser['profile']['first_name'];
							$newLastName = $slackUser['profile']['last_name'];
							$newEmail = $slackUser['profile']['email'];
							$oldFirstName = $existingUsers[$slackUser['id']]['firstname'];
							$oldLastName = $existingUsers[$slackUser['id']]['lastname'];
							$oldEmail = $existingUsers[$slackUser['id']]['email'];
							$userId = $existingUsers[$slackUser['id']]['user_id'];

							// Checking if anything needs to be updated
							if(!empty($newFirstName) && ($newFirstName != $oldFirstName)){
								$updates['firstname'] = $newFirstName;
							}
							if(!empty($newLastName) && ($newLastName != $oldLastName)){
								$updates['lastname'] = $newLastName;
							}
							if(!empty($newEmail) && ($newEmail != $oldEmail)){
								$updates['email'] = $newEmail;
							}
							if(!empty($updates)){ // Update if necessary
								Yii::app()->db->createCommand()
									->update(CoreUser::model()->tableName(), $updates, 'idst = :userId', array(':userId' => $existingUsers[$slackUser['id']]['user_id']));
							}

							// Check if user branches need to be updated
							if(!array_key_exists($userId, $currentUserBranches) || !in_array($teamModel->branch_id, $currentUserBranches[$userId])){ // Slack user isn't yet assigned to current Slack branch --> add him
								$currentUserBranches[$userId][] = $teamModel->branch_id;
								$userModel = CoreUser::model()->findByPk($existingUsers[$slackUser['id']]['user_id']);
								$userModel->chartGroups = $currentUserBranches[$userId];
								$userModel->saveChartGroups();
							}
							$updated++;
						}
						$counter++;
					}
					//update job status
					$jobParams['synchronized_users'] = $counter;
					$jobParams['total_users'] = $total;
					$job->params = CJSON::encode($jobParams);
					$job->save();

					//update log status
					$logData['synchronized_users'] = $counter;
					$log->data = CJSON::encode($logData);
					$log->save();

				}
			}

			$timeEnd = microtime(true);

			//put job results into log record
			$logData['status'] = 'done';
			$logData['time_end'] = $timeEnd;
			$logData['errors'] = $errors;
			$logData['created'] = $created;
			$logData['updated'] = $updated;
			$logData['messages'] = $messages;
			$log->data = CJSON::encode($logData);
			$log->save();

		} catch (Exception $e) {

			//TODO: do some other stuff here, like rollbacking transactions or similar things
			Yii::log(__METHOD__." EXCEPTION: ".$e->getMessage(), CLogger::LEVEL_ERROR);
			throw $e; //this will be managed by the job executor
		}

	}

	/**
	 * Returns an array containing the necessary info to compare the current state with the new values with
	 *
	 * @param int $teamModelId
	 * @return array
	 */
	static public function getExistingUsers($teamModelId){
		$existingUsers = array();
		$existingUsersTmp = Yii::app()->db->createCommand("
			SELECT su.slack_id, su.slack_name, su.user_id, cu.firstname, cu.lastname, cu.email
			FROM ".CoreUser::model()->tableName()." AS cu
			INNER JOIN ".SlackUser::model()->tableName()." AS su ON cu.idst = su.user_id
			WHERE su.team_id = ".$teamModelId."
		")->queryAll();
		foreach($existingUsersTmp as $existingUser){
			$existingUsers[$existingUser['slack_id']] = array(
				'user_id' => $existingUser['user_id'],
				'slack_name' => $existingUser['slack_name'],
				'firstname' => $existingUser['firstname'],
				'lastname' => $existingUser['lastname'],
				'email' => $existingUser['email']
			);
		}
		return $existingUsers;
	}

	/**
	 * Returns an array of the current Slack users and the branches they belong to.
	 *
	 * @param object $teamModel: SlackTeam object
	 * @return array
	 */
	static public function getCurrentSlackUsersBranchIds($teamModel){
		$slackUserBranchIds = array();
		$slackUserBranchIdsTmp = Yii::app()->db->createCommand("
			SELECT su.user_id, cot.idOrg
			FROM ".SlackUser::model()->tableName()." AS su
			INNER JOIN ".CoreGroupMembers::model()->tableName()." AS cgm ON su.user_id = cgm.idstMember
			INNER JOIN ".CoreOrgChartTree::model()->tableName()." AS cot ON cgm.idst = cot.idst_oc
			WHERE su.team_id = ".$teamModel->id." AND cot.idOrg > 0
		")->queryAll();
		foreach($slackUserBranchIdsTmp as $slackUserBranch){
			$slackUserBranchIds[$slackUserBranch['user_id']][] = $slackUserBranch['idOrg'];
		}
		return $slackUserBranchIds;
	}

	/**
	 * Returns an array with branch Id's the current LMS (but not Slack) user is assigned to
	 *
	 * @param int $lmsUserId
	 * @return array
	 */
	static public function getCurrentLmsUserBranchIds($lmsUserId){
		$lmsUserBranchIds = array();
		$lmsUserBranchIdsTmp = Yii::app()->db->createCommand("
			SELECT cot.idOrg
			FROM ".CoreGroupMembers::model()->tableName()." as cgm
			INNER JOIN ".CoreOrgChartTree::model()->tableName()." AS cot ON cgm.idst = cot.idst_oc
			WHERE cgm.idstMember = '".$lmsUserId."' AND cot.idOrg > 0
		")->queryAll();
		foreach($lmsUserBranchIdsTmp as $lmsUserBranchId){
			$lmsUserBranchIds[] = $lmsUserBranchId['idOrg'];
		}
		return $lmsUserBranchIds;
	}
}