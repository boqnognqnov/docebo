<div class="row-fluid">

	<div class="span12">
		<div class="control-group channel-selector-box">
			<label><?= Yii::t('slack', 'Choose whether to send the notification to the individual or to a Slack channel') ?></label>
			<!--<div class="span12">-->
				<?php

				$_inputName = 'slack_transport_additional_info[recipient_type]';

				echo '<label for="slack_recipient_type_user">';
				echo CHtml::radioButton($_inputName, $recipient_type == 'user', array('id' => 'slack_recipient_type_user', 'value' => 'user'));
				echo '&nbsp;';
				echo Yii::t('slack', 'Direct notification');
				echo '</label>';

				echo '<label for="slack_recipient_type_channel">';
				echo CHtml::radioButton($_inputName, $recipient_type == 'channel', array('id' => 'slack_recipient_type_channel', 'value' => 'channel'));
				echo '&nbsp;';
				echo Yii::t('slack', 'Specific channel');
				echo '</label>';

				echo CHtml::dropDownList('slack_transport_additional_info[recipient_channel]', $recipient_channel, $channels, array(
					'id' => 'slack_recipient_channel_selector',
					'class' => 'channel-selector span12'
				));

				?>
			<!--</div>-->
		</div>
	</div>

</div>
<script type="text/javascript">
	$(function() {

		var checkSlackRecipientOptions = function() {
			var dd = $('#slack_recipient_channel_selector');
			if ($('#slack_recipient_type_user').prop('checked')) {
				dd.prop('disabled', true);
				dd.addClass('disabled');
			}
			if ($('#slack_recipient_type_channel').prop('checked')) {
				dd.prop('disabled', false);
				dd.removeClass('disabled');
			}
		};

		checkSlackRecipientOptions(); //make always a first call
		$('#slack_recipient_type_user,#slack_recipient_type_channel').on('change', checkSlackRecipientOptions);
	});
</script>