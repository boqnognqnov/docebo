<?php
/* @var $team SlackTeam */

$_breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('slack', 'Slack')
);

$this->breadcrumbs = $_breadcrumbs;

$_rowCounter = 1;
?>

<?php
if ($team->isAuthorized()) {
	echo CHtml::beginForm();
} else {
	echo CHtml::beginForm(Docebo::createAdminUrl('SlackApp/slackApp/authorize'));
}
?>

<div class="row-fluid">
	<div class="span6">
		<h3 class="advanced-slack-settings-form-title">
			<?= Yii::t('slack', 'Slack') ?>
		</h3>
	</div>
	<?php if ($team->isAuthorized()): ?>
		<div class="span6" style="text-align: right">
			<?= CHtml::submitButton(Yii::t('slack', 'Reset configuration'), array(
				'class' => 'btn btn-docebo red big',
				'id' => 'slack-reset-config',
				'name' => 'reset_config'
			)) ?>
		</div>
	<?php endif; ?>
</div>
<br>

<?php DoceboUI::printFlashMessages(); ?>

<div class="advanced-main slack-settings">

	<div class="section">

		<div class="row <?php echo ($_rowCounter % 2 > 0 ? 'odd' : 'even') ?>">
			<div class="row">

				<div class="subsetting">
					<div class="setting-name">
						<?= Yii::t('slack', 'Slack domain') ?>
					</div>
					<div class="values">
						<div>
							<?php if (!$team->isAuthorized()): ?>
							<?= CHtml::textField('slack_domain', $team->domain) ?>&nbsp;.slack.com
							<?php else: ?>
							<?= CHtml::textField(false, $team->domain, array('disabled' => true, 'class' => 'disabled')) ?>&nbsp;.slack.com
							<?php endif; ?>
						</div>
					</div>
				</div>

				<div class="subsetting">
					<div class="setting-name">
						<?= Yii::t('slack', 'Client ID') ?>
					</div>
					<div class="values">
						<div>
							<?php if (!$team->isAuthorized()): ?>
							<?= CHtml::textField('slack_client_id', $team->client_id); ?>
							<?php else: ?>
							<?= CHtml::textField(false, $team->client_id, array('disabled' => true, 'class' => 'disabled')) ?>
							<?php endif; ?>
						</div>
					</div>
				</div>

				<div class="subsetting">
					<div class="setting-name">
						<?= Yii::t('slack', 'Client secret') ?>
					</div>
					<div class="values">
						<div>
							<?= CHtml::textField('slack_client_secret', $team->client_secret); ?>
						</div>
					</div>
				</div>

				<!--<div class="subsetting">
					<div class="setting-name">
						<?= Yii::t('slack', 'Verification token') ?>
					</div>
					<div class="values">
						<div>
							<?= CHtml::textField('slack_verification_token', $team->verification_token); ?>
						</div>
					</div>
				</div>-->

				<!--<div>
					<div class="setting-name last">
						&nbsp;
					</div>
					<div class="values pull-right">
						<div>
							<?php
							if (!empty($team)) {
								if ($team->isNewRecord || empty($team->token)) {
									//application has not been authorized yet
									$_authorizeUrl = 'https://api.slack.com/oauth/authorize'; //$slackUrl = 'https://slack.com/api/oauth.access';
									$_authorizeUrl .= '?client_id='.$team->client_id;
									$_authorizeUrl .= '&scope='.urlencode('incoming-webhook commands bot users:read groups:read usergroups:read channels:read channels:write chat:write:bot emoji:read ');
									$_authorizeUrl .= '&redirect_uri='.urlencode(Docebo::createAbsoluteUrl('SlackApp/slackApp/settings')); //'https://sf2.docebosandbox.com/oauth2/authorize'
									$_authorizeUrl .= '&state='.md5('test');
									echo CHtml::link(Yii::t('login', 'Authorize'), $_authorizeUrl, array('id' => 'slack_authorize_link', 'class' => 'btn btn-docebo green big'));
								} else {
									echo CHtml::link(Yii::t('login', 'Authorize'), 'javascript:;', array('class' => 'btn btn-docebo green big disabled'));
								}
							}
							?>
						</div>
					</div>
				</div>-->

			</div>
		</div>



		<?php if ($team->isAuthorized()): ?>

		<?php $_rowCounter++;  ?>

		<div class="row <?php echo ($_rowCounter % 2 > 0 ? 'odd' : 'even') ?>">
			<div class="row">
				<div class="setting-name">
					<?= Yii::t('slack', 'Verification token') ?>
				</div>
				<div class="values">
					<div>
						<?= CHtml::textField('slack_verification_token', $team->verification_token); ?>
					</div>
				</div>
			</div>
		</div>

		<?php $_rowCounter++;  ?>

		<div class="row <?php echo ($_rowCounter % 2 > 0 ? 'odd' : 'even') ?>">
			<div class="row">
				<div class="setting-name">
					<?= Yii::t('slack', 'Sign in with slack') ?>
				</div>
				<div class="values">
					<div>
						<?= CHtml::checkBox('slack_enable_signin', $team->enable_signin, array()) ?>
					</div>
				</div>
			</div>
		</div>


		<?php $_rowCounter++;  ?>

		<div class="row <?php echo ($_rowCounter % 2 > 0 ? 'odd' : 'even').(empty($team->token) ? ' unavailable' : '') ?>">
			<div class="row">
				<div class="setting-name">
					<?= Yii::t('slack', 'Branch/group') ?>
				</div>
				<div class="values">
					<div>
						<span id="branch-path">
						<?php
						if (empty($team->branch_id)) {
							echo CoreOrgChartTree::getTranslatedName(CoreOrgChartTree::getOrgRootNode()->idOrg); //default: root node
						} else {
							echo $this->_getBranchPath($team->branch_id);
						}
						?>
						</span>
						<?= CHtml::hiddenField('slack_branch_id', ($team->branch_id ? $team->branch_id : CoreOrgChartTree::getOrgRootNode()->idOrg), array('id' => 'slack_branch_id')); ?>
						<a
							data-dialog-class="org-chart-popup-content<?= empty($team->token) ? ' disabled' : '' ?>"
							class="sf-button open-dialog gray"
							id="select-org-chart"
							href="<?php
								if (!empty($team->token)) {
									echo Docebo::createAdminUrl('SlackApp/slackApp/axSelectOrganizationChartNode', array('node' => (!empty($team->branch_id) ? $team->branch_id : CoreOrgChartTree::getOrgRootNode()->idOrg)));
								} else {
									echo 'javascript:;';
								}
							?>"
							>
							<?=Yii::t('standard', '_SELECT');?>
						</a>
					</div>
				</div>
			</div>
		</div>


		<?php $_rowCounter++;  ?>

		<div class="row <?php echo ($_rowCounter % 2 > 0 ? 'odd' : 'even').(empty($team->token) ? ' unavailable' : '') ?>">
			<div class="row">
				<div class="setting-name">
					<?= Yii::t('slack', 'Synchronization frequency') ?>
				</div>
				<div class="values">
					<div>
						<?php
						echo CHtml::dropDownList('slack_synch_type', $team->synch_type, SlackTeam::getSynchTypeOptions(), array('disabled' => (empty($team->token))));
						echo CHtml::button(Yii::t('slack', 'Synch'), array(
							'class' => 'btn btn-docebo green big pull-right',
							'id' => 'synch_users_action',
							'disabled' => empty($team->token)
						));
						?>
					</div>
					<div id="synch-users-result-message" style="margin-top: 20px;">

					</div>
				</div>
			</div>
		</div>


		<?php endif; ?>


		<?php $_rowCounter++;  ?>

		<?php if (!$team->isAuthorized()): ?>

		<div class="row <?php echo ($row % 2 > 0 ? 'odd' : 'even') ?>">
			<div class="row">
				<div class="row-fluid right" style="text-align: right">
					<?php
					//application has not been authorized yet
					/*
					$_authorizeUrl = 'https://api.slack.com/oauth/authorize'; //$slackUrl = 'https://slack.com/api/oauth.access';
					$_authorizeUrl .= '?client_id='.$team->client_id;
					$_authorizeUrl .= '&scope='.urlencode('incoming-webhook commands bot users:read groups:read usergroups:read channels:read channels:write chat:write:bot emoji:read ');
					$_authorizeUrl .= '&redirect_uri='.urlencode(Docebo::createAbsoluteUrl('SlackApp/slackApp/settings')); //'https://sf2.docebosandbox.com/oauth2/authorize'
					$_authorizeUrl .= '&state='.md5('test');
					echo CHtml::link(Yii::t('login', 'Authorize'), $_authorizeUrl, array('id' => 'slack_authorize_link', 'class' => 'btn btn-docebo green big'));
					*/
					echo CHtml::submitButton(Yii::t('login', 'Authorize'), array('id' => 'slack_authorize_button', 'class' => 'btn btn-docebo green big'));
					?>
				</div>
			</div>
		</div>

		<?php else: ?>

		<div class="row <?php echo ($row % 2 > 0 ? 'odd' : 'even') ?>">
			<div class="row">
				<div class="row-fluid right" style="text-align: right">
					<?= CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
						'class' => 'btn btn-docebo green big',
						'name' => 'submit'
					)); ?>
					<!--<?= CHtml::submitButton(Yii::t('standard', '_CANCEL'), array(
						'class' => 'btn btn-docebo black big',
						'name' => 'cancel'
					)); ?>-->
				</div>
			</div>
		</div>

		<?php endif; ?>

	</div>
</div>

<?= CHtml::endForm() ?>

<script type="text/javascript">
	$(function () {

		$('.slack-settings').find('input:checkbox').styler();

		$('#synch_users_action').on('click', function() {

			var job_id = false;

			var poll = function() {
				if (!job_id) return;
				$.ajax({
					url: '<?= Docebo::createAdminUrl('SlackApp/slackApp/axSynchUsersStatus') ?>',
					type: 'POST',
					dataType: 'json',
					data: { job_id: job_id }
				}).done(function(o) {
					var r = $('#synch-users-result-message');
					switch (o.status) {
						case 'error':
							var m = $('<span style="color: red; font-weight: bold;"></span>');
							m.text(o.message);
							r.html('');
							r.append(m);
							break;
						case 'running':
							setTimeout(poll, 500);
							break;
						case 'done':
							var m = $('<span style="color: green; font-weight: bold;"></span>');
							m.text(o.message);
							r.html('');
							r.append(m);
							break;
					}
				}).fail(function(){

				});
			}

			$.ajax({
				url: '<?= Docebo::createAdminUrl('SlackApp/slackApp/axSynchUsers') ?>',
				type: 'POST',
				dataType: 'json',
				beforeSend: function() {
					var r = $('#synch-users-result-message');
					r.html('<i class="fa fa-spi fa-spinner"></i>');
				}
			}).done(function(o) {
				var r = $('#synch-users-result-message');
				if (o.success) {
					//start polling for job status
					job_id = o.job_id;
					poll();
				} else {
					//just display an error message
					var m = $('<span style="color: red; font-weight: bold;"></span>');
					m.text(o.message);
					r.html('');
					r.append(m);
				}
			}).fail(function() {
				var r = $('#synch-users-result-message');
				var m = $('<span style="color: red; font-weight: bold;"></span>');
				m.text(o.message);
				r.html('');
				r.append(m);
			}).always(function() {
			});

		});

	});

	function updateBranchPath() {
		$.ajax({
			url: '<?= Docebo::createAdminUrl('SlackApp/slackApp/axGetBranchPath') ?>',
			type: 'POST',
			dataType: 'json',
			data: {
				branch_id: $('#slack_branch_id').val()
			},
			beforeSend: function() {
				$('#branch-path').html('<i class="fa fa-spin fa-spinner"></i>');
			}
		}).done(function(o) {
			var content = o.path;
			if (content === false) { content = <?= CoreOrgChartTree::getTranslatedName(CoreOrgChartTree::getOrgRootNode()->idOrg) ?> }
			$('#branch-path').html(content);
		}).fail(function() {
			$('#branch-path').html(<?= CJSON::encode(Yii::t('standard', '_OPERATION_FAILURE')) ?>);
		}).always(function() {
			$('#branch-path').find('.fa-spin').remove();
		})
	}
</script>