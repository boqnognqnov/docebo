<h1><?=Yii::t('slack', 'Reset Slack Configuration')?></h1>
<?php
$this->widget('common.widgets.warningStrip.WarningStrip', array(
	'message'	=> Yii::t('slack', 'You are about to delete your current Slack account inside Docebo. By doing so, you will reset your configuration. Are you sure you want to proceed?'),
	'type'		=>'warning',
));
?>
<?php echo CHtml::beginForm('', 'POST', array(
	'class'	=> 'ajax',
	'id'	=> 'slack-clear-config-form'
)); ?>
<div class="row-fluid">
	<div class="span1">
		<?php
		echo CHtml::checkBox('confirm', false, array(
			'class' => 'confirm_checkbox',
			'value'	=> 'on'
		));
		?>
	</div>
	<div class="span11">
		<label id="confirm_label" for="confirm"><?= Yii::t('slack', 'Yes, I want to proceed!')?></label>
	</div>
</div>



<div class="form-actions">
	<input class="btn-docebo hidden green big yes" type="submit" name="confirm_yes"
		   value="<?php echo Yii::t('standard', '_YES'); ?>" /> <input
		class="btn-docebo red big close-dialog" type="reset"
		value="<?php echo Yii::t('standard', '_NO'); ?>" />
</div>
<?php CHtml::endForm(); ?>

<script type="text/javascript">
	$(function() {
		$(".confirm_checkbox").on('change', function() {
			if ( $(".confirm_checkbox").hasClass("checked") ) {
				$(".btn-docebo.green.yes").removeClass("hidden");
			} else {
				$(".btn-docebo.green.yes").addClass("hidden");
			}
		});

		$(".confirm_checkbox").styler();
		$("#confirm_label").styler();
	});
</script>
<style type="text/css">
	#slack-clear-config-form {
		margin-top: 30px;
	}
	.warning-text {
		float: none !important;
		display: block !important;
	}
	.warning-strip {
		min-height: 70px;
	}
</style>