<h1><?=Yii::t('salesforce', 'Synchronization logs')?></h1>
<form class="form-horizontal">
	<div class="salesforce-logs">
		<div class="filters-wrapper">
			<div class="row-fluid filters">
				<div class="span12">
					<div class="row-fluid">
						<p><?=Yii::t('salesforce', 'Last synchronization');?>: <strong><?=Yii::app()->localtime->toLocalDateTime($logModel['datetime'])?></strong></p>
					</div>
					<div class="row-fluid">
						<p><?=Yii::t('salesforce', 'Synchronized users')?>: <strong><?=(intval($params['created']) + intval($params['updated']))?></strong></p>
					</div>
				</div>
			</div>
		</div>
		<div class="form-wrapper">
			<?php
			$this->beginWidget('DoceboCGridView', array(
				'id' => 'notification-logs-grid',
				'htmlOptions' => array(
					'class' => 'grid-view clearfix'
				),
				'dataProvider' => $dataProvider,
				'summaryText' => Yii::t('standard', '_TOTAL'),
				'template' => '{items}{pager}',
				'pager' => array(
					'class' => 'DoceboCLinkPager'
				),
				'ajaxUpdate' => true,
				'columns' => array(
					array(
						'header' => Yii::t('standard', '_DESCRIPTION'),
						'value' => '$data'
					),
				)
			));
			$this->endWidget();
			?>
		</div>
		<div class="form-actions">
			<?=CHtml::button(Yii::t('standard', '_CLOSE'), array(
				'class' => 'btn btn-docebo black big close-dialog pull-right'
			))?>
		</div>
	</div>
</form>