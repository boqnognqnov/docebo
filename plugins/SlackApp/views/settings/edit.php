<?php

// NOTE: this setting is temporary while the app is in developing status
$useVerificationToken = false;

/* @var $team SlackTeam */

$_breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('slack', 'Slack')
);

$this->breadcrumbs = $_breadcrumbs;

$_rowCounter = 1;

if (Settings::get('slack_direct_install', 'off') == 'on') {
	$_buttonUrl = 'https://slack.com/oauth/authorize?'
		.'scope=incoming-webhook,commands,bot'
		.'&client_id='.SlackHelper::getClientId()
		.'&redirect_uri='.urlencode(Docebo::createAbsoluteAdminUrl('SlackApp/settings/authorize'))
		.'&state='.$this->generateStateAuthorizeParameter();
} else {
	$_installerUrl = Yii::app()->params['installer_url'];
	$_authId = sha1(Docebo::getErpInstallationId() . Settings::getCfg('erp_secret_key', ''));
	$_buttonUrl = $_installerUrl . (substr($_installerUrl, -1, 1) != '/' ? '/' : '') . 'slack/authenticate?id=' . $_authId;
}

?>

<?php
if ($team->isAuthorized()) {
	echo CHtml::beginForm(Docebo::createAdminUrl('SlackApp/settings/edit'));
} else {
	echo CHtml::beginForm(Docebo::createAdminUrl('SlackApp/settings/authorize'));
}
?>

<div class="row-fluid">
	<div class="span6">
		<h3 class="advanced-slack-settings-form-title">
			<?= Yii::t('slack', 'Slack') ?>
		</h3>
	</div>
	<?php if ($team->isAuthorized()): ?>
		<div class="span6" style="text-align: right">
			<a href="<?=Docebo::createAdminUrl('SlackApp/settings/resetConfiguration')?>"
				class="open-dialog btn btn-docebo red big"
				id="slack-reset-config"><?=Yii::t('slack', 'Reset configuration')?></a>
		</div>
	<?php endif; ?>
</div>
<br>

<?php
DoceboUI::printFlashMessages();
//TODO: print other messages here if needed
?>

<div class="advanced-main slack-settings">

	<div class="section">

		<div class="row <?php echo ($_rowCounter % 2 > 0 ? 'odd' : 'even') ?>">
			<div class="row">
				<div class="setting-name">
					<?= Yii::t('slack', 'Slack integration') ?>
					<p class="setting-description"><?= Yii::t('slack', 'Send notifications into slack') ?></p>
				</div>
				<div class="values">
					<div>
						<?php if ($team->isAuthorized()): ?>
						<span class="pull-right already-configured">
							<img
								alt="<?= Yii::t('slack', 'Add to Slack') ?>"
								height="40"
								width="139"
								src="https://platform.slack-edge.com/img/add_to_slack.png"
								srcset="https://platform.slack-edge.com/img/add_to_slack.png 1x, https://platform.slack-edge.com/img/add_to_slack@2x.png 2x" />
							<br/>
							<span class="already-configured-text">
								<?= Yii::t('slack', 'Configuration done') ?>!
							</span>
						</span>
						<?php else: ?>
						<a href="<?= $_buttonUrl ?>" class="pull-right">
							<img
								alt="<?= Yii::t('slack', 'Add to Slack') ?>"
								height="40"
								width="139"
								src="https://platform.slack-edge.com/img/add_to_slack.png"
								srcset="https://platform.slack-edge.com/img/add_to_slack.png 1x, https://platform.slack-edge.com/img/add_to_slack@2x.png 2x" />
						</a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>

		<?php if ($useVerificationToken): ?>
		<?php $_rowCounter++;  ?>

		<div class="row <?php echo ($_rowCounter % 2 > 0 ? 'odd' : 'even').(!$team->isAuthorized() ? ' unavailable' : '') ?>">
			<div class="row">
				<div class="setting-name">
					<?= Yii::t('slack', 'Verification token') ?>
				</div>
				<div class="values">
					<div>
						<?= CHtml::textField('verification_token', $team->verification_token, array(
							'disabled' => !$team->isAuthorized(),
							'class' => 'long'.(!$team->isAuthorized() ? ' disabled' : '')
						)); ?>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>


		<?php /*$_rowCounter++;  */?><!--

		<div class="row <?php /*echo ($_rowCounter % 2 > 0 ? 'odd' : 'even').(!$team->isAuthorized() ? ' unavailable' : '') */?>">
			<div class="row">
				<div class="setting-name">
					<?/*= Yii::t('slack', 'Sign in with slack') */?>
				</div>
				<div class="values">
					<div>
						<?/*= CHtml::checkBox('enable_signin', $team->enable_signin, array(
							'disabled' => !$team->isAuthorized()
						)) */?>
						<span class="signin-info">
							<?/*= Yii::t('slack', 'Enable single sign-on with Slack') */?>
						</span>
					</div>
				</div>
			</div>
		</div>-->


		<?php $_rowCounter++;  ?>

		<div class="row <?php echo ($_rowCounter % 2 > 0 ? 'odd' : 'even').(!$team->isAuthorized() ? ' unavailable' : '') ?>">
			<div class="row">
				<div class="setting-name">
					<?= Yii::t('slack', 'Destination branch') ?>
				</div>
				<div class="values">
					<div>
						<table class="action-container">
							<tbody>
								<tr>
									<td>
										<?= Yii::t('slack', 'Before synchronizing the users you can select the destination branch') ?>
									</td>
									<td>
										<a
											data-dialog-class="org-chart-popup-content"
											class="btn btn-docebo green big pull-right <?= !$team->isAuthorized() ? 'disabled' : 'open-dialog' ?>"
											id="select-org-chart"
											href="<?php
											if ($team->isAuthorized()) {
												echo Docebo::createAdminUrl('SlackApp/settings/axSelectOrganizationChartNode', array('node' => (!empty($team->branch_id) ? $team->branch_id : CoreOrgChartTree::getOrgRootNode()->idOrg)));
											} else {
												echo 'javascript:;';
											}
											?>"
											>
											<?=Yii::t('standard', '_SELECT');?>
										</a>
									</td>
								</tr>
								<tr>
									<td>
										<span class="branch-current-selection-label">
											<?= Yii::t('slack', 'Current node') ?>:
										</span>
										<span class="root-branch-label">
											<?= CoreOrgChartTree::getTranslatedName(CoreOrgChartTree::getOrgRootNode()->idOrg) ?>
										</span>
										<span id="branch-path">
										<?php
										if (!empty($team->branch_id)) {
											echo $this->getBranchPath($team->branch_id);
										}
										?>
										</span>
									</td>
								</tr>
							</tbody>
						</table>
						<?= CHtml::hiddenField('branch_id', ($team->branch_id ? $team->branch_id : CoreOrgChartTree::getOrgRootNode()->idOrg), array('id' => 'slack_branch_id')); ?>
					</div>
				</div>
			</div>
		</div>


		<?php $_rowCounter++;  ?>

		<div class="row <?php echo ($_rowCounter % 2 > 0 ? 'odd' : 'even').(!$team->isAuthorized() ? ' unavailable' : '') ?>">
			<div class="row">
				<div class="setting-name">
					<?= Yii::t('slack', 'Synchronization frequency') ?>
				</div>
				<div class="values">
					<div>
						<table class="action-container">
							<tbody>
								<tr>
									<td>
										<span style="display:block; margin-bottom: 10px;"><?= Yii::t('slack', 'Launch the automatic Docebo {synch-symbol} Slack synchronization', array(
											'{synch-symbol}' => '<i class="icon-retweet"></i>'
										)) ?></span>
									</td>
								</tr>
								<tr>
									<td>
										<?= CHtml::dropDownList('synch_info', $team->synch_info, SlackTeam::getSynchOptions(), array(
											'disabled' => !$team->isAuthorized(),
											'class' => 'long'
										)) ?>
									</td>
									<td id="tdSyncButton">
										<?= CHtml::button(Yii::t('slack', 'Sync'), array(
											'class' => 'btn btn-docebo green big pull-right'.(!$team->isAuthorized() ? ' disabled' : ''),
											'id' => 'synch_users_action',
											'disabled' => !$team->isAuthorized()
										)) ?>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div id="synch-users-result-message">
						<?= $this->getLastSynchInfo() ?>
					</div>
				</div>
			</div>
		</div>


		<?php $_rowCounter++;  ?>


		<div class="row even<?= (!$team->isAuthorized() ? ' unavailable' : '') ?>">
			<div class="row">
				<div class="row-fluid right" style="text-align: right">
					<?php
					if ($team->isAuthorized()) {
						//show the true save button
						echo CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
							'class' => 'btn btn-docebo green big',
							'name' => 'save'
						));
					} else {
						//show a dummy, disabled version of the save button
						echo CHtml::button(Yii::t('standard', '_SAVE'), array(
							'class' => 'btn btn-docebo green big disabled',
							'disabled' => true,
							'name' => false
						));
					}
					?>
				</div>
			</div>
		</div>

	</div>
</div>

<?= CHtml::endForm() ?>

<script type="text/javascript">
	$(function () {

		//apply styles to other inputs
		$('.slack-settings').find('input:checkbox').styler();

		if ( $(".alert-success").length) {
			setInterval(function(){
				$(".alert-success").fadeOut(1500);
			}, 3000);
		}
		if ( $(".alert-error").length) {
			setInterval(function(){
				$(".alert-error").fadeOut(1500);
			}, 3000);
		}

		// Give dropdown list and sync button a 20px margin
		var syncButtonWidth = $('#synch_users_action').outerWidth() + 20;
		$('#tdSyncButton').css('width', syncButtonWidth+'px');

		function pollUsers(){
			var job_id = false;

			var poll = function() {
				if (!job_id) return;
				$.ajax({
					url: '<?= Docebo::createAdminUrl('SlackApp/settings/axCheckUsersSynchronizationStatus') ?>',
					type: 'POST',
					dataType: 'json',
					data: { job_id: job_id }
				}).done(function(o) {
					var r = $('#synch-users-result-message');
					var b = $('#synch_users_action');
					switch (o.status) {
						case 'error':
							b.val(<?= CJSON::encode(Yii::t('slack', 'Synch')) ?>);
							r.html(o.error);
							break;
						case 'running':
							setTimeout(poll, 500);
							b.val('...'+ o.percentage+'%');
							break;
						case 'done':
							b.val(<?= CJSON::encode(Yii::t('slack', 'Synch')) ?>);
							r.html(o.synch_info);
							$(document).controls();
							break;
					}
				}).fail(function(){
					var r = $('#synch-users-result-message');
					var b = $('#synch_users_action');
					b.val(<?= CJSON::encode(Yii::t('slack', 'Synch')) ?>);
					r.html(<?= CJSON::encode(Yii::t('standard', '_OPERATION_FAILURE')) ?>);
				});
			}

			$.ajax({
				url: '<?= Docebo::createAdminUrl('SlackApp/settings/axSynchronizeUsers') ?>',
				type: 'POST',
				dataType: 'json',
				beforeSend: function() {
					var r = $('#synch-users-result-message');
					r.html('<i class="fa fa-spi fa-spinner"></i>');
				}
			}).done(function(o) {
				var r = $('#synch-users-result-message');
				if (o.success) {
					//start polling for job status
					job_id = o.job_id;
					poll();
				} else {
					//just display an error message
					var r = $('#synch-users-result-message');
					var b = $('#synch_users_action');
					b.val(<?= CJSON::encode(Yii::t('slack', 'Synch')) ?>);
					r.html(o.error ? o.error : <?= CJSON::encode(Yii::t('standard', '_OPERATION_FAILURE')) ?>);
				}
			}).fail(function() {
				var r = $('#synch-users-result-message');
				var b = $('#synch_users_action');
				b.val(<?= CJSON::encode(Yii::t('slack', 'Synch')) ?>);
				r.html(<?= CJSON::encode(Yii::t('standard', '_OPERATION_FAILURE')) ?>);
			}).always(function() {
			});
		}

		//pressing synchronizing users button
		$('#synch_users_action').on('click', function() {
			pollUsers();
		});

		<?php if($jobExists){ ?>
		pollUsers();
		<?php } ?>

	});

	function updateBranchPath() {
		$.ajax({
			url: '<?= Docebo::createAdminUrl('SlackApp/settings/axGetBranchPath') ?>',
			type: 'POST',
			dataType: 'json',
			data: {
				branch_id: $('#slack_branch_id').val()
			},
			beforeSend: function() {
				$('#branch-path').html('<i class="fa fa-spin fa-spinner"></i>');
			}
		}).done(function(o) {
			var content = o.path;
			if (content === false) { content = "<?= CoreOrgChartTree::getTranslatedName(CoreOrgChartTree::getOrgRootNode()->idOrg) ?>" }
			$('#branch-path').html(content);

			// Disable the sync button after the branch has been updated until the form has been saved.
			$('#synch_users_action').prop('disabled', 'disabled').prop('title', '<?=Yii::t('slack', 'Please, save your form before synchronizing users')?>');
		}).fail(function() {
			$('#branch-path').html(<?= CJSON::encode(Yii::t('standard', '_OPERATION_FAILURE')) ?>);
		}).always(function() {
			$('#branch-path').find('.fa-spin').remove();
		})
	}
</script>