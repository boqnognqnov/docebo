<?php

class MultidomainAppModule extends CWebModule
{
	
	/**
	 * URL to this module's assets (web accessible)
	 *
	 * @var string
	 */
	protected $_assetsUrl = null;
	
	
	public function init()
	{

		Yii::app()->event->on('GetLogoPath', array(new MultidomainListeners(), 'onGetLogoPath'));
		Yii::app()->event->on('GetFaviconPath', array(new MultidomainListeners(), 'onGetFaviconPath'));
		Yii::app()->event->on('GetLoginImage', array(new MultidomainListeners(), 'onGetLoginImage'));
		Yii::app()->event->on('GetPlayerBgImage', array(new MultidomainListeners(), 'onGetPlayerBgImage'));
		Yii::app()->event->on('GetWhiteLabelSettings', array(new MultidomainListeners(), 'onGetWhiteLabelSettings'));
		Yii::app()->event->on('BeforeLoginPageRender', array(new MultidomainListeners(), 'onBeforeLoginPageRender'));
		Yii::app()->event->on('GetSigninTitle', array(new MultidomainListeners(), 'onGetSigninTitleOrCaption'));
		Yii::app()->event->on('GetSigninCaption', array(new MultidomainListeners(), 'onGetSigninTitleOrCaption'));
		Yii::app()->event->on('FilterSigninPages', array(new MultidomainListeners(), 'onFilterSigninPages'));
		Yii::app()->event->on('AfterUserSelfRegisterSave', array(new MultidomainListeners(), 'onAfterUserSelfRegisterSave'));

		// User Management hooks
		Yii::app()->event->on('RenderOrgChartTreeNodeMenu', array(new MultidomainListeners(), 'onRenderOrgChartTreeNodeMenu'));
		Yii::app()->event->on('BeforeCreateOrgchartTree', array(new MultidomainListeners(), 'onHighlightMultidomainNodes'));

		/*
		Yii::app()->event->on('RenderLoginPage', array(new MultidomainListeners(), 'onRenderLoginPage'));

		*/
		
		/* NEW */
		Yii::app()->event->on('BaseControllerInitStarted', array(new MultidomainListeners(), 'onBaseControllerInitStarted'));
		Yii::app()->event->on('ResolvingCustomCss', array(new MultidomainListeners(), 'onResolvingCustomCss'));
		Yii::app()->event->on('ResolvingCoursePlayerLayout', array(new MultidomainListeners(), 'onResolvingCoursePlayerLayout'));
		Yii::app()->event->on('ResolvingCoursePlayerHtmlLoCss', array(new MultidomainListeners(), 'onResolvingCoursePlayerHtmlLoCss'));
		
		/* Special */
		Yii::app()->event->on('GetCoreSetting', array(new MultidomainListeners(), 'onGetCoreSetting'));


		/* Courses Catalog App related */
		Yii::app()->event->on('RenderCoursesLabelTile', array(new MultidomainListeners(), 'onRenderCoursesLabelTile'));

		Yii::app()->event->on('GetMultidomainHeaderMessage', array($this, 'getMultidomainHeaderMessage'));

		$this->setImport(array(
			'MultidomainApp.components.*',
			'MultidomainApp.models.*',
		));
		
		$this->defaultController = 'main';
        if(Yii::app() instanceof CWebApplication){
            $this->getMultidomainLanguage();
        }
	}
	
	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu()
	{
		// Add menu voices !
		Yii::app()->mainmenu->addAppMenu('multidomain', array(
			'icon' => 'admin-ico users',
			'app_icon' => 'fa-sitemap', // icon used in the new menu
			'link' => Docebo::createAdminUrl('MultidomainApp'),
			'label' => Yii::t('multidomain', 'Multidomain'),
		), array(Yii::t('standard', 'Settings') => Docebo::createAdminUrl('MultidomainApp/main/mainSettings')));
	}
	
	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('MultidomainApp.assets'));
		}
		return $this->_assetsUrl;
	}

	/**
	 * Here we REPLACE the currently selected LMS language per current domain /if any/ and if needed, the rules for that change is as follows:
	 * 1. First we check for need of using the custom domain language setting, if the option is not set, just ignore the custom logic here
	 * 2. Then we check for explicitly change of user language /when the user is trying to change the language/
	 * 2.1 If there isn't such change, then we check if the user have already some preferences in core_setting_user table from the DB, if he's logged in
	 * - if he has and the chosen language is inactive for the current domain, we replace the language with the default, chosen from admin for the current domain
	 * - but without changing the user preferred language from the core_setting_user table
	 * - if user is anonymous, we leave the logic for changing language for the main LMS logic in Docebo::resolveLanguage()
	 * - here we have one exception - if the user have not yet session with his language choise /ex. he's visiting the lms for the first site/ we set language
	 * to default one, for the current domain
	 * 2.2 If there is such change, we have to use that choice
	 * - if the user is logged in, we check if the selected language is active for the current domain, if not, we again get the default language for the domain
	 * - also if the user is logged in, we change his choice in core_setting_user table
	 */
	public function getMultidomainLanguage(){
		$domain = CoreMultidomain::resolveClient();

		// get current settings from DB, not these which are in session
		$domain = CoreMultidomain::model()->findByPk($domain->id);
		if(!$domain || $domain->use_custom_settings == 0) return;

		$run_language = false;
		$disabledLanguages = explode(',', $domain->disabled_languages);
		if (empty($_GET['lang'])){
			/**
			 * we have request from the user for change the language
			 * we do this when one of the following is fillfiled:
			 * - if the user is logged, and the chosen language is active by current domain language settings
			 * - if the user is guest, he's not logged yet
			 */

			if (!Yii::app()->user->isGuest) {
				$settingUserLanguage = Yii::app()->db->createCommand()
						->select('value')
						->from(CoreSettingUser::model()->tableName())
						->where('path_name="ui.language" AND id_user=:id', array(':id' => Yii::app()->user->id))->queryScalar();
				if(!$settingUserLanguage) return;

				if (in_array($settingUserLanguage, $disabledLanguages))
					$run_language = Lang::getBrowserCodeByCode($domain->default_language);
			} elseif (Yii::app()->session['first_time_language'] === true) {

				// we apply the default language per this domain, ONLY if the user is anonymous and until now he haven't session with his language preferences
				$run_language = Lang::getBrowserCodeByCode($domain->default_language);
			}
		}
		else{
			// here we have explicitly change of user language
			$chosenLanguage = $_GET['lang'];
			$fullLanguage = Lang::getCodeByBrowserCode($chosenLanguage);
			if (!in_array($fullLanguage, $disabledLanguages)) {
				// the chosen language is active in the current domain
				$run_language = $chosenLanguage;
				if (!Yii::app()->user->isGuest)
					Yii::app()->db->createCommand()
							->update(CoreSettingUser::model()->tableName(), array(
									'value' => $fullLanguage
							), 'path_name  = "ui.language" AND id_user=:id', array(':id' => Yii::app()->user->id));
			} else {
				// the chosen language is forbidden for the domain, so we use the default language for the domain.
				$run_language = Lang::getBrowserCodeByCode($domain->default_language);
			}
		}
		if($run_language){
			Yii::app()->language = $run_language;  // browser code, like 'en'
			Yii::app()->session['current_lang'] = Lang::getCodeByBrowserCode($run_language); // lang-code style (for LMS)
			Yii::app()->session['_lang'] = $run_language;
		}
	}

	public function getMultidomainHeaderMessage(DEvent $event){
		$customMessageInHeader = &$event->params['customMessageInHeader'];

		$domain = CoreMultidomain::resolveClient();
		if($domain) {
			$domain = CoreMultidomain::model()->findByPk($domain->id);
			if ($domain->header_message_active == 1) {
				$currentLanguage = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
				$defaultLanguage = CoreLangLanguage::getDefaultLanguage();
				if ($domain->use_custom_settings == 1) {
					$defaultLanguage = $domain->default_language;
				}
				$model = CoreMultidomainHeaderFooter::model()->findByAttributes(
						array(
								'id_multidomain' => $domain->id,
								'language' => $currentLanguage,
								'type' => CoreMultidomainHeaderFooter::TYPE_HEADER_MESSAGE,
						)
				);
				if (!$model || empty($model->value)) {
					$model = CoreMultidomainHeaderFooter::model()->findByAttributes(
							array(
									'id_multidomain' => $domain->id,
									'language' => $defaultLanguage,
									'type' => CoreMultidomainHeaderFooter::TYPE_HEADER_MESSAGE,
							)
					);
				}

				$customMessageInHeader = $model->value;
			}
		}
	}
}
