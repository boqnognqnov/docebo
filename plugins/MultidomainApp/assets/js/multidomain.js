var MultidomainManClass = null;

(function ( $ ) {

	/**
	 * Constructor
	 */
	MultidomainManClass = function (options) {
		if (typeof options === "undefined" ) {
			var options = {};
		}
		this.init(options);
	};


	/**
	 * Prototype
	 */
	MultidomainManClass.prototype = {

		defaultOptions: {
			editClientDialogId			: 'edit-client-dialog',
			multidomainGridId			: 'multidomain-grid',
			massActionUrl				: '',
			settingsContainer			: '.multidomain-client-settings',
			selectedTab					: '#multidomain-settings-logo-and-colors',
            gridUrl                     : ''
		},

		options: {},

		translatedLanguages: [],

		init: function(options) {
			var that = this;

			$('body').addClass('multidomain-admin');

			$('input,select').styler();

			// Merge incoming options to default ones
			this.options = $.extend({}, this.defaultOptions, options);

			/**
			 * When some massive action is selected in some Combo Grid/List views
			 */
			$(document).on("combogridview.massive-action-selected", function(e, data) {

				if (data.selection === null || typeof data.selection === "undefined" || data.selection.length <= 0) {
					return;
				}

				var url = that.options.massActionUrl;
				var title = '';
				var id = 'mass-action-dialog';
				var modalClass = 'mass-action-dialog';
				var ajaxType = 'POST';

				switch (data.action) {
					case 'delete' :
						title = Yii.t('standard', '_DEL');
						break;

					default:
						// Unknown action? Get out!
						return;
				}

				var settings = {
					title: title,
				    content: url,
			        ajaxType: ajaxType,  // POST, GET, null
			        id: id,
			        modalClass: modalClass,
			        data: data
				};
			    $('<div/>').dialog2(settings);

			});


			/**
			 * Dialog update listeners for various Dialog2's
			 */
			$(document).on("dialog2.content-update", '.modal-body', function (e) {

				if ($(this).find("a.auto-close").length > 0) {

					$(this).dialog2("close");

                    if ((that.options.ui == 'settings') && (that.options.gridUrl))
                        window.location.href = that.options.gridUrl;

					// Update grid, if any
					if ($('#' + that.options.multidomainGridId).length > 0) {
						$('#' + that.options.multidomainGridId).yiiGridView('update');
					}

					// Mass action dialog has been closed, update appropriate grids or views, if any
					if ($(this).attr('id') === 'mass-action-dialog') {
						if (that.options.multidomainGridId && $('#'+that.options.multidomainGridId).length > 0) {
							$('#' + that.options.multidomainGridId).yiiGridView('update');
							$('#' + that.options.multidomainGridId).comboGridView('setSelection', []);
						}
					}
				}

				/**
				 * Edit Client dialog2 loaded, do some stuff, if any
				 */
				if ($(this).attr('id') === that.options.editClientDialogId) {
					that.editClientReady();
				}


			});


			// Clicks on Activate/Deactivate
			$(document).on('click', 'a.change-active-status', function () {
				$.get($(this).attr('href'), {}, function () {
					$('#' + that.options.multidomainGridId).yiiGridView('update', {});
				});
				return false;
			});


			// Click/Change (but CHECKED!!!!) on domain-type (subfolder, subdomain or just new custom domain)
			$(document).on('change', '.multidomain-client-domaintype[type="radio"]:checked', function () {
				var selector = '.domain-type-value.' + $(this).val();
				$('.domain-type-value').hide();
				$(selector).show();
			});


			// On Client DSettings-> Sign Page layout type is changed
			$(document).on('change', 'input[name="CoreMultidomain[signInPageLayoutOption]"]', function(e) {
				var target = e.currentTarget;
				that.updateLoginLayoutImage($(target).val());
			})

			// On ENEABLE/DISABLE checkbox change for some settings sections
			$(document).on('change', 'input.on-off-switch[type="checkbox"]', function(e){
				that.updateBlockableSections();
			});


	        // Intercept tab toggle event to save the current tab
	        $('.multidomain-client-settings a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	            $('#selectedTab').val($(e.target).data('target'));
	            $('.selectedTab').val($(e.target).data('target'));
	        });



	        // On modal "Image selector" success
	        $(document).on('modal-image-selector.success', '.image-selector-input', function(e, extraData) {
	        	var $target = $(e.currentTarget);  // the <input>
	        });


	        // On SAVE CHANGES click
	        $(document).on('click', 'input[name="save_multidomain_settings"]', function(e) {

	        	// Trigger "change" event for SIGN IN texts language selector, to activate translations populating process
	        	$('#signin-texts-lang-select').change();
	        });



	        // On Language change for Sign In Texts
	        $(document).on('change', '#signin-texts-lang-select', function(e) {
				tinyMCE.get('signin-text-textarea').focus();
	        	var $target = $(e.currentTarget);  // the <select>
	        	var newSelection 	= $target.val();
	        	var prevSelection 	= $target.attr('data-prev-selection');
	        	var inputTitle 		= $('input[name="signin_title_input"]').val();
	        	var inputText		= '';
	        	try {
	        		inputText  = tinymce.activeEditor.getContent();
	        	}
	        	catch (err) {

	        	}

	        	// If we are changing from some valid language, SAVE the input
	        	if (prevSelection !== '0') {
	        		var c = '.signin-texts-translations .' + prevSelection;
	        		$(c + ' .translation-title').val(inputTitle);
	        		$(c + ' .translation-title').attr('value', inputTitle);

	        		$(c + ' .translation-text').attr('value', inputText);
	        		$(c + ' .translation-text').val(inputText);
	        	}

	        	// Now update INPUT elements with saved translations
	        	var nc = '.signin-texts-translations .' + newSelection;
	        	var translatedTitle = $(nc + ' .translation-title');
	        	var translatedText  = $(nc + ' .translation-text');

                if (translatedTitle.val() && translatedTitle.val() !== null)
                    $('input[name="signin_title_input"]').val(translatedTitle.val());
				else
					$('input[name="signin_title_input"]').val('');

	        	if (translatedText.val() && translatedText.val() !== null)
					tinymce.activeEditor.setContent(translatedText.val());
				else
					tinymce.activeEditor.setContent('');


	        	// Save current (newly) selected language as previous selection for the next change
	        	$target.attr('data-prev-selection', $target.val());

	        	that.updateTranslatedSigninTexts();

	        });

	        /**
	         * When a "toggle signin webpage visibility" link is clicked (the eye)
	         */
	        $(document).on('click', '.toggle-signin-webpage', function(e) {
	        	var page = $(this).data('id');
	        	e.preventDefault();
				$.get($(this).attr('href'), {}, function (data) {
					if (data === 'ON') {
						$('.toggle-signin-webpage[data-id="'+page+'"]').removeClass('grayed');
					}
					else {
						$('.toggle-signin-webpage[data-id="'+page+'"]').addClass('grayed');
					}

				});
	        });

			// Self registrations
			$(document).on('change', 'input[name*=register_type]:radio', function(e) {
				that.updateAllowedDomainsVisibility($(this).val() == 2);
			});

			// ----

	        if (this.options.ui == 'settings') {
	        	this.visualizeInitialSettings();
	        	TinyMce.attach('#signin-text-textarea', 'standard', {height: 170, setup: function (ed) {
					ed.on('init', function(args) {
						$('#signin-texts-lang-select').change();
					});
				}});

				$('input').styler();
	        }


		},


		updateTranslatedSigninTexts: function() {
			var that = this;

			// Empty current list of translated languages
			that.translatedLanguages = [];

			// Count assigned/translated languages
			$('.signin-texts-translations > div').each(function() {
				var langCode = $(this).attr('class');
				if ($(this).find('input').val() != "" || $(this).find('textarea').val() != "") {
					that.translatedLanguages.push(langCode);
				}
			});
			$('.translated-signin-texts > span').html(that.translatedLanguages.length);

			// Update visual feedback of translated languages (*)
			$('#signin-texts-lang-select option').each(function(){
				var langCode = $(this).val();
				var lastChar = $(this).text().substr($(this).text().length - 1);

				if ($.inArray($(this).val(), that.translatedLanguages) !== -1) {
					$(this).attr('data-translated', "1");
					if (lastChar !== '*') {
						$(this).text($(this).text() + ' *');
					}
				}
				else {
					$(this).attr('data-translated', "0");
					if (lastChar === '*') {
						$(this).text($(this).text().slice(0, -1));
					}
				}
			});



		},


		/**
		 * Get initial form settings (on page load) and do some visualization, if any
		 */
		visualizeInitialSettings: function() {
			if (this.options.initialFormSettings) {
				this.updateLoginLayoutImage(this.options.initialFormSettings.signInPageLayoutOption);
			}
			this.updateBlockableSections();
			this.updateAllowedDomainsVisibility($('input[name*=register_type]:checked').val() == 2 );

	        // Open the currently selected tab (if any)
	        var initialTab = 'a[data-target="' + this.options.selectedTab + '"]';
	        $(initialTab).tab('show');

		},


		/**
		 * Set visible the layout sample image for the currently selected layout
		 */
		updateLoginLayoutImage: function(layoutOptionValue) {
			$('.multidomain-client-settings #multidomain-settings-sign-in-page .login-images > div').hide();
			$('.multidomain-client-settings #multidomain-settings-sign-in-page .login-images').find('.' + layoutOptionValue).show();
		},

		/**
		 * Some sections get visually and UI blocked if certain checkbox is not checked
		 */
		updateBlockableSections: function() {

			// Temporary do nothing to make the development easier
			// return;

			// SIGN IN
			if ($('input[name="CoreMultidomain[signInPageLayoutEnabled]"]').is(':checked')) {
				$('#multidomain-settings-sign-in-page .blockable').unblock();
			}
			else {
				this.blockBlockable($('#multidomain-settings-sign-in-page .blockable'));
			}


			// COURSE PLAYER
			if ($('input[name="CoreMultidomain[coursePlayerEnabled]"]').is(':checked')) {
				$('#multidomain-settings-course-player .blockable').unblock();
			}
			else {
				this.blockBlockable($('#multidomain-settings-course-player .blockable'));
			}


			// WHITE LABLE
			if ($('input[name="CoreMultidomain[whiteLabelEnabled]"]').is(':checked')) {
				$('#multidomain-settings-white-label .blockable').unblock();
			}
			else {
				this.blockBlockable($('#multidomain-settings-white-label .blockable'));
			}

			// E-COMMERCE
			if ($('input[name="CoreMultidomain[ecommerceEnabled]"]').is(':checked')) {
				$('#multidomain-settings-ecommerce .blockable').unblock();
			}
			else {
				this.blockBlockable($('#multidomain-settings-ecommerce .blockable'));
			}

			// COURSE CATALOG
			if ($('input[name="CoreMultidomain[catalog_custom_settings]"]').is(':checked')) {
				$('#multidomain-settings-coursecatalog .blockable').unblock();
			}
			else {
				this.blockBlockable($('#multidomain-settings-coursecatalog .blockable'));
			}

			// SELF REGISTRATION
			if ($('input[name="CoreMultidomain[enable_self_registration_settings]"]').is(':checked')) {
				$('#multidomain-self-registration-form .blockable').unblock();
			}
			else {
				this.blockBlockable($('#multidomain-self-registration-form .blockable'));
			}

		},

		updateAllowedDomainsVisibility: function(disabled){
			if(!disabled){
				$('#multidomain-self-registration-form div.allowed_domains').unblock();
			}else{
				this.blockBlockable($('#multidomain-self-registration-form div.allowed_domains'));
			}
		},

		/**
		 * Using jQuery blockUI plugin, block specific element
		 */
		blockBlockable: function(elem) {
			elem.block({
				message: "",
				timeout: 0,
				overlayCSS:  {
					backgroundColor: 	'#FFF',
					opacity:         	0.6,
					cursor:          	'auto'
				}
			});
		},



		/**
		 * EDIT Client dialog has been loaded
		 */
		editClientReady: function() {
		}


	};


}( jQuery ));
