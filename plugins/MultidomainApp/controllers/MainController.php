<?php

class MainController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl'
		);
	}

	public function accessRules()
	{
		$res = array();

		$res[] = array(
			'allow',
			'expression' => 'Yii::app()->user->getIsGodadmin()',
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*')
		);

		return $res;
	}

	/**
	 * Register client resources (CSS, JS,..)
	 */
	public function registerResources() {
		if (!Yii::app()->request->isAjaxRequest) {

			$cs = Yii::app()->getClientScript();

			$assetsUrl = $this->module->getAssetsUrl();
			Yii::app()->tinymce->init();

			$cs->registerScriptFile($assetsUrl 	. '/js/multidomain.js');
			$cs->registerCssFile($assetsUrl 	. '/css/multidomain.css');

			$themeUrl = Yii::app()->theme->baseUrl;
			$cs->registerScriptFile($themeUrl . '/js/jwizard.js');

			// BlockUI
			$cs->registerScriptFile($assetsUrl . '/js/jquery.blockUI.js');

			//FCBK complete
			Yii::app()->clientScript->registerCssFile($themeUrl . '/css/fcbkcomplete.css');
			Yii::app()->clientScript->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js');

		}

	}



	public function beforeAction($action) {
		JsTrans::addCategories('multidomain');
		return parent::beforeAction($action);

	}

	public function actionIndex()
	{
		$this->registerResources();

		$searchInput = Yii::app()->request->getParam('search_input', false);
		$selectAll = Yii::app()->request->getParam('comboGridViewSelectAll', false);

		$model = new CoreMultidomain();

		if ($selectAll) {
			$dataProvider = $model->sqlDataProvider();
			$dataProvider->setPagination(false);
			$keys = $dataProvider->keys;
			foreach ($keys as $index => $key) {
				$keys[$index] = (int) $key;
			}
			echo json_encode($keys);
			Yii::app()->end();
		}


		$gridId = 'multidomain-grid';

		// Define array of available massive actions to show: <value> => <label>
		$massiveActions = array(
			'delete' => Yii::t('standard', '_DEL'),
		);

		$model->search_input = $searchInput ? $searchInput : null;
		$dataProvider = $model->sqlDataProvider();



		$massActionUrl = Docebo::createAdminUrl('MultidomainApp/main/massAction');

		$gridParams = array(
			'gridId' 				=> $gridId,
			'dataProvider'			=> $dataProvider,
			'columns'				=> $this->getMultidomainGridColumns(),
			'autocompleteRoute'		=> '//MultidomainApp/main/multidomainAutocomplete',
			'massiveActions'		=> $massiveActions,
		);

		$params = array(
			'gridParams'				=> $gridParams,
			'model'						=> $model,
			'editClientDialogId'		=> 'edit-client-dialog',
			'massActionUrl'				=> $massActionUrl,
		);


		// If this is an Yii Grid Update request (detected by ajax=<grid-ID> in the Request)
		// just render-partial the grid. This hugely improves the speed!!!
		if (isset($_REQUEST['ajax']) && ($_REQUEST['ajax'] == $gridId)) {
			$this->widget('common.widgets.ComboGridView', $gridParams);
			Yii::app()->end();
		}


		$this->render('index', $params);
	}


	protected function getMultidomainGridColumns() {

		$columns = array(
			array(
				'class' => 'CCheckBoxColumn',
				'id' 	=> 'items-grid-checkboxes',
				'value' => '$data["id"]',
			),

			array(
				'name' => 'name',
				'value' => '$data["name"]',
				'type' => 'raw',
				'header' => Yii::t('standard', '_NAME'),
				'htmlOptions' => array(
					'class' => 'center-aligned'
				),
				'headerHtmlOptions' => array(
					'class' => 'center-aligned'
				),
			),

			array(
				'name' => 'logo',
				'value' => array($this, 'renderCompanyLogo'),
				'type' => 'raw',
				'header' => Yii::t('templatemanager', 'Logo'),
				'htmlOptions' => array(
					'class' => 'center-aligned'
				),
				'headerHtmlOptions' => array(
					'class' => 'center-aligned'
				),
			),

			array(
				'name' => 'domain',
				'value' => array($this, 'renderClientDomain'),
				'type' => 'raw',
				'header' => Yii::t('custom_domain', '_DOMAIN_NAME_'),
				'htmlOptions' => array(
					'class' => 'center-aligned'
				),
				'headerHtmlOptions' => array(
					'class' => 'center-aligned'
				),
			),

			array(
				'name' => 'node',
				'value' => array($this, 'renderOrgChartColumn'),
				'type' => 'raw',
				'header' => Yii::t('standard', 'Node'),
				'htmlOptions' => array(
					'class' => 'center-aligned'
				),
				'headerHtmlOptions' => array(
					'class' => 'center-aligned'
				),
			),

			array(
				'name' => 'users',
				'value' => array($this, 'renderUsersNumberColumn'),
				'type' => 'raw',
				'header' => Yii::t('standard', '_USERS'),
				'htmlOptions' => array(
					'class' => 'center-aligned'
				),
				'headerHtmlOptions' => array(
					'class' => 'center-aligned'
				),
			),


			array(
				'name' => 'operations',
				'value' => array($this, 'renderMultidomainGridActions'),
				'type' => 'raw',
				'header' => '',
				'htmlOptions' => array(
					'class' => 'text-right'
				),
				'headerHtmlOptions' => array(
				),
			),

		);


		return $columns;

	}


	/**
	 * The backend counterpart of the "search autocomplete" (JuiAutocomplete)
	 */
	public function actionMultidomainAutocomplete() {

		$maxNumber 		= Yii::app()->request->getParam('max_number', 10);
		$term 			= Yii::app()->request->getParam('term', false);

		$model = new CoreMultidomain();

		$model->search_input = $term ? $term : null;

		$dataProvider = $model->sqlDataProvider();
		$dataProvider->setPagination(array(
			'pageSize' => $maxNumber,
		));
		$data = $dataProvider->getData();
		$result = array();
		if ($data) {
			foreach ($data as $item) {
				$result[] = array(
					'label' => $item['name'],
					'value' => $item['name'],
				);
			}
		}
		echo CJSON::encode($result);
	}

	/**
	 * Mass operations logic
	 *
	 */
	public function actionMassAction() {

		$action		= Yii::app()->request->getParam('action', false);
		$selection 	= Yii::app()->request->getParam('selection', false);

		if ($action === 'delete') {
			$this->massDeleteClients($selection);
			Yii::app()->end();
		}

		$this->renderPartial('admin.protected.views.common._dialog2_error', array(
			'type' => 'error',
			'message' => 'Invalid mass action',
		));
		Yii::app()->end();

	}


	/**
	 * Delete a dashboard layout
	 */
	public function actionDeleteClient() {

		try {
			$id = Yii::app()->request->getParam('id', false);
			$model = CoreMultidomain::model()->findByPk($id);

			if (!$model) {
				throw new Exception('Invalid model');
			}

			if (isset($_REQUEST['confirm'])) {
				if (isset($_REQUEST['agree_delete']) && $_REQUEST['agree_delete'] == 'agree') {
					$model->delete();
				}
				echo '<a class="auto-close success"></a>';
				Yii::app()->end();
			}

			$this->renderPartial('_delete_client', array(
				'model'	=> $model,
			));
			Yii::app()->end();
		}
		catch (Exception $e) {
			$this->renderPartial('//common/_dialog2_error', array('type' => 'error',  'message' => $e->getMessage()));
			Yii::app()->end();
		}
	}



	/**
	 * Mass-Remove Multidomain clients
	 *
	 * @param array|string $selection
	 *
	 * @throws Exception
	 */
	protected function massDeleteClients($selection) {

		if (is_string($selection)) {
			$selection = explode(',', $selection);
		}

		try {

			$confirmDelete	= Yii::app()->request->getParam('confirm_delete', false);

			if (count($selection) <= 0 || empty($selection)) {
				throw new Exception(Yii::t('certification', 'Invalid selection'));
			}

			if ($confirmDelete) {

				if (isset($_REQUEST['agree_delete']) && $_REQUEST['agree_delete'] == 'agree') {
					$model = new CoreMultidomain();
					$model->deleteAllByAttributes(array('id' => $selection));
				}

				echo '<a href="" class="auto-close"></a>';
				Yii::app()->end();
			}

			$html = $this->renderPartial('_mass_delete', array(
				'selection' => $selection,
			), true, true);
			echo $html;
		}
		catch (Exception $e) {
			$this->renderPartial('admin.protected.views.common._dialog2_error', array(
				'type' => 'error',
				'message' => $e->getMessage(),
			));
			Yii::app()->end();
		}
	}


	/**
	 * Render the "operations" column
	 *
	 * @param array $data
	 * @param integer $index
	 * @return string
	 */
	protected function renderMultidomainGridActions($data, $index) {
		$html = $this->renderPartial('_operations', array(
			'data' => $data,
		));
		return $html;
	}

	/**
	 * Set a Client active/inactive
	 *
	 * @throws CException
	 */
	public function actionSetActive()  {

		$active = Yii::app()->request->getParam("active", false);
		$id 	= Yii::app()->request->getParam("id", false);

		// DB Start transaction
		$transaction = Yii::app()->db->beginTransaction();

		try {
			$model = CoreMultidomain::model()->findByPk($id);
			if (!$model) {
				throw new CException(Yii::t('standard','_OPERATION_FAILURE'));
			}

			$model->active = $active;

			if (!$model->validate()) {
				throw new CException(Yii::t('standard','_OPERATION_FAILURE'));
			}

			$model->save(false);

			$transaction->commit();
			Yii::app()->end();

		}
		catch (CException $e) {
			$transaction->rollback();
			$html = $this->renderPartial('//common/_dialog2_error', array(
					'message' => $e->getMessage(),
			), true);

			echo $html;
			Yii::app()->end();
		}
	}


	public function actionSettings() {
		Yii::app()->clientScript->registerCssFile('js/farbtastic/farbtastic.css');
//		Yii::app()->clientScript->registerCoreScript('jquery.ui');
		Yii::app()->clientScript->registerScriptFile('js/farbtastic/farbtastic.min.js');
//		Yii::app()->clientScript->registerScriptFile('js/jquery.tools.min.js');

		$selectedTab 		= Yii::app()->request->getParam("selectedTab", '#multidomain-settings-logo-and-colors');


		$idClient 			= Yii::app()->request->getParam("id", false);
		$save				= Yii::app()->request->getParam("save_multidomain_settings", false);
		$validationErrors = array();

		$clientModel 		= CoreMultidomain::model()->findByPk($idClient);

		// Old minimal layout settings
		$oldMinimalLoginType = $clientModel->signInMinimalType;
		$oldMinimalLoginBg = $clientModel->signInMinimalBackground;

		$clientModel->setDefaultAttributeValues();
		$defaultLanguage = CoreLangLanguage::getDefaultLanguage();
		if ($clientModel->use_custom_settings == 1) {
			$defaultLanguage = $clientModel->default_language;
		}

		if ($save && isset($_REQUEST['CoreMultidomain']) && is_array($_REQUEST['CoreMultidomain'])) {


			// Prevent overwriting these two Asset IDs if NO files are uploaded
			if (!$_REQUEST['CoreMultidomain']['companyLogo'])
				unset($_REQUEST['CoreMultidomain']['companyLogo']);
			if (!$_REQUEST['CoreMultidomain']['favicon'])
				unset($_REQUEST['CoreMultidomain']['favicon']);

			$oldValueForHeaderMessage = $clientModel->header_message_active;
			$oldValueWhiteLabelHeader = $clientModel->whiteLabelHeader;
			$clientModel->attributes = $_REQUEST['CoreMultidomain'];

			Yii::app()->getDb()->schema->refresh();
			$activeThemeVariant = Yii::app()->getDb()->createCommand()
					->select("*")
					->from("theme_variant")
					->where("`id_multidomain`=:idMultidomain AND `active`= :active")
					->limit(1)
					->queryRow(true, array(
							':idMultidomain'	=>	$clientModel->id,
							':active'			=>	1
					));

			if ($activeThemeVariant){
				$settingsArr = CJSON::decode($activeThemeVariant['settings']);
				if ($_REQUEST['CoreMultidomain']['catalog_custom_settings'] == 0){
					if (isset($settingsArr['signin']['display_catalog'])){
						unset($settingsArr['signin']['display_catalog']);
						if (empty($settingsArr['signin'])){
							unset($settingsArr['signin']);
						}
					}
				}elseif($_REQUEST['CoreMultidomain']['catalog_custom_settings'] == 1){
					if (isset($_REQUEST['CoreMultidomain']['catalog_external'])){
						$settingsArr['signin']['display_catalog'] = (int)$_REQUEST['CoreMultidomain']['catalog_external'];
					}
				}
				$settingsArr = CJSON::encode($settingsArr);
				Yii::app()->getDb()->createCommand('UPDATE theme_variant SET settings = \''.$settingsArr.'\' WHERE `id_multidomain`=:idMultidomain AND `active`= :active')->execute(array(
						':idMultidomain'	=>	$clientModel->id,
						':active'			=>	1
				));
			}


			if($clientModel->whiteLabelEnabled == 0 || $clientModel->whiteLabelPoweredByOption != CoreLangWhiteLabelSetting::TYPE_FOOTER_URL)
				$clientModel->whiteLabelFooterExtUrl = '';
			// Reset to default minimal login beackground value if the minimal login type is changed(before we add asset(-s))
			if($oldMinimalLoginType <> $clientModel->signInMinimalType) {
//				$clientModel->signInMinimalBackground = '#ffffff';
			}

			// Get and set the selected catalogs

			$isSetSelectedCategories = Yii::app()->request->getParam('selected_catalogs');

			if(isset($isSetSelectedCategories)){
				$selected_catalogs = Yii::app()->htmlpurifier->purify($isSetSelectedCategories);
				$clientModel->catalog_external_selected_catalogs = $selected_catalogs;
			}

			if(isset($_REQUEST['CoreMultidomain']['allowed_domains'])){
				$clientModel->allowed_domains = $_REQUEST['CoreMultidomain']['allowed_domains'];
			}else{
				if($selectedTab == "#multidomain-settings-self-registration")
					$clientModel->allowed_domains = array();
			}


			// Raise event to let plugins check the submitted data
			Yii::app()->event->raise('DomainBrandingSettingsFormSubmit', new DEvent($this, array(
				'id_org'						=> $clientModel->org_chart, 	// Branch, associated to the Multidomain client
				'multidomain_client_id' 		=> $clientModel->id,			// Multidomain Client ID
				'multidomain_client_model' 		=> $clientModel,  			// Just in case, pass also CoreMultidomain model
			)));

			if ($clientModel->save()) {

				// Trigger hydra invalidation if the domain we're changing is the same we're currently on
				$currentClient = CoreMultidomain::resolveClient();
				if($currentClient && ($currentClient->id == $clientModel->id))
					Yii::app()->legacyWrapper->invalidate();

				// Proceed with creating assets

				$minimal_bg_img_path = !empty($_POST['CoreMultidomain']['minimal_bg_img_path']) ? $_POST['CoreMultidomain']['minimal_bg_img_path'] : '';
				$minimal_bg_img_original_filename = !empty($_POST['CoreMultidomain']['minimal_bg_img_original_filename']) ? $_POST['CoreMultidomain']['minimal_bg_img_original_filename'] : '';

				$minimal_bg_video_path = !empty($_POST['CoreMultidomain']['minimal_bg_video_path']) ? $_POST['CoreMultidomain']['minimal_bg_video_path'] : '';
				$minimal_bg_video_original_filename = !empty($_POST['CoreMultidomain']['minimal_bg_video_original_filename']) ? $_POST['CoreMultidomain']['minimal_bg_video_original_filename'] : '';

				$minimal_video_fallback_img_path = !empty($_POST['CoreMultidomain']['minimal_video_fallback_img_path']) ? $_POST['CoreMultidomain']['minimal_video_fallback_img_path'] : '';
				$minimal_video_fallback_img_original_filename = !empty($_POST['CoreMultidomain']['minimal_video_fallback_img_original_filename']) ? $_POST['CoreMultidomain']['minimal_video_fallback_img_original_filename'] : '';

				if($clientModel->signInMinimalType == 'image' && !empty($minimal_bg_img_original_filename) && !empty($minimal_bg_img_path)) {
					BrandingSigninForm::createAsset($minimal_bg_img_original_filename, $minimal_bg_img_path, $clientModel, 'signInMinimalBackground', CoreAsset::TYPE_GENERAL_ASSET, array(), true);
				}

				if($clientModel->signInMinimalType == 'video' && !empty($minimal_bg_video_original_filename) && !empty($minimal_bg_video_path)) {
					BrandingSigninForm::createAsset($minimal_bg_video_original_filename, $minimal_bg_video_path, $clientModel, 'signInMinimalBackground', CoreAsset::TYPE_GENERAL_ASSET);
				}

				if($clientModel->signInMinimalType == 'video' && !empty($minimal_video_fallback_img_original_filename) && !empty($minimal_video_fallback_img_path)) {
					BrandingSigninForm::createAsset($minimal_video_fallback_img_original_filename, $minimal_video_fallback_img_path, $clientModel, 'signInMinimalVideoFallbackImg', CoreAsset::TYPE_GENERAL_ASSET, array(), true);
				}
			}

			// Save SignIn Texts
			if (isset($_REQUEST['settings_tab_pane']) && $_REQUEST['settings_tab_pane'] == CoreMultidomain::GROUPSETTING_SIGNIN)
				CoreMultidomainSigninText::updateTranslations($idClient, $_REQUEST['SigninTitle'], $_REQUEST['SigninText']);

			// update header messages for current domain
			if ($clientModel->header_message_active == 1 && isset($_POST['valuesForHeaderMsgMultidomain'])) {
				$valuesForHeaderMsgMultidomain = $_POST['valuesForHeaderMsgMultidomain'];
				if (empty($valuesForHeaderMsgMultidomain[$defaultLanguage])) {
					Yii::app()->user->setFlash('error', Yii::t('user_management', '_ERROR_ADDITIONAL_FIELDS_ENGLISH_REQUIRED'));
					$_POST['CoreMultidomain']['header_message_active'] = $oldValueForHeaderMessage;
					$clientModel->header_message_active = $oldValueForHeaderMessage;
					$clientModel->save();
				}
				else
					foreach ($valuesForHeaderMsgMultidomain as $lang => $value) {
						$model = CoreMultidomainHeaderFooter::model()->findByAttributes(
								array(
										'language' => $lang,
										'id_multidomain' => $clientModel->id,
										'type' => CoreMultidomainHeaderFooter::TYPE_HEADER_MESSAGE
								)
						);
						if (!$model) {
							$model = new CoreMultidomainHeaderFooter();
							$model->language = $lang;
							$model->type = CoreMultidomainHeaderFooter::TYPE_HEADER_MESSAGE;
							$model->id_multidomain = $clientModel->id;
						}

						$model->value = $value;
						$model->save();
					}
			}

			// update header url for this domain
			if($_POST['CoreMultidomain']['whiteLabelEnabled'] == 1 && $_POST['CoreMultidomain']['whiteLabelHeader'] == 'external_header') {
				$customMultiHeaderUrl = $_POST['valuesForUrlMultiHeader'];
				if (empty($customMultiHeaderUrl[$defaultLanguage]) || $customMultiHeaderUrl[$defaultLanguage] == 'http://') {
					$validationErrors['whiteLabelHeader'] = Yii::t('user_management', '_ERROR_ADDITIONAL_FIELDS_ENGLISH_REQUIRED');
					$clientModel->whiteLabelHeader = $oldValueWhiteLabelHeader;
					$clientModel->whiteLabelHeaderExtUrl = null;
					$clientModel->save();
				} else {

					$idMultiDomain = intval($_GET['id']);
					$wrongUrls = array();
					$urlValidator = new CUrlValidator();
					foreach ($customMultiHeaderUrl as $lang => $item) {
						$customDomainSettingsModel = CoreMultidomainHeaderFooter::model()->findByAttributes(array(
								'id_multidomain' => $idMultiDomain,
								'language' => $lang,
								'type' => BrandingWhiteLabelForm::POWEREDBY_CUSTOM_URL_HEADER,
						));
						if (!$customDomainSettingsModel) {
							$customDomainSettingsModel = new CoreMultidomainHeaderFooter();
							$customDomainSettingsModel->id_multidomain = $idMultiDomain;
							$customDomainSettingsModel->language = $lang;
							$customDomainSettingsModel->type = BrandingWhiteLabelForm::POWEREDBY_CUSTOM_URL_HEADER;
						}
						if (empty($item)) $item = 'http://';
						if ($item == 'http://') {
							$isUrlValid = true;
						} else {
							$isUrlValid = $urlValidator->validateValue($item);
						}
						if ($isUrlValid) {
							$customDomainSettingsModel->value = $item;
							$customDomainSettingsModel->save();
						} else {
							$wrongUrls[] = $lang;
						}
					}
					if(!empty($wrongUrls))
						$validationErrors['whiteLabelHeader'] = Yii::t('standard', 'Please, enter a valid URL') . ' - ' . implode(', ', $wrongUrls);
				}
			}

			// update footer custom text multidomain
			if($_POST['CoreMultidomain']['whiteLabelPoweredByOption'] == CoreLangWhiteLabelSetting::TYPE_FOOTER_TEXT){
				$valuesForFooterMsgMultidomain = $_POST['valuesForTextMulti'];
				if (empty($valuesForFooterMsgMultidomain[$defaultLanguage]))
					$validationErrors['whiteLabelPoweredByOption'] = Yii::t('user_management', '_ERROR_ADDITIONAL_FIELDS_ENGLISH_REQUIRED');
				else
					foreach ($valuesForFooterMsgMultidomain as $lang => $value) {
						$model = CoreMultidomainHeaderFooter::model()->findByAttributes(
								array(
										'language' => $lang,
										'id_multidomain' => $clientModel->id,
										'type' => CoreMultidomainHeaderFooter::TYPE_FOOTER_CUSTOM_MESSAGE
								)
						);
						if (!$model) {
							$model = new CoreMultidomainHeaderFooter();
							$model->language = $lang;
							$model->type = CoreMultidomainHeaderFooter::TYPE_FOOTER_CUSTOM_MESSAGE;
							$model->id_multidomain = $clientModel->id;
						}

						$model->value = $value;
						$model->save();
					}
			}

			// update footer custom url multidomain
			if($_POST['CoreMultidomain']['whiteLabelPoweredByOption'] == CoreLangWhiteLabelSetting::TYPE_FOOTER_URL){
				$valuesForFooterUrlMultidomain = $_POST['valuesForUrlMulti'];
				if (empty($valuesForFooterUrlMultidomain[$defaultLanguage]))
					$validationErrors['whiteLabelPoweredByOption'] = Yii::t('user_management', '_ERROR_ADDITIONAL_FIELDS_ENGLISH_REQUIRED');
				else {
					$wrongUrls = array();
					$urlValidator = new CUrlValidator();
					foreach ($valuesForFooterUrlMultidomain as $lang => $value) {
						$model = CoreMultidomainHeaderFooter::model()->findByAttributes(
								array(
										'language' => $lang,
										'id_multidomain' => $clientModel->id,
										'type' => CoreMultidomainHeaderFooter::TYPE_FOOTER_CUSTOM_URL
								)
						);
						if (!$model) {
							$model = new CoreMultidomainHeaderFooter();
							$model->language = $lang;
							$model->type = CoreMultidomainHeaderFooter::TYPE_FOOTER_CUSTOM_URL;
							$model->id_multidomain = $clientModel->id;
						}
						if(empty($value)) $value = 'http://';
						if ($value == 'http://') {
							$isUrlValid = true;
						} else {
							$isUrlValid = $urlValidator->validateValue($value);
						}
						if ($isUrlValid) {
							$model->value = $value;
							$model->save();
						} else {
							$wrongUrls[] = $lang;
						}
					}
					if(!empty($wrongUrls))
						$validationErrors['whiteLabelPoweredByOption'] = Yii::t('standard', 'Please, enter a valid URL') . ' - ' . implode(', ', $wrongUrls);
				}
			}

			Yii::app()->event->raise(CoreNotification::NTYPE_ECOMMERCE_SETTINGS_CHANGED, new DEvent($this, array('user' => Yii::app()->user->id)));


		}


        if (isset($_POST['save_subscription_settings'])) {

			$customSettingsAreEnabled = 'off';

			if (isset($_POST['subscription_custom_settings'])) {

				$clientModel->subscription_custom_settings = self::convertBooleanToString($_POST['subscription_custom_settings']);

				$customSettingsAreEnabled = self::convertBooleanToString($_POST['subscription_custom_settings']);
			}

			if ($customSettingsAreEnabled == 'on') {
				if (isset($_POST['subscription_enforce_seats_limit'])) {

					$clientModel->subscription_enforce_seats_limit = self::convertBooleanToString($_POST['subscription_enforce_seats_limit']);
				}

				if (isset($_POST['subscription_enforce_licences_limit'])) {

					$clientModel->subscription_enforce_licences_limit = self::convertBooleanToString($_POST['subscription_enforce_licences_limit']);
				}

				if (isset($_POST['enforce_expiration_dates'])) {

					$clientModel->enforce_expiration_dates = self::convertBooleanToString($_POST['enforce_expiration_dates']);
				}

				if (isset($_POST['default_renewal_method'])) {

					$clientModel->default_renewal_method = $_POST['default_renewal_method'];
				}

				if (isset($_POST['sub_automatic_refund'])) {

					$clientModel->sub_automatic_refund = self::convertBooleanToString($_POST['sub_automatic_refund']);
				}
				
				if (isset($_POST['licences_refunding'])) {

					$clientModel->licences_refunding = self::convertBooleanToString($_POST['licences_refunding']);
				}
				
				if (isset($_POST['seats_refunding'])) {

					$clientModel->seats_refunding = self::convertBooleanToString($_POST['seats_refunding']);
				}
				
				if (isset($_POST['sub_tab_first'])) {

					$clientModel->sub_tab_first = self::convertBooleanToString($_POST['sub_tab_first']);
				}
				
				if (isset($_POST['block_seats_refunding'])) {

					$clientModel->block_seats_refunding = $_POST['block_seats_refunding'];
				}
			}
			$clientModel->save();
		}


		if (PluginManager::isPluginActive('HttpsApp'))
			Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . "/js/https_settings.js", CClientScript::POS_END);

		$this->registerResources();

		$countSelectedCatalogs = 0;
		if ($clientModel->catalog_external_selected_catalogs <> '') {
			$countSelectedCatalogs = count(explode(',', $clientModel->catalog_external_selected_catalogs));
		}

		$langModel = new CoreLangLanguage();

		if (Yii::app()->request->isAjaxRequest && isset($_REQUEST['CoreLangLanguage'])) {
			$langModel->attributes = $_REQUEST['CoreLangLanguage'];
		}
		Yii::app()->tinymce->init();

		// get custom domain headers
		if($clientModel->use_custom_settings == 1) {
			$disabledLanguages = explode(',', $clientModel->disabled_languages);
			$languagesDB = Yii::app()->db->createCommand()
					->select('lang_code, lang_description')
					->from(CoreLangLanguage::model()->tableName())
					->where(array('NOT IN', 'lang_code', $disabledLanguages))->queryAll();
			$languages = array();
			foreach ($languagesDB as $lang) {
				$languages[$lang['lang_code']] = $lang['lang_description'];
			}
		} else {
			// get main domain language configuration
			$languages = CoreLangLanguage::getActiveLanguages();
		}

		$languagesForUrlHeader = $languages;

		// get custom domain header urls
		$countValuesForUrlHeader = 0;
		$domainHeaderUrls = array();
		$valuesForHeaderUrl = Yii::app()->db->createCommand()->select('language, value')
				->from(CoreMultidomainHeaderFooter::model()->tableName())
				->where('type=:type AND id_multidomain=:id', array(':type' => BrandingWhiteLabelForm::POWEREDBY_CUSTOM_URL_HEADER,':id' => $clientModel->id))
				->andWhere(array('IN', 'language', array_keys($languages)))->queryAll();
		foreach ($valuesForHeaderUrl as $item) {
			if(!empty($item['value']) && $item['value'] != 'http://') {
				$languagesForUrlHeader[$item['language']] .= ' *';
				$countValuesForUrlHeader++;
			}
			if(empty($item['value'])) $item['value'] = 'http://';
			$domainHeaderUrls[$item['language']] = $item['value'];
		}
		$languagesForUrlHeader[$defaultLanguage] .= ' (' . Yii::t('standard', '_DEFAULT_LANGUAGE') . ')';
		foreach ($languages as $langCode => $language) {
			if (!isset($domainHeaderUrls[strtolower($langCode)]))
				$domainHeaderUrls[strtolower($langCode)] = 'http://';
		}
		$languages[$defaultLanguage].=' ('.Yii::t('standard', '_DEFAULT_LANGUAGE').')';
		$languagesForFooterText = $languages;
		$languagesForFooterUrl = $languages;
		$languagesValuesDB = Yii::app()->db->createCommand()
				->select()->from(CoreMultidomainHeaderFooter::model()->tableName())
				->where('type=:type AND id_multidomain=:idDomain', array(':type' => CoreMultidomainHeaderFooter::TYPE_HEADER_MESSAGE, ':idDomain' => $clientModel->id))
				->andWhere(array('IN', 'language', array_keys($languages)))->queryAll();
		$languagesValues = array();
		foreach ($languagesValuesDB as $lang) {
			if (!empty($lang['value'])) {
				$languagesValues[$lang['language']] = $lang['value'];
				$languages[$lang['language']] .= ' *';
			}
		}

		// get custom domain footers - for texts
		$domainFooterTextsDB = Yii::app()->db->createCommand()
				->select()
				->from(CoreMultidomainHeaderFooter::model()->tableName())
				->where('type=:type AND id_multidomain=:idDomain', array(':type' => CoreMultidomainHeaderFooter::TYPE_FOOTER_CUSTOM_MESSAGE, ':idDomain' => $clientModel->id))
				->andWhere(array('IN', 'language', array_keys($languages)))->queryAll();
		$domainFooterTexts = array();
		foreach ($domainFooterTextsDB as $lang) {
			if (!empty($lang['value'])) {
				$domainFooterTexts[$lang['language']] = $lang['value'];
				$languagesForFooterText[$lang['language']] .= ' *';
			}
		}

		// and urls...
		$domainFooterUrlsDB = Yii::app()->db->createCommand()
				->select()
				->from(CoreMultidomainHeaderFooter::model()->tableName())
				->where('type=:type AND id_multidomain=:idDomain', array(':type' => CoreMultidomainHeaderFooter::TYPE_FOOTER_CUSTOM_URL, ':idDomain' => $clientModel->id))
				->andWhere(array('IN', 'language', array_keys($languages)))->queryAll();
		$domainFooterUrls = array();
		$countValuesForUrl = 0;
		foreach ($domainFooterUrlsDB as $lang) {
			if (!empty($lang['value'])) {
				$domainFooterUrls[$lang['language']] = $lang['value'];
				if($lang['value'] != 'http://') {
					$languagesForFooterUrl[$lang['language']] .= ' *';
					$countValuesForUrl++;
				}
			}
		}

		foreach($validationErrors as $attribute => $error) {
			if(isset($clientModel->$attribute))
				$clientModel->addError($attribute, $error);
		}

		if (PluginManager::isPluginActive('SubscriptionsApp')) {

			$subscriptionSettingsData['subscription_custom_settings'] =$clientModel->subscription_custom_settings;

			$subscriptionSettingsData['subscription_enforce_seats_limit'] =$clientModel->subscription_enforce_seats_limit;

			$subscriptionSettingsData['subscription_enforce_licences_limit'] =$clientModel->subscription_enforce_licences_limit;

			$subscriptionSettingsData['enforce_expiration_dates'] =$clientModel->enforce_expiration_dates;

			$subscriptionSettingsData['default_renewal_method'] =$clientModel->default_renewal_method;

			$subscriptionSettingsData['licences_refunding'] =$clientModel->licences_refunding;
			
			$subscriptionSettingsData['seats_refunding'] =$clientModel->seats_refunding;
			
			$subscriptionSettingsData['sub_tab_first'] =$clientModel->sub_tab_first;
			
			$subscriptionSettingsData['block_seats_refunding'] =$clientModel->block_seats_refunding;

		}

		$currentTheme = ThemeVariant::model()->findByAttributes(array(
		    'id_multidomain' => $clientModel->id,
            'active' => 1
        ));

		$activeTheme = array(
		    'code' => ThemeVariant::LEGACY_THEME_CODE,
            'name'=> ThemeVariant::LEGACY_THEME_NAME
        );

		if($currentTheme){
		    $activeTheme['code'] = $currentTheme->theme_code;
		    $activeTheme['name'] = $currentTheme->name;
        }


		$params = array(
			'model' 		            => $clientModel,
			'selectedTab'	            => $selectedTab,
			'countSelectedCatalogs'     => $countSelectedCatalogs,
			'langModel'    				=> $langModel,
			'defaultLanguage'    		=> $defaultLanguage,
			'languages'    				=> $languages,
			'languagesValues'    		=> $languagesValues,
			'languagesForFooterText'    => $languagesForFooterText,
			'languagesForFooterUrl'    	=> $languagesForFooterUrl,
			'domainFooterTexts'    		=> $domainFooterTexts,
			'domainFooterUrls'    		=> $domainFooterUrls,
			'domainHeaderUrls'    		=> $domainHeaderUrls,
			'countValuesForUrl'    		=> $countValuesForUrl,
			'countValuesForUrlHeader'	=> $countValuesForUrlHeader,
			'languagesForUrlHeader' 	=> $languagesForUrlHeader,
			'subscriptionSettingsData' 	=> $subscriptionSettingsData,
            'active_theme'              => $activeTheme
		);

//		Log::_($params);
		$this->render('settings', $params);
	}

	/**
	 *  AJAX called to return Multidomain/Client Settings/Https UI
	 */
	public function actionHttps() {

		if(!PluginManager::isPluginActive('HttpsApp'))
			$this->sendJSON(array('success' => false));

		$idClient = Yii::app()->request->getParam("id", false);

		$httpsModel = CoreHttps::getMultidomainClientHttps($idClient, true);

		$httpsModel->processSettingsRequest($_REQUEST, CoreHttps::CONTEXT_MULTIDOMAIN_HTTPS);

		$clientModel = CoreMultidomain::model()->findByPk($idClient);

		if (Yii::app()->request->isAjaxRequest) {

            $html = $this->renderPartial('_settings_https', array(
			     'model' 		    => $httpsModel,
				 'context'		    => CoreHttps::CONTEXT_MULTIDOMAIN_HTTPS,
                 'skipCertUpload'   => CoreHttps::sibling2ndLvlDomainIsWildcarded($clientModel->getClientUri(true), $idClient),
            ), true, false);

			$this->sendJSON(array(
				'success' => true,
				'html' => $html,
			));
		}

	}


	/**
	 * Change the availablility of a given Web page to the client (mini web pages at the Sign In UI)
	 * AJAX called to just toggle (add/delete) from a mapping table
	 */
	public function actionToggleWebPageVisibility() {
		$id		= Yii::app()->request->getParam("id", false);
		$page	= Yii::app()->request->getParam("page", false);

		$model = CoreMultidomainWebpage::model()->findByAttributes(array(
			'id_multidomain'	=> $id,
			'id_webpage'		=> $page,
		));

		if (!$model) {
			$model = new CoreMultidomainWebpage();
			$model->id_multidomain = $id;
			$model->id_webpage = $page;
			$model->save();
			$newStatus = 'ON';
		}
		else {
			$model->delete();
			$newStatus = 'OFF';
		}

		echo $newStatus;

	}


	/**
	 * Wizard related constants
	 * @var string
	 */
	const STEP_ORGCHART		= 'step_orgchart';
	const STEP_INITIAL 		= 'step_initial';
	const STEP_FINAL		= 'step_final';


	public function actionClientWizard() {

		$fromStep 					= Yii::app()->request->getParam('from_step', false);
		$nextButton 				= Yii::app()->request->getParam('next_button', false);
		$prevButton 				= Yii::app()->request->getParam('prev_button', false);
		$finishWizardBack 			= Yii::app()->request->getParam('finish_wizard_back', false);
		$finishWizardSetup 			= Yii::app()->request->getParam('finish_wizard_setup', false);
		$idForm						= Yii::app()->request->getParam('idForm', false);
		$noWizardNav				= Yii::app()->request->getParam('nowiznav', false);


		// Get client model, if any
		$idClient = Yii::app()->request->getParam("id", false);
		$model = CoreMultidomain::model()->findByPk($idClient);
		if (!$model) {
			$model = new CoreMultidomain();
		}


		// Just have all submitted data in this array
		$inData = $this->collectClientWizardData($_REQUEST);

		// Validate incoming step and go back to the same step if failed
		$valid = $this->validateStepData($inData, $fromStep, $model);
		if (!$valid) {
			$this->goClientWizardStep($fromStep, $inData, $model);
			Yii::app()->end();
		}

		// FINISH Wizard & Back to List requested
		if ($finishWizardBack) {
			$ok = $this->saveClient($inData, $model);
			if ($ok) {
				echo '<a class="auto-close success"></a>';
			}
			else {
				$this->goClientWizardStep(self::STEP_INITIAL, $inData, $model);
			}
			Yii::app()->end();
		}

		// FINISH Wizard & Go to Client Settings requested
		if ($finishWizardSetup) {
			$ok = $this->saveClient($inData, $model);
			if ($ok) {
				$redirectUrl = Docebo::createAdminUrl('MultidomainApp/main/settings', array('id' => $model->id));
				// Use commonly used Dialog2/JS content to redirect
				$this->renderPartial('lms.protected.views.site._js_redirect', array(
					'url'=>$redirectUrl,
					'showMessage' => true,
					'message'	=> Yii::t('standard', 'Redirecting') . "...",
				));
			}
			else {
				$this->goClientWizardStep(self::STEP_INITIAL, $inData, $model);
			}
			Yii::app()->end();
		}



		// Dispatch Wizard Steps: Depending on FROM STEP and PREV/NEXT button, render/execute the next dialog/action
		if ($fromStep == self::STEP_INITIAL) {
			$this->goClientWizardStep(self::STEP_ORGCHART, $inData, $model);
		}
		else if ($fromStep == self::STEP_ORGCHART && $nextButton) {
			$this->goClientWizardStep(self::STEP_FINAL, $inData, $model);
		}
		else if ($fromStep == self::STEP_ORGCHART && $prevButton) {
			$this->goClientWizardStep(self::STEP_INITIAL, $inData, $model);
		}
		else if ($fromStep == self::STEP_FINAL && $prevButton) {
			$this->goClientWizardStep(self::STEP_ORGCHART, $inData, $model);
		}
		else {
			$this->goClientWizardStep(self::STEP_INITIAL, $inData, $model);
		}


	}


	/**
	 * Render a grid column
	 * @param unknown $data
	 * @param unknown $index
	 * @return Ambigous <string, unknown>
	 */
	public function renderOrgChartColumn($data, $index) {

		$node = CoreOrgChartTree::model()->findByPk($data['org_chart']);
		$ocGroup = $node->idst_oc;
		$path = CoreOrgChartTree::getOcGroupPath($ocGroup, ' / ', false);

		$path = str_replace(' / ', ' / <strong>', $path) . "</strong>";

		$path = Docebo::ellipsis($path, 50);

		$html = $path;
		return $html;

	}

	/**
	 * Render a grid column
	 * @param unknown $data
	 * @param unknown $index
	 * @return Ambigous <string, unknown>
	 */
	public function renderUsersNumberColumn($data, $index) {
		$node = CoreOrgChartTree::model()->findByPk($data['org_chart']);
		$html = '-';
		if ($node) {
			$html = count($node->getBranchUsers());
		}
		return $html;

	}


	/**
	 * Render a grid column
	 * @param unknown $data
	 * @param unknown $index
	 * @return Ambigous <string, unknown>
	 */
	public function renderClientDomain($data, $index) {
		$model = CoreMultidomain::model()->findByPk($data['id']);
		$html = $model->getClientUri();
		return $html;

	}


	/**
	 *
	 */
	public function renderCompanyLogo($data, $index) {
		$logoUrl = CoreAsset::url($data['companyLogo'], CoreAsset::VARIANT_SMALL);
		if(!$logoUrl)
			$logoUrl = Docebo::getRootBaseUrl(true) . '/themes/spt/images/course/course_nologo.png';

		$html = CHtml::image($logoUrl, 'Logo', array(
			'width' => '60px',
		));
		return $html;
	}

	public function actionChangeLanguagePerDomain(){
		if(Yii::app()->request->isAjaxRequest) {
			$idDomain = Yii::app()->request->getParam('id');
			$langCode = Yii::app()->request->getParam('langcode');
			$domain = CoreMultidomain::model()->findByPk($idDomain);
			if (!$domain) {
				throw new Exception('not a valid domain');
			}

			$disabledLanguages = explode(',', $domain->disabled_languages);
			if (in_array($langCode, $disabledLanguages)) {
				$key = array_search($langCode, $disabledLanguages);
				unset($disabledLanguages[$key]);
				$disabledLanguages = array_filter($disabledLanguages);
				$domain->disabled_languages = implode(',', $disabledLanguages);
			}

			$domain->default_language = $langCode;
			$domain->save();
		}
	}

	public function actionToggleLanguagePerDomain(){
		if(Yii::app()->request->isAjaxRequest) {
			$idDomain = intval(Yii::app()->request->getParam('id'));
			$langCode = Yii::app()->request->getParam('langcode');
			$domain = CoreMultidomain::model()->findByPk($idDomain);
			if (!$domain) {
				throw new Exception('not a valid domain');
			}

			$disabledLanguages = explode(',', $domain->disabled_languages);
			if (!in_array($langCode, $disabledLanguages)) {
				$disabledLanguages[] = $langCode;
			} else {
				$key = array_search($langCode, $disabledLanguages);
				unset($disabledLanguages[$key]);
			}

			$disabledLanguages = array_filter($disabledLanguages);
			$domain->disabled_languages = implode(',', $disabledLanguages);
			$domain->save();
		}
	}

	/**
	 * Here we change the status of the language settings per domain
	 * Also here if the admin choose to activate the custom settings for the first time, we get the current language configuration
	 * and pass it to the current domain. This is happening only one time - at first activation of the custom settings
	 * All made changes are preserved for the current domain, even if the admin disables the custom setting
	 * That allows in later time, when admin reactivate the settings, to use the previously chosen, but not the current global LMS settings
	 */
	public function actionChangeState(){
		$status = intval(Yii::app()->request->getParam('status', 0));
		$domainId = intval(Yii::app()->request->getParam('id'));
		$domain = CoreMultidomain::model()->findByPk($domainId);
		if ($domain) {
			$domain->use_custom_settings = $status;

			// first time activation check
			if(empty($domain->default_language) && $status == 1){

				$activeLanguagesData = Lang::getLanguages();
				$allLanguagesData = Lang::getLanguages(false);
				$activeLanguages = array();
				$allLanguages = array();
				foreach($activeLanguagesData as $active) {
					$activeLanguages[] = $active['code'];
				}

				foreach($allLanguagesData as $all) {
					$allLanguages[] = $all['code'];
				}

				// get current global language settings and applies it to the current domain
				$inactiveLanguages = array_diff($allLanguages, $activeLanguages);
				$domain->default_language = Settings::get('default_language');
				$domain->disabled_languages = implode(',', $inactiveLanguages);
			}
			$domain->save();
			$this->sendJSON(array('success'));
		}
		$this->sendJSON(array('no domain found'));
	}

	public function actionMainSettings()
	{

		if (!empty($_POST['save_multidomain_main_settings'])) {
			if (isset($_POST['user_login_check'])) {
				Settings::save('user_login_check', $_POST['user_login_check']);
			}



			Yii::app()->user->setFlash('success', Yii::t('standard', '_OPERATION_SUCCESSFUL'));
		}

		$checkUserLogin = Settings::get('user_login_check', 0);


		$this->render('mainSettings', [
			'checkUserLogin' => $checkUserLogin
		]);
	}

	private static function convertBooleanToString($boolVal)
	{
		$strVal = 'off';
		if ($boolVal == 1) {
			$strVal = 'on';
		}
		return $strVal;
	}

	/**
	 * Collect all expected and known wizard data (from all possible steps)
	 *
	 * @param array $request
	 * @return array
	 */
	protected function collectClientWizardData($request) {
		$data = array();

		// INITIAL FORM (cleint name, domain etc.
		if (isset($request['Wizard']['client-step-initial-form']['CoreMultidomain'])) {
			$data = $request['Wizard']['client-step-initial-form']['CoreMultidomain'];
			switch ($data['domain_type']) {
				case CoreMultidomain::DOMAINTYPE_SUBFOLDER:
					$data['domain'] = trim($request['Wizard']['client-step-initial-form']['domain_subfolder']);
					break;
				case CoreMultidomain::DOMAINTYPE_SUBDOMAIN:
					$data['domain'] = trim($request['Wizard']['client-step-initial-form']['domain_subdomain']);
					break;
				case CoreMultidomain::DOMAINTYPE_CUSTOMDOMAIN:
					$data['domain'] = trim($request['Wizard']['client-step-initial-form']['domain_customdomain']);
					break;
			}
		}

		if(isset($request['Wizard']['client-step-initial-form']['theme'])){
		    $data['theme'] = $request['Wizard']['client-step-initial-form']['theme'];
        }

		// Add multidomain ID if exists (if it's edit action) in data array
		if(isset($request['id'])) {
			$data['id'] = $request['id'];
		}

		// ORGCHART selection (SINGLE orgchart selection is assumed!!!!)
		if (isset($request['Wizard']['client-step-orgchart-form'])) {
			$tmp = json_decode($request['Wizard']['client-step-orgchart-form']['orgcharts_selected-json'], true);
			if (count($tmp) > 0)
				$data['org_chart'] = $tmp[0]['key'];
		}

		return $data;
	}


	/**
	 * Validate && Sanitize user input from different steps.
	 *
	 * @param array $data Incoming (usually submitted) data
	 * @param string $fromStep Which step we are coming FROM ?
	 *
	 * @return boolean
	 */
	protected function validateStepData($data, $fromStep, CoreMultidomain $model) {
        $existingRecord = Yii::app()->db->createCommand()
            ->select('*')
            ->from('core_multidomain')
            ->where('name=:name OR (domain=:domain AND domain_type=:domain_type)', array(':name'=>$data['name'],
                ':domain'=>$data['domain'], ':domain_type'=>$data['domain_type']))->queryAll();

		switch ($fromStep) {
			case self::STEP_INITIAL:
				if (!$data['name']) {
					$error = Yii::t('multidomain', 'Please type a valid client name');
				}
                else if (!empty($existingRecord) && $model->id == null) {
                    $error = Yii::t('multidomain', 'Client name or domain name already exist');
                }
				else if (!$data['domain_type']) {
					$error = Yii::t('multidomain', 'Select domain type');
				}
				else if (!$data['domain']) {
					$error = Yii::t('multidomain', 'Type a valid domain or subfolder value');
				}
				else if ($data['domain_type'] == CoreMultidomain::DOMAINTYPE_SUBFOLDER &&  in_array($data['domain'], Docebo::getKnownAppNames())) {
					$error = Yii::t('multidomain', 'Invalid or not allowed subfolder');
				}

				// Check for valid custom domain
				if ($data['domain_type'] == CoreMultidomain::DOMAINTYPE_CUSTOMDOMAIN) {
					// Validate for "good" custom domain
					$domain = str_replace('www.','',$data['domain']);
					$valid = (
									preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $domain) 	//valid chars check
								&& 	preg_match("/^.{1,253}$/", $domain) 											// overall length check
								&& 	preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $domain)							// length of each domain level
							);

					// Check if the domain contains the bad word 'docebo' on its second level
					$valid &= !(stripos($domain, 'docebo') !== false);
					if (!$valid) {
						$error = Yii::t('multidomain', 'Type a valid custom domain');
					}
				}

				// Check for subdomain and subfolder for "good" strings
				if ($data['domain_type'] == CoreMultidomain::DOMAINTYPE_SUBDOMAIN) {
					if (!preg_match("/^[a-zA-Z0-9-]+$/", $data['domain'])) {
						$error = Yii::t('multidomain', 'Type a valid subdomain');
					}
					else if ($data['domain'] == 'www') {
						$error = Yii::t('multidomain', 'You cannot use "www" as a subdomain');
					}
				}
				if ($data['domain_type'] == CoreMultidomain::DOMAINTYPE_SUBFOLDER) {
					if (!preg_match("/^[a-zA-Z0-9-]+$/", $data['domain'])) {
						$error = Yii::t('multidomain', 'Type a valid subfolder');
					}
				}


				break;

			case self::STEP_ORGCHART:
				if (!$data['org_chart']) {
					$error = Yii::t('multidomain', 'Please select a branch');
				}
				else {
					//Check if branch is already assigned show error
					if(!$data['id']) {
						if(CoreMultidomain::model()->exists('org_chart=:org_chart', array(':org_chart' => $data['org_chart']))) {
							$error = Yii::t('multidomain', 'This branch is already assigned');
						}
					} else {
						if (CoreMultidomain::model()->exists('(org_chart=:org_chart) AND (id <> :id)', array(':org_chart' => $data['org_chart'], 'id' => $data['id']))) {
							$error = Yii::t('multidomain', 'This branch is already assigned');
						}
					}
				}
				break;

			case self::STEP_FINAL:
				break;

		}

		if ($error) {
			Yii::app()->user->setFlash('error', $error);
			return false;
		}

		return true;
	}


	/**
	 * Save client
	 *
	 * @param array $data
	 */
	protected function saveClient($data, CoreMultidomain $model) {

		try {
			// Get current model and save it, used to compare old/new model attributes later, if required
			$oldModel = clone $model;
			$model->attributes = $data;
			if ($model->validate()) {
				if ($this->updateLmsConfiguration($model, $oldModel)) {
					$model->save();
					if($data['theme']){
                        ThemeVariant::saveClientTheme($model->id, $data['theme']);
                    }
				}
			}
			return true;
		}
		catch (Exception $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			Yii::app()->user->setFlash('error', $e->getMessage());
			return false;
		}

	}

	/**
	 * RUN Wizard step
	 *
	 * @param string $step
	 */
	protected function goClientWizardStep($step, $data, CoreMultidomain $model) {


		$params = array();
		$params['model'] 		= $model;
		$params['wizardData'] 	= $data;

		switch ($step) {
			case self::STEP_INITIAL:
			    $params['availableThemes'] = ThemeVariant::getAvailableThemes();
			    $params['selectedTheme'] = ThemeVariant::getMultidomainTheme($model->id);
				$html = $this->renderPartial('_wizard_client_initial', $params, true);
				echo $html;
				Yii::app()->end();
				break;

			case self::STEP_ORGCHART:
				$preSelected = array();
				if ((int) $model->org_chart > 0)
					$preSelected = array(array('key' => $model->org_chart, 'selectState' => 1));
				$params['preSelected'] = $preSelected;
				$html = $this->renderPartial('_wizard_client_orgchart', $params, true, true);
				echo $html;
				Yii::app()->end();
				break;

			case self::STEP_FINAL:
				$html = $this->renderPartial('_wizard_client_final', $params, true);
				echo $html;
				Yii::app()->end();
				break;
		}

	}

	public function isMultilevelDocebosaasDomain() {
        $currentLmsDomain = trim(Docebo::getCurrentDomain(true), '/');
        if (preg_match("/\.docebosaas.com$/", strtolower($currentLmsDomain))) {
            return true;
        }

        return false;
    }

	/**
	 * Update client LMS configuration: Redis, File, etc.
	 *
	 * @param CoreMultidomain $model
	 * @param CoreMultidomain $oldModel
	 */
	protected function updateLmsConfiguration(CoreMultidomain $model, CoreMultidomain $oldModel) {

		// NEW/CUSTOM DOMAIN ?
		if (in_array($model->domain_type, array(CoreMultidomain::DOMAINTYPE_CUSTOMDOMAIN, CoreMultidomain::DOMAINTYPE_SUBDOMAIN))) {
			return MultidomainManager::updateClientDomain($model->getClientUri(), $oldModel->getClientUri());
		}

		return true;

	}

}