<?php
	/* @var $data CoreMultidomain */
	$deleteUrl 		= Docebo::createAdminUrl("//MultidomainApp/main/deleteClient", array('id' => $data['id']));
	$wizardUrl 		= Docebo::createAdminUrl('MultidomainApp/main/clientWizard', array('id' => $data['id']));
	
	$isActive 		= $data['active'] > 0;
	$setActiveValue = $isActive ? '0' : '1';
	$setActiveUrl 	= Docebo::createAdminUrl("//MultidomainApp/main/setActive", array('id' => $data['id'], 'active' => $setActiveValue));
	$activeColor 	= ($data['active']) ? "green" : "grey";
	
	$clientSettingsUrl = Docebo::createAdminUrl("//MultidomainApp/main/settings", array('id' => $data['id']));
	
?>

<a 
	href="<?= $setActiveUrl ?>" 
	class="change-active-status">
	<span class="i-sprite is-circle-check <?= $activeColor ?>"></span>
</a>&nbsp;&nbsp;


<a 
	href="<?= $clientSettingsUrl ?>">
	<span class="i-sprite is-gearpair"></span>
</a>&nbsp;&nbsp;


<a 
	href="<?= $wizardUrl ?>" 
	class="open-dialog"
	data-dialog-id 		= "edit-client-dialog" 
	data-dialog-class	= "edit-client-dialog"
	data-dialog-title	= "<?= Yii::t('standard', '_MOD') ?>"
	>
	<span class="i-sprite is-edit"></span>
</a>&nbsp;&nbsp;

<a href="<?php echo $deleteUrl; ?>"
	class="open-dialog"
	data-dialog-class="delete-item-modal"
	closeOnEscape="false"
	closeOnOverlayClick="false"
	data-dialog-id="delete-client-dialog"
	data-dialog-title	= "<?= Yii::t('standard', '_DEL') ?>"	
	>
	
	<span class="i-sprite is-remove red"></span>
</a>