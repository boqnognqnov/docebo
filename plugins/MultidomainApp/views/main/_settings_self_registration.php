<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'multidomain-self-registration-form',
    'method' => 'POST',
    'htmlOptions' => array(
        'enctype' 	=> 'multipart/form-data',
        'class' 	=> 'form-horizontal docebo-form'
    )
));
?>

<div class="row-fluid setting-row">
    <div class="span12">
        <div class="setting-name span3">
            <span class="setting-title"><?= Yii::t('multidomain', 'Custom settings') ?></span>
        </div>
        <div class="setting-value span9">
            <label class="checkbox">
                <?php
                echo $form->checkBox($model, 'enable_self_registration_settings', array('class' => 'on-off-switch', 'id' => 'enable_custom_settings'));
                echo $model->getAttributeLabel('enable_self_registration_settings');
                ?>
            </label>
        </div>
    </div>
</div>

<div class="blockable">
    <div class="row-fluid setting-row-wrapper odd">
        <div class="row-fluid setting-row">
            <div class="span12">
                <div class="setting-name span3">
                    <span class="setting-title"><?= Yii::t('configuration', '_REGISTER_TYPE') ?></span>
                </div>
                <div class="setting-value span9 registration_type">
                    <div class="row-fluid">
                        <?php echo $form->radioButtonList($model, 'register_type',
                            array(
                                CoreMultidomain::FREE_SELF_REGISTRATION_CODE => Yii::t('configuration', '_REGISTER_TYPE_SELF'),
                                CoreMultidomain::MODERATED_SELF_REGISTRATION_CODE => Yii::t('configuration', '_REGISTER_TYPE_MODERATE'),
                                CoreMultidomain::REGISTRATION_BY_ADMIN_CODE => Yii::t('configuration', '_REGISTER_TYPE_ADMIN'),
                            )); ?>
                        <div class="checkbox-item">
                            <br/>
                            <?php echo $form->checkbox($model, 'disable_registration_email_confirmation', array('uncheckValue' => 0, 'value' => 1)); ?>
                            <?php echo $form->labelEx($model, 'disable_registration_email_confirmation'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row-fluid setting-row allowed_domains">
        <div class="span12">
            <div class="setting-name span3">
                <span class="setting-title"><?= Yii::t('configok auration', 'Restrict domains'); ?></span>
                <p class="setting-description"><?= Yii::t('configuration', 'You can define the allowed domains that can self register in this client (e.g. @mycompany.com)') ?></p>
            </div>
            <div class="setting-value span9">
                <div class="row-fluid">
                    <div class="values allowed_domains_container">
                        <select multiple="multiple" id="allowed_domains" name="CoreMultidomain[allowed_domains][]">
                            <? if(count($model->allowed_domains) > 0): ?>
                                <? foreach($model->allowed_domains as $key=>$value): ?>
                                    <option selected="selected" class="selected" value="<?=$value?>"><?=$value?></option>
                                <? endforeach; ?>
                            <? endif; ?>
                        </select>
                    </div>
                    <?php echo $form->error($model, 'allowed_domains'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<br>
<div class="row-fluid">
    <div class="span12 text-right">
        <?php
        echo CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
            'name'	=> 'save_multidomain_settings',
            'class'	=> 'btn btn-docebo green big',
        ));
        ?>
        <?php
        echo CHtml::link(Yii::t('standard', '_CANCEL'), Docebo::createAdminUrl('MultidomainApp'), array(
            'name'	=> 'save_multidomain_settings',
            'class'	=> 'btn btn-docebo black big',
        ));
        ?>
    </div>
</div>
<?php
echo CHtml::hiddenField('selectedTab', $selectedTab, array('id' => 'selectedTab', 'class' => 'selectedTab'));
$this->endWidget();
?>
<script type="text/javascript">
	$(function(){
		$('select#allowed_domains').fcbkcomplete({
            width: '98.5%',
            addontab: false, // <==buggy
            newel: true,
            cache: false,
			addonblurtext: true,
            input_name: 'maininput-name',
            filter_selected: true,
            maxitems: 9999,
            placeholder: '<?=Yii::t('standard', 'Type here...')?>',
        });
	});
</script>

<style>
    div.facebook-auto{
        display: none !important;
    }
</style>
