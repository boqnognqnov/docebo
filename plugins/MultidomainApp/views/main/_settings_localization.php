<?php
/*
 * @var $model CoreMultidomain
 * */
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'multidomain-settings-localization',
	'method' => 'POST',
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
		'class' => 'ajax-grid-form',
		'data-grid' => '#lang-management-grid'
	)
));
?>
<div class="row-fluid setting-row" style="height: 50px">
	<div class="span12">
		<div class="setting-name span3">
			<span class="setting-title"><?= Yii::t('multidomain', 'Custom settings') ?></span>
		</div>
		<div class="setting-value span9">
			<label class="checkbox"
				   data-url="<?= Docebo::createAbsoluteUrl('MultidomainApp/main/changeState', array('id' => $model->id))?>">
				<?php
				echo $form->checkBox($model, 'use_custom_settings', array('class' => 'on-off-switch'));
				echo $model->getAttributeLabel('use_custom_settings');
				?>
			</label>
		</div>
	</div>
</div>

<div class="blockable" style="position:relative;">
	<div class="bottom-section clearfix">
		<div id="grid-wrapper">
			<?php
			$disabledLanguages = explode(',', $model->disabled_languages);

			$this->widget('zii.widgets.grid.CGridView', array(
				'id' => 'lang-management-grid',
				'htmlOptions' => array('class' => 'grid-view clearfix'),
				'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
				'dataProvider' => $langModel->dataProvider(),
				'template' => '{items}{summary}{pager}',
				'summaryText' => Yii::t('standard', '_TOTAL'),
				'pager' => array(
					'class' => 'DoceboCLinkPager',
					'maxButtonCount' => 8,
					'pageSize' => 50,
				),
				'ajaxUpdate' => 'all-items',
				'afterAjaxUpdate' => 'function(id, data){ $(\'a[rel="tooltip"]\').tooltip(); }',
				'columns' => array(
					'lang_code' => array(
						'header' => Yii::t('standard', '_LANGUAGE'),
						'type' => 'raw',
						'value' => '"<img src=\"".$data->getFlagSrc()."\" alt=\" \"> {$data->lang_code}"',
					),
					'lang_description',
					array(
						'name' => Yii::t('standard', 'Default'),
						'type' => 'raw',
						'value' => function ($data) use ($model) {
							if ($model->default_language == $data->lang_code) {
								return "<i class='i-sprite is-flag green' rel='tooltip' title='" . Yii::t("standard", "_DEFAULT_LANGUAGE") . "'></i>";
							} else {
								return CHtml::link("", Yii::app()->createUrl("MultidomainApp/main/changeLanguagePerDomain", array("langcode" => $data->lang_code, 'id' => $_REQUEST['id'])), array("class" => "i-sprite is-flag", "rel" => "tooltip", "title" => Yii::t('standard','Make default')));
							}
						}
					),
					array(
						'name' => '',
						'type' => 'raw',
						'value' => function ($data) use ($model, $disabledLanguages) {
							if ($data->lang_code == $model->default_language) {
								return '';
							}
							if (in_array($data->lang_code, $disabledLanguages)) {
								return CHtml::link("", Yii::app()->createUrl("MultidomainApp/main/toggleLanguagePerDomain", array("langcode" => $data->lang_code, 'id' => $_REQUEST['id'])), array("class" => "lang-status activate-action", "rel" => "tooltip", "title" => Yii::t("standard", "_ACTIVATE")));
							} else {
								return CHtml::link("", Yii::app()->createUrl("MultidomainApp/main/toggleLanguagePerDomain", array("langcode" => $data->lang_code, 'id' => $_REQUEST['id'])), array("class" => "lang-status suspend-action", "rel" => "tooltip", "title" => Yii::t("standard", "_DEACTIVATE")));
							}
						},
						'htmlOptions' => array('class' => 'button-column-single')
					),
				),
			)); ?>
		</div>
	</div>
	<div class="blockUI blockOverlay" style="z-index: 1000; border: none; margin: 0px; padding: 0px; width: 100%; height: 100%; top: 0px; left: 0px; opacity: 0.6; cursor: auto; position: absolute; background-color: rgb(255, 255, 255);"></div>
</div>

<?php
echo CHtml::hiddenField('selectedTab', $selectedTab, array('id' => 'selectedTab', 'class' => 'selectedTab'));
$this->endWidget();
?>
<script type="text/javascript">
	function toggleSettings(withAjax){
		withAjax = withAjax || true;
		var checkbox = $('#CoreMultidomain_use_custom_settings');
		var status = checkbox.is(':checked');
		if (status === true) status = 1;
		else status = 0;
		var url = $('#multidomain-settings-localization .setting-value.span9 label.checkbox').data('url');
		if(withAjax === false) {
			if (checkbox.is(':checked')) {
				$('#multidomain-settings-localization .blockUI.blockOverlay').hide();
			} else {
				$('#multidomain-settings-localization .blockUI.blockOverlay').show();
			}
		}
		if(withAjax == true) {
			$.ajax({
				type: 'post',
				url: url,
				data: {status: status}
			}).success(function(data){
				$('#lang-management-grid').yiiGridView('update');
				if(checkbox.is(':checked')){
					$('#multidomain-settings-localization .blockUI.blockOverlay').hide();
					status = 1;
				}else{
					$('#multidomain-settings-localization .blockUI.blockOverlay').show();
				}
			})
		}
	}
	$(function () {
		$(document).on('click', 'a.i-sprite.is-flag, a.lang-status', function (e) {
			e.preventDefault();
			var url = $(this).prop('href');
			$.ajax({
				type: 'post',
				url: url
			}).success(function (data) {
				$('#lang-management-grid').yiiGridView('update');
			})
		});

		$(document).on('change', '#CoreMultidomain_use_custom_settings', function(e){
			e.preventDefault();
			e.stopPropagation();
			toggleSettings();
		})
		toggleSettings(false);
	});
</script>

