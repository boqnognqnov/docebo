<?php
/**
 *  !!! N O T E !!! : This view is used in two places:
 *
 *  	Admin/Advanced Settings/Https Settings tab UI
 *  	plugins/MultidomainApp/Client/Https Settings tab UI
 *
 *
 *
 * @var $model CoreHttps
 * @var $form CActiveForm
 *
 */


	Yii::app()->clientScript->scriptMap['font-awesome.min.css'] = false;
	Yii::app()->clientScript->scriptMap['gamification.css'] = false;
	Yii::app()->clientScript->scriptMap['jquery.textPlaceholder.js'] = false;

	$removeCertificateUrl = Docebo::createAdminUrl('https/removeCertificate', array(
		'id'	=> $model->id,
	));

	$hasCsrPendingOrReady = $model->csrGenerationStatus;


	if ($hasCsrPendingOrReady) {
		$model->customCertificateOption = CoreHttps::CUSTOMCERT_NEEDS_CSR;
	}

?>
<div class="advanced-settings-https section docebo-form">
<h3>Https</h3>
<div class="row-fluid"><div class="span12">

	<?php
		$params = array();

		switch ($context) {
			case CoreHttps::CONTEXT_GENERAL_HTTPS:
				$route = 'advancedSettings/https';
				break;
			case CoreHttps::CONTEXT_MULTIDOMAIN_HTTPS:
				$route = '//MultidomainApp/main/https';
				$params['id'] = $model->idMultidomainClient;
				break;
		}
		$action = Docebo::createAdminUrl($route, $params);

		$downloadCsrUrl = Docebo::createAdminUrl('https/download', array('type' => CoreHttps::FILETYPE_CSR, 'id' => $model->id));
		$downloadKeyUrl = Docebo::createAdminUrl('https/download', array('type' => CoreHttps::FILETYPE_KEY, 'id' => $model->id));

		$form = $this->beginWidget('CActiveForm', array(
			'action'	=> $action,
			'id' 		=> 'settings-https-form',
		));

		echo $form->hiddenField($model, 'id');

		if ($skipCertUpload) {
		    echo CHtml::hiddenField("skipCertUpload", 1);
		}

	?>




		<div class="row-fluid">

			<div class="row-fluid setting-row-wrapper odd">
			<div class="span12 setting-row">

				<div class="setting-name span3">
					<span class="setting-title"><?= Yii::t('configuration', 'Activate Https') ?></span>
				</div>


				<div class="setting-value span9">
					<?php if ($context != CoreHttps::CONTEXT_MULTIDOMAIN_HTTPS) : ?>
						<label class="radio">
							<?php
								echo $form->radioButton($model, 'mode', array('value' => CoreHttps::MODE_ON_DOCEBO_WILDCARD_SSL, 'uncheckValue' => null));
								echo Yii::t('configuration', 'Yes, on {domain}', array('{domain}' => Docebo::getOriginalDomain()));
							?>
						</label>
					<?php endif; ?>
					<?php if ((PluginManager::isPluginActive('CustomdomainApp') && CoreSetting::get('custom_domain_url' , false) )|| $context == CoreHttps::CONTEXT_MULTIDOMAIN_HTTPS) : ?>
						<label class="radio">
							<?php
								echo $form->radioButton($model, 'mode', array('value' => CoreHttps::MODE_ON_CUSTOM_SSL, 'uncheckValue' => null));
								if ($context == CoreHttps::CONTEXT_GENERAL_HTTPS) {
									echo Yii::t('configuration', 'Yes, on {domain}', array('{domain}' => trim(Docebo::getCurrentDomain(true), '/')));
								}
								else {
									echo Yii::t('standard', '_YES');
								}
							?>
						</label>
					<?php endif; ?>
				</div>
			</div>
			</div>

		</div>


		<?php if (!$skipCertUpload) { ?>

		<div class="row-fluid custom-certificate-data">

			<div class="span12 setting-row">

				<div class="setting-name span3">
					<span class="setting-title"><?= Yii::t('player', 'Certificate') ?></span>
				</div>



				<?php if (!$model->sslStatus || $model->sslStatus  == CoreHttps::SSL_STATUS_NOT_INSTALLED || $model->sslStatus == CoreHttps::SSL_STATUS_ERROR) : ?>
					<div class="setting-value span9">

						<?php if ($model->sslStatus == CoreHttps::SSL_STATUS_ERROR) : ?>
							<div class="row-fluid">
								<div class="span12 ssl-pending-installation-error">
									<div class="alert alert-error">
										<?= $model->sslStatusLastError ?>
									</div>
								</div>
							</div>
							<br>
						<?php endif; ?>

						<div class="row-fluid">
							<div class="span12">
								<?= Yii::t('configuration', 'In order to activate Https protocol, <strong>you need an SSL certificate</strong>.') ?>
							</div>
						</div>

						<div class="row-fluid">
							<div class="span12">
								<label class="radio" for="customCertificateOption_1">
									<?php
										echo $form->radioButton($model, 'customCertificateOption', array(
											'value'			=> 	CoreHttps::CUSTOMCERT_OWN_UPLOAD,
											'uncheckValue' 	=> null,
											'id'			=> 'customCertificateOption_1'
										));
										echo Yii::t('configuration', 'I have my certificate and I want to upload it');
									?>
								</label>
							</div>
						</div>
						<div class="row-fluid">

							<div class="span12 customer-has-ssl">

								<div class="customer-has-ssl-to-upload">

									<?php if (Yii::app()->user->hasFlash('error')) : ?>
										<div class="row-fluid">
											<div class="span12">
									   			<div class='alert alert-error text-left'>
								    				<?= Yii::app()->user->getFlash('error'); ?>
								    			</div>
								    		</div>
								    	</div>
									<?php endif; ?>

									<?php $model->scenario = CoreHttps::SCENARIO_UPLOAD_OWN_CERT; ?>

									<div class="row-fluid">
											<div class="span3">
												<?= $form->labelEx($model, 'certFile') ?>
											</div>
											<div class="span9">
												<div class="">
													<?php echo $form->fileField($model, 'certFile'); ?>
													<?php echo $form->error($model, 'certFile'); ?>
													<p class="setting-description">
														<?= ''// description ?>
													</p>
												</div>
											</div>
									</div>
									<br>

									<div class="row-fluid">
											<div class="span3">
												<?= $form->labelEx($model, 'keyFile') ?>
											</div>
											<div class="span9">

													<div class="keyfile-already-set" style="display: none;">

														<span class="remove-key-file i-sprite is-remove red"></span> <strong><span class="key-file-name"><?= $model->keyFile ?></span></strong>
														<p class="setting-description">
															<?= Yii::t('configuration', 'We\'ve already uploaded this Key file as part of your CSR generation request. ') ?>
														</p>
													</div>

													<div class="keyfile-upload" style="display: none;">
														<?php echo $form->fileField($model, 'keyFile'); ?>
														<?php echo $form->error($model, 'keyFile'); ?>
														<p class="setting-description">
															<?= ''// description ?>
														</p>
													</div>

											</div>
									</div>
									<br>

									<div class="row-fluid">
											<div class="span3">
												<?= $form->labelEx($model, 'intermCaFile') ?>
											</div>
											<div class="span9">
												<div class="">
													<?php echo $form->fileField($model, 'intermCaFile'); ?>
													<?php echo $form->error($model, 'intermCaFile'); ?>
													<p class="setting-description">
														<?= '' ?>
													</p>
												</div>
											</div>
									</div>
									<br>


								</div>

							</div>
						</div>

						<div class="row-fluid">
							<div class="span12">
								<label class="radio" for="customCertificateOption_2">
									<?php
										echo $form->radioButton($model, 'customCertificateOption', array(
											'value'	=> 	CoreHttps::CUSTOMCERT_NEEDS_CSR,
											'uncheckValue' 	=> null,
											'id'	=> 'customCertificateOption_2'
										));
										echo Yii::t('configuration', 'I don\'t have any certificate and I need one');
									?>
								</label>
							</div>
						</div>

						<br>
						<div class="row-fluid">
							<div class="span12 customer-needs-csr">

								<?php if (Yii::app()->user->hasFlash('error_csr')) : ?>
									<div class="row-fluid">
										<div class="span12">
								   			<div class='alert alert-error text-left'>
							    				<?= Yii::app()->user->getFlash('error_csr'); ?>
							    			</div>
							    		</div>
							    	</div>
								<?php endif; ?>


								<?php if (!$model->csrGenerationStatus || $model->csrGenerationStatus == CoreHttps::CSRGEN_STATUS_ERROR) : ?>

									<div class="csr-form-data">
										<div class="row-fluid">
											<div class="span12">
												<?php
													$this->widget('common.widgets.InfoBox', array(
														'type' 		=> 'info',
														'text'		=> Yii::t('configuration','To activate Https you must buy an SSL Certificate from third party vendors. To do this, <strong>you must provide them a CSR file</strong> you can generate right here'),
													));
												?>
											</div>
										</div>
										<div class="row-fluid">
											<div class="span6">
												<?php $model->scenario = CoreHttps::SCENARIO_GENERATE_CSR; ?>
												<?= $form->labelEx($model, 'csrCountryName') ?>
												<?= $form->dropDownList($model, csrCountryName, CoreCountry::getCountryList(), array(
													'class'	=> 'span12',
												)) ?>
												<div class="setting-description"><?= '' ?></div>
											</div>
											<div class="span6">
												<?= $form->labelEx($model, 'csrOrgUnitName') ?>
												<?= $form->textField($model, 'csrOrgUnitName') ?>
												<div class="setting-description"><?= ''   ?></div>
											</div>
										</div>
										<div class="row-fluid">
											<div class="span6">
												<?= $form->labelEx($model, 'csrStateOrProvinceName') ?>
												<?= $form->textField($model, 'csrStateOrProvinceName') ?>
												<div class="setting-description"><?= ''   ?></div>
											</div>
											<div class="span6">
												<?= $form->labelEx($model, 'csrCommonName') ?>
												<?= $form->textField($model, 'csrCommonName') ?>
												<div class="setting-description"><?= ''   ?></div>
											</div>
										</div>
										<div class="row-fluid">
											<div class="span6">
												<?= $form->labelEx($model, 'csrLocalityName') ?>
												<?= $form->textField($model, 'csrLocalityName') ?>
												<div class="setting-description"><?= ''   ?></div>
											</div>
											<div class="span6">
												<?= $form->labelEx($model, 'csrEmail') ?>
												<?= $form->textField($model, 'csrEmail') ?>
												<div class="setting-description"><?= ''   ?></div>
											</div>
										</div>
										<div class="row-fluid">
											<div class="span6">
												<?= $form->labelEx($model, 'csrOrgName') ?>
												<?= $form->textField($model, 'csrOrgName') ?>
												<div class="setting-description"><?= ''   ?></div>
											</div>
											<div class="span6 text-right">
												<label>&nbsp;</label>
												<?php echo CHtml::submitButton(Yii::t('configuration', 'Generate CSR File'), array(
													'class' => 'btn-save',
													'name'	=> 'generate_csr_button',
												)); ?>
											</div>
										</div>
									</div>

								<?php elseif ($model->csrGenerationStatus == CoreHttps::CSRGEN_STATUS_PENDING_GENERATION || $model->csrGenerationStatus == CoreHttps::CSRGEN_STATUS_READY) : ?>

									<?php
										if ($model->csrGenerationStatus != CoreHttps::CSRGEN_STATUS_READY) {
											$style = "style='display: none;'";
										}
									?>
									<div class="customer-needs-csr-ready" <?= $style ?>>

										<div class="row-fluid">
											<div class="span12">
												<?php
													$this->widget('common.widgets.InfoBox', array(
														'type' 		=> 'info',
														'text'		=> Yii::t('configuration','If you have already bought your certificate, please select <strong>\'I have my certificate and I want to upload it\'</strong>'),
													));
												?>
											</div>
										</div>

										<br>

										<div class="row-fluid">


											<div class="span12 download-csr-generated-files">

												<?php if ($model->csrGenerationTime) : ?>
													<div class="row-fluid">
														<?= Yii::t('configuration', 'You already generated a CSR file on <strong>{date}</strong>', array('{date}' => Yii::app()->localtime->toLocalDate($model->csrGenerationTime))) ?>
													</div>
													<br>
												<?php endif;?>

												<div class="row-fluid">
													<div class="span4">
														<?= Chtml::link(Yii::t('configuration', 'Download CSR File'), $downloadCsrUrl, array(
															'class'	=> 'btn btn-save',
														)) ?>
													</div>
													<div class="span4">
														<?= Chtml::link(Yii::t('configuration', 'Download KEY File'), $downloadKeyUrl, array(
															'class'	=> 'btn btn-save',
														)) ?>
													</div>
													<div class="span4 text-right">
														<?= Chtml::submitButton(Yii::t('configuration', 'Generate New Request'), array(
															'class'	=> 'btn btn-cancel span12',
															'name'	=> 'generate_new_csr'
														)) ?>
													</div>
												</div>

												<div class="row-fluid">
													<div class="span4 setting-description">
														<?= Yii::t('configuration','Download this CSR file and use it to obtain SSL certificate from third party vendors') ?>
													</div>
													<div class="span4 setting-description">
														<?= Yii::t('configuration','It is strongly recommended to download also this Key file and keep it in a safe place') ?>
													</div>
													<div class="span4">
														 <p class="setting-description text-left" style="color: red;">
														 	<?= Yii::t('configuration','Warning: previously generated Key and CSR files will be lost!') ?>
														 </p>
													</div>
												</div>

											</div>

										</div>



									</div>


									<?php if ($model->csrGenerationStatus == CoreHttps::CSRGEN_STATUS_PENDING_GENERATION): ?>
										<div class="customer-needs-csr-pending-generation">
											<div class="row-fluid csr-pending-message">
												<div class="span12">
													<label>
														<span class="video-converting-animation"></span> <?= Yii::t('configuration', 'Your Certificate Request is being generated at the moment, please wait!') ?>
														<?= CHtml::link('Cancel', 'javascript:;', array(
															'id'	=> 'cancel-csrgen-pending-installation',
														)) ?>
													</label>
												</div>
											</div>
										</div>

										<div class="row-fluid">
											<div class="span12 csr-pending-generation-error" style="display: none;">
												<?= Yii::t('configuration', 'An error occured during status check') ?>
											</div>
										</div>
									<?php endif;?>

								<?php endif; ?>



							</div>
						</div>
					</div>


				<?php elseif (($model->sslStatus == CoreHttps::SSL_STATUS_PENDING_INSTALLATION) || ($model->sslStatus == CoreHttps::SSL_STATUS_INSTALLED)) : ?>

					<div class="setting-value span9">

						<?php
							if ($model->sslStatus != CoreHttps::SSL_STATUS_INSTALLED) {
								$style = "style='display: none;'";
							}
						?>

						<div class="customer-has-ssl-successfully-uploaded" <?= $style ?>>
							<div class="row-fluid">
								<div class="span8">
									<div class="btn-save nouppercase"><i class="i-sprite is-check white"></i> <?= Yii::t('configuration', 'Your certificate has been uploaded correctly') ?></div>
								</div>
								<div class="span4 text-right">
									<a
										href="<?= $removeCertificateUrl ?>"
										class="open-dialog btn btn-cancel"
										data-dialog-class="narrow"
										data-dialog-id="remove-certificate"
										data-dialog-title= "<?= Yii::t('standard', 'Remove SSL certificate') ?>"
									>
										<?= Yii::t('configuration', 'Remove certificate') ?>
									</a>
								</div>
							</div>

							<br>
							<div class="row-fluid">
								<div class="span8 text-center">
									<?= Yii::t('configuration', 'This certificate expire on <strong>{date}</strong>', array('{date}' => Yii::app()->localtime->toLocalDate($model->sslExpireTime))) ?>
								</div>
							</div>

						</div>

						<?php if ($model->sslStatus == CoreHttps::SSL_STATUS_PENDING_INSTALLATION) :  ?>
							<div class="customer-has-ssl-pending-installation">
								<div class="row-fluid ssl-pending-message">
									<div class="span12">
										<label>
											<span class="video-converting-animation"></span> <?= Yii::t('configuration', 'Your certificate is being installed at the moment, please wait!') ?>
											&nbsp;<?= CHtml::link('Cancel', 'javascript:;', array(
												'id'	=> 'cancel-ssl-pending-installation',
											)) ?>
										</label>
									</div>
								</div>
							</div>

							<div class="row-fluid">
								<div class="span12 ssl-pending-installation-error" style="display: none;">
									<?= Yii::t('configuration', 'An error occured during SSL installation status check') ?>
								</div>
							</div>
						<?php endif;?>

					</div>
				<?php endif; ?>

			</div>
		</div>

		<?php } ?>

		<div class="actions right-buttons">
			<?php
				echo CHtml::hiddenField('selectedTab', 'https');

				echo CHtml::submitButton(Yii::t('admin', 'Save'), array(
					'name'	=> 'save_multidomain_settings',
					'class'	=> 'btn btn-docebo green big',
				));

				if ($context == CoreHttps::CONTEXT_MULTIDOMAIN_HTTPS) {
					echo "&nbsp;";
					echo CHtml::link(Yii::t('standard', '_CANCEL'), Docebo::createAdminUrl('MultidomainApp'), array(
							'name'	=> 'save_multidomain_settings',
							'class'	=> 'btn btn-docebo black big',
					));
				}
			?>
		</div>

	<?php $this->endWidget(); ?>
</div>


</div></div>

<script type="text/javascript">

	var doPollSslInstallStatus 	= <?= json_encode($model->sslStatus == CoreHttps::SSL_STATUS_PENDING_INSTALLATION) ?>;
	var doPollCsrGenStatus 		= <?= json_encode($model->csrGenerationStatus == CoreHttps::CSRGEN_STATUS_PENDING_GENERATION) ?>;
	var keyFileAlreadySet		= <?= json_encode($model->keyFile && $model->keyFileOrigin == CoreHttps::KEYFILE_ORIGIN_CSRGEN) ?>;

	$(function(){
		var options = {
			sslStatusCheckUrl				: <?= json_encode(Docebo::createAdminUrl('https/checkSslInstallationStatus', array('id' => $model->id))) ?>,
			csrStatusCheckUrl				: <?= json_encode(Docebo::createAdminUrl('https/checkCsrStatus', array('id' => $model->id))) ?>,
			idHttps							: <?= json_encode($model->id) ?>,
			keyFileAlreadySet				: keyFileAlreadySet,
			httpsControllerSettingsRoute	: <?= json_encode(Docebo::createAdminUrl('https/settings')) ?>
		};

		if (!AdvancedHttps)
			var AdvancedHttps = new AdvancedHttpsClass(options);

		$('.advanced-settings-https input').styler({browseText: "<?= Yii::t('organization', 'Upload File') ?>"});

		if(doPollSslInstallStatus) {
			AdvancedHttps.pollSslInstallStatus();
		}

		if (doPollCsrGenStatus) {
			AdvancedHttps.pollCsrGenStatus();
		}

	});


</script>
