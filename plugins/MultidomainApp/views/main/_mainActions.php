<div class="main-actions clearfix">
	<ul class="clearfix">
		<li>
			<div>
				<a href="<?= Docebo::createAdminUrl('MultidomainApp/main/clientWizard') ?>"
				    alt="<?= Yii::t('multidomain', 'Create new client') ?>"
					class="open-dialog p-hover"
					data-dialog-id 		= '<?= $editClientDialogId ?>'
					data-dialog-class 	= '<?= $editClientDialogId ?>'
					data-dialog-title	= "<?= Yii::t('multidomain', 'New Client') ?>"
					>
					<span class="p-sprite plus-user-large"></span> <?= Yii::t('multidomain', 'New Client') ?>
				</a>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>
			<p></p>
		</div>
	</div>
</div>
