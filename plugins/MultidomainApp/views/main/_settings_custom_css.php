				<?php 
					$form = $this->beginWidget('CActiveForm', array(
						'id' => 'multidomain-settings-form',
						'method' => 'POST',
						'htmlOptions' => array(
							'enctype' 	=> 'multipart/form-data',
							'class' 	=> 'form-horizontal docebo-form'
						)
					));
				?>
			
				<div class="row-fluid setting-row">
					<div class="span12">
						<div class="setting-name span3">
							<p class="setting-description"><?= Yii::t('templatemanager', '_CUSTOM_CSS_DESCRIPTION') ?></p>
						</div>
						<div class="setting-value span9">
							<?php
								echo $form->textArea($model, 'customCss', array('class' => 'span12', 'rows' => 30)); 
							?>
						</div>
					</div>
				</div>
				
				<br>
				<div class="row-fluid">
					<div class="span12 text-right">
						<?php
							echo CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
								'name'	=> 'save_multidomain_settings',
								'class'	=> 'btn btn-docebo green big',		
							)); 
						?>
						<?php
						echo CHtml::link(Yii::t('standard', '_CANCEL'), Docebo::createAdminUrl('MultidomainApp'), array(
							'name'	=> 'save_multidomain_settings',
							'class'	=> 'btn btn-docebo black big',
						));
						?>
					</div>
				</div>
				<?php
					echo CHtml::hiddenField('selectedTab', $selectedTab, array('id' => 'selectedTab', 'class' => 'selectedTab')); 
					$this->endWidget(); 
				?>
