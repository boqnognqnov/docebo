<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'multidomain-settings-form',
    'method' => 'POST',
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
        'class' => 'form-horizontal docebo-form'
    )
));
?>

<div class="row-fluid setting-row">
    <div class="span12">
        <div class="setting-name span3">
            <span class="setting-title"><?= Yii::t('multidomain', 'Custom settings') ?></span>
        </div>
        <div class="setting-value span9">
            <label class="checkbox">
                <?php
                echo $form->checkBox($model, 'ecommerceEnabled', array('class' => 'on-off-switch'));
                echo $model->getAttributeLabel('ecommerceEnabled');
                ?>
            </label>
        </div>
    </div>
</div>


<div class="blockable">


    <div class="row-fluid setting-row-wrapper odd">
        <div class="row-fluid setting-row">
            <div class="span12">
                <div class="setting-name span3">
                    <span class="setting-title"><?= Yii::t('configuration', '_CURRENCY_SYMBOL') ?></span>
                </div>
                <div class="setting-value span9">
                    <?php
                    echo $form->textField($model, 'currencySymbol', array('class' => 'span12'));
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid setting-row">
        <div class="span12">
            <div class="setting-name span3">
                <span class="setting-title"><?= Yii::t('standard', 'Currency') ?></span>
            </div>
            <div class="setting-value span9">
                <?php
                echo $form->dropDownList($model, 'currency', $model->getCurrencyCodes(), array(
                    'class' => ''
                ))
                ?>
                <span class="settings">
						<p class="description"
                           style="margin-left: 0px;margin-top: 5px;"><?= Yii::t('configuration', 'Authorize.net account currency setting must match the currency selected from this dropdown.'); ?></p>
					</span>
            </div>
        </div>
    </div>

    <div class="row-fluid setting-row-wrapper odd">
        <div class="row-fluid setting-row">
            <div class="span12">
                <div class="setting-name span3">
                    <span class="setting-title">Paypal</span>
                </div>
                <div class="setting-value span9">

                    <div class="row-fluid">
                        <label class="checkbox">
                            <?php
                            echo $form->checkBox($model, 'payPalEnabled', array());
                            echo $model->getAttributeLabel('payPalEnabled');
                            ?>
                        </label>
                    </div>

                    <div class="row-fluid">
                        <?php
                        echo $model->getAttributeLabel('payPalAccount');
                        echo $form->textField($model, 'payPalAccount', array('class' => 'span12'));
                        ?>
                    </div>
                    <div class="row-fluid">
                        <br>
                        <label class="checkbox">
                            <?php
                            echo $form->checkBox($model, 'payPalSandbox', array());
                            echo $model->getAttributeLabel('payPalSandbox');
                            ?>
                        </label>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid setting-row">
        <div class="span12">
            <div class="setting-name span3">
                <span class="setting-title">Authorize.net</span>
            </div>
            <div class="setting-value span9">

                <div class="row-fluid">
                    <label class="checkbox">
                        <?php
                        echo $form->checkBox($model, 'authorizeNetEnabled', array());
                        echo $model->getAttributeLabel('payPalEnabled');
                        ?>
                    </label>
                </div>
                <div class="row-fluid">
                    <?php
                    echo $model->getAttributeLabel('authorizeNetLoginId');
                    echo $form->textField($model, 'authorizeNetLoginId', array('class' => 'span12'));
                    ?>

                </div>
                <div class="row-fluid">
                    <br>
                    <?php
                    echo $model->getAttributeLabel('authorizeNetTransactionKey');
                    echo $form->textField($model, 'authorizeNetTransactionKey', array('class' => 'span12'));
                    ?>
                </div>
                <div class="row-fluid">
                    <br>
                    <?php
                    echo $model->getAttributeLabel('authorizeNetMD5Hash');
                    echo $form->textField($model, 'authorizeNetMD5Hash', array('class' => 'span12'));
                    ?>
                </div>
                <div class="row-fluid">
                    <br>
                    <label class="checkbox">
                        <?php
                        echo $form->checkBox($model, 'authorizeNetSandbox', array());
                        echo $model->getAttributeLabel('authorizeNetSandbox');
                        ?>
                    </label>
                </div>

            </div>
        </div>
    </div>

    <!-- Adyen -->
    <div class="row-fluid setting-row odd">
        <div class="span12">
            <div class="setting-name span3">
                <span class="setting-title">Adyen</span>
            </div>
            <div class="setting-value span9">

                <div class="row-fluid">
                    <label class="checkbox">
                        <?php
                        echo $form->checkBox($model, 'adyen_enabled', array());
                        echo $model->getAttributeLabel('adyen_enabled');
                        ?>
                    </label>
                </div>
                <div class="row-fluid">
                    <?php
                    echo $model->getAttributeLabel('adyen_merchant_account');
                    echo $form->textField($model, 'adyen_merchant_account', array('class' => 'span12'));
                    ?>

                </div>
                <div class="row-fluid">
                    <br>
                    <?php
                    echo $model->getAttributeLabel('adyen_hmac_key');
                    echo $form->textField($model, 'adyen_hmac_key', array('class' => 'span12'));
                    ?>
                </div>
                <div class="row-fluid">
                    <br>
                    <?php
                    echo $model->getAttributeLabel('adyen_skin_code');
                    echo $form->textField($model, 'adyen_skin_code', array('class' => 'span12'));
                    ?>
                </div>
                <div class="row-fluid">
                    <br>
                    <label class="checkbox">
                        <?php
                        echo $form->checkBox($model, 'adyen_single_page_hpp', array());
                        echo $model->getAttributeLabel('adyen_single_page_hpp');
                        ?>
                    </label>
                </div>
                <div class="row-fluid">
                    <br>
                    <label class="checkbox">
                        <?php
                        echo $form->checkBox($model, 'adyen_sandbox', array());
                        echo $model->getAttributeLabel('adyen_sandbox');
                        ?>
                    </label>
                </div>
            </div>
        </div>
    </div>

    <!-- Stripe -->
    <div class="row-fluid setting-row">
        <div class="span12">
            <div class="setting-name span3">
                <span class="setting-title">Stripe</span>
            </div>
            <div class="setting-value span9">

                <div class="row-fluid">
                    <label class="checkbox">
                        <?php
                        echo $form->checkBox($model, 'stripe_enabled', array());
                        echo $model->getAttributeLabel('stripe_enabled');
                        ?>
                    </label>
                </div>

                <div class="row-fluid">
                    <p><strong><?=Yii::t('configuration', 'Payment type')?></strong></p>
                    <div class="multidomain_stripe_type">
                        <?php
                        //---Fix: we need this field to be "simple" or "advanced", must be initialized somehow---
                        if ($model->stripe_type != 'simple' && $model->stripe_type != 'advanced') {
                            $model->stripe_type = 'simple'; //default value
                        }
                        ?>
                        <label>
                            <?php
                            echo CHtml::radioButton('CoreMultidomain[stripe_type]', $model->stripe_type == 'simple', array(
                                'class' => 'stripe_payment_type',
                                'value' => 'simple',
                                'id' => 'stripe-radio-input-type-simple'
                            ));
                            //echo $form->radioButton($model, 'stripe_type', array('class' => 'stripe_payment_type', 'value' => 'simple', 'id' => 'stripe-radio-input-type-1'));
                            echo Yii::t('configuration', 'Simple charge');
                            ?>
                        </label>
                    </div>
                    <div class="multidomain_stripe_type">
                        <label>
                            <?php
                            echo CHtml::radioButton('CoreMultidomain[stripe_type]', $model->stripe_type == 'advanced', array(
                                'class' => 'stripe_payment_type',
                                'value' => 'advanced',
                                'id' => 'stripe-radio-input-type-advanced'
                            ));
                            //echo $form->radioButton($model, 'stripe_type', array('class' => 'stripe_payment_type', 'value' => 'advanced', 'id' => 'stripe-radio-input-type-2'));
                            echo Yii::t('configuration', 'Stripe order');
                            ?>
                            <span class="multidomain_stripe_advanced_text"><?=Yii::t('configuration', 'Allows you to automatically collect taxes. For more information, {LS}click here{LE}.', array(
                                '{LS}' => '<a href="https://stripe.com/docs/orders/tax-integration" target="_blank">',
                                '{LE}' => '</a>'
                            ))?></span>
                        </label>
                        <div class="row-fluid" id="stripe_tax_code_container" style="display: <?=($model->stripe_type == "advanced")?"block":"none"?>;">
                            <?php
                            echo $model->getAttributeLabel('stripe_tax_code');
                            echo $form->textField($model, 'stripe_tax_code', array('class' => 'span8'));
                            ?>
                        </div>
                    </div>
                </div>

                <div class="row-fluid">
                    <br />
                    <label class="checkbox">
                        <?php
                        echo $form->checkBox($model, 'stripe_alipay_enabled', array());
                        echo $model->getAttributeLabel('stripe_alipay_enabled');
                        ?>
                    </label>
                </div>

                <div class="row-fluid">
                    <?php
                    echo $model->getAttributeLabel('stripe_account_email');
                    echo $form->textField($model, 'stripe_account_email', array('class' => 'span12'));
                    ?>
                </div>
                <br/>

                <div class="row-fluid">
                    <?php
                    echo $model->getAttributeLabel('stripe_private_key');
                    echo $form->passwordField($model, 'stripe_private_key', array('class' => 'span12'));
                    ?>
                </div>
                <br/>

                <div class="row-fluid">
                    <?php
                    echo $model->getAttributeLabel('stripe_public_key');
                    echo $form->textField($model, 'stripe_public_key', array('class' => 'span12'));
                    ?>
                </div>
                <br/>

                <div class="row-fluid">
                    <label class="checkbox">
                        <?php
                        echo $form->checkBox($model, 'stripe_sandbox', array());
                        echo $model->getAttributeLabel('stripe_sandbox');
                        ?>
                    </label>
                </div>

                <div class="stripe-sandbox-controls">
                    <br/>
                    <div class="row-fluid">
                        <?php
                        echo $model->getAttributeLabel('stripe_private_key_test');
                        echo $form->passwordField($model, 'stripe_private_key_test', array('class' => 'span12'));
                        ?>
                    </div>
                    <br/>

                    <div class="row-fluid">
                        <?php
                        echo $model->getAttributeLabel('stripe_public_key_test');
                        echo $form->textField($model, 'stripe_public_key_test', array('class' => 'span12'));
                        ?>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Cybersource-->
    <div class="row-fluid setting-row odd">
        <div class="span12">
            <div class="setting-name span3">
                <span class="setting-title">CyberSource</span>
            </div>
            <div class="setting-value span9">

                <div class="row-fluid">
                    <label class="checkbox">
                        <?php
                        echo $form->checkBox($model, 'cybersource_enabled', array());
                        echo $model->getAttributeLabel('cybersource_enabled');
                        ?>
                    </label>
                </div>

                <div class="row-fluid">
                    <?php
                    echo $model->getAttributeLabel('cybersource_access_key');
                    echo $form->passwordField($model, 'cybersource_access_key', array('class' => 'span12'));
                    ?>
                </div>
                <br/>

                <div class="row-fluid">
                    <?php
                    echo $model->getAttributeLabel('cybersource_secret_key');
                    echo $form->passwordField($model, 'cybersource_secret_key', array('class' => 'span12'));
                    ?>
                </div>
                <br/>

                <div class="row-fluid">
                    <?php
                    echo $model->getAttributeLabel('cybersource_profile_id');
                    echo $form->textField($model, 'cybersource_profile_id', array('class' => 'span12'));
                    ?>
                </div>
                <br/>

                <div class="row-fluid">
                    <label class="checkbox">
                        <?php
                        echo $form->checkBox($model, 'cybersource_sandbox_mode', array());
                        echo $model->getAttributeLabel('cybersource_sandbox_mode');
                        ?>
                    </label>
                </div>

            </div>
        </div>
    </div>

</div>

<br>
<div class="row-fluid">
    <div class="span12 text-right">
        <?php
        echo CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
            'name' => 'save_multidomain_settings',
            'class' => 'btn btn-docebo green big',
        ));
        ?>
        <?php
        echo CHtml::link(Yii::t('standard', '_CANCEL'), Docebo::createAdminUrl('MultidomainApp'), array(
            'name' => 'save_multidomain_settings',
            'class' => 'btn btn-docebo black big',
        ));
        ?>
    </div>
</div>
<?php
echo CHtml::hiddenField('selectedTab', $selectedTab, array('id' => 'selectedTab', 'class' => 'selectedTab'));
$this->endWidget();
?>
<script type="text/javascript">

    var stripeChecks = function() {
        $('.stripe_payment_type').on('change', function () {
            var paymentType = $('.stripe_payment_type:checked').val();
            console.log(paymentType);
            if (paymentType == "advanced") {
                $('#stripe_tax_code_container').css('display', 'block');
            } else {
                $('#stripe_tax_code_container').css('display', 'none');
            }
        });
        $('#CoreMultidomain_stripe_sandbox').on('change', function () {
            var sandboxActive = $('#CoreMultidomain_stripe_sandbox-styler').hasClass('checked');
            if (sandboxActive) {
                $('.stripe-sandbox-controls').css('display', 'block');
            } else {
                $('.stripe-sandbox-controls').css('display', 'none');
            }
        });
    }

    $(document).on('ready', function(){
        stripeChecks();
    });

</script>