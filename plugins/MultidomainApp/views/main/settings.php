<style>
	.final-tabs .tab-content{
		overflow: visible;
		margin-left: 200px;
	}
	.row-fluid.setting-row{
		display: inline-block;
	}

</style>
<?php DoceboUI::printFlashMessages()?>
<?php
/**
 * @var $model CoreMultidomain
 * @var $form CActiveForm
 *
 *
 * NOTE: Every tab pane has its own FORM!!
 */


	$this->breadcrumbs = array(
		Yii::t('adminrules', '_ADMIN_MENU'),
		Yii::t('multidomain', 'Multidomain') => Docebo::createAdminUrl('MultidomainApp'),
		$model->name,
	);

	// Make an array of tabs (groups) having any error inside
	$tabsWithErrors = array();
	if ($model->hasErrors()) {
		foreach ($model->errors as $attribute => $errors) {
			$tabsWithErrors[] = CoreMultidomain::getAttributeGroup($attribute);
		}
	}


?>

<?php $this->renderPartial('_settings_header', array('model' => $model, 'active_theme' => $active_theme)); ?>
<?php DoceboUI::printFlashMessages(); ?>
<div class="final-tabs multidomain-client-settings">

	<div class="row-fluid">

	    <ul class="nav nav-tabs">
            <?php if(isset($active_theme['code']) && $active_theme['code'] == ThemeVariant::LEGACY_THEME_CODE): ?>
            <li class="active <?= in_array(CoreMultidomain::GROUPSETTING_LOGO_AND_COLORS, $tabsWithErrors) ? 'has-error' : '' ?>">
                <a href="#" class="" data-toggle="tab" data-target="<?= '#' . CoreMultidomain::GROUPSETTING_LOGO_AND_COLORS ?>">
                    <i class="ft-icon ft-logo"></i>
                    <?= Yii::t('multidomain', 'Logo &amp; Colors') ?>
                </a>
            </li>

            <li class="<?= in_array(CoreMultidomain::GROUPSETTING_SIGNIN, $tabsWithErrors) ? 'has-error' : '' ?>">
                <a href="#" data-toggle="tab" data-target="<?= '#' . CoreMultidomain::GROUPSETTING_SIGNIN ?>">
                    <i class="ft-icon ft-home"></i>
                    <?= Yii::t('templatemanager', 'Sign In Page') ?>
                </a>
            </li>

            <li class="<?= in_array(CoreMultidomain::GROUPSETTING_CUSTIOM_CSS, $tabsWithErrors) ? 'has-error' : '' ?>">
                <a href="#" data-toggle="tab" data-target="<?= '#' . CoreMultidomain::GROUPSETTING_CUSTIOM_CSS ?>">
                    <i class="ft-icon ft-design"></i>
                    <?= Yii::t('templatemanager', 'Customize css') ?>
                </a>
            </li>

            <li class="<?= in_array(CoreMultidomain::GROUPSETTING_COURSE_PLAYER, $tabsWithErrors) ? 'has-error' : '' ?>">
                <a href="#" data-toggle="tab" data-target="<?= '#' . CoreMultidomain::GROUPSETTING_COURSE_PLAYER ?>">
                    <i class="ft-icon ft-player"></i>
                    <?= Yii::t('templatemanager', 'Course Player') ?>
                </a>
            </li>
            <?php endif; ?>
	    	<?php if (PluginManager::isPluginActive('WhitelabelApp')) :  ?>
				<li class="<?= $active_theme['code'] != ThemeVariant::LEGACY_THEME_CODE ? 'active ' : '' ?> <?= in_array(CoreMultidomain::GROUPSETTING_WHITE_LABEL, $tabsWithErrors) ? 'has-error' : '' ?>">
		            <a href="#" data-toggle="tab" data-target="<?= '#' . CoreMultidomain::GROUPSETTING_WHITE_LABEL ?>">
		            	<i class="ft-icon ft-whitelabel"></i>
		                <?= Yii::t('templatemanager', 'White Label') ?>
		            </a>
		    	</li>
	    	<?php endif; ?>


			<?php if ($model->allowHttps()) : ?>
	    		<li class="<?php echo (in_array(CoreMultidomain::GROUPSETTING_HTTPS, $tabsWithErrors) ? 'has-error' : ''); ?>">
	            	<a href="#" data-toggle="tab" data-target="<?php echo '#' . CoreMultidomain::GROUPSETTING_HTTPS; ?>">
	            		<i class="ft-icon ft-locker"></i>
	                	Https
	            	</a>
	    		</li>
	    	<?php endif; ?>

			<?php if (PluginManager::isPluginActive('CoursecatalogApp')) :  ?>
				<li class="<?= in_array(CoreMultidomain::GROUPSETTING_COURSECATALOG, $tabsWithErrors) ? 'has-error' : '' ?>">
					<a href="#" data-toggle="tab" data-target="<?= '#' . CoreMultidomain::GROUPSETTING_COURSECATALOG ?>">
						<i class="admin-ico catalog"></i>
						<?= Yii::t('standard' ,'Catalog') ?>
					</a>
				</li>
			<?php endif; ?>

	    	<?php if (PluginManager::isPluginActive('EcommerceApp')) :  ?>
				<li class="<?= in_array(CoreMultidomain::GROUPSETTING_ECOMMERCE, $tabsWithErrors) ? 'has-error' : '' ?>">
		            <a href="#" data-toggle="tab" data-target="<?= '#' . CoreMultidomain::GROUPSETTING_ECOMMERCE ?>">
		            	<i class="ft-icon ft-basket"></i>
		                <?= Yii::t('configuration', '_ECOMMERCE') ?>
		            </a>
		    	</li>
	    	<?php endif; ?>

			<li class="<?= in_array(CoreMultidomain::GROUPSETTING_SELF_REGISTRATION, $tabsWithErrors) ? 'has-error' : '' ?>">
				<a href="#" data-toggle="tab" data-target="<?= '#' . CoreMultidomain::GROUPSETTING_SELF_REGISTRATION ?>">
					<i class="ft-icon ft-register"></i>
					<?= Yii::t('admin_directory', '_DIRECOTRY_SELFREGISTERED'); ?>
				</a>
			</li>

	    	<?php
				// Raise event to let plugins add new tabs
				Yii::app()->event->raise('RenderDomainBrandingTabs', new DEvent($this, array()));
			?>

			<li class="<?= in_array(CoreMultidomain::GROUPSETTING_LOCALIZATION, $tabsWithErrors) ? 'has-error' : '' ?>">
				<a href="#" data-toggle="tab" data-target="<?= '#' . CoreMultidomain::GROUPSETTING_LOCALIZATION ?>">
					<i class="fa fa-globe fa-lg"></i>
					<?= Yii::t('adminrules', '_LANG_SETTING'); ?>
				</a>
			</li>



			<?php if (PluginManager::isPluginActive('SubscriptionsApp')) :  ?>
				<li class="<?= in_array(CoreMultidomain::GROUPSETTING_SUBSCRIPTION_SETTINGS, $tabsWithErrors) ? 'has-error' : '' ?>">
					<a href="#" data-toggle="tab" data-target="<?= '#' . CoreMultidomain::GROUPSETTING_SUBSCRIPTION_SETTINGS ?>">
						<i class="fa fa-pencil-square-o " style="font-size: 15px; margin-left: 5px"></i>
						<?= Yii::t('configuration', 'Subscription settings') ?>
					</a>
				</li>
			<?php endif; ?>

		</ul>


		<div class="tab-content">
            <?php if(isset($active_theme['code']) && $active_theme['code'] == ThemeVariant::LEGACY_THEME_CODE): ?>
            <!-- LOG & COLORS -->
            <div class="tab-pane active" id="<?= CoreMultidomain::GROUPSETTING_LOGO_AND_COLORS ?>">
                <h1><?= Yii::t('multidomain', 'Logo &amp; Colors') ?></h1>
                <?php $this->renderPartial('_settings_logo_and_colors', array(
                    'form' => $form,
                    'model' => $model,
                    'defaultLanguage' => $defaultLanguage,
                    'languages' => $languages,
                    'languagesValues' => $languagesValues,
                )); ?>
            </div>



            <!-- SIGN IN PAGE -->
            <div class="tab-pane" id="<?= CoreMultidomain::GROUPSETTING_SIGNIN ?>">
                <h1><?= Yii::t('templatemanager', 'Sign In Page') ?></h1>
                <?php $this->renderPartial('_settings_signin', array('form' => $form, 'model' => $model)); ?>
            </div>



            <!-- CUSTOM CSS -->
            <div class="tab-pane" id="<?= CoreMultidomain::GROUPSETTING_CUSTIOM_CSS ?>">
                <h1><?= Yii::t('templatemanager', 'Customize css') ?></h1>
                <?php $this->renderPartial('_settings_custom_css', array('form' => $form, 'model' => $model)); ?>
            </div>


            <!-- COURSE PLAYER -->
            <div class="tab-pane" id="<?= CoreMultidomain::GROUPSETTING_COURSE_PLAYER ?>">
                <h1><?= Yii::t('templatemanager', 'Course Player') ?></h1>
                <?php $this->renderPartial('_settings_course_player', array('form' => $form, 'model' => $model)); ?>
            </div>
            <?php endif; ?>
			<!-- WHITE LABEL -->
			<?php if (PluginManager::isPluginActive('WhitelabelApp')) :  ?>
				<div class="tab-pane <?= $active_theme['code'] != ThemeVariant::LEGACY_THEME_CODE ? 'active ' : '' ?>" id="<?= CoreMultidomain::GROUPSETTING_WHITE_LABEL ?>">
					<h1><?= Yii::t('templatemanager', 'White Label') ?></h1>
					<?php $this->renderPartial('_settings_white_label',
							array(
									'form' => $form,
									'model' => $model,
									'defaultLanguage' => $defaultLanguage,
									'languages' => $languages,
									'domainFooterTexts' => $domainFooterTexts,
									'domainFooterUrls' => $domainFooterUrls,
									'domainHeaderUrls' => $domainHeaderUrls,
									'languagesForFooterText' => $languagesForFooterText,
									'languagesForFooterUrl' => $languagesForFooterUrl,
									'countValuesForUrl' => $countValuesForUrl,
									'languagesForUrlHeader' => $languagesForUrlHeader,
									'countValuesForUrlHeader' => $countValuesForUrlHeader,
							)); ?>
				</div>
			<?php endif; ?>


			<!-- HTTPS -->
			<?php if ($model->allowHttps()) : ?>
				<div class="tab-pane" id="<?= CoreMultidomain::GROUPSETTING_HTTPS ?>">
				</div>
			<?php endif; ?>

			<!-- E-COMMERCE -->
			<?php if (PluginManager::isPluginActive('EcommerceApp')) :  ?>
				<div class="tab-pane" id="<?= CoreMultidomain::GROUPSETTING_ECOMMERCE ?>">
					<h1><?= Yii::t('configuration', '_ECOMMERCE') ?></h1>
					<?php $this->renderPartial('_settings_ecommerce', array('form' => $form, 'model' => $model)); ?>
				</div>
			<?php endif; ?>

			<?php
				// Raise event to let plugins add new tabs
				Yii::app()->event->raise('RenderDomainBrandingTabsContent', new DEvent($this, array(
					'id_org'						=> $model->org_chart, 	// Branch, associated to the Multidomain client
					'multidomain_client_id' 		=> $model->id,			// Multidomain Client ID
					'multidomain_client_model' 		=> $model,  			// Just in case, pass also CoreMultidomain model
				)));
			?>

			<!-- CourseCatalog App -->
			<?php if (PluginManager::isPluginActive('CoursecatalogApp')) :  ?>
				<div class="tab-pane" id="<?= CoreMultidomain::GROUPSETTING_COURSECATALOG ?>">
					<h1><?= Yii::t('standard' ,'Catalog') ?></h1>

					<?php $this->renderPartial('_settings_coursecatalog', array('form' => $form, 'model' => $model, 'countSelectedCatalogs' => $countSelectedCatalogs, 'active_theme' => $active_theme)); ?>

				</div>
			<?php endif; ?>

			<!-- SELF REGISTRATION -->
			<div class="tab-pane" id="<?= CoreMultidomain::GROUPSETTING_SELF_REGISTRATION ?>">
				<h1><?= Yii::t('admin_directory', '_DIRECOTRY_SELFREGISTERED'); ?></h1>
				<?php $this->renderPartial('_settings_self_registration', array('form' => $form, 'model' => $model)); ?>
			</div>

			<!-- LOCALIZATION -->
			<div class="tab-pane" id="<?= CoreMultidomain::GROUPSETTING_LOCALIZATION ?>">
				<h1><?= Yii::t('adminrules', '_LANG_SETTING'); ?></h1>
				<?php $this->renderPartial('_settings_localization', array('form' => $form, 'model' => $model, 'langModel' => $langModel)); ?>
			</div>

			<?php if (PluginManager::isPluginActive('SubscriptionsApp')) :  ?>
			<div class="tab-pane" id="<?= CoreMultidomain::GROUPSETTING_SUBSCRIPTION_SETTINGS ?>">
				<h1><?= Yii::t('configuration', 'Subscription settings'); ?></h1>
				<?php $this->renderPartial('_settings_subscription',['subscriptionSettings'=>$subscriptionSettingsData]); ?>
			</div>
            <?php endif; ?>

		</div>

	</div>	<!-- row-fluid -->

</div>

<script type="text/javascript">
	// Create Wizard object (themes/spt/js/jwizard.js)
	var jwizard = new jwizardClass({
		dialogId		: 'edit-client-dialog',
		alwaysPostGlobal: true,
		debug			: false
	});

	$(function(){

		var options = {
			ui						: 'settings',
			initialFormSettings		: <?= json_encode($model->getAttributes()) ?>,
			selectedTab				: <?= json_encode($selectedTab) ?>,
			gridUrl					: <?= json_encode(Docebo::createAdminUrl('//MultidomainApp/main/index')) ?>
		};
		var MultidomainMan = new MultidomainManClass(options);
		window['multidomainman'] = MultidomainMan;
		var httpsUrl = <?= json_encode(Docebo::createAdminUrl('//MultidomainApp/main/https', array('id' => $model->id))) ?>;
		$.get(httpsUrl, function(res){
			if (res && res.success) {
				$('#<?= CoreMultidomain::GROUPSETTING_HTTPS ?>').html(res.html);
				$(document).controls();
			}
		});
	});

	var oImageManagePanel = false;
</script>
