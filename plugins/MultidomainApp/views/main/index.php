<?php
	/* @var $model CoreMultidomain */
	$this->breadcrumbs = array(
		Yii::t('menu', 'Admin'),
		Yii::t('multidomain', 'Multidomain'),
	);
	
?>

<h2><?= Yii::t('multidomain', 'Multidomain') ?></h2>
<br/>

<?php
 	$this->renderPartial('_mainActions', array(
 		'editClientDialogId' => $editClientDialogId,
 	));
?>


<div class="clearfix">
	<?php $this->widget('common.widgets.ComboGridView', $gridParams); ?>
	
</div>


<script type="text/javascript">
/*<![CDATA[*/
    
	// Create Wizard object (themes/spt/js/jwizard.js)
	var jwizard = new jwizardClass({
		dialogId		: '<?= $editClientDialogId ?>',
		alwaysPostGlobal: true,
		debug			: false
	});

	var options = {
		editClientDialogId					: <?= json_encode($editClientDialogId) ?>,
		multidomainGridId					: <?= json_encode($gridParams['gridId']) ?>,
		massActionUrl						: <?= json_encode($massActionUrl) ?>		
	};           
	var MultidomainMan = new MultidomainManClass(options);

	
/*]]>*/
</script>

