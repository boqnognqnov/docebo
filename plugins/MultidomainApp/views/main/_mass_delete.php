<style>
<!--

	.modal.mass-action-dialog {
		width: 600px;
		margin-left: -300px;
	} 

-->
</style>


<?php 
	$form = $this->beginWidget('CActiveForm', array(
    	'id' => 'delete-certification-form',
    	'method' => 'POST',
    	'htmlOptions' => array(
        	'class' => 'ajax'
    	),
	));
?>

<?= Chtml::hiddenField('selection', implode(',', $selection)) ?>
<?= Chtml::hiddenField('action', 'delete') ?>

<div class="row-fluid">
	<div class="span12">
		<label class="checkbox">
			<?= CHtml::checkBox('agree_delete', false, array('value' => 'agree')) ?>
			<?= Yii::t('multidomain', 'I agree to delete the selected client(s) and I understand this operation <strong>cannot be undone!</strong>') ?>
		</label>
	</div>
</div>


<div class="form-actions">
    <input class="btn-docebo green big" type="submit" name="confirm_delete" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" />
    <input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>

<?php $this->endWidget(); ?>


<script type="text/javascript">
	$('input[name="agree_delete"]').styler();
</script>