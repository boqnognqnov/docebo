				<?php 
					$form = $this->beginWidget('CActiveForm', array(
						'id' => 'multidomain-settings-form',
						'method' => 'POST',
						'htmlOptions' => array(
							'enctype' 	=> 'multipart/form-data',
							'class' 	=> 'form-horizontal docebo-form'
						)
					));
				?>
			
			
				<div class="row-fluid setting-row">
					<div class="span12">
						<div class="setting-name span3">
							<span class="setting-title"><?= Yii::t('multidomain', 'Custom settings') ?></span>
						</div>
						<div class="setting-value span9">
							<label class="checkbox">
								<?php
									echo $form->checkBox($model, 'coursePlayerEnabled', array('class' => 'on-off-switch'));
									echo $model->getAttributeLabel('coursePlayerEnabled');
								?>
							</label>
						</div>
					</div>
				</div>
				
				
				<div class="blockable">
				
					<div class="row-fluid setting-row-wrapper odd">
					<div class="row-fluid setting-row">
						<div class="span12">
							<div class="setting-name span3">
								<span class="setting-title"><?= Yii::t('standard', 'Training Material View') ?></span>
								<p class="setting-description"><?= Yii::t('branding', 'Choose the layout to to display your course\'s training material') ?></p>
							</div>
							<div class="setting-value span9">
								
								<div class="row-fluid">
									<div class="span6">
										<label class="radio">
											<?=	$form->radioButton($model, 'coursePlayerViewLayout', array('value'=>LearningCourse::$PLAYER_LAYOUT_PLAY_BUTTON, 'uncheckValue'=>NULL))?>
											<?=Yii::t('branding', 'Player view')?>
										</label>
										<div class="player-layout-preview player"></div>
									</div>
									
									<div class="span6">
										<label class="radio">
											<?=	$form->radioButton($model, 'coursePlayerViewLayout', array('value'=>LearningCourse::$PLAYER_LAYOUT_LISTVIEW, 'uncheckValue'=>NULL))?>
											<?=Yii::t('branding', 'List view')?>
										</label>
										<div class="player-layout-preview listview"></div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					</div>
					
					<div class="row-fluid setting-row">
						<div class="span12">
							<div class="setting-name span3">
								<span class="setting-title"><?= Yii::t('certificate', '_BACK_IMAGE') ?></span>
								<p class="setting-description"><?= Yii::t('branding', 'Upload or select a default image for the course player background') ?></p>
							</div>
							<div class="setting-value span9">
								<div class="row-fluid">
									<div class="span6 text-center">
										<!-- Image selector -->
										<?php 
											$this->widget('common.widgets.ImageSelector', array(
												'imageType'			=> CoreAsset::TYPE_PLAYER_BACKGROUND,
												'assetId' 			=> $model->coursePlayerImage,
												'imgVariant'		=> CoreAsset::VARIANT_ORIGINAL, 
												'buttonText'		=> Yii::t('setup', 'Upload your background'),
												'buttonClass'		=> 'btn btn-docebo green big',
												'dialogId'			=> 'image-selector-modal-player-bg',
												'dialogClass'		=> 'image-selector-modal',
												'inputHtmlOptions'	=> array(
													'name'	=> 'CoreMultidomain[coursePlayerImage]',
													'id'	=> 'player-bg-image-id',
													'class'	=> 'image-selector-input',
												),
											));
										?>
										<!-- Image selector -->
									</div>
									<div class="span6">
										<br>
										<label class="radio">
											<?php
												echo $form->radioButton($model, 'coursePlayerImageAspect', array('value' => CoreMultidomain::IMAGE_ASPECT_FILL, 'uncheckValue' => null));
												echo Yii::t('branding', '_HOME_IMG_POSITION_FILL', array('{w}'=>1045, '{h}'=>411));
											?>
										</label>
										<label class="radio">
											<?php
												echo $form->radioButton($model, 'coursePlayerImageAspect', array('value' => CoreMultidomain::IMAGE_ASPECT_TILE, 'uncheckValue' => null));
												echo Yii::t('branding', '_HOME_IMG_POSITION_TILE');
											?>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row-fluid setting-row-wrapper odd">
					<div class="row-fluid setting-row">
						<div class="span12">
							<div class="setting-name span3">
								<span class="setting-title"><?= Yii::t('branding', 'HTML page CSS') ?></span>
								<p class="setting-description"><?= Yii::t('branding', 'HTML page CSS help') ?></p>
							</div>
							<div class="setting-value span9">
								<?php
									echo $form->textArea($model, 'coursePlayerHtmlCss', array('class' => 'span12', 'rows' => 20)); 
								?>
							</div>
						</div>
					</div>
					</div>
				
				</div>

				
				<br>
				<div class="row-fluid">
					<div class="span12 text-right">
						<?php
							echo CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
								'name'	=> 'save_multidomain_settings',
								'class'	=> 'btn btn-docebo green big',		
							)); 
						?>
						<?php
						echo CHtml::link(Yii::t('standard', '_CANCEL'), Docebo::createAdminUrl('MultidomainApp'), array(
							'name'	=> 'save_multidomain_settings',
							'class'	=> 'btn btn-docebo black big',
						));
						?>
					</div>
				</div>
				<?php
					echo CHtml::hiddenField('selectedTab', $selectedTab, array('id' => 'selectedTab', 'class' => 'selectedTab')); 
					$this->endWidget(); 
				?>
