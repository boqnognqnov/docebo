<style>
	.orgChart_languages{
		margin-bottom: 10px;
	}
</style>


<input type="hidden" id="languages" value="<?=(implode(',', array_keys($languages)))?>">
					<?php $modelErrors = CHtml::errorSummary($model); /* $form->errorSummary($model); */ ?>
					<?php if($modelErrors) { ?>
						<div class="messages">
							<?php echo $modelErrors; ?>
						</div>
					<?php } ?>

					<?php
						$form = $this->beginWidget('CActiveForm', array(
							'id' => 'multidomain-settings-form',
							'method' => 'POST',
							'htmlOptions' => array(
								'enctype' 	=> 'multipart/form-data',
								'class' 	=> 'form-horizontal docebo-form'
							)
						));
					?>			
				

					<div class="row-fluid setting-row">
						<div class="span12">
							<div class="setting-name span3">
								<span class="setting-title"><?= Yii::t('multidomain', 'Custom settings') ?></span>
							</div>
							<div class="setting-value span9">
								<label class="checkbox">
									<?php
										echo $form->checkBox($model, 'whiteLabelEnabled', array('class' => 'on-off-switch'));
										echo $model->getAttributeLabel('whiteLabelEnabled');
									?>
								</label>
							</div>
						</div>

					</div>
					
					<div class="blockable">
					
						<div class="row-fluid setting-row-wrapper odd">
						<div class="row-fluid setting-row">
							<div class="span12">
								<div class="setting-name span3">
									<p class="setting-description"><?= Yii::t('branding', 'Hide some Docebo\'s custom branding elements from your Docebo E-Learning Platform') ?></p>
								</div>
								<div class="setting-value span9">
									<span class="wlabel-preview wl-menu"><?=Yii::t('adminrules', '_ADMIN_MENU')?></span>
									<span class="wlabel-preview wl-user-counter"><?=Yii::t('templatemanager', 'User Counter')?></span>
									<span class="wlabel-preview wl-footer"><?=Yii::t('templatemanager', 'Foooter')?></span>
									<div class="white-label-preview"></div>
								</div>
							</div>
						</div>
						</div>

						<div class="row-fluid setting-row">
							<div class="span12">
								<div class="setting-name span3">
									<span class="setting-title"><?php echo Yii::t('templatemanager', 'Header'); ?></span>
								</div>

								<div class="setting-value span9">
									<label class="radio">
										<?php
										echo $form->radioButton($model, 'whiteLabelHeader', array('value' => 'current_header', 'uncheckValue' => null, 'id' => 'whiteLabelHeader0'));
										echo Yii::t('templatemanager', 'Use current header');
										?>
									</label>
									<label class="radio">
										<?php
										echo $form->radioButton($model, 'whiteLabelHeader', array('value' => 'external_header', 'uncheckValue' => null, 'id' => 'whiteLabelHeader1'));
										echo Yii::t('templatemanager', 'Load an external custom header (iframe)');
										?>
									</label>

									<div class="max-input show-hide-header-url">
										<div class="orgChart_languages">
											<div>
												<strong><?= Yii::t('branding', '_PAGE_LANGUAGE_LABEL') ?></strong>
												<?=CHtml::dropDownList('customUrlsMultidomainHeader', $defaultLanguage, $languagesForUrlHeader)?>
											</div>
											<div>
												<?= Yii::t('admin_lang', '_LANG_ALL') . ': <span>' . count($languagesForUrlHeader) . '</span>'; ?>
												<div id="orgChart_assignedCount"><?php echo Yii::t('standard', 'Filled languages') . ' <span>' . $countValuesForUrlHeader . '</span>'; ?></div>
											</div>
										</div>
										<?php echo $form->textField($model, 'whiteLabelHeaderExtUrl', array()) ?>
										<br />
										<div class="max-input-footnote"><?=Yii::t('templatemanager', 'Write here the external URL for the custom header'); ?></div>
										<br />
										<div class="row-fluid">
											<div class="span4"><?=Yii::t('templatemanager', 'Header') ?> <?=Yii::t('templatemanager', 'maximum width') ?></div>
											<div class="span8"><?=Yii::t('templatemanager', 'Header') ?> <?=Yii::t('templatemanager', 'height') ?></div>
										</div>
										<div class="row-fluid">
											<div class="span4">1100px</div>
											<div class="span8"><?php echo $form->textField($model, 'whiteLabelHeaderExtHeight',
													array('style' => "width: 80px", 'min' => 50, 'max' => 500)) ?> px</div>
										</div>
										<div class="row-fluid">
											<div class="span4"><?=Yii::t('templatemanager', 'Width is responsive'); ?></div>
											<div class="span8"><?=Yii::t('templatemanager', 'Min height') ?>: 50px</div>
										</div>
										<?php
										foreach ($domainHeaderUrls as $lang => $item) { ?>
											<input type="hidden" name="valuesForUrlMultiHeader[<?=$lang?>]" value="<?=$item?>" />
										<?php } ?>
									</div>
								</div>
							</div>
						</div>

						<div class="row-fluid setting-row-wrapper odd">
							<div class="row-fluid setting-row">
								<div class="span12">
									<div class="setting-name span3">
										<span class="setting-title"><?= Yii::t('templatemanager', 'Foooter') ?></span>
									</div>
									<div class="setting-value span9">
										<label class="radio">
											<?php
												echo $form->radioButton($model, 'whiteLabelPoweredByOption', array('value' => 'show_docebo', 'uncheckValue' => null, 'id' => 'whiteLabelPoweredByOption0'));
												echo Yii::t('branding', 'Show "Powered by Docebo" in the footer', array('{year}'=>date('Y')));
											?>
										</label>
										<label class="radio">
											<?php
												echo $form->radioButton($model, 'whiteLabelPoweredByOption', array('value' => 'hide_docebo', 'uncheckValue' => null, 'id' => 'whiteLabelPoweredByOption1'));
												echo Yii::t('branding', 'Hide "Powered by Docebo" in the footer', array('{year}'=>date('Y')));
											?>
										</label>
										<label class="radio">
											<?php
												echo $form->radioButton($model, 'whiteLabelPoweredByOption', array('value' => 'custom_text', 'uncheckValue' => null, 'id' => 'whiteLabelPoweredByOption2'));
												echo Yii::t('branding', 'Use customized text in the footer');
											?>
										</label>

										<div class="max-input show-hide-footer-custom-text">
											<div class="orgChart_languages">
												<div>
													<strong><?= Yii::t('branding', '_PAGE_LANGUAGE_LABEL') ?></strong>
													<?=CHtml::dropDownList('customtextMultidomain', $defaultLanguage, $languagesForFooterText)?>
												</div>
												<div>
													<?= Yii::t('admin_lang', '_LANG_ALL') . ': <span>' . count($languagesForFooterText) . '</span>'; ?>
													<div id="orgChart_assignedCount"><?php echo Yii::t('standard', 'Filled languages') . ' <span>' . count($domainFooterTexts) . '</span>'; ?></div>
												</div>
											</div>
											<?=CHtml::textArea('textareaCustomText')?>
											<br>
											<br>
										</div>

										<label class="radio">
											<?php
											echo $form->radioButton($model, 'whiteLabelPoweredByOption', array('value' => 'custom_url', 'uncheckValue' => null, 'id' => 'whiteLabelPoweredByOption4'));
											echo Yii::t('templatemanager', 'Load an external custom footer (iframe)');
											?>
										</label>

										<div class="max-input show-hide-footer-url">
											<div class="orgChart_languages">
												<div>
													<strong><?= Yii::t('branding', '_PAGE_LANGUAGE_LABEL') ?></strong>
													<?=CHtml::dropDownList('languageForUrlMulti', $defaultLanguage, $languagesForFooterUrl)?>
												</div>
												<div>
													<?= Yii::t('admin_lang', '_LANG_ALL') . ': <span>' . count($languagesForFooterUrl) . '</span>'; ?>
													<div id="orgChart_assignedCount"><?php echo Yii::t('standard', 'Filled languages') . ' <span>' . $countValuesForUrl . '</span>'; ?></div>
												</div>
											</div>
											<br>
											<?= $form->textField($model, 'whiteLabelFooterExtUrl', array()) ?>
											<br />
											<div class="max-input-footnote"><?=Yii::t('templatemanager', 'Write here the external URL for the custom footer'); ?></div>
											<br />
											<div class="row-fluid">
												<div class="span4"><?=Yii::t('templatemanager', 'Foooter') ?> <?=Yii::t('templatemanager', 'maximum width') ?></div>
												<div class="span8"><?=Yii::t('templatemanager', 'Foooter') ?> <?=Yii::t('templatemanager', 'height') ?></div>
											</div>
											<div class="row-fluid">
												<div class="span4">1100px</div>
												<div class="span8"><?php echo $form->textField($model, 'whiteLabelFooterExtHeight',
														array('style' => "width: 80px", 'min' => 30, 'max' => 500)) ?> px</div>
											</div>
											<div class="row-fluid">
												<div class="span4"><?=Yii::t('templatemanager', 'Width is responsive'); ?></div>
												<div class="span8"><?=Yii::t('templatemanager', 'Min height') ?>: 30px</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="row-fluid setting-row">
							<div class="span12">
								<div class="setting-name span3">
									<span class="setting-title"><?= Yii::t('standard', 'Contact us') ?></span>
								</div>
								<div class="setting-value span9">
									
									
									<label class="radio">
										<?php
											echo $form->radioButton($model, 'whiteLabelContactOption', array('value' => 'to_docebo', 'uncheckValue' => null, 'id' => 'whiteLabelContactOption0'));
											echo Yii::t('branding', 'Send the request to Docebo');
										?>
									</label>
									<label class="radio">
										<?php
											echo $form->radioButton($model, 'whiteLabelContactOption', array('value' => 'to_email', 'uncheckValue' => null, 'id' => 'whiteLabelContactOption1'));
											echo Yii::t('branding', 'Send the requests to a specific email address');
										?>
									</label>
									
									<div class="max-input show-hide-helpdesk-email">
										<?php echo $form->emailField($model, 'whiteLabelContactCustomEmail', array()) ?>
									</div>
									
								</div>
							</div>
						</div>

						<div class="row-fluid setting-row-wrapper odd">
							<div class="row-fluid setting-row">
								<div class="span12">
									<div class="setting-name span3">
										<span class="setting-title"><?= Yii::t('templatemanager', 'Naming') ?></span>
									</div>
									<div class="setting-value span9">
										<label class="checkbox">
											<?php
												echo $form->checkBox($model, 'replaceDoceboWordOption', array());
												echo Yii::t('branding', 'Replace, in every page of your E-Learning platform, the word <b>Docebo</b> with a custom word');
											?>
										</label>
										<div class="max-input show-hide-naming-input">
											<label class="checkbox">
												<?php
													echo $form->checkBox($model, 'whiteLabelNamingOverride', array());
													echo Yii::t('branding', 'Do not replace the text in translations containing hyperlinks');
												?>
											</label>

												<?php echo $form->textField($model, 'replaceDoceboCustomWord', array()); ?>
										</div>

										<br>
										<label class="checkbox">
											<?php
												echo $form->checkbox($model, 'whitelabelNamingSiteEnable', array('id' => 'CoreMultidomain_whitelabelNamingSiteEnable'));
												echo Yii::t('branding', 'Replace, where used, the website link <b>www.docebo.com</b> with a custom URL');
											?>
										</label>
										<div class="max-input show-hide-naming-site-input">
											<?php echo $form->textField($model, 'replaceDoceboCustomLink', array()) ?>
										</div>

									</div>
								</div>
							</div>
						</div>

					
					</div>
					
					<br>
					<div class="row-fluid">
						<div class="span12 text-right">
							<?php
								echo CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
									'name'	=> 'save_multidomain_settings',
									'class'	=> 'btn btn-docebo green big',		
								)); 
							?>
							<?php
							echo CHtml::link(Yii::t('standard', '_CANCEL'), Docebo::createAdminUrl('MultidomainApp'), array(
								'name'	=> 'save_multidomain_settings',
								'class'	=> 'btn btn-docebo black big',
							));
							?>
						</div>
					</div>
					<?php
						echo CHtml::hiddenField('selectedTab', $selectedTab, array('id' => 'selectedTab', 'class' => 'selectedTab')); 
						$this->endWidget();
					?>

					<script type="text/javascript">
						/*<![CDATA[*/

//						var options = {};
/* 							editClientDialogId					: <?= json_encode($editClientDialogId) ?>,
							multidomainGridId					: <?= json_encode($gridParams['gridId']) ?>,
							massActionUrl						: <?= json_encode($massActionUrl) ?>
						}; */
//						var MultidomainMan = new MultidomainManClass(options);


						var translationsListMulti = <?=json_encode($domainFooterTexts, JSON_HEX_APOS)?>;

						var translationsListForURLMulti = JSON.parse('<?=json_encode($domainFooterUrls)?>');
						var languages = $('#languages').val().split(',');

						(function ($) {
							$(function () {
								// Remember the header/footer url user set/get from db
								var initialHeaderUrl = $('#CoreMultidomain_whiteLabelHeaderExtUrl').val();
								initialHeaderUrl = initialHeaderUrl.replace(' ', '');
								if (initialHeaderUrl == '') { initialHeaderUrl = 'http://'; }
//								var initialFooterUrl = $('#CoreMultidomain_whiteLabelFooterExtUrl').val();
//								initialFooterUrl = initialFooterUrl.replace(' ', '');
//								if (initialFooterUrl == '') { initialFooterUrl = 'http://'; }

								if($('#whiteLabelHeader1').is(':checked')) {
									$('.show-hide-header-url').show();
									if($('#CoreMultidomain_whiteLabelHeaderExtUrl').val() == '') {
										if (initialHeaderUrl == '') { initialHeaderUrl = 'http://'; }
										$('#CoreMultidomain_whiteLabelHeaderExtUrl').val(initialHeaderUrl);//'http://');
									}

									if($('#CoreMultidomain_whiteLabelHeaderExtHeight').val() == '' || $('#CoreMultidomain_whiteLabelHeaderExtHeight').val() == 0) {
										$('#CoreMultidomain_whiteLabelHeaderExtHeight').val(50);
									}
								} else {
									$('.show-hide-header-url').hide();
									initialHeaderUrl = $('#CoreMultidomain_whiteLabelHeaderExtUrl').val();
									$('#CoreMultidomain_whiteLabelHeaderExtUrl').val('');
								}

								// Show/Hide set max-input show-hide-header-url on change
								$(document).on('change', 'input[name=\'CoreMultidomain[whiteLabelHeader]\']', function(){
									if($('#whiteLabelHeader1').is(':checked')) {
										$('.show-hide-header-url').show();
										if($('#CoreMultidomain_whiteLabelHeaderExtUrl').val() == '') {
											if (initialHeaderUrl == '') { initialHeaderUrl = 'http://'; }
											$('#CoreMultidomain_whiteLabelHeaderExtUrl').val(initialHeaderUrl);//'http://');
										}

										if($('#CoreMultidomain_whiteLabelHeaderExtHeight').val() == '' || $('#CoreMultidomain_whiteLabelHeaderExtHeight').val() == 0) {
											$('#CoreMultidomain_whiteLabelHeaderExtHeight').val(50);
										}
									} else {
										$('.show-hide-header-url').hide();
										initialHeaderUrl = $('#CoreMultidomain_whiteLabelHeaderExtUrl').val();
										$('#CoreMultidomain_whiteLabelHeaderExtUrl').val('');
									}
								});

								// Show/hide footer custom url area #whiteLabelPoweredByOption4
								if($('#whiteLabelPoweredByOption4').is(':checked')) {
									$('.show-hide-footer-url').show();
//									if($('#CoreMultidomain_whiteLabelFooterExtUrl').val() == '') {
//										if (initialFooterUrl == '') { initialFooterUrl = 'http://'; }
//										$('#CoreMultidomain_whiteLabelFooterExtUrl').val(initialFooterUrl);//'http://');
//									}

									if($('#CoreMultidomain_whiteLabelFooterExtHeight').val() == '' || $('#CoreMultidomain_whiteLabelFooterExtHeight').val() == 0) {
										$('#CoreMultidomain_whiteLabelFooterExtHeight').val(50);
									}
								} else {
									$('.show-hide-footer-url').hide();
//									if ($('#CoreMultidomain_whiteLabelFooterExtUrl').val() != '') {
//										initialFooterUrl = $('#CoreMultidomain_whiteLabelFooterExtUrl').val();
//									}

									$('#CoreMultidomain_whiteLabelFooterExtUrl').val('');
								}

								// Show/Hide set max-input show-hide-footer-url on change
								$(document).on('change', 'input[name=\'CoreMultidomain[whiteLabelPoweredByOption]\']', function(){
									if($('#whiteLabelPoweredByOption4').is(':checked')) {
										$('.show-hide-footer-url').show();
//										if($('#CoreMultidomain_whiteLabelFooterExtUrl').val() == '') {
//											if (initialFooterUrl == '') { initialFooterUrl = 'http://'; }
//											$('#CoreMultidomain_whiteLabelFooterExtUrl').val(initialFooterUrl);//'http://');
//										}

										if($('#CoreMultidomain_whiteLabelFooterExtHeight').val() == '' || $('#CoreMultidomain_whiteLabelFooterExtHeight').val() == 0) {
											$('#CoreMultidomain_whiteLabelFooterExtHeight').val(30);
										}
									} else {
										$('.show-hide-footer-url').hide();
//										if ($('#CoreMultidomain_whiteLabelFooterExtUrl').val() != '') {
//											initialFooterUrl = $('#CoreMultidomain_whiteLabelFooterExtUrl').val();
//										}

										$('#CoreMultidomain_whiteLabelFooterExtUrl').val('');
									}
								});

								// Footer custom text .show-hide-footer-custom-text
								if($('#whiteLabelPoweredByOption2').is(':checked')) {
									$('.show-hide-footer-custom-text').show();
								} else {
									$('.show-hide-footer-custom-text').hide();
								}

								// Show/Hide Footer custom text .show-hide-footer-custom-text
								$(document).on('change', 'input[name=\'CoreMultidomain[whiteLabelPoweredByOption]\']', function(){
									if($('#whiteLabelPoweredByOption2').is(':checked')) {
										$('.show-hide-footer-custom-text').show();
									} else {
										$('.show-hide-footer-custom-text').hide();
									}
								});


								// Contact us show/hide input
								if($('#whiteLabelContactOption1').is(':checked')) {
									$('.show-hide-helpdesk-email').show();
								} else {
									$('.show-hide-helpdesk-email').hide();
									$('#CoreMultidomain_whiteLabelContactCustomEmail').val('');
								}

								// Contact us show/hide input
								$(document).on('change', 'input[name=\'CoreMultidomain[whiteLabelContactOption]\']', function(){
									if($('#whiteLabelContactOption1').is(':checked')) {
										$('.show-hide-helpdesk-email').show();
									} else {
										$('.show-hide-helpdesk-email').hide();
										$('#CoreMultidomain_whiteLabelContactCustomEmail').val('');
									}
								});

								// Replace Docebo word show/hide input
								if($('#CoreMultidomain_replaceDoceboWordOption').is(':checked')) {
									$('.show-hide-naming-input').show();
								} else {
									$('.show-hide-naming-input').hide();
								}

								// Replace Docebo word show/hide input
								$(document).on('change', 'input[name=\'CoreMultidomain[replaceDoceboWordOption]\']', function(){
									if($('#CoreMultidomain_replaceDoceboWordOption').is(':checked')) {
										$('.show-hide-naming-input').show();
									} else {
										$('.show-hide-naming-input').hide();
									}
								});

								// Replace Docebo.com show/hide input
								if($('#CoreMultidomain_whitelabelNamingSiteEnable').is(':checked')) {
									$('.show-hide-naming-site-input').show();
									// CoreMultidomain_replaceDoceboCustomLink
									if($('#CoreMultidomain_replaceDoceboCustomLink').val() == '') {
										$('#CoreMultidomain_replaceDoceboCustomLink').val('http://');
									}
								} else {
									$('.show-hide-naming-site-input').hide();
									$('#CoreMultidomain_replaceDoceboCustomLink').val('');
								}

								// Replace Docebo.com show/hide input
								$(document).on('change', 'input[name=\'CoreMultidomain[whitelabelNamingSiteEnable]\']', function(){
									if($('#CoreMultidomain_whitelabelNamingSiteEnable').is(':checked')) {
										$('.show-hide-naming-site-input').show();

										// CoreMultidomain_replaceDoceboCustomLink
										if($('#CoreMultidomain_replaceDoceboCustomLink').val() == '') {
											$('#CoreMultidomain_replaceDoceboCustomLink').val('http://');
										}
									} else {
										$('.show-hide-naming-site-input').hide();
										$('#CoreMultidomain_replaceDoceboCustomLink').val('');
									}
								});

								// ***************-----custom TEXT multidomain events section----***************
								initTinyMCE('#textareaCustomText', 150);

								tinymce.EditorManager.on("AddEditor", function (e) {
									var editor = e.editor;

									if (editor.id == 'textareaCustomText') {
										editor.on('input change', function (e) {
											var value = editor.getContent();
											var lang = $('#customtextMultidomain').val();
											$('input[name="valuesForTextMulti[' + lang + ']"]').val(value);
										});
									}
								});

								$(document).on('change', '#customtextMultidomain', function(){
										tinymce.get('textareaCustomText').focus();
										var chosenLanguage = $(this).val();
										newVal = $('input[name="valuesForTextMulti[' + chosenLanguage + ']"]').val();
										tinymce.activeEditor.setContent(newVal);
								});

								// create hidden fields for custom text inputs
								$.each(languages, function (index, lang) {
									var value = translationsListMulti[lang];
									if (value != undefined) {
										$('<input>').attr({
											type: 'hidden',
											name: 'valuesForTextMulti[' + lang + ']',
											value: value
										}).appendTo('div.max-input.show-hide-footer-custom-text');
									}else{
										$('<input>').attr({
											type: 'hidden',
											name: 'valuesForTextMulti[' + lang + ']',
											value: ''
										}).appendTo('div.max-input.show-hide-footer-custom-text');
									}
								});
								// ***************-----end of custom TEXT multidomain events section-----***************

								// ***************-----custom URL multidomain events section----***************
								$(document).on('change', '#languageForUrlMulti', function(){
									var chosenLanguage = $(this).val();
									var value = $('input[name="valuesForUrlMulti[' + chosenLanguage + ']"]').val();
									$('#CoreMultidomain_whiteLabelFooterExtUrl').val(value);
								});

								$(document).on('input', '#CoreMultidomain_whiteLabelFooterExtUrl', function(){
									var value = $(this).val();
									var lang = $('#languageForUrlMulti').val();
									$('input[name="valuesForUrlMulti[' + lang + ']"]').val(value);
								})

								// create hidden fields for custom text inputs
								$.each(languages, function (index, lang) {
									var value = translationsListForURLMulti[lang];
									if (value != undefined) {
										$('<input>').attr({
											type: 'hidden',
											name: 'valuesForUrlMulti[' + lang + ']',
											value: value
										}).appendTo('div.max-input.show-hide-footer-url');
									}else{
										$('<input>').attr({
											type: 'hidden',
											name: 'valuesForUrlMulti[' + lang + ']',
											value: ''
										}).appendTo('div.max-input.show-hide-footer-url');
									}
								});

								// header multidomains
								$(document).on('change', '#customUrlsMultidomainHeader', function () {
									var lang = $(this).val();
									var value = $('input[name="valuesForUrlMultiHeader[' + lang + ']"]').val();
									$('#CoreMultidomain_whiteLabelHeaderExtUrl').val(value);
								});
								$(document).on('input', '#CoreMultidomain_whiteLabelHeaderExtUrl', function(){
									var lang = $('#customUrlsMultidomainHeader').val();
									$('input[name="valuesForUrlMultiHeader[' + lang + ']"]').val($(this).val());
								});
								if($('#whiteLabelHeader1').is(':checked')){
									$('#customUrlsMultidomainHeader').trigger('change');
								}

								// ***************-----end of custom URL multidomain events section-----***************

								var time = setInterval(function () {
									if (tinymce.activeEditor != null) {
										if($('#languageForUrlMulti').is(':visible')) {
											tinymce.get('textareaCustomText').focus();
										}
										$('#customtextMultidomain').trigger('change');
										$('#languageForUrlMulti').trigger('change');
										clearInterval(time);
									}
								}, 500);
							});
						})(jQuery);
						/*]]>*/
					</script>