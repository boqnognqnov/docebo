<style>
    #multidomain-settings-subscription .setting-label-subscription {
        float: left;

    }

    #multidomain-settings-subscription .control-group span {
        float: left;
        padding-right: 3px;
    }

    #multidomain-settings-subscription .row-fluid label {
        float: left;
    }

    #multidomain-settings-subscription .row-fluid span {
        float: left;
        padding-right: 3px;
    }

    #multidomain-settings-subscription  #sub-settings-renewal-type {
        font-size: 14px;
        font-weight: bold;
    }

    #multidomain-settings-subscription .disabledbutton {
        pointer-events: none;
        opacity: 0.4;
    }

    #multidomain-settings-subscription .odd {
        border: none !important;
    }

    #multidomain-settings-subscription .setting-row {
        border: none !important;
    }
</style>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'multidomain-settings-subscription',
    'method' => 'POST',
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
        'class' => 'form-horizontal docebo-form'
    )
));
?>

<?php
echo CHtml::beginForm(); ?>

<div class="row-fluid row"
     style="margin-left:0!important;">

    <?php if (PluginManager::isPluginActive('SubscriptionsApp')) { ?>

        <div class="row-fluid setting-row-wrapper">
            <div class="row-fluid setting-row">
                <div class="span12">
                    <div class="setting-name span3">
                        <span class="setting-title"><?= Yii::t('multidomain', 'Custom settings') ?></span>
                    </div>
                    <div class="setting-value span9">
                        <?= CHtml::checkBox('subscription_custom_settings', ($subscriptionSettings['subscription_custom_settings'] == 'on') ? true : false,
                            array('uncheckValue' => 0, 'value' => 1, 'id' => 'subscription_custom_settings')) ?>
                        &nbsp;
                        <span
                            class="setting-title"><?= Yii::t('multidomain', 'Enable custom settings for this client') ?></span>
                    </div>
                </div>
            </div>
        </div>

        <div id="subscription-fields">

            <div class="row-fluid setting-row-wrapper odd">
                <div class="row-fluid setting-row">
                    <div class="span12">
                        <div class="setting-name span3">
                            <span class="setting-title"><?= Yii::t('multidomain', 'Enforce seat limit') ?></span>
                        </div>
                        <div class="setting-value  span9">
                            <?= CHtml::checkBox('subscription_enforce_seats_limit', ($subscriptionSettings['subscription_enforce_seats_limit'] == 'on') ? true : false,
                                array('uncheckValue' => 0, 'value' => 1, 'id' => 'subscription_enforce_seats_limit')) ?>
                            &nbsp;
                            <span class="setting-title"><?= Yii::t('multidomain', 'Enable enforce seat limit') ?></span>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row-fluid setting-row-wrapper">
                <div class="row-fluid setting-row">
                    <div class="span12">
                        <div class="setting-name span3">
                            <?= CHtml::label(Yii::t('multidomain',
                                'Enforce licences limit'),
                                'subscription_enforce_licences_limit', ['class' => 'setting-label-subscription']) ?>
                        </div>
                        <div class="setting-value span9">
                            <?= CHtml::checkBox('subscription_enforce_licences_limit', ($subscriptionSettings['subscription_enforce_licences_limit'] == 'on') ? true : false,
                                array('uncheckValue' => 0, 'value' => 1, 'id' => 'subscription_enforce_licences_limit')) ?>
                            &nbsp;
                            <span class="setting-title"><?= Yii::t('multidomain', 'Enable enforce licences limit') ?></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row-fluid setting-row-wrapper odd">
                <div class="row-fluid setting-row">
                    <div class="span12">
                        <div class="setting-name span3">
                            <?= CHtml::label(Yii::t('multidomain', 'Enforce expiration dates'), 'enforce_expiration_dates', ['class' => 'setting-label-subscription']) ?>
                        </div>

                        <div class="setting-value span9">
                            <?= CHtml::checkBox('enforce_expiration_dates', ($subscriptionSettings['enforce_expiration_dates'] == 'on') ? true : false,
                                array('uncheckValue' => 0, 'value' => 1, 'id' => 'enforce_expiration_dates')) ?>
                            &nbsp;
                            <span
                                class="setting-title"><?= CHtml::label(Yii::t('multidomain', 'Enable enforce expiration dates'), 'enforce_expiration_dates', ['class' => 'setting-label-subscription']) ?></span>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row-fluid setting-row-wrapper">
                <div class="row-fluid setting-row">
                    <div class="span12">
                        <div class="setting-name span3">
                            <?= CHtml::label(Yii::t('multidomain', 'Renewal method :'), 'Renewal method', ['id' => 'sub-settings-renewal-type']) ?>
                        </div>
                        <div class="setting-value span9">
                            <?php echo CHtml::radioButtonList('default_renewal_method', $subscriptionSettings['default_renewal_method'], array('2' => Yii::t('configuration', 'Manual'), '1' => Yii::t('configuration', 'Automatic')), array('separator' => '')); ?>
                        </div>
                    </div>
                </div>
            </div>

            
            <div class="row-fluid setting-row-wrapper odd">
                <div class="row-fluid setting-row">
                    <div class="span12">
                        <div class="setting-name span3">
                            <?= CHtml::label(Yii::t('multidomain', 'Allow licences refunding'), 'licences_refunding', ['class' => 'setting-label-subscription']) ?>
                        </div>

                        <div class="setting-value span9">
                            <?= CHtml::checkBox('licences_refunding', ($subscriptionSettings['licences_refunding'] == 'on') ? true : false,
                                array('uncheckValue' => 0, 'value' => 1, 'id' => 'licences_refunding')) ?>
                            &nbsp;
                            <span
                                class="setting-title"><?= CHtml::label(Yii::t('multidomain', 'Increase the number of licences if a user is removed from a subscription.'), 'licences_refunding', ['class' => 'setting-label-subscription']) ?></span>
                        </div>

                    </div>
                </div>
            </div>
			
			<div class="row-fluid setting-row-wrapper odd">
                <div class="row-fluid setting-row">
                    <div class="span12">
                        <div class="setting-name span3">
                            <?= CHtml::label(Yii::t('multidomain', 'Allow seats refunding'), 'seats_refunding', ['class' => 'setting-label-subscription']) ?>
                        </div>

                        <div class="setting-value span9">
                            <?= CHtml::checkBox('seats_refunding', ($subscriptionSettings['seats_refunding'] == 'on') ? true : false,
                                array('uncheckValue' => 0, 'value' => 1, 'id' => 'seats_refunding')) ?>
                            &nbsp;
                            <span
                                class="setting-title"><?= CHtml::label(Yii::t('multidomain', 'Increase the number of seats if a user is removed from a subscription.'), 'seats_refunding', ['class' => 'setting-label-subscription']) ?></span>
                        </div>

                    </div>
                </div>
            </div>
			
			<div class="row-fluid setting-row-wrapper">
                <div class="row-fluid setting-row">
                    <div class="span12">
                        <div class="setting-name span3">
                            <?= CHtml::label(Yii::t('multidomain', 'Block refund if course is'), 'Block refund if course is', ['id' => 'block_seats_refunding']) ?>
                        </div>
                        <div class="setting-value span9">
                            <?php echo CHtml::radioButtonList('block_seats_refunding', $subscriptionSettings['block_seats_refunding'], array('started' => Yii::t('configuration', 'Started'), 'completed' => Yii::t('configuration', 'Completed')), array('separator' => '')); ?>
                        </div>
                    </div>
                </div>
            </div>
			
			<div class="row-fluid setting-row-wrapper odd">
                <div class="row-fluid setting-row">
                    <div class="span12">
                        <div class="setting-name span3">
                            <?= CHtml::label(Yii::t('multidomain', 'Show subscription tab first in assign seats'), 'sub_tab_first', ['class' => 'setting-label-subscription']) ?>
                        </div>

                        <div class="setting-value span9">
                            <?= CHtml::checkBox('sub_tab_first', ($subscriptionSettings['sub_tab_first'] == 'on') ? true : false,
                                array('uncheckValue' => 0, 'value' => 1, 'id' => 'sub_tab_first')) ?>
                            &nbsp;
                            <span
                                class="setting-title"><?= CHtml::label(Yii::t('multidomain', 'In the manage seat page show as first page the subscription menu.'), 'sub_tab_first', ['class' => 'setting-label-subscription']) ?></span>
                        </div>

                    </div>
                </div>
            </div>

        </div>

        <br/>
        <div class="row-fluid text-right row even">
            <div class="span12 text-right">
                <?php
                echo CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
                    'name' => 'save_subscription_settings',
                    'class' => 'btn btn-docebo green big',
                ));
                ?>
                <?php
                echo CHtml::link(Yii::t('standard', '_CANCEL'), Docebo::createAdminUrl('MultidomainApp'), array(
                    'name' => 'save_multidomain_settings',
                    'class' => 'btn btn-docebo black big',
                ));
                ?>
            </div>
        </div>
    <?php } ?>

    <?php CHtml::endForm(); ?>



    <?php
    echo CHtml::hiddenField('selectedTab', $selectedTab, array('id' => 'selectedTab', 'class' => 'selectedTab'));
    $this->endWidget();
    ?>

    <script type="text/javascript">
        $(document).ready(function () {
            defineEnableDisable();
            trackCustomSettingsCheckBox();
        });

        function trackCustomSettingsCheckBox() {
            $("#subscription_custom_settings").on("change", function () {
                defineEnableDisable();
            });
        }

        function defineEnableDisable() {
            if ($("#subscription_custom_settings").is(':checked')) {
//                    alert('is checked');
                $("#subscription-fields").removeClass("disabledbutton");
            } else {
//                    alert('is not checked');
                $("#subscription-fields").addClass("disabledbutton");
            }
        }
    </script>
