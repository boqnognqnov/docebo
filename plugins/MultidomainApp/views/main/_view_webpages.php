<?php

	$isVisible = CoreMultidomainWebpage::model()->findAllByAttributes(array('id_multidomain' => $data->idMultidomain, 'id_webpage' => $data->id));
	$toggleUrl = Docebo::createAdminUrl('MultidomainApp/main/toggleWebPageVisibility', array(
		'id' 	=> $data->idMultidomain,
		'page'	=> $data->id,
	));

?>

<div class="row-fluid" data-page-id="<?php echo $data->id; ?>">
    <div class="title span8">
        <?php
        $currentLanguageTitle = trim($data->translation->title);
        if (!empty($currentLanguageTitle)) {
            echo $currentLanguageTitle;
        }
        else {
            echo '-';
        }
        ?>
    </div>
    <div class="actions span4 text-right">
        <ul>
            <li class="visibility">
                <?php 
                	echo CHtml::link('', $toggleUrl, array(
                    	'class'		=>	'toggle-signin-webpage branding-page-visibility ' . ($isVisible ? '' : 'grayed'),
                		'data-id'	=> $data->id,	
                	)); 
                ?>
            </li>
        </ul>
    </div>
</div>