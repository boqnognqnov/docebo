<style>
    .mainMultidomainSettings label {
        display: inline-block;
        margin-left: 10px;
        vertical-align: middle;
    }

    .mainMultidomainSettings.advanced-main {
        margin-left: 0 !important;
    }
</style>
<?php
/**
 * Created by PhpStorm.
 * User: asen
 * Date: 29-Aug-16
 * Time: 3:22 PM
 */
DoceboUI::printFlashMessages();
$this->breadcrumbs = array(
    Yii::t('menu', 'Admin'),
    Yii::t('multidomain', 'Multidomain') => Docebo::createAdminUrl('MultidomainApp'),
    Yii::t('standard', 'Settings') => Docebo::createAdminUrl('MultidomainApp/main/mainSettings')
);
echo CHtml::beginForm(); ?>
<div class="row-fluid mainMultidomainSettings advanced-main">
    <div class="row-fluid row"
         style="margin-left:0!important;border-top: 1px solid #e4e6e5; border-bottom: 1px solid #e4e6e5;">
        <div class="row"
             style="background: #f1f3f2; border-top: 1px solid #ffffff; border-bottom: 1px solid #ffffff; padding: 22px 18px;margin-left:0!important;">
            <div class="span2">
                <h3 style="padding-left: 18px;color:#333333;background: none"><?= Yii::t('standard', 'Settings') ?></h3>
            </div>
            <div class="span10">
                <div class="control-group row-fluid">
                    <?= CHtml::checkBox('user_login_check', ($checkUserLogin == 1) ? true : false,
                        array('uncheckValue' => 0, 'value' => 1, 'id' => 'user_login_check')) ?>
                    <?= CHtml::label(Yii::t('multidomain',
                        Yii::t('multidomain','Enable multidomain log in restriction')),
                        'user_login_check') ?>

                </div>
                
                
            </div>


        </div>
    </div>
    <br/>
    <br/>
    <br/>
    <div class="row-fluid text-right row even">
        <?= CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
            'name' => 'save_multidomain_main_settings',
            'class' => 'btn btn-save',
        )); ?>

        <?= CHtml::link(Yii::t('standard', '_CANCEL'), Docebo::createLmsUrl('site/index'), array(
            'name' => 'save_multidomain_settings',
            'class' => 'btn btn-cancel',
        )); ?>
    </div>
</div>
<?php CHtml::endForm(); ?>

<script>
    $(function () {
        $('input').styler();
    });
</script>