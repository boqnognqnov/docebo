<style>
	.orgChart_languages > div:first-of-type{
		width: 55%;
		display: inline-block;
	}
	.orgChart_languages > div:nth-of-type(2){
		width: 41%;
		display: inline-block;
	}
</style>

				<input type="hidden" id="languages" value="<?=(implode(',', array_keys($languages)))?>">
				<?php
					$form = $this->beginWidget('CActiveForm', array(
						'id' => 'multidomain-settings-form',
						'method' => 'POST',
						'htmlOptions' => array(
							'enctype' 	=> 'multipart/form-data',
							'class' 	=> 'form-horizontal docebo-form'
						)
					));
				?>
			
				<div class="row-fluid setting-row">
					<div class="span12">
						<div class="setting-name span3">
							<span class="setting-title"><?= Yii::t('configuration', '_PAGE_TITLE') ?></span>
							<p class="setting-description"><?= Yii::t('branding', '_PAGE_TITLE_DESCRIPTION')?></p>						
						</div>
						<div class="setting-value span9">
							<div class="row-fluid">
							<?php
								echo $form->textField($model, 'pageTitle', array('class' => 'span12'));
							?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row-fluid setting-row-wrapper odd">
				<div class="row-fluid setting-row">
					<div class="span12">
						<div class="setting-name span3">
							<span class="setting-title"><?= Yii::t('course', '_SPONSOR_LOGO') ?></span>
							<p class="setting-description"><?= Yii::t('branding', '_COMPANY_LOGO_DESCRIPTION') ?></p>						
						</div>
						<div class="setting-value span9">
							<div class="row-fluid">
								<?php echo CHtml::image(CoreAsset::url($model->companyLogo, CoreAsset::VARIANT_SMALL)); ?>
							</div>
							<div class="row-fluid">
								<div class="span12">
									<br>
									<?php 
										echo $form->fileField($model, 'companyLogo');
										echo $form->error($model, 'companyLogo');
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>

				<div class="row-fluid setting-row">
					<div class="span12">
						<div class="setting-name span3">
							<span class="setting-title"><?=Yii::t('templatemanager', 'Header').' <span style="text-transform:lowercase">'.Yii::t('htmlframechat', '_MSGTXT').'</span>'?></span>
							<p class="setting-description"><?= Yii::t('branding', 'Add here an header message that will be displayed underneath the logo (make sure it fits within your Sign In Form layout)') ?></p>
						</div>
						<div class="setting-value span9">
							<div class="row-fluid">
								<div class="span12">
									<br>
									<label style="margin-left: 0">
										<?= CHtml::activeCheckBox($model, 'header_message_active') ?>
										<?= Yii::t('standard', 'Enable') . ' <span style="text-transform:lowercase">' . Yii::t('templatemanager', 'Header') . ' ' . Yii::t('htmlframechat', '_MSGTXT') . '</span>'; ?>
									</label>

									<div id="header_message_settings_multidomain">
										<div class="orgChart_languages">
											<div>
												<strong><?= Yii::t('branding', '_PAGE_LANGUAGE_LABEL') ?></strong>
												<?= CHtml::dropDownList('custom_text_header_multidomain', $defaultLanguage, $languages);
												?>
											</div>
											<div>
												<?= Yii::t('admin_lang', '_LANG_ALL') . ': <span>' . count($languages) . '</span>'; ?>
												<div id="orgChart_assignedCount"><?php echo Yii::t('standard', 'Filled languages') . ' <span>' . count($languagesValues) . '</span>'; ?></div>
											</div>
										</div>
										<div style="margin-bottom: 5px">
											<?= Yii::t('htmlframechat', '_MSGTXT') ?>
										</div>

										<?= CHtml::textArea('header_message_area_multidomain') ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row-fluid setting-row-wrapper odd">
				<div class="row-fluid setting-row">
					<div class="span12">
						<div class="setting-name span3">
							<span class="setting-title">Favicon</span>
							<p class="setting-description"><?= Yii::t('branding', 'Upload a 16x16 png/ico image that will represent your website\'s favicon.') ?></p>						
						</div>
						<div class="setting-value span9">
							<div class="row-fluid">
								<?php echo CHtml::image(CoreAsset::url($model->favicon, CoreAsset::VARIANT_ORIGINAL)); ?>
							</div>
							<div class="row-fluid">
								<div class="span12">
									<br>
									<?php 
										echo $form->fileField($model, 'favicon');
										echo $form->error($model, 'favicon');
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
				
				<div class="row-fluid setting-row">
					<div class="span12">
						<div class="setting-name span3">
							<span class="setting-title"><?= Yii::t('branding', '_COLORS_SCHEME'); ?></span>
							<p class="setting-description"><?= Yii::t('multidomain', 'Choose your color scheme') ?></p>						
						</div>
						<div class="setting-value span9">
							<?php
								 echo $form->dropDownList($model, 'colorScheme', $model->getColorSchemesDropdownList(), array(
                            		'class' => ''
                        		)) 
							?>
						</div>
					</div>
				</div>

				<br>
				<div class="row-fluid">
					<div class="span12 text-right">
						<?php
							echo CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
								'name'	=> 'save_multidomain_settings',
								'class'	=> 'btn btn-docebo green big',		
							)); 
						?>
						<?php
						echo CHtml::link(Yii::t('standard', '_CANCEL'), Docebo::createAdminUrl('MultidomainApp'), array(
							'name'	=> 'save_multidomain_settings',
							'class'	=> 'btn btn-docebo black big',
						));
						?>
					</div>
				</div>
				<?php
					echo CHtml::hiddenField('selectedTab', $selectedTab, array('id' => 'selectedTab', 'class' => 'selectedTab')); 
					$this->endWidget(); 
				?>

				
<script type="text/javascript">

	var translationsList = <?=json_encode($languagesValues, JSON_HEX_APOS)?>;
	var languages = $('#languages').val().split(',');
	$(function(){
		$('input#CoreMultidomain_companyLogo').styler({browseText:'<?php echo Yii::t('setup', 'Upload your logo');?>'});
        $('input#CoreMultidomain_favicon').styler({browseText:'<?php echo Yii::t('setup', 'Upload your favicon');?>'});
		initTinyMCE('#header_message_area_multidomain', 150);
		tinymce.EditorManager.on("AddEditor", function (e) {
			var editor = e.editor;

			if (editor.id == 'header_message_area_multidomain') {
				editor.on('input change', function (e) {
					var value = editor.getContent();
					var lang = $('#custom_text_header_multidomain').val();
					$('input[name="valuesForHeaderMsgMultidomain[' + lang + ']"]').val(value);
				});
			}
		});

		$(document).on('change', '#CoreMultidomain_header_message_active', function(){
			var that = $(this);
			if(that.is(':checked')){
				$('#header_message_settings_multidomain').show();
			}else{
				$('#header_message_settings_multidomain').hide();
			}
		});
		// create hidden fields for custom text inputs
		$.each(languages, function (index, lang) {
			var value = translationsList[lang];
			if (value != undefined) {
				$('<input>').attr({
					type: 'hidden',
					name: 'valuesForHeaderMsgMultidomain[' + lang + ']',
					value: value
				}).appendTo('#header_message_settings_multidomain');
			}else{
				$('<input>').attr({
					type: 'hidden',
					name: 'valuesForHeaderMsgMultidomain[' + lang + ']',
					value: ''
				}).appendTo('#header_message_settings_multidomain');
			}
		});

		var time = setInterval(function () {
			if (tinymce.activeEditor != null) {
				if($('#custom_text_header_multidomain').is(':visible')) {
					tinyMCE.get('header_message_area_multidomain').focus();
				}
				$('#custom_text_header_multidomain').trigger('change');
				clearInterval(time);
			}
		}, 500);

		$(document).on('change', '#custom_text_header_multidomain', function(){
			tinymce.get('header_message_area_multidomain').focus();
			var chosenLanguage = $(this).val();
			var newVal = $('input[name="valuesForHeaderMsgMultidomain[' + chosenLanguage + ']"]').val();
			tinymce.activeEditor.setContent(newVal);
		});
		$('#CoreMultidomain_header_message_active').trigger('change');
	});
</script>
				