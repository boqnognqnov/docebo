<?php
/* @var $model CoreMultidomain
 * @var $active_theme string
 */
$wizardUrl 		= Docebo::createAdminUrl('MultidomainApp/main/clientWizard', array('id' => $model->id));
?>
<div class="row-fluid multidomain-header">
    <div class="span3">
        <?php echo CHtml::image(CoreAsset::url($model->companyLogo, CoreAsset::VARIANT_SMALL), '', array('class' => 'logo')); ?>
    </div>
    <div class="span6">
        <div class="row-fluid">
            <div class="span6">
                <i class="admin-ico users"></i><?= $model->name ?>
            </div>
            <div class="span6">
                <i class="admin-ico curriculas"></i><?= $model->getClientUri() ?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <i class="fa fa-folder" style="font-size: 1.5em; margin-right: 22px;"></i><?php
                $node = $model->orgChart;
                $path = CoreOrgChartTree::getOcGroupPath($node->idst_oc, ' / ', false);
                echo Docebo::ellipsis($path, 50);
                ?>
            </div>
            <div class="span6">
                <i class="zmdi zmdi-view-compact  zmdi-hc-2x"></i><?= $active_theme['name'] ?>
                <?php if($active_theme['code'] != ThemeVariant::LEGACY_THEME_CODE): ?>
                <a href="#" class="btn btn-docebo big green configure-theme" id="manage-multidomain-theme"><?= Yii::t('standard', 'Configure Theme') ?></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="span3">
        <a href="<?= $wizardUrl ?>" class="open-dialog edit-multidomain-dialog"
           data-dialog-id 		= "edit-client-dialog"  data-dialog-class	= "edit-client-dialog"
           data-dialog-title	= "<?= Yii::t('standard', '_MOD') ?>">
            <span class="i-sprite is-edit"></span>
        </a>
    </div>
</div>
<script type="text/javascript">
    $(document).on('click', 'a#manage-multidomain-theme', function(){
        <?= Yii::app()->legacyWrapper->setHydraRoute('/manage/themes/' . $active_theme['code'] . '/multidomain/' . $model->id, false) ?>
    });
</script>