<?php
/**
 * @var $active_theme array
 */
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'multidomain-settings-form',
	'method' => 'POST',
	'htmlOptions' => array(
		'enctype' 	=> 'multipart/form-data',
		'class' 	=> 'form-horizontal docebo-form'
	)
));
?>

<div class="row-fluid setting-row-wrapper even">
	<div class="row-fluid setting-row">
		<div class="span12">
			<div class="setting-name span3">
				<?= Yii::t('catalogue', 'Custom settings'); ?>
			</div>
			<div class="setting-value span9">
				<label class="checkbox">
					<?php
					echo $form->checkBox($model, 'catalog_custom_settings', array('class' => 'on-off-switch'));
					echo $model->getAttributeLabel('catalog_custom_settings');
					?>
				</label>
				<?=$form->error($model, 'catalog_custom_settings'); ?>
			</div>
		</div>
	</div>
</div>

<div class="blockable">
		<div class="row-fluid setting-row-wrapper odd">
			<div class="row-fluid setting-row">
				<div class="span12">
					<div class="setting-name span3">
						<?= Yii::t('player', 'Catalog options'); ?>
					</div>
					<div class="setting-value span9">
						<label class="checkbox">
						<?php
						echo $form->checkBox($model, 'catalog_use_categories_tree');
						echo $model->getAttributeLabel('catalog_use_categories_tree');
						?>
						</label>
						<?=$form->error($model, 'catalog_use_categories_tree'); ?>
					</div>
				</div>
			</div>
		</div>


		<div class="row-fluid setting-row-wrapper even setting-row-wrapper-last">
			<div class="row-fluid setting-row">
				<div class="span12">
					<div class="setting-name span3">
						<?php echo Yii::t('catalogue', 'Public Catalog'); ?>
					</div>


					<div class="values setting-value span9">
						<div class="checkbox-item">
							<?=$form->checkBox($model, 'catalog_external', array('uncheckValue' => 0, 'value' => 1));?>
							<?=CHtml::label(Yii::t('catalogue', 'Show the catalog also to non authenticated users'), 'CoreMultidomain_catalog_external', array('class'=>''));?>
							<?=$form->error($model, 'catalog_external'); ?>
						</div>
						<div class="row extra-catalog_external" style="margin-left: 30px;">
							<br />
							<div class="checkbox-item">
								<?=CHtml::radioButton('catalog_external_selected_catalogs', ($model->catalog_external_selected_catalogs == '' || !$model->catalog_external_selected_catalogs) ? true : false, array('id'=>'catalog_external_no_selected_catalogs', 'value'=> 0));?>
								<?=CHtml::label(Yii::t('catalogue', 'Display all available public courses and learning plans'), 'catalog_external_no_selected_catalogs', array('class'=>''));?>
							</div>
							<div class="checkbox-item">
								<?=CHtml::radioButton('catalog_external_selected_catalogs', ($model->catalog_external_selected_catalogs != '' && $model->catalog_external_selected_catalogs) ? true : false, array('id'=>'catalog_external_selected_catalogs', 'value'=> 1));?>
								<?=CHtml::label(Yii::t('catalogue', 'Select catalogs to display') .'(<span id="selected_catalogs_count">'.$countSelectedCatalogs.'</span>)', 'catalog_external_selected_catalogs', array('class'=>''));?>
								<a href="index.php?r=CoursecatalogApp/CoursecatalogApp/assignCatalogs&idst=<?= $idAccount ?>"
								   data-dialog-class="catalogapp-dialog-select-catalogs"
								   class="open-dialog btn btn-docebo green big select-custom-catalogs" style="margin-left: 200px; display: inline-block; margin-top: -35px; ">
									<?=Yii::t('standard', '_SELECT');?>
								</a>
								<?=CHtml::hiddenField('selected_catalogs_holder', $model->catalog_external_selected_catalogs); ?>
								<?=CHtml::hiddenField('selected_catalogs', $model->catalog_external_selected_catalogs) ; ?>
								<?=$form->error($model, 'catalog_external_selected_catalogs'); ?>
							</div>
						</div>
                        <?php if($active_theme['code'] == ThemeVariant::LEGACY_THEME_CODE): ?>
                        <div class="layout-thumbnail" style="display:inline-block; margin-right:10px;margin-top: 15px;float: right;position:relative;">
                            <span class="preview" style="max-width:265px;left:50px;top:-20px;font-size:13px !important;"><strong><?php echo Yii::t('catalogue', 'Public Catalog preview in Homepage form'); ?></strong></span>
                            <span class="background" style="max-width:136px;left:-20px;top:190px;"><strong><?php echo Yii::t('certificate', '_BACK_IMAGE'); ?></strong></span>
                            <span class="title" style="left:170px;top:295px;"><strong><?php echo Yii::t('catalogue', 'Catalog courses'); ?></strong></span>
                            <span class="text" style="left:240px;top:10px;"><strong><?php echo Yii::t('branding', '_TITLE_TEXT_SECTION'); ?></strong></span>
                            <span class="external" style="left:10px;top:110px;"><strong><?php echo Yii::t('admin_webpages', '_WEBPAGES_CAPTION'); ?></strong></span>
                            <?php echo CHtml::image(Yii::app()->theme->baseUrl . '/images/loginlayouts/preview-catalog.jpg','',array('style'=>'width: 380px;')); ?>
                        </div>
                        <?php endif; ?>
					</div>


				</div>
			</div>
		</div>

	</div>



	<br />
	<div class="row-fluid">
		<div class="span12 text-right">
			<?php
			echo CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
				'name'	=> 'save_multidomain_settings',
				'class'	=> 'btn btn-docebo green big',
			));
			?>
			<?php
			echo CHtml::link(Yii::t('standard', '_CANCEL'), Docebo::createAdminUrl('MultidomainApp'), array(
				'name'	=> 'save_multidomain_settings',
				'class'	=> 'btn btn-docebo black big',
			));
			?>
		</div>
	</div>

	<br />
	<br />
<?php
echo CHtml::hiddenField('selectedTab', $selectedTab, array('id' => 'selectedTab', 'class' => 'selectedTab'));
$this->endWidget();
?>
<style type="text/css">
    div.layout-thumbnail span{
        color:#0465AC;
        font-family:'Handlee',cursive;
        font-size:19px;
        line-height:22px;
        text-align:right;
        max-width:150px;
        position:absolute;
    }
</style>
<script type="text/javascript">
	// Check the Public Catalog checkbox on custom settings is selecting !!!
	$('input[id=CoreMultidomain_catalog_custom_settings]').on('change', function(){
		if ($('input[id=CoreMultidomain_catalog_custom_settings]').is(':checked') &&
			!($('#CoreMultidomain_catalog_external').is(':checked'))) {
			$('#CoreMultidomain_catalog_external').trigger('click');
		}
	});

	// Hide select custom catalogs radio group if needed
	if (!($('input[id=CoreMultidomain_catalog_external]').is(':checked'))) {
        $('.layout-thumbnail').hide();
		$('.extra-catalog_external').addClass('hidden');
	}

	// Show/Hide select custom catalogs
	$(document).on('change', 'input[id=CoreMultidomain_catalog_external]', function(){
		if($(this).is(':checked')){
			$('.extra-catalog_external').removeClass('hidden');
            $('.layout-thumbnail').show();
		} else{
            $('.layout-thumbnail').hide();
			$('.extra-catalog_external').addClass('hidden');
		}
	});

	// Enable/Disable select catalogs button
	if ($('input[name=catalog_external_selected_catalogs]:checked').val() == 0) {
		$('.select-custom-catalogs').addClass('hidden');
		$('.select-custom-catalogs').addClass('disabled');
	} else {
		$('.select-custom-catalogs').removeClass('hidden');
		$('.select-custom-catalogs').removeClass('disabled');
	}

	$(document).on('change', 'input[id=catalog_external_no_selected_catalogs]', function(){
		if($(this).is(':checked')){
			$('.select-custom-catalogs').addClass('hidden');
			$('.select-custom-catalogs').addClass('disabled');

			$('input[name=selected_catalogs]').val('');
			$('span#selected_catalogs_count').html(0);
		} else{
			$('.select-custom-catalogs').removeClass('hidden');
			$('.select-custom-catalogs').removeClass('disabled');
		}
	});

	$(document).on('change', 'input[id=catalog_external_selected_catalogs]', function(){
		if($(this).is(':checked')){
			$('.select-custom-catalogs').removeClass('hidden');
			$('.select-custom-catalogs').removeClass('disabled');
		} else{
			$('.select-custom-catalogs').addClass('hidden');
			$('.select-custom-catalogs').addClass('disabled');
		}
	});


	$('#multidomain-settings-form').submit(function(e) {
		if ($('input[name=catalog_external_selected_catalogs]:checked').val() == 1 && $('input[name=selected_catalogs]').val() == '') {
			e.preventDefault();
			Docebo.Feedback.show("error", "<?= Yii::t('catalogue', 'Please, select at least one custom catalog.'); ?>");
		}

	});

</script>


