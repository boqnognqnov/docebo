<?php

/**
 * @var $selectedTheme string
 * @var $availableThemes array
 */
	$domainType = $model->domain_type ? $model->domain_type : 'none';
	$currentLmsDomain = trim(Docebo::getCurrentDomain(true), '/');
	$currentLmsDomainNoWww = preg_replace('/^www\\./', '', strtolower($currentLmsDomain));
	

?>

<h1><?= Yii::t('multidomain', 'Create/Edit Client') ?></h1>

<div class="form wizard-client-initial">

	<?php 
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'client-step-initial-form',
			'htmlOptions' => array(
				'class' => 'ajax jwizard-form',
				'name' => 'client-step-initial-form'
			),
		));
		
		// Wizard related
		echo CHtml::hiddenField('from_step', 'step_initial');
		
	?>
	
	<?php if (Yii::app()->user->hasFlash('error')) : ?>
		<div class="row-fluid">
			<div class='alert alert-error alert-compact text-right'>
				<?= Yii::app()->user->getFlash('error'); ?>
			</div>
		</div>
	<?php endif; ?>
	
	
	<div class="row-fluid client-name">
		<div class="span12">
			<label><?= Yii::t('standard','_NAME') ?></label>
			<?= $form->textField($model, 'name', array('class' => 'span12')) ?>
		</div>
	</div>
	<br>
	
	
	<div class="row-fluid">
		<div class="span12">
			<label class="radio">
				<?= $form->radioButton($model, 'domain_type', array('value' => CoreMultidomain::DOMAINTYPE_SUBFOLDER, 'class' => 'multidomain-client-domaintype', 'uncheckValue' => null)) ?>
				<?= Yii::t('multidomain','Subfolder of {customdomain}', array('{customdomain}' => $currentLmsDomain)) ?>
			</label>
		</div>
	</div>
	<div class="row-fluid domain-type-value <?= CoreMultidomain::DOMAINTYPE_SUBFOLDER ?>">
		<div class="span12">
			<?= $currentLmsDomain ?>/ <?= CHtml::textField('domain_subfolder', $model->domain, array('class' => '')) ?>
		</div>
	</div>
	<br>
	
	<?php if(PluginManager::isPluginActive('CustomdomainApp'))  : ?>
		<?php if(!$this->isMultilevelDocebosaasDomain()) : ?>
			<div class="row-fluid">
				<div class="span12">
					<label class="radio">
						<?= $form->radioButton($model, 'domain_type', array('value' => CoreMultidomain::DOMAINTYPE_SUBDOMAIN, 'class' => 'multidomain-client-domaintype', 'uncheckValue' => null)) ?>
						<?= Yii::t('multidomain','Subdomain of {customdomain}', array('{customdomain}' => $currentLmsDomainNoWww)) ?>
					</label>
				</div>
			</div>
			<div class="row-fluid domain-type-value <?= CoreMultidomain::DOMAINTYPE_SUBDOMAIN ?>">
				<div class="span12">
					<?= CHtml::textField('domain_subdomain', $model->domain, array('class' => '')) ?> .<?= $currentLmsDomainNoWww ?>
				</div>
			</div>
			<br>
		<?php endif; ?>
	<?php endif; ?>
	
	
	<div class="row-fluid">
		<div class="span12">
			<label class="radio">
				<?= $form->radioButton($model, 'domain_type', array('value' => CoreMultidomain::DOMAINTYPE_CUSTOMDOMAIN, 'class' => 'multidomain-client-domaintype', 'uncheckValue' => null)) ?>
				<?= Yii::t('multidomain','New domain') ?>
			</label>
		</div>
	</div>
	<div class="row-fluid domain-type-value <?= CoreMultidomain::DOMAINTYPE_CUSTOMDOMAIN ?>">
		<div class="span12">
			<?= CHtml::textField('domain_customdomain', $model->domain, array('class' => '')) ?>
		</div>
	</div>
	<br>

    <div class="row-fluid">
        <div class="span12">
            <label class="select" for="theme-selection"><?= Yii::t('standard', 'Select a theme for this client') ?></label>
            <?= CHtml::dropDownList('theme', $selectedTheme, $availableThemes, array(
                'id' => 'theme-selection',
                'class' => 'span12'
            )) ?>
            <a href="<?= Docebo::createHydraUrl('manage', 'themes') ?>" class="blue-text" id="manage-themes"><?= Yii::t('standard', 'Manage themes') ?></a>
        </div>
    </div>
	
	<!-- For Dialog2 - they go in footer -->
	<div class="form-actions jwizard">
		
		<?php
			echo CHtml::submitButton(Yii::t('standard', '_NEXT'), array(
      			'class'  => 'btn btn-docebo green big jwizard-nav',
      			'name'	=> 'next_button', 
   			));
		?>
		<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
	</div>
	
	
	<?php $this->endWidget(); ?>
	
</div>	
	
<script type="text/javascript">

	$(function(){
		jwizard.setDialogClass('multidomain-wizard-client-initial');
		
		var selector = '.domain-type-value.' + <?= json_encode($domainType) ?>;
 		$(selector).show();

		$('.wizard-client-initial input[type="radio"]').styler();

        $(document).on('click', 'a#manage-themes', function(){
            <?= Yii::app()->legacyWrapper->setHydraRoute('/manage/themes', false) ?>
        });

	});
	
	
</script>