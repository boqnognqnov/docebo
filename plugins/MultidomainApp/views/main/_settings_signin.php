<!-- SIGN IN PAGE -->
<?php
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'multidomain-settings-form',
		'method' => 'POST',
		'htmlOptions' => array(
			'enctype' 	=> 'multipart/form-data',
			'class' 	=> 'form-horizontal docebo-form'
		)
	));
?>

<div class="row-fluid setting-row">
	<div class="span12">
		<div class="setting-name span3">
			<span class="setting-title"><?= Yii::t('multidomain', 'Custom settings') ?></span>
		</div>
		<div class="setting-value span9">
			<label class="checkbox">
				<?php
					echo $form->checkBox($model, 'signInPageLayoutEnabled', array('class' => 'on-off-switch'));
					echo $model->getAttributeLabel('signInPageLayoutEnabled');
				?>
			</label>
		</div>
	</div>
</div>

<div class="blockable"> <!-- blockable -->

	<div class="row-fluid setting-row-wrapper odd">
	<div class="row-fluid setting-row">
		<div class="span12">
			<div class="setting-name span3">
				<span class="setting-title"><?= Yii::t('multidomain', 'Login restriction') ?></span>
				<p class="setting-description"><?=Yii::t('multidomain','Users associated to this Multidomain won\'t be able to access other Multidomain(s)')?></p>
			</div>
			<div class="setting-value span9">
				<label class="checkbox">
					<?php
					echo $form->checkBox($model, 'enableLoginRestriction', array('class' => 'on-off-switch'));
					echo $model->getAttributeLabel('enableLoginRestriction');
					?>
				</label>
			</div>
		</div>
	</div>
	</div>

	<div class="row-fluid setting-row">
		<div class="span12">
			<div class="setting-name span3">
				<span class="setting-title"><?php echo Yii::t('branding', 'Login form'); ?></span>
				<p class="description description-regular"><?php echo Yii::t('branding', 'Use this option if you want only login via Google,Okta,SAML,etc'); ?></p>
			</div>
			<div class="setting-value span9">
				<label class="checkbox">
					<?php echo $form->checkbox($model, 'hide_signin_form', array('class' => 'on-off-switch'));?>
					<?php echo Yii::t('branding', 'Show only SSO buttons and hide login form'); ?>
				</label>
			</div>
		</div>
	</div>

	<div class="row-fluid setting-row-wrapper odd blockable-layout" style="overflow: hidden; max-height: 390px;">
	<div class="row-fluid setting-row">
		<?php
		$catalog_external = Settings::get('catalog_external', 'off');

		// Make sure the catalog custom settings for the Multidomain client are applied(not Settings:Get doesn't work here ok !!!
		if ($model->catalog_custom_settings == 1) {
			$catalog_external = ($model->catalog_external == 1) ? 'on' : 'off';
		}

		if ($catalog_external == 'on') { ?>
<!--			<div class="row-fluid">-->
				<div class="span12" style="padding: 0px!important; margin: 0px!important; ">
					<?php
					$this->widget('common.widgets.warningStrip.WarningStrip',
						array(
							'message'=>Yii::t('branding',
								'All options below are not applicable when the Public Catalog is enabled in Catalog App settings page.'),
							'type'=>'warning'
						)
					);
					?>
					<script type="text/javascript">
						$('#multidomain-settings-sign-in-page .blockable-layout').block({
							message: "",
							timeout: 0,
							overlayCSS:  {
								backgroundColor: 	'#FFF',
								opacity:         	0.6,
								cursor:          	'auto'
							}
						});

						$('#multidomain-settings-sign-in-page .blockable-layout').css('max-height', '450px');
					</script>

				</div>
<!--			</div>-->
		<?php } ?>

		<div class="span12" style="margin-left: 0;">
			<div class="setting-name span3">
				<span class="setting-title"><?= Yii::t('login', '_HOMEPAGE') ?></span>
				<p class="setting-description"><?= Yii::t('branding', 'Sign in layout description') ?></p>
			</div>
			<div class="setting-value span9">
				<div class="row-fluid">
					<div class="span4">
						<label class="radio">
							<?php
								echo $form->radioButton($model, 'signInPageLayoutOption', array('value' => 'layout2', 'uncheckValue' => null));
								echo Yii::t('templatemanager', 'Top login form');
							?>
						</label>
						<label class="radio">
							<?php
								echo $form->radioButton($model, 'signInPageLayoutOption', array('value' => 'layout3', 'uncheckValue' => null));
								echo Yii::t('templatemanager', 'Bottom login form');
							?>
						</label>
						<label class="radio">
							<?php
								echo $form->radioButton($model, 'signInPageLayoutOption', array('value' => 'layout1', 'uncheckValue' => null));
								echo Yii::t('templatemanager', 'Side Login form');
							?>
						</label>
						<label class="radio">
							<?php
							echo $form->radioButton($model, 'signInPageLayoutOption', array('value' => 'layout4', 'uncheckValue' => null, 'id' => 'signInPageLayoutOptionMinimal'));
							echo Yii::t('templatemanager', 'Minimal Sign In form');
							?>
						</label>
					</div>
					<div class="span8">
						<div class="login-images">
							<div class="top layout2">
								<span class="background"><?php echo Yii::t('certificate', '_BACK_IMAGE'); ?></span>
								<span class="title-text"><?php echo Yii::t('branding', '_TITLE_TEXT_SECTION'); ?></span>
								<span class="external"><?php echo Yii::t('admin_webpages', '_WEBPAGES_CAPTION'); ?></span>
								<?php echo CHtml::image(Yii::app()->theme->baseUrl . '/images/loginlayouts/top.jpg'); ?>
							</div>
							<div class="bottom layout3">
								<span class="background"><?php echo Yii::t('certificate', '_BACK_IMAGE'); ?></span>
								<span class="text"><?php echo Yii::t('branding', '_TITLE_TEXT_SECTION'); ?></span>
								<span class="external"><?php echo Yii::t('admin_webpages', '_WEBPAGES_CAPTION'); ?></span>
								<?php echo CHtml::image(Yii::app()->theme->baseUrl . '/images/loginlayouts/bottom.jpg'); ?>
							</div>
							<div class="side layout1">
								<span class="background"><?php echo Yii::t('certificate', '_BACK_IMAGE'); ?></span>
								<span class="title-text"><?php echo Yii::t('branding', '_TITLE_TEXT_SECTION'); ?></span>
								<span class="external"><?php echo Yii::t('admin_webpages', '_WEBPAGES_CAPTION'); ?></span>
								<?php echo CHtml::image(Yii::app()->theme->baseUrl . '/images/loginlayouts/side.jpg'); ?>
							</div>
							<div class="minimal layout4">
								<span class="background"><?php echo Yii::t('certificate', '_BACK_IMAGE'); ?></span>
								<span class="title-text"><?php echo Yii::t('branding', '_TITLE_TEXT_SECTION'); ?></span>
								<?php echo CHtml::image(Yii::app()->theme->baseUrl . '/images/loginlayouts/minimal.jpg'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>

	<div class="row-fluid setting-row">
		<div class="span12">
			<div class="setting-name span3">
				<span class="setting-title"><?= Yii::t('login', '_HOMEPAGE') ?></span>
				<p class="setting-description description-regular"><?= Yii::t('branding', '_HOME_LOGIN_IMG_DESCRIPTION') ?></p>
				<p class="setting-description description-minimal" style="display: none"><?php echo Yii::t('branding', 'Select a color or upload image or video for the background'); ?></p>
			</div>
			<div class="setting-value span9">
				<div class="row-fluid values-regular">
					<div class="span6 text-center">
						<!-- Image selector -->
						<?php
							$this->widget('common.widgets.ImageSelector', array(
								'imageType'			=> CoreAsset::TYPE_LOGIN_BACKGROUND,
								'assetId' 			=> $model->signInPageImage,
								'imgVariant'		=> CoreAsset::VARIANT_ORIGINAL,
								'buttonText'		=> Yii::t('setup', 'Upload your background'),
								'buttonClass'		=> 'btn btn-docebo green big',
								'dialogId'			=> 'image-selector-modal-signin',
								'dialogClass'		=> 'image-selector-modal',
								'inputHtmlOptions'	=> array(
									'name'	=> 'CoreMultidomain[signInPageImage]',
									'id'	=> 'signin-image-id',
									'class'	=> 'image-selector-input',
								),
							));
						?>
						<!-- Image selector -->
					</div>
					<div class="span6">
						<br>
						<label class="radio">
							<?php
								$sub_array = array('900' => '900');
								if(Settings::get('catalog_external') == 'on')
									$sub_array = array('900' => '1156');
								if(empty($model->getAttribute('signInPageImageAspect'))) {
									$model->setAttribute('signInPageImageAspect', CoreMultidomain::IMAGE_ASPECT_FILL);
								}

								echo $form->radioButton($model, 'signInPageImageAspect', array('value' => CoreMultidomain::IMAGE_ASPECT_FILL, 'uncheckValue' => null));
								echo Yii::t('branding', '_HOME_IMG_POSITION_FILL', $sub_array);
							?>
						</label>
						<label class="radio">
							<?php
								echo $form->radioButton($model, 'signInPageImageAspect', array('value' => CoreMultidomain::IMAGE_ASPECT_TILE, 'uncheckValue' => null));
								echo Yii::t('branding', '_HOME_IMG_POSITION_TILE');
							?>
						</label>
					</div>
				</div>
				<div  class="values-minimal" style="display: none">
<!--					<div class="span6 text-center_">-->
					<?= CHtml::hiddenField('CoreMultidomain[signInMinimalType]', $model->signInMinimalType); ?>
					<?= CHtml::hiddenField('CoreMultidomain[signInMinimalBackground]', $model->signInMinimalBackground); ?>
					<?= CHtml::hiddenField('CoreMultidomain[signInMinimalVideoFallbackImg]', $model->signInMinimalVideoFallbackImg); ?>


<!--					</div>-->
					<?= CHtml::radioButton('CoreMultidomain[signInMinimalType]', (($model->signInMinimalType == 'color') ? true : false), array('id' => 'minimal_login_type_color', 'value' => 'color')); ?>
					<?= CHtml::label(Yii::t('branding', 'Color'), 'minimal_login_type_color'); ?> <br />

					<div class="color-selector colors">
						<div class="color-item">
							<div class="wrap">
								<?php
//									(strpos($model->signInMinimalBackground, '#') !== false) ? $model->signInMinimalBackground : '#FFFFFF',
								echo CHtml::textField('CoreMultidomain[signInMinimalBackground]',
										$model->signInMinimalBackground,
										array(
											'autocomplete' => 'off',
											'class' => 'color-tooltip',
									)) ?>
								<div class="tooltip farbtastic-tooltip" style="opacity: 1; display: none;">
									<div class="colorpicker"></div>
								</div>
								<div class="color-preview-wrap">
									<div class="color-preview" style="display: block; width: 21px; height: 21px; background-color: <?php echo ((strpos($model->signInMinimalBackground, '#') !== false) ? $model->signInMinimalBackground : '#FFFFFF'); ?>;"></div>
								</div>
							</div>
						</div>
						<p class="description" style="margin-top: -20px!important;"><?= Yii::t('branding', 'Insert HEX code or choose a color by clicking on the icon'); ?></p>
					</div>


					<?= CHtml::radioButton('CoreMultidomain[signInMinimalType]', (($model->signInMinimalType == 'image') ? true : false), array('id' => 'minimal_login_type_image', 'value' => 'image')); ?>
					<?= CHtml::label(Yii::t('branding', 'Full width background image'), 'minimal_login_type_image'); ?>
					<div class="minimal-layout-image-selector">
						<p class="description" style="margin-top: 0px; font-weight: normal;"><?= Yii::t('branding', 'Suggested image dimensions at least'); ?> 1280x720 px</p>
						<div id="container_minimal_bg_img">
							<div style="float: left; width: 100%; ">
								<a id="pickfiles_minimal_bg_img" style="float: left; margin-bottom: 10px;" class="pickfiles confirm-btn btn-docebo green big" href="javascript:void(0);"><?php echo Yii::t('organization', 'UPLOAD'); ?></a>
								<?= CHtml::hiddenField('CoreMultidomain[minimal_bg_img_path]'); ?>
								<?= CHtml::hiddenField('CoreMultidomain[minimal_bg_img_original_filename]'); ?>
								<div id="minimal_bg_img_file" style="display: inline-block; float: left; margin-left: 10px; vertical-align: middle; margin-top: 5px;"></div>
							</div>

							<div style="float: right; margin-right: 60px;">
								<?php if(strpos($model->signInMinimalBackground, '#') === false && $model->signInMinimalBackground <> '' && !is_null($model->signInMinimalBackground) && $model->signInMinimalType == 'image') {
									$bgImgAsset =  CoreAsset::model()->findByPk($model->signInMinimalBackground);
									$bgImgUrl = ($bgImgAsset) ? $bgImgAsset->getUrl() : '';
									if($bgImgUrl <> '') {
										?>
										<img id="minimal-layout-img-preview" src="<?=$bgImgUrl; ?>" style="outline: 1px solid #e4e6e5; padding: 1px; height: 100px; width: 185px; position:relative; top: -70px;" />
									<?php }
								} ?>
							</div>
						</div>
					</div> <br />

					<?= CHtml::radioButton('CoreMultidomain[signInMinimalType]', (($model->signInMinimalType == 'video') ? true : false), array('id' => 'minimal_login_type_video', 'value' => 'video')); ?>
					<?= CHtml::label(Yii::t('branding', 'Full width background video'), 'minimal_login_type_video'); ?>
					<div class="minimal-layout-video-selector" style="width: 100%; margin-left: 25px; display: none">
						<p class="description" style="margin-top: 0px; font-weight: normal;"><?= Yii::t('branding', 'Video formats supported MP4, H264. Suggested size 1,5 to 2 MB, bitrate 500 to 800 kbps, resolution 1280x720 at least'); ?></p>
						<div id="container_minimal_bg_video">
							<div style="float: left; width: 100%; ">
								<a id="pickfiles_minimal_bg_video" style="float: left" class="pickfiles confirm-btn btn-docebo green big" href="javascript:void(0);"><?php echo Yii::t('organization', 'UPLOAD'); ?></a>
								<?= CHtml::hiddenField('CoreMultidomain[minimal_bg_video_path]'); ?>
								<?= CHtml::hiddenField('CoreMultidomain[minimal_bg_video_original_filename]'); ?>
								<div id="minimal_bg_video_file" style="display: inline-block; float: left; margin-left: 60px; vertical-align: middle; margin-top: 5px; font-weight: bold;"><?php
									if ($model->signInMinimalBackground && (strpos($model->signInMinimalBackground, '#') === false) && ($model->signInMinimalType == 'video')) {
										$bgVideoAsset =  CoreAsset::model()->findByPk($model->signInMinimalBackground);
										if($bgVideoAsset) { echo $bgVideoAsset->original_filename; }
									}
									?></div>
							</div>

							<br />
							<br />
							<br />
							<label style="margin-left: 0;"><?= Yii::t('branding', 'Fallback image'); ?></label>
							<p class="description" style="margin-top: 0px; font-weight: normal;"><?= Yii::t('branding', 'In case of video is not supported or did not load this image will remain as fallback.'); ?></p>
							<div id="container_minimal_video_fallback_img">
								<div style="float: left; width: 100%;">
									<a id="pickfiles_minimal_video_fallback_img" style="float: left" class="pickfiles confirm-btn btn-docebo green big" href="javascript:void(0);"><?php echo Yii::t('organization', 'UPLOAD'); ?></a>
									<?= CHtml::hiddenField('CoreMultidomain[minimal_video_fallback_img_path]'); ?>
									<?= CHtml::hiddenField('CoreMultidomain[minimal_video_fallback_img_original_filename]'); ?>
									<div id="minimal_video_fallback_img_file" style="position: absolute; left: -10px; top: 30px; width: 130px; word-break: break-all;"></div>
								</div>
								<div style="float: right; margin-right: 60px; position: relative; top: -30px;">
									<?php if ($model->signInMinimalVideoFallbackImg <> '' && !is_null($model->signInMinimalVideoFallbackImg)) {
										$bgFallbackImgAsset =  CoreAsset::model()->findByPk($model->signInMinimalVideoFallbackImg);
										$bgFallbackImgUrl = ($bgFallbackImgAsset) ? $bgFallbackImgAsset->getUrl() : '';
										if($bgFallbackImgUrl <> '') {
											?>
											<img id="minimal-layout-fallback-img-preview" src="<?=$bgFallbackImgUrl; ?>" style="outline: 1px solid #e4e6e5; padding: 1px; height: 100px; width: 185px; position:relative; right: 250px;" />
										<?php }
									} ?>
								</div>
							</div>

						</div>
					</div><br />
				</div>



			</div>
		</div>
	</div>

	<div class="row-fluid setting-row-wrapper odd">
	<div class="row-fluid setting-row">
		<div class="span12">
			<div class="setting-name span3">
				<span class="setting-title"><?= Yii::t('branding', '_TITLE_TEXT_SECTION') ?></span>
				<p class="setting-description"><?= Yii::t('branding', '_TITLE_TEXT_SECTION_DESCRIPTION') ?></p>
			</div>
			<div class="setting-value span9">
				<div class="languages">
					<p><?php echo Yii::t('branding', '_PAGE_LANGUAGE_LABEL'); ?></p>
					<?php
						$tmp = $model->getSigninTextsTranslations();
						$languages = $tmp['languages'];
						$translations = $tmp['translations'];
						$translated = $tmp['translated'];
						$preselectedTextsLanguage = $tmp['preselectedTextsLanguage'];
					?>
					<div class="lang-wrapper">
						<?php
							$options = array();
							foreach ($languages as $langCode => $langName) {
								if (in_array($langCode, $translated)) {
									$options[$langCode] = array('data-translated' => "1");
								}
							}
							$htmlOptions = array(
								'size' => 1,
								'id' => 'signin-texts-lang-select',
								'data-prev-selection'	=> '0',
								'options'	=> $options,
							);
							echo CHtml::listBox('languages', $preselectedTextsLanguage, array('0' => Yii::t('adminrules', '_SELECT_LANG_TO_ASSIGN')) + $languages, $htmlOptions);
						?>
						<div class="stats">
							<p class="available"><?php echo Yii::t('admin_lang', '_LANG_ALL'); ?>: <span><?php echo count($languages); ?></span></p>
							<p class="translated-signin-texts"><?php echo Yii::t('standard', 'Filled languages'); ?>: <span></span></p>
						</div>
					</div>
					<div class="input-fields">
						<p><?php echo Yii::t('standard', '_TITLE'); ?></p>
						<div class="max-input">
							<?php echo CHtml::textfield('signin_title_input', '', array('class' => 'title')); ?>
						</div>
						<p><?php echo Yii::t('standard', '_TEXTOF'); ?></p>
						<?php echo CHtml::textarea('signin_text_input', '', array('class' => 'text', 'id' => 'signin-text-textarea')); ?>
					</div>
					<div class="signin-texts-translations" style="display: none;">
						<?php foreach ($translations as $langCode => $translation): ?>
							<div class="<?php echo $langCode ?>">
								<?php echo CHtml::textfield("SigninTitle[$langCode]", $translation['title'], array('class' => 'translation-title')); ?>
								<?php echo CHtml::textarea ("SigninText[$langCode]" , $translation['text'], array('class' => 'translation-text')); ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>

	<div class="row-fluid" id="show-hide-webpages-warning">
		<div class="span12">
			<?php $this->widget('common.widgets.warningStrip.WarningStrip',
				array(
					'message'=>Yii::t('branding',
						'Web pages are not supported in the "Minimal sign in form" home page layout. Please, switch to top, bottom or side sign in form layouts in order to enable them.'),
					'type'=>'warning'
				)
			); ?>
		</div>
	</div>

	<div class="row-fluid setting-row">
		<div class="span12">
			<div class="setting-name span3">
				<span class="setting-title"><?= Yii::t('admin_webpages', '_TITLE_WEBPAGES') ?></span>
				<p class="setting-description"><?= Yii::t('branding', '_EXTERNAL_PAGES_SECTION_DESCRIPTION') ?></p>
				</div>
				<div class="setting-value span9 blockable-pages">
					<div class="">
                    	<div class="row-fluid">
                        	<div class="span8">
                            	<h6><?php echo Yii::t('configuration', '_PAGE_TITLE'); ?></h6>
							</div>
                            <div class="span4">
								<?php
    	                       		echo CHtml::link(
        	                   			Yii::t('admin_webpages', 'Manage pages'),
            	               			Docebo::createAbsoluteUrl('admin:branding/index#signin'),
                	           			array('class'=>'btn btn-docebo green big pull-right')
                    	       		);
                        	   	?>
                            </div>
						</div>
                        <br>
						<?php
						$webPageModel = new LearningWebpage();
						//$webPageModel->publish = 1;
						$this->widget('zii.widgets.CListView', array(
							'id' => 'external-pages-management-list',
							'htmlOptions' => array('class' => 'list-view clearfix'),
							'dataProvider' => $webPageModel->dataProvider($model->id, true),
							'itemView' => '_view_webpages',
							'template' => '{items}',
							'ajaxUpdate' => true,
							'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
						));
						?>
				</div>
			</div>
		</div>
	</div>


</div> <!-- blockable -->


<br>
<div class="row-fluid">
	<div class="span12 text-right">
		<?php
			echo CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
				'name'	=> 'save_multidomain_settings',
				'class'	=> 'btn btn-docebo green big',
				'id'    => 'signin-save-btn',
			));
		?>
		<?php
		echo CHtml::link(Yii::t('standard', '_CANCEL'), Docebo::createAdminUrl('MultidomainApp'), array(
			'name'	=> 'save_multidomain_settings',
			'class'	=> 'btn btn-docebo black big',
		));
		?>
	</div>
</div>
<?php
	echo CHtml::hiddenField('selectedTab', $selectedTab, array('id' => 'selectedTab', 'class' => 'selectedTab'));
	echo CHtml::hiddenField('settings_tab_pane', CoreMultidomain::GROUPSETTING_SIGNIN);

	$this->endWidget();
?>
<script type="text/javascript">
	/*<![CDATA[*/

	(function ($) {
		$(function () {
			var minImgUploader = false;
			var minVideoUploader = false;
			var minVideoFallbackUploader = false;
			var currentBgAsset = '';

			function addSignInTabUploadBtns(uploadBtnId, extensions) {
				var allFilesSize = 0; //Current files size;
				var maxFilesSize = 10*1024*1024; //10MB
				var upUrl =  yii.urls.base + '/index.php?r=branding/handleFile&thread_id=47';

				var uploader = new plupload.Uploader({
					chunk_size: '10mb',
					runtimes: 'html5,flash,html4,gears,silverlight,browserplus',
					browse_button: 'pickfiles_'+ uploadBtnId, //minimal_bg_video',//'pickfiles',
					container: 'container_'+ uploadBtnId, //'container',
					max_file_size: '10mb',
					url : upUrl,
					flash_swf_url: yii.urls.base + '/../common/extensions/plupload/assets/plupload.flash.swf',
					silverlight_xap_url: '/plupload/js/plupload.silverlight.xap',
					multipart_params: {
						YII_CSRF_TOKEN: $('input[name=YII_CSRF_TOKEN]').val(),
						fileId: '',
						extensions: extensions,
					}
				});

				uploader.bind('Init', function (up, params) {
					//$('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
				});

				uploader.init();

				uploader.bind('BeforeUpload', function (up, files) {
				});

				uploader.bind('FilesAdded', function (up, files) {

					$('#fileserror').html("");
					$.each(files, function (i, file) {

						//Calculate new total files size!
						allFilesSize += file.size;

						//If we outweigh the files limits!
						if(allFilesSize > maxFilesSize){
							$('#fileserror').append([
								'<div>Error: '+Yii.t('forum', 'Max. file size {size} MB', {'{size}': 3}) + '</div>'
							].join(''));

							//Remove the uploaded file and change the files size!
							uploader.removeFile(file);
							allFilesSize -= file.size;
						} else {
							var fileSize = '';
							if (file.size != undefined) {
								fileSize = '(' + plupload.formatSize(file.size) + ')';
							}

							// Add file id as additional param in order to be used for temp file name(it is an unique hash)
							uploader.settings.multipart_params.fileId = file.id;

							$('#'+uploadBtnId+'_file').html(file.name+'&nbsp;<i class="fa fa-refresh fa-spin"></i>');
							$('input[name="CoreMultidomain['+uploadBtnId+'_path]"]').val(file.id);
							$('input[name="CoreMultidomain['+uploadBtnId+'_original_filename]"]').val(file.name);
						}
					});

					up.refresh(); // Reposition Flash/Silverlight

					uploader.start();
				});

				uploader.bind('UploadProgress', function (up, file) {
				});

				uploader.bind('Error', function (up, err) {
					Docebo.Feedback.show('error', Yii.t('app7020', 'Error') + ': ' + err.message);

					up.refresh(); // Reposition Flash/Silverlight
				});

				uploader.bind('FileUploaded', function (up, file, response) {
					try {
						$('#'+uploadBtnId+'_file .fa-refresh').remove();
						response = jQuery.parseJSON(response.response);

						if (response.error.code == '500'){

							Docebo.Feedback.show('error', Yii.t('app7020', 'Error') + ': ' + response.error.message);

							file.status = plupload.FAILED;


							$('#'+uploadBtnId+'_file').html('&nbsp;');
							$('input[name="CoreMultidomain['+uploadBtnId+'_path]"]').val('');
							$('input[name="CoreMultidomain['+uploadBtnId+'_original_filename]"]').val('');
						}
						else {
							// Success
							file.status = plupload.DONE;
						}
					}
					catch (e) {
						Docebo.log('Code Error: ' + e);
					}

					//$('#filelist .filename .fa-refresh').remove();
				});

				return uploader;
			}

			// --- COLORPICKER START

			// Init the color picker tooltip
			$('#multidomain-settings-form .color-tooltip').tooltip({
				title: function() {
					return $(this).closest('.wrap').find('.farbtastic-tooltip').html();
				},
				trigger:  'focus',
				html: true
			});

			// Init farbtastic picker
			$('#multidomain-settings-form .color-tooltip').on('shown.bs.tooltip', function() {
				// Add color picker
				$('.colors .color-item input').each(function() {
					var _this = $(this);
					var _preview = $(this).parents('.wrap').find('.color-preview');

					var f = $.farbtastic($(this).parents('.wrap').find('.colorpicker'), function(data) {
						_this.val(data);
						_preview.css('background-color', data);
					})

					// Initializes colorpicker color
					f.setColor($(this).val());

					$(this).on('keyup', farbtasticUpdateCallback(f, $(this)));
				});
			});

			$('.colors').on('click', '.color-preview', function() {
				$(this).parents('.wrap').find('input').focus();
			});

			// --- COLORPICKER END

			// Add PlUpload to the upload buttons here
			if (!minImgUploader) {
				minImgUploader = addSignInTabUploadBtns('minimal_bg_img', ['png', 'jpg', 'jpeg', 'gif']);
			}

			if (!minVideoUploader) {
				minVideoUploader = addSignInTabUploadBtns('minimal_bg_video', ['mp4']);
			}

			if (!minVideoFallbackUploader) {
				minVideoFallbackUploader = addSignInTabUploadBtns('minimal_video_fallback_img', ['png', 'jpg', 'jpeg', 'gif']);
			}

			function doShowHideMinimalSubOptions() {
				var colorType = $('#minimal_login_type_color').is(':checked');
				var imgType = $('#minimal_login_type_image').is(':checked');
				var videoType = $('#minimal_login_type_video').is(':checked');

				if (colorType) {
					var currentColor = $('input[name="CoreMultidomain[signInMinimalBackground]"]').val();
					if (currentColor.indexOf("#") === -1) {
						currentBgAsset = currentColor;
						// Set default color
						$('input[name="CoreMultidomain[signInMinimalBackground]"]').val('#FFFFFF');
					}

					$('.color-selector').show();
					$('.minimal-layout-image-selector').hide();
					$('.minimal-layout-video-selector').hide();
				} else if (imgType) {
					var currentBg = $('input[name="CoreMultidomain[signInMinimalBackground]"]').val();
					if (currentBgAsset !== '' && currentBg.indexOf('#') !== -1) {
						$('input[name="CoreMultidomain[signInMinimalBackground]"]').val(currentBgAsset);
					}

					$('.color-selector').hide();
					$('.minimal-layout-image-selector').show();
					$('.minimal-layout-video-selector').hide();
				} else {
					var currentBg = $('input[name="CoreMultidomain[signInMinimalBackground]"]').val();
					if (currentBgAsset !== '' && currentBg.indexOf('#') !== -1) {
						$('input[name="CoreMultidomain[signInMinimalBackground]"]').val(currentBgAsset);
					}

					$('.color-selector').hide();
					$('.minimal-layout-image-selector').hide();
					$('.minimal-layout-video-selector').show();
				}
			}

			function doShowHideMinimalOptions() {
				if ($('#signInPageLayoutOptionMinimal').is(':checked')) { // Minimal sign in page layout is selected
					// Disable Web pages
					$('#multidomain-settings-sign-in-page .blockable-pages').block({
						message: "",
						timeout: 0,
						overlayCSS:  {
							backgroundColor: 	'#FFF',
							opacity:         	0.6,
							cursor:          	'auto'
						}
					});

					// Hide regular layout options
					$('.description-regular').hide();
					$('.values-regular').hide();

					// Show minimal options
					$('.description-minimal').show();
					$('.values-minimal').show();
					$('#show-hide-webpages-warning').show();
				} else {
					// Enable Web pages
					$('#multidomain-settings-sign-in-page .blockable-pages').unblock();

					// Hide minimal options
					$('.description-minimal').hide();
					$('.values-minimal').hide();
					$('#show-hide-webpages-warning').hide();

					// Show regular layout options
					$('.description-regular').show();
					$('.values-regular').show();
				}
			}

			doShowHideMinimalOptions();
			doShowHideMinimalSubOptions();

			$('input[name="CoreMultidomain[signInPageLayoutOption]"]').on('change', function() {
				doShowHideMinimalOptions();
				doShowHideMinimalSubOptions();
			});

			$('input[name="CoreMultidomain[signInMinimalType]"]').on('change', function() {
				doShowHideMinimalSubOptions();
			});

			// On saving the Sign In settings for multidomain
			$('#signin-save-btn').on('click', function(e) {
				var radioChecked = $('input[name="CoreMultidomain[signInPageLayoutOption]"]:checked');

				// If minimal layout is chosen !!!
				if (radioChecked.val() == 'layout4') {
					// If there is no image selected !!!
					if ($('#minimal_login_type_image').is(':checked')) {
						if($('#minimal-layout-img-preview').length == 0 && $('#CoreMultidomain_minimal_bg_img_original_filename').val() == '') {
							// Display error
							Docebo.Feedback.show('error', Yii.t('app7020', 'Error') + ': ' + Yii.t('branding', 'Please, select an image for the background'));
							e.preventDefault();
							return false;
						}
					}

					// If there is no video selected - minimal-layout-fallback-img-preview
					if ($('#minimal_login_type_video').is(':checked')) {
						if($('#minimal_bg_video_file').html() == '' && $('#CoreMultidomain_minimal_bg_video_original_filename').val() == '') {
							// Display error
							Docebo.Feedback.show('error', Yii.t('app7020', 'Error') + ': ' + Yii.t('branding', 'Please, select a video for the background'));
							e.preventDefault();
							return false;
						}

						//
						if($('#minimal-layout-fallback-img-preview').length == 0 && $('#CoreMultidomain_minimal_video_fallback_img_original_filename').val() == '') {
							// Display error
							Docebo.Feedback.show('error', Yii.t('app7020', 'Error') + ': ' + Yii.t('branding', 'Please, select a fallback image for the background'));
							e.preventDefault();
							return false;
						}
					}
				}
			});

		});
	})(jQuery);

	/*]]>*/
</script>