<?php 

	$fancyTreeData = CoreOrgChartTree::buildFancytreeDataArray();
	$clientName = '';
	if (isset($wizardData['name'])) {
		$clientName = $wizardData['name'];
	}  
	else if ($model->name) {
		$clientName = $model->name;
	}

?>


<h1><?= Yii::t('multidomain', 'Create/Edit Client') ?></h1>




<div class="form">

	<?php 
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'client-step-orgchart-form',
			'htmlOptions' => array(
				'class' => 'ajax form-horizontal jwizard-form',
				'name' => 'client-step-orgchart-form'
			),
		));
		
		// Wizard related
		echo CHtml::hiddenField('from_step', 'step_orgchart');
		
	?>
	

	<div class="row-fluid">
		<p><?= Yii::t('multidomain', 'Select the branch to associate with "<strong>{client}</strong>"', array('{client}' => $clientName)) ?></p>
		<?php if (Yii::app()->user->hasFlash('error')) : ?>
			<div class="row-fluid">
				<div class='alert alert-error alert-compact'>
					<?= Yii::app()->user->getFlash('error'); ?>
				</div>
			</div>
		<?php endif; ?>
		<br>
	</div>

	<div class="row-fluid">
		<?php
			$this->widget('common.extensions.fancytree.FancyTree', array(
				'id' 			=> 'orgcharts-fancytree',
				'treeData' 		=> $fancyTreeData,
				'selectMode'	=> FancyTree::SELECT_MODE_SINGLE,
				'formFieldNames' => array(
					'selectedInputName'	 		=> 'orgcharts_selected',
					'activeInputName' 			=> 'orgcharts_active_node',
					'selectModeInputName'		=> 'orgcharts_select_mode',
				),
				'preSelected'					=> $preSelected,	
			));
		?>
	</div>
	

	<!-- For Dialog2 - they go in footer -->
	<div class="form-actions jwizard">
		
		<?php
			echo CHtml::submitButton(Yii::t('standard', '_PREV'), array(
				'class'  => 'btn btn-docebo green big jwizard-nav',
				'name'	=> 'prev_button',
			));
		
			echo CHtml::submitButton(Yii::t('standard', '_NEXT'), array(
      			'class'  => 'btn btn-docebo green big jwizard-nav',
      			'name'	=> 'next_button', 
   			));
		?>
		<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
	</div>
	
	
	<?php $this->endWidget(); ?>
	
</div>	

<script type="text/javascript">

	$(function(){
		$(document).trigger('notify-jwizard-restore-data', {fieldNames: ['orgcharts_selected-json']});
	});	
	
		
</script>
