<?php
	$clientName = '';
	if (isset($wizardData['name'])) {
		$clientName = $wizardData['name'];
	}
	else if ($model->name) {
		$clientName = $model->name;
	}
?>
<h1><?= Yii::t('multidomain', 'Create/Edit Client') ?></h1>

<div class="form report-success">

	<?php 
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'client-step-final-form',
			'htmlOptions' => array(
				'class' => 'ajax form-horizontal jwizard-form',
				'name' => 'client-step-final-form'
			),
		));
		
		// Wizard related
		echo CHtml::hiddenField('from_step', 'step_final');
		
	?>

	
	
	<div class="row-fluid">
	
		<div class="span3">
			<div class="text-center success success-text">
				<div class="copy-success-icon"></div>
				<div class=""></div><?php echo Yii::t('standard', '_OPERATION_SUCCESSFUL'); ?>
			</div>
		</div>
		
		<div class="span9">
		
			<div class="row-fluid success-question row-with-border">
				<div class="span12">
					<?php echo Yii::t('standard', 'What do you want to do next?'); ?>
				</div>
			</div>
			
			<div class="row-fluid success-action row-with-border">
				<div class="span7 text-left">
					<?php echo Yii::t('multidomain', 'I want to <strong>save & set up "{client}</strong>"', array('{client}' => $clientName)); ?>
				</div>
				<div class="span5 text-right">
					<?php
						echo CHtml::submitButton(Yii::t('standard', 'Set Up'), array(
							'name' => 'finish_wizard_setup',
							'class' => 'btn btn-docebo green big jwizard-finish',
						));
					?>
				</div>
			</div>

			<div class="row-fluid success-action">
				<div class="span7 text-left">
					<?php echo Yii::t('multidomain', 'I want to <strong>save and go back to list</strong>'); ?>
				</div>
				<div class="span5 text-right">
					<?php
						echo CHtml::submitButton(Yii::t('standard', 'Done'), array(
							'name' => 'finish_wizard_back',
							'class' => 'btn btn-docebo green big jwizard-finish',
						));
					?>
				</div>
			</div>
			
			
			
		
		</div>
		
	</div>
	
	

	<!-- For Dialog2 - they go in footer -->
	<div class="form-actions jwizard">
		
		<?php
			echo CHtml::submitButton(Yii::t('standard', '_PREV'), array(
				'class'  => 'btn btn-docebo green big jwizard-nav',
				'name'	=> 'prev_button',
			));
		?>
		<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
	</div>
	
	
	<?php $this->endWidget(); ?>
	
</div>

<script type="text/javascript">
	$('input[name="finish_wizard_setup"], input[name="finish_wizard_back"]').on('click', function(e){
		var button = $(e.target);

		if(button.hasClass('disabled')) {
			e.preventDefault();
		} else {
			button.addClass('disabled');
		}
	});
</script>