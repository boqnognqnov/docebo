<?php

/**
 * Plugin for MultidomainApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class MultidomainApp extends DoceboPlugin {
	
	// Override parent 
	public static $HAS_SETTINGS = false;

	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'MultidomainApp';

	/**
	 * @var bool The plugin should set this variable to true if the plugin should be loaded also on the mobile/ version
	 */
	public $is_mobile_capable = true;
	
	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {
		parent::activate();
	}


	public function deactivate() {
		parent::deactivate();
	}


}
