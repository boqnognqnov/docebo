<?php
/**
 * Multidomain related event listeners
 *  
 */
class MultidomainListeners extends CComponent {


	public static $client;

	/**
	 * Holds the resolved multidomain client
	 * 
	 * @var CoreMultidomain
	 */
	private $_clientModel = false;


	/**
	 * This is used to make sure that at every request the client data stored in session is properly refreshed from
	 * database, in case something has changed while the user is still logged in
	 *
	 * @var bool
	 */
	private static $_refreshed = false;


	/**
	 * Constructor
	 */
	public function __construct() {
		if (Yii::app() instanceof CWebApplication) {
			$this->_clientModel = CoreMultidomain::resolveClient();
		}

		if (!self::$client) {
			self::$client = CoreMultidomain::model()->findByPk($this->_clientModel->id);
		}

		//make sure to have properly updated data, do the refresh just the first time this is instantiated in the current request
		if (!self::$_refreshed) {
			if (is_subclass_of($this->_clientModel, 'CActiveRecord')) { $this->_clientModel->refresh(); }
			if (is_subclass_of(self::$client, 'CActiveRecord')) { self::$client->refresh(); }
			self::$_refreshed = true;
		}
	}

	/**
	 * Listen for an event and act accordingly:
	 *
	 * @param DEvent $event
	 */
	public function onHighlightMultidomainNodes(DEvent $event) {
		$sourceData = & $event->params['sourceData'];

		// Check if this is a selector tree (checkboxes)
		$firstItem = reset($sourceData);
		if(isset($firstItem['selectable']))
			return;

		// Go through the list of domain nodes and mark them as special
		$allIdOrgs = array_keys($sourceData);
		if(!empty($allIdOrgs)) {
			$clients = CoreMultidomain::model()->findAllBySql("SELECT * from core_multidomain WHERE org_chart IN (" . implode(",", $allIdOrgs) . ")");
			foreach ($clients as $client) {
				/* @var $client CoreMultidomain */
				$sourceData[$client->org_chart]['customCssClasses'] = 'multidomain';
				$sourceData[$client->org_chart]['title'] .= '<span class="multidomain-client"> <span>|</span> ' . $client->getClientUri() . '</span>';
			}
		}
	}

	/**
	 * Listen for an event and act accordingly:
	 *
	 * @param DEvent $event
	 */
	public function onRenderOrgChartTreeNodeMenu(DEvent $event) {
		if (Yii::app()->user->getIsGodadmin()) {
			$nodeId = $event->params['id'];
			$multiDomainClient = CoreMultidomain::model()->findByAttributes(array('org_chart' => $nodeId));
			if($multiDomainClient) {
				$html = '<li class="node-action">' .
					CHtml::link(Yii::t('domainbranding', 'Edit branding template'), Docebo::createAppUrl('admin:MultidomainApp/main/settings', array('id' => $multiDomainClient->id)), array(
						'class' => 'edit-branding',
						'onClick' => 'window.location = this.href'
					)) . '</li>'
					. '<li class="node-action"><hr/></li>';
				echo $html;
			}
		}

		return true;
	}


	/**
	 * Listen for an event and act accordingly:
	 *
	 * @param DEvent $event
	 */
	public function onGetLogoPath(DEvent $event) {
		if ($this->_clientModel) {
			$url = CoreAsset::url($this->_clientModel->companyLogo, CoreAsset::VARIANT_ORIGINAL);
			$event->params['logo_image_url'] = $url && !$event->params['master_logo'] ? $url : $event->params['logo_image_url'];   
		}
	}
	
	/**
	 * Listen for an event and act accordingly:
	 *
	 * @param DEvent $event
	 */
	public function onGetFaviconPath(DEvent $event) {
		if ($this->_clientModel) {
			$url = CoreAsset::url($this->_clientModel->favicon, CoreAsset::VARIANT_ORIGINAL);
			$event->params['favicon_url'] = $url && !$event->params['master_favicon'] ? $url : $event->params['favicon_url'];
		}
	}
	
	/**
	 * Listen for an event and act accordingly:
	 *
	 * @param DEvent $event
	 */
	public function onGetLoginImage(DEvent $event) {
		if ($this->_clientModel && $this->_clientModel->signInPageLayoutEnabled) {
			$aspect = $this->_clientModel->signInPageImageAspect;
			/*
			switch ($aspect) {
				case CoreMultidomain::IMAGE_ASPECT_FILL:
					$variant = (Settings::get('catalog_external', 'off') == 'on'
						? CoreAsset::VARIANT_STRETCHED_LARGE
						: CoreAsset::VARIANT_STRETCHED);
					break;
				case CoreMultidomain::IMAGE_ASPECT_TILE:
					$variant = CoreAsset::VARIANT_ORIGINAL;
					break;
				default:
					$variant = CoreAsset::VARIANT_ORIGINAL;
					break;
			}
			*/
			$variant = CoreAsset::VARIANT_ORIGINAL;// NOTE: the background size in resolved via CSS
			$url = CoreAsset::url($this->_clientModel->signInPageImage, $variant);
			$event->params['login_image']['src'] = $url ? $url : $event->params['login_image']['src'];
			$event->params['login_image']['aspect'] = $aspect ? $aspect : $event->params['login_image']['aspect'];
		}
	}

	/**
	 * Listen for an event and act accordingly:
	 * Return Player Background image URL for the Client
	 *
	 * @param DEvent $event
	 */
	public function onGetPlayerBgImage(DEvent $event) {
		if ($this->_clientModel && $this->_clientModel->coursePlayerEnabled) {
			$url 		= CoreAsset::url($this->_clientModel->coursePlayerImage, CoreAsset::VARIANT_ORIGINAL);
			$aspect 	= $this->_clientModel->coursePlayerImageAspect;
			$event->params['player_bg_image']['src'] 	= $url 		? $url 		: $event->params['player_bg_image']['src'];
			$event->params['player_bg_image']['aspect'] = $aspect 	? $aspect 	: $event->params['player_bg_image']['aspect'];
				
		}
	}
	
	
	/**
	 * Listen for an event and act accordingly:
	 * Return White Label settings for the Client
	 *
	 * @param DEvent $event
	 */
	public function onGetWhiteLabelSettings(DEvent $event) {
		$cm = CoreMultidomain::resolveWhiteLabelClient();
		$currentLang = $_SESSION['current_lang'];

		 if ($cm && $cm->whiteLabelEnabled && PluginManager::isPluginActive('WhitelabelApp')){
			$event->params['whitelabel_footer'] 		= $cm->whiteLabelPoweredByOption 		? $cm->whiteLabelPoweredByOption : $event->params['whitelabel_footer'];  
			$event->params['whitelabel_footer_text'] 	= $cm->whiteLabelPoweredByCustomText 	? $cm->whiteLabelPoweredByCustomText : $event->params['whitelabel_footer_text'];
			$event->params['whitelabel_naming_text'] 	= $cm->replaceDoceboCustomWord 			? $cm->replaceDoceboCustomWord : $event->params['whitelabel_naming_text'];
			$event->params['whitelabel_naming_site'] 	= $cm->replaceDoceboCustomLink 			? $cm->replaceDoceboCustomLink : $event->params['whitelabel_naming_site'];
			$event->params['whitelabel_helpdesk'] 		= $cm->whiteLabelContactOption 			? $cm->whiteLabelContactOption : $event->params['whitelabel_helpdesk'];
			$event->params['whitelabel_helpdesk_email']	= $cm->whiteLabelContactCustomEmail 	? $cm->whiteLabelContactCustomEmail : $event->params['whitelabel_helpdesk_email'];
			$event->params['whitelabel_naming'] 		= $cm->replaceDoceboWordOption;
			$event->params['whitelabel_disable_naming']	= $cm->whiteLabelNamingOverride;

			// Header settings
			$event->params['whitelabel_header'] = $cm->whiteLabelHeader ? $cm->whiteLabelHeader : $event->params['whitelabel_header'];

			// Footer iframe height
			$whiteLabelFooterExtHeight = $cm->whiteLabelFooterExtHeight;
			$event->params['whitelabel_footer_ext_height'] = !empty($whiteLabelFooterExtHeight) ? $whiteLabelFooterExtHeight : (CoreMultidomain::WL_FOOTER_IFRAME_HEIGHT);

			// Header iframe height
			$whiteLabelHeaderExtHeight = self::$client->whiteLabelHeaderExtHeight;
			$event->params['whitelabel_header_ext_height'] = !empty($whiteLabelHeaderExtHeight) ? $whiteLabelHeaderExtHeight : (CoreMultidomain::WL_HEADER_IFRAME_HEIGHT);

			// Header / Footer iframe URLs
			$event->params['whitelabel_footer_ext_url'] = $cm->whiteLabelFooterExtUrl ? $cm->whiteLabelFooterExtUrl : '';
			$event->params['whitelabel_header_ext_url'] = $cm->whiteLabelHeaderExtUrl ? $cm->whiteLabelHeaderExtUrl : '';

			$this->getCustomSettingsForDomain($event, $cm, $currentLang);
		}

		// custom settings is off, so we get the main LMS footer
		if ($cm && $cm->whiteLabelEnabled == 0 && PluginManager::isPluginActive('WhitelabelApp')) {
			$mainDomainSettings = Yii::app()->db->createCommand()
					->select()->from(CoreLangWhiteLabelSetting::model()->tableName())
					->where('language=:lang AND type=:type', array(':lang' => $currentLang, ':type' => Settings::get('whitelabel_footer')))->queryRow();
			if (empty($mainDomainSettings) || empty($mainDomainSettings['value']) || $mainDomainSettings['value'] == 'http://') {

				// we have empty entry, or don't have value for this language yet, so we get the text and url for default language
				$mainDomainSettings = Yii::app()->db->createCommand()
						->select()->from(CoreLangWhiteLabelSetting::model()->tableName())
						->where('language=:lang AND type=:type', array(':lang' => CoreLangLanguage::getDefaultLanguage(), ':type' => Settings::get('whitelabel_footer')))->queryRow();
			}

			switch ($mainDomainSettings['type']) {
				case CoreLangWhiteLabelSetting::TYPE_FOOTER_TEXT:
					if (Settings::get('whitelabel_naming_site_enable') == 1) {
						$siteReplacement = Settings::get('whitelabel_naming_site');
						if (strpos($siteReplacement, 'http://') !== false) $siteReplacement = substr($siteReplacement, 7);
						$mainDomainSettings['value'] = str_replace('www.docebo.com', $siteReplacement, $mainDomainSettings['value']);
					}
					$event->params['whitelabel_footer_text'] = $mainDomainSettings['value'];
					break;
				case CoreLangWhiteLabelSetting::TYPE_FOOTER_URL:
					$event->params['whitelabel_footer_ext_url'] = $mainDomainSettings['value'];
					break;
			}
		}
	}
	
	/**
	 * Listen for an event and act accordingly:
	 * Return Client's layout
	 *
	 * @param DEvent $event
	 */
	public function onBeforeLoginPageRender(DEvent $event) {
		if ($this->_clientModel && $this->_clientModel->signInPageLayoutEnabled) {
 			$layout = $this->_clientModel->signInPageLayoutOption;
 			$event->params['login_layout']	= $layout ? $layout : $event->params['login_layout'];
		}
	}

	/**
	 * Listen for an event and act accordingly:
	 * Return translatee Sign In Text & Caption
	 *
	 * @param DEvent $event
	 */
	public function onGetSigninTitleOrCaption(DEvent $event) {
		
		if ($this->_clientModel && $this->_clientModel->signInPageLayoutEnabled) {
			
			$currentLangCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
			$texts = CoreMultidomainSigninText::model()->findByAttributes(array('id_multidomain' => $this->_clientModel->id, 'lang_code' => $currentLangCode));
			if ($texts) {
					$event->params['signin_title'] = Yii::t('login',$texts->title);
					$event->params['signin_caption'] = Yii::t('login',$texts->text);
			} 
			else
			{
				$event->params['signin_title'] = '';
				$event->params['signin_caption'] = '';
		}
	}
	}
	
	/**
	 * Listen for an event and act accordingly:
	 * Filter out signin-web-pages not selected for the Client
	 *
	 * @param DEvent $event
	 */
	public function onFilterSigninPages(DEvent $event) {
		$event->preventDefault();

		if ($this->_clientModel && $this->_clientModel->signInPageLayoutEnabled && !empty($event->params['signin_pages'])) {
			
			// Get list of model of web pages enabled for this client, then make an array of IDs
			$clientPageModels = CoreMultidomainWebpage::model()->findAllByAttributes(array('id_multidomain' => $this->_clientModel->id));
			$pageIds = array();
				
			if ($clientPageModels) {
				foreach ($clientPageModels as $pageModel) {
					$pageIds[] = $pageModel->id_webpage;
				}
			}
			
			// Now UNSET the incoming page from the list (caller made) if page is NOT enabled for this client
			$signinPages = $event->params['signin_pages'];
			foreach ($signinPages as $k => $learningWebpageTranslation) {
				if (!in_array($learningWebpageTranslation->page->id, $pageIds)) {
					unset($signinPages[$k]);
				}
			}
			$event->params['signin_pages'] = $signinPages;
		}elseif(!empty($event->params['signin_pages'])){
			$signinPages = $event->params['signin_pages'];
			foreach ($signinPages as $k => $learningWebpageTranslation) {
				if(!isset($learningWebpageTranslation->page->publish) || ($learningWebpageTranslation->page->publish != 1) ){
					unset($signinPages[$k]);
				}

			}
			$event->params['signin_pages'] = $signinPages;
		}
	}
	

	/**
	 * Listen for an event and act accordingly:
	 * Assign passed user to the Multidomain CLient branch
	 *  
	 * @param DEvent $event
	 */
	public function onAfterUserSelfRegisterSave(DEvent $event) {
		if ($this->_clientModel && $this->_clientModel->org_chart) {
			/* @var $userTemp CoreUserTemp */
			$userTemp = $event->params['coreUserTemp']; 
		
			// Check if the user use a reg_code and is already assigned to a node
			$regcode = Yii::app()->request->getParam('reg_code');
			if(!$regcode) {
				$orgChart = CoreOrgChartTree::model()->findByPk($this->_clientModel->org_chart);
				if($orgChart) {
					// Org chart exists, get groups associated
					// with it and subscribe the temp user to them
					$c = new CDbCriteria();
					$c->addInCondition('idst', array($orgChart->idst_oc, $orgChart->idst_ocd));
					$groups = CoreGroup::model()->findAll($c);
					if($groups){
						foreach($groups as $group){
							// Remember to use the 'self-register' scenario
							// to avoid triggering enrollment rules
							// which won't work on temporary user IDs (the
							// user is still at core_user_temp until he is approved
							// by admin or confirms his email (on "free" self-registration mode)
							$exists = CoreGroupMembers::model()->exists('idst = :idst AND idstMember = :idstMember', array(
								'idst' => $group->idst,
								'idstMember' => $userTemp->idst
							));
							if(!$exists) {
								$addMember = new CoreGroupMembers('self-register');
								$addMember->idst = $group->idst;
								$addMember->idstMember = $userTemp->idst;
								$addMember->save();
							}
						}
					}
				}
			}
		}
	}
	
	
	/**
	 * Listen for an event and act accordingly:
	 * When BASE Yii controller init() method is started, return various Multidomain related settings or just execute some global actions
	 * 
	 * @param DEvent $event
	 */
	public function onBaseControllerInitStarted(DEvent $event) {
		if ($this->_clientModel) {

			// Web Site page title
			if ($this->_clientModel->pageTitle)
			$event->params['page_title'] = $this->_clientModel->pageTitle;
			
			// Use Client's Color scheme
			if ($this->_clientModel->colorScheme)
				Yii::app()->session['selected_color_scheme'] = $this->_clientModel->colorScheme;
			
		}
	}
	
	/**
	 * Listen for an event and act accordingly:
	 *
	 * @param DEvent $event
	 */
	public function onResolvingCustomCss(DEvent $event) {
		if ($this->_clientModel) {
			if ($this->_clientModel->customCss)
				$event->params['custom_css_content'] = $this->_clientModel->customCss;
		}
	}
	
	/**
	 * Listen for an event and act accordingly:
	 *
	 * @param DEvent $event
	 */
	public function onResolvingCoursePlayerLayout(DEvent $event) {
		if ($this->_clientModel && $this->_clientModel->coursePlayerEnabled) {
			if ($this->_clientModel->coursePlayerViewLayout)
				$event->params['player_layout'] = $this->_clientModel->coursePlayerViewLayout;
		}
	}
	
	/**
	 * Listen for an event and act accordingly:
	 *
	 * @param DEvent $event
	 */
	public function onResolvingCoursePlayerHtmlLoCss(DEvent $event) {
		if ($this->_clientModel && $this->_clientModel->coursePlayerEnabled) {
			if ($this->_clientModel->coursePlayerHtmlCss)
				$event->params['player_html_lo_css'] = $this->_clientModel->coursePlayerHtmlCss;
		}
	}
	
	/**
	 * Listen for an event and act accordingly:
	 * Currently, this event is raised in Settings::get().
	 * What we do is to intercept this call, raise an event and allow this App to change returned Core Settings.
	 * Pretty sensitive and probably dangerous operation!
	 * 
	 * @see Settings::get()
	 *
	 * @param DEvent $event
	 */
	public function onGetCoreSetting(DEvent $event) {

		if ($this->_clientModel) {
			$paramName = isset($event->params['param_name']) ? trim($event->params['param_name']) : false;

			if ($paramName) {
				
				// Ecommerce Related
				// NOTE: we completely override "master" settings, to avoid "security issues"!
				// Meaning, once "Multidomain E-Commerce" is enabled, user MUST enter ALL required information
				// or face the consequences, like failing payments etc. No default "master" values are used.
				if ($this->_clientModel->ecommerceEnabled) {

					switch ($paramName) {

						// PAYPAL
						case 'currency_symbol':
							$event->params['value'] = $this->_clientModel->currencySymbol;
							break;
							
						case 'currency_code':
							$event->params['value'] = $this->_clientModel->currency;
							break;
								
						case 'paypal_enabled':
							$event->params['value'] = $this->_clientModel->payPalEnabled ? 'on' : 'off';
							break;
								
						case 'paypal_mail':
							$event->params['value'] = $this->_clientModel->payPalAccount;
							break;
								
						case 'paypal_sandbox':
							$event->params['value'] = $this->_clientModel->payPalSandbox ? 'on' : 'off';
							break;

							
						// AUTHORIZE	
						case 'authorize_enabled':
							$event->params['value'] = $this->_clientModel->authorizeNetEnabled ? 'on' : 'off';
							break;
										
						case 'authorize_loginid':
							$event->params['value'] = $this->_clientModel->authorizeNetLoginId;
							break;
							
						case 'authorize_key':
							$event->params['value'] = $this->_clientModel->authorizeNetTransactionKey;
							break;
										
						case 'authorize_hash':
							$event->params['value'] = $this->_clientModel->authorizeNetMD5Hash;
							break;

						case 'authorize_sandbox':
							$event->params['value'] = $this->_clientModel->authorizeNetSandbox ? 'on' : 'off';
							break;

						// ADYEN
						case 'adyen_enabled':
							$event->params['value'] = $this->_clientModel->adyen_enabled ? 'on' : 'off';
							break;

						case 'adyen_hmac_key':
							$event->params['value'] = $this->_clientModel->adyen_hmac_key;
							break;

						case 'adyen_merchant_account':
							$event->params['value'] = $this->_clientModel->adyen_merchant_account;
							break;

						case 'adyen_skin_code':
							$event->params['value'] = $this->_clientModel->adyen_skin_code;
							break;

						case 'adyen_single_page_hpp':
							$event->params['value'] = $this->_clientModel->adyen_single_page_hpp;
							break;

						case 'adyen_sandbox':
							$event->params['value'] = $this->_clientModel->adyen_sandbox ? 'on' : 'off';
							break;

						// Cybersource
						case 'cybersource_enabled':
							$event->params['value'] = $this->_clientModel->cybersource_enabled;
							break;

						case 'cybersource_access_key':
							$event->params['value'] = $this->_clientModel->cybersource_access_key;
							break;

						case 'cybersource_secret_key':
							$event->params['value'] = $this->_clientModel->cybersource_secret_key;
							break;

						case 'cybersource_profile_id':
							$event->params['value'] = $this->_clientModel->cybersource_profile_id;
							break;

						case 'cybersource_sandbox_mode':
							$event->params['value'] = $this->_clientModel->cybersource_sandbox_mode;
							break;

						case 'stripe_enabled':
							$event->params['value'] = $this->_clientModel->stripe_enabled;
							break;

						case 'stripe_type':
							$event->params['value'] = $this->_clientModel->stripe_type;
							break;

						case 'stripe_tax_code':
							$event->params['value'] = $this->_clientModel->stripe_tax_code;
							break;

						case 'stripe_alipay_enabled':
							$event->params['value'] = $this->_clientModel->stripe_alipay_enabled;
							break;

						case 'stripe_public_key':
							$event->params['value'] = $this->_clientModel->stripe_public_key;
							break;

						case 'stripe_private_key':
							$event->params['value'] = $this->_clientModel->stripe_private_key;
							break;

						case 'stripe_account_email':
							$event->params['value'] = $this->_clientModel->stripe_account_email;
							break;

						case 'stripe_sandbox':
							$event->params['value'] = $this->_clientModel->stripe_sandbox;
							break;

						case 'stripe_public_key_test':
							$event->params['value'] = $this->_clientModel->stripe_public_key_test;
							break;

						case 'stripe_private_key_test':
							$event->params['value'] = $this->_clientModel->stripe_private_key_test;
							break;
					}
				}

				switch ($paramName) {

					case 'catalog_external':
						if (self::$client->catalog_custom_settings && self::$client->catalog_external) {
							$event->params['value'] = 'on';
						} elseif (self::$client->catalog_custom_settings && self::$client->catalog_external == 0) {
							$event->params['value'] = 'off';
						}
						break;

					case 'catalog_external_selected_catalogs':
						if (self::$client->catalog_custom_settings && self::$client->catalog_external) {
							$event->params['value'] = self::$client->catalog_external_selected_catalogs;
						}
					break;

					case 'catalog_use_categories_tree':
						if (self::$client->catalog_custom_settings) {
							$event->params['value'] = (self::$client->catalog_use_categories_tree == 0) ? 'off' : 'on';
						}

						break;

					case 'whitelabel_footer':
						if (self::$client->whiteLabelEnabled) {
							// Header settings
							$event->params['value'] = self::$client->whiteLabelPoweredByOption;
						}

						break;

					case 'whitelabel_header':
						if (self::$client->whiteLabelEnabled) {
							// Header settings
							$event->params['value'] = self::$client->whiteLabelHeader;
						}

						break;

					case 'whitelabel_footer_ext_height':
						if (self::$client->whiteLabelEnabled) {
							// Footer iframe height
							$whiteLabelFooterExtHeight = self::$client->whiteLabelFooterExtHeight;
							$event->params['value'] = !empty($whiteLabelFooterExtHeight) ? $whiteLabelFooterExtHeight : (CoreMultidomain::WL_FOOTER_IFRAME_HEIGHT);
						}

						break;


					case 'whitelabel_header_ext_height':
						if (self::$client->whiteLabelEnabled) {
							// Header iframe height
							$whiteLabelHeaderExtHeight = self::$client->whiteLabelHeaderExtHeight;
							$event->params['value'] = !empty($whiteLabelHeaderExtHeight) ? $whiteLabelHeaderExtHeight : (CoreMultidomain::WL_HEADER_IFRAME_HEIGHT);
						}

						break;

					// Header / Footer iframe URLs
					case 'whitelabel_footer_ext_url':
						if (self::$client->whiteLabelEnabled) {
							// Footer settings
							$event->params['value'] = self::$client->whiteLabelFooterExtUrl ? self::$client->whiteLabelFooterExtUrl : '';
						}

						break;

					case 'whitelabel_header_ext_url':
						if (self::$client->whiteLabelEnabled) {
							// Header settings
							$event->params['value'] = self::$client->whiteLabelHeaderExtUrl ? self::$client->whiteLabelHeaderExtUrl : '';
						}

						break;
                    case 'login_layout':
                        if(self::$client->signInPageLayoutEnabled)
                            $event->params['value'] = self::$client->signInPageLayoutOption;
                        break;
                    case 'minimal_login_type':
                        if(self::$client->signInPageLayoutEnabled)
                            $event->params['value'] = self::$client->signInMinimalType;
                        break;
                    case 'minimal_login_background':
                        if(self::$client->signInPageLayoutEnabled)
                            $event->params['value'] = self::$client->signInMinimalBackground;
                        break;
                    case 'minimal_login_video_fallback_img':
                        if(self::$client->signInPageLayoutEnabled)
                            $event->params['value'] = self::$client->signInMinimalVideoFallbackImg;
                        break;
					case 'hide_signin_form':
						if(self::$client->signInPageLayoutEnabled)
							$event->params['value'] = self::$client->hide_signin_form;
						break;

					// Self Registration
					case 'register_type':{
						if(self::$client->enable_self_registration_settings){
							$event->params['value'] = self::$client->register_types[self::$client->$paramName];
						}
					}break;
					case 'disable_registration_email_confirmation':{
						if(self::$client->enable_self_registration_settings){
							$event->params['value'] = self::$client->$paramName ? 'on' : 'off';
						}
					}break;
					case 'allowed_domains':{
						if($this->_clientModel->enable_self_registration_settings){
							$event->params['value'] = $this->_clientModel->$paramName;
						}
					}break;
				}


			}
		}
	}


	/**
	 * Listen for an event and act accordingly:
	 *
	 * @param DEvent $event
	 */
	public function onRenderCoursesLabelTile(DEvent $event) {

		if ($this->_clientModel) {
			if (self::$client->catalog_custom_settings && self::$client->catalog_external_selected_catalogs) {
				$event->params['title'] = Yii::t('standard', 'Courses catalogs');
				$event->params['description'] = '';
			}
		}
	}

	private function getCustomSettingsForDomain($event, $cm, $currentLang){
		$cm = CoreMultidomain::model()->findByPk($cm->id);
		$event->params['whitelabel_footer'] = $cm->whiteLabelPoweredByOption;
		$defaultLanguage = '';
		if ($cm->use_custom_settings == 1) {
			// use default language for the current domain
			$defaultLanguage = $cm->default_language;
		}else{
			$defaultLanguage = CoreLangLanguage::getDefaultLanguage();
		}
		// custom text for current language
		$value = Yii::app()->db->createCommand()
				->select('value')
				->from(CoreMultidomainHeaderFooter::model()->tableName())
				->where('id_multidomain = :id AND language = :lang AND type=:type',
						array(
								':id' => $cm->id,
								':lang' => $currentLang,
								':type' => CoreMultidomainHeaderFooter::TYPE_FOOTER_CUSTOM_MESSAGE,
						))->queryScalar();
		if (empty($value))
			// no text for the current language, then get the default one
			$value = Yii::app()->db->createCommand()
				->select('value')
				->from(CoreMultidomainHeaderFooter::model()->tableName())
				->where('id_multidomain = :id AND language = :lang AND type=:type',
						array(
								':id' => $cm->id,
								':lang' => $defaultLanguage,
								':type' => CoreMultidomainHeaderFooter::TYPE_FOOTER_CUSTOM_MESSAGE,
						))->queryScalar();
		if(!empty($value)) {
			if($cm->whitelabelNamingSiteEnable == 1){
				$replaceMent = $cm->replaceDoceboCustomLink;
				if (strpos($replaceMent, 'http://') !== false) $replaceMent = substr($replaceMent, 7);
				$value = str_replace('www.docebo.com', $replaceMent, $value);
			}
			$event->params['whitelabel_footer_text'] = $value;
		}

		// custom url
		$value = Yii::app()->db->createCommand()
				->select('value')
				->from(CoreMultidomainHeaderFooter::model()->tableName())
				->where('id_multidomain = :id AND language = :lang AND type=:type',
						array(
								':id' => $cm->id,
								':lang' => $currentLang,
								':type' => CoreMultidomainHeaderFooter::TYPE_FOOTER_CUSTOM_URL,
						))->queryScalar();
		if (empty($value) || $value == 'http://')
			// no url for the current language, then get the default one
			$value = Yii::app()->db->createCommand()
				->select('value')
				->from(CoreMultidomainHeaderFooter::model()->tableName())
				->where('id_multidomain = :id AND language = :lang AND type=:type',
						array(
								':id' => $cm->id,
								':lang' => $cm->default_language,
								':type' => CoreMultidomainHeaderFooter::TYPE_FOOTER_CUSTOM_URL,
						))->queryScalar();
		if(!empty($value)) {
			$event->params['whitelabel_footer_ext_url'] = $value;
		}

		if($event->params['whitelabel_header'] == 'external_header'){
			// we get the custom url header
			$currentLanguageUrl = Yii::app()->db->createCommand()
			->select('value')->from(CoreMultidomainHeaderFooter::model()->tableName())
			->where('id_multidomain=:id AND language=:lang AND type=:type', array(
					':id' => $cm->id,
					':lang' => $currentLang,
					':type' => BrandingWhiteLabelForm::POWEREDBY_CUSTOM_URL_HEADER,
			))->queryScalar();

			if(empty($currentLanguageUrl) || $currentLanguageUrl == 'http://') {
				// no current url for that language, we get the url for default language
				$currentLanguageUrl = Yii::app()->db->createCommand()
						->select('value')->from(CoreMultidomainHeaderFooter::model()->tableName())
						->where('id_multidomain=:id AND language=:lang AND type=:type', array(
								':id' => $cm->id,
								':lang' => $cm->default_language,
								':type' => BrandingWhiteLabelForm::POWEREDBY_CUSTOM_URL_HEADER,
						))->queryScalar();
			}
			$event->params['whitelabel_header_ext_url'] = $currentLanguageUrl;
		}
	}
}