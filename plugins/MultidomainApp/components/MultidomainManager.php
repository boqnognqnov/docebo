<?php
/**
 * 
 *
 */
class MultidomainManager extends CComponent {
	
	/**
	 * Activate requested Client's domain (Configuration AND Hipache REDIS databases)
	 * 
	 * @param string $domain
	 * @param string $oldDomain
	 * @throws Exception
	 */
	public static function updateClientDomain($domain, $oldDomain) {
		
		$currentLmsDomain = trim(Docebo::getCurrentDomain(true), '/');
		if ($oldDomain == $currentLmsDomain) {
			$oldDomain = '';
		}

		// if OLD domain is empty, it means we are going to CREATE NEW custom domain whatsoever
		// So, we have to check if the incoming domain is NOT used already!!
		if (!self::checkDomainAvail($domain)) {
			throw new Exception(Yii::t('multidomain', 'Domain {domain} is already in use!', array('{domain}' => $domain)));
		}
		
		$apiInfo = ErpApiClient::getInstallationInfo();
		
		if ($apiInfo && $apiInfo['success']) {
			
			if (trim($apiInfo['data']['domain_name']) == trim($domain)) {
				throw new Exception(Yii::t('multidomain', 'Invalid domain'));
			}
			
			self::updateClientDomainConfigRedis($apiInfo['data'], $domain, $oldDomain);
			
			// Update HIPACHE routing
			self::updateHipacheCustomDomain($domain, $oldDomain);
			
		}
		else {
			Yii::log('Could not collect LMS information from ERP!', CLogger::LEVEL_ERROR);
			throw new Exception(Yii::t('standard', '_OPERATION_FAILURE'));
		}
		
		return true;
		
	}
	
	
	/**
	 * Create/Update CustomDomain record in Redis configuration database
	 *
	 * @param array $lmsInfo
	 * @param string $domain
	 * @param string $oldDomain
	 * @throws Exception
	 * @return boolean
	 */
	public static function updateClientDomainConfigRedis($lmsInfo, $domain, $oldDomain) {
	
		
		// Get Redis client
		require_once(Yii::getPathOfAlias('common.vendors.Predis') . "/Autoloader.php");
		Predis\Autoloader::register();
		$redis = new Predis\Client(array(
				'host' 		=> Yii::app()->params['config_redis_host'],
				'port' 		=> Yii::app()->params['config_redis_port'],
				'database' 	=> Yii::app()->params['config_redis_db'],
		));
	
	
		// Original domain is the one from ERP (backup list -> domain name)
		$original_domain = $lmsInfo['domain_name'];
	
		try {

			// Delete Previous domain from Redis, if any
			if ($oldDomain) {
				$oldDomain = preg_replace('/^www\\./', '', strtolower($oldDomain));
				$redis->del($oldDomain);
			}
				
			// Get the type of the key and use the same type for all operations
			$type = $redis->type($original_domain);
			
			// Get main domain config, loading the Main Domain Redis record
			$config = array();
			
			// String or Hash only
			if ($type == 'string') {
				$config = CJSON::decode($redis->get($original_domain));
			}
			else if ($type == 'hash') {
				$config = $redis->hgetall($original_domain);
				$config['valid_domains'] = CJSON::decode($config['valid_domains']);
			}
				
			$domain = preg_replace('/^www\\./', '', strtolower($domain));
	
			// Modify some values and create the Custom domain config in Redis.
			// Custom domain record is a clone of the main one, but having below data set
			$config['original_domain'] = $original_domain; // Redis config loader will "notice" this is set and will act accordingly
			$config['valid_domains'][] = $domain;

			// Write back, use the same type
			if ($type == 'string') {
				$redis->set($domain, CJSON::encode($config));
			}
			else if ($type == 'hash') {
				$config['valid_domains'] = CJSON::encode($config['valid_domains']);
				$redis->del($domain);
				$redis->hmset($domain, $config);
			}
			return true;
		}
		catch (Exception $e) {
			Yii::log("Failed to call Redis and create custom domain configuration for custom domain: " . $domain, 'error');
			// Re-throw the exception, in case we have a higher level of try/catch
			throw new Exception($e->getMessage());
			return false;
		}
	
	}
	
	
	/**
	 * 
	 * Contact Redis server holding hipache configuration database and add/remove "custom domain" information.
	 *
	 * Hipache Config for a single domain is of Redis Type LIST and looks like:
	 *
	 * <key> =>  <arbitrary-domain-id> <vhost>
	 *
	 * Example:
	 *
	 * frontend:my.domain.com  =>   my.domain.com  http://192.169.0.1
	 * 
	 * @param string $domain
	 * @param string $oldDomain
	 * @param boolean $remove
	 * @return boolean
	 */
	public static function updateHipacheCustomDomain($domain, $oldDomain, $remove = false){
		
		require_once(Yii::getPathOfAlias('common.vendors.Predis') . "/Autoloader.php");
		Predis\Autoloader::register();
	
		$redisHost 				= Yii::app()->params['hipache_redis_host'];
		$redisPort 				= Yii::app()->params['hipache_redis_port'];
		$redisHipacheDbNumber 	= Yii::app()->params['hipache_redis_db'];
		$vhost 					= Yii::app()->params['hipache_lms_vhost'];
	
		$redis = new Predis\Client(array(
				'host' => $redisHost,
				'port' => $redisPort,
				'database' => $redisHipacheDbNumber,
		));
	
		$res = false;
	
		if ($domain) {
			// Clean up the value and get the domain only (just in case)
			$domain = strtolower($domain);
				
			// Add HTTP schema if needed
			$domain = Docebo::fixUrlSchema($domain);
				
			$domain = parse_url($domain, PHP_URL_HOST);
	
			if($domain){
				// Do the Redis call
				try {
					$listKey = "frontend:" . self::addWww($domain);
						
					$exists = $redis->exists($listKey);
	
					// Delete old key
					$redis->del($listKey);
						
					// Add the new one if NOT asked to remove only
					if ( !($remove === true)) {
						// Put original domain as first element in the list
						$redis->rpush($listKey, Docebo::getOriginalDomain());
	
						// Hipache route parameter could be an array of values, lets check this scenario
						if (is_array($vhost)) {
							foreach ($vhost as $vh) {
								// We do not add HTTP schema; configuration parameter MUST provide it, including port
								// e.g.   array("http://1.2.3.4:10100", "http://1.2.3.4:10100", ...)
								$redis->rpush($listKey, $vh);
							}
						}
						// Mkey, a string (providing backward compatibility)
						else {
							$redis->rpush($listKey, "http://" . $vhost);
						}
					}
						
					// Delete Previous custom domain config from Redis/hipache
					if ( ($oldDomain != $domain) || $remove) {
						$prevCustomDomainListKey = "frontend:" . self::addWww($oldDomain);
						$redis->del($prevCustomDomainListKey);
						if ($remove) {
							$redis->del("frontend:" . self::addWww($domain));
						}
					}
	
					Yii::log("Redis notified on custom domain change. Custom domain: ".$domain, CLogger::LEVEL_INFO);
						
					$res = true;
				}
				catch(Exception $e) {
					Yii::log("Failed to call Redis and set custom domain information in hipache: " . $domain, CLogger::LEVEL_ERROR);
					Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
						
					$res = false;
				}
			}
		}
	
		return $res;
	
	}
	

	/**
	 * Add "www." to 2-level domains as well as to .co.uk, if there is no  www. as last level/
	 * This is to cope with the forcefully added www. (to such domains) by the frontend nginx
	 *
	 * @param string $domain
	 * @return string
	 */
	public static function addWww($domain) {
	
		// Add www. to 2 level domains (example.com)
		$domainArr = explode('.', $domain);
		if (count($domainArr) == 2) {
			return 'www.' . $domain;
		}
	
		// Add www. to CO.UK domains (example.co.uk)
		$test = strtolower($domainArr[1] . '.' . $domainArr[2]);
		if (count($domainArr) == 3 && ($test == 'co.uk')) {
			return 'www.' . $domain;
		}
	
		return $domain;
	}
	
	/**
	 * Removes dashes and dots from the name
	 * @param $domain
	 * @return string
	 */
	public static function domainNameNormalize($domain)
	{
		return preg_replace('/(\W)/is', '_', $domain);
	}

	
	/**
	 * Check if domain is available
	 * 
	 * @param string $domain
	 * @return boolean
	 */
	public static function checkDomainAvail($domain) {
		$apiRes = ErpApiClient::apiCheckIfCustomDomainExists($domain);
		return ($apiRes['success'] != false);
	}

	
	
	
}