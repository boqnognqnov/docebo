<div class="content lead" style="border-top: 1px solid #e4e6e5; border-bottom: 1px solid #e4e6e5; background: #f1f3f2; padding: 18px 12px;">
	<div class="row-fluid text-center">
		<div class="span6">
			<?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/student-status.png');?>
			<div class="clearfix" style="margin-bottom: 0">
				<span style="color: #0465ac; font-size: 40px; font-weight: 700;"><?=$usersCountGapps?></span> Users in your Google Apps domain
			</div>
		</div>
		<div class="span6">
			<?php echo CHtml::image(Yii::app()->theme->baseUrl. '/images/student-status.png');?>
			<div class="clearfix" style="margin-bottom: 0">
				<span style="color: #0465ac; font-size: 40px; font-weight: 700;"><?=$usersCountDocebo?></span> Users in your Docebo platform
			</div>
		</div>
	</div>
</div>

<a class="btn confirm-btn sync-users-btn"><i class="i-sprite is-assign white"></i> <?=Yii::t('user_management', 'Import users from GA')?></a>

<div class="bottom-section clearfix">
	<div id="grid-wrapper">
		<?php $this->widget('DoceboCGridView', array(
			//'id' => 'group-management-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			'dataProvider' => $gappsUsers,
			'template' => '{items}{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'pager' => array(
				'class' => 'adminExt.local.pagers.DoceboLinkPager',
			),
			'ajaxUpdate' => 'all-items',
			'columns' => array(
				'firstname',
				'lastname',
				array(
					'header'=>'Email',
					'value'=>'$data[\'gapps_username\']'
				),
				array(
					'header'=>'User level',
					'value'=>'$data[\'admin\']==true ? \'Admin\': \'User\''
				),

				array(
					'header'=>'Suspended',
					'type'	=> 'html',
					'value'=>'$data[\'suspended\']==1 ? \'<span style="color: red;">' . Yii::t('standard', '_SUSPENDED'). '</span>\': \'\''
				),

				array(
					'header' => 'Synced',
					'type' => 'raw',
					'value' => '$data[\'exists_in_lms\'] ? "<i class=\'i-sprite is-check green\'/>" : "<i class=\'i-sprite is-remove\'/>"',
					'htmlOptions' => array('class' => 'button-column-single')
				),
			),
		));?>
	</div>
</div>

<script type="text/javascript">
(function($){
	$('.sync-users-btn').click(function(e){
		e.preventDefault();

		var reauthUrl = '<?=$model->installFrom=='saas' ? $model->getAuthClient()->createAuthUrl() : '' ;?>';

		$.getJSON('<?=Docebo::createLmsUrl('GoogleappsApp/GoogleappsApp/axSyncUsers')?>', function(data, status, xhr){
			if(status==='success'){
				if(data.success){
					// Redirect to user management area if all goes well
					window.location = '<?=Docebo::createAdminUrl('userManagement/index')?>';
				}else{
					alert(data.message);
					window.location = reauthUrl;
				}
			}else{
				// Reauthenticate
				window.location = reauthUrl;
			}
		});
	});
})(jQuery);
</script>