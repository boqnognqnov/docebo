<?php
/* @var $form CActiveForm */
/* @var $settings GoogleappsAppSettingsForm */
?>

<a href="<?= ('it' == Yii::app()->getLanguage()) ? 'https://www.docebo.com/it/knowledge-base/elearning-docebo-per-google-app/' : 'https://www.docebo.com/knowledge-base/elearning-docebo-for-google-apps/' ?>" target="_blank" class="app-link-read-manual"><?= Yii::t('apps', 'Read Manual'); ?></a>
<h2><?=Yii::t('standard','Settings') ?>: Google Apps</h2>
<br>

<?php DoceboUI::printFlashMessages(); ?>


<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'app-settings-form',
	'htmlOptions'=>array('class' => 'form-horizontal'),
));
?>

<div class="error-summary"><?php echo $form->errorSummary($settings); ?></div>

<div class="control-group">
    <?=CHtml::label(Yii::t('standard', 'Website'), false, array('class'=>'control-label'));?>
    <div class="controls">
        <a class="app-settings-url" href="http://www.google.com/intl/en/enterprise/apps/business/" target="_blank">http://www.google.com/intl/en/enterprise/apps/business/</a>
    </div>
</div>

<div class="control-group">
	<?=$form->labelEx($settings, 'googleapps_domain', array('class'=>'control-label'));?>
	<div class="controls">
		<?=$form->textField($settings, 'googleapps_domain');?>
	</div>
</div>

<div class="control-group">
	<?=$form->labelEx($settings, 'googleapps_consumer_key', array('class'=>'control-label'));?>
	<div class="controls">
		<?=$form->textField($settings, 'googleapps_consumer_key');?>
	</div>
</div>

<div class="control-group">
	<?=$form->labelEx($settings, 'googleapps_consumer_secret', array('class'=>'control-label'));?>
	<div class="controls">
		<?=$form->textField($settings, 'googleapps_consumer_secret');?>
	</div>
</div>

<div class="control-group">
	<?= CHtml::label(Yii::t('GoogleappsApp', 'Redirect URI'), null, array('class'=>'control-label'))?>
	<?= CHtml::label(Docebo::createAbsoluteLmsUrl('GoogleappsApp/GoogleappsApp/googleappsRedirect'), null, array('style' => 'white-space: nowrap;',   'class'=>'span12 control-label')) ?>
</div>

<div class="form-actions">
	<?=CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class'=>'btn-docebo green big')); ?>
	&nbsp;
	<a href="<?=Docebo::createLmsUrl("app/index");?>" class="btn-docebo black big"><?=Yii::t('standard', '_CANCEL');?></a>
</div>

<?php $this->endWidget(); ?>