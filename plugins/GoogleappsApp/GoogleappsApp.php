<?php

/**
 * Plugin for GoogleappsApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class GoogleappsApp extends DoceboPlugin {


	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'GoogleappsApp';


	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {
		parent::activate();
		$this->activateOldPlugin();
		Settings::save('googleapps_active', 'on', 'enum', 255, 'main', 14); // enable the feature
	}


	public function deactivate() {
		parent::deactivate();
		$this->deactivateOldPlugin();
		Settings::save('googleapps_active', 'off', 'enum', 255, 'main', 14); // disable the feature

		// Remove old unused setting from old databases that have it set to avoid clutter
		Settings::remove('googleapps_redirect_uri');
	}


	/**
	 * Activate / enable the "old" google apps plugin for the installation
	 * This method is a copy of the parent with fixed plugin name set to googleapps
	 * @return bool
	 */
	public function activateOldPlugin() {
		$plugin_name = 'googleapps';
		$plugin_model = CorePlugin::model()->find('plugin_name=:name', array(':name'=>$plugin_name));

		if (!$plugin_model) {
			$plugin_model = new CorePlugin();
			$plugin_model->plugin_name = $plugin_name;
			$plugin_model->plugin_title = ucfirst($plugin_name).'Plugin';
		}

		//TODO: check if installation is needed (db migrations?)

		$plugin_model->is_active = 1;
		return $plugin_model->save();
	}


	/**
	 * Deactivate / disabled the current plugin for the installation
	 * This method is a copy of the parent with fixed plugin name set to googleapps
	 * @return bool
	 */
	public function deactivateOldPlugin() {
		$res = true;
		$plugin_name = 'googleapps';
		$plugin_model = CorePlugin::model()->find('plugin_name=:name', array(':name'=>$plugin_name));

		if ($plugin_model) {
			$plugin_model->is_active = 0;
			$plugin_model->save();
		}

		return $res;
	}

}
