<?php

class GoogleappsHelper {

	// 'gapps' - LMS installed from Google App Marketplace,
	//	using oAuth1 and the old Google Provisioning API.
	// 'saas' - LMS installed from SAAS and using our internal Google Apps app,
	//	using oAuth2 and the consumer key/consumer secret and redirect URL
	//	must be registered in the Google API Console.
	public $installedFrom = 'saas'; // gapps|saas

	private $domain;
	private $consumerKey;
	private $consumerSecret;

	private $scopes;

	// Needed for oAuth2. Must be registered in the Google API console
	private $redirectUrl;

	// This internal redirect URL is stored in a $_SESSION variable
	// where our internal redirector (on which the user lands after
	// authenticating with his Google account) gets it and
	// redirects the user there.
	// This is put in place, so that we don't have ot register
	// multiple redirect URLs in the Google API console, but just one
	// - our internal redirector.
	private $internalRedirectUrl;

	private $client; // a Google_Client object; or null (for Gapps based LMS)

	function __construct($installedFrom = null) {
		$this->installedFrom = ($installedFrom=='gapps') ? 'gapps' : 'saas';

		if($this->installedFrom=='gapps'){
			// installed from Google Marketplace
			$this->domain = Settings::getCfg('gapps_domain');
			$this->consumerKey = isset(Yii::app()->params['gapps_consumer_key']) ? Yii::app()->params['gapps_consumer_key'] : false;
			$this->consumerSecret = isset(Yii::app()->params['gapps_consumer_secret']) ? Yii::app()->params['gapps_consumer_secret'] : false;

			if(!$this->domain || !$this->consumerKey || !$this->consumerSecret){
				throw new Exception("Domain, consumer key or consumer secret not set.");
			}
		}else{
			// using our app
			$this->domain = Settings::get('googleapps_domain');
			$this->consumerKey = Settings::get('googleapps_consumer_key');
			$this->consumerSecret = Settings::get('googleapps_consumer_secret');
			$this->redirectUrl = Docebo::createAbsoluteLmsUrl('GoogleappsApp/GoogleappsApp/googleappsRedirect');

			if(!$this->domain || !$this->consumerKey || !$this->consumerSecret || !$this->redirectUrl){
				throw new Exception("Domain, redirect URL, consumer key or consumer secret not set.");
			}
		}
	}

	public function getAuthClient(){

		if(!$this->client){
			if($this->installedFrom == 'gapps'){
				// LMS installed from Google Apps Marketplace

				// Include the Google API library for authentication and API calls
				Yii::import('addons.oauth.*');

				$this->client = $this->getOauth1Client();
			}else{
				// Saas LMS. Installed our Google Apps App

				// Include the Google API library for authentication and API calls
				Yii::import('common.extensions.googleApiClientLib.*');

				$client = new Google_Client();
				$client->setApplicationName('Docebo User Synchronization');
				// Visit https://code.google.com/apis/console?api=plus to generate your
				// client id, client secret, and to register your redirect uri.
				$client->setClientId($this->consumerKey);
				$client->setClientSecret($this->consumerSecret);
				$client->setRedirectUri($this->redirectUrl);

				$client->setApprovalPrompt('auto');
				$client->setAccessType('online');

				if($this->scopes){
					$client->setScopes($this->scopes);
				}else{
					$client->setScopes(array(
						'https://apps-apis.google.com/a/feeds/domain/',
						'https://apps-apis.google.com/a/feeds/user/',
						'https://www.googleapis.com/auth/admin.directory.user.readonly'
					));
				}

				$this->client = $client;
			}
		}

		return $this->client;

	}

	private function getOauth1Client(){
		// $domain			= $this->domain;

		if(!defined('OAUTH_TMP_DIR'))
			define('OAUTH_TMP_DIR', function_exists('sys_get_temp_dir') ? sys_get_temp_dir() : realpath($_ENV["TMP"]));

		//  Init the OAuthStore
		$options = array(
			'consumer_key' => $this->consumerKey,
			'consumer_secret' => $this->consumerSecret,
			'server_uri' => "https://www.google.com",
			'request_token_uri' => "https://www.google.com/accounts/OAuthGetRequestToken",
			'authorize_uri' => "https://www.google.com/accounts/OAuthAuthorizeToken",
			'access_token_uri' => "https://www.google.com/accounts/OAuthGetAccessToken"
		);

		// Note: do not use "Session" storage in production. Prefer a database
		// storage, such as MySQL.

		OAuthStore::instance("Session", $options);

		return $options;
	}

	public function authenticate(){
		$client = $this->getAuthClient();

		if($this->installedFrom=='saas'){

			if (isset($_GET['code'])) {
				try{
					$client->authenticate( $_GET['code'] );
				}catch(Exception $e){
					$this->setToken(false);

					// Check if the use is Coming from Hydra Frontend
					if(Yii::app()->session['isComingFromHydra']) {
						unset(Yii::app()->session['isComingFromHydra']);
						$url = Docebo::createHydraUrl('learn', 'signin', array( "type" => 'oauth2_response', 'error' => Yii::t('login', '_NOACCESS')) );
						Yii::app()->controller->redirect($url);
					}

					throw new Exception($e->getMessage());
				}

				// Success, get access token and keep it in session
				$this->setToken($client->getAccessToken());

				// Reload the page, so we strip any unneded codes from the URL
				$this->selfRedirectCleanUrl();
			}

			if ($this->getToken()) { // Restore from session variable
				$client->setAccessToken($this->getToken());
			}

			if ($client->getAccessToken() && !$client->isAccessTokenExpired()) {
				$this->client = $client;
				return TRUE;
			}

			Yii::app()->controller->redirect( $client->createAuthUrl() );

		}else{ // LMS installed from "Google Apps Marketplace"

			try
			{
				// Restore access token from Session if found.
				// If not found in session:
				// 1. call Google and ask for "request token"
				// 2. redirect user to Google to approve permissions, appending "request token"
				// 3. Google redirects back to this page with an "auth token" in GET
				// 4. Call Google with "auto token" to exchange it for real "access token"
				// 5. Use "access token" to call the Google APIs and request all kinds of stuff
				//
				// Note: Never assume that an "access token" restored
				// from Session will work. It may have expired or maybe it
				// was manually revoked by the Google user in his account settings.
				// Always be prepared to receive an error while making an API call to request
				// stuff, and if we encounter one - redirect the user to Google for reauthentication.

				$token = $this->getToken();

				if(!$token){
					//  STEP 1:  If we do not have a OAuth request token yet, go get one
					if (!isset($_GET["oauth_token"]) || empty($_GET["oauth_token"]))
					{
						$getAuthTokenParams = array(
							'scope' => 'https://apps-apis.google.com/a/feeds/',
							// Where to redirect with "auth token" after user approves permission
							'oauth_callback' => Docebo::createAbsoluteAdminUrl('GoogleappsApp/GoogleappsApp/syncUsers'),//Docebo::getRootBaseUrl(true).'/doceboLms/'.'?r=lms/gapps/syncUsers',
							// the Google Apps domain from which to get users
							'hd'=>$this->domain,
							// A friendly app name which will be displayed to the user while approving permissions
							'xoauth_displayname'=>  Settings::get('page_title')
						);

						// get a request token, needed for building the Google Authorization URL
						$tokenResultParams = OAuthRequester::requestRequestToken($this->consumerKey, 0, $getAuthTokenParams);

						// redirect to the google authorization page, they will redirect back with "auth token" in GET
						header("Location: " . $client['authorize_uri'] . "?oauth_token=" . $tokenResultParams['token'].'&hd='.$this->domain);
						exit;
					} else {
						//  STEP 2:  Get an access token using the auth token provided
						//  by Google during redirection in $_GET

						try {
							// Exchange auth token for access token
							OAuthRequester::requestAccessToken($this->consumerKey, $_GET["oauth_token"], 0, 'POST', $_GET);

							// --------------------------------------------------- //
							// ALL OK! The "auth token" is now a valid "access token"
							// --------------------------------------------------- //

							// Save it in memory so it can
							// be reused between page reloads
							$this->setToken($_GET);

							// Remove the "auth/access token" from the URL
							// Useful to avoid errors if the user reloads the page
							// with the same "auth token" in the URL, since exchanging
							// it for an "access token" will fail (it's already an "access token")
							$this->selfRedirectCleanUrl();
						}
						catch (OAuthException2 $e)
						{
							// Usually happens when trying to exchange
							// a request/auth token for access token if it was
							// already exchanged before. In this case, we need to
							// ask for a new request/auth token and authorize that new one

							// reset the saved token
							$this->setToken(false);
							throw new OAuthException2('Can not exhange auth token for access token. Error: '.$e->getMessage());
						}

					}
				}

				return TRUE;

			}
			catch(OAuthException2 $e) {
				// As a last resort only...
				// We can't do anything but die()
				echo "OAuthException:  " . $e->getMessage();
				exit;
			}

		}
	}

	/**
	 * Redirect to the same page, but strip any one-time use
	 * GET params that may cause errors if used for second API calls
	 */
	private function selfRedirectCleanUrl() {
		unset($_GET['oauth_token']);
		unset($_GET['code']);
		unset($_GET['oauth_verifier']);

		$r = (isset($_GET['r']) && $_GET['r'] !== '' ? $_GET['r'] : '');

		unset($_GET['r']);

		$redirect = Docebo::createLmsUrl($r, $_GET);

		header('Location: ' . $redirect);
		exit;
	}

	/**
	 * Return array of users from this Google apps domain
	 * using existing access token
	 * @param boolean $only_active - Include only non-suspended
	 * Google apps users (default: true)
	 * @return array - a key=>value based array with the
	 * user's email as the key and his info as the value
	 * (again, as another associative array). When no users,
	 * returns an empty array.
	 * @throws Exception - on API call failure
	 */
	public function getGappsUsers($only_active = true){
		if(!$this->client)
			$this->authenticate();

		if($this->installedFrom=='saas'){

			// LMS installed from Saas; later added the Google Apps app
			return $this->_getGappsUsersAppVersion($only_active);
		}else{ // installedFrom=='gapps'
		//
			// LMS installed from Google Apps marketplace
			return $this->_getGappsUsersMarketplaceVersion($only_active);
		}

	}

	/**
	 * @see function getGappsUsers
	 */
	private function _getGappsUsersAppVersion($only_active = true){
		if(!$this->client || !is_a($this->client, 'Google_Client')){
			throw new Exception('Can not get Google Apps users becase the Google API Client is not valid');
		}

		$accessTokenData = json_decode($this->client->getAccessToken(), true);

		if(!$accessTokenData || !isset($accessTokenData['access_token'])){
			throw new Exception('Can not get Google Apps users becase the provided access token is invalid');
		}

		$accessToken = $accessTokenData['access_token'];


		$pageToken = null;
		$res = array();

		do{
			$params = array(
				'domain'=>$this->domain,
				'access_token'=>$accessToken,
				'maxResults'=>500,
				'pageToken' => $pageToken
			);

			// We manually set "&" as param separator since "&amp;" is not accepted by Google
			$apiQuery = http_build_query($params, null, '&');

			$ch = curl_init();

			// Set query data here with the URL
			curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/admin/directory/v1/users?{$apiQuery}");

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, '5');
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

			// Response example:
			// https://developers.google.com/admin-sdk/directory/v1/guides/manage-users#get_all_domain_users

			$rawResult = trim(curl_exec($ch));
			$apiResponse = json_decode($rawResult);
			$curlHeaders = curl_getinfo($ch);

			curl_close($ch);

			if($curlHeaders['http_code'] != 200){
				$errorMessages = isset($apiResponse->error->errors) ? $apiResponse->error->errors : false;
				$errors = array();
				if($errorMessages){
					foreach($errorMessages as $errorMsg){
						if(isset($errorMsg->message)){
							$errors[] = $errorMsg->message;
						}
					}
				}
				throw new Exception('Can not get Google Apps users. Expected code 200, got '.$curlHeaders['http_code'].' instead. API response errors: '.  implode(', ', $errors));
			}

			if(!$apiResponse){
				Yii::log('Invalid response from Google API. Response body: '.$rawResult, CLogger::LEVEL_ERROR);
				throw new Exception('Can not get Google Apps users. Invalid API response.');
			}

			if($apiResponse && isset($apiResponse->users) && is_array($apiResponse->users)){

				foreach($apiResponse->users as $user){

					if($only_active && $user->suspended=='true'){
						// The user is inactive and we only request
						// active ones, so skip him
						continue;
					}

					$res[$user->primaryEmail] = array(
						'gapps_username' => $user->primaryEmail,
						'firstname' => $user->name->givenName,
						'lastname' => $user->name->familyName,
						'email' => $user->primaryEmail,
						'force_pwd_change' => $user->changePasswordAtNextLogin,
						'suspended' => $user->suspended,
						'is_admin' => $user->isAdmin,
						'admin' => ($user->isAdmin==true ? true : false),
						'exists_in_lms' => false // a check is performed later
					);
				}

				if(isset($apiResponse->nextPageToken)){
					$pageToken = $apiResponse->nextPageToken;
				} else{
					$pageToken = null;
				}

				// We also have $response->nextPageToken available here
				// @TODO store it in session to use it as access token for next page of results

			}else{
				throw new Exception('No users returned from Google Apps API call');
			}
		} while($pageToken);

		// GET params to be sent to the Google API

		if(empty($res)){
			Yii::log('Google Apps API call succeeded, but no users were returned', CLogger::LEVEL_INFO);
		}

		return $res;

	}

	/**
	 * @see function getGappsUsers
	 */
	private function _getGappsUsersMarketplaceVersion($only_active = true){
		$users = array();

		// Restore access token from session
		$token = $this->getToken();

		if(!$token){
			$this->authenticate();
			throw new Exception('Can not get Google Apps users because an access token is not present');
		}

		// STEP 3: Access protected resources using access token
		$request = new OAuthRequester("https://apps-apis.google.com/a/feeds/user/2.0/".$this->domain, 'GET', $token);
		$result = $request->doRequest(0);

		if ($result['code'] == 200) {

			// Parse the XML response from the api call
			// and format it as a real array of users
			$xml = new DOMDocument();
			$xml->formatOutput = true;
			$xml->loadXML($result['body']);
			$users = $xml->getElementsByTagName('entry');

			$newUsersArr = array();
			foreach($users as $user){
				$newUserElem = array();

				// Get all child XML tags for this user
				$children = $user->childNodes;
				foreach($children as $child){

					// Valid attributes for each user are stored
					// inside these XML elements
					// e.g. <apps:property name="userEmail" value="myemail@mydomain.mygbiz.com" />
					if($child->tagName=='apps:property'){
						$attrs = $child->attributes;

						$newUserElem[$child->getAttribute('name')] = $child->getAttribute('value');

					}

				}

				if(!$only_active || ($only_active && $newUserElem['isSuspended']!='true')){
					if(isset($newUserElem['userEmail'])){

						// We use these array indexes internally, so fake them
						$newUserElem['gapps_username'] = $newUserElem['userEmail'];
						$newUserElem['firstname'] = (isset($newUserElem['firstName']) ? $newUserElem['firstName'] : null);
						$newUserElem['lastname'] = (isset($newUserElem['lastName']) ? $newUserElem['lastName'] : null);
						$newUserElem['admin'] = ($newUserElem['isAdmin']=='true' ? true : false);

						$newUsersArr[$newUserElem['userEmail']] = $newUserElem;
					}else{
						$newUsersArr[] = $newUserElem;
					}
				}
			}

			return $newUsersArr;

		} else {
			// Reset the token, because it may be
			// the one causing the error
			// (may be old or revoked by the user)
			$this->setToken(false);

			throw new Exception('Google API responded with an error. Expected code 200, got '.(int)$result['code']);
		}

		return (isset($users) && is_array($users) ? $users : array());
	}

	/**
	 * Make an API call to Google and get the count of users on
	 * this Google apps domain. NOTE: Only works for App version.
	 * Doesn't work for LMS installed from Google Apps Marketplace
	 *
	 * @return int - count of users on this Google Apps domain
	 * @throws Exception - On API failure
	 */
	public function countGappsUsers(){
		if(!$this->client)
			$this->authenticate();

		if(!$this->client || !is_a($this->client, 'Google_Client')){
			throw new Exception('Can not count Google Apps users becase the Google API Client is not valid');
		}

		$accessTokenData = json_decode($this->client->getAccessToken(), true);

		if(!$accessTokenData || !isset($accessTokenData['access_token'])){
			throw new Exception('Can not count Google Apps users becase the provided access token is invalid');
		}

		$accessToken = $accessTokenData['access_token'];

		$googleFeed = "https://apps-apis.google.com/a/feeds/domain/2.0/".$this->domain."/general/currentNumberOfUsers";

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $googleFeed);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		// See https://developers.google.com/accounts/docs/OAuth2WebServer#callinganapi
		curl_setopt($ch, CURLOPT_HTTPHEADER, array( "Authorization: Bearer {$accessToken}"));
		curl_setopt($ch, CURLOPT_TIMEOUT, '5');
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

		$apiResponse = trim(curl_exec($ch));
		$curlError = curl_error($ch);

		$headers = curl_getinfo($ch);

		curl_close($ch);

		$responseCode = $headers['http_code'];

		if($curlError)
			throw new Exception('Error calling Google API feed. Curl error: '. $curlError);

		if(!$apiResponse)
			throw new Exception('Error calling Google API feed. Empty response.');

		if($responseCode != 200){
			throw new Exception("Error calling Google API feed. Response code expected 200, got {$responseCode} instead. ");
		}

		$matches = array();
		$count = 0;
		@preg_match("#<apps:property name='currentNumberOfUsers' value='(?<count>.*)'/>#i", $apiResponse, $matches);
		if(isset($matches['count']) && $matches['count'] > 0){
			$count = (int)$matches['count'];
		}elseif(stripos('currentNumberOfUsers', $apiResponse)!==false){
			// Try string explosion
			$explode = @explode("currentNumberOfUsers", $apiResponse, 2);
			$explode = @explode("value='", $explode[1], 2);
			$explode = @explode("'", $explode[1], 2);
			$count = ((isset($explode[0]) && $explode[0]) > 0 ? $explode[0] : 0);
		}else{
			throw new Exception("Can not count Google Apps users due to invalid XML response");
		}

		return (int)$count;

	}

	public function syncUsers($gappsUsers){
		// Check which gapps users exist in the LMS
		// and mark them as such in the array that will populate the grid
		$gappsUserEmailsArr = array_keys($gappsUsers);

		$condition = new CDbCriteria();
		$condition->select = 'email';
		$condition->addInCondition('email', $gappsUserEmailsArr);
		$matchingDoceboUsers = CoreUser::model()->findAll($condition);
		if($matchingDoceboUsers){
			foreach($matchingDoceboUsers as $doceboUser){
				if(isset($doceboUser->email) && !empty($doceboUser->email)){
					$email = $doceboUser->email;

					if(isset($gappsUsers[$email]) && is_array($gappsUsers[$email])){
						$gappsUsers[$email]['exists_in_lms'] = true;
					}
				}
			}
		}

		$result = array(
			'success' => true,
			'user_count' => 0,
			'error_count' => 0,
		);
		foreach($gappsUsers as $gappsEmail=>$user){
			if($user['exists_in_lms']){
				continue;
			}

			$firstname = addslashes($user['firstname']);
			$lastname = addslashes($user['lastname']);
			$email = $gappsEmail;
			$pass = uniqid(substr($email, 0, 10) . "%", true);
			$isGappsAdmin = (isset($user['admin']) && $user['admin']==true) ? true : false;

			$newUser = new CoreUser();
			$newUser->userid = $email;
			$newUser->firstname = $firstname;
			$newUser->lastname = $lastname;
			$newUser->email = $email;
			$newUser->pass = $pass;
			$save = $newUser->save();
			if($save){

				if($isGappsAdmin){
					$assign = Yii::app()->authManager->assign(Yii::app()->user->level_godadmin, $newUser->idst);
				}else{
					$assign = Yii::app()->authManager->assign(Yii::app()->user->level_user, $newUser->idst);
				}
				$result['user_count']++;
			} else {
				$result['error_count']++;
			}

			// If ALL users synced, return TRUE,
			// if at least one failed - return FALSE
			$result['success'] &= $save;
		}
		return $result;
	}

	public function loginUserGapps(){

		if($this->installedFrom=='saas'){
			$this->_loginAppVersion();
		}else{
			$this->_loginMarketplaceVersion();
		}

	}

	private function _loginAppVersion(){

		// Read auth mode. Possible auth modes:
		// - "session" (default): LMS login session
		// - "oauth2": generates and returns an OAuth2 session (used only by webapp)
		$authentication_mode = Yii::app()->session['sso_auth_mode'];
		if(!$authentication_mode)
			$authentication_mode = Yii::app()->request->getParam('auth_mode', 'session');

		$this->getAuthClient();

		Yii::import('common.extensions.googleApiClientLib.contrib.Google_Oauth2Service');

		// We must add services like this one BEFORE calling authenticate()
		$oauth2Service = new Google_Oauth2Service($this->client);

		$this->authenticate();

		if($oauth2Service && isset($oauth2Service->userinfo)){
			$userInfo = $oauth2Service->userinfo->get();

			$gappsEmailVerified = (isset($userInfo['verified_email']) && $userInfo['verified_email']=='true');
			$email = isset($userInfo['email']) ? $userInfo['email'] : false;

			if($gappsEmailVerified && $email){

				$ui = new DoceboUserIdentity($email, 'dummy');
				if ($ui->authenticateWithEmail()) {

					unset(Yii::app()->session['sso_auth_mode']);
					$server = DoceboOauth2Server::getInstance();
					$server->init();

					if(Yii::app()->session['isComingFromHydra']) {
						unset(Yii::app()->session['isComingFromHydra']);
						$token = $server->createAccessToken("hydra_frontend", $ui->getId(), "api");
						$url = Docebo::createHydraUrl('learn', 'signin', array_merge(array( "type" => 'oauth2_response'), $token) );
						Yii::app()->controller->redirect($url);
					} else if($authentication_mode == 'oauth2') {
						$token = $server->createAccessToken("mobile_app", $ui->getId(), "webapp");
						$response = array('success' => false, 'message' => "Could not create Oauth2 token");
						if($token)
							$response = array('success' => true, 'token' => $token);

						Yii::app()->controller->redirect( Docebo::getWebappUrl(true) . '?oauth2_response=' . urlencode(CJSON::encode($response)) );

					} else {
						$login = Yii::app()->user->login($ui);
						if ($login) {
							//  Login success
							// Make way for next login requests
							$this->setToken(false);
							// Redirect to the user area
							Yii::app()->controller->redirect( Docebo::getUserAfetrLoginRedirectUrl() );
						} else {
							// Login failure
							// The user exists in the LMS but he is probably suspended
							// or some other internal error
							Yii::log('Google apps login requested by user with email ' . $email . ' but his LMS user does not allow login', CLogger::LEVEL_WARNING);
						}
					}
				}else{
					// User does not exist
					Yii::log('Google apps login requested by user with email '.$email.' but LMS user does not exists', CLogger::LEVEL_WARNING);
				}
			}
		}

		// The user did not Manage to login, Check from where he is coming from and redirect him accordingly
		if (Yii::app()->session['isComingFromHydra']) {
			unset(Yii::app()->session['isComingFromHydra']);
			$url = Docebo::createHydraUrl('learn', 'signin', array( "type" => 'oauth2_response', 'error' => Yii::t('login', '_NOACCESS') ) );
			Yii::app()->controller->redirect($url);
		}

		// Force redirect to Google authentication page on next request
		$this->setToken(false);

		Yii::app()->controller->redirect( Docebo::createLmsUrl('site/index', array('error'=>2)) );
	}

	private function _loginMarketplaceVersion(){
		// Set the default separator to '&', used by http_build_query of openid.php
		ini_set('arg_separator.output', '&');

		// Simple Yii::import was giving issues here
		$includePath = Yii::getPathOfAlias('addons.social.openid').'/openid.php';

		if(is_file($includePath))
			require_once($includePath);
		else
			die('Can not include Google OpenID library');


		try {
			if (!isset($_GET['openid_mode'])) {
				$openid = new LightOpenID();
				$openid->identity = 'https://www.google.com/accounts/o8/id';
				$openid->required =array('contact/email', 'namePerson/first', 'namePerson/last');
				header('Location: ' . str_replace('&amp;', '&', $openid->authUrl()));
			} elseif ($_GET['openid_mode'] == 'cancel') {
				Util::jump_to('index.php?access_fail=3');
			} else {
				$openid = new LightOpenID;
				$_GET['openid_return_to']=$_REQUEST['openid_return_to']; // to avoid having &amp; instead of &

				if ($openid->validate()) {

					$user_data = array(
					    'email'=> isset($_GET['openid_ext1_value_contact_email']) ? filter_var($_GET['openid_ext1_value_contact_email'], FILTER_VALIDATE_EMAIL) : false,
					    'firstname'=> isset($_GET['openid_ext1_value_namePerson_first']) ? filter_var($_GET['openid_ext1_value_namePerson_first'], FILTER_VALIDATE_EMAIL) : false,
					    'lastname'=> isset($_GET['openid_ext1_value_namePerson_last']) ? filter_var($_GET['openid_ext1_value_namePerson_last'], FILTER_VALIDATE_EMAIL) : false,
					);

					if (!empty($user_data['email'])) {

						$ui = new DoceboUserIdentity($user_data['email'], 'dummy');
						if ($ui->authenticateWithEmail()) {
							$login = Yii::app()->user->login($ui);

							if($login){
								/* ------------------------ */
								/* Login success            */
								/* ------------------------ */

								// Redirect to the user area
								header("Location: ".Docebo::getUserAfetrLoginRedirectUrl());
								exit;
							}
						}

						header("Location: ".Docebo::createLmsUrl('site/index', array('error'=>2)));

					} else {
						header("Location: ".Docebo::createLmsUrl('site/index', array('error'=>2)));
					}
				} else {
					// OpenID response validation failed
					header("Location: ".Docebo::createLmsUrl('site/index', array('error'=>2)));
				}
				die();
			}
		} catch (ErrorException $e) {
			echo $e->getMessage();
		}
	}

	public function setToken($accessTokenJson){
		if($this->scopes){
			$scopeSpecificTokenName = 'access_token_usersync_'.md5(json_encode($this->scopes));
		}else{
			$scopeSpecificTokenName = 'access_token_usersync';
		}


		if(!$accessTokenJson){
			unset($_SESSION[$scopeSpecificTokenName]);
		}else{
			$_SESSION[$scopeSpecificTokenName] = $accessTokenJson;
		}
		return $this;
	}
	public function getToken(){
		if($this->scopes){
			$scopeSpecificTokenName = 'access_token_usersync_'.md5(json_encode($this->scopes));
		}else{
			$scopeSpecificTokenName = 'access_token_usersync';
		}

		return (isset($_SESSION[$scopeSpecificTokenName]) ? $_SESSION[$scopeSpecificTokenName] : false);
	}

	public function setInternalRedirect($redirectUrl){
		$_SESSION['ga_internal_redirect_url'] = $redirectUrl;
		$this->internalRedirectUrl = $redirectUrl;
	}

	public function setScopes($scopes){
		$this->scopes = $scopes;
	}

	public function getDomain(){
		return $this->domain;
	}
}

?>
