<?php

/**
 * GoogleappsAppSettingsForm class.
 *
 * @property string $googleapps_domain
 * @property string $googleapps_consumer_key
 * @property string $googleapps_consumer_secret
 */
class GoogleappsAppSettingsForm extends CFormModel {
	public $googleapps_domain;
	public $googleapps_consumer_key;
	public $googleapps_consumer_secret;


	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			array('googleapps_domain, googleapps_consumer_key, googleapps_consumer_secret', 'safe'),
		);
	}


	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array(
			'googleapps_domain' => Yii::t('GoogleappsApp', 'Google Apps Domain'),
			'googleapps_consumer_key' => Yii::t('GoogleappsApp', 'Consumer key'),
			'googleapps_consumer_secret' => Yii::t('GoogleappsApp', 'Consumer secret'),
		);
	}
}
