<?php

class GoogleappsAppController extends Controller {


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		$admin_only_actions = array('settings');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow the Google login to non-logged in users
		$res[] = array(
			'allow',
			'actions' => array('gappsLogin', 'googleappsRedirect'),
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}

	public function actionGappsLogin(){

        // Check if the use is Coming from Hydra Frontend
        $isComingFromHydra = isset($_GET['sso_auth_mode']) && $_GET['sso_auth_mode']  == 'oauth2' && isset($_GET['sso_target']) && $_GET['sso_target'] == 'hydra';
        Yii::app()->session['isComingFromHydra'] = $isComingFromHydra || Yii::app()->legacyWrapper->hydraFrontendEnabled();

		$installFrom = (Settings::getCfg('install_from')=='gapps_docebo' ? 'gapps' : 'saas');

		// Read auth mode. Possible auth modes:
		// - "session" (default): LMS login session
		// - "oauth2": generates and returns an OAuth2 session (used only by webapp)
		$authentication_mode = Yii::app()->session['sso_auth_mode'];
		if(!$authentication_mode)
			$authentication_mode = Yii::app()->request->getParam('auth_mode', 'session');
		Yii::app()->session['sso_auth_mode'] = $authentication_mode;

		try{
			$model = new GoogleappsHelper($installFrom);
		} catch (Exception $e){
			if($installFrom=='gapps'){
                // The user did not Manage to login, Check from where he is coming from and redirect him accordingly
                if (Yii::app()->session['isComingFromHydra']) {
                    unset(Yii::app()->session['isComingFromHydra']);
                    $url = Docebo::createHydraUrl('learn', 'signin', array( "type" => 'oauth2_response' , 'error' => Yii::t('login', '_NOACCESS')) );
                    $this->redirect($url);
                }

				// Just rethrow the exception, we can't do much
				// for LMSes installed from Gapps Marketplace
				// since they don't have a settings screen where
				// they can modify secret keys (besides editing config files)
				throw new Exception($e->getMessage());
			}else{
                // The user did not Manage to login, Check from where he is coming from and redirect him accordingly
                if (Yii::app()->session['isComingFromHydra']) {
                    unset(Yii::app()->session['isComingFromHydra']);
                    $url = Docebo::createHydraUrl('learn', 'signin', array( "type" => 'oauth2_response', 'error' => $e->getMessage() ) );
                    $this->redirect($url);
                }

				echo $e->getMessage();exit;
				// LMS using our APP so redirect user to the app's
				// settings screen where he may enter the secret keys, etc
                $this->redirect( Docebo::createAdminUrl('GoogleappsApp/GoogleappsApp/settings') );
			}
			Yii::app()->end();
		}

		// Where should the user be redirected after loggin in (back here)
		$model->setInternalRedirect(Docebo::createAbsoluteUrl('GoogleappsApp/GoogleappsApp/gappsLogin'));

		$model->setScopes(array('openid','email'));

		$model->loginUserGapps();

	}

	/**
	 * A universal action for our app and Google App Marketplace installed
	 * LMSes. If there's an access token in session, will make API calls
	 * to Google and render the sync users interface with the following
	 * information: 1) count of users in Google apps domain; 2) count of
	 * LMS users; 3) grid with Google apps users with their firstname, email, etc
	 * If there is no access token, it will redirect to Google for authentication
	 * by the user with his Google account and then redirect back once approved
	 *
	 * @throws Exception - If LMS installed from Google Marketplace
	 * and the redirect for authentication failed (e.g. the config
	 * file is missing the consumer key/secret). Also thrown for a few
	 * other API call failures.
	 */
	public function actionSyncUsers(){
		if(Settings::get('googleapps_active') != 'on'){
			echo 'The app is not active'; return;
		}

		$installFrom = (Settings::getCfg('install_from')=='gapps_docebo' ? 'gapps' : 'saas');

		try{
			$model = new GoogleappsHelper($installFrom);
		} catch (Exception $e){
			if($installFrom=='gapps'){
				// Just rethrow the exception, we can't do much
				// for LMSes installed from Gapps Marketplace
				// since they don't have a settings screen where
				// they can modify secret keys (besides editing config files)
				throw new Exception($e->getMessage());
			}else{
				// LMS using our APP so redirect user to the app's
				// settings screen where he may enter the secret keys, etc
				header("Location: ".Docebo::createAdminUrl('GoogleappsApp/GoogleappsApp/settings'));
			}
			die();
		}

		// Set the page where to redirect users after approval with his
		// Google account in the Google authorization page. After Google
		// redirects back it appends a request token to this redirect URL.
		// We use that token to make API calls (after exchanging it for
		// an access token with an API call).
		$model->setInternalRedirect(Docebo::createAbsoluteUrl('GoogleappsApp/GoogleappsApp/syncUsers'));

		// Will redirect for authentication if needed, otherwise does nothing
		$model->authenticate();

		try{
			// Get an array of users in the Google apps domain
			// using existing access token
			$gappsUsers = $model->getGappsUsers(false);
		}catch(Exception $e){
			// redirect for reauthentication, probably the token
			// is invalid (expired or revoked)
			Yii::log('Error getting Google Apps users. Error: '.$e->getMessage());

			$model->setToken(false);
			$model->authenticate();

			// Redirection for reauthentication failed. Die!
			throw new Exception($e->getMessage());
			exit;
		}

		if($installFrom=='saas'){
			try{
				// 3legged oAuth has a separate API method
				// for getting total users count, since the
				// real API call is slower and is paginated
				// (doesn't return all users)
				$usersCountGapps = $model->countGappsUsers();
			} catch (Exception $e){
				// Redirect for reauthentication
				// Access token failure (expired or revoked)
				Yii::log('Error counting Google Apps users. Error: '.$e->getMessage());
				$model->setToken(false);
				$model->authenticate();
				exit;
			}
		}else{
			$usersCountGapps = count($gappsUsers);
		}


		if($gappsUsers){
			// Check which gapps users exist in the LMS
			// and mark them as such in the array that will populate the grid
			$gappsUserEmailsArr = array_keys($gappsUsers);

			$condition = new CDbCriteria();
			$condition->select = 'email';
			$condition->addInCondition('email', $gappsUserEmailsArr);
			$matchingDoceboUsers = CoreUser::model()->findAll($condition);
			if($matchingDoceboUsers){
				foreach($matchingDoceboUsers as $doceboUser){
					if(isset($doceboUser->email) && !empty($doceboUser->email)){
						$email = $doceboUser->email;

						if(isset($gappsUsers[$email]) && is_array($gappsUsers[$email])){
							$gappsUsers[$email]['exists_in_lms'] = true;
						}
					}
				}
			}
		}

		// Used to populate the grid
		$gappsUsersArr = new CArrayDataProvider(array_values($gappsUsers), array(
			'pagination' => array('pageSize' => 50),
		));
		
		$usersCountDocebo = CoreUser::model()->getUsersCount();

		$this->render('sync_users', array(
		    'usersCountGapps' => (int)$usersCountGapps,
		    'usersCountDocebo' => (int)$usersCountDocebo,
		    'gappsUsers' => $gappsUsersArr, // data provider for CGridView
		    'users'=>$users,
		    'model'=>$model
		));
	}

	/**
	 * Ajax call after button click. Does the actual syncing of Google users to LMS
	 */
	public function actionAxSyncUsers(){
		$installFrom = (Settings::getCfg('install_from')=='gapps_docebo' ? 'gapps' : 'saas');

		$model = new GoogleappsHelper($installFrom);
		// $model->setInternalRedirect(Docebo::createAbsoluteUrl('GoogleappsApp/GoogleappsApp/syncUsers'));

		// Will redirect for authentication if needed
		$model->authenticate();

		try{
			$gappsUsers = $model->getGappsUsers();
		}catch(Exception $e){
			// redirect for reauthentication
			Yii::log('Error getting Google Apps users. Error: '.$e->getMessage());
			$response = array(
			    'success'=>false,
			    'message'=>'Error getting Google Apps users. Response: '.$e->getMessage(),
			);
			echo CJSON::encode($response);
		}

		if($gappsUsers){

			$sync = $model->syncUsers($gappsUsers);

			if($sync['success']){
				$result = array(
				    'success'=>true,
				    'message'=>Yii::t('standard', 'Users synchronized: {count}', array('{count}'=>$sync['user_count']))
				);
				echo CJSON::encode($result);
			}else{
				$result = array(
				    'success'=>false,
				    'message'=>Yii::t('standard', 'At least one user failed synchronization', array(
						'{count}' => $sync['error_count']
					))
				);
				echo CJSON::encode($result);
			}
		} else {
			$result = array(
				'success'=>true,
				'message'=>Yii::t('standard', 'Users synchronized: {count}', array('{count}'=>$userSyncedCounter))
			);
			echo CJSON::encode($result);
		}
	}

	public function actionSettings() {

		$settings = new GoogleappsAppSettingsForm();
		$settings->googleapps_domain = Settings::get('googleapps_domain');
		$settings->googleapps_consumer_key = Settings::get('googleapps_consumer_key');
		$settings->googleapps_consumer_secret = Settings::get('googleapps_consumer_secret');

		if (isset($_POST['GoogleappsAppSettingsForm'])) {
			$settings->setAttributes($_POST['GoogleappsAppSettingsForm']);

			if ($settings->validate()) {

				Settings::save('googleapps_domain', $settings->googleapps_domain, 'string', 255, 'main', 14, 1);
				Settings::save('googleapps_consumer_key', $settings->googleapps_consumer_key, 'string', 255, 'main', 14, 2);
				Settings::save('googleapps_consumer_secret', $settings->googleapps_consumer_secret, 'string', 255, 'main', 14, 3);

				Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));
			}
			else {
				Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
			}
		}

		$this->render('settings', array('settings' => $settings));
	}

	public function actionGoogleappsRedirect(){
		$code = isset($_GET['code']) ? $_GET['code'] : null;
		if($code){
			if(isset($_SESSION['ga_internal_redirect_url']))
				header("Location: ".$_SESSION['ga_internal_redirect_url'].'&code='.$code);
			elseif(isset($_GET['state']))
				header("Location: ".$_GET['state'].'&code='. $code);
		}else{
			echo 'No code provided';
		}
	}

}
