<?php

class LabelAppModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'LabelApp.models.*',
			'LabelApp.components.*',
			'LabelApp.controllers.*',
		));
	}

	/**
	* This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	*/
	public function menu() {
		// Add menu voices !
		Yii::app()->mainmenu->addAppMenu('label', array(
			'icon' => 'admin-ico labels',
			'app_icon' => 'fa-tags', // icon used in the new menu
			'link' => Docebo::createAdminUrl('//LabelApp/labelManagement/index'),
			'label' => Yii::t('standard', '_LABELS'),
		), array());
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}

}
