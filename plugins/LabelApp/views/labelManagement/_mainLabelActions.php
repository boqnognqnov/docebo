<div class="main-actions clearfix">
	<h3 class="title-bold"><?php echo Yii::t('standard', '_LABELS'); ?></h3>
	<ul class="clearfix">
		<li>
			<div>
				<?php $this->renderPartial('//common/_modal', array(
					'config' => array(
						'class' => 'new-label',
						'modalTitle' => Yii::t('standard', '_CREATE'),
						'linkTitle' => '<span></span>'.Yii::t('standard', '_CREATE'),
						'linkOptions' => array(
							'alt' => Yii::t('helper', 'New label')
						),
						'url' => 'LabelApp/labelManagement/create',
						'buttons' => array(
							array(
								'type' => 'submit',
								'title' => Yii::t('standard','Confirm'),
							),
							array(
								'type' => 'cancel',
								'title' => Yii::t('standard','_CANCEL'),
							),
						),
						'afterLoadingContent' => 'function() { schemeFormInit(); $(\'.modal.in\').find(\'#userForm-details, #userForm-orgchat\').jScrollPane({autoReinitialise: true}); }',
						'beforeSubmit' => 'beforeCourseCreateSubmit'
						),
				)); ?>
			</div>
		</li>
	</ul>
	<div class="info">
	    <div>
			<h4 class="clearfix"></h4>
			<p></p>
	    </div>
	</div>
</div>

<script>

	$('.modal.new-label a.btn.btn-submit').live('click', function(ev) {
		var btn = $(this);
		btn.addClass('disabled');
		btn.prop('disabled', true);

		var modal = $('.modal.new-label');
		var data = modal.find("form").serializeArray();

		$.ajax({
			'dataType': 'json',
			'type': 'POST',
			'url': modal.find('form').attr('action'),
			'cache': false,
			'data': data,
			'success': function (data) {
				$.fn.updateLabelContent(data);
				hideLoading('contentLoading');
				setTimeout(function() {
					btn.removeClass('disabled');
					btn.prop('disabled', false);
				}, 1000);
			}
		});
	})

</script>
