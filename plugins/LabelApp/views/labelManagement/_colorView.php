<div class="color-item">
	<div class="wrap">
		<?php echo $form->textfield($model, 'color', array('value' => (!empty($model->color)?$model->color:'#444444'))); ?>
		<div class="tooltip" style="opacity: 1; display: none;">
			<div class="colorpicker"></div>
		</div>
		<div class="color-preview-wrap">
			<div class="color-preview" style="display: block; width: 21px; height: 21px; background-color: <?php
				if(!$model->color) {
					echo '#444444';
				} else {
					echo $model->color;
				}
			?>;"></div>
		</div>
	</div>
</div>