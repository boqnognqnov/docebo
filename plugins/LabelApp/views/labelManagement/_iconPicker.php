<?php
/* @var $model LearningLabel */
/* @var $form CActiveForm */
?>
<button id="iconPicker" class="btn btn-default" data-search="true" data-search-text="<?=Yii::t('standard', '_SEARCH')?>..."
				 data-placement="right" data-iconset="fontawesome" data-icon="<?=$model->icon?>" name="LearningLabel[icon]"></button>

<script type="text/javascript">
	<!--
	$(document).ready(function(){
		$("#iconPicker").each(function() {
			if($(this).children().length > 0)
				$(this).remove();
		});

		$("#iconPicker").iconpicker({
			arrowNextIconClass: 'fa fa-arrow-right',
			arrowPrevIconClass: 'fa fa-arrow-left'
		});

		$("#iconPicker").on('change', function(e) {
			$('.preview-tile > i').attr('class', 'colorable fa '+ e.icon+' fa-3x fa-inverse');
		});
	});
	//-->
</script>