<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', '_LABELS'),
); ?>

<?php $this->renderPartial('_mainLabelActions'); ?>

<div class="bottom-section border-bottom-section clearfix">
	<div id="grid-wrapper">
		<?php
		$buttonsJson = json_encode(array(
			array("type" => "submit", "title" => Yii::t("standard", "Confirm")),
			array("type" => "cancel", "title" => Yii::t("standard", "_CANCEL"))
		));
		$this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'label-management-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix label-management-table'),
			'dataProvider' => $labelModel->dataProvider(),
			'template' => '{items}{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
			'pager' => array(
				'class' => 'ext.local.pagers.DoceboLinkPager',
			),
			'ajaxUpdate' => 'all-items',
			'afterAjaxUpdate' => 'function(id, data){ labelSortInit(); }',
			'columns' => array(
				array(
					'header' => Yii::t('standard' ,'_NAME'),
					'name' => 'title',
				),
				array(
					'header' => Yii::t('standard' ,'_DESCRIPTION'),
					'name' => 'description',
				),
				array(
					'header' => Yii::t('templatemanager', 'Background color'),
					'type' => 'raw',
					'value' => '$data->renderColor();',
					'htmlOptions' => array('class' => 'center-aligned'),
					'headerHtmlOptions' => array('class' => 'center-aligned'),
				),
				array(
					'type' => 'raw',
					'value' => '$data->renderMove();',
					'htmlOptions' => array('class' => 'move button-column-single')
				),
				array(
					'name' => '',
					'type' => 'raw',
					'value' => 'CHtml::link("", "", array(
						"class" => "ajaxModal edit-action",
						"data-toggle" => "modal",
						"data-modal-class" => "edit-node",
						"data-modal-title" => Yii::t("standard", "Edit Label"),
						"data-buttons" => \''.$buttonsJson.'\',
						"data-url" => "LabelApp/labelManagement/update&id=$data->id_common_label",

						"data-after-loading-content" => "schemeFormInit(); $(\'.modal.in\').find(\'#userForm-details, #userForm-orgchat\').jScrollPane({autoReinitialise: true});
						//$(\'#user_form, #orgChart_form, #add-field-form\').jClever({applyTo: {select: true,checkbox: false,radio: false,button: false,file: false,input: false,textarea: false}});",

						"data-after-submit" => "updateLabelContent",
					));',
					'htmlOptions' => array('class' => 'button-column-single')
				),
				array(
					'name' => '',
					'type' => 'raw',
					'value' => 'CHtml::link("", "", array(
						"class" => "ajaxModal delete-action",
						"data-toggle" => "modal",
						"data-modal-class" => "delete-node",
						"data-modal-title" => Yii::t("standard", "Delete Label"),
						"data-buttons" => \''.$buttonsJson.'\',
						"data-url" => "LabelApp/labelManagement/delete&id=$data->id_common_label",
						"data-after-loading-content" => "hideConfirmButton();",
						"data-after-submit" => "updateLabelContent",
					));',
					'htmlOptions' => array('class' => 'button-column-single')
				),
			),
		)); ?>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('select').styler();
		$('.powerUserProfileSelect').hide();
	});
</script>
