<div class="form" style="min-height: 550px;">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'label_form',
		'htmlOptions' => array('class' => 'clearfix'),
	)); ?>

	<?php
		if (!empty($model->id_common_label)) {
			echo $form->hiddenField($model, 'id_common_label');
			echo $form->hiddenField($model, 'sequence');                        
		}
        
        $userLangCode = $model->lang_code;
	?>
		<div class="preview-title"><?= Yii::t('label', 'Label Preview') ?></div>
		<div class="preview-content">
			<div class="row-fluid">
				<div class="span2 preview-tile">
					<i class="colorable fa <?php
						if(!$model->icon) {
							echo 'fa-500px';
						} else {
							echo $model->icon;
						}
					?> fa-3x fa-inverse"<?php
						if(!$model->color) {
							echo ' style="background-color: #444444;"';
						} else {
							echo ' style="background-color: '.$model->color.'"';
						}
					?>></i>
				</div>
				<div class="span10" style="margin-left: 0px;">
					<div class="preview-content-title colorable-text">
						<?php echo (!empty($labelLangs[$userLangCode]['title'])?$labelLangs[$userLangCode]['title']:'Label Title'); ?>
					</div>
					<div class="preview-content-description">
						<?php echo ((!empty($labelLangs[$userLangCode]['description']))?$labelLangs[$userLangCode]['description']:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur, quod porro ducimus natus eius blanditiis!'); ?>
					</div>
				</div>
			</div>
		</div>

		<hr />

		<div class="label-icon">
			<div class="label-icon-title"><?= Yii::t('label', 'Icon') ?></div>
			<?php $this->renderPartial('_iconPicker', array('model' => $model, 'form' => $form)); ?>
		</div>

		<div class="label-color clearfix">
			<div class="label-color-title"><?= Yii::t('label', 'Icon background and title color') ?>:</div>
			<div class="colors">
				<?php $this->renderPartial('_colorView', array('model' => $model, 'form' => $form)); ?>
			</div>
		</div>

		<hr />
		<div class="clearfix">
			<div class="dropdown_form_title"><?= Yii::t('label', 'Label title and description') ?></div>
			<div class="languagesList" style="margin-right: 20px;">
				<?php echo $form->dropDownList($model, 'lang_code', $activeLanguagesList); ?>
				<?php echo $form->error($model, 'lang_code'); ?>
			</div>

			<div class="label_languages">
				<div><?php echo Yii::t('admin_lang', '_LANG_ALL').': <span>' . count($activeLanguagesList) . '</span>'; ?></div>
				<div id="label_assignedCount"><?php echo Yii::t('standard', 'Filled languages').': <span>' . ((!empty($assignedCount)) ? $assignedCount : 0) . '</span>'; ?></div>
			</div>

			<div class="label_translationsList">
				<?php if (!empty($activeLanguagesList)) {
					reset($activeLanguagesList);
					$first_key = key($activeLanguagesList);
					?>
					<?php foreach ($activeLanguagesList as $key => $lang) {
						if (($key == $first_key && empty($model->lang_code)) || ($model->lang_code == $key)) {
							$class = 'show';
						} else {
							$class = 'hide';
						}
						?>
						<div id="labelTranslation_<?php echo $key; ?>" class="languagesName <?php echo $class; ?>">
							<div>
								<div class="label_form_title"><?php echo Yii::t('standard','Title') . ' (' . str_replace(' *', '', $lang) . ')'; ?></div>
								<?php echo $form->textField($model, 'title[' . $key . ']', array(
									'class' => 'label_title',
									'value' => (!empty($labelLangs[$key]['title']) ? $labelLangs[$key]['title'] : '')
								)); ?>
								<?php echo $form->error($model, 'title'); ?>
							</div>
							<div>
								<div class="label_form_title"><?php echo Yii::t('standard','Description') .' (' . str_replace(' *', '', $lang) . ')'; ?></div>
								<?php echo $form->textArea($model, 'description[' . $key . ']', array(
									'class' => 'label_description',
									'value' => (!empty($labelLangs[$key]['description']) ? $labelLangs[$key]['description'] : '')
								)); ?>
								<?php echo $form->error($model, 'description'); ?>
							</div>
						</div>
					<?php } ?>
				<?php } ?>
			</div>
		</div>

	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
	$('select').styler();
	schemeFormInit();
</script>
