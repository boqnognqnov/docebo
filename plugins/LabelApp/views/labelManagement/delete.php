<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'label_form',
	)); ?>

	<p><?php echo Yii::t('label', 'Are you sure you want to delete "{label}" label?', array("{label}" => $labelTitle)); ?></p>

	<div class="clearfix">
		<?php echo $form->checkbox($model, 'confirm', array('id' => 'LearningLabel_confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo $form->labelEx($model, Yii::t('label', 'Yes, I want to delete "{label}" label', array('{label}' => $labelTitle)), array('for' => 'LearningLabel_confirm_'.$checkboxIdKey)); ?>
		<?php echo $form->error($model, 'confirm'); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>