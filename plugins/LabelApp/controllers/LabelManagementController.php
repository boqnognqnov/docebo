<?php

	class LabelManagementController extends AdminController {


		/**
		 * @return array action filters
		 */
		public function filters() {
			return array(
				'accessControl', // perform access control for CRUD operations
			);
		}


		/**
		 * Specifies the access control rules.
		 * This method is used by the 'accessControl' filter.
		 * @return array access control rules
		 */
		public function accessRules() {
			$res = array();
			// Allow following actions to admin only:
			$admin_only_actions = array(
				'settings',
				'index',
				'create',
				'update',
				'delete',
				'order',
			);

			$res[] = array(
				'allow',
				'roles' => array(Yii::app()->user->level_godadmin),
				'actions' => $admin_only_actions,
			);

			// deny admin only actions to all other users:
			$res[] = array(
				'deny',
				'actions' => $admin_only_actions,
			);

			// Allow access to other actions only to logged-in users:
			$res[] = array(
				'allow',
				'users' => array('@'),
			);

			$res[] = array(
				'deny', // deny all
				'users' => array('*'),
			);

			return $res;
		}

		/**
		 * This is the default 'index' action that is invoked
		 * when an action is not explicitly requested by users.
		 */
		public function actionIndex() {
			Yii::app()->clientScript->registerCssFile('js/farbtastic/farbtastic.css');
			Yii::app()->clientScript->registerCoreScript('jquery.ui');
			Yii::app()->clientScript->registerScriptFile('js/farbtastic/farbtastic.min.js');
			Yii::app()->clientScript->registerScriptFile('js/jquery.tools.min.js');
			Yii::app()->fontawesome->loadIconPicker();

			$labelModel = new LearningLabel();
			$this->render('index', array(
				'labelModel' => $labelModel,
			));
		}

		public function actionCreate() {
			if (Yii::app()->request->isAjaxRequest) {
				$labelModel = new LearningLabel();
                $labelModel->lang_code = CoreUser::getDefaultLangCode();
				$activeLanguagesList    = CoreLangLanguage::getActiveLanguages();
				if (isset($_POST['LearningLabel'])) {
					$allTitleTranslationEmpty = true;
					if (!empty($_POST['LearningLabel']['title'])) {
						foreach ($_POST['LearningLabel']['title'] as $title) {
							if (!empty($title)) {
								$allTitleTranslationEmpty = false;
							}
						}
						if ($allTitleTranslationEmpty) {
							$labelModel->addError("title", Yii::t("label", "Title name can't be empty"));
						} elseif(isset($_POST['LearningLabel']['title'][CoreLangLanguage::getDefaultLanguage()]) && !$_POST['LearningLabel']['title'][CoreLangLanguage::getDefaultLanguage()]) {
							$labelModel->addError('lang_code', Yii::t('user_management', '_ERROR_ADDITIONAL_FIELDS_ENGLISH_REQUIRED'));
						} elseif ($labelModel->validate()) {
							$criteria          = new CDbcriteria();
							$criteria->select  = 'max(id_common_label) as id_common_label';
							$maxLabel 				 = $labelModel->find($criteria);
							$currentId         = $maxLabel->id_common_label + 1;

							$criteria          = new CDbcriteria();
							$criteria->select  = 'max(sequence) as sequence';
							$maxLabelOrder 		 = $labelModel->find($criteria);
							$currentOrder      = $maxLabelOrder->sequence + 1;
							if (!empty($currentId) && !empty($activeLanguagesList)) {
								foreach ($activeLanguagesList as $key => $lang) {
									$translation              		= new LearningLabel();
									$translation->id_common_label	= $currentId;
									$translation->lang_code   		= $key;			
									$translation->title 					= $_POST['LearningLabel']['title'][$key];
									$translation->description 		= $_POST['LearningLabel']['description'][$key];
									$translation->color 					= $_POST['LearningLabel']['color'];
									$translation->icon 					= $_POST['LearningLabel']['icon'];
									$translation->sequence 				= $currentOrder;
									$translation->save();
								}
							}
							$this->sendJSON(array());
						}
					}
				}
				$content = $this->renderPartial('_form', array(
					'model' => $labelModel,
					'activeLanguagesList' => $activeLanguagesList
				), true);
				$this->sendJSON(array('html' => $content));
			}
			Yii::app()->end();
		}

		public function actionUpdate($id = null) {
			if (Yii::app()->request->isAjaxRequest) {
				$labelLangModels = LearningLabel::model()->findAllByAttributes(array('id_common_label' => $id));
				if (empty($labelLangModels)) {
					$this->sendJSON(array());
				}


				$labelLangs = CHtml::listData($labelLangModels, 'lang_code', function($data){
					return array(
						'title' => $data->title,
						'description' => $data->description,
					);
				});

				$assignedCount = 0;
				foreach ($labelLangs as $label) {
					if (!empty($label['title']) || !empty($label['description']))  $assignedCount++;
				}
                
                // select label by default language
                $lang_code = CoreUser::getDefaultLangCode();
                
                foreach ($labelLangModels as $row) {
                    if ($row->lang_code == $lang_code && $row->title) {
                        $labelModel = $row;
                        break;
                    }
                }
                
                // get first label with title
                if (!isset($labelModel)) {
                    foreach ($labelLangModels as $row) {
                        if ($row->title) {
                            $labelModel = $row;
                            break;
                        }
                    }
                }

                if (!isset($labelModel))
                    $labelModel = $labelLangModels[0];

				$activeLanguagesList    = CoreLangLanguage::getActiveLanguages();
				if (isset($_POST['LearningLabel'])) {
					$allTitleTranslationEmpty = true;
					if (!empty($_POST['LearningLabel']['title'])) {
						foreach ($_POST['LearningLabel']['title'] as $title) {
							if (!empty($title)) {
								$allTitleTranslationEmpty = false;
							}
						}
						if ($allTitleTranslationEmpty) {
							$labelModel->addError("title", Yii::t("label", "Title name can't be empty"));
						} elseif ($labelModel->validate()) {
							$currentId    = $_POST['LearningLabel']['id_common_label'];
							$currentOrder	= $_POST['LearningLabel']['sequence'];
							if (!empty($currentId) && !empty($activeLanguagesList)) {
								foreach ($activeLanguagesList as $key => $lang) {
									$translation = LearningLabel::model()->findByAttributes(array('id_common_label' => $currentId, 'lang_code' => $key));
									if (empty($translation)) {
										$translation = new LearningLabel();
										$translation->id_common_label	= $currentId;
										$translation->lang_code   		= $key;			
										$translation->sequence 				= $currentOrder;
									}
									$translation->title 					= $_POST['LearningLabel']['title'][$key];
									$translation->description 		= $_POST['LearningLabel']['description'][$key];
									$translation->color 					= $_POST['LearningLabel']['color'];
									$translation->icon 					= $_POST['LearningLabel']['icon'];
									$translation->save();
								}
							}
							$this->sendJSON(array());
						}
					}
				}
				$content = $this->renderPartial('_form', array(
					'model' => $labelModel,
					'labelLangs' => $labelLangs,
					'activeLanguagesList' => $activeLanguagesList,
					'assignedCount' => $assignedCount
				), true);
				$this->sendJSON(array('html' => $content));
			}
			Yii::app()->end();
		}

		public function actionDelete($id) {
			$labelModel = LearningLabel::model()->findByAttributes(array('id_common_label' => $id));
			$labelTitle = LearningLabel::model()->findByAttributes(array('id_common_label' => $id, 'lang_code' => Lang::getCodeByBrowserCode(Yii::app()->getLanguage())));
			if(empty($labelTitle->title)) {
				$labelTitle = LearningLabel::model()->findByAttributes(array('id_common_label' => $id, 'lang_code' => CoreLangLanguage::getDefaultLanguage()));
			}
			if (empty($labelModel)) {
				$this->sendJSON(array());
			}

			if (!empty($_POST['LearningLabel'])) {
				if ($_POST['LearningLabel']['confirm']) {
					$labelModel->deleteAllByAttributes(array('id_common_label' => $id));
					$this->sendJSON(array());
				}
			}
			if (Yii::app()->request->isAjaxRequest) {
				$content = $this->renderPartial('delete', array(
					'model' => $labelModel,
					'labelTitle' => $labelTitle->title
				), true);
				$this->sendJSON(array(
					'html' => $content,
				));
			}
			Yii:app()->end();
		}

		public function actionOrder() {
			$ids = $_POST['ids'];
			$command = Yii::app()->db->createCommand();

			if (!empty($ids)) {
				foreach ($ids as $weight => $id) {
					$command->update(LearningLabel::model()->tableName(), array('sequence' => $weight), 'id_common_label = :id', array(':id' => $id));
				}
			}
			$this->sendJSON(array());
		}

}
