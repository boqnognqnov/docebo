<?php

class EyHcmAppController extends Controller {

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		$admin_only_actions = array('settings');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}

	/**
	 * Returns the list of fields to be showed in the dropdown
	 * @return array
	 */
	private function getFieldsList() {
		// array of attributes "type" for CoreUserField model
		$additionalFieldsExcept = array(CoreUserField::TYPE_UPLOAD);

		$list = array(0 => "=== " . Yii::t('standard', '_NONE') . " ===");

		$criteria = new CDbCriteria();
		$criteria->addNotInCondition('type', $additionalFieldsExcept);

		foreach (CoreUserField::model()->language()->findAll($criteria) as $field) {
		    /** @var CoreUserField $field */
			$list[$field->getFieldId()] = $field->getTranslation();
        }

		return $list;
	}

	/**
	 * Settings page action
	 */
	public function actionSettings() {
		$settings = new EyHcmAppSettingsForm();

		try {
			if (isset($_POST['EyHcmAppSettingsForm'])) {
				$settings->setAttributes($_POST['EyHcmAppSettingsForm']);
				if ($settings->validate()) {
					Settings::save('eyhcm_code_field', $settings->code_field);
					Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));
				} else
					throw new Exception(var_export($settings->getErrors(), true));
			}
		} catch (CHttpException $e) {
			Yii::app()->user->setFlash('error', $e->getMessage());
		} catch (Exception $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
		}

		$settings->code_field = Settings::get('eyhcm_code_field');
		$fields = $this->getFieldsList();
		$this->render('settings', array('settings' => $settings, 'additional_fields' => $fields));
	}

}
