<?php

/**
 * EyHcmAppSettingsForm class.
 *
 * @property string $catalog_type
 */

/**
 * Nothing to do here, really
 */
class EyHcmAppSettingsForm extends CFormModel {

	/**
	 * Code field to use in API
	 * @var
	 */
	public $code_field;

	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			array('code_field', 'safe'),
			array('code_field', 'required')
		);
	}
}
