<?php

class EyHcmAppModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'EyHcmApp.models.*',
			'EyHcmApp.components.*',
		));

		Yii::app()->event->on('LoadApiModule', array($this, 'registerApiModules'));
	}


	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu() {
		Yii::app()->mainmenu->addAppMenu('eyHcm', array(
			'icon' => 'home-ico eyhcm',
			'settings' => Docebo::createAppUrl('admin:EyHcmApp/EyHcmApp/settings'),
			'label' => 'EY Hcm',
			'app_initials' => 'EY'
		), array());
	}

	/**
	 * Register custom api modules of this plugin
	 * @param $event DEvent
	 */
	public function registerApiModules($event)
	{
		$moduleName = $event->params['moduleName'];
		if ($moduleName == 'user')
			$event->return_value['targetApiComponent'] = new EyHcmApiModule();
	}
}
