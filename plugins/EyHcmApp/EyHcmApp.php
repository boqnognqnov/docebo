<?php

/**
 * Plugin for CoursecatalogApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class EyHcmApp extends DoceboPlugin {

	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'EyHcmApp';

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name = __CLASS__) {
		return parent::plugin($class_name);
	}

	public function activate() {
		parent::activate();

		// Create the setting
		Settings::save('eyhcm_code_field', 0);
	}

	public function deactivate() {
		parent::deactivate();
	}

}
