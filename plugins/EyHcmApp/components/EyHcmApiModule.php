<?php

/**
 * Ey Hcm extension for User API Module
 * @description includes a specific new function
 */
class EyHcmApiModule extends UserApiModule {

	/**
	 * @summary Gets the list of user idsts, given user's code
	 * @parameter code [string, required] User code
	 *
	 * @response id_users [string, required] Set of comma separated user ids
	 *
	 * @status 201 Missing code additional field
	 * @status 202 Code additional field not properly set
	 * @status 203 Missing code mandatory param
	 */
	public function getUsersByCode() {
		// Check the input param first
		$code = $this->readParameter('code', false);
		if (!$code)
			throw new CHttpException(203, "Missing code mandatory param");

		// Check if there is an additional field set for the code
		$codeFieldId = Settings::get('eyhcm_code_field', null);
		if(is_null($codeFieldId)) {
			// Settings not yet in DB -> let's create it and set it to 0
			Settings::save('eyhcm_code_field', 0);
			$codeFieldId = 0;
		}
		if($codeFieldId === 0)
			throw new CHttpException(201, "Missing code additional field");

		// If field is set, then try to load it
		$codeField = CoreUserField::model()->findByPk($codeFieldId);
		if(!$codeField) {
			throw new CHttpException(202, "Code additional field not properly set");
        }

		// Find all users with the given field value
		$idsts = array();
		$command = Yii::app()->db->createCommand('SELECT id_user '
			. ' FROM ' . CoreUserFieldValue::model()->tableName() . ' '
			. ' WHERE field_' . $codeFieldId . ' LIKE :code');

		/* @var $command CDbCommand */
		$command->bindValue(':code', $code);
		$reader = $command->query();

		while ($record = $reader->read()) {
			$idsts[] = $record['id_user'];
        }

		// Return list of idst for all users having that code
		$output = array();
		$output['id_users'] = implode(",", $idsts);

		return $output;
	}

}