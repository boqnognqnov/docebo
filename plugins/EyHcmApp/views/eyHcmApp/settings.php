<?php
/* @var $form CActiveForm */
/* @var $settings EyHcmAppSettingsForm */
?>

<h2><?= 'EY Hcm ' . Yii::t('standard', 'Settings') ?></h2>
<br/>


<?php DoceboUI::printFlashMessages(); ?>

<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'app-settings-form',
	'htmlOptions'=>array('class' => 'form-horizontal'),
));
?>

<div class="control-group">
	<label class="span2">API Key :</label>
	<div class="controls">
		<?=$GLOBALS['cfg']['api_key'];?>
	</div>
</div>

<div class="control-group">
	<label class="span2">API Secret :</label>
	<div class="controls">
		<?=$GLOBALS['cfg']['api_secret'];?>
	</div>
</div>

<hr/>

<div class="control-group">
	<label class="span2"><?= Yii::t('standard', '_CODE'); ?></label>
	<?= CHtml::dropDownList('EyHcmAppSettingsForm[code_field]', $settings->code_field, $additional_fields, array('id' => 'code_field')); ?>
	<div class="controls">
	</div>
</div>

<div class="form-actions">
	<?=CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
		'class'=>'btn-docebo green big'
	)); ?>
	&nbsp;
	<a href="<?=$this->createUrl('/site/index');?>" class="btn-docebo black big"><?=Yii::t('standard', '_CANCEL');?></a>
</div>

<?php $this->endWidget(); ?>