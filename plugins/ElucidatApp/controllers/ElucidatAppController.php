<?php

class ElucidatAppController extends Controller {


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();
		
		$admin_only_actions = array('settings');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);
		
		// Allow access to other actions only to logged-in users:
		$res[] = array(
		    'allow',
		    'actions' => array('webhook'),
		    'users' => array('*'),
		);
		

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}


	public function actionIndex() {
        $this->forward('settings');
	}

	/**
	 * Generates random string for password
	 *
	 * @return string Random string
	 */
	public static function generatePassword(){
		$randomPassword = chr(mt_rand(65, 90))
			. chr(mt_rand(97, 122))
			. mt_rand(0, 9)
			. chr(mt_rand(97, 122))
			. chr(mt_rand(97, 122))
			. chr(mt_rand(97, 122))
			. mt_rand(0, 9)
			. chr(mt_rand(97, 122));

		return $randomPassword;
	}

	/**
	 * Create a new user with userid = /elucidat.user. If we already have it, just return his username and pass.
	 *
	 * @return array Username and Password
	 */
	private function createUser(){

		$username = '';
		$pass = '';

		$userModel = new CoreUser();
		$userModel->userid = ElucidatAppModule::ELUCIDAT_USERID;

		$password = self::generatePassword();
		$userModel->new_password = $password;
		$userModel->new_password_repeat = $password;
		$userModel->isNewRecord = true;
		$userModel->force_change = 0;
		$userModel->setScenario('import');
		if($userModel->save()){
			$userModel->saveChartGroups();

			$username = $userModel->userid;
			$pass = $userModel->pass;
		}

		return array(
			'username' => $username,
			'pass' => $pass
		);
	}

	/**
	 * Find the /elucidat.user. If its not found, create it.
	 *
	 * @return array Username and Password
	 */
	private function findUser(){

		$username = '';
		$pass = '';

		$userModel = CoreUser::model()->findByAttributes(array(
			'userid' => ElucidatAppModule::ELUCIDAT_USERID
		));

		if($userModel instanceof CoreUser){
			$username = $userModel->userid;
			$pass = $userModel->pass;
		} else{
			$user = $this->createUser();

			$username = $user['username'];
			$pass = $user['pass'];
		}

		return array(
			'username' => $username,
			'pass' => $pass
		);
	}

	private function getEndpoint(){
		//Creating Endpoint URL
		$url = Yii::app()->getBaseUrl(true);
		$baseUrl = Yii::app()->baseUrl;

		if($baseUrl === '/admin'){
			$url = str_replace('/admin', '', $url);
			$endpoint = $url . '/tcapi/statements';
		} else if($baseUrl = '/lms'){
			$lmsPos = strrpos($url, $baseUrl);
			$urlBase = substr($url, 0, $lmsPos);
			$endpoint = $urlBase . '/tcapi/statements';
		}

		return $endpoint;
	}


	public function retrieveElucidatFullInfoArrayConditionally(ElucidatApiClient $client, $skipProjectsWithoutRelease)
	{
		$releaseTypes = 'all';//Settings::get('elucidat_release_types', 'online_public');

		if ($releaseTypes == 'all')
			return $client->getFullInfoArray(ElucidatApiClient::RELEASE_MODE_ALL, $skipProjectsWithoutRelease);
		elseif ($releaseTypes == 'online_public')
			return $client->getFullInfoArray(ElucidatApiClient::RELEASE_MODE_ONLINE_PUBLIC, $skipProjectsWithoutRelease);
		elseif ($releaseTypes == 'online_private')
			return $client->getFullInfoArray(ElucidatApiClient::RELEASE_MODE_ONLINE_PRIVATE, $skipProjectsWithoutRelease);
	}

	public function actionSettings() {

		$publicKey = Settings::get('elucidat_consumer_key', false);
		$secretKey = Settings::get('elucidat_secret_key', false);
		$releaseTypes = Settings::get('elucidat_release_types', 'all');

		$isConfigured = $publicKey && $secretKey;

		//Just call this function it will get the user or create a new one
		$user = $this->findUser();

		if($_POST){
			if(isset($_POST['cancel'])){
				$this->redirect(Docebo::createLmsUrl('app/index'));
			}

			if(isset($_POST['submit'])){
				$publicKey = Yii::app()->request->getParam('public_key', false);
				$secretKey = Yii::app()->request->getParam('secret_key', false);
				$releaseTypes = Yii::app()->request->getParam('release_type', 'all');

				if(($publicKey && $secretKey) && (strlen($publicKey) > 0 && strlen($secretKey) > 0)){


					$client = new ElucidatApiClient($publicKey, $secretKey);

					$clientInfo = $client->getAccountInfo();

					if($clientInfo->success === true){

						//Subscribe to Elucidat WebHook
						$eventName = ElucidatApiClient::EVENT_RELEASE_COURSE;
						$event = $client->subscribeToEvent($eventName, Docebo::createAbsoluteUrl("ElucidatApp/ElucidatApp/webhook"));

						if($event->success === true){

							//Save settings
							Settings::save('elucidat_consumer_key', $publicKey);
							Settings::save('elucidat_secret_key', $secretKey);
							Settings::save('elucidat_release_types', $releaseTypes);

							Yii::app()->user->setFlash('success', Yii::t("standard", '_OPERATION_SUCCESSFUL'));
							$this->redirect(Docebo::createAdminUrl('ElucidatApp/ElucidatApp/settings'));
						} else{
							Yii::app()->user->setFlash('error', Yii::t('elucidat', 'Something wrong with Elucidat connection'));
							$this->redirect(Docebo::createAdminUrl('ElucidatApp/ElucidatApp/settings'));
						}
					} else{
						Yii::app()->user->setFlash('error', Yii::t('elucidat', 'Invalid Elucidat account provided'));
						$this->redirect(Docebo::createAdminUrl('ElucidatApp/ElucidatApp/settings'));
					}
				} else{
					Yii::app()->user->setFlash('error', Yii::t('elucidat', 'Please, enter consumer and secret key'));
					$this->redirect(Docebo::createAdminUrl('ElucidatApp/ElucidatApp/settings'));
				}
			}
		}
		
        $this->render('index', array(
			'publicKey' => $publicKey,
			'secretKey' => $secretKey,
			'isConfigured' => $isConfigured,
			'releaseType' => $releaseTypes
        ));
	}

	public function actionEditCentralRepo(){
		$step = Yii::app()->request->getParam('step', 1);
		/*Elucidat connection parameters*/
		$publicKey = Settings::get('elucidat_consumer_key', false);
		$secretKey = Settings::get('elucidat_secret_key', false);

		$client = new ElucidatApiClient($publicKey, $secretKey);
		$clientInfo = $client->getAccountInfo(); // Only for configuration check

		Yii::app()->getModule('centralrepo')->registerResources();
		$cs = Yii::app()->getClientScript();
		$playerAssetsUrl = Yii::app()->findModule('player')->assetsUrl;
		$cs->registerCssFile ($playerAssetsUrl.'/css/base.css');

		$validElucidat = true; // Check is the the plugin is configured correctly
		if (!$publicKey || !$secretKey || $clientInfo->success != 1)
			$validElucidat = false;

		$validAuthor = false;
		try {
			$currentUser = CoreUser::model()->findByPk(Yii::app()->user->getId());
			$authors = $client->getAuthors();
			if ( $authors->success ) {
				foreach ( $authors->data as $author ) {
					if ( $currentUser->email === $author['email'] ) {
						$validAuthor = true;
						$ssoLink = $client->getSsoUrl($currentUser->email, 'https://app.elucidat.com/projects');
						break;
					}
				}
			}
		} catch ( Exception $e ) {
			Yii::log( $e->getMessage(), CLogger::LEVEL_ERROR);
		}
		if ( !$validElucidat ) $validAuthor = true;
		switch ( $step ) {
			case 1:
				$this->render('_add_lo_intro',
					array(
						'validAuthor'   => $validAuthor,
						'validElucidat' => $validElucidat,
						'ssoLink'       => $ssoLink,
						'step'          => $step,
						'centralRepoContext' => true
					));
				break;
			case 2:
				$idObject = Yii::app()->request->getParam('id_object', false);

				if($idObject){
					$repoObject = LearningRepositoryObject::model()->findByPk($idObject);
					$short_description = $repoObject->short_description;
					$versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
						'id_object' => $repoObject->id_object
					));

					if($versionModel){
						$loElucidat = LearningElucidat::model()->findByAttributes(array('id_resource'=>$versionModel->id_resource));
						$mode = 'edit';
					}
				}else{
					$repoObject = new LearningRepositoryObject();
					$loElucidat = new LearningElucidat();
					$mode = 'create';
				}
				$result =
					($clientInfo->success != 1)
						? array()
						: $this->retrieveElucidatFullInfoArrayConditionally($client, true);

				$projects = array(0=>Yii::t('elucidat','Select your course...'));
				$releases = array();
				$editUrls = array();
				if($result && count($result)>0) {
					foreach ($result as $project) {
						$projects[$project['project_code']] = $project['name'];
						$editUrls[$project['project_code']] = Docebo::createAbsoluteLmsUrl('ElucidatApp/ElucidatApp/redirectEdit', array(
							'project_url'	=> 	$project['edit_url']
						));
						$counter = 1;
						foreach (array_reverse($project['releases'], true) as $releaseCode => $releaseOptions) {
							$note = ($releaseOptions['description']) ? " (" . $releaseOptions['description'] . ")" : '';
							$releases[$project['project_code']][$releaseCode] = Yii::t('elucidat', 'Release') . " " . Yii::app()->localtime->toLocalDateTime($releaseOptions['created'], Localtime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, null, 'Etc/GMT-1') . $note;
							$counter++;
						}
					}
				}
				$this->render('_edit_lo', array(
					'repoObject'        => $repoObject,
					'loElucidat'    =>$loElucidat,
					'mode'          =>$mode,
					'validAuthor'   => $validAuthor,
					'validElucidat' => $validElucidat,
					'projects'      =>  $projects,
					'editUrls'      =>  $editUrls,
					'releases'      =>  $releases,
					'fullInfo'      =>  $result,
					'short_description'  => $short_description,
					'centralRepoContext' => true,
					'saveUrl' => Docebo::createAbsoluteAdminUrl('ElucidatApp/ElucidatApp/saveCentralRepo')
				));
				break;
			default:
				break;
		}
		Yii::app()->end(); // End !!!
	}


    /**
     * @throws CException
     */
	public function actionAxEditElucidat() {
        
		$step = Yii::app()->request->getParam('step', 1);
        /*Elucidat connection parameters*/
        $publicKey = Settings::get('elucidat_consumer_key', false);
        $secretKey = Settings::get('elucidat_secret_key', false);

        $client = new ElucidatApiClient($publicKey, $secretKey);
        $clientInfo = $client->getAccountInfo(); // Only for configuration check

        $validElucidat = true; // Check is the the plugin is configured correctly
        if (!$publicKey || !$secretKey || $clientInfo->success != 1)
            $validElucidat = false;

        $validAuthor = false;
        try {
            $currentUser = CoreUser::model()->findByPk(Yii::app()->user->getId());
            $authors = $client->getAuthors();
            if ( $authors->success ) {
                foreach ( $authors->data as $author ) {
                    if ( $currentUser->email === $author['email'] ) {
                        $validAuthor = true;
                        $ssoLink = $client->getSsoUrl($currentUser->email, 'https://app.elucidat.com/projects');
                        break;
                    }
                }
            }
        } catch ( Exception $e ) {
            Yii::log( $e->getMessage(), CLogger::LEVEL_ERROR);
        }
        if ( !$validElucidat ) $validAuthor = true;
		switch ( $step ) {
            case 1:
                $html = $this->renderPartial('_add_lo_intro',
                    array(
                        'validAuthor'   => $validAuthor,
                        'validElucidat' => $validElucidat,
                        'ssoLink'       => $ssoLink,
                        'step'          => $step
                    ), true, false);
                break;
            case 2:
                $idCourse = Yii::app()->request->getParam('course_id', false);
                $idResource = Yii::app()->request->getParam('idResource', false);
                $courseModel = LearningCourse::model()->findByPk($idCourse);

                if($idResource && $idCourse){
                	$orgObj = LearningOrganization::model()->findByPk($idResource);
                    $short_description = $orgObj->short_description;
                    $loElucidat = LearningElucidat::model()->findByAttributes(array('id_resource'=>$orgObj->idResource));
                    $mode = 'edit';
                }else{
                    $orgObj = new LearningOrganization();
                    $loElucidat = new LearningElucidat();
                    $mode = 'create';
                }
                $result =
					($clientInfo->success != 1)
						? array()
						: $this->retrieveElucidatFullInfoArrayConditionally($client, true);

                $projects = array(0=>Yii::t('elucidat','Select your course...'));
                $releases = array();
                $editUrls = array();
                if($result && count($result)>0) {
                    foreach ($result as $project) {
                        $projects[$project['project_code']] = $project['name'];
                        
                        
                        //$editUrls[$project['project_code']] = $client->getSsoUrl($currentUser->email, $project['edit_url']);
                        
                        $editUrls[$project['project_code']] = Docebo::createAbsoluteLmsUrl('ElucidatApp/ElucidatApp/redirectEdit', array(
                        	'project_url'	=> 	$project['edit_url']
                        ));
                        
                        
                        $counter = 1;
                        foreach (array_reverse($project['releases'], true) as $releaseCode => $releaseOptions) {
                            $note = ($releaseOptions['description']) ? " (" . $releaseOptions['description'] . ")" : '';
                            $releases[$project['project_code']][$releaseCode] = Yii::t('elucidat', 'Release') . " " . Yii::app()->localtime->toLocalDateTime($releaseOptions['created'], Localtime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, null, 'Etc/GMT-1') . $note;
                            $counter++;
                        }
                    }
                }
                $html = $this->renderPartial('_edit_lo', array(
                    'courseModel'   =>$courseModel,
                    'orgObj'        =>$orgObj,
                    'loElucidat'    =>$loElucidat,
                    'mode'          =>$mode,
                    'validAuthor'   => $validAuthor,
                    'validElucidat' => $validElucidat,
                    'projects'      =>  $projects,
                    'editUrls'      =>  $editUrls,
                    'releases'      =>  $releases,
                    'fullInfo'      =>  $result,
                    'short_description'  => $short_description
                ), true, false);
                break;
            default:
                break;
        }

        $response = new AjaxResult();
		$response->setStatus(true)->setHtml($html)->toJSON();
		    Yii::app()->end(); // End !!!
	}

	/**
	 * This action is still in progress, because the Elucidat webhooks are not stable for now.
	 * You can test it with url like -> http://youLMS.com/admin/index.php?r=ElucidatApp/ElucidatApp/webhook&project_code=2523r23f233&release_code=23523f23rf2
	 *
	 * Elucidat WehHook handler
	 */
	public function actionWebhook(){
	    
	    if (!isset($_POST['event_json'])) Yii::app()->end();
	    
	    $eventData = CJSON::decode($_POST['event_json']);
	    if (!is_array($eventData)) Yii::app()->end();

	    if (!isset($eventData['project']['project_code'])) Yii::app()->end();
	    if (!isset($eventData['release_code'])) Yii::app()->end();
	    
	    $projectCode = $eventData['project']['project_code'];
	    $releaseCode = $eventData['release_code'];
	    
		if($projectCode && $releaseCode){
		    
		    Yii::log("Elucidat Webhook callback received. Project Code: $projectCode, Release Code: $releaseCode", CLogger::LEVEL_INFO);
		    
			$elucidatModels = LearningElucidat::model()->findAllByAttributes(array(
				'p_project_code' => $projectCode,
				'keep_updated' => 1
			));

			if(!empty($elucidatModels)){
			    
			    Yii::log("Elucidat learning object(s) related to the above project found. Updating release of all LOs...", CLogger::LEVEL_INFO);
			    
			    /* @var $model LearningElucidat  */
			    
				foreach($elucidatModels as $model){
					try{
						$lo = LearningOrganization::model()->findByAttributes(array(
							'objectType' => LearningOrganization::OBJECT_TYPE_ELUCIDAT,
							'idResource' => $model->id_resource,
							'id_object'  => null
						));

						$repoVersionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
							'object_type' => LearningOrganization::OBJECT_TYPE_ELUCIDAT,
							'id_resource' => $model->id_resource
						));

						if($lo || $repoVersionModel){
						    
						    Yii::log("Updating Learning Object " .  isset($lo) ? $lo->idOrg : $repoVersionModel->id_object . " - " . $lo->title . ", Course -" . $lo->idCourse  . "...", CLogger::LEVEL_INFO);
						    
							$model->r_release_code = $releaseCode;
							$model->last_status = LearningElucidat::STATUS_SUCCESS;
							$model->last_status_updated = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
							$model->last_successful_update = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
							if(!$model->save()){
								Yii::log('Error while saving Elucidat Model', CLogger::LEVEL_ERROR);
								Yii::log(print_r($model->getErrors(), true), CLogger::LEVEL_ERROR);
							}
							else {
							    Yii::log("Updated...", CLogger::LEVEL_INFO);
							}
						} else{
							throw new Exception('Attempt to update Elucidat model, not related to any Learning Object');
						}
					} catch(Exception $e){
						Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
					}
				}
			} 
			else {
			    Yii::log("No Elucidat learning object(s) related to the above project found. End of story.", CLogger::LEVEL_INFO);
			}
		}
	}

	/**
	 * actionAxGetLaunchInfo() - gets the launch info(mainly the launch URL) and passes it back to be used in arana.js
	 */
	public function actionAxGetLaunchInfo() {
		$idObject = Yii::app()->request->getParam("id_object", 0);
		$object = LearningOrganization::model()->findByPk($idObject);
		$response = new AjaxResult();

		try {
			if (!$object || $object->objectType != LearningOrganization::OBJECT_TYPE_ELUCIDAT) {
				throw new CException('Invalid Elucidat Learning Object: ' . $idObject);
			}

			$learningElucidat = LearningElucidat::model()->findByAttributes(array('id_resource' => $object->idResource));

			if (!$learningElucidat) {
				throw new CException('Invalid Elucidat Learning Object: ' . $idObject);
			}

			// Launcher Activity (as per tincan.xml)
			$activity = LearningTcActivity::model()->findByPk($object->idResource);
			if (!$activity) {
				throw new CException('Invalid Activity: '.$object->idResource);
			}

			// Activity Provider (not to mess with Activity (which are children of AP)
			$activityProvider = $activity->ap;  // using AR relation

			$userData = CoreUser::model()->findByPk(Yii::app()->user->id);
			$actor = $activityProvider->buildActor($userData);

			// Get Elucidat keys from the settings
			$publicKey = Settings::get('elucidat_consumer_key', null);
			$secretKey = Settings::get('elucidat_secret_key', null);

			$client = new ElucidatApiClient($publicKey, $secretKey);

			// Get the Elucidat launch url
			$res = $client->getLaunchUrl($learningElucidat->r_release_code  , Yii::app()->user->loadUserModel()->getFullName(), Yii::app()->user->loadUserModel()->email, $res->data['url'] . '&iduser='.Yii::app()->user->id.'&lmsreg='.$activityProvider->registration);

			if ($res->success == true) {
				// Build Learner specific temporary URL
				$launch = $res->data['url'];
			} else {
				$message = Yii::t('elucidat', 'Failed to launch Elucidat object. Maybe it is still not released. Please try again in few minutes');
				throw new CException($message);
			}
			
			// Check if the URL has a query and send the result back to JS; just to easy the process
			$parsed = parse_url($launch);
			$hasQueryString = isset($parsed['query']);

			$backUrl = '#';

			$data = array(
				'idReference' => $idObject,
				'activityId' => $activity->id,
				'endpoint' => '',//$this->getLrsEndpoint(),
				'launch' => $launch,
				'actor' => CJSON::encode($actor),
				'registration' => $activityProvider->registration,
				'back_url' => $backUrl,
				'acceptLanguage' => Yii::app()->getLanguage(),
				'content_token'  => $activityProvider->registration,
				'hasQueryString'	=> $hasQueryString,
			);
			$response->setStatus(true);
			$response->setData($data);
			$response->toJSON();
			Yii::app()->end();

		}
		catch (CException $e) {
			$response->setStatus(false)->setMessage($e->getMessage())->toJSON();
			Yii::app()->end();
		}

	}

	private function configureProject($projectCode){
		$publicKey = Settings::get('elucidat_consumer_key', false);
		$secretKey = Settings::get('elucidat_secret_key', false);

		if($publicKey && $secretKey){
			$client = new ElucidatApiClient($publicKey, $secretKey);

			if($projectCode){
				$projectParams = new ElucidatProjectParams();
				$user = $this->findUser();

				$projectParams->xapi_lrs_endpoint_url = $this->getEndpoint();
				$projectParams->xapi_lrs_endpoint_username = $user['username'];
				$projectParams->xapi_lrs_endpoint_password = $user['pass'];
				$projectParams->tracking_mode = ElucidatProjectParams::TRACKING_MODE_XAPI; 
				$project = $client->configureProject($projectCode, $projectParams);

				if($project->success === true){
					Yii::log('Elucidat: project ' . $projectCode . ' configured successful', CLogger::LEVEL_INFO);
				}

				return true;
			}

			return false;
		}

		return false;
	}

	
	/**
	 * Re-release a given release. 
	 * Do only a release, do not send any additional parameters! Just "refresh" it
	 * 
	 * @param string $projectCode
	 * @param string $releaseCode
	 * 
	 */
	private function reRelease($projectCode, $releaseCode) {
		
		$publicKey = Settings::get('elucidat_consumer_key', false);
		$secretKey = Settings::get('elucidat_secret_key', false);
		
		if($publicKey && $secretKey){
			$client = new ElucidatApiClient($publicKey, $secretKey);
			$fields = array(
				'project_code'	=> $projectCode,
				'release_code'	=> $releaseCode,
			);
			$result = $client->reRelease($fields);
			if($result->success === true){
				Yii::log('Elucidat: Release ' . $projectCode . '/' . $releaseCode . ' has been RE-released', CLogger::LEVEL_INFO);
			}
			
			return true;
		}
		
		return false;
		
	}

	
	/**
	 * Validate Elucidat settings (including a call to Elucidat if requested)
	 * 
	 * @param boolean $connectElucidat
	 * @return boolean
	 */
	public static function validateElucidatAccount($connectElucidat = true) {
	    
	    /*Elucidat connection parameters*/
	    $publicKey = Settings::get('elucidat_consumer_key', false);
	    $secretKey = Settings::get('elucidat_secret_key', false);
	    
	    $client = new ElucidatApiClient($publicKey, $secretKey);
	    
	    $clientInfo = $client->getAccountInfo(); // Only for configuration check
	    
	    $validElucidat = true; // Check is the the plugin is configured correctly
	    if (!$publicKey || !$secretKey || (!$connectElucidat || ($clientInfo->success != 1)) ) {
	        $validElucidat = false;
	    }
	    
	    return $validElucidat;
	     
	    
	}

	public function actionSaveCentralRepo(){
		$elucidatInfo 	        = Yii::app()->request->getParam("LearningElucidat", null);
		$loInfo 		        = Yii::app()->request->getParam("LearningRepositoryObject", null);
		$desktop_openmode 		= Yii::app()->request->getParam("desktop_openmode", null);
		$tablet_openmode 		= Yii::app()->request->getParam("tablet_openmode", null);
		$smartphone_openmode 	= Yii::app()->request->getParam("smartphone_openmode", null);
		$short_description      = Yii::app()->request->getParam("short_description", null);
		$thumb                  = Yii::app()->request->getParam("thumb", null);
		$id_object				= Yii::app()->request->getParam('id_object', false);

		$transaction = Yii::app()->db->beginTransaction();

		$versionModel = new LearningRepositoryObjectVersion();
		$repoObject = new LearningRepositoryObject();
		$response = new AjaxResult();
		try {
			if(!$elucidatInfo['p_project_code'])
				throw new Exception(Yii::t('elucidat', 'You must select an Elucidat course'));
			if(!$elucidatInfo['r_release_code'])
				throw new Exception(Yii::t('elucidat', 'You must select an Elucidat course release'));
			if(!$loInfo['title'])
				throw new Exception(Yii::t('elucidat', 'You must select a title'));
			if (!$elucidatInfo || !$loInfo) {
				throw new Exception(Yii::t('elucidat', 'Invalid request'));
			}

			if ($id_object > 0) {
				$repoObject = LearningRepositoryObject::model()->findByPk($id_object);
				if($repoObject){
					$versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array(
						'id_object' => $repoObject->id_object
					));

					if($versionModel){
						$elucidatModel = LearningElucidat::model()->findByAttributes(array(
							'id_resource' => $versionModel->id_resource
						));
					}
				}
			} else {

				$repoObject->object_type = LearningOrganization::OBJECT_TYPE_ELUCIDAT;
				$categoryNodeId = LearningCourseCategoryTree::getRootNodeId();
				if(isset(Yii::app()->session['currentCentralRepoNodeId'])){
					$categoryNodeId = Yii::app()->session['currentCentralRepoNodeId'];
				}
				$repoObject->id_category = $categoryNodeId;
				// Create TC_AP
				$tcApModel = new LearningTcAp();
				$tcApModel->detachBehavior('TinCan');

				$tcApModel->registration = Docebo::generateUuid();
				$tcApModel->path = "none";
				$tcApModel->xapi_version = "1.0.1";

				if ($tcApModel->save()) {
					$tcActivityModel = new LearningTcActivity();
					$tcActivityModel->id_tc_ap = $tcApModel->id_tc_ap;
					$tcActivityModel->id = "https://learning.elucidat.com/course/" . $elucidatInfo['p_project_code'] . "-" . $elucidatInfo['r_release_code'];
					$tcActivityModel->type = 'http://adlnet.gov/expapi/activities/course';
					$tcActivityModel->name = $loInfo['title'];
					$tcActivityModel->description = $short_description;
					$tcActivityModel->launch = "index.html";

					$loParams['short_description'] = $short_description;
					if ($tcActivityModel->save()) {
						if($thumb){
							$loParams['resource'] = $thumb;
						}

						$elucidatModel =  new LearningElucidat();
						$elucidatModel->id_resource = $tcActivityModel->id_tc_activity;
						$elucidatModel->last_status = LearningElucidat::STATUS_SUCCESS;
						$elucidatModel->last_status_updated = Yii::app()->localtime->getUTCNow();
						$elucidatModel->last_successful_update = Yii::app()->localtime->getUTCNow();
					}
					else {
						Yii::log(print_r($tcActivityModel->errors, true), CLogger::LEVEL_ERROR);
						throw new Exception(Yii::t('elucidat', 'Failed to save Elucidat Activity'));
					}
				}
				else {
					Yii::log(print_r($tcApModel->errors, true), CLogger::LEVEL_ERROR);
					throw new Exception(Yii::t('elucidat', 'Failed to save Elucidat Activity Provider'));
				}
			}


			if(isset($loInfo['title']))
				$repoObject->title = $loInfo['title'];
			$repoParams = array();
			if(isset($short_description))
				$repoObject->short_description = $short_description;
			if($desktop_openmode)
				$repoParams['desktop_openmode']    = $desktop_openmode;
			if($tablet_openmode)
				$repoParams['tablet_openmode']     = $tablet_openmode;
			if($smartphone_openmode)
				$repoParams['smartphone_openmode'] = $smartphone_openmode;
			if($thumb){
				$repoObject->resource = intval($thumb);
			}elseif($repoObject->resource ){
				$repoObject->resource = 0;
			}


			$repoObject->params_json = CJSON::encode($repoParams);
			$repoObject->save();

			//-----------
			//project attributes
			$elucidatModel->p_project_code  = $elucidatInfo['p_project_code'];
			$elucidatModel->p_name          = $elucidatInfo['p_name'];//$loInfo['title'];
			$elucidatModel->p_project_key   = ($elucidatInfo['p_project_key'])?$elucidatInfo['p_project_key'] : "none";//"none";
			$elucidatModel->p_created       = $elucidatInfo['p_created'];//Yii::app()->localtime->getUTCNow();
			$elucidatModel->edit_url        = $elucidatInfo['edit_url'];

			//release attributes
			$elucidatModel->r_release_code      = $elucidatInfo['r_release_code'];
			$elucidatModel->r_created           = $elucidatInfo['r_created'];
			$elucidatModel->r_description       = $elucidatInfo['r_description'];
			$elucidatModel->r_version_number    = $elucidatInfo['r_version_number'];
			$elucidatModel->r_release_mode      = $elucidatInfo['r_release_mode'];

			//lms attributes
			$elucidatModel->keep_updated = $elucidatInfo['keep_updated'];



			if (!$elucidatModel->save()) {
				Yii::log(print_r($elucidatModel->errors, true), CLogger::LEVEL_ERROR);
				throw new Exception(Yii::t('elucidat', 'Failed to save Elucidat Model'));
			}

			if($repoObject->save()){
				if($versionModel->getScenario() === 'insert'){
					$versionModel->object_type = LearningOrganization::OBJECT_TYPE_ELUCIDAT;
					$versionModel->id_object = $repoObject->id_object;
					$versionModel->version = 1;
					$versionModel->version_name = 'V1';
					$versionModel->version_description = '';
					$userModel = Yii::app()->user->loadUserModel();
					/**
					 * @var $userModel CoreUser
					 */
					$authorData = array(
						'idUser' => $userModel->idst,
						'username' => $userModel->getUserNameFormatted()
					);
					$versionModel->author = CJSON::encode($authorData);
					$versionModel->create_date = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
					$versionModel->id_resource = $elucidatModel->id_resource;

					$versionModel->save();
				}
			}

			// Set Tracking Settings for the project
			$this->configureProject($elucidatModel->p_project_code);

			// And now: RE-RELEASE the release so that it "gets" the new project settings
			$this->reRelease($elucidatModel->p_project_code, $elucidatModel->r_release_code);

			// DB Commit
			$transaction->commit();
			$response->setMessage(Yii::t('standard', '_OPERATION_SUCCESSFUL'))->setStatus(true)->toJSON();

		} catch (Exception $e) {
			$transaction->rollback();
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			$response->setMessage($e->getMessage())->setStatus(false)->toJSON();
		}
	}
	
	
	public function actionAxSaveLo(){

		$elucidatInfo 	        = Yii::app()->request->getParam("LearningElucidat", null);
		$loInfo 		        = Yii::app()->request->getParam("LearningOrganization", null);
		$idCourse 		        = (int) Yii::app()->request->getParam("course_id", null);
		$idOrg 			        = (int) Yii::app()->request->getParam("id_object", null);
        $desktop_openmode 		= Yii::app()->request->getParam("desktop_openmode", null);
        $tablet_openmode 		= Yii::app()->request->getParam("tablet_openmode", null);
        $smartphone_openmode 	= Yii::app()->request->getParam("smartphone_openmode", null);
        $short_description      = Yii::app()->request->getParam("short_description", null);
        $thumb                  = Yii::app()->request->getParam("thumb", null);

		$response = new AjaxResult();
		$transaction = Yii::app()->db->beginTransaction();
		
		try {
			if(!$elucidatInfo['p_project_code'])
                throw new Exception(Yii::t('elucidat', 'You must select an Elucidat course'));
			if(!$elucidatInfo['r_release_code'])
				throw new Exception(Yii::t('elucidat', 'You must select an Elucidat course release'));
			if(!$loInfo['title'])
				throw new Exception(Yii::t('elucidat', 'You must select a title'));
			if (!$elucidatInfo || !$loInfo || !$idCourse) {
				throw new Exception(Yii::t('elucidat', 'Invalid request'));
			}
			
			$learningObject = null;
			
			if ($idOrg > 0) {
				$learningObject = LearningOrganization::model()->findByPk($idOrg);
				$elucidatModel = LearningElucidat::model()->findByAttributes(array(
					'id_resource' => $learningObject->idResource
				));
                $learningObject->short_description = $short_description;;
                if($thumb){
	                $learningObject->resource = $thumb;
                }elseif($learningObject->resource){
					$learningObject->resource = 0;
                }

			} else {

				// Create TC_AP
				$tcApModel = new LearningTcAp();
				$tcApModel->detachBehavior('TinCan');
				
				$tcApModel->registration = Docebo::generateUuid();
				$tcApModel->path = "none";
				$tcApModel->xapi_version = "1.0.1";
				
				if ($tcApModel->save()) {
					$tcActivityModel = new LearningTcActivity();
					$tcActivityModel->id_tc_ap = $tcApModel->id_tc_ap;
					$tcActivityModel->id = "https://learning.elucidat.com/course/" . $elucidatInfo['p_project_code'] . "-" . $elucidatInfo['r_release_code'];
					$tcActivityModel->type = 'http://adlnet.gov/expapi/activities/course';
					$tcActivityModel->name = $loInfo['title'];
					$tcActivityModel->description = $short_description;
					$tcActivityModel->launch = "index.html";

                    $loParams['short_description'] = $short_description;
					if ($tcActivityModel->save()) {
                        if($thumb){
                            $loParams['resource'] = $thumb;
                        }

						$loManager = new LearningObjectsManager($idCourse);
						$learningObject = $loManager->addLearningObject(LearningOrganization::OBJECT_TYPE_ELUCIDAT, $tcActivityModel->id_tc_activity, $loInfo['title'], false, $loParams);

                        $elucidatModel =  new LearningElucidat();
						$elucidatModel->id_resource = $learningObject->idResource;
                        $elucidatModel->last_status = LearningElucidat::STATUS_SUCCESS;
                        $elucidatModel->last_status_updated = Yii::app()->localtime->getUTCNow();
                        $elucidatModel->last_successful_update = Yii::app()->localtime->getUTCNow();
					}
					else {
						Yii::log(print_r($tcActivityModel->errors, true), CLogger::LEVEL_ERROR);
						throw new Exception(Yii::t('elucidat', 'Failed to save Elucidat Activity'));
					}
				}
				else {
					Yii::log(print_r($tcApModel->errors, true), CLogger::LEVEL_ERROR);
					throw new Exception(Yii::t('elucidat', 'Failed to save Elucidat Activity Provider'));
				}
				
				
			}
            if(isset($loInfo['title']))
                $learningObject->title = $loInfo['title'];
            if(isset($short_description))
                $learningObject->short_description = $short_description;
            if($desktop_openmode)
                $learningObject->desktop_openmode    = $desktop_openmode;
            if($tablet_openmode)
                $learningObject->tablet_openmode     = $tablet_openmode;
            if($smartphone_openmode)
                $learningObject->smartphone_openmode = $smartphone_openmode;
            $learningObject->saveNode(false);
			
			//-----------
            //project attributes
			$elucidatModel->p_project_code  = $elucidatInfo['p_project_code'];
            $elucidatModel->p_name          = $elucidatInfo['p_name'];//$loInfo['title'];
            $elucidatModel->p_project_key   = ($elucidatInfo['p_project_key'])?$elucidatInfo['p_project_key'] : "none";//"none";
            $elucidatModel->p_created       = $elucidatInfo['p_created'];//Yii::app()->localtime->getUTCNow();
            $elucidatModel->edit_url        = $elucidatInfo['edit_url'];

            //release attributes
			$elucidatModel->r_release_code      = $elucidatInfo['r_release_code'];
            $elucidatModel->r_created           = $elucidatInfo['r_created'];
            $elucidatModel->r_description       = $elucidatInfo['r_description'];
            $elucidatModel->r_version_number    = $elucidatInfo['r_version_number'];
            $elucidatModel->r_release_mode      = $elucidatInfo['r_release_mode'];

            //lms attributes
			$elucidatModel->keep_updated = $elucidatInfo['keep_updated'];
			

			
			if (!$elucidatModel->save()) {
				Yii::log(print_r($elucidatModel->errors, true), CLogger::LEVEL_ERROR);
				throw new Exception(Yii::t('elucidat', 'Failed to save Elucidat Model'));
			}
			
			// Set Tracking Settings for the project 
			$this->configureProject($elucidatModel->p_project_code);

			// And now: RE-RELEASE the release so that it "gets" the new project settings
                $this->reRelease($elucidatModel->p_project_code, $elucidatModel->r_release_code);
			
			// DB Commit
			$transaction->commit();
			
			$data = array(
				'message' => Yii::t('standard', '_OPERATION_SUCCESSFUL')
			);
            Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
                'loModel' => $learningObject
            )));
			$response->setData($data)->setStatus(true);
			
		} catch (Exception $e) {
			$transaction->rollback();
			$response->setMessage($e->getMessage())->setStatus(false);
		}

		$response->toJSON();
		Yii::app()->end();
		
		
		$projectCode = Yii::app()->request->getParam('p_project_code', false);

		

	}

	public function actionClearSettings(){
		Settings::remove('elucidat_consumer_key');
		Settings::remove('elucidat_secret_key');

		Yii::app()->user->setFlash('success', Yii::t("standard", '_OPERATION_SUCCESSFUL'));
		$this->redirect(Docebo::createAbsoluteUrl('ElucidatApp/ElucidatApp/settings'));
	}
	
	public function actionAxGetCoursesInfo() {
	    
	    $response = new AjaxResult();
	    
	    $valid = self::validateElucidatAccount();
	    if (!$valid) {
	        $response->setStatus(false)->setMessage('Invalid Elucidat account')->toJSON();
	        Yii::app()->end(); // End !!!
	    }
	    

	    $publicKey = Settings::get('elucidat_consumer_key', false);
	    $secretKey = Settings::get('elucidat_secret_key', false);
	    $client    = new ElucidatApiClient($publicKey, $secretKey);

	    $result = $this->retrieveElucidatFullInfoArrayConditionally($client, true);
	    
	    $all       = $result;
	    $releases  = array();
	    $projects  = array();
	     
	    if ( $result && count($result) > 0 ) {
	        foreach ($result as $project) {
	            $projects[$project['project_code']] = $project['name'];
	            foreach (array_reverse($project['releases'], true) as $releaseCode => $releaseOptions) {
	                $note = ($releaseOptions['description']) ? " (" . $releaseOptions['description'] . ")" : '';
	                $releases[$project['project_code']][$releaseCode] = Yii::t('elucidat', 'Release') . " " . Yii::app()->localtime->toLocalDateTime($releaseOptions['created'], Localtime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, null, 'Etc/GMT-1') . $note;
	            }
	        }
	    }
	     
		$response = new AjaxResult();
		
        $data = array(
            'projects'  => $projects,
            'releases'  => $releases,
            'all'       => $all,
        );		
		
		
		$response->setStatus(true)->setData($data)->toJSON();
		
		Yii::app()->end(); // End !!!
		
		
	}

	public function actionRedirectEdit() {
		$p_url = Yii::app()->request->getParam('project_url', false);
        $publicKey = Settings::get('elucidat_consumer_key', false);
        $secretKey = Settings::get('elucidat_secret_key', false);
        $client = new ElucidatApiClient($publicKey, $secretKey);

        try {
            $currentUser = CoreUser::model()->findByPk(Yii::app()->user->getId());
            $authors = $client->getAuthors();
            if ( $authors->success ) {
                foreach ( $authors->data as $author ) {
                    if ( $currentUser->email === $author['email'] ) {
                        $ssoLink = $client->getSsoUrl($currentUser->email, $p_url);
                        break;
                    }
                }
            }
            $this->redirect($ssoLink);
        } catch ( Exception $e ) {
            Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
            $this->redirect(array('/site'), TRUE);
        }
	}

}
