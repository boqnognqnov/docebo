<?php

/**
 * A helper class returned by Api Client
 *
 */
class ElucidatApiResponse extends stdClass {

    public $success;
    public $message;
    public $data;

    public function __construct($apiResultArray) {
        if (!is_array($apiResultArray) || !isset($apiResultArray['status']) || !isset($apiResultArray['response'])) {
            $this->success = false;
            $this->message = "Invalid API response structure";
        }
        else if ($apiResultArray['status'] != 200) {
            $this->success = false;
            $this->message = isset($apiResultArray['response']['message']) ? $apiResultArray['response']['message'] : "Failed API call, returned status " . $apiResultArray['status'];
        }
        else {
            $this->success = true;
            $this->data = $apiResultArray['response'];
        }

        if ($this->success != true) {
            Yii::log('Error while calling Elucidat API', CLogger::LEVEL_ERROR);
            Yii::log('Response:', CLogger::LEVEL_ERROR);
            Yii::log(var_export($apiResultArray, true), CLogger::LEVEL_ERROR);
        }

    }

}

?>