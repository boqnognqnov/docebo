<?php
/**
 *
 * API Client for Elucidat API v2
 *
 */
class ElucidatApiClient extends CComponent {
    
    const API_BASE_URL = 'https://api.elucidat.com/v2';                         // NO TRAILING SLASH
    const SSO_BASE_URL = 'https://app.elucidat.com/single_sign_on/login';       // NO TRAILING SLASH

    // API types (final endpoints)
    const TYPE_ACCOUNT          = "account";
    const TYPE_ACCOUNT_UPDATE   = "account/update";
    const TYPE_PROJECTS         = "projects";
    const TYPE_PROJECTS_CONFIGURE = "projects/configure";
    const TYPE_AUTHORS          = "authors";
    const TYPE_AUTHORS_CREATE   = "authors/create";
    const TYPE_AUTHORS_ROLE     = "authors/role";
    const TYPE_AUTHORS_DELETE   = "authors/delete";
    const TYPE_RELEASES         = "releases";
    const TYPE_RELEASES_DETAILS = "releases/details";
    const TYPE_RELEASES_LAUNCH  = "releases/launch";
    const TYPE_RELEASES_ANSWERS = "releases/answers";
    const TYPE_RELEASES_CREATE  = "releases/create";
    const TYPE_EVENT            = "event";
    const TYPE_EVENT_SUBSCRIBE  = "event/subscribe";
    const TYPE_EVENT_UNSUBSCRIBE= "event/unsubscribe";
    
    // Available event names (for webhooks)
    const EVENT_RELEASE_COURSE  = "release_course";
    
    
    // Release modes for Create Release API
    const RELEASE_MODE_ONLINE_PRIVATE   = "online-private";
    const RELEASE_MODE_ONLINE_PUBLIC    = "online-public";
    const RELEASE_MODE_ALL              = false;
    const RELEASE_MODE_SCORM            = "scorm";
    const RELEASE_MODE_OFFLINE_BACKUP   = "onffline-backup";
    
    /**
     * Elucidat API Base URL
     * @var string
     */
    private $_apiBaseUrl;
    
    /**
     * Elucidat API Application's Key/Secret
     * @var string
     */
    private $_consumerKey;
    private $_consumerSecret;
    
    /**
     * Simulation mode
     * @var Boolean
     */
    private $_simulation = false;
    
    
    /**
     * Constructor
     *  
     * @param string $consumerKey
     * @param string $consumerSecret
     * @param boolean $simulation
     */
    public function __construct($consumerKey, $consumerSecret, $simulation = false) {
        
        // Get API base URL and make sure there is no trailing forward slash
        $this->_apiBaseUrl = rtrim(self::API_BASE_URL, "/");
        
        $this->_consumerKey 	= $consumerKey;
        $this->_consumerSecret 	= $consumerSecret;

        // Set simulation mode
        $this->_simulation = $simulation;
        
    }
    
    /**
     * Return list of projets
     *  
     * @return ElucidatApiResponse
     */
    public function getProjects(){
        return $this->call('GET', self::TYPE_PROJECTS);
    }
    

    /**
     * Return Elucidat account information 
     * 
     * @return ElucidatApiResponse
     */
    public function getAccountInfo() {
        return $this->call('GET', self::TYPE_ACCOUNT);
    }


    /**
     * Update Account info
     * 
     * @param string $first_name
     * @param string $last_name
     * @param string $company_email
     * @param string $company_name
     * @param string $telephone
     * @param string $address1
     * @param string $address2
     * @param string $postcode
     * @param string $country
     * 
     * @return ElucidatApiResponse
     */
    public function updateAccountInfo($first_name,$last_name,$company_email,$company_name,$telephone,$address1,$address2,$postcode,$country) {
        $fields = array(
            'first_name'        => $first_name,
            'last_name'         => $last_name,
            'company_email'     => $company_email,
            'company_name'      => $company_name,
            'telephone'         => $telephone,
            'address1'          => $address1,
            'address2'          => $address2,
            'postcode'          => $postcode,
            'country'           => $country,
        );
        return $this->call('POST', self::TYPE_ACCOUNT_UPDATE);
    }
    
    
    /**
     * Get list of authors
     * 
     * @return ElucidatApiResponse
     */
    function getAuthors(){
        return $this->call('GET', self::TYPE_AUTHORS);
    }
    
    

    /**
     * Create new author
     * 
     * @param string $first_name
     * @param string $last_name
     * @param string $email
     * 
     * @return ElucidatApiResponse
     */
    function createAuthor($first_name,$last_name,$email){
        $fields = array(
            'first_name'    => $first_name,
            'last_name'     => $last_name,
            'email'         => $email,
        );
        return $this->call('GET', self::TYPE_AUTHORS_CREATE, $fields);
    }
    

    /**
     * Delete Author by email
     * 
     * @param string $company_email
     * 
     * @return ElucidatApiResponse
     */
    function deleteAuthor($email){
        $fields = array(
            'email'=>$email,
        );
        return $this->call('POST', self::TYPE_AUTHORS_DELETE, $fields);
    }

    /**
     * Get particular Project Release, by ID
     * 
     * @param string $project_code
     * 
     * @return ElucidatApiResponse
     */
    function getReleases($project_code){
        $fields = array(
            'project_code' => $project_code,
        );
        return $this->call('GET', self::TYPE_RELEASES, $fields);
    }

    
    /**
     * Create new release in a project
     * 
     * @param string $project_code
     * @param string $release_mode
     * 
     * @return ElucidatApiResponse
     */
    function createRelease($project_code, $release_mode){
        $fields = array(
            'project_code'  => $project_code,
            'release_mode'  => $release_mode,
        );        
        return $this->call('POST', self::TYPE_RELEASES_CREATE, $fields);
    }
    
    
    
    /**
     * Get a release details
     * 
     * @param string $release_code
     * 
     * @return ElucidatApiResponse
     */
    function getReleaseDetails($release_code){
        $fields = array(
            'release_code'=>$release_code,
        );
        return $this->call('GET', self::TYPE_RELEASES_DETAILS, $fields);
    }
    
    
    /**
     * Get Launch Url for a release for particular Actor (name and email)
     * 
     * @param string $release_code
     * @param string $name
     * @param string $email_address
     * 
     * @return ElucidatApiResponse
     */
    function getLaunchUrl($release_code, $name, $email_address, $additionalParams=""){
        $fields = array(
            'release_code'  =>$release_code,
            'name'          =>$name,
            'email_address' =>$email_address,
            'params'        => $additionalParams,
        );
        return $this->call('GET', self::TYPE_RELEASES_LAUNCH, $fields);
    }

    /**
     * Get Answers for a poll in a project release, if any
     * 
     * @param string $release_code
     * 
     * @return ElucidatApiResponse
     */
    function getPollResults($release_code){
        $fields = array(
            'release_code'=>$release_code,
        );
        return $this->call('GET', self::TYPE_RELEASES_ANSWERS, $fields);
    }

    
    /**
     * Get subscribed callbacks for all events (webhooks)
     * 
     * @return ElucidatApiResponse
     */
    function getEventCallbacks(){
        return $this->call('GET', self::TYPE_EVENT);
    }
    

    /**
     * Subscribe a URL to a named event
     * 
     * @param string $eventName
     * @param string $callbackUrl
     * 
     * @return ElucidatApiResponse
     */
    function subscribeToEvent($eventName, $callbackUrl){
        
        // First, unsubscribe, otherwise the NEW URL will not be subscribed if there is already an old subscription
        $this->unSubscribeFromEvent($eventName);            
        
        $fields = array(
            'event'             =>  $eventName,
            'callback_url'      =>  $callbackUrl
        );
        return $this->call('POST', self::TYPE_EVENT_SUBSCRIBE, $fields);
    }
    
    
    /**
     * Unsubscribe URL(s) from a named event
     * 
     * @param string $eventName
     * 
     * @return ElucidatApiResponse
     */
    function unSubscribeFromEvent($eventName){
        $fields = array(
            'event'             =>  $eventName,
        );
        return $this->call('POST', self::TYPE_EVENT_UNSUBSCRIBE, $fields);
    }
    
    /**
     * Update Author role 
     * 
     * @param string $email
     * @param string $role
     * 
     * @return ElucidatApiResponse
     */
    function updateAuthorRole($email, $role){
        $fields = array(
            'email'     => $email,
            'role'      => $role,
        );
        return $this->call('POST', self::TYPE_AUTHORS_ROLE, $fields);
    }
    

    /**
     * Get SSO URL (@TODO What exactly is this for?)
     * 
     * @param string $email
     * @param string $redirectUri
     * 
     * @return string
     */
    function getSsoUrl($email, $redirectUri){
        
        $apiUrl = $this->_apiBaseUrl . "/" . self::TYPE_PROJECTS;
        
        $nonce = self::get_nonce($apiUrl, $this->_consumerKey,  $this->_consumerSecret);
        
        $ssoBaseUrl = rtrim(self::SSO_BASE_URL, "/");
        
        $params = array(
            'oauth_consumer_key' => $this->_consumerKey,
            'oauth_nonce' => $nonce,
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_timestamp' => time(),
            'oauth_version' => '1.0',
            'email' => $email,
            'redirect_url' => $redirectUri
        );
        
        $params['oauth_signature'] = self::build_signature($this->_consumerSecret, $params, 'GET', $ssoBaseUrl);
        
        $request = self::build_base_string($params, '&');
        
        $ssoUrl = $ssoBaseUrl . "?" . $request;
        
        return $ssoUrl;
        
    }
    
    
    /**
     * Collect full list of projects (courses) and their releases in one single array.
     * 
     * Contains all information from Elucidat: project names, dates, release codes and names and so on.
     * 
     * @param string $mode  (online-private, online-public, false)
     * 
     */
    public function getFullInfoArray($mode=false, $skipProjectsWithoutRelease = false) {
        
        $projects = $this->getProjects();
        
        $info = array();
        foreach ($projects->data as $project) {
            $releases = $this->getReleases($project['project_code']);
            $elem = $project;
            $elem['releases'] = array();
            foreach ($releases->data as $release) {
            	// Filter by MODE, if any
            	if ($mode !== false) {
            		if ($release['release_mode'] == $mode) 
            			$elem['releases'][$release['release_code']] = $release;
            	}
            	else {
                	$elem['releases'][$release['release_code']] = $release;
            	}
            }
            if(!$skipProjectsWithoutRelease || count($elem['releases'])>0)
                $info[$project['project_code']] = $elem;
        }
        
        return $info;
        
    }
    
    /**
     * Configure a given project, applying passed parameters
     * 
     * @param string $projectCode
     * @param ElucidatProjectParams $params
     */
    public function configureProject($projectCode, ElucidatProjectParams $params) {

    	$vars = get_object_vars($params);
    	$fields = array();
    	
    	foreach ($vars as $name => $value) 
    		if ($value) 
    			$fields[$name] = $value;
    	
    	if (!empty($fields)) {
    		$fields['project_code'] = $projectCode;
    		return $this->call('POST', self::TYPE_PROJECTS_CONFIGURE, $fields);
    	}
    	
    	$result = new ElucidatApiResponse();
    	$result->success = true;
    	return $result;
    	
    }
    

    public function reRelease($fields) {
    	return $this->call('POST', self::TYPE_RELEASES_CREATE, $fields);
    }
    
    /**
     * Make an API call
     * 
     * @param string  $method  (GET, POST, ...)
     * @param string  $apiType  See TYPE_ constants
     * @param array $fields Array of additional data, if any
     * 
     * @return ElucidatApiResponse
     */
    protected function call($method, $apiType, $fields = false) {
        
        $apiUrl = $this->_apiBaseUrl . "/" . $apiType;
        $nonce = self::get_nonce($apiUrl, $this->_consumerKey,  $this->_consumerSecret);
        $headers = self::auth_headers($this->_consumerKey, $nonce);
        
        if (!is_array($fields)) {
            $fields = array();
        }
        
        if ($this->_simulation) {
            $fields['simulation_mode'] = "simulation";
        }
        
        $result = self::call_elucidat($headers, $fields, $method, $apiUrl, $this->_consumerSecret);
            
        $response = new ElucidatApiResponse($result);
        
        
        return $response;
        
    }
    

    
    /* *************************************************************************************************** */
    /*                                                                                                     */ 
    /* All methods below are based on Elucidat API Documentation: https://app.elucidat.com/api/project_v2  */
    /*                                                                                                     */
    /* *************************************************************************************************** */
    
    
    /**
     * Makes an API request to elucidat
     * 
     * @param $headers
     * @param $fields
     * @param $url
     * @param $consumer_secret
     * @return mixed
     */
    static function call_elucidat($headers, $fields, $method, $url, $consumer_secret){
        //Build a signature
        $headers['oauth_signature'] = self::build_signature($consumer_secret, array_merge($headers, $fields), $method, $url);
        //Build OAuth headers
        $auth_headers = 'Authorization:';
        $auth_headers .= self::build_base_string($headers, ',');
        //Build the request string
        $fields_string = self::build_base_string($fields, '&');
        //Set the headers
        $header = array($auth_headers, 'Expect:');
        // Create curl options
        if(strcasecmp($method, "GET") == 0){
            $url .= '?'.$fields_string;
            $options = array(
                CURLOPT_HTTPHEADER => $header,
                CURLOPT_HEADER => false,
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYPEER => false);
        } else {
            $options = array(
                CURLOPT_HTTPHEADER => $header,
                CURLOPT_HEADER => false,
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_POST => count($fields),
                CURLOPT_POSTFIELDS => $fields_string);
        }
    
        //Init the request and set its params
        $request = curl_init();
        curl_setopt_array($request, $options);
    
        //Make the request
        $response = curl_exec($request);
        $status = curl_getinfo($request, CURLINFO_HTTP_CODE);
        curl_close($request);
    
        return array(
            'status' => $status,
            'response' => json_decode($response, true)
        );
    }
    
    /**
     * Each request to the elucidat API must be accompanied by a unique key known as a nonce.
     * This key adds an additional level of security to the API.
     * A new key must be requested for each API call.
     * @param $api_url
     * @param $consumer_key
     * @param $consumer_secret
     * @return bool
     */
    static function get_nonce($api_url, $consumer_key, $consumer_secret){
        // Start with some standard headers, unsetting the oauth_nonce. without the nonce header the API will automatically issue us one.
        $auth_headers = self::auth_headers($consumer_key);
        unset($auth_headers['oauth_nonce']);
    
        //Make a request to elucidat for a nonce...any url is fine providing it doesnt already have a nonce
        $json = self::call_elucidat($auth_headers, array(), 'GET', $api_url, $consumer_secret);
    
        if(isset($json['response']['nonce'])){
            return $json['response']['nonce'];
        }
        return false;
    }
    
    /**
     * Computes and returns a signature for the request.
     * @param $secret
     * @param $fields
     * @param $request_type
     * @param $url
     * @return string
     */
    static function build_signature($secret, $fields, $request_type, $url){
        ksort($fields);
        //Build base string to be used as a signature
        $base_info = $request_type.'&'.$url.'&'.self::build_base_string($fields, '&'); //return complete base string
        //Create the signature from the secret and base string
        $composite_key = rawurlencode($secret);
        return base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
    
    }
    
    /**
     * Builds a segment from an array of fields.  Its used to create string representations of headers and URIs
     * @param $fields
     * @param $delim
     * @return string
     */
    static function build_base_string($fields, $delim){
        $r = array();
        foreach($fields as $key=>$value){
            $r[] = rawurlencode($key) . "=" . rawurlencode($value);
        }
        return implode($delim, $r); //return complete base string
    
    }
    
    /**
     * Returns typical headers needed for a request
     * @param $consumer_key
     * @param $nonce
     */
    static function auth_headers($consumer_key, $nonce = ''){
        return array('oauth_consumer_key' => $consumer_key,
            'oauth_nonce' => $nonce,
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_timestamp' => time(),
            'oauth_version' => '1.0');
    }

    
    
}


