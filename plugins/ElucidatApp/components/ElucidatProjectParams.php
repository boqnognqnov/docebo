<?php
/**
 * Helper class for Elucidat Project parameters
 *
 */
class ElucidatProjectParams extends stdClass {
	
	const TRACKING_MODE_XAPI = 'xapi';
	
	public $allow_completed_pages; 		//(0-1) 
	public $allow_future_pages; 		//(0-1)
	public $global_pass_rate; 			//(0-100) 
	public $allow_retakes; 				//(0-1)
	public $global_completion_rate; 	//(0-100)
	public $no_partial_scores; 			//(0-1)
	public $tracking_mode; 				//('scorm_1_2' || 'scorm_2004' || 'xapi')
	public $scorm_1_2_pass_action; 		//('passed' || 'completed')
	public $xapi_lrs_endpoint_url; 		//(valid url)
	public $xapi_lrs_endpoint_username; //(text)
	public $xapi_lrs_endpoint_password; //(text)
	public $learner_access; 			//('any' || 'identify' || 'restricted')
	public $google_analytics_code; 		//(GA code)
	
	
}