<?php
/**
 * Yii DB Migration template.
 * 
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 * 
 */
class m160419_083806_CONVERT_ELUCIDAT_REFERENCE_COLUMNS_WITH_LO extends DoceboDbMigration {

	private function getDataFromLearningOrganization(){
		$selectCommand = Yii::app()->db->createCommand();
		/**
		 * @var $selectCommand CDbCommand
		 */

		$selectCommand->select('idOrg, idResource')
			->from('learning_organization')
			->where('objectType = "elucidat"');

		return $selectCommand->queryAll();
	}

	private function addColumnIdResource(){
		$this->addColumn('learning_elucidat', 'id_resource', 'int(11)');
	}

	public function safeUp()
	{

		$this->addColumnIdResource();
		
		$elucidatData = $this->getDataFromLearningOrganization();

		$dataCount = count($elucidatData);

		$updateQuery = '';

		if(!empty($elucidatData)){
			foreach ($elucidatData as $key => $value){
				$updateQuery .= ' WHEN id_org = ' . $value['idOrg'] . ' THEN ' . $value['idResource'];
				if($key + 1 == $dataCount){
					$updateQuery .= ' END';
				}
			}
			if (!empty($updateQuery)) {
				$this->execute('UPDATE learning_elucidat SET  id_resource = CASE ' . $updateQuery);
			}
		}

		$fks = DoceboDbMigration::getColumnForeignKeys("learning_elucidat", "id_org");
		if (is_array($fks)) {
			foreach ($fks as $fk) {
				if (isset($fk['CONSTRAINT_NAME']) && $fk['CONSTRAINT_NAME']) {
					$this->dropForeignKey($fk['CONSTRAINT_NAME'], 'learning_elucidat' );
				}
			}
		}
		
		$this->dropColumn('learning_elucidat', 'id_org');
		
	}

	public function safeDown()
	{

	}
	
	
}
