<?php
    $editAppConfigUrl = Docebo::createAdminUrl('ElucidatApp/ElucidatApp/settings');
?>
<?php
/**
 * @var $centralRepoContext boolean
 * @var $saveUrl string
 * @var $orgObj LearningOrganization
 * @var $repoObject LearningRepositoryObject
 */

if(isset($repoObject)){
    $orgObj = $repoObject;
}

?>
<?php if($centralRepoContext) echo '<div class="row-fluid" data-bootstro-id="bootstroLaunchpad" id="player-arena-content">' ?>
<div id="player-arena-elucidat-editor-panel" class="player-arena-editor-panel create-edit-elucidat-panel <?= $centralRepoContext ? 'player-centralrepo-uploader-class' : '' ?>">
    <div class="header"><?php echo ($mode=='edit')? Yii::t('elucidat', 'Edit selected Elucidat course') : Yii::t('elucidat', 'Select Elucidat course'); ?></div>
    <div class="content">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'elucidat-editor-form',
            'method' => 'post',
            'action' => isset($saveUrl) ? $saveUrl : Docebo::createAbsoluteLmsUrl("//ElucidatApp/ElucidatApp/axSaveLo"),
            'htmlOptions' => array(
                'class' => 'form-horizontal'
            )
        ));
        ?>
        <?= $form->hiddenField($orgObj, $orgObj instanceof LearningOrganization ? 'idOrg' : 'id_object',array('name'=>'id_object')); ?>
        <?= $form->hiddenField($loElucidat,'p_name');?>
        <?= $form->hiddenField($loElucidat,'p_created');?>
        <?= $form->hiddenField($loElucidat,'p_project_key');?>
        <?= $form->hiddenField($loElucidat,'edit_url');?>

        <?= $form->hiddenField($loElucidat,'r_created');?>
        <?= $form->hiddenField($loElucidat,'r_description');?>
        <?= $form->hiddenField($loElucidat,'r_version_number');?>
        <?= $form->hiddenField($loElucidat,'r_release_mode');?>
        <div id="player-arena-uploader">
            <div>
                <div class="tabbable"> <!-- Only required for left/right tabs -->
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab1" data-toggle="tab"><?=LearningOrganization::objectTypeValue(LearningOrganization::OBJECT_TYPE_ELUCIDAT)?></a></li>
                        <li><a href="#lo-additional-information" data-toggle="tab"><?=Yii::t('myactivities', 'Additional info')?></a></li>

                        <?php
                        // Raise event to let plugins add new tabs
                        if(!$centralRepoContext) {
                            Yii::app()->event->raise('RenderEditLoTabs', new DEvent($this, array(
                                'courseModel' => $courseModel,
                                'loModel' => $orgObj
                            )));
                        }
                        ?>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                            <?php if(!$validElucidat){?>
                                <div id="wrong-config-error" class="span12" style="float:right;">
                                    <?php $this->widget('common.widgets.warningStrip.WarningStrip', array(
                                        'message'   => (Yii::app()->user->getIsGodAdmin())?Yii::t('elucidat', '<b>WARNING:</b> It looks like <b>your Elucidat app is not configured properly.</b> Check your <a class="elucidat-app-config-link" href="{link}" >Elucidat app configuration</a>',array('{link}'=>$editAppConfigUrl)) : Yii::t('elucidat','<b>WARNING:</b> Please report the problem to your administrator!'),//TODO
                                    ));?>
                                </div>
                            <?php }elseif($mode=='edit'){?>
                                    <div class="player-arena-editor-panel">
                                        <div class="elucidat-create-course-container">
                                            <div class="row-fluid">
                                                <div class="span2 elucidat-logo-container">
                                                    <span class="elucidat-logo"></span>
                                                </div>
                                                <div class="span8">
                                                    <span class="elucidat-box-title"><?=Yii::t('elucidat', 'Edit Elucidat course content')?></span></br>
                                                    <span class="elucidat-box-text">
                                                        <?=Yii::t('elucidat', 'You will be redirected to your Elucidat account in order to edit a course content')?>
                                                    </span>
                                                </div>

                                                <?php if ( !$validAuthor) { ?>
                                                    <a id="edit-elucidat-btn" target="_blank" class="btn btn-docebo green big elucidat-btn disabled" href=""><?= Yii::t('elucidat', 'Edit course') ?></a>
                                                    <div class="span10" style="float:right;">
                                                        <?php $this->widget('common.widgets.warningStrip.WarningStrip', array(
                                                            'message'   => Yii::t('elucidat', 'In order to edit this course in Elucidat, <b>you must be a valid author in Elucidat</b>. Please contact your Elucidat administrator for more information'),
                                                        )); ?>
                                                    </div>
                                                <?php }else {?>
                                                	<a id="edit-elucidat-btn" target="_blank" class="btn btn-docebo green big elucidat-btn" href=""><?= Yii::t('elucidat', 'Edit course') ?></a>
                                                <?php } ;?>
                                            </div>
                                        </div>
                                    </div>
                            <?php };?>
                            <div class="player-arena-uploader">

                                <div class="control-group">
                                    <?php echo CHtml::label(Yii::t('elucidat', 'Select Elucidat course'), 'LearningElucidat_course', array(
                                        'class' => 'control-label'
                                    )); ?>
                                    <div class="controls">
                                    	<div class="span10">
                                        	<?= $form->dropDownList($loElucidat,'p_project_code',$projects,array('class'=>'select-field')); ?>
                                        </div>
                                        <div class="span2">
<!--                                        	<a class="refresh-courses" href="javascript:;" rel="tooltip" title="--><?//=Yii::t('message','_REFRESH')?><!--"><i class="fa fa-refresh"></i></a>-->
                                            <?= CHtml::link('<i class="fa fa-refresh"></i>', 'javascript:void();', array(
                                                'class'			=>	'refresh-courses tooltipize',
                                                'data-template' => '<div class="tooltip elucidat-course-refresh-tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
                                                'data-html'		=> 'true',
                                                'data-trigger'  => 'hover focus',
                                                'title'			=> Yii::t('message','_REFRESH'),
                                            )); ?>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div id="release-control-group" class="control-group">
                                    <?php echo CHtml::label(Yii::t('elucidat', 'Select course release'), 'LearningElucidat_release', array(
                                        'class' => 'control-label'
                                    )); ?>
                                    <div class="controls">
                                   		<div class="span10">
                                        	<?=
                                                $form->dropDownList(
                                                    /*LearningElucidat*/$loElucidat,
                                                    'r_release_code',
                                                    ($mode=='edit')
                                                        ? (
                                                                ($releases[$loElucidat->p_project_code])
                                                                    ? $releases[$loElucidat->p_project_code]
                                                                    : array()
                                                          )
                                                        : array(),
                                                    array(
                                                        'id' => 'LearningElucidat_releases_list',
                                                        'class' => 'select-field'
                                                    )
                                                );
                                            ?>
                                        	<?= CHtml::label(Yii::t('elucidat', 'Be sure that releases are set as "Online(Public)" in your Elucidat account, in order to be able to select them here'), 'LearningElucidat_releases_list',array('class'=>'input-hint')) ?>
                                        </div>
										<div class="span2">
<!--											<a class="refresh-courses" href="javascript:;" rel="tooltip" title="--><?//=Yii::t('message','_REFRESH')?><!--"><i class="fa fa-refresh"></i></a>-->
                                            <?= CHtml::link('<i class="fa fa-refresh"></i>', 'javascript:void();', array(
                                                'class'			=>	'refresh-courses tooltipize',
                                                'data-template' => '<div class="tooltip elucidat-course-refresh-tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
                                                'data-html'		=> 'true',
                                                'data-trigger'  => 'hover focus',
                                                'title'			=> Yii::t('message','_REFRESH'),
                                            )); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="controls">
                                        <?= CHtml::activeCheckBox($loElucidat, 'keep_updated',array('id'=>'LearningElucidat_keep_updated')); ?>
                                        <?= CHtml::label(Yii::t('elucidat', 'Keep the course updated to the latest release'), 'LearningElucidat_keep_updated',array('id'=>'keep-updated-label')) ?>
                                        <?= CHtml::label(Yii::t('elucidat', 'Flag this checkbox in order to auto-update the course content to the latest release.'), 'LearningElucidat_keep_updated',array('id'=>'keep-updated-hint','class'=>'input-hint')) ?>
                                        <?php if($mode=='edit' && $loElucidat->keep_updated && count($fullInfo)>0){?>
                                            <div id="keep-updated-last-status">
                                                <?php if($loElucidat->last_status==LearningElucidat::STATUS_SUCCESS){?>
                                                    <i class="fa fa-check-circle green-text"></i>
                                                    <span id="status-text"><?= Yii::t('elucidat', 'Your course has been <span style="color:green;">succesfully updated</span> to the latest release on {date}',array('{date}'=> Yii::app()->localtime->toLocalDateTime($loElucidat->last_status_updated)))?></span>
                                                <?php }else{ ?>
                                                    <i class="fa fa-times-circle red-text"></i>
                                                    <span id="status-text"><?= Yii::t('elucidat', 'Your course <span style="color:red;">cannot be updated</span> to the latest release. Last succesfull update on {date}. <strong>Please contact your Elucidat administrator to solve this issue!</strong>',array('{date}'=>Yii::app()->localtime->toLocalDateTime($loElucidat->last_successful_update)))?></span>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <?php echo CHtml::label(Yii::t('standard', '_TITLE'), $orgObj instanceof LearningOrganization ? 'LearningOrganization_title' : 'LearningRepositoryObject_title', array(
                                        'class' => 'control-label'
                                    )); ?>
                                    <div class="controls">
                                        <?= $form->textField($orgObj, 'title', array('class' => 'input-block-level')) ?>
                                    </div>
                                </div>
<!--                                <div class="control-group">-->
<!--                                    --><?php //echo CHtml::label(Yii::t('standard', '_DESCRIPTION'), 'LearningOrganization_short_description', array(
//                                        'class' => 'control-label'
//                                    )); ?>
<!--                                    <div class="controls">-->
<!--                                        --><?//= $form->textArea($orgObj, 'short_description', array('id' => 'player-arena-elucidat-editor-textarea')); ?>
<!--                                    </div>-->
<!--                                </div>-->

                                <?php
                                // Same options, different model
                                $options['desktop_openmode'] 	= $orgObj->desktop_openmode;
                                $options['tablet_openmode'] 	= $orgObj->tablet_openmode;
                                $options['smartphone_openmode'] = $orgObj->smartphone_openmode;

                                $this->renderPartial('_view_mode_options', array(
                                    'options' 		=> $options,
                                    'courseModel' 	=> $courseModel,
                                ));
                                ?>

                                <br />
                            </div>
                        </div>
                            <div class="tab-pane" id="lo-additional-information">
                                <?php
                                $additionalInfoWidgetParams = array(
                                    'loModel'=> isset($loModel) ? $loModel : $orgObj,
                                    'idCourse' => isset($courseModel) ? $courseModel->idCourse : null,
                                );

                                if($centralRepoContext){
                                    $additionalInfoWidgetParams['refreshUrl'] = Docebo::createAbsoluteLmsUrl('centralrepo/centralLo/getSlidersContent');
                                    $additionalInfoWidgetParams['deleteUrl'] = Docebo::createAbsoluteLmsUrl('centralrepo/centralLo/deleteImage');
                                }
                                ?>
                                <? $this->widget('common.widgets.PlayerLayoutAdditionalInfo', $additionalInfoWidgetParams); ?>
                            </div>
                        <?php
                        // Raise event to let plugins add new tab contents
                        if(!$centralRepoContext){
                            Yii::app()->event->raise('RenderEditLoTabsContent', new DEvent($this, array(
                                'courseModel' => $courseModel,
                                'loModel' => $orgObj,
                            )));
                        }
                        ?>
                    </div>
                </div>

            </div>

        </div>

        <div class="text-right">
            <input type="submit" name="confirm_save_htmlpage" class="btn-docebo green big"
                   value="<?php echo Yii::t('standard', '_SAVE'); ?>">&nbsp;&nbsp;
            <a id="cancel_save_elucidat"
               class="btn-docebo black big"><?php echo Yii::t('standard', '_CANCEL'); ?></a>
        </div>




        <?php $this->endWidget(); ?>
    </div>
</div>
<?php if($centralRepoContext) echo '</div>' ?>
<script type="text/javascript">


	/************************ Elucidat Class: can be moved to a JS file *******************************/
	var ElucidatClass = function(options) {
		for(var option in options) {
			this[option] = options[option];
		}

		this.addListeners();
		
	};

	/**
	 * Elucidat Class prototype
	 */
	ElucidatClass.prototype = {

		// Semaphore to prevent double course refreshing (we've got TWO refresh-courses buttons)
		inRefreshCourses: false,

		// Reflect changes in course & release selection
		initReleasesList: function (all,editUrls, fillTitle){

	        var p_code = $('#LearningElucidat_p_project_code').val();
	        var r_code = $('#LearningElucidat_releases_list').val();
	        
	        $('#LearningElucidat_r_created').val(all[p_code]['releases'][r_code]['created']);
	        $('#LearningElucidat_r_description').val(all[p_code]['releases'][r_code]['description']);
	        $('#LearningElucidat_r_version_number').val(all[p_code]['releases'][r_code]['version_number']);
	        $('#LearningElucidat_r_release_mode').val(all[p_code]['releases'][r_code]['release_mode']);

            if(p_code!=0) {
                $('#edit-elucidat-btn').attr('href', editUrls[p_code]);
            }
            else{
                $('#edit-elucidat-btn').attr('href', 'javascript:;');
            }

	        
// 	        $('#edit-elucidat-btn').off('click');
// 	        $('#edit-elucidat-btn').on('click',function(){
// 	            window.location.href = editUrls[p_code];
// 	        });
	        
	        if(fillTitle) {
	            $(<?= json_encode($orgObj instanceof LearningOrganization ? '#LearningOrganization_title' : '#LearningRepositoryObject_title') ?>).val(all[p_code]['releases'][r_code]['description']);
	        }
	        
	    }, 

		// Called upon Course-refreshing 
		updateDropdowns: function() {
			// First, remove all NON-0 options from Courses dropdown (keeping "Select..." prompt, which is "0"
			var oSelectProject = $('select[name="LearningElucidat[p_project_code]"]');
            var currentSelectRelease = $('#LearningElucidat_releases_list').val();
			var currentProjectCode = oSelectProject.val(); 
			oSelectProject.find('option:not([value="0"])').remove();
			// Enumerate all courses and add OPTION in Project selector
            var projectExists = false;
			for(var projectCode in this.all) {
				var option = $('<option></option>').val(projectCode).text(this.all[projectCode].name).appendTo(oSelectProject);
                if(projectCode == currentProjectCode)
                    projectExists = true;
			}
            if(projectExists) {
                oSelectProject.val(currentProjectCode).change();
                if(typeof this.all[currentProjectCode].releases[currentSelectRelease] === 'undefined')
                    $('#keep-updated-last-status').html("<i class=\"fa fa-times-circle red-text\"></i><span id=\"status-text\"><?= Yii::t('elucidat','Previously selected Elucidat course release doesn\'t exist anymore. Please select another one from the list!');?></span>");
            }
            else{
                oSelectProject.val(0).change();
                $('#keep-updated-last-status').html("<i class=\"fa fa-times-circle red-text\"></i><span id=\"status-text\"><?= Yii::t('elucidat','Previously selected Elucidat course doesn\'t exist anymore. Please select another one from the list!');?></span>");
            }
		},


		// Add event listeners
		addListeners: function() {
			var that = this;

			/**
			 * Listen for REFRESH click(s)
			 */
            $(document).off('click', '#player-arena-elucidat-editor-panel .refresh-courses, #player-centralrepo-uploader .refresh-courses');
			$(document).on('click', '#player-arena-elucidat-editor-panel .refresh-courses, #player-centralrepo-uploader .refresh-courses', function(e){
				e.preventDefault();
				e.stopPropagation();

				if (that.inRefreshCourses) return;
				
				that.inRefreshCourses = true;
				
				var url = <?= json_encode(Docebo::createAbsoluteLmsUrl('ElucidatApp/ElucidatApp/axGetCoursesInfo')) ?>;

				var data = {};

				$.ajax({
					url: url,
					data: data,
					type: 'POST',
					dataType: 'json',
	                beforeSend: function() {
                        $('#player-arena-elucidat-editor-panel .refresh-courses').html('<div class="ajaxloader" style="display: block;"></div>');
	                },
	                success:function(result) {
	                    if (result.success && typeof result.data === "object") {
		                    that.releases 	= result.data.releases;
		                    that.all 		= result.data.all;
	                		that.updateDropdowns();
	                    }
					},
					complete: function() {
						that.inRefreshCourses = false;
                        $('#player-arena-elucidat-editor-panel .refresh-courses').html('<i class="fa fa-refresh"></i>');
					}
				});
						
			});


			/**
			 * Listen for Project selector change
			 */
			$(document).on('change', '#LearningElucidat_p_project_code', function(){
	            if($(this).val()==0){
	                $('#edit-elucidat-btn').off('click');
	                $('#release-control-group').hide();
	            }else {
	                var p_code = $(this).val();
	                //Set LO properties for project
	                $('#LearningElucidat_edit_url').val(that.all[p_code]['edit_url']);
	                $('#LearningElucidat_p_name').val(that.all[p_code]['name']);
	                $('#LearningElucidat_p_created').val(that.all[p_code]['created']);

	                var options = '';
	                var currRel = that.releases[$(this).val()];
                    var releaseModes = {};
                    for (var releaseKey in currRel)
                    {
                        releaseModes[releaseKey] = that.all[$(this).val()].releases[releaseKey].release_mode;
                        releaseModes[releaseKey] = releaseModes[releaseKey].replace('online-public', 'Online (Public)');
                        releaseModes[releaseKey] = releaseModes[releaseKey].replace('online-private', 'Online (Private)');
                    }

	                $.each(currRel,function( index, value ){
	                    options += '<option value="' + index + '">' + value + ' - ' + releaseModes[index] + '</option>';
	                });
	                $('#LearningElucidat_releases_list').html(options);
	                $('#release-control-group').show(0,function(){
	                	that.initReleasesList(that.all,that.editUrls,true);
	                });

	            }
	        });

			/**
			 * Listen for Release selector change
			 */
	        $(document).on('change', '#LearningElucidat_releases_list', function(){that.initReleasesList(that.all,that.editUrls,true);});
				
		},

		
		/**
		 * Call this ONCE, when DOM is ready
		 */
		ready: function(projectExists, releaseExists, releasesExist, mode) {

			var that = this;
			
	        $('#player-arena-elucidat-editor-panel input[type="radio"],input[type="checkbox"], #player-centralrepo-uploader input[type="radio"],input[type="checkbox"]').styler();
            $('.tooltipize').tooltip();
            $('.tooltipize').on('shown.bs.tooltip', function () {
                $('.elucidat-course-refresh-tooltip.right.in').remove();
            });


            // If project exist but release doesn't
	        if (projectExists && !releaseExists){
		        
	            var p_code = $('#LearningElucidat_p_project_code').val();
	            
	            //Set LO properties for project
	            $('#LearningElucidat_edit_url').val(that.all[p_code]['edit_url']);
	            $('#LearningElucidat_p_name').val(that.all[p_code]['name']);
	            $('#LearningElucidat_p_created').val(that.all[p_code]['created']);

	            var options = '';
	            var currRel = that.releases[p_code];
	            $.each(currRel,function( index, value ){
	                options += '<option value="' + index + '">' + value + '</option>';
	            });
	            
	            $('#LearningElucidat_releases_list').html(options);
	            
	            $('#release-control-group').show(0, function(){
	            	that.initReleasesList(that.all,that.editUrls, false);
	            });
	            
	            $('#keep-updated-last-status').html("<i class=\"fa fa-times-circle red-text\"></i><span id=\"status-text\"><?= Yii::t('elucidat','Previously selected Elucidat course release doesn\'t exist anymore. Please select another one from the list!');?></span>");
	            
	        }
			//Else if project doesn't exist
	        else if ( mode == 'edit' && !projectExists){
	            $('#keep-updated-last-status').html("<i class=\"fa fa-times-circle red-text\"></i><span id=\"status-text\"><?= Yii::t('elucidat','Previously selected Elucidat course doesn\'t exist anymore. Please select another one from the list!');?></span>");
	        }

	        if ( mode =='edit' && releasesExist) {
	            $('#release-control-group').show(0, function(){
	            	that.initReleasesList(that.all,that.editUrls, false);
	            });
	        }
			
		}
		
		
	};
	/************************ Elucidat Class *******************************/


	
	
	// -- On Document Ready
    $(function(){

		var projectExists = <?= isset($projects[$loElucidat->p_project_code]) ? "true" : "false" ?>;
		var releaseExists = <?= isset($releases[$loElucidat->p_project_code][$loElucidat->r_release_code]) ? "true" : "false" ?>;
		var releasesExist = <?= isset($releases[$loElucidat->p_project_code]) ? "true" : "false" ?>;
		var mode = <?= json_encode($mode) ?>;

		// Create an instance of Elucidat class
    	var Elucidat = new ElucidatClass({
    		releases: <?= json_encode($releases) ?>, 
    		editUrls: <?= json_encode($editUrls) ?>,
    		all		: <?= json_encode($fullInfo) ?>,
    	});

		// Tell Elucidat we are ready, call this ONCE ONLY!
    	Elucidat.ready(projectExists, releaseExists, releasesExist, mode); 
        
        <?php if($centralRepoContext): ?>
            var elucidatEdit = new CentralLoElucidatEdit() ;
            elucidatEdit.formListeners();
        <?php endif; ?>

    });

    
    
</script>