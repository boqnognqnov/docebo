<div class="row-fluid">
    <strong><?= Yii::t('standard', 'View Mode') ?></strong>
</div>


<div class="row-fluid elucidat-upload-options">

    <div class="span4 text-center">
        <div class="row-fluid device-type">
            <i class="course-device-type devices-sprite dev-desktop-tablet">&nbsp;</i><?= Yii::t('standard', 'Desktop') ?>
        </div>
        <div class="row-fluid">



            <div class="control-group text-left">
                <div class="controls">
                    <label class="radio" for="desktop_openmode_1"><?= LearningOrganization::openModeName(LearningOrganization::OPENMODE_INLINE) ?>
                        <input type="radio" id="desktop_openmode_1" name="desktop_openmode" value="<?= LearningOrganization::OPENMODE_INLINE ?>" <?= $options['desktop_openmode']==LearningOrganization::OPENMODE_INLINE ? 'checked' : '' ?>>
                    </label>
                </div>
            </div>

            <div class="control-group text-left">
                <div class="controls">
                    <label class="radio" for="desktop_openmode_2"><?= LearningOrganization::openModeName(LearningOrganization::OPENMODE_LIGHTBOX) ?>
                        <input type="radio" id="desktop_openmode_2" name="desktop_openmode" value="<?= LearningOrganization::OPENMODE_LIGHTBOX ?>" <?= ($options['desktop_openmode']==LearningOrganization::OPENMODE_LIGHTBOX || is_null($options['desktop_openmode'])) ? 'checked' : '' ?>>
                    </label>
                </div>
            </div>


            <div class="control-group text-left">
                <div class="controls">
                    <label class="radio" for="desktop_openmode_3"><?= LearningOrganization::openModeName(LearningOrganization::OPENMODE_FULLSCREEN) ?>
                        <input type="radio" id="desktop_openmode_3" name="desktop_openmode" value="<?= LearningOrganization::OPENMODE_FULLSCREEN ?>" <?= $options['desktop_openmode']==LearningOrganization::OPENMODE_FULLSCREEN ? 'checked' : '' ?>>
                    </label>
                </div>
            </div>

            <div class="control-group text-left">
                <div class="controls">
                    <label class="radio" for="desktop_openmode_4"><?= LearningOrganization::openModeName(LearningOrganization::OPENMODE_POPUP) ?>
                        <input type="radio" id="desktop_openmode_4" name="desktop_openmode" value="<?= LearningOrganization::OPENMODE_POPUP ?>" <?= $options['desktop_openmode']==LearningOrganization::OPENMODE_POPUP ? 'checked' : '' ?>>
                    </label>
                </div>
            </div>


        </div>
    </div>

    <div class="span4 text-center">
        <div class="row-fluid device-type">
            <i class="course-device-type devices-sprite dev-tablet dev-large">&nbsp;</i><?=Yii::t('standard', 'Tablet')?>
        </div>
        <div class="row-fluid">

            <div class="control-group text-left">
                <div class="controls">
                    <label class="radio" for="tablet_openmode_1"><?= LearningOrganization::openModeName(LearningOrganization::OPENMODE_FULLSCREEN) ?>
                        <input type="radio" id="tablet_openmode_1" name="tablet_openmode" value="<?= LearningOrganization::OPENMODE_FULLSCREEN ?>" <?= ($options['tablet_openmode']==LearningOrganization::OPENMODE_FULLSCREEN || is_null($options['tablet_openmode'])) ? 'checked' : '' ?>>
                    </label>
                </div>
            </div>
            <div class="control-group text-left">
                <div class="controls">
                    <label class="radio" for="tablet_openmode_2"><?= LearningOrganization::openModeName(LearningOrganization::OPENMODE_POPUP) ?>
                        <input type="radio" id="tablet_openmode_2" name="tablet_openmode" value="<?= LearningOrganization::OPENMODE_POPUP ?>" <?= $options['tablet_openmode']==LearningOrganization::OPENMODE_POPUP ? 'checked' : '' ?>>
                    </label>
                </div>
            </div>

        </div>

    </div>


    <?php // all elearning are smatphone capable now // if ($courseModel->course_type == LearningCourse::TYPE_MOBILE) : ?>
    <div class="span4 text-center">
        <div class="row-fluid device-type">
            <i class="course-device-type devices-sprite dev-smartphone">&nbsp;</i><?=Yii::t('standard', 'Smartphone')?>
        </div>
        <div class="row-fluid">

            <div class="control-group text-left">
                <div class="controls">
                    <label class="radio" for="smartphone_openmode_1"><?= LearningOrganization::openModeName(LearningOrganization::OPENMODE_FULLSCREEN) ?>
                        <input type="radio" id="smartphone_openmode_1" name="smartphone_openmode" value="<?= LearningOrganization::OPENMODE_FULLSCREEN ?>" <?= ($options['smartphone_openmode']==LearningOrganization::OPENMODE_FULLSCREEN || is_null($options['smartphone_openmode'])) ? 'checked' : '' ?>>
                    </label>
                </div>
            </div>

        </div>
    </div>
    <?php // endif; ?>



</div>