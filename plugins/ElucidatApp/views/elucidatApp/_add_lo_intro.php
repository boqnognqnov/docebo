<?php
/**
 * @var $centralRepoContext boolean
 */
?>
<?php if($centralRepoContext) echo '<div class="row-fluid" data-bootstro-id="bootstroLaunchpad" id="player-arena-content">' ?>
<div id="<?= ($centralRepoContext) ? 'player-centralrepo-uploader-panel' : 'player-arena-elucidat-editor-panel' ?>" class="player-arena-editor-panel">
    <div class="header">
        <?php echo Yii::t('elucidat', 'Import'); echo " Elucidat"; ?>
    </div>
    <div class="row-fluid">
        <div class="span1"></div>
        <?php if ( !$validElucidat ) { ?>
            <?php if ( Yii::app()->user->getIsGodadmin() ) { ?>
                <?php $this->widget('common.widgets.warningStrip.WarningStrip', array(
                    'id'        => 'invalid-elucidat',
                    'message'   => Yii::t('elucidat', '<b>WARNING: </b>It looks like your <b>Elucidat app is not properly configured.</b> Please check your <a href="javascript:;">Elucidat app configuration</a>'),
                    'type'      => 'warning',
                )); ?>
            <?php } else {?>
                        <?php $this->widget('common.widgets.warningStrip.WarningStrip', array(
                            'id'        => 'invalid-elucidat',
                            'message'   => Yii::t('elucidat', 'Please, report your problem to your local administrator'),
                            'type'      => 'warning',
                        )); ?>
                    <?php } ?>
        <?php } ?>
    </div>
    <div id="main-elucidat-section" class="player-arena-editor-panel content row">
            <div class="span6 player-arena-editor-panel">
                <div class="elucidat-select-course-container">
                    <div class="row-fluid">
                        <div class="span4 elucidat-logo-container">
                            <span class="elucidat-logo">

                    </span>
                        </div>
                        <div class="span8 elucidat-select-course-option-text">
                            <span class="elucidat-box-title"><?=Yii::t('elucidat', 'Select Elucidat course')?></span></br>
                            <span class="elucidat-box-text"><?=Yii::t('elucidat', 'Select a course and it\'s releases from your Elucidat account')?></span>
                        </div>
                        <?php if ( $validElucidat ) { ?>
                        <input class="btn btn-docebo green big elucidat-btn lo-select-elucidat" type="button" value="<?= Yii::t('course', '_COURSE_SELECTION') ?>" />
                        <?php } else {?>
                            <input class="btn btn-docebo green big elucidat-btn disabled" type="submit" value="<?= Yii::t('course', '_COURSE_SELECTION') ?>" />
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="span6 player-arena-editor-panel">
                <div class="elucidat-create-course-container">
                    <div class="row-fluid">
                        <div class="span4 elucidat-logo-container">
                            <span class="elucidat-logo"></span>
                        </div>
                        <div class="span8 elucidat-create-course-text">
                            <span class="elucidat-box-title"><?=Yii::t('elucidat', 'Create a new Elucidat course')?></span></br>
                            <span class="elucidat-box-text">
                                <?=Yii::t('elucidat', 'You will be redirected to your Elucidat account in order to create a new course')?>
                            </span>
                        </div>
                        </br>
                        <?php if ( !$validElucidat) { ?>
                            <input class="btn btn-docebo green big elucidat-btn disabled" type="button" value="<?= Yii::t('elucidat', 'Create course') ?>" />
                        <?php } ?>
                        <?php if ( $validAuthor && $validElucidat) {?>
                            <a id="create-elucidat-btn" target="_blank" class="btn btn-docebo green big elucidat-btn" href="<?=Docebo::createAbsoluteLmsUrl('ElucidatApp/ElucidatApp/redirectEdit', array(
                                'project_url'	=> 	'https://app.elucidat.com/projects'
                            ));?>"><?= Yii::t('elucidat', 'Create course') ?></a>
                        <?php } else if ( $validElucidat ) {?>
                            <div class="span12 intro-warning-container-author">
                                <?php $this->widget('common.widgets.warningStrip.WarningStrip', array(
                                    'message'   => Yii::t('elucidat', 'In order to create a new course, <b>you must be a valid author in Elucidat</b>. Please contact your Elucidat administrator for more information'),
                                    'type'      => 'info',
                                )); ?>
                                <a id="create-elucidat-btn" class="btn btn-docebo green big elucidat-btn disabled" href=""><?= Yii::t('elucidat', 'Create course') ?></a>
                                <?php } ?>
                            </div>
                    </div>
                </div>
                <div class="text-right">
                    <a id="player-cancel-uploaded-lo" href="<?= $centralRepoContext ? Docebo::createAbsoluteLmsUrl('centralrepo') : '#' ?>" class="btn btn-docebo black big"><?php echo Yii::t('standard', '_CANCEL'); ?></a>
                    </div>
            </div>
    </div>
</div>
<?php if($centralRepoContext) echo '</div>' ?>
<script type="text/javascript">
    $( document ).ready(function() {
		var settingsUrl = <?= json_encode(Docebo::createAdminUrl('ElucidatApp/ElucidatApp/settings')) ?>;
        
        $("#invalid-elucidat a").click(function(){
            window.location.href = settingsUrl;
        });

        <?php if(!$centralRepoContext): ?>
            $("#player-cancel-uploaded-lo").click(function(e){
                e.preventDefault();
                $("#player-arena-elucidat-editor-panel").html("");
                Arena.setEditCourseMode();
                return false;
            });
        <?php else: ?>
            var eEdit = new CentralLoElucidatEdit({
                editPageUrl: <?= json_encode(Docebo::createAbsoluteAdminUrl('ElucidatApp/ElucidatApp/editCentralRepo')) ?>
            });
            eEdit.globalListeners();
        <?php endif; ?>
    });
</script>

