<?php
$_breadcrumbs = array(
    Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
    'Elucidat'
);

if (is_array($breadcrumbs)) {
    $_breadcrumbs = array_merge($_breadcrumbs, $breadcrumbs);
} else {
    $_breadcrumbs[] = $breadcrumbs;
}

$this->breadcrumbs = $_breadcrumbs;
?>

<div class="row-fluid">
    <div class="span6">
        <h3 class="advanced-elucidat-settings-form-title">Elucidat</h3>
    </div>
    <div class="span6">
        <a href="<?= ('it' == Yii::app()->getLanguage()) ? 'https://www.docebo.com/it/knowledge-base/docebo-per-elucidat/' : 'https://www.docebo.com/knowledge-base/docebo-for-elucidat/' ?>" target="_blank" class="app-link-read-manual">
            <?= Yii::t('apps', 'Read Manual'); ?>
        </a>
    </div>
</div>
<br>

<?php DoceboUI::printFlashMessages(); ?>

<?= CHtml::beginForm(); ?>

<div class="advanced-main elucidat-settings">

    <div class="section">

        <div class="row odd">
            <div class="row">
                <div class="setting-name public-key-setting-name">
                    <?= Yii::t('elucidat', 'Public Key') ?>
                    <p class="muted"><?= Yii::t('elucidat', 'Also known as Custom key') ?></p>
                </div>
                <div class="values">
                    <?= CHtml::textField('public_key', $publicKey) ?>
                </div>
            </div>
        </div>

        <div class="row even">
            <div class="row">

                <div class="setting-name public-key-setting-name">
                    <?= Yii::t('apps', 'API Secret') ?>
                </div>
                <div class="values">
                    <?= CHtml::textField('secret_key', $secretKey) ?>
                </div>

            </div>
        </div>

        <div class="row odd" style="display: none;">
            <div class="row">

                <div class="setting-name public-key-setting-name">
                    <?= Yii::t('apps', 'Release Types') ?>
                    <p class="muted"><?= Yii::t('elucidat', 'Define which type of release are allowed to be imported') ?></p>
                </div>
                <div class="values">
                    <?= CHtml::radioButton(
                        'release_type',
                        ($releaseType == 'all'),
                        array(
                            'id' => 'all',
                            'value' => 'all',
                            'style' => 'display: none'
                        )
                    )
                    ?>
                    <span
                        id="all-styler"
                        onclick="javascript: $('#all')[0].checked = true; $('#online_public')[0].checked = false; $('#online_private')[0].checked = false; $('#all-styler')[0].classList.add('checked'); $('#online-public-styler')[0].classList.remove('checked'); $('#online-private-styler')[0].classList.remove('checked'); "
                        class="jq-radio pull-right <?= ($releaseType == 'all') ? 'checked' : '' ?>"
                        style="display: inline">
                        <span></span>
                    </span>
                    <?=
                        CHtml::label(
                            Yii::t('elucidat', 'All'),
                            'all',
                            array(
                                'onclick'
                                    => "javascript: $('#all-styler')[0].classList.add('checked'); $('#online-public-styler')[0].classList.remove('checked'); $('#online-private-styler')[0].classList.remove('checked');"
                            )
                        )
                    ?>
                    <br />
                    <?= CHtml::radioButton(
                        'release_type',
                        ($releaseType == 'online_public'),
                        array(
                            'id' => 'online_public',
                            'value' => 'online_public',
                            'style' => 'display: none'
                        )
                    )
                    ?>
                    <span
                        id="online-public-styler"
                        onclick="javascript: $('#all')[0].checked = false; $('#online_public')[0].checked = true; $('#online_private')[0].checked = false; $('#all-styler')[0].classList.remove('checked'); $('#online-public-styler')[0].classList.add('checked'); $('#online-private-styler')[0].classList.remove('checked'); "
                        class="jq-radio pull-right <?= ($releaseType == 'online_public') ? 'checked' : '' ?>"
                        style="display: inline">
                        <span></span>
                    </span>
                    <?=
                        CHtml::label(
                            Yii::t('elucidat', 'Online-Public only'),
                            'online_public',
                            array(
                                'onclick'
                                    => "javascript: $('#all-styler')[0].classList.remove('checked'); $('#online-public-styler')[0].classList.add('checked'); $('#online-private-styler')[0].classList.remove('checked');"
                            )
                        )
                    ?>
                    <br />
                    <?= CHtml::radioButton(
                        'release_type',
                        ($releaseType == 'online_private'),
                        array(
                            'id' => 'online_private',
                            'value' => 'online_private',
                            'style' => 'display: none'
                        )
                    )
                    ?>
                    <span
                        id="online-private-styler"
                        onclick="javascript: $('#all')[0].checked = false; $('#online_public')[0].checked = false; $('#online_private')[0].checked = true; $('#all-styler')[0].classList.remove('checked'); $('#online-public-styler')[0].classList.remove('checked'); $('#online-private-styler')[0].classList.add('checked'); "
                        class="jq-radio pull-right <?= ($releaseType == 'online_private') ? 'checked' : '' ?>"
                        style="display: inline">
                        <span></span>
                    </span>
                    <?=
                        CHtml::label(
                            Yii::t('elucidat', 'Online-Private only'),
                            'online_private',
                            array(
                                'onclick'
                                    => "javascript: $('#all-styler')[0].classList.remove('checked'); $('#online-public-styler')[0].classList.remove('checked'); $('#online-private-styler')[0].classList.add('checked');"
                            )
                        )
                    ?>
                </div>

            </div>
        </div>

        <br>

        <div class="row-fluid right">
            <?php if($isConfigured): ?>

                <?= CHtml::button(Yii::t('elucidat', 'Reset configuration'), array(
                    'class' => 'btn btn-docebo red big',
                    'id' => 'elucidat-clear'
                )) ?>
            <?php endif; ?>
            <?= CHtml::submitButton(Yii::t('app7020','Save Changes'), array(
                'class' => 'btn btn-docebo green big',
                'name' => 'submit'
            )); ?>
            <?= CHtml::submitButton(Yii::t('app7020','Cancel'), array(
                'class' => 'btn btn-docebo black big',
                'name' => 'cancel'
            )); ?>
        </div>

    </div>
</div>

<?= CHtml::endForm() ?>

<script type="text/javascript">
    $(function(){
        if ( $(".alert-success").length) {
            setInterval(function(){
                $(".alert-success").fadeOut(1500);
            }, 3000);
        }
        if ( $(".alert-error").length) {
            setInterval(function(){
                $(".alert-error").fadeOut(1500);
            }, 3000);
        }

        $(document).on('click', '#elucidat-clear', function(event){
            var url = HTTP_HOST + "index.php?r=ElucidatApp/ElucidatApp/clearSettings";
            window.location.href = url;
        });
    });
</script>