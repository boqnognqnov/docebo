<?php

class ElucidatAppModule extends CWebModule
{

	private $_assetsUrl;

	const ELUCIDAT_USERID = '/elucidat.user';
	
	public function init()
	{
		// import the module-level models and components
		$this->setImport(array(
			'ElucidatApp.components.*'
		));
		
		$this->defaultController = 'elucidatApp';
		
		// Register CSS ONLY if this is NOT an ajax call. Plugins' init() is always called during their loading
        if(Yii::app() instanceof CWebApplication){
            if (!Yii::app()->request->isAjaxRequest) {
                Yii::app()->getClientScript()->registerCssFile($this->getAssetsUrl() . "/css/elucidat.css");
            }
        }

		Yii::app()->event->on(EventManager::EVENT_ON_RENDER_USERMAN_USER_ACTIONS, array($this, 'onUsersActionsRender'));

		Yii::app()->event->on(EventManager::EVENT_ON_PLUGIN_DEACTIVATE, array($this, 'onPluginDeactivate'));
		
	}
	
	
	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu()
	{
		// Add menu voices !
		Yii::app()->mainmenu->addAppMenu('elucidat', array(
				'icon' => 'admin-ico elucidat',
				'app_icon' => 'fa-image', // icon used in the new menu
				'link' => Docebo::createAdminUrl('//ElucidatApp/ElucidatApp/settings'),
				'label' => 'Elucidat',
				//'settings' => Docebo::createAdminUrl('//ElucidatApp/ElucidatApp/settings'),
		), array());
	}
	
	
	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('ElucidatApp.assets'));
		}
		return $this->_assetsUrl;
	}


	/**
	 * @param $event DEvent
	 */
	public function onUsersActionsRender($event){
		$userModel = $event->params['user'];

		/**
		 * @var $userModel CoreUser
		 */
		if($userModel->userid == self::ELUCIDAT_USERID){
			$event->return_value['hideActions'] = true;
		}
	}


	public function onPluginDeactivate(DEvent $event){
	    
	    // We don't care about Plugins anyway, we only do this check for ElucidatApp!
	    if (!isset($event->params['pluginName']) || $event->params['pluginName'] !== $this->name) {
	        $event->return_value['canProceed'] = true;
	        return;
	    }
	    
		$canProceed = false;

		$elucidatLOs = LearningElucidat::model()->findAll();

		$numLOs = count($elucidatLOs);

		if($numLOs === 0){
			$canProceed = true;
		}

		if($canProceed === false){
			Yii::app()->user->setFlash('error', Yii::t('elucidat', 'In order to deactivate this app, you should delete all Elucidat training materials from your courses'));
		}

		$event->return_value['canProceed'] = $canProceed;
	}
	
}
