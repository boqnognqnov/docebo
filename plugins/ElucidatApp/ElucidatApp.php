<?php

class ElucidatApp extends DoceboPlugin {

	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'ElucidatApp';
	public static $HAS_SETTINGS = false;

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name = __CLASS__) {
		return parent::plugin($class_name);
	}

	public function activate() {
		Yii::import('plugin.ElucidatApp.components.*');
		parent::activate();

		$consumerKey = Settings::get('elucidat_consumer_key', false);
		$secretKey = Settings::get('elucidat_secret_key', false);

		if($consumerKey && $secretKey){
			$client = new ElucidatApiClient($consumerKey, $secretKey);

			$clientInfo = $client->getAccountInfo();

			if($clientInfo->success === true){
				$eventName = ElucidatApiClient::EVENT_RELEASE_COURSE;
				$event = $client->subscribeToEvent($eventName, Docebo::createAbsoluteUrl("ElucidatApp/ElucidatApp/webhook"));

				if($event->success === true){
					Yii::log('Elucidat: subscribed to event: ' . $eventName, CLogger::LEVEL_INFO);
				}
			}
		}
	}

	public function deactivate() {
		parent::deactivate();

		$consumerKey = Settings::get('elucidat_consumer_key', false);
		$secretkey = Settings::get('elucidat_secret_key', false);

		if($consumerKey && $secretkey){
			$client = new ElucidatApiClient($consumerKey, $secretkey);

			$eventName = ElucidatApiClient::EVENT_RELEASE_COURSE;
			$event = $client->unSubscribeFromEvent($eventName);

			if($event->success === true){
				Yii::log('Elucidat: unsubscribe from event: ' . $eventName, CLogger::LEVEL_INFO);
			}
		}
	}

}
