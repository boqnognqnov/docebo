<?php

class m151012_142304_SAMPLE_APP_SAMPLE_MIGRATION extends CDbMigration
{
	public function up()
	{
		Settings::save('app_migrations_success', 1);
	}

	public function down()
	{
		echo "m151012_142304_SAMPLE_APP_SAMPLE_MIGRATION does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}