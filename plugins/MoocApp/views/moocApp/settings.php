<?php
	$this->breadcrumbs[] = 'MOOC ' . Yii::t('standard', 'Settings');
?>

<h2>MOOC <?= Yii::t('standard', 'Settings') ?></h2>
<br/>

<p class="description-with-link">
	<?= Yii::t('app', 'The initial configuration has been completed. From here you can setup your user and courses normally, please take a look at the manual for further information on how to setup yout MOOC environment.'); ?>
</p>

<p class="description-with-link">
	<a target="_blank" href="https://www.docebo.com/knowledge-base/create-mooc-docebo/">https://www.docebo.com/knowledge-base/create-mooc-docebo/</a>
</p>