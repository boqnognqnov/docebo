<?php

/**
 * Plugin for MoocApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class MoocAppModule extends CWebModule {

	public function init() {

		parent::init();
	}

	public function beforeControllerAction($controller, $action) {

		if (parent::beforeControllerAction($controller, $action)) {

			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		} else {

			return false;
		}
	}


	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu() {
		//compose the entire menu
		Yii::app()->mainmenu->addAppMenu('mooc', array(
			'app_icon' => 'fa-th',
			'settings' => Docebo::createAdminUrl('MoocApp/MoocApp/settings'),
			'label' => 'Mooc',
		),
			array()
		);
	}
}
