<?php

/**
 * Plugin for MoocApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class MoocApp extends DoceboPlugin {

	/**
	 * @var string Name of the plugin; needed to identify plugin folder and entry script
	 */
	public $plugin_name = 'MoocApp';

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name = __CLASS__) {

		return parent::plugin($class_name);
	}

    /**
     * 
     * @param unknown $app
     * @param unknown $lang
     * @param unknown $hasToContactResellerToInstall
     * @param unknown $isInTrialPeriod
     * @param unknown $cycle
     * @return boolean[]|string[]
     */
	public static function installDetail($app, $lang, $hasToContactResellerToInstall, $isInTrialPeriod, $cycle) {
	    
	    // If New Pricing model is enabled for this LMS, we must return a bit different UI
	    $api = new ErpApiClient2();
	    $isBundledPricing = $api->call('lms/is-bundle-pricing');
	    if ($isBundledPricing) {
	        $api->params = array(
	            'internal_codename' => $app['internal_codename'],
	        );
	        $app = $api->call('lms/get-app');
	        return self::installDetailsBundleVersion($app);
	    }
	     
	    
	    // Here come the "OLD" pricing UI 
	    
		// Sorry but i have had to do a strange thing to manage this case :/
		$params = array(
			'installation_id' => Docebo::getErpInstallationId(),
			'filter' => 'all',
			'lang' => Yii::app()->getLanguage(),
			'ref' => isset(Yii::app()->request->cookies['reseller_cookie'])? Yii::app()->request->cookies['reseller_cookie']->value : false
		);

		$apiResponse = AppsMpApiClient::apiListAvailableApps($params);
		$response = CJSON::decode($apiResponse);
		$apps = isset($response['data']) ? $response['data'] : false;
		if(!$apps){
			echo CJSON::encode(array('html'  => 'Can not retrieve apps'));
			Yii::app()->end();
		}

		$apps_id = array();
		foreach ($apps as $id => $app_scan) {

			// Check if this App is available as Yii module/plugin here
			$plugin_name =  ucfirst($app_scan['internal_codename']) . "App";
			$apps_id[$plugin_name] = $app_scan['pricing'];
		}

		$out = array();
		$out['skip_default_button'] = true;
		$out['custom_desciption'] = ''
			. CHtml::beginForm(Yii::app()->createUrl('app/axMoocConfirmInstall', array('id' => $app['app_id'], 'type' => 'install')), 'POST', array(
				'class' => 'ajax form-inline'
			))

			. Yii::t('app', 'This free App helps you setup a Corporate MOOC using your Docebo LMS.')
			.'<br/><br/>'

			. '<div class="checkbox"><label>'
				. CHtml::checkBox('public_catalog', true, array('class' => 'form-control')) . ' '
				. Yii::t('app', 'I want to have a public course catalog visible even to non registered users')
			. '</label></div>'
			. '<div class="checkbox"><label>'
				. CHtml::checkBox('self_registration', true, array('class' => 'form-control')) . ' '
				. Yii::t('app', 'I want to enable self-registration to my LMS from the public page')
			. '</label></div>'
			. '<div class="checkbox"><label>'
				. CHtml::checkBox('ecommerce', true, array('class' => 'form-control')) . ' '
				. Yii::t('app', 'I want to sell my courses to end users (this will enable the E-commerce App - {$$$info})', array(
				'{$$$info}' => $apps_id['EcommerceApp']['price']
					.($apps_id['EcommerceApp']['currency'] == 'usd' ? '$' : '&euro;' ) . ' '
					.($cycle == 'monthly' ? Yii::t('order', '/Month') : Yii::t('yearly', '/Year') ),
			))
			. '</label></div>'
			. '<div class="checkbox"><label>'
				. CHtml::checkBox('gamification', true, array('class' => 'form-control')) . ' '
				. Yii::t('app', 'I want to create a more engaging environment for my users to increase user retention (this will enable the Gamification App - {$$$info})', array(
					'{$$$info}' => $apps_id['GamificationApp']['price']
						.($apps_id['GamificationApp']['currency'] == 'usd' ? '$' : '&euro;' ) . ' '
						.($cycle == 'monthly' ? Yii::t('order', '/Month') : Yii::t('yearly', '/Year') ),
				))
			. '</label></div>'
			. '<br/>'
			.'<p class="description-with-link">'
			. Yii::t('app', 'For more information, read our article on how to create MOOCs with the Docebo LMS: <a target="_blank" href="{link_to_article}">{link_to_article}</a>', array(
				'{link_to_article}' => 'https://www.docebo.com/2014/11/26/creating-moocs-docebo-lms/'
			))
			. '</p>'
			.'<p class="description-with-link">'
			. Yii::t('app', 'Please also see our MOOC manual: <a target="_blank" href="{link_to_manual}">{link_to_manual}</a>', array(
				'{link_to_manual}' => 'https://www.docebo.com/knowledge-base/create-mooc-docebo/'
			))
			. '</p>'
			. '<br/>'
			.'<div class="text-right"><input type="submit" class="ajax btn-docebo blue big text-center" value="' . Yii::t('apps', 'Installnow') . '" /></div>'

			.CHtml::endForm();

		return $out;
	}

	/**
	 * 
	 * @param unknown $app
	 * @return array
	 */
	private static function installDetailsBundleVersion($app) {
	    
	    $out = array();
	    $out['skip_default_button'] = true;
	    $out['custom_desciption'] = ''
	        
	        . CHtml::beginForm(Yii::app()->createUrl('apps2/axMoocConfirmInstall', array('code' => $app['internal_codename'], 'action' => Apps2Widget::ACTION_ACTIVATE)), 'POST', array(
	            'class' => 'ajax form-inline'
	        ))
	        . Yii::t('app', 'This free App helps you setup a Corporate MOOC using your Docebo LMS.')
	        .'<br/><br/>'
	    
	        . '<div class="checkbox"><label>'
	        . CHtml::checkBox('public_catalog', true, array('class' => 'form-control')) . ' '
	        . Yii::t('app', 'I want to have a public course catalog visible even to non registered users')
	        . '</label></div>'
	        . '<div class="checkbox"><label>'
            . CHtml::checkBox('self_registration', true, array('class' => 'form-control')) . ' '
	        . Yii::t('app', 'I want to enable self-registration to my LMS from the public page')
	        . '</label></div>'
	        . '<div class="checkbox"><label>'
	        . CHtml::checkBox('ecommerce', true, array('class' => 'form-control')) . ' '
	        . Yii::t('app', 'I want to sell my courses to end users (this will enable the E-commerce App)')
            . '</label></div>'
	        . '<div class="checkbox"><label>'
	        . CHtml::checkBox('gamification', true, array('class' => 'form-control')) . ' '
	        . Yii::t('app', 'I want to create a more engaging environment for my users to increase user retention (this will enable the Gamification App)')
            . '</label></div>'
	        . '<br/>'
	        . '<p class="description-with-link">'
	        . Yii::t('app', 'For more information, read our article on how to create MOOCs with the Docebo LMS: <a target="_blank" href="{link_to_article}">{link_to_article}</a>', array(
                '{link_to_article}' => 'https://www.docebo.com/2014/11/26/creating-moocs-docebo-lms/'
	        ))
	        . '</p>'
	        . '<p class="description-with-link">'
	        . Yii::t('app', 'Please also see our MOOC manual: <a target="_blank" href="{link_to_manual}">{link_to_manual}</a>', array(
                '{link_to_manual}' => 'https://www.docebo.com/knowledge-base/create-mooc-docebo/'
	        ))
	        . '</p>'
	        . '<br/>'
	        . '<div class="text-right"><input type="submit" class="ajax btn-docebo blue big text-center" value="' . Yii::t('apps', 'Installnow') . '" /></div>'
	    
	    .CHtml::endForm();
	    
	    return $out;
	                                                                                         
	     
	}
	
	
	/**
	 * Activation of the app
	 */
	public function activate() {

		parent::activate();
	}

	/**
	 * Deactivation of the app
	 */
	public function deactivate() {

		parent::deactivate();
	}

}
