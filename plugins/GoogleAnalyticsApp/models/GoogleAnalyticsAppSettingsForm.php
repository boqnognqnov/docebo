<?php

/**
 * GoogleAnalyticsAppSettingsForm class.
 *
 * @property string $ga_code
 * @property string $enable_ga
 */
class GoogleAnalyticsAppSettingsForm extends CFormModel {
	public $ga_code;
	public $enable_ga;


	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			array('ga_code, enable_ga', 'safe')
		);
	}


	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array(
			'ga_code' => Yii::t('configuration', '_GOOGLE_STAT_CODE'),
			'enable_ga' => Yii::t('configuration','_GOOGLE_STAT_IN_LMS')
		);
	}


	public function beforeValidate() {
		$this->enable_ga = ($this->enable_ga ? '1' : '0');
		return parent::beforeValidate();
	}


}
