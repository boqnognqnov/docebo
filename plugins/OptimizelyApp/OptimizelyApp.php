<?php

class OptimizelyApp extends DoceboPlugin
{
    // Name of the plugin; needed to identify plugin folder and entry script
    public $plugin_name = 'OptimizelyApp';

    /**
     * This will allow us to access "statically" (singleton) to plugin methods
     * @param string $class_name
     * @return Plugin Object
     */
    public static function plugin($class_name=__CLASS__) {
        return parent::plugin($class_name);
    }


    public function activate() {
        Settings::save('optimizely_js_enabled', '1');
        parent::activate();
    }


    public function deactivate() {
        Settings::save('optimizely_js_enabled', '0');
        parent::deactivate();
    }
}