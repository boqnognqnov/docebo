<?php
/* @var $form CActiveForm */
/* @var $settings ZendeskAppSettingsForm */
?>

    <a href="<?= ( 'it' == Yii::app()->getLanguage() ) ? 'https://www.docebo.com/it/knowledge-base/docebo-per-optimizely/'
        : 'https://www.docebo.com/knowledge-base/docebo-for-optimizely/' ?>" target="_blank"
       class="app-link-read-manual"><?= Yii::t( 'apps', 'Read Manual' ); ?></a>

<?php DoceboUI::printFlashMessages(); ?>

	<div class="advanced-main" id="optimizely-settings-form">
		<?php
		$form = $this->beginWidget( 'CActiveForm', array(
			'id'          => 'app-settings-form',
			'htmlOptions' => array( 'class' => 'form-horizontal' ),
		) );
		?>

		<div class="section">

			<h3 class="advanced-catalog-settings-form-title">Optimizely <?= Yii::t( 'standard', 'Settings' ) ?></h3>
			<br/>

			<!--<div class="error-summary">--><?php //echo $form->errorSummary($settings); ?><!--</div>-->

			<div class="row odd">
				<div class="row">
					<div class="setting-name">
						<?= $form->labelEx( $settings, 'enabled' ); ?>
					</div>
					<div class="values">
						<?= $form->checkBox( $settings, 'enabled' ); ?>
						<?= $form->error( $settings, 'enabled' ); ?>
					</div>
				</div>
			</div>

			<div class="row even">
				<div class="row">
					<div class="setting-name">
						<?= $form->labelEx( $settings, 'js_code' ); ?>
					</div>
					<div class="values">
						<?= $form->textArea( $settings, 'js_code', array(
							'class' => 'input-xxlarge',
							'style' => 'height: 250px'
						) ); ?>
						<?= $form->error( $settings, 'js_code' ); ?>
					</div>
				</div>
			</div>

			<div class="form-actions">
				<a href="<?= Docebo::getUserHomePageUrl(); ?>"
				   class="btn-docebo black big"><?= Yii::t( 'standard', '_CANCEL' ); ?></a>
				&nbsp;
				<?= CHtml::submitButton( Yii::t( 'standard', '_SAVE' ), array( 'class' => 'btn-docebo green big' ) ); ?>
			</div>

		</div>

		<?php $this->endWidget(); ?>

	</div>