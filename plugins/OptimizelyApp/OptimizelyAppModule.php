<?php

class OptimizelyAppModule extends CWebModule
{
    private static $excludedPages = array(
        'site/index',
    );

    public function init () {

        Yii::app()->event->on( 'beforeHtmlHeadTagClose', array( $this, 'beforeHeadClose' ) );
    }

    /**
     * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
     */
    public function menu () {

        Yii::app()->mainmenu->addAppMenu('optimizely', array(
            'icon'     => 'admin-ico apps',
            'app_icon' => 'fa-gears', // icon used in the new menu
            'link'     => Docebo::createAdminUrl( '//OptimizelyApp/OptimizelyApp/settings' ),
            'label'    => "Optimizely",
        ), array() );
    }

    public function beforeHeadClose ( DEvent $event ) {

        if( Settings::get( 'optimizely_js_enabled' ) ) {

            $script = Settings::get( 'optimizely_js_code' );

            if( stripos( $script, '<script' ) === FALSE ) {
                echo '<script type="text/javascript">/*<![CDATA[*/' . PHP_EOL;
                echo '/* Optimizely JS code */' . PHP_EOL;
                echo $script;
                echo PHP_EOL . '/*]]>*/</script>';
            } else {
                echo $script;
            }

        }
    }
}