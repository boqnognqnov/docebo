<?php

class OptimizelyAppController extends  Controller
{
    /**
     * @return array action filters
     */
    public function filters () {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }


    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules () {
        $res = array();

        // keep it in the following order:

        // http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
        // Allow following actions to admin only:
        $admin_only_actions = array( 'settings' );
        $res[]              = array(
            'allow',
            'roles'   => array( Yii::app()->user->level_godadmin ),
            'actions' => $admin_only_actions,
        );

        // deny admin only actions to all other users:
        $res[] = array(
            'deny',
            'actions' => $admin_only_actions,
        );

        // Allow access to other actions only to logged-in users:
        $res[] = array(
            'allow',
            'users' => array( '@' ),
        );

        $res[] = array(
            'deny', // deny all
            'users' => array( '*' ),
        );

        return $res;
    }

    public function actionSettings(){
        $settings = new OptimizelyAppSettingsForm();

        $settings->enabled = Settings::get('optimizely_js_enabled');
        $settings->js_code = Settings::get('optimizely_js_code');

        if (isset($_POST['OptimizelyAppSettingsForm'])) {
            $settings->setAttributes($_POST['OptimizelyAppSettingsForm']);

            if ($settings->validate()) {

                Settings::save('optimizely_js_enabled', (bool) $settings->enabled);
                Settings::save('optimizely_js_code', $settings->js_code);

                Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));
            }
            else {
                Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
            }
        }

		Yii::app()->clientScript->registerCss('zendesk-settings-form', '
			#optimizely-settings-form.advanced-main {
				margin-left: 0;
			}
			#optimizely-settings-form .form-actions .btn-docebo{
				float: right;
				margin-left: 20px;
			}
		');
		Yii::app()->clientScript->registerScript('zendesk-settings-form', '
			$("input,select", "#optimizely-settings-form").styler();
		');
        Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/admin.css');

        $this->render('settings', array(
            'settings'=>$settings,
        ));
    }
}