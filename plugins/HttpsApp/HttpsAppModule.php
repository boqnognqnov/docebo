<?php

/**
 * Plugin for HttpsApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class HttpsAppModule extends CWebModule {

	public function init() {

		parent::init();
	}
}
