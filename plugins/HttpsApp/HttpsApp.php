<?php

/**
 * Plugin for HttpsAp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class HttpsApp extends DoceboPlugin {

	/**
	 * @var string Name of the plugin; needed to identify plugin folder and entry script
	 */
	public $plugin_name = 'HttpsApp';

	public static $HAS_SETTINGS = false;

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name = __CLASS__) {

		return parent::plugin($class_name);
	}

	/**
	 * Activation of the app
	 */
	public function activate() {

		parent::activate();
		CoreHttps::getGeneralHttps();
	}

	/**
	 * Deactivation of the app
	 */
	public function deactivate() {

		parent::deactivate();
	}

}
