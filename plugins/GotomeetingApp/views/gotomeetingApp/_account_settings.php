<?php
/* @var $form CActiveForm */
/* @var $settings GotomeetingAppSettingsForm */
?>

<h1><?= $modalTitle ?></h1>
<div class="form edit-webinar-account-dialog">

	<?php $form = $this->beginWidget('CActiveForm',
		array(
			'id' => 'webinar-account-settings-form',
			'htmlOptions' => array(
				'class' => 'ajax webinar-account-settings-form'
			)
		)
	); ?>

	<div class="control-group">
		<?=$form->labelEx($settings, 'account_name', array('class'=>'control-label'));?>
		<div class="controls">
			<?=$form->textField($settings, 'account_name', array('class'=>'input-xlarge'));?>
			<?=$form->error($settings, 'account_name'); ?>
		</div>
	</div>

	<div class="control-group">
		<?=$form->labelEx($settings, 'api_key', array('class'=>'control-label'));?>
		<div class="controls">
			<?=$form->textField($settings, 'api_key', array('class'=>'input-xlarge'));?>
			<?php if (!$isApiKeyValid) { ?>
				<br><label class="label label-important"><?= Yii::t('conference', 'API key reported as invalid. Please acquire a new one!') ?></label>
			<?php } ?>
			<br>
			<?=CHtml::link('https://developer.citrixonline.com/user/register', 'https://developer.citrixonline.com/user/register', array('target'=>'_blank', 'class'=>'app-settings-url'))?>
			<?=$form->error($settings, 'api_key'); ?>
		</div>
	</div>

	<?php if ($acStatus == 'present') : ?>
		<div class="control-group">
			<?=$form->label($settings, 'access_token', array('class'=>'control-label'));?>
			<div class="controls">
				<span class="label label-success lbl-accesstoken-status" title='<?= $accessToken ?>' data-present-label="<?= Yii::t('standard', '_SET') ?>" data-notpresent-label="<?= Yii::t('standard', 'Not set') ?>"><?= Yii::t('standard', '_SET') ?> (<?= $g2mEmail ?>)</span>
				<br>
				<?php if (!empty($settings["api_key"]) && $isApiKeyValid) : ?>
					<a href="<?= $authorizeUrl ?>" class=""><?= Yii::t('configuration', 'Request access to a different GoToMeeting account') ?></a>
				<?php endif;?>
			</div>
		</div>
	<?php else : ?>
		<div class="control-group">
			<?=$form->label($settings, 'access_token', array('class'=>'control-label'));?>
			<div class="controls">
				<?php if ($acStatus == 'expired') { ?>
					<span class="label label-important lbl-accesstoken-status"><?= Yii::t('configuration', 'Expired') ?></span>
				<?php } else if ($acStatus == 'notpresent') { ?>
					<span class="label label-important lbl-accesstoken-status"><?= Yii::t('standard', '_NONE') ?></span>
				<?php } ?>
				<br>
				<?php if (!empty($settings["api_key"]) && $isApiKeyValid) : ?>
					<a href="<?= $authorizeUrl ?>" class=""><?= Yii::t('configuration', 'Request access to GoToMeeting account') ?></a>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>

	<div class="control-group">
		<?=$form->labelEx($settings, 'additional_info', array('class'=>'control-label'));?>
		<div class="controls">
			<?=$form->textArea($settings, 'additional_info', array('class'=>'input-xlarge'));?>
			<?=$form->error($settings, 'additional_info'); ?>
		</div>
	</div>

	<hr>
	<div class="control-group webinar-info-text">
		<?=Yii::t('webinar', 'To allow an unlimited amount of sessions or meetings, enter "0" in the following fields')?>
	</div>

	<div class="control-group">
		<?=$form->labelEx($settings, 'max_rooms_per_course', array('class'=>'control-label'));?>
		<div class="controls">
			<?=$form->textField($settings, 'max_rooms_per_course', array('class'=>'input-xlarge'));?>
			<?=$form->error($settings, 'max_rooms_per_course'); ?>
		</div>
	</div>

	<div class="control-group">
		<?=$form->labelEx($settings, 'max_rooms', array('class'=>'control-label'));?>
		<div class="controls">
			<?=$form->textField($settings, 'max_rooms', array('class'=>'input-xlarge'));?>
			<?=$form->error($settings, 'max_rooms'); ?>
		</div>
	</div>

    <div class="control-group">
        <?=$form->labelEx($settings, 'max_concurrent_rooms', array('class'=>'control-label'));?>
        <div class="controls">
            <?=$form->textField($settings, 'max_concurrent_rooms', array('class'=>'input-xlarge'));?>
            <?=$form->error($settings, 'max_concurrent_rooms'); ?>
        </div>
    </div>

	<div class="form-actions">
		<input class="btn-docebo green big" type="submit" value="<?= Yii::t('standard', '_CONFIRM') ?>" />
		<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
	</div>

	<?php $this->endWidget(); ?>

</div>

<script type="text/javascript">
	$(function() {
		//clean possible delegated events from previous dialogs
		$(document).undelegate('.modal-edit-webinar-account', "dialog2.content-update");

		//set dialog behaviors on server answer
		$(document).delegate(".modal-edit-webinar-account", "dialog2.content-update", function() {
			var e = $(this), autoclose = e.find("a.auto-close");
			if (autoclose.length > 0) {
				e.find('.modal-body').dialog2("close");
				$.fn.yiiListView.update("webinar-accounts-management-list");
			} else {
				var err = e.find("a.error");
				if (err.length > 0) {
					var msg = $(err[0]).data('message');
					Docebo.Feedback.show('error', msg);
					e.find('.modal-body').dialog2("close");
				}
			}
		});
	});
</script>