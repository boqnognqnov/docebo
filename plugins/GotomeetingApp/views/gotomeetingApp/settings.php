<?php
/* @var $form CActiveForm */
/* @var $settings GotomeetingAppSettingsForm */
?>

<?php DoceboUI::printFlashMessages(); ?>

<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'app-settings-form',
	'htmlOptions'=>array('class' => 'form-horizontal docebo-form'),
));

?>

<a href="<?= ('it' == Yii::app()->getLanguage()) ? 'https://www.docebo.com/it/knowledge-base/docebo-per-gotomeeting/' : 'https://www.docebo.com/knowledge-base/elearning-docebo-for-gotomeeting/' ?>" target="_blank" class="app-link-read-manual"><?= Yii::t('apps', 'Read Manual'); ?></a>

<h2><?=Yii::t('standard','Settings') ?>: GoToMeeting</h2>
<br>

<div class="control-container odd">
	<div class="control-group">
		<?=CHtml::label(Yii::t('standard', 'Website'), false, array('class'=>'control-label'));?>
		<div class="controls">
			<a class="app-settings-url" href="http://www.gotomeeting.com/fec/" target="_blank">http://www.gotomeeting.com/fec/</a>
		</div>
	</div>
</div>


<div class="control-container even">
	<div class="control-group">
		<?=CHtml::label(Yii::t('webinar', 'Accounts'), false, array('class'=>'control-label'));?>
		<div class="controls">
			<?php
			$this->widget('common.widgets.WebinarAccountsEditor', array(
				'dataProvider' => WebinarToolAccount::model()->dataProvider('gotomeeting'),
				'editAccountUrl' => $this->createUrl('editAccount'),
				'enableMultiAccount' => WebinarTool::getById('gotomeeting')->hasMultiAccountSupport()
			));
			?>
		</div>
	</div>
</div>

<?php $this->endWidget(); ?>