<?php
/**
 * RESTfull API client for GoToMeeting (www.gotomeeting.com)/ GoToTraining (www.gototraining.com)
 *
 * All methods are throwable. Use them in try/catch blocks.
 *
 * All methods return an array like:
 *  array(
 *      "response_status" => <Api return status>,   // 200, 400, 401, etc.
 *      "response_message" => <Api message>,        // Ok, Created, Bad Status, etc.
 *      "data" => array(<spcific for all methods>)
 *      )
 *
 * @author Plamen Petkov
 *
 * Copyright (c) 2013 Docebo
 * http://www.docebo.com
 *
 *
 */
class GotomeetingApiClient extends CComponent {

    // Some constants
    const MEETINGTYPE_SCHEDULED = 'Scheduled';
    const MEETINGTYPE_IMMEDIATE = 'Immediate';
    const MEETINGTYPE_RECURRING = 'Recurring';

    // Can be used to debug the traffic
    public $proxyHost = false;
    public $proxyPort = false;


    // will hold the EHttpClient
    private $_client = null;

    // MUST BE HTTPS: !!
    private $_baseDomain = 'https://api.citrixonline.com';

    // Will be taken from settings
    private $_apiKey = '';
    private $_accessToken = '';
    private $_organizerKey = '';

    /**
     *
     */
    public function __construct($api_key = null, $access_token = null, $accesstoken_organizer_key = null) {
		// If the new parameters are not passed, try to load the default webinar tools account
		if(!$api_key && !$access_token) {
			$account = WebinarToolAccount::model()->findByAttributes(array('tool' => 'gotomeeting'));
			if($account) {
				$api_key = $account->settings->api_key;
				$access_token = $account->settings->access_token;
                $accesstoken_organizer_key = $account->settings->accesstoken_organizer_key;
			}
		}
        $this->init($api_key, $access_token, $accesstoken_organizer_key);
    }


    /**
     *
     * @throws Exception
     */
    public function init($api_key = null, $access_token = null, $accesstoken_organizer_key = null) {

        Yii::import('common.extensions.httpclient.*');

        $this->_apiKey = $api_key;
        if (empty($this->_apiKey)) {
            throw new Exception("API Key not set");
        }
        $this->_accessToken = $access_token;
        $this->_organizerKey = $accesstoken_organizer_key;

        // For testing with Fiddler2
        //$this->proxyHost = '127.0.0.1';
        //$this->proxyPort = 8888;

    }

    /**
     * Check if the account used is for GoToTraining
     *
     */
    private function checkIfGoToTraining(){
        //$this->isGoToTraining = true;
        try {
            $response = $this->call('organizers/' . $this->_organizerKey . '/trainings', EHttpClient::GET, $this->_accessToken,null,'g2t');
            if($response["response_status"] == '200'){
                //$this->isGoToTraining = true;
                return true;
            }
            else{
                //$this->isGoToTraining = false;
                return false;
            }
        }catch(exception $e){
            //$this->isGoToTraining = false;
            return false;
        }
    }


    /**
     * Make an API call
     *
     * @param string $actionPath
     * @param string $method
     * @param string $accessToken
     * @param array $params
     * @param string $apiType
     * @throws Exception
     * @return array
     */
    private function call($actionPath, $method, $accessToken, $params=null, $apiType='g2m') {
        //API specific REST path
        switch($apiType){
            case 'g2t':
                $restPath = '/G2T/rest/';
                break;
            case 'g2w':
                $restPath = '/G2W/rest/';
                break;
            case 'g2m':
            default:
                $restPath = '/G2M/rest/';
        }
        // Build URL
        $url = $this->_baseDomain . $restPath . $actionPath;
        $this->_client = new EHttpClient($url, array(
                'adapter'      => 'EHttpClientAdapterCurl',
                'proxy_host'   => $this->proxyHost,
                'proxy_port'   => $this->proxyPort,
                'disable_ssl_verifier' => true,
        ));

        // Add Content length header
        $json_data = "";
        if (is_array($params)) {
			if(isset($params['description']) && strlen($params['description']) > 2048 && $apiType == 'g2t')
				$params['description'] = substr($params['description'], 0, 2048);
            if(empty($params['description'])){
                $params['description'] = $params['name'];
            }
            $json_data = CJSON::encode($params);
            $this->_client->setHeaders('Content-Length', strlen($json_data));
        }

        // Add other headers
        $this->_client->setHeaders('Accept', "application/json");
        $this->_client->setHeaders('Content-Type', "application/json");

        // Add auth
        $this->_client->setHeaders('Authorization', "OAuth oauth_token=" . $accessToken);

        // Add raw data, if any
        if (!empty($json_data)) {
            $this->_client->setRawData($json_data);
        }

        // Make the call, get the response
        $response = $this->_client->request($method);

	    if($response){
		    Yii::log('Sending params to Gotomeeting API: '.print_r($params,true), 'debug');
            // NOTE: this line causes php error and skips reload of the My coach widgets, like this:
            // non-utf8 string: ....
            // Maybe it is due lack of full UTF8 support in php - to be fixed
		    //Yii::log('Full response debug: '.$response->asString(), 'debug');
	    }

        // Check if we've got an error; throw exception with error message
        if ($this->_client->getLastResponse()->isError()) {
            throw new Exception($this->_client->getLastResponse()->getMessage(), $this->_client->getLastResponse()->getStatus());
        }

        if (!$response) {
            throw new Exception('Invalid response from server');
        }

        // Decode response to an array
        $body = $response->getBody();

        // If we get body, it must be JSON and be an array
        $response_data = array();
        if ($body) {
            $response_data = CJSON::decode($body);
            if ( ($response_data === null) || ( !is_array($response_data)  && $restPath != '/G2T/rest/' ) ) {
                throw new Exception('Invalid data format received from server');
            }
        }

        // Always add response status/message
        $result["response_status"] = $this->_client->getLastResponse()->getStatus();
        $result["response_message"] = $this->_client->getLastResponse()->getMessage();
        $result["data"] = $response_data;

        // Return array
        return $result;

    }

    /**
     * Create a meeting
     *
     * @param satring $meetingName
     * @param satring $startDateTime
     * @param satring $endDateTime
     * @param satring $meetingType
     * @return attay
     */
    public function createMeeting($meetingName, $startDateTime, $endDateTime, $meetingType = self::MEETINGTYPE_SCHEDULED) {

        $startTime = strtotime($startDateTime);
        $endTime = strtotime($endDateTime);

        $startDateTime = gmdate('c', $startTime);
        $endDateTime =   gmdate('c', $endTime);

        $params = array(
                'subject' => $meetingName,
                'starttime' => $startDateTime,
                'endtime' => $endDateTime,
                'passwordrequired' => 'false',
                'conferencecallinfo' => 'Hybrid',
                'timezonekey' => '',
                'meetingtype' => $meetingType,
        );

        $result = $this->call('meetings', EHttpClient::POST, $this->_accessToken,  $params);

        return $result;


    }

    /**
     * Create a training
     *
     * @param string $trainingName
     * @param string $roomDescription
     * @param string $startDateTime
     * @param string $endDateTime
     * @return array
     */
    public function createTraining($trainingName, $roomDescription, $startDateTime, $endDateTime) {
        $startTime = strtotime($startDateTime);
        $endTime = strtotime($endDateTime);

        $startDateTime = gmdate('c', $startTime);
        $endDateTime =   gmdate('c', $endTime);

        $params = array(
            'name' => $trainingName,
            'description' => $roomDescription,
            'timeZone' => '',
            'times' => array(array(
                'startDate' => $startDateTime,
                'endDate' => $endDateTime
            )
            ),

            'registrationSettings' => array (
                'disableConfirmationEmail' => true,
                'disableWebRegistration' => false
            ),

            "organizers" => array($this->_organizerKey)
        );


        $result = $this->call('organizers/'.$this->_organizerKey.'/trainings', EHttpClient::POST, $this->_accessToken,  $params, 'g2t');
        $joinUrlResponse = $this->getStartTrainingUrl($result['data']);
        $result['join_url'] = $joinUrlResponse['data'];
        return $result;


    }

    /**
     * Create a training
     *
     * @param string $webinarName
     * @param string $roomDescription
     * @param string $startDateTime
     * @param string $endDateTime
     * @return array
     */
    public function createWebinar($webinarName, $roomDescription, $startDateTime, $endDateTime) {
        $startTime = strtotime($startDateTime);
        $endTime = strtotime($endDateTime);

        $startDateTime = gmdate('c', $startTime);
        $endDateTime =   gmdate('c', $endTime);

        $userTimezone = Yii::app()->user->timezone;
        $translatedTimezone = CoreTimezoneTranslate::translateTimezone($userTimezone, 'citrix');
        if(!$translatedTimezone) $translatedTimezone = '';

        $params = array(
            'subject' => $webinarName,
            'description' => $roomDescription,
            'timeZone' => $translatedTimezone,
            'times' => array(
                array(
                    'startTime' => $startDateTime,
                    'endTime' => $endDateTime
                )
            )
        );

        $result = $this->call('organizers/'.$this->_organizerKey.'/webinars', EHttpClient::POST, $this->_accessToken,  $params, 'g2w');
        return $result;
    }



    /**
     * Update a meeting
     *
     * @param string $meetingId
     * @param string $meetingName
     * @param string $startDateTime
     * @param string $endDateTime
     * @param string $meetingType
     * @throws Exception
     * @return array
     */
    public function updateMeeting($meetingId, $meetingName, $startDateTime, $endDateTime, $meetingType = self::MEETINGTYPE_SCHEDULED) {

        $meetingId = trim($meetingId);
        if (empty($meetingId)) {
            throw new Exception('Invalid meeting Id provided');
        }


        $startTime = strtotime($startDateTime);
        $endTime = strtotime($endDateTime);

        $startDateTime = gmdate('c', $startTime);
        $endDateTime = gmdate('c', $endTime);

        $params = array(
                'subject' => $meetingName,
                'starttime' => $startDateTime,
                'endtime' => $endDateTime,
                'passwordrequired' => 'false',
                'conferencecallinfo' => 'Hybrid',
                'timezonekey' => '',
                'meetingtype' => $meetingType,
        );

        $result = $this->call('meetings/' . $meetingId, EHttpClient::PUT, $this->_accessToken,  $params);

        return $result;


    }

    /**
     * Update a training
     *
     * @param string $trainingId
     * @param string $trainingName
     * @param string $trainingDescription
     * @param string $startDateTime
     * @param string $endDateTime
     * @throws Exception
     * @return array
     */
    public function updateTraining($trainingId, $trainingName, $trainingDescription, $startDateTime, $endDateTime, $updateTimes = true) {
        $trainingId = trim($trainingId);
        if (empty($trainingId))
            throw new Exception('Invalid training Id provided');

        $startDateTime = str_replace("+0000", "z", $startDateTime);
        $endDateTime = str_replace("+0000", "z", $endDateTime);

        $paramsNameDescr = array(
            'name' => $trainingName,
            'description' => $trainingDescription,
        );
        $paramsTimes = array(
            'timeZone' => '',
            'times' => array(array(
                'startDate' => $startDateTime,
                'endDate' => $endDateTime
            )
            ),
            "notifyRegistrants" => true,
            "notifyTrainers" => true
        );

        $result = $this->call('organizers/'.$this->_organizerKey.'/trainings/' . $trainingId . '/nameDescription', EHttpClient::PUT, $this->_accessToken,  $paramsNameDescr,'g2t');
	    if($updateTimes){
		    $result = $this->call('organizers/'.$this->_organizerKey.'/trainings/' . $trainingId . '/times', EHttpClient::PUT, $this->_accessToken,  $paramsTimes,'g2t');
	    }
        return $result;
    }

    /**
     * Update a webinar
     *
     * @param string $webinarId
     * @param string $webinarName
     * @param string $webinarDescription
     * @param string $startDateTime
     * @param string $endDateTime
     * @throws Exception
     * @return array
     */
    public function updateWebinar($webinarId, $webinarName, $webinarDescription, $startDateTime, $endDateTime){
        $webinarId = trim($webinarId);
        if (empty($webinarId))
            throw new Exception('Invalid webinar Id provided');

        $startDateTime = str_replace("+0000", "z", $startDateTime);
        $endDateTime = str_replace("+0000", "z", $endDateTime);

        $userTimezone = Yii::app()->user->timezone;
        $translatedTimezone = CoreTimezoneTranslate::translateTimezone($userTimezone, 'citrix');
        if(!$translatedTimezone) $translatedTimezone = '';

        $params = array(
            'subject' => $webinarName,
            'description' => $webinarDescription,
            'timeZone' => $translatedTimezone,
            'times' => array(
                array(
                    'startTime' => $startDateTime,
                    'endTime' => $endDateTime
                )
            )
        );

        $result = $this->call('organizers/'.$this->_organizerKey.'/webinars/' . $webinarId, EHttpClient::PUT, $this->_accessToken,  $params,'g2w');
        return $result;
    }

    /**
     * Returns list of meetings
     *
     * @return array
     */
    public function getMeetings() {
        $result = $this->call('meetings', EHttpClient::GET, $this->_accessToken);
        return $result;
    }

    /**
     * Returns list of trainings
     *
     * @return array
     */
    public function getTrainings() {
        $result = $this->call('organizers/' . $this->_organizerKey . '/trainings', EHttpClient::GET, $this->_accessToken,'g2t');
        return $result;
    }


    /**
     * Returns information about a given meeting
     *
     * @param string $meetingId
     * @throws Exception
     * @return array
     */
    public function getMeetingById($meetingId) {
        $meetingId = trim($meetingId);
        if (empty($meetingId)) {
            throw new Exception('Invalid meeting Id provided');
        }
        $result = $this->call('meetings/' . $meetingId, EHttpClient::GET, $this->_accessToken);

        return $result;
    }

    /**
     * Returns information about a given training
     *
     * @param string $trainingId
     * @throws Exception
     * @return array
     */
    public function getTrainingById($trainingId) {
        $trainingId = trim($trainingId);
        if (empty($trainingId)) {
            throw new Exception('Invalid meeting Id provided');
        }
        $result = $this->call('organizers/' . $this->_organizerKey . '/trainings/' . $trainingId, EHttpClient::GET, $this->_accessToken,null,'g2t');

        return $result;
    }



    /**
     * Deletes a meeting by ID
     *
     * @param string $meetingId
     * @throws Exception
     * @return array
     */
    public function deleteMeeting($meetingId) {
        $meetingId = trim($meetingId);
        if (empty($meetingId)) {
            throw new Exception('Invalid meeting Id provided');
        }
		try {
        $result = $this->call('meetings/' . $meetingId, EHttpClient::DELETE, $this->_accessToken);
		} catch(Exception $e) {
			if($e->getCode() == 404) { //already deleted
				Yii::log('GoToMeeting API error (meetingId = '.$meetingId.'): '.$e->getMessage());
				return true;
			} else
				throw new Exception($e->getMessage(), $e->getCode());
		}

        return $result;
    }

    /**
     * Deletes a training by ID
     *
     * @param string $trainingId
     * @throws Exception
     * @return array
     */
    public function deleteTraining($trainingId) {
        $trainingId = trim($trainingId);
        if (empty($trainingId)) {
            throw new Exception('Invalid meeting Id provided');
        }
		try {
        $result = $this->call('organizers/'.$this->_organizerKey.'/trainings/' . $trainingId, EHttpClient::DELETE, $this->_accessToken,null,'g2t');
		} catch(Exception $e) {
			if($e->getCode() == 404) { //already deleted
				Yii::log('GoToTraining API error (organizerKey = '.$this->_organizerKey.', trainingKey = '.$trainingId.'): '.$e->getMessage());
				return true;
			}else
				throw new Exception($e->getMessage(), $e->getCode());
		}
        return $result;
    }

    /**
     * Deletes a webinar by ID
     *
     * @param string $webinarId
     * @throws Exception
     * @return array
     */
    public function deleteWebinar($webinarId) {
        $webinarId = trim($webinarId);
        if (empty($webinarId)) {
            throw new Exception('Invalid meeting Id provided');
        }
        try {
            $result = $this->call('organizers/'.$this->_organizerKey.'/webinars/' . $webinarId, EHttpClient::DELETE, $this->_accessToken,null,'g2w');
        } catch(Exception $e) {
            if($e->getCode() == 404) { //already deleted
                Yii::log('GoToWebinar API error (organizerKey = '.$this->_organizerKey.', webinarKey = '.$webinarId.'): '.$e->getMessage());
                return true;
            }else
                throw new Exception($e->getMessage(), $e->getCode());
        }
        return $result;
    }

    /**
     * Returns a "host url", which will start a meeting and join the user as Moderator (Organizer)
     *
     * @param string $meetingId
     * @throws Exception
     * @return array
     */
    public function getStartMeetingUrl($meetingId) {
        $meetingId = trim($meetingId);
        if (empty($meetingId)) {
            throw new Exception('Invalid meeting Id provided');
        }
        $result = $this->call('meetings/' . $meetingId . '/start', EHttpClient::GET, $this->_accessToken);

        return $result;
    }

    /**
     * Returns a "host url", which will start a training and join the user as Moderator (Organizer)
     *
     * @param string $trainingId
     * @throws Exception
     * @return array
     */
    public function getStartTrainingUrl($trainingId) {
        $trainingId = trim($trainingId);
        if (empty($trainingId)) {
            throw new Exception('Invalid training Id provided');
        }
		try {
       		$result = $this->call('trainings/' . $trainingId . '/start', EHttpClient::GET, $this->_accessToken,null,'g2t');
		} catch(Exception $e) {
			if($e->getCode() == 409)
        $result = $this->call('organizers/'.$this->_organizerKey.'/trainings/' . $trainingId . '/startUrl', EHttpClient::GET, $this->_accessToken,null,'g2t');
		}
        return $result;
    }

    /**
     * Returns the "join url" for the passed user. The API first register the user to the training.
     * @param $trainingId
     * @param $email
     * @param $firstname
     * @param $lastname
     */
    public function getJoinTrainingUrl($trainingId, $email, $firstname, $lastname) {
        try {
            $trainingId = trim($trainingId);
            if (empty($trainingId))
                throw new Exception('Invalid training Id provided');

            // Check if the email is already registered to the training
            $result = $this->call('organizers/' . $this->_organizerKey . '/trainings/' . $trainingId . '/registrants', EHttpClient::GET, $this->_accessToken, null, 'g2t');
            foreach ($result['data'] as $registrant) {
                if ($registrant['email'] == $email)
                    return $registrant['joinUrl'];
            }

            // New registrant
            $params = array(
                'email' => $email,
                'givenName' => $firstname ? $firstname : 'Firstname',
                'surname' => $lastname ? $lastname : 'Lastname'
            );
            $result = $this->call('organizers/' . $this->_organizerKey . '/trainings/' . $trainingId . '/registrants', EHttpClient::POST, $this->_accessToken, $params, 'g2t');
            return $result['data']['joinUrl'];
        } catch(Exception $e) {
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
            return null;
        }
    }

    /**
     * Returns the "join url" for the passed user. The API first register the user to the training.
     * @param $trainingId
     * @param $email
     * @param $firstname
     * @param $lastname
     */
    public function getJoinWebinarUrl($webinarId, $email, $firstname, $lastname) {
        try {
            $webinarId = trim($webinarId);
            if (empty($webinarId))
                throw new Exception('Invalid webinar Id provided');

            // Check if the email is already registered to the training
            $result = $this->call('organizers/' . $this->_organizerKey . '/webinars/' . $webinarId . '/registrants', EHttpClient::GET, $this->_accessToken, null, 'g2w');
            foreach ($result['data'] as $registrant) {
                if ($registrant['email'] == $email)
                    return $registrant['joinUrl'];
            }

            // New registrant
            $params = array(
                'email' => $email,
                'firstName' => $firstname ? $firstname : 'Firstname',
                'lastName' => $lastname ? $lastname : 'Lastname'
            );
            $result = $this->call('organizers/' . $this->_organizerKey . '/webinars/' . $webinarId . '/registrants', EHttpClient::POST, $this->_accessToken, $params, 'g2w');
            return $result['data']['joinUrl'];
        } catch(Exception $e) {
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
            return null;
        }
    }

    public function getTrainingRecording($trainingId){
        $errors = array();
        try{
            $trainingId = trim($trainingId);
            if(empty($trainingId)){
                throw new Exception('Invalid training Id provided');
            }
            $result = $this->call('trainings/'.$trainingId.'/recordings', EHttpClient::GET, $this->_accessToken, null, 'g2t');
            return $result;
        } catch(Exception $e) {
            $errors[] = $e->getMessage();
            return $errors;
        }
    }


    /**
     * Acquires an access token from Gotomeeting/Gototraining OAuth server
     *
     * @param string $responseKey
     * @throws Exception
     * @return array
     */
    public function getAccessToken($responseKey) {
        $url = $this->_baseDomain . "/oauth/access_token?grant_type=authorization_code&code=$responseKey&client_id=" . $this->_apiKey;

        $this->_client = new EHttpClient($url, array(
                'adapter'      => 'EHttpClientAdapterCurl',
                'proxy_host'   => $this->proxyHost,
                'proxy_port'   => $this->proxyPort,
                'disable_ssl_verifier' => true,
        ));

        $response = $this->_client->request(EHttpClient::GET);

        // Check if we've got an error; throw exception with error message
        if ($this->_client->getLastResponse()->isError()) {
            throw new Exception($this->_client->getLastResponse()->getMessage(), $this->_client->getLastResponse()->getStatus());
        }


        if (!$response) {
            throw new Exception('Invalid response from GoToMeeting/GoToTraining OAuth server');
        }

        // Decode response to an array
        $body = $response->getBody();

        // If we get body, it must be JSON and be an array
        $response_data = array();
        if ($body) {
            $response_data = CJSON::decode($body);
            if ( ($response_data == null) || !is_array($response_data) ) {
                throw new Exception('Invalid data format received from OAuth server');
            }
        }

        // Always add response status/message
        $result["response_status"] = $this->_client->getLastResponse()->getStatus();
        $result["response_message"] = $this->_client->getLastResponse()->getMessage();
        $result["data"] = $response_data;

        return $result;


    }


    /**
     * Workaround method to check if given API key is valid (accepted) by G2M/G2T server
     *
     * @param string $apiKey
     * @throws Exception
     * @return boolean
     */
    public function checkApiKey($apiKey) {
        //$url = 'https://api.citrixonline.com/oauth/authorize/?client_id='  . $apiKey;
        $url = 'https://api.citrixonline.com/oauth/authorize?client_id='  . $apiKey;

        $this->_client = new EHttpClient($url, array(
                'adapter'      => 'EHttpClientAdapterCurl',
                'maxredirects' => 1,    // VERY IMPORTANT for this method!
                'proxy_host'   => $this->proxyHost,
                'proxy_port'   => $this->proxyPort,
                'disable_ssl_verifier' => true,
        ));

        $response = $this->_client->request(EHttpClient::GET);

        // Check if we've got an error; throw exception with error message
        // ANY error means Invalid API key!  Otherwise G2M redirects to the authorization page
        // and no reposnse is returned.
        if ($this->_client->getLastResponse()->isError()) {
            throw new Exception($this->_client->getLastResponse()->getMessage(), $this->_client->getLastResponse()->getStatus());
        }

        return true;
    }



}

?>