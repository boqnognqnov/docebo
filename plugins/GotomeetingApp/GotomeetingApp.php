<?php

/**
 * Plugin for GotomeetingApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class GotomeetingApp extends DoceboPlugin {


	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'GotomeetingApp';

	/**
	 * Returns the settings page (admin app)
	 */
	public static function settingsUrl() {
		return Docebo::createAdminUrl('GotomeetingApp/gotomeetingApp/settings');
	}

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {
		parent::activate();
		$sql = "DROP TABLE IF EXISTS `conference_gotomeeting_meeting`";
		$command = Yii::app()->db->createCommand($sql);
		$command->execute();

		$sql = "CREATE TABLE IF NOT EXISTS `conference_gotomeeting_meeting` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `id_room` int(11) NOT NULL COMMENT 'Id of the LMS room (local)',
		  `name` varchar(255) NOT NULL,
		  `meeting_id` varchar(64) NOT NULL COMMENT 'The ID of the meeting at Gotomeeting server',
		  `unique_meeting_id` varchar(64) NOT NULL COMMENT 'Another ID returned from G2M on creation',
		  `join_url` varchar(512) NOT NULL,
		  `conference_call_info` text NOT NULL,
		  `logout_url` varchar(512) DEFAULT '',
		  `max_participants` int(11) DEFAULT '-1',
		  PRIMARY KEY (`id`),
		  KEY `id_room` (`id_room`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8";
		$command = Yii::app()->db->createCommand($sql);
		$command->execute();
	}


	public function deactivate() {
		parent::deactivate();
	}


}
