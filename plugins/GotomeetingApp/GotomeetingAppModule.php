<?php

class GotomeetingAppModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'GotomeetingApp.models.*',
			'GotomeetingApp.components.*',
		));

		Yii::app()->event->on('WebinarCollectTools', array($this, 'installWebinarTool'));
	}

	/**
	 * Installs the webinar tool for GoToMeeting
	 * @param $event DEvent
	 */
	public function installWebinarTool($event) {
		$event->return_value['tools']['gotomeeting'] = 'GotomeetingWebinarTool';
		$event->return_value['tools']['gototraining'] = 'GototrainingWebinarTool';
		$event->return_value['tools']['gotowebinar'] = 'GotowebinarWebinarTool';
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}

	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu() {
		//compose the entire menu
		Yii::app()->mainmenu->addAppMenu('gotomeeting', array(
			'app_initials' => 'GM',
			'settings' => Docebo::createAdminUrl('GotomeetingApp/gotomeetingApp/settings'),
			'label' => 'Gotomeeting',
		),
			array()
		);
	}
}
