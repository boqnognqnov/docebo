<?php

class GotomeetingAppController extends Controller {


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		$admin_only_actions = array('settings', 'editAccount', 'afterOauthAuthorized');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}


	/**
	 * Renders the edit webinar account form for big blue button
	 */
	public function actionEditAccount($id_account = null) {
		// Load the webinar account
		if(!$id_account) {
			$account = new WebinarToolAccount();
			$account->tool = 'gotomeeting';
			$account->settings = new stdClass();
		} else
			$account = WebinarToolAccount::model()->findByPk($id_account);

		// Load the settings form
		$accountSettings = $account->settings;
		$settings = new GotomeetingAppSettingsForm();
		if (isset($_POST['GotomeetingAppSettingsForm'])) {
			$settings->setAttributes($_POST['GotomeetingAppSettingsForm']);
			if ($settings->validate()) {

				// Save the settings in the webinar account
				$accountSettings->api_key = $settings->api_key;
				$accountSettings->max_rooms = $settings->max_rooms;
				$accountSettings->max_rooms_per_course = $settings->max_rooms_per_course;
                $accountSettings->max_concurrent_rooms = $settings->max_concurrent_rooms;
				$account->name = $settings->account_name;
				$account->additional_info = $settings->additional_info;
				$account->settings = $accountSettings;
				$account->save();

				// Close the modal
				echo '<a class="auto-close"></a>';
				Yii::app()->end();
			}
		}

		// Load account settings in form
		$settings->account_name = $account->name;
		$settings->api_key = $accountSettings->api_key;
		$settings->access_token = $accountSettings->access_token;
        $settings->accesstoken_organizer_key = $accountSettings->accesstoken_organizer_key;
		$settings->max_rooms = $accountSettings->max_rooms;
		if (empty($settings->max_rooms) && !$id_account)
			$settings->max_rooms = intval(Yii::app()->params['videoconf_max_rooms_default']);
		$settings->max_rooms_per_course = $accountSettings->max_rooms_per_course;
		if (empty($settings->max_rooms_per_course) && !$id_account)
			$settings->max_rooms_per_course = intval(Yii::app()->params['videoconf_max_rooms_per_course_default']);
        $settings->max_concurrent_rooms = $accountSettings->max_concurrent_rooms;
        if (empty($settings->max_concurrent_rooms) && !$id_account)
            $settings->max_concurrent_rooms = intval(Yii::app()->params['videoconf_max_concurrent_rooms_default']);
		$settings->additional_info = $account->additional_info;


		// Gotomeeting OAuth related
		$acStatus = "present";
		$g2mEmail = 'not available';
		if (!$settings->access_token || empty($settings->access_token))
			$acStatus = "notpresent";
		else  {
			$expireTime = $accountSettings->accesstoken_expire_time ? $accountSettings->accesstoken_expire_time : (time()-1);
			if($accountSettings->accesstoken_email)
				$g2mEmail = $accountSettings->accesstoken_email;
			if (time() > $expireTime)
				$acStatus = "expired";
		}

		$isApiKeyValid = false;
		try {
			$apiClient = new GotomeetingApiClient($settings->api_key, $settings->access_token,$settings->accesstoken_organizer_key);
			$isApiKeyValid = $apiClient->checkApiKey($settings->api_key);
		}
		catch (Exception $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
		}

		$returnUri = $this->createAbsoluteUrl("AfterOauthAuthorized", array('id_account' => $account->id_account));
		$authorizeUrl = 'https://api.citrixonline.com/oauth/authorize?client_id='  . $settings->api_key .  '&redirect_uri=' . urlencode($returnUri);

		// Render the edit account view in the modal
		$this->renderPartial('_account_settings', array(
			'modalTitle' => $account->id_account ? Yii::t('webinar', 'Edit webinar account') : Yii::t('webinar', 'New webinar account'),
			'settings' => $settings,
			'authorizeUrl' => $authorizeUrl,
			'acStatus' => $acStatus,
			'g2mEmail' => $g2mEmail,
			'accessToken' => $settings->access_token,
			'isApiKeyValid' => $isApiKeyValid
		));
	}

	/**
	 * Renders the main adobe settings page
	 */
	public function actionSettings() {
		Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl . '/css/webinar-setup.css');

		$settings = new GotomeetingAppSettingsForm();
		$this->render('settings', array('settings' => $settings));
	}
    
    /**
     * Redirected here from GoToMeeting OAuth server after successful authorization, 
     * delivering ResponseKey, used to retrieve access token. 
     * Access token is saved in webinar account.
     */
    public function actionAfterOauthAuthorized() {
        try {
			// Load the webinar account
			$id_account = Yii::app()->request->getParam('id_account', false);
			$account = WebinarToolAccount::model()->findByPk($id_account);
			if(!$account)
				throw new CHttpException(404, 'Missing required parameter "id_account"');

			$responseKey = Yii::app()->request->getParam('code', false);
			if (!$responseKey)
				throw new CHttpException(404, Yii::t('conference', 'No response key received from OAuth server'));

            // API-Call OAuth server to get the access token
			$accountSettings = $account->settings;
			$apiClient = new GotomeetingApiClient($accountSettings->api_key, $accountSettings->access_token,$accountSettings->accesstoken_organizer_key);
            $result = $apiClient->getAccessToken($responseKey);
            if ($result["response_status"] != 200)
                throw new CHttpException(500, Yii::t('conference', 'Bad status received from OAuth server during access request'));

            $expireTime = time() + $result["data"]["expires_in"];
			$accountSettings->access_token = $result["data"]["access_token"];
			$accountSettings->accesstoken_expire_time = $expireTime;
			$accountSettings->accesstoken_refresh_token = $result["data"]["refresh_token"];
			$accountSettings->accesstoken_organizer_key = $result["data"]["organizer_key"];
			$accountSettings->accesstoken_email = $result["data"]["email"];
			$account->settings = $accountSettings;
			$account->save();
        }
        catch (Exception $e) {
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
        }

		$this->redirect($this->createAbsoluteUrl("settings", array('id_account' => $id_account)));
    }
}
