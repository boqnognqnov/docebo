<?php

/**
 * Class GotomeetingWebinarTool
 * Implemenents webinar tool fo gotomeeting
 */
class GotomeetingWebinarTool extends WebinarTool {

	/**
	 * Main constructor
	 */
	protected function __construct() {
		$this->id = 'gotomeeting';
		$this->name = 'Gotomeeting';
	}

	/**
	 * Returns true if the current webinar tools has support for multi account
	 * @return bool
	 */
	public function hasMultiAccountSupport() {
		return true;
	}

	/**
	 * Creates a new room using the Gotomeeting api
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param string $roomDescription The description of the room
	 * @param integer $idAccount The id of the account to use to create the room
	 * @param $startDateTime string The start date&time in UTC + MYSQL/ISO format
	 * @param $endDateTime string The end date&time in UTC + MYSQL/ISO format
	 * @param $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 *
	 * @return array "tool specific" params returned by the api
	 */
	public function createRoom($roomName, $roomDescription, $idAccount, $startDateTime, $endDateTime, $maxParticipants, $api_params = array()) {
		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new GotomeetingApiClient($accountSettings->api_key, $accountSettings->access_token);
		$meeting = $apiClient->createMeeting($roomName, $startDateTime, $endDateTime, GotomeetingApiClient::MEETINGTYPE_SCHEDULED);

		if (!isset($meeting["data"]) || (!is_array($meeting["data"])))
			throw new CHttpException(500, ConferenceManager::$MSG_INVALID_DATA_RETURNED);

		$data = $meeting["data"][0];
		$result = array(
			"name" => $roomName,
			"meeting_id" => $data["meetingid"],
			"unique_meeting_id" => $data["uniqueMeetingId"],
			"join_url" => $data["joinURL"],
			"conference_call_info" => $data["conferenceCallInfo"],
			"max_participants" => $data["maxParticipants"],
		);

		return $result;
	}

	/**
	 * Updates an existing room with the provided params
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param string $roomDescription The description of the room
	 * @param integer $idAccount The id of the account to use to update the room
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @param $startDateTime string The start date&time in UTC + MYSQL/ISO format
	 * @param $endDateTime string The end date&time in UTC + MYSQL/ISO format
	 * @param $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 *
	 * @return array "tool specific" params returned by the api
	 */
	public function updateRoom($roomName, $roomDescription, $idAccount, $api_params = array(), $startDateTime, $endDateTime, $maxParticipants) {
		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new GotomeetingApiClient($accountSettings->api_key, $accountSettings->access_token);
		$apiClient->updateMeeting($api_params['meeting_id'], $roomName, $startDateTime, $endDateTime);
		$api_params['name'] = $roomName;
		return $api_params;
	}

	/**
	 * Deletes an existing room
	 *
	 * @param integer $idAccount The id of the account to use to delete the room
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 */
	public function deleteRoom($idAccount, $api_params = array()) {
		$meetingId = $api_params['meeting_id'];

		$account = $this->getAccountById($idAccount, true);
		if($account !== false)
		{
			$accountSettings = $account->settings;
			$apiClient = new GotomeetingApiClient($accountSettings->api_key, $accountSettings->access_token);
			$apiClient->deleteMeeting($meetingId);
		}
	}

	/**
	 * Returns the URL to join a room
	 *
	 * @param string         $roomName         The name of the room to create in the videconference platform
	 * @param integer        $maxParticipants  integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param integer        $idAccount        The id of the account to use
	 * @param array          $api_params       An array of "tool specific" params to pass to the api
	 * @param CoreUser       $userModel        The id of the user for whom the current URL is returned
	 * @param WebinarSession $sessionDateModel The current session object
	 */
	public function getJoinRoomUrl($roomName, $maxParticipants, $idAccount, $api_params, $userModel, $sessionDateModel, $widgetContext = false) {
		return $api_params['join_url'];
	}

	/**
	 * Returns the URL to start a room
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param integer $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param integer $idAccount The id of the account to use
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @param CoreUser $userModel The id of the user for whom the current URL is returned
	 * @param WebinarSession $sessionModel The current session object
	 */
	public function getStartRoomUrl($roomName, $maxParticipants, $idAccount, $api_params, $userModel, $sessionModel) {
		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new GotomeetingApiClient($accountSettings->api_key, $accountSettings->access_token);
		$result = $apiClient->getStartMeetingUrl($api_params['meeting_id']);
		return $result["data"]["hostURL"];
	}


	/**
	 * Check if the user is valid for current tool
	 * @param $user int|CoreUser user IDST
	 * @return array validation result
	 */
	public function validateUser($user) {
		$userModel = false;
		if (is_numeric($user) && (int)$user > 0) {
			$userModel = CoreUser::model()->findByPk((int)$user);
		} elseif (is_object($user) && get_class($user) == 'CoreUser') {
			$userModel = $user;
		}
		if (empty($userModel)) {
			return array(
				'valid' => false,
				'message' => Yii::t('standard', 'Invalid user')
			);
		}
		// NOTE: GoToMeeting conferences require a valid email in order to be able to join users
		if (empty($user->email)) {
			$message = '<span class="bold">'.Yii::t('webinar', 'Your profile seems not to have a valid email setup').'</span>';
			$message .= '. ';
			if (Settings::get('profile_only_pwd', 'off') == 'off') {
				$message .= preg_replace(
					'/\{begin\_link\}([\w\s]*)\{end\_link\}/',
					'<a href="'.Docebo::createLmsUrl('user/editProfile').'" class="open-dialog" data-dialog-class="modal-edit-profile" title="'.Yii::t('standard', '_VIEW_PROFILE').'">$1</a>',
					Yii::t('webinar', 'To join this session, please update your profile with a valid email')
				);
			} else {
				$message .= Yii::t('webinar', 'To join this session, please contact your instructor or administrator');
			}
			return array(
				'valid' => false,
				'message' => $message
			);
		}
		return array('valid' => true, 'message' => '');
	}

}