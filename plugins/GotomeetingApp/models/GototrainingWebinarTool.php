<?php

/**
 * Class GototrainingWebinarTool
 * Implemenents webinar tool for gototraining
 */
class GototrainingWebinarTool extends WebinarTool
{
	/**
	 * @var bool does the current webinar tool supports grabbing a recording via API call?
	 */
	protected $apiGrabSupported = true;

	/**
	 * Main constructor
	 */
	protected function __construct()
	{
		$this->id = 'gototraining';
		$this->name = 'Gototraining';
		$this->directJoin = false;
	}

	/**
	 * Returns true if the current webinar tools has support for multi account
	 * @return bool
	 */
	public function hasMultiAccountSupport()
	{
		return true;
	}

	/**
	 * Returns the array of accounts configured for the current tool indexed by id
	 * @return WebinarToolAccount[]
	 */
	public function getAccounts()
	{
		if (!$this->accounts) {
			$records = WebinarToolAccount::model()->findAllByAttributes(array('tool' => 'gotomeeting'));
			$this->accounts = CHtml::listData($records, 'id_account', function ($row) {
				return $row;
			});
		}

		return $this->accounts;
	}

	/**
	 * Creates a new room using the Gototraining api
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param string $roomDescription The description of the room
	 * @param integer $idAccount The id of the account to use to create the room
	 * @param $startDateTime string The start date&time in UTC + MYSQL/ISO format
	 * @param $endDateTime string The end date&time in UTC + MYSQL/ISO format
	 * @param $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 *
	 * @return array "tool specific" params returned by the api
	 */
	public function createRoom($roomName, $roomDescription, $idAccount, $startDateTime, $endDateTime, $maxParticipants, $api_params = array())
	{
		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new GotomeetingApiClient($accountSettings->api_key, $accountSettings->access_token, $accountSettings->accesstoken_organizer_key);
		$training = $apiClient->createTraining($roomName, $roomDescription, $startDateTime, $endDateTime);

		if (!isset($training["data"]))
			throw new CHttpException(500, ConferenceManager::$MSG_INVALID_DATA_RETURNED);

		$result = array(
			"name" => $roomName,
			"training_id" => $training["data"],
			"join_url" => $training['join_url']
		);

		return $result;
	}

	/**
	 * Updates an existing room with the provided params
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param string $roomDescription The description of the room
	 * @param integer $idAccount The id of the account to use to update the room
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @param $startDateTime string The start date&time in UTC + MYSQL/ISO format
	 * @param $endDateTime string The end date&time in UTC + MYSQL/ISO format
	 * @param $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 *
	 * @return array "tool specific" params returned by the api
	 */
	public function updateRoom($roomName, $roomDescription, $idAccount, $api_params = array(), $startDateTime, $endDateTime, $maxParticipants, $updateTimes = true)
	{
		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new GotomeetingApiClient($accountSettings->api_key, $accountSettings->access_token, $accountSettings->accesstoken_organizer_key);
		$apiClient->updateTraining($api_params['training_id'], $roomName, $roomDescription, $startDateTime, $endDateTime, $updateTimes);
		$api_params['name'] = $roomName;
		return $api_params;
	}

	/**
	 * Deletes an existing room
	 *
	 * @param integer $idAccount The id of the account to use to delete the room
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 */
	public function deleteRoom($idAccount, $api_params = array())
	{
		$trainingId = $api_params['training_id'];
		$account = $this->getAccountById($idAccount, true);
		if($account !== false)
		{
			$accountSettings = $account->settings;
			$apiClient = new GotomeetingApiClient($accountSettings->api_key, $accountSettings->access_token, $accountSettings->accesstoken_organizer_key);
			$apiClient->deleteTraining($trainingId);
		}
	}

	/**
	 * Returns the URL to join a room
	 *
	 * @param string         $roomName         The name of the room to create in the videconference platform
	 * @param integer        $maxParticipants  integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param integer        $idAccount        The id of the account to use
	 * @param array          $api_params       An array of "tool specific" params to pass to the api
	 * @param CoreUser       $userModel        The id of the user for whom the current URL is returned
	 * @param WebinarSession $sessionDateModel The current session object
	 */
	public function getJoinRoomUrl($roomName, $maxParticipants, $idAccount, $api_params, $userModel, $sessionDateModel, $widgetContext = false)
	{
		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new GotomeetingApiClient($accountSettings->api_key, $accountSettings->access_token, $accountSettings->accesstoken_organizer_key);
		$joinUrl = $apiClient->getJoinTrainingUrl($api_params['training_id'], $userModel->email, $userModel->firstname, $userModel->lastname);
		return $joinUrl;
	}

	/**
	 * Returns the URL to start a room
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param integer $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param integer $idAccount The id of the account to use
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @param CoreUser $userModel The id of the user for whom the current URL is returned
	 * @param WebinarSession $sessionModel The current session object
	 */
	public function getStartRoomUrl($roomName, $maxParticipants, $idAccount, $api_params, $userModel, $sessionModel)
	{
		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new GotomeetingApiClient($accountSettings->api_key, $accountSettings->access_token, $accountSettings->accesstoken_organizer_key);
		$result = $apiClient->getStartTrainingUrl($api_params['training_id']);
		if (is_array($result['data']) && isset($result['data']['hostURL']))
			return $result['data']['hostURL'];
		else
			return $result["data"];
	}

	/**
	 * @param int $idAccount $idAccount The id of the account to use
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @return array
	 */
	public function getApiRecordings($idAccount, $api_params){
		$return = array(
			'error' => '',
			'recordingUrl' => ''
		);
		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new GotomeetingApiClient($accountSettings->api_key, $accountSettings->access_token, $accountSettings->accesstoken_organizer_key);
		$result = $apiClient->getTrainingRecording($api_params['training_id']);

		if(!empty($result['data']['recordingList'])){
			$return['recordingUrl'] = $result['data']['recordingList'][0]['downloadUrl'];
		} else {
			$return['error'] = Yii::t('webinar', 'Could not find a recording for this session');
		}
		return $return;
	}


	/**
	 * Check if the user is valid for current tool
	 * @param $user int|CoreUser user IDST
	 * @return array validation result
	 */
	public function validateUser($user)
	{
		$userModel = false;
		if (is_numeric($user) && (int)$user > 0) {
			$userModel = CoreUser::model()->findByPk((int)$user);
		} elseif (is_object($user) && get_class($user) == 'CoreUser') {
			$userModel = $user;
		}
		if (empty($userModel)) {
			return array(
				'valid' => false,
				'message' => Yii::t('standard', 'Invalid user')
			);
		}
		// NOTE: GoToTraining conferences require a valid email in order to be able to join users
		if (empty($user->email)) {
			$message = '<span class="bold">'.Yii::t('webinar', 'Your profile seems not to have a valid email setup').'</span>';
			$message .= '. ';
			if (Settings::get('profile_only_pwd', 'off') == 'off') {
				$message .= preg_replace(
					'/\{begin\_link\}([\w\s]*)\{end\_link\}/',
					'<a href="'.Docebo::createLmsUrl('user/editProfile').'" class="open-dialog" data-dialog-class="modal-edit-profile" title="'.Yii::t('standard', '_VIEW_PROFILE').'">$1</a>',
					Yii::t('webinar', 'To join this session, please update your profile with a valid email')
				);
			} else {
				$message .= Yii::t('webinar', 'To join this session, please contact your instructor or administrator');
			}
			return array(
				'valid' => false,
				'message' => $message
			);
		}
		return array('valid' => true, 'message' => '');
	}

}