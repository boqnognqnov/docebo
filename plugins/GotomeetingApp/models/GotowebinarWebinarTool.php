<?php

/**
 * Class GotowebinarWebinarTool
 * Implemenents webinar tool for gotowebinar
 */
class GotowebinarWebinarTool extends WebinarTool
{
	/**
	 * Main constructor
	 */
	protected function __construct()
	{
		$this->id = 'gotowebinar';
		$this->name = 'Gotowebinar';
		$this->directJoin = false;
	}

	/**
	 * Returns true if the current webinar tools has support for multi account
	 * @return bool
	 */
	public function hasMultiAccountSupport()
	{
		return true;
	}

	/**
	 * Returns the array of accounts configured for the current tool indexed by id
	 * @return WebinarToolAccount[]
	 */
	public function getAccounts()
	{
		if (!$this->accounts) {
			$records = WebinarToolAccount::model()->findAllByAttributes(array('tool' => 'gotomeeting'));
			$this->accounts = CHtml::listData($records, 'id_account', function ($row) {
				return $row;
			});
		}

		return $this->accounts;
	}

	/**
	 * Creates a new room using the Gotowebinar api
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param string $roomDescription The description of the room
	 * @param integer $idAccount The id of the account to use to create the room
	 * @param $startDateTime string The start date&time in UTC + MYSQL/ISO format
	 * @param $endDateTime string The end date&time in UTC + MYSQL/ISO format
	 * @param $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 *
	 * @return array "tool specific" params returned by the api
	 */
	public function createRoom($roomName, $roomDescription, $idAccount, $startDateTime, $endDateTime, $maxParticipants, $api_params = array())
	{
		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new GotomeetingApiClient($accountSettings->api_key, $accountSettings->access_token, $accountSettings->accesstoken_organizer_key);
		$webinar = $apiClient->createWebinar($roomName, $roomDescription, $startDateTime, $endDateTime);

		if (!isset($webinar["data"]))
			throw new CHttpException(500, ConferenceManager::$MSG_INVALID_DATA_RETURNED);

		$result = array(
			"name" => $roomName,
			"webinar_id" => $webinar['data']['webinarKey']
		);

		return $result;
	}

	/**
	 * Updates an existing room with the provided params
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param string $roomDescription The description of the room
	 * @param integer $idAccount The id of the account to use to update the room
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @param $startDateTime string The start date&time in UTC + MYSQL/ISO format
	 * @param $endDateTime string The end date&time in UTC + MYSQL/ISO format
	 * @param $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 *
	 * @return array "tool specific" params returned by the api
	 */
	public function updateRoom($roomName, $roomDescription, $idAccount, $api_params = array(), $startDateTime, $endDateTime, $maxParticipants)
	{
		$startTime = strtotime($startDateTime);
		$endTime = strtotime($endDateTime);

		$startDateTime = gmdate('c', $startTime);
		$endDateTime =   gmdate('c', $endTime);

		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new GotomeetingApiClient($accountSettings->api_key, $accountSettings->access_token, $accountSettings->accesstoken_organizer_key);
		$apiClient->updateWebinar($api_params['webinar_id'], $roomName, $roomDescription, $startDateTime, $endDateTime);
		$api_params['name'] = $roomName;
		return $api_params;
	}

	/**
	 * Deletes an existing room
	 *
	 * @param integer $idAccount The id of the account to use to delete the room
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 */
	public function deleteRoom($idAccount, $api_params = array())
	{
		$webinarId = $api_params['webinar_id'];
		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new GotomeetingApiClient($accountSettings->api_key, $accountSettings->access_token, $accountSettings->accesstoken_organizer_key);
		$apiClient->deleteWebinar($webinarId);
	}

	/**
	 * Returns the URL to join a room
	 *
	 * @param string         $roomName         The name of the room to create in the videconference platform
	 * @param integer        $maxParticipants  integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param integer        $idAccount        The id of the account to use
	 * @param array          $api_params       An array of "tool specific" params to pass to the api
	 * @param CoreUser       $userModel        The id of the user for whom the current URL is returned
	 * @param WebinarSession $sessionDateModel The current session object
	 */
	public function getJoinRoomUrl($roomName, $maxParticipants, $idAccount, $api_params, $userModel, $sessionDateModel, $widgetContext = false)
	{
		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new GotomeetingApiClient($accountSettings->api_key, $accountSettings->access_token, $accountSettings->accesstoken_organizer_key);
		$joinUrl = $apiClient->getJoinWebinarUrl($api_params['webinar_id'], $userModel->email, $userModel->firstname, $userModel->lastname);
		return $joinUrl;
	}

	public function getStartRoomUrl($roomName, $maxParticipants, $idAccount, $api_params, $userModel, $sessionModel)
	{
		// Added a static redirect to gotoWebinar because gotoWebinar's API doesn't yet provide a getStartUrl call...
		return "http://www.gotowebinar.com";
	}


	/**
	 * Check if the user is valid for current tool
	 * @param $user int|CoreUser user IDST
	 * @return array validation result
	 */
	public function validateUser($user)
	{
		$userModel = false;
		if (is_numeric($user) && (int)$user > 0) {
			$userModel = CoreUser::model()->findByPk((int)$user);
		} elseif (is_object($user) && get_class($user) == 'CoreUser') {
			$userModel = $user;
		}
		if (empty($userModel)) {
			return array(
				'valid' => false,
				'message' => Yii::t('standard', 'Invalid user')
			);
		}
		// NOTE: Gotowebinar conferences require a valid email in order to be able to join users
		if (empty($user->email)) {
			$message = '<span class="bold">'.Yii::t('webinar', 'Your profile seems not to have a valid email setup').'</span>';
			$message .= '. ';
			if (Settings::get('profile_only_pwd', 'off') == 'off') {
				$message .= preg_replace(
					'/\{begin\_link\}([\w\s]*)\{end\_link\}/',
					'<a href="'.Docebo::createLmsUrl('user/editProfile').'" class="open-dialog" data-dialog-class="modal-edit-profile" title="'.Yii::t('standard', '_VIEW_PROFILE').'">$1</a>',
					Yii::t('webinar', 'To join this session, please update your profile with a valid email')
				);
			} else {
				$message .= Yii::t('webinar', 'To join this session, please contact your instructor or administrator');
			}
			return array(
				'valid' => false,
				'message' => $message
			);
		}
		return array('valid' => true, 'message' => '');
	}

}