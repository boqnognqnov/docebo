<?php

/**
 * Plugin for CustomdomainApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class CustomdomainApp extends DoceboPlugin {


	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'CustomdomainApp';


	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {
		parent::activate();
		$custom_domain_url = Settings::get('custom_domain_url');

		//preventing overwrite on app reactivate
		if ($custom_domain_url == null)
			Settings::save('custom_domain_url', '');
		
		if ($custom_domain_url) {
			$custom_domain_prev_url = Settings::get('custom_domain_prev_url', '');
			Yii::import('CustomdomainApp.components.*'); //make sure that the class 'CustomdomainManager' is available at this point
			$hipacheRes = CustomdomainManager::updateHipacheCustomDomain($custom_domain_url, $custom_domain_prev_url);
			if (!$hipacheRes) {
				return false;
			}
		}
		
		
		Settings::save('custom_domain_last_apply', '0');
		
		// Mark this module as active
		Settings::save('module_customdomain', 'on');
		
		$originalDomain = Settings::get('custom_domain_original');
		if(!$originalDomain){
			// Also get and store the original (Docebo SAAS) domain in the LMS
			$installation_info = ErpApiClient::getInstallationInfo();
			$domain = (isset($installation_info['data']) && isset($installation_info['data']['domain_name']))
								? $installation_info['data']['domain_name'] 
								: false;
			
			if ($domain) {
				Settings::save('custom_domain_original', $domain);
			}
			elseif (Settings::get('url')) {
				// We couldn't get the original domain via ERP api call
				// Then use the one we have locally
				$host = parse_url(Settings::get('url'), PHP_URL_HOST);
				if($host){
					Settings::save('custom_domain_original', $host);
				}
			}
		}
		
		

	}


	public function deactivate() {
		
		parent::deactivate();

		$url = Settings::get('custom_domain_url');
		
		$custom_domain_prev_url = Settings::get('custom_domain_prev_url', '');
		$hipacheRes = CustomdomainManager::updateHipacheCustomDomain($url, $custom_domain_prev_url, true);
		if (!$hipacheRes) {
			return false;
		}
		
		//remove S3 file
		ErpApiClient::removeCustomDomain();
		$config_path = Docebo::getRootBasePath() . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR;
		//remove config file
		$domain_code = preg_replace('/(\W)/is', '_', $url).".php";
		if (file_exists($config_path.$domain_code))
		{
			unlink($config_path.$domain_code);
		}

		Settings::save('custom_domain_url', '');
		Settings::save('custom_domain_prev_url', '');

		// Restore core_setting->url field (http://<domain.com>/
		if(Settings::get('custom_domain_original')) 
			$urlToSave = Docebo::fixUrlSchema(Settings::get('custom_domain_original'));
		else 	
			$urlToSave = Docebo::fixUrlSchema($GLOBALS['cfg']['valid_domains'][0]);
		Settings::save('url', $urlToSave . '/');

		// Mark this module as inactive
		Settings::save('module_customdomain', 'off');
		
		Settings::save('custom_domain_original', '');
		
		

	}

}
