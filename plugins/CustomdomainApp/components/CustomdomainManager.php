<?php
/**
 * Class to handle Custom Domain operations
 */

class CustomdomainManager {

	const PENDING_TIME = 5; // minutes

	/**
	 * Server name of the Amazon
	 * @return string
	 */
	public static function getServerName()
	{
		return 'test1';
	}
	/**
	 * Activate the custom domain and eventually remove the previous one
	 * @param string $domain_url custom url domain
	 * @param string $prev_url if this method is called for the first time this will be empty
	 * @return bool
	 */
	public static function activateCustomDomain($domain_url, $prev_url='') {
		$res = true;

		//TODO: log actions and eventually send report by email
		// a customer could enter the name of an already taken domain or "remove" (if prev_url)
		// the info of a domain not owned by himself; we should find a way to handle this
		// but a log email could be a start

		//TODO: 2 - add plesk alias
//		$api_res = ErpApiClient::addPleskAlias($main_domain, $domain_url, self::getServerName());
//		$res &= $api_res['succcess'];

		//TODO: 3 - remove plesk alias of prev_url if found (?)
		if ($prev_url)
		{
//			$api_res = ErpApiClient::removePleskAlias($domain_url);
//			$res &= $api_res['succcess'];
		}

		//update core_settings table url parameter with the new custom url
		$domain = $domain_url;
		$domain_url = Docebo::fixUrlSchema($domain);
		Settings::save('url', $domain_url . '/');

		$api_res = ErpApiClient::getInstallationInfo();
		$res &= $api_res['success'];

		//update main config file
		//remove config file for the old custom url (prev_url) if found / given
		//create the new config file for the custom domain:
		$res &= self::modifyCustomDomainConfig($api_res['data'], $domain, $prev_url);

		// Note: we have a doc called "Procedura assegnazione dominio 2o livello SAAS" on google docs; but for now it is in
		// Italian; eventually ask if someone can translate this in English.


		return $res;
	}

	/**
	 * Removes dashes and dots from the name
	 * @param $domain
	 * @return string
	 */
	public static function domainNameNormalize($domain)
	{
		return preg_replace('/(\W)/is', '_', $domain);
	}

	/**
	 * Contact Redis server holding hipache configuration database and add/remove "custom domain" information.
	 *
	 * Hipache Config for a single domain is of Redis Type LIST and looks like:
	 *
	 * <key> =>  <arbitrary-domain-id> <vhost>
	 *
	 * Example:
	 *
	 * frontend:my.domain.com  =>   my.domain.com  http://192.169.0.1
	 *
	 * @param string $custom_domain  The custome domain
	 * @param boolean $remove  If set to true, custom domain KEY will be removed ONLY from Redis hipache config
	 *
	 */
	public static function updateHipacheCustomDomain($custom_domain, $custom_domain_prev, $remove = false){
		require_once(Yii::getPathOfAlias('common.vendors.Predis') . "/Autoloader.php");
		Predis\Autoloader::register();

		$redisHost 				= Yii::app()->params['hipache_redis_host'];
		$redisPort 				= Yii::app()->params['hipache_redis_port'];
		$redisHipacheDbNumber 	= Yii::app()->params['hipache_redis_db'];
		//$vhost 					= 'http://' . $_SERVER['SERVER_ADDR'];
		//$vhost 					= 'http://' . Yii::app()->params['hipache_lms_vhost'];
		$vhost 					= Yii::app()->params['hipache_lms_vhost'];

		$redis = new Predis\Client(array(
				'host' => $redisHost,
				'port' => $redisPort,
				'database' => $redisHipacheDbNumber,
		));

		$res = false;

		if ($custom_domain) {
			// Clean up the value and get the domain only (just in case)
			$custom_domain = strtolower($custom_domain);

			// Add HTTP schema if needed
			$custom_domain = Docebo::fixUrlSchema($custom_domain);

			$custom_domain = parse_url($custom_domain, PHP_URL_HOST);

			if($custom_domain){
				// Do the Redis call
				try {

					$listKey = "frontend:" . self::addWww($custom_domain);


					// Check if KEY exists @TODO Should we allow this scenario??
					$exists = $redis->exists($listKey);

					// Delete old key
					$redis->del($listKey);

					// Add the new one if NOT asked to remove only
					if ( !($remove === true)) {
						// Put original domain as first element in the list
						$redis->rpush($listKey, Docebo::getOriginalDomain());

						// Hipache route parameter could be an array of values, lets check this scenario
						if (is_array($vhost)) {
							foreach ($vhost as $vh) {
								// We do not add HTTP schema; configuration parameter MUST provide it, including port
								// e.g.   array("http://1.2.3.4:10100", "http://1.2.3.4:10100", ...)
								$redis->rpush($listKey, $vh);
							}
						}
						// Mkey, a string (providing backward compatibility)
						else {
							$redis->rpush($listKey, "http://" . $vhost);
						}

					}

					// Delete Previous custom domain config from Redis/hipache
					if ( ($custom_domain_prev != $custom_domain) || $remove) {
						$prevCustomDomainListKey = "frontend:" . self::addWww($custom_domain_prev);
						$redis->del($prevCustomDomainListKey);

						if ($remove) {
							$redis->del("frontend:" . self::addWww($custom_domain));
						}
					}

					$res = true;
				}
				catch(Exception $e) {
					Yii::log("Failed to call Redis and set custom domain information in hipache: " . $custom_domain, CLogger::LEVEL_ERROR);
					Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);

					$res = false;
				}
			}
		}

		return $res;

	}


	/**
	 * Manipulate/Update Custom domain configuration file OR Redis key
	 *
	 * @param $backup_info array with values from backup_list table
	 * @param $domain_url new custom domain
	 * @param $prev_url prev custom domain (if exists)
	 */
	public static function modifyCustomDomainConfig($backup_info, $domain_url, $prev_url)
	{
		self::modifyCustomDomainConfigRedis($backup_info, $domain_url, $prev_url);
		return true;
	}



	/**
	 * Create/Update CustomDomain record in Redis config.
	 * This method assumes that the Main domain config is loaded from Redis, not from Config FILE (?)
	 *
	 * @param array $backup_info
	 * @param string $domain_url
	 * @param string $prev_url
	 * @return boolean
	 */
	public static function modifyCustomDomainConfigRedis($backup_info, $custom_domain, $prev_url) {

		// Get Redis client
		require_once(Yii::getPathOfAlias('common.vendors.Predis') . "/Autoloader.php");
		Predis\Autoloader::register();
		$redis = new Predis\Client(array(
				'host' 		=> Yii::app()->params['config_redis_host'],
				'port' 		=> Yii::app()->params['config_redis_port'],
				'database' 	=> Yii::app()->params['config_redis_db'],
		));


		$res = true;

		// Original domain is the one from ERP (backup list -> domain name)
		$original_domain = $backup_info['domain_name'];

		try {
			// Delete Previous domain from Redis, if any
			if ($prev_url) {
				$prev_url = preg_replace('/^www\\./', '', strtolower($prev_url));
				// Protect ORIGINAL DOMAIN KEY, always
				if ($prev_url != $original_domain) {
				    $redis->del($prev_url);
				}
			}

			// Get the type of the key and use the same type for alloperations
			$type = $redis->type($original_domain);

			// Get main domain config, loading the Main Domain Redis record
			$config = array();

			// String or Hash only
			if ($type == 'string') {
				$config = CJSON::decode($redis->get($original_domain));
			}
			else if ($type == 'hash') {
				$config = $redis->hgetall($original_domain);
				$config['valid_domains'] = CJSON::decode($config['valid_domains']);
			}

			$custom_domain = preg_replace('/^www\\./', '', strtolower($custom_domain));

			// Modify some values and create the Custom domain config in Redis.
			// Custom domain record is a clone of the main one, but having below data set
			$config['original_domain'] = $original_domain; // Redis config loader will "notice" this is set and will act accordingly
			$config['valid_domains'][] = $custom_domain;

			// Always protect ORIGINAL DOMAIN'S key, it should never be overwritten or deleted
			if ($custom_domain != $original_domain) {
                // Write back, use the same type
                if ($type == 'string') {
                    $redis->set($custom_domain, CJSON::encode($config));
                }
			    else if ($type == 'hash') {
				    $config['valid_domains'] = CJSON::encode($config['valid_domains']);
    		        $redis->del($custom_domain);
			        $redis->hmset($custom_domain, $config);
			    }
			}

			return true;
		}
		catch (Exception $e) {
			Yii::log("Failed to call Redis and create custom domain configuration for custom domain: " . $custom_domain, CLogger::LEVEL_ERROR);
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);

			return false;
		}

	}


	/**
	 * Add "www." to 2-level domains as well as to .co.uk, if there is no  www. as last level/
	 * This is to cope with the forcefully added www. (to such domains) by the frontend nginx
	 *
	 * @param string $domain
	 * @return string
	 */
	public static function addWww($domain) {

		// Add www. to 2 level domains (example.com)
		$domainArr = explode('.', $domain);
		if (count($domainArr) == 2) {
			return 'www.' . $domain;
		}

		// Add www. to CO.UK domains (example.co.uk)
		$test = strtolower($domainArr[1] . '.' . $domainArr[2]);
		if (count($domainArr) == 3 && ($test == 'co.uk')) {
			return 'www.' . $domain;
		}

		return $domain;
	}





}
