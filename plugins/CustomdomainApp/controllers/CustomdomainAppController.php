<?php

class CustomdomainAppController extends Controller {


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		$admin_only_actions = array('settings');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}


	/**
	 * Step1: settings dialog where the user can set the custom domain name
	 */
	public function actionSettings() {

		$settings = new CustomdomainAppSettingsForm();
		$last_apply = Settings::get('custom_domain_last_apply', 0);
		$settings->custom_domain_url = Settings::get('custom_domain_url');
		$prev_url = $settings->custom_domain_url;


		if (isset($_GET['done'])) {
			Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));
		}

		if (isset($_POST['CustomdomainAppSettingsForm'])) {
			$settings->setAttributes($_POST['CustomdomainAppSettingsForm']);
			$settings->custom_domain_url = preg_replace('/^(\\s*http[s]?:\\/\\/)(.*)/is', '$2', $settings->custom_domain_url);

			if ($settings->validate() ) {
				Settings::save('custom_domain_url', $settings->custom_domain_url); // new url
				Settings::save('custom_domain_prev_url', $prev_url); // previous url
				$last_apply = Settings::get('custom_domain_last_apply');
				$this->redirect(array('apply'));
			}
			else {
				Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
			}
		}


		$pending_changes = false;
		if ((time()-$last_apply) / 60 < CustomdomainManager::PENDING_TIME) {
			// if elapsed time from last save is less than N minutes:
			$pending_changes = true;
		}

		$this->render('settings', array('settings' => $settings, 'pending_changes' => $pending_changes));
	}


	public function actionApply() {
		// Note "url" is misleading; they are DOMAINS actually, NO http://, no slashes whatsoever
		$custom_domain_url = Settings::get('custom_domain_url');
		$custom_domain_prev_url = Settings::get('custom_domain_prev_url', '');

		if (isset($_POST['do_submit'])) {
			if (isset($_POST['do_apply'])) {
				if (!CustomdomainManager::updateHipacheCustomDomain($custom_domain_url, $custom_domain_prev_url)) {
					Yii::app()->user->setFlash('error', Yii::t('standard', "Error updating hipache proxy configuration."));
				}
				else {
					$res = CustomdomainManager::activateCustomDomain($custom_domain_url, $custom_domain_prev_url);
					$api_res = ErpApiClient::updateCustomDomain($custom_domain_url);
					$res &= $api_res['success'];
					if ($res) {
						// We are good with ERP, lets save and redirect
						Settings::save('custom_domain_last_apply', time());
						//redirect to the main domain, otherwise it will show an error
						$this->redirect('http'.(Yii::app()->request->isSecureConnection ? 's' : '').'://'.$GLOBALS['cfg']['valid_domains'][0].Docebo::createLmsUrl('CustomdomainApp/customdomainApp/settings', array('done'=>1)), true);
						Yii::app()->end();
					}
					Yii::app()->user->setFlash('error', Yii::t('custom_domain', "_APPLY_ERROR_"));
				}
			}
			else {
				Yii::app()->user->setFlash('error', Yii::t('custom_domain', "_REVIEW_AND_CONFIRM_"));
			}
		}


		$this->render('apply', array(
			'custom_domain_url' => $custom_domain_url,
			'custom_domain_prev_url' => $custom_domain_prev_url,
		));
	}

	
	
	
	

}
