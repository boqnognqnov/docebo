<?php

/**
 * CustomdomainAppSettingsForm class.
 *
 * @property string $custom_domain_url
 */
class CustomdomainAppSettingsForm extends CFormModel {
	public $custom_domain_url;


	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			array('custom_domain_url', 'required'),
			array('custom_domain_url', 'validateDomain'),
			array('custom_domain_url', 'checkIfExists'),
		);
	}

	public function validateDomain($attribute, $params)
	{
		$domain = str_replace('www.','',$this->$attribute);

		if(!$domain){
			return;
		}
		
		$valid = (preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $domain) //valid chars check
					&& preg_match("/^.{1,253}$/", $domain) //overall length check
					&& preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $domain)   ); //length of each label
		
		// Check if the domain contains the bad word 'docebo' on its second level
		$valid &= !$this->domainContainsBadWord($domain);

		if(!$valid)
		{
			$this->addError($attribute, Yii::t('configuration', 'Domain name is not valid'));
		}
	}

	public function checkIfExists($attribute, $params)
	{
		$domain = str_replace('www.','',$this->$attribute);
		$api_res = ErpApiClient::apiCheckIfCustomDomainExists($domain);
		if($api_res['success'] == false)
		{
			$this->addError($attribute, Yii::t('configuration', 'Domain name is already in use'));
		}
	}

	/**
	 * Checks if the provided domain contains the word "docebo" (not allowed)
	 *
	 * @param $input
	 *
	 * @return boolean True if the domain contains the bad word
	 */
	public function domainContainsBadWord($input){
		$badword = 'docebo';

		$badWordFound = (stripos($input, $badword) !== false);

		return $badWordFound;
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array(
			'custom_domain_url' => Yii::t('custom_domain', '_DOMAIN_NAME_'),
		);
	}
}
