
<h2><?=Yii::t('custom_domain','_APPLY_CHANGES_'); ?></h2>
<br>

<?php DoceboUI::printFlashMessages(); ?>

<?php echo CHtml::beginForm('', 'post', array('class' => 'form-inline')); ?>


<div><p>
	<?=Yii::t('custom_domain',"_APPLY_STEP_TEXT_", array(
		'{pending_time}'=>CustomdomainManager::PENDING_TIME,
	));?>
</p></div>


<div style="font-size: 18px; margin-bottom: 10pt;"><strong><?=Yii::t('custom_domain', '_DOMAIN_NAME_')?>:</strong>
	<span class="text-info"><?=$custom_domain_url;?></span>
</div>

<div class="control-group">
	<div class="controls">
		<label class="checkbox">
			<?=CHtml::checkBox('do_apply', false);?>
			<?=Yii::t('standard','_CONFIRM');?>
		</label>
	</div>
</div>

<?=CHtml::hiddenField('do_submit', '1');?>

<div class="form-actions">
	<?=CHtml::submitButton(Yii::t('standard', '_CONTINUE'), array('class'=>'btn-docebo green big')); ?>
	&nbsp;
	<a href="<?=$this->createUrl('settings');?>" class="btn-docebo black big"><?=Yii::t('standard', '_BACK');?></a>
</div>

<?php echo CHtml::endForm(); ?>