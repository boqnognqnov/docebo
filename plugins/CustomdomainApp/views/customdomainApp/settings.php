<?php
/* @var $form CActiveForm */
/* @var $settings CustomdomainAppSettingsForm */
?>

<a href="<?= ('it' == Yii::app()->getLanguage()) ? 'http://www.docebo.com/it/lms-elearning-moduli-e-integrazioni/' : 'http://www.docebo.com/lms-elearning-modules-integrations/' ?>" target="_blank" class="app-link-read-manual"><?= Yii::t('apps', 'Read Manual'); ?></a>
<h2><?=Yii::t('custom_domain','_CUSTOM_DOMAIN_'); ?> - <?=Yii::t('standard','Settings'); ?></h2>
<br>

<?php DoceboUI::printFlashMessages(); ?>

<?php if ($pending_changes): ?>
<div class="alert">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	<?=Yii::t('custom_domain', '_WARNING_PROCESSING_MSG_');?>
</div>
<?php endif; ?>
<div><p>
	<? if (isset($_GET['done']) && $_GET['done'] == 1) : ?>
		<?=Yii::t('custom_domain', "_SETTINGS_STEP_HOW_TO_SETUP_", array('85.94.212.199'=>'85.94.200.197')); ?>
	<? else : ?>
		<?=Yii::t('custom_domain', "_SETTINGS_STEP_"); ?>
	<? endif; ?>
</p></div>
<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'app-settings-form',
	'htmlOptions'=>array('class' => 'form-horizontal'),
));
?>

<div class="error-summary"><?php echo $form->errorSummary($settings); ?></div>

<div class="control-group">
	<?=$form->labelEx($settings, 'custom_domain_url', array('class'=>'control-label'));?>
	<div class="controls">
		<?=$form->textField($settings, 'custom_domain_url', array('placeholder'=>'yourcompany.com'));?>
	</div>
</div>


<div class="form-actions">

	<?=CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
		'class'=>'btn-docebo green big'.($pending_changes ? ' disabled' : ''),
		'disabled'=>$pending_changes,
	)); ?>
	&nbsp;
	<a href="<?=Docebo::createLmsUrl("app/index");?>" class="btn-docebo black big"><?=Yii::t('standard', '_CANCEL');?></a>
</div>

<?php $this->endWidget(); ?>