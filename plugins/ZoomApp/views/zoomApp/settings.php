<?php
/* @var $form CActiveForm */
/* @var $settings BlueJeansAppSettingsForm */
?>

<?php DoceboUI::printFlashMessages(); ?>

<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'app-settings-form',
	'htmlOptions'=>array('class' => 'form-horizontal  docebo-form'),
));
?>

	<a href="<?= ('it' == Yii::app()->getLanguage()) ? 'http://www.docebo.com/it/lms-elearning-moduli-e-integrazioni/' : 'http://www.docebo.com/lms-elearning-modules-integrations/' ?>" target="_blank" class="app-link-read-manual"><?= Yii::t('apps', 'Read Manual'); ?></a>

	<h2><?=Yii::t('standard','Settings') ?>: Zoom</h2>
	<br>

	<div class="control-container odd">
		<div class="control-group">
			<?=CHtml::label(Yii::t('standard', 'Website'), false, array('class'=>'control-label'));?>
			<div class="controls">
				<a class="app-settings-url" href="http://zoom.us/" target="_blank">http://zoom.us/</a>
			</div>
		</div>
	</div>

	<div class="control-container even">
		<div class="control-group">
			<?=CHtml::label(Yii::t('webinar', 'Accounts'), false, array('class'=>'control-label'));?>
			<div class="controls">
				<?php
				$this->widget('common.widgets.WebinarAccountsEditor', array(
					'dataProvider' => WebinarToolAccount::model()->dataProvider('zoommeeting'),
					'editAccountUrl' => $this->createUrl('editAccount'),
					'enableMultiAccount' => WebinarTool::getById('zoommeeting')->hasMultiAccountSupport(),
				));
				?>
			</div>
		</div>
	</div>

<?php $this->endWidget(); ?>