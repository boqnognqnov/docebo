<?php
/* @var $form CActiveForm */
/* @var $settings ZoomAppSettingsForm */
?>

<h1><?= $modalTitle ?></h1>
<div class="form edit-webinar-account-dialog">

	<?php $form = $this->beginWidget('CActiveForm',
		array(
			'id' => 'webinar-account-settings-form',
			'htmlOptions' => array(
				'class' => 'ajax webinar-account-settings-form'
			)
		)
	); ?>

	<div class="control-group">
		<?=$form->labelEx($settings, 'account_name', array('class'=>'control-label'));?>
		<div class="controls">
			<?=$form->textField($settings, 'account_name', array('class'=>'input-xlarge'));?>
			<?=$form->error($settings, 'account_name'); ?>
		</div>
	</div>

	<div class="control-group">
		<?=$form->labelEx($settings, 'app_key', array('class'=>'control-label'));?>
		<div class="controls">
			<?=$form->textField($settings, 'app_key', array('class'=>'input-xlarge'));?>
			<?=$form->error($settings, 'app_key'); ?>
		</div>
	</div>

	<div class="control-group">
		<?=$form->labelEx($settings, 'app_secret', array('class'=>'control-label'));?>
		<div class="controls">
			<?=$form->textField($settings, 'app_secret', array('class'=>'input-xlarge'));?>
			<?=$form->error($settings, 'app_secret'); ?>
		</div>
	</div>

	<!--<div class="control-group">
		<?/*=$form->labelEx($settings, 'access_token', array('class'=>'control-label'));*/?>
		<div class="controls">
			<?/*=$form->textField($settings, 'access_token', array('class'=>'input-xlarge'));*/?>
			<?/*=$form->error($settings, 'access_token'); */?>
		</div>
	</div>-->

	<div class="control-group">
		<?=$form->labelEx($settings, 'account_email', array('class'=>'control-label'));?>
		<div class="controls">
			<?=$form->textField($settings, 'account_email', array('class'=>'input-xlarge'));?>
			<?=$form->error($settings, 'account_email'); ?>
		</div>
	</div>

	<div class="control-group">
		<?=$form->labelEx($settings, 'session_type', array('class'=>'control-label'));?>
		<?=$form->radioButtonList($settings, 'session_type', $paramsTypeSelection, $optionsTypeSelection)?>
		<?=$form->error($settings, 'session_type'); ?>
	</div>

	<div class="control-group">
		<?=$form->labelEx($settings, 'additional_info', array('class'=>'control-label'));?>
		<div class="controls">
			<?=$form->textArea($settings, 'additional_info', array('class'=>'input-xlarge'));?>
			<?=$form->error($settings, 'additional_info'); ?>
		</div>
	</div>

	<hr>
	<div class="control-group webinar-info-text">
		<?=Yii::t('webinar', 'To allow an unlimited amount of sessions or meetings, enter "0" in the following fields')?>
	</div>

	<div class="control-group" style="clear: both;">
		<?=$form->labelEx($settings, 'max_rooms_per_course', array('class'=>'control-label'));?>
		<div class="controls">
			<?=$form->textField($settings, 'max_rooms_per_course', array('class'=>'input-xlarge'));?>
			<?=$form->error($settings, 'max_rooms_per_course'); ?>
		</div>
	</div>

	<div class="control-group">
		<?=$form->labelEx($settings, 'max_rooms', array('class'=>'control-label'));?>
		<div class="controls">
			<?=$form->textField($settings, 'max_rooms', array('class'=>'input-xlarge'));?>
			<?=$form->error($settings, 'max_rooms'); ?>
		</div>
	</div>

	<div class="control-group">
		<?=$form->labelEx($settings, 'max_concurrent_rooms', array('class'=>'control-label'));?>
		<div class="controls">
			<?=$form->textField($settings, 'max_concurrent_rooms', array('class'=>'input-xlarge'));?>
			<?=$form->error($settings, 'max_concurrent_rooms'); ?>
		</div>
	</div>

	<div class="form-actions">
		<input class="btn-docebo green big" type="submit" value="<?= Yii::t('standard', '_CONFIRM') ?>" />
		<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
	</div>

	<?php $this->endWidget(); ?>

</div>

<script type="text/javascript">
	$('input[type=radio]').styler();

	$(function() {
		//clean possible delegated events from previous dialogs
		$(document).undelegate('.modal-edit-webinar-account', "dialog2.content-update");

		//set dialog behaviors on server answer
		$(document).delegate(".modal-edit-webinar-account", "dialog2.content-update", function() {
			var e = $(this), autoclose = e.find("a.auto-close");
			if (autoclose.length > 0) {
				e.find('.modal-body').dialog2("close");
				$.fn.yiiListView.update("webinar-accounts-management-list");
			} else {
				var err = e.find("a.error");
				if (err.length > 0) {
					var msg = $(err[0]).data('message');
					Docebo.Feedback.show('error', msg);
					e.find('.modal-body').dialog2("close");
				}
			}
		});

		<?php
		if($prefillExistingRadioButton){
			echo "$('#ytZoomAppSettingsForm_session_type').val('".$prefillExistingRadioButton."');";
		}
		?>
	});
</script>