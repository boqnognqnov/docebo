<?php

/**
 * Class ZoomMeetingWebinarTool
 * Implements meeting tool for Zoom.us
 */
class ZoomMeetingWebinarTool extends WebinarTool {
	/**
	 * @var bool does the current webinar tool supports grabbing a recording via API call?
	 */
	protected $apiGrabSupported = true;

	/**
	 * Main constructor
	 */
	protected function __construct() {
		$this->id = 'zoommeeting';
		$this->name = 'ZoomMeeting';
	}

	/**
	 * Returns true if the current webinar tools has support for multi account
	 * @return bool
	 */
	public function hasMultiAccountSupport()
	{
		return true;
	}

	/**
	 * Returns the array of accounts configured for the current tool indexed by id
	 * @return WebinarToolAccount[]
	 */
	public function getAccounts()
	{
		$accounts = array();
		if (!$this->accounts) {
			$records = WebinarToolAccount::model()->findAllByAttributes(array('tool' => 'zoommeeting'));
			foreach($records as $record){
				if($record->settings->session_type == "zoommeeting"){
					$accounts[] = $record;
				}
			}
		}
		return $accounts;
	}

	/**
	 * Creates a new room using the Zoom.us api
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param string $roomDescription The description of the room
	 * @param integer $idAccount The id of the account to use to create the room
	 * @param $startDateTime string The start date&time in UTC + MYSQL/ISO format
	 * @param $endDateTime string The end date&time in UTC + MYSQL/ISO format
	 * @param $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @throws CHttpException
	 * @return array "tool specific" params returned by the api
	 */
	public function createRoom($roomName, $roomDescription, $idAccount, $startDateTime, $endDateTime, $maxParticipants, $api_params = array()) {
		$apiClient = new ZoomApiClient($idAccount);
		$meeting = $apiClient->createMeeting($roomName, $roomDescription, $startDateTime, $endDateTime);

		if (!isset($meeting["data"]))
			throw new CHttpException(500, ConferenceManager::$MSG_INVALID_DATA_RETURNED);

		$result = array(
			'name' => $roomName,
			'meeting_id' => $meeting['data']['id'],
			'meeting_uuid' => $meeting['data']['uuid'],
			'join_url' => $meeting['data']['join_url'],
			'start_url' => $meeting['data']['start_url']
		);

		return $result;
	}

	/**
	 * Updates an existing room with the provided params
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param string $roomDescription The description of the room
	 * @param integer $idAccount The id of the account to use to update the room
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @param $startDateTime string The start date&time in UTC + MYSQL/ISO format
	 * @param $endDateTime string The end date&time in UTC + MYSQL/ISO format
	 * @param $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 *
	 * @return array "tool specific" params returned by the api
	 */
	public function updateRoom($roomName, $roomDescription, $idAccount, $api_params = array(), $startDateTime, $endDateTime, $maxParticipants) {
		$apiClient = new ZoomApiClient($idAccount);
		$apiClient->updateMeeting($roomName, $roomDescription, $startDateTime, $endDateTime, $api_params);
		$api_params['name'] = $roomName;
		return $api_params;
	}

	/**
	 * Deletes an existing room
	 *
	 * @param integer $idAccount The id of the account to use to delete the room
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 */
	public function deleteRoom($idAccount, $api_params = array()) {
		$meetingId = $api_params['meeting_id'];
		$apiClient = new ZoomApiClient($idAccount);
		$apiClient->deleteMeeting($meetingId);
	}

	/**
	 * Returns the URL to join a room
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param integer $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param integer $idAccount The id of the account to use
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @param CoreUser $userModel The id of the user for whom the current URL is returned
	 * @param WebinarSession $sessionModel The current session object
	 * @return string;
	 */
	public function getJoinRoomUrl($roomName, $maxParticipants, $idAccount, $api_params, $userModel, $sessionModel, $widgetContext = false) {
		$addGet = '';
		if(!empty($userModel->firstname) || !empty($userModel->lastname)){
			$username = urlencode(trim($userModel->firstname." ".$userModel->lastname));
			$addGet = '?uname='.$username;
		}
		return $api_params['join_url'].$addGet;
	}

	/**
	 * Returns the URL to start a room
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param integer $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param integer $idAccount The id of the account to use
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @param CoreUser $userModel The id of the user for whom the current URL is returned
	 * @param WebinarSession $sessionModel The current session object
	 * @return string
	 */
	public function getStartRoomUrl($roomName, $maxParticipants, $idAccount, $api_params, $userModel, $sessionModel) {
		return $api_params['start_url'];
	}

	/**
	 * @param int $idAccount $idAccount The id of the account to use
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @return array
	 */
	public function getApiRecordings($idAccount, $api_params){
		$return = array(
			'error' => '',
			'recordingUrl' => ''
		);

		$apiClient = new ZoomApiClient($idAccount);
		$result = $apiClient->getMeetingRecording($api_params['meeting_uuid']);

		if(!empty($result['data']['recording_files'])){
			foreach($result['data']['recording_files'] as $recordingFile){
				if($recordingFile['file_type'] == 'MP4'){
					$return['recordingUrl'] = $recordingFile['download_url'];
				}
			}
			if(empty($return['recordingUrl'])){
				$return['error'] = Yii::t('webinar', 'Could not find a MP4 file for this session');
			}
		} else {
			$return['error'] = Yii::t('webinar', 'Could not find a recording for this session');
		}
		return $return;
	}
}