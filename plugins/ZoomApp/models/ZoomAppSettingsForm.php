<?php

/**
 * BlueJeansAppSettingsForm class.
 *
 * @property string $account_name
 * @property string $max_rooms
 * @property string $max_rooms_per_course
 * @property string $app_key
 * @property string $app_secret
 * @property string $access_token
 * @property string $account_email
 * @property string $session_type
 * @property string $max_concurrent_rooms
 * @property string $additional_info
 */
class ZoomAppSettingsForm extends CFormModel {

	public $account_name;
	public $max_rooms;
	public $max_rooms_per_course;
	public $additional_info;
	public $app_key;
	public $app_secret;
	public $access_token;
	public $account_email;
	public $session_type;
	public $max_concurrent_rooms;


	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			array('account_name, max_rooms, max_rooms_per_course, additional_info, app_key, app_secret, access_token, account_email, session_type, max_concurrent_rooms', 'safe'),
			array('account_name, max_rooms, max_rooms_per_course, app_key, app_secret, account_email, session_type, max_concurrent_rooms', 'required'),
			array('max_rooms, max_rooms_per_course, max_concurrent_rooms', 'numerical'),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array(
			'max_rooms' => Yii::t('configuration','Max total rooms'),
			'max_rooms_per_course' => Yii::t('configuration','Max rooms per course'),
			'max_concurrent_rooms' => Yii::t('configuration','Max concurrent rooms'),
			'additional_info' => Yii::t('webinar', 'Additional information'),
			'app_key' => Yii::t('configuration','App key'),
			'app_secret' => Yii::t('configuration','App secret'),
			'access_token' => Yii::t('webinar','Access token'),
			'account_email' => Yii::t('webinar','Account email'),
			'meeting_type' => Yii::t('webinar','Meeting type'),
			'account_name' => Yii::t('webinar', 'Account Name')
		);
	}
}
