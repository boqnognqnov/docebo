<?php
/**
 * Blue Jeans Video communications - http://zoom.us/
 * API Client
 *
 * @author Kurt De Ridder
 *
 * Copyright (c) 2016 Docebo
 * http://www.docebo.com
 *
 */
class ZoomApiClient extends CComponent {

	// Can be used to debug the traffic
	public $proxyHost = false;
	public $proxyPort = false;

	// will hold the EHttpClient
	private $_client = null;

	// MUST BE HTTPS: !!
	private $_baseDomain = 'https://api.zoom.us/v1';

	// Will be taken from settings
	private $_app_key = '';
	private $_app_secret = '';
	private $_access_token = '';
	private $_account_email = '';

	const ZOOM_MEETING_TYPE_INSTANT = 1;
	const ZOOM_MEETING_TYPE_SCHEDULED = 2;
	const ZOOM_MEETING_TYPE_RECURRING = 3;

	const ZOOM_WEBINAR_TYPE_SCHEDULED = 5;
	const ZOOM_WEBINAR_TYPE_RECURRING = 6;

	const ZOOM_LOGIN_TYPE_FACEBOOK = 0;
	const ZOOM_LOGIN_TYPE_GOOGLE = 1;
	const ZOOM_LOGIN_TYPE_API = 99;
	const ZOOM_LOGIN_TYPE_WORK = 100;
	const ZOOM_LOGIN_TYPE_SSO = 101;

	public function __construct($idAccount = null) {
		// If the new parameters are not passed, try to load the default webinar tools account
		if(!$idAccount) {
			$account = WebinarToolAccount::model()->findByAttributes(array('tool' => 'zoommeeting'));
			if($account) {
				$idAccount = $account->id_account;
			}
		}
		$this->init($idAccount);
	}

	/**
	 *
	 * @throws Exception
	 */
	public function init($idAccount = null) {

		Yii::import('common.extensions.httpclient.*');

		if(empty($idAccount)){
			throw new Exception("App key not set");
		}

		$account = WebinarToolAccount::model()->findByPk($idAccount);

		$this->_app_key = $account->settings->app_key;
		$this->_app_secret = $account->settings->app_secret;
		$this->_access_token = $account->settings->access_token;
		$this->_account_email = $account->settings->account_email;
		if (empty($this->_app_key)) {
			throw new Exception("Credentials not set");
		}
	}

	/**
	 * @param string $actionPath
	 * @param array $params
	 * @throws Exception
	 * @return array
	 */
	private function call($actionPath, $params = array()) {
		// Build URL
		$url = $this->_baseDomain . $actionPath;
//		var_dump($url);

		$this->_client = new EHttpClient($url, array(
			'adapter'      => 'EHttpClientAdapterCurl',
			'proxy_host'   => $this->proxyHost,
			'proxy_port'   => $this->proxyPort,
			'disable_ssl_verifier' => true,
		));

		// Add mandatory params
		$params['api_key'] = $this->_app_key;
		$params['api_secret'] = $this->_app_secret;
		$params['data_type'] = 'JSON';

//		var_dump($params);//die;

//		// Add headers
//		$json_data = CJSON::encode($params);
//		$this->_client->setHeaders('Content-Length', strlen($json_data));
//		$this->_client->setHeaders('Accept', "application/json");
//		$this->_client->setHeaders('Content-Type', "application/json");

		$this->_client->setParameterPost($params);

		// Make the call, get the response
		$response = $this->_client->request('POST');
//		var_dump($response);die;

		if($response){
			Yii::log('Sending params to Zoom API: '.print_r($params,true), 'debug');
		}

		// Check if we've got an error; throw exception with error message
		if ($this->_client->getLastResponse()->isError()) {
			throw new Exception($this->_client->getLastResponse()->getMessage(), $this->_client->getLastResponse()->getStatus());
		}

		if (!$response) {
			throw new Exception('Invalid response from server');
		}

		// Decode response to an array
		$body = $response->getBody();
		// If we get body, it must be JSON and be an array
		$response_data = array();
		if ($body) {
			$response_data = CJSON::decode($body);
			if(isset($response_data['error'])){
				throw new Exception($response_data['error']['message'], $response_data['error']['code']);
			}

			if ( ($response_data === null) || !is_array($response_data) ) {
				throw new Exception('Invalid data format received from server');
			}
		}

		// Always add response status/message
		$result["response_status"] = $this->_client->getLastResponse()->getStatus();
		$result["response_message"] = $this->_client->getLastResponse()->getMessage();
		$result["data"] = $response_data;

		// Return array
		return $result;
	}

	/**
	 * Create a meeting
	 *
	 * @param string $roomName
	 * @param string $roomDescription
	 * @param string $startDateTime
	 * @param string $endDateTime
	 * @return array
	 */
	public function createMeeting($roomName, $roomDescription, $startDateTime, $endDateTime) {
		$startTime = strtotime($startDateTime);
		$endTime = strtotime($endDateTime);

		$startDateTime = gmdate('Y-n-j\TH:i:s\Z', $startTime);
		$duration = round(($endTime - $startTime) / 60);

		$hostId = $this->getUserIdByEmail($this->_account_email);

		$params = array(
			'host_id' => $hostId,
			'topic' => $roomName,
			'type' => self::ZOOM_MEETING_TYPE_SCHEDULED,
			'start_time' => $startDateTime,
			'duration' => $duration,
			'timezone' => CoreSetting::getEntity('timezone_default')->param_value,
		);

		$result = $this->call('/meeting/create', $params);
		return $result;
	}

	/**
	 * Create a meeting
	 *
	 * @param string $roomName
	 * @param string $roomDescription
	 * @param string $startDateTime
	 * @param string $endDateTime
	 * @return array
	 */
	public function createWebinar($roomName, $roomDescription, $startDateTime, $endDateTime) {
		$startTime = strtotime($startDateTime);
		$endTime = strtotime($endDateTime);

		$startDateTime = gmdate('Y-n-j\TH:i:s\Z', $startTime);
		$duration = round(($endTime - $startTime) / 60);

		$hostId = $this->getUserIdByEmail($this->_account_email);

		$params = array(
			'host_id' => $hostId,
			'topic' => $roomName,
			'agenda' => $roomDescription,
			'type' => self::ZOOM_WEBINAR_TYPE_SCHEDULED,
			'start_time' => $startDateTime,
			'duration' => $duration,
			'timezone' => CoreSetting::getEntity('timezone_default')->param_value,
		);

		$result = $this->call('/webinar/create', $params);
		return $result;
	}

	/**
	 * Update a meeting
	 *
	 * @param string $roomName
	 * @param string $roomDescription
	 * @param string $startDateTime
	 * @param string $endDateTime
	 * @param array $api_params
	 * @throws Exception
	 * @return array
	 */
	public function updateMeeting($roomName, $roomDescription, $startDateTime, $endDateTime, $api_params) {
		$startTime = strtotime($startDateTime);
		$endTime = strtotime($endDateTime);

		$startDateTime = gmdate('Y-n-j\TH:i:s\Z', $startTime);
		$duration = round(($endTime - $startTime) / 60);

		$hostId = $this->getUserIdByEmail($this->_account_email);

		$params = array(
			'id' => $api_params['meeting_id'],
			'host_id' => $hostId,
			'topic' => $roomName,
			'start_time' => $startDateTime,
			'duration' => $duration
		);

		$result = $this->call('/meeting/update', $params);
		return $result;
	}

	/**
	 * Update a meeting
	 *
	 * @param string $roomName
	 * @param string $roomDescription
	 * @param string $startDateTime
	 * @param string $endDateTime
	 * @param array $api_params
	 * @throws Exception
	 * @return array
	 */
	public function updateWebinar($roomName, $roomDescription, $startDateTime, $endDateTime, $api_params) {
		$startTime = strtotime($startDateTime);
		$endTime = strtotime($endDateTime);

		$startDateTime = gmdate('Y-n-j\TH:i:s\Z', $startTime);
		$duration = round(($endTime - $startTime) / 60);

		$hostId = $this->getUserIdByEmail($this->_account_email);

		$params = array(
			'id' => $api_params['webinar_id'],
			'host_id' => $hostId,
			'topic' => $roomName,
			'start_time' => $startDateTime,
			'duration' => $duration,
			'agenda' => $roomDescription
		);

		$result = $this->call('/webinar/update', $params);
		return $result;
	}

	/**
	 * Deletes a meeting by ID
	 *
	 * @param string $roomId
	 * @throws Exception
	 * @return array
	 */
	public function deleteMeeting($roomId) {
		$roomId = trim($roomId);
		if (empty($roomId)) {
			throw new Exception('Invalid meeting Id provided');
		}
		try {
			$hostId = $this->getUserIdByEmail($this->_account_email);

			$params = array(
				'id' => $roomId,
				'host_id' => $hostId
			);
			$result = $this->call('/meeting/delete', $params);
		} catch(Exception $e) {
			if($e->getCode() == 404) { //already deleted
				Yii::log('Zoom API error: '.$e->getMessage());
				return true;
			}else
				throw new Exception($e->getMessage(), $e->getCode());
		}
		return $result;
	}

	/**
	 * Deletes a webinar by ID
	 *
	 * @param string $roomId
	 * @throws Exception
	 * @return array
	 */
	public function deleteWebinar($roomId) {
		$roomId = trim($roomId);
		if (empty($roomId)) {
			throw new Exception('Invalid webinar Id provided');
		}
		try {
			$hostId = $this->getUserIdByEmail($this->_account_email);

			$params = array(
				'id' => $roomId,
				'host_id' => $hostId
			);
			$result = $this->call('/webinar/delete', $params);
		} catch(Exception $e) {
			if($e->getCode() == 404) { //already deleted
				Yii::log('Zoom API error: '.$e->getMessage());
				return true;
			}else
				throw new Exception($e->getMessage(), $e->getCode());
		}
		return $result;
	}

	/**
	 * Returns the "join url" for the passed user. The API first register the user to the webinar.
	 * @param  array $api_params
	 * @param CoreUser $userModel
	 * @return string
	 */
	public function getJoinWebinarUrl($api_params, $userModel) {
		if(!empty($userModel->email) && !empty($userModel->firstname) && !empty($userModel->lastname)) {
			$params = array(
				'id' => $api_params['webinar_id'],
				'email' => $userModel->email,
				'first_name' => $userModel->firstname,
				'last_name' => $userModel->lastname
			);

			$result = $this->call('/webinar/register', $params);
			$url = (!empty($result['data']['join_url']))?$result['data']['join_url']:$api_params['join_url'];
		} else {
			$url = $api_params['join_url'];
		}
		return $url;
	}

	public function getMeetingRecording($meetingUuid){
		$errors = array();
		try{
			if(empty($meetingUuid)){
				throw new Exception('Invalid meeting Id provided');
			}

			$params = array(
				'meeting_id' => trim($meetingUuid)
			);

			$result = $this->call('/recording/get', $params);
			return $result;
		} catch(Exception $e) {
			$errors[] = $e->getMessage();
			return $errors;
		}
	}

	public function getWebinarRecording($webinarUuid){
		$errors = array();
		try{
			if(empty($webinarUuid)){
				throw new Exception('Invalid webinar Id provided');
			}

			$params = array(
				'meeting_id' => trim($webinarUuid)
			);

			$result = $this->call('/recording/get', $params);
			return $result;
		} catch(Exception $e) {
			$errors[] = $e->getMessage();
			return $errors;
		}
	}

	/**
	 * @param string $email
	 * @return string $id
	 */
	public function getUserIdByEmail($email = null){
		if(!empty($email)){
			$params = array(
				'email' => $email,
				'login_type' => self::ZOOM_LOGIN_TYPE_WORK
			);
			$result = $this->call('/user/getbyemail', $params);
			return $result['data']['id'];
		} else {
			return false;
		}
	}
}

?>