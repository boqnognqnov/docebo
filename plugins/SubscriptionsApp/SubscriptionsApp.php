<?php

/**
 * Plugin for SubscriptionsApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class SubscriptionsApp extends DoceboPlugin
{

    // Override parent
    public static $HAS_SETTINGS = false;

    // Name of the plugin; needed to identify plugin folder and entry script
    public $plugin_name = 'SubscriptionsApp';

    /**
     * This will allow us to access "statically" (singleton) to plugin methods
     * @param string $class_name
     * @return Plugin Object
     */
    public static function plugin($class_name = __CLASS__)
    {
        return parent::plugin($class_name);
    }

    public function activate()
    {
        parent::activate();
    }

    public function deactivate()
    {
        parent::deactivate();
    }
}
