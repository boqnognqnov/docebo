<style>
<!--
	.dashlet-learningplan-settings .initial-display-mode-radio label {
		display: inline-block;
	}
-->
</style>


<?php
	/* @var $this DashletWidget */

	$selectedInitialDisplayStyle 	= isset($this->params['initial_display_style']) ? $this->params['initial_display_style'] : DashletMyCourses::DISPLAY_STYLE_THUMB;
	$selectedInitialStatusFilter	= isset($this->params['initial_status_filter']) ? $this->params['initial_status_filter'] : DashletMyCourses::STATUS_ACTIVE;

	$selectedSortingFields			= $this->getParam('selected_sorting_fields', array());
	$availSortingFields 			= DashletLearningPlan::getAvailableSortingFields();
	$fieldsToRestore = array();

	if (is_array($selectedSortingFields)) {
		foreach ($selectedSortingFields as $field) {
			$fieldsToRestore[$field] = $availSortingFields[$field];
		}
	}

	$displayStyles 	= DashletLearningPlan::getDisplayStyles();
	$statusesList	= DashletLearningPlan::getStatusesList();
?>

<div class="dashlet-learningplan-settings">

	<div class="row-fluid">
		<div class="row-fluid">
			<div class="span12">
				<label><?= Yii::t('dashboard', '<strong>Start with the following</strong> filters view') ?></label>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span3"><?= Yii::t('dashboard','Display mode') ?></div>
			<div class="span9">
				<div class="initial-display-mode-radio">
					<?php
						echo CHtml::radioButtonList('initial_display_style', $this->getParam('initial_display_style', DashletLearningPlan::DISPLAY_STYLE_THUMB), $displayStyles, array(
							'separator' => '',
						));
					?>
                </div>
                <label class="setting-description compact"><?= Yii::t('dashboard', '\'Thumbnails\' is available for <strong>one or two</strong> columns layout only') ?></label>
                <br>
			</div>

		</div>
	</div>

	<div class="row-fluid">
		<div class="span3"><?= Yii::t('standard','_STATUS') ?></div>
		<div class="span9">
			<?php
				echo CHtml::dropDownList('initial_status_filter', $selectedInitialStatusFilter, $statusesList, array(
					'class'	=> 'span12',
				));
			?>
		</div>
	</div>

	<hr>


	<div class="row-fluid">
		<div class="span12">
			<label class="checkbox">
				<?php
					echo CHtml::checkBox('show_filter_and_switch', $this->getParam('show_filter_and_switch'));
					echo Yii::t('dashboard', 'Show <strong>filters</strong> selector and <strong>thumbnail/list</strong> view switch');
				?>
			</label>
			<div class="setting-description styler indented"><?= Yii::t('dashboard', 'Allow users to change the default view and LP display mode') ?></div>
		</div>
	</div>
	<br>

	<div class="row-fluid">
		<div class="span12">
			<label class="checkbox">
				<?php
				echo CHtml::checkBox('show_search', $this->getParam('show_search'));
				echo Yii::t('dashboard', 'Show <strong>search</strong> bar');
				?>
			</label>
			<div class="setting-description styler indented"><?= Yii::t('dashboard', 'Allow users to search LP') ?></div>
		</div>
	</div>

	<hr>

	<div class="row-fluid">
		<div class="span12">
			<p><?=Yii::t('social', 'Sort by')?></p>
			<div id="selected-sorting-fields">
				<select name="selected_sorting_fields" multiple="multiple">
					<? if(count($fieldsToRestore)): ?>
						<? foreach($fieldsToRestore as $key=>$value): ?>
							<option selected="selected" class="selected" value="<?=$key?>"><?=$value?></option>
						<? endforeach; ?>
					<? endif; ?>
				</select>
			</div>
		</div>
	</div>
	<br>
	<hr>

	<div class="row-fluid">
		<div class="span12">
				<?php
					$n = isset($this->params['display_number_items']) ? $this->params['display_number_items'] : 10;
					echo Yii::t('dashboard', 'Display') . "&nbsp;&nbsp;";
					echo CHtml::textField('display_number_items', $n, array(
						'class' => 'span3',
					));
					echo '&nbsp;&nbsp;' . Yii::t('dashboard', 'items in widget');
				?>
				<br>
				<span class="setting-description compact">&nbsp;<?= Yii::t('dashboard', 'Leave blank to display all items') ?></span>
		</div>
	</div>

	<br>

	<div class="row-fluid">
		<div class="span12">
			<label class="checkbox">
				<?php
					echo CHtml::checkBox('show_view_all_link', $this->getParam('show_view_all_link'));
					echo Yii::t('dashboard', 'Show "view all learning plans" link');
				?>
			</label>
		</div>
	</div>

</div>

<script type="text/javascript">
	$(function(){

		var selectElem = $('select[name="selected_sorting_fields"]');

		selectElem.fcbkcomplete({
			json_url: <?= json_encode(Docebo::createAbsoluteLmsUrl('//mydashboard/DashletWidgets/axLPSortingFieldsAutocomplete')) ?>,
			addontab: false, // <==buggy
			width: '98.5%',
			cache: false,
			complete_text: '',
			maxshownitems: 8,
			input_min_size: -1,
			input_name: 'maininput-name',
			filter_selected: true,
			onselect : function(){
				//console.log(arguments);
			}
		});

	});
</script>

