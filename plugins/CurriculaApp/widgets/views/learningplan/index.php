<?php
/* @var $this  DashletLearningPlan */

	// Width
	switch ($this->dashletModel->dashboardCell->width) {
		case 8:	
			$dashletWidth = "twothird";
			break;
		case 6:
			$dashletWidth = "half";
			break;
		case 4:
			$dashletWidth = "onethird";
			break;
		case 12 :
		default:	
			$dashletWidth = "full";
			break;
	}
	
	// Initial display style
	$displayStyleSwitchIcon = "fa-list";
	$thumbClass = "tb";
	$displayStyle = $this->getParam('initial_display_style', 'thumb');
	// However, cookies are overriding
	$cookieName = 'dashlet_display_style_' . $this->dashletModel->id;
	if(isset(Yii::app()->request->cookies[$cookieName]))
		$displayStyle = Yii::app()->request->cookies[$cookieName];
	
	switch ($displayStyle) {
		case "list":
			$displayStyleSwitchIcon = "fa-th";
			$thumbClass = "";
			break;
				
		case "thumb":
		default:
			$displayStyleSwitchIcon = "fa-list";
			$thumbClass = "tb";
			break;	
	}

	// However, if the WIDTH is onethird (4), we switch to list
	if ($dashletWidth == "onethird") {
		$thumbClass = "";
	}
	
	// View all courses link
	$showViewAllCourses = (bool) $this->getParam('show_view_all_link', false);
	$viewAllCoursesUrl = Docebo::createLmsUrl('curricula/show', array('sop' => 'unregistercourse'));

?>


<div class="w-mycourses w-learningplans <?= $dashletWidth ?> <?= !$this->getParam('show_search') ? 'no-search' : null; ?> <?= !$this->getParam('show_filter_and_switch') ? 'no-filter-no-switch' : null; ?>">

	<?php 
		echo CHtml::beginForm('', 'POST', array(
			'name' 		=> 'mycourses-filter-form-' . $this->dashletModel->id,
			'class'		=> 'mycourses-filter-form',
			'id'		=> 'mycourses-filter-form-' . $this->dashletModel->id,		
		));
	?>


	<div class="row-fluid filter-header">
		<div class="filters-button"><?= Yii::t('dashboard', 'Filters') ?></div>
		<div class="search-button"><i class="fa fa-search fa-lg"></i></div>
		<div class="search-field">
			<button type="button" class="close clear-search">&times;</button>
			<?= CHtml::textField('search_input', $_REQUEST['search_input']) ?>
			
		</div>
		<div class="display-style-switch"><i class="fa <?= $displayStyleSwitchIcon ?> fa-lg view-switch"></i></div>
	</div>


	<div class="course-list lps-list <?= $thumbClass ?> <?= $dashletWidth ?>">
	
		<?php foreach ($lps as $lp) : ?>
			<?php 
				$this->render('learningplan/course_tile', array(
					'lp'	=> $lp,
				));
			?>
		<?php endforeach; ?>
		
	</div>
	
	<div class="filters-selector">
		<div class="row-fluid">
			<label><strong><?= Yii::t('standard', 'Status') ?></strong></label>
			<? foreach(DashletLearningPlan::getStatusesList() as $key=>$label): ?>
				<label class="radio">
					<input <?=($key==$currentStatus ? 'checked' : null);?> type="radio" name="course_type" value="<?=$key?>"><?=$label?>
				</label>
			<? endforeach; ?>
		</div>
	</div>
	
	
	<div class="clearfix"></div>
	
	<?= CHtml::endForm() ?>
	
</div>


<!-- Use .dl-footer as a special class. dashboard plugin will put the WHOLE element in the footer zone -->
<div class="dl-footer">
	<div class="span6 dl-footer-left 	text-left"></div>
	<?php if ($showViewAllCourses) : ?>
	<div class="span6 dl-footer-right 	text-right"><a href="<?= $viewAllCoursesUrl ?>"><?= Yii::t('dashboard', 'View all learning plans') ?></a></div>
	<?php endif; ?>
</div>



<script type="text/javascript">
/*<![CDATA[*/

	$(function(){
		var thisDashlet = $('[id="dashlet-<?= json_encode((int) $this->dashletModel->id) ?>"]');

		// Attach "mycourses" jquery plugin to THIS VERY dashlet.
        thisDashlet.mycourses({
       		dashControllerUrl	: <?= json_encode(Docebo::createLmsUrl('//mydashboard/dash')) ?>,            
			adminLayoutsMode: false
       });
        
	});           

           
/*]]>*/
</script>
