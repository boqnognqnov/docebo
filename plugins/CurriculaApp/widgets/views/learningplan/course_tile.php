<?php

	$playUrl = Docebo::createLmsUrl("curricula/show", array(
		'sop'	=> 'unregistercourse',
		'id_path'=>$lp['id_path'],
	));
	
	// Resolve how and if the course can be played, enrolled, etc. and return data for labels, icons, classes (presentation data)
	$lpModel = LearningCoursepath::model()->findByPk($lp['id_path']);
	$accessData = $lpModel->resolvePlanAccess();
	
	
?>


<div class="course-tile">


	
	<?php 
		if ($accessData['linkTag']) { 
			echo $accessData['linkTag']; 
		}  
	?>
	
		<div class="course-logo">
			<div class="thumb">
			
				<!-- tags: in thumbnail -->
				<?php if (count($accessData['tags']) > 0): ?>
					<div class="course-tags">
						<?php foreach ($accessData['tags'] as $tag) : ?>
							<span class="course-tag <?= $tag['class'] ?>" rel="tooltip" title="<?= $tag['tooltip'] ?>"><?= $tag['label'] ?></span>
						<?php endforeach;?>
					</div>
				<?php endif; ?>
				
	
				<? $logoUrl = Docebo::getRootBaseUrl(true) . '/themes/spt/images/course/course_nologo.png'; ?>
				
				<? if($lp['img']): ?>
					<?
					$asset = CoreAsset::model()->findByPk($lp['img']);
					$logoUrl = null;
					if($asset){
						$logoUrl =  $asset->getUrl(CoreAsset::VARIANT_ORIGINAL);
					}
					?>
				<? endif;
	
				if (stripos($logoUrl, 'http:') === 0) {
					$logoUrl = str_ireplace('http:', '', $logoUrl);
				}
				?>
				<?=CHtml::image($logoUrl, 'Logo')?>
			</div>
	
			<p class="course-action <?= $accessData['action']['class'] ?>">
			
			
				<!-- Course tags: in action label -->
				<?php if (count($accessData['tags']) > 0): ?>
					<span class="course-tags">
						<?php foreach ($accessData['tags'] as $tag) : ?>
							<span class="course-tag <?= $tag['class'] ?>"><?= $tag['label'] ?></span>
						<?php endforeach;?>
					</span>
				<?php endif; ?>
				
	
				<span class="icon">
					<i class="<?= $accessData['action']['icon'] ?>"></i>
				</span>
				<span class="action-label"><?= $accessData['action']['label'] ?></span>
			</p>
			
			<h3 class="course-name"><?= $lp['path_name'] ?></h3>
		</div>
		<div class="clearfix"></div>
		
	<?php 
		if ($accessData['linkTag']) {
			echo "</a>";
		}	
	?>
			
	
	<div class="course-info">
		<span class="lp-label long">
			<?=Yii::t('standard', '_COURSEPATH')?>
		</span>
		<span class="lp-label short">
			<?=Yii::t('standard', 'LP')?>
		</span>
		|
		<?=Yii::t('standard', '{x} courses', array('{x}'=>intval($lp['totalCourses'])))?>
	</div>
	

</div>
