<?php

class DashletLearningPlan extends DashletWidget implements IDashletWidget {
	
	const DISPLAY_STYLE_THUMB 	= 'thumb';
	const DISPLAY_STYLE_LIST 	= 'list';
	
	const STATUS_ALL 			= 'all';
	const STATUS_ACTIVE 		= 'active';
	const STATUS_COMPLETED 		= 'completed';
	
	const LABEL_FILTER_ALL		= 'all';
	
	
	public static function getDisplayStyles() {
		return array(
			self::DISPLAY_STYLE_THUMB 	=> Yii::t('dashboard', 'Thumbnails'),
			self::DISPLAY_STYLE_LIST 	=> Yii::t('dashboard', 'List'),
		);
	}
	

	public static function getStatusesList() {
		return array(
				self::STATUS_ALL 		=> Yii::t('standard', '_ALL'),
				self::STATUS_ACTIVE 	=> Yii::t('dashboard', 'Active learning plans'),
				self::STATUS_COMPLETED	=> Yii::t('standard', 'Completed learning plans'),
		);
	}

	/**
	 *
	 * @param int    $limit
	 *
	 * @param string $status
	 *
	 * @return array
	 */
	public function getListOfLearningplans($limit = -1, $status = self::STATUS_ALL) {

		$params = array();
		
		$command = Yii::app()->getDb()->createCommand()
				->select('lp.id_path, lp.path_name, lp.img, COUNT(cpc.id_item) AS totalCourses, SUM(cu.status=0) AS nonStartedCourses, SUM(cu.status=2) AS completedCourses')
				->from(LearningCoursepath::model()->tableName().' lp')
				->join(LearningCoursepathUser::model()->tableName().' u', '(lp.id_path=u.id_path) AND (u.idUser=:idUser)')
				->leftJoin(LearningCoursepathCourses::model()->tableName().' cpc', 'lp.id_path=cpc.id_path')
				->leftJoin(LearningCourseuser::model()->tableName().' cu', 'cpc.id_item=cu.idCourse AND cu.idUser=:idUser')
				->group('lp.id_path')
				->order('lp.path_name ASC');

		$params = array(
			':idUser'	=> 	Yii::app()->user->getIdst(),
		);
		
		if ($_REQUEST['search_input']) {
			$command->andWhere('path_name LIKE :search OR path_descr LIKE :search');
			$params[':search'] = "%" . $_REQUEST['search_input'] . "%";
		}


		// ORDERING
		$sortingFields = $this->getParam('selected_sorting_fields');
		if (!empty($sortingFields))
			$command->order = implode(',', $sortingFields);

		$lps = $command->queryAll(true, $params);
		
		switch($status){
			case self::STATUS_ACTIVE:
				foreach($lps as $key=>$lp){
					if($lp['totalCourses']==0 || $lp['totalCourses'] <= $lp['completedCourses']){
						unset($lps[$key]);
					}
				}
				break;
			case self::STATUS_COMPLETED:
				foreach($lps as $key=>$lp){
					if($lp['totalCourses']==0 || $lp['totalCourses'] > $lp['completedCourses']){
						unset($lps[$key]);
					}
				}
				break;
			case self::STATUS_ALL: // no status filtering, show Completed and In progress
				break;
		}

		// If there is some limit requested, return only part ofthe result
		if ($limit) {
			return array_slice($lps, 0, $limit);
		}
		
		return $lps;
	}
	
	/**
	 * 
	 */
	public static function descriptor($params=false) {
		if(!PluginManager::isPluginActive('CurriculaApp')){
			return false;
		}

		$sampleImageUrl = Yii::app()->mydashboard->getModule()->getAssetsUrl() . "/images/mylp_sample.jpg";

		$descriptor = new DashletDescriptor();
		
		$descriptor->name 					= 'core_dashlet_learningplan';
		$descriptor->handler				= 'plugin.CurriculaApp.widgets.DashletLearningPlan';
		$descriptor->title					= Yii::t('dashlets', 'My Learning Plans');
		$descriptor->description			= Yii::t('dashlets', 'Shows the list of Learning Plans the user is enrolled in');
		$descriptor->sampleImageUrl 		= $sampleImageUrl;
		$descriptor->settingsFieldNames		= array(
			'initial_display_style',
			'initial_status_filter',
			'show_filter_and_switch',
			'show_search',
			'display_number_items',
			'show_view_all_link',
			'selected_sorting_fields',
		);
		
		return $descriptor;
	}
	
	
	/**
	 * @see IDashletWidget::renderSettings()
	 */
	public function renderSettings() {
		$cs = Yii::app()->getClientScript();
		$themeUrl = Yii::app()->theme->baseUrl;

		// FCBK
		$cs->registerCssFile($themeUrl . '/css/fcbkcomplete.css');
		$cs->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js');


		$this->render('learningplan/settings');
	}
	

	/**
	 * @see IDashletWidget::renderFrontEnd()
	 */
	public function renderFrontEnd() {
		if ($this->dashletModel && PluginManager::isPluginActive('CurriculaApp')) {
			$currentStatus = isset($_REQUEST['course_type']) && in_array($_REQUEST['course_type'], array_keys(self::getStatusesList())) ? $_REQUEST['course_type'] : $this->getParam('initial_status_filter');

			$this->render( 'learningplan/index', array(
				'currentStatus'=>$currentStatus,
				'lps'=>$lps = $this->getListOfLearningplans($this->getParam('display_number_items'), $currentStatus),
			) );
		}else {
			echo "Curricula app not active";
		}
	}
	
	
	/**
	 * @see IDashletWidget::bootstrap()
	 */
	public function bootstrap($params=false) {
	}

	/**
	 * Return available sorting fields and their labels for Settings UI
	 *
	 * @return array
	 */
	public static function getAvailableSortingFields(){
		return array(
			'lp.path_name'					=> Yii::t('certificate', 'Learning Plan Name').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			'lp.path_name DESC'				=> Yii::t('certificate', 'Learning Plan Name').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
			'lp.path_code'					=> Yii::t('standard', '_CODE').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			'lp.path_code DESC'				=> Yii::t('standard', '_CODE').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
			'lp.create_date'				=> Yii::t('report', '_CREATION_DATE').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			'lp.create_date DESC' 			=> Yii::t('report', '_CREATION_DATE').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
			'u.date_assign'					=> Yii::t('report', '_DATE_INSCR').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			'u.date_assign DESC' 			=> Yii::t('report', '_DATE_INSCR').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
		);
	}
		
}


