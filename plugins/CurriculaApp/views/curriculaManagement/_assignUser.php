<div id="grid-wrapper" class="user-table">
	<h2><?php echo Yii::t('standard', 'Enroll users'); ?></h2>
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'user-grid',
		'htmlOptions' => array('class' => 'grid-view clearfix'),
		'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
		'dataProvider' => $model->dataProvider(),
		'cssFile' => false,
		'columns' => array(
			array(
				'class' => 'CCheckBoxColumn',
				'selectableRows' => 2,
				'id' => 'user-grid-checkboxes',
				'value' => '$data->idst',
				'checked' => '$data->inSessionList(\'selectedItems\', \'idst\');',
			),
			array(
				'header' => Yii::t('standard', '_USERNAME'),
				'name' => 'userid',
				'value' => 'Yii::app()->user->getRelativeUsername($data->userid);',
				'type' => 'raw',
			),
			array(
				'name' => 'fullname',
				'header' => Yii::t('admin_directory', '_DIRECTORY_FULLNAME'),
				'value' => '$data->fullname',
				'type' => 'raw',
			),
			'email',
		),
		'template' => '{items}{summary}{pager}',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'pager' => array(
			'class' => 'ext.local.pagers.DoceboLinkPager',
		),
		'beforeAjaxUpdate' => 'function(id, options) {
							options.type = "POST";
							if (options.data == undefined) {
								options.data = {
									contentType: "html",
									selectedItems: $(\'#\'+id+\'-selected-items-list\').val()
								}
							}
						}',
		'ajaxUpdate' => 'user-grid-all-items',
		'afterAjaxUpdate' => 'function(id, data){$("#user-grid input").styler(); afterGridViewUpdate(id);}',
	)); ?>
</div>
<script type="text/javascript">
	$(function() {
		$("#user-grid input").styler();
		afterGridViewUpdate('user-grid');
	});
</script>