<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'label_form',
	)); ?>

	<p><?php echo Yii::t('standard', '_UNASSIGN').': "'.count($courses).' '.Yii::t('standard', '_COURSES').'"'; ?></p>

	<div class="clearfix">
		<?php echo CHtml::checkBox('delete_confirm', false, array('onchange' => $onChange)); ?>
		<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed', array(count($courses), '{coursename}' => $courses[0]->idItem->name)), 'delete_confirm'); ?>
	</div>
	<?php foreach ($courses as $course) {
		echo CHtml::hiddenField('id_item[]', $course->id_item);
	} ?>

	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
	$(function(){
		updateSelectAllCounter('curricula-course-list',"");
		$.fn.yiiListView.update('curricula-course-list');
	});
</script>