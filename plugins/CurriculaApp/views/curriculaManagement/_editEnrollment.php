<?
/* @var $model LearningCoursepathUser */
?>
<h3><?php echo Yii::t('course_management', '_ENROLLMENT_EDIT', array($model->user->fullName)); ?></h3>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'enrollment_form',
	)); ?>

	<div class='input-append date clearfix' data-date-format='<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>' data-date-language='<?= Yii::app()->getLanguage() ?>'>

		<div>
			<?php echo CHtml::textField(CHtml::activeName($model, 'date_begin_validity'), ($model->date_begin_validity != '0000-00-00 00:00:00' ? Yii::app()->localtime->stripTimeFromLocalDateTime($model->date_begin_validity) : '')); ?>
			<span class="add-on"><i class="icon-calendar"></i></span>
			<?php echo CHtml::link(Yii::t('standard', '_RESET'), 'javascript:void(0);', array(
				'class' => 'clear-input',
				'data-input-id' => chtml::activeId($model, 'date_begin_validity'),
			)); ?>
		</div>
		<?php echo $form->label($model, 'date_begin_validity'); ?>
	</div>



	<div class='input-append date clearfix' data-date-format='<?= Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) ?>' data-date-language='<?= Yii::app()->getLanguage() ?>'>

		<div>
			<?php echo CHtml::textField(CHtml::activeName($model, 'date_end_validity'), ($model->date_end_validity != '0000-00-00 00:00:00' ? Yii::app()->localtime->stripTimeFromLocalDateTime($model->date_end_validity) : '')); ?>
			<span class="add-on"><i class="icon-calendar"></i></span>
			<?php echo CHtml::link(Yii::t('standard', '_RESET'), 'javascript:void(0);', array(
				'class' => 'clear-input',
				'data-input-id' => chtml::activeId($model, 'date_end_validity'),
			)); ?>
		</div>
		<?php echo $form->label($model, 'date_end_validity'); ?>
	</div>


	<?php $this->endWidget(); ?>
</div>

<div class="modal-footer">
	<input class="btn confirm-btn" type="button" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" name="submit" data-submit-form="enrollment_form"/>
	<input class="btn close-btn" type="button" value="<?php echo Yii::t('standard', '_CLOSE'); ?>"/>
</div>
<script type="text/javascript">
	$(function(){
		$('select').styler();
	});
</script>
