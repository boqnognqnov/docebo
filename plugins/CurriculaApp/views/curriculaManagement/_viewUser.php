<div class="item">
	<div class="checkbox-column">
		<?php echo '<input id="curricula-course-list-checkboxes_' . $data->idUser . '" type="checkbox" name="curricula-course-list-checkboxes[]" value="' . $data->idUser . '" ' . (($data->inSessionList('selectedItems', 'idUser')) ? 'checked="checked"' : '') . '>'; ?>
	</div>
	<div class="name">
		<?php echo $data->coreUser->getFullName(); ?>
		(<?php echo $data->coreUser->getClearUserId(); ?>)
	</div>
    <?php   $coursepathCourse = LearningCoursepath::model()->findByPk($data->id_path);
            $coursepathUser = LearningCoursepathUser::model()->findByAttributes(array('id_path' => $data->id_path, 'idUser' => $data->idUser));
        if ($coursepathUser && $coursepathUser->catchup_user_limit > 0) {
                $catchupLimit = $coursepathUser->catchup_user_limit; }
        else if ($coursepathUser->catchup_user_limit == 0){
                $catchupLimit = $coursepathCourse->catchup_limit;
        }
    $countUsedCatchups = $coursepathCourse->countPlayedCatchupCourses($data->idUser); ?>
    <?php
        // raise event here in order to add extra column here after user name
        Yii::app()->event->raise('OnLPUserList', new DEvent(self, array(
            'idUser'  => $data->idUser,
            'id_path' => $data->id_path
        )));
    ?>

    <? if ($coursepathCourse->catchup_enabled) : ?>
    <div class="catchup-actions catchup">
        <? if ($catchupLimit > 0):
            if ($catchupLimit == $countUsedCatchups):
                echo('<i class="fa fa-exclamation-circle catchup"></i><script type="text/javascript">$(".catchup").addClass("orange");</script>');
            endif ?>
        <?= $countUsedCatchups?> / <span class="catchup-limit-row-value"><?= $catchupLimit ?></span>
        <?php else: ?>
        <?=$countUsedCatchups?>
        <? endif ?>
        <a class="open-dialog catchup fa fa-life-ring"
           data-dialog-id="catchup-dialog"
           data-dialog-class="catchup-dialog "
           data-dialog-title="Catch-up session usage"
           href="<?=
                Docebo::createAdminUrl('CurriculaApp/curriculaManagement/catchup',
               array('idPath' => $data->id_path,
                   'idUser' => $data->coreUser->idst,))
           ?>">
        </a>
    </div>
    <? endif //catchup_enabled?>
	<div class="course-actions">
		<?
		echo CHtml::link('', 'javascript:void(0);', array(
			'id' => 'popover-'.uniqid(),
			'class' => 'popover-action popover-trigger ',
			'data-toggle' => 'popover',
			'data-html' => TRUE,
			'data-content' => $this->renderPartial('_enrollmentActions', array(
				'model' => $data,
				'activeFrom' => ((!$data->date_begin_validity) ? '-' : $data->date_begin_validity),
				'activeTo' => ((!$data->date_end_validity) ? '-' : $data->date_end_validity),
			), TRUE),
		));
		?>
	</div>
	<div class="delete-button">
		<?php $this->renderPartial('//common/_modal', array(
			'config' => array(
				'class' => 'delete-node',
				'modalTitle' => Yii::t('standard', '_UNASSIGN'),
				'linkTitle' => Yii::t('standard', '_DEL'),
				'url' => 'CurriculaApp/curriculaManagement/unassignUser&id='.$data->id_path.'&ids='.$data->idUser,
				'buttons' => array(
					array(
						'type' => 'submit',
						'title' => Yii::t('standard', '_CONFIRM'),
					),
					array(
						'type' => 'cancel',
						'title' => Yii::t('standard', '_CANCEL'),
					),
				),
				'afterLoadingContent' => 'hideConfirmButton();',
				'afterSubmit' => 'updateCurriculaUsersContent',
			),
		)); ?>
	</div>
</div>