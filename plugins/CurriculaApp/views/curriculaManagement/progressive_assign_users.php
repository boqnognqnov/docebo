<script type='text/javascript'>
	$(function(){
		$.post('<?=Docebo::createLmsUrl('progress/run')?>', {
			'type': '<?=LearningPlanLib::JOB_TYPE_ASSIGN_LEARNING_PLAN_USERS?>',
			'ids': '<?= $ids ?>',
			'id_path': <?=$id_path; ?>
		}, function(data){
			var dialogId = 'assign-element-to-catalog';

			$('<div/>').dialog2({
				autoOpen: true, // We will open the dialog later
				id: dialogId,
				title: '<?=Yii::t('standard', 'Enroll users')?>'
			});
			$('#'+dialogId).html(data);
		});
	});
</script>
<a class='auto-close'></a>