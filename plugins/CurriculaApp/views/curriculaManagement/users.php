<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', '_COURSEPATH') => Yii::app()->createUrl('CurriculaApp/curriculaManagement/index'),
	$curriculaModel->path_name,
	Yii::t('standard', '_ASSIGN_USERS'),
); ?>

<?php //$this->renderPartial('_mainCurriculaUsersActions', array('model' => $curriculaModel)); ?>
<?php $this->renderPartial('_mainUsersActions', array('model' => $curriculaModel)); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'curricula-users-form',
	'htmlOptions' => array(
		'class' => 'ajax-list-form',
		'data-grid' => '#curricula-user-list',
	),
)); ?>

	<div class="main-section curricula-section">
		<div class="filters-wrapper">
			<div class="filters clearfix">
				<div class="order-wrapper span4">
					<label for="order_by"><?= Yii::t('social', 'Sort by'); ?></label>
					<?php echo CHtml::dropDownList('CoreUser[order_by]', $userModel->coreUser->order_by, array( Yii::t('standard', '_SELECT') ) + CurriculaAppSettingsForm::getOrderList(), array('class' => 'orderLPEnrollments')); ?>
				</div>
				<div class="input-wrapper span4">
					<input data-url="<?= Docebo::createAppUrl('lms:CurriculaApp/curriculaManagement/assignedUserAutocomplete') ?>" class="typeahead" id="advanced-search-course" autocomplete="off" type="text" name="CoreUser[search_input]" placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>" data-id="<?php echo $curriculaModel->id_path; ?>"/>
					<span class="search-icon"></span>
				</div>
			</div>
		</div>
	</div>
<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>
<div class="selections clearfix" data-grid="curricula-user-list">
	<?php $this->widget('ext.local.widgets.ListViewSelectAll', array(
		'gridId' => 'curricula-user-list',
		'class' => 'left-selections clearfix',
		'dataProvider' => $userModel->dataProviderSelectAll(),
		'itemValue' => 'idUser',
	)); ?>
	<div class="right-selections clearfix">
		<select name="massive_action" id="curricula-user-massive-action">
			<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
			<option value="delete"><?php echo Yii::t('standard', '_DEL'); ?></option>
		</select>
		<label for="users-management-massive-action"><?php echo Yii::t('standard', '_ON_SELECTED'); ?></label>
	</div>
</div>
<h3 class="title-bold title-with-border"><?php echo Yii::t('standard', 'Assigned users').': '.$curriculaModel->path_name; ?></h3>

<div id="grid-wrapper">
	<?php $this->widget('ext.local.widgets.CXListView', array(
		'id' => 'curricula-user-list',
		'htmlOptions' => array('class' => 'list-view curricula-view clearfix'),
		'dataProvider' => $userModel->dataProvider(),
		'itemView' => '_viewUser',
		'itemsCssClass' => 'items clearfix',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'pager' => array(
			//'class' => 'ext.local.pagers.DoceboLinkPager',
			'class' => 'common.components.DoceboCLinkPager',
		),
		'template' => '{items}{pager}{summary}',
		'ajaxUpdate' => 'curricula-user-list-all-items',
		'beforeAjaxUpdate' => 'function(id, options) {
				options.type = "POST";
				if (options.data == undefined) {
					options.data = {
						selectedItems: $(\'#\'+id+\'-selected-items-list\').val()
					}
				}
			}',
		'afterAjaxUpdate' => 'function(id, data) {
			$("#curricula-user-list input").styler();
			afterListViewUpdate(id);
			$(document).controls();
			$.fn.updateListViewSelectPage();
		}',
	)); ?>
</div>
<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input, select').styler();

            /**
             * On wizard close after click "Save"
             */
            $(document).on('jwizard.closed.success', '', function(e, data){
                if ($('#curricula-user-list').length > 0) {
                    $.fn.yiiListView.update('curricula-user-list');
                }
                // Otherwise reload page to reflect the new content
                else {
                    location.reload();
                }
            });

			// Attach event listener to the "ORDER_BY" dropdown field
			$(document).on("change", ".orderLPEnrollments", function(){
				// Somebody just changed the field value so let's refresh the grid and apply the changes
				$('.ajax-list-form').submit();
			});
		});
	})(jQuery);
</script>