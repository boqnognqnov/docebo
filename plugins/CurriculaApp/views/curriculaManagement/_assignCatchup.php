<style>
	.modal {
		width: 850px;
		margin-left: -425px;
	}
</style>
<h1>
	<?=Yii::t('coursepath', 'Assign catch-up course')?>
</h1>
<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'assign-catchup-form',
)); ?>
	<div class="main-section">

		<div class="filters-wrapper">
			<div class="selections" data-grid="curricula-assign-course-grid">
				<div class="input-wrapper">
					<?=CHtml::activeTextField($courseModel, 'search_input', array('placeholder' => Yii::t('standard', '_SEARCH')))?>
					<span class="search-icon"></span>
				</div>
			</div>
		</div>

	</div>

	<input type="hidden" name="contentType" value="html"/>

<?php $this->endWidget(); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'select-catchup-form',
	'htmlOptions' => array(
		'data-grid-items' => 'true',
		'class' => 'ajax'
	),
)); ?>
<?php echo $form->errorSummary($model); ?>
<div id="grid-wrapper" class="courseEnroll-course-table">
	<h2><?php echo Yii::t('report', '_REPORT_COURSE_SELECTION'); ?></h2>
	<?php

	$this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'curricula-assign-course-grid',
		'htmlOptions' => array('class' => 'grid-view clearfix'),
		'dataProvider' => $courseModel->dataProviderCatchupCourses($lpCourses),
		'template' => '{items}{summary}{pager}',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
		'pager' => array(
			'class' => 'ext.local.pagers.DoceboLinkPager',
		),
		'afterAjaxUpdate' => 'function(id, data){prepareGrid()}',
		'cssFile' => false,
		'columns' => array(
			array(
				'name' => '',
				'value' => 'CHtml::radioButton("catchup", false, array("class"=>"catchup_selection", "id" => "catchup_selection_".$data->idCourse, "value"=>$data->idCourse))',
				'type' => 'raw'
			),
			'code',
			'name',
			array(
				'id' => 'course_type',
				'value' => '$data->getCourseTypeTranslation()',
				'header' => Yii::t('course', '_COURSE_TYPE')
			)
		)
	));

	echo CHtml::activeHiddenField($model, 'id_catch_up', array('id' => 'catchup_selection'));
	?>
	<input type="hidden" id="catchup_selection" name="catchup_selection"/>
</div>

<div class="form-actions">
	<input class="btn-docebo green big" type="submit" name="confirm" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" />
	<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(function () {
		prepareGrid();
		$('#assign-catchup-form').submit(function(e){
			$('#curricula-assign-course-grid').yiiGridView('update', {
				data: $(this).serialize()
			});
			return false;
		});
	});

	function prepareGrid()
	{
		$("#curricula-assign-course-grid input").styler();
		$('.catchup_selection').change(function(){
			$('#catchup_selection').val($(this).val());
		});
		if ($('#catchup_selection').val())
			$('#catchup_selection_' + $('#catchup_selection').val()).trigger('click');
	}
</script>