<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', '_COURSEPATH'),
); ?>

<?php
$can_edit = Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/lms/admin/coursepath/mod');
$this->renderPartial('_mainCurriculaActions');
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'curricula-management-form',
	'htmlOptions' => array(
		'class' => 'ajax-grid-form',
		'data-grid' => '#curricula-management-grid'
	),
)); ?>

<div class="main-section group-management">
	<div class="filters-wrapper">
		<div class="filters clearfix">
			<div class="input-wrapper">
				<?php echo $form->textField($curriculaModel, 'path_name', array(
					'id' => 'advanced-search-curricula',
					'class' => 'typeahead',
					'autocomplete' => 'off',
					'placeholder' => Yii::t('standard', '_SEARCH'),
				)); ?>
				<span>
				    <button class="search-btn-icon"></button>
				</span>
			</div>
		</div>
	</div>
</div>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<div class="bottom-section curricula-bottom-section border-bottom-section clearfix">
	<div id="grid-wrapper">
		<?php

		$lpLogoVariant = CoreAsset::VARIANT_MICRO;
		$columns = array(
			'path_code',
			array(
				'header' => Yii::t('standard', 'Thumbnail'),
				'type' => 'raw',
				'value' => '$data->renderLogo("'.$lpLogoVariant.'");',
				'htmlOptions' => array('class' => 'center-aligned'),
				'headerHtmlOptions' => array('class' => 'center-aligned'),
			),
			array(
				'name' => 'path_name',
				'type' => 'raw',
				'htmlOptions' => array('class' => 'wrapped')
			),
			array(
				'name' => 'path_descr',
				'type' => 'raw',
				'htmlOptions' => array('class' => 'native-styled')
			),
			array(
				'header' => '',
				'type' => 'raw',
				'value' => '(empty($data->learningCoursepathCourses)?\'<span class="exclamation"></span>\':\'\');',
				'htmlOptions' => array('class' => 'single-action'),
			),
			array(
				'type' => 'raw',
				'value' => '$data->renderCourses();',
				'htmlOptions' => array(
					'class' => 'curricula-button-column',
				),
			),
			array(
				'type' => 'raw',
				'value' => '$data->renderUsers();',
				'htmlOptions' => array(
					'class' => 'curricula-button-column',
				),
			),
		);

		if(Yii::app()->user->getIsPu() && CoreUser::adminRuleEnabled(Yii::app()->user->id, 'allow_buy_seats')){
			// The current user is a Power User and he is only
			// allowed to enroll users to courses through "seats"
			// Do a check if the PU has enough seats to distribute in
			// the course (enough to cover the selected users)
			$columns[] = array(
				'type' => 'raw',
				'header' => Yii::t('standard', 'Seats'),
				'value' => function($data, $index){
					/* @var $data LearningCoursepath */
					$idUser = Yii::app()->user->getIdst();
					$idPath = $data->id_path;
					if (Yii::app()->legacyWrapper->hydraFrontendEnabled()) {
						$bridge = new HydraFrontendBridge();
						$link = $bridge->getHydraLearningPlanViewUrl(LearningCoursepath::model()->findByPk($idPath));
						echo '<a href="'.$link.'" title="' . Yii::t('standard', 'Seats') . '" rel="tooltip">
						<span class="maxAvailable"></span></a>';
					} else {
						echo '<a href="' . Docebo::createLmsUrl('course/buyMoreSeats',
								array('id' => $idPath, 'type' => 'coursepath'))
							. '" class="open-dialog" data-dialog-class="course-buy-seats"
					title="' . Yii::t('standard', 'Seats') . '" rel="tooltip" data-dialog-title="'
							. Yii::t('standard', 'Seats') . '"><span class="maxAvailable"></span></a>';
					}
				},
				'htmlOptions' => array(
					'class' => 'curricula-button-column',
				),
			);
		}

		if ($can_edit) {

			$columns[] = array(
				'class' => 'CButtonColumn',
				'headerHtmlOptions' => array(
					'class' => 'button-column-single',
				),
				'htmlOptions' => array(
					'class' => 'button-column-single',
				),
				'buttons' => array(
					'update' => array(
						'url' => 'Yii::app()->createUrl(\'CurriculaApp/curriculaManagement/update\', array(\'id\' => $data->id_path))',
						'options' => array(
							'class' => 'edit-action popup-handler',
							'data-modal-title' => Yii::t('standard', '_MOD'),
							'data-modal-class' => 'curricula-edit',
							'data-after-send' => 'newCurriculaAfterSubmit(data);',
							'data-after-close' => 'function(){TinyMce.removeEditorById("path-textarea-descr");}',
							'data-after-show' => 'function(){}',

						),
					),
				),
				'template' => '{update}',
			);
			$columns[] = array(
				'name' => 'delete',
				'header' => '',
				'type' => 'raw',
				'value' => 'CHtml::link("", "", array(
					"class" => "ajaxModal delete-action",
					"data-toggle" => "modal",
					"data-modal-class" => "delete-node",
					"data-modal-title" => "'.Yii::t('standard', '_DEL').'",
					"data-buttons" => \'[
						{"type": "submit", "title": "'.Yii::t('standard', '_CONFIRM').'"},
						{"type": "cancel", "title": "'.Yii::t('standard', '_CANCEL').'"}
					]\',
					"data-url" => "CurriculaApp/curriculaManagement/delete&id=$data->id_path",
					"data-after-loading-content" => "hideConfirmButton();",
					"data-after-submit" => "updateCurriculaContent",
				));',
				'htmlOptions' => array('class' => 'button-column-single')
			);
		}
		Yii::app()->event->raise('BeforeCurriculaManagementGridRender', new DEvent($this, array(
			'canEdit' => $can_edit,
			'columns' => &$columns
		)));
		$this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'curricula-management-grid',
			'dataProvider' => $curriculaModel->dataProvider(),
			'template' => '{items}{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
			'pager' => array(
				'class' => 'ext.local.pagers.DoceboLinkPager',
			),
			'columns' => $columns,
			'ajaxUpdate' => 'all-items',
			'afterAjaxUpdate' => 'function(id, data){
				$(\'a[rel="tooltip"]\').tooltip();
				$(document).controls();
			}'
		)); ?>
	</div>
</div>
<script type="text/javascript">
	/*$(document).ready(function () {
		$('select').styler();
		$('.powerUserProfileSelect').hide();
	});*/

	$(function(){
		$(document).on('dialog2.content-update', '.modal.course-buy-seats', function(){
			var autoCloseAndRedirect = $(this).find('.auto-close.redirect');

			if(autoCloseAndRedirect.length > 0){
				var href = autoCloseAndRedirect.attr('href');
				window.location = href;
			}
		})
	});

</script>
