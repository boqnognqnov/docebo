<div class="main-actions clearfix">
	<h3 class="title-bold back-button">
		<?php echo CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('CurriculaApp/curriculaManagement/index')); ?>
		<span><?php echo Yii::t('standard', '_ASSIGN_USERS'); ?></span>
	</h3>
	<ul class="clearfix">
		<li>
			<div>
				<?php echo CHtml::link('<span></span>'.Yii::t('standard', '_ASSIGN_USERS'), Yii::app()->createUrl('CurriculaApp/curriculaManagement/assignUser', array('id' => $model->id_path)), array(
					'class' => 'popup-handler curricula-assign-users',
					'data-modal-title' => Yii::t('standard', '_ASSIGN_USERS'),
					'data-modal-class' => 'curricula-assign-users',
					'alt' => Yii::t('helper', 'Curricula assign users'),
					'data-after-send' => 'newCurriculaUsersAfterSubmit(data)',
				)); ?>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>
			<p></p>
		</div>
	</div>
</div>