<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', '_COURSEPATH') => Yii::app()->createUrl('CurriculaApp/curriculaManagement/index'),
	$curriculaModel->path_name,
	Yii::t('standard', '_ASSIGN_COURSES'),
); ?>

<?php
$can_edit = Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/lms/admin/coursepath/mod');
$this->renderPartial('_mainCurriculaCoursesActions', array('model' => $curriculaModel));
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'curricula-courses-form',
	'htmlOptions' => array(
		'class' => 'ajax-list-form',
		'data-grid' => '#curricula-course-list',
	),
)); ?>

	<div class="main-section curricula-section">
			<div class="filters-wrapper">
       			<div class="filters clearfix">
		            <div class="input-wrapper">
		           <input data-url="<?= Docebo::createAppUrl('lms:CurriculaApp/curriculaManagement/assignedCourseAutocomplete') ?>" class="typeahead" id="advanced-search-course" autocomplete="off" type="text" name="LearningCourse[name]" placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>" data-id="<?php echo $curriculaModel->id_path; ?>"/>
		            <span class="search-icon"></span>
		            </div>
        		</div>
			</div>
	</div>
<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>
<?php if ($can_edit) : ?>
<div class="selections clearfix" data-grid="curricula-course-list">
	<?php $this->widget('ext.local.widgets.ListViewSelectAll', array(
		'gridId' => 'curricula-course-list',
		'class' => 'left-selections clearfix',
		'dataProvider' => $courseModel->dataProvider(),
		'itemValue' => 'id_item',
	)); ?>
	<div class="right-selections clearfix">
		<select name="massive_action" id="curricula-course-massive-action">
			<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
			<option value="delete"><?php echo Yii::t('standard', '_DEL'); ?></option>
		</select>
		<label for="users-management-massive-action"><?php echo Yii::t('standard', '_ON_SELECTED'); ?></label>
	</div>
</div>
<?php endif; ?>

<h3 class="title-bold title-with-border"><?php echo Yii::t('standard', 'Assigned courses').': '.$curriculaModel->path_name; ?></h3>

<div id="grid-wrapper">
	<?php $this->widget('ext.local.widgets.CXListView', array(
		'id' => 'curricula-course-list',
		'htmlOptions' => array('class' => 'list-view curricula-view clearfix'),
		'dataProvider' => $courseModel->dataProvider(true),
		'itemView' => '_viewCourse',
		'itemsCssClass' => 'items clearfix',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'pager' => array(
			'class' => 'DoceboCLinkPager',
		),
		'template' => '{items}{pager}{summary}',
		'ajaxUpdate' => 'curricula-course-list-all-items',
		'beforeAjaxUpdate' => 'function(id, options) {
				options.type = "POST";
				if (options.data == undefined) {
					options.data = {
						selectedItems: $(\'#\'+id+\'-selected-items-list\').val()
					}
				}
			}',
		'afterAjaxUpdate' => 'function(id, data) {$("#curricula-course-list input").styler();curriculaCourseSortInit();afterListViewUpdate(id);$(document).controls();updateSelectedCheckboxesCounter(id);$.fn.updateListViewSelectPage();}',
	)); ?>
</div>
<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input, select').styler();
			curriculaCourseSortInit();

			$(document).on("dialog2.content-update", '[id="assign-catchup-dialog"], [id="remove-catchup-dialog"]', function () {
				if ($(this).find("a.auto-close").length > 0) {
					$.fn.yiiListView.update('curricula-course-list');
				}
			});
		});
	})(jQuery);
</script>
