<?php
$can_edit = Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/lms/admin/coursepath/mod');
if (Yii::app()->user->isAdmin && $can_edit) { //power users may not be allowed to manage certain courses inside the lerning path
	$can_edit = LearningCoursepathCourses::isPowerUserCourse(Yii::app()->user->id, $data->id_item);
}
?>
<div class="item">
	<?php if ($can_edit) : ?>
	<div class="checkbox-column curricula-checkbox-vertical-align">
		<?php echo '<input id="curricula-course-list-checkboxes_' . $data->id_item . '" type="checkbox" name="curricula-course-list-checkboxes[]" value="' . $data->id_item . '" ' . (($data->inSessionList('selectedItems', 'id_item')) ? 'checked="checked"' : '') . '>'; ?>
	</div>
	<div class="move">
		<?php echo $data->renderMove(); ?>
	</div>
	<?php endif; ?>
	<table>
		<tr>
			<td>
			<?php echo $data->idItem->name; ?>
			<?
				if ($data->learningCourse->isCatchupType() && $data->idPath->catchup_enabled)
				{
					$catchup = LearningCourseCatchUp::loadAssignedCatchup($data->idPath->id_path, $data->learningCourse->idCourse);
					if ($catchup) : ?>
						<br>
						<div>
							<strong><?=Yii::t('coursepath', 'Catch-up course')?>: </strong>
							<a href="<?=Docebo::createAdminUrl('CurriculaApp/curriculaManagement/axAssignCatchup', array('idPath' => $data->idPath->id_path, 'idCourse' => $data->learningCourse->idCourse))?>" class="assign-catchup-link blue open-dialog" rel="assign-catchup-dialog"><?=$catchup->catchUpCourseModel->name?></a>
							(<?=$catchup->catchUpCourseModel->getCourseTypeTranslation()?>)
							<a href="<?=Docebo::createAdminUrl('CurriculaApp/curriculaManagement/axRemoveCatchup', array('idPath' => $data->idPath->id_path, 'idCourse' => $data->learningCourse->idCourse))?>" class="open-dialog" rel="remove-catchup-dialog"><i class="fa fa-times assign-catchup-link-remove"></i></a>
						</div>
					<? else : ?>
						<br>
						<div><?=CHtml::link(Yii::t('coursepath', 'Assign catch-up course').'<i class="fa fa-exclamation-circle"></i>', array('curriculaManagement/axAssignCatchup', 'idPath' => $data->idPath->id_path, 'idCourse' => $data->learningCourse->idCourse), array('class' => 'assign-catchup-link orange open-dialog', 'rel' => 'assign-catchup-dialog'))?></div>
					<? endif;
				}
			?>
			</td>
			<?php

				// Start Manage Seats for god admins
				if ($data->learningCourse->selling && Yii::app()->user->getIsGodadmin()) {
						$showManageSeats = '<td><div class="pull-right manage-plan-course-pu-seats"> '.
							CHtml::link(Yii::t('course', 'Manage Seats') . '<span class="maxAvailable"></span> ',
							Docebo::createAdminUrl('courseManagement/manageSeats',
								array('id' => $data->id_item, 'ref_src' => 'admin')
							),
							array(
								'class'             => 'open-dialog course-manage-seats',
								'data-dialog-title' => Yii::t('course', 'Manage Seats'),
								'data-dialog-class' => "manage-pu-seats-dialog",
								'data-dialog-id'    => "manage-pu-seats-dialog"
							)
						).'</div></td>';

					echo $showManageSeats;
				} // End Manage Seats


			?>
			<?php if(PluginManager::isPluginActive('ClassroomApp')): ?>
			<td><div class="course-type"><span title="<?= $data->idItem->getCourseTypeTranslation() ?>"><?= $data->idItem->getCourseTypeTranslation() ?></span><i class="<?= $data->idItem->course_type ?>"></i></div></td>
			<?php endif; ?>
		</tr>
	</table>
	<?php if ($can_edit) : ?>
	<?php echo $data->renderPrerequisites(); ?>
	<div class="delete-button">
		<?php $this->renderPartial('//common/_modal', array(
			'config' => array(
				'class' => 'delete-node',
				'modalTitle' => Yii::t('standard', '_UNASSIGN'),
				'linkTitle' => 'Delete',
				'url' => 'CurriculaApp/curriculaManagement/unassignCourse&id='.$data->id_path.'&id_item='.$data->id_item,
				'buttons' => array(
					array(
						'type' => 'submit',
						'title' => Yii::t('standard', '_CONFIRM'),
					),
					array(
						'type' => 'cancel',
						'title' => Yii::t('standard', '_CANCEL'),
					),
				),
				'afterLoadingContent' => 'hideConfirmButton();',
				'afterSubmit' => 'updateCurriculaCoursesContent',
			),
		)); ?>
	</div>
	<?php endif; ?>
	<div class="sequence"><?php echo $data->sequence; ?></div>
</div>