<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'curricula_form',
		'htmlOptions' => array('class' => 'form-horizontal'),
	)); ?>

	<div class="control-group">
		<?php echo $form->labelEx($model, 'path_name', array('class'=>'control-label')); ?>
		<div class="controls">
			<?php echo $form->error($model, 'path_name'); ?>
			<?php echo $form->textField($model, 'path_name'); ?>
		</div>
	</div>

	<?
	list($count, $defaultImages, $userImages, $selected) = LearningCoursepath::model()->prepareThumbnails($model);
	$uniqueId = substr(md5(uniqid(rand(), true)), 0, 10);
	?>
	<?=$this->renderPartial('_sliders', array(
		'model'=>$model,
		'defaultImages' => $defaultImages,
		'userImages' => $userImages,
		'count' => $count,
		'selectedTab' => 'user',
		'uniqueId' => $uniqueId,
	))?>

	<div class="control-group description-area">
		<?php echo $form->labelEx($model, 'path_descr', array('class'=>'control-label')); ?>
		<div class="controls">
			<?php echo $form->error($model, 'path_descr'); ?>
			<?php echo $form->textArea($model, 'path_descr', array('class' => 'tinymce', 'id' => 'path-textarea-descr')); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo CHtml::label(Yii::t('standard', 'Catalog'), null, array('class'=>'control-label')); ?>
		<div class="controls">
			<div>
				<?=CHtml::activeCheckBox($model, 'visible_in_catalog', array())?>
				<span style="margin-left: 10px;"><?=Yii::t('coursepath', 'Show this learning plan in the courses catalog')?></span>
			</div>
		</div>
	</div>

	<div class="control-group" id="lp-selling-options" style="display: <?= $model->visible_in_catalog ? "block" : "none" ?>">
		<?php echo CHtml::label(Yii::t('coursepath', 'Sell this Learning Plan'), 'certificate', array('class'=>'control-label')); ?>
		<div class="controls">
			<div>
				<?=CHtml::activeCheckBox($model, 'is_selling', array())?>
				<span style="margin-left: 10px;"><?=Yii::t('coursepath', 'Put this learning plan on sale')?></span>
			</div>

			<br/>

			<div style="margin-left:30px;">
				<div class="pull-left" style="margin-right:10px;margin-top:7px;">Price:</div>
				<div class="input-append">
					<?=CHtml::activeTextField($model, 'price', array('class'=>'span4'))?>
					<span class="add-on" style="height: 23px;">$</span>
				</div>
			</div>

			<br/>

			<div style="margin-left:30px;">
				<div>
					<div class="pull-left">
						<?=CHtml::activeRadioButton($model, 'purchase_type', array('value'=>LearningCoursepath::PURCHASE_TYPE_FULL_PACK_ONLY, 'uncheckValue'=>null))?>
					</div>
					<div class="pull-left" style="margin-left: 10px; width: 560px;">
						<p><?=Yii::t('coursepath', 'Users must buy the full learning plan')?></p>
						<p style="color: #888;"><?=Yii::t('coursepath', "Users won't be able to buy single courses and the price is the one fixed above.")?></p>
					</div>
					<div class="clearfix"></div>
				</div>

				<br/>

				<div>
					<div class="pull-left">
						<?=CHtml::activeRadioButton($model, 'purchase_type', array('value'=>LearningCoursepath::PURCHASE_TYPE_SINGLE_COURSES_IN_SEQUENCE, 'uncheckValue'=>null))?>
					</div>
					<div class="pull-left" style="margin-left: 10px; width: 560px;">
						<p><?=Yii::t('coursepath', 'Users decide which courses to buy')?></p>
						<p style="color: #888;"><?=Yii::t('coursepath', "The final sum price is the sum of each single course price. If the price above is fixed and the user selects all the courses, that price will be used.")?></p>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="control-group">
		<?php echo CHtml::label(Yii::t('player', 'Certificate'), 'certificate', array('class'=>'control-label')); ?>
		<div class="controls">
			<?php echo CHtml::dropDownList('certificate', $model->certificate->id_certificate, array(Yii::t('standard', '_NONE')) + CHtml::listData(LearningCertificate::model()->findAll(), 'id_certificate', 'name')); ?>
		</div>
	</div>

	<? if(PluginManager::isPluginActive('CertificationApp')): ?>
	<div class="control-group">
		<?php echo CHtml::label(Yii::t('certification', 'Certifications & Retraining'), 'certification', array('class'=>'control-label')); ?>
		<div class="controls">
			<?php echo CHtml::dropDownList('certification', ($model->certification ? $model->certification->id_cert : null), array(Yii::t('standard', '_NONE')) + CHtml::listData(Certification::model()->findAll(), 'id_cert', 'title')); ?>
		</div>
	</div>
	<? endif; ?>

	<div class="control-group">
		<?php echo $form->labelEx($model, 'path_code', array('class'=>'control-label')); ?>
		<div class="controls">
			<?php echo $form->error($model, 'path_code'); ?>
			<?php echo $form->textField($model, 'path_code'); ?>
		</div>
	</div>

    <?php
        $html = '';
        Yii::app()->event->raise('LearningPlanCustomAttributes', new DEvent($this, array(
            'html' => &$html,
            'learning_plan' => $model,
            'form' => $form
        )));
        echo $html;
    ?>

<div class="modal-footer">
	<input class="btn confirm-btn" type="button" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" name="submit" data-submit-form="curricula_form"/>
	<input class="btn close-btn" type="button" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>
    
    
	<?php $this->endWidget(); ?>
</div>



<script type="text/javascript">
	TinyMce.attach("#path-textarea-descr", {height: 150});
	$(function(){
		$('input[name="LearningCoursepath[visible_in_catalog]"]').change(function() {
			$("#lp-selling-options").toggle(this.checked);
		});
	});
</script>