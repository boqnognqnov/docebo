<?php 
	$html = $this->renderPartial('//common/_usersSelector', array(
		'userModel' => $userModel,
		'groupModel' => $groupModel,
		'isNeedRegisterYiiGridJs' => $isNeedRegisterYiiGridJs,
		'fancyTreeData' => $fancyTreeData,
		'preSelected' => $preSelected,
	), true, true);

	echo $html;
?>


<div class="modal-footer">
	<input class="btn confirm-btn" type="button" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" name="submit" data-submit-form="select-form"/>
	<input class="btn close-btn" type="button" value="<?php echo Yii::t('standard', '_CLOSE'); ?>"/>
</div>