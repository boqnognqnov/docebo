<?php
$overwriteEnrollUsersButton = 0; //default do not overwrite the assign button
?>
<div class="main-actions clearfix">
	<h3 class="title-bold back-button">
		<?php echo CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('CurriculaApp/curriculaManagement/index')); ?>
		<span><?php echo Yii::t('standard', '_ASSIGN_USERS');?></span>
	</h3>
	
	<ul id="assign-users-button-container" class="clearfix">
		<li>
			<div>
				<?php
					$title 	= '<span></span>'.Yii::t('standard', '_ASSIGN_USERS'); 
					$url 	= Docebo::createLmsUrl('usersSelector/axOpen', array(
								'type' 		=> UsersSelector::TYPE_CURRICULA_MANAGEMENT,
								'idPlan' => $model->id_path,
							));

                $rel = 'users-selector';
                $enableWizard = false; //by default there is no wizard

                Yii::app()->event->raise('OnLPUsersAssignButtonRender',
                    new DEvent($this, array(
                        'overwriteEnrollUsersButton' => &$overwriteEnrollUsersButton,
                        'model' => &$model,
                        'url' => &$url,
                        'rel' => &$rel,
                        'enableWizard' => &$enableWizard
                    )));

					echo CHtml::link($title, $url, array(
						'class' => 'open-dialog curricula-assign-users',
						'rel' 	=> $rel,
						'alt' 	=> Yii::t('helper', 'Curricula assign users'),
						'data-dialog-title' => Yii::t('standard', '_ASSIGN_USERS'),
                        'data-dialog-id' => $rel,
						'title'	=> Yii::t('standard', '_ASSIGN_USERS')
					));

                    if ($enableWizard) :
				?>
                        <script type="text/javascript">
                            // Initialize jWizard class and create a global variable "jwizard". Give the wizard the ID of the opening dialog (the dialog)
                            // NOTE: As of this writing the global variable MUST be named "jwizard"
                            var options = {
                                dialogId: "<?=$rel ?>"    // Must be equal to the  a.open-dialog's  'rel' attribute! See above
                            };
                            var	jwizard = new jwizardClass(options);


                        </script>
                <?php endif; ?>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4></h4>
			<p></p>
		</div>
	</div>
	
</div>


<script>

	/**
	 * The name of this function is hardcoded in UsersSelector JS as a callback upon successfully closing the dialog
	 *  Use in on your discretion to update current UI
	 */
	function usersSelectorSuccessCallback(data) {
		$.fn.yiiListView.update('curricula-user-list');
	}
			
</script>
