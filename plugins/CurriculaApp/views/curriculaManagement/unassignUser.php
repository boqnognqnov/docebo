<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<div class="form">
	<?php
	$formParams =  array('id' => 'label_form');
	if (is_array($users) && count($users) > 10) { //smaller operations don't need to be progressive
		$formParams['action'] = Docebo::createAbsoluteLmsUrl('progress/run', array('type' => LearningPlanLib::JOB_TYPE_UNASSIGN_LEARNING_PLAN_USERS, 'format' => 'json'));
	}
	?>
	<?php $form = $this->beginWidget('CActiveForm',$formParams); ?>

	<p><?php echo Yii::t('standard', '_UNASSIGN').': "'.count($users).' '.Yii::t('standard', '_USERS').'"'; ?></p>

	<div class="clearfix">
		<?php echo CHtml::checkBox('delete_confirm', false, array('onchange' => $onChange)); ?>
		<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed'), 'delete_confirm'); ?>
	</div>
	<?php
		echo CHtml::hiddenField('ids', implode( ',', $users));
		echo CHtml::hiddenField('id_path', $idPath);
	 ?>

	<?php $this->endWidget(); ?>
</div>