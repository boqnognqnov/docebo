<?php
$this->breadcrumbs = array(
    Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
    Yii::t('standard', '_COURSES')
);

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'catchup-management-form',
    'htmlOptions' => array(
        'class' => 'ajax',
        'data-grid-items' => 'true'
    )
));
?>

<div class="catchup-header">
    <div class="catchup-username"><b><?= Yii::t('standard', '_USER') ?>: </b><?= $userName['fname'] . ' ' . $userName['lname'] ?></div>
    <div class="catchup-limit"><b><?= Yii::t('standard', 'Usage') ?>:</b>
        <?php
        if ($catchupLimit > 0): // if there is set limit for LP?
        ?>
        <span class="catchup-values">
            <span id="catchup-count-value">
                <?= $catchupCount ?>
            </span>
            /
            <span id="catchup-limit-value">
                <?= $catchupLimit ?>
            </span>
        </span>
    </div>
    <?php
    else: // Show only the assigned Catch-ups
    ?>
    <span class="catchup-values"><span id="catchup-count-value"><?= $catchupCount ?></span></span></div>
<?php
endif;
?>
<div class="catchup-learning-plan"><b><?= Yii::t('myactivities', 'Learning plans') ?>: </b><?= $coursePathName ?></div>
<br/><hr/>
</div>
<?= CHtml::hiddenField('limit', $catchupLimit, array(
    'id' => 'limit-contain'
)); ?>
<?= CHtml::hiddenField('catchup-checkbox-mail', 0, array(
    'id' => 'catchup-checkbox-notification'
)); // Hold value for checked 
?>

<span class="checkbox-catchup-container"> <?= CHtml::checkBox('notify-checkbox', 0, array(
        'class' => 'notify checkbox',
        'id' => 'catchup-checkbox'
    )) ?>
    <?= Yii::t('general', 'Notify <b>' . $userName['fname'] . ' ' . $userName['lname'] . '</b> about the new catch-up session limit!') ?> </span>

<div class="grid-wrapper">
	<?php

	$_columns = array(
		array(
			'name' => 'date_first_access',
			'header' => Yii::t('standard', '_DATE_FIRST_ACCESS')
		),
		array(
			'name' => 'name',
			'header' => Yii::t('standard', '_NAME')
		),
		array(
			'name' => 'catchup_course',
			'header' => Yii::t('standard', 'CATCHUP COURSE')
		)
	);

	$this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'catchup-grid',
		'htmlOptions' => array(
			'class' => 'grid-view clearfix'
		),
		'dataProvider' => $dataProvider,
		'template' => '{items}{summary}{pager}',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
		'pager' => array(
			'class' => 'DoceboCLinkPager',
			'maxButtonCount' => 8
		),
		'ajaxUpdate' => 'all-items',
		'columns' => $_columns
	));
	?>
    <div class="form-actions">

        <?= CHtml::submitButton(Yii::t('standard', '_CONFIRM'), array(
            'id' => 'confirm_button',
            'name' => 'confirm_button',
            'class' => 'btn btn-docebo confirm-btn green big'
        ));
        ?>
        <?= CHtml::button('Close', array(
            'class' => 'btn close-btn close-dialog'
        )) ?>

    </div>

</div>
<?php
$this->endWidget();
?>

<div id="grid-wrapper">
    <?php
    $courseLogoVariant = CoreAsset::VARIANT_MICRO;
    ?>
</div>
<script type="text/javascript">
    $('select').styler();
</script>