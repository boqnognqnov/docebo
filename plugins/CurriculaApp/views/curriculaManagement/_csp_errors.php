<div id="grid-wrapper" class="courseEnroll-course-table lp-csp-error-message-container">

	<div class="assign-course-csp-error-icon">
		<i class="fa fa-warning fa-3x" style="color: #e67e22;"></i>
	</div>

	<div class="assign-course-csp-error-message">
		<div class="text">
			<?php echo Yii::t('marketplace', '{{num_courses}} courses could not be assigned to the learning plan because they contain training materials from C.S.P. vendor catalog', array(
					'{{num_courses}}' => '<strong>'.count($cspErrors).'/'.$totalCourses.'</strong>'
				)).':'; ?>
		</div>

		<div class="list">
			<ul>
				<?php
				foreach ($cspErrors as $cspError) {
					echo '<li>';
					echo $cspError['name'];
					echo '</li>';
				}
				?>
			</ul>
		</div>
	</div>

</div>

<div class="modal-footer">
	<input class="btn close-btn" type="button" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>

<?php if (count($cspErrors) < $totalCourses): ?>
<script type="text/javascript">
	$(function() {
		$.fn.yiiListView.update('curricula-course-list');
	});
</script>
<?php endif; ?>