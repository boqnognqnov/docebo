<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<div class="form">
	<div class="row-fluid">
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'label_form',
		)); ?>

		<p class="break-word"><?php echo Yii::t('standard', '_COURSEPATH').': "'.$model->path_name.'"'; ?></p>

		<div class="clearfix">
			<?php echo CHtml::checkBox('delete_confirm', false, array('onchange' => $onChange)); ?>
			<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed', array($model->path_name)), 'delete_confirm'); ?>
		</div>
		<br>
		<br>
		<p><i class="info"><?php echo Yii::t('curricula_management', 'When deleting a curricula, users aren\\\'t automatically unenrolled by courses included in the curricula itself. Unenrollment must be performed manually.'); ?></i></p>
		<?php $this->endWidget(); ?>
	</div>
</div>