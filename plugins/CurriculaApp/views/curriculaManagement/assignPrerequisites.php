<?php
/* @var $form CActiveForm */
/* @var $model LearningCoursepathCourses */
/* @var $courseModel LearningCoursepathCourses */
?>
<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'curricula-assign-prerequisites-form',
	'htmlOptions' => array('class' => 'ajax-grid-form', 'data-grid' => '#curricula-assign-prerequisites-grid'),
)); ?>
<div class="main-section">

	<div class="filters-wrapper">
		<div class="selections" data-grid="curricula-assign-prerequisites-grid">
			<div class="input-wrapper">
				<input data-url="<?= Docebo::createAppUrl('lms:CurriculaApp/curriculaManagement/assignPrerequisitesAutocomplete') ?>"
					data-type="core" data-id="<?php echo $model->id_path; ?>"
					data-id_item="<?php echo $model->id_item; ?>" class="typeahead" id="enroll-advanced-search-course"
					autocomplete="off" type="text" name="LearningCourse[name]"
					placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"/>
				<span class="search-icon"></span>
			</div>
			<?php $this->widget('ext.local.widgets.GridViewSelectAll', array(
				'gridId' => 'curricula-assign-prerequisites-grid',
				'class' => 'left-selections',
				'dataProvider' => $model->dataProvider(true),
				'itemValue' => 'id_item',
			)); ?>
		</div>
	</div>
</div>

<input type="hidden" name="contentType" value="html"/>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'select-course-form',
	'htmlOptions' => array(
		'data-grid-items' => 'true',
	),
)); ?>

<div id="grid-wrapper" class="courseEnroll-course-table">
	<h2><?php echo Yii::t('standard', 'Prerequisites for {course} in {learningplan}', array(
			'{course}' => "<span>".$courseModel->learningCourse->name."</span>",
			'{learningplan}' => "<span>".$courseModel->idPath->path_name."</span>"
		)); ?></h2>
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'curricula-assign-prerequisites-grid',
		'htmlOptions' => array('class' => 'grid-view clearfix'),
		'dataProvider' => $model->dataProvider(true),
		'columns' => array(
			array(
				'class' => 'CCheckBoxColumn',
				'selectableRows' => 2,
				'id' => 'curricula-assign-prerequisites-grid-checkboxes',
				'value' => '$data->id_item',
				'checked' => 'in_array($data->id_item, Yii::app()->session[\'selectedItems\'])',
			),
			'idItem.code',
			'idItem.name',
		),
		'template' => '{items}{summary}{pager}',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
		'pager' => array(
			'class' => 'ext.local.pagers.DoceboLinkPager',
		),
		'beforeAjaxUpdate' => 'function(id, options) {
							options.type = "POST";
							if (options.data == undefined) {
								options.data = {
									contentType: "html",
									selectedItems: $(\'#\'+id+\'-selected-items-list\').val()
								}
							}
						}',
		'ajaxUpdate' => 'curricula-assign-prerequisites-grid-all-items',
		'afterAjaxUpdate' => 'function(id, data){$("#curricula-assign-prerequisites-grid input").styler(); afterGridViewUpdate(id);}',
		'cssFile' => false,
	)); ?>
</div>

<br/>
<div class="row-fluid">
	<div class="span6" style="margin-top: 8px;">
		<label class="radio coursepath-mode" for="LearningCoursepathCourses_mode_all">
			<?= $form->radioButton($model, 'mode', array('uncheckValue'=>null, 'value'=>'all', 'checked'=>($courseModel->mode==='all'), 'id'=>'LearningCoursepathCourses_mode_all')) . ' ' . Yii::t('curricula', 'All courses are required to be completed') ?>
		</label>
	</div>
	<div class="span6">
		<label class="radio coursepath-mode" for="LearningCoursepathCourses_mode_min">
			<?= $form->radioButton($model, 'mode', array('uncheckValue'=>null, 'value'=>0, 'checked'=>(intval($courseModel->mode)>0), 'id'=>'LearningCoursepathCourses_mode_min')) . ' ' . Yii::t('curricula', 'Require at least {count_input} courses to be completed', array(
				'{count_input}' => $form->textField($model, 'min_courses', array('value'=>(intval($courseModel->mode)>0 ? $courseModel->mode : ''), 'style'=>'width:30px;margin:0 5px;'))
			)) ?>
		</label>
	</div>
</div>
<div class="row-fluid">
	<div class="span12">
		<?php
		$input = '<input type="number" style="width: 6%" min="1" name="delayTime" id="delayTime" value="'.$courseModel->delay_time.'">';
		$select = CHtml::dropDownList('delayTimeUnit', '', array(
			'day' => Yii::t('standard', '_DAYS'),
			'week' => Yii::t('standard', 'week(s)'),
			'month' => Yii::t('standard', 'Months'),
		), array('style' => 'width:15%', 'id' => 'delayTimeUnit','options'=>array($courseModel->delay_time_unit=>array('selected'=>true))));
		?>
		<label>
			<input type="checkbox" name="isDelay" value="1" id="isDelay"
				<?= ($courseModel->delay_time_unit != null && $courseModel->delay_time != null) ? 'checked' : ''; ?>/>&nbsp;&nbsp;
			<?= Yii::t('standard', 'postpone the unlock until {time} {measure unit} after prerequisites are met',
				array('{time}' => $input, '{measure unit}' => $select)) ?>

		</label>
	</div>
</div>
<?php $this->endWidget(); ?>
<div class="modal-footer">
	<input class="btn confirm-btn btn-submit" type="button" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" name="submit" data-submit-form="select-course-form"/>
	<input class="btn close-btn" type="button" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>

<script type="text/javascript">
	$(function() {
		afterGridViewUpdate('curricula-assign-prerequisites-grid');
		replacePlaceholder();

		$('.coursepath-mode').find(':radio').unbind('change').change(function() {
			if ($(this).val() === 'all') {
				$('#<?= CHtml::activeId($courseModel, 'min_courses') ?>').attr('disabled',true);
				showConfirmButton();
			} else {
				$('#<?= CHtml::activeId($courseModel, 'min_courses') ?>').attr('disabled',false).focus();
				hideConfirmButton();
			}
		});
		$('#<?= CHtml::activeId($courseModel, 'min_courses') ?>').unbind('keyup').bind('keyup', function() {
			var minCourses = parseInt($(this).val());
			if (minCourses > 0) {
				showConfirmButton();
			} else {
				hideConfirmButton();
			}
		});
	});
	$('#delayTime, #delayTimeUnit').click(function (e) {
		var checkBox = $('#isDelay');
		if (checkBox.is(':checked')) {
			checkBox.prop('checked', true);
		} else {
			checkBox.prop('checked', false);
		}
		e.stopPropagation();
	});
</script>