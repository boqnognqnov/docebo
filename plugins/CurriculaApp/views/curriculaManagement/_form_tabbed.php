<?php 
	$thumbnailCarouselUrl = Docebo::createAdminUrl('CurriculaApp/CurriculaManagement/renderThumbnailsCarousel', array(
		'loId'		=> $model->id_path,
		'courseId' => $model->id_path,
	));
?>

<ul class="nav nav-tabs" id="myTab">
	<li class="active"><a href="#" data-target="#lp-details"><?=Yii::t('standard', '_DETAILS')?></a></li>
	<li><a href="#" data-target="#lp-settings"><?=Yii::t('standard', 'Settings')?></a></li>
	<li><a href="#" data-target="#lp-share"><?=Yii::t('app7020', 'Share')?></a></li>

	<? if(PluginManager::isPluginActive('EcommerceApp')): ?>
		<li id="lp-ecommerce-tab-link" style="display: <?=$model->visible_in_catalog ? 'block' : 'none'?>">
			<a href="#" data-target="#lp-ecommerce">
				<?=Yii::t('app', 'ecommerce')?>
			</a>
		</li>
	<? endif; ?>

	<? if(PluginManager::isPluginActive('ShopifyApp')): ?>
		<li id="lp-ecommerce-tab-link" style="display: <?=$model->visible_in_catalog ? 'block' : 'none'?>">
			<a href="#" data-target="#lp-shopify">
				<?=Yii::t('app', 'Shopify')?>
			</a>
		</li>
	<? endif; ?>

</ul>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'curricula_form',
	'htmlOptions' => array(
		'class' => 'form',
		'enctype' => 'multipart/form-data'
	 ),
)); ?>
	<div class="tab-content" style="margin-left: 225px">
		<div class="tab-pane active" id="lp-details">

			<div class="control-group">
				<?php echo $form->labelEx($model, 'path_name', array('class'=>'control-label')); ?>
				<div class="controls">
					<?php echo $form->error($model, 'path_name'); ?>
					<?php echo $form->textField($model, 'path_name'); ?>
				</div>
			</div>

			<!-- Thumbnails sliders, AJAX loaded on document ready, see JS in THIS file -->
			<div class="loSlider"
					 data-refresh-url="<?= $thumbnailCarouselUrl ?>"
					 data-idcourse="<?= !empty($model->id_path) ? $model->id_path : '999' ?>"
					 data-lo-id="<?= !empty($model->id_path) ? $model->id_path : '999' ?>">
			</div>

			<div class="control-group description-area">
				<?php echo $form->labelEx($model, 'path_descr', array('class'=>'control-label')); ?>
				<div class="controls">
					<?php echo $form->error($model, 'path_descr'); ?>
					<?php echo $form->textArea($model, 'path_descr', array('class' => 'tinymce', 'id' => 'path-textarea-descr')); ?>
				</div>
			</div>

			<div class="control-group">
				<?php echo $form->labelEx($model, 'path_code', array('class'=>'control-label')); ?>
				<div class="controls">
					<?php echo $form->error($model, 'path_code'); ?>
					<?php echo $form->textField($model, 'path_code'); ?>
				</div>
			</div>

		</div>
		<div class="tab-pane" id="lp-settings">

			<div class="control-group">
				<div class="controls">
					<div>
						<?=CHtml::activeCheckBox($model, 'visible_in_catalog', array())?>
						<span style="margin-left: 10px;"><?=Yii::t('coursepath', 'Show this learning plan in the courses catalog')?></span>
					</div>
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<div>
						<?=CHtml::activeCheckBox($model, 'catchup_enabled')?>
						<?=CHtml::activeLabelEx($model, 'catchup_enabled', array('style' => 'margin-left: 10px; display: inline'));?><br>
						<span style="margin-left: 33px; font-size: 11px;"><?=Yii::t('coursepath', 'Catch-up courses can be applied ONLY to {catchup_types} courses', array('{catchup_types}' => implode(', ', LearningCourse::getCatchupTypesTranslated())))?></span>
					</div>
				</div>
				<div class="controls catchup_limit" style="margin: 5px 0px 0px 33px; display: <?=$model->catchup_enabled ? 'block' : 'none'?>;">
					<div>
						<?=CHtml::activeCheckBox($model, 'catchup_limit_tmp', array())?>
                        <?=Yii::t('coursepath', 'Limit to')?>
						<?=CHtml::activeTextField($model, 'catchup_limit', array('style' => 'width: 30px; margin: 3px 5px 0px 5px;'))?>
                        <?=Yii::t('coursepath', 'catch-up courses per user in this learning plan')?>
					</div>
				</div>
			</div>

			<hr/>

			<div class="control-group">
				<?=CHtml::activeLabel($model, 'days_valid', array('class'=>'control-label'))?>
				<div class="controls">
					<div class="row-fluid">
						<div class="span12">
							<?php echo CHtml::activeTextField($model, 'days_valid', array(
								'class'=>'pull-left',
								'style'=>'width: 150px; margin-right: 10px;'
							)); ?>
							<div class="muted">
								<?=Yii::t('coursepath', 'This setting allows users to access this learning plan only for a specific number of days (days of validity)')?>
							</div>
						</div>

						<div class="span4 daysValidityRadio">
							<label>
								<?=$form->radioButton($model, 'days_valid_type', array('value'=>LearningCoursepath::DAYS_VALID_TYPE_FIRST_COURSE_ACCESS, 'uncheckValue'=>NULL))?>
								<?=Yii::t('course', 'after first course access')?>
							</label>
						</div>
						<div class="span8 daysValidityRadio">
							<label>
								<?=$form->radioButton($model, 'days_valid_type', array('value'=>LearningCoursepath::DAYS_VALID_TYPE_BEING_ENROLLED, 'uncheckValue'=>NULL))?>
								<?=Yii::t('course', 'after being enrolled in the learning plan')?>
							</label>
						</div>
					</div>
				</div>
			</div>

			<div class="clearfix"></div>

			<? if(!$model->getIsNewRecord()): ?>
			<div class="control-group">
				<div class="controls">
					<div>
						<?=CHtml::checkBox('reset_days_validity')?>
						<span style="margin-left: 10px;" id="alreadyStartedLabel"></span>
					</div>
				</div>
			</div>
			<? endif; ?>

			<hr/>

			<?php if($showChannelsSelector): ?>
			<div class="control-group">
				<?php echo CHtml::label(Yii::t('app7020', 'Channels'), 'channels', array('class'=>'control-label')); ?>
				<div class="controls">
					<?php $this->widget('lms.protected.modules.channels.widgets.Selector', array('itemType' => App7020ChannelAssets::ASSET_TYPE_LERNING_PLANS, 'item' => $model, 'form' => $form)); ?>
				</div>
			</div>
			<?php endif; ?>

			<div class="control-group">
				<?php echo CHtml::label(Yii::t('player', 'Certificate'), 'certificate', array('class'=>'control-label')); ?>
				<div class="controls">
					<?php echo CHtml::dropDownList('certificate', $model->certificate->id_certificate, array(Yii::t('standard', '_NONE')) + CHtml::listData(LearningCertificate::model()->findAll(), 'id_certificate', 'name')); ?>
				</div>
			</div>

			<? if(PluginManager::isPluginActive('CertificationApp')): ?>
				<div class="control-group">
					<?php echo CHtml::label(Yii::t('certification', 'Certifications & Retraining'), 'certification', array('class'=>'control-label')); ?>
					<div class="controls">
						<?php echo CHtml::dropDownList('certification', ($model->certification ? $model->certification->id_cert : null), array(Yii::t('standard', '_NONE')) + CHtml::listData(Certification::model()->findAll('deleted = 0'), 'id_cert', 'title')); ?>
					</div>
				</div>
			<? endif; ?>

			<div class="control-group">
				<?php echo $form->labelEx($model, 'credits', array('class'=>'control-label')); ?>
				<div class="controls">
					<?php echo $form->error($model, 'credits'); ?>
					<?php echo $form->textField($model, 'credits'); ?>
				</div>
			</div>

			<?php
			$html = '';
			Yii::app()->event->raise('LearningPlanCustomAttributes', new DEvent($this, array(
				'html' => &$html,
				'learning_plan' => $model,
				'form' => $form
			)));
			echo $html;
			?>

			<?php /* Deep LINK */
				$deepLink = $model->getDeeplink();
			?>
		</div>

		<div class="tab-pane" id="lp-share">
			<div class="control-group">
				<div class="controls">
					<div>
						<?= CHtml::activeCheckBox($model, 'deep_link', array()) ?>
						<?= CHtml::activeLabelEx($model, 'deep_link', array('style' => 'margin-left: 10px; display: inline')); ?>
						<br>
						<span style="margin-left: 33px; font-size: 11px; display: inline-block;">
							<?= Yii::t('coursepath', 'Generate a URL to share this learning plan directly from the learning plan page. Automatic enrollment will be performed for users that will open the URL without being enrolled.') ?>
						</span>
					</div>
				</div>
				<?php if ($model->id_path) : ?>
					<div class="controls deep_link_demo"
							 style="margin: 5px 0px 0px 33px; display: <?= $model->deep_link ? 'block' : 'none' ?>;">
						<div class="input-group">
							<input style="width: 83%" id="copy-example" type="text" class="form-control" readonly="readonly"
										 value="<?= $deepLink ?>"/>
							<input rel="tooltip" data-original-title="<?= Yii::t('course', 'Copied to Clipboard!'); ?>"
										 id="copy_button" data-clipboard-target="#copy-example"
										 style="margin-bottom: 14px; margin-left: -4px; padding-top: 9px;"
										 class="btn btn-docebo black tooltip big" type="button"
										 value="<?php echo Yii::t('coursepath', 'Copy'); ?>"/>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>

		<?php if (PluginManager::isPluginActive('EcommerceApp')) : ?>
			<div class="tab-pane" id="lp-ecommerce">

				<?php
				if (LearningCoursepath::hasCspObjects($model->id_path)):
				?>

				<div class="no-csp-notification-box">
					<div class="warning-message-yellow">
						<i class="fa fa-info-circle fa-2x"></i>
						<?= Yii::t('marketplace', 'You cannot sell course plans containing training materials from content service providers') ?>
					</div>
				</div>

				<?php
				else:
				?>

				<div class="control-group" id="lp-selling-options">
					<div class="controls">
						<div>
							<?= CHtml::activeCheckBox($model, 'is_selling', array()) ?>
							<span style="margin-left: 10px;"><?=Yii::t('coursepath', 'Put this learning plan on sale')?></span>
						</div>

						<br/>

						<div style="margin-left:30px;">
							<div class="pull-left" style="margin-right:10px;margin-top:7px;"><?= Yii::t('order', 'Price') ?>:</div>
							<div class="input-append">
								<?= CHtml::activeTextField($model, 'price', array('class'=>'span4')) ?>
								<span class="add-on" style="height: 23px;"><?php echo Settings::get('currency_symbol', '-');?></span>
							</div>
						</div>

						<?php
							$html = '';
							Yii::app()->event->raise('LearningPlanCustomEcommerceAttributes', new DEvent($this, array(
								'html' => &$html,
								'learning_plan' => $model,
								'form' => $form
							)));
							echo $html;
						?>

						<br/>

						<div style="margin-left:30px;">
							<div>
								<div class="pull-left">
									<?= CHtml::activeRadioButton($model, 'purchase_type', array('value'=>LearningCoursepath::PURCHASE_TYPE_FULL_PACK_ONLY, 'checked'=>true)) ?>
								</div>
								<div class="pull-left" style="margin-left: 10px; width: 500px;">
									<p><?=Yii::t('coursepath', 'Users must buy the full learning plan')?></p>
									<p style="color: #888;"><?=Yii::t('coursepath', "Users won't be able to buy single courses and the price is the one fixed above.")?></p>
								</div>
								<div class="clearfix"></div>
							</div>

							<br/>

							<div>
								<div class="pull-left">
									<?= CHtml::activeRadioButton($model, 'purchase_type', array('value'=>LearningCoursepath::PURCHASE_TYPE_SINGLE_COURSES_IN_SEQUENCE, 'uncheckValue'=>null)) ?>
								</div>
								<div class="pull-left" style="margin-left: 10px; width: 500px;">
									<p><?=Yii::t('coursepath', 'Users decide which courses to buy')?></p>
									<p style="color: #888;"><?=Yii::t('coursepath', "The final sum price is the sum of each single course price. If the price above is fixed and the user selects all the courses, that price will be used.")?></p>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>

					<?php
					endif;
					?>

				</div>
			</div>
		<?php endif; ?>

		<?php if (PluginManager::isPluginActive('ShopifyApp')) : ?>
			<div class="tab-pane" id="lp-shopify">

				<?php
				if (LearningCoursepath::hasCspObjects($model->id_path)):
				?>

				<div class="no-csp-notification-box">
					<div class="warning-message-yellow">
						<i class="fa fa-info-circle fa-2x"></i>
						<?= Yii::t('marketplace', 'You cannot sell course plans containing training materials from content service providers') ?>
					</div>
				</div>

				<?php
				else:
				?>

				<div class="control-group" id="lp-selling-options">
					<div class="controls">
						<div>
							<?=CHtml::activeCheckBox($model, 'is_selling', array())?>
							<span style="margin-left: 10px;"><?=Yii::t('coursepath', 'Put this learning plan on sale')?></span>
						</div>

						<br/>

						<div>
							<div class="pull-left price-label">
								<label for="LearningCoursepath_price">
									<?php
									echo Yii::t('transaction', '_PRICE');
									$_currency_code = Settings::get('currency_code', '');
									if (!empty($_currency_code)) {
										echo '&nbsp;('.$_currency_code.')';
									}
									?>
								</label>
							</div>
							<div class="input-append">
								<?=CHtml::activeTextField($model, 'price', array('class'=>'span4 price-input', 'readonly' => ($model->is_selling ? 'readonly' : '')))?>
								<!--<span class="add-on" style="height: 23px;"><?php echo Settings::get('currency_symbol', '-');?></span>-->
							</div>
						</div>

						<?php
						$html = '';
						Yii::app()->event->raise('LearningPlanCustomEcommerceAttributes', new DEvent($this, array(
							'html' => &$html,
							'learning_plan' => $model,
							'form' => $form
						)));
						echo $html;
						?>

					</div>
				</div>

				<?php
				endif;
				?>

			</div>
		<?php endif; ?>
	</div>
<?php $this->endWidget(); ?>

<div class="modal-footer">
	<input class="btn confirm-btn" type="button" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" name="formsubmit" data-submit-form="curricula_form"/>
	<input class="btn close-btn" type="button" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>

<script type="text/javascript">
		$(function(){

			$('input[name="LearningCoursepath[visible_in_catalog]"]').change(function () {
				$("#lp-ecommerce-tab-link").toggle(this.checked);
			});

		// Load thumbnails carousel slider by AJAX
		var url = $('.loSlider').data('refresh-url');
		$.get(url, function(data) {
			$('.loSlider').html(data.content);
			$(document).controls();
			$('.loSlider input').styler();
		});

		$('#LearningCoursepath_catchup_enabled').change(function(){
			if ($(this).is(':checked'))
				$('.catchup_limit').show();
			else
				$('.catchup_limit').hide();
		});

		$('#LearningCoursepath_catchup_limit_tmp').change(function(){
			if ($(this).is(':checked'))
				$('#LearningCoursepath_catchup_limit').removeAttr('readonly').css('background-color', 'transparent');
			else
				$('#LearningCoursepath_catchup_limit').attr('readonly', 'readonly').css('background-color', '#fafafa');
		}).change();

		//Change Days of validity type
		$(document).on('change', '[name*=days_valid_type]', function(){
			changeEnrollmentsExpireDatesLabel();
		});

		var changeEnrollmentsExpireDatesLabel = function(){
			var label = "<?php echo Yii::t('coursepath', 'Update all enrollment expiration dates when users have already started this learning plan'); ?>";

			if ($('[name*=days_valid_type]:checked').val() == 1){
				label = "<?php echo Yii::t('coursepath', 'Update all enrollment expiration dates when users have already enrolled this learning plan'); ?>";
			}

			$("#alreadyStartedLabel").text(label);
		}

		changeEnrollmentsExpireDatesLabel();
    });

    $('#LearningCoursepath_deep_link').change(function(){
        if ($(this).is(':checked'))
            $('.deep_link_demo').show();
        else
            $('.deep_link_demo').hide();
    });



    
    $("div#popup").ready(function(){ // When the modal popup is load then load the MCE and Fix the Modal Body

            TinyMce.attach("#path-textarea-descr", {height: 150}); // Load the TinyMCE

        var modalBody = $('div.modal-body');
        if ($("div.menu").length) { // Check if the menu exist
            var menu = $("div.menu");
            modalBody.css({ // Fix the modal body not to overflow the height of the document and hidden
                "max-height": "none",
                "overflow-y":"auto"
            });
        } else if ($("body.docebo").length) {
            var bodyDcbo = $("body.docebo");
            modalBody.css({
                "max-height": "none",
                "overflow-y":"auto"
            });
        } else {
            modalBody.css({
                "max-height": "none",
                "overflow-y":"auto"
            });
        }
    });

    /* Copy To Clipboard */
    var clipboard = new Clipboard('#copy_button.btn');
    var copy_button = $('#copy_button');
    var options = {
        'trigger' : 'click'
    };

    clipboard.on('success', function(e) {
        copy_button.tooltip(options);
        copy_button.tooltip('show');

        copy_button.on('mouseleave', function() {
            copy_button.tooltip('hide');
        });
        e.clearSelection();
    });

</script>
<style>
    div#popup{ position: absolute !important; }

    .nav-tabs > li > a:hover {
        color: #fff;
        background-color: #333;
        text-shadow: 0px 0px 0 #ffffff;
    }
	.daysValidityRadio {
		margin: 5px 0 !important;
	}

	.daysValidityRadio label, .daysValidityRadio label span {
		margin-left: 0px;
	}

	div.no-csp-notification-box {
		padding-right: 17px;
		margin-top: -20px;
	}
</style>
