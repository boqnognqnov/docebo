<div class="main-actions clearfix">
	<h3 class="title-bold"><?php echo Yii::t('standard', '_COURSEPATH'); ?></h3>
	<?php if (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/lms/admin/coursepath/mod')) : ?>
	<ul class="clearfix">
		<li>
			<div>
				<?php echo CHtml::link('<span></span>'.Yii::t('curricula_management', '_NEW_CURRICULA'), Yii::app()->createUrl('CurriculaApp/curriculaManagement/create'), array(
					'class' => 'popup-handler new-curricula',
					'data-modal-title' => Yii::t('curricula_management', '_NEW_CURRICULA'),
					'data-modal-class' => 'new-curricula',
					'alt' => Yii::t('helper', 'New curricula'),
					//'data-after-close' => 'newUserAfterClose();',
					'data-after-send' => 'newCurriculaAfterSubmit(data);',
					'data-after-close' => 'function(){TinyMce.removeEditorById("path-textarea-descr");}',
					'data-before-send' => 'disableModalSubmitButton()',
				)); ?>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>
			<p></p>
		</div>
	</div>
	<?php endif; ?>
</div>