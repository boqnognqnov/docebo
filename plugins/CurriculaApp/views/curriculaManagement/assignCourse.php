<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'curricula-assign-course-form',
	'htmlOptions' => array('class' => 'ajax-grid-form', 'data-grid' => '#curricula-assign-course-grid'),
)); ?>
<div class="main-section">

	<div class="filters-wrapper">
		<div class="selections" data-grid="curricula-assign-course-grid">
			<div class="input-wrapper">
				<input data-url="<?= Docebo::createAppUrl('lms:CurriculaApp/curriculaManagement/assignCourseAutocomplete') ?>" data-type="core" data-id="<?php echo $curriculaModel->id_path; ?>" class="typeahead" id="enroll-advanced-search-course" autocomplete="off" type="text" name="LearningCourse[name]" placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"  data-poweruser-not="<?php echo $model->powerUserManager->idst; ?>"/>
				<span class="search-icon"></span>
			</div>
			<?php $this->widget('ext.local.widgets.GridViewSelectAll', array(
				'gridId' => 'curricula-assign-course-grid',
				'class' => 'left-selections',
				'dataProvider' => $model->dataProviderCurricula($curriculaModel->id_path),
			)); ?>
		</div>
	</div>
</div>

<input type="hidden" name="contentType" value="html"/>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'select-course-form',
	'htmlOptions' => array(
		'data-grid-items' => 'true',
	),
)); ?>

<div id="grid-wrapper" class="courseEnroll-course-table">
	<h2><?php echo Yii::t('report', '_REPORT_COURSE_SELECTION'); ?></h2>
	<?php

		$_columns = array();

		$_columns[] = array(
			'class' => 'CCheckBoxColumn',
			'selectableRows' => 2,
			'id' => 'curricula-assign-course-grid-checkboxes',
			'value' => '$data->idCourse',
			'checked' => 'in_array($data->idCourse, Yii::app()->session[\'selectedItems\'])',
		);

		$_columns[] = 'code';

		$_columns[] = array(
			'name' => 'name',
			'htmlOptions' => array('class' => 'wrapped'),
			'type' => 'raw'
		);

		if (PluginManager::isPluginActive('ClassroomApp')) {
			$_columns[] = array(
				'id' => 'course_type',
				'value' => '$data->getCourseTypeTranslation()',
				'header' => Yii::t('course', '_COURSE_TYPE')
			);
		}

		$this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'curricula-assign-course-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			'dataProvider' => $model->dataProviderCurricula($curriculaModel->id_path),
			'columns' => $_columns,
			'template' => '{items}{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
			'pager' => array(
				'class' => 'ext.local.pagers.DoceboLinkPager',
			),
			'beforeAjaxUpdate' => 'function(id, options) {
								options.type = "POST";
								if (options.data == undefined) {
									options.data = {
										contentType: "html",
									}
								}
								options.data.selectedItems =  $(\'#\'+id+\'-selected-items-list\').val();
								options.data.search =  $(\'#enroll-advanced-search-course\').val();
							}',
			'ajaxUpdate' => 'curricula-assign-course-grid-all-items',
			'afterAjaxUpdate' => 'function(id, data){$("#curricula-assign-course-grid input").styler(); afterGridViewUpdate(id);}',
			'cssFile' => false,
		));
	?>
</div>
<?php $this->endWidget(); ?>
<div class="modal-footer">
	<input class="btn confirm-btn" type="button" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" name="submit" data-submit-form="select-course-form"/>
	<input class="btn close-btn" type="button" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>

<script type="text/javascript">
	(function ($) {
		$(function () {
			replacePlaceholder();
		});
	});
	$(function(){
		$('#popup.curricula-assign-courses').on('hidden', function () {
			$('#popup.curricula-assign-courses').remove();
		});
	});
</script>