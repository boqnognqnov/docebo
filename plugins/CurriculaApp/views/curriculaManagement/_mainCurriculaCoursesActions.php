<div class="main-actions clearfix">
	<h3 class="title-bold back-button">
		<?php echo CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('CurriculaApp/curriculaManagement/index')); ?>
		<span><?php echo Yii::t('standard', 'Assigned courses'); ?></span>
	</h3>
	<?php if (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/lms/admin/coursepath/mod')) : ?>
	<ul class="clearfix">
		<li>
			<div>
				<?php echo CHtml::link('<span></span>'.Yii::t('standard', '_ASSIGN_COURSES'), Yii::app()->createUrl('CurriculaApp/curriculaManagement/assignCourse', array('id' => $model->id_path)), array(
					'class' => 'popup-handler curricula-assign-courses',
					'data-modal-title' => Yii::t('standard', '_ASSIGN_COURSES'),
					'data-modal-class' => 'curricula-assign-courses',
					'alt' => Yii::t('helper', 'Curricula assign courses'),
					'data-after-send' => 'newCurriculaCourseAfterSubmit(data)',
				)); ?>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>
			<p></p>
		</div>
	</div>
	<?php endif; ?>
</div>