<?php
/* @var $form CActiveForm */
/* @var $settings CurriculaAppSettingsForm */
$this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', '_COURSEPATH') => Docebo::createAppUrl('admin:CurriculaApp/curriculaManagement/index'),
	Yii::t('standard','Settings')
);
?>

<?php DoceboUI::printFlashMessages(); ?>
<a href="<?= ('it' == Yii::app()->getLanguage()) ?	'https://www.docebo.com/it/knowledge-base/elearning-come-attivare-e-gestire-lapp-percorsi-formativi/'
	: 'https://www.docebo.com/knowledge-base/elearning-how-to-manage-curricula/' ?>" target="_blank" class="app-link-read-manual"><?= Yii::t('apps', 'Read Manual'); ?></a>

<div class="advanced-main" id="advanced-catalog-settings-form">
	<?php
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'app-settings-form',
			'htmlOptions'=>array('class' => 'form-horizontal'),
		));
	?>

	<div class="section">
		<h3 class="advanced-catalog-settings-form-title"><?= Yii::t('standard', '_COURSEPATH') ?></h3>
		<br />

		<div class="row odd">
			<div class="row">
				<div class="setting-name">
					<?php echo CHtml::label(Yii::t('standard', 'Redirect behaviour'), 'CurriculaAppSettingsForm'); ?>
				</div>
				<div class="values">
					<div class="checkbox-item">
						<?=$form->checkBox($settings, 'redirect_to_learning_plan');?>
						<?=$form->labelEx($settings, 'redirect_to_learning_plan', array('class'=>''));?>
						<?=$form->error($settings, 'redirect_to_learning_plan'); ?>
					</div>
				</div>
			</div>
		</div>

		<div class="form-actions">
			<a href="<?=Docebo::createLmsUrl("app/index");?>" class="btn-docebo black big"><?=Yii::t('standard', '_CANCEL');?></a>
			&nbsp;
			<?=CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class'=>'btn-docebo green big')); ?>
		</div>

	</div>
	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
	$('input,select').styler();
</script>
