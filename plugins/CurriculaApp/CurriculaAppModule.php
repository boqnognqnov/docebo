<?php

class CurriculaAppModule extends CWebModule {

    /**
     * @var string Path to assets for this module
     */

    protected $_assetsUrl = null;
    protected $_playerAssetsUrl = null;

	public function init() {
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'CurriculaApp.models.*',
			'CurriculaApp.components.*',
			'CurriculaApp.controllers.*',
			'CurriculaApp.widgets.*',
		));

		Yii::app()->event->on(EventManager::EVENT_STUDENT_COMPLETED_COURSE, array(new LearningPlanEventListener(), 'detectLearningPlanCompletion'));
		Yii::app()->event->on(EventManager::EVENT_STUDENT_COMPLETED_COURSE, array(new LearningPlanEventListener(), 'checkCatchupCompletions'));
		Yii::app()->event->on('StudentFailedILTSession', array(new LearningPlanEventListener(), 'checkCatchupAvailability'));

		Yii::app()->event->on('CollectMyDashboardDashletWidgets', array($this, 'provideDashlet'));
		Yii::app()->event->on('CollectCustomMyKPIs', array($this, 'provideLastCompletedLpKPI'));
		Yii::app()->event->on('CollectProgressiveJobRun', array(new LearningPlanLib(), 'progressiveJobRun'));

		$r = Yii::app()->request->getParam("r");
		if ($r == 'CurriculaApp/curriculaManagement/users') {
			$cs = Yii::app()->getClientScript();
			$cs->registerScriptFile($this->getAssetsUrl() . '/js/catchup-dialog.js');
			$cs->registerCssFile($this->getAssetsUrl() . '/css/catchup-dialog.css');
		}
		if ($r == 'CurriculaApp/curriculaManagement/courses') {
			$cs = Yii::app()->getClientScript();
			$cs->registerCssFile($this->getAssetsUrl() . '/css/courses-dialog.css');
		}
	}


	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu() {
		// Add menu voices !
		Yii::app()->mainmenu->addAppMenu('curricula', array(
			'icon' 			=> 'admin-ico curriculas',
			'link' 			=> Docebo::createAdminUrl('//CurriculaApp/curriculaManagement/index'),
			'label' 		=> Yii::t('standard', '_COURSEPATH'),
			'settings' 		=> Docebo::createLmsUrl('//CurriculaApp/CurriculaApp/settings'),
			'permission' 	=> '/lms/admin/coursepath/view',
			'app_icon' => 'fa-th-list', // icon used in the new menu
		), array());
	}


	/**
	 * Return the info needed to allow the management of this admin in the lms
	 * @return array
	 */
	public function getRoleIds() {
		$roles = array();
		$roles['learning_plans'] = array(
			'title' => Yii::t('standard', '_COURSEPATH'),
			'roleId' => '/lms/admin/coursepath/',
		);
		return $roles;
	}


	public function beforeControllerAction($controller, $action) {
		if (parent::beforeControllerAction($controller, $action)) {
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		} else {
			return false;
		}
	}

	public function provideDashlet(DEvent $event){
		if(!isset($event->params['descriptors'])){
			return;
		}

		$dashletDescriptorLearningplan = DashletLearningPlan::descriptor();

		$collectedSoFarHandlers = array();
		foreach($event->params['descriptors'] as $descriptorObj){
			if ($descriptorObj instanceof DashletDescriptor) {
				$collectedSoFarHandlers[] = $descriptorObj->handler;
			}
		}

		if(!in_array($dashletDescriptorLearningplan->handler, $collectedSoFarHandlers)){
			// Add the MyBlogApp dashlet to the event params
			// so the Blog dashlet is available everywhere
			$event->params['descriptors'][] = $dashletDescriptorLearningplan;
		}
	}

	public function provideLastCompletedLpKPI(DEvent $event){
		$customKpis = &$event->params['kpis'];
		$customKpis['last_lp_progress'] = Yii::t( 'dashlets', 'Last learning plan progress' );
	}
    public function getAssetsUrl()
    {
        if ($this->_assetsUrl === null) {
            $this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('CurriculaApp.assets'));
        }
        return $this->_assetsUrl;
    }
}
