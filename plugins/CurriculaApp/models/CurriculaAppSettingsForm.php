<?php

/**
 * CurriculaAppSettingsForm class.
 *
 * @property string $redirect_to_learning_plan
 */
class CurriculaAppSettingsForm extends CFormModel {
	public $redirect_to_learning_plan;


	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			array('redirect_to_learning_plan', 'safe'),
		);
	}


	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array(
			'redirect_to_learning_plan' => Yii::t('configuration', 'Redirect the learner to the Learning Plan page after completing any course inside a learning plan'),
		);
	}


	public function beforeValidate() {
		$this->redirect_to_learning_plan = ($this->redirect_to_learning_plan ? 'on' : 'off');
		return parent::beforeValidate();
	}

	/**
	 * Return available sorting fields and their labels for LP enrollment list
	 *
	 * @return array
	 */
	public static function getOrderList(){
		return array(
			'date_assign'					=> Yii::t('standard', 'Enrollment date').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			'date_assign DESC' 				=> Yii::t('standard', 'Enrollment date').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
			'coreUser.userid'				=> Yii::t('standard', '_USERNAME').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			'coreUser.userid DESC' 			=> Yii::t('standard', '_USERNAME').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
			'coreUser.firstname'			=> Yii::t('standard', '_FIRSTNAME').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			'coreUser.firstname DESC' 		=> Yii::t('standard', '_FIRSTNAME').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
			'coreUser.lastname'				=> Yii::t('standard', '_LASTNAME').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			'coreUser.lastname DESC' 		=> Yii::t('standard', '_LASTNAME').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
		);
	}

}
