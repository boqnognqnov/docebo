<?php

class CurriculaManagementController extends AdminController {


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl' => 'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		$res[] = array(
			'allow',
			'roles' => array('/lms/admin/coursepath/view'),
			'actions' => array(
				'index',
				'order',
				'courses',
				'users',
				'assignUser',
				'unassignUser',
				'assignedUserAutocomplete',
				'assignedCourseAutocomplete',
				'assignUsersToPlan',
				'curriculaAutocomplete',
				'editEnrollment',
				'resetDates',
				'deleteCertificates',
				'renderThumbnailsCarousel',
				'catchup',
				'axProgressiveAssignUsers',
				'axProgressiveUnassignUsers'
			),
		);

		$res[] = array(
			'allow',
			'roles' => array('/lms/admin/coursepath/mod'),
			'actions' => array(
				'create',
				'update',
				'delete',
				'assignCourse',
				'unassignCourse',
				'assignPrerequisites',
				'assignCourseAutocomplete',
				'assignPrerequisitesAutocomplete',
				'renderThumbnailsCarousel',
				'deleteImage',
				'curriculaAutocomplete',
				'axAssignCatchup',
				'axRemoveCatchup',
				'catchup'
			),
		);

		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => array('settings'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}


	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {
		Yii::app()->tinymce->init();

		$cs = Yii::app()->getClientScript();
		$themeUrl = Yii::app()->theme->baseUrl;
		$cs->registerCssFile($themeUrl.'/css/fcbkcomplete.css');
		$cs->registerScriptFile($themeUrl.'/js/jquery.fcbkcomplete.js');
		$cs->registerScriptFile($themeUrl . '/js/clipboard.min.js');

		$curriculaModel = new LearningCoursepath('curricula_management');
		if (Yii::app()->user->getIsPu()) {
			$curriculaModel->powerUserManager = CoreUser::model()->findByPk(Yii::app()->user->id);
		}
		$this->render('index', array(
			'curriculaModel' => $curriculaModel,
		));
	}


	public function actionCreate() {
		if (Yii::app()->request->isAjaxRequest) {
			$curriculaModel = new LearningCoursepath();
			$curriculaModel->certificate = new LearningCertificateCoursepath();

			if (isset($_POST['LearningCoursepath'])) {
				$curriculaModel->attributes = $_POST['LearningCoursepath'];
				$curriculaModel->certificate->id_certificate = $_POST['certificate'];
				if ($curriculaModel->save()) {

					// If we are passed channels, lets assign this course to them
					if(!empty($_POST['LearningCoursepath']['channels']) && is_array($_POST['LearningCoursepath']['channels'])) {
						// Remove all channel assignments for this LP
						App7020ChannelAssets::model()->deleteAllByAttributes(array('idAsset' => $curriculaModel->getPrimaryKey(), 'asset_type' => App7020ChannelAssets::ASSET_TYPE_LERNING_PLANS));
						App7020ChannelAssets::massAppendChannelToAnAsset($_POST['LearningCoursepath']['channels'], $curriculaModel->getPrimaryKey(), App7020ChannelAssets::ASSET_TYPE_LERNING_PLANS);
					}

					//check if we are a power-user: if so then self-assign the newly create curricula
					if (Yii::app()->user->isAdmin) {
						$puModel = new CoreUserPuCoursepath();
						$puModel->puser_id = Yii::app()->user->id;
						$puModel->path_id = $curriculaModel->getPrimaryKey();
						$puModel->save();

						// TODO: CoreAdminCourse handling will be upgraded in the future
						$adminCourseModel = new CoreAdminCourse();
						$adminCourseModel->idst_user = Yii::app()->user->id;
						$adminCourseModel->type_of_entry = CoreAdminCourse::TYPE_COURSEPATH;
						$adminCourseModel->id_entry = $curriculaModel->getPrimaryKey();
						$adminCourseModel->save();
					}

					if(isset($_POST['certification'])){
						// Save Certification association (retraining)
						CertificationItem::assignItemToCertification(CertificationItem::TYPE_LEARNING_PLAN, $curriculaModel->getPrimaryKey(), intval($_POST['certification']));
					}

					$event = new DEvent($this, array(
						'learningPlan'=>$curriculaModel,
					));
					Yii::app()->event->raise('NewLPCreated', $event);

					$result['status'] = 'saved';
					$this->sendJSON($result);
				} else {
					$result['status'] = 'error';
					$result['html'] = $this->renderPartial('_form_tabbed', array(
						'model' => $curriculaModel,
					), true);
					$this->sendJSON($result);
					Yii::app()->end();
				}
			}
			else
			{
				$curriculaModel->catchup_limit = $curriculaModel->catchup_limit ? $curriculaModel->catchup_limit : null;
				if ($curriculaModel->catchup_limit)
					$curriculaModel->catchup_limit_tmp = 1;
			}

			$result['html'] = $this->renderPartial('_form_tabbed', array(
				'model' => $curriculaModel,
				'showChannelsSelector' => true
			), true);
			$this->sendJSON($result);
		}
		Yii::app()->end();
	}


	public function actionUpdate($id = null) {
		if (Yii::app()->request->isAjaxRequest) {
			/* @var $curriculaModel LearningCoursepath */
			$curriculaModel = LearningCoursepath::model()->findByPk($id);
			if ($curriculaModel->certificate === null) {
				$curriculaModel->certificate = new LearningCertificateCoursepath();
			}

			if ($curriculaModel === null) {
				$this->sendJSON(array());
			}

			if (isset($_POST['LearningCoursepath'])) {
				$curriculaModel->attributes = $_POST['LearningCoursepath'];

				//--- CSP check ---
				// NOTE: this action is prevented at interface level, anyway an additional check won't hurt ...
				if ($curriculaModel->is_selling) {
					if (LearningCoursepath::hasCspObjects($curriculaModel->id_path)) {
						$result['status'] = 'error';
						$result['html'] = $this->renderPartial('_form_tabbed', array(
							'model' => $curriculaModel,
						), true);
						$this->sendJSON($result);
						Yii::app()->end();
					}
				}
				//---

				// Cert is changed: Clean all assigned certificates to this Curricula
				if ($curriculaModel->certificate->id_certificate != $_POST['certificate']) {
					$assignments = LearningCertificateAssignCp::model()->findAllByAttributes(array('id_path' => $id));
					if ($assignments) {
						foreach ($assignments as $assignment) {
							$assignment->delete();
						}
					}
				}

				$curriculaModel->certificate->id_certificate = $_POST['certificate'];

				if ($curriculaModel->save()) {

					// Remove all channel assignments for this LP
					App7020ChannelAssets::model()->deleteAllByAttributes(array('idAsset' => $curriculaModel->getPrimaryKey(), 'asset_type' => App7020ChannelAssets::ASSET_TYPE_LERNING_PLANS));
					// If we are passed channels, lets assign this course to them
					if(!empty($_POST['LearningCoursepath']['channels']) && is_array($_POST['LearningCoursepath']['channels'])) {
						App7020ChannelAssets::massAppendChannelToAnAsset($_POST['LearningCoursepath']['channels'], $curriculaModel->getPrimaryKey(), App7020ChannelAssets::ASSET_TYPE_LERNING_PLANS);
					}

					if(isset($_REQUEST['certification'])){
						// Save Certification association (retraining)
						CertificationItem::assignItemToCertification(CertificationItem::TYPE_LEARNING_PLAN, $curriculaModel->getPrimaryKey(), intval($_REQUEST['certification']));
					}

					if(isset($_REQUEST['reset_days_validity']) && $_REQUEST['reset_days_validity']){
						// Reset the days of validity to/from dates for all LP enrollments
						$this->resetDates(null, $curriculaModel);
					}

					$event = new DEvent($this, array('model' => $curriculaModel));
					Yii::app()->event->raise('AfterLearningPlanSaveAction', $event);

					if (!$event->shouldPerformAsDefault())
					{
						//inject return_value to the html of the result so we can manipulate the dialog and whatever we want with it
						if ($event->return_value)
							$result['html'] = $event->return_value;
					}
					else
						$result['status'] = 'saved';


					$this->sendJSON($result);
				}
			}
			else
			{
				$curriculaModel->catchup_limit = $curriculaModel->catchup_limit ? $curriculaModel->catchup_limit : null;
				if ($curriculaModel->catchup_limit)
					$curriculaModel->catchup_limit_tmp = 1;
			}

			$result['html'] = $this->renderPartial('_form_tabbed', array(
				'model' => $curriculaModel,
				'showChannelsSelector' => true
			), true);
			// Enable the above to see the new dialog view
			/*$result['html'] = $this->renderPartial('_form', array(
				'model' => $curriculaModel,
			), true);*/
			$this->sendJSON($result);
		}
		Yii::app()->end();
	}


	public function actionDelete($id) {
		if (Yii::app()->request->isAjaxRequest) {
			$curriculaModel = LearningCoursepath::model()->findByPk($id);

			if (empty($curriculaModel)) {
				$this->sendJSON(array());
			}

			if ($_POST['delete_confirm']) {

				// Unsubscribe all users from all courses of the curricula
				// $curriculaModel->unSubscribeUsersFromCourses();

				if ($curriculaModel->delete()) {
					try {
						if ($curriculaModel->certificate !== null) {
							$curriculaModel->certificate->delete();
						}
					} catch (Exception $ex1) {};
					if(PluginManager::isPluginActive('CertificationApp')) {
						try {
							if($curriculaModel->certification !== null){
								$curriculaModel->certification->delete();
							}
						} catch (Exception $ex2) {};
					}

					$event = new DEvent($this, array('model' => $curriculaModel));
					Yii::app()->event->raise('AfterLearningPlanDeleteAction', $event);
					Log::_('DELETION: Coursepath with id '.$id.' deleted by user '.Yii::app()->user->id, CLogger::LEVEL_WARNING);

					$this->sendJSON(array());
				}
			}

			$this->sendJSON(array(
				'html' => $this->renderPartial('delete', array(
						'model' => $curriculaModel,
					), true),
			));
		}
		Yii::app()->end();
	}

	public function actionEditEnrollment($curricula = null, $user = null){

		if(!$user || !$curricula){
			throw new CException('Invalid curricula or user');
		}

		if(Yii::app()->user->getIsPu()){
			if(!CoreUserPU::model()->exists('puser_id=:p AND user_id=:u', array(':p'=>Yii::app()->user->getIdst(), ':u'=>intval($user)))){
				throw new CException('You do not own this user');
			}
		}

		$model = LearningCoursepathUser::model()->findByAttributes(array(
			'idUser'=>$user,
			'id_path'=>$curricula,
		));

		$dateBeginValidity = isset($_POST['LearningCoursepathUser']['date_begin_validity']) ? $_POST['LearningCoursepathUser']['date_begin_validity'] : false;
		$dateExpireValidity = isset($_POST['LearningCoursepathUser']['date_end_validity']) ? $_POST['LearningCoursepathUser']['date_end_validity'] : false;

		if($dateBeginValidity || $dateExpireValidity){
			$localFormat = Yii::app()->localtime->YiitoPHPDateFormat(Yii::app()->localtime->getLocalDateTimeFormat('short', 'short'));

			$dateBeginValidityTimestamp = strtotime(Yii::app()->localtime->fromLocalDate($dateBeginValidity) . " 00:00:00");
			$dateExpireValidityTimestamp = strtotime(Yii::app()->localtime->fromLocalDate($dateExpireValidity) . " 23:59:59");

			$model->date_begin_validity = date($localFormat, $dateBeginValidityTimestamp);
			$model->date_end_validity = date($localFormat, $dateExpireValidityTimestamp);

			if(!$model->save()){
				Yii::log(var_export($model->getErrors(),true), CLogger::LEVEL_ERROR);
			}else{
				Yii::app()->event->raise(EventManager::EVENT_LEARNING_PLAN_UPDATED, new DEvent($this, array('user_id' => $user, 'learning_plan' => $model)));
				$this->sendJSON(array('html' => ''));
			}

		}

		$html = $this->renderPartial('_editEnrollment', array(
			'model' => $model
		), true);
		$this->sendJSON(array('html' => $html));

	}

	public function actionResetDates($user = null, $curricula = null){
		if(!$curricula || !$user){
			throw new CException('Invalid curricula or user');
		}

		/* @var $curriculaModel LearningCoursepath */
		$curriculaModel = LearningCoursepath::model()->findByPk($curricula);


		// Reset the days of validity to/from dates for LP user enrollment
		$this->resetDates($user, $curriculaModel);

		$this->sendJSON(array('success'=>true));
	}

	public function resetDates($userId = null, $curriculaModel = null){
		$condition = ($userId == null) ? 'id_path=:idPath' : 'id_path=:idPath AND idUser=:idUser';
		$params = ($userId == null) ? array(':idPath'=>$curriculaModel->id_path) : array(':idPath'=>$curriculaModel->id_path, ':idUser'=>$userId);

		if($curriculaModel->days_valid){
			if($curriculaModel->days_valid_type == LearningCoursepath::DAYS_VALID_TYPE_BEING_ENROLLED) {
				Yii::app()->getDb()->createCommand()
					->update(LearningCoursepathUser::model()->tableName(), array(
						'date_begin_validity' => new CDbExpression('date_assign'),
						'date_end_validity' => new CDbExpression('DATE_ADD(date_assign, INTERVAL :days DAY)')
					), $condition, array_merge($params, array(':days' => $curriculaModel->days_valid)));
			} else {
				$courses = $curriculaModel->getLPCoursesIds();

				if ($userId == null) {
					foreach ($curriculaModel->coreUsers as $lpUser) {
						$curriculaModel->updateLPUser($courses, $lpUser->idst, true);
					}
				} else {
					$curriculaModel->updateLPUser($courses, $userId, true);
				}
			}
		}else{
			// date_end_validity is infinite
			Yii::app()->getDb()->createCommand()
				->update(LearningCoursepathUser::model()->tableName(), array(
					'date_begin_validity'=>'',
					'date_end_validity'=>''
				), $condition, $params);
		}
	}

	public function actionDeleteCertificates($curricula=null, $user=null){
		if(!$curricula || !$user){
			throw new CException('Invalid curricula or user');
		}

		$models = LearningCertificateAssignCp::model()->findAllByAttributes(array(
			'id_path'=>$curricula,
			'id_user'=>$user,
		));

		$success = true;

		if(empty($models)){
			$success = false;
		}

		foreach($models as $model){
			$success &= $model->delete();
		}

		$this->sendJSON(array('success'=>$success));
	}

	public function actionOrder($id) {
		$ids = $_POST['ids'];
		$command = Yii::app()->db->createCommand();

		if (!empty($ids)) {
			foreach ($ids as $weight => $id_item) {
				$command->update(LearningCoursepathCourses::model()->tableName(), array('sequence' => $weight), 'id_path = :id_path AND id_item = :id_item', array(
					':id_path' => $id,
					':id_item' => $id_item,
				));
			}
		}
		$this->sendJSON(array());
	}


	public function actionCourses($id) {
		Yii::app()->clientScript->registerCssFile('js/farbtastic/farbtastic.css');
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
		Yii::app()->clientScript->registerScriptFile('js/farbtastic/farbtastic.min.js');
		Yii::app()->clientScript->registerScriptFile('js/jquery.tools.min.js');

		$cs = Yii::app()->getClientScript();
		$themeUrl = Yii::app()->theme->baseUrl;

		// FCBK
		$cs->registerCssFile($themeUrl . '/css/fcbkcomplete.css');
		$cs->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js');

		$curriculaModel = LearningCoursepath::model()->findByPk($id);
		$courseModel = LearningCoursepathCourses::model();
		$courseModel->scenario = 'curricula_management';
		$courseModel->id_path = $id;

		if (isset($_REQUEST['LearningCourse'])) {
			$courseModel->idItem = LearningCourse::model();
			$courseModel->idItem->name = $_REQUEST['LearningCourse']['name'];
		}

		$this->render('courses', array(
			'curriculaModel' => $curriculaModel,
			'courseModel' => $courseModel,
		));
	}


	public function actionUsers($id) {
        // FancyTree initialization
        Yii::app()->fancytree->init();

		$curriculaModel = LearningCoursepath::model()->findByPk($id);
		$userModel = LearningCoursepathUser::model();
		$userModel->scenario = 'curricula_management';
		$userModel->id_path = $id;

		if (Yii::app()->user->isAdmin) {
			$userModel->powerUser = CoreUser::model()->findByPk(Yii::app()->user->id);
		}

		if (isset($_REQUEST['CoreUser'])) {
			$userModel->coreUser = CoreUser::model();
			$userModel->coreUser->search_input = $_REQUEST['CoreUser']['search_input'];
			$userModel->coreUser->order_by = $_REQUEST['CoreUser']['order_by'];
		}

		UsersSelector::registerClientScripts();
		DatePickerHelper::registerAssets();

		$this->render('users', array(
			'curriculaModel' => $curriculaModel,
			'userModel' => $userModel,
		));
	}


	public function actionAssignCourse($id) {
		set_time_limit(2*60);
		$model = LearningCourse::model();
		$curriculaModel = LearningCoursepath::model()->findByPk($id);

		if (Yii::app()->user->isAdmin) {
			$model->powerUserManager = CoreUser::model()->findByPk(Yii::app()->user->id);
		}

		if (isset($_REQUEST['LearningCourse'])) {
			$model->attributes = $_REQUEST['LearningCourse'];
		}

		if(isset($_REQUEST['search'])) {
			$model->name = $_REQUEST['search'];
		}

		if ((isset($_REQUEST['curricula-assign-course-grid-selected-items-list']) && ($_REQUEST['curricula-assign-course-grid-selected-items-list'] != ''))
		) {
			$selectedItems = explode(',', $_REQUEST['curricula-assign-course-grid-selected-items-list']);
			if(!empty($selectedItems) && is_array($selectedItems)){
				Yii::app()->session['selectedItems'] = $selectedItems;
			}
		}


		if ((isset($_REQUEST['curricula-assign-course-grid-selected-items']) && ($_REQUEST['curricula-assign-course-grid-selected-items'] != '')))
		{
			$cspErrors = array();
			$totalCourses = 0;

			$courselist = explode(',', $_REQUEST['curricula-assign-course-grid-selected-items']);
			if (!empty($courselist)) {
				$courses_already_in_coursepath =	Yii::app()->db->createCommand()
													->select('id_item')
													->from(LearningCoursepathCourses::model()->tableName())
													->where('id_path = :id_path', array(':id_path' => $id))
													->queryAll();

				$sequence = count($courses_already_in_coursepath);

				foreach ($courselist as $idCourse)
				{

					$course = LearningCourse::model()->findByAttributes(array(
						'idCourse' => $idCourse
					));

					$course_price = $course->getCoursePrice();

					$isLpForSale = ($curriculaModel->price == 0 || empty($curriculaModel->price)) ? false : true;
					$isCourseForSale = ($course_price == 0 || empty($course_price)) ? false : true;

					if(array_search($idCourse, $courses_already_in_coursepath) === false)
					{
						$totalCourses++;

						//--- CSP check ---
						//NOTE: we must deny courses with C.S.P. training materials into selling plans
						if ($curriculaModel->is_selling) {
							if (LearningCourse::hasCspObjects($idCourse)) {
								//this course cannot be added to hte selling LP
								$cspErrors[] = $idCourse;
								continue; //go directly to the next loop of foreach
							}
						}
						//---

						$curriculaCourse = new LearningCoursepathCourses();
						$curriculaCourse->id_path = $id;
						$curriculaCourse->id_item = $idCourse;
						$curriculaCourse->sequence = $sequence;
						if($curriculaCourse->save())
						{
							$sequence++;

							Yii::app()->event->raise('CourseAddedToCoursepath', new DEvent($this, array(
								'targetCourse' => $course,
								'coursepath' => $curriculaModel
							)));
						}
						// Auto enroll users to the assigned course only when the LP and the Course are free
						if($isLpForSale == false && $isCourseForSale == false) {
							$curriculaModel->subscribeUsersToSingleCourse($idCourse);
						}

					}
				}
				// Subscribe all Curricula users to all its courses
				//$curriculaModel->refresh(); // !! Important
				//$curriculaModel->subscribeUsersToCourses();


				Yii::app()->event->raise('NewContentInLearningPlan', new DEvent($this, array(
					'learningPlan'=>$curriculaModel
				)));

				if (!empty($cspErrors)) {
					//tell the client which courses could not be added to the learning plan due to C.S.P. limitations
					$cspErrorsInfo = array();
					foreach ($cspErrors as $cspError) {
						$course = LearningCourse::model()->findByPk($cspError);
						$cspErrorsInfo[] = array(
							'id_course' => $idCourse,
							'code' => $course->code,
							'name' => $course->name
						);
					}
					$html = $this->renderPartial('_csp_errors', array(
						'cspErrors' => $cspErrorsInfo,
						'totalCourses' => $totalCourses
					), true);
					$output = array('html' => $html);
				} else {
					$output = array('status' => 'saved');
				}
				$this->sendJSON($output);
			}
		}

		$processOutput = false;
		if (isset($_REQUEST['popupFirstShow']) && $_REQUEST['popupFirstShow'] === '1') {
			$processOutput = true;
		}

		$result = array();

		$result['html'] = $this->renderPartial('assignCourse', array(
			'model' => $model,
			'curriculaModel' => $curriculaModel,
		), true, $processOutput);

		if (isset($_REQUEST['contentType']) && ($_REQUEST['contentType'] == 'html')) {
			echo $result['html'];
		} else {
			$this->sendJSON($result);
		}
	}


	/**
	 * Get the REQUEST from UsersSelector dialog and assign selected enitites (users, groups, org charts) to a given Catalog
	 *
	 */
	public function actionAssignUsersToPlan() {

		$idPlan = Yii::app()->request->getParam('idPlan', false);

		$usersList = UsersSelector::getUsersList($_REQUEST, false, true);
		$selectedUsers = array();

		if (!empty($usersList)) {

			try {

				$curriculaModel = LearningCoursepath::model()->findByPk($idPlan);

				if (!$curriculaModel) {
					throw new CException(Yii::t('standard', _OPERATION_FAILURE) . " (invalid learning plan)");
				}

				$assignedUsers = Yii::app()->db->createCommand()
					->select('lcu.idUser')
					->from(LearningCoursepathUser::model()->tableName()." lcu")
					->where('lcu.id_path = :id_path', array(':id_path'=>$idPlan))->queryAll();

				$curriculaUsers = array();
				foreach($assignedUsers as $user){
					if(isset($user['idUser'])){
						$idUser = (int) $user['idUser'];
						$curriculaUsers[$idUser] = $user['idUser'];
					}
				}

				if(Yii::app()->user->getIsPu() && CoreUser::adminRuleEnabled(Yii::app()->user->id, 'allow_buy_seats')) {
					$coursesInLp = array();
					foreach($curriculaModel->learningCourse as $course){
						$coursesInLp[] = $course->idCourse;
					}

					$crit = new CDbCriteria();
					$crit->addInCondition('t.course_id', $coursesInLp);
					$crit->compare('t.puser_id', Yii::app()->user->getIdst());
					$crit->index = 'course_id';
					$puSeatInfoPerCourse = CoreUserPuCourse::model()->with('course')->findAll($crit);

					$usersToSubscribeNowCount = 0;
					foreach ($usersList as $userId) {
						if ( ! in_array( $userId, $curriculaUsers ) ) $usersToSubscribeNowCount++;
					}

					$errors = array();
					foreach($puSeatInfoPerCourse as $puSeatInfoForSingleCourse) {
						if($puSeatInfoForSingleCourse->course->selling && $puSeatInfoForSingleCourse->available_seats < $usersToSubscribeNowCount)
							$errors[] = $puSeatInfoForSingleCourse->course->name;
					}

					if(!empty($errors))
						throw new CException('<p style="margin-bottom: 10px;">'.Yii::t('standard', 'Not enough seats for the following courses: {course_names}',
								array('{course_names}'=> '"'.implode('", "', $errors).'"')).'.</p>');
				}
				foreach ($usersList as $userId) {
					if (!in_array($userId, $curriculaUsers)){
						$selectedUsers[] = $userId;
					}
				}
				if (!empty($selectedUsers)){
					$this->renderPartial('progressive_assign_users', array('id_path' => $idPlan, 'ids' => implode(',', $selectedUsers)));
				}
			}
			catch (CException $e) {
				Yii::log($e->getMessage(),'error');
				Yii::app()->user->setFlash('error', $e->getMessage());
				$selectorDialogUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
						'type' 	=> UsersSelector::TYPE_CURRICULA_MANAGEMENT,
						'idPlan'	=> $idPlan,
				));
				$this->redirect($selectorDialogUrl, true);
			}

		}


		echo '<a class="auto-close success"></a>';
		Yii::app()->end();

	}

	protected function _generateCacheKey($prefix = '') {
		return uniqid($prefix).'_'.time();
	}

	protected function _setCachedUsers(array $users) {
		$content = implode(',', $users);
		//generate a key (it will be used as file name for cached data)
		$cacheKey = $this->_generateCacheKey('_learning_plan_users_');
		$pathName = Docebo::getUploadTmpPath();
		$fileName = $cacheKey.'.tmp';
		$file = fopen($pathName . DIRECTORY_SEPARATOR . $fileName, 'w');
		if (!$file) {
			//TODO: handle error
		}
		fwrite($file, $content);
		fclose($file);

		return $cacheKey;
	}

	protected function _getCachedUsers($cacheKey) {
		$pathName = Docebo::getUploadTmpPath();
		$fileName = $cacheKey.'.tmp';
		if (!file_exists($pathName . DIRECTORY_SEPARATOR . $fileName)) { throw new CException('Cache file not found [key: '.$cacheKey.']'); }
		$content = file_get_contents($pathName . DIRECTORY_SEPARATOR . $fileName);
		return explode(',', $content);
	}


	protected function _unsetCachedUsers($cacheKey){
		$pathName = Docebo::getUploadTmpPath();
		$fileName = $cacheKey.'.tmp';
		unlink($pathName . DIRECTORY_SEPARATOR . $fileName);
	}


	public function actionAxProgressiveAssignUsers(){

		//Path must always be specified
		$idPath = Yii::app()->request->getParam('id_path', 0);
		$totalAssignedUsers = Yii::app()->request->getParam('totalAssignedUsers', 0);
		$cacheError = false;

		if ($idPath <= 0) {
			$result['success'] = false;
			$result['message'] = 'Invalid specified learning path';
			$result['status'] = 'error';
			$result['stop'] = true;
			$this->sendJSON($result);
			Yii::app()->end();
		}


		$curriculaModel = LearningCoursepath::model()->findByPk($idPath);

		//read input and prepare cached data
		if (isset($_POST['ids']) && !isset($_POST['_key_'])) {

			//NOTE: if we enter this branch of the "if", then we are starting the progressive operation, since 'ids' input parameter is passed the first time only.

			//Form submission from assigning dialog: read input data and create local file cache
			$usersStr = trim(Yii::app()->request->getParam('ids', ''));
			if (empty($usersStr)) {
				$result['success'] = false;
				$result['message'] = 'No selected users';
				$result['status'] = 'error';
				$result['stop'] = true;
				$this->sendJSON($result);
				Yii::app()->end();
			}
			$users = explode(",", $usersStr);
			$cacheKey = $this->_setCachedUsers($users);

		} else {
			// Get the users array from the cache
			$cacheKey = Yii::app()->request->getParam('_key_', 0);
		}


		//read cached data
		try {
			$res = $this->_getCachedUsers($cacheKey);
		} catch (Exception $e) {
			$cacheError = true;
		}

		if (empty($res) || !is_array($res)) { $cacheError = true; }
		if ($cacheError) {
			//something went wrong while reading cached data ... handle the error
			$result['success'] = false;
			$result['message'] = Yii::t('standard', '_OPERATION_FAILURE');
			$result['status'] = 'error';
			$result['stop'] = true;
			$this->sendJSON($result);
			Yii::app()->end();
		}




		$offset = Yii::app()->request->getParam('offset', 0);
		$totalCount = count($res);

		//define chunk size, depending un users count (smaller chunks will make big operations too slow, and viceversa)
		if ($totalCount < 100) {
			$chunkSize = 10;
		} elseif ($totalCount < 500) {
			$chunkSize = 20;
		} elseif ($totalCount < 1000) {
			$chunkSize = 30;
		} elseif ($totalCount < 3000) {
			$chunkSize = 40;
		} else {
			$chunkSize = 50;
		}

		if(Yii::app()->user->getIsPu() && CoreUser::adminRuleEnabled(Yii::app()->user->id, 'allow_buy_seats')) {
			$coursesInLp = array();
			foreach($curriculaModel->learningCourse as $course){
				$coursesInLp[] = $course->idCourse;
			}

			$crit = new CDbCriteria();
			$crit->addInCondition('t.course_id', $coursesInLp);
			$crit->compare('t.puser_id', Yii::app()->user->getIdst());
			$crit->index = 'course_id';
			$puSeatInfoPerCourse = CoreUserPuCourse::model()->with('course')->findAll($crit);

			$errors = array();
			foreach($puSeatInfoPerCourse as $puSeatInfoForSingleCourse) {
				if($puSeatInfoForSingleCourse->course->selling && $puSeatInfoForSingleCourse->available_seats < ($totalCount - $totalAssignedUsers))
					$errors[] = $puSeatInfoForSingleCourse->course->name;
			}

			if(!empty($errors)){
				$result['success'] = false;
				$result['message'] = Yii::t('standard', 'Not enough seats for the following courses: {course_names}',array('{course_names}'=> '"'.implode('", "', $errors).'"'));
				$result['status'] = 'error';
				$result['stop'] = true;
				$this->sendJSON($result);
				Yii::app()->end();
			}


		}

		$newOffset = $offset + $chunkSize;
		$stop = ($newOffset < $totalCount) ? false : true;

		// Preparing result for importer
		$result = array (
			'usersAssigned' => 0,
			'usersFailed' => 0,
		);


		$counter = 0;
		for ($index = $offset; ($index < $newOffset && $index < $totalCount); $index++) { //NOTE: $newOffset may be greater than $totalCount, but we wouldn't count over $totalCount
			//security check
			$counter++;
			if ($counter > $chunkSize) break; //NOTE: this shouldn't happen

			//read user ID, if we are over user list limit, then invalidate the value and stop this iteration
			$user = (isset($res[$index]) ? $res[$index] : -1);
			if ($user < 0) continue;


			//main operations
			try {
				//validate input user
				if (!$user) { throw new CException('Invalid user (index: '.$index.')'); }

				$userModel = new LearningCoursepathUser();
				$userModel->id_path = $idPath;
				$userModel->idUser = $user;
				$userModel->waiting = 0;
				$userModel->course_completed = 0;
				$userModel->date_assign = Yii::app()->localtime->toLocalDateTime();
				$userModel->subscribed_by = Yii::app()->user->id;
				$userModel->save();
				$curriculaModel->subscribeSingleUserToCourses($user);
				Yii::app()->event->raise(EventManager::EVENT_USER_ENROLLED_IN_LEARNING_PLAN, new DEvent($this, array('user_id' => $user, 'learning_plan' => $curriculaModel)));


				$totalAssignedUsers++;
				$result['usersAssigned']++;

			} catch(Exception $e) {

				Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);

				$result['usersFailed']++;
			}
		}

		if(Yii::app()->user->getIsPu() && CoreUser::adminRuleEnabled(Yii::app()->user->id, 'allow_buy_seats')) {
			// Reduce Power User's allowance/seats in the courses
			// he just enrolled users in
			foreach($puSeatInfoPerCourse as $puSeatInfoPerCourseSingle){
				if($puSeatInfoPerCourseSingle->course->selling == 1) {
					$puSeatInfoPerCourseSingle->available_seats = $puSeatInfoPerCourseSingle->available_seats - $result['usersAssigned'];
				}
				$puSeatInfoPerCourseSingle->save();
			}
		}

		if ($index > $totalCount) { $index = $totalCount; } //normalize value
		$completedPercent = ($totalCount == 0 ? 0 : (number_format($index/$totalCount * 100, 0)));

		$html = $this->renderPartial('assign_progressive_dialog', array(
			'offset' => $offset,
			'assigned' => $result['usersAssigned'],
			'failed' => $result['usersFailed'],
			'processedUsers' => $index,
			'totalUsers' => $totalCount,
			'totalBranches' => count($res['groups']),
			'completedPercent' => $completedPercent,
			'start' => ($offset == 0),
			'stop' => $stop,
			'cacheKey' => $cacheKey
		), true, true);

		//we are finished here, we do not need cache file anymore
		if ($stop){
			$this->_unsetCachedUsers($cacheKey);
		}

		// Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
		// These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stopping when $stop == true)
		$data = array(
			'offset'	=> $newOffset,
			'stop'		=> $stop,
			//specific parameters for this function
			'_key_'		=> $cacheKey,
			'totalAssignedUsers' => $totalAssignedUsers
		);
		$response = new AjaxResult();
		$response->setStatus(true);
		$response->setHtml($html)->setData($data)->toJSON();
		Yii::app()->end();

	}




	public function actionAssignUser($id) {
		if (Yii::app()->request->isAjaxRequest) {
			$result = array();

			$userModel = CoreUser::model();
			$userModel->scenario = 'search';
			$userModel->unsetAttributes();
			if (isset($_REQUEST['CoreUser'])) {
				$userModel->attributes = $_REQUEST['CoreUser'];
			}

			$groupModel = new CoreGroup();
			if (isset($_REQUEST['CoreGroup'])) {
				$groupModel->attributes = $_REQUEST['CoreGroup'];
			}


			// I don't know why this session variable is set, but I'll set it; this is from the old code
			$userListFromGroups = Yii::app()->userSelectHelper->getUsersListFromRequest(array('groups' => 'select-group-grid-selected-items'));
			if (!empty($userListFromGroups)) {
				Yii::app()->session['newsletterGroupList'] = $userListFromGroups;
			}

			// Extract the FULL list of USER IDs from th request
			// These are inputs coming from usersSelector dialog
			$userlist = array();
			$userlist = Yii::app()->userSelectHelper->getUsersListFromRequest(
				array(
					'users' => 'select-user-grid-selected-items',
					'groups' => 'select-group-grid-selected-items',
					'orgChart' => 'select-orgchart',
				)
			);


			// save selected users
			if (!empty($userlist)) {

				$curriculaModel = LearningCoursepath::model()->findByPk($id);
				$courseUsers = CHtml::listData($curriculaModel->coreUsers, 'idst', 'idst');

				foreach ($userlist as $userId) {
					if (!in_array($userId, $courseUsers)) {
						$user = new LearningCoursepathUser();
						$user->id_path = $id;
						$user->idUser = $userId;
						$user->waiting = 0;
						$user->course_completed = 0;
						$user->date_assign = Yii::app()->localtime->toLocalDateTime();
						$user->subscribed_by = Yii::app()->user->id;
						$user->save();
					}
				}

				// Subscribe all Curricula users to all its courses
				$curriculaModel->refresh(); // !! Important
				$curriculaModel->subscribeUsersToCourses();

				$this->sendJSON(array('status' => 'saved'));
			}

			$processOutput = false;
			if (isset($_REQUEST['popupFirstShow']) && $_REQUEST['popupFirstShow'] === '1') {
				$processOutput = true;
			}

			$fancyTreeData = CoreOrgChartTree::buildFancytreeDataArray();
			$result['html'] = $this->renderPartial('assignUser', array(
				'userModel' => $userModel,
				'groupModel' => $groupModel,
				'isNeedRegisterYiiGridJs' => true,
				'fancyTreeData' => $fancyTreeData,
				'preSelected' => array(),
			), true, $processOutput);

			if (isset($_REQUEST['contentType']) && ($_REQUEST['contentType'] == 'html')) {
				echo $result['html'];
			} else {
				$this->sendJSON($result);
			}
		}
	}


	public function actionUnassignCourse($id) {
		if (Yii::app()->request->isAjaxRequest) {
			$id_item = empty($_REQUEST['id_item']) ? null : $_REQUEST['id_item'];

			$curriculaCourses = LearningCoursepathCourses::model()->findAllByAttributes(array(
				'id_path' => $id,
				'id_item' => $id_item,
			));
			if (empty($curriculaCourses)) {
				$this->sendJSON(array());
			}

			if ($_POST['delete_confirm']) {
				$prerequisitesIds = array();
				$courses = array();
				foreach ($curriculaCourses as $course) {
					$courses[] = $course->id_item;
					$catchUps = LearningCourseCatchUp::model()->findAllByAttributes(array(
						'id_path' => $id,
						'id_course' => $course->id_item,
					));
					foreach ($catchUps as $catchup)
					{
						$courses[] = $catchup->id_catch_up;
						$catchup->delete();
					}
					$prerequisitesIds[] = $course->id_item;
					$course->delete();

					Yii::app()->event->raise('CourseRemovedFromCoursepath', new DEvent($this, array(
						'targetCourse' => LearningCourse::model()->findByPk($course->id_item),
						'coursepath' => LearningCoursepath::model()->findByPk($id)
					)));
				}
				$curriculaModel = LearningCoursepath::model()->findByPk($id);
				$curriculaModel->updateCourseSequence();
				$curriculaModel->removePrerequisites($prerequisitesIds);

				//TODO: this should be handled with events instead directly here
				if (PluginManager::isPluginActive('PowerUserApp')) {
					Yii::app()->scheduler->createImmediateJob('update_power_user_selection', PowerUserJobHandler::id(), array('learning_plans' => array((int)$id)));
				}

				$this->sendJSON(array());
			}

			$this->sendJSON(array(
				'html' => $this->renderPartial('unassignCourse', array(
						'courses' => $curriculaCourses,
					), true),
			));
		}
		Yii::app()->end();
	}


	public function actionUnassignUser($id) {
		if (Yii::app()->request->isAjaxRequest) {
			$id_user = empty($_REQUEST['ids']) ? null : $_REQUEST['ids'];
			$arrayOfUsers = array();
			if($id_user){
				$arrayOfUsers = explode( ',', $id_user);
			}


			$assignedUsers = Yii::app()->db->createCommand()
				->select('lcu.idUser')
				->from(LearningCoursepathUser::model()->tableName()." lcu")
				->where('lcu.id_path = :id_path', array(':id_path'=>$id))
				->andwhere(array('in', 'lcu.idUser', $arrayOfUsers))
				->queryAll();

			$curriculaUsers = array();
			foreach($assignedUsers as $user){
				if(isset($user['idUser'])){
					$idUser = (int) $user['idUser'];
					$curriculaUsers[$idUser] = $user['idUser'];
				}
			}

//			$curriculaUsers = array();
//			foreach($assignedUsers as $user){
//				if(isset($user['idUser'])){
//					$idUser = (int) $user['idUser'];
//					$curriculaUsers[$idUser] = $user['idUser'];
//				}
//			}

//			$curriculaUsers = LearningCoursepathUser::model()->with(array('path','user'))->findAllByAttributes(array(
//				'id_path' => $id,
//				'idUser' => $id_user,
//			));



			if (empty($curriculaUsers)) {
				$this->sendJSON(array());
			}

			if ($_POST['delete_confirm']) {
				$users = array();
				foreach ($curriculaUsers as $cuUser) {
					$curriculaUserModel = LearningCoursepathUser::model()->findByAttributes(array(
						'id_path' => $id,
						'idUser' => $cuUser,
					));

					if($curriculaUserModel){

						$curriculaUserModel->delete(); //this the effective unassignment from the learning plan

						Yii::app()->event->raise(EventManager::EVENT_USER_UNENROLLED_FROM_LEARNING_PLAN, new DEvent($this, array(
							'learningPlan' => $curriculaUserModel->path,
							'user' => $curriculaUserModel->user,
						)));

					}
				}

				// NOTE: when unassigning users from learning plans, DO NOT unenroll them from courses too
				/*
				// Get ALL curricula courses and unsubscribe all users that are to be de-assigned from thme
				$courses = LearningCoursepath::model()->findByPk($id)->getCoursesIdList();

				if (CoreUserPU::isPUAndSeatManager()) {
					// The current Power User can manage course subscriptions
					// through seats only

					// Since he is now unsubscrubing users from a Learning Plan,
					// restore seats that are still unused in his courses

					// Get all "unused seats" count based on the selected users
					$query = Yii::app()->getDb()->createCommand()
						->select('idCourse, COUNT(idUser) AS unusedSeats')
						->from(LearningCourseuser::model()->tableName())
						->where(array('IN', 'idCourse', $courses))
						->andWhere(array('IN', 'idUser', $users))
						->andWhere(array('IN', 'status', array(LearningCourseuser::$COURSE_USER_SUBSCRIBED)))
						->group('idCourse')
						->queryAll();
					$unusedSeatsFromSelectedByCourse = array();
					foreach($query as $tmp){
						$unusedSeatsFromSelectedByCourse[$tmp['idCourse']] = $tmp['unusedSeats'];
					}

					foreach($courses as $idCourse){
						CoreUserPU::modifyAvailableSeats($idCourse, $unusedSeatsFromSelectedByCourse[$idCourse]);
					}

				}

				LearningCourseuser::unSubscribeUsersFromCourses($users, $courses);
				*/
				$this->sendJSON(array());
			}

			$this->sendJSON(array(
				'html' => $this->renderPartial('unassignUser', array(
						'users' => $curriculaUsers,
						'idPath' => $id,
					), true),
			));
		}
		Yii::app()->end();
	}


	public function actionAxProgressiveUnassignUsers(){

		$idPath = Yii::app()->request->getParam('id_path', 0);
		if ($idPath <= 0) {
			$result['success'] = false;
			$result['message'] = 'Invalid specified learning path';
			$result['status'] = 'error';
			$result['stop'] = true;
			$this->sendJSON($result);
			Yii::app()->end();
		}



		//read input and prepare cached data
		if (isset($_POST['ids']) && !isset($_POST['_key_'])) {

			//NOTE: if we enter this branch of the "if", then we are starting the progressive operation, since 'ids' input parameter is passed the first time only.

			//Form submission from assigning dialog: read input data and create local file cache
			$usersStr = trim(Yii::app()->request->getParam('ids', ''));
			if (empty($usersStr)) {
				$result['success'] = false;
				$result['message'] = 'No selected users';
				$result['status'] = 'error';
				$result['stop'] = true;
				$this->sendJSON($result);
				Yii::app()->end();
			}
			$users = explode(",", $usersStr);

			$cacheKey = $this->_setCachedUsers($users);

		} else {

			// Get the users array from the cache
			$cacheKey = Yii::app()->request->getParam('_key_', 0);
		}

		//read cached data
		try {
			$res = $this->_getCachedUsers($cacheKey);
		} catch (Exception $e) {
			$cacheError = true;
		}

		if (empty($res) || !is_array($res)) { $cacheError = true; } //make sure that the var type is right

		if ($cacheError) {
			//something went wrong while reading cached data ... handle the error
			$result['success'] = false;
			$result['message'] = Yii::t('standard', '_OPERATION_FAILURE');
			$result['status'] = 'error';
			$result['stop'] = true;
			$this->sendJSON($result);
			Yii::app()->end();
		}

		// Get current offset and calculate stop flag
		$offset = Yii::app()->request->getParam('offset', 0);
		$totalCount = count($res);

		//define chunk size, depending un users count (smaller chunks will make big operations too slow, and viceversa)
		if ($totalCount < 100) {
			$chunkSize = 10;
		} elseif ($totalCount < 500) {
			$chunkSize = 20;
		} elseif ($totalCount < 1000) {
			$chunkSize = 30;
		} elseif ($totalCount < 3000) {
			$chunkSize = 40;
		} else {
			$chunkSize = 50;
		}


		$newOffset = $offset + $chunkSize;
		$stop = ($newOffset < $totalCount) ? false : true;

		// Preparing result for importer
		$result = array (
			'usersUnassigned' => 0,
			'usersFailed' => 0,
		);

		$counter = 0;
		$toBeUnselected = array();
		for ($index = $offset; ($index < $newOffset && $index < $totalCount); $index++) { //NOTE: $newOffset may be greater than $totalCount, but we wouldn't count over $totalCount

			//security check
			$counter++;
			if ($counter > $chunkSize) break; //NOTE: this shouldn't happen

			//read user ID, if we are over user list limit, then invalidate the value and stop this iteration
			$user = (isset($res[$index]) ? $res[$index] : -1);
			if ($user < 0) continue;

			try {

				//validate input user
				if (!$user) { throw new CException('Invalid user (index: '.$index.')'); }

				$curriculaUserModel = LearningCoursepathUser::model()->findByAttributes(array(
					'id_path' => $idPath,
					'idUser' => $user,
				));

				if($curriculaUserModel){
					$curriculaUserModel->delete(); //this the effective unassignment from the learning plan

					Yii::app()->event->raise(EventManager::EVENT_USER_UNENROLLED_FROM_LEARNING_PLAN, new DEvent($this, array(
						'learningPlan' => $curriculaUserModel->path,
						'user' => $curriculaUserModel->user,
					)));
				}

				$result['usersUnassigned']++;
				$toBeUnselected[] = (int)$user;

			} catch(Exception $e) {

				Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
				$result['usersFailed']++;
			}
		}
			//render dialog
			if ($index > $totalCount) { $index = $totalCount; } //normalize value
			$completedPercent = ($totalCount == 0 ? 0 : (number_format($index/$totalCount * 100, 0)));
			$html = $this->renderPartial('unassign_progressive_dialog', array(
				'offset' => $offset,
				'assigned' => $result['usersUnassigned'],
				'failed' => $result['usersFailed'],
				'processedUsers' => $index,
				'totalUsers' => $totalCount,
				'totalBranches' => count($res['groups']),
				'completedPercent' => $completedPercent,
				'start' => ($offset == 0),
				'stop' => $stop,
				'toBeUnselected' => $toBeUnselected,
				'cacheKey' => $cacheKey,
				'afterUpdateCallback'=>($stop ? '$.fn.updateCheckboxesAfterChangeStatus();   $("#curricula-user-massive-action").val("");' : null)
			), true, true);


			//we are finished here, we do not need cache file anymore
			if ($stop) {
				$this->_unsetCachedUsers($cacheKey);
			}

			// Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
			// These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stopping when $stop == true)
			$data = array(
				'offset'	=> $newOffset,
				'stop'		=> $stop,
				//specific parameters for this function
				'_key_'		=> $cacheKey
			);
			$response = new AjaxResult();
			$response->setStatus(true);
			$response->setHtml($html)->setData($data)->toJSON();
			Yii::app()->end();









	}


	public function actionAssignPrerequisites($id, $id_item) {
		try {
			$model = new LearningCoursepathCourses();
			$model->id_path = $id;
			$model->id_item = $id_item;
			if (isset($_REQUEST['LearningCourse'])) {
				$model->idItem->attributes = $_REQUEST['LearningCourse'];
			} else {
				$model->idItem = null;
			}

			/** @var LearningCoursepathCourses $courseModel */
			$courseModel = LearningCoursepathCourses::model()->findByAttributes(array(
				'id_path' => $id,
				'id_item' => $id_item,
			));
			if ($courseModel === null) {
				$this->sendJSON(array());
			} else {
				if (!isset($_REQUEST['contentType']) || ($_REQUEST['contentType'] != 'html')) {
					Yii::app()->session['selectedItems'] = $courseModel->prerequisitesToArray();
				}
			}
			if (isset($_POST['isDelay']) && $_POST['isDelay'] == 1) {
				$courseModel->delay_time = intval($_POST['delayTime']);
				$courseModel->delay_time_unit = $_POST['delayTimeUnit'];
			} else {
				if (!isset($_POST['isDelay']) && isset($_POST['delayTime']) && isset($_POST['delayTimeUnit'])) {
					unset($_POST['delayTime']);
					unset($_POST['delayTimeUnit']);
					$courseModel->delay_time = null;
					$courseModel->delay_time_unit = null;
				}
			}

			if (isset($_REQUEST['curricula-assign-prerequisites-grid-selected-items'])) {
				$courseModel->prerequisites = $_REQUEST['curricula-assign-prerequisites-grid-selected-items'];

				$pAttrs = Yii::app()->request->getParam('LearningCoursepathCourses');
				if (intval($pAttrs['min_courses']) > 0) {
					$minCourses = intval($pAttrs['min_courses']);
					$selectedItems = explode(',',$courseModel->prerequisites);
					if ($minCourses > count($selectedItems)) {
						$model->addError('min_courses', 'Invalid value');
						throw new CException();
					}
					$courseModel->mode = $minCourses;
				} else {
					$courseModel->mode = 'all';
				}

				if ($courseModel->save()) {
					$this->sendJSON(array('status' => 'saved'));
				}
			}
		}
		catch (CException $ex) { }

		$processOutput = false;
		if (isset($_REQUEST['popupFirstShow']) && $_REQUEST['popupFirstShow'] === '1') {
			$processOutput = true;
		}

		$result = array();

		$result['html'] = $this->renderPartial('assignPrerequisites', array(
			'model' => $model,
			'courseModel' => $courseModel,
		), true, $processOutput);

		if (isset($_REQUEST['contentType']) && ($_REQUEST['contentType'] == 'html')) {
			echo $result['html'];
		} else {
			$this->sendJSON($result);
		}
	}


	public function actionAssignedCourseAutocomplete() {
		if (isset($_REQUEST['data'])) {
			$query = $_REQUEST['data']['query'];
			$criteria = new CDbCriteria();
			$criteria->with = array(
				'idItem' => array(),
			);
			$criteria->condition = 'idItem.name LIKE :match AND t.id_path=' . $_REQUEST['data']['id'];
			$criteria->params = array(
				':match' => '%' . $query . '%',
			);
			$criteria->together = true;

			$curriculaCourses = LearningCoursepathCourses::model()->findAll($criteria);
			$coursesList = array_values(CHtml::listData($curriculaCourses, 'id_item', 'idItem.name'));
			$this->sendJSON(array('options' => $coursesList));
		}
	}


	public function actionAssignedUserAutocomplete() {
		if (isset($_REQUEST['data'])) {
			$query = $_REQUEST['data']['query'];
			$criteria = new CDbCriteria();
			$criteria->with = array(
				'coreUser' => array(),
			);
			$criteria->compare('id_path', $_REQUEST['data']['id']);
			$criteria->addCondition('CONCAT(coreUser.firstname, " ", coreUser.lastname) LIKE :search OR coreUser.email LIKE :search OR coreUser.userid LIKE :search');
			$criteria->params[':search'] = '%' . $query . '%';

			$criteria->together = true;

			$curriculaUsers = LearningCoursepathUser::model()->findAll($criteria);
			$usersList = array_values(CHtml::listData($curriculaUsers, 'idUser', 'coreUser.clearUserId'));
			$this->sendJSON(array('options' => $usersList));
		}
	}


	public function actionAssignCourseAutocomplete() {
		if (isset($_REQUEST['data'])) {
			$query = $_REQUEST['data']['query'];

			$courseModel = LearningCourse::model();
			$courseModel->name = $query;

			if (Yii::app()->user->isAdmin) {
				$courseModel->powerUserManager = CoreUser::model()->findByPk(Yii::app()->user->id);
			}

			$dataProvider = $courseModel->dataProviderCurricula($_REQUEST['data']['id']);
			$dataProvider->pagination = false;

			$courses = $dataProvider->data;
			$coursesList = array_values(CHtml::listData($courses, 'idCourse', 'name'));
			$this->sendJSON(array('options' => $coursesList));
		}
	}


	public function actionAssignPrerequisitesAutocomplete() {
		if (isset($_REQUEST['data'])) {
			$query = $_REQUEST['data']['query'];

			$model = new LearningCoursepathCourses();
			$model->id_path = $_REQUEST['data']['id'];
			$model->id_item = $_REQUEST['data']['id_item'];
			$model->idItem->name = $query;

			$dataProvider = $model->dataProvider(true);
			$dataProvider->pagination = false;

			$courses = $dataProvider->data;
			$coursesList = array_values(CHtml::listData($courses, 'id_item', 'idItem.name'));
			$this->sendJSON(array('options' => $coursesList));
		}
	}



	public function actionCurriculaAutocomplete() {
		$query = addcslashes($_REQUEST['data']['query'], '%_');

		$criteria = new CDbCriteria(array(
			'condition' => 'path_name LIKE :match',
			'params' => array(
				':match' => "%$query%"
			),
		));

		if (Yii::app()->user->getIsAdmin()) {
			$criteria->with['powerUserManagers'] = array(
				'condition' => 'powerUserManagers.idst=:puser_id',
				'params' => array(':puser_id' => Yii::app()->user->id),
				'together' => true,
			);
		} elseif (isset($_REQUEST['data']['powerusermanager'])) {
			$criteria->with['powerUserManagers'] = array(
				'condition' => 'powerUserManagers.idst=:puser_id',
				'params' => array(':puser_id' => $_REQUEST['data']['powerusermanager']),
				'together' => true,
			);
		}

		if (isset($_REQUEST['data']['poweruserNot'])) {
			$powerUserModel = CoreUser::model()->findByPk($_REQUEST['data']['poweruserNot']);
			$criteria->with['powerUserManagers'] = array(
				'joinType' => 'LEFT JOIN',
			);
			if (!empty($powerUserModel->manageCourses)) {
                $withArray = CHtml::listData($powerUserModel->manageCoursepaths, 'id_path', 'id_path');
                if (count($withArray) > 0) {
                    $criteria->with['powerUserManagers']['condition'] = 't.id_path NOT IN
                    ('.implode(',', $withArray).')
                    OR powerUserManagers.idst IS NULL';
                }
			}
			$criteria->together = true;
			$criteria->group = 't.id_path';
		}

		$coursepaths = LearningCoursepath::model()->findAll($criteria);
		if(isset($_REQUEST['data']['formattedCoursepathName']) && $_REQUEST['data']['formattedCoursepathName']) {
			$coursepathsList = array_values(CHtml::listData($coursepaths, 'id_path', 'coursepathNameFormatted'));
		} else {
			$coursepathsList = array_values(CHtml::listData($coursepaths, 'id_path', 'path_name'));
		}
		$this->sendJSON(array('options' => $coursepathsList));
	}



	/**
	 *
	 */
    public function actionRenderThumbnailsCarousel() {
    	/* @var $curriculaModel LearningCoursepath */

        $id = Yii::app()->getRequest()->getParam('loId', false);

        $curriculaModel = LearningCoursepath::model()->findByPk($id);

        if (!$curriculaModel) { $curriculaModel = new LearningCoursepath('curricula_management'); }

        if ($curriculaModel->certificate === null) {
        	$curriculaModel->certificate = new LearningCertificateCoursepath();
		}

		list($count, $defaultImages, $userImages, $selected) = LearningCoursepath::model()->prepareThumbnails($curriculaModel);
		$uniqueId = substr(md5(uniqid(rand(), true)), 0, 10);

		// Cropper size
		$cropSize = array(
			'width' => 300,
			'height' => 300,
		);
		Yii::app()->event->raise('OverrideCourseImageCropSize', new DEvent($this, array(
			'cropSize' => &$cropSize
		)));

		$result['content'] = $this->renderPartial('_sliders', array(
			'model' => $curriculaModel,
			'defaultImages' => $defaultImages,
			'userImages' => $userImages,
			'count' => $count,
			'selectedTab' => 'default',
			'uniqueId' => $uniqueId,
			'cropSize'	=> $cropSize,
		), true);

			$this->sendJSON($result);

		Yii::app()->end();
    }


    /**
     * Ajax action that handles custom image uploaded deletion icon click
     */
    public function actionDeleteImage(){

    	$imageId = Yii::app()->getRequest()->getParam('image_id');

    	$success = false;

    	if ((int) $imageId > 0) {
    		$asset = CoreAsset::model()->findByPk($imageId);
    		if ($asset) {
    			$success = $asset->delete();
    		}

    		$ajaxResult = new AjaxResult();
    		$data = array();
    		$ajaxResult->setStatus($success)->setData($data)->toJSON();
    		Yii::app()->end();
    	}
    	else{
    		throw new CException('Invalid image ID');
    	}
    }

	public function actionAxAssignCatchup($idPath, $idCourse)
	{
		$curriculaModel = LearningCoursepath::model()->findByPk($idPath);
		$courseModel = new LearningCourse();
		$courseModel->idCourse = $idCourse;

		//search input handler
		if (isset($_GET['LearningCourse']))
			$courseModel->attributes = $_GET['LearningCourse'];

		$model = LearningCourseCatchUp::model()->findByAttributes(array(
			'id_path' => $idPath,
			'id_course' => $idCourse
		));

		if ($model === null) {
			$model = new LearningCourseCatchUp();
			$model->id_path = $idPath;
			$model->id_course = $idCourse;
		}

		if (isset($_POST['LearningCourseCatchUp'])) {
			$model->attributes = $_POST['LearningCourseCatchUp'];
			if ($model->validate()) {
				$model->save(false);
				$curriculaModel->subscribeUsersToSingleCourse($model->id_catch_up);
				echo '<a class="auto-close"></a>';
				exit;
			}
		}

		$this->renderPartial('_assignCatchup', array(
			'courseModel' => $courseModel,
			'model' => $model,
			'lpCourses' => $curriculaModel->getCoursesIdList()
		), false, true);
	}

	public function actionAxRemoveCatchup($idPath, $idCourse)
	{
		$model = LearningCourseCatchUp::model()->findByAttributes(array(
			'id_path' => $idPath,
			'id_course' => $idCourse
		));
		if (isset($_POST['confirm_delete']))
		{
			$model->delete();

			echo '<a class="auto-close"></a>';
			exit;
		}
		$this->renderPartial('_removeCatchup', array());
	}
    public function actionCatchup($idPath, $idUser)
    {
        $finishButton = Yii::app()->request->getParam('confirm_button', false);

        if ($finishButton) { // The Confirm button is pressed
            $model = LearningCoursepathUser::model()->findByAttributes(array(
                'idUser'=>$idUser,
                'id_path'=>$idPath,
            ));
            /******- Get the limit from the (catchup) form -******/
            $limit = Yii::app()->request->getParam('limit', false);
            $checkbox = Yii::app()->request->getParam('catchup-checkbox-mail', false);

            if ($model) {
                $model->catchup_user_limit = $limit;
                $model->save(true);
                if ($checkbox) {
                    /********- Event Notification for changed limit -******/
                    $event = new DEvent($this, array(
                        'catchupLimit' => $limit,
                        'lpModel' => $model->path,
                        'userModel' => $model->user
                    ));
                    Yii::app()->event->raise('CatchupLimitChanged', $event);
                }
            }
            echo '<a class="auto-close success"></a>';
            Yii::app()->end();
        }

        $user = CoreUser::model()->findByPk($idUser);
        if (!$user) {
            throw new CHttpException(401,'User not found');
        } /*- Get the name of the assigned userName*/
        $userName = array(
			'fname' => $user->firstname,
            'lname' => $user->lastname);
        $coursepathUser = LearningCoursepathUser::model()->findByAttributes(array(
            'idUser'=>$idUser,
            'id_path'=>$idPath,
        ));
        $coursePath = LearningCoursepath::model()->findByPk($idPath);
        if ($coursePath && $coursePath->catchup_enabled) {
            $catchupLimit = $coursePath->catchup_limit;
            if (strlen($coursePath->path_name) > 0) {
                $coursePathName = $coursePath->path_name;
            }
            if ($coursepathUser && $coursepathUser->catchup_user_limit) {
                $catchupLimit = $coursepathUser->catchup_user_limit;
            }

            /******* Get the data from the data provider and get the catchup Count *******/
            $dataProvider = $coursePath->getCatchupCoursesForUser($idPath, $idUser);
            $catchupCount = $coursePath->countPlayedCatchupCourses($idUser);
        } else {
            throw new CHttpException(401,"Learning Plan not found");
        }
        $coursepathCourses = LearningCoursepathCourses::model()->findAllByAttributes(array('id_path' => $idPath));
        if ($coursepathCourses) {
            $coursesInfo = array();
            foreach($coursepathCourses as $cpc) {
                $courses = $cpc->learningCourse;
                if (count($courses) > 0) {
                    array_push($coursesInfo, array(
                            'id' => $courses->idCourse,
                            'name' => $courses->name)
                    );
                }
            }
            $catchUpArray = array();
            foreach($coursesInfo as $course) {
                $courseCatchUp = LearningCourseCatchUp::model()->loadAssignedCatchup($idPath, $course['id']);
                array_push($catchUpArray, $courseCatchUp->id_catch_up);
            }
            $catchUpNames = array();
            foreach($catchUpArray as $cua) {
                $catchup = LearningCourse::model()->findByPk($cua);
                array_push($catchUpNames, $catchup->name);
            }
        } else {
            throw new CHttpException(401, 'No courses found for LP: '.$idPath);
        }

        $this->renderPartial('catchup', array(
            'dataProvider' => $dataProvider,
            'coursePathName' => $coursePathName,
            'userName' => $userName,
            'catchUpNames' => $catchUpNames,
            'coursesInfo' => $coursesInfo,
            'catchupLimit' => $catchupLimit,
            'catchupCount' => $catchupCount,
        ));
    }
}