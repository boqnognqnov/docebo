<?php

/**
 * Course catalogs events handler and more
 * Class LearningPlanLib
 */
class LearningPlanLib extends CComponent {


	const JOB_TYPE_UNASSIGN_LEARNING_PLAN_USERS = 'learning_plan_unassign_users';
	const JOB_TYPE_ASSIGN_LEARNING_PLAN_USERS = 'learning_plan_assign_users';

	/**
	 * Handle progressive job, but with our own custom job type(s)
	 * @param DEvent $event
	 */
	public function progressiveJobRun(DEvent $event) {

		$data = $event->params['data'];

		switch ($data['type']) {
			case self::JOB_TYPE_ASSIGN_LEARNING_PLAN_USERS:
				$params = array(
					'id_path' => $data['id_path']
				);
				$url = Docebo::createAbsoluteAdminUrl('CurriculaApp/CurriculaManagement/axProgressiveAssignUsers', $params);
				$event->preventDefault();
				$event->return_value['url'] = $url;
				break;
			case self::JOB_TYPE_UNASSIGN_LEARNING_PLAN_USERS:
				$params = array(
					'id_path' => $data['id_path']
				);
				$url = Docebo::createAbsoluteAdminUrl('CurriculaApp/CurriculaManagement/axProgressiveUnassignUsers', $params);
				$event->preventDefault();
				$event->return_value['url'] = $url;
				break;
		}

	}

}