<?php

/**
 * Listener for the curricula app
 *
 * Class LearningPlanEventListener
 */
class LearningPlanEventListener {

	/**
	 * Checks whether a catch-up course is available for the current user in the LP
	 * @param $event DEvent
	 */
	public function checkCatchupAvailability($event) {
		$userSessionModel = $event->params['userSessionModel']; /* @var $userSessionModel LtCourseuserSession */

		$canHaveCatchup = true;
		$event = new DEvent($this, array (
			'showCatchup' => &$canHaveCatchup,
			'idCourse' => $userSessionModel->ltCourseSession->course_id,
			'idUser' => $userSessionModel->id_user
		));
		Yii::app()->event->raise('onUserSessionEvaluationSubmit', $event);
		if(!$canHaveCatchup)
			return;

		// Select all catch-up courses for which the failed course is a "main course"
		$sql = "select * from learning_course_catch_up as lccu
inner JOIN learning_coursepath_user as lcu ON lcu.id_path = lccu.id_path AND lcu.idUser = :idUser
inner JOIN learning_coursepath as cp ON cp.id_path = lccu.id_path
left join learning_courseuser as cu on cu.idCourse = lccu.id_catch_up AND cu.idUser = :idUser
where `id_course` = :idCourse";
		$catchUpCourses = Yii::app()->db->createCommand($sql)->queryAll(true, array(
			':idUser' => $userSessionModel->id_user,
			':idCourse' => $userSessionModel->ltCourseSession->course_id
		));

		// For each catch-up course, we should check the following
		// 1. The user is not enrolled in the catch-up or he has never entered the course and is in "subscribed" status
		// 2. The user has not reached the limit for the learning plan
		foreach($catchUpCourses as $row) {
			if(!$row['date_inscr'] || (($row['status'] == LearningCourseuser::$COURSE_USER_SUBSCRIBED) && !$row['date_first_access'])) {
				// Now we can check the limit
				$pathModel = LearningCoursepath::model()->findByPk($row['id_path']);
				if($pathModel) {
					$playedCatchups = $pathModel->countPlayedCatchupCourses($userSessionModel->id_user);
					if(($row['catchup_limit'] == 0) || ($row['catchup_user_limit'] && ($playedCatchups < $row['catchup_user_limit'])) || ($playedCatchups < $row['catchup_limit'])) {
						// Trigger CatchupCourseAvailable event for other to intercept
						Yii::app()->event->raise('CatchupCourseAvailable', new DEvent($this, array(
							'learningPlan' => $pathModel,
							'user' => $userSessionModel->idUser,
							'id_catchup' => $row['id_catch_up'],
							'id_main_course' =>$row['id_course']
						)));
					}
				}
			}
		}

	}

	/**
	 * Checks if the current course is a catchup course and completes the main course accordingly
	 * @param $event
	 */
	public function checkCatchupCompletions($event){
		// Fetch event params
		$course = $event->params['course']; /* @var $course LearningCourse */
		$user = $event->params['user']; /* @var $user CoreUser */

		// Select all "main courses" for which the current course is a catch up
		// and mark them as completed
		$sql = "select * from learning_course_catch_up as lccu
inner JOIN learning_coursepath_user as lcu ON lcu.id_path = lccu.id_path AND lcu.idUser = :idUser
where `id_catch_up` = :idCatchup";
		$mainCourses = Yii::app()->db->createCommand($sql)->queryAll(true, array(':idUser' => $user->idst, ':idCatchup' => $course->idCourse));

		foreach($mainCourses as $mainCourse) {
			$enrollment = LearningCourseuser::model()->findByAttributes(array(
				'idCourse' => $mainCourse['id_course'],
				'idUser' => $user->idst
			));

			// Force completion of the main course
			if($enrollment) {
				$enrollment->status = LearningCourseuser::$COURSE_USER_END;
				$enrollment->save();
			}
		}
	}

    /**
     * Detect learning plan completion, using course completion event
     * @param $event
     */
    public function detectLearningPlanCompletion($event) {

        // Fetch event params
        $course = $event->params['course']; /* @var $course LearningCourse */
		$user = $event->params['user']; /* @var $user CoreUser */

        // Find all learning plans where this course is included and this user is subscribed
        $criteria = new CDbCriteria();
        $criteria->with['learningCoursepathCourses'] = array('joinType' => 'INNER JOIN');
        $criteria->with['coreUsers'] = array('joinType' => 'INNER JOIN');
        $criteria->condition ='learningCoursepathCourses.id_item = :idCourse AND coreUsers_coreUsers.idUser = :idUser';
        $criteria->params[':idCourse'] = $course->idCourse;
        $criteria->params[':idUser'] = $user->idst;
        $learningPlans = LearningCoursepath::model()->findAll($criteria);
        foreach($learningPlans as $learningPlan) {
            // Check if this course is the one that make the LP completed
            if($this->isLastCourseInLearningPlan($learningPlan->id_path, $user->idst, $course->idCourse)){

                // Try to generate a certificate for this learning plan
                $coursePathUserModel = LearningCoursepathUser::loadSubscription($user->idst, $learningPlan->id_path);
                $coursePathUserModel->generateCertificate();

                // If a certificate was generated for this learning plan, then retrieve its details
                $certificate = LearningCertificateAssignCp::model()->findByAttributes(array(
                    'id_user' => $user->idst,
                    'id_path' => $learningPlan->id_path,
                ));

                // Trigger StudentCompletedLearningPlan event for other to intercept
                Yii::app()->event->raise(EventManager::EVENT_STUDENT_COMPLETED_LEARNING_PLAN, new DEvent($this, array(
                    'learningPlan' => $learningPlan,
                    'user' => $user,
                    'certificate' => $certificate
                )));
            }
        }

    }

    /**
     * Returns true if the learning plan passed is about to be completed with the next completed course
     * @param $idPath
     * @param $idUser
     * @param $idCourse
     * @return bool
     */
    private function isLastCourseInLearningPlan($idPath, $idUser, $idCourse){

			// Calculate number of completed courses for this user
			$criteria = new CDbCriteria();
			$criteria->with['learningCoursepathCourses'] = array('joinType' => 'INNER JOIN');
			$criteria->condition = '((t.status = :status) OR (t.idCourse = :idCourse)) AND t.idUser = :idUser AND learningCoursepathCourses.id_path = :id_path';
			$criteria->params[':status'] = LearningCourseuser::$COURSE_USER_END;
			$criteria->params[':idCourse'] = $idCourse;
			$criteria->params[':id_path'] = $idPath;
			$criteria->params[':idUser'] = $idUser;
			$completed = LearningCourseuser::model()->count($criteria);

			// Calculate the total number of courses in the learning plan
			$criteria = new CDbCriteria();
			$criteria->condition = 't.id_path = :id_path';
			$criteria->params[':id_path'] = $idPath;
			$total = LearningCoursepathCourses::model()->count($criteria);

			// If the difference is 1 -> this course will complete the learning plan (after being saved to LearningCourseUser)
			return ($total == $completed);
    }

} 