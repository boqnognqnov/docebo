/**
 * Created by vladimir on 13.5.2015 г..
 */
$(document).ready(function() {
    $(document).on("dialog2.content-update", "#catchup-dialog", function() {
        /*- Add the checkbox for sending notification -*/
        var confirmButton = $("a.confirm-btn");
        if ($("span.checkbox-catchup-container").length) {
            var checkboxContainer = $("span.checkbox-catchup-container");
            $(".modal-footer").prepend(checkboxContainer);
            checkboxContainer.hide();
        }
        if ($('#catchup-limit-value').text() > 0) {
            var countValue = $('#catchup-count-value').text();
            var limitValue = $('#catchup-limit-value').text();
            var $limitContain = $("#limit-contain");
            confirmButton.hide();
            if (countValue > 0 && limitValue > 0 && countValue == limitValue) {
                $("<i class='fa fa-exclamation-circle'></i>").insertBefore("span#catchup-count-value");
                $("<a href='#' class='node-edit'></a>").insertAfter("#catchup-limit-value");
                $("span.catchup-values").addClass("orange");

                if ($("a.node-edit").length) {
                    $(document).delegate("a.node-edit", "click", function() {
                        var that = $(this);
                        var $limit = $("#catchup-limit-value");
                        that.hide();
                        $limit
                            .replaceWith("<input id='" + $limit.attr('id') + "' value='" + $limit.text() + "'/>")
                            .select();
                        var $input = $(this).prev("input");
                        var oldLimit = $limit.text();

                        $input.on('keyup', function() {
                            var numberMatch = new RegExp("^[0-9]{1,2}$");
                            if (numberMatch.test($(this).val())) {
                                return $(this).val();
                            } else {
                                $(this).val("");
                            }
                        });

                        $input.on('blur', function() {

                            var postCheckbox = $("#catchup-checkbox-notification");
                            var newLimit = $input.val();
                            if (newLimit > countValue) {
                                $input.replaceWith("<span id='" + $input.attr('id') + "'>" + newLimit + "");
                                checkboxContainer.show();
                                confirmButton.removeClass("hidden").show();
                                $limitContain.val(newLimit);
                            } else {
                                $input.replaceWith("<span id='" + $input.attr('id') + "'>" + countValue + "");
                                checkboxContainer.hide();
                                confirmButton.addClass("hidden").hide();
                                $limitContain.val(countValue);
                            }
                            that.show();
                            /***********- When checkbox is checked write in hidden field -***********/
                            if ($('input#catchup-checkbox').length) {
                                var $catchupCheckbox = $('input#catchup-checkbox');
                                $catchupCheckbox.on('click', function() {
                                    if (($catchupCheckbox).is(':checked')) {
                                        postCheckbox.val('1');
                                    } else {
                                        postCheckbox.val('0');
                                    }
                                });
                            } /************- End of checkbox value insertion -***********/
                        });
                    });
                }
            }
        }
        /*************- When the dialog2 is closed refresh the grid user list -***********/
        $('.confirm-btn').on("click", function() {
            if ($('#curricula-user-list').length) {
                $.fn.yiiListView.update('curricula-user-list');
            } else {
                location.reload();
            }
        });
    });
});