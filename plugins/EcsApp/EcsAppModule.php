<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2014 Docebo
 */

/**
 * Class EcsppModule
 */
class EcsAppModule extends CWebModule {


	public function init() {
		// this method is called when the module is being created
		// you may place code here to customize the module or the application
	}

}
