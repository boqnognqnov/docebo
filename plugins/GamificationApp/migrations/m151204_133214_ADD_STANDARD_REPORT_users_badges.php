<?php
/**
 * Yii DB Migration template.
 * 
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 * 
 */
class m151204_133214_ADD_STANDARD_REPORT_users_badges extends DoceboDbMigration {

	public function safeUp()
	{
		$sql = 'INSERT INTO `learning_report_filter` (`id_filter`, `report_type_id`, `author`, `creation_date`, `filter_name`, `filter_data`, `is_public`, `views`, `is_standard`, `id_job`) VALUES (NULL, \'29\', \'0\', \'0000-00-00 00:00:00.000000\', \'Users - Badges\', \'{"filters":{"date_from":null,"date_to":null,"custom_fields":{"32":"","94":""}},"fields":{"user":{"userid":"1","firstname":"1","lastname":"1","email":"1"},"badges":{"icon":"1","name":"1","description":"1","score":"1"},"assignment":{"issued_on":"1","event_key":"1"}},"users":[],"plans":[],"groups":[],"courses":[],"orgchartnodes":[],"certifications":[],"badges":[],"reportId":"","author":"0","id_filter":"","report_type_id":"29","creation_date":"12\/4\/2015 11:36:12 am","filter_name":"ub2","is_public":"0","is_standard":"0","id_job":null,"order":{"orderBy":"user.userid","type":"ASC"}}\',\'0\', \'0\', \'1\', NULL);';
		$this->execute($sql);
		return true;
	}

	public function safeDown()
	{
		$sql = "DELETE FROM `learning_report_filter` WHERE `learning_report_filter`.`report_type_id` = 29 AND `learning_report_filter`.`author`=0;";
		$this->execute($sql);
		return true;
	}
	
	
}
