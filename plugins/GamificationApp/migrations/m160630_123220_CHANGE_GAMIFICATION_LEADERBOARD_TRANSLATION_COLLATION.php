<?php
/**
 * Yii DB Migration template.
 *
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 *
 */
class m160630_123220_CHANGE_GAMIFICATION_LEADERBOARD_TRANSLATION_COLLATION extends DoceboDbMigration {

    public function safeUp()
    {
        $sql = "ALTER TABLE gamification_leaderboard_translation CONVERT TO CHARACTER SET utf8";
        $this->execute($sql);
        return true;
    }

    public function safeDown()
    {
        $sql = "ALTER TABLE gamification_leaderboard_translation CONVERT TO CHARACTER SET latin1";
        $this->execute($sql);
        return true;
    }
}
