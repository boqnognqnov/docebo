<?php
/**
 * Yii DB Migration template.
 * 
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 * 
 */
class m151209_135856_ADD_NEW_CUSTOM_REPORT_user_contest extends DoceboDbMigration {

	public function safeUp()
	{

		$this->insert('learning_report_type', array(
				'id'=>LearningReportType::USERS_CONTESTS,
				'title'=>'Users - Contests',
		));
		// PUT YOUR MIGRATION-UP CODE HERE
		// DO NOT USE try/catch or DB transactions!
		
		// Internally, this method call is wrapped in a try/catch block and DB transaction.
		// You can throw exceptions here to be catched internally.
		// If you wish to cancel the migration, return false
		return true;
	}

	public function safeDown()
	{
		$this->delete('learning_report_type', 'id = :id', array(
			':id' => LearningReportType::USERS_CONTESTS
		));
		// PUT YOUR MIGRATION-DOWN CODE HERE
		// DO NOT USE try/catch or DB transactions!
		
		// Internally, this method call is wrapped in a try/catch block and DB transaction
		// You can throw exceptions here to be catched internally.
		// If you wish to cancel the migration, return false
		return true;
	}
	
	
}
