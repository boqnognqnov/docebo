<?php
/**
 * Yii DB Migration template.
 * 
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 * 
 */
class m160215_125317_GAMIFICATION_CONVERT_BADGES_POINTS_TO_COINS extends DoceboDbMigration {

	public function safeUp()
	{

		$command = Yii::app()->db->createCommand();

		/**
		 * @var $command CDbCommand
		 */
		$command->select('id_user, SUM(score) as sc');
		$command->from('gamification_assigned_badges');
		$command->group('id_user');

		$usersScores = $command->queryAll();

		if(!empty($usersScores)){
			foreach($usersScores as $scores){
				$this->insert('gamification_user_wallet', array(
					'id_user' => $scores['id_user'],
					'coins' => $scores['sc']
				));
			}
		}

		// PUT YOUR MIGRATION-UP CODE HERE
		// DO NOT USE try/catch or DB transactions!
		
		// Internally, this method call is wrapped in a try/catch block and DB transaction.
		// You can throw exceptions here to be catched internally.
		// If you wish to cancel the migration, return false
		return true;
	}

	public function safeDown()
	{

		$this->truncateTable('gamification_user_wallet');
		// PUT YOUR MIGRATION-DOWN CODE HERE
		// DO NOT USE try/catch or DB transactions!
		
		// Internally, this method call is wrapped in a try/catch block and DB transaction
		// You can throw exceptions here to be catched internally.
		// If you wish to cancel the migration, return false
		return true;
	}
	
	
}
