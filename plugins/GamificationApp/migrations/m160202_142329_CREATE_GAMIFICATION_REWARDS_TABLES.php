<?php
/**
 * Yii DB Migration template.
 * 
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 * 
 */
class m160202_142329_CREATE_GAMIFICATION_REWARDS_TABLES extends DoceboDbMigration {

	public function safeUp()
	{

		$sql = <<<SQL

CREATE TABLE `gamification_reward` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`coins` INT(11) NOT NULL DEFAULT '0',
	`availability` INT(11) NOT NULL DEFAULT '0',
	`picture` VARCHAR(2000) NULL DEFAULT NULL,
	`association` VARCHAR(50) NULL DEFAULT 'all',
	`date_added` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
;

CREATE TABLE `gamification_reward_translation` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`id_reward` INT(11) NOT NULL,
	`lang_code` VARCHAR(50) NOT NULL,
	`name` VARCHAR(255) NULL DEFAULT NULL,
	`description` TEXT NULL,
	PRIMARY KEY (`id`),
	INDEX `FK__gamification_reward` (`id_reward`),
	CONSTRAINT `FK__gamification_reward` FOREIGN KEY (`id_reward`) REFERENCES `gamification_reward` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
;

CREATE TABLE `gamification_rewards_set` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NULL DEFAULT NULL,
	`description` TEXT NULL DEFAULT NULL,
	`filter` VARCHAR(1000) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
;

CREATE TABLE `gamification_associated_sets` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`id_reward` INT(11) NOT NULL,
	`id_set` INT(11) NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `FK1__gamification_reward` (`id_reward`),
	INDEX `FK2__gamification_rewards_set` (`id_set`),
	CONSTRAINT `FK1__gamification_reward` FOREIGN KEY (`id_reward`) REFERENCES `gamification_reward` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `FK2__gamification_rewards_set` FOREIGN KEY (`id_set`) REFERENCES `gamification_rewards_set` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
;

CREATE TABLE `gamification_user_rewards` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`id_user` INT(11) NOT NULL,
	`id_reward` INT(11) NOT NULL,
	`status` TINYINT(4) NOT NULL DEFAULT '0',
	`data` TEXT NULL,
	`coins` INT(11) NOT NULL DEFAULT '0',
	`date_created` TIMESTAMP NULL DEFAULT NULL,
	`date_modified` TIMESTAMP NULL DEFAULT NULL,
	`modified_by` INT(11) NULL,
	PRIMARY KEY (`id`)
-- 	INDEX `FK__core_user` (`id_user`),
-- 	INDEX `FK_gamification_user_rewards_gamification_reward` (`id_reward`),
-- 	CONSTRAINT `FK__core_user` FOREIGN KEY (`id_user`) REFERENCES `core_user` (`idst`) ON UPDATE NO ACTION ON DELETE NO ACTION,
-- 	CONSTRAINT `FK_gamification_user_rewards_gamification_reward` FOREIGN KEY (`id_reward`) REFERENCES `gamification_reward` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='utf8_general_ci'
;

CREATE TABLE `gamification_user_wallet` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`id_user` INT(11) NOT NULL,
	`coins` INT(11) NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	INDEX `FK1__core_user` (`id_user`),
	CONSTRAINT `FK1__core_user` FOREIGN KEY (`id_user`) REFERENCES `core_user` (`idst`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
;


SQL;
		$this->execute($sql);

		return true;
	}

	public function safeDown()
	{
		try{
			$this->dropForeignKey('FK__gamification_reward','gamification_reward_translation');
			$this->dropForeignKey('FK1__gamification_reward','gamification_associated_sets');
			$this->dropForeignKey('FK2__gamification_rewards_set','gamification_associated_sets');
//			$this->dropForeignKey('FK__core_user','gamification_user_rewards');
//			$this->dropForeignKey('FK_gamification_user_rewards_gamification_reward','gamification_user_rewards');
			$this->dropForeignKey('FK1__core_user','gamification_user_wallet');
		} catch (CException $e) {
			echo "\nMIGRATION EXCEPTION:\n";
			echo $e->getMessage();
			echo "\n";
		}

		$this->dropTable('gamification_reward');
		$this->dropTable('gamification_reward_translation');
		$this->dropTable('gamification_rewards_set');
		$this->dropTable('gamification_associated_sets');
		$this->dropTable('gamification_user_rewards');
		$this->dropTable('gamification_user_wallet');

		return true;
	}
	
	
}
