<?php
/**
 * Yii DB Migration template.
 * 
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 * 
 */
class m151203_110518_CREATE_TABLES_gamification_contest_ADD_FOREIGN_KEYS extends DoceboDbMigration {

	public function safeUp()
	{
		// PUT YOUR MIGRATION-UP CODE HERE
		// DO NOT USE try/catch or DB transactions!
		
		// Internally, this method call is wrapped in a try/catch block and DB transaction.
		// You can throw exceptions here to be catched internally.
		// If you wish to cancel the migration, return false
		$sql = "CREATE TABLE `gamification_contest` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `from_date` TIMESTAMP NULL DEFAULT NULL , `to_date` TIMESTAMP NULL DEFAULT NULL , `goal` VARCHAR(255) NULL DEFAULT NULL , `goal_settings` TEXT NULL DEFAULT NULL , `filter` TEXT NULL DEFAULT NULL , PRIMARY KEY (`id`) ) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

				CREATE TABLE `gamification_contest_translation` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `id_contest` INT(11) NOT NULL , `lang_code` VARCHAR(50) NULL DEFAULT NULL , `name` VARCHAR(255) NULL DEFAULT NULL , `description` TEXT NULL DEFAULT NULL , PRIMARY KEY (`id`) , INDEX `fk_gamification_contest_translation` (`id_contest`) ) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

				ALTER TABLE `gamification_contest_translation` ADD CONSTRAINT `fk_gamification_contest` FOREIGN KEY (`id_contest`) REFERENCES `gamification_contest`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

				CREATE TABLE `gamification_contest_reward` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `id_contest` INT(11) NOT NULL , `rank` INT NULL DEFAULT NULL , `type` VARCHAR(255) NULL DEFAULT NULL , `id_item` INT NULL DEFAULT NULL , PRIMARY KEY (`id`) , INDEX `fk_gamification_contest_reward` (`id_contest`) ) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

				ALTER TABLE `gamification_contest_reward` ADD CONSTRAINT `fk_gamification_contest_reward` FOREIGN KEY (`id_contest`) REFERENCES `gamification_contest`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

				CREATE TABLE `gamification_contest_chart` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `id_contest` INT(11) NOT NULL , `rank` INT NULL DEFAULT NULL , `id_user` INT(11) NOT NULL , `full_name` VARCHAR(255) NOT NULL, `ranking_result` INT(3) NOT NULL, `reward_data` TEXT NULL DEFAULT NULL , PRIMARY KEY (`id`) , INDEX `fk_gamification_contest_chart` (`id_contest`) ) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

				ALTER TABLE `gamification_contest_chart` ADD CONSTRAINT `fk_gamification_contest_chart` FOREIGN KEY (`id_contest`) REFERENCES `gamification_contest`(`id`);";
		$this->execute($sql);
		return true;
	}

	public function safeDown()
	{
		// PUT YOUR MIGRATION-DOWN CODE HERE
		// DO NOT USE try/catch or DB transactions!
		
		// Internally, this method call is wrapped in a try/catch block and DB transaction
		// You can throw exceptions here to be catched internally.
		// If you wish to cancel the migration, return false
		$sql = "DROP TABLE gamification_contest_chart;
				DROP TABLE gamification_contest_translation;
				DROP TABLE gamification_contest_reward;
				DROP TABLE gamification_contest;";
		$this->execute($sql);
		return true;
	}
	
	
}
