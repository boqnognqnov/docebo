<?php
/**
 * Yii DB Migration template.
 * 
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 * 
 */
class m160121_120649_ALTER_TABLE_gamification_contest_ADD_NEW_COLUMN_timezone extends DoceboDbMigration {

	public function safeUp()
	{
		$this->addColumn('gamification_contest', 'timezone', 'varchar(255) NULL');
		return true;
	}

	public function safeDown()
	{
		$this->dropColumn('gamification_contest', 'timezone');
		return true;
	}
	
	
}
