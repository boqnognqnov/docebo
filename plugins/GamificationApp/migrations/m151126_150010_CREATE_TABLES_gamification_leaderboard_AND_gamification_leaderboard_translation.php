<?php
/**
 * Yii DB Migration template.
 * 
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 * 
 */
class m151126_150010_CREATE_TABLES_gamification_leaderboard_AND_gamification_leaderboard_translation extends DoceboDbMigration {

	public function safeUp()
	{
		// PUT YOUR MIGRATION-UP CODE HERE
		// DO NOT USE try/catch or DB transactions!
		
		// Internally, this method call is wrapped in a try/catch block and DB transaction.
		// You can throw exceptions here to be catched internally.
		// If you wish to cancel the migration, return false


		$sql = <<<SQL
CREATE TABLE IF NOT EXISTS `gamification_leaderboard` (
`id` INT NOT NULL AUTO_INCREMENT,
`filter` TEXT NOT NULL,
`is_active` TINYINT(1) NULL DEFAULT 0,
`default` TINYINT(1) NULL DEFAULT 0,
PRIMARY KEY (`id`));
SQL;

		$this->execute($sql);

		$sql = <<<SQL
CREATE TABLE IF NOT EXISTS `gamification_leaderboard_translation` (
				`id` INT NOT NULL AUTO_INCREMENT,
				`id_board` INT(11) NOT NULL,
				`lang_code` VARCHAR(45) NOT NULL,
				`name` VARCHAR(256) NOT NULL,
				PRIMARY KEY (`id`),
				INDEX `leaderboard_idx` (`id_board` ASC),
				CONSTRAINT `leaderboard`
				FOREIGN KEY (`id_board`)
				REFERENCES `gamification_leaderboard` (`id`)
				ON DELETE CASCADE
				ON UPDATE CASCADE
			);
SQL;

		$this->execute($sql);

		$command = Yii::app()->db->createCommand();
		$command->from('gamification_leaderboard');
		$row = $command->queryAll();

		if(empty($row)){
			$this->insert('gamification_leaderboard', array(
				'id' => 1,
				'filter' => 'all',
				'is_active' => 1,
				'default' => 1
			));


			$command = Yii::app()->db->createCommand();
			$command->select('lang_code');
			$command->where('lang_active = 1');
			$command->from('core_lang_language');
			$langs = $command->queryAll();

			foreach($langs as $lang){
				$this->insert('gamification_leaderboard_translation', array(
					'id_board' => 1,
					'lang_code' => $lang['lang_code'],
					'name' => 'Default Leaderboard'
				));
			}
		}

		return true;
	}

	public function safeDown()
	{

		$this->dropTable('gamification_leaderboard_translation');
		$this->dropTable('gamification_leaderboard');

		// PUT YOUR MIGRATION-DOWN CODE HERE
		// DO NOT USE try/catch or DB transactions!
		
		// Internally, this method call is wrapped in a try/catch block and DB transaction
		// You can throw exceptions here to be catched internally.
		// If you wish to cancel the migration, return false
		return true;
	}
	
	
}
