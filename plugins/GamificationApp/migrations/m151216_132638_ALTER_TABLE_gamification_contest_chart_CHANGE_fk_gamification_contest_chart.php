<?php
/**
 * Yii DB Migration template.
 * 
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 * 
 */
class m151216_132638_ALTER_TABLE_gamification_contest_chart_CHANGE_fk_gamification_contest_chart extends DoceboDbMigration {

	public function safeUp()
	{
		$sql = "ALTER TABLE `gamification_contest_chart` DROP FOREIGN KEY `fk_gamification_contest_chart`; ALTER TABLE `gamification_contest_chart` ADD CONSTRAINT `fk_gamification_contest_chart` FOREIGN KEY (`id_contest`) REFERENCES `gamification_contest`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;";
		$this->execute($sql);
		return true;
	}

	public function safeDown()
	{
		$sql = "ALTER TABLE `gamification_contest_chart` DROP FOREIGN KEY `fk_gamification_contest_chart`; ALTER TABLE `gamification_contest_chart` ADD CONSTRAINT `fk_gamification_contest_chart` FOREIGN KEY (`id_contest`) REFERENCES `gamification_contest`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;";
		$this->execute($sql);
		return true;
	}
	
	
}
