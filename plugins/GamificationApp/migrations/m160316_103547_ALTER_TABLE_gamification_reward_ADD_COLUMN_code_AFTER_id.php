<?php
/**
 * Yii DB Migration template.
 * 
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 * 
 */
class m160316_103547_ALTER_TABLE_gamification_reward_ADD_COLUMN_code_AFTER_id extends DoceboDbMigration {

	public function safeUp()
	{
		$this->addColumn('gamification_reward', 'code', 'varchar(255) NULL');
		return true;
	}

	public function safeDown()
	{
		$this->dropColumn('gamification_reward', 'code');
		return true;
	}
	
	
}
