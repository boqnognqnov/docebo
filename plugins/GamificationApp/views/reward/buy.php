<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 18.2.2016 г.
 * Time: 14:23
 *
 * @var $this RewardController
 * @var $rewardModel GamificationReward
 * @var $userModel CoreUser
 * @var $userCoins integer
 * @var $hasPendingRequests boolean
 */

$this->breadcrumbs = array(
    Yii::t('gamification', 'Gamification') => Docebo::createLmsUrl('//GamificationApp/GamificationApp/MyBadges', array(
        'tab' => 'rewards'
    )),
    Yii::t('gamification', 'Reward Shop'),
    !empty($rewardModel->translation) ? $rewardModel->translation->name : $rewardModel->defaultTranslation->name
);

$hasAvailability = $rewardModel->availability > 0;

$canRedeem = $userCoins >= $rewardModel->coins;

$disableButton = !$hasAvailability || !$canRedeem || $hasPendingRequests;
?>


<div class="main-actions clearfix">
    <h3 class="title-bold back-button">
        <?= CHtml::link(Yii::t('standard', '_BACK'), Docebo::createAppUrl('lms:site/index', array(
            'opt' => 'fullrewardsshop'
        ))); ?>
    </h3>
</div>
<hr>
<div class="reward-detail-container">
    <div class="row-fluid">
        <div class="span3">
            <div class="user-summary">
                <h3><?= Yii::t('gamification', 'My personal panel') ?></h3>
                <hr>
                <div class="row-fluid">
                    <div class="span3">
                        <div class="user-image">
                            <?= $userModel->getAvatarImage()?>
                        </div>
                    </div>
                    <div class="span8">
                        <div class="row-fluid username">
                            <?= $userModel->getFullName() ?>
                        </div>
                        <div class="row-fluid usercoins">
                            <i class="fa fa fa-database fa-lg yellow"></i>
                            <span><?= Yii::t('gamification', 'My coins') ?>:&nbsp;<strong><?= $userCoins ?></strong></span>
                        </div>
                    </div>
                </div>
                    <?= CHtml::link(Yii::t('gamification', 'My rewards request history'), Docebo::createAbsoluteUrl('//GamificationApp/GamificationApp/MyBadges&tab=rewards'), array(
                        'class' => 'rewards-history-link'
                    )); ?>
                <hr>
            </div>
        </div>
        <div class="span9">
            <div class="reward-details">
                <h3><?=!empty($rewardModel->translation) ? $rewardModel->translation->name : $rewardModel->defaultTranslation->name ?></h3>
                <hr>
                <div class="row-fluid">
                    <div class="span3">
                        <?= CHtml::image($rewardModel->getPicture(), !empty($rewardModel->translation) ? $rewardModel->translation->name : $rewardModel->defaultTranslation->name, array(
                        ))?>
                    </div>
                    <div class="span9">
                        <div class="row-fluid">
                            <div class="span1">
                                <i class="fa fa fa-database fa-2x orange"></i>
                            </div>
                            <div class="span3 coins-needed">
                                <p><?= Yii::t('gamification', 'You need <strong>{coins} coins</strong> to redeem this reward', array(
                                    '{coins}' => $rewardModel->coins
                                )) ?></p>
                            </div>
                            <div class="span7 pull-right">
                                <div class="span3 pull-right text-right" >
                                    <?= CHtml::link(Yii::t('gamification', 'Redeem'), Docebo::createAppUrl('//GamificationApp/Reward/redeem', array(
                                        'id' => $rewardModel->id
                                    )), array(
                                        'class' => 'btn btn-docebo green big redeem ' . ($disableButton ? 'disabled' : 'open-dialog'),
                                        'data-dialog-class' => 'redeem-reward'
                                    )) ?>
                                </div>
                                <div class="span8 pull-right" style="position: relative;top: -4px;">
                                    <?php if(!$canRedeem): ?>
                                        <p><?=Yii::t('gamification', 'You are not allowed redeem this reward because you need {coins} coins', array(
                                                '{coins}' => $rewardModel->coins
                                            ))?></p>
                                    <?php elseif(!$hasAvailability): ?>
                                        <p style="margin-top: 8px;margin-right: 10px;"  class="text-right"><?=Yii::t('gamification', 'This reward is not available')?></p>
                                    <?php elseif($hasPendingRequests): ?>
                                        <p><?=Yii::t('gamification', 'You already have a pending request for this reward')?></p>
                                    <?php endif; ?>
                                </div>

                            </div>
                        </div>
                        <br>
                        <div class="row-fluid">
                            <h5><?= Yii::t('standard', '_DESCRIPTION') ?></h5>
                            <hr>
                            <p><?= !empty($rewardModel->translation) ? $rewardModel->translation->description : $rewardModel->defaultTranslation->description ?></p>
                            <hr>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $(document).on('click', 'a.disabled', function(e){
            e.preventDefault();

            return false;
        });

        $(document).on('dialog2.closed', '.modal.redeem-reward', function(){
			location.reload();
        });
    });
</script>