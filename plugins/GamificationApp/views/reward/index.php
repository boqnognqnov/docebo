<?php
$this->breadcrumbs = array(
    Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
    Yii::t('gamification', 'Rewards')
);
?>

<?= $this->renderPartial('mainActionsMenu/_newReward', array('showHints' => $showHints), true); ?>

<!-- Main Content -->
<div id="reward-content">
    <div class="main-section group-management">
        <h3 class="title-bold"><?php echo Yii::t('gamification', 'Rewards list'); ?></h3>
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'rewards-form',
            'htmlOptions' => array(
                'class' => 'ajax-grid-form',
                'data-grid' => '#rewards-grid'
            )
        ));
        ?>
        <div class="filters-wrapper">
            <div class="filters clearfix">
                <div class="input-wrapper">
                    <?php
                    echo CHtml::textField('search_text', '', array(
                        'id' => 'advanced-search-reward',
                        'class' => 'typeahead',
                        'autocomplete' => 'off',
                        'data-url' => Docebo::createAdminUrl('GamificationApp/Reward/autocomplete'),
                        'placeholder' => Yii::t('standard', '_SEARCH'),
                    ));
                    ?>
                    <span class="search-icon"></span>
                </div>
            </div>
        </div>
    </div>
    <?= CHtml::submitButton('', array('style' => 'display: none;')); ?>
    <?php $this->endWidget(); ?>
</div>

<div class="bottom-section clearfix">
    <div id="grid-wrapper">
        <?php
        $_columns = array(
// TODO: include column named 'code'
            array(
                'header' => Yii::t('gamification', 'Code'),
                'headerHtmlOptions' => array('class' => 'reward-management-table-header'),
                'name' => 'code',
                'type' => 'raw',
                'value' => '$data->getCode();',
                'htmlOptions' => array(
                    'style' => 'word-break:break-all;',
                )
            ),
            array(
                'header' => Yii::t('gamification', 'Picture'),
                'headerHtmlOptions' => array('class' => 'text-center reward-management-table-header'),
                'name' => 'picture',
                'type' => 'raw',
                'value' => '$data->renderPicture();',
                'htmlOptions' => array(
                    'style' => 'word-break:break-all;',
                    'class' => 'text-center',
                )
            ),
            array(
                'header' => Yii::t('gamification', 'Reward name'),
                'headerHtmlOptions' => array('class' => 'reward-management-table-header'),
                'name' => 'name',
                'type' => 'raw',
                'value' => '$data->getName();',
                'htmlOptions' => array(
                    'style' => 'width: 30%;word-break:break-all;',
                    'class' => 'reward-name'
                )
            ),
            array(
                'header' => Yii::t('gamification', 'Coins'),
                'headerHtmlOptions' => array('class' => 'text-center reward-management-table-header'),
                'name' => 'filter',
                'type' => 'raw',
                'value' => '$data->coins;',
                'htmlOptions' => array(
                    'class' => 'text-center'
                )
            ),
            array(
                'header' => Yii::t('gamification', 'Availability'),
                'headerHtmlOptions' => array('class' => 'text-center reward-management-table-header'),
                'name' => 'period',
                'type' => 'html',
                'value' => '$data->availability ? $data->availability : "<span class=\"exclamation\"></span>" . $data->availability;',
                'htmlOptions' => array(
                    'class' => 'text-center'
                )
            ),
            array(
                'header' => Yii::t('gamification', 'Associate reward set'),
                'headerHtmlOptions' => array('class' => 'reward-management-table-header'),
                'htmlOptions' => array(
                    'style' => 'width: 30%;word-break:break-all;',
                ),
                'name' => 'active',
                'type' => 'raw',
                'value' => '$data->renderRewardSets();',
            ),
            array(
                'header' => '',
                'name' => 'actions',
                'type' => 'raw',
                'value' => '$data->renderActions();',
                'htmlOptions' => array(
                    'class' => 'text-center'
                )
            ),
        );

        $gridOptions = array(
            'id' => 'rewards-grid',
            'htmlOptions' => array('class' => 'grid-view clearfix'),
            'dataProvider' => GamificationReward::model()->dataProvider(),
            'template' => '{items}{summary}{pager}',
            'summaryText' => Yii::t('standard', '_TOTAL'),
            'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
            'pager' => array(
                'class' => 'ext.local.pagers.DoceboLinkPager',
            ),
            "afterAjaxUpdate" => "function(id, data) { $(document).controls(); }",
            'ajaxUpdate' => 'all-items',
            'columns' => $_columns
        );

        $this->widget('zii.widgets.grid.CGridView', $gridOptions);
        ?>
    </div>
</div>

<script type="text/javascript">

    $(function () {
        $(document).off('userselector.closed.success', '.modal.users-selector')
            .on('userselector.closed.success', '.modal.users-selector', function(){
                $.fn.yiiGridView.update('rewards-grid', {});
            });
        $(document).on("dialog2.content-update", ".delete-reward", function () {
            var autoClose = $(this).find('.auto-close');
            if (autoClose.length > 0) {
                $.fn.yiiGridView.update('rewards-grid', {});
            }
        });
    });

</script>
<!-- Main Content - END -->