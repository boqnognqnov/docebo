<div class="main-actions clearfix main-action-rewards">
    <h3 class="title-bold"><?php echo Yii::t('gamification', 'Rewards'); ?></h3>
    <ul class="clearfix">
        <li data-bootstro-id="bootstroNewReward" id="create_reward" data-bootstro-placement="bottom" data-bootstro-width="600px" data-original-title="<?= Yii::t('gamification', 'New Reward'); ?>" data-bootstro-title="<?= Yii::t('overlay_hints', 'Start here!') ?>" title="<?= Yii::t('overlay_hints', 'Start here!') ?>" class="bootstro-highlight">
            <div>
                <a href="<?= $this->createUrl('reward/new'); ?>" class="additional-fields" alt="<?= Yii::t('gamification', 'New reward - gamification'); ?>">
                    <i class="fa fa-gift main-rewards-actions-icon"></i>
                    <div class="main-rewards-actions"><?= Yii::t('gamification', 'New Reward'); ?></div>
                </a>
            </div>
        </li>
        <li data-bootstro-id="bootstroRewardsSet" id="rewards_set" data-bootstro-placement="bottom" data-bootstro-width="600px" data-original-title="<?= Yii::t('gamification', 'Rewards Set'); ?>" data-bootstro-title="<?= Yii::t('overlay_hints', 'Start here!') ?>" title="<?= Yii::t('overlay_hints', 'Start here!') ?>" class="bootstro-highlight">
            <div>
                <a href="<?= $this->createUrl('rewardsSet/index'); ?>" class="additional-fields rewards-sets" alt="<?= Yii::t('gamification', 'Rewards Set - gamification'); ?>">
                    <i class="fa fa-list main-rewards-actions-icon"></i>
                    <div class="main-rewards-actions"><?= Yii::t('gamification', 'Rewards Set'); ?></div>
                </a>
            </div>
        </li>
        <li id="manage_rewards_requests" data-bootstro-placement="bottom" data-bootstro-width="600px" data-original-title="<?= Yii::t('gamification', 'Manage Rewards Requests'); ?>" data-bootstro-title="<?= Yii::t('overlay_hints', 'Start here!') ?>" title="<?= Yii::t('overlay_hints', 'Start here!') ?>" class="bootstro-highlight">
            <div>
                <a href="<?= $this->createUrl('reward/requestsManagement'); ?>" class="additional-fields" alt="<?= Yii::t('gamification', 'Manage Rewards Requests - gamification'); ?>">
                    <i class="fa fa-cogs main-rewards-actions-icon"></i>
                    <div class="main-rewards-actions"><?= Yii::t('gamification', 'Manage Rewards Requests'); ?></div>
                </a>
            </div>
        </li>
    </ul>
    <div class="info">
        <div>
            <h4></h4>
            <p></p>
        </div>
    </div>
</div>