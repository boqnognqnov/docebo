<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 18.2.2016 г.
 * Time: 15:17
 *
 * @var $this RewardController
 * @var $rewardModel GamificationReward
 * @var $userCoins integer
 */
?>

<h1><?= Yii::t('gamification', 'Redeem Reward') ?></h1>

<h6><?= Yii::t('gamification', 'Reward Request') ?></h6>
<br>
<br>
<p><?= Yii::t('gamification', 'Are you sure you want to redeem this reward "{name}" for {coins} coins?<br>After this operation your new coins balance will be {balance}.', array(
        '{name}' => !empty($rewardModel->translation) ? $rewardModel->translation->name : $rewardModel->defaultTranslation->name,
        '{coins}' => $rewardModel->coins,
        '{balance}' => ($userCoins - $rewardModel->coins)
    ))?></p>
<?= CHtml::beginForm('', 'post', array(
    'class' => 'ajax'
)) ?>
<div class="form-actions">
    <?= CHtml::submitButton(Yii::t('standard', '_CONFIRM'), array(
        'class' => 'btn btn-docebo green big confirm',
        'name' => 'redeem'
    )) ?>
    <?= CHtml::button(Yii::t('standard', '_CANCEL'), array(
            'class' => 'btn btn-docebo black big cancel close-dialog'
        )) ?>
</div>

<?= CHtml::endForm() ?>
