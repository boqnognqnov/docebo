<?php
/**
 * Created by PhpStorm.
 * User: NikVas
 * Date: 19.2.2016 г.
 * Time: 13:07
 *
 * @var $this RewardController
 * @var $userModel CoreUser
 */
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'reward-request-send-email-from',
    'htmlOptions' => array(
        'class' => 'ajax form-horizontal'
    )
));

/**
 * @var $form CActiveForm
 */

echo Yii::t('standard','_USER') . ' :   ';
?>
<strong><?= ($userModel)? $userModel->getFullName():''; ?></strong>
<?php

echo CHtml::hiddenField('user',($userModel)? $userModel->getFullName():'',array('id'=>'email-recipient-input'));


echo "<br><br>";

echo CHtml::label(Yii::t('gamification','Message'),'user');
echo CHtml::textArea('body','',array(
        'readonly'=>'readonly',
        'id'    =>  'reward-request-email-body',
        'style' => 'width: 95%',
    )
);

echo CHtml::hiddenField('userId',$userModel->idst);
echo CHTML::hiddenField('requestId',$requestId);
?>
    <div class="form-actions">
        <input type="submit" value="<?=Yii::t('standard', '_SEND')?>" name="send" class="btn btn-docebo big green confirm" />
        <input type="button" name="cancel" value="<?= Yii::t('standard', '_CANCEL') ?>" class="btn close-btn big black close-dialog" id="email-close-btn"/>
    </div>
<?php
$this->endWidget();
?>
<script type="text/javascript">
    $(function(){
        $(document).on('click', '#email-close-btn', function(){
            TinyMce.removeEditorById('reward-request-email-body');
        });
    });
</script>
