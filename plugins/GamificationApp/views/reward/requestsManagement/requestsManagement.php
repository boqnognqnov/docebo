<?php
/**
 * Created by PhpStorm.
 * User: NikVas
 * Date: 16-Feb-16
 * Time: 17:05
 */?>

<?php
$this->breadcrumbs = array(
    Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
    Yii::t('gamification', 'Rewards') => Docebo::createAbsoluteUrl('//GamificationApp/Reward/index'),
    Yii::t('gamification', 'Manage Rewards Requests')
);
?>

<!-- Main Content -->
<div id="rewards-requests-content">
    <div class="main-section group-management">
        <h3 class="title-bold"><?php echo Yii::t('gamification', 'Manage Rewards Requests'); ?></h3>
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'rewards-requests-form',
            'htmlOptions' => array(
                'class' => 'ajax-grid-form',
                'data-grid' => '#rewards-requests-grid'
            )
        ));
        ?>
        <div class="filters-wrapper">
            <div class="filters clearfix">
                <div class="pull-left show-approved-rejected-requests" style="padding-top: 15px;">
                    <?= CHtml::radioButton('show_approved_rejected_requests', true,  array('id' => 'show-all-requests', 'value' => 'all')) . ' ' . CHtml::label(Yii::t('standard', 'All') . '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp', 'show-all-requests', array('style' => 'display:inline-block;')); ?>
                    <?= CHtml::radioButton('show_approved_rejected_requests', false, array('id' => 'show-approved-requests', 'value' => 'approved')) . ' ' . CHtml::label(Yii::t('gamification', 'Approved only') . '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp', 'show-approved-requests', array('style' => 'display:inline-block;')); ?>
                    <?= CHtml::radioButton('show_approved_rejected_requests', false, array('id' => 'show-pending-requests', 'value' => 'pending')) . ' ' . CHtml::label(Yii::t('gamification', 'Pending only') . '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp', 'show-pending-requests', array('style' => 'display:inline-block;')); ?>
                    <?= CHtml::radioButton('show_approved_rejected_requests', false, array('id' => 'show-rejected-requests', 'value' => 'rejected')) . ' ' . CHtml::label(Yii::t('gamification', 'Rejected only') . '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp', 'show-rejected-requests', array('style' => 'display:inline-block;')); ?>
                    <?= CHtml::radioButton('show_approved_rejected_requests', false, array('id' => 'show-canceled-requests', 'value' => 'canceled')) . ' ' . CHtml::label(Yii::t('gamification', 'Canceled only'), 'show-canceled-requests', array('style' => 'display:inline-block;')); ?>
                    <span class="search-icon"></span>
                </div>
                <div class="input-wrapper">
                    <?php
                    echo CHtml::textField('search_text', '', array(
                        'id' => 'advanced-search-rewards-requests',
                        'class' => 'typeahead',
                        'autocomplete' => 'off',
                        'data-url' => Docebo::createAdminUrl('GamificationApp/Reward/autocompleteRequests'),
                        'placeholder' => Yii::t('standard', '_SEARCH'),
                    ));
                    ?>
                    <span class="search-icon"></span>
                </div>
            </div>
        </div>
    </div>
    <?= CHtml::submitButton('', array('style' => 'display: none;')); ?>
    <?php $this->endWidget(); ?>
</div>

<div class="bottom-section clearfix">
    <div id="grid-wrapper">
        <?php
        $_columns = array(
            array(
                'header' => Yii::t('admin_directory', '_DIRECTORY_GROUPID'),
                'name' => 'id',
                'type' => 'raw',
                'value' => '$data->id',
                'headerHtmlOptions' => array(
                    'class' => 'text-center',
                    'style' => 'font-size: 13px!important;',
                ),
                'htmlOptions' => array(
                    'style' => 'word-break:break-all;',
                    'class' => 'text-center user-fullname',
                )
            ),
            array(
                'header' => Yii::t('standard', '_USER'),
                'headerHtmlOptions' => array(
                    'style' => 'width: 100px; font-size: 13px!important;',
                    'class' => 'text-left rewards-requests-management-table-header'
                ),
                'name' => 'user',
                'type' => 'raw',
                'value' => '$data->getUsersFullName()',
                'htmlOptions' => array(
                    'style' => 'word-break:break-all;',
                    'class' => 'text-left user-fullname',
                )
            ),
            array(
                'header' => Yii::t('gamification', 'Picture'),
                'headerHtmlOptions' => array(
                    'class' => 'text-center rewards-requests-management-table-header',
                    'style' => 'width: 60px; font-size: 13px!important;',
                ),
                'name' => 'picture',
                'type' => 'raw',
                'value' => '$data->renderPicture();',
                'htmlOptions' => array(
                    'style' => 'word-break:break-all; margin: 0 auto;',
                    'class' => 'text-center user-reward-picture',
                )
            ),
            array(
                'header' => Yii::t('standard', '_CODE'),
                'name' => 'code',
                'type' => 'raw',
                'value' => '$data->rewardModel->code',
                'headerHtmlOptions' => array(
                    'class' => 'text-center rewards-requests-management-table-header',
                    'style' => 'font-size: 13px!important;',
                ),
                'htmlOptions' => array(
                    'style' => 'word-break:break-all;',
                    'class' => 'text-center user-fullname',
                )
            ),
            array(
                'header' => Yii::t('gamification', 'Reward name'),
                'headerHtmlOptions' => array(
                    'class' => 'text-left rewards-requests-management-table-header',
                    'style' => 'font-size: 13px!important;',
                ),
                'name' => 'name',
                'type' => 'raw',
                'value' => '$data->getRewardName()',
                'htmlOptions' => array(
                    'style' => 'width: 18%;word-break:break-all;',
                    'class' => 'text-left reward-name'
                )
            ),
            array(
                'header' => Yii::t('standard', 'Coins'),
                'headerHtmlOptions' => array(
                    'class' => 'text-center rewards-requests-management-table-header',
                    'style' => 'font-size: 13px!important;',
                ),
                'name' => 'filter',
                'type' => 'raw',
                'value' => '$data->coins',
                'htmlOptions' => array(
                    'class' => 'text-center'
                )
            ),
            array(
                'header' => Yii::t('gamification', 'Requested on'),
                'headerHtmlOptions' => array(
                    'class' => 'text-left rewards-requests-management-table-header',
                    'style' => 'font-size: 13px!important;',
                ),
                'name' => 'period',
                'type' => 'html',
                'value' => '$data->renderRequestedOn(true)',
                'htmlOptions' => array(
                    'class' => 'text-left'
                )
            ),
            array(
                'header' => Yii::t('standard', 'Status'),
                'headerHtmlOptions' => array(
                    'class' => 'text-left rewards-requests-management-table-header',
                    'style' => 'font-size: 13px!important;',
                ),
                'htmlOptions' => array(
                    'class' => 'text-left request-status',
                    'style' => 'width: 8%;word-break:break-all;',
                ),
                'name' => 'active',
                'type' => 'raw',
                'value' => '$data->renderStatus()',
            ),
            array(
                'header' => '',
                'name' => 'actions',
                'type' => 'raw',
                'value' => '$data->renderActions();',
                'htmlOptions' => array(
                    'class' => 'text-right requests-actions-td',
                    'style' => 'width: 120px;font-size:24px;padding-left:20px;'
                )
            ),
            array(
                'header' => '',
                'name' => 'actions',
                'type' => 'raw',
                'value' => '$data->renderEmailAction()',
                'htmlOptions' => array(
                    'class' => 'text-center request-email',
                    'style' => 'width: 50px;font-size:14px;',
                )
            ),
        );

        $gridOptions = array(
            'id' => 'rewards-requests-grid',
            'htmlOptions' => array('class' => 'grid-view clearfix'),
            'dataProvider' => GamificationUserRewards::model()->dataProvider(),
            'template' => '{items}{summary}{pager}',
            'summaryText' => Yii::t('standard', '_TOTAL'),
            'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
            'pager' => array(
                'class' => 'ext.local.pagers.DoceboLinkPager',
            ),
            "afterAjaxUpdate" => "function(id, data) { $(document).controls();
                        $('.rewards-requests-management-action').tooltip({'placement': 'bottom'});
                        $('.action-inactive-reset').tooltip({
                            'placement': 'bottom',
                            'template' : '<div class=\"tooltip bottom\" role=\"tooltip\"><div class=\"tooltip-arrow\"></div><div class=\"tooltip-inner inactive-reset\"></div></div>'
                        });
            }",
            'ajaxUpdate' => 'all-items',
            'columns' => $_columns
        );

        $this->widget('zii.widgets.grid.CGridView', $gridOptions);
        ?>
    </div>
</div>

<script type="text/javascript">

    $(function () {
        $(document).on('click','i.rewards-requests-management-action',function(){
            var requestId = $(this).attr('id');
            var type = requestId.substring(0,requestId.indexOf("-"));
            requestId = requestId.substring(requestId.indexOf("-")+1);
            $.ajax({
                url: '<?= Docebo::createAbsoluteUrl('//GamificationApp/Reward/axChangeRequestStatus'); ?>',
                method : 'POST',
                dataType: 'json',
                data: {'type':type,'id': requestId},
                success: function (data) {
                    if(data.success==false){
                        Docebo.Feedback.show('error', data.message);
                    }
                    $.fn.yiiGridView.update('rewards-requests-grid', {data: {show_approved_rejected_requests: $('input[name="show_approved_rejected_requests"]:checked').val()}});
                }
            });
        });
        $('.rewards-requests-management-action').tooltip({'placement': 'bottom'});
        $('.action-inactive-reset').tooltip({
            'placement': 'bottom',
            'template' : '<div class="tooltip bottom" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner inactive-reset"></div></div>'
        });
        $('.show-approved-rejected-requests input').styler();
        $('input[name="show_approved_rejected_requests"]').on('change', function () {
            $.fn.yiiGridView.update('rewards-requests-grid', {data: {show_approved_rejected_requests: $(this).val(),search_text: $('#advanced-search-rewards-requests').val()}});
        });
        $(document).on('dialog2.content-update', '.modal.modal-send-email', function() {
            TinyMce.attach("#reward-request-email-body", 'standard_embed', {height: 200});
        });
    });
</script>
<!-- Main Content - END -->
