<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 18.2.2016 г.
 * Time: 15:28
 *
 * @var $this RewardController
 */
?>

<h1><?=Yii::t('gamification', 'Thank you')?></h1>

<div class="redeem-success">
    <i class="fa  fa-4x fa-check-circle-o"></i>
    <p><?= Yii::t('gamification', 'Your request has been send to the administrator. You can check the status in your "my rewards requests history" area.') ?></p>
</div>
<?= CHtml::beginForm(Docebo::createAppUrl('//GamificationApp/GamificationApp/MyBadges', array(
    'tab' => 'rewards'
))) ?>
<div class="form-actions">
    <?= CHtml::submitButton(Yii::t('gamification', 'My rewards requests history'), array(
        'class' => 'btn btn-docebo green big'
    )) ?>
    <?= CHtml::button(Yii::t('standard', '_CLOSE'), array(
        'class' => 'btn btn-docebo black big close-dialog cancel'
    )) ?>
</div>
<?= CHtml::endForm() ?>
