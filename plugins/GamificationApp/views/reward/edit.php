<style>
.minitext {
    margin-left: 5px !important;
    font-size: 11px !important;
    line-height: initial;
    width: 21.729915% !important;
    font-weight: 600 !important;
    color: #999999 !important;
}
</style>
<?php
	$this->breadcrumbs = array(
		Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
		Yii::t('gamification', 'Rewards') => Docebo::createAppUrl('admin:GamificationApp/Reward/index'),
		($id_reward == 0 ? Yii::t('gamification', 'New Reward') : Yii::t('gamification', 'Edit Reward'))
	);
?>

<div class="main-actions clearfix">
	<h3 class="title-bold back-button">
		<?= CHtml::link(Yii::t('standard', '_BACK'), $this->createUrl('Reward/index')); ?>
		<?= ($id_reward == 0 ? Yii::t('gamification', 'New Reward') : Yii::t('gamification', 'Edit Reward')); ?>
	</h3>
</div>

<?php $form = $this->beginWidget('CActiveForm',
	array(
		'id' => 'reward-create-form'
	)
); ?>

<?= CHtml::hiddenField('id_reward', $id_reward); ?>
<?= $this->renderPartial('editReward/_translationBlock', array(
	'langs' => $langs,
	'model' => $model,
    'defTimezone' => $defTimezone
), true); ?>

<?= $this->renderPartial('editReward/_imageBlock', array(
	'model' => $model,
    'default_picture' => $default_picture,
    'default_picture_id' => $default_picture_id
), true); ?>

<?= $this->renderPartial('editReward/_associationBlock', array(
	'model' => $model,
    'sets' => $sets
), true); ?>

<div class="right-buttons">
	<?= CHtml::submitButton(Yii::t('standard', '_SAVE'), array('id' => 'save', 'name' => 'save', 'class' => 'btn-docebo green big', 'style' => 'line-height: 16px')); ?>
    <a href="<?=$this->createUrl('/GamificationApp/Reward/index');?>" class="btn-docebo black big"><?=Yii::t('standard', '_CANCEL');?></a>
</div>
<?php $this->endWidget(); ?>

<script>
var Reward = new Reward({debug: false, idReward: '<?= $id_reward ?>', page: 'page_edit_reward'});
</script>

