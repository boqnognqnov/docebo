<div class="advanced-main advanced-main-badge">
	<div class="row odd has-border">
		<div class="row">
			<div class="setting-name">
				<?php echo Yii::t('gamification', 'Associate this reward to a set'); ?>
				<p class="description minitext" style="width: 100% !important;margin-left:0!important;"><?php echo Yii::t('gamification', 'Associate the reward to all or a specific set of rewards'); ?></p>
			</div>
			<div class="values  row-fluid">
                <div id="association_mode" class="span3">
                    <input value="<?= GamificationReward::ASSOCIATION_ALL ?>" id="association_mode_0" <?php if($model->association !== GamificationReward::ASSOCIATION_SELECTION && $model->association !== GamificationReward::ASSOCIATION_NONE): ?>checked="checked"<?php endif; ?> type="radio" name="association_mode" />
                        <label for="association_mode_0"><?= Yii::t('gamification', 'This reward is valid for all sets') ?></label>
                </div>
                <div id="association_mode" class="span3">
                    <input value="<?= GamificationReward::ASSOCIATION_SELECTION ?>" id="association_mode_1" <?php if($model->association == GamificationReward::ASSOCIATION_SELECTION): ?>checked="checked"<?php endif; ?> type="radio" name="association_mode" />
                        <label for="association_mode_1"><?= Yii::t('gamification', 'Associate this reward to one or more sets') ?></label>
                </div>
                <div id="association_mode" class="span4">
                    <input value="<?= GamificationReward::ASSOCIATION_NONE ?>" id="association_mode_2" <?php if($model->association == GamificationReward::ASSOCIATION_NONE): ?>checked="checked"<?php endif; ?> type="radio" name="association_mode" />
                        <label for="association_mode_2"><?= Yii::t('gamification', 'Associate this reward with no set') ?></label>
                </div>
                <div class="span9" style="margin-left: 0px; width: 79%; margin-top: -2px;">
                    <div id="sets_selector" style="display: <?= ($model->association == 'selection' ? 'block' : 'none'); ?>;">
                        <select id="sets" name="params[sets]">
                            <?php if(!empty($sets)): ?>
                                <?php foreach($sets as $info) : ?>
                                    <option value="<?= $info['id_set'] ?>" class="selected"><?= $info['name'] ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                        <div class="errorMessage"></div>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>