<?php $id_reward = $model->id; 
?>
<style>
    
</style>
<div class="advanced-main advanced-main-reward advanced-main-reward-white">
	<div class="row odd has-border">
		<div class="row">
			<div class="setting-name">
				<?= Yii::t('gamification', 'Reward picture'); ?>
				<p class="description minitext" style="width: 100% !important;margin-left:0!important;"><?php echo Yii::t('gamification', 'Upload the reward picture'); ?></p>
			</div>
			<div class="values">
                <span><?= Yii::t('gamification', 'Reward picture'); ?></span>
				<div class="current_picture" style="border:none!important;margin-top: 5px;">
                    <img id="image-player-bg-image-id" class="asset-image-selector-img-class" src="<?= $default_picture ?>" alt="" width="100" height="100">
					<?php 
                        $this->widget('common.widgets.ImageSelector', array(
                            'imageType'            => CoreAsset::TYPE_GAMIFICATION_REWARDS,
                            'assetId'             => $default_picture_id,
                            'imgVariant'        => CoreAsset::VARIANT_SMALL,
                            'buttonText'        => Yii::t('gamification', 'Change reward picture'),
                            'buttonClass'        => 'btn btn-docebo green big new-reward-picture btn-reward-picture',
                            'dialogId'            => 'image-selector-modal-player-bg',
                            'dialogClass'        => 'image-selector-modal new-reward-picture',
                            'inputHtmlOptions'    => array(
                                    'name'    => 'RewardPicture',
                                    'id'    => 'player-bg-image-id',
                                    'class'    => 'image-selector-input'
                            ),
                            'showStandardImages' => false,
                            'showImgPreview' => false
                        ));
                ?>
				</div>
			</div>
		</div>
	</div>
</div>