<?php 
    $request = Yii::app()->request;
    $id_reward = $request->getParam('id', 0);
?>
<div class="advanced-main advanced-main-reward" style="margin-left:0!important;">
	<div class="row odd has-border">
		<div class="row">
			<div class="setting-name">
				<?= Yii::t('gamification', 'Reward details'); ?>
				<p class="description minitext" style="width: 100% !important;margin-left:0!important;"><?= Yii::t('gamification', 'Define the details of the reward'); ?></p>
			</div>
			<div class="values">
				<div class="languages">
					<p><?= Yii::t('gamification', 'Assign details for each language'); ?></p>
				</div>
				<div class="lang-wrapper">
					<?= CHtml::dropDownList('lang_code', Lang::getCodeByBrowserCode(Yii::app()->getLanguage()), $langs); ?>
					<div class="stats">
						<p class="available"><?= Yii::t('admin_lang', '_LANG_ALL'); ?>: <span><?= count($langs); ?></span></p>
						<p class="assigned"><?= Yii::t('standard', 'Filled languages'); ?>: <span></span></p>
					</div>
				</div>

				<div class="input-fields">
					<p><?= Yii::t('gamification', 'Reward name'); ?></p>
					<div class="max-input">
						<?= CHtml::textfield('title_current', '', array('class' => 'title')); ?>
					</div>
					<p><?= Yii::t('gamification', 'Reward description'); ?></p>
					<?= CHtml::textarea('description_current', '', array('class' => 'reward-text')); ?>
				</div>
          
				<div class="translations" style="display: none;">
					<?php
						$models = GamificationRewardTranslation::model()->getModelsList($id_reward);
						foreach ($langs as $langCode => $translation):
                            
                            $langCode = strtolower($langCode);
							$model_l = $models[$langCode];
                            
                            $grt_name = '';
                            $grt_description = '';
                            
                            if(empty($id_reward)) {
                                $grt_name = $request->getParam("name_{$langCode}", '');
                                $grt_description = $request->getParam("description_{$langCode}", '');
                            } else {
                                $grt_name = !empty($model_l->name) ? $model_l->name : $request->getParam("name_{$langCode}", '');
                                $grt_description = !empty($model_l->description) ? $model_l->description : $request->getParam("description_{$langCode}", '');
                            }
					?>
						<div class="<?= $langCode ?>">
							<?= CHtml::textField('name_'.$langCode, $grt_name); ?>
							<?= CHtml::textArea('description_'.$langCode, $grt_description); ?>
						</div>
					<?php endforeach; ?>
				</div>
                <div class="row-fluid" style="padding-top:10px;">
                    <div class="offset12"><p>&nbsp;</p></div>
				</div>
				<div class="row-fluid">
                    <div class="input-fields">
                        <p><?= Yii::t('gamification', 'Reward code'); ?></p>
                        <?= CHtml::textfield('RewardCode', $model->code ? $model->code : '', array('style' => 'width:98%;')); ?>
                    </div>
                </div>
				<div class="row-fluid">
                    <div class="span3" style="width:17.9% !important;">
                        <?= CHtml::label(Yii::t('gamification','Coins'), 'RewardCoins', array(
                            'style' => 'display: inline-block; margin-right: 5px;margin-left:0;'
                        )) ?>
                        <div class='input-append'>
                            <?php echo CHtml::textField('RewardCoins', $model->coins ? $model->coins : '', array(
                                'class' => 'span12',
                                'style' => 'width: 110px;'
                            )); ?>
                        </div>
                    </div>
                    <div class="span2 minitext"><?= Yii::t('gamification', 'How many coins users must spend to redeem this reward') ?></div>
                    <div class="span4" style="width:24.6%;">
                        <?= CHtml::label(Yii::t('gamification','Availability'), 'RewardQuantity', array(
                            'style' => 'display: inline-block; margin-right: 5px;'
                        )) ?>
                        <div class='input-append'>
                            <?= CHtml::textField('RewardQuantity', $model->availability ? $model->availability : '', array(
                                'class' => 'span12',
                                'style' => 'width: 110px;'
                            )) ?>
                        </div>
                    </div>
                    <div class="span3 minitext"><?= Yii::t('gamification', 'How many pieces for this reward are available to be redeemed') ?></div>
                </div>
			</div>
		</div>
	</div>
</div>