<?php
// Prepare  breadcrumbs
$this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('gamification', 'Rewards') => Docebo::createAdminUrl('GamificationApp/reward/index'),
	Yii::t('gamification', 'Reward Set') => Docebo::createAdminUrl('GamificationApp/RewardsSet/index'),
	$rewardsSetModel->name
);

// Show available actions
$this->renderPartial('_mainRewardsSetsActions', array('rewardsSetModel' => $rewardsSetModel));

// Display the list with awards
?>
<div class="selections clearfix">
	<?php $this->widget('common.widgets.ComboListView', array(
		'listId'=>'associated-sets-management-list',
		'dataProvider' => $rewardSetModel->sqlDataProvider(),

		// Autocomplete related:
		'autocompleteRoute' => '/GamificationApp/RewardsSet/rewardsInaSetAutocomplete',
		'hiddenFields'=>array(
			'reward_set_id' => Yii::app()->request->getParam('id', ''),
			'data[reward_set_id]' => Yii::app()->request->getParam('id', ''),
			'responseType'=>'cjuiautocomplete',
		),

		'massiveActions'	=> array('delete' => Yii::t('standard', '_UNASSIGN')),
		'listItemView'=>'plugin.GamificationApp.views.rewardsSet._viewAssociatedSets',
		'afterAjaxUpdate'=>'RewardsSet.prototype.updateFilters();', //'courseAssignModeChanged();',
	)); ?>
</div>

<script>
	$(function(){
		var rewardsSet = new RewardsSet({
			page: 'page_assign_rewards',
			debug: true
		});
	});
</script>
