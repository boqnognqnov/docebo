<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 4.2.2016 г.
 * Time: 13:07
 *
 * @var $this RewardsSetController
 * @var $rewardsSetModel GamificationRewardsSet
 */

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'new-rewards-set-from',
    'htmlOptions' => array(
        'class' => 'ajax form-horizontal'
    )
));

/**
 * @var $form CActiveForm
 */

echo $form->labelEx($rewardsSetModel, 'name');
echo $form->textField($rewardsSetModel, 'name', array(
    'style' => 'width: 95%',
    'class' => 'set-name'
));
echo $form->error($rewardsSetModel, 'name');

echo "<br><br>";

echo $form->labelEx($rewardsSetModel, 'description');
echo $form->textArea($rewardsSetModel, 'description', array(
    'style' => 'width: 95%',
    'id' => 'description-textarea',
    'class' => 'set-description'
));

echo $form->hiddenField($rewardsSetModel, 'id');
?>
<div class="form-actions">
    <input type="submit" value="<?=Yii::t('standard', '_CONFIRM')?>" name="save" class="btn btn-docebo big green confirm" />
    <input type="button" name="cancel" value="<?= Yii::t('standard', '_CANCEL') ?>" class="btn btn-docebo big black close-dialog" />
</div>
<?php
$this->endWidget();
?>