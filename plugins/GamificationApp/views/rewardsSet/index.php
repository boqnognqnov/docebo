<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 4.2.2016 г.
 * Time: 10:04
 *
 * @var $this GamificationRewardsSet
 */
?>
<div class="main-actions clearfix">
    <h3 class="title-bold rewards-set-title"><?php echo Yii::t('gamification', 'Rewards Set'); ?></h3>
    <ul class="clearfix">
        <li>
            <div>
                <?php
                echo CHtml::link('<i class="fa fa-align-justify fa-3x"></i><br><br>'.Yii::t('gamification', 'New Set'), Yii::app()->createUrl('GamificationApp/RewardsSet/edit'), array(
                    'class' => 'new-set open-dialog',
                    'data-dialog-title' => Yii::t('gamification', 'New Reward Set'),
                    'data-dialog-class' => 'modal-new-set',
                ));
                ?>
            </div>
        </li>
    </ul>
    <div class="info">
        <div>
            <h4></h4>
            <p></p>
        </div>
    </div>
</div>

<div class="main-section group-management">
    <h3 class="title-bold"><?php echo Yii::t('gamification', 'Rewards Set'); ?></h3>
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'rewards-set-form',
        'htmlOptions' => array(
            'class' => 'ajax-grid-form',
            'data-grid' => '#rewards-set-grid'
        )
    ));
    ?>
    <div class="filters-wrapper">
        <div class="filters clearfix">
            <div class="input-wrapper">
                <?php echo CHtml::textField('search_text', '', array(
                    'id' => 'advanced-search-rewards-set',
                    'class' => 'typeahead',
                    'autocomplete' => 'off',
                    'data-url' => Docebo::createAdminUrl('GamificationApp/RewardsSet/autocomplete'),
                    'placeholder' => Yii::t('standard', '_SEARCH'),
                )); ?>
                <span class="search-icon"></span>
            </div>
        </div>
    </div>
</div>
<?= CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<div class="bottom-section clearfix">
    <div id="grid-wrapper">
        <?php
        $_columns = array(
            array(
                'name' => 'name',
                'type' => 'raw',
                'htmlOptions' => array(
                    'class' => 'name'
                )
            ),
            array(
                'name' => 'description',
                'type' => 'raw',
                'htmlOptions' => array(
                    'class' => 'description'
                )
            ),
            array(
                'header' => Yii::t('standard', 'Filter'),
                'name' => 'filter',
                'type' => 'raw',
                'value' => '$data->renderFilter();',
                'htmlOptions' => array(
                    'class' => 'filter'
                )
            ),
            array(
                'header' => Yii::t('gamification', 'Assigned Rewards'),
                'headerHtmlOptions' => array(
                    'class' => 'assigned-rewards-header',
                ),
                'name' => 'assigned_rewards',
                'type' => 'raw',
                'value' => '$data->renderAssignedRewards();',
                'htmlOptions' => array(
                    'class' => 'assigned-rewards text-right'
                )
            ),
            array(
                'header' => '',
                'name' => 'edit',
                'type' => 'raw',
                'value' => '$data->renderEdit();',
                'htmlOptions' => array(
                    'class' => 'edit'
                )
            ),
            array(
                'header' => '',
                'name' => 'delete',
                'type' => 'raw',
                'value' => '$data->renderDelete();',
                'htmlOptions' => array(
                    'class' => 'delete'
                )
            ),
        );

        $gridOptions = array(
            'id' => 'rewards-set-grid',
            'htmlOptions' => array('class' => 'grid-view clearfix'),
            'dataProvider' => GamificationRewardsSet::model()->dataProvider(),
            'template' => '{items}{summary}{pager}',
            'summaryText' => Yii::t('standard', '_TOTAL'),
            'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
            'pager' => array(
                'class' => 'ext.local.pagers.DoceboLinkPager',
            ),
            "afterAjaxUpdate" => "function(id, data) { $(document).controls(); }",
            'ajaxUpdate' => 'all-items',
            'columns' => $_columns
        );

        $this->widget('zii.widgets.grid.CGridView', $gridOptions);
        ?>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        var rewardsSet = new RewardsSet({
            page: 'page_rewardset_index',
            debug: true
        });
    });
</script>
