<div class="main-actions reward-set-assignment-actions clearfix">

	<h3 class="title-bold back-button">
		<?php echo CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('GamificationApp/RewardsSet/index')); ?>
		<span><?php echo Yii::t('gamification', 'Assign reward to set'); ?></span>
	</h3>
	<ul class="clearfix">
		<li>
			<div>
				<?php
 //				Yii::app()->createUrl('PowerUserApp/powerUserManagement/assignCourses',
//						array('idst' => $user->idst)),
//					echo CHtml::link('<span></span>'.Yii::t('gamification', 'Assign Rewards'),
//						Yii::app()->createUrl('GamificationApp/RewardsSet/assignRewards',
//								array('id' => $rewardsSetModel->id)),
//
//								array(
//									'class' => 'assign-rewards-to-set popup-handler',
//									'alt' => Yii::t('gamification', 'Assign rewards to a set'),
//									'data-modal-title' => Yii::t('standard', '_ASSIGN_COURSES'),
//									'data-modal-class' => 'assign-rewards-to-set',//'assign-courses',
//									'id' => 'assign-rewards-to-set',//'assign-courses',
//									//'data-after-close' => 'newUserAfterClose();',
////									'data-after-send' => 'powerUserAssignCourseAfterSubmit(data);',
//				));


				echo CHtml::link('<i class="fa fa-gift fa-2x"></i>&nbsp;<i class="fa fa-plus fa-lg"></i><br /><br />'.Yii::t('gamification', 'Assign Rewards'),
						Yii::app()->createUrl('GamificationApp/RewardsSet/axAssignRewardsToSet',
								array('id' => $rewardsSetModel->id)),
						array(
								'class' => 'open-dialog assign-rewards-to-set',
								'data-dialog-title' => Yii::t('gamification', 'Assign Rewards'),
								'data-dialog-class' => 'modal-assign-rewards-to-set'
						));

				?>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>
			<p></p>
		</div>
	</div>

</div>