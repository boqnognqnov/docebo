<?php

// Ensure we have id_set always in order of the proper data processing
$idSet = Yii::app()->request->getParam('id', false);
$id_set = !empty($data['id_set']) ? $data['id_set'] :$idSet;

$itemTypeLabel = '<i class="fa fa-database" style="color: #F4AE3F;"></i>&nbsp;<strong>' . $data['coins'] . '</strong> ' . Yii::t('gamification', 'coins');
$unassignUrl = Docebo::createAbsoluteLmsUrl('GamificationApp/RewardsSet/unassignReward', array('id'=>$id_set, 'id_reward'=>$data['id_reward']));;

?>
<div style="position: relative; padding-right: 25px;">
	<div class="pull-right">
		<div class="course-type"><?= $itemTypeLabel ?></div>
	</div>
	<div class="reward-name"><?php echo $data['reward_name']; ?></div>
	<div class="delete-button">
		<?php

		echo CHtml::link('<span class="i-sprite is-remove red">&nbsp;</span>', $unassignUrl,
				array(
				'class' => 'open-dialog delete-set-reward delete-set-reward-'.$data['id_reward'],
				'data-dialog-title' => Yii::t('gamification', 'Delete Reward From Set'),
				'data-dialog-class' => 'modal-delete-set-reward'
		));

		?>
	</div>
</div>