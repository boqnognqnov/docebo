<?= CHtml::beginForm('','POST', array('class'=>'ajax', 'id'=>'submitForm')) ?>
<div id="grid-wrapper">
	<?php $this->widget('common.widgets.ComboGridView', array(
			'doNotCreateForm' => true,
			'gridTemplate'	=> '{items}{summary}{pager}',
			'gridId' => 'assign-rewards-management-grid',
			'dataProvider' => GamificationAssociatedSets::model()->sqlDataProviderNotAssignedrewards(),
			'massSelectorsInFilterForm' => true,
			'disableSearch' => false,
			'autocompleteRoute'		=> '//GamificationApp/RewardsSet/rewardsNotInaSetAutocomplete&id='.(Yii::app()->request->getParam('id', '')),
			'disableMassActions' => true,
			'gridTitleLabel' => Yii::t('gamification', 'Select Rewards'),
			'massSelectorUrl' => Docebo::createAdminUrl('GamificationApp/RewardsSet/rewardsSelectAll', array('id' => Yii::app()->request->getParam('id', ''))),
			'columns' => array(
					array(
							'header' => '<input type="checkbox" class="select-on-check-all" />',
							'type' => 'raw',
							'headerHtmlOptions' => array(
									'class' => 'reward-checkbox-header-cell'
							),
							'htmlOptions' => array(
									'class' => 'reward-checkbox-cell'
							),
							'value' => 'CHtml::checkBox("id_reward[]",false,array("value"=>$data["id"], "class"=>"select-on-check"))'
					),
					array(
							'header'=> Yii::t('gamification', 'Picture'),
							'type'      => 'html',
							'value'     => array($this, 'renderRewardPicture'),
							'headerHtmlOptions' => array(
								'class' => 'reward-image-header-cell'
							),
							'htmlOptions' => array(
								'class' => 'reward-image'
							)
					),
					array(
							'header'=> Yii::t('gamification', 'Reward Name'),
							'value'=>'$data["reward_name"]',
							'htmlOptions' => array(
								'class'=>'name'
							)

					),
					array(
							'header'=> Yii::t('gamification', 'Coins'),
							'value'=>'$data["coins"]',
							'headerHtmlOptions' => array(
									'class' => 'reward-coins-header-cell'
							),
							'htmlOptions' => array(
									'class' => 'reward-coins-cell'
							)
					),
					array(
							'header'=> Yii::t('gamification', 'Availability'),
							'value'=>'$data["availability"]',
							'headerHtmlOptions' => array(
									'class' => 'reward-availability-header-cell'
							),
							'htmlOptions' => array(
									'class' => 'reward-availability-cell'
							)
					),

			),
	)); ?>


</div>

<div class="form-actions">
	<input class="btn confirm-btn" type="submit" value="<?= Yii::t('standard', '_CONFIRM'); ?>" name="submit"
	       data-submit-form="select-form"/>
	<input class="btn close-btn close-dialog" type="button" value="<?= Yii::t('standard', '_CLOSE'); ?>"/>
</div>

<?= CHtml::hiddenField('selected_rewards', ''); ?>
<?= CHtml::endForm() ?>

<script>
	$(function() {
//		// Use this if we need to show all rewards and already assigned should be checked
//		var preselectedItems = $('input[name=selected_rewards]').val();
//
//		if (preselectedItems != '') {
//			var preselectedArr1 = preselectedItems.split(",");
//			var preselectedArr2 = [];
//			$.each( preselectedArr1, function( key, value ) {
//				preselectedArr2.push(parseInt(value));
//			});
//
//			$('#assign-rewards-management-grid').comboGridView('setSelection', preselectedArr2);
//		}

		// Reset selection in order to prevent error with duplicated ids, when nothing selected
		$('#assign-rewards-management-grid').comboGridView('setSelection', []);

		$('#submitForm').submit(function(e){
			var selectedRewards = $('#assign-rewards-management-grid').comboGridView('getSelection');
			var data = {};
			data['selectedRewards'] = selectedRewards;

			$('input[name=selected_rewards]').val(selectedRewards);
		});

//		$('input').styler();
	});
</script>