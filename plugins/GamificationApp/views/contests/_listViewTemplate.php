<div class="listview-item clearfix row-fluid table-like-container">
    <div class="span3 table-like-cell"><?= empty($data->getName()) ? '&nbsp;' : $data->getName()  ?></div>
    <div class="span3 text-center period table-like-cell"><?= $data->renderPeriod() ?></div>
    <div class="span3 user table-like-cell" style="padding-left: 3% !important;">
        <?php if(!empty($data->winner) && $data->isFinished()) { ?>
            <img src="<?= $data->winner->getUserAvatar() ?>" />
            <?= $data->winner->getFullName() ?>
        <?php } else { echo '&nbsp;';}?>
    </div>
    <div class="span3 text-center table-like-cell"><?= $data->renderStatus() ?></div>
    <div class="span1 details-header text-right table-like-cell"><i class="fa fa-chevron-down fa-lg"></i></div>
</div>
<div class="accordion" style="display: none;">
    <?php
        Yii::app()->controller->widget('GamificationContestResult', array('contest_id' => $data->id, 'showInDialog' => false));
    ?>
</div>