<?php
	$this->breadcrumbs = array(
		Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
		Yii::t('gamification', 'Contests')
	);    
?>

<?= $this->renderPartial('mainActionsMenu/_newContest', array('showHints' => $showHints), true); ?>  

<!-- Main Content -->
<div id="contest-content">
    <div class="main-section group-management">
        <h3 class="title-bold"><?php echo Yii::t('gamification', 'Contests list'); ?></h3>
        <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'contests-form',
                'htmlOptions' => array(
                    'class' => 'ajax-grid-form',
                    'data-grid' => '#contests-grid'
                )
            ));
        ?>
        <div class="filters-wrapper">
            <div class="filters clearfix">
                <div class="pull-left show-open-closed-contests" style="padding-top: 15px;">
                    <?= CHtml::radioButton('show_open_closed_contests', true,  array('id' => 'show-all-contests', 'value' => 'all')) . ' ' . CHtml::label(Yii::t('standard', 'All') . '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp', 'show-all-contests', array('style' => 'display:inline-block;')); ?>
                    <?= CHtml::radioButton('show_open_closed_contests', false, array('id' => 'show-open-contests', 'value' => 'open')) . ' ' . CHtml::label(Yii::t('gamification', 'Open contests') . '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp', 'show-open-contests', array('style' => 'display:inline-block;')); ?>
                    <?= CHtml::radioButton('show_open_closed_contests', false, array('id' => 'show-closed-contests', 'value' => 'closed')) . ' ' . CHtml::label(Yii::t('gamification', 'Closed contests'), 'show-closed-contests', array('style' => 'display:inline-block;')); ?>
                    <span class="search-icon"></span>
                </div>
                <div class="input-wrapper">
                    <?php
                        echo CHtml::textField('search_text', '', array(
                            'id' => 'advanced-search-badge',
                            'class' => 'typeahead',
                            'autocomplete' => 'off',
                            'data-url' => Docebo::createAdminUrl('GamificationApp/Contests/autocomplete'),
                            'placeholder' => Yii::t('standard', '_SEARCH'),
                        ));
                    ?>
                    <span class="search-icon"></span>
                </div>
            </div>
        </div>
    </div>
<?= CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>
</div>

<div class="bottom-section clearfix">
    <div id="grid-wrapper">
        <?php
            $_columns = array(
                array(
                    'header' => Yii::t('gamification', 'Contest name'),
                    'name' => 'name',
                    'type' => 'raw',
                    'value' => '$data->getName();',
                    'htmlOptions' => array(
                        'style' => 'width: 40%;word-break:break-all;'
                    )
                ),
                array(
                    'header' => Yii::t('standard', 'Filter'),
                    'headerHtmlOptions' => array('class' => 'text-center'),
                    'name' => 'filter',
                    'type' => 'raw',
                    'value' => '$data->renderFilter();',
                    'htmlOptions' => array(
                        'class' => 'text-center'
                    )
                ),
                array(
                    'header' => Yii::t('gamification', 'Contest period'),
                    'headerHtmlOptions' => array('class' => 'text-center'),
                    'name' => 'period',
                    'type' => 'html',
                    'value' => '$data->renderPeriod();',
                    'htmlOptions' => array(
                        'class' => 'text-center'
                    )
                ),
                array(
                    'header' => Yii::t('standard', '_STATUS'),
                    'headerHtmlOptions' => array('class' => 'text-center'),
                    'htmlOptions' => array('class' => 'text-center'),
                    'name' => 'active',
                    'type' => 'raw',
                    'value' => '$data->renderStatus();',
                ),
                array(
                    'header' => '',
                    'name' => 'view',
                    'type' => 'raw',
                    'value' => '$data->renderView();',
                    'htmlOptions' => array(
                        'style' => 'width: 30px;'
                    )
                ),
                array(
                    'header' => '',
                    'name' => 'edit',
                    'type' => 'raw',
                    'value' => '$data->renderEdit();',
                    'htmlOptions' => array(
                        'style' => 'width: 30px;'
                    )
                ),
                array(
                    'header' => '',
                    'name' => 'delete',
                    'type' => 'raw',
                    'value' => '$data->renderDelete();',
                    'htmlOptions' => array(
                        'style' => 'width: 30px;'
                    )
                ),
            );

            $gridOptions = array(
                'id' => 'contests-grid',
                'htmlOptions' => array('class' => 'grid-view clearfix'),
                'dataProvider' => GamificationContest::model()->dataProvider(),
                'template' => '{items}{summary}{pager}',
                'summaryText' => Yii::t('standard', '_TOTAL'),
                'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
                'pager' => array(
                    'class' => 'ext.local.pagers.DoceboLinkPager',
                ),
                "afterAjaxUpdate" => "function(id, data) { $(document).controls(); }",
                'ajaxUpdate' => 'all-items',
                'columns' => $_columns
            );

            $this->widget('zii.widgets.grid.CGridView', $gridOptions);
        ?>
    </div>
</div>

<script type="text/javascript">

    $(function () {
        $('.show-open-closed-contests input').styler();
        $('input[name="show_open_closed_contests"]').on('change', function () {
            $.fn.yiiGridView.update('contests-grid', {data: {show_open_closed_contests: $(this).val()}});
        });
        $(document).off('userselector.closed.success', '.modal.users-selector')
            .on('userselector.closed.success', '.modal.users-selector', function(){
                $.fn.yiiGridView.update('contests-grid', {});
            });
        $(document).on("dialog2.content-update", ".delete-contest", function () {
            var autoClose = $(this).find('.auto-close');
            if (autoClose.length > 0) {
                $.fn.yiiGridView.update('contests-grid', {});
            }
        });
    });

</script>
<!-- Main Content - END -->