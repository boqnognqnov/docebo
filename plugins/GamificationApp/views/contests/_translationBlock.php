<?php
    $id_contest = Yii::app()->request->getParam('id', 0);
    $dpFormat = Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short'));
    $timezonesArray = Yii::app()->localtime->getTimezonesArray();
    $currentTimezone = $model->timezone ?: $defTimezone;
    $currentTimezoneText = '';
    preg_match('#\(.*?\)#', $timezonesArray[$currentTimezone], $currentTimezoneText);
?>
<div class="advanced-main advanced-main-contest">
	<div class="row odd has-border">
		<div class="row">
			<div class="setting-name">
				<?= Yii::t('gamification', 'Contest details'); ?>
				<p class="description"><?= Yii::t('gamification', 'Define the details of the contest'); ?></p>
			</div>
			<div class="values">
				<div class="languages">
					<p><?= Yii::t('gamification', 'Assign details for each language'); ?></p>
				</div>
				<div class="lang-wrapper">
					<?= CHtml::dropDownList('lang_code', Settings::get('default_language', 'english'), $langs); ?>
					<div class="stats">
						<p class="available"><?= Yii::t('admin_lang', '_LANG_ALL'); ?>: <span><?= count($langs); ?></span></p>
						<p class="assigned"><?= Yii::t('standard', 'Filled languages'); ?>: <span></span></p>
					</div>
				</div>

				<div class="input-fields">
					<p><?= Yii::t('gamification', 'Contest name'); ?></p>
					<div class="max-input">
						<?= CHtml::textfield('title_current', '', array('class' => 'title')); ?>
					</div>
					<p><?= Yii::t('gamification', 'Contest description'); ?></p>
					<?= CHtml::textarea('description_current', '', array('class' => 'text')); ?>
				</div>

				<div class="translations" style="display: none;">
					<?php
						$models = GamificationContestTranslation::model()->getModelsList($id_contest);
						foreach ($langs as $langCode => $translation):
							$model_l = $models[$langCode];
					?>
						<div class="<?= $langCode ?>">
							<?= CHtml::textField('name_'.$langCode, $model_l->name); ?>
							<?= CHtml::textArea('description_'.$langCode, $model_l->description); ?>
						</div>
					<?php endforeach; ?>
				</div>
                <div class="dates" style="padding-top:10px;">
					<p><?= Yii::t('gamification', 'Contest period'); ?></p>
				</div>
				<div class="row-fluid date-field">
                    <div class="span3">
                        <?= CHtml::label('From', 'ContestDateField_filters_date_from', array(
                            'style' => 'display: inline-block; margin-right: 5px;'
                        )) ?>
                        <div class='input-append date' data-date-format="<?= $dpFormat ?>" data-date-language="<?= Yii::app()->getLanguage() ?>">
                            <?php echo CHtml::textField('ContestDateField[date_start]', $model->from_date ? Yii::app()->localtime->toLocalDate($model->from_date) : '', array(
                                'class' => 'span12 datepicker date-start',
                                'style' => 'width: 110px;'
                            )); ?>
                            <span><i class="home-ico my-calendar datepicker-icon"></i></span>
                        </div>
                    </div>
                    <div class="span3">
                        <?= CHtml::label('To', 'ContestDateField_filters_date_to', array(
                            'style' => 'display: inline-block; margin-right: 5px;'
                        )) ?>
                        <div class='input-append date' data-date-format="<?= $dpFormat ?>" data-date-language= "<?= Yii::app()->getLanguage() ?>">
                            <?= CHtml::textField('ContestDateField[date_end]', $model->to_date ? Yii::app()->localtime->toLocalDate($model->to_date) : '', array(
                                'class' => 'span12 datepicker date-end',
                                'style' => 'width: 110px;'
                            )) ?>
                            <span><i class="home-ico my-calendar datepicker-icon"></i></span>
                        </div>
                    </div>
                    <div class="span6" style="vertical-align:middle;margin:0;">
                        <p style="margin:0;margin-top:-4px;" class="timezone-change-text"><?= !empty($currentTimezoneText) ? $currentTimezoneText[0] : '' ?></p>
                        <a href="javascript:void(0);" id="timezone-change" style="color:#0465AC;"><?= Yii::t('gamification', 'Change timezone?') ?></a>
                    </div>
                </div>
                <div class="row-fluid timezone-field hidden">
                    <br>
                    <p><?= Yii::t('classroom', 'Select Timezone') ?></p>
                    <?= CHtml::dropDownList('timezone', !$model->timezone ? $defTimezone : $model->timezone, Yii::app()->localtime->getTimezonesArray(), array(
                        'class' => 'span12'
                    )) ?>
                </div>
			</div>
		</div>
	</div>
</div>

<script>
    $(function(){
        $('.datepicker').bdatepicker({
            'startDate': 'today',
            'format': '<?= Yii::app()->localtime->dateStringToBDatepickerFormat() ?>',
            'autoclose': true
        });

        $(document).on('click', '.datepicker-icon', function(){
            $(this).parent().parent().find(':input').focus();
        });

        $('.datepicker').change(function(){
            var dateStart = $('.datepicker.date-start').val() === '' ? new Date() : new Date($('.datepicker.date-start').val());
            var dateEnd = $('.datepicker.date-end').val() === '' ? new Date() : new Date($('.datepicker.date-end').val());
            if(dateStart > dateEnd) {
                $('.datepicker.date-end').val('');
            }
        });

        $(document).on('click', 'a#timezone-change', function(){
            var selector = this.parentNode.parentNode.nextElementSibling;
            if($(selector).hasClass('hidden')) {
                $(selector).removeClass('hidden');
            }
        });

        $(document).on('input', 'select#timezone', function(){
            var selectorVal = $('option[value="'+$(this).val()+'"]', this).text().match(/\(.*?\)/)[0];
            $('p.timezone-change-text').text(selectorVal);
        });
    });
</script>