<?php $id_contest = $model->id; ?>
<script>console.log('<?= json_encode(empty($contestRewards)) ?>')</script>
<div class="advanced-main advanced-main-badge">
    <div class="row odd has-border">
        <div class="row">
            <div class="setting-name">
                <?= Yii::t('gamification', 'User Rewards'); ?>
                <p class="description"><?php echo Yii::t('gamification', 'Define the badges that top users will gain once the contest is ended, based on rank'); ?></p>
            </div>
            <div class="values">
                <div class="row-fluid">
                    <div class="span11">
                        <label for="select-reward" class="reward-label" style="margin:auto auto 5px 0;"> <?= Yii::t('gamification', 'Rank'); ?> #<span><?=count($rewards) === $allBadgesCount ? 1 : (count($rewards) + 1) ?></label>
                        <select id="select-reward" name="select-reward" class="event_dropdown reward-dropdown" data-toggle="dropdown-badges">
                            <option><?= Yii::t('standard', 'Please select...') ?></option>
                        </select>
                        <ul class="dropdown-badges" style="display: none">
                            <?php foreach($badges as $badge){
                                /**
                                 * @var $board GamificationBadge
                                 */
								$badgeName = $badge->renderBadgeName();
                                if(!empty($badgeName)){
                                    echo '<li id="' . $badge->id_badge . '" data-value="' . $badge->id_badge . '"> <img width="30" height="30" src="' . CoreAsset::model()->findByPk($badge->icon)->getUrl() . '" />' . CHtml::encode($badgeName) . '</li>';
                                }
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <div class="rank-rewards">
                    <table>
                        <thead>
                            <th style="text-transform:uppercase;">Rank</th>
                            <th style="text-transform:uppercase;">Assigned Badge</th>
                            <th></th>
                            <th></th>
                        </thead>
                        <tbody class="sortable">
                            <tr class="no-rewards <?= !empty($id_contest) ? 'hidden' : '' ?>">
                                <td class="rank-holder-empty">#<span class="rank-number-empty">1</span></td>
                                <td class="icon" style="padding: 10px 0;"><?= Yii::t('gamification', 'No assigned badge') ?></td>
                                <td></td>
                                <td></td>
                            <tr>
                        <?php
                            if($id_contest){
                                $contestRewards = GamificationContestReward::model()->findAllByAttributes(array(
                                    'id_contest' => $id_contest
                                ));

                                if(!empty($contestRewards)){
                                    foreach($contestRewards as $reward):
                                        /**
                                         * @var $reward GamificationContestReward
                                         */
                                        ?>
                                        <tr>
                                            <td class="rank-holder">#<span class="rank-number"><?=$reward->rank ?></span>
                                                <input data-item-type="id_item" type="hidden" name="GamificationContestReward[<?=$reward->rank?>][id_item]" value="<?=$reward->id_item ?>" />
                                                <input data-item-type="type" type="hidden" name="GamificationContestReward[<?=$reward->rank?>][type]" value="<?=$reward->type ?>" />
                                            </td>
                                            <td class="icon"><img src="<?= $reward->type === GamificationContestReward::REWARD_TYPE_BADGE ? $reward->getBadgeUrl() : '#'?>" /><?= $reward->type === GamificationContestReward::REWARD_TYPE_BADGE ? $reward->getBadgeName() : ""?></td>
                                            <td class="actions"><span class="p-sprite drag-small-black move"></span></td>
                                            <td class="actions"><span data-option-id="<?=$reward->id_item?>" class="remove-reward p-sprite cross-small-red"></span></td>
                                        <tr>
                                    <?php endforeach;
                            }
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <script>
                $(function () {

                    $('#select-reward').dropdownImage({
                        select: function(){
                            addReweard();
                        },
                    });

                    var current_rank = <?= count($rewards) > 0 ? count($rewards) : 0 ?>;
                    <?php
                        $preselectedArray = array();
                        if(!empty($preselectedBadges)){
                            foreach($preselectedBadges as $badge){
                            /**
                            * @var $badge GamificationBadge
                             */
                                $preselectedArray[$badge->id_badge] = !empty($badge->badgeTranslation) ? $badge->badgeTranslation->name : $badge->defaultTranslation->name;
                            }

                            $preselectedArray = json_encode($preselectedArray);

                            echo "var cachedBadges = " . $preselectedArray . ";";
                        } else{
                            echo "var cachedBadges = [];";
                        }
                     ?>

                    function addReweard(){
                        var requestedBadge = $('select[name=select-reward]').val();
                        var optionElement = $('.dropdown-badges li#' + requestedBadge);
                        cachedBadges[requestedBadge] = optionElement.text();
                        $.ajax({
                            type: "POST",
                            url: "./index.php?r=GamificationApp/Contests/getBadgeData",
                            async: true,
                            data: { id_badge:requestedBadge },
                            success: function(result) {
                                var data = JSON.parse(result);
                                current_rank++;
                                $('.reward-label span').html(current_rank + 1);
                                $('.sortable').append('' +
                                    '<tr>' +
                                    '<td class="rank-holder">#<span class="rank-number">' + current_rank + '</span>' +
                                    '<input data-item-type="id_item" type="hidden" name="GamificationContestReward[' + current_rank + '][id_item]" value="' + data.id + '" />' +
                                    '<input data-item-type="type" type="hidden" name="GamificationContestReward[' + current_rank + '][type]" value="badge" />' +
                                    '</td>' +
                                    '<td class="icon"><img src="' + data.url + '" />' + data.name + '</td>' +
                                    '<td class="actions"><span class="p-sprite drag-small-black move"></span></td>' +
                                    '<td class="actions"><span data-option-id="' + requestedBadge + '" class="remove-reward p-sprite cross-small-red"></span></td>' +
                                    '<tr>' +
                                    '');
                                $('#select-reward').html('<option>Please select...</option>');
                                $('tr.no-rewards').not('.hidden').addClass('hidden');
                            }
                        });
                    }

                    var sort = function(){
                        var rank = 0;
                        $('.rank-number').each(function(index){
                            $(this).html(++rank);
                        });

                        rank = 0;
                        $('.rank-holder').each(function(){
                            ++rank;
                            var inputs = $(this).children('input');
                            inputs.each(function(){
                                var name = $(this).attr('name');
                                var type = $(this).data('item-type');
                                $(this).attr('name', 'GamificationContestReward[' + rank + '][' + type + ']');
                            });
                        })
                        if(rank == 0) {
                            $('tr.no-rewards.hidden').removeClass('hidden');
                        }
                    };

                    $(document).on('click', '.remove-reward', function(){
                        var optionId = $(this).data('option-id');
                        //$('.dropdown-badges').append('<option value="' + optionId + '">' + cachedBadges[optionId] + '</option>');
                        var imgUrl = $(this).parent().parent().find('img').attr('src');
                        $('.dropdown-badges').append('<li id="' + optionId + '" data-value="' + optionId + '"> <img width="30" height="30" src="' + imgUrl + '" />' + cachedBadges[optionId] + '</li>');
                        delete cachedBadges[optionId];
                        $(this).parent().parent().remove();
                        $('.reward-label span').html(current_rank--);
                        sort();
                    });

                    $('.sortable').sortable({
                        handle: '.move',
                        opacity: 0.6,
                        cursor: 'move',
                        tolerance: 'pointer',
                        forcePlaceholderSize: true,
                        update: function(event, ui){
                           sort();
                        },
                        helper: function(e, tr) {
                            var $originals = tr.children();
                            var $helper = tr.clone();

                            $helper.children().each(function(index) {
                                // Set helper cell sizes to match the original sizes
                                $(this).width($originals.eq(index).width());
                            });
                            return $helper;
                        },
                        create: function(){
                            $('body').css('overflow-y', 'visible');
                            $(this).css('width', '100%');
                        },
                    });
                });
            </script>
        </div>
    </div>
</div>