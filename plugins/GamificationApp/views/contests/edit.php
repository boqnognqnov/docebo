<?php
	$this->breadcrumbs = array(
		Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
		Yii::t('gamification', 'Contests') => Docebo::createAppUrl('admin:GamificationApp/Contests/index'),
		($id_contest == 0 ? Yii::t('gamification', 'New Contest') : Yii::t('gamification', 'Edit Contest'))
	);
?>

<div class="main-actions clearfix">
	<h3 class="title-bold back-button">
		<?= CHtml::link(Yii::t('standard', '_BACK'), $this->createUrl('Contests/index')); ?>
		<?= ($id_contest == 0 ? Yii::t('gamification', 'New Contest') : Yii::t('gamification', 'Edit Contest')); ?>
	</h3>
</div>

<?php $form = $this->beginWidget('CActiveForm',
	array(
		'id' => 'contest-create-form'
	)
); ?>

<?= CHtml::hiddenField('id_contest', $id_contest); ?>
<?= $this->renderPartial('_translationBlock', array(
	'langs' => $langs,
	'model' => $model,
    'defTimezone' => $defTimezone
), true); ?>

<?= $this->renderPartial('_associationBlock', array(
	'events' => $events,
    'goals' => $goals,
	'model' => $model
), true); ?>

<?= $this->renderPartial('_rewardsBlock', array(
	'events' => $events,
    'rewards' => $rewards,
	'badges' => $badges,
	'model' => $model,
	'preselectedBadges' => $preselectedBadges
), true); ?>

<div class="right-buttons">
	<?= CHtml::submitButton(Yii::t('standard', '_SAVE'), array('id' => 'save', 'name' => 'save', 'class' => 'btn-docebo green big', 'style' => 'line-height: 16px')); ?>
    <a href="<?=$this->createUrl('/GamificationApp/Contests/index');?>" class="btn-docebo black big"><?=Yii::t('standard', '_CANCEL');?></a>
</div>

<?= $this->renderPartial('plugin.GamificationApp.views.gamificationApp._editJavascript', array(
	'id_contest' => $id_contest
), true); ?>
<?php $this->endWidget(); ?>