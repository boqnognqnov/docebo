<div class="advanced-main advanced-main-badge advanced-main-badge-white">
	<div class="row odd has-border">
		<div class="row">
			<div class="setting-name">
				<?php echo Yii::t('gamification', 'Contest Goal'); ?>
				<p class="description"><?php echo Yii::t('gamification', 'Define the goal that users must accomplish in order to get rewarded'); ?></p>
			</div>
			<div class="values">
                <div <?= ($model->goal == 'manual' ? '' : ' class="display-block"'); ?>>
                    <label for="event" style="margin:auto auto 5px 0;"><?= Yii::t('gamification', 'Choose contest goal')?></label>
					<div>
						<?php
							$goals_list = array();
							$goals_details = '<div class="main-actions main-actions-event">';
							$first = true;
							foreach($goals as $goal_name => $details)
							{
								$goals_list[$goal_name] = $details;
								$goals_details .= '
									<div id="info_'.$goal_name.'" class="info" style="display: '.((($model->goal && ($goal_name == $model->goal)) || (!$model->goal && $first)) ? 'block' : 'none').';">
										<div>
											<h4><span></span>'.$details.'</h4>
											<p>'.$details.'</p>
										</div>
									</div>
								';
								$first = false;
							}
							$goals_details .= '</div>';
						?>
						<?= CHtml::dropDownList('goal', $model->goal, $goals_list, array('class' => 'goal_dropdown')); ?>
						<?= $goals_details ?>
					</div>
					<div id="goal_setting">
                        <?php /* EVENT SETTINGS FORM HERE (AJAX LOADED) */ ?>
					</div>
                    <script>
                    $(function(){
                        $('#goal').trigger('change');
                    });
                    </script>
				</div>
			</div>
		</div>
	</div>
</div>