<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 3.12.2015 г.
 * Time: 12:11
 */

?>

<div id="course-selector-settings">
    <div>
        <?= CHtml::radioButton('params[courses_association]', (!isset($params['courses_association']) || (isset($params['courses_association']) && $params['courses_association'] == 'all') ? true : false), array('id' => 'courses_association_0', 'class' => 'radio-course-selection', 'value' => 'all')); ?>
        <label for="courses_association_0" class="label-course-selection"><?= Yii::t('standard', 'Any course'); ?></label>

        <?= CHtml::radioButton('params[courses_association]', (isset($params['courses_association']) && $params['courses_association'] == 'selection' ? true : false), array('id' => 'courses_association_1', 'class' => 'radio-course-selection', 'value' => 'selection')); ?>
        <label for="courses_association_1" class="label-course-selection"><?= Yii::t('report', '_SEL_COURSES'); ?></label>
    </div>
    <div class="clearfix"></div>
    <div id="course_selector" style="display: <?= (isset($params['courses_association']) && $params['courses_association'] == 'selection' ? 'block' : 'none'); ?>;">
        <select id="courses" name="params[courses]">
            <?php if(!empty($courses)): ?>
                <?php foreach($courses as $info) : ?>
                    <option value="<?= $info['idCourse']; ?>" class="selected"><?= ($info['code'] != '' ? $info['code'].' - ' : '').$info['name']; ?></option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
        <div class="errorMessage"></div>
    </div>
</div>
<script type="text/javascript">
    $('.radio-course-selection').styler();

    $('.radio-course-selection').change(function(e)
    {
        $('#course_selector').find('.errorMessage').html('');
        if($('#courses_association_1').is(':checked')) {
            $('#course_selector').show();
            $('#number_selector').hide();
            $('#limit').prop('disabled', 'disabled');
        } else {
            $('#course_selector').hide();
            $('#number_selector').hide();
            $('#courses').val('');
            $('#limit').prop('disabled', 'disabled');
        }

    });

    $(document).ready(function(){
        $("#courses").fcbkcomplete({
            json_url: "./index.php?r=GamificationApp/GamificationApp/getCourseList",
            addontab: true,
            input_min_size: 0,
            width: '98.5%',
            cache: true,
            complete_text: '',
            filter_selected: true
        });

        $('#badge-create-form').submit(function(e){
            if($('#association_mode_0:checked').length == 0) {
                if($('#course-selector-settings').length == 0)
                    return true;

                if(($('input:radio[name="params[courses_association]"]:checked').val() == 'selection') && !$("#courses").val()) {
                    $('#course_selector').find('.errorMessage').html(<?=CJSON::encode(Yii::t('gamification', 'Please select at least one course'))?>);
                    e.preventDefault();
                }
            }
        });
    });
</script>
