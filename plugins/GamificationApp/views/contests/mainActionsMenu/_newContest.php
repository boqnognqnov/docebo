<div class="main-actions clearfix">
    <h3 class="title-bold"><?php echo Yii::t('gamification', 'Contests'); ?></h3>
    <ul class="clearfix">
        <li data-bootstro-id="bootstroNewContest" id="create_contest" data-bootstro-placement="bottom" data-bootstro-width="600px" data-original-title="<?= Yii::t('overlay_hints', 'Start here!') ?>" data-bootstro-title="<?= Yii::t('overlay_hints', 'Start here!') ?>" title="<?= Yii::t('overlay_hints', 'Start here!') ?>" class="bootstro-highlight">
            <div>
                <a href="<?= $this->createUrl('Contests/edit'); ?>" class="additional-fields" alt="<?= Yii::t('helper', 'New contest - gamification'); ?>">
                    <span></span><?= Yii::t('gamification', 'New Contest'); ?>
                </a>
            </div>
        </li>
    </ul>
    <div class="info">
        <div>
            <h4></h4>
            <p></p>
        </div>
    </div>
</div>
<?php
    if ($showHints) {
        $this->widget('OverlayHints', array('hints' => 'bootstroNewContest'));
    }
?>