<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 18.11.2015 г.
 * Time: 10:15
 *
 * @var $this UsersSelectorController
 *
 * @var $model LearningReportFilter
 */
?>
<?php
$model = GamificationLeaderboard::model()->findByPk($this->idGeneral);
$hasUsersSelected = $model ? $model->hasUsersSelected() : false;
?>

<div class="leaderboard-users-filter-top-panel">
    <br>
    <div class="row-fluid">
        <div class="span12">
            <?= Yii::t('gamification','Apply this leaderboard to all Branches or select custom Branches and/or groups') ?>
        </div>
    </div>
    <?php if (Yii::app()->user->hasFlash('error')) : ?>
        <div class="row-fluid">
            <div class="span12">
                <div class='alert alert-error alert-compact text-left'>
                    <?= Yii::app()->user->getFlash('error'); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="row-fluid">
        <div class="row-fluid">
            <div class="grey-wrapper">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <div class="span5">
                                <label class="radio">
                                    <?php
                                    echo CHtml::radioButton('users-filter', !$hasUsersSelected, array('value' => LearningReportFilter::ALL_COURSES));
                                    echo Yii::t('standard', 'All branches and groups');
                                    ?>
                                </label>
                            </div>
                            <div class="span5">
                                <label class="radio">
                                    <?php
                                    echo CHtml::radioButton('users-filter', $hasUsersSelected, array('value' => LearningReportFilter::SELECT_COURSES));
                                    echo Yii::t('standard', 'Select branches and/or groups');
                                    ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $(function(){
            var selectorTopPanelReady = function() {

                /* Local function to update UI  */
                var updateUi = function(xtype) {
                    var elem;

                    if(xtype == 'users'){
                        elem = $('input[name="users-filter"]:checked');
                    }

                    var modal = elem.closest('.modal');
                    modal.css("min-height", "1px");
                    modal.find('.modal-body').css("min-height", "1px");


                    if (elem.val() == 1) {
                        $('.main-selector-area').hide();
                    }
                    else if(elem.val() == 0){
                        $('.main-selector-area').fadeIn();
                    }
                }


                // Detect type (Courses ?) by available input option
                var xtype;
                var elem;

                if($('input[name="users-filter"]').length > 0){
                    xtype = 'users';
                    elem = $('input[name="users-filter"]:checked');
                }

                $('input[name="users-filter"]').on('change', function(){
                    $(this).trigger('refresh');
                    updateUi('users');
                });

                $('input[name="users-filter"]').trigger('change');

                // Style inputs (but see listeners, wehere we trigger "refresh"!
                $('.leaderboard-users-filter-top-panel :input[type="radio"]').styler();


                updateUi(xtype);
            };

            selectorTopPanelReady();
        });

    </script>
