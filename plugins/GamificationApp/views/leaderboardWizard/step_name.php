<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 27.11.2015 г.
 * Time: 11:48
 *
 * @var $model GamificationLeaderboard
 * @var $langs array
 */
?>
<?php $id_board = $model->id; ?>

<?php DoceboUI::printFlashMessages(); ?>

<div class="leaderboard-name-dialog">
    <div class="row-fluid">
        <div class="span6">
                <?=CHtml::label(Yii::t('gamification', 'Assign details for each language'), 'lang_code')?>
                <?= CHtml::dropDownList('lang_code', Settings::get('default_language', 'english'), $langs); ?>
        </div>
        <div class="span6">
            <div class="stats">
                <p class="available"><?= Yii::t('admin_lang', '_LANG_ALL'); ?>: <span><?= count($langs); ?></span></p>
                <p class="assigned"><?= Yii::t('standard', 'Filled languages'); ?>: <span></span></p>
            </div>
        </div>
    </div>
    <br>
    <div class="row-fluid input-fields">
            <?= CHtml::label(Yii::t('gamification', 'Leaderboard name')  . '*', 'title_current')?>
            <?= CHtml::textfield('title_current', '', array('class' => 'title', 'style' => 'width: 100%')); ?>
    </div>

    <div class="translations" style="display: none;">
        <?php
        $models = GamificationLeaderboardTranslation::model()->getModelsList($id_board);
        foreach ($langs as $langCode => $translation):
            $model_l = $models[$langCode];
            ?>
            <div class="<?= $langCode ?>">
                <?= CHtml::textField('name_'.$langCode, $model_l->name); ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>

<?php
    if(!empty($model->id)){
        echo CHtml::hiddenField('id', $model->id);
    }
?>

<?= $this->renderPartial('plugin.GamificationApp.views.gamificationApp._editJavascript', array(
    'id_badge' => $id_board,
    'leaderboardContext' => true
), true); ?>

<script type="text/javascript">
    jwizard.setDialogClass('leaderboard-wizard-step-name');

    $(function(){
        if ( $(".alert-error").length) {
            setInterval(function(){
                $(".alert-error").fadeOut(1500);
            }, 3000);
        }
    })
</script>
