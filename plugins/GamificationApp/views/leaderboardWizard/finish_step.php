<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 27.11.2015 г.
 * Time: 17:52
 */
?>

<div class="wrapper clearfix report-success row-fluid">

    <div class="span4">
        <div class="left success" style="margin-top: 40px;">
            <div class="copy-success-icon"></div>
            <?php echo Yii::t('standard', '_OPERATION_SUCCESSFUL'); ?>
        </div>
    </div>

    <div class="span8">

        <p class="intro">
            <?php echo Yii::t('standard', 'What do you want to do next?'); ?>
        </p>
        <div class="row-fluid row-with-border">
            <div class="span7">
                <p><?php echo Yii::t('gamification', '<strong>Create</strong> another leaderboard'); ?></p>
            </div>
            <div class="span5 text-right">
                <?php
                echo CHtml::submitButton(Yii::t('standard', '_CREATE'), array(
                    'name' => 'new_leaderboard',
                    'class' => 'btn btn-docebo green big pull-right jwizard-nav jwizard-finish',
                ));
                ?>
            </div>
        </div>

        <div class="row-fluid row-with-border">
            <div class="gamification-save-leaderboard-msg-container">
                <p><?php echo Yii::t('gamification', '<strong>Just save</strong> the leaderboard'); ?></p>
            </div>
            <div class="gamification-save-leaderboard-btn-container text-right">
                <?php
                echo CHtml::submitButton(Yii::t('report', '_SAVE_BACK'), array(
                    'name' => 'finish_wizard_show',
                    'class' => 'btn btn-docebo green big jwizard-nav jwizard-finish',
                ));
                ?>
            </div>
        </div>

    </div>

</div>

<?php
if(!empty($model->id)){
    echo CHtml::hiddenField('id', $model->id);
}
?>
<script type="text/javascript">
    $(function() {
        jwizard.setDialogClass('leaderboard-wizard-step-finish');
    });
</script>
