<?php
/* @var $this LeaderboardWizardController
 * @var $form      CActiveForm
 * @var $alias     string
 * @var $uniqueId  string
 * @var $innerHtml string
 * @var $showNextButton
 */

$uniqueId = 'wizard-' . Docebo::randomHash();
?>

<div class="form wizard-step <?= $alias ?>" id="<?= $uniqueId ?>">

    <?php
    $form = $this->beginWidget( 'CActiveForm', array(
        'id'          => $uniqueId,
        'htmlOptions' => array(
            'class' => 'ajax jwizard-form',
            'name'  => 'step-' . $alias
        ),
    ) );

    // Wizard related
    echo CHtml::hiddenField( 'from_step', $alias );
    ?>

    <?php if( Yii::app()->user->hasFlash( 'error' ) ) : ?>
        <div class="row-fluid">
            <div class='alert alert-error alert-compact text-right'>
                <?= Yii::app()->user->getFlash( 'error' ); ?>
            </div>
        </div>
    <?php endif; ?>


    <?= $innerHtml ?>

    <br>

    <!-- For Dialog2 - they go in footer -->
    <div class="form-actions jwizard">

        <?php

        $firstStep = $this->getFirstStep();

        if($firstStep['alias'] !== $alias) {
            echo CHtml::submitButton(Yii::t('standard', '_PREV'), array(
                'class' => 'btn btn-docebo green big jwizard-nav prev_button',
                'name' => 'prev_button',
            ));
        }
        if($showNextButton) :
        echo CHtml::submitButton( Yii::t( 'standard', '_NEXT' ), array(
            'class' => 'btn btn-docebo green big jwizard-nav next_button',
            'name'  => 'next_button',
        ) );
        endif;
        ?>
        <?= CHtml::button( Yii::t( 'standard', '_CANCEL' ), array( 'class' => 'btn btn-cancel close-dialog' ) ); ?>
    </div>

    <?php $this->endWidget(); ?>

</div>

<script type="text/javascript">

    $(function () {
        $('input,select', '#<?=$uniqueId?>').styler();
    });

</script>