<div id="grid-wrapper" class="courseEnroll-user-table">
<h2><?php echo Yii::t('standard', 'Enroll users'); ?></h2>
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'courseEnroll-user-grid',
		'htmlOptions' => array('class' => 'grid-view clearfix'),
		'dataProvider' => $model->dataProviderEnroll($courseId),
		'cssFile' => false,
		'columns' => array(
			array(
				'class' => 'CCheckBoxColumn',
				'selectableRows' => 2,
				'id' => 'courseEnroll-user-grid-checkboxes',
				'value' => '$data->idst',
				'checked' => 'in_array($data->idst, Yii::app()->session[\'selectedItems\'])',
			),
			array(
				'name' => 'userid',
				'value' => 'Yii::app()->user->getRelativeUsername($data->userid);',
				'type' => 'raw',
			),
			array(
				'name' => 'fullname',
				'header' => Yii::t('standard', '_FULLNAME'),
				'value' => '$data->fullname',
				'type' => 'raw',
			),
			'email',
		),
		'template' => '{items}{summary}{pager}',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
		'pager' => array(
			'class' => 'ext.local.pagers.DoceboLinkPager',
		),
		'beforeAjaxUpdate' => 'function(id, options) {
			options.type = "POST";
			if (typeof options.data == "undefined") {
				options.data = {};
			}

			// Pass Search text; Controller is expecting: CoreUser[search_input]
			options.data["CoreUser[search_input]"] = $("#enroll-advanced-search-user").val();

			options.data.contentType = "html";
			options.data.selectedItems = $(\'#\'+id+\'-selected-items-list\').val();
		}',
		'ajaxUpdate' => 'courseEnroll-user-grid-all-items',
		'afterAjaxUpdate' => 'function(id, data){$("#courseEnroll-user-grid input").styler(); afterGridViewUpdate(id);}',
	)); ?>
</div>
