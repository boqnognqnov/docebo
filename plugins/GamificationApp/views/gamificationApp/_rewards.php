<h4><?php echo Yii::t('gamification', 'My Rewards History'); ?></h4>

<div id="rewards-content">
	<?= CHtml::beginForm('','POST', array('class'=>'ajax', 'id'=>'submitForm')) ?>
	<div id="grid-wrapper">
		<?php
			 $this->widget('DoceboCGridView', array(
				'template'	=> '{items}{pager}',//'{items}{summary}{pager}',
				'id' => 'my-rewards-grid',
				'dataProvider' => GamificationUserRewards::model()->dataProvider(Yii::app()->user->idst),
				'afterAjaxUpdate' => ' function(id, data){
						$(document).controls();
					}',
				'columns' => array(
						array(
								'header'        => Yii::t('admin_directory', '_DIRECTORY_GROUPID'),
								'value'         =>'$data->id',
								'headerHtmlOptions'   => array(
										'style' => 'width: 30px;'
								),
								'htmlOptions'   => array(
										'class' =>'id',
								),
								'cssClassExpression' => '"id-" . $data->id',

						),

						array(
								'header'=> Yii::t('gamification', 'Picture'),
								'headerHtmlOptions' => array(
									'class' => 'text-center',
									'style' => 'width: 80px;'
								),
								'type'      => 'raw',
								'value'     => '$data->renderPicture()',
								'htmlOptions'   => array(
									'class' => 'gamification-request-img',
								),
						),
						array(
								'header'        => Yii::t('gamification', 'Reward'),
								'value'         =>'$data->getRewardName()',
								'htmlOptions'   => array(
														'class' =>'name'
													),
								'cssClassExpression' => '"name-" . $data->id',

						),
						array(
								'header'    => Yii::t('gamification', 'Date'),
								'headerHtmlOptions' => array(
									'style' => 'width: 140px;'
								),
								'value'     =>'$data->renderRequestedOn(false, true)',
								'htmlOptions'   => array(
										'style' => 'padding-right: 10px;',
										'class' => 'date text-left'
								)
						),
						array(
								'header'    => Yii::t('gamification', 'Status'),
								'headerHtmlOptions' => array(
									'style' => 'width: 100px;'
								),
								'htmlOptions'   => array(
										'class' => 'text-right'
								),
								'type'      => 'raw',
								'value'     => function($data) {
													return $data->renderStatus();
												},
								'cssClassExpression' => '"status-" . $data->id',
						),
						array(
								'header'    => '',
								'type'      => 'raw',
								'headerHtmlOptions' => array(
									'style' => 'width: 80px;',
									'class' => 'text-right'
								),
								'htmlOptions'   => array(
									'style' => 'padding-right: 20px;',
									'class' => 'text-right'
								),
								'cssClassExpression' => '"message-" . $data->id',
								'value'     => function($data) {
													// Build modal URL
													$responseUrl = Yii::app()->createUrl('GamificationApp/GamificationApp/sendMessage');
													$responseUrl .= '&userId=' . $data->userModel->idst;
													$responseUrl .= '&rewardId=' . $data->rewardModel->id;
													$responseUrl .= '&id=' . $data->id;

													$response = '';
													$currentUserEmail = Yii::app()->user->getEmail();

													if(!empty($currentUserEmail)) {
														$response = CHtml::link('<i class="fa fa-envelope-o fa-2x"></i>',
																$responseUrl,
																array(
																		'class' => 'reward-send-message open-dialog',
																		'data-dialog-title' => Yii::t('standard', 'Send Message'),
																		'data-dialog-class' => 'modal-reward-send-message',
																));
													}

													return $response;
												}
						),
				),
		)); ?>


	</div>



	<?= CHtml::hiddenField('selected_rewards', ''); ?>
	<?= CHtml::endForm() ?>
</div>

<script type="text/javascript">
	(function ($) {
		$(function () {

			$(document).controls();

		});
	})(jQuery);
</script>