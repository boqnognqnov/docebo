<div class="expert-goal-settings row-fluid">
	<?php
	$id_badge = Yii::app()->request->getParam("id_badge", false);
	$sub_event = Yii::app()->request->getParam("sub_event", -1);
	$labels = array(
		array(
			'before'=>Yii::t("app7020","Assign when expert satisfied at least"),
			'after' =>Yii::t("app7020","requests")
		),
		array(
			'before'=>Yii::t("app7020","Assign when expert made at least"),
			'after' =>Yii::t("app7020","peer review message in asset")
		),
		array(
			'before'=>Yii::t("app7020","Assign when expert published at least"),
			'after' =>Yii::t("app7020","asset")
		)
	);

	if($id_badge){
		$model = GamificationBadge::model()->findByPk($id_badge);
		$event_params = json_decode($model->event_params);
		$before = $labels[$event_params->goal]['before'];
		$after = $labels[$event_params->goal]['after'];
	} else {
		$before = $labels[0]['before'];
		$after = $labels[0]['after'];
	}

	if($sub_event != -1){
		$before = $labels[$sub_event]['before'];
		$after = $labels[$sub_event]['after'];
	}

	?>

	<div class="row-fluid"><?php echo Yii::t('app7020','Goal') ?></div>
	<div id="user-goal-event-selector" class="row-fluid">
		<select id="user-goal" name="params[goal]">
			<?php if(!empty($goals)): ?>
				<?php foreach($goals as $key=>$value) : ?>
					<option value="<?php echo $key ?>" <?php if ($id_badge && $sub_event == -1 && $key == $event_params->goal) {echo 'selected="selected"';} if ($sub_event && $key == $sub_event) {echo 'selected="selected"';}  ?> class="selected"><?php echo $value; ?></option>
				<?php endforeach; ?>
			<?php endif; ?>
		</select>
		<div class="errorMessage"></div>
	</div>

	<div class="row-fluid">
		<?php echo $before; ?> &nbsp;
		<input style="width: 28px; height:28px; text-align:center" id="limit" type="text" name="params[limit]" value="<?= isset($params['limit']) ? $params['limit'] : '' ?>" /> &nbsp;
		<?php echo $after; ?>
	</div>
</div>

<script>
	$(function(){
		var url = window.location.href;
		$('#user-goal').change(function(){
			previous_event = $("#event").val();
			$.ajax({
				type: "POST",
				url: "./index.php?r=GamificationApp/GamificationApp/getEventSetting",
				async: true,
				data: {sub_event:this.value,event:previous_event, id_badge:<?= $id_badge ? $id_badge : 0 ?>},
				beforeSend: function(xhr) {
					$('#event_setting').html('');
					$('#event_setting').addClass("event-loading");
				},
				success: function(result) {
					$('#event_setting').html(result);
				}
			}).always(function() {
				$('#event_setting').removeClass("event-loading");
			});
		});
	});
</script>