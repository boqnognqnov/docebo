<div id="courseEnroll-orgchart-table" class="courseEnroll-orgchart-table">

	<?php if (!empty($fullOrgChartTree)) { ?>
		<ul class='node-tree'>
			<?php $level = 1; ?>
			<?php foreach ($fullOrgChartTree as $node) {
				$left  = 10 * $node->lev;
				$rootNode = '';

				$hasChildren = 'hasChildren';
				// If node is a REAL leaf, set hasChildren to '' (NO children)
				if ($node->isLeaf()) {
					$hasChildren = '';
				}
				// Maybe this node is a "fake" leaf due to Power User assignments? (see CoreAdminTree::getPowerUserOrgChartsOnly())
				else if (in_array($node->idOrg, $puLeafs)) {
					$hasChildren = '';
				}


				if ($node->isRoot()) {
					$rootNode = 'rootNode';
				}
				?>
				<?php if ($node->lev < $level) { ?>
					<?php for ($i = $node->lev; $i < $level; $i++) {
						echo '</ul>';
					} ?>
				<?php } ?>

			<li class="node nodeTree-node <?php echo $hasChildren.' '.$rootNode; ?>">
				<div class="radio-selectors">
					<!--<span class="node-icon"></span>-->
					<?php if ($node->isRoot()) { ?>
						<span class="node-icon"></span>
					<?php } elseif (!$node->isLeaf()) { ?>
						<span class="open-close-link"></span>
					<?php } ?>
					<span id="courseEnroll-orgchart_<?php echo $node->idOrg; ?>">
						<?php
						echo CHtml::radioButton('courseEnroll-orgchart['.$node->idOrg.']', true, array(
							'id' => 'courseEnroll-orgchart-radiobuttons_'.$node->idOrg.'_0',
							'value' => 0,
							'class' => 'select-node-no',
						));
						if ($node->lev > 1) {
							echo CHtml::radioButton('courseEnroll-orgchart['.$node->idOrg.']', false, array(
								'id' => 'courseEnroll-orgchart-radiobuttons_'.$node->idOrg.'_1',
								'value' => 1,
								'class' => 'select-node-yes',
							));
						}
						echo CHtml::radioButton('courseEnroll-orgchart['.$node->idOrg.']', false, array(
							'id' => 'courseEnroll-orgchart-radiobuttons_'.$node->idOrg.'_2',
							'value' => 2,
							'class' => 'select-node-descendants',
						));
						?>
					</span>

					<?php echo CHtml::label($node->coreOrgChart->translation, 'courseEnroll-orgchart_'.$node->idOrg, array('class' => 'label radioButtonTrigger')); ?>
				</div>
				<?php if (!empty($hasChildren)) { ?>
				<ul class="nodeUl">
			<?php } ?>

				<?php if (empty($hasChildren)) { ?>
					</li>
				<?php } ?>

				<?php $level = $node->lev; ?>
			<?php } ?>
		</ul><!-- .node-tree -->
	<?php } ?>

</div>
<script type="text/javascript">
(function ($) {
        $(function () {
			OrgChartTree();
			 $('#select-user-form .courseEnroll-page-orgchart').jScrollPane({
                autoReinitialise: true
            });
        });
})(jQuery);
</script>
