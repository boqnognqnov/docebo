<div class="main-section group-management">
	<div class="filters-wrapper">
		<div class="filters clearfix">
			<div class="single-assign-info">
				<?= Yii::t('gamification', 'Manually issue one <strong>"{badge_name}"</strong> badge to <strong>{username}</strong>', array('{badge_name}' => $model_t->name, '{username}' => $model_u->getFullName())); ?>
			</div>
			<div class="input-wrapper" style="margin-top:-10px;">
				<?= CHtml::button(Yii::t('gamification', 'Issue'), array('class' => 'btn confirm-btn', 'id' => 'single_issue_button')); ?>
			</div>
		</div>
	</div>
</div>

<?php $dataProvider = $gamificationAssignedBadges->dataProviderSingle(); ?>
<div style="margin-bottom: 20px;"><?= Yii::t('gamification', '<strong>{badge_name}</strong> badge has been issued {badge_count} time(s) to <strong>{username}</strong>.', array(
       '{badge_name}' => $model_t->name,
       '{badge_count}' => $dataProvider->getTotalItemCount(),
       '{username}' => $model_u->getFullName()
    ))?></div>
<div class="bottom-section clearfix">
	<div id="grid-wrapper">
		<?php
			$_columns = array(
				array(
					'header' => Yii::t('gamification', 'Issued on'),
					'name' => 'icon',
					'type' => 'raw',
					'value' => '$data->renderBadgeDate();',
				),
				array(
					'header' => Yii::t('standard', '_EVENT'),
					'name' => 'name',
					'type' => 'raw',
					'value' => '$data->renderBadgeEvent();',
				),
				array(
					'header' => '',
					'name' => 'delete',
					'type' => 'raw',
					'value' => 'CHtml::link("", "?r=GamificationApp/GamificationApp/deleteAssociationSingle&id_badge=".$data->id_badge."&id_user=".$data->id_user."&issued_on=".str_replace(" ", "_", $data->issued_on), array(
						"class" => "open-dialog delete-action",
						"data-dialog-class" => "delete-node",
						"rel" => "badge-modal-assign-deletion",
						"title" => "<span></span>".Yii::t("standard", "_UNASSIGN")
					));',
					'htmlOptions' => array('class' => 'button-column-single')
				)
			);

			$gridOptions = array(
				'id' => 'badges-grid-'.$model_u->idst,
				'htmlOptions' => array('class' => 'grid-view clearfix'),
				'dataProvider' => $dataProvider,
                'cssFile' => false,
				'template' => '{items}{summary}{pager}',
				'summaryText' => Yii::t('standard', '_TOTAL'),
				'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
				'pager' => array(
					'class' => 'ext.local.pagers.DoceboLinkPager'
				),
                'beforeAjaxUpdate' => 'function(id, options) {
                    options.type = "POST";
                    if (typeof options.data == "undefined") {
                        options.data = {};
                    }
                    options.data.contentType = "html";
                }',
				'ajaxUpdate' => 'badges-grid-'.$model_u->idst.'-all-items',
				'columns' => $_columns,
				'afterAjaxUpdate' => 'function(id, data) {dialog2Set(id, data);}'
			);

			$this->widget('zii.widgets.grid.CGridView', $gridOptions);
		?>
	</div>
</div>

<input type="hidden" id="current_modal_id" value="" />
<script type="text/javascript">
	$.fn.updateBadgeAssignList = function(data) {
		$('#badges-grid-<?= $model_u->idst; ?>').yiiGridView('update');
		$.fn.yiiListView.update('badge-assign-list');
		$('.modal.in').modal('hide');
	}

	$(function() {
		dialog2Set();
	});

	$('#single_issue_button').click(function(e){
		$.ajax({
			type: "POST",
			url: "./index.php?r=GamificationApp/GamificationApp/assignSingle",
			async: false,
			data: {id_badge: <?= $model->id_badge?>, id_user: <?= $model_u->idst; ?>,confirm: 1},
			success: function(result) {
				$('#badges-grid-<?= $model_u->idst; ?>').yiiGridView('update');
			}
		});
	})

	function dialog2Set(id, data)
	{
		var empty = true;

		$("a.open-dialog").each(function() {
			if(empty)
				empty = false;
			var a = $(this).removeClass("open-dialog");

			var id = a.attr("rel");
			var content = a.attr("href");

			var options = {
				modal: true
			};

			var element;

			if(id)
			{
				var e = $("#" + id);
				if (e.length)
					element = e;
			}

			if(!element)
				if(id)
					options.id = id;
			if(a.attr("data-dialog-class"))
				options.modalClass = a.attr("data-dialog-class");

			if(a.attr("title"))
				options.title = a.attr("title");

			$.each($.fn.dialog2.defaults, function(key, value)
			{
				if(a.attr(key))
					options[key] = a.attr(key) == "true";
			});

			if(content && content != "#")
			{
				options.content = content;

				a.click(function(event)
				{
					event.preventDefault();
					var modal_id = $('.modal.in')[0].id;
					$('#current_modal_id').val(modal_id);
					$('#' + modal_id).modal('hide');
					$(element || "<div></div>").dialog2(options);
				});
			}
			else
			{
				options.removeOnClose = true;
				options.autoOpen = false;
				options.closeOnEscape = true;
				options.closeOnOverlayClick = true;

				element = element || "<div></div>";

				// Pre initialize dialog
				$(element).dialog2(options);

				a.attr("href", "#").click(function(event)
				{
					event.preventDefault();
					var modal_id = $('.modal.in')[0].id;
					$('#current_modal_id').val(modal_id);
					$('#' + modal_id).modal('hide');
					$(element).dialog2("open");
				});
			}
		});

		if(empty)
			$.fn.yiiListView.update('badge-assign-list');
	}
</script>