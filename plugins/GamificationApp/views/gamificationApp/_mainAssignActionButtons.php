<div class="main-actions course-enrollments-actions clearfix">
	<h3 class="title-bold back-button">
		<?= CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('GamificationApp/GamificationApp/index')); ?>
		<span><?= Yii::t('gamification', 'Assign user to badge manually'); ?></span>
	</h3>
	<ul class="clearfix">
		<li data-bootstro-id="bootstroEnrollUsers">
			<div>

				<?php
					$title 	= '<span></span>'.Yii::t('standard', '_ASSIGN_USERS'); 
					$url 	= Docebo::createLmsUrl('usersSelector/axOpen', array(
								'type' 	=> UsersSelector::TYPE_GAMIFICATION_ASSIGN_BADGES,
								'idBadge' 	=> $id_badge,	
							));
					
					echo CHtml::link($title, $url, array(
						'class' => 'open-dialog group-select-users',
						'rel' 	=> 'users-selector',	
						'alt' 	=> Yii::t('helper', 'Assign badge - gamification'),
						'data-dialog-title' => Yii::t('standard', '_ASSIGN_USERS'),
						'title'	=> Yii::t('standard', '_ASSIGN_USERS')
					)); 
				?>
				
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4></h4>
			<p></p>
		</div>
	</div>
</div>


<script>
	/**
	 * The name of this function is hardcoded in UsersSelector JS as a callback upon successfully closing the dialog
	 *  Use in on your discretion to update current UI
	 */
	function usersSelectorSuccessCallback(data) {
		$.fn.yiiListView.update('badge-assign-list');
	}
			
</script>
