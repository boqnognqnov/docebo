<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 27.11.2015 г.
 * Time: 10:03
 */

$this->breadcrumbs = array(
    Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
    Yii::t('gamification', 'Leaderboards')
);
?>

<div class="main-actions clearfix">
    <h3 class="title-bold"><?php echo Yii::t('gamification', 'Leaderboards'); ?></h3>
    <ul class="clearfix" data-bootstro-id="bootstroNewCourse">
        <li id="create_leaderboard">
            <div>
                <a id="leaderboar-new-dialog" href="<?= Docebo::createAdminUrl('GamificationApp/LeaderboardWizard/wizard') ?>" class="open-dialog additional-fields"  data-dialog-id="leaderboard-dialog"  data-dialog-title="<?= Yii::t('gamification', 'New Leaderboard') ?>" alt="<?= Yii::t('helper', 'New leaderboard - gamification'); ?>">
                    <i class="fa fa-star fa-2x"></i>
                    <br><br><?= Yii::t('gamification', 'New Leaderboard'); ?>
                </a>
            </div>
        </li>
    </ul>
    <div class="info">
        <div>
            <h4></h4>
            <p></p>
        </div>
    </div>
</div>

<div class="main-section group-management">
    <h3 class="title-bold"><?php echo Yii::t('gamification', 'Leaderboards'); ?></h3>
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'leaderboars-form',
        'htmlOptions' => array(
            'class' => 'ajax-grid-form',
            'data-grid' => '#leaderboards-grid'
        )
    ));
    ?>
    <div class="filters-wrapper">
        <div class="filters clearfix">
            <div class="input-wrapper">
                <?php echo CHtml::textField('search_text', '', array(
                    'id' => 'advanced-search-badge',
                    'class' => 'typeahead',
                    'autocomplete' => 'off',
                    'data-url' => Docebo::createAdminUrl('GamificationApp/LeaderboardWizard/autocomplete'),
                    'placeholder' => Yii::t('standard', '_SEARCH'),
                )); ?>
                <span class="search-icon"></span>
            </div>
        </div>
    </div>
</div>
<?= CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<div class="bottom-section clearfix">
    <div id="grid-wrapper">
        <?php
        $_columns = array(
            array(
                'header' => Yii::t('standard', '_NAME'),
                'name' => 'name',
                'type' => 'raw',
                'value' => '$data->renderBoardName();',
                'htmlOptions' => array(
                    'style' => 'width: 60%;'
                )
            ),
            array(
                'header' => Yii::t('standard', 'Filter'),
                'name' => 'filter',
                'type' => 'raw',
                'value' => '$data->renderFilter();',
            ),
            array(
                'header' => '',
                'name' => 'active',
                'type' => 'raw',
                'value' => '$data->renderActivate();',
            ),
            array(
                'header' => '',
                'name' => 'edit',
                'type' => 'raw',
                'value' => '$data->renderEdit();',
            ),
            array(
                'header' => '',
                'name' => 'view',
                'type' => 'raw',
                'value' => '$data->renderView();',
            ),
            array(
                'header' => '',
                'name' => 'delete',
                'type' => 'raw',
                'value' => '$data->renderDelete();',
            ),
        );

        $gridOptions = array(
            'id' => 'leaderboards-grid',
            'htmlOptions' => array('class' => 'grid-view clearfix'),
            'dataProvider' => GamificationLeaderboard::model()->dataProvider(),
            'template' => '{items}{summary}{pager}',
            'summaryText' => Yii::t('standard', '_TOTAL'),
            'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
            'pager' => array(
                'class' => 'ext.local.pagers.DoceboLinkPager',
            ),
            "afterAjaxUpdate" => "function(id, data) { $(document).controls(); }",
            'ajaxUpdate' => 'all-items',
            'columns' => $_columns
        );

        $this->widget('zii.widgets.grid.CGridView', $gridOptions);
        ?>
    </div>
</div>

<script type="text/javascript">

    // Create Wizard object (themes/spt/js/jwizard.js)
    var jwizard = new jwizardClass({
        dialogId		: 'leaderboard-dialog',
        dispatcherUrl	: <?= json_encode(Docebo::createAdminUrl('GamificationApp/LeaderboardWizard/wizard')) ?>,
        alwaysPostGlobal: true,
        debug			: false
    });

    $(function(){
        $(document).off('userselector.closed.success', '.modal.users-selector')
            .on('userselector.closed.success', '.modal.users-selector', function(){
                $.fn.yiiGridView.update('leaderboards-grid', {});
            });

        $(document).on("dialog2.content-update", ".leaderboard-wizard-step-finish, .leaderboard-edit", function () {
            var autoClose = $(this).find('.auto-close.success');
            if(autoClose.length>0){
                $.fn.yiiGridView.update('leaderboards-grid', {});
                var resetDialog = autoClose.data('reset-dialog');
                if(resetDialog){
                    $('#leaderboar-new-dialog').click();
                }
                jwizard.resetFormData();
            }
        });

        $(document).on("dialog2.content-update", ".delete-leaderboar", function () {
            var autoClose = $(this).find('.auto-close');
            if(autoClose.length > 0){
                $.fn.yiiGridView.update('leaderboards-grid', {});
            }
        });

        $(document).on('click', '.activate-leaderboard', function(){
            var leaderboardId = $(this).data('leaderboard-id');
            $.post('<?=Docebo::createAdminUrl('//GamificationApp/LeaderboardWizard/switchLeaderboard') ?>', {id: leaderboardId})
                .done(function(){
                    $.fn.yiiGridView.update('leaderboards-grid', {});
                });
        })
    });

</script>