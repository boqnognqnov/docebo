<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 21.12.2015 г.
 * Time: 10:54
 */
?>

<h4><?php echo Yii::t('gamification', 'My Badges and Points'); ?></h4>

<div class="row-fluid">
    <div class="span3">
        <span class="gamification-sprite star-square-small-black"></span>
        <?= Yii::t('gamification', 'My Badges') . ': ' . CHtml::tag('strong', array(), intval($countBadges)) ?>
    </div>
    <div class="span3">
        <span class="gamification-sprite star-big"></span>
        <?= Yii::t('gamification', 'My Points') . ': ' . CHtml::tag('strong', array(), intval($countPoints)) ?>
    </div>
</div>


<?php if (count($allBadges)) : ?>
    <?php foreach ($allBadges as $badge) : ?>
        <?php
        $counter = $badge instanceof GamificationBadge ? $badge->badgeCounter : $badge->gamificationBadge->badgeCounter;
        $counterScore = $badge instanceof GamificationBadge ? $badge->badgeCounterScore : $badge->gamificationBadge->badgeCounterScore;
        $score = $badge instanceof GamificationAssignedBadges ? $badge->score : $counterScore;
        $pkString = $badge instanceof GamificationAssignedBadges ? $badge->gamificationBadge->pkToString() : $badge->pkToString();
        ?>
        <?php $_badgeId = $pkString; ?>
        <?php if (!isset($_renderedBadges[$_badgeId])) : ?>
            <div class="badge-spotlight <?= (empty($_renderedBadges) ? ($userHasBadges ? '' : 'hide') : 'hide') ?>" id="badge-spotlight-<?= $_badgeId ?>">
                <div class="well">
                    <?= $badge instanceof GamificationAssignedBadges ? $badge->gamificationBadge->renderBadge() : $badge->renderBadge() ?>
                    <div class="inner">
                        <h4 class="badge-spotlight-title">
                            <span class="pull-right"><?= (intval($counter) > 0 ? intval($counterScore) : intval($score)) ?> <i class="gamification-sprite star-big"></i></span>
                            <?= $badge instanceof GamificationAssignedBadges ? (isset($badge->gamificationBadge->badgeTranslation) ? $badge->gamificationBadge->badgeTranslation->name : $badge->gamificationBadge->defaultTranslation->name) : (isset($badge->badgeTranslation) ? $badge->badgeTranslation->name : $badge->defaultTranslation->name) ?>
                        </h4>
                        <div class="hr"></div>
                        <?php if ($showNotAwarded): ?>
                            <div class="row-fluid">
                                <div class="span9">
                                    <div class="badge-spotlight-description">
                                        <?= $badge instanceof GamificationAssignedBadges ? (isset($badge->gamificationBadge->badgeTranslation) ? $badge->gamificationBadge->badgeTranslation->description : $badge->gamificationBadge->defaultTranslation->description) : (isset($badge->badgeTranslation) ? $badge->badgeTranslation->description : $badge->defaultTranslation->description) ?>
                                    </div>
                                </div>
                                <div class="span3">
                                    <?php if ($badge instanceof GamificationBadge): ?>
                                        <div class="not-yet-awarded">
                                            <?= Yii::t('gamification', 'Not Yet Awarded') ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php else: ?>
                            <div class="badge-spotlight-description">
                                <?= $badge instanceof GamificationAssignedBadges ? (isset($badge->gamificationBadge->badgeTranslation) ? $badge->gamificationBadge->badgeTranslation->description : $badge->gamificationBadge->defaultTranslation->description) : (isset($badge->badgeTranslation) ? $badge->badgeTranslation->description : $badge->defaultTranslation->description) ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php $_renderedBadges[$_badgeId] = true; ?>
        <?php endif; ?>
    <?php endforeach; ?>

    <?php if (!$showNotAwarded): ?>
        <div class="badge-list">
            <?php
            foreach ($badges as $badge) {
                echo CHtml::tag('div', array(
                    'data-id' => $badge->gamificationBadge->pkToString()
                ), $badge->gamificationBadge->renderBadge());
            }
            ?>
        </div>
    <?php else: ?>
        <?php
        $this->widget('common.widgets.TabbedContent', array('content' => array(
            array(
                'header' => Yii::t('standard', 'All'),
                'content' => $this->renderPartial('_all_badges', array(
                    'badges' => $badges,
                    'notAwarded' => $notAwarded
                ), true),
                'active' => true
            ),
            array(
                'header' => Yii::t('gamification', 'Awarded'),
                'content' => $this->renderPartial('_awarded_badges', array(
                    'badges' => $badges
                ), true)
            ),
            array(
                'header' => Yii::t('gamification', 'Not awarded'),
                'content' => $this->renderPartial('_not_awarded', array(
                    'badges' => $notAwarded
                ), true)
            )
        ), 'containerClass' => 'tabbed-bages'));
        ?>
    <?php endif; ?>
<?php endif; ?>

<script type="text/javascript">
    $(document).ready(function(){
        var $badges = $('.badge-list .preview_badge');
        $badges.click(function (e) {
            e.preventDefault();
            var id = $(this).parent().data('id');
            $('.badge-spotlight').addClass('hide').filter('#badge-spotlight-' + id).removeClass('hide');
            return false;
        });
    });
</script>
