<div class="item  <?= (($index % 2) ? 'odd' : 'even'); ?>">
	<div class="checkbox-column">
		<?= '<input id="badge-assign-list-checkboxes_'. $data->id_user .'" type="checkbox" name="gbadge-assign-list-checkboxes[]" value="'.$data->id_user.'" '.(($data->inSessionList('selectedItems', $data->id_user))?'checked="checked"':'').'>';
		?>
	</div>
	<div class="group-member-username"><?= $data->coreUser->getFullName() . ' (' . (Yii::app()->user->getRelativeUsername($data->coreUser->userid)) . ')'; ?></div>
	<div class="delete-button gamification-info">
		<?php
			$this->renderPartial('//common/_modal', array(
				'config' => array(
					'class' => 'info-node course-enrollment',
					'modalTitle' => Yii::t('gamification', 'Issued badge(s)'),
					'linkTitle' => Yii::t('gamification', 'Issued badge(s)'),
					'url' => 'GamificationApp/GamificationApp/badgeAssignDetails&id_user='.$data->id_user.'&id_badge='.$data->id_badge,
					'buttons' => array(
						array(
							'type' => 'cancel',
							'title' => Yii::t('standard', '_CANCEL'),
						),
					)
				),
			));
		?>
	</div>
	<div class="delete-button">
		<?php
			$this->renderPartial('//common/_modal', array(
				'config' => array(
					'modalId' => 'user_details_modal_'.$data->id_user,
					'class' => 'delete-node',
					'modalTitle' => Yii::t('standard', '_UNASSIGN'),
					'linkTitle' => Yii::t('standard', '_DEL'),
					'url' => 'GamificationApp/GamificationApp/deleteAssignedBadge&id_user='.$data->id_user.'&id_badge='.$data->id_badge,
					'buttons' => array(
						array(
							'type' => 'submit',
							'title' => Yii::t('standard', '_CONFIRM'),
						),
						array(
							'type' => 'cancel',
							'title' => Yii::t('standard', '_CANCEL'),
						),
					),
					'afterLoadingContent' => 'hideConfirmButton();',
					'afterSubmit' => 'updateBadgeAssignList',
				),
			));
		?>
	</div>
</div>