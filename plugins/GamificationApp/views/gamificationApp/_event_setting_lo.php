<?php
    /** @var $selected_learning_object LearningOrganization */
    /** @var $learning_objects array */
?>
<div id="learning-object-selector">
	<div id="course-selector">
        <label><?= Yii::t('gamification', 'Choose course')?></label>
        <input type="text" name="course_name" id="selected-course" value="<?= $selected_learning_object->course->name ?>"
               data-url="<?= Docebo::createAppUrl('GamificationApp/GamificationApp/courseAutocomplete') ?>" class="typeahead" autocomplete="off"
               placeholder="<?php echo Yii::t('gamification', 'Type here the name of the course'); ?>"/>
	</div>
    <div id="lo-selector">
        <label><?= Yii::t('gamification', 'Choose training material')?></label>
        <select id="learning-objects" name="params[id_organization]">
            <?php foreach($learning_objects as $lo): ?>
            <option value="<?= $lo->idOrg ?>" <?php if($lo->idOrg == $selected_learning_object->idOrg): ?>selected="selected"<?php endif; ?>><?= $lo->title ?></option>
            <?php endforeach; ?>
        </select>
        <div class="errorMessage"></div>
    </div>
    <div id="test-score" style="display: <?= ($handler->isObjectWithScore($selected_learning_object) ? 'block' : 'none') ?>;">
        <div class="row">
            <div class="span12 score">
                <input type="radio" name="score" class="score" id="min_score" <?= ($params['min_score'] > 0) ? 'checked="checked"' : '' ?>/>
                <label style="display: inline-block" for="min_score"><?= Yii::t('gamification', 'Assign badge only if score is at least')?></label>
                <input style="display: inline-block; width: 60px;" type="number" min="0" name="params[min_score]" class="<?= ($params['min_score']) ? '' : 'hidden'?>" value="<?= isset($params['min_score']) ? $params['min_score'] : '' ?>" />
            </div>
        </div>
        <div class="row">
            <div class="span12 score">
                <input type="radio" name="score" class="score" id="under_score" <?= ($params['under_score'] > 0) ? 'checked="checked"' : '' ?>/>
                <label style="display: inline-block" for="under_score"><?= Yii::t('gamification', 'Assign badge only if score is under')?></label>
                <input style="display: inline-block; width: 60px;" type="number" min="0" name="params[under_score]" class="<?= ($params['under_score']) ? '' : 'hidden'?>" value="<?= isset($params['under_score']) ? $params['under_score'] : '' ?>" />
            </div>
        </div>
        <div class="errorMessage"></div>
    </div>
</div>
<script type="text/javascript">
$(function() {
    var objects = <?= $handler->getObjectsAsJson($learning_objects) ?>;

    $(".score").styler();

    applyTypeahead($('.typeahead'));

    // Intercept change event on the course input
    $("#selected-course").keypress (function(event){
        $('#learning-objects').html('');
    });

    $('#learning-objects').change(function(event) {
        var idLo = $(this).val();
        if(objects[idLo] == true) {
            $('#test-score').show();
            $('#min_score').prop('disabled','');
        } else {
            $('#test-score').hide();
            $('#min_score').prop('disabled','disabled');
        }
    });

    $("#selected-course").change(function(event){
        // Update the learning objects combo
        $.ajax({
            type: "POST",
            url: "./index.php?r=GamificationApp/GamificationApp/getLearningObjects",
            data: {course_name:$(this).val()},
            beforeSend: function(xhr) {
                $('#learning-objects').html('');
            },
            success: function(result) {
                if(result.list)
                    objects = result.list;
                $('#learning-objects').html(result.html);

                $.each(objects, function(key, value) {
                    if(objects[key] == true) {
                        $('#test-score').show();
                        $('#min_score').prop('disabled','');
                    } else {
                        $('#test-score').hide();
                        $('#min_score').prop('disabled','disabled');
                    }
                    return false;
                });
            }
        }).always(function(event){
            $('#learning-objects').next('.errorMessage').html('');
        });
    });

    $('#badge-create-form').submit(function(e){
		if($('#association_mode_0:checked').length == 0) {
			if($('#learning-object-selector').length == 0)
				return true;

			if(!$("#learning-objects").val()) {
				$('#learning-objects').next('.errorMessage').html(<?=CJSON::encode(Yii::t('gamification', 'Please select one training material'))?>);
				e.preventDefault();
                $('input#save').removeClass('disabled');
			}
		}

        if ( $("#min_score").is(':checked') ) {
            if(!$('input[name="params[min_score]"]').val() || ($('input[name="params[min_score]"]').val() <= 0)) {
                $('#test-score').find('.errorMessage').html(<?=CJSON::encode(Yii::t('gamification', 'Please enter a number higher than') . ' ' . 0)?>);
                e.preventDefault();
                $('input#save').removeClass('disabled');
            }
        }

        if ( $("#under_score").is(':checked') ) {
            if(!$('input[name="params[under_score]"]').val() || ($('input[name="params[under_score]"]').val() <= 0)) {
                $('#test-score').find('.errorMessage').html(<?=CJSON::encode(Yii::t('gamification', 'Please enter a number higher than') . ' ' . 0)?>);
                e.preventDefault();
                $('input#save').removeClass('disabled');
            }
        }
    });

    $("div.score, .jq-radio.score, [name='params[under_score]'], [name='params[min_score]'], span#under_score-styler, span#min_score-styler").on('click', function(){
        $('#test-score').find('.errorMessage').html('');

        if ( $("#min_score").is(':checked') ) {
            $("[name='params[under_score]']").addClass('hidden');
            $("[name='params[min_score]']").removeClass('hidden');

        } else if ( $("#under_score").is(':checked') ) {
            $("[name='params[min_score]']").addClass('hidden');
            $("[name='params[under_score]']").removeClass('hidden');
        }
    });

    $("[name='params[under_score]']").on('blur', function(){
        if ( $(this).val() > 0 ) {
            $("[name='params[min_score]']").removeAttr('value');
        }
    });
    $("[name='params[min_score]']").on('blur', function(){
        if ( $(this).val() > 0 ) {
            $("[name='params[under_score]']").removeAttr('value');
        }
    });

});
</script>