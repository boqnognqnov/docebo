<?php
	$this->breadcrumbs = array(
		Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
		Yii::t('gamification', 'Badges')
	);
?>

<?php $this->renderPartial('_mainGamificationAction'); ?>

<div class="main-section group-management">
	<h3 class="title-bold"><?php echo Yii::t('gamification', 'Badges'); ?></h3>
	<?php
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'badges-form',
			'htmlOptions' => array(
				'class' => 'ajax-grid-form',
				'data-grid' => '#badges-grid'
			)
		));
	?>
	<div class="filters-wrapper">
		<div class="filters clearfix">
			<div class="input-wrapper">
				<?php echo CHtml::textField('GamificationBadgeTranslation[name]', '', array(
					'id' => 'advanced-search-badge',
					'class' => 'typeahead',
					'autocomplete' => 'off',
					'data-url' => Docebo::createAppUrl('lms:GamificationApp/GamificationApp/autocomplete'),
					'placeholder' => Yii::t('standard', '_SEARCH'),
				)); ?>
				<span class="search-icon"></span>
			</div>
		</div>
	</div>
</div>
<?= CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<div class="bottom-section clearfix">
	<div id="grid-wrapper">
		<?php
			$_columns = array(
				array(
					'header' => Yii::t('label', 'Icon'),
					'name' => 'icon',
					'type' => 'raw',
					'value' => '$data->renderBadgePopover();',
				),
				array(
					'header' => Yii::t('standard', '_NAME'),
					'name' => 'name',
					'type' => 'raw',
					'value' => '$data->renderBadgeName();',
				),
				array(
					'header' => Yii::t('test', '_TEST_SCORES'),
					'name' => 'point',
					'type' => 'raw',
					'value' => '$data->renderBadgePoints();',
				),
				array(
					'header' => Yii::t('standard', '_ASSIGN'),
					'name' => 'assign',
					'type' => 'raw',
					'value' => '$data->renderBadgeAssignment();',
				),
				array(
					'header' => '',
					'name' => 'edit',
					'type' => 'raw',
					'value' => 'CHtml::link("", Docebo::createAdminUrl("GamificationApp/GamificationApp/edit", array("id_badge"=>$data->id_badge)), array("class" => "edit-action"));',
					'htmlOptions' => array('class' => 'button-column-single')
				),
				array(
					'header' => '',
					'name' => 'delete',
					'type' => 'raw',
					'value' => 'CHtml::link("", "", array(
						"class" => "ajaxModal delete-action",
						"data-toggle" => "modal",
						"data-modal-class" => "delete-node",
						"data-modal-title" => "'.Yii::t('standard', '_DEL').'",
						"data-buttons" => \'[
							{"type": "submit", "title": "'.Yii::t('standard', '_CONFIRM').'"},
							{"type": "cancel", "title": "'.Yii::t('standard', '_CANCEL').'"}
						]\',
						"data-url" => "GamificationApp/GamificationApp/delete&id_badge=".$data->id_badge,
						"data-after-loading-content" => "hideConfirmButton();",
						"data-after-submit" => "updateBadgesContent",
						"rel" => "tooltip",
						"title" => Yii::t("standard", "_DEL")
					));',
					'htmlOptions' => array('class' => 'button-column-single')
				)
			);

			$gridOptions = array(
				'id' => 'badges-grid',
				'htmlOptions' => array('class' => 'grid-view clearfix'),
				'dataProvider' => GamificationBadge::model()->dataProvider(),
				'template' => '{items}{summary}{pager}',
				'summaryText' => Yii::t('standard', '_TOTAL'),
				'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
				'pager' => array(
					'class' => 'ext.local.pagers.DoceboLinkPager',
				),
				"afterAjaxUpdate" => "function(id, data) { installBadgesPopover(); }",
				'ajaxUpdate' => 'all-items',
				'columns' => $_columns
			);

			$this->widget('zii.widgets.grid.CGridView', $gridOptions);
		?>
	</div>
</div>

<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input, select').styler();
			replacePlaceholder();
            installBadgesPopover();
		});
	})(jQuery);

	$.fn.updateBadgesContent = function(data)
	{
		$('#badges-grid').yiiGridView('update');
		$('.modal.in').modal('hide');
	}

	function installBadgesPopover() {
		$('.popover_selector').each(function(index, element){
			$(element).popover({
				html: true,
				placement: 'bottom',
				trigger: 'hover'
			});
		});
	}
</script>