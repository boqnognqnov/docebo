<?php
    /* @var $this GamificationAppController */
    /* @var $countBadges int */
    /* @var $countPoints int */
    /* @var $badges GamificationAssignedBadges[] */
    /* @var $pointsChart array */
    /* @var $userChartPosition array */
    /* @var $allBadges array */
    /* @var $showNotAwarded boolean */
    /* @var $hasActiveLeaderboard boolean */
    /* @var $leaderboars array */
    /* @var $leaderboardChartData array */


    $this->breadcrumbs[] = Yii::t('gamification', 'My Badges and Points');
?>

<div class="gamification-mybadges-tabs tabbable">
    <ul class="nav nav-tabs advanced-sidebar">
        <li class="<?php echo $selectedTab == 'default' ? 'active hover-cancel' : ''; ?>">
            <a class="ajax-change <?php echo $selectedTab == 'default' ? 'active  hover-cancel' : ''; ?>" data-toggle="tab" data-target="#mybadges-tab-mybadges" href="#mybadges-tab-mybadges">
                <i class="gamification-sprite star-square-small-white when-active"></i>
                <i class="gamification-sprite star-square-small-black"></i>
                <div class="tab-label"><?php echo Yii::t('gamification', 'My Badges'); ?></div>
            </a>
        </li>
        <?php if ($hasActiveLeaderboard): ?>
                <li class="<?php echo $selectedTab == 'chart' ? 'active hover-cancel' : ''; ?>">
                    <a class="ajax-change <?php echo $selectedTab == 'chart' ? 'active  hover-cancel' : ''; ?>" data-toggle="tab" data-target="#mybadges-tab-chart" href="#mybadges-tab-chart">
                        <i class="gamification-sprite star-white when-active"></i>
                        <i class="gamification-sprite star-black"></i>
                        <div class="tab-label"><?php echo Yii::t('gamification', 'Top points chart'); ?></div>
                    </a>
                </li>
        <?php endif; ?>
        <li class="<?php echo $selectedTab == 'contests' ? 'active hover-cancel' : ''; ?>">
            <a class="ajax-change <?php echo $selectedTab == 'contests' ? 'active  hover-cancel' : ''; ?>" data-toggle="tab" data-target="#mybadges-tab-contests" href="#mybadges-tab-contests">
                <i class="gamification-sprite podium-small-white when-active"></i>
                <i class="gamification-sprite podium-small-black"></i>
                <div class="tab-label"><?php echo Yii::t('gamification', 'Contests'); ?></div>
            </a>
        </li>

        <?php if($enable_reward_shop == 'on' && is_array($userSets) && count($userSets) > 0) { ?>
        <li class="<?php echo $selectedTab == 'rewards' ? 'active hover-cancel' : ''; ?>">
            <a class="ajax-change <?php echo $selectedTab == 'rewards' ? 'active  hover-cancel' : ''; ?>" data-toggle="tab" data-target="#mybadges-tab-rewards" href="#mybadges-tab-rewards">
                <i class="fa fa-gift fa-lg gamification-sprite when-active" style="margin-left: 0px !important; background-image: none !important; text-align: center !important; font-size: 23px! important; margin-bottom: -2px !important;"></i>
                <i class="fa fa-gift fa-lg gamification-sprite" style="margin-left: 0px !important; background-image: none !important; text-align: center !important; font-size: 23px! important; margin-bottom: -2px !important;"></i>
                <div class="tab-label"><?php echo Yii::t('gamification', 'Rewards'); ?></div>
            </a>
        </li>
        <?php } ?>
    </ul>

    <div class="tab-content <?= (Lang::isRTL(Yii::app()->getLanguage())) ? 'rightToLeftCustom' : '' ?>">
        <div id="mybadges-tab-mybadges" class="tab-pane <?php echo $selectedTab == 'default' ? 'active' : ''; ?>">
            <?php
                if(!empty($html['default'])){
                    echo $html['default'];
                }
            ?>
        </div>
        <?php if ($hasActiveLeaderboard): ?>
                <div id="mybadges-tab-chart" class="tab-pane <?php echo $selectedTab == 'chart' ? 'active' : ''; ?>">
                    <?php
                        if(!empty($html['chart'])){
                            echo $html['chart'];
                        }
                    ?>
                </div>
            <?php endif; ?>
        <div id="mybadges-tab-contests" class="tab-pane <?php echo $selectedTab == 'contests' ? 'active' : ''; ?>">
            <?php
                if(!empty($html['contests'])){
                    echo $html['contests'];
                }
            ?>
        </div>
        <div id="mybadges-tab-rewards" class="tab-pane <?php echo $selectedTab == 'rewards' ? 'active' : ''; ?>">
            <?php
            // TODO
            if(!empty($html['rewards'])){
                echo $html['rewards'];
            }
            ?>
        </div>

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $('.gamification-mybadges-tabs a.ajax-change').each(function(){
            $(this).on('click', function(event){
                var target = $(this).attr('data-target');
                var targetElement = $('.gamification-mybadges-tabs ' + target);
                var html = targetElement.html();
                var tab = null;
                if(html.trim().length <= 0){
                    switch (target){
                        case '#mybadges-tab-mybadges':
                            tab = 'default'
                            break;
                        case '#mybadges-tab-chart':
                            tab = 'chart';
                            break;
                        case '#mybadges-tab-contests':
                            tab = 'contests';
                            break;
                        case '#mybadges-tab-rewards':
                            tab = 'rewards';
                            break;
                    }

                    targetElement.css('min-height', '200px');
                    targetElement.html('<div class="ajaxloader" style="display: block;"></div>');

                    $.get('index.php?r=GamificationApp/GamificationApp/MyBadges&tab=' + tab, function(result){
                        targetElement.html(result);
                    });

                    $(document).controls();
                }
                $('.gamification-mybadges-tabs .nav.nav-tabs.advanced-sidebar li').removeClass('hover-cancel');
                $('.gamification-mybadges-tabs .nav.nav-tabs.advanced-sidebar li').removeClass('active');
                $('.gamification-mybadges-tabs .nav.nav-tabs.advanced-sidebar a').removeClass('hover-cancel').removeClass('active');
                $(this).parent().addClass('hover-cancel');
                $(this).addClass('active hover-cancel');

            });
        });
        $('.gamification-mybadges-tabs .nav.nav-tabs.advanced-sidebar a').mouseout(function() {
            if (!$(this).hasClass('hover-cancel'))
                $(this).removeClass('active');
            if (!$(this).parent().hasClass('hover-cancel'))
                $(this).parent().removeClass('active');

        }).mouseenter(function() {
            if (!$(this).hasClass('active'))
                $(this).addClass('active');
            if (!$(this).parent().hasClass('active'))
                $(this).parent().addClass('active');

        });
    });
</script>

<style>
    .list-view-item div.listview-item div.details-header i {
        cursor: pointer;
    }
    .listview-item, .accordion {
        border-top: 1px solid #E4E6E5;
        border-left: none;
        border-right: none;
        min-height: 53px;
    }
    .list-view-item {
        border-bottom: 1px solid #E4E6E5;
    }
    .listview-item div:not(.user) {
        padding: 3px 0 !important;
        margin: 1.3em 0 !important;
    }
    .listview-item div.user {
        margin: 5px 0;
    }
    .hidden {
        display: none !important;
    }
    .list-view-item {
        margin-bottom: 10px;
    }
    .listview-container div{
        font-size: 12px;
        text-align: left;
        vertical-align: middle;
        color: rgb(51,51,51);
        text-transform: uppercase;
        font-weight: 600;
        text-decoration: none;
        margin-left: 0 !important;
        padding: 10px 0 !important;
    }
    .details:not(.hidden) {
        border-top: 1px solid #E4E6E5;
    }
    .listview-item div.user {
        margin-left:0 !important;
    }
    .listview-item div.user img {
        max-width: 42px;
        max-height: 42px;
        min-width: 42px;
        min-height: 42px;
        vertical-align: middle;
        border: 1px solid #ddd;
        padding: 1px;
        margin-right: 10px;
    }
    .accordion {
        padding-bottom: 50px;
    }
    #mybadges-tab-contests h4 {
        margin-bottom: 0;
    }
    /*ul.advanced-sidebar > li.active, ul.advanced-sidebar > li:hover {*/
        /*!*background-color: #55AB54 !important;*!*/
    /*}*/
    .gamification-mybadges-tabs .nav.nav-tabs.advanced-sidebar li{
        cursor: pointer !important;
    }
    .gamification-mybadges-tabs .nav.nav-tabs.advanced-sidebar a{
        cursor: pointer !important;
        overflow: hidden;
    }
    .gamification-mybadges-tabs .nav.nav-tabs.advanced-sidebar li:hover{
        color: #ffffff;
    }
    div.listview-item > div.period {
        font-size:12px;
        white-space:nowrap;
    }
    div.table-like-container {
        display: table !important;
        width: 100% !important;
        height: 54px !important;
    }
    div.table-like-cell {
        display: table-cell !important;
        vertical-align: middle !important;
        float: none !important;
    }
    div.table-like-cell:first-child {
        word-wrap: break-word;
    }
</style>
