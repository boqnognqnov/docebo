<?php
    $id_badge = $model->id_badge;
	$this->breadcrumbs = array(
		Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
		Yii::t('gamification', 'Badges') => Docebo::createAppUrl('admin:GamificationApp/GamificationApp/index'),
		($id_badge == 0 ? Yii::t('gamification', 'New badge') : Yii::t('gamification', 'Edit Badge'))
	);
?>

<div class="main-actions clearfix">
	<h3 class="title-bold back-button">
		<?= CHtml::link(Yii::t('standard', '_BACK'), $this->createUrl('GamificationApp/index')); ?>
		<?= ($id_badge == 0 ? Yii::t('gamification', 'New badge') : Yii::t('gamification', 'Edit Badge')); ?>
	</h3>
</div>

<?php $form = $this->beginWidget('CActiveForm',
	array(
		'id' => 'badge-create-form',
		'htmlOptions' => array('enctype' => 'multipart/form-data')
	)
); ?>

<?= CHtml::hiddenField('id_badge', $id_badge); ?>

<?= $this->renderPartial('_translationBlock', array(
	'langs' => $langs,
	'model' => $model
), true); ?>

<?= $this->renderPartial('_imageBlock', array(
	'default_image' => $default_image,
	'flag_image' => $flag_image,
	'default_icon' => $default_icon,
    'model' => $model
), true); ?>

<?= $this->renderPartial('_associationBlock', array(
	'events' => $events,
	'model' => $model
), true); ?>

<div class="right-buttons">
	<?= CHtml::submitButton(Yii::t('standard', '_SAVE'), array('id' => 'save', 'name' => 'save', 'class' => 'btn-docebo green big', 'style' => 'line-height: 16px')); ?>
    <a href="<?=$this->createUrl('/GamificationApp/GamificationApp/index');?>" class="btn-docebo black big"><?=Yii::t('standard', '_CANCEL');?></a>
</div>

<?= $this->renderPartial('_editJavascript', array(
	'id_badge' => $id_badge
), true); ?>

<?php $this->endWidget(); ?>