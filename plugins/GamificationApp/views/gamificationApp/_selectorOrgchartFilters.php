<div class="main-section">

	<div class="filters-wrapper">
		<div class="selections" data-container="courseEnroll-orgchart-table">
			<div class="input-wrapper-orgchart">
				<p><span class="select-node-yes"></span> <?php echo Yii::t('standard', '_YES')?><span class="select-node-no"></span> <?php echo Yii::t('standard', '_NO')?><span class="select-node-descendants"></span> <?php echo Yii::t('standard', '_INHERIT')?></p>
			</div>
			<div class="left-selections">
				<p><?php echo Yii::t('standard', 'You have selected'); ?> <strong><span id="courseEnroll-orgchart-table-selected-items-count">0</span> <?php echo Yii::t('standard', 'branches'); ?></strong>.</p>
				<a href="javascript:void(0);" class="deselect-all-radio" data-value="0"><?php echo Yii::t('standard', '_UNSELECT_ALL'); ?></a>
			</div>
		</div>
	</div>
</div>
