<div id="course-selector-settings">
	<div>
		<?= CHtml::radioButton('params[courses_association]', (!isset($params['courses_association']) || (isset($params['courses_association']) && $params['courses_association'] == 'all') ? true : false), array('id' => 'courses_association_0', 'class' => 'radio-course-selection', 'value' => 'all')); ?>
		<label for="courses_association_0" class="label-course-selection"><?= Yii::t('standard', '_ALL_COURSES'); ?></label>
		<?= CHtml::radioButton('params[courses_association]', (isset($params['courses_association']) && $params['courses_association'] == 'selection' ? true : false), array('id' => 'courses_association_1', 'class' => 'radio-course-selection', 'value' => 'selection')); ?>
		<label for="courses_association_1" class="label-course-selection"><?= Yii::t('report', '_SEL_COURSES'); ?></label>
	</div>
	<div class="clearfix"></div>
	<div id="course_selector" style="display: <?= (isset($params['courses_association']) && $params['courses_association'] == 'selection' ? 'block' : 'none'); ?>;">
		<select id="courses" name="params[courses]">
			<?php foreach($courses as $info) : ?>
				<option value="<?= $info['idCourse']; ?>" class="selected"><?= ($info['code'] != '' ? $info['code'].' - ' : '').$info['name']; ?></option>
			<?php endforeach; ?>
		</select>
        <div class="errorMessage"></div>
	</div>
    <div class="clearfix"></div>
    <div id="replies-limit" style="margin-top: 10px;">
        <?= Yii::t('gamification', 'Assign badge when the number of replies reaches')?>&nbsp;
        <input style="width: 60px;" id="limit" type="text" name="params[limit]" value="<?= isset($params['limit']) ? $params['limit'] : '' ?>" />
        <div class="errorMessage"></div>
    </div>
</div>
<script type="text/javascript">
	$('.radio-course-selection').styler();

	$('.radio-course-selection').change(function(e)
	{
        $('#course_selector').find('.errorMessage').html('');
        $('#replies-limit').find('.errorMessage').html('');
		if($('#courses_association_1').is(':checked')) {
            $('#course_selector').show();
        } else {
            $('#course_selector').hide();
            $('#courses').val('');
        }

	});

	$(document).ready(function(){
		$("#courses").fcbkcomplete({
			json_url: "./index.php?r=GamificationApp/GamificationApp/getCourseList",
			addontab: true,
			input_min_size: 0,
            width: '98.5%',
			cache: true,
			complete_text: '',
			maxitems: '<?php echo $totalCourses?>',
			filter_selected: true
		});

        function isNormalInteger(str) {
            var n = ~~Number(str);
            return String(n) === str && n > 0;
        }

        $('#badge-create-form').submit(function(e){
			if($('#association_mode_0:checked').length == 0) {
				if($('#course-selector-settings').length == 0)
					return true;

				if(($('input:radio[name="params[courses_association]"]:checked').val() == 'selection') && !$("#courses").val()) {
					$('#course_selector').find('.errorMessage').html(<?=CJSON::encode(Yii::t('gamification', 'Please select at least one course'))?>);
					e.preventDefault();
                    $('input#save').removeClass('disabled');
				} else {
                    $('#course_selector').find('.errorMessage').html('');
                }

                if(!$("#limit").val() || !isNormalInteger($("#limit").val())) {
                    $('#replies-limit').find('.errorMessage').html(<?=CJSON::encode(Yii::t('gamification', 'Please enter a number higher than') . ' ' . 0)?>);
                    e.preventDefault();
                    $('input#save').removeClass('disabled');
                } else {
                    $('#replies-limit').find('.errorMessage').html('');
                }
			}
        });
	});
</script>