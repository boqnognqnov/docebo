<?php $id_badge = $model->id_badge; ?>
<div class="new-course">
	<div class="courseSlider">
		<?php $form = $this->beginWidget('CActiveForm',
			array(
				'id' => 'badge-image-form',
				'htmlOptions' => array('enctype' => 'multipart/form-data'),
			)
		); ?>
		<div class="courseEnroll-tabs">
			<ul class="nav nav-tabs">
				<li class="<?php echo $selectedTab == 'default' ? 'active' : ''; ?>">
					<a data-target=".courseEnroll-page-users" href="#">
						<?php echo Yii::t('course', 'Available icons'); ?> (<?php echo $count['default']; ?>)
					</a>
				</li>
				<li class="<?php echo $selectedTab == 'user' ? 'active' : ''; ?>">
					<a data-target=".courseEnroll-page-groups" href="#">
						<?php echo Yii::t('course', 'Your icons collection'); ?> (<?php echo $count['user']; ?>)
					</a>
				</li>
			</ul>

			<div class="select-user-form-wrapper">
				<div class="upload-form-wrapper">
					<div class="upload-form">
                        <div class="fileUploadContent" style="display: <?php echo $selectedTab == 'user' ? 'block' : 'none';?>">
                            <table>
                                <tr>
                                    <td><label for="upload-btn"><?php echo Yii::t('gamification', 'Upload your icon'); ?></label></td>
                                    <td><?php echo CHtml::fileField('attachment', '', array(
                                            'class' => 'custom-image'
                                        )); ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="selectIconContent" style="display: <?php echo $selectedTab == 'default' ? 'block' : 'none';?>">
                            <?= Yii::t('gamification', 'Select an icon') ?>
                        </div>
					</div>
				</div>
				<div class="tab-content">
					<div class="courseEnroll-page-users tab-pane <?php echo $selectedTab == 'default' ? 'active' : ''; ?>">
						<div id="defaultSlider_<?php echo $uniqueId; ?>" class="carousel slide thumbnails-carousel">
							<!-- Carousel items -->
							<div class="carousel-inner">
								<?php if (!empty($defaultImages)) { ?>
									<?php foreach ($defaultImages as $key => $defaultThumbnail) { ?>
										<div class="item<?php echo ($key == 0) ? ' active' : ''; ?>">
											<?php foreach ($defaultThumbnail as $key => $imageUrl) {
												$html = CHtml::image($imageUrl);
												$html .= '<input type="radio" name="slider" value="' . $key . '" '. ($key == $model->icon ? 'checked="checked"' : '') .' />';
												echo '<div class="sub-item ' . ($key == $model->icon ? 'checked' : '') .'">' . $html . '</div>';
											}
											?>
										</div>
									<?php } ?>
								<?php } ?>
							</div>
							<!-- Carousel nav -->
							<a class="carousel-control left" href="#defaultSlider_<?php echo $uniqueId; ?>" data-slide="prev">&lsaquo;</a>
							<a class="carousel-control right" href="#defaultSlider_<?php echo $uniqueId; ?>" data-slide="next">&rsaquo;</a>
						</div>
					</div>
					<div class="courseEnroll-page-groups tab-pane <?php echo $selectedTab == 'user' ? 'active' : ''; ?>">
						<div id="userSlider_<?php echo $uniqueId; ?>" class="carousel slide thumbnails-carousel">
							<!-- Carousel items -->
							<div class="carousel-inner">
								<?php if (!empty($userImages)) { ?>
									<?php foreach ($userImages as $key => $userThumbnail) { ?>
										<div class="item<?php echo ($key == 0) ? ' active' : ''; ?>">
											<?php foreach ($userThumbnail as $key => $imageUrl) {
												$html = CHtml::image($imageUrl);
												$html .= '<input type="radio" name="slider" value="' . $key . '" '. ($key == $model->icon ? 'checked="checked"' : '') .' />';
												echo '<div class="sub-item ' . ($key == $model->icon ? 'checked' : '') .'">' . $html . '</div>';
											}
											?>
										</div>
									<?php } ?>
								<?php } ?>
							</div>
							<!-- Carousel nav -->
							<a class="carousel-control left" href="#userSlider_<?php echo $uniqueId; ?>" data-slide="prev">&lsaquo;</a>
							<a class="carousel-control right" href="#userSlider_<?php echo $uniqueId; ?>" data-slide="next">&rsaquo;</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php $this->endWidget(); ?>
	</div>
</div>
<script>
$(function(){

    // Hides or shows the file upload, depending on the selected tab
	$('.nav.nav-tabs li').bind('click', function (e) {
		if ($(e.target).attr('data-target') == '.courseEnroll-page-groups') {
			$('.fileUploadContent').show();
            $('.selectIconContent').hide();
        }
		else {
            $('.fileUploadContent').hide();
            $('.selectIconContent').show();
        }
	});

    // Handle file selected
    $('#attachment').change( function (e) {
        e.stopPropagation();

        var form = $(this).parents('form');
        var url = HTTP_HOST + 'index.php?r=GamificationApp/GamificationApp/uploadUserBadge';

        // Show file uploader
        if (!$(this).parents('.fileUploadContent').next().hasClass('contentUploadLoading'))
            $(this).parents('.fileUploadContent').after($('<div/>', {'class': 'contentUploadLoading'}));
        $(this).parents('.fileUploadContent').hide();
        showLoading('contentUploadLoading');

        // Prepare the ajax request
        var options = {
            url         : url,
            type        : 'post',
            dataType    : 'json',
            success     : function (responseText, statusText, xhr, $form) {
                reloadBadgeSliderContent();
            },
            complete    : function(jqXHR, textStatus) {
                hideLoading('contentUploadLoading');
                $('.fileUploadContent').show();
            }
        };

        // bind form using 'ajaxForm' and submit
        form.ajaxForm(options).submit().unbind('submit');
        return false;
    });

    /**
     * Submits the selector form and reloads it (in case of errors)
     */
    function reloadBadgeSliderContent() {
        $.post(HTTP_HOST + 'index.php?r=GamificationApp/GamificationApp/imageBlock', {
                'id_badge': '<?= $id_badge ?>',
                'selected_tab': 'user'
            }, function(ajaxData) {
                if (ajaxData.html) {
                    $('.modal.in').find('.courseSlider').html(ajaxData.html);
                    $('input, select').styler();
                }
            }
        );
    }
});
</script>