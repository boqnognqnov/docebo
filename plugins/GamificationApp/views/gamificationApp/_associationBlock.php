<div class="advanced-main advanced-main-badge">
	<div class="row odd has-border">
		<div class="row">
			<div class="setting-name">
				<?php echo Yii::t('gamification', 'Associate badge'); ?>
				<p class="description"><?php echo Yii::t('gamification', 'Decide if associate the badge to a specific action/event or manually'); ?></p>
			</div>
			<div class="values">
                <span id="association_mode">
                    <input value="manual" id="association_mode_0" <?php if($model->event_name == 'manual'): ?>checked="checked"<?php endif; ?> type="radio" name="association_mode" />
                        <label for="association_mode_0"><?= Yii::t('gamification', 'Manually') ?></label>
                        <div class="sublabel"><?= Yii::t('gamification', 'I will associate this badge manually, from the badges list area') ?></div><br/>
                    <input value="automatic" id="association_mode_1" <?php if($model->event_name !== 'manual'): ?>checked="checked"<?php endif; ?> type="radio" name="association_mode" />
                        <label for="association_mode_1"><?= Yii::t('gamification', 'Automatically') ?></label>
                        <div class="sublabel"><?= Yii::t('gamification', 'Associate a badge to an event, such as a user action, a course/learning object completion, etc...') ?></div>
                </span>

                <div class="automatic-association<?= ($model->event_name == 'manual' ? '' : ' display-block'); ?>">
                    <label for="event"><?= Yii::t('gamification', 'Choose an <strong>event</strong> that must occur to get the badge')?></label>
					<div>
						<?php
							$events_list = array(
								GamificationAppModule::GROUP_LMS => array()
							);

							if(PluginManager::isPluginActive('Share7020App')){
								$events_list [GamificationAppModule::GROUP_APP7020] = array();
							}
							$events_details = '<div class="main-actions main-actions-event">';
							$first = true;
							foreach($events as $event_name => $details)
							{
								$events_list[$details['group']][$event_name] = $details['event_name'];
								$events_details .= '
									<div id="info_'.$event_name.'" class="info" style="display: '.((($model->event_name && ($event_name == $model->event_name)) || (!$model->event_name && $first)) ? 'block' : 'none').';">
										<div>
											<h4><span></span>'.$details['event_name'].'</h4>
											<p>'.$details['event_description'].'</p>
										</div>
									</div>
								';
								$first = false;
							}
							$events_details .= '</div>';
						?>

						<?= CHtml::dropDownList('event', $model->event_name, $events_list, array('class' => 'event_dropdown')); ?>
						<?= $events_details ?>
					</div>
					<div id="event_setting">
                        <?php /* EVENT SETTINGS FORM HERE (AJAX LOADED) */ ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>