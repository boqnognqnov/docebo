<script type='text/javascript'>
	$(function(){
		$.post('<?=Docebo::createLmsUrl('progress/run')?>', {
			'type': <?=Progress::JOBTYPE_DELETE_MULTI_ASSIGNED_BADGE?>,
			'users': '<?= $users ?>',
			'id_badge': '<?= $id_badge ?>'
		}, function(data){
			var dialogId = 'delete-multi-assigned-badge-progressive';

			$('<div/>').dialog2({
				autoOpen: true, // We will open the dialog later
				id: dialogId,
				title: '<?=Yii::t('standard', '_DEL_SELECTED')?>'
			});
			$('#'+dialogId).html(data);
		});
	})
</script>
