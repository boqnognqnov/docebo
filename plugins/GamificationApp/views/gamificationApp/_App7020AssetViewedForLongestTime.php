<div class="user-goal-settings row-fluid">
	<div class="span12">
		<?= Yii::t('gamification', "Assign when user's asset has been viewed at least")?>&nbsp;
		<input style="width: 28px; height:28px; text-align:center" id="limit-hours" type="text" name="params[hours]" value="<?= isset($params['hours']) ? $params['hours'] : '' ?>" />&nbsp;
		<?php echo 'hh' ?>
		<input style="width: 28px; height:28px; text-align:center" id="limit-minutes" type="text" name="params[minutes]" value="<?= isset($params['minutes']) ? $params['minutes'] : '' ?>" />&nbsp;
		<?php echo 'mm' ?>
	</div>
</div>