<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'multi_del_badge_form',
	)); ?>

	<p><?= Yii::t('gamification', 'Delete <strong>all "{badge}"</strong> badges issued to selected users', array('{badge}' => $model_t->name)); ?></p>

	<div class="clearfix">
		<?= $form->checkbox($model, 'confirm', array('id' => 'GamificationBadge_confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?= CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed', array($model_t->name)), 'GamificationBadge_confirm_'.$checkboxIdKey); ?>
		<?= $form->error($model, 'confirm'); ?>
		<?= CHtml::hiddenField('ids', $id_users); ?>
		<?= CHtml::hiddenField('id_badge', $id_badge); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>