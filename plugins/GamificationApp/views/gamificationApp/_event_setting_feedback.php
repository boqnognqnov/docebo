<div id="feedback-selector-settings">
    <div style="margin-bottom: 10px;"><?= Yii::t('gamification', 'Event target')?></div>
	<div>
    <?php foreach($targets as $index => $target): ?>
		<?= CHtml::checkBox('params[target][]', (!isset($params['target']) || in_array($target['value'],$params['target']) ? true : false), array('id' => 'target_'.$index, 'class' => 'feedback-selection feedback-target', 'value' => $target['value'])); ?>
		<label for="target_<?=$index?>" class="label-course-selection"><?= $target['label'] ?></label>
    <?php endforeach; ?>
	</div>
    <div class="clearfix"></div>
    <div style="margin: 20px 0px 10px 0px;"><?= Yii::t('gamification', 'Feedback values')?></div>
    <?php if($feedback_type == 'helpful'): ?>
    <span id="helpful" class="docebo-helpful">
        <a data-value="1" style="margin-left: 0px;" class="thumbs-up <?= in_array(1, $feedback_values) ? 'my-vote' : '' ?>" href="javascript:void(0);"><span class="count"><?= Yii::t('standard', '_YES') ?></span><span class="icon"></span></a>
        <a data-value="0" class="thumbs-down <?= in_array(0, $feedback_values) ? 'my-vote' : '' ?>" href="javascript:void(0);"><span class="count"><?= Yii::t('standard', '_NO') ?></span><span class="icon"></span></a>
        <input type="checkbox" name="params[feedback_values][]" id="feedback_value_0" style="display: none;" value="0" <?= in_array(0, $feedback_values) ? 'checked="checked"' : '' ?>/>
        <input type="checkbox" name="params[feedback_values][]" id="feedback_value_1" style="display: none;" value="1" <?= in_array(1, $feedback_values) ? 'checked="checked"' : '' ?>/>
    </span>
    <?php else: ?>
    <span class="smiley-type-rating rating-container">
        <span class="star-rating-control">
            <div class="rating-cancel"></div>
            <div class="animate"></div>
            <?php for($i=1; $i<=5; $i++): ?>
                <div class="star-rating <?= in_array($i, $feedback_values) ? 'star-rating-on' : ''?>" id="rating_<?= $i ?>">
                    <a data-value="<?= $i ?>" title="<?= $i ?>"><?= $i ?></a>
                    <input type="checkbox" name="params[feedback_values][]" id="feedback_value_<?= $i ?>" style="display: block;" value="<?= $i ?>" <?= in_array($i, $feedback_values) ? 'checked="checked"' : '' ?>/>
                </div>
            <?php endfor; ?>
        </span>
    </span>
    <?php endif; ?>
    <div class="clearfix"></div>
    <div style="margin: 20px 0px 10px 0px;"><?= Yii::t('standard', 'Badge assigned to')?></div>
    <div>
        <?= CHtml::radioButton('params[action]', (!isset($params['action']) || (isset($params['action']) && $params['action'] == 'give_feedback') ? true : false), array('id' => 'give_feedback', 'class' => 'feedback-selection receiver-selection', 'value' => 'give_feedback')); ?>
        <label for="give_feedback" class="label-course-selection"><?= Yii::t('gamification', 'the user giving the feedback') ?></label>
        <?= CHtml::radioButton('params[action]', (isset($params['action']) && $params['action'] == 'receive_feedback') ? true : false, array('id' => 'receive_feedback', 'class' => 'feedback-selection receiver-selection', 'value' => 'receive_feedback')); ?>
        <label for="receive_feedback" class="label-course-selection"><?= Yii::t('gamification', 'the user receiving the feedback') ?></label>
    </div>
    <div class="clearfix"></div>
    <div id="receiving-feedback-options" style="<?= (isset($params['action']) && $params['action'] == 'receive_feedback') ? '' : 'display: none' ?>">
        <div style="margin: 20px 0px 10px 0px;">
            <?= CHtml::checkBox('params[use_threashold]', (isset($params['use_threashold']) && $params['use_threashold'] == 1) ? true : false, array('id' => 'use_threashold', 'class' => 'feedback-selection ', 'value' => '1')); ?>
            <label for="use_threashold" class="label-course-selection"><?= Yii::t('gamification', 'Set up limit of received feedbacks') ?></label>
        </div>
        <div class="clearfix"></div>
        <div id="threashold-params" style="margin: 20px 0px 10px 0px; <?= (!isset($params['use_threashold']) && $params['use_threashold'] == 0) ? 'display: none;' : ''?>">
            <label for="threashold"><?= Yii::t('gamification', 'Assign badge only when user receives') ?></label>
            <input type="text" style="width: 60px;" id="threashold" name="params[threashold]" value="<?= $params['threashold'] ?>" <?= (!isset($params['use_threashold']) && $params['use_threashold'] == 0) ? 'disabled="disabled"' : ''?>/>
            <span style="margin-left: 10px;"><?= Yii::t('gamification', 'similar feedbacks') ?></span>
            <div class="errorMessage"></div>
            <div class="clearfix"></div>
            <div style="margin: 20px 0px 10px 0px;">
                <?= CHtml::radioButton('params[threashold_type]', (!isset($params['threashold_type']) || (isset($params['threashold_type']) && $params['threashold_type'] == 'same_item') ? true : false),
                    array('id' => 'same_item', 'class' => 'feedback-selection threashold-type', 'value' => 'same_item', 'disabled' => (!isset($params['use_threashold']) || $params['use_threashold'] == 0) ? 'disabled' : '')); ?>
                <label for="same_item" class="label-course-selection"><?= Yii::t('gamification', 'for the same item') ?></label>
                <?= CHtml::radioButton('params[threashold_type]', (isset($params['threashold_type']) && $params['threashold_type'] == 'multiple_items') ? true : false,
                    array('id' => 'multiple_items', 'class' => 'feedback-selection threashold-type', 'value' => 'multiple_items', 'disabled' => (!isset($params['use_threashold']) || $params['use_threashold'] == 0) ? 'disabled' : '')); ?>
                <label for="multiple_items" class="label-course-selection"><?= Yii::t('gamification', 'for multiple items') ?></label>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$('.feedback-selection').styler();

    $(document).ready(function(){

        $('.feedback-target').change(function(e) {
            if(($(this).prop('checked') == false) && ($('.feedback-target:checked').length == 0)) {
                $(this).prop('checked', true);
                $(this).trigger('refresh');
            }
        });

        $('.receiver-selection').change(function(e) {
            $("#use_threashold").prop('checked', '').trigger('refresh');
            $("#threashold").prop('disabled', 'disabled');
            $('#threashold-params').hide();
            $(".threashold-type").prop('disabled', 'disabled').trigger('refresh');

            if($(this).val() == 'receive_feedback')
                $('#receiving-feedback-options').show();
            else
                $('#receiving-feedback-options').hide();
        });

        $('#use_threashold').change(function(e) {
            if($(this).prop('checked') == true) {
                $("#threashold").prop('disabled', '');
                $('#threashold-params').show();
                $(".threashold-type").prop('disabled', '');
            } else {
                $("#threashold").prop('disabled', 'disabled');
                $('#threashold-params').hide();
                $(".threashold-type").prop('disabled', 'disabled');
            }

            $(".threashold-type").trigger('refresh');
        });

        <?php if($feedback_type == 'helpful'): ?>
        $("#helpful > a").click(function(e) {
            var value = $(this).data('value');
            if($(this).hasClass('my-vote') && ($("#helpful > a.my-vote").length > 1)) {
                $(this).removeClass('my-vote');
                $('#feedback_value_'+value).prop('checked', '');
            } else {
                $(this).addClass('my-vote');
                $('#feedback_value_'+value).prop('checked', 'checked');
            }
        });
        <?php else: ?>
        $(".star-rating > a").click(function(e) {
            var value = $(this).data('value');
            var div = $(this).parent();
            if(div.hasClass('star-rating-on') && ($(".star-rating.star-rating-on").length > 1)) {
                div.removeClass('star-rating-on');
                $('#feedback_value_'+value).prop('checked', '');
            } else {
                div.addClass('star-rating-on');
                $('#feedback_value_'+value).prop('checked', 'checked');
            }
        });
        <?php endif; ?>

        function isNormalInteger(str) {
            var n = ~~Number(str);
            return String(n) === str && n > 0;
        }

        $('#badge-create-form').submit(function(e){
            if($('#association_mode_0:checked').length == 0 && $('#feedback-selector-settings').length > 0) {
                if($('#use_threashold').is(':checked') && !isNormalInteger($("#threashold").val())) {
                    $('#threashold-params').find('.errorMessage').html(<?=CJSON::encode(Yii::t('gamification', 'Please enter a valid number for the threashold'))?>);
                    e.preventDefault();
                    $('input#save').removeClass('disabled');
                }
            }
        });
    });
</script>