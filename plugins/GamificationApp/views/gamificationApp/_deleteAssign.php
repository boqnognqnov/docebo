<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'badge_form',
	)); ?>

	<p><?= Yii::t('gamification', 'Delete <strong>all "{badge}"</strong> badges issued to <strong>{user}</strong>', array(
            '{badge}' => $model_t->name,
            '{user}' => Yii::app()->user->getRelativeUsername($model_u->userid))
    ); ?></p>

	<div class="clearfix">
		<?= $form->checkbox($model, 'confirm', array('id' => 'GamificationBadge_confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?= CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed', array($model_t->name)), 'GamificationBadge_confirm_'.$checkboxIdKey); ?>
		<?= $form->error($model, 'confirm'); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>