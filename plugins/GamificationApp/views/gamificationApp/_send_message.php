<?php
/**
 *
 * @var $this GamificationAppController
 * @var $gamificationUserRewards GamificationUserRewards
 */

$form = $this->beginWidget('CActiveForm', array(
	'id' => 'new-message-from',
	'htmlOptions' => array(
		'class' => 'ajax form-horizontal'
	)
));

/**
 * @var $form CActiveForm
 */

$uniqueId = substr(md5(uniqid(rand(), true)), 0, 10);

echo Yii::t('standard', '_USER') . ' : <strong>' . $gamificationUserRewards->userModel->getFullName() .'</strong>';
echo CHtml::hiddenField('name', $gamificationUserRewards->userModel->getFullName());

$requestStatus = $gamificationUserRewards->status;

echo "<br><br>";

// If there is some text to be prepopulated for the approved rewards
//$msgText = (($requestStatus == GamificationUserRewards::$STATUS_APPROVED) ? Yii::t('gamification', "Hello, I haven't received my reward yet") : '');
$msgText = '';

echo CHtml::label(Yii::t('htmlframechat', '_MSGTXT'), 'message');
echo CHtml::textArea('message', $msgText, array(
	'style' => 'width: 95%',
	'id' => 'message-textarea-'.$uniqueId,
	'class' => 'set-description'
));

echo CHtml::hiddenField('id', $gamificationUserRewards->id);
echo CHtml::hiddenField('userId', Yii::app()->user->idst);
echo CHtml::hiddenField('rewardId', $gamificationUserRewards->id_reward);
echo CHtml::hiddenField('sendByEmail', $gamificationUserRewards->userModel->email);
?>
	<div class="form-actions">
		<input type="submit" value="<?=Yii::t('standard', '_SEND')?>" name="save" class="btn btn-docebo big green confirm" />
		<input type="button" name="cancel" value="<?= Yii::t('standard', '_CANCEL') ?>" class="btn btn-docebo big black close-dialog" />
	</div>
<?php
$this->endWidget();
?>

<script type="text/javascript">
	(function ($) {
		$(function () {
			TinyMce.attach("textarea#message-textarea-<?= $uniqueId; ?>", 'standard_embed', {height: 200});
		});
	})(jQuery);
</script>
