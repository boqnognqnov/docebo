<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 21.12.2015 г.
 * Time: 10:54
 */
    
$randomString = str_shuffle('abcdefghijklmnopqstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
$randomStart = rand(0, strlen($randomString)-1);
$randomLength = strlen($randomString) - $randomStart;
$uniqueId = substr($randomString, $randomStart, $randomLength);
if(!$hideTitle) {
    $topKey = "Top {count} points chart";
    $topKey_ = "{count}";
} else {
    $topKey = "Top {number} points chart";
    $topKey_ = "{number}";
}

?>
<?php if(isset($dashboardCellModel->width) && $dashboardCellModel->width <= 4){ ?>
    <style type="text/css">
        .chart-user-name{
            clear: both;
            height: auto !important;
        }
        .gamification-points-chart .chart-user{
            top: -5px !important;
        }
    </style>
<?php } ?>
<div id="leaderboard-<?= $uniqueId ?>">
    <?php if(!$hideTitle): ?>
<h3><?= Yii::t('gamification', 'My Leaderboard') ?></h3>
<br>
    <?php endif; ?>
<?php if (count($leaderboards) > 1): ?>
    <div class="row-fluid leadeboard-selection">
        <div class="span2">
            <p><strong><?= Yii::t('gamification', 'Select leaderboard') ?></strong></p>
        </div>
        <div class="span10">
            <?=
            CHtml::dropDownList('select-leaderboard', 0, CHtml::listData($leaderboards, 'id', function($board) {
                return $board['name'];
            }))
            ?>
        </div>
    </div>
    <br>
<?php else: ?>
<?php endif; ?>
<?php
$first = key($leaderboardChartData);
?>

<?php foreach ($leaderboardChartData as $key => $chart): ?>

    <div class="leaderboard-chart-<?= $key ?>" style="<?= ($key === $first ? '' : 'display: none;') ?>">
        <?php if (!empty($chart['userPoints'])) : ?>

        <h4><?php echo Yii::t('gamification', 'My position'); ?></h4>
        <hr>
            <ul class="gamification-points-chart">
                <li style="list-style:none;">
                    <div id="personal-counter" class="chart-counter personal-counter"><?= $chart['userPoints']['position'] ?></div>
                    <div class="chart-avatar">
                        <?= $chart['userPoints']['user']->getAvatarImage() ?>
                    </div>
                    <div class="chart-user">
                        <div class="chart-user-points"><span class="gamification-sprite star-small"></span> <?= $chart['userPoints']['points'] ?></div>
                        <div class="chart-user-name"><?= $chart['userPoints']['user']->getFullName() ?></div>
                        <div class="chart-user-progress" style="width:<?= intval($chart['userPoints']['progress']) ?>%;"></div>
                    </div>
                </li>
            </ul>
        <?php endif; ?>
        <?php if (!empty($chart['points'])) : ?>
        <h4><?= Yii::t('gamification', $topKey, array($topKey_ => !empty($topTitleCount) ? $topTitleCount : 10)) ?></h4>
        <hr>
            <ul class="gamification-points-chart">
                <?php foreach ($chart['points'] as $key => $row) : ?>
                    <?php
                    switch ($key) {
                        case 0:
                            $_placeIcon = '<span class="gamification-sprite cup-gold"></span>';
                            break;
                        case 1:
                            $_placeIcon = '<span class="gamification-sprite cup-silver"></span>';
                            break;
                        case 2:
                            $_placeIcon = '<span class="gamification-sprite cup-bronze"></span>';
                            break;
                        default:
                            $_placeIcon = '';
                            break;
                    }
                    ?>
                    <li style="list-style:none;">
                        <div class="chart-counter"><?= $key + 1 ?></div>
                        <div class="chart-avatar">
                            <?= $row['user']->getAvatarImage() ?>
                        </div>
                        <div class="chart-user">
                            <div class="chart-user-points"><span class="gamification-sprite star-small"></span> <?= $row['points'] ?></div>
                            <?php echo $_placeIcon; ?>
                            <div class="chart-user-name"><?= $row['user']->getFullName() ?></div>
                            <div class="chart-user-progress" style="width:<?= intval($row['progress']) ?>%;"></div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php else: ?>
            <h4 class="text-center" style="color:#aaa !important;margin: 0 0 5px 0;"><?= Yii::t('report', '_NULL_REPORT_RESULT') ?></h4>
        <?php endif; ?>
    </div>

<?php endforeach; ?>
</div>
<?php if($showInDialog): ?>
    <div class="form-actions">
        <?= CHtml::button(Yii::t('standard', '_CLOSE'), array(
            'class' => 'btn btn-docebo black big close-dialog'
        )) ?>
    </div>
<?php endif; ?>
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('change', "select#select-leaderboard", function () {
            var parent = $(this).parent();
            var i = 0;
            while ((!$(parent).attr('id') || !/^leaderboard\-[a-zA-Z0-9]+$/.test($(parent).attr('id'))) && i < 12 ) {
                parent = $(parent).parent();
                i++;
            }
            
            var divId = $(parent).attr('id') == undefined ? console.trace() : $(parent).attr('id');
            var leaderboardId = $(this).val();

            $('#' + divId + ' div[class^=leaderboard-chart').each(function () {
                $(this).hide();
            })

            $('#' + divId + ' .leaderboard-chart-' + leaderboardId).show(); 
        });
        //Resize personal position according to how big the number is.
        //This takes as GRANTED that the initial font-size is 26px AND the font is the initial font for this
        $('.chart-counter.personal-counter').each(function(k,v){
            var normalPosLength = 2;
            window.initialChartCounterPersonalCounterFontSize = window.initialChartCounterPersonalCounterFontSize ? window.initialChartCounterPersonalCounterFontSize : parseInt($(v).css('font-size'));
            var personalPosition = $(v).text();
            var personPosLength = personalPosition.length - normalPosLength;
            var fontSize = parseInt($(v).css('font-size'));
            if(fontSize == window.initialChartCounterPersonalCounterFontSize && personPosLength > 0) {
                for(var i = 0; i < personPosLength; i++) {
                    fontSize -= 7;
                }
                $(v).css('font-size', (fontSize) + 'px');
                $(v).css('width', 'auto');
            }
        });
    });
</script>
