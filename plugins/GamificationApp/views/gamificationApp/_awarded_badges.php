<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 25.11.2015 г.
 * Time: 17:03
 *
 * @var $badges array
 */
?>

<?php if(count($badges) > 0): ?>
<div class="badge-list">
    <?php


        foreach ($badges as $badge) {
            echo CHtml::tag('div', array(
                'data-id' => $badge->gamificationBadge->pkToString()
            ), $badge->gamificationBadge->renderBadge());
        }
    ?>
</div>
<?php else: ?>

    <div class="not-awarded-message">
        <div class="row">
            <div class="green-big-message">
                <?= Yii::t('gamification', 'Oops') ?>
            </div>
        </div>
        <div class="row">
            <div class="gray-message">
                <?= Yii::t('gamification', "You don't have any badges yet") ?>
            </div>
        </div>
    </div>

<?php endif; ?>
