<?php $id_badge = $model->id_badge; ?>
<div class="advanced-main advanced-main-badge">
	<div class="row odd has-border">
		<div class="row">
			<div class="setting-name">
				<?= Yii::t('gamification', 'Badge details'); ?>
				<p class="description"><?= Yii::t('gamification', 'Define the details of the badge'); ?></p>
			</div>
			<div class="values">
				<div class="languages">
					<p><?= Yii::t('gamification', 'Assign details for each language'); ?></p>
				</div>
				<div class="lang-wrapper">
					<?= CHtml::dropDownList('lang_code', Settings::get('default_language', 'english'), $langs); ?>
					<div class="stats">
						<p class="available"><?= Yii::t('admin_lang', '_LANG_ALL'); ?>: <span><?= count($langs); ?></span></p>
						<p class="assigned"><?= Yii::t('standard', 'Filled languages'); ?>: <span></span></p>
					</div>
				</div>

				<div class="input-fields">
					<p><?= Yii::t('standard', '_TITLE'); ?></p>
					<div class="max-input">
						<?= CHtml::textfield('title_current', '', array('class' => 'title')); ?>
					</div>
					<p><?= Yii::t('standard', '_TEXTOF'); ?></p>
					<?= CHtml::textarea('description_current', '', array('class' => 'text')); ?>
				</div>
				<div class="translations" style="display: none;">
					<?php
						$models = GamificationBadgeTranslation::model()->getModelsList($id_badge);
						foreach ($langs as $langCode => $translation):
							$model_l = $models[$langCode];
					?>
						<div class="<?= $langCode ?>">
							<?= CHtml::textField('name_'.$langCode, $model_l->name); ?>
							<?= CHtml::textArea('description_'.$langCode, $model_l->description); ?>
						</div>
					<?php endforeach; ?>
				</div>
				<div class="points">
					<p><?= ucfirst(Yii::t('test', '_TEST_SCORES')); ?></p>
					<?= CHtml::textField('points', (!$model->score ? 0 : $model->score)); ?>
					<span class="gamification-sprite star-small"></span>
				</div>
			</div>
		</div>
	</div>
</div>