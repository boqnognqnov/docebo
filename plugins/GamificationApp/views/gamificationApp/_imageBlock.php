<?php $id_badge = $model->id_badge; ?>
<div class="advanced-main advanced-main-badge advanced-main-badge-white">
	<div class="row odd has-border">
		<div class="row">
			<div class="setting-name">
				<?= Yii::t('gamification', 'Badge icon'); ?>
				<p class="description"><?php echo Yii::t('gamification', 'Upload or select an icon for the badge'); ?></p>
			</div>
			<div class="values">
				<?= Yii::t('gamification', 'Current badge icon'); ?>
				<div class="current_image">
					<img id="current_badge_icon" src="<?= $default_image; ?>" alt="<?= Yii::t('gamification', 'Current badge icon'); ?>" title="<?= Yii::t('gamification', 'Current badge icon'); ?>" />
					<?php $this->renderPartial('//common/_modal', array(
											'config' => array(
												'class' => 'new-user-summary btn-badge-image',
												'modalTitle' => Yii::t('gamification', 'Change badge icon'),
												'linkTitle' => Yii::t('gamification', 'Change badge icon'),
												'url' => 'GamificationApp/GamificationApp/imageBlock&id_badge='.$id_badge,
												'buttons' => array(
													array(
														'type' => 'submit',
														'title' => Yii::t('standard', '_SAVE'),
														'formId' => 'select-form',
													),
													array(
														'type' => 'cancel',
														'title' => Yii::t('standard', '_CANCEL'),
													),
												),
                                                'afterLoadingContent' => 'courseFormInit();',
                                                'afterSubmit' => 'updateBadgeContent',
											),
										)); ?>
				</div>
				<?= CHtml::hiddenField('icon', $default_icon); ?>
				<?= Yii::t('gamification', 'Badge preview'); ?>
				<div class="preview_badge wide">
					<div>
						<img id="preview_icon" src="<?= $default_image; ?>" alt="<?= Yii::t('gamification', 'Current badge icon'); ?>" title="<?= Yii::t('gamification', 'Current badge icon'); ?>" />
						<img class="flag" src="<?= $flag_image; ?>" alt="<?= Yii::t('gamification', 'Current badge icon'); ?>" title="<?= Yii::t('gamification', 'Current badge icon'); ?>" />
						<span class="flag_text">0</span>
						<span class="badge_title"></span>
						<span class="badge_description"></span>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
$(function () {
    $.fn.updateBadgeContent = function(data) {
        if (data.html) {
            $('.modal.in .modal-body').html(data.html);
        } else {
            if(data.icon) {
                $("#icon").val(data.icon);
                $("#preview_icon").attr('src', data.url);
                $("#current_badge_icon").attr('src', data.url);
            }

            $('.modal.in').modal('hide');
        }
    };
});

</script>