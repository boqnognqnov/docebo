<?php
$leaderboardContext = isset($leaderboardContext) ? $leaderboardContext : false;
?>
<script type="text/javascript">

    function redrawBadgePreview() {
        var description = '';
        if(tinymce.activeEditor)
            description = tinymce.activeEditor.getContent();
        else
            description = $('#description_current').val();

        if(description.length > 200)
            description = description.substring(0, 196)+'...';

        $('.badge_description').html(description);
        $('.badge_title').html($("#title_current").val());
        $('.flag_text').html($('#points').val());
    }

	$(function() {
		<?php if(!$leaderboardContext): ?>
		//Language block
		TinyMce.attach(".text", {height: 200,
			plugins : [
				"advlist autolink lists link image charmap print preview anchor",
				"searchreplace visualblocks code fullscreen",
				"insertdatetime media table contextmenu paste moxiemanager textcolor"
			],
			toolbar : "undo redo | styleselect | bold italic fontsizeselect forecolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
			language: yii.language,
			setup:	function(editor) {
				editor.on('change', function(e) {redrawBadgePreview();});
			}
		});
		<?php endif; ?>

		var previous = $("#lang_code").val();
		var default_language = '<?=Settings::get('default_language', 'english')?>';

		$('.input-fields input[name^="title_"].title').val($("."+previous+" input").val());
		$(".text").val($("."+previous+" textarea").val());

		$("#lang_code").change(function()
		{
			$("."+previous+" input").val($('.input-fields input[name^="title_"].title').val());
			<?php if(!$leaderboardContext): ?>
			$("."+previous+" textarea").val(tinymce.activeEditor.getContent());
			<?php endif; ?>
			previous = this.value;
			$('.input-fields input[name^="title_"].title').val($("."+previous+" input").val());
			<?php if(!$leaderboardContext): ?>
			$(".text").val($("."+previous+" textarea").val());
			tinymce.activeEditor.setContent($("."+previous+" textarea").val());

			redrawBadgePreview();
			<?php endif; ?>
			checkCompiledLanguages();
		});

		<?php if($leaderboardContext): ?>
		var updated = false;
		$(document).on('dialog2.content-update', function(){
			updated = true;
			checkCompiledLanguages();
		});
		$(document).on('jwizard.form-restored', function(){
			if(!updated){
				checkCompiledLanguages();
			}
		});
//
//		}, 100);
		<?php else: ?>
		checkCompiledLanguages();
		<?php endif; ?>

		<?php if(!$leaderboardContext): ?>
		// Image block
		$('#points').change(function(e){redrawBadgePreview();});

		$('.input-fields input[name^="title_"].title').change(function(e){redrawBadgePreview();});
		<?php endif; ?>;
            
        $('.input-fields input[name^="title_"].title').keyup(function(){checkCompiledLanguages();});

		$('input#save').on('click', function(e){
			var button = $(e.target);

			if(button.hasClass('disabled')) {
				e.preventDefault();
			} else {
				button.addClass('disabled');
			}
		});

		<?php if(!$leaderboardContext): ?>
		$('#badge-create-form').submit(function(e){
			$("."+previous+" input").val($('.input-fields input[name^="title_"].title').val());
			$("."+previous+" textarea").val(tinymce.activeEditor.getContent());

			if($("."+default_language+" input").val() === '') {
				if ($('#lang_code').val() == default_language) {
					Docebo.Feedback.show('error', Yii.t('user_management', 'Title is required'));
				} else {
					Docebo.Feedback.show('error', Yii.t('user_management', '_ERROR_ADDITIONAL_FIELDS_ENGLISH_REQUIRED'));
				}
				e.preventDefault();
				$('input#save').removeClass('disabled');
			}
		});
		redrawBadgePreview();
		<?php endif; ?>;
	});

	function toggleAsterisk(language, action) {
		var text, el = $('#lang_code option[value="'+language+'"]');
		if (el.length > 0) {
			text = el.text();
			if (action) { //we want the asterisk
				if (text.indexOf("* ") < 0) { //check if it is already present
					el.text("* "+text);
				}
			} else { //delete asterisk, if present
				if (text.indexOf("* ") === 0) {
					el.text(text.replace("* ", ''));
				}
			}
		}
	}

	function checkCompiledLanguages()
	{
		var languages = $('.translations :input');
		var counter = 0;
		var lang_code;
		for(var i = 0; i < languages.length; i++)
		{
			if (languages[i].name && languages[i].name.indexOf("name_") === 0) { //exclude descriptions, we need only names
				lang_code = languages[i].name.replace("name_", "");
                if(lang_code === $("#lang_code").val()) {
                    languages[i].value = $('.input-fields input[name^="title_"].title').val();
                }
				if (languages[i].value.length > 0) {
					counter++;
					toggleAsterisk(lang_code, true);
				} else {
					toggleAsterisk(lang_code, false);
				}
			}
		}

		$('.assigned span').html(counter);
	}

	<?php if(!$leaderboardContext): ?>
    // Event association animation
	var previous_event = $("#event").val();
	var previous_goal = $("#goal").val();
	$('#association_mode input').styler();

    // Switch between manual and automatic event modes
	$('#association_mode').change(function(e)
	{
		if($('#association_mode_0:checked').length > 0)
			$('.automatic-association').removeClass('display-block');
		else
			$('.automatic-association').addClass('display-block');
	});

    // Handle event dropdown selection
	$('#event').change(function(e){
		$('#info_' + previous_event).hide();
		previous_event = $("#event").val();
		$('#info_' + previous_event).show();
		$.ajax({
			type: "POST",
			url: "./index.php?r=GamificationApp/GamificationApp/getEventSetting",
			async: true,
			data: {event:previous_event, id_badge:<?= $id_badge ? $id_badge : 0 ?>},
            beforeSend: function(xhr) {
                $('#event_setting').html('');
                $('#event_setting').addClass("event-loading");
            },
			success: function(result) {
				$('#event_setting').html(result);
			}
		}).always(function() {
            $('#event_setting').removeClass("event-loading");
        });
	});

    // Handle goal dropdown selection
	$('#goal').change(function(e){
		$('#info_' + previous_goal).hide();
		previous_goal = $("#goal").val();
		$('#info_' + previous_goal).show();
		$.ajax({
			type: "POST",
			url: "./index.php?r=GamificationApp/Contests/getGoalSetting",
			async: true,
			data: {goal:previous_goal, id_contest:<?= $id_contest ? $id_contest : 0 ?>},
            beforeSend: function(xhr) {
                $('#goal_setting').html('');
                $('#goal_setting').addClass("event-loading");
            },
			success: function(result) {
				$('#goal_setting').html(result);
			}
		}).always(function() {
            $('#goal_setting').removeClass("event-loading");
        });
	});
    
    // Force loading of selected event settings
    previous_event = $("#event").val();
    $('#info_' + previous_event).show();
	$.ajax({
		type: "POST",
		url: "./index.php?r=GamificationApp/GamificationApp/getEventSetting",
		async: false,
		data: {event:previous_event, id_badge:<?= $id_badge ? $id_badge : 0 ?>},
        beforeSend: function(xhr) {
            $('#event_setting').html('');
            $('#event_setting').addClass("event-loading");
        },
		success: function(result) {
			$('#event_setting').html(result);
		}
	}).always(function() {
        $('#event_setting').removeClass("event-loading");
    });
	<?php endif; ?>
</script>