<div id="course-selector-settings">
	<div>
		<?= CHtml::radioButton('params[courses_association]', (!isset($params['courses_association']) || (isset($params['courses_association']) && $params['courses_association'] == 'all') ? true : false), array('id' => 'courses_association_0', 'class' => 'radio-course-selection', 'value' => 'all')); ?>
		<label for="courses_association_0" class="label-course-selection"><?= Yii::t('standard', 'Any course'); ?></label>

		<?= CHtml::radioButton('params[courses_association]', (isset($params['courses_association']) && $params['courses_association'] == 'selection' ? true : false), array('id' => 'courses_association_1', 'class' => 'radio-course-selection', 'value' => 'selection')); ?>
		<label for="courses_association_1" class="label-course-selection"><?= Yii::t('gamification', 'Any of the following courses'); ?></label>

        <?= CHtml::radioButton('params[courses_association]', (isset($params['courses_association']) && $params['courses_association'] == 'all_selection' ? true : false), array('id' => 'courses_association_2', 'class' => 'radio-course-selection', 'value' => 'all_selection')); ?>
        <label for="courses_association_2" class="label-course-selection"><?= Yii::t('gamification', 'All of the following courses'); ?></label>

        <?= CHtml::radioButton('params[courses_association]', (isset($params['courses_association']) && $params['courses_association'] == 'nth' ? true : false), array('id' => 'courses_association_3', 'class' => 'radio-course-selection', 'value' => 'nth')); ?>
        <label for="courses_association_3" class="label-course-selection"><?= Yii::t('gamification', 'After completing a certain number of courses'); ?></label>
	</div>
	<div class="clearfix"></div>
	<div id="course_selector" style="display: <?= (isset($params['courses_association']) && in_array($params['courses_association'], array('selection', 'all_selection')) ? 'block' : 'none'); ?>;">
		<select id="courses" name="params[courses]">
			<?php foreach($courses as $info) : ?>
				<option value="<?= $info['idCourse']; ?>" class="selected"><?= ($info['code'] != '' ? $info['code'].' - ' : '').$info['name']; ?></option>
			<?php endforeach; ?>
		</select>
        <div class="errorMessage"></div>
	</div>
    <div class="clearfix"></div>
    <div id="number_selector" style="display: <?= (isset($params['courses_association']) && $params['courses_association'] == 'nth' ? 'block' : 'none'); ?>; margin-top: 10px;">
        <?= Yii::t('gamification', 'Assign only if completed the') ?>
        <input style="width: 80px;" id="limit" type="text" name="params[limit]" value="<?= isset($params['limit']) ? $params['limit'] : '' ?>" <?= ($params['courses_association'] !== 'nth') ? 'disabled="disabled"' : '' ?> />
        <?= Yii::t('gamification', '-th course') ?>
        <div class="errorMessage"></div>
    </div>
</div>
<script type="text/javascript">
	$('.radio-course-selection').styler();

	$('.radio-course-selection').change(function(e)
	{
        $('#course_selector').find('.errorMessage').html('');
        $('#number_selector').find('.errorMessage').html('');
		if($('#courses_association_1').is(':checked')) {
            $('#course_selector').show();
            $('#number_selector').hide();
            $('#limit').prop('disabled', 'disabled');
        } else if ($('#courses_association_2').is(':checked')) {
            $('#course_selector').show();
            $('#number_selector').hide();
            $('#limit').prop('disabled', 'disabled');
        } else if($('#courses_association_3').is(':checked')) {
            $('#number_selector').show();
            $('#course_selector').hide();
            $('#courses').val('');
            $('#limit').prop('disabled', '');
        } else {
            $('#course_selector').hide();
            $('#number_selector').hide();
            $('#courses').val('');
            $('#limit').prop('disabled', 'disabled');
        }

	});

	$(document).ready(function(){
		$("#courses").fcbkcomplete({
			json_url: "./index.php?r=GamificationApp/GamificationApp/getCourseList",
			addontab: true,
			input_min_size: 0,
            width: '98.5%',
			cache: true,
			complete_text: '',
			maxitems: '<?php echo $totalCourses?>',
			filter_selected: true
		});

        function isNormalInteger(str) {
            var n = ~~Number(str);
            return String(n) === str && n > 0;
        }

        $('#badge-create-form').submit(function(e){
			if($('#association_mode_0:checked').length == 0) {
				if($('#course-selector-settings').length == 0)
					return true;

				if((($('input:radio[name="params[courses_association]"]:checked').val() == 'selection')
                    || $('input:radio[name="params[courses_association]"]:checked').val() == 'all_selection') && !$("#courses").val()) {
					$('#course_selector').find('.errorMessage').html(<?=CJSON::encode(Yii::t('gamification', 'Please select at least one course'))?>);
					e.preventDefault();
                    $('input#save').removeClass('disabled');
				}

                if($('input:radio[name="params[courses_association]"]:checked').val() == 'nth' && (!$("#limit").val() || !isNormalInteger($("#limit").val()))) {
                    $('#number_selector').find('.errorMessage').html(<?=CJSON::encode(Yii::t('gamification', 'Please enter a number higher than') . ' ' . 0)?>);
                    e.preventDefault();
                    $('input#save').removeClass('disabled');
                }
			}
        });
	});
</script>