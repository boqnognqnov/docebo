<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButtonBadge() : hideConfirmButtonBadge());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="activate-action">
	<div class="modal-body">
		<div class="form">
			<?php $form = $this->beginWidget('CActiveForm', array(
				'id' => 'single_badge_assign_delete'
			)); ?>

			<p><?= Yii::t('gamification', 'Delete <strong>"{badge}"</strong> badge issued to <strong>{user}</strong> on date {date}', array('{badge}' => $model_t->name, '{user}' => Yii::app()->user->getRelativeUsername($model_u->userid), '{date}' => $model_a->issued_on)); ?></p>

			<div class="clearfix">
				<?= CHtml::checkBox('confirm', false); ?>
				<?= CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed', array($model_t->name)), 'confirm'); ?>
			</div>
			<?php $this->endWidget(); ?>
		</div>
	</div>
</div>

<div class="form-actions">
	<a class="btn btn-submit  disabled" href="javascript:void(0);" id="deletion-submit" ><?= Yii::t('standard', '_CONFIRM'); ?></a>
	<a class="btn btn-cancel " href="javascript:void(0);" id="deletion-undo" ><?= Yii::t('standard', '_UNDO'); ?></a>
</div>

<script type="text/javascript">
	var modal_id = $('#current_modal_id').val();
	$('#confirm').styler();
	$('#confirm').change(function(e){
		if($(this).prop('checked') == true)
		{
			$('.modal-footer .btn-submit').removeClass('disabled');
		}
		else
		{
			$('.modal-footer .btn-submit').addClass('disabled');
		}
	});
	$('#deletion-undo').click(function(e){
		$('#badge-modal-assign-deletion').dialog2('close');
	});
	$(document).delegate(".modal", "dialog2.closed", function() {
		$('#' + modal_id).modal('show');
	});
	$('#deletion-submit').click(function(e){
		if($('.modal-footer .btn-submit.disabled').length > 0)
		{}
		else
		{
			$.ajax({
				type: "POST",
				url: "./index.php?r=GamificationApp/GamificationApp/deleteAssociationSingle",
				async: true,
				data: {id_badge:<?= $model->id_badge; ?>, id_user:<?= $model_u->idst; ?>, issued_on:'<?= $model_a->issued_on; ?>', confirm:1},
				success:function(result)
				{
					if(result.success == true)
					{
						$('#badge-modal-assign-deletion').dialog2('close');
						$('#badges-grid-<?= $model_u->idst; ?>').yiiGridView('update');
					}
				}
			});
		}
	});
	var stringify = function(o) {
		var temp = '{';
		var fields = [];
		var def;
		for (x in o) {
			try { def = o[x].toString(); } catch(e) { def = '[not valid]'; }
			fields.push(x+': '+def);
		}
		temp += fields.join(', ');
		temp += '}';
		return temp;
	}
</script>