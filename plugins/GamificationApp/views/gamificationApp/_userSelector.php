<div class="courseEnroll-tabs">
	<ul class="nav nav-tabs">
		<li class="active">
			<a data-target=".courseEnroll-page-users" href="#">
				<span class="courseEnroll-users"></span><?php echo Yii::t('standard', '_USERS'); ?>
			</a>
		</li>
		<li>
			<a data-target=".courseEnroll-page-groups" href="#">
				<span class="courseEnroll-groups"></span><?php echo Yii::t('standard', '_GROUPS'); ?>
			</a>
		</li>
		<!-- HIDE IF NO BRANCHES -->
		<?php if(!empty($fullOrgChartTree)) : ?>
			<li>
				<a data-target=".courseEnroll-page-orgchart" href="#">
					<span class="courseEnroll-orgchart"></span><?php echo Yii::t('standard', '_ORGCHART'); ?>
				</a>
			</li>
		<?php endif; ?>
	</ul>

	<div class="select-user-form-wrapper">
		<div class="tab-content">
			<div class="courseEnroll-page-users tab-pane active">
				<?php $this->renderPartial('_selectorUserFilters', array(
					'model' => $userModel
				)); ?>
			</div>
			<div class="courseEnroll-page-groups tab-pane">
				<?php $this->renderPartial('_selectorGroupFilters', array(
					'model' => $groupModel,
				)); ?>
			</div>
			<div class="courseEnroll-page-orgchart tab-pane">
				<?php $this->renderPartial('_selectorOrgchartFilters'); ?>
			</div>
			<div class="courseEnroll-page-roles tab-pane">
				filter-4
			</div>
		</div>

		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'select-user-form',
			'htmlOptions' => array(
				'data-grid-items' => 'true',
			),
		)); ?>

			<div class="tab-content">
				<div class="courseEnroll-page-users tab-pane active">
					<?php $this->renderPartial('_selectorUser', array(
						'model' => $userModel
					)); ?>
				</div>
				<div class="courseEnroll-page-groups tab-pane">
					<?php $this->renderPartial('_selectorGroup', array(
						'model' => $groupModel,
					)); ?>
				</div>
				<div class="courseEnroll-page-orgchart tab-pane">
					<?php
						$this->renderPartial('_selectorOrgchart', array(
							'fullOrgChartTree' => $fullOrgChartTree,
							'puLeafs' => $puLeafs,
						));
					?>
				</div>
				<div class="courseEnroll-page-roles tab-pane">
					form-4
				</div>
			</div>

		<?php $this->endWidget(); ?>

	</div>
</div>