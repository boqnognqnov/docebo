<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 25.11.2015 г.
 * Time: 15:42
 */
?>
<div class="all-badges-block">
    <h6><?= Yii::t('gamification', 'Awarded') ?></h6>
    <hr>
    <div class="badge-list">
        <?php

        if(count($badges) > 0) {
            foreach ($badges as $badge) {
                echo CHtml::tag('div', array(
                    'data-id' => $badge->gamificationBadge->pkToString()
                ), $badge->gamificationBadge->renderBadge());
            }
        } else{
            echo Yii::t('gamification', "You don't have any badges yet");
        }
        ?>
    </div>
    <br>
    <br>
    <h6><?= Yii::t('gamification', 'Not Awarded') ?></h6>
    <hr>
    <div class="badge-list">
        <?php
            foreach ($notAwarded as $badge) {
                echo CHtml::tag('div', array(
                    'data-id' => $badge->pkToString()
                ), $badge->renderBadge());
            }
        ?>
    </div>
</div>