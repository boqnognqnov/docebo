<?php
	$this->breadcrumbs = array(
		Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
		Yii::t('gamification', 'Badges') => Docebo::createAppUrl('admin:GamificationApp/GamificationApp/index'),
		Yii::t('gamification', 'Assign user to badge manually')
	);
?>

<?php $this->renderPartial('_mainAssignActionButtons', array('id_badge' => $id_badge)); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'badge-assign-form',
	'htmlOptions' => array(
		'class' => 'ajax-list-form',
		'data-grid' => '#badge-assign-list'
	),
)); ?>

<div class="main-section group-management">
	<div class="filters-wrapper">
		<div class="filters clearfix">
			<div class="input-wrapper">
				<?php echo $form->textField($gamificationAssignedBadges->coreUser, 'search_input', array(
					'id' => 'advanced-search-badge-assign',
					'class' => 'typeahead',
					'autocomplete' => 'off',
					'data-url' => Docebo::createAppUrl('admin:userManagement/usersAutocomplete'),
					'data-group' => $gamificationAssignedBadges->id_user,
					'placeholder' => Yii::t('standard', '_SEARCH'),
					'data-source-desc' => 'true',
				)); ?>
				<span class="search-icon"></span>
			</div>
		</div>
	</div>
</div>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<div class="selections clearfix">
	<?php $this->widget('ext.local.widgets.ListViewSelectAll', array(
		'gridId' => 'badge-assign-list',
		'class' => 'left-selections clearfix',
		'dataProvider' => $gamificationAssignedBadges->sqlDataProviderSelector(),
		'itemValue' => 'id_user',
		'displayFullSelection' => true
	)); ?>

	<div class="right-selections clearfix">
		<select name="massive_action" id="badge-assign-list-massive-action">
			<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
			<option value="delete"><?php echo Yii::t('standard', '_DEL'); ?></option>
		</select>
	</div>
</div>

<div class="bottom-section">
	<div id="grid-wrapper">
		<?php $this->widget('ext.local.widgets.CXListView', array(
			'id' => 'badge-assign-list',
			'htmlOptions' => array('class' => 'list-view clearfix'),
			'dataProvider' => $gamificationAssignedBadges->dataProvider(),
			'itemView' => '_view',
			'itemsCssClass' => 'items clearfix',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'pager' => array(
				'class' => 'common.components.DoceboCLinkPager',
			),
			'template' => '{items}{pager}{summary}',
			'ajaxUpdate' => 'badge-assign-list-all-items',
			'beforeAjaxUpdate' => 'function(id, options) {
				options.type = "POST";
				options.data = $("#grid-filter-form").serialize();
				options.data += "&selectedItems=" + $(\'#\'+id+\'-selected-items-list\').val();
			}',
			'afterAjaxUpdate' => 'function(id, data) { $("#badge-assign-list input").styler();  $.fn.updateListViewSelectPage();}',
		)); ?>
	</div>
</div>

<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input, select').styler();
			replacePlaceholder();
		});
	})(jQuery);

	$.fn.updateBadgeAssignList = function(data)
	{
		if (data.html) {
			$('.modal.in .modal-body').html(data.html);

			if($('.modal.in .modal-body').find('.auto-close').length > 0){
				$('.modal.in').modal('hide');
			}
		} else {
			//hide dialog and manually update selector
			$('.modal.in').modal('hide');
			$('#badge-assign-list-massive-action').val('');
			$('#badge-assign-list-all-items-list').val('');
			$('#badge-assign-list-selected-items-list').val('');
			$('#badge-assign-list-selected-items-count').text('0');
			//Restore the page selector
			$("a.deselect-all").removeClass('active');
			$("a.deselect-this").removeClass('active');
			$("a.select-all").addClass('active');
			$("a.select-this").addClass('active');
			$.fn.yiiListView.update('badge-assign-list');
		}
	}

	$('#badge-assign-list-massive-action').live('change', function() {
		var selectedUsers = getSelectedCheckboxes('badge-assign-list', 'badge-assign-list-checkboxes');
		if (selectedUsers.length > 0) {
			switch ($(this).val()) {
				case 'delete':
	        config = {
	          'id'                 : 'modal-' + getRandKey(),
	          'modalClass'         : 'delete-node',
	          'modalTitle'         : Yii.t("standard", "_DEL_SELECTED"),
	          'modalUrl'           : HTTP_HOST + '?r=GamificationApp/GamificationApp/deleteMultiAssignedBadge',
	          'modalRequestType'   : 'POST',
	          'modalRequestData'   : {'ids' : selectedUsers.join(','), id_badge: <?= $id_badge; ?>},
	          'buttons'            : [
	            {"type": "submit", "title": Yii.t("standard", "_CONFIRM")},
	            {"type": "cancel", "title": Yii.t("standard", "_CLOSE")}
	          ],
	          'afterLoadingContent': 'hideConfirmButton();',
	          'afterSubmit'        : 'updateBadgeAssignList'
	        }
	        $('body').showModal(config);
				break;
			}
		}
		$('#'+$(this).attr('id')+'-styler ul li:first').click();
		$('option:first-child', this).attr("selected", "selected");
	})
</script>