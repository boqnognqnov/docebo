<div class="user-goal-settings row-fluid">
	<?= Yii::t('gamification', "Assign when user is top contributor (upload assets) in this timeframe")?>
	<div class="span12" style="padding-top:10px">
		<div>
			<?= CHtml::radioButton('params[goal]', (!isset($params['goal']) || (isset($params['goal']) && $params['courses_association'] == 0) ? true : false), array('id' => 'goal_0', 'class' => 'radio-goal radio-course-selection', 'value' => 0)); ?>
			<label for="goal_0" class="label-goal label-course-selection"><?= Yii::t('app7020', 'Daily'); ?></label>

			<?= CHtml::radioButton('params[goal]', (isset($params['goal']) && $params['goal'] == 1 ? true : false), array('id' => 'goal_1', 'class' => 'radio-goal radio-course-selection', 'value' => 1)); ?>
			<label for="goal_1" class="label-goal label-course-selection"><?= Yii::t('app7020', 'Weekly'); ?></label>

			<?= CHtml::radioButton('params[goal]', (isset($params['goal']) && $params['goal'] == 2 ? true : false), array('id' => 'goal_2', 'class' => 'radio-goal radio-course-selection', 'value' => 2)); ?>
			<label for="goal_2" class="label-goal label-course-selection"><?= Yii::t('app7020', 'Monthly'); ?></label>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('.radio-course-selection').styler();

	$('.radio-course-selection').change(function(e)
	{
		$('#course_selector').find('.errorMessage').html('');
		if($('#courses_association_1').is(':checked')) {
			$('#course_selector').show();
			$('#number_selector').hide();
			$('#limit').prop('disabled', 'disabled');
		} else if ($('#courses_association_2').is(':checked')) {
			$('#course_selector').show();
			$('#number_selector').hide();
			$('#limit').prop('disabled', 'disabled');
		} else if($('#courses_association_3').is(':checked')) {
			$('#number_selector').show();
			$('#course_selector').hide();
			$('#courses').val('');
			$('#limit').prop('disabled', '');
		} else {
			$('#course_selector').hide();
			$('#number_selector').hide();
			$('#courses').val('');
			$('#limit').prop('disabled', 'disabled');
		}

	});
</script>