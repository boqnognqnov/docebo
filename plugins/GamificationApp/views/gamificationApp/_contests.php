<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 21.12.2015 г.
 * Time: 10:55
 */
?>

<h4><?php echo Yii::t('gamification', 'Contests'); ?></h4>

<div id="contest-content">
    <div class="main-section group-management">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'contests-form',
            'htmlOptions' => array(
                'class' => 'ajax-grid-form',
                'data-grid' => '#gamification-my-contests-list'
            )
        ));
        ?>
        <div class="filters-wrapper">
            <div class="filters clearfix">
                <div class="pull-left show-open-closed-contests" style="padding: 17px 0;">
                    <?= CHtml::radioButton('show_open_closed_contests', true, array('id' => 'show-all-contests', 'value' => 'all', 'style' => 'margin:0!important;')) . ' ' . CHtml::label(Yii::t('standard', 'All') . '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp', 'show-all-contests', array('style' => 'display:inline-block;margin:0!important;')); ?>
                    <?= CHtml::radioButton('show_open_closed_contests', false, array('id' => 'show-open-contests', 'value' => 'open', 'style' => 'margin:0!important;')) . ' ' . CHtml::label(Yii::t('gamification', 'Open contests') . '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp', 'show-open-contests', array('style' => 'display:inline-block;margin:0!important;')); ?>
                    <?= CHtml::radioButton('show_open_closed_contests', false, array('id' => 'show-closed-contests', 'value' => 'closed', 'style' => 'margin:0!important;')) . ' ' . CHtml::label(Yii::t('gamification', 'Closed contests'), 'show-closed-contests', array('style' => 'display:inline-block;margin:0!important;')); ?>
                    <span class="search-icon"></span>
                </div>
                <div class="input-wrapper">
                    <?php
                    echo CHtml::textField('search_text', '', array(
                        'id' => 'advanced-search-contest',
                        'class' => 'typeahead',
                        'autocomplete' => 'off',
                        'data-url' => Docebo::createAdminUrl('GamificationApp/Contests/autocomplete'),
                        'placeholder' => Yii::t('standard', '_SEARCH'),
                    ));
                    ?>
                    <span class="search-icon"></span>
                </div>
            </div>
        </div>
    </div>
    <?= CHtml::submitButton('', array('style' => 'display: none;')); ?>
    <?php $this->endWidget(); ?>
    <div class="bottom-section clearfix">
        <div id="list-wrapper">
            <?php
            $contest = new GamificationContest();
            $userContestsIds = $contest->getUserContests();
            $dataProvider = $contest->dataProvider(false, $userContestsIds, true);
            $listOptions = array(
                'ajaxType' => 'GET',
                'id' => 'gamification-my-contests-list',
                'htmlOptions' => array('class' => 'list-view clearfix'),
                'dataProvider' => $dataProvider,
                'itemView' => 'plugin.GamificationApp.views.contests._listViewTemplate',
                'itemsCssClass' => 'list-view-item clearfix',
                'template' => '{items}{summary}{pager}',
                'pager' => array('class' => 'common.components.DoceboCLinkPager'),
                'summaryText' => Yii::t('standard', '_TOTAL'),
                'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
                'afterAjaxUpdate' => 'function(id, data) { }',
                'ajaxUpdate' => 'all-items',
            );
            ?>
            <div class="listview-container row-fluid contest-titles">
                <div class="span3" style="padding-left:14px!important;"><?= Yii::t('gamification', 'Contest name') ?></div>
                <div class="span3 text-center"><?= Yii::t('gamification', 'Contest period') ?></div>
                <div class="span3" style="padding-left: 10% !important;"><?= Yii::t('gamification', 'Winner') ?></div>
                <div class="span3 text-center"><?= Yii::t('standard', '_STATUS') ?></div>
            </div>
            <?php $this->widget('zii.widgets.CListView', $listOptions); ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    function updateArrowIconsOfAccordion(el) {
        if($(el).hasClass('fa-chevron-up')) {
            $(el).removeClass('fa-chevron-up').addClass('fa-chevron-down');
        } else if($(el).hasClass('fa-chevron-down')) {
            $(el).removeClass('fa-chevron-down').addClass('fa-chevron-up');
        }
    }

    $(function () {
        // contests scripts
        if (location.hash !== '') {
            var hash = location.hash.replace(/\&.*/gmi, '');
            if(/(mybadges\-tab)/gmi.test(hash)) {
                $('a[data-target=' + hash + ']').trigger('click');
            }
        }

        $('.show-open-closed-contests input').styler();
        $('input[name="show_open_closed_contests"]').on('change', function () {
            $.fn.yiiListView.update('gamification-my-contests-list', {data: {show_open_closed_contests: $(this).val()}});
        });
        $('input[name=search_text]').on('keyup', function () {
            $.fn.yiiListView.update('gamification-my-contests-list', {data: {search_text: $(this).val()}});
        });
        $(document).on('click', '.list-view-item div.listview-item div.details-header i', function(e){
            //remove the comment to collapse all before open current row.
            $('.list-view-item div.accordion:visible').not($(e.target.parentNode.parentNode).next()).each(function(){
                $(this).slideToggle(200, 'linear');
                $rowHandleIcon = $(this).prev().children('div.details-header').children('i');
                updateArrowIconsOfAccordion($rowHandleIcon);
            });
            $(e.target.parentNode.parentNode).next().slideToggle(200, 'linear');
            updateArrowIconsOfAccordion(e.target);
        });
    });
</script>
<style type="text/css">
    div#contest-content > div.main-section.group-management > form > div.filters-wrapper > div.filters > div.show-open-closed-contests span.jq-radio {
        margin:0!important;
    }
    div#contest-content > div.bottom-section > div#list-wrapper div.listview-item div.span3:first-of-type {
        padding-left: 14px !important;
    }
    .listview-item.clearfix.row-fluid:hover {
        background-color: #f1f3f2;
    }
</style>