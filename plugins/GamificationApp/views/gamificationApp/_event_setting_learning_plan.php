<div id="learning-plan-selector-settings">
    <div>
        <?= CHtml::radioButton('params[learning_plan_association]', (!isset($params['learning_plan_association']) || (isset($params['learning_plan_association']) && $params['learning_plan_association'] == 'all') ? true : false), array('id' => 'learning_plan_association_0', 'class' => 'radio-learning-plan-selection', 'value' => 'all')); ?>
        <label for="learning_plan_association_0" class="label-learning-plan-selection"><?= Yii::t('standard', 'Any learning_plan'); ?></label>

        <?= CHtml::radioButton('params[learning_plan_association]', (isset($params['learning_plan_association']) && $params['learning_plan_association'] == 'selection' ? true : false), array('id' => 'learning_plan_association_1', 'class' => 'radio-learning-plan-selection', 'value' => 'selection')); ?>
        <label for="learning_plan_association_1" class="label-learning-plan-selection"><?= Yii::t('gamification', 'Any of the following learning plans'); ?></label>

        <?= CHtml::radioButton('params[learning_plan_association]', (isset($params['learning_plan_association']) && $params['learning_plan_association'] == 'all_selection' ? true : false), array('id' => 'learning_plan_association_2', 'class' => 'radio-learning-plan-selection', 'value' => 'all_selection')); ?>
        <label for="learning_plan_association_2" class="label-learning-plan-selection"><?= Yii::t('gamification', 'All of the following learning plans'); ?></label>

    </div>
    <div class="clearfix"></div>
    <div id="learning_plan_selector" style="display: <?= (isset($params['learning_plan_association']) && $params['learning_plan_association'] == 'selection' || $params['learning_plan_association'] == 'all_selection' ? 'block' : 'none'); ?>;">
        <select id="learning_plan" name="params[learning_plan]">
            <?php foreach($learning_plan as $info) : ?>
                <option value="<?= $info['id_path']; ?>" class="selected"><?= ($info['path_code'] != '' ? $info['path_code'].' - ' : '').$info['path_name']; ?></option>
            <?php endforeach; ?>
        </select>
        <div class="errorMessage"></div>
    </div>
</div>

<script type="text/javascript">
    var learningPlan         = $("#learning_plan");
    var learningPlanSelector = $('#learning_plan_selector');
    var radioSelection       = $('.radio-learning-plan-selection');

    radioSelection.styler();
    radioSelection.change(function(e)
    {
        learningPlanSelector.find('.errorMessage').html('');
        if($('#learning_plan_association_1').is(':checked')) {
            learningPlanSelector.show();
        } else if ($('#learning_plan_association_2').is(':checked')) {
            learningPlanSelector.show();
        } else {
            learningPlanSelector.hide();
            learningPlan.val('');
        }
    });

    $(document).ready(function(){
        $("#learning_plan").fcbkcomplete({
            json_url: "./index.php?r=GamificationApp/GamificationApp/getLearningPlanList",
            addontab: true,
            input_min_size: 0,
            width: '98.5%',
            cache: true,
            complete_text: '',
            maxitems: '<?php echo $totalLearningPlans?>',
            filter_selected: true
        });

        $('#badge-create-form').submit(function(e){
            if($('#association_mode_0:checked').length == 0) {
                if($('#learning-plan-selector-settings').length == 0)
                    return true;
                var $selectionLPs = $('input:radio[name="params[learning_plan_association]"]:checked'); // The input field for fcbk autocomplete and selection of Learning Plans
                if ( ( $selectionLPs.val() == 'selection' || $selectionLPs.val() == 'all_selection') && !$("#learning_plan").val() ) {

                    learningPlanSelector.find('.errorMessage').html(<?=CJSON::encode(Yii::t('gamification', 'Please select at least one learning_plan'))?>);
                    e.preventDefault();
                    $('input#save').removeClass('disabled');
                }
            }
        });
    });
</script>