<div class="main-actions course-enrollments-actions clearfix">
	<h3 class="title-bold back-button">
		<?= CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('GamificationApp/GamificationApp/index')); ?>
		<span><?= Yii::t('gamification', 'Assign user to badge manually'); ?></span>
	</h3>
	<ul class="clearfix">
		<li data-bootstro-id="bootstroEnrollUsers">
			<div>
				<?php $courseEnrollWidget = $this->widget('ext.local.widgets.ModalWidget', array(
					'type' => 'link',
					'modalClass' => 'course-enrollment',
					'handler' => array(
						'title' => '<span></span>'.Yii::t('gamification', 'Assign users'),
						'htmlOptions' => array(
							'class' => 'course-enrollment',
							'alt' => Yii::t('helper', 'Assign badge - gamification'),
							'modal-title' => Yii::t('gamification', 'Assign users'),
						),
					),
					'headerTitle' => Yii::t('gamification', 'Assign users'),
					'url' => $this->id.'/assignSelector',
					'urlParams' => array('id_badge' => $id_badge),
					'updateEveryTime' => true,
					'remoteDataType' => 'json',
					'buttons' => array(
						array(
							'title' => Yii::t('standard', '_ASSIGN'),
							'type' => 'confirm',
							'class' => 'confirm-btn',
						),
						array(
							'title' => Yii::t('standard', '_CANCEL'),
							'type' => 'close',
							'class' => 'close-btn',
						),
					),
					'beforeShowModalCallBack' => 'applyTypeahead($(\'.course-enrollment .typeahead\'));',
				)); ?>
				<script type="text/javascript">
					$('#modal-<?= $courseEnrollWidget->uniqueId; ?> .confirm-btn').live('click', function () {
						var modal = $('#modal-<?= $courseEnrollWidget->uniqueId; ?> .modal-body');
						var form = modal.find("#select-user-form");

						if (form.data('grid-items') == true) {
							form.find('div.grid-view').each(function() {
								var selectedItems = $('#'+$(this).prop('id')+'-selected-items');
								if (selectedItems.length == 0) {
									selectedItems = $('#'+$(this).prop('id')+'-selected-items-list').clone();
									selectedItems.prop('name', $(this).prop('id')+'-selected-items')
										.prop('id', $(this).prop('id')+'-selected-items');
									form.append(selectedItems);
								} else {
									selectedItems.val($('#'+$(this).prop('id')+'-selected-items-list').val());
								}
							});
						}

						var formData = form.serialize();

						$.ajax({
							'dataType' : 'json',
							'type' : 'POST',
							'url' : modal.find('#select-user-form').attr('action'),
							'cache' : false,
							'data' : formData,
							'success' : function (data)
							{
								modal.html(data.html);
								if (data.modalClass !== undefined) {
									$('#modal-<?php echo $courseEnrollWidget->uniqueId; ?>').addClass(data.modalClass);
								}
								if (data.modalTitle !== undefined) {
									$('#label-<?php echo $courseEnrollWidget->uniqueId; ?>').html('<span></span>'+data.modalTitle);
								}
								applyTypeahead(modal.find('.typeahead'));
								modal.find('input').styler();
								modalPosition();
								if (data.status == 'saved') {

									$('#modal-<?php echo $courseEnrollWidget->uniqueId; ?>').modal('hide');
									$.fn.yiiListView.update('badge-assign-list');
								}
							}
						});
					});
				</script>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4></h4>
			<p></p>
		</div>
	</div>
</div>