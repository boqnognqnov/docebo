<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 24.11.2015 г.
 * Time: 18:00
 *
 * @var $showNotYetAwarded boolean
 */

$_breadcrumbs = array(
    Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
    Yii::t('gamification', 'Gamification') => Docebo::createAdminUrl('//GamificationApp/GamificationApp/index'),
    Yii::t('standard', 'Settings')
);

if (is_array($breadcrumbs)) {
    $_breadcrumbs = array_merge($_breadcrumbs, $breadcrumbs);
} else {
    $_breadcrumbs[] = $breadcrumbs;
}

$this->breadcrumbs = $_breadcrumbs;
?>

<div class="row-fluid">
    <div class="span6">
        <h3 class="advanced-elucidat-settings-form-title"><?= Yii::t('gamification', 'Gamification settings') ?></h3>
    </div>
</div>
<br>

<?php DoceboUI::printFlashMessages(); ?>

<div class="advanced-main gamification-settings">
<?= CHtml::beginForm(); ?>
    <div class="section">
        <div class="row odd">
            <div class="row">
                <div class="setting-name">
                    <?=Yii::t('gamification', 'Badges visibility') ?>
                </div>
                <div class="values">
                    <div class="control-group">
                        <div class="checkbox">
                            <?php
                            echo CHtml::checkBox('show_not_awarded', ($showNotYetAwarded === 'on') ? true : false, array(
                                'name' => 'show_not_awarded',
                            ));
                            echo CHtml::label(Yii::t('gamification', 'Show not yet awarded badges in my badges list'), 'show_not_awarded', array(
                                'style' => 'display: inline-block'
                            ));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row even">
                <div class="setting-name">
                    <?=Yii::t('gamification', 'Reward Shop') ?>
                </div>
                <div class="values">
                    <div class="control-group">
                        <div class="checkbox">
                            <?php
                            echo CHtml::checkBox('enable_reward_shop', ($enable_reward_shop === 'on') ? true : false, array(
                                'name' => 'enable_reward_shop',
                            ));
                            echo CHtml::label(Yii::t('gamification', 'Enable reward shop and allow users to use points to get reward prizes'), 'enable_reward_shop', array(
                                'style' => 'display: inline-block'
                            ));
                            ?>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <br>
    <div class="row-fluid right">
        <?= CHtml::submitButton(Yii::t('app7020','Save Changes'), array(
            'class' => 'btn btn-docebo green big',
            'name' => 'submit'
        )); ?>
        <?= CHtml::submitButton(Yii::t('app7020','Cancel'), array(
            'class' => 'btn btn-docebo black big',
            'name' => 'cancel'
        )); ?>
    </div>
    <?= CHtml::endForm(); ?>
</div>

<script type="text/javascript">
    $(function(){
        $('input[name=show_not_awarded], input[name=enable_reward_shop]').styler();

        if ( $(".alert-success").length) {
            setInterval(function(){
                $(".alert-success").fadeOut(1500);
            }, 3000);
        }
    })
</script>
