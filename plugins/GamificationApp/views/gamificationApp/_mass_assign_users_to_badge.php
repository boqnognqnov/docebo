<script type='text/javascript'>
	$(function(){
		$.post('<?=Docebo::createLmsUrl('progress/run')?>', {
			'type': <?=Progress::JOBTYPE_ASSIGN_USERS_TO_BADGE?>,
			'users': '<?= $users ?>',
			'id_badge': '<?= $id_badge ?>'
		}, function(data){
			var dialogId = 'assign-users-to-badge-progressive';

			$('<div/>').dialog2({
				autoOpen: true, // We will open the dialog later
				id: dialogId,
				title: '<?=Yii::t('gamification', 'Assign user to badge manually')?>'
			});
			$('#'+dialogId).html(data);
		});
	})
</script>
