<div class="main-actions clearfix">
	<h3 class="title-bold"><?php echo Yii::t('gamification', 'Badges'); ?></h3>
	<ul class="clearfix" data-bootstro-id="bootstroNewCourse">
		<li id="create_badge">
			<div>
				<a href="<?= $this->createUrl('GamificationApp/edit'); ?>" class="additional-fields" alt="<?= Yii::t('helper', 'New badge - gamification'); ?>">
					<span></span><?= Yii::t('gamification', 'New badge'); ?>
				</a>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4></h4>
			<p></p>
		</div>
	</div>
</div>