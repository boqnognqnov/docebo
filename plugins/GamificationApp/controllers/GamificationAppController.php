<?php
/**
 * Main gamification controller
 * Class GamificationAppController
 */
class GamificationAppController extends AdminController
{
	/**
	 * @var array List of collected gamification events
	 */
	public $event_list;
	protected $default_image;
	protected $flag_image;
	protected $default_icon;

	protected $activeLeaderbaords = null;

    /**
     * Initializes the controller
     */
    public function init()
	{
		parent::init();

		//$this->default_icon = 'badge_r1_c4.png';
		//$this->default_image = Yii::app()->baseUrl.'/../plugins/GamificationApp/assets/images/badges/'.$this->default_icon;

		// d94f8ae514f41d7e046c774e1d9fee2a.png is MD5() of 'badge_r1_c4.png'
		$asset = CoreAsset::model()->findByAttributes(array('filename' => 'd94f8ae514f41d7e046c774e1d9fee2a.png', 'type' => CoreAsset::TYPE_GAMIFICATION_BADGES));
		if (!$asset) {
			$asset = CoreAsset::model()->findByAttributes(array('type' => CoreAsset::TYPE_GAMIFICATION_BADGES));
		}
		if ($asset) {
			$this->default_icon = $asset->id;
			$this->default_image = $asset->getUrl();
		}
		else {
			$this->default_icon = null;
			$this->default_image = Yii::app()->baseUrl.'/../plugins/GamificationApp/assets/images/badge_blank.png';
		}

		$this->flag_image = Yii::app()->baseUrl.'/../plugins/GamificationApp/assets/images/badge_flag.png';
		JsTrans::addCategories("user_management");
	}

	/**
	 * Register module specific assets
	 *
	 * (non-PHPdoc)
	 * @see LoBaseController::registerResources()
	 */
	public function registerResources()
	{
		if(!Yii::app()->request->isAjaxRequest)
		{
			$assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias("plugin.GamificationApp.assets"));
			//Load some specific assets needed
			$cs = Yii::app()->getClientScript();
			$cs->registerCssFile($assetsUrl.'/css/fcbkcomplete.css');
			$cs->registerCssFile($assetsUrl.'/css/gamification.css');
			$cs->registerScriptFile($assetsUrl.'/js/jquery.fcbkcomplete.js');
			Yii::app()->tinymce->init();
		}
	}

    /**
     * Filters actions based on permissions
     * @return array
     */
    public function filters()
	{
		return array(
			'accessControl' => 'accessControl',
		);
	}

	/**
	 * Returns list of allowed actions.
	 * @return array actions configuration.
	 */
	public function accessRules()
	{
		$res = array();

		$res[] = array(
			'allow',
			'roles' => array('/framework/admin/gamificationapp/view')
		);

        $res[] = array(
            'allow',
            'users' => array('@'),
            'actions' => array('myBadges', 'sendMessage')
        );

		$res[] = array(
			'deny',
			'users' => array('*'),
			'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);

		return $res;
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionSettings(){
		$showNotYetAwarded = PluginSettings::get('show_not_awarded', 'GamificationApp');
		$showNotYetAwarded = !empty($showNotYetAwarded) && ($showNotYetAwarded == 'on' || $showNotYetAwarded == 'off') ? $showNotYetAwarded : false;

		$enable_reward_shop = PluginSettings::get('enable_reward_shop', 'GamificationApp');
		$enable_reward_shop = !empty($enable_reward_shop) && ($enable_reward_shop == 'on' || $enable_reward_shop == 'off') ? $enable_reward_shop : 'off';

		if(isset($_POST['submit'])){
			if(isset($_POST['enable_reward_shop'])){
				$enable_reward_shop = Yii::app()->request->getParam('enable_reward_shop', 0);
				PluginSettings::save('enable_reward_shop', (($enable_reward_shop == 0) ? 'off' : 'on'), 'GamificationApp');
			} else {
				PluginSettings::save('enable_reward_shop', 'off', 'GamificationApp');
			}

			if(isset($_POST['show_not_awarded'])){
				$showNotAwardedReceived = Yii::app()->request->getParam('show_not_awarded', false);
				PluginSettings::save('show_not_awarded', ($showNotAwardedReceived !== false ? 'on' : 'off'), 'GamificationApp');

				Yii::app()->user->setFlash('success', Yii::t("standard", '_OPERATION_SUCCESSFUL'));
				$this->redirect(Docebo::createAdminUrl('//GamificationApp/GamificationApp/settings'));
			} else{
				PluginSettings::save('show_not_awarded', 'off', 'GamificationApp');

				Yii::app()->user->setFlash('success', Yii::t("standard", '_OPERATION_SUCCESSFUL'));
				$this->redirect(Docebo::createAdminUrl('//GamificationApp/GamificationApp/settings'));
			}
		}

		$this->render('settings', array(
			'showNotYetAwarded' => $showNotYetAwarded,
			'enable_reward_shop' => $enable_reward_shop
		));
	}

	public function actionLeaderboard(){
        // FancyTree initialization
        Yii::app()->fancytree->init();

		UsersSelector::registerClientScripts();
		$cs = Yii::app()->getClientScript();
		$themeUrl = Yii::app()->theme->baseUrl;
		$cs->registerScriptFile($themeUrl . '/js/jwizard.js');


		$this->render('leaderboard');
	}

    /**
     * Edit badge action controller
     */
    public function actionEdit()
	{
        // Collect gamification events (to populate the dropdown)
        GamificationAppModule::fetchGamificationEvents($this->event_list);

        // Load badge model
		$id_badge = Yii::app()->request->getParam('id_badge', 0);
        if($id_badge == 0) // New badge
            $model = new GamificationBadge();
        else // Existing badge
            $model = GamificationBadge::model()->findByPk($id_badge);

        // Load active language
		$langs = CoreLangLanguage::getActiveLanguages();
		$defaultLang = Settings::get('default_language', 'english');
		$langs[$defaultLang] = $langs[$defaultLang] . " (".Yii::t('standard', '_DEFAULT_LANGUAGE').")";

        // Are we saving form?
		if(isset($_POST['save']))
		{
			$icon = Yii::app()->request->getParam('icon', $this->default_icon);
			$points = Yii::app()->request->getParam('points', 0);
			$association_mode = Yii::app()->request->getParam('association_mode', 'manual');

            if($association_mode == 'manual') {
                $model->event_name = $association_mode;
                $model->event_params = CJSON::encode(array());
            } else {
                $event = Yii::app()->request->getParam('event', null);
                $params = Yii::app()->request->getParam('params', array());
                $model->event_name = $event;
                $model->event_params = CJSON::encode($params);
            }

            // Save badge model values
            $model->icon = $icon;
            $model->score = $points;
			if($model->save())
			{
                // Load translations (if updating badge)
                $translations = array();
                if($id_badge) {
                    $translationModels = GamificationBadgeTranslation::model()->findAllByAttributes(array('id_badge' => $id_badge));
                    foreach($translationModels as $translation)
                        $translations[$translation->lang_code] = $translation;
                }

                // Save existing translations and create missing ones
				foreach($langs as $lang_code => $lang) {
                    $model_t = isset($translations[$lang_code]) ? $translations[$lang_code] : null;
					if(!$model_t) {
						$model_t = new GamificationBadgeTranslation();
					    $model_t->id_badge = $model->getPrimaryKey();
					    $model_t->lang_code = $lang_code;
                    }

                    $name = Yii::app()->request->getParam('name_'.$lang_code, '');
                    $description = Yii::app()->request->getParam('description_'.$lang_code, '');
					$model_t->name = $name;
					$model_t->description = $description;
					$model_t->save();
				}

                Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));
				$this->redirect('?r=GamificationApp/GamificationApp/index');
			} else
                Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
		}

        unset(Yii::app()->session['current_badge_icon']);
		if(!$model->id_badge)
			$model->event_name = 'manual';
		else {
            $this->default_icon = $model->icon;
			$this->default_image = $model->getIconUrl();
		}

		$this->render('edit', array(
            'langs' => $langs,
            'default_image' => $this->default_image,
            'flag_image' => $this->flag_image,
            'default_icon' => $this->default_icon,
            'events' => $this->event_list,
            'model' => $model
        ));
	}

    /**
     * Deletes a badge
     */
    public function actionDelete()
	{
		$id_badge = Yii::app()->request->getParam('id_badge', 0);
		$gamificationBadge = GamificationBadge::model()->findByPk($id_badge);
		if(empty($gamificationBadge))
			$this->sendJSON(array());

		if(!empty($_POST['GamificationBadge']) && $_POST['GamificationBadge']['confirm']) {
            $gamificationBadge->delete();
			$this->sendJSON(array());
		}

		if(Yii::app()->request->isAjaxRequest)
		{
			$content = $this->renderPartial('_delete', array(
				'model' => $gamificationBadge,
				'model_t' => GamificationBadgeTranslation::model()->findByAttributes(array('id_badge' => $id_badge, 'lang_code' => Lang::getCodeByBrowserCode(Yii::app()->getLanguage()))),
			), true);

			$this->sendJSON(array(
				'html' => $content,
			));
		}

		Yii:app()->end();
	}

    /**
     * Shows the image selector modal
     */
    public function actionImageBlock()
	{
        // Modal is being rendered -> load the model
        $id_badge = Yii::app()->request->getParam('id_badge', 0);
        if($id_badge == 0)
            $model = GamificationBadge::model();
        else
            $model = GamificationBadge::model()->findByPk($id_badge);

        // Check if an image was selected and modal is about to be closed
        if (!empty($_POST['slider'])) {
            $model->icon = $_POST['slider'];
            Yii::app()->session['current_badge_icon'] = $model->icon;
            $result = array(
                'icon' => $model->icon,
                'url' => $model->getIconUrl()
            );

            $this->sendJSON($result);
        } else if (Yii::app()->request->isAjaxRequest) {
            // Check if another icon was selected for the current badge
            $current_badge_icon = Yii::app()->session['current_badge_icon'];
            if($current_badge_icon)
                $model->icon = $current_badge_icon;

            // Model loaded?
            if($model) {
                // Prepare thumbnails and params fro the slider
                list($count, $defaultImages, $userImages, $selected) = $this->prepareThumbnails($model);
                $uniqueId = substr(md5(uniqid(rand(), true)), 0, 10);
                $selected_tab = Yii::app()->request->getParam('selected_tab', null);
                $params = array(
                    'selectedTab' => ($selected_tab) ? $selected_tab : $selected,
                    'count' => $count,
                    'defaultImages' => $defaultImages,
                    'userImages' => $userImages,
                    'model' => $model,
                    'uniqueId' => $uniqueId
                );

                // Render the modal
                $this->sendJSON(array('html' => $this->renderPartial('_slider', $params, true)));
            }
        }

		Yii::app()->end();
	}

    /**
     * Prepare the list of thumbnails for the icon selector
     * @param $model
     * @return array
     */
    private function prepareThumbnails($model)
	{
		$gallerySize = 27;
		$path = Yii::app()->getBasePath(true).'/../../plugins/GamificationApp/assets/images';


		// Get list of URLs of SHARED and USER player backgrounds
		$assets = new CoreAsset();
		$defaultImages   = $assets->urlList(CoreAsset::TYPE_GAMIFICATION_BADGES, CoreAsset::SCOPE_SHARED, false, CoreAsset::VARIANT_SMALL);
		$userImages   	= $assets->urlList(CoreAsset::TYPE_GAMIFICATION_BADGES, CoreAsset::SCOPE_PRIVATE, false, CoreAsset::VARIANT_SMALL);


		$count = array(
			'default' => count($defaultImages),
			'user' => count($userImages),
		);

		foreach($defaultImages as $key => $value) {
			if($key == $model->icon) {
				$selected = 'default';
				$this->reorderArray($defaultImages, $key, $value);
				break;
			}
		}

		if(empty($selected)) {
			foreach($userImages as $key => $value) {
				if($key == $model->icon) {
					$selected = 'user';
					$this->reorderArray($userImages, $key, $value);
					break;
				}
			}
		}

		$selected = empty($selected) ? 'default' : $selected;

		$defaultImages = array_chunk($defaultImages, $gallerySize, true);
		$userImages = array_chunk($userImages, $gallerySize, true);

		return array($count, $defaultImages, $userImages, $selected);
	}

    /**
     * Uploads a user badge icon
     * @throws Exception
     */
    public function actionUploadUserBadge() {
        if (Yii::app()->request->isAjaxRequest) {
            if (!empty($_FILES['attachment'])) {
                $adminRoot = Yii::getPathOfAlias('admin');

                // Validate file type (image)
                $validator = new FileTypeValidator(FileTypeValidator::TYPE_IMAGE);
                if (!$validator->validateFile('attachment'))
                    throw new Exception($validator->errorMsg, 1);


                $uploadedFile = CUploadedFile::getInstanceByName('attachment');
                $asset = new CoreAsset();
                $asset->type = CoreAsset::TYPE_GAMIFICATION_BADGES;
                $asset->sourceFile = $uploadedFile;
                $asset->save();

                // Return empty json array
                $this->sendJSON(array());

            }
        }
        Yii::app()->end();
    }

    /**
     * Autocomplete badge search input
     */
    public function actionAutocomplete()
	{
        $term 			= Yii::app()->request->getParam('term', false);

        if(!$term) {
            $inputData = Yii::app()->request->getParam('data', array());
            $output = array();

            if (!empty($inputData)) {
                $criteria = new CDbCriteria();
                $criteria->addCondition("(name LIKE :search) OR (description LIKE :search)");
                $criteria->params[':search'] = '%' . $inputData['query'] . '%';

                $badges = GamificationBadgeTranslation::model()->findAll($criteria);
                foreach ($badges as $badge)
                    $output[] = $badge->name;
            }

            $this->sendJSON(array('options' => $output));
        }
        else {
            $maxNumber = Yii::app()->request->getParam('max_number', 10);
//            $term = Yii::app()->request->getParam('term', false);

            $model = new GamificationBadge();

            $model->search_input = $term ? $term : null;

            $dataProvider = $model->sqlDataProvider();
            $dataProvider->setPagination(array(
                'pageSize' => $maxNumber,
            ));
            $data = $dataProvider->getData();

            $result = array();
            if ($data) {
                foreach ($data as $item) {
                    $result[] = array(
                        'label' => $item['name'],
                        'value' => $item['name'],
                    );
                }
            }
            echo CJSON::encode($result);
        }
	}

    /**
     * Renders manual badge assignment page
     */
    public function actionAssign()
	{
		$id_badge = Yii::app()->request->getParam('id_badge', 0);

		if($id_badge == 0)
			$this->redirect('?r=GamificationApp/GamificationApp/index');

		$gamificationAssignedBadges = new GamificationAssignedBadges();
		$gamificationAssignedBadges->coreUser = new CoreUser();
		$gamificationAssignedBadges->id_badge = $id_badge;
		if(Yii::app()->request->isAjaxRequest && isset($_REQUEST['CoreUser']))
			$gamificationAssignedBadges->coreUser->search_input = $_REQUEST['CoreUser']['search_input'];

		$params = array(
			'id_badge' => $id_badge,
			'gamificationAssignedBadges' => $gamificationAssignedBadges
		);


		UsersSelector::registerClientScripts();

		$this->render('assign', $params);
	}

	public function actionDeleteAssignedBadge()
	{
		$id_badge = Yii::app()->request->getParam('id_badge', 0);
		$id_user = Yii::app()->request->getParam('id_user', 0);

		$gamificationBadge = GamificationBadge::model()->findByPk($id_badge);
		$coreUser = CoreUser::model()->findByPk($id_user);
		if(empty($gamificationBadge) || empty($coreUser))
			$this->sendJSON(array());

		if(!empty($_POST['GamificationBadge']))
		{
			if($_POST['GamificationBadge']['confirm'])
			{
				GamificationAssignedBadges::model()->deleteAllByAttributes(array('id_badge' => $id_badge, 'id_user' => $id_user));
				$this->sendJSON(array());
			}
		}

		if(Yii::app()->request->isAjaxRequest)
		{
			$content = $this->renderPartial('_deleteAssign', array(
				'model' => $gamificationBadge,
				'model_t' => GamificationBadgeTranslation::model()->findByAttributes(array('id_badge' => $id_badge, 'lang_code' => Lang::getCodeByBrowserCode(Yii::app()->getLanguage()))),
				'model_u' => $coreUser
			), true);
			$this->sendJSON(array(
				'html' => $content,
			));
		}

		Yii:app()->end();
	}

	public function actionDeleteMultiAssignedBadge()
	{
		$id_badge = Yii::app()->request->getParam('id_badge', 0);
		$id_users = Yii::app()->request->getParam('ids', '');

		$gamificationBadge = GamificationBadge::model()->findByPk($id_badge);
		if(empty($gamificationBadge))
			$this->sendJSON(array());

		if(!empty($_POST['GamificationBadge']))
		{
			if($_POST['GamificationBadge']['confirm'])
			{
				$html = $this->renderPartial('_mass_delete_multi_assigned_badge', array(
					'users'=> $id_users,
					'id_badge'=>$id_badge,
				), true);

				$this->sendJSON(array('html'=>$html.'<a class="auto-close"></a>'));
				Yii::app()->end();
			}
		}

		if(Yii::app()->request->isAjaxRequest)
		{
			$content = $this->renderPartial('_deleteMultiAssign', array(
				'model' => $gamificationBadge,
				'model_t' => GamificationBadgeTranslation::model()->findByAttributes(array('id_badge' => $id_badge, 'lang_code' => Lang::getCodeByBrowserCode(Yii::app()->getLanguage()))),
				'id_badge' => $id_badge,
				'id_users' => $id_users
			), true);
			$this->sendJSON(array(
				'html' => $content,
			));
		}

		Yii:app()->end();
	}

	public function actionAxProgressiveDeleteMultiAssignedBadge() {
		$chunkSize = 200;
		$userIdsThisIteration = array();
		$users = $_POST['users'];
		$id_badge = $_POST['id_badge'];

		// Check if this is string
		if(is_string($users))
		{
			// if is only one user
			if(!strpos($users,',') !== false)
				$userList = array($users);
			else
				$userList = explode(',', $users);
		}
		else{
			$this->sendJSON(array());
		}

		// Get current offset and calculate stop flag
		$offset = Yii::app()->request->getParam('offset', 0);
		$totalCount = count($userList);
		$newOffset = $offset + $chunkSize;
		$stop = ($newOffset < $totalCount) ? false : true;
		$counter = 0;
		for ($index = $offset; ($index < $newOffset && $index < $totalCount); $index++) { //NOTE: $newOffset may be greater than $totalCount, but we wouldn't count over $totalCount
			$counter ++;
			if ( $counter > $chunkSize ) break; //NOTE: this shouldn't happen
			// Get current enrollment "job". If we are over the limit, invalidate value and stop this iteration
			$userId = ( isset( $userList[ $index ] ) ? $userList[ $index ] : false );
			if (!$userId || empty($userId))continue;
			$userIdsThisIteration[] = $userId;
		}

		if(!empty($userIdsThisIteration)) {
			GamificationAssignedBadges::model()->deleteAllByAttributes(array('id_badge' => $id_badge), 'id_user in ('.implode(',', $userIdsThisIteration).')');
		}

		$completedPercent = $totalCount==0 ? 100 : $index/$totalCount*100;
		if($completedPercent>100) $completedPercent = 100;
		if($completedPercent<0) $completedPercent = 0;

		$html = $this->renderPartial('_progressive_delete_multi_assigned_badge', array(
			'offset' 				=> $offset,
			'processedUsers' 		=> $index,
			'totalUsers' 			=> $totalCount,
			'completedPercent' 		=> intval($completedPercent),
			'start' 				=> ($offset == 0),
			'stop' 					=> $stop,
		), true);

		// Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
		// These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stopping when $stop == true)
		$data = array(
			'offset'				=> $newOffset,
			'stop'					=> $stop,
			'users'					=> $users,
			'id_badge'				=> $id_badge,
		);

		$response = new AjaxResult();
		$response->setStatus(true);
		$response->setHtml($html)->setData($data)->toJSON();
		Yii::app()->end();
	}

    /**
     * Shows the issued bagdes modal
     */
    public function actionBadgeAssignDetails()
	{
		$id_badge = Yii::app()->request->getParam('id_badge', 0);
		$id_user = Yii::app()->request->getParam('id_user', 0);

		$gamificationBadge = GamificationBadge::model()->findByPk($id_badge);
		$coreUser = CoreUser::model()->findByPk($id_user);
		if(empty($gamificationBadge) || empty($coreUser))
			$this->sendJSON(array());

		$gamificationAssignedBadges = GamificationAssignedBadges::model();
		$gamificationAssignedBadges->id_badge = $id_badge;
		$gamificationAssignedBadges->id_user = $id_user;

		if(Yii::app()->request->isAjaxRequest)
		{
            $processOutput = true;
            if (isset($_REQUEST['contentType']) && ($_REQUEST['contentType'] == 'html'))
                $processOutput = false;

			Yii::app()->clientScript->scriptMap['pager.css'] = false;
            $content = $this->renderPartial('_assignDetails', array(
                'model' => $gamificationBadge,
                'model_t' => GamificationBadgeTranslation::model()->findByAttributes(array('id_badge' => $id_badge, 'lang_code' => Lang::getCodeByBrowserCode(Yii::app()->getLanguage()))),
                'model_u' => $coreUser,
                'gamificationAssignedBadges' => $gamificationAssignedBadges
            ), true, $processOutput);

            if (isset($_REQUEST['contentType']) && ($_REQUEST['contentType'] == 'html')) {
                echo $content;
            } else {
                $this->sendJSON(array(
                    'html' => $content
                ));
            }
		}

		Yii::app()->end();
	}

	public function actionAssignSingle()
	{
		$id_badge = Yii::app()->request->getParam('id_badge', 0);
		$id_user = Yii::app()->request->getParam('id_user', 0);
		$gamificationBadge = GamificationBadge::model()->findByPk($id_badge);
		if(empty($gamificationBadge))
			$this->sendJSON(array());

		if($_POST['confirm'])
		{
			$model_a = new GamificationAssignedBadges();
			$model_a->id_badge = $id_badge;
			$model_a->id_user =  $id_user;
			$model_a->issued_on = Yii::app()->localtime->getLocalNow();
			$model_a->score = $gamificationBadge->score;
			$model_a->event_name = 'manual';
			$model_a->event_key = 'Assigned manually';
			$model_a->event_params = CJSON::encode(array());
			$model_a->save();

			$event = new DEvent($this, array(
				'id_user' => $id_user,
				'badge' => $gamificationBadge
			));
			Yii::app()->event->raise('OnBeforeBadgeAssign', $event);

			$this->sendJSON(array('status' => 'saved'));
			Yii::app()->end();
		}

		if(Yii::app()->request->isAjaxRequest)
		{
			//Confirm
		}

		Yii:app()->end();
	}

	public function actionDeleteAssociationSingle()
	{
		$id_badge = Yii::app()->request->getParam('id_badge', 0);
		$id_user = Yii::app()->request->getParam('id_user', 0);
		$issued_on = Yii::app()->request->getParam('issued_on', '0000-00-00 00:00:00');

		$gamificationBadge = GamificationBadge::model()->findByPk($id_badge);
		$coreUser = CoreUser::model()->findByPk($id_user);
		//$gamificationAssignedBadges = GamificationAssignedBadges::model()->find('id_badge = :id_badge AND id_user = :id_user AND issued_on = :issued_on', array(':id_badge' => $id_badge, ':id_user' => $id_user, ':issued_on' => Yii::app()->localtime->fromLocalDateTime(str_replace('_', ' ', $issued_on))));

		$criteria = new CDbCriteria();
		$criteria->addCondition('id_badge = :id_badge');
		$criteria->params[':id_badge'] = $id_badge;
		$criteria->addCondition('id_user = :id_user');
		$criteria->params[':id_user'] = $id_user;
		$criteria->addCondition('issued_on = :issued_on');
		$criteria->params[':issued_on'] = Yii::app()->localtime->fromLocalDateTime(str_replace('_', ' ', $issued_on));

		$gamificationAssignedBadges = GamificationAssignedBadges::model()->find($criteria);

		if(empty($gamificationBadge) || empty($coreUser) || empty($gamificationAssignedBadges))
			$this->sendJSON(array('success' => false));

		if(isset($_POST['confirm']) && $_POST['confirm'] == 1)
		{
			$gamificationAssignedBadges->issued_on = Yii::app()->localtime->fromLocalDateTime($gamificationAssignedBadges->issued_on);
			$date = new DateTime($gamificationAssignedBadges->issued_on);
			$gamificationAssignedBadges->issued_on = $date->format('Y-m-d H:i:s');
			$this->sendJSON(array('success' => $gamificationAssignedBadges->delete()));
			Yii::app()->end();
		}

		if(Yii::app()->request->isAjaxRequest)
		{
			$content = $this->renderPartial('_deleteAssignSingle', array(
				'model' => $gamificationBadge,
				'model_t' => GamificationBadgeTranslation::model()->findByAttributes(array('id_badge' => $id_badge, 'lang_code' => Lang::getCodeByBrowserCode(Yii::app()->getLanguage()))),
				'model_u' => $coreUser,
				'model_a' => $gamificationAssignedBadges
			), true);
			echo $content;
		}

		Yii::app()->end();
	}

	public function actionAssignSelector()
	{
		$id_badge = Yii::app()->request->getParam('id_badge', 0);

		$user = CoreUser::model();
		$user->scenario = 'search';
		$user->unsetAttributes();
		if(isset($_REQUEST['CoreUser']))
			$user->attributes = $_REQUEST['CoreUser'];

		$userlist = array();

		// if isset selected users
		if(isset($_REQUEST['courseEnroll-user-grid-selected-items']) && $_REQUEST['courseEnroll-user-grid-selected-items'] != '')
			$userlist = explode(',', $_REQUEST['courseEnroll-user-grid-selected-items']);

		if(isset($_REQUEST['courseEnroll-orgchart']))
		{
			$orgChartGroups = $_REQUEST['courseEnroll-orgchart'];

			$allNodes = array();
			// Get the root node to handle the "allnodes" case
			$rootOrgNode = CoreOrgChartTree::getOrgRootNode();
			foreach($orgChartGroups as $id => $value)
			{
				if(($id == $rootOrgNode->idOrg) && ($value != '0'))
				{
					$allNodes = 'allnodes';
					break;
				}
				if($value == '1')
				{
					if(!in_array($id, $allNodes))
						$allNodes[] = $id;
				}
				elseif($value == '2')
				{
					$allChildrenNodesIds = CoreUser::model()->getCurrentAndChildrenNodesList($id);
					if(!empty($allChildrenNodesIds))
					{
						foreach($allChildrenNodesIds as $childId)
						{
							if(!in_array($childId, $allNodes))
								$allNodes[] = $childId;
						}
					}
				}
			}

			if(!empty($allNodes))
			{
				$criteria = new CDbCriteria();

				// criteria for all users not in current course
				$criteriaAll = CoreUser::model()->dataProviderEnroll(Yii::app()->session['currentCourseId'])->criteria;

				if(($allNodes !== 'allnodes') && (is_array($allNodes)))
				{
					// criteria for selected nodes
					$criteria->with = array(
						'orgChartGroups' => array(
							'joinType' => 'INNER JOIN',
							'condition' => 'orgChartGroups.idOrg IN (\''.implode('\',\'', $allNodes).'\')',
						)
					);
					$criteria->together = true;
				}
				else
					$criteria = $criteriaAll;

				$users = CoreUser::model()->findAll($criteria);
				foreach($users as $user)
				{
					if(!in_array($user->idst, $userlist))
						$userlist[] = $user->idst;
				}
			}
		}

		$group = new CoreGroup();
		if(isset($_REQUEST['CoreGroup']))
			$group->attributes = $_REQUEST['CoreGroup'];

		if(isset($_REQUEST['group-grid-selected-items']))
		{
			if($_REQUEST['group-grid-selected-items'] != '')
			{
				$grouplist = $_REQUEST['group-grid-selected-items'];
				$criteria = new CDbCriteria();
				$criteria->with = array(
					'groups' => array(
						'joinType' => 'INNER JOIN',
						'condition' => 'groups.idst IN ('.$grouplist.')',
					),
				);
				$criteria->together = true;
				$users = CoreUser::model()->findAll($criteria);

				foreach($users as $user)
				{
					if(!in_array($user->idst, $userlist))
						$userlist[] = $user->idst;
				}
			}
		}

		if(isset($_POST['courseEnroll-orgchart']))
		{
			if(!empty($userlist))
			{
				$model = GamificationBadge::model()->findByPk($id_badge);

				foreach($userlist as $id_user)
				{
					$model_a = new GamificationAssignedBadges();
					$model_a->id_badge = $id_badge;
					$model_a->id_user =  $id_user;
					$model_a->issued_on = Yii::app()->localtime->getLocalNow();
					$model_a->score = $model->score;
					$model_a->event_name = 'manual';
					$model_a->event_key = 'Assigned manually';
					$model_a->event_params = CJSON::encode(array());
					$model_a->save();
				}
			}

			$this->sendJSON(array('status' => 'saved'));
			Yii::app()->end();
		}

		$orgChartsInfo = CoreOrgChartTree::getFullOrgChartTree(false, true, true);
		$html = $this->renderPartial('_userSelector', array(
			'userModel' => $user,
			'groupModel' => $group,
			'fullOrgChartTree' 	=> $orgChartsInfo['list'],
			'puLeafs'			=> $orgChartsInfo['leafs']
		), true, true);

		if(isset($_REQUEST['contentType']) && ($_REQUEST['contentType'] == 'html'))
			echo $html;
		else
			$this->sendJSON(array('html' => $html));
	}



	/**
	 * Receive Selector's Request data and assign users to a given badge
	 */
	public function actionAssignUsersToBadge() {

		$id_badge = Yii::app()->request->getParam('idBadge', 0);
		$usersList = UsersSelector::getUsersList($_REQUEST);
		$model = GamificationBadge::model()->findByPk($id_badge);

		if($model && !empty($usersList))
		{
			// Progressive assigning users to badge
			$this->renderPartial('_mass_assign_users_to_badge', array(
				'users' => implode(',', $usersList),
				'id_badge' => $id_badge
			));

			$response = new AjaxResult();
			$response->setStatus(true);
			$response->setHtml("<a class='auto-close'></a>")->toJSON();
			Yii::app()->end();
		}


		echo '<a class="auto-close success"></a>';
		Yii::app()->end();

	}

	public function actionAxProgressiveAssignUsersToBadge() {
		$chunkSize = 200;
		$userIdsThisIteration = array();
		$users = $_POST['users'];
		$id_badge = $_POST['id_badge'];

		// Check if this is string
		if(is_string($users))
		{
			// if is only one user
			if(!strpos($users,',') !== false)
				$userList = array($users);
			else
				$userList = explode(',', $users);
		}
		else{
			$this->sendJSON(array());
		}

		// Get current offset and calculate stop flag
		$offset = Yii::app()->request->getParam('offset', 0);
		$totalCount = count($userList);
		$newOffset = $offset + $chunkSize;
		$stop = ($newOffset < $totalCount) ? false : true;
		$counter = 0;
		for ($index = $offset; ($index < $newOffset && $index < $totalCount); $index++) { //NOTE: $newOffset may be greater than $totalCount, but we wouldn't count over $totalCount
			$counter ++;
			if ( $counter > $chunkSize ) break; //NOTE: this shouldn't happen
			// Get current enrollment "job". If we are over the limit, invalidate value and stop this iteration
			$userId = ( isset( $userList[ $index ] ) ? $userList[ $index ] : false );
			if (!$userId || empty($userId))continue;
			$userIdsThisIteration[] = $userId;
		}

		$badge = GamificationBadge::model()->findByPk($id_badge);

		foreach ($userIdsThisIteration as $idUser) {
			$model = new GamificationAssignedBadges();
			$model->id_badge = $id_badge;
			$model->id_user =  $idUser;
			$model->issued_on = Yii::app()->localtime->getLocalNow();
			$model->score = $badge->score;
			$model->event_name = 'manual';
			$model->event_key = 'Assigned manually';
			$model->event_params = CJSON::encode(array());
			$model->save();

			$event = new DEvent($this, array(
				'id_user' => $idUser,
				'badge' => $badge
			));
			Yii::app()->event->raise('OnBeforeBadgeAssign', $event);
		}

		$completedPercent = $totalCount==0 ? 100 : $index/$totalCount*100;
		if($completedPercent>100) $completedPercent = 100;
		if($completedPercent<0) $completedPercent = 0;

		$html = $this->renderPartial('_progressive_assign_users_to_badge', array(
			'offset' 				=> $offset,
			'processedUsers' 		=> $index,
			'totalUsers' 			=> $totalCount,
			'completedPercent' 		=> intval($completedPercent),
			'start' 				=> ($offset == 0),
			'stop' 					=> $stop,
			'afterUpdateCallback'	=> ($stop ? 'var gridId = "badge-assign-list"; $.fn.yiiListView.update(gridId);' : null)
		), true);

		// Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
		// These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stopping when $stop == true)
		$data = array(
			'offset'				=> $newOffset,
			'stop'					=> $stop,
			'users'					=> $users,
			'id_badge'				=> $id_badge,
		);

		$response = new AjaxResult();
		$response->setStatus(true);
		$response->setHtml($html)->setData($data)->toJSON();
		Yii::app()->end();
	}




    /**
     * Return the list of learning objects for a given course
     */
    public function actionGetLearningObjects()
    {
        $result = array(
            'list' => array(),
            'html' => ''
        );

        $course_name = Yii::app()->request->getParam('course_name', null);
        if($course_name) {
            $model = LearningCourse::model()->findByAttributes(array('name' => $course_name));
            if($model) {
                $learning_objects = $model->learningOrganizations;
                foreach($learning_objects as $lo) {
                    if(($lo->objectType !== '') && ($lo->visible == 1)) {
                        $result['html'] .= '<option value="'.$lo->idOrg.'">'.$lo->title.'</option>\r\n';
                        $result['list'][$lo->idOrg] = in_array($lo->objectType, array(
                            LearningOrganization::OBJECT_TYPE_TEST, LearningOrganization::OBJECT_TYPE_DELIVERABLE,
                            LearningOrganization::OBJECT_TYPE_SCORMORG, LearningOrganization::OBJECT_TYPE_TINCAN));
                    }
                }
            }
        }

        $this->sendJSON($result);
    }

    /**
     * Autocompletes course names (used in Student completed learning object)
     */
    public function actionCourseAutocomplete() {
        $query = addcslashes($_REQUEST['data']['query'], '%_');

        $criteria = new CDbCriteria(array(
            'condition' => 'name LIKE :match OR name LIKE :match2',
            'params' => array(
                ':match' => "/%$query%",
                ':match2' => "%$query%"
            ),
        ));

        $criteria->addCondition("course_type = '".LearningCourse::TYPE_ELEARNING."'");
        $courses = LearningCourse::model()->findAll($criteria);
        $coursesList = array_values(CHtml::listData($courses, 'idCourse', 'name'));
        $this->sendJSON(array('options' => $coursesList));
    }

    /**
     * Load event settings for given event
     */
    public function actionGetEventSetting()
	{
        // Load installed events
        GamificationAppModule::fetchGamificationEvents($this->event_list);

        // Check passed params
		$event = Yii::app()->request->getParam('event', '');
		$id_badge = Yii::app()->request->getParam('id_badge', 0);
		$html = '';
		if($event && isset($this->event_list[$event]))
		{

            // Load badge from DB
            $model = GamificationBadge::model()->findByPk($id_badge);
			if(!$model)
				$model = GamificationBadge::model();

            // Create handler
            $eventRecord = $this->event_list[$event];
            if($eventRecord['handler'])
            {
                try {
                    $handler = Yii::createComponent($eventRecord['handler']);
                    if($handler && ($handler instanceof GamificationEventHandler))
                    {
                        $params = array(
                            'badge' => $model,
                            'params' => (is_null($model->event_params) ? array() : CJSON::decode($model->event_params))
                        );
                        // Ask the handler to render the settings view
                        $html = $handler->renderEventSettingsView($params);

                    }
                } catch(CException $e) {
                    Yii::log($e->getMessage(), CLogger::LEVEL_INFO);
                }
            }
		}

		echo $html;
		Yii::app()->end();
	}

    /**
     * Returns a list of courses filtered using search text
     */
    public function actionGetCourseList()
	{
		$filter = Yii::app()->request->getParam('tag', '');

        // Filter courses using text
        $criteria = new CDbCriteria();
        $criteria->select = 'idCourse, code, name';
        if($filter != '')
        {
            $criteria->condition = '(name like :filter) OR (code like :filter)';
            $criteria->params = array(':filter' => '%'.$filter.'%');
        }

        // Return json result
        $rows = LearningCourse::model()->findAll($criteria);
		$res = array();
		foreach($rows as $row)
			$res[] = array(
				'key' => $row['idCourse'],
				'value' => ($row['code'] != '' ? $row['code'].' - ' : '').$row['name']
			);

        $this->sendJSON($res);
	}

    /**
     * Returns a lsit of all Learning Plans in the system
     */
    public function actionGetLearningPlanList() {
        $filter = Yii::app()->request->getParam('tag', '');

        // Filter courses using text
        $criteria = new CDbCriteria();
        $criteria->select = 'id_path, path_code, path_name';
        if($filter != '')
        {
            $criteria->condition = '(path_name like :filter) OR (path_name like :filter)';
            $criteria->params = array(':filter' => '%'.$filter.'%');
        }

        // Return json result
        $rows = LearningCoursepath::model()->findAll($criteria);
        $res = array();
        foreach($rows as $row)
            $res[] = array(
                'key' => $row['id_path'],
                'value' => ($row['path_code'] != '' ? $row['path_code'].' - ' : '').$row['path_name']
            );

        $this->sendJSON($res);
    }

	/**
	 * Get HTML for badges section
	 *
	 * @return string
	 * @throws CException
	 */
	private function renderBadgesTab(){
		$scorePoints = 0;
		$countBadges = 0;
		$showNotAwarded = PluginSettings::get('show_not_awarded', 'GamificationApp');
		$showNotAwarded = !empty($showNotAwarded) && ($showNotAwarded == 'on');

		$badges = GamificationAssignedBadges::model()->getUserBadges(Yii::app()->user->id);

		if($showNotAwarded)
			$notAwarded = GamificationBadge::model()->getAllBadgesWithScore(Yii::app()->user->id);
		else
			$notAwarded = array();

		$allBadges = array_merge($badges, $notAwarded);
		if (count($badges) > 0) {
			foreach ($badges as $badge) {
				$scorePoints += intval($badge->gamificationBadge->badgeCounterScore);
				$countBadges += (is_null($badge->gamificationBadge->badgeCounter) ? 1 : $badge->gamificationBadge->badgeCounter);
			}
		}

		$userHasBadges = !empty($badges);

		return $this->renderPartial('_badges', array(
			'countBadges' => $countBadges,
			'countPoints' => $scorePoints,
			'badges' => $badges,
			'allBadges' => $allBadges,
			'notAwarded' => $notAwarded,
			'showNotAwarded' => $showNotAwarded,
			'userHasBadges' => $userHasBadges
		), true);
	}

	/**
	 * Get HTML for leaderboars section
	 *
	 * @return string
	 * @throws CException
	 */
	private function renderLeaderboadTab(){
		$hasActiveLeaderboards = !empty($this->activeLeaderbaords) ? true : false;
		$leaderboardsChartData = array();

		$leaderboards = array();

		if($hasActiveLeaderboards){
			$command = Yii::app()->db->createCommand();
			$command->select('t.id as id, tr.name as name')
				->from('gamification_leaderboard t')
				->leftJoin('gamification_leaderboard_translation tr', 'tr.id_board = t.id and tr.lang_code = "' . Lang::getCodeByBrowserCode(Yii::app()->getLanguage()) . '"')
				->where('t.id IN ( ' . implode(',', $this->activeLeaderbaords['leaderboards']) . ')')
				->group('t.id');
			$leaderboards = $command->queryAll();
            
            foreach($leaderboards as $key => $board) {
                if(empty($board['name'])) {
                    $b = GamificationLeaderboardTranslation::model()->findByAttributes(array('id_board' => $board['id'], 'lang_code' => Settings::get('default_language')));
                    $leaderboards[$key]['name'] = $b->name;
                }
            }
            
			foreach($this->activeLeaderbaords['leaderboards'] as $leaderboard){
				$leaderboardsChartData[$leaderboard]['points'] = GamificationAssignedBadges::getPointsChart(10, $this->activeLeaderbaords['users'][$leaderboard]);
				$leaderboardsChartData[$leaderboard]['userPoints'] = GamificationAssignedBadges::getPointsChartPosition(Yii::app()->user->id, $this->activeLeaderbaords['users'][$leaderboard]);
			}
		}



		return $this->renderPartial('_leaderboards', array(
			'hasActiveLeaderboard' => $hasActiveLeaderboards,
			'leaderboards' => $leaderboards,
			'leaderboardChartData' => $leaderboardsChartData
		), true);
	}

	/**
	 * Get HTML for Contests section
	 *
	 * @return string
	 * @throws CException
	 */
	private function renderContestsTab(){
		return $this->renderPartial('_contests', array(), true, true);
	}

	/**
	 * Here we do some optimization, because a lot of contest, badges and Leaderboards, and the page is getting ready really slowly
	 */
    public function actionMyBadges()
    {
		if(!Yii::app()->request->isAjaxRequest) {
			/* @var $cs CClientScript */
			$cs = Yii::app()->getClientScript();
//			$cs->registerCoreScript('jquery.ui');
//			$cs->registerCssFile($cs->getCoreScriptUrl() . '/jui/css/base/jquery-ui.css');

			// admin.css
			$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/admin.css');
		}

		$selectedTab = 'default';
		$selectedTab = Yii::app()->request->getParam('tab', $selectedTab);
		$hasActiveLeaderboards = false;
		$html = array();

		if(Yii::app()->request->isAjaxRequest && ($selectedTab === 'default' || $selectedTab == 'contests')){

		} else{
			$html = array();
			$this->activeLeaderbaords = GamificationLeaderboard::getActiveLeaderboards(Yii::app()->user->id);
			$hasActiveLeaderboards = !empty($this->activeLeaderbaords) ? true : false;
		}

		switch($selectedTab){
			case 'default':
				$html[$selectedTab] = $this->renderBadgesTab();
				break;
			case 'chart':
				$html[$selectedTab] = $this->renderLeaderboadTab();
				break;
			case 'contests':
				$html[$selectedTab] = $this->renderContestsTab();
				break;
			case 'rewards':
				$html[$selectedTab] = $this->renderRewardsTab();
				break;
		}

		// Should we show My Rewards History tabs ?
		$enable_reward_shop = PluginSettings::get('enable_reward_shop', 'GamificationApp');
		$enable_reward_shop =  (!$enable_reward_shop || $enable_reward_shop == 'off') ? 'off' : 'on';
		$userSets = GamificationRewardsSet::getUserSets(Yii::app()->user->idst);

		if(Yii::app()->request->isAjaxRequest){
			$this->sendJSON($html[$selectedTab]);
		} else{
			$this->render('my_badges', array(
				'html' => $html,
				'selectedTab' => $selectedTab,
				'userSets' => $userSets,
				'enable_reward_shop' => $enable_reward_shop,
				'hasActiveLeaderboard' => $hasActiveLeaderboards
			));
		}
    }

	/**
	 * Get HTML for Rewards tab section
	 *
	 * @return string
	 * @throws CException
	 */
	private function renderRewardsTab(){
		return $this->renderPartial('_rewards', array(), true, true);
	}

	public function actionSendMessage(){
		$request = Yii::app()->request;

		/**
		 * @var $request CHttpRequest
		 */

		$id = $request->getParam('id', false);

		$gamificationUserRewards = null;

		if($id){
			$gamificationUserRewards = GamificationUserRewards::model()->findByPk($id);
		} else{
			$gamificationUserRewards = new GamificationUserRewards();
		}

		$save = $request->getParam('save', false);
		$name = $request->getParam('name', false);
		$message = $request->getParam('message', false);
		$sendByEmail = $request->getParam('sendByEmail', false);
		$rewardId = $request->getParam('rewardId', false);

		if($save){
			// Send emails to administrators
			// Get reward model
			$reward = GamificationReward::model()->findByPk($rewardId);

			// Get the receiver email
			$toAdminEmail = array();

			$requestStatus = $gamificationUserRewards->status;
			if($requestStatus != GamificationUserRewards::$STATUS_APPROVED) {
				$godAdminEmails = Yii::app()->user->getGodadminsEmails();
				if (is_array($godAdminEmails) && (count($godAdminEmails) > 0 )) {
					foreach($godAdminEmails as $godAdminEmail) {
						if(!empty($godAdminEmail)) {
							$toAdminEmail[] = $godAdminEmail;
						}
					}
				}
			} else {
				// Default Settings Email to be send to
				$defaultSenderEmail = Settings::get('mail_sender');
				if(!empty($defaultSenderEmail)) {
					$toAdminEmail[] = $defaultSenderEmail;
				}

				// The reward is approved by admin (id)
				$approvedByAdminId = $gamificationUserRewards->modified_by;

				if(intval($approvedByAdminId)) {
					$approvedByAdmin = CoreUser::model()->findByPk($approvedByAdminId);

					if($approvedByAdmin && $approvedByAdmin->email != '') {
						$toAdminEmail = array($approvedByAdmin->email);
					}
				}
			}

			// If there are administrators emails found proceed with sending the email
			if (count($toAdminEmail) > 0) {
				// Send the email object
				$mm = new MailManager();
				$mm->setContentType('text/html');

				// Email subject
				$mailSubject = Yii::t('gamification', 'Gamification Reward message for reward: "' . $reward->getName() .
						'"(Request ID: ' . $gamificationUserRewards->id . ') by ' . $name);

				// Send the email
				$result = $mm->mail(array($sendByEmail), $toAdminEmail, $mailSubject, $message);
				if (!$result)
					Yii::log('Failed to send session change email to enrolled users', 'error');
			}

			echo "<a class='auto-close'></a>";
			Yii::app()->end();
		}

		$this->renderPartial('_send_message', array(
				'gamificationUserRewards' => $gamificationUserRewards
		));
	}

	/**
	 * Before Render
	 *
	 *  we need to register resources here in order the resources to be registered only if there is something to render !!!
	 * @param $view
	 * @return bool
	 */
	public function beforeRender($view) {
		$this->registerResources();
		return parent::beforeRender($view);
	}
}
