<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 27.11.2015 г.
 * Time: 11:36
 */
class LeaderboardWizardController extends DoceboWizardController
{

    /**
     * Filters actions based on permissions
     * @return array
     */
    public function filters()
    {
        return array(
            'accessControl' => 'accessControl',
        );
    }

    /**
     * Returns list of allowed actions.
     * @return array actions configuration.
     */
    public function accessRules()
    {
        $res = array();

        $res[] = array(
            'allow',
            'users' => array('@')
        );

        $res[] = array(
            'deny',
            'users' => array('*'),
            'deniedCallback' => array($this, 'adminHomepageRedirect'),
        );

        return $res;
    }

    /**
     * Returns an array of step methods that this wizard has (ordered)
     * Each element of the array must be an array and contain the following:
     * - alias - a unique short name descripting the step's purpose
     * - handler - a callable (e.g. array($this, 'someMethod')) that will
     *                be invoked when it's time for this step to be rendered
     * - validator - a callable or an anonymous function that validates if
     *                all the required data up to this step is provided and valid.
     *                Returns boolean indicating whether or not the wizard should
     *                proceed with rendering the current step (the step of
     *                this validator) or it should render again the step
     *                you came from so the user can fix the errors
     * @throws \CException
     * @return array
     */
    public function steps()
    {
        return array(
            array(
                'alias' => 'stepNameHandler',
                'handler' => array($this, 'stepNameHandler'),
                'validator' => array($this, 'stepNameValidator')
            ),
            array(
                'alias' => 'usersSelector',
                'handler' => array($this, 'stepUsersHandler'),
                'validator' => array($this, 'stepUsersValidator')
            ),
            array(
                'alias' => 'finishStep',
                'handler' => array($this, 'stepFinishHandler'),
                'validator' => array($this, 'stepFinishValidator')
            ),
            array(
                'alias' => 'saveStep',
                'handler' => array($this, 'saveStepHandler')
            )
        );
    }

    public function navigate($params = array(), $noNextMeansReloadSameStep = false)
    {
        $fromStep   = Yii::app()->request->getParam( 'from_step', FALSE );
        $prevButton = Yii::app()->request->getParam( 'prev_button', FALSE );

        $id         = Yii::app()->request->getParam('id', false);

        $goBackward = ( $prevButton ? TRUE : FALSE );

        if($id){
            $params['skipUsersStep'] = true;
        }

        $this->_processStep( $fromStep, $goBackward, $params );
    }

    protected function renderStep($stepAlias, $stepHTML, $noPrev = false, $noNext = false, $noCancel = false, $btnPrevTxt = false, $btnNextTxt = false, $btnCancelTxt = false   )
    {
        $showNextButton = true;

        if($stepAlias === 'finishStep'){
            $showNextButton = false;
        }
        $this->renderPartial( '_wizard_template', array(
            'alias'     => $stepAlias, // needed for the hidden 'from_step' field
            'innerHtml' => $stepHTML,
            'showNextButton' => $showNextButton
        ), FALSE, TRUE );
    }

    public function saveStepHandler($params = array()){

        $html = $this->renderPartial('save_step', array(), true, true);

        $this->renderStep('saveStep', $html);
    }

    public function stepFinishHandler($params = array()){
        $model = $params['model'];
        $stepHtml = $this->renderPartial('finish_step', array(
            'model' => $model
        ), true, true);

        $this->renderStep('finishStep', $stepHtml);
    }

    public function stepFinishValidator($stepName = '', $inData = array(), $params = array(), $goBack, &$nextStep){

        if($goBack && $params['skipUsersStep']){
            $nextStep = 'stepNameHandler';
            return true;
        }

        if(!$goBack){
            $boardModel = $params['model'];

            if($boardModel instanceof GamificationLeaderboard){

                if($boardModel->save()){
                    $translations = $params['translationModels'];

                    foreach($translations as $translationModel){
                        /**
                         * @var $translationModel GamificationLeaderboardTranslation
                         */
                        $translationModel->id_board = $boardModel->id;

                        $translationModel->save();
                    }

                    if(isset($params['createNewLeaderboard']) && $params['createNewLeaderboard'] == true){
                        echo "<a class='auto-close success' data-reset-dialog='true'></a>";
                        Yii::app()->end();
                    }

                    return true;
                } else{
                    return false;
                }
            } else{
                return false;
            }
        } else{
            return true;
        }
    }

    public function stepUsersHandler($params = array()){
        $model = $params['model'];
        $usersSelectorUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
            'type'  		=> UsersSelector::TYPE_GAMIFICATION_LEADERBOARD,
            'idForm' 		=> 'leaderboard-users-selector',
            'name'          => 'usersSelector',
            'noyiixviewjs'	=> true,  // very important: main page already has widget(s) loading yiigridview.js, so, don't
            'tp'            => 'plugin.GamificationApp.views.leaderboardWizard.users_top_panel',
            'idGeneral'     => $model->id
        ));

        $this->redirect($usersSelectorUrl, true);
    }

    public function stepUsersValidator($stepname = '', $inData = array(), $params = array(), $goBack, &$nextStep){
        $leaderboardModel = $params['model'];
        /**
         * @var $leaderboardModel GamificationLeaderboard
         */

        if($goBack){
            return true;
        }

        if(!$leaderboardModel->validate()){
            Yii::app()->user->setFlash('error', $leaderboardModel->getError('filter'));
            return false;
        }
        return true;
    }

    public function stepNameHandler($params = array()){
        $model = (isset($params['model']) ? $params['model'] : new GamificationLeaderboard());
        $stepAlias = 'stepNameHandler';

        $langs = CoreLangLanguage::getActiveLanguages();
		$defaultLang = Settings::get('default_language', 'english');
		$langs[$defaultLang] = $langs[$defaultLang] . " (".Yii::t('standard', '_DEFAULT_LANGUAGE').")";
        $stepHtml = $this->renderPartial('step_name', array(
            'model' => $model,
            'langs' => $langs
        ), true, true);

        $this->renderStep($stepAlias, $stepHtml);
    }

    public function stepNameValidator($stepname = '', $inData = array(), $params = array(), $goBack, &$nextStep){

        $leaderboardModel = $params['model'];
        $translation = $params['translationModels'];

        if(isset($params['skipUsersStep']) && $params['skipUsersStep'] == true){
            $nextStep = 'finishStep';
        }

        $defaultLanguage = Settings::get('default_language', 'english');

        $hasDefaultLang = false;
        foreach($translation as $model){
            /**
             * @var $model GamificationLeaderboardTranslation
             */
            if($model->lang_code == $defaultLanguage){
                $hasDefaultLang = true;
            }
            if(empty($model->name) && count($translation) == 1){
                Yii::app()->user->setFlash('error', Yii::t('standard', 'Please fill all required fields'));
                return false;
            }
        }

        if(!$hasDefaultLang){
            Yii::app()->user->setFlash('error', Yii::t('user_management', '_ERROR_ADDITIONAL_FIELDS_ENGLISH_REQUIRED'));
            return false;
        }
        return true;
    }

    /**
     * This will be a centralized landing point/action for all
     * your steps. In your custom implementation, do the custom
     * logic, e.g. loading a model from DB or whatever and then
     * call $this->navigator($params) to let DoceboWizardController
     * handle the rendering of the next/previous step (depending
     * on if we are going forward or backward in the wizard)
     *
     * ($params may contain your custom logic data, e.g. the
     * loaded model; this variable is later passed to the
     * validator and the step handler so they can render and
     * validate stuff properly)
     * @return mixed
     */
    public function actionWizard()
    {
        // Get client model, if any
        $idBoard    = Yii::app()->request->getParam( "id", FALSE );
        $boardModel = GamificationLeaderboard::model()->findByPk( $idBoard );

        if( ! $boardModel ) {
            $boardModel = new GamificationLeaderboard();
        }

        // Collect data from all steps of the wizard and set them to the model
        $inData = $this->collectStepData();

        if(empty($inData)){
            $inData = $_REQUEST;
        }

        $translations = $this->buildTranslations($inData, $idBoard);
        $boardModel = $this->buildUsersSelection($inData, $boardModel);

        $createNewLeaderboard = false;

        if(isset($_REQUEST['new_leaderboard'])){
            $createNewLeaderboard = true;
        }

        $params = array(
            'model' => $boardModel,
            'translationModels' => $translations,
            'createNewLeaderboard' => $createNewLeaderboard
        );

        $this->navigate( $params );
    }

    private function buildUsersSelection($inData = array(), $boardModel){
        $groups = array();
        $branches = array();

        if(isset($_REQUEST['Wizard']['leaderboard-users-selector']['users-filter']) && $_REQUEST['Wizard']['leaderboard-users-selector']['users-filter'] == 1){
            $boardModel->filter = 'all';
        }else{
            $continue = false;
            if(isset($_REQUEST['selected_groups']) || isset($_REQUEST['orgcharts_selected-json'])) {
                $selectedGroups = UsersSelector::getGroupsListOnly($_REQUEST);
                $selectedBranches = UsersSelector::getBranchesListOnly($_REQUEST);
                $continue = true;
            } else if(isset($_REQUEST['Wizard']['leaderboard-users-selector']['selected_groups']) || isset($_REQUEST['Wizard']['leaderboard-users-selector']['orgcharts_selected-json'])) {

                $selectedGroups = UsersSelector::getGroupsListOnly($_REQUEST['Wizard']['leaderboard-users-selector']);
                $selectedBranches = UsersSelector::getBranchesListOnly($_REQUEST['Wizard']['leaderboard-users-selector']);
                $continue = true;
            }

            if($continue){
                if(!empty($selectedGroups) || !empty($selectedBranches)){
                    if(!empty($selectedGroups)){
                        $groups = $selectedGroups;
                    }

                    if(!empty($selectedBranches)){
                        $branches = $selectedBranches;
                    }

                    $data = array(
                        'groups' => $groups,
                        'branches' => $branches
                    );

                    $boardModel->filter = json_encode($data);
                } else{
                    Yii::app()->user->setFlash('error', Yii::t('standard', 'Please, select group or branch'));
                    $usersSelectorUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
                        'type'  		=> UsersSelector::TYPE_GAMIFICATION_LEADERBOARD,
                        'idForm' 		=> 'leaderboard-users-selector',
                        'name'          => 'usersSelector',
                        'noyiixviewjs'	=> true,  // very important: main page already has widget(s) loading yiigridview.js, so, don't
                        'tp'            => 'plugin.GamificationApp.views.leaderboardWizard.users_top_panel',
                        'idGeneral'     => $boardModel->id
                    ));

                    $this->redirect($usersSelectorUrl, true);
                }
            }
        }

        return $boardModel;
    }

    private function buildTranslations($inData = array(), $idBoard = false){

        $translations = array();

        $addedLangs = array();
        foreach($inData as $attr => $item){
            if($attr === 'lang_code' || strpos($attr, 'name_') !== false){

                if($idBoard){
                    $lang_code = '';

                    if(isset($item) && $attr == 'lang_code'){
                        $lang_code = $item;
                    } else{
                        $lang_code = str_replace('name_', '', $attr);
                    }

                    $trModel = GamificationLeaderboardTranslation::model()->findByAttributes(array(
                        'id_board' => $idBoard,
                        'lang_code' => $lang_code
                    ));

                    if(!in_array($lang_code, $addedLangs)) {

                        if ($trModel) {
                            if (isset($item) && $attr == 'lang_code') {
                                $trModel->name = $inData['title_current'];
                                $translations[] = $trModel;
                            } else {
                                if (!empty($item)) {
                                    $trModel->name = $item;
                                    $translations[] = $trModel;
                                }
                            }
                        } else {
                            $trModel = new GamificationLeaderboardTranslation();
                            if (isset($item) && $attr == 'lang_code') {
                                $trModel->lang_code = $item;
                                $trModel->name = $inData['title_current'];

                                $translations[] = $trModel;
                            } else {
                                if (!empty($item)) {
                                    $trModel->lang_code = str_replace('name_', '', $attr);
                                    $trModel->name = $item;

                                    $translations[] = $trModel;
                                }
                            }
                        }

                        $addedLangs[] = $lang_code;
                    }
                } else{
                    $trModel = new GamificationLeaderboardTranslation();
                    if(isset($item) && $attr == 'lang_code'){
                        if(!in_array($item, $addedLangs)){
                            $addedLangs[] = $item;
                            $trModel->lang_code = $item;
                            $trModel->name = $inData['title_current'];

                            $translations[] = $trModel;
                        }
                    }else{
                        if(!empty($item)){
                            $lang = str_replace('name_', '', $attr);
                            if(!in_array($lang, $addedLangs)){
                                $trModel->lang_code = $lang;
                                $trModel->name = $item;

                                $translations[] = $trModel;
                            }
                        }
                    }
                }

            }
        }

        return $translations;
    }

    public function actionEditSelection(){
        $request = Yii::app()->request;

        $id = $request->getParam('idGeneral', false);
        $useAll = $request->getParam('users-filter', false);

        if($id){
            $leaderboardModel = GamificationLeaderboard::model()->findByPk($id);

            if($useAll){
                $leaderboardModel->filter = 'all';
                $leaderboardModel->setScenario('filterChanged');
                if($leaderboardModel->validate()){
                    $leaderboardModel->save();

                    echo "<a class='auto-close success'></a>";
                    Yii::app()->end();
                } else{
                    Yii::app()->user->setFlash('error', $leaderboardModel->getError('filter'));
                    $usersSelectorUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
                        'type'  		=> UsersSelector::TYPE_GAMIFICATION_LEADERBOARD_NO_WIZARD,
                        'idForm' 		=> 'leaderboard-users-selector',
                        'name'          => 'usersSelector',
                        'noyiixviewjs'	=> true,  // very important: main page already has widget(s) loading yiigridview.js, so, don't
                        'tp'            => 'plugin.GamificationApp.views.leaderboardWizard.users_top_panel',
                        'idGeneral'     => $leaderboardModel->id,
                        'collectorUrl'	=> Docebo::createAdminUrl('GamificationApp/LeaderboardWizard/editSelection')
                    ));

                    $this->redirect($usersSelectorUrl, true);
                }

            } else{
                $groups = array();
                $branches = array();

                $selectedGroups = UsersSelector::getGroupsListOnly($_REQUEST);
                $selectedBranches = UsersSelector::getBranchesListOnly($_REQUEST);

                if(!empty($selectedGroups) || !empty($selectedBranches)){
                    if(!empty($selectedGroups)){
                        $groups = $selectedGroups;
                    }

                    if(!empty($selectedBranches)){
                        $branches = $selectedBranches;
                    }

                    $data = array(
                        'groups' => $groups,
                        'branches' => $branches
                    );

                    $leaderboardModel->filter = json_encode($data);
                    $leaderboardModel->setScenario('filterChanged');

                    if($leaderboardModel->validate()){
                        $leaderboardModel->save();

                        echo "<a class='auto-close success'></a>";
                        Yii::app()->end();
                    } else{
                        Yii::app()->user->setFlash('error', $leaderboardModel->getError('filter'));
                        $usersSelectorUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
                            'type'  		=> UsersSelector::TYPE_GAMIFICATION_LEADERBOARD_NO_WIZARD,
                            'idForm' 		=> 'leaderboard-users-selector',
                            'name'          => 'usersSelector',
                            'noyiixviewjs'	=> true,  // very important: main page already has widget(s) loading yiigridview.js, so, don't
                            'tp'            => 'plugin.GamificationApp.views.leaderboardWizard.users_top_panel',
                            'idGeneral'     => $leaderboardModel->id,
                            'collectorUrl'	=> Docebo::createAdminUrl('GamificationApp/LeaderboardWizard/editSelection')
                        ));

                        $this->redirect($usersSelectorUrl, true);
                    }


                    echo "<a class='auto-close success'></a>";
                    Yii::app()->end();

                } else{
                    Yii::app()->user->setFlash('error', Yii::t('standard', 'Please, select group or branch'));
                    $usersSelectorUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
                        'type'  		=> UsersSelector::TYPE_GAMIFICATION_LEADERBOARD_NO_WIZARD,
                        'idForm' 		=> 'leaderboard-users-selector',
                        'name'          => 'usersSelector',
                        'noyiixviewjs'	=> true,  // very important: main page already has widget(s) loading yiigridview.js, so, don't
                        'tp'            => 'plugin.GamificationApp.views.leaderboardWizard.users_top_panel',
                        'idGeneral'     => $leaderboardModel->id,
                        'collectorUrl'	=> Docebo::createAdminUrl('GamificationApp/LeaderboardWizard/editSelection')
                    ));

                    $this->redirect($usersSelectorUrl, true);
                }
            }
        }
    }

    public function actionSwitchLeaderboard(){
        $request = Yii::app()->request;

        $id = $request->getParam('id', false);

        if($id){
            $leaderboardModel = GamificationLeaderboard::model()->findByPk($id);

            if($leaderboardModel){
                $presentState = $leaderboardModel->is_active;

                if($presentState === '0'){
                    $leaderboardModel->is_active = 1;
                } else if($presentState === '1'){
                    $leaderboardModel->is_active = 0;
                }

                $leaderboardModel->save();
            }
        }
    }

    public function actionDeleteLeaderboard(){
        $request = Yii::app()->request;

        $id = $request->getParam('id', false);

        if($id){
            $leaderboardModel = GamificationLeaderboard::model()->findByPk($id);

            if($leaderboardModel){

                if(isset($_POST['delete-leaderboar-confirm'])){
                    if($leaderboardModel->delete()){
                        echo "<a class='auto-close'></a>";
                    }
                }

                $this->renderPartial('delete', array(
                    'model' => $leaderboardModel,
                    'lang' => Lang::getCodeByBrowserCode(Yii::app()->getLanguage())
                ));
            }
        }
    }

    public function actionAutocomplete(){
        $inputData = Yii::app()->request->getParam('data', array());
        $output = array();

        if (!empty($inputData)) {
            $criteria = new CDbCriteria();
            $criteria->addCondition('(name LIKE :search) AND lang_code = "' . Lang::getCodeByBrowserCode(Yii::app()->getLanguage()) . '"');
            $criteria->group = 'id_board';
            $criteria->params[':search'] = '%'.$inputData['query'].'%';

            $boards = GamificationLeaderboardTranslation::model()->findAll($criteria);
            foreach($boards as $board)
                $output[] = $board->name;
        }

        $this->sendJSON(array('options' => $output));
    }

    public function actionViewBoard(){
        $request = Yii::app()->request;

        $id = $request->getParam('id', false);

        if($id){
            $activeLeaderboars = GamificationLeaderboard::model()->getActiveLeaderboards(null, $id, true);
            $hasActiveLeaderboards = !empty($activeLeaderboars) ? true : false;
            $leaderboardsChartData = array();

            $leaderboards = array();

            if($hasActiveLeaderboards){
                foreach($activeLeaderboars['leaderboards'] as $leaderboard){
                    $leaderboardsChartData[$leaderboard]['points'] = GamificationAssignedBadges::getPointsChart(10, $activeLeaderboars['users'][$leaderboard]);
                }
            } else{
                echo Yii::t('gamification', "The board is empty!");
            }

            $html = Yii::app()->getController()->renderPartial('plugin.GamificationApp.views.gamificationApp._leaderboards', array(
                'hasActiveLeaderboard' => $hasActiveLeaderboards,
                'leaderboards' => $leaderboards,
                'leaderboardChartData' => $leaderboardsChartData,
                'topTitleCount' => 10,
                'hideTitle' => true,
                'showInDialog' => true
            ), true, true);

            echo $html;
        }
    }
}