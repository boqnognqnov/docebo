<?php
/**
 * Class GamificationAppModule
 * Main Gamification plugin module
 */
class GamificationAppModule extends CWebModule
{
    // DEFAULT GAMIFICATION EVENTS (NOT REGISTERED BY OTHER APPS)
    public static $EVENT_LEARNING_PLAN_COMPLETED                                    = EventManager::EVENT_STUDENT_COMPLETED_LEARNING_PLAN;
    public static $EVENT_COURSE_COMPLETED                                           = EventManager::EVENT_STUDENT_COMPLETED_COURSE;
    public static $EVENT_LO_COMPLETED                                               = EventManager::EVENT_STUDENT_COMPLETED_LO;
    public static $EVENT_NEW_FORUM_REPLY                                            = 'NewReply';
    public static $EVENT_NEW_FORUM_THREAD                                           = 'NewThread';
    public static $EVENT_NEW_HELPFUL_FEEDBACK                                       = 'NewHelpfulFeedback';
    public static $EVENT_NEW_TONE_FEEDBACK                                          = 'NewToneFeedback';
    public static $EVENT_APP7020_USER_REACHED_A_GOAL                                = 'UserReachedAGoal';
    public static $EVENT_APP7020_ASSET_REACHED_A_GOAL                               = 'AssetReachedAGoal';
    public static $EVENT_APP7020_ASSET_VIEWED_FOR_LONGEST_TIME                      = 'AssetViewedForLongestTime';
    public static $EVENT_APP7020_EXPERT_REACHED_A_GOAL                              = 'ExpertReachedAGoal';
    public static $EVENT_APP7020_USER_REACHED_A_GOAL_AS_TOP_CONTRIBUTOR             = 'UserReachedAGoalAsTopContributor';




    /* Event type groups */
    const GROUP_LMS = 'LMS Badges';
    const GROUP_APP7020 = 'Coach & Share Badges';


    /**
     * Maps events into groups
     */
    public static function groupsPerType() {
        return array(
            self::$EVENT_LEARNING_PLAN_COMPLETED                                    => self::GROUP_LMS,
            self::$EVENT_COURSE_COMPLETED                                           => self::GROUP_LMS,
            self::$EVENT_LO_COMPLETED                                               => self::GROUP_LMS,
            self::$EVENT_NEW_FORUM_REPLY                                            => self::GROUP_LMS,
            self::$EVENT_NEW_FORUM_THREAD                                           => self::GROUP_LMS,
            self::$EVENT_NEW_HELPFUL_FEEDBACK                                       => self::GROUP_LMS,
            self::$EVENT_NEW_TONE_FEEDBACK                                          => self::GROUP_LMS,
            self::$EVENT_APP7020_USER_REACHED_A_GOAL                                => self::GROUP_APP7020,
            self::$EVENT_APP7020_ASSET_REACHED_A_GOAL                               => self::GROUP_APP7020,
            self::$EVENT_APP7020_ASSET_VIEWED_FOR_LONGEST_TIME                      => self::GROUP_APP7020,
            self::$EVENT_APP7020_EXPERT_REACHED_A_GOAL                              => self::GROUP_APP7020,
            self::$EVENT_APP7020_USER_REACHED_A_GOAL_AS_TOP_CONTRIBUTOR             => self::GROUP_APP7020,
        );
    }



    /**
     * Module init method
     * This method is called when the module is being created
     */
    public function init()
	{
		// Import the module-level models and components
		$this->setImport(array(
			'GamificationApp.controllers.*',
			'GamificationApp.models.*',
            'GamificationApp.models.helpers.*',
			'GamificationApp.widgets.*',
			'GamificationApp.views.*',
            'GamificationApp.views.goals_partials.*',
            'GamificationApp.components.*',
            'GamificationApp.components.handlers.*',
		));


        if(Yii::app() instanceof CWebApplication){
            if (!Yii::app()->request->isAjaxRequest) {
            	$assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias("plugin.GamificationApp.assets"));
            	Yii::app()->getClientScript()->registerCssFile($assetsUrl.'/css/gamification.css');
            }
        }

        Yii::app()->event->on('OnBeforeBadgeAssign', array($this, 'updateUserWallet'));
        Yii::app()->event->on('MainMenuAfterMenuRender', array($this, 'addGamificationAppUserMenu'));
        Yii::app()->event->on('MainMenuAfterNewMenuProfileRender', array($this, 'addGamificationAppNewUserMenu'));
        Yii::app()->event->on('BeforeBodyTagClose', array($this, 'showGamificationNotificationModal'));
        Yii::app()->event->on('BeforeUpdatingUserLastEnter', array($this, 'prepareNotifications'));
        Yii::app()->event->on('CollectMyDashboardDashletWidgets', array($this, 'installGamificationDashlet'));
        Yii::app()->event->on('CollectCustomNotifications', array($this, 'collectCustomNotifications'));
        Yii::app()->event->on('DeletedUser', array($this, 'onAfterUserDelete'));

        // Register handlers for all badge events
		try {
            $this->registerBadgeEventListeners();
        } catch(Exception $e) {
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
        }
	}


    /**
     * @param DEvent $event
     */
    public function installGamificationDashlet(DEvent $event){
        if (!isset($event->params['descriptors']))
            return;

        $dashletGamificationDescriptor = DashletGamification::descriptor();
        $dashletRewardShopDescriptor = DashletRewardShop::descriptor();

        $collectedSoFarHandlers = array();
        foreach($event->params['descriptors'] as $descriptorObj){
            if ($descriptorObj instanceof DashletDescriptor)
                $collectedSoFarHandlers[] = $descriptorObj->handler;
        }

        if(!in_array($dashletGamificationDescriptor->handler, $collectedSoFarHandlers)){
            // Add the SubscriptionCodesDashlet dashlet to the event params
            $event->params['descriptors'][] = $dashletGamificationDescriptor;
        }

        $enableRewardShop = PluginSettings::get('enable_reward_shop', 'GamificationApp');

        if($enableRewardShop == 'on' ){
            if(!in_array($dashletRewardShopDescriptor->handler, $collectedSoFarHandlers)){
                $event->params['descriptors'][] = $dashletRewardShopDescriptor;
            }
        }
    }


	/**
	* This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	*/
	public function menu()
	{

        $adminMenuElements = array(
            Yii::t('gamification', 'Badges') => Docebo::createAdminUrl('//GamificationApp/GamificationApp/index'),
            Yii::t('gamification', 'Leaderboards') => Docebo::createAdminUrl('//GamificationApp/GamificationApp/leaderboard'),
            Yii::t('gamification', 'Contests') => Docebo::createAdminUrl('//GamificationApp/Contests/index'),
        );
        if(PluginSettings::get('enable_reward_shop','GamificationApp') == 'on'){
            $adminMenuElements[Yii::t('gamification', 'Rewards')] = Docebo::createAdminUrl('//GamificationApp/Reward/index');
        }

        // Add menu voices !
		Yii::app()->mainmenu->addAppMenu('gamification', array(
			'icon' => 'admin-ico gamification',
			'app_icon' => 'fa-trophy', // icon used in the new menu
			'link' => 'javascript:void(0);',
			'label' => Yii::t('gamification', 'Gamification'),
            'settings' => Docebo::createAdminUrl('//GamificationApp/GamificationApp/settings')
		), $adminMenuElements);
	}

    /**
     * Registers the listener for all installed badges
     */
    public function registerBadgeEventListeners()
	{
        // Retrieve all badge event names
        $criteria = new CDbCriteria();
        $criteria->distinct = true;
        $criteria->select = 'event_name';
        $criteria->condition = "event_name <> 'manual'";
        $badges = GamificationBadge::model()->findAll($criteria);
        foreach($badges as $badge)
            Yii::app()->event->on($badge->event_name, array($this, 'handleGamificationEvent'));

        Yii::app()->event->on(EventManager::EVENT_USER_WINNED_CONTEST, array($this, 'handleGamificationContestEvent'));
	}

    /**
     * Collects all possible gamification events (standard events and custom plugin events)
     */
    public static function fetchGamificationEvents(&$event_list)
    {
        // Add default events
        $event_list = array(
            self::$EVENT_COURSE_COMPLETED => array(
                'event_name' => Yii::t('gamification', 'Student completed course'),
                'group'=>self::GROUP_LMS,
                'event_description' => Yii::t('gamification', 'Student completed course - description'),
                'handler' => 'GamificationApp.components.StudentCompletedCourseHandler'
            ),
            self::$EVENT_LO_COMPLETED => array (
                'event_name' => Yii::t('gamification', 'Student completed LO'),
                'group'=>self::GROUP_LMS,
                'event_description' => Yii::t('gamification', 'Student completed LO - description'),
                'handler' => 'GamificationApp.components.StudentCompletedLoHandler'
            ),
            self::$EVENT_NEW_FORUM_REPLY => array (
                'event_name' => Yii::t('gamification', 'Student posted a reply on a forum discussion'),
                'group'=>self::GROUP_LMS,
                'event_description' => Yii::t('gamification', 'Student posted a reply on a forum discussion - description'),
                'handler' => 'GamificationApp.components.StudentPostedForumReplyHandler'
            ),
            self::$EVENT_NEW_FORUM_THREAD => array (
                'event_name' => Yii::t('gamification', 'Student started a new discussion in a forum'),
                'group'=>self::GROUP_LMS,
                'event_description' => Yii::t('gamification', 'Student started a new discussion in a forum - description'),
                'handler' => 'GamificationApp.components.StudentCreatedForumThreadHandler'
            ),
            self::$EVENT_NEW_HELPFUL_FEEDBACK => array (
                'event_name' => Yii::t('gamification', 'Student clicked on "Helpful" on some item'),
                'group'=>self::GROUP_LMS,
                'event_description' => Yii::t('gamification', 'Student clicked on the "Helpful" link (e.g. on a forum reply)'),
                'handler' => 'GamificationApp.components.StudentClickedHelpfulHandler'
            ),
            self::$EVENT_NEW_TONE_FEEDBACK => array (
                'event_name' => Yii::t('gamification', 'Student clicked on "Set the tone" on some item'),
                'group'=>self::GROUP_LMS,
                'event_description' => Yii::t('gamification', 'Student clicked on the "Set the tone" link (e.g. on a forum reply)'),
                'handler' => 'GamificationApp.components.StudentClickedSetToneHandler'
            )
        );


        if ( PluginManager::isPluginActive('CurriculaApp') ) {
            $event_list [self::$EVENT_LEARNING_PLAN_COMPLETED] = array(
                'event_name'        => Yii::t('gamification', 'Student completed a Learning Plan'),
                'group'=>self::GROUP_LMS,
                'event_description' => Yii::t('gamification', 'Student completed a Learning Plan - description'),
                'handler'           => 'GamificationApp.components.StudentCompletedLearningPlanHandler'
            );
        }

        if(PluginManager::isPluginActive('Share7020App')){
            $event_list [self::$EVENT_APP7020_USER_REACHED_A_GOAL] = array(
                'event_name'        => Yii::t('gamification', 'User met a goal'),
                'group'=>self::GROUP_APP7020,
                'event_description' => Yii::t('gamification', 'User met a goal - description'),
                'handler'           => 'GamificationApp.components.UserReachedAGoal'
            );
            $event_list [self::$EVENT_APP7020_ASSET_REACHED_A_GOAL] = array(
                'event_name'        => Yii::t('gamification', "User's asset met a goal"),
                'group'=>self::GROUP_APP7020,
                'event_description' => Yii::t('gamification', "User's asset met a goal- description"),
                'handler'           => 'GamificationApp.components.AssetReachedAGoal'
            );
            $event_list [self::$EVENT_APP7020_ASSET_VIEWED_FOR_LONGEST_TIME] = array(
                'event_name'        => Yii::t('gamification', "User's asset viewed for longest duration of time"),
                'group'=>self::GROUP_APP7020,
                'event_description' => Yii::t('gamification', "User's asset viewed for longest duration of time- description"),
                'handler'           => 'GamificationApp.components.AssetViewedForLongestTime'
            );
            $event_list [self::$EVENT_APP7020_EXPERT_REACHED_A_GOAL] = array(
                'event_name'        => Yii::t('gamification', 'Expert met a goal'),
                'group'=>self::GROUP_APP7020,
                'event_description' => Yii::t('gamification', 'Expert met a goal- description'),
                'handler'           => 'GamificationApp.components.ExpertReachedAGoal'
            );
            $event_list [self::$EVENT_APP7020_USER_REACHED_A_GOAL_AS_TOP_CONTRIBUTOR] = array(
                'event_name'        => Yii::t('gamification', 'User became top contributor (uploaded assets)'),
                'group'=>self::GROUP_APP7020,
                'event_description' => Yii::t('gamification', 'User became top contributor (uploaded assets) - description'),
                'handler'           => 'GamificationApp.components.UserReachedAGoalAsTopContributor'
            );

        }


        // Let plugins add their own events (or override default ones)
		$caller = new stdClass();
        Yii::app()->event->raise('GamificationEventsFetching', new DEvent($caller, array('event_list' => &$event_list)));
    }

    /**
     * Method invoked when contest end.
     *
     * @param DEvent $event
     */
    public function handleGamificationContestEvent(DEvent $event){

        Yii::log("Handling gamification event '".$event->event_name."'", CLogger::LEVEL_INFO);

        // Search for all badges with the raised event
        $event_name = $event->event_name;

        $handler = Yii::createComponent('UserWinnedContest');

        try{
            $result = $handler->shouldAssignBadge(null, null, $event->params);

            if($result['action'] == true) {

                $badge = $result['badge'];

                // Trigger an event to let plugins handle the badges releasing in their own custom way
                $event = new DEvent($this, array(
                    'id_user' => $result['id_user'],
                    'badge' => $badge
                ));
                Yii::app()->event->raise('OnBeforeBadgeAssign', $event);
                if ($event->shouldPerformAsDefault()) {
                    // Assign the bagde to the user
                    $model_a = new GamificationAssignedBadges();
                    $model_a->id_badge = $badge->id_badge;
                    $model_a->id_user = $result['id_user'];
                    $model_a->issued_on = Yii::app()->localtime->toLocalDateTime();
                    $model_a->score = $badge->score;
                    $model_a->event_name = $event_name;
                    $model_a->event_key = $result['event_key'];
                    $model_a->event_params = CJSON::encode($result['event_params']);
                    $model_a->save();

                    Yii::log("Badged for event '" . $event_name . "' assigned to user '" . $model_a->id_user . "'", CLogger::LEVEL_INFO);
                }
            } else
                Yii::log("Skipped badge assignment for event '".$event_name."'", CLogger::LEVEL_INFO);
        } catch(Exception $e){
            Yii::log($e->getMessage(), CLogger::LEVEL_INFO);
        }
    }

    /**
     * Method invoked when any gamification event is triggered by clients (e.g. plugins)
     * @param DEvent $event
     */
    public function handleGamificationEvent(DEvent $event)
    {
        Yii::log("Handling gamification event '".$event->event_name."'", CLogger::LEVEL_INFO);

        $localFormat = Yii::app()->localtime->YiitoPHPDateFormat(Yii::app()->localtime->getLocalDateTimeFormat('short', 'medium'));

        // Search for all badges with the raised event
        $event_name = $event->event_name;
        $badges = GamificationBadge::model()->findAllByAttributes(array('event_name' => $event_name));
        if(count($badges) > 0) {
            // Load the list of events
            $event_list = array();
            self::fetchGamificationEvents($event_list);

            // Load the handler for this event
            if(isset($event_list[$event_name]) && isset($event_list[$event_name]['handler'])) {
                $handler_class = $event_list[$event_name]['handler'];
                try {
                    $handler = Yii::createComponent($handler_class);
                    if($handler && ($handler instanceof GamificationEventHandler)) {
                        // Pass the event to the handler and get the answer (assign/not assign)
                        foreach($badges as $badge) {
                            $event_config = is_null($badge->event_params) ? array() : CJSON::decode($badge->event_params);
                            $event_params = $event->params;
                            if(isset($_SESSION['time']) && $_SESSION['time'] != NULL){
                                if(($event_name == GamificationAppModule::$EVENT_APP7020_USER_REACHED_A_GOAL && $event_params['goal'] == 4 ) || ($event_name == GamificationAppModule::$EVENT_APP7020_EXPERT_REACHED_A_GOAL && $event_params['goal'] == 1 )) {
                                    if (strtotime($_SESSION['time']) >= strtotime(Yii::app()->localtime->toLocalDateTime())) {
                                        $addtime = Yii::app()->localtime->toUTC(date($localFormat, strtotime('+1 second', strtotime($_SESSION['time']))));
                                        $newtime = Yii::app()->localtime->toLocalDateTime($addtime);
                                    } else {
                                        $newtime = $_SESSION['time'];
                                    }
                                } else {
                                    $newtime = Yii::app()->localtime->toLocalDateTime();
                                }
                            } else {
                                $newtime = Yii::app()->localtime->toLocalDateTime();
                            }
                            // Ask the handler what to do
                            try {
                                $result = $handler->shouldAssignBadge($badge, $event_config, $event_params);
                                if($result['action'] == true) {

									// Trigger an event to let plugins handle the badges releasing in their own custom way
									$subEvent = new DEvent($this, array(
										'id_user' => $result['id_user'],
										'badge' => $badge
									));
									Yii::app()->event->raise('OnBeforeBadgeAssign', $subEvent);
									if ($subEvent->shouldPerformAsDefault()) {
										// Assign the bagde to the user
										$model_a = new GamificationAssignedBadges();
										$model_a->id_badge = $badge->id_badge;
										$model_a->id_user = $result['id_user'];
										$model_a->issued_on = $newtime;
										$model_a->score = $badge->score;
										$model_a->event_name = $event_name;
										$model_a->event_key = $result['event_key'];
										$model_a->event_params = CJSON::encode($result['event_params']);
										$model_a->save();

                                        if($model_a->save()){
                                            $_SESSION['time'] = $newtime;
                                        }

										Yii::log("Badged for event '" . $event_name . "' assigned to user '" . $model_a->id_user . "'", CLogger::LEVEL_INFO);
									}
                                } else
                                    Yii::log("Skipped badge assignment for event '".$event_name."'", CLogger::LEVEL_INFO);
                            } catch(Exception $e) {
                                Yii::log($e->getMessage(), CLogger::LEVEL_INFO);
                            }
                        }
                    }
                } catch(CException $e) {
                    Yii::log($e->getMessage(), CLogger::LEVEL_INFO);
                }
            }
        }
    }

    /**
     * Adds the Gamification user menu
     */
    public function addGamificationAppUserMenu()
    {
        Yii::app()->controller->widget('GamificationAppUserMenu');
    }

    /**
     * Adds the Gamification user menu
     */
    public function addGamificationAppNewUserMenu()
    {
        Yii::app()->controller->widget('GamificationAppNewUserMenu');
    }

    /**
     * Displays a popup with details about the last badge assigned
     * to the user since he's last login
     */
    public function showGamificationNotificationModal()
    {
        // only show the popup once per session
        if (isset(Yii::app()->session['gamificationShowBadgeNotification']) && !isset(Yii::app()->session['gamificationBadgeNotificationRendered'])) {
            // mark as rendered
            Yii::app()->session['gamificationBadgeNotificationRendered'] = true;
            $badge = unserialize(Yii::app()->session['gamificationShowBadgeNotification']);
            Yii::app()->controller->widget('GamificationBadgeNotification', array('badge'=>$badge));
        }
    }

    /**
     * If the user needs to be notified after login, will set a flag in session that
     * we can then check if it is set and display the required notifications
     */
    public function prepareNotifications()
    {
        $badge = GamificationAssignedBadges::getLatestAssignedBadgeSinceLogin();
        if ($badge) {
            Yii::app()->session['gamificationShowBadgeNotification'] = serialize($badge);
        }
    }

    public function updateUserWallet(DEvent $event){

        $userId = $event->params['id_user'];
        $badge = $event->params['badge'];

        $command = Yii::app()->db->createCommand();

        /**
         * @var $badge GamificationBadge
         * @var $command CDbCommand
         */

        $userCoins = $command->select('coins')
            ->from('gamification_user_wallet')
            ->where('id_user = :user', array(
                ':user' => $userId
            ))
            ->queryScalar();

        if($userCoins){
            $command->update('gamification_user_wallet', array(
                'coins' => $userCoins += $badge->score
            ), 'id_user = :user', array(
                ':user' => $userId
            ));
        } else{
            $command->insert('gamification_user_wallet', array(
                'id_user' => $userId,
                'coins' => $badge->score
            ));
        }

    }

    public function collectCustomNotifications(DEvent $event){
        if (isset($event->params['notifications']) && is_array($event->params['notifications'])) {

			// This is passed by reference,  s we can modify it
			$notificationsArray = $event->params['notifications'];

			// List classes known to THIS plugin
			// BE SURE THIS IS A VALID, loadable class name!!!!
			$myHandlers = array(CoreNotification::NTYPE_GAMIFICATION_REWARD_PENDING, CoreNotification::NTYPE_GAMIFICATION_REWARD_APPROVED, CoreNotification::NTYPE_GAMIFICATION_REWARD_REJECTED);

			// Enumerate all classes and call their descriptor() methods
			foreach ($myHandlers as $handlerName) {
				$descriptor = $handlerName::descriptor();
				$descriptor->group = Yii::t('gamification','Gamification');
				$event->params['notifications'][] = $descriptor;
			}

		} 
    }

    public function onAfterUserDelete(DEvent $event){
        $userModel = $event->params['userModel'];
        if($userModel){
            $pendingRequests = GamificationUserRewards::model()->findAllByAttributes(array('id_user'=>$userModel->idst, 'status'=>GamificationUserRewards::$STATUS_PENDING));

            foreach($pendingRequests as $pendingRequest){
                $pendingRequest->status = GamificationUserRewards::$STATUS_CANCELED;
                $reward = GamificationReward::model()->findByPk($pendingRequest->id_reward);

                if($reward){
                    $reward->availability = $reward->availability + 1;
                    $reward->save();
                }

                $pendingRequest->save();
            }
        }

    }
}
