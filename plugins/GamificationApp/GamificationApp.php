<?php

/**
 * Plugin for Label
 */

class GamificationApp extends DoceboPlugin {

	// Override parent
	public static $HAS_SETTINGS = false;

	/**
	 * @var string Name of the plugin; needed to identify plugin folder and entry script
	 */
	public $plugin_name = 'GamificationApp';


	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {

		$core_role = CoreRole::model()->findByAttributes(array('roleid' => '/framework/admin/gamificationapp/view'));

		if(empty($core_role)) {
			$sql = "INSERT INTO `core_st` VALUES (NULL);";
			$command = Yii::app()->db->createCommand($sql);
			$command->execute();

			$sql = "INSERT INTO `core_role` (`idst`, `roleid`) VALUES (LAST_INSERT_ID(), '/framework/admin/gamificationapp/view');";
			$command = Yii::app()->db->createCommand($sql);
			$command->execute();

			$sql = "INSERT INTO `core_role_members` (`idst`, `idstMember`) VALUES (LAST_INSERT_ID(), 3);";
			$command = Yii::app()->db->createCommand($sql);
			$command->execute();
		}

        // refresh session  permission for the logged in users
        Yii::app()->authManager->refreshCache( Yii::app()->user->id );
		
		// activate options
		$sql = "
			CREATE TABLE IF NOT EXISTS `gamification_badge`
			(
				`id_badge` INT(11) NOT NULL AUTO_INCREMENT,
				`icon` VARCHAR(255) NOT NULL,
				`score` INT(11) NOT NULL,
				`event_name` VARCHAR(100) NULL,
				`event_params` TEXT NULL,
				PRIMARY KEY (`id_badge`)
			)
			COLLATE='utf8_general_ci'
			ENGINE = InnoDB;

			CREATE TABLE IF NOT EXISTS `gamification_assigned_badges`
			(
				`id_badge` INT(11) NOT NULL,
				`id_user` INT(11) NOT NULL,
				`issued_on` TIMESTAMP NOT NULL,
				`score` INT(11) NOT NULL DEFAULT '0',
				`event_name` VARCHAR(100) NULL DEFAULT NULL,
				`event_key` VARCHAR(255) NULL DEFAULT NULL,
				`event_module` VARCHAR(128) NOT NULL DEFAULT 'gamification',
				`event_params` VARCHAR(1024) NULL DEFAULT NULL,
				PRIMARY KEY (`id_badge`, `id_user`, `issued_on`),
				INDEX `fk_gamification_assigned_badges_core_user1_idx` (`id_user`),
				CONSTRAINT `fk_gamification_assigned_badges_core_user1` FOREIGN KEY (`id_user`) REFERENCES `core_user` (`idst`) ON UPDATE CASCADE ON DELETE CASCADE,
				CONSTRAINT `fk_gamification_assigned_badges_gamification_badge1` FOREIGN KEY (`id_badge`) REFERENCES `gamification_badge` (`id_badge`) ON UPDATE CASCADE ON DELETE CASCADE
			)
			COLLATE='utf8_general_ci'
			ENGINE=InnoDB;

			CREATE TABLE IF NOT EXISTS `gamification_badge_translation`
			(
				`id_badge` INT(11) NOT NULL,
				`lang_code` VARCHAR(50) NOT NULL,
				`name` VARCHAR(255) NOT NULL,
				`description` TEXT NULL,
				PRIMARY KEY (`id_badge`, `lang_code`),
				INDEX `fk_gamification_badge_translation_gamification_badge_idx` (`id_badge` ASC),
				CONSTRAINT `fk_gamification_badge_translation_gamification_badge`
				FOREIGN KEY (`id_badge`)
				REFERENCES `gamification_badge` (`id_badge`)
				ON DELETE CASCADE
				ON UPDATE CASCADE
			)
			COLLATE='utf8_general_ci'
			ENGINE = InnoDB;";
		$command = Yii::app()->db->createCommand($sql);
		$command->execute();
		// When doing command->execute(), always issue an unset(command) 
		// before any other SQL related code, to close the MySQL cursor
		// Otherwise you will get:
		//     Cannot execute queries while other unbuffered queries are active.
		unset($command);	
        parent::activate();
	}


	public function deactivate() {
		parent::deactivate();
		// deactivate options
	}


}
