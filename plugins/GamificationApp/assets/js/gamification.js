/**
 * Created by Georgi on 4.2.2016 г..
 */

var RewardsSet = false;
var Reward = false;
var RewardCarousel = false;
var RewardsShop = false;

(function($){
    RewardsSet = function(options){
        this.options = {};


        this.defaultOptions = {
            debug: false
        };

        if(typeof options === "undefined"){
            options = {};
        }

        this.page = null;

        this.init(options);
    };

    RewardsSet.prototype = {

        /**
         * Initialize the object.
         *
         * @param options Object options
         */
        init: function(options){
            this.options = $.extend({}, this.defaultOptions, options);

            if(typeof this.options.page !== "undifined"){
                this.page = this.options.page;
            }

            this.debug('Initializing RewardsSet javascript');
            this.addListeners();
        },

        /**
         *
         * Display some message to the console. NOTE: you have to send to the object options -> debug: true!!!
         *
         * @param message Message to display to the console
         */
        debug: function(message){
            if(this.options.debug === true){
                console.log('RewardsSet: ' + message);
            }
        },

        /**
         * add RewardSet Index Listeners
         */
        addRewardSetIndexListeners: function() {
            var $this = this;

            $(document).off('userselector.closed.success', '.modal.users-selector')
                .on('userselector.closed.success', '.modal.users-selector', function(){
                    $this.updateGridView();
                });

            $(document).on('dialog2.content-update', '.modal.modal-new-set, .modal.modal-edit-set', function(){
                if($this.options.debug === false){
                    TinyMce.attach("#description-textarea", {height: 200,
                        plugins : [
                            "advlist autolink lists link image charmap print preview anchor",
                            "searchreplace visualblocks code fullscreen",
                            "insertdatetime media table contextmenu paste moxiemanager textcolor"
                        ],
                        toolbar : "undo redo | styleselect | bold italic fontsizeselect forecolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                        language: yii.language
                    });
                } else{
                    setTimeout(function(){
                        TinyMce.attach("#description-textarea", {height: 200,
                            plugins : [
                                "advlist autolink lists link image charmap print preview anchor",
                                "searchreplace visualblocks code fullscreen",
                                "insertdatetime media table contextmenu paste moxiemanager textcolor"
                            ],
                            toolbar : "undo redo | styleselect | bold italic fontsizeselect forecolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                            language: yii.language
                        });
                    }, 2000);
                }
            });

            $(document).on('dialog2.closed.success', '.modal.modal-delete-set', function(){
                $this.updateGridView();
            });

            $(document).on('dialog2.closed.success', '.modal.modal-new-set, .modal.modal-edit-set', function(){
                $this.updateGridView();
                TinyMce.removeEditorById('description-textarea');
            });
        },

        /**
         * add Assign Rewards Listeners
         */
        addAssignRewardsListeners: function() {
            var $this = this;

            // Add styler
            $('input[type="checkbox"]').styler();

            //remove the break rule from the list that moves the filters to the next line
            $("#combo-list-view-container-associated-sets-management-list").find("br").remove();

            // hide all odd selectors (Deselect-all/Deselect-page-all)
            $(".combo-list-view-container .left-selections a:odd").hide();

            // on key up on filter selector it will hide/reveal the background element
//            $this.prototype.updateSelectionCounter();
            $(document).on('click', '.combo-list-view-container .left-selections a', this.updateFilters);

            $(document).on('change', 'select#associated-sets-management-list_massive_action', this.updateFilters);

//            $(document).on('ajaxComplete', this.updateFilters);

            // Do a combolistview update
            $(document).on('dialog2.closed', '.modal-delete-set-reward', function (e) {
                // Do the yiiGridView update here
                $('#associated-sets-management-list').comboListView('updateListView');
				$('.combo-list-deselect-all').click();
            });

            // Procee combolistview mass action(-s)
            $(document).on('combolistview.massive-action-selected', function (e, data) {
                var idRewardSet = $('#combo-list-view-container-associated-sets-management-list input[name="reward_set_id"]').val();
                var handler = data.listId; // # associated-sets-management-list

                // Check is selected items OR set default option
                // from selection
                if (data.selection.length === 0) {
                    this.activeElement.value = '';
                }

                // Switch all actions from selection
                switch (data.action) {
                    case "delete":
                        if (data.selection.length > 0 && handler != 'null') {
                            openDialog(false, // Event
                                Yii.t('gamification', 'Delete Rewards From Set'), // Title
                                'index.php?r=GamificationApp/RewardsSet/massiveUnassignRewards&id='+idRewardSet, // Url
                                'modal-delete-set-reward', // Dialog-Id
                                'modal-delete-set-reward', // Dialog-Class
                                'POST', // Ajax Method
                                {// Ajax Data
                                    'idSet': idRewardSet,
                                    'idAssignedRewards': data.selection.join(","),
                                });
                        }
                }

            });

            // On closing the dialog 2 do update of the combo list view
            $(document).on('dialog2.closed', '.modal-assign-rewards-to-set', function (e) {
                $('#associated-sets-management-list').comboListView('updateListView');
            });
        },

        /**
         * Add your listeners here!
         */
        addListeners: function(){
            //Add listeners for Editing Reward Page ONLY
            if(this.page == 'page_assign_rewards'){
                this.addAssignRewardsListeners();
            } else if (this.page == 'page_rewardset_index')  {
                this.addRewardSetIndexListeners();
            }


        },

        updateGridView: function(){
            $.fn.yiiGridView.update('rewards-set-grid', {});
        },

        updateFilters: function(){
            var $actions = {
                _show: [],
                _hide: [],
                add: function(type, elements){
                    for(var i = 0; i < elements.length; i++) {
                        this['_'+type].push(elements[i]);
                    }
                }
            };

            $actions.$combo_list_selected_count = $('#associated-sets-management-list').comboListView('getSelection').length;
            $actions.$combo_list_selected_count_page = $('#associated-sets-management-list > div.items > div.row-fluid > div.combo-list-item.selected').length;
            $actions.$combo_list_total = parseInt($('div#associated-sets-management-list div.summary > strong').text());
            $actions.$combo_list_total_page = $('#associated-sets-management-list > div.items > div.row-fluid > div.combo-list-item').length

            if($actions.$combo_list_total <= $actions.$combo_list_total_page || !$actions.$combo_list_total) {
                $actions.$combo_list_selected_count = $actions.$combo_list_selected_count_page;
            }

            if($actions.$combo_list_selected_count == 0) {
                $actions.add('hide', ['deselect-all','deselect-page-all']);
                $actions.add('show', ['select-all','select-page-all']);
            }
            if($actions.$combo_list_selected_count == $actions.$combo_list_total) {
                $actions.add('hide', ['select-all','select-page-all']);
                $actions.add('show', ['deselect-all','deselect-page-all']);
            }
            if($actions.$combo_list_selected_count > 0 && $actions.$combo_list_selected_count_page == $actions.$combo_list_total_page ) {
                $actions.add('hide', ['select-page-all']);
                $actions.add('show', ['deselect-page-all']);
            }
            if($actions.$combo_list_selected_count > 0 && $actions.$combo_list_selected_count_page < $actions.$combo_list_total_page ) {
                $actions.add('hide', ['deselect-page-all']);
                $actions.add('show', ['select-page-all']);
            }

            $('span#combo-list-selected-count').text($actions.$combo_list_selected_count);

            if(!$actions.$combo_list_selected_count) {
                $('#associated-sets-management-list_massive_action').val('');
            }

            if($actions.hasOwnProperty('_show') || $actions.hasOwnProperty('_hide')) {
                for(var i = 0; i < Math.max.apply(null, [$actions._show.length, $actions._hide.length]); i++) {
                    if($actions._show[i] != undefined) $('.combo-list-view-container .left-selections a.combo-list-' + $actions._show[i]).show();
                    if($actions._hide[i] != undefined) $('.combo-list-view-container .left-selections a.combo-list-' + $actions._hide[i]).hide();
                }
            }
        }
    };

    Reward = function(options){
        this.options = {};

        this.page = null;

        this.defaultOptions = {
            debug: false
        };

        if(typeof options === "undefined"){
            options = {};
        }

        this.init(options);
    };

    Reward.prototype = {

        /**
         * Initialize the object.
         *
         * @param options Object options
         */
        init: function(options){
            this.options = $.extend({}, this.defaultOptions, options);
            if(typeof this.options.page !== "undifined"){
                this.page = this.options.page;
            }
            this.debug('Initializing Rewards javascript');
            this.addListeners();
        },

        /**
         *
         * Display some message to the console. NOTE: you have to send to the object options -> debug: true!!!
         *
         * @param message Message to display to the console
         */
        debug: function(message){
            if(this.options.debug === true){
                console.log('Rewards: ' + message);
            }
        },

        addEditRewardListeners: function(){

            $this = this;

            $("#sets").fcbkcomplete({
                json_url: "./index.php?r=GamificationApp/RewardsSet/getSetsList",
                addontab: true,
                input_min_size: 0,
                width: '98.5%',
                cache: true,
                complete_text: '',
                filter_selected: true,
                maxitems: 999999999999999
            });

            $(document).on('submit', '#reward-create-form', function(e){
                $('#save').attr('disabled','disabled').addClass("disabled");
                if($('#association_mode_0:checked').length == 0) {
                    if(($('input:radio[name="association_mode"]:checked').val() == 'selection') && !$("#sets").val()) {
                        $('#sets_selector').find('.errorMessage').html(Yii.t('gamification', 'Please select at least one set'));
                        $('#save').removeAttr('disabled').removeClass("disabled");
                        e.preventDefault();
                    }
                }
            });

            $(document).on('change', 'input[name="association_mode"]', function(e){
                if($(e.target).val() == 'selection') {
                    $('#sets_selector').show();
                } else {
                    $('#sets_selector').hide();
                }
            });

            TinyMce.attach(".reward-text", {height: 200,
                plugins : [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste moxiemanager textcolor"
                ],
                toolbar : "undo redo | styleselect | bold italic fontsizeselect forecolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                language: yii.language
            });

            var previous = $("#lang_code").val();
            var default_language = $("#lang_code").val();

            $('.input-fields input[name^="title_"].title').val($("."+previous+" input").val());
            $(".reward-text").val($("."+previous+" textarea").val());

//            $('textarea[name=description_current]').val($());
            this.checkCompiledLanguages();

            $(document).on('keyup', '.input-fields input[name^="title_"].title', function(){
                $this.checkCompiledLanguages();
            });

            $(document).on('change', "#lang_code", function(){
                $("."+previous+" input").val($('.input-fields input[name^="title_"].title').val());
                $("."+previous+" textarea").val(tinymce.activeEditor.getContent());
                previous = this.value;
                $('.input-fields input[name^="title_"].title').val($("."+previous+" input").val());
                $(".reward-text").val($("."+previous+" textarea").val());
                tinymce.activeEditor.setContent($("."+previous+" textarea").val());
                $this.checkCompiledLanguages();
            });

            $(document).on('mousedown', 'form#reward-create-form input#save', function(){
                if($('input#association_mode_0').is(':checked') && $('form#reward-create-form div#sets_selector select#sets option').length > 0) {
                    $('form#reward-create-form div#sets_selector select#sets option').remove();
                    $('form#reward-create-form div#sets_selector ul > li.bit-input').remove();
                }

                $current_language = $('select#lang_code').val();
                $current_description = $('textarea[name="description_current"]').val();
                $('textarea[name="description_'+$current_language+'"]').val($current_description);
            });

            $('input[name=association_mode]').styler();
        },

        toggleAsterisk: function(language, action) {
            var text, el = $('#lang_code option[value="'+language+'"]');
            if (el.length > 0) {
                text = el.text();
                if (action) { //we want the asterisk
                    if (text.indexOf("* ") < 0) { //check if it is already present
                        el.text("* "+text);
                    }
                } else { //delete asterisk, if present
                    if (text.indexOf("* ") === 0) {
                        el.text(text.replace("* ", ''));
                    }
                }
            }
        },

        checkCompiledLanguages: function(){
            var languages = $('.translations :input');
            var counter = 0;
            var lang_code;
            for(var i = 0; i < languages.length; i++)
            {
                if (languages[i].name && languages[i].name.indexOf("name_") === 0) { //exclude descriptions, we need only names
                    lang_code = languages[i].name.replace("name_", "");
                    if(lang_code === $("#lang_code").val()) {
                        languages[i].value = $('.input-fields input[name^="title_"].title').val();
                    }
                    if (languages[i].value.length > 0) {
                        counter++;
                        this.toggleAsterisk(lang_code, true);
                    } else {
                        this.toggleAsterisk(lang_code, false);
                    }
                }
            }

            $('.assigned span').html(counter);
        },

        /**
         * Add your listeners here!
         */
        addListeners: function(){
            //Add listeners for Editing Reward Page ONLY
            if(this.page == 'page_edit_reward'){
                this.addEditRewardListeners();
            }
        }
    };

    RewardCarousel = function(options){
        this.options = {};

        this.numOfElements = 0;
        this.element = null;
        this.leftControl = null;
        this.rightControl = null;
        this.items = 1;
		this.loop = true;

        this.defaultOptions = {
            debug: false
        };

        if(typeof options === "undefined"){
            options = {};
        }

        this.init(options);
    };

    RewardCarousel.prototype = {
        init: function(options){

            this.options = $.extend({}, this.defaultOptions, options);
            if(typeof this.options.owlElement !== "undefined"){
                this.element = $('#' + this.options.owlElement);
            }
            if(typeof this.options.moveLeftControl !== "undefined"){
                this.leftControl = '#' + this.options.moveLeftControl;
            }
            if(typeof this.options.moveRightControl !== "undefined"){
                this.rightControl= '#' + this.options.moveRightControl;
            }
            if(typeof this.options.items !== "undefined"){
                this.items = this.options.items;
            }
            if(typeof this.options.loop !== "undefined"){
                this.loop = this.options.loop;
            }
            this.debug('Initializing Reward Carousel');
            this.addListeners();
        },

        debug: function(message){
            if(this.options.debug){
                console.log('RewardCarousel: ' + message);
            }
        },

        addListeners: function(){

            var $this = this;

            this.debug(this.element);

            this.element.owlCarousel({
                loop:this.loop,
                center:false,
                nav:false,
                dots:false,
                responsiveClass:true,
                mouseDrag: false,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:($this.items > 1) ? $this.items - 1 : 1
                    },
                    1000:{
                        items:$this.items
                    }
                }

            });


            $(document).on('click', this.leftControl, function(){
                $this.element.trigger('prev.owl.carousel',[300]);
            });

            $(document).on('click', this.rightControl, function(){
                $this.element.trigger('next.owl.carousel', [300]);
            });

        }
    };

    RewardsShop = function(options){
        this.options = {};

        this.dashletElement = null;
        this.dashletId = null;
        this.inSearchField = false;

        this.minRangePrice = null;
        this.maxRangePrice = null;

        this.defaultOptions = {
            debug: false
        };

        if(typeof options === "undefined"){
            options = {};
        }

        this.init(options);
    };

    RewardsShop.prototype = {
        init: function(options){

            this.options = $.extend({}, this.defaultOptions, options);

            if(typeof this.options.dashletElement !== "undefined"){
                this.dashletElement = '#' + this.options.dashletElement;
            }

            if(typeof this.options.dashletId !== "undefined"){
                this.dashletId = this.options.dashletId;
            }

            if(typeof this.options.minRangePrice !== "undefined"){
                this.minRangePrice = this.options.minRangePrice;
            }

            if(typeof this.options.maxRangePrice !== "undefined"){
                this.maxRangePrice = this.options.maxRangePrice;
            }

            this.debug('Initializing Reward Shop');
            this.addListeners();
        },

        debug: function(message){
            if(this.options.debug){
                console.log('RewardShop: ' + message);
            }
        },

        addListeners: function(){

            var $this = this;

            $('input[type=checkbox], input[type=radio]').styler();

            var currentMinRange = $($this.dashletElement + ' .filter-header .filter-container .slider-values .min').attr('data-price');
            var currentMaxRange = $($this.dashletElement + ' .filter-header .filter-container .slider-values .max').attr('data-price');

            $($this.dashletElement + ' .filter-header .filter-container .slider').slider({
                range: true,
                min: $this.minRangePrice,
                max: $this.maxRangePrice,
                values: [ currentMinRange, currentMaxRange ],
                slide: function( event, ui ) {
                    $('input#price-min-' + $this.dashletId).val(ui.values[ 0 ]);
                    $('input#price-max-' + $this.dashletId).val(ui.values[ 1 ]);
                    $($this.dashletElement + ' .filter-header .filter-container .slider-values .min').text(ui.values[ 0 ]);
                    $($this.dashletElement + ' .filter-header .filter-container .slider-values .max').text(ui.values[ 1 ]);
                }
            });

            $(document).off('click', $this.dashletElement + ' .filter-header .filters-button')
                .on('click', $this.dashletElement + ' .filter-header .filters-button', function(){
                    $this.openFilterContainer();
            });

            $(document).off('click', $this.dashletElement + ' .filter-header .apply-filter')
                .on('click', $this.dashletElement + ' .filter-header .apply-filter', function(){
                    $this.postForm();
                    $this.closeFilterContainer();
            });

            $(document).off('click', $this.dashletElement + ' .filter-header .filter-container .buttons .cancel')
                .on('click', $this.dashletElement + ' .filter-header .filter-container .buttons .cancel', function(){
                    $this.closeFilterContainer();
            });

            $(document).off('click', $this.dashletElement + ' .filter-header .filter-container .buttons .apply')
                .on('click', $this.dashletElement + ' .filter-header .filter-container .buttons .apply', function(){
                    $this.postForm();
                    $this.closeFilterContainer();
            });


            $(document).off('click', $this.dashletElement + ' .rewards-sets-selector .item')
                .on('click', $this.dashletElement + ' .rewards-sets-selector .item', function(){
                    $this.hideSetsChecks();
                    $(this).addClass('active');
                    $($this.dashletElement + ' input#set-filter-' + $this.dashletId).val($(this).attr('data-set'));
                    $($this.dashletElement + ' input[name=pnumber]').val(0);
                    $this.postForm();
            });

            $(document).off('click', $this.dashletElement + ' .filter-header .search-button')
                .on('click', $this.dashletElement + ' .filter-header .search-button', function(){
                    if(!$this.inSearchField){
                        $($this.dashletElement + ' .filter-header').addClass('search-activated').find('.search-field').show("slide", { direction: "right" }, 500, function() {
                            $this.inSearchField = true;
                            $(this).find('input[name="search_input"]').focus()
                        });
                    } else{
                        $this.postForm();
                        $this.inSearchField = false;
                    }
            });

            $(document).off('keydown', $this.dashletElement + ' .search-field input').on('keydown', $this.dashletElement + ' .search-field input', function(e){
                var elem = $(e.currentTarget);
                var code = e.keyCode || e.which;
                console.log(code);
                if (code == 13) {
                    e.preventDefault();
                    $this.postForm();
                    return false;
                } else if( code == 27){
                    $(this).parent('.search-field').hide();
                    $($this.dashletElement + ' .filter-header').removeClass('search-activated');
                    $this.inSearchField = false;
                }

                return true;
            });

            $(document).off('click', $this.dashletElement + ' .filter-header .search-field .clear-search')
                .on('click', $this.dashletElement + ' .filter-header .search-field .clear-search', function(){
                    $this.inSearchField = false;
                    $(this).parent('.search-field').hide();
                    $(this).parent().parent('.filter-header').removeClass('search-activated');
                    $(this).parent().parent().find('#search_input').val('');
                    $this.postForm();
            });

            $(document).off('click', $this.dashletElement + ' .reward-container .rewards-pager .dashlet-pager-next')
                .on('click', $this.dashletElement + ' .reward-container .rewards-pager .dashlet-pager-next', function (){
                    var page = $($this.dashletElement + ' input[name=pnumber]');
                    var currentPage = parseInt(page.val());
                    page.val(currentPage + 1);
                    $this.postForm();
            });

            $(document).off('click', $this.dashletElement + ' .reward-container .rewards-pager .dashlet-pager-prev')
                .on('click', $this.dashletElement + ' .reward-container .rewards-pager .dashlet-pager-prev', function (){
                    var page = $($this.dashletElement + ' input[name=pnumber]');
                    var currentPage = parseInt(page.val());
                    page.val(currentPage - 1);
                    $this.postForm();
            });
        },

        closeFilterContainer: function(){
            $(this.dashletElement + ' .filter-header .filters-button').removeClass('apply-filter');
            $(this.dashletElement + ' .filter-header .filters-button').text('Filter');
            $(this.dashletElement + ' .filter-header .filter-container').hide();
        },

        openFilterContainer: function(){
            $(this.dashletElement + ' .filter-header .filters-button').addClass('apply-filter');
            $(this.dashletElement + ' .filter-header .filters-button').text('Apply');
            $(this.dashletElement + ' .filter-header .filter-container').show();
        },

        hideSetsChecks: function(){
            $(this.dashletElement + ' .rewards-sets-selector .item').each(function(){
                $(this).removeClass('active');
            });
        },

        postForm: function(){
            var params = $.deparam($(this.dashletElement + ' form').serialize());

            var dlContent = $(this.dashletElement).parent();
            // Preserve the height during loading to avoid screen flickering
            // But first add "reloading" class to dl-content
            dlContent.addClass('reloading');
            var currentContentHeight = dlContent.height();
            dlContent.height(currentContentHeight);

            // Load this very specific dashlet!! (by id)
            // On callback, remove the "reloading" class
            $('#mydashboard').dashboard('loadContent', '.dashlet[data-dashlet-id="' + this.dashletId + '"]', params, function(){
                dlContent.removeClass('reloading');
            });

            delete this;
        }
    };


})(jQuery);