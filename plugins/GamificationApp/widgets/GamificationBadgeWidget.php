<?php

/**
 * Class GamificationBadgeWidget
 *
 * Renders the html for a gamification badge
 */
class GamificationBadgeWidget extends CWidget {

    public $showDetails = false;

    public function init() {}

    public function run() {
        $this->render('gamification_badge');
    }

} 