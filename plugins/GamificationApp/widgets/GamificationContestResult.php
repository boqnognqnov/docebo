<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 4.12.2015 г.
 * Time: 17:46
 */
class GamificationContestResult extends CWidget {

    /**
     * @var $contest GamificationContest
     */
    public $contest;
    public $chardData;
    public $langCode;
    public $columnNames;
    public $showCurrentUser;
    /**
     * @var $dataCurrentUser ChartData
     */
    public $dataCurrentUser;
    /**
     * @var $userModel CoreUser
     */
    public $userModel;

    public $contest_id;
    public $resultsNum;
    public $showContestDescription;
    public $showOnlyDescription = false;
    public $showFullChart = false;
    public $showInDialog = true;

    public function init(){
        Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl.'/css/admin.css');
        $this->setId('contest-' . $this->contest_id);

        $this->langCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

        $this->contest = GamificationContest::model()->findByPk($this->contest_id);

        $chart = GamificationContestChart::model()->findAllByAttributes(array(
            'id_contest' =>$this->contest->id
        ));

        if(empty($chart)){
            $this->showOnlyDescription = true;
        }

        $userId = Yii::app()->user->id;

        if(!$this->showOnlyDescription){

            if(!$this->showFullChart){
                if(!isset($this->resultsNum)){
                    $this->resultsNum = count($this->contest->rewards);
                }
            }

            $this->columnNames = GamificationContest::scoreColumnName();

            if(!empty($chart)){
                foreach($chart as $row){
                    if($row->id_user == $userId){
                        $this->dataCurrentUser = $row;
                    }

                    $this->chardData[] = $row;
                }
            }

            if(!empty($this->dataCurrentUser)){
                $this->showCurrentUser = true;
                $this->userModel = CoreUser::model()->findByPk($userId);
            }
        }
    }

    public function run(){
        if($this->showOnlyDescription){
            $this->render('contest_result/_description');
        } else {
            $currentUserKey = $this->dataCurrentUser->rank - 1;
            $previousUser = '';
            $nextUser = '';
            $headerUsersResults = array();

            if ($currentUserKey != -1) {
                // the current user is in the contest, so get data for rendering the header part of the view
                if ($currentUserKey - 1 >= 0) {
                    $previousUser = CoreUser::model()->findByPk($this->chardData[$currentUserKey - 1]->id_user);
                    if(!$previousUser){
                        $previousUser = $this->chardData[$currentUserKey - 1];
                    }
                    $headerUsersResults[] = $this->chardData[$currentUserKey - 1];
                }
                $headerUsersResults[] = $this->chardData[$currentUserKey];
                if ($currentUserKey + 1 < count($this->chardData)) {
                    $nextUser = CoreUser::model()->findByPk($this->chardData[$currentUserKey + 1]->id_user);
                    if(!$nextUser){
                        $nextUser = $this->chardData[$currentUserKey + 1];
                    }
                    $headerUsersResults[] = $this->chardData[$currentUserKey + 1];
                }
            }
            $surroundedUserModels = array_filter(array($previousUser, $nextUser));

            $this->render('contest_result/index', array(
                'surroundedUserModels' => $surroundedUserModels,
                'headerUsersResults' => $headerUsersResults
            ));
        }
    }
}