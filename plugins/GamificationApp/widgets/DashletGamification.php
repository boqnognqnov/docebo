<?php
/**
 * Created by PhpStorm.
 * User: NikVas
 * Date: 16-Dec-15
 * Time: 17:38
 */

class DashletGamification extends DashletWidget implements IDashletWidget
{


    /**
     * Dashlet descriptor
     *
     * @return DashletDescriptor Object, describing the dashlet to the outside world
     */
    public static function descriptor($params = false)
    {
        $sampleImageUrl = Yii::app()->mydashboard->getModule()->getAssetsUrl() . "/images/gamification_widget_preview.jpg";

        $descriptor = new DashletDescriptor();

        $descriptor->name = 'core_dashlet_gamification';
        $descriptor->handler = 'plugin.GamificationApp.widgets.DashletGamification';
        $descriptor->title = 'Gamification';
        $descriptor->description = Yii::t('gamification', 'Display gamification badges, leaderboards and latest contests in the user dashboard.');
        $descriptor->sampleImageUrl = $sampleImageUrl;
        $descriptor->settingsFieldNames = array(
            'showing_order',
            'badges_display_max',
            'badges_show_not_awarded',
            'badges_show_view_all',
            'contests_display_max',
            'contests_show_last',
            'contests_show_last_if_no_active',
        );
        $descriptor->disallowMultiInstance = false; // do not allow more than one dashlet of this type in the same layout

        return $descriptor;
    }

    /**
     * Render dashlet settings, used in Admin Dashlets UI
     */
    public function renderSettings()
    {   $prevousShowSettings = $this->getParam('showing_order',false);
        if(!$prevousShowSettings || empty($prevousShowSettings)) {
            $prevousShowSettings = array('badges' => false, 'leaderboard' => false, 'contests' => false);
        }
        elseif(count($prevousShowSettings) < 3){
            $nonSelected = array('badges'=>'off','leaderboard'=>'off','contests'=>'off');
            $prevousShowSettings = array_merge($prevousShowSettings,array_diff_key($nonSelected,$prevousShowSettings));

        }
        $badgesDisplayMax           =   $this->getParam('badges_display_max',10);
        $badgesShowNotAwarded       =   $this->getParam('badges_show_not_awarded',false);
        $badgesShowViewAll          =   $this->getParam('badges_show_view_all',false);
        $contestsDisplayMax         =   $this->getParam('contests_display_max',10);
        $contestsShowLast           =   $this->getParam('contests_show_last',false);
        $contestsShowLastIfNoActive =   $this->getParam('contests_show_last_if_no_active',false);
        $this->render('dashlet_gamification_settings',
                array(
                'prevousShowSettings'       =>$prevousShowSettings,
                'badgesDisplayMax'          =>$badgesDisplayMax,
                'badgesShowNotAwarded'      =>$badgesShowNotAwarded,
                'badgesShowViewAll'         =>$badgesShowViewAll,
                'contestsDisplayMax'        =>$contestsDisplayMax,
                'contestsShowLast'          =>$contestsShowLast,
                'contestsShowLastIfNoActive'=>$contestsShowLastIfNoActive,
                )
        );
    }

    /**
     * Render fronend content.
     *
     * NOTE: Don't use Yii's CClientScript to register js/css resources
     * here since it doesn't work. Use echo '<script src=...</script>' instead
     */
    public function renderFrontEnd()
    {
        // TODO: Implement renderFrontEnd() method.
        $prevousShowSettings = $this->getParam('showing_order',false);
        if(!$prevousShowSettings || empty($prevousShowSettings)) {
            $prevousShowSettings = array('contests' => false, 'badges' => false, 'leaderboard' => false);
        }
        elseif(count($prevousShowSettings) < 3){
            $nonSelected = array('contests'=>'off','badges'=>'off','leaderboard'=>'off');
            $prevousShowSettings = array_merge($prevousShowSettings,array_diff_key($nonSelected,$prevousShowSettings));

        }
        $removeContestsFromTabs = FALSE;
        $removeBadgesFromTabs = FALSE;
        $removeChartsFromTabs = FALSE;
        $dashboardCellModel = null;

        if(isset($this->dashletModel->cell)){
            $dashboardCellModel = DashboardCell::model()->findByPk($this->dashletModel->cell);
        }

        $localFormat = Yii::app()->localtime->YiitoPHPDateFormat(Yii::app()->localtime->getLocalDateTimeFormat('short'));
        $badgesContent = array();
        $contestsContent = array();
        $chartContent = array();
        
        $tabs = array();
        $previousShowSettings = $prevousShowSettings;
        
        foreach($previousShowSettings as $key => $tab) {
            $tabStatus = $tab == 'on';
            $previousShowSettings[$key] = $tabStatus;
                        
            $tabs[] = array(
                'name' => ucfirst($key),
                'enabled' => $tabStatus,
                'key' => $key,
                'url' => ''
            );
        }
        
        $settings = (object) array(
            'badgesDisplayMax'          =>$this->getParam('badges_display_max',10),
            'badgesShowNotAwarded'      =>$this->getParam('badges_show_not_awarded',false),
            'badgesShowViewAll'         =>$this->getParam('badges_show_view_all',false),
            'contestsDisplayMax'        =>$this->getParam('contests_display_max',10),
            'contestsShowLast'          =>$this->getParam('contests_show_last',false),
            'contestsShowLastIfNoActive'=>$this->getParam('contests_show_last_if_no_active',false),
        );
        
        if(!empty($previousShowSettings['contests'])) {
            $contestsTabTitle = '';
            $settings->contestsGoals = GamificationContest::scoreColumnName();
            // IMPORTANT TODO: Style the contests dashlet
            $lang_code = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
            $user_id = Yii::app()->user->getId();
            if($settings->contestsShowLast) {
                $contestsTabTitle = 'My last contest';
                $contest_ids = implode("','", GamificationContest::model()->getLastContestByUser($user_id, array(TRUE)));

                $select = array(
                    'contest_id'                => 'gc.id',
                    'contest_date_start'        => "CONVERT_TZ(`gc`.`from_date`, IFNULL(`gc`.`timezone`, '{$defaultTimezone}'), '+00:00')",
                    'contest_date_end'          => "CONVERT_TZ(`gc`.`to_date`, IFNULL(`gc`.`timezone`, '{$defaultTimezone}'), '+00:00')",
                    'contest_title'             => 'gct.name',
                    'contest_title_default'     => 'gcd.name',
                    'contest_goal_key'          => 'gc.goal',
                    'contest_has_finished'      => "IF(CONVERT_TZ(`gc`.`to_date`, IFNULL(`gc`.`timezone`, '{$defaultTimezone}'), '+00:00') < NOW(),'YES', 'NO')",
                    'contest_has_not_started'   => "IF(CONVERT_TZ(`gc`.`from_date`, IFNULL(`gc`.`timezone`, '{$defaultTimezone}'), '+00:00') > NOW(),'YES', 'NO')",
                    'user_id'                   => 'gcc.id_user',
                    'user_rank'                 => 'gcc.rank',
                    'user_name'                 => 'gcc.full_name',
                    'user_result'               => 'gcc.ranking_result',
                    'user_reward_data'          => 'gcc.reward_data',
                    'user_avatar'               => 'cu.avatar',
                );

                $select_sql = '';
                foreach($select as $alias => $field) {
                    $select_sql .= "{$field} AS {$alias}";
                    if($field != end($select)) {
                        $select_sql .= ", ";
                    }
                }
                
                $useUserTimezone = Settings::get('timezone_allow_user_override', 'off');

                $timezone = '';
                $defaultTimezone = Settings::get('timezone_default', 'UTC');

                if($useUserTimezone == 'on'){
                    $userTimezone = CoreSettingUser::model()->findByAttributes(array(
                        'path_name' => 'timezone',
                        'id_user' => Yii::app()->user->id
                    ));

                    if($userTimezone){
                        $timezone = $userTimezone->value;
                    }
                } else{
                    $timezone = $defaultTimezone;
                }
                
                $default_language = Settings::get('default_language', 'english');
                
                $last_contests_sql = Yii::app()->db->createCommand(
                    "SELECT {$select_sql}" . "\n" . 
                    'FROM ' . GamificationContest::model()->tableName() . ' AS gc' . "\n" .
                    'LEFT JOIN ' . GamificationContestTranslation::model()->tableName() . " AS gct ON `gct`.`id_contest` = `gc`.`id` AND `gct`.`lang_code` = '{$lang_code}'" . "\n" .
                    'LEFT JOIN ' . GamificationContestTranslation::model()->tableName() . " AS gcd ON gcd.id_contest = gc.id AND gcd.lang_code = '{$default_language}' \n" .
                    'LEFT JOIN ' . GamificationContestChart::model()->tableName() . " AS gcc ON `gcc`.`id_contest` = gc.id AND `gcc`.`id_user` = '{$user_id}'" . "\n" .
                    'LEFT JOIN ' . CoreUser::model()->tableName() . " AS cu ON `cu`.`idst` = '{$user_id}'" . "\n" .
                    "WHERE CONVERT_TZ(`gc`.`from_date`, IFNULL(`gc`.`timezone`, '{$defaultTimezone}'), '+00:00') <= NOW() AND CONVERT_TZ(`gc`.`to_date`, IFNULL(`gc`.`timezone`, '{$defaultTimezone}'), '+00:00') >= NOW() AND `gc`.`id` IN ('{$contest_ids}')"
                );
                $last_contests_db_ = $last_contests_sql->queryAll();
                $last_contests_db = $last_contests_db_;
                $last_contests = array();
                
                foreach($last_contests_db as $key => $contest) {
                    $last_contests_db[$key] = $contest = (object) $contest;
                    if($contest->contest_has_finished == "YES" || $contest->contest_has_not_started == "YES") {
                        unset($last_contests_db[$key]);
                        continue;
                    }
                    $last_contests_db[$key]->contest_title = !empty($contest->contest_title) ? $contest->contest_title : $contest->contest_title_default;
                }
                
                if(empty($last_contests_db) && $settings->contestsShowLastIfNoActive) {
                    $contest = GamificationContest::model()->getLastContestByUser($user_id);
	                $contestModel = new GamificationContest();
	                $userContestsIds = $contestModel->getUserContests();
	                $contestPk = $contest->getPrimaryKey();
	                if(is_array($userContestsIds) && $contest && $contestPk && in_array($contestPk, $userContestsIds)){
		                $contest_keys = array_keys($select);
		                $contestChart = GamificationContestChart::model()->findByAttributes(array('id_contest' => $contest->id, 'id_user' => $user_id));
		                $today = Yii::app()->localtime->getUTCNow('Y-m-d') . ' 00:00:00';
		                $contest_is_finished = Yii::app()->localtime->isGt($today, $contest->to_date) && Yii::app()->localtime->isLt($contest->from_date, $today);
		                $contest_is_scheduled = Yii::app()->localtime->isGt($contest->from_date, $today) && Yii::app()->localtime->isGt($contest->to_date, $today);
		                $contest_title_default = GamificationContestTranslation::model()->findByAttributes(array('id_contest' => $contest->id, 'lang_code' => Settings::get('default_lanugage')));
		                $contest_values = array(
			                $contest->id,
			                $contest->from_date,
			                $contest->to_date,
			                $contest->name,
			                $contest_title_default,
			                $contest->goal,
			                ($contest_is_finished) ? 'YES' : 'NO',
			                ($contest_is_scheduled) ? 'YES' : 'NO',
			                $contestChart->id_user,
			                $contestChart->rank,
			                $contestChart->full_name,
			                $contestChart->ranking_result,
			                $contestChart->reward_data,
			                CoreUser::model()->findByPk($user_id)->getAvatarImage()
		                );
		                $last_contests_db[$contest->id] = $this->_array_combine($contest_keys, $contest_values);
	                }

                }
                
                foreach($last_contests_db as $key => $contest) {
                    $last_contests_db[$key] = $contest = (object) $contest;
                    if(!empty($contest->user_reward_data))
                        $last_contests_db[$key]->user_reward_data = json_decode($contest->user_reward_data);
                    
                    $fromDate = substr(date($localFormat, strtotime($contest->contest_date_start)), 0, 10);
                    $toDate = substr(date($localFormat, strtotime($contest->contest_date_end)), 0, 10);
                    $last_contests_db[$key]->contest_period = "{$fromDate} / {$toDate}";
                    
                    $contest_model = GamificationContest::model()->findAllByPk($contest->contest_id);
                    $last_contests_db[$key]->contest_model = $contest_model[0];
                    
                    $user_avatar = '';
                    $user_model = CoreUser::model()->findByPk($user_id);
                    if(!empty($user_model)) $last_contests_db[$key]->user_avatar = $user_model->getAvatarImage();
                    
                    $chart_url = Docebo::createAdminUrl('GamificationApp/Contests/showResults', array(
                        'id' => $contest->contest_id,
                        'full'=>true,
                        'current_user'=>false
                    ));
                    $last_contests_db[$key]->contest_url = $chart_url;
                    
                    if(!empty($contest->user_reward_data) && !empty($contest->user_reward_data->id_item)) {
                        $rewardIcon = '';

                        switch($contest->user_reward_data->type) {
                            default:
                            case GamificationContestReward::REWARD_TYPE_BADGE:
                                $reward = GamificationContestReward::model()->findByAttributes(array(
                                    'id_contest' => $contest->contest_id,
                                    'rank' => $contest->user_rank
                                ));
                                if(!empty($reward)) {
                                    $rewardIcon = $reward->getBadgeUrl();
                                }
                                break;
                        }
                        $last_contests_db[$key]->user_reward_data->reward_icon_url = $rewardIcon;
                    }
                    
                    $last_contests[$contest->contest_id] = $last_contests_db[$key];
                }
                
                foreach($last_contests as $key => $contest) {
                    if(empty($key)) {
                        unset($last_contests[$key]);
                    }
                }

                $last_contests_count = count($last_contests);
                $last_contest_ = end($last_contests);

                if($last_contests_count > 1) {
                    $contestsTabTitle = 'My active contests';
                } elseif($last_contests_count == 1 && $last_contest_->contest_has_finished == 'NO' && $last_contest_->contest_has_not_started == 'NO') {
                    $contestsTabTitle = 'My active contest';
                } elseif($last_contests_count == 1 && $settings->contestsShowLastIfNoActive && ($last_contest_->contest_has_finished == 'YES' || $last_contest_->contest_has_not_started == 'YES')) {
                    $contestsTabTitle = 'My last contest';
                }
            }
                
            
            $upcoming_contests_ids = GamificationContest::model()->getUserContests();
            
            $upcoming_contests = array();
            
			$upcoming_contests_db = Yii::app()->db->createCommand('SELECT gc.id as cid, gc.from_date, gc.to_date, gct.lang_code, gct.name FROM ' . GamificationContest::model()->tableName() . ' AS gc
                LEFT JOIN ' . GamificationContestTranslation::model()->tableName() . ' AS gct ON gct.id_contest LIKE gc.id AND gct.lang_code = "' . $lang_code . '"
                WHERE gc.id IN ("' . implode('","', $upcoming_contests_ids) . '") AND gc.from_date > NOW()'
            )->queryAll();
            
            $upcoming_contests_db = array_slice($upcoming_contests_db, 0, $settings->contestsDisplayMax);

            
            foreach($upcoming_contests_db as $contest) {
                
                $fromDate = substr(date($localFormat, strtotime($contest['from_date'])), 0, 10);
                $toDate = substr(date($localFormat, strtotime($contest['to_date'])), 0, 10);
                
                if(empty($contest['name'])) {
                    $model_t = GamificationContestTranslation::model()->findByAttributes(array('id_contest' => $contest['cid'], 'lang_code' => Settings::get('default_language')));
                    $contest['name'] = $model_t->name;
                }
                
                $upcoming_contests[] = (object) array(
                    'period'    => "{$fromDate} / {$toDate}",
                    'title'     => $contest['name']
                );
            }
            
            if(empty($upcoming_contests_db) && $last_contests_count == 0) $removeContestsFromTabs = TRUE;
                
            $contestsContent = array(
                'last_contests' => $last_contests,
                'upcoming_contests' => $upcoming_contests,
                'contests_tab_title' => Yii::t('gamification', $contestsTabTitle),
                'removeContestsFromTabs' => $removeContestsFromTabs
            );
        }
        
        if(!empty($previousShowSettings['badges'])) {
            $countPoints = 0;
            $countBadges = 0;

            $badges = GamificationAssignedBadges::model()->getUserBadges(Yii::app()->user->id);
            $badgesNotAwarded = GamificationBadge::model()->getAllBadgesWithScore(Yii::app()->user->id);
            $allBadges = array_merge($badges, $badgesNotAwarded);

            if (count($badges) > 0) {
                foreach ($badges as $badge) {
                    $countPoints += intval($badge->gamificationBadge->badgeCounterScore);
                    $countBadges += (is_null($badge->gamificationBadge->badgeCounter) ? 1 : $badge->gamificationBadge->badgeCounter);
                }
            } else {
                $removeBadgesFromTabs = TRUE;
            }
            
            $badgesContent = array(
                'countBadges'       => $countBadges,
                'countPoints'       => $countPoints,
                'badges'            => $badges,
                'allBadges'         => $allBadges,
                'badgesNotAwarded'  => $badgesNotAwarded,
                'removeBadgesFromTabs' => $removeBadgesFromTabs
            );
        }
        
        if(!empty($previousShowSettings['leaderboard'])) {
            // IMPORTANT TODO: CREATE THE LEADERBOARD FROM SCRATCH !!!
            
                  
            $activeLeaderboars = GamificationLeaderboard::model()->getActiveLeaderboards(Yii::app()->user->id);
            $hasActiveLeaderboards = !empty($activeLeaderboars) ? true : false;
            $leaderboardsChartData = array();
            $leaderboards = array();

            if($hasActiveLeaderboards){
                $command = Yii::app()->db->createCommand();
                $command->select('t.id as id, tr.name as name')
                    ->from(GamificationLeaderboard::model()->tableName() . ' t')
                    ->leftJoin(GamificationLeaderboardTranslation::model()->tableName() . ' tr', 'tr.id_board = t.id AND tr.lang_code = "' . Lang::getCodeByBrowserCode(Yii::app()->getLanguage()) . '"')
                    ->where('t.id IN ( ' . implode(',', $activeLeaderboars['leaderboards']) . ')')
                    ->group('t.id');
                $leaderboards = $command->queryAll();

                foreach($leaderboards as $key => $board) {
                    if(empty($board['name'])) {
                        $b = GamificationLeaderboardTranslation::model()->findByAttributes(array('id_board' => $board['id'], 'lang_code' => Settings::get('default_language')));
                        $leaderboards[$key]['name'] = $b->name;
                    }
                }
            
                foreach($activeLeaderboars['leaderboards'] as $leaderboard){
                    $leaderboardsChartData[$leaderboard]['points'] = GamificationAssignedBadges::getPointsChart(5, $activeLeaderboars['users'][$leaderboard]);
                    $leaderboardsChartData[$leaderboard]['userPoints'] = GamificationAssignedBadges::getPointsChartPosition(Yii::app()->user->id, $activeLeaderboars['users'][$leaderboard]);
                }
            } else {
                $removeChartsFromTabs = TRUE;
            }
            
            $chartContent = array(
                'leaderboards_' => Yii::app()->getController()->renderPartial('plugin.GamificationApp.views.gamificationApp._leaderboards', array(
                    'hasActiveLeaderboard' => $hasActiveLeaderboards,
                    'leaderboards' => $leaderboards,
                    'dashboardCellModel' => $dashboardCellModel,
                    'leaderboardChartData' => $leaderboardsChartData,
                    'topTitleCount' => 5,
                    'hideTitle' => true
                ), true),
                'removeChartsFromTabs' => $removeChartsFromTabs
            );
        }
        
        $viewContent = array(
            'badgesContent' => (object) $badgesContent,
            'contestsContent' => (object) $contestsContent,
            'leaderboardContent' => (object) $chartContent,
            'settings' => $settings,
            'tabs' => $tabs
        );
        
        $this->render('dashlet_gamification/gamification_dashlet', $viewContent);
    }
    
    
    /**
     ***** I have created this function because the default "array_combine" PHP function is from version 5.4.0... *****
     * Creates an array by using the values from the keys array as keys and the values from the values array as the corresponding values.
     * @param array $array_1
     * @param array $array_2
     * @return array Returns the combined array, FALSE if the number of elements for each array isn't equal.
     */
    private function _array_combine($keys = array(), $values = array()) {
        if(function_exists('array_combine')) {
            return call_user_func('array_combine', $keys, $values);
        }
        $result = array();
        if(count($keys) != count($values)) {
            trigger_error('array_combine(): Both parameters should have an equal number of elements', E_USER_WARNING);
            return FALSE;
        }
        foreach($keys as $key => $value) {
            $result[$value] = $values[$key];
        }
        return $result;
    }

    /**
     * Run arbitrary code just before the main page (dashboard) is going to be loaded (rendered) IF the dashlet takes part of it
     *
     * <strong>Note 1</strong>: this method is called BEFORE rendering of the main page, as an attribute of an instance created BEFORE rendering!
     * Which means, DURING the rendering of the dashlet, another object instance is created/used, having no relation to the first instance
     * The first intance is purely created (and dismissed) for the porpose of bootstraping to allow, mainly, preloading client (browser) resources
     *
     * <strong>Note 2</strong>: Be aware, if you have 2 dashlets of the same type in the dashboard, bootstrap() will be called 2 times!!!
     *
     * <strong>Hint</strong>: Use this to execute Yii::app()->getClientScript()->registerXXXXX(<url>) to load dashlet specific own javascript/css
     *
     * @param string $params
     */
    public function bootstrap($params = false)
    {
        // TODO: Implement bootstrap() method.
    }
}

