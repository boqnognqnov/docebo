<?php

class GamificationBadgeNotification extends CWidget {

    /**
     * @var GamificationAssignedBadges
     */
    public $badge;

    public function init() {}

    public function run() {
        if ($this->badge) {
            $this->render('widget_badge_notification');
        }
    }
}