<?php

class GamificationAppUserMenu extends CWidget {

    /**
     * The max number of badges to show in the user menu
     * @var int
     */
    public $limit = 4;
    /**
     * @var GamificationAssignedBadges[]
     */
    public $badges;
    /**
     * @var int
     */
    public $totalBadges;
    /**
     * @var int
     */
    public $totalScore;
    /**
     * @var int
     */
    public $scoreRank;
    /**
     * @var int
     */
    public $lastContest;

    public function init() {
        $this->badges = GamificationAssignedBadges::model()->getUserBadges(Yii::app()->user->id, $this->limit);
    }

    public function run() {
        if (count($this->badges)>0) {
            /*
             * Calculate the score for ALL badges
             */
            $scorePoints = 0;
		  $totalBadges = 0;
            $badges = GamificationAssignedBadges::model()->getUserBadges(Yii::app()->user->id);
            if (count($badges) > 0) {
                foreach ($badges as $badge) {
                    $scorePoints += intval($badge->gamificationBadge->badgeCounterScore);
				$totalBadges += (is_null($badge->gamificationBadge->badgeCounter) ? 1 : $badge->gamificationBadge->badgeCounter);
                }
            }
            $this->totalScore = $scorePoints;
            $this->totalBadges = $totalBadges;

            // get user rank
            $userChartPosition = GamificationAssignedBadges::getPointsChartPosition(Yii::app()->user->id);
            if (isset($userChartPosition['position'])) {
                $this->scoreRank = $userChartPosition['position'];
            }
        }
        $this->lastContest = GamificationContest::getLastContestByUser(Yii::app()->user->id);

        $this->render('widget_user_menu');
    }
}