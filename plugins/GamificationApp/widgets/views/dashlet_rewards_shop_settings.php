<?php
/**
 * Created by PhpStorm.
 * User: NikVas
 * Date: 17-Dec-15
 * Time: 16:11
 */
?>
<div>
    <strong><?=Yii::t('app7020','Show');?></strong>
    <hr>
</div>
<div class="tabs-settings-container gamification-rewards-shop-dashlet-settings">
    <div class="max-items-settings">
        <?=Yii::t('gamification','Display max {input} items',array('{input}'=>CHtml::textField('max_items',(isset($maxItems) && $maxItems > -1)?$maxItems:10,array('class'=>'max-items-inputs'))));?>
        <?= CHtml::label(Yii::t('app7020', 'Leave blank to display all items'),'max_items',array('class'=>'max-hints')); ?>
    </div>
    <div class="tabs-settings-option-container first-setting">
        <?= CHtml::checkBox('show_reward_history', ((isset($showRewardHistory) && $showRewardHistory==true)?true:false)  ); ?>
        <?= CHtml::label(Yii::t('gamification', 'Show "My reward history" link'),'show_reward_history',array('class'=>'tabs-settings-label')); ?>
    </div>
    <div class="tabs-settings-option-container">
        <?= CHtml::checkBox('show_go_to_reward_shop', ((isset($showGotoRewardShop) && $showGotoRewardShop==true)?true:false)  ); ?>
        <?= CHtml::label(Yii::t('gamification', 'Show "Go to Rewards Shop" link'),'show_go_to_reward_shop',array('class'=>'tabs-settings-label')); ?>
    </div>
</div>

