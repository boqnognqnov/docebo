<?php
/**
 * Created by PhpStorm.
 * User: NikVas
 * Date: 17-Dec-15
 * Time: 16:11
 */
?>
<div>
    <strong><?=Yii::t('app7020','Show');?></strong>
</div>
<div id="showing-order-hint">
    <?=Yii::t('standard','You must select at least one item. Drag & drop to reorder items')?>
</div>

<table id="gamification-dashlet-tabs-order">
    <tbody class="sortable">

    <?php foreach($prevousShowSettings as $key=>$setting){
    ?>
    <tr id="showing-order-<?=$key?>" data-tab-type="<?=$key?>">
        <td class="checkbox-table-data">
            <input name="showing_order[<?=$key?>]" data-item-type="id_item" type="checkbox"  <?= ($setting=="on")?'checked="checked"' : ''; ?>/><!--value="<?//=$key ?>"-->
        </td>
        <td class="name-table-data"><?php
            switch($key){
                case 'badges':
                    echo Yii::t('gamification','Badges');
                    break;
                case 'contests':
                    echo Yii::t('gamification','Contests');
                    break;
                case 'leaderboard':
                    echo Yii::t('gamification','Top points chart');
                    break;
            }
            ?></td>
        <td class="actions-table-data"><span class="p-sprite drag-small-black move"></span></td>
    <tr>
        <?php } ?>
    </tbody>
</table>
<div class="gamification-dashlet-tabs-settings" id="gamification-dashlet-badges-settings">
    <div class="gamification-dashlet-tabs-title" id="gamification-dashlet-badges-settings-titles">
        <strong><?=strtoupper(Yii::t('gamification','Badges'));?></strong>
    </div>
    <div class="tabs-settings-container">
        <div class="max-items-settings">
            <?=Yii::t('gamification','Display max {input} items',array('{input}'=>CHtml::textField('badges_display_max',(isset($badgesDisplayMax) && $badgesDisplayMax > -1)?$badgesDisplayMax:10,array('class'=>'max-items-inputs'))));?>
        </div>
        <div class="tabs-settings-option-container first-setting">
            <?= CHtml::checkBox('badges_show_not_awarded', ((isset($badgesShowNotAwarded) && $badgesShowNotAwarded==true)?true:false)  ); ?>
            <?= CHtml::label(Yii::t('gamification', 'Show also how many badges are not yet awarded'),'badges_show_not_awarded',array('class'=>'tabs-settings-label')); ?>
        </div>
        <div class="tabs-settings-option-container">
            <?= CHtml::checkBox('badges_show_view_all', ((isset($badgesShowViewAll) && $badgesShowViewAll==true)?true:false)  ); ?>
            <?= CHtml::label(Yii::t('gamification', 'Show "view all" link'),'badges_show_view_all',array('class'=>'tabs-settings-label')); ?>
        </div>
    </div>
</div>
<div class="gamification-dashlet-tabs-settings" id="gamification-dashlet-contests-settings">
    <div class="gamification-dashlet-tabs-title" id="gamification-dashlet-contests-settings-titles">
        <strong><?=strtoupper(Yii::t('gamification','Contests'));?></strong>
    </div>
    <div class="tabs-settings-container">
        <div class="max-items-settings">
            <?=Yii::t('gamification','Display max {input} upcoming contests',array('{input}'=>CHtml::textField('contests_display_max',(isset($contestsDisplayMax) && $contestsDisplayMax > -1)? $contestsDisplayMax : 10,array('class'=>'max-items-inputs'))));?>
        </div>
        <div class="tabs-settings-option-container first-setting">
            <?= CHtml::checkBox('contests_show_last', ((isset($contestsShowLast) && $contestsShowLast==true)?true:false) ); ?>
            <?= CHtml::label(Yii::t('gamification', 'Show "My last contest"'),'contests_show_last',array('class'=>'tabs-settings-label')); ?>
        </div>
        <div class="tabs-settings-option-container">
            <?= CHtml::checkBox('contests_show_last_if_no_active', ((isset($contestsShowLastIfNoActive) && $contestsShowLastIfNoActive==true)?true:false) ); ?>
            <?= CHtml::label(Yii::t('gamification', 'Show "My last contests if no contests are active"'),'contests_show_last_if_no_active',array('class'=>'tabs-settings-label')); ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){

        $('.sortable').sortable({
            handle: '.move',
            opacity: 0.6,
            cursor: 'move',
            tolerance: 'pointer',
            forcePlaceholderSize: true,
            update: function(event, ui){
                //sort();
            },
            helper: function(e, tr) {
                var $originals = tr.children();
                var $helper = tr.clone();

                $helper.children().each(function(index) {
                    // Set helper cell sizes to match the original sizes
                    $(this).width($originals.eq(index).width());
                });
                return $helper;
            },
            create: function(){
//                $('body').css('overflow-y', 'visible');
//                $(this).css('width', '100%');
            },
        });
        $('table#gamification-dashlet-tabs-order tr').on('mouseover', function(){
            $('.move', $(this)).css('display','block');
            $('td', $(this)).css('background-color','#f7f7f7');
        }).on('mouseout',function(){
            $('.move', $(this)).css('display','none');
            $('td', $(this)).css('background-color','transparent');
        });
        $('tr#showing-order-badges input[type="checkbox"]').on('change',function(){
            if($(this).is(':checked')){
                $('div#gamification-dashlet-badges-settings.gamification-dashlet-tabs-settings').show();
            }
            else{
                $('div#gamification-dashlet-badges-settings.gamification-dashlet-tabs-settings').hide();
            }
        });
        $('tr#showing-order-contests input[type="checkbox"]').on('change',function(){
            if($(this).is(':checked')){
                $('div#gamification-dashlet-contests-settings.gamification-dashlet-tabs-settings').show();
            }
            else{
                $('div#gamification-dashlet-contests-settings.gamification-dashlet-tabs-settings').hide();
            }
        });

        if(!$('tr#showing-order-badges input[type="checkbox"]').is(':checked')){
            $('div#gamification-dashlet-badges-settings.gamification-dashlet-tabs-settings').hide();
        }

        if(!$('tr#showing-order-contests input[type="checkbox"]').is(':checked')){
            $('div#gamification-dashlet-contests-settings.gamification-dashlet-tabs-settings').hide();
        }

        $('#create-dashlet-wizard.modal-body.opened').css({height: '680px', paddingBottom: 0});
    });
</script>
