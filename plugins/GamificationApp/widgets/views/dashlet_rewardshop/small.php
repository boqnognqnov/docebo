<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 16.2.2016 г.
 * Time: 10:04
 *
 * @var $this DashletRewardShop
 * @var $userModel CoreUser
 * @var $userCoins integer
 * @var $rewardsData array
 * @var $showGoToRewardShop
 * @var $showHistory
 * @var $maxItems
 */
?>
<?php if($showGoToRewardShop): ?>
<div class="dl-footer">
    <div class="span6 dl-footer-left 	text-left"></div>
    <div class="span6 dl-footer-right 	text-right"><a href="<?=Docebo::createAppUrl('lms:site/index', array(
            'opt' => 'fullrewardsshop'
        ))?>"><?=Yii::t('gamification', 'Go to Rewards Shop')?></a></div>
    <div class="clearfix"></div>
</div>
<?php endif; ?>

<div class="dashlet-rewards-shop-two-third" id="dashlet-reward-shop-<?=$this->dashletModel->id ?>">
    <h5><?=Yii::t('gamification', 'Rewards') ?></h5>
    <div class="user-info-container">
        <hr>
        <div class="row-fluid">
            <div class="block">
                <div class="user-image">
                    <?= $userModel->getAvatarImage()?>
                </div>
            </div>
            <div class="block">
                <div class="row-fluid username">
                    <?= $userModel->getFullName() ?>
                </div>
                <div class="row-fluid usercoins">
                    <i class="fa fa fa-database fa-lg yellow"></i>
                    <span><?= Yii::t('gamification', 'My coins') ?>&nbsp;<strong><?= $userCoins ?></strong></span>
                </div>
            </div>
            <?php if($showHistory): ?>
                <div class="buttonHistoryContainer">
                    <?= CHtml::link(Yii::t('gamification', 'My rewards request history'), Docebo::createAbsoluteUrl('//GamificationApp/GamificationApp/MyBadges&tab=rewards'), array(
                        'class' => 'rewards-history-link'
                    )); ?>
                </div>
            <?php endif; ?>
        </div>

        <hr>
    </div>
    <br>
    <br>
    <div class="rewards-carousel">
        <?php if(!empty($rewardsData)): ?>
            <div class="row-fluid">
                <div class="span1">
                    <i class="fa fa-chevron-left carousel-control-left" id="carousel-left-control-<?=$this->dashletModel->id?>"></i>
                </div>
                <div class="span10">
                    <div class="reward-owl-wrapper-<?=$this->dashletModel->id?>">
                        <div class="rewards-owl-carousel owl-carousel owl-theme" id="owl-carousel-<?=$this->dashletModel->id?>">
                            <?php foreach($rewardsData as $reward): ?>
                                <div class="item reward-item">
                                    <div class="title">
                                        <?= $reward['name'] ?>
                                    </div>
                                    <br>
                                    <a href="<?=Docebo::createLmsUrl('//GamificationApp/Reward/buy', array(
                                        'id' => $reward['id']
                                    )) ?>">
                                    <div class="reward-image <?= $this->dashletModel->dashboardCell->width < 6 ? 'thin' : ''?>">
                                                                                    <?php
                                            $assetUrl = CoreAsset::url($reward['picture'], CoreAsset::VARIANT_ORIGINAL);

                                            if($assetUrl): ?>
                                                <?= CHtml::image($assetUrl) ?>
                                            <?php else: ?>
                                                <i class="fa fa-gift fa-5x"></i>
                                            <?php endif; ?>
                                    </div>
                                    </a>
                                    <div class="reward-price">
                                        <strong><?=$reward['coins']?></strong> <?=Yii::t('gamification', 'coins') ?><i class="fa fa fa-database fa-lg yellow"></i>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="span1">
                    <i class="fa fa-chevron-right carousel-control-right" id="carousel-right-control-<?=$this->dashletModel->id?>"></i>
                </div>
            </div>
        <?php else: ?>
        <?php endif; ?>
    </div>
</div>

<?php
$items = 0;
if($this->dashletModel->dashboardCell->width == 8){
    $items = 4;
} else if($this->dashletModel->dashboardCell->width == 6){
    $items = 3;
} else{
    $items = 1;
}
?>

<script type="text/javascript">
<?php if(count($rewardsData) > 1) : ?>
	var carousel = new RewardCarousel({
		debug: true,
		owlElement: 'owl-carousel-<?=$this->dashletModel->id?>',
		moveLeftControl: 'carousel-left-control-<?=$this->dashletModel->id?>',
		moveRightControl: 'carousel-right-control-<?=$this->dashletModel->id?>',
		items: <?= $items ?>
	});
<?php else : ?>
	var carousel = new RewardCarousel({
		debug: true,
		owlElement: 'owl-carousel-<?=$this->dashletModel->id?>',
		moveLeftControl: 'carousel-left-control-<?=$this->dashletModel->id?>',
		moveRightControl: 'carousel-right-control-<?=$this->dashletModel->id?>',
		items: <?= $items ?>,
		loop: false
	});
<?php endif; ?>
</script>