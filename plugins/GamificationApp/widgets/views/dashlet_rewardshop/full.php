<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 16.2.2016 г.
 * Time: 10:03
 *
 * @var $this DashletRewardShop
 * @var $userSets array
 * @var $minCoins integer
 * @var $maxCoins integer
 * @var $rewards array
 * @var $hasNextPage boolean
 * @var $hasPrevPage boolean
 * @var $newRewardDate string
 * @var $showGoToRewardShop boolean
 */

?>

<?php if($this->idLayout !== -1 && $showGoToRewardShop): ?>
    <div class="dl-footer">
        <div class="span6 dl-footer-left 	text-left"></div>
        <div class="span6 dl-footer-right 	text-right"><a href="<?=Docebo::createAppUrl('lms:site/index', array(
                'opt' => 'fullrewardsshop'
            ))?>"><?=Yii::t('gamification', 'Go to Rewards Shop')?></a></div>
        <div class="clearfix"></div>
    </div>
<?php endif; ?>


<div class="w-rewards-shop w-mycourses" id="reward-shop-<?=$this->dashletModel->id?>">
    <?php
    echo CHtml::beginForm('', 'POST', array(
        'name' 		=> 'rewards-filter-form-' . $this->dashletModel->id,
        'class'		=> 'rewards-filter-form',
        'id'		=> 'rewards-filter-form-' . $this->dashletModel->id,
    ));

    // Page number to display on form submit
    echo CHtml::hiddenField('pnumber',$this->pageNumber);
    ?>

    <div class="row-fluid filter-header">
            <a class="filters-button" id="filter-button"><?= Yii::t('dashboard', 'Filters') ?></a>
            <div class="filter-container">
                <h6><?= Yii::t('gamification', 'Rewards') ?></h6>
                <?= CHtml::radioButtonList('filter_new', $this->filterByNew, array(
                    0 => Yii::t('gamification', 'Show all'),
                    1 => Yii::t('gamification', 'Show only new rewards')
                ), array(
                    'labelOptions' => array(
                        'style' => 'display: inline-block'
                    )
                )) ?>
                <h6><?= Yii::t('gamification', 'Filter by coins') ?></h6>
                <div class="slider"></div>
                <?= CHtml::hiddenField('price_filter_min', $this->priceFilterMin, array(
                    'id' => 'price-min-' . $this->dashletModel->id
                )) ?>
                <?= CHtml::hiddenField('price_filter_max', $this->priceFilterMax, array(
                    'id' => 'price-max-' . $this->dashletModel->id
                )) ?>
                <div class="slider-values">
                    <span class="min" data-price="<?= $this->priceFilterMin ?>">
                        <?= $this->priceFilterMin ?>
                    </span>
                    <span class="max" data-price="<?= $this->priceFilterMax ?>">
                        <?= $this->priceFilterMax ?>
                    </span>
                </div>
                <br>
                <?= CHtml::checkBox('filter_user_coins', $this->filterByUserCoins) ?>
                <?= CHtml::label(Yii::t('gamification', 'Show only the rewards I can get with my coins'), 'filter_user_coins', array(
                    'style' => 'display: inline-block; margin-left: 5px;'
                )) ?>

                <br>
                <br>
                <div class="buttons pull-right">
                    <?= CHtml::link(Yii::t('standard', '_CANCEL'), '#', array(
                        'class' => 'btn btn-docebo black big cancel'
                    )) ?>

                    <?= CHtml::link(Yii::t('billing', 'Apply'), '#', array(
                        'class' => 'btn btn-docebo green big apply'
                    )) ?>
                </div>
            </div>

            <a class="search-button" id="search-link"><i class="fa fa-search fa-lg"></i></a>
            <div class="search-field">
                <button type="button" class="close clear-search">&times;</button>
                <?= CHtml::textField('search_input', $this->searchText) ?>
            </div>
    </div>
    <div class="row-fluid">
        <div class="span3">
            <div class="user-summary">
                <h5><?= Yii::t('gamification', 'My personal panel') ?></h5>
                <hr>
                <div class="row-fluid">
                    <div class="span3">
                        <div class="user-image">
                            <?= $userModel->getAvatarImage()?>
                        </div>
                    </div>
                    <div class="span8">
                        <div class="row-fluid username">
                            <?= $userModel->getFullName() ?>
                        </div>
                        <div class="row-fluid usercoins">
                            <i class="fa fa fa-database fa-lg yellow"></i>
                            <span><?= Yii::t('gamification', 'My coins') ?>:&nbsp;<strong><?= $userCoins ?></strong></span>
                        </div>
                    </div>
                </div>
                <?php if($showHistory): ?>
                    <?= CHtml::link(Yii::t('gamification', 'My rewards request history'), Docebo::createAbsoluteUrl('//GamificationApp/GamificationApp/MyBadges&tab=rewards'), array(
                        'class' => 'rewards-history-link'
                    )); ?>
                <?php endif; ?>
                <hr>
            </div>
            <?= CHtml::hiddenField('set_filter', $this->setFilter, array(
                'id' => 'set-filter-' . $this->dashletModel->id
            )) ?>
            <h5><?= Yii::t('gamification', 'Reward Sets') ?></h5>
            <br>
            <div class="rewards-sets-selector">
                <div class="item <?=$this->setFilter === 'all' ? 'active' : ''?>" data-set="all">
                    <a href="#"><?=Yii::t('standard', 'All')?></a>
                    <i class="fa fa-check"></i>
                </div>
                <?php foreach($userSets as $set): ?>
                    <div class="item <?=$this->setFilter === $set['id'] ? 'active' : ''?>" data-set="<?=$set['id']?>">
                        <a href="#"><?= $set['name'] ?></a>
                        <i class="fa fa-check"></i>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="span9">
            <div class="reward-container">
                <hr>
                <?php if(!empty($rewards)): ?>
                    <?php foreach($rewards as $key => $reward): ?>
                        <div class="single-reward">
                            <a href="<?=Docebo::createLmsUrl('//GamificationApp/Reward/buy', array(
                                'id' => $reward['id']
                            )) ?>">
                                <div class="image">
                                    <?php
                                    $assetUrl = CoreAsset::url($reward['picture'], CoreAsset::VARIANT_ORIGINAL);

                                    if($assetUrl): ?>
                                        <img src="<?= $assetUrl?>" />
                                    <?php else: ?>
                                        <i class="fa fa-gift fa-5x"></i>
                                    <?php endif; ?>
                                    <?php if(Yii::app()->localtime->isGte($reward['date_added'], $newRewardDate)): ?>
                                        <div class="new-reward <?=!$assetUrl ? 'not-image' : ''?>"><?= Yii::t('standard', '_NEW') ?></div>
                                    <?php endif; ?>

                                </div>
                            </a>
                            <div class="reward-info">
                                <a href="<?=Docebo::createLmsUrl('//GamificationApp/Reward/buy', array(
                                    'id' => $reward['id']
                                )) ?>"><?=$reward['name']?></a>
                            </div>
                            <div class="reward-price">
                                <span><strong><?=$reward['coins']?></strong><?=Yii::t('gamification', 'coins')?> <i class="fa fa fa-database fa-lg yellow"></i></span>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
                <?php if($hasNextPage): ?>
                    <div class="rewards-pager">
                        <div class="dashlet-pager-next"><?= Yii::t('test', '_TEST_NEXT_PAGE') ?> <i class="fa fa-chevron-right"></i></div>
                    </div>
                <?php endif; ?>
                <?php if($hasPrevPage): ?>
                    <div class="rewards-pager">
                        <div class="dashlet-pager-prev"><i class="fa fa-chevron-left"></i> <?= Yii::t('test', '_TEST_PREV_PAGE') ?></div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        var shop = new RewardsShop({
            debug: true,
            dashletElement: 'reward-shop-<?=$this->dashletModel->id?>',
            dashletId: '<?=$this->dashletModel->id?>',
            minRangePrice: <?=$minCoins ?>,
            maxRangePrice: <?=$maxCoins ?>
        });
    });
</script>
