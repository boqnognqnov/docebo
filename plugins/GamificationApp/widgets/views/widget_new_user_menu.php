<?php
/* @var $this GamificationAppNewUserMenu */
/* @var $badge GamificationAssignedBadges */

// Does the user have badges or not
$hasBadges = count($this->badges)>0;

// Count reward sets assigned to the current user
$userSetsCount = count($this->userSets);

// Whatever to show or hide the reward shop and my points
$showMyPoints = $showRewardShop = ($this->enable_reward_shop && ($userSetsCount > 0));
?>

<div class="gamification-app-user-menu">

	<div class="row-fluid" id="gamification-app-new-user-menu-container">
	<?php if ($hasBadges || ($userSetsCount > 0)) {?>
		<div class="span4">
			<span  id="new-user-menu-my-badges">
				<?= Yii::t('gamification', 'My Badges') ?>
				<strong><?= intval($this->totalBadges) ?></strong>
			</span>
			<span id="fa-star-dark-bg-icon">
				<i class="fa fa-star fa-lg"></i>
			</span>
		</div>

		<div class="span4" id="new-user-menu-my-points-container">
			<span  id="new-user-menu-my-points">
				<?= Yii::t('gamification', 'My Points') ?>
				<strong><?= intval($this->totalScore) ?></strong>
			</span>

			<i class="fa fa-star fa-lg"></i>
		</div>
		<?php if($showMyPoints) { ?>
		<div class="span4" id="new-user-menu-my-coins-container">
			<span  id="new-user-menu-my-coins">
				<?= Yii::t('gamification', 'My coins') ?>
				<strong id="my-coins-counter"><?= intval($this->totalCoins) ?></strong>
			</span>

			<i class="fa fa fa-database fa-lg yellow"></i>
		</div>
		<?php } else {  ?>
		<div class="span4"></div>
		<?php } ?>
	<?php } ?>

		<div class="row-fluid">
			<div id="new-user-menu-view-all-container" class="span4" style="<?= $hasBadges || ($userSetsCount > 0) ? '' : 'margin-top: 35px;'?>">
				<a  id="new-user-menu-view-all" href="<?= Docebo::createAppUrl('lms:GamificationApp/GamificationApp/MyBadges') ?>"><?= Yii::t('gamification', 'View all badges and points') ?></a>
			</div>
	<?php if($showRewardShop) { ?>
			<div id="new-user-menu-view-reward-shop-container" class="span4" style="<?= ($hasBadges || ($userSetsCount > 0)) ? '' : 'margin-top: 35px;'?>">
				<a  id="new-user-menu-view-reward-shop" href="<?= Docebo::createAppUrl('lms:site/index', array(
					'opt' => 'fullrewardsshop'
				)) ?>"><?= Yii::t('gamification', 'Go to Reward Shop') ?></a>
			</div>
	<?php } ?>

		</div>
	</div>

</div>