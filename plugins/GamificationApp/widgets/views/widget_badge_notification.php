<?php
/* @var $this GamificationBadgeNotification */
?>

<button class="hide" id="triggerGamificationNotification" type="button" data-toggle="modal" data-target="#gamificationNotificationModal" data-backdrop="false"></button>

<div id="gamificationNotificationModal" class="gamification-notification modal hide fade">
    <div class="modal-header">
        <a href="#" class="close" data-dismiss="modal">&times;</a>
    </div>
    <div class="modal-body">
        <div class="earned-badge">
            <img src="<?= $this->badge->gamificationBadge->getIconUrl() ?>" alt="" title="">
            <div class="earned-badge-points">
                <span class="gamification-sprite star-small"></span> <?= $this->badge->score ?>
            </div>
        </div>
        <div class="earned-badge-message">
            <h2><?= Yii::t('standard', 'Congratulations') ?></h2>
            <p><?= Yii::t('gamification', 'You\'ve earned the {badge_name} badge', array(
                    '{badge_name}' => CHtml::tag('strong', array(), $this->badge->gamificationBadge->renderBadgeName())
            )) ?></p>
            <a href="<?= Docebo::createAppUrl('lms:GamificationApp/GamificationApp/MyBadges') ?>" class="text-colored"><?= Yii::t('gamification', 'View all badges') ?></a>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {

        function showNotification() {
            $('#triggerGamificationNotification').click();
            setTimeout(hideNotification, 10000);
        }
        function hideNotification() {
            $('#gamificationNotificationModal .modal-header .close').trigger('click');
        }

        setTimeout(showNotification, 2000);
    });
</script>