<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 4.12.2015 г.
 * Time: 18:03
 *
 * @var $this GamificationContestResult
 */
?>

<div class="gamification-contest-description">
    <div class="row-fluid">
        <div class="span5">
            <div class="description">
                <strong><?=Yii::t('standard', '_DESCRIPTION') ?></strong><br>
                <?php
                if($this->contest->translation){
                    echo $this->contest->translation->description;
                } else{
                   echo $this->contest->defaultTranslation->description;
                }
                ?>
            </div>
            <br>
            <?=$this->contest->renderStatus() ?>
        </div>
        <div class="span4">
            <div class="goal">
                <i class="fa fa-bullseye fa-3x"></i>
                <br>
                <?php
                $goals = GamificationContest::getGoals();

                echo $goals[$this->contest->goal];
                ?>
                <p><?=Yii::t('gamification', 'Contest Goal')?></p>
            </div>
        </div>
        <div class="span3">
            <div class="period">
                <i class="fa fa-calendar fa-3x"></i>
                <br>
                <span>
                    <?=Yii::app()->localtime->toLocalDate($this->contest->from_date) ?>
                    <br>
                    <?=Yii::app()->localtime->toLocalDate($this->contest->to_date) ?>
                </span>
                <p><?=Yii::t('gamification', 'Contest period')?></p>
            </div>
        </div>
    </div>
</div>