<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 4.12.2015 г.
 * Time: 18:03
 *
 * @var $this GamificationContestResult
 */
?>
<style>
    .greenClass{
        color: rgb(85, 171, 84);
        font-weight: bold;
    }
</style>
<div class="contest-chart-widget">
    <?php if(!$this->showFullChart): ?>
    <div class="name">
        <?php
            if($this->contest->translation){
                echo $this->contest->translation->name;
            } else{
                echo $this->contest->defaultTranslation->name;
            }
        ?>
    </div>
<?php
$this->render('contest_result/_description');

    else: ?>

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'contests-results-form',
            'htmlOptions' => array(
                'class' => 'ajax-grid-form ajax',
                'data-grid' => '#contests-result-grid'
            )
        ));
        ?>
        <div class="filters-wrapper">
            <div class="filters clearfix">
                <div class="input-wrapper">
                    <?php

                    // Add overall selector params, so controller can do filterng

                    $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                        'id' 				=> 'users-contests-search-input',
                        'name' 				=> 'search_text',
                        'value' 			=> '',
                        'options' => array(
                            'minLength' => 1,
                        ),
                        'source' => Docebo::createAdminUrl('GamificationApp/Contests/autocompleteResults', array('id_contest' => $this->contest->id)),
                        'htmlOptions' => array('placeholder' => Yii::t('standard', '_SEARCH')),
                    ));
                    ?>
                    <span class="search-icon"></span>
                </div>
            </div>
        </div>
        <?= CHtml::submitButton('', array('style' => 'display: none;')); ?>
        <?php $this->endWidget(); ?>
    <?php endif; ?>
    <br>
    <div class="chart">
        <?php

            if($this->showCurrentUser): ?>
                <table class="chart-user <?=$this->showInDialog && $this->showFullChart ? 'dialog' : ''?>">
                    <thead>
                        <th><?=Yii::t('gamification', 'My position') ?></th>
                        <th></th>
                        <th><?= $this->columnNames[$this->contest->goal] ?></th>
                        <?php if($this->contest->isFinished()): ?>
                            <th><?=Yii::t('gamification', 'Points') ?></th>
                            <th><?=Yii::t('gamification', 'Badge')?></th>
                        <?php endif; ?>
                    </thead>
                    <tbody>
                    <?php
                    foreach($headerUsersResults as $userResult){
                        $userModel = null;
                        $greenClass = '';
                        foreach ($surroundedUserModels as $user) {
                            if($user instanceof CoreUser){
                                if ($user->idst == $userResult->id_user) {
                                    $userModel = $user;
                                    break;
                                }
                            } else{
                                if ($user->id_user == $userResult->id_user) {
                                    $userModel = $user;
                                    break;
                                }
                            }
                        }
                        if ($userModel === null) {
                            $userModel = $this->userModel;
                            $greenClass = 'greenClass';
                        }

                    ?>
                        <tr>
                            <td class="position<?= !$this->contest->isFinished() ? ' not-finished' : ''?> <?=$greenClass?>"><?=$userResult->rank ?></td>
                            <td class="user <?=$greenClass?>" style="padding: 0 4px;"><img src="<?=$userModel->getAvatarImage(true)?>" /> <?=$userResult->getFullName() ?></td>
                            <td class="result <?=$greenClass?>"><?=$userResult->ranking_result?></td>
                            <?php if ($this->contest->isFinished()): ?>
                                <?php
                                $rewardData = $userResult->getRewardData();
                                ?>
                                <td class="points <?=$greenClass?>">
                                    <?php if($rewardData): ?>
                                        <span class="gamification-sprite star-small"></span>&nbsp;<?=$rewardData['points']?>
                                    <?php endif; ?>
                                </td>
                                <td class="icon <?=$greenClass?>">
                                    <?php if($rewardData): ?>
                                        <img src="<?=CoreAsset::model()->findByPk($rewardData['icon'])->getUrl()?>" />
                                    <?php endif; ?>
                                </td>
                            <?php endif; ?>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            <?php endif; ?>

        <?php if($this->showFullChart): ?>
            <div id="grid-wrapper">
                <?php
                $_columns = array(
                    array(
                        'name' => 'rank',
                        'headerHtmlOptions' => array('style' => 'text-align:center;'),
                        'htmlOptions' => array(
                            'class' => 'rank' . (!$this->contest->isFinished() ? ' not-finished' : '')
                        )
                    ),
                    array(
                        'header' => Yii::t('standard', '_NAME'),
                        'type' => 'raw',
                        'value' => '$data->renderUser();',
                        'htmlOptions' => array(
                            'class' => 'name'
                        ),
                    ),
                    array(
                        'header' => $this->columnNames[$this->contest->goal],
                        'name' => 'ranking_result',
                        'headerHtmlOptions' => array('style' => 'text-align:center;'),
                        'type' => 'html',
                        'htmlOptions' => array(
                            'class' => 'result'
                        )
                    )
                );

                if($this->contest->isFinished()){
                    $_columns[] = array(
                        'header' => Yii::t('gamification', 'Points'),
                        'headerHtmlOptions' => array('style' => 'text-align:center;'),
                        'name' => 'points',
                        'type' => 'raw',
                        'value' => '$data->renderPoints();',
                        'htmlOptions' => array(
                            'class' => 'points'
                        )
                    );
                    $_columns[] = array(
                        'header' => Yii::t('gamification', 'Badge'),
                        'name' => 'badge',
                        'type' => 'raw',
                        'value' => '$data->renderBadge();',
                        'htmlOptions' => array(
                            'class' => 'badge-icon'
                        ),
                        'headerHtmlOptions' => array('style' => 'text-align: center;'),
                    );
                }

                $gridOptions = array(
                    'id' => 'contests-result-grid',
                    'htmlOptions' => array('class' => 'grid-view clearfix'),
                    'dataProvider' => GamificationContestChart::model()->dataProvider($this->contest->id),
                    'template' => '{items}{summary}{pager}',
                    'summaryText' => Yii::t('standard', '_TOTAL'),
                    'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
                    'pager' => array(
                        'class' => 'ext.local.pagers.DoceboLinkPager',
                    ),
                    'cssFile' => false,
                    "afterAjaxUpdate" => "function(id, data) { }",
                    'ajaxUpdate' => 'all-items',
                    'columns' => $_columns
                );

                $this->widget('zii.widgets.grid.CGridView', $gridOptions);
                ?>
            </div>
        <?php else: ?>
            <?php if(!empty($this->chardData)): ?>
                <table class="chart-users">
                    <thead>
                        <th><?=Yii::t('gamification', 'Rank') ?></th>
                        <th></th>
                        <th><?= $this->columnNames[$this->contest->goal] ?></th>
                        <?php if($this->contest->isFinished()): ?>
                            <th><?=Yii::t('gamification', 'Points') ?></th>
                            <th><?=Yii::t('gamification', 'Badge')?></th>
                        <?php endif; ?>
                    </thead>
                    <tbody>
                    <?php foreach($this->chardData as $key => $data): $index = $key + 1;?>
                        <?php if(($index === $this->resultsNum + 1)) break; ?>
                            <tr>
                                <td class="position"><?=$data->rank ?></td>
                                <td class="user"><img src="<?=$data->getUserAvatar()?>" /> <?=$data->getFullName() ?></td>
                                <td class="result"><?=$data->ranking_result?></td>
                                <?php if($this->contest->isFinished()): ?>
                                    <?php
                                    $rewardData = $data->getRewardData();

                                    ?>
                                    <td>
                                        <?php if($rewardData): ?>
                                            <span class="gamification-sprite star-small"></span>&nbsp;<?=$rewardData['points']?>
                                        <?php endif; ?>
                                    </td>
                                    <td class="icon">
                                        <?php if($rewardData): ?>
                                            <img src="<?=CoreAsset::model()->findByPk($rewardData['icon'])->getUrl()?>" />
                                        <?php endif; ?>
                                    </td>
                                <?php endif; ?>
                            </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            <?php else: ?>
                <strong><?=Yii::t('gamification', 'Contest finished without any activity')?></strong>
            <?php endif; ?>
        <?php endif; ?>
    </div>
    <div class="row pull-right buttons">
        <?php if(!$this->showFullChart && !empty($this->chardData)): ?>
        <a class="btn btn-docebo green big open-dialog" data-dialog-id="contest-full-chart-<?=$this->getId()?>" data-dialog-class="contest-full-chart" data-dialog-title="<?=Yii::t('gamification', 'Contest Full Chart')?>" id="show-full-chart-modal-<?=$this->getId()?>" href="<?=Docebo::createAdminUrl('GamificationApp/Contests/showResults', array('id'=>$this->contest_id,'full'=>true, 'current_user'=>false))?>"><?=Yii::t('gamification', 'View Full Chart')?></a>
        <?php endif; ?>
        <?php if($this->showInDialog): ?>
        <a class="btn btn-docebo black big close-dialog" id="closeDialog" href="javascript:"><?=Yii::t('standard', '_CLOSE')?></a>
        <?php endif; ?>
    </div>

</div>

<?php if($this->showInDialog): ?>
    <style>
        .modal.contest-full-chart-contest-<?=$this->contest->id?>{
            position: absolute;
        }
    </style>
<?php endif; ?>

<script type="text/javascript">

    $(function() {

        $(document).controls();

        <?php if($this->showInDialog): ?>
            $('body').addClass('contest-results');
        <?php endif; ?>
        
        $('#closeDialog').on('click', function(){
            $(this).closest('.modal-body').dialog2('close');
        });

        $('#users-contests-search-input').on('autocompleteselect', function(e, data){
            $.fn.yiiGridView.update('contests-result-grid', {data: {
                search_text: data.item.value
            }});
        });

        $('form#contests-results-form').on('submit', function(e){
            e.preventDefault();
            var searchText = $('input#users-contests-search-input').val();           
            $.fn.yiiGridView.update('contests-result-grid', {data: {
                search_text: searchText
            }});
        });

        $('form#contests-results-form .search-icon').on('click', function(){
            var searchText = $('input#users-contests-search-input').val();
            $.fn.yiiGridView.update('contests-result-grid', {data: {
                search_text: searchText
            }});
        });
    })
</script>
