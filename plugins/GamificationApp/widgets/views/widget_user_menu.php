<?php
/* @var $this GamificationAppUserMenu */

/* @var $badge GamificationAssignedBadges */
?>

<div class="gamification-app-user-menu">
    <?php if($this->badges): ?>
    <h4><strong><?= Yii::t('gamification', 'My last {count} badges', array('{count}' => $this->badges ? count($this->badges) : 0)) ?></strong></h4>
    <?php else: ?>
    <h4><strong><?= Yii::t('gamification', 'My Badges') ?></strong></h4>
    <?php endif; ?>

    <?php if (count($this->badges)>0) {?>
    <div class="badge-list">
    <?php
        foreach ($this->badges as $badge) {
            echo $badge->gamificationBadge->renderBadge();
        }
    ?>
    </div>

    <div class="badge-total">
        <div>
            <?= intval($this->totalBadges) ?> <span class="gamification-sprite star-square-small-black"></span>
        </div>
        <?= Yii::t('gamification', 'My total badges') ?>
    </div>

    <div class="badge-rank">
        <div>
            <?= intval($this->totalScore) ?> <span class="gamification-sprite star-small"></span>
        </div>
        <div>
            <?= intval($this->scoreRank) ?> <span class="gamification-sprite cup-gold"></span>
        </div>
        <?= Yii::t('gamification', 'My points rank') ?>
    </div>
    <?php } ?>
    <div class="badge-link">
        <a href="<?= Docebo::createAppUrl('lms:GamificationApp/GamificationApp/MyBadges') ?>"><?= Yii::t('gamification', 'View all badges') ?></a>
    </div>
<?php if($this->lastContest){?>
    <br>
    <h4 class="title-contest"><strong><?= Yii::t('gamification', 'My last contest'); ?></strong></h4>
    <hr/>
    <div class="contest-status-container">
        <?echo $this->lastContest->renderStatus(); ?>
    </div>
    <div class="contest-info">
        <div class="contest-name">
        <?= $this->lastContest->getName();?>
        </div>
        <div class="contest-description">
        <?= $this->lastContest->getDescription();?>
        </div>
    </div>

    <div class="badge-link">
        <a href="<?= Docebo::createLmsUrl('GamificationApp/GamificationApp/MyBadges', array(
            'tab' => 'contests'
        )); ?>"><?= Yii::t('gamification', 'View all contests') ?></a>
    </div>
<?php } ?>

</div>