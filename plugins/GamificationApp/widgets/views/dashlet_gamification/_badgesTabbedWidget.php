<?php
    foreach ($badgesContent as $key => $item) {
        ${$key} = $item;
    }
    $settings->badgesDisplayMax = intval($settings->badgesDisplayMax);
    $countBadges = intval($countBadges);
    $countNotAwarded = intval(count($badgesNotAwarded));
    $countPoints = intval($countPoints);
    $allBadges_url = Docebo::createLmsUrl('GamificationApp/GamificationApp/MyBadges');
?>
<div id="badgesTabbedContent">
    <table class="full-width">
        <?php if(!$removeBadgesFromTabs): ?>
        <tbody>
            <tr class="bottom-border myBadges">
                <td>
                    <?php if($countBadges > 0):
                                echo '<div class="badge-list clearfix">';
                                    
                                    if (count($badges) > 0) {
                                        $maxBadgesC = 0;
                                        foreach ($badges as $key => $badge) {
                                            if($maxBadgesC < $settings->badgesDisplayMax || empty($settings->badgesDisplayMax)) {
                                                echo CHtml::tag('div', array(
                                                    'data-id' => $badge->gamificationBadge->pkToString()
                                                    ), $badge->gamificationBadge->renderBadge());
                                                $maxBadgesC++;
                                            }
                                        }
                                    } else {
                                        echo Yii::t('gamification', "You don't have any badges yet");
                                    }
                                echo '</div>';
                    endif; ?>
                </td>
            </tr>
            <tr class="bottom-border badgesStats">
                <td>
                    <div class="myBadges">
                        <div class="gamification-fake-badge">
                            <i class="fa fa-star fa-2p5x color-override" style="color:#FFFFFF !important;"></i>
                        </div>
                        <?= CHtml::tag('h3', array('style' => 'color: #0465AC;'), $countBadges) ?>
                        <span><?= Yii::t('gamification', 'My awarded badges') ?></span>
                    </div>
                    <?php if($settings->badgesShowNotAwarded): ?>
                    <div class="notAwarded">
                        <div class="gamification-fake-badge">
                            <i class="fa fa-minus-circle fa-2p5x color-override" style="color:#FFFFFF !important;"></i>
                        </div>
                        <?= CHtml::tag('h3', array('style' => 'color: #0465AC;'), $countNotAwarded) ?>
                        <span><?= Yii::t('gamification', 'Not yet awarded badges') ?></span>
                    </div>
                    <?php endif; ?>
                    <div class="myPoints">
                        <div class="gamification-fake-badge background-override" style="background-color:#FFFFFF !important;">
                            <i class="fa fa-star fa-2p5x color-override" style="color:#F1C40F !important;"></i>
                        </div>
                        <?= CHtml::tag('h3', array('style' => 'color: #0465AC;'), $countPoints) ?>
                        <span><?= Yii::t('gamification', 'My Total Points') ?></span>
                    </div>
                </td>
            </tr>
        </tbody>
        <?php else: ?>
        <tr style="border: none !important;">
            <td>
                <h4 class="text-center" style="color:#aaa !important;margin: 0 0 5px 0;"><?= Yii::t('report', '_NULL_REPORT_RESULT') ?></h4>
            </td>
        </tr>
        <?php endif; ?>
    </table>
</div>
<?php
if($settings->badgesShowViewAll):
?>
<a href="<?= $allBadges_url ?>" class="gamification-viewAll clearfix outter-link" id="badges"><?= Yii::t('gamification','View all badges') ?></a>
<?php
endif;
?>