<?php
    foreach ($contestsContent as $key => $item) {
        ${$key} = $item;
    }
    $currentIndex = 0;
    $allContests_url = Docebo::createLmsUrl('GamificationApp/GamificationApp/MyBadges', array(
        'tab' => 'contests'
    ));
?>
<div id="contestsTabbedContent">
<?php if(!$removeContestsFromTabs): ?>
    <?php if($contests_tab_title != false): ?>
    <h4 style="margin-bottom: 10px;"><?= $contests_tab_title ?></h4>
    <?php endif;?>
    <?php
    if(is_array($last_contests)):
        foreach ($last_contests as $contest):
            if ($currentIndex + 1 > $settings->contestsDisplayMax) break;
            if ($contests_tab_title == false) break;
            ?>
            <table class="last-contest-<?= $currentIndex ?> last-contest-main">
                <tr>
                    <td colspan="4">
                        <h4 class="title" style="color: #0465AC; margin-bottom: 5px;"><?= $contest->contest_title ?></h4>
                        <div class="last-contest-goal">
                            <i class="fa fa-bullseye fa-15x"></i>
                            <span class="bold-text"><?= Yii::t('gamification', 'Goal') ?>:</span>
                            <span><?= $settings->contestsGoals[$contest->contest_goal_key] ?></span>
                        </div>
                        <div class="last-contest-period-status">
                            <i class="fa fa-calendar fa-15x"></i>
                            <span class="bold-text"><?= Yii::t('gamification', 'Period') ?>:</span>
                            <span><?= $contest->contest_period ?></span>
                            <div class="contest-status-outerring"><?= $contest->contest_model->renderStatus() ?></div>
                        </div>
                    </td>
                </tr>
                <?php if(!empty($contest->user_id)): ?>
                <tr>
                    <th <?= empty($contest->user_reward_data) ? 'colspan="4"' : 'colspan="3"' ?>><?= Yii::t('gamification', 'My position') ?></th>
                    <?php if(!empty($contest->user_reward_data)): ?>
                    <th width="25%"><?= Yii::t('gamification', 'Awarded badge') ?></th>
                    <?php endif; ?>
                </tr>
                <tr>
                    <td class="my-pos" style="padding-right:10px;"><h3><?= $contest->user_rank ?></h3></td>
                    <td class="my-avatar" <?= empty($contest->user_reward_data) ? 'colspan="2"' : '' ?>><?= $contest->user_avatar ?></td>
                    <td class="my-name"><span class="user-name bold-text"><?= $contest->user_name ?></span></td>
                    <?php if (!empty($contest->user_reward_data)): ?>
                    <td class="badge-score">
                            <img src="<?= !empty($contest->user_reward_data->reward_icon_url) ? $contest->user_reward_data->reward_icon_url : '#' ?>" style="margin-right: 10px;" />
                            <i class="fa fa-star fa-15x color-override" data-color-override="#F1C40F"></i>
                            <span class="bold-text"><?= $contest->user_reward_data->value->points ?></span>
                    </td>
                    <?php endif; ?>
                </tr>
                <?php endif; ?>
            </table>
            <a class="open-dialog gamification-viewAll margin-bottom-15px" data-dialog-id="contest-full-chart-<?= $contest->contest_id ?>" data-dialog-class="contest-full-chart-<?=$contest->contest_id?>" data-dialog-title="<?=Yii::t('gamification', 'Contest Full Chart')?>" id="show-full-chart-modal-<?=$contest->contest_id?>" href="<?= $contest->contest_url ?>"><?=Yii::t('gamification', 'View full chart')?></a>
        <?php $currentIndex++;
        endforeach;
    endif;
    ?>

    <div class="upcoming-contests" <?= ($contests_tab_title != false) ? 'style="margin-top: 35px;"' : '' ?>>
        <table>
            <tr>
                <th><?= Yii::t('gamification', 'Upcoming contests') ?></th>
                <th><?= Yii::t('gamification', 'Period') ?></th>
            </tr>
            <?php if(!empty($upcoming_contests)): ?>
            <?php foreach ($upcoming_contests as $contest): ?>
            <tr>
                <td class="contest-title"><?= $contest->title ?></td>
                <td class="contest-period"><?= $contest->period ?></td>
            </tr>
            <?php endforeach; ?>
            <?php else: ?>
            <tr>
                <td colspan="2" class="text-center" style="color:#aaa !important"><?= Yii::t('report','_NULL_REPORT_RESULT') ?></td>
            </tr>
            <?php endif; ?>
        </table>
    </div>
<?php else: ?>
            <table class="full-width">
                <tr style="border: none !important;">
                    <td>
                        <h4 class="text-center" style="color:#aaa !important;"><?= Yii::t('report', '_NULL_REPORT_RESULT') ?></h4>
                    </td>
                </tr>
            </table>
<?php endif;?>
</div>
<a href="<?= $allContests_url ?>" class="gamification-viewAll clearfix outter-link" id="contests"><?= Yii::t('gamification', 'View all contests') ?></a>

<script>
        $(document).controls();
        
        $(document).on('click', '#closeDialog', function(){
            $(this).closest('.modal-body').dialog2('close');
        });
</script>