<?php
foreach ($leaderboardContent as $key => $item) {
    ${$key} = $item;
}
$allCharts_url = Docebo::createLmsUrl('GamificationApp/GamificationApp/MyBadges', array(
    'tab' => 'chart'
));
$topFiveTitle = 5;
if(!$removeChartsFromTabs) {
    echo $leaderboards_;
} else {
?>
<table class="full-width">
    <tr style="border: none !important;">
        <td>
            <h4 class="text-center" style="color:#aaa !important;"><?= Yii::t('report','_NULL_REPORT_RESULT') ?></h4>
        </td>
    </tr>
</table>
<?php
}
?>
<a href="<?= $allCharts_url ?>" class="gamification-viewAll clearfix outter-link" id="leaderboard"><?= Yii::t('gamification','View full chart') ?></a>  
