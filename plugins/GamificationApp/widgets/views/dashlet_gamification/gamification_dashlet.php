<?php
$mainContent = array(
    'content' => array(),
    'containerClass' => 'tabbed-bages'
);

foreach($tabs as $content) {
    if(!$content['enabled']) continue;
    
    $randomString = str_shuffle('abcdefghijklmnopqstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
    $randomStart = rand(0, strlen($randomString)-1);
    $randomLength = strlen($randomString) - $randomStart;
    $uniqueId = substr($randomString, $randomStart, $randomLength);
    
    $key = strtolower($content['name']);
    
    $mainContentDashletId = "gamification-dashlet-{$key}-{$uniqueId}";
    
    $mainContent['content'][$mainContentDashletId] = array(
        'header' => Yii::t('gamification', $content['name']),
        'content' => Yii::app()->getController()->renderPartial("plugin.GamificationApp.widgets.views.dashlet_gamification._{$key}TabbedWidget", array(
            "{$key}Content" => ${"{$key}Content"},
            'settings' => $settings,
        ), true),
        'active' => true
    );
}

$show_widget = !empty($mainContent['content']);

if($show_widget) {

    $this->widget(
        'common.widgets.TabbedContent', 
        $mainContent
    );
?>

<script type="text/javascript">
    
    $(function () {
        $(document).off('click', 'ul.tabbed-content-nav li a[href^="#tab-gamification-dashlet-"]');
        $(document).on('click', 'ul.tabbed-content-nav li a[href^="#tab-gamification-dashlet-"]', function() {
            var id = this.href.split('-')[3];
            var parent = $(this).parent().parent().parent().parent().parent().next();
            $('a#'+id, parent).parent().children().hide();
            $('a#'+id, parent).show();
        });        
        setTimeout(function(){
            $('div.get-this-parent').parent().parent().each(function(k, v){
                var parent = v;
                var footer = $('<div class="dl-footer"><div class="span6 dl-footer-left text-left"></div><div class="span6 dl-footer-right text-right"></div></div>');
                var children = $('a.gamification-viewAll.outter-link', parent);
                $(children).hide().each(function(i, j){
                    $(j).removeClass('outter-link');
                    var id = $('ul.tabbed-content-nav li.active a[href^="#tab-gamification-dashlet-"]', parent).attr('href').split('-')[3];
                    if($(j).attr('id') == id) {
                        $(j).removeAttr('style');
                    }
                });
                $('.dl-footer-right', footer).append(children);
                $(parent).next().append(footer);
                $('div.get-this-parent', parent).remove();
            });
        }, 500);
        
        $('[class*=-override]').each(function(){
            var that = this;
            var data = this.dataset;
            if(!$.isEmptyObject(data)) {
                var override = '';
                var originalClass;
                $(this.classList).each(function(k,v){
                    if(v.match('-override')) { 
                        override = originalClass = that.classList[k];
                        return;
                    }
                })
                override = override.split('-');
                $(override).each(function(k,v){
                    if(k > 0) {
                        override[k] = v.charAt(0).toUpperCase() + '' + v.slice(1, v.length);
                    }
                });
                override = override.join('');
                var overrideData = data[override];
                var overrideEl = '';
                override = override.split('Override');
                override.pop();
                overrideEl = override.join('-').toLowerCase();
                
                if(this.hasAttribute('style')) {
                    $(this).attr('style', $(this).attr('style') + ' ' + overrideEl + ': ' + overrideData + ' !important;');
                    this.removeAttribute('data-' + originalClass);
                    this.className = this.a.className.split(originalClass).join('');
                } else {
                    $(this).attr('style', overrideEl + ': ' + overrideData + ' !important;');
                    this.removeAttribute('data-' + originalClass);
                }
            }
        });
        
        $('[id^="tab-gamification-dashlet-leaderboard-"] .leadeboard-selection').each(function(){
            var alreadyDone = $(this.parentNode).hasClass('leaderboard-selection-parent');
            if(!alreadyDone) {
                $(this).wrapAll('<div class="leaderboard-selection-parent">');
            }
        });
    });
</script>

<div class="get-this-parent"></div>

<?php
} else {
    echo '<p class="text-center" style="color: #777777;">' . Yii::t('gamification', 'There is no information to be displayed here!') . '</p>';
}    
?>