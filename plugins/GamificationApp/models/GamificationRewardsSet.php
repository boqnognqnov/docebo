<?php

/**
 * This is the model class for table "gamification_rewards_set".
 *
 * The followings are the available columns in table 'gamification_rewards_set':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $filter
 *
 * The followings are the available model relations:
 * @property GamificationAssociatedSets[] $gamificationAssociatedSets
 */
class GamificationRewardsSet extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gamification_rewards_set';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id', 'numerical', 'integerOnly'=>true),
			array('name', 'required'),
			array('name', 'length', 'max'=>255),
			array('filter', 'length', 'max'=>1000),
			array('description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, description, filter', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'gamificationAssociatedSets' => array(self::HAS_MANY, 'GamificationAssociatedSets', 'id_set'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'description' => 'Description',
			'filter' => 'Filter',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('filter',$this->filter,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GamificationRewardsSet the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function dataProvider(){
		$search = '';
		if (isset($_POST['search_text']))
			$search = $_POST['search_text'];

		// Count records
		$criteria = new CDbCriteria();

		if ($search != '') {
			$criteria->addCondition('t.name like :search OR t.description LIKE :search');
			$criteria->params = array(':search' => '%'.$search.'%');
		}

		$count = GamificationRewardsSet::model()->count($criteria);

		// Select all badges
		$config = array();
		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.'.$attributeName;
		}

		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 't.id asc';
		$config['pagination'] = array (
			'pageSize' => Settings::get('elements_per_page', 10)
		);
		$config['totalItemCount'] = $count;

		return new CActiveDataProvider(get_class($this), $config);
	}

	public function renderFilter(){
		$filterText = '';

		if($this->filter === 'all' || !$this->filter){
			$text = Yii::t('standard', 'All branches & groups');
		} else{
			$filter = CJSON::decode($this->filter);
			$groups = $filter['groups'];
			$branches = $filter['branches'];
			$text = count($branches) . ' ' . Yii::t('standard', 'branches') . ' / ' . count($groups) . ' ' . Yii::t('standard', '_GROUPS');
		}

		$usersSelectorUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
			'type'  		=> UsersSelector::TYPE_REWARDS_SET_FILTER,
			'idForm' 		=> 'leaderboar-edit-selector',
			'idGeneral' 	=> $this->id,
			'noyiixviewjs'	=> true,  // very important: main page already has widget(s) loading yiigridview.js, so, don't
			'name'          => 'usersSelector',
			'tp'            => 'plugin.GamificationApp.views.rewardsSet.users_top_panel',
			'collectorUrl'	=> Docebo::createAdminUrl('GamificationApp/RewardsSet/editSelection')
		));

		$filterText = CHtml::link($text, $usersSelectorUrl, array(
			'class' => 'open-dialog rewards-set-filter',
			'data-dialog-title' => Yii::t('gamification', 'Edit Reward Set Visibility'),
			'data-dialog-class' => 'users-selector modal-rewards-set-filter',
			'data-dialog-id' => 'users-selector-rewards-set'
		));


		return $filterText;
	}

	public function renderExclamation(){
		$command = Yii::app()->db->createCommand();

		/**
		 * @var $command CDbCommand
		 */

		$assignedRewards = $command->select('COUNT(*)')
			->from(GamificationAssociatedSets::model()->tableName())
			->where('id_set = :set', array(
				':set' => $this->id
			))
			->queryScalar();

		if($assignedRewards <= 0){
			return '<span class="exclamation"></span>';
		} else{
			return '';
		}
	}

	public function renderAssignedRewards(){
		$command = Yii::app()->db->createCommand();

		/**
		 * @var $command CDbCommand
		 */
		$assignedRewards = $command->select('COUNT(*)')
				->from(GamificationReward::model()->tableName() . ' t')
				->leftJoin(GamificationAssociatedSets::model()->tableName() . ' gas', 'gas.id_reward = t.id')
				->andWhere('gas.id_set = :set', array(
						':set' => $this->id
				))
				->orWhere('t.association = "'. GamificationReward::ASSOCIATION_ALL .'"')
				->queryScalar();

		$exclamation = '';

		if($assignedRewards <= 0){
			$exclamation = '<i class="fa fa-exclamation-circle fa-lg exclamation"></i>';
		} else{
			$exclamation = '<span class="exclamation none"></span>';
		}

		return "<div class='assign-reward-container'>" . $exclamation .  CHtml::link('<span style="" class="assign-rewards-count assign-rewards-' . $this->id . '">' . $assignedRewards .
				'</span><i class="fa fa-list-ul fa-lg ' . ($assignedRewards <= 0 ? 'yellow' : '') .'"></i>',
				Docebo::createAdminUrl('GamificationApp/RewardsSet/assignRewards', array(
					'id' => $this->id
		))) . "</div>";
	}

	public function renderEdit(){
		return CHtml::link('<span class="i-sprite is-edit">&nbsp;</span>', Docebo::createAdminUrl('GamificationApp/RewardsSet/edit', array(
			'id' => $this->id
		)), array(
			'class' => 'open-dialog edit-set',
			'data-dialog-title' => Yii::t('gamification', 'Edit Reward Set'),
			'data-dialog-class' => 'modal-edit-set',
			'data-dialog-id' => 'modal-edit-set'
		));
	}

	public function renderDelete(){
		return CHtml::link('<span class="i-sprite is-remove red">&nbsp;</span>', Docebo::createAdminUrl('GamificationApp/RewardsSet/delete', array(
			'id' => $this->id
		)), array(
			'class' => 'open-dialog delete-set',
			'data-dialog-title' => Yii::t('gamification', 'Delete Reward Set'),
			'data-dialog-class' => 'modal-delete-set'
		));
	}

	public function hasUsersSelected(){
		$filter = $this->filter;

		if($filter === 'all' || empty($filter)){
			return false;
		}

		return true;
	}

	public function getSelectionFromFilter(){
		if($this->filter === 'all'){
			return $this->filter;
		}

		$filter = CJSON::decode($this->filter);

		$data = array(
			'groups' => $filter['groups'],
			'branches' => $filter['branches']
		);

		return $data;
	}

	public static function getUserSets($idUser){
		$command = Yii::app()->db->createCommand();
		$userSets = array();

		/**
		 * @var $command CDbCommand
		 */
		$command->select('*')
			->from(GamificationRewardsSet::model()->tableName());

		$sets = $command->queryAll();

		foreach($sets as $key => $set){
			$groups = array();
			$branches = array();

			if(empty($set['filter']) || $set['filter'] === 'all'){
				$userSets[] = array(
					'id' => $set['id'],
					'name' => $set['name']
				);
				continue;
			}

			$filter = CJSON::decode($set['filter']);
			$groups = $filter['groups'];

			foreach($filter['branches'] as $branch){
				$branches[] = $branch['key'];
			}

			if(!empty($groups)){
				$command2 = Yii::app()->db->createCommand();
				/**
				 * @var $command2 CDbCommand
				 */
				$command2->select('*')
					->from('user_org_assignment')
					->where("idUser = " . $idUser ." AND (idGroup IN (" . implode(',', $groups) . ') AND isUserGroup = 1)');

				$userInGroup = $command2->queryAll();

				if(!empty($userInGroup)){
					$userSets[] = array(
						'id' => $set['id'],
						'name' => $set['name']
					);
					continue;
				}
			}

			if(!empty($branches)){
				$command3 = Yii::app()->db->createCommand();

				/**
				 * @var $command3 CDbCommand
				 */

				$command3->select('*')
					->from('user_org_assignment')
					->where("idUser = " . $idUser . " AND ( idOrgDirectMemberOf IN (" . implode(',', $branches) . ") OR idOrgDirectMemberOrInChildOf IN (" . implode(',', $branches) . '))');

				$userInBranches = $command3->queryAll();

				if(!empty($userInBranches)){
					$userSets[] = array(
						'id' => $set['id'],
						'name' => $set['name']
					);
				}
			}
		}

		return $userSets;
	}
}
