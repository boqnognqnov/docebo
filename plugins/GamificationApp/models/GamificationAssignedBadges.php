<?php

/**
 * This is the model class for table "gamification_assigned_badges".
 *
 * The followings are the available columns in table 'gamification_assigned_badges':
 * @property integer $id_badge
 * @property integer $id_user
 * @property string $issued_on
 * @property integer $score
 * @property string $event_name
 * @property string $event_key
 * @property string $event_module
 * @property string $event_params
 *
 * The followings are the available model relations:
 * @property CoreUser $coreUser
 * @property GamificationBadge $gamificationBadge
 */
class GamificationAssignedBadges extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gamification_assigned_badges';
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
                'timestampAttributes' => array('issued_on medium'),
                'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_badge, id_user, issued_on', 'required'),
			array('id_badge, id_user, score', 'numerical', 'integerOnly'=>true),
			array('event_name', 'length', 'max'=>100),
			array('event_key', 'length', 'max'=>255),
            array('event_module', 'length', 'max'=>128),
			array('event_params', 'length', 'max'=>1024),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_badge, id_user, issued_on, score, event_name, event_key, event_params', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'coreUser' => array(self::BELONGS_TO, 'CoreUser', 'id_user'),
			'gamificationBadge' => array(self::BELONGS_TO, 'GamificationBadge', 'id_badge'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_badge' => 'Id Badge',
			'id_user' => 'Id User',
			'issued_on' => 'Issued On',
			'score' => Yii::t('standard', '_SCORE'),
			'event_name' => 'Event Name',
			'event_key' => 'Event Key',
            'event_module' => 'Event Module',
			'event_params' => 'Event Params',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_badge',$this->id_badge);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('issued_on',$this->issued_on,true);
		$criteria->compare('score',$this->score);
		$criteria->compare('event_name',$this->event_name,true);
		$criteria->compare('event_key',$this->event_key,true);
        $criteria->compare('event_module',$this->event_module,true);
		$criteria->compare('event_params',$this->event_params,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GamificationAssignedBadges the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function dataProvider()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('t.id_badge', $this->id_badge);

		if (!empty($this->coreUser->search_input)) {
			$criteria->with = array('coreUser' => array(
				'condition' => 'coreUser.userid LIKE :search OR coreUser.firstname like :search OR coreUser.lastname like :search',
				'params' => array(':search' => '%'.$this->coreUser->search_input.'%'),
			));
		} else {
			$criteria->with = array('coreUser');
		}

		$criteria->group = 't.id_user';

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName)
		{
			$sortAttributes[$attributeName] = 't.' . $attributeName;
		}
		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 't.id_user desc';
		$config['pagination'] = array(
			'pageSize' => Settings::get('elements_per_page', 10)
		);

		return new CActiveDataProvider(get_class($this), $config);
	}

	public function sqlDataProviderSelector()
	{
		$params = array();
		$command = Yii::app()->db->createCommand()
			->from(self::model()->tableName().' t');

		if($this->id_badge != null) {
			$command->where('t.id_badge = :id_badge');
			$params[':id_badge'] = $this->id_badge;
		}

		if (!empty($this->coreUser->search_input)) {
			$command->join(CoreUser::model()->tableName().' cu',
				't.id_user = cu.idst AND (cu.userid LIKE :search OR cu.firstname like :search OR cu.lastname like :search)'
			);
			$params[':search'] = '%'.$this->coreUser->search_input.'%';
		} else {
			$command->join(CoreUser::model()->tableName().' cu', 't.id_user = cu.idst');
		}

		$command->group('t.id_user');

		// COUNTER first
		$commandCounter = clone $command;
		$commandCounter->select('COUNT(t.id_user)');
		$commandCounter->params = $params;
		$numRecords = $commandCounter->queryScalar();


		// Data provider config
		$config['sort']['defaultOrder'] = 't.id_user desc';
		$config['pagination'] 			= array('pageSize' => Settings::get('elements_per_page', 10));
		$config['keyField'] 			= 'id_user';
		$config['params'] 				= $params;
		$config['totalItemCount'] 		= $numRecords;

		// Create data provider and return to caller
		$dataProvider = new CSqlDataProvider($command->getText(), $config);
		return $dataProvider;
	}


	public function dataProviderMyActivities($idUser = false)
	{
		if (empty($idUser)) { $idUser = Yii::app()->user->id; }

		$criteria = new CDbCriteria;

		$criteria->addCondition("t.id_user = :id_user");
		$criteria->params[':id_user'] = $idUser;

		$criteria->with['gamificationBadge'] = array();

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName)
		{
			$sortAttributes[$attributeName] = 't.' . $attributeName;
		}
		//...
		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 't.issued_on DESC';
		$config['pagination'] = array(
			'pageSize' => Settings::get('elements_per_page', 10)
		);

		return new CActiveDataProvider(get_class($this), $config);
	}


	public function inSessionList($index, $value)
	{
		$list = Yii::app()->session['selectedItems'];
		return in_array($value, (!$list ? array() : $list));
	}

	public function dataProviderSingle()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('t.id_badge', $this->id_badge);
		$criteria->compare('t.id_user', $this->id_user);

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName)
		{
			$sortAttributes[$attributeName] = 't.' . $attributeName;
		}
		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 't.issued_on desc';
		$config['pagination'] = array(
			'pageSize' => Settings::get('elements_per_page', 3)
		);

		return new CActiveDataProvider(get_class($this), $config);
	}

	public function renderBadgeDate()
	{
		return $this->issued_on;
	}

	public function renderBadgeEvent()
	{
        $params = CJSON::decode($this->event_params);
        return Yii::t($this->event_module ? $this->event_module : 'gamification', $this->event_key, $params[0]);
	}

    /**
     * Returns a string that can be used as an id of the current object for html elements
     * @return string
     */
    public function pkToString()
    {
        $id = implode('_', array(
            $this->id_badge,
            $this->id_user,
            str_replace(array('-',':',' ','/', '.', ','), '_', $this->issued_on)
        ));

        return $id;
    }

    /**
     * @param $idUser
     * @param null $limit
     * @return GamificationAssignedBadges[]
     */
    public function getUserBadges($idUser, $limit = null)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('id_user = :id_user');
        $criteria->params[':id_user'] = $idUser;
        $criteria->with = array('gamificationBadge');
        $criteria->order = 'issued_on DESC';
        $badges = $this->findAll($criteria);

        // let's process the records and update the badgeCounter field
        $data = array();
        $counter = 0;
        foreach ($badges as $badge) {
            if (isset($data[$badge->id_badge])) {
                $target =& $data[$badge->id_badge];

                $badgeCounter = intval($target->gamificationBadge->badgeCounter);
                if (!$badgeCounter) $badgeCounter = 1;
                $target->gamificationBadge->badgeCounter = $badgeCounter + 1;

                $badgeCounterScore = intval($target->gamificationBadge->badgeCounterScore);
                // initialize with the score of t
                if (!$badgeCounterScore) $badgeCounterScore = $target->score;
                $target->gamificationBadge->badgeCounterScore = $badgeCounterScore + intval($badge->score);
            } else {
                $data[$badge->id_badge] = $badge;
                $badge->gamificationBadge->badgeCounterScore = intval($badge->score);
                $counter++;
                if($limit && ($counter == $limit))
                    break;
            }
        }

        return array_values($data);
    }

    /**
     * Returns an array containing a CoreUser in the 'user' field and the score in the 'points' field
     * The results are ordered by score in descending order
     * @param null|int $limit
     * @return array
     */
    public static function getPointsChart($limit = null, $leaderboardUsers = array()) {

		/**
		 * @var $command CDbCommand
		 */
		$chart = array();
		$command = Yii::app()->db->createCommand()
			->select('t.id_user, SUM(t.score) AS total_score, MAX(t.issued_on) as last_assign')
			->from(self::model()->tableName() . ' t');
		if(!empty($leaderboardUsers)){
			$command->where('t.id_user IN (' . implode(',', $leaderboardUsers) . ')');
		}
		$command->group('t.id_user')
			->order('total_score DESC, last_assign DESC')
			->limit($limit);
		$rows = $command->queryAll();

        if (count($rows) > 0) {
            $userScore = array();
            foreach ($rows as $row) {
                $userScore[$row['id_user']] = array('score' => $row['total_score']);
            }

            // retrieve user info for all rows
            $criteria = new CDbCriteria();
            $criteria->addInCondition('idst', array_keys($userScore));
            $users = CoreUser::model()->findAll($criteria);
            foreach($users as $user)
                $userScore[$user->idst]['user'] = $user;


            $maxScore = intval( $rows[0]['total_score'] );
            foreach ($userScore as $id_user => $record) {
				$progress = ($maxScore > 0) ? intval($record['score']) * doubleval(100 / $maxScore) : 0;
                $chart[] = array(
                    'user' => $record['user'],
                    'points' => $record['score'],
                    'progress' => $progress
                );
            }
        }

        return $chart;

    }

    /**
     * Returns the specified user's points chart position information
     * @param $idUser
     * @return array
     */
    public static function getPointsChartPosition($idUser, $leaderboardUsers = array()) {

		$chartPosition = array();

		/**
		 * @var $command CDbCommand
		 */
		$command = Yii::app()->db->createCommand()
			->select('t.id_user, SUM(t.score) AS total_score, MAX(t.issued_on) as last_assign')
			->from(self::model()->tableName() . ' t');
		if(!empty($leaderboardUsers)){
			$command->where('t.id_user IN (' . implode(',', $leaderboardUsers) . ')');
		}
		$command->group('t.id_user')
			->order('total_score DESC, last_assign DESC');
		$rows = $command->queryAll();

        if (count($rows) > 0) {

            // save the score for the chart top position
            $maxScore = intval( $rows[0]['total_score'] );

            foreach ($rows as $k => $row) {
                if ($row['id_user'] == $idUser) {
                    // found the user position in the chart
                    $chartPosition['position'] = $k + 1;
                    $chartPosition['user'] = CoreUser::model()->findByPk($idUser);
                    $chartPosition['points'] = $row['total_score'];
                    if ($maxScore > 0) {
                    	$chartPosition['progress'] = intval($row['total_score']) * doubleval(100 / $maxScore);;
                    }
                    else {
                    	$chartPosition['progress'] = 0;
                    }
                    break;
                }
            }
        }

        return $chartPosition;
    }

    /**
     * Returns the latest assigned badge to be displayed in the gamification notification modal
     * @return GamificationAssignedBadges|null
     */
    public static function getLatestAssignedBadgeSinceLogin() {
        $idUser = Yii::app()->user->id;
        $previousEnter = Yii::app()->user->getLastEnter();

        $criteria = new CDbCriteria();

        $criteria->addCondition('id_user = :id_user');
        $criteria->params[':id_user'] = $idUser;

        $criteria->addCondition('issued_on >= :last_enter');
        $criteria->params[':last_enter'] = Yii::app()->localtime->fromLocalDateTime($previousEnter);

        $criteria->order = 'issued_on DESC';
        $criteria->limit = 1;

        $badge = GamificationAssignedBadges::model()->with('gamificationBadge')->find($criteria);

        return $badge;
    }


	public static function getLatestAssignedBadgeParams($idUser,$idBadge){

		$command = Yii::app()->db->createCommand()
			->select('event_params')
			->from(self::model()->tableName());
		$command->where('id_badge = :id_badge AND id_user = :id_user', array(':id_badge' => $idBadge,':id_user'=>$idUser));
		$command->order('issued_on DESC')->limit(1);

		$result = $command->queryAll();
		$params = json_decode($result[0]['event_params'],True);
		return $params[0];
	}

}
