<?php

/**
 * This is the model class for table "gamification_user_wallet".
 *
 * The followings are the available columns in table 'gamification_user_wallet':
 * @property integer $id
 * @property integer $id_user
 * @property integer $coins
 *
 * The followings are the available model relations:
 * @property CoreUser $userModel
 */
class GamificationUserWallet extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gamification_user_wallet';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user', 'required'),
			array('id, id_user, coins', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user, coins', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userModel' => array(self::BELONGS_TO, 'CoreUser', 'id_user'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'coins' => 'Coins',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('coins',$this->coins);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GamificationUserWallet the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getUserCoins($userId){
		$command = Yii::app()->db->createCommand();

		/**
		 * @var $command CDbCommand
		 */


		if($userId){
			$command->select('coins')
				->from(self::model()->tableName())
				->where('id_user = :user', array(
					':user' => $userId
				));

			$data = $command->queryScalar();

			return $data;
		}

		return false;
	}

	/**
	 * Subtract user coins from his wallet
	 *
	 * @param $userId integer ID of LMS user
	 * @param $coins integer Number of coins to be subtracted
	 * @return bool Result of saving the model
	 */
	public static function removeCoins($userId, $coins){
		if($userId){
			$userWallet = self::model()->findByAttributes(array(
				'id_user' => $userId
			));

			/**
			 * @var $userWallet self
			 */

			if($userWallet){
				$currentUserCoins = $userWallet->coins;

				$userWallet->coins = $currentUserCoins - $coins;

				if($userWallet->coins < 0){
					$userWallet->coins = 0;
				}

				return $userWallet->save();
			}
		}

		return false;
	}

	/**
	 * Add coins to the user wallet. If no such a record for the users, will be created a new one.
	 *
	 * @param $userId integer ID of LMS user
	 * @param $coins integer Number of coins to be added
	 * @return bool Result of saving the model
	 */
	public static function addCoins($userId, $coins){
		if($userId){
			$userWallet = self::model()->findByAttributes(array(
				'id_user' => $userId
			));

			/**
			 * @var $userWallet self
			 */

			if($userWallet){
				$currentUserCoins = $userWallet->coins;

				$userWallet->coins = $currentUserCoins + $coins;

				return $userWallet->save();
			} else{
				$userWallet = new GamificationUserWallet();
				$userWallet->id_user = $userId;
				$userWallet->coins = $coins;

				return $userWallet->save();
			}
		}

		return false;
	}
}
