<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 3.12.2015 г.
 * Time: 13:25
 */
class ChartData extends CModel
{

    public $rank;
    public $id_user;
    public $full_name;
    public $ranking_result;
    public $reward_data;

    public function rules(){
        return array(
            array('rank, id_user, full_name, reward_data, ranking_result', 'required')
        );
    }

    public function encode(){
        $attributes = $this->getAttributes();
        return CJSON::encode($attributes);
    }

    public function attributeNames()
    {
        return array(
            'rank',
            'id_user',
            'full_name',
            'ranking_result',
            'reward_data'
        );
        // TODO: Implement attributeNames() method.
    }

    protected function getUserModel(){
        $userModel = CoreUser::model()->findByPk($this->id_user);

        return $userModel;
    }

    public function getUserAvatar(){
        $userModel = $this->getUserModel();

        /**
         * @var $userModel CoreUser
         */

        if($userModel){
            return $userModel->getAvatarImage(true);
        } else{
            return Yii::app()->theme->baseUrl .  '/images/standard/user.png';
        }
    }

    public function getFullName(){
        $userModel = $this->getUserModel();

        if($userModel){
            return $userModel->getFullName();
        } else{
            return $this->full_name;
        }
    }

    public function getRewardData(){

        if(!$this->reward_data){
            return null;
        }

        $rewardData = CJSON::decode($this->reward_data);

        if($rewardData['type'] == GamificationContestReward::REWARD_TYPE_BADGE){
            return $this->getBadgeData();
        }
    }

    protected function getBadgeData(){
        $rewardData = CJSON::decode($this->reward_data);

        $data = array();
        if($rewardData['type'] == GamificationContestReward::REWARD_TYPE_BADGE){
            $badgeModel = GamificationBadge::model()->findByPk($rewardData['id_item']);

            if($badgeModel){
                $data['points'] = $badgeModel->score;
                $data['icon'] = $badgeModel->icon;
            } else{
                $rewarValue = CJSON::decode($rewardData['value']);
                $data['points'] = $rewarValue['points'];
                $data['icon'] = $rewarValue['badge_icon'];
            }
        }

        return $data;
    }
}