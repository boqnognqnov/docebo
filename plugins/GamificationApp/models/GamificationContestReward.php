<?php

/**
 * This is the model class for table "gamification_contest_reward".
 *
 * The followings are the available columns in table 'gamification_contest_reward':
 * @property integer $id
 * @property integer $id_contest
 * @property integer $rank
 * @property string $type
 * @property integer $id_item
 *
 * The followings are the available model relations:
 * @property GamificationContest $idContest
 */
class GamificationContestReward extends CActiveRecord
{

	const REWARD_TYPE_BADGE = 'badge';
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gamification_contest_reward';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_contest', 'required'),
			array('id_contest, rank, id_item', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_contest, rank, type, id_item', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idContest' => array(self::BELONGS_TO, 'GamificationContest', 'id_contest'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_contest' => 'Id Contest',
			'rank' => 'Rank',
			'type' => 'Type',
			'id_item' => 'Id Item',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_contest',$this->id_contest);
		$criteria->compare('rank',$this->rank);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('id_item',$this->id_item);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GamificationContestReward the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getBadgeUrl(){
		if($this->type === GamificationContestReward::REWARD_TYPE_BADGE){
			$badge = GamificationBadge::model()->findByPk($this->id_item);

			if($badge){
				return CoreAsset::model()->findByPk($badge->icon)->getUrl();
			}
		}
	}

	public function getBadgeName(){
		if($this->type === GamificationContestReward::REWARD_TYPE_BADGE){
			$badge = GamificationBadge::model()->findByPk($this->id_item);

			if($badge){
				if($badge->badgeTranslation){
					return $badge->badgeTranslation->name;
				} else{
					return $badge->defaultTranslation->name;
				}
			}
		}

	}
}
