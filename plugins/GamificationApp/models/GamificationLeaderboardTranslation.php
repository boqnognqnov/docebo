<?php

/**
 * This is the model class for table "gamification_leaderboard_translation".
 *
 * The followings are the available columns in table 'gamification_leaderboard_translation':
 * @property integer $id
 * @property integer $id_board
 * @property string $lang_code
 * @property string $name
 *
 * The followings are the available model relations:
 * @property GamificationLeaderboard $idBoard
 */
class GamificationLeaderboardTranslation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gamification_leaderboard_translation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array('id_board, lang_code, name', 'required'),
				array('id_board', 'numerical', 'integerOnly'=>true),
				array('lang_code', 'length', 'max'=>45),
				array('name', 'length', 'max'=>256),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
				array('id, id_board, lang_code, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'leaderboard' => array(self::BELONGS_TO, 'GamificationLeaderboard', 'id_board'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
				'id' => 'ID',
				'id_board' => 'Id Board',
				'lang_code' => 'Lang Code',
				'name' => 'Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_board',$this->id_board);
		$criteria->compare('lang_code',$this->lang_code,true);
		$criteria->compare('name',$this->name,true);

		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GamificationLeaderboardTranslation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getModelsList($id_board = 0)
	{
		$languages = CoreLangLanguage::getActiveLanguages();
		$models = array();

		foreach($languages as $code => $lang)
		{
			if($id_board === 0)
			{
				$models[$code] = GamificationBadgeTranslation::model();
				$models[$code]->name = '';
				$models[$code]->lang_code = $code;
			}
			else
			{
				$model = GamificationLeaderboardTranslation::findByAttributes(array('id_board' => $id_board, 'lang_code' => $code));
				if($model)
					$models[$code] = $model;
				else
				{
					$models[$code] = GamificationLeaderboardTranslation::model();
					$models[$code]->name = '';
					$models[$code]->lang_code = $code;
				}
			}
		}

		return $models;
	}
}