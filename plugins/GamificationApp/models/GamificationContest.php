<?php

/**
 * This is the model class for table "gamification_contest".
 *
 * The followings are the available columns in table 'gamification_contest':
 * @property integer $id
 * @property string $from_date
 * @property string $to_date
 * @property string $goal
 * @property string $goal_settings
 * @property string $filter
 * @property string $timezone
 *
 * The followings are the available model relations:
 * @property GamificationContestReward[] $rewards
 * @property GamificationContestChart[] $chart
 * @property GamificationContestTranslation[] $gamificationContestTranslations
 * @property GamificationContestTranslation $defaultTranslation
 * @property GamificationContestTranslation $translation
 * @property GamificationContestTranslation $englishTranslation
 * @property GamificationContestChart $winner
 */
class GamificationContest extends CActiveRecord
{

	const GOAL_HANDLER_COMPLETE_HIGHEST_COURSES = 'CompletedHighestCourses';
	const GOAL_HANDLER_GOT_HIGHEST_GAMIFICATION_POINTS = 'GotHighestGamificationPoints';
	const GOAL_HANDLER_RECEIVED_HIGHEST_HELPFUL = 'ReceivedHighestHelpful';
	const GOAL_HANDLER_SCORED_HIGHESTIN_COURSE = 'ScoredHighestInCourse';

	const CONTEST_CHART_RECURRING_LENGTH = 1;


    const STATUS_SCHEDULED = 1;
    const STATUS_INPROGRESS = 2;
    const STATUS_FINISHED = 3;

	/**
	 * @var GamificationContest
	 */
	private $oldAttributes = null;

    public $reward;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gamification_contest';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('goal, timezone', 'length', 'max'=>255),
            array('from_date, to_date', 'required'),    
			array('from_date, to_date, goal_settings, filter', 'safe'),
            array('from_date', 'validateDates', 'on' => 'insert'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, from_date, to_date, goal, goal_settings, filter, timezone', 'safe', 'on'=>'search'),
		);
	}
    
    public function validateDates() {
        // return error if date-start if before today and if date-end is before/equal date-start
        if(empty($this->from_date) || empty($this->to_date)) {
            $msg = Yii::t('gamification', 'Please select a period for the contest!');
            $this->addError('from_date', $msg);
        } else {
			$fromDate = null;
			$toDate = null;
            $today = Yii::app()->localtime->getUTCNow('Y-m-d') . ' 00:00:00';
			$fromDate = Yii::app()->localtime->fromLocalDate($this->from_date, LocalTime::SHORT) . ' 00:00:00';
			$toDate = Yii::app()->localtime->fromLocalDate($this->to_date, LocalTime::SHORT) . ' 23:59:59';


            $isTodayBeforeDateStart = Yii::app()->localtime->isGt($today, $fromDate);
            $isTodayAfterDateEnd = Yii::app()->localtime->isGt($today, $toDate);
            $isDateEndBeforeDateStart = Yii::app()->localtime->isGt($fromDate, $toDate);


            if($isDateEndBeforeDateStart || $isTodayAfterDateEnd || $isTodayBeforeDateStart) {
                $msg = Yii::t('gamification', 'You have selected incorrect period!');
                $this->addError('from_date', $msg);
            }
        }
    }

	public function afterFind(){
		$this->oldAttributes = clone $this;
	}

	public function beforeSave(){
		if($this->getScenario() !== 'filterChange'){
			$this->from_date = Yii::app()->localtime->fromLocalDate($this->from_date, LocalTime::SHORT) . ' 00:00:00';
			$this->to_date = Yii::app()->localtime->fromLocalDate($this->to_date, LocalTime::SHORT) . ' 23:59:59';
		}

		if(!$this->isNewRecord){

			if($this->oldAttributes->goal !== $this->goal){
				GamificationContestChart::model()->deleteAllByAttributes(array(
					'id_contest' => $this->id
				));
			}
		}
		return parent::beforeSave();
	}
    
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rewards' => array(self::HAS_MANY, 'GamificationContestReward', 'id_contest'),
			'gamificationContestTranslations' => array(self::HAS_MANY, 'GamificationContestTranslation', 'id_contest'),
			'defaultTranslation' => array(self::HAS_ONE, 'GamificationContestTranslation', 'id_contest', 'condition' => 'lang_code = "' . CoreLangLanguage::getDefaultLanguage() . '"'),
			'translation' => array(self::HAS_ONE, 'GamificationContestTranslation', 'id_contest', 'condition'=>'lang_code="'.Lang::getCodeByBrowserCode(Yii::app()->getLanguage()).'"'),
			'englishTranslation' => array(self::HAS_ONE, 'GamificationContestTranslation', 'id_contest', 'condition' => 'lang_code = "english"'),
			'winner' => array(self::HAS_ONE, 'GamificationContestChart', 'id_contest', 'condition' => 'rank = 1'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'from_date' => 'From Date',
			'to_date' => 'To Date',
			'goal' => 'Goal',
			'goal_settings' => 'Goal Settings',
			'filter' => 'Filter',
			'timezone' => 'Timezone',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('from_date',$this->from_date,true);
		$criteria->compare('to_date',$this->to_date,true);
		$criteria->compare('goal',$this->goal,true);
		$criteria->compare('goal_settings',$this->goal_settings,true);
		$criteria->compare('filter',$this->filter,true);
		$criteria->compare('timezone',$this->timezone,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GamificationContest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function afterSave(){
		if($this->isNewRecord){
			$job = Yii::app()->scheduler->createJob('Contest', GoalJobHandler::HANDLER_ID, CoreJob::TYPE_RECURRING,
					CoreJob::PERIOD_HOUR,
					self::CONTEST_CHART_RECURRING_LENGTH,
					0,  // minute is always 0
					0,
					false,
					false,
					false,
					Yii::app()->localtime->toLocalDate($this->from_date),
					array(
						'contest_id' => $this->id,
						'firstRun' => true
					),
					false,
					false,
					false);
		}
	}

	public static function getGoals(){
		$result = array(
			self::GOAL_HANDLER_COMPLETE_HIGHEST_COURSES => Yii::t('gamification', 'Complete the highest number of courses'),
			self::GOAL_HANDLER_GOT_HIGHEST_GAMIFICATION_POINTS => Yii::t('gamification', 'Got the highest Gamification points'),
			self::GOAL_HANDLER_RECEIVED_HIGHEST_HELPFUL => Yii::t('gamification', 'Received the highest number of "Helpful" (Thumbs up) in forums'),
			self::GOAL_HANDLER_SCORED_HIGHESTIN_COURSE => Yii::t('gamification', 'Scored highest in course')
		);

		return $result;
	}

	public static function scoreColumnName(){
		$result = array(
			self::GOAL_HANDLER_COMPLETE_HIGHEST_COURSES => Yii::t('gamification', 'Courses completed'),
			self::GOAL_HANDLER_SCORED_HIGHESTIN_COURSE => Yii::t('gamification', 'Courses scores'),
			self::GOAL_HANDLER_GOT_HIGHEST_GAMIFICATION_POINTS => Yii::t('gamification', 'Gamification points'),
			self::GOAL_HANDLER_RECEIVED_HIGHEST_HELPFUL => Yii::t('gamification', 'Helpful points')
		);

		return $result;
	}

	public function getGoalSettings(){

		if($this->goal_settings === 'all'){
			return $this->goal_settings;
		}

		$data = CJSON::decode($this->goal_settings);

		return $data;
	}

	public function renderStatus(){

		$now = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
		$fromDate = self::convertFromContestTimezoneToUTC($this->from_date, $this->timezone);
		$toDate = self::convertFromContestTimezoneToUTC($this->to_date, $this->timezone);
		if(Yii::app()->localtime->isGt($fromDate,$now)) {
			$text = Yii::t('automation', 'Scheduled');
			$class = 'scheduled';
		} else if(Yii::app()->localtime->isGte($now, $fromDate) && Yii::app()->localtime->isGte($toDate, $now)){
			$text = Yii::t('standard', '_USER_STATUS_BEGIN');
			$class = 'in-progress';
		} else if(Yii::app()->localtime->isGt($now, $toDate)){
			$text = Yii::t('gamification', 'Finished');
			$class = 'finished';
		}

		return "<span class='contest-status {$class}'>{$text}</span>";
	}


    public function dataProvider($showOnlyFinished = false, $showOnlyUserContest = false, $myBadgesView = false){
        $search = '';
        if(isset($_REQUEST['show_open_closed_contests']) && !empty($_REQUEST['show_open_closed_contests'])){
            $showMode = $_REQUEST['show_open_closed_contests'];
        }
        if (isset($_REQUEST['search_text']))
            $search = $_REQUEST['search_text'];
		elseif(isset($_REQUEST['term'])){
			$search = $_REQUEST['term'];
		}

        // Count records
        $criteria = new CDbCriteria();
        $criteria->together = true;
        $criteria->with = array('gamificationContestTranslations' => array (
            'alias' => 'contestTranslations',
            'on' => 'lang_code="'.Lang::getCodeByBrowserCode(Yii::app()->getLanguage()).'"'
        ));

        if ($search != '') {
            $criteria->addCondition('contestTranslations.name like :search');
            $criteria->params[':search'] = '%'.$search.'%';
        }

		if($showOnlyFinished){
			$showMode = 'closed';
		}

        if($showOnlyUserContest && is_array($showOnlyUserContest)) {

            $criteria->addCondition('t.id IN(' . implode(',',$showOnlyUserContest) . ')');
        }elseif(is_array($showOnlyUserContest)  && $myBadgesView){
	        //This is so that the dataprovider does not return all the  records when a user does not have contests
	        $criteria->addCondition('t.id IN(0)');
        }

		$defaultTimezone = Settings::get('timezone_default');

        if($showMode){
            switch ($showMode){
                case 'open':
                    $criteria->addCondition('CONVERT_TZ(t.to_date, IFNULL(t.timezone, "' . $defaultTimezone .'"), "+00:00") >= NOW()');
                    break;
                case 'closed':
                    $criteria->addCondition('CONVERT_TZ(t.to_date, IFNULL(t.timezone, "' . $defaultTimezone .'"), "+00:00") < NOW()');
                    break;
            }
        }

        // Select all badges
        $config = array();
		if($showOnlyUserContest && is_array($showOnlyUserContest)){
			$sortAttributes = array();
			foreach ($this->attributeNames() as $attributeName) {
				$sortAttributes[$attributeName] = 't.'.$attributeName;
			}
		}

        $config['criteria'] = $criteria;
		$config['countCriteria'] = $criteria;
		if($showOnlyUserContest && is_array($showOnlyUserContest)){
			$config['sort']['attributes'] = $sortAttributes;
			$config['sort']['defaultOrder'] = 't.id asc';
		}
        $config['pagination'] = array (
            'pageSize' => Settings::get('elements_per_page', 10)
        );

        return new CActiveDataProvider(get_class($this), $config);
    }


    public function renderFilter(){
        $filterText = '';

        if ($this->filter === 'all') {
            $text = Yii::t('standard', 'All branches and groups');
        } else {
            $filter = CJSON::decode($this->filter);
            $groups = $filter['groups'];
            $branches = $filter['branches'];
            $text = count($branches) . ' ' . Yii::t('standard', 'branches') . ' / ' . count($groups) . ' ' . Yii::t('standard', '_GROUPS');
        }

        $usersSelectorUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
            'type' => UsersSelector::TYPE_GAMIFICATION_CONTEST_NO_WIZARD,
            'idForm' => 'contest-edit-selector',
            'idGeneral' => $this->id,
            'noyiixviewjs' => true,  // very important: main page already has widget(s) loading yiigridview.js, so, don't
            'name' => 'usersSelector',
            'tp' => 'plugin.GamificationApp.views.contests.users_top_panel',
            'collectorUrl' => Docebo::createAdminUrl('GamificationApp/Contests/editSelection')
        ));

        $filterText = CHtml::link($text, $usersSelectorUrl, array(
            'class' => 'open-dialog contest-filter',
            'data-dialog-title' => Yii::t('gamification', 'Edit Contest'),
            'data-dialog-class' => 'users-selector',
            'data-dialog-id' => 'users-selector-contest'
        ));

        return $filterText;
    }

    public function renderPeriod(){
		$currentTimezoneText = '';

		$useUserTimezone = Settings::get('timezone_allow_user_override', 'off');

		$timezone = '';
		$defaultTimezone = Settings::get('timezone_default', 'UTC');

		if($useUserTimezone == 'on'){
			$userTimezone = CoreSettingUser::model()->findByAttributes(array(
				'path_name' => 'timezone',
				'id_user' => Yii::app()->user->id
			));

			if($userTimezone){
				$timezone = $userTimezone->value;
			}
		} else{
			$timezone = $defaultTimezone;
		}


		if(empty($this->timezone)){
			$this->timezone = $defaultTimezone;
		}
		if($this->timezone !== $timezone){
			$timezonesArray = Yii::app()->localtime->getTimezonesArray();
			preg_match('#\(.*?\)#', $timezonesArray[$this->timezone], $currentTimezoneText);
		}
        return Yii::app()->localtime->toLocalDate($this->from_date) . ' / ' . Yii::app()->localtime->toLocalDate($this->to_date) . ' ' . $currentTimezoneText[0];
    }

	public function renderView(){
		$command = Yii::app()->db->createCommand();
		/**
		 * @var $command CDbCommand
		 */
		$command->where('id_contest = :contest', array(
			':contest' => $this->id
		))
		->from(GamificationContestChart::model()->tableName());

		$rows = $command->queryAll();

		$now = Yii::app()->localtime->getLocalNow('Y-m-d H:i:s');
		$toDate = self::convertFromContestTimezoneToUTC($this->from_date, $this->timezone);

		if(!empty($rows) || Yii::app()->localtime->isGt($now, $toDate)){
			return CHtml::link('<i class="fa fa-search" style="font-size: 18px;"></i>', Docebo::createAdminUrl('GamificationApp/Contests/showResults', array(
				'id' => $this->id
			)), array(
				'class' => 'open-dialog contests-filter',
				'data-dialog-title' => Yii::t('gamification', 'Contest Results'),
				'data-dialog-class' => 'contest-full-chart',
				'data-dialog-id' => 'contest-full-chart-contest-' . $this->id
			));
		} else{
			return '';
		}
	}

    public function renderEdit(){
        $now = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
		$fromDate = self::convertFromContestTimezoneToUTC($this->from_date, $this->timezone);
		$toDate = self::convertFromContestTimezoneToUTC($this->to_date, $this->timezone);

        if(Yii::app()->localtime->isGt($now, $toDate) && !Yii::app()->localtime->isEqual($now, $fromDate)){
            return '';
        }
        else {
            return CHtml::link('<span class="i-sprite is-edit">&nbsp;</span>', Docebo::createAdminUrl('GamificationApp/Contests/edit', array(
                'id' => $this->id
            )));
        }
    }

    public function renderDelete(){

            return CHtml::link('<span class="i-sprite is-remove red">&nbsp;</span>', Docebo::createAdminUrl('GamificationApp/Contests/delete', array(
                'id' => $this->id
            )), array(
                'class' => 'open-dialog',
                'data-dialog-title' => Yii::t('gamification', 'Delete Contest'),
                'data-dialog-class' => 'delete-contest'
            ));
    }

    public function getSelectionFromFilter(){

        if($this->filter === 'all'){
            return $this->filter;
        }

        $filter = CJSON::decode($this->filter);

        $data = array(
            'groups' => $filter['groups'],
            'branches' => $filter['branches']
        );

        return $data;
    }

    public function hasUsersSelected(){
        $filter = $this->filter;

        if($filter === 'all'){
            return false;
        }

        return true;
    }


	public function getFilterInfo(){
		if($this->filter === 'all'){
			return $this->filter;
		}

		$filter = CJSON::decode($this->filter);
		$result = array('groups' => array(), 'branches' => array());

		$filterGroups = $filter['groups'];
		$filterBranches = $filter['branches'];

		if(!empty($filterGroups) || !empty($filterBranches)){
			if(!empty($filterGroups)){
				foreach($filterGroups as $group){
					$result['groups'][] = $group;
				}
			}

			if(!empty($filterBranches)){
				foreach($filterBranches as $branch){
					if ($branch['selectState'] == 1) {
						$result['branches'][] = $branch['key'];
					}
					else {
						$orgNode = CoreOrgChartTree::model()->findByPk($branch['key']);
						if($orgNode) {
							$result['branches'][] = $orgNode->idOrg;
							$childrenArray = $orgNode->descendants()->findAll();
							foreach ($childrenArray as $branchModel) {
								$result['branches'][] = $branchModel->idOrg;
							}
						}
					}
				}
			}
		}

		return $result;
	}

	public function getUsersFromFilter(){
		$filter = $this->getFilterInfo();

		if($filter === 'all'){
			return array();
		}

		$users = array();

		$groups = $filter['groups'];
		$branches = $filter['branches'];

		if(!empty($branches)) {
			$sql = "select gm.idstMember as user
from core_org_chart_tree t1
join core_group_members gm ON gm.idst = t1.idst_oc
where t1.idOrg IN (" . implode(',', $branches) .")";
			$command = Yii::app()->db->createCommand($sql);
			$nodeUsers = $command->queryAll();
			$usersTmp = array();
			foreach($nodeUsers as $user){
				$usersTmp[] = $user['user'];
			}

			$users = array_merge($users, $usersTmp);
		}

		if(!empty($groups)){
			$command = Yii::app()->db->createCommand();
			$command->select('cgm.idstMember')
				->from('core_group_members cgm')
				->where('cgm.idst IN (' . implode(',', $groups) . ')');
			$groupUsers = $command->queryAll();
			$usersTmpGroup = array();
			foreach($groupUsers as $groupUser){
				if(is_array($groupUser) && isset($groupUser['idstMember'])){
					$usersTmpGroup[] = $groupUser['idstMember'];
				}else{
					$usersTmpGroup[] = $groupUser;
				}
			}
			$users = array_merge($users, $usersTmpGroup);
		}

		return $users;
	}

	public function getName($useFallback = true){
		if(!empty($this->translation)){
			return $this->translation->name;
		} else{
			if($useFallback){
				return $this->englishTranslation->name;
			} else{
				return $this->defaultTranslation->name;
			}
		}
	}

	public function getDescription($useFallback = true){
		if(!empty($this->translation)){
			return $this->translation->description;
		} else{
			if($useFallback){
				return $this->englishTranslation->description;
			} else{
				return $this->defaultTranslation->description;
			}
		}
	}

    /**
     * 
     * @param integer $userId
     * @param array $attr
     * 0 = moreThanOne
     * 1 = onlyFINISHEDOrINPROGRESS
     * @return mixed
     */
    public static function getLastContestByUser($userId, $attr = false){
        $closestContest = false;
        $hasInProgress = false;
        $hasScheduled = false;
        $allContests = self::model()->findAll();
        if(!empty($attr) && !empty($attr[0])) $lastContests = array();        
        
        foreach($allContests as $contest){
            if(!empty($attr)) {
                if(!empty($attr[0]) && $contest->filter == 'all' && in_array($contest->getStatus(), array(self::STATUS_INPROGRESS, self::STATUS_FINISHED))) $lastContests[] = $contest->id;
            }
            $contestUsers = $contest->getUsersFromFilter();
            if((is_array($contestUsers) && empty($contestUsers)) || (is_array($contestUsers) && !empty($contestUsers) && in_array($userId,$contestUsers))){
                $saveContest = false;
                switch ($contest->getStatus()){
                    case self::STATUS_INPROGRESS:
                        $hasInProgress = true;
                        $saveContest = true;
                        break;
                    case self::STATUS_SCHEDULED:
                        $hasScheduled = true;
                        if(!$hasInProgress){
                            if(!empty($attr) && !empty($attr[1]) && empty($attr[0])) break;
                            $saveContest = true;
                        }
                        break;
                    case self::STATUS_FINISHED:
                        if(!$hasInProgress && !$hasScheduled){
                            if ($closestContest) {
                                if ($contest->to_date > $closestContest->to_date)
                                    $closestContest = $contest;
                            } else {
                                $closestContest = $contest;
                            }
                        }
                        break;
                }

                if($saveContest) {
                    if ($closestContest) {
                        if ($closestContest->getStatus() == $contest->getStatus()) {
                            if (Yii::app()->localtime->isLt($contest->to_date, $closestContest->to_date)/*$contest->to_date < $closestContest->to_date*/) {
								$closestContest = $contest;
							}
                        } else
                            $closestContest = $contest;
                    } else {
                        $closestContest = $contest;
                    }
                }

            }

        }
        
        if(!(!empty($attr) && !empty($attr[0]))) {
            return $closestContest;
        } else {
            if(!empty($attr[1])) {
                return $closestContest;
            }
            return $lastContests;
        }

    }


    public function getStatus(){
        $now = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
		$fromDate = self::convertFromContestTimezoneToUTC($this->from_date, $this->timezone);
		$toDate = self::convertFromContestTimezoneToUTC($this->to_date, $this->timezone);

        if(Yii::app()->localtime->isGt($fromDate, $now)){
            return self::STATUS_SCHEDULED;
        } else if(Yii::app()->localtime->isLte($fromDate, $now) && Yii::app()->localtime->isGte($toDate, $now)){
            return self::STATUS_INPROGRESS;
        } else if(Yii::app()->localtime->isLt($toDate, $now)){
            return self::STATUS_FINISHED;
        }

        return false;
    }

	public static function renderContestGoal($data){
		$goals = self::scoreColumnName();

		return $goals[$data['goal']];
	}

	public static function renderContestPeriod($data){

		$useUserTimezone = Settings::get('timezone_allow_user_override', 'off');

		$timezone = '';
		$defaultTimezone = Settings::get('timezone_default', 'UTC');

		if($useUserTimezone == 'on'){
			$userTimezone = CoreSettingUser::model()->findByAttributes(array(
				'path_name' => 'timezone',
				'id_user' => Yii::app()->user->id
			));

			if($userTimezone){
				$timezone = $userTimezone->value;
			}
		} else{
			$timezone = $defaultTimezone;
		}

		if(empty($data['timezone'])){
			$data['timezone'] = $defaultTimezone;
		}


		$currentTimezoneText = '';
		if($data['timezone'] !== $timezone){
			$timezonesArray = Yii::app()->localtime->getTimezonesArray();
			preg_match('#\(.*?\)#', $timezonesArray[$data['timezone']], $currentTimezoneText);
		}
		return Yii::app()->localtime->toLocalDate($data['from_date']).' / '.Yii::app()->localtime->toLocalDate($data['to_date']) . ' ' . $currentTimezoneText[0];
	}

	public function getContestsOfPu($puId){
		$contests = self::model()->findAll();

		$puContests = array();
		foreach($contests as $contest){
			/**
			 * @var $contest GamificationContest
			 */
			$contestUsers = $contest->getUsersFromFilter();
			$criteria = new CDbCriteria();
			$criteria->addCondition('puser_id = ' . $puId);
			$criteria->addInCondition('user_id', $contestUsers);
			$puUsers = CoreUserPU::model()->findAll($criteria);

			if(count($puUsers) > 0){
				$puContests[] = $contest->id;
			}
		}

		return $puContests;
	}
    
    public function getUserContests() {
        $user_id = Yii::app()->user->getId();
        $allContests = self::model()->findAll();
        $user_contests = array();
        
        foreach($allContests as $contest) {
            $contestUsers = $contest->getUsersFromFilter();
            if(empty($contestUsers) && $contest->filter === 'all') {
                $user_contests[] = $contest->getPrimaryKey();
            }
            if(in_array($user_id, $contestUsers)) {
                $user_contests[] = $contest->getPrimaryKey();
            }

	        foreach($contestUsers as $contestUser){
		        if(is_array($contestUser)){
			        if(in_array($user_id, $contestUser)) {
				        $user_contests[] = $contest->getPrimaryKey();
			        }
		        }
	        }
        }
        
        return $user_contests;
    }
    
    public function isFinished() {
        return Yii::app()->localtime->isGte(
            Yii::app()->localtime->getLocalNow(),
            $this->to_date
        );
    }

	/**
	 * Removes a group $groupToRemove from the filter column
	 * @param int $groupToRemove
	 */
	public static function cleanUpGroups($groupToRemove) {
		$contests = Yii::app()->db->createCommand()
			->select('id, filter')
			->from(self::model()->tableName())
			->where('filter LIKE :filter', array(':filter' => '%'.$groupToRemove.'%'))
			->queryAll();
		foreach($contests as $contest) {
			$tmpData = json_decode($contest['filter'], true);
			if(isset($tmpData['groups']) && in_array($groupToRemove, $tmpData['groups'])) {
				$tmpData['groups'] = array_values(array_diff($tmpData['groups'], array($groupToRemove)));
				$filter = json_encode($tmpData);
				Yii::app()->db->createCommand()->update(self::model()->tableName(), array('filter' => $filter), 'id = :id', array(':id' => $contest['id']));
			}
		}
	}

	/**
	 * Removes a branch $branchToRemove from the filter column
	 * @param int $branchToRemove
	 */
	public static function cleanUpBranches($branchToRemove) {
		$contests = Yii::app()->db->createCommand()
			->select('id, filter')
			->from(self::model()->tableName())
			->where('filter LIKE :filter', array(':filter' => '%'.$branchToRemove.'%'))
			->queryAll();
		foreach($contests as $contest) {
			$tmpData = json_decode($contest['filter'], true);
			if(isset($tmpData['branches'])) {
				foreach($tmpData['branches'] as $key => $branch) {
					if($branchToRemove == $branch['key']) {
						unset($tmpData['branches'][$key]);
						break;
					}
				}
				$tmpData['branches'] = array_values($tmpData['branches']);
				$filter = json_encode($tmpData);
				Yii::app()->db->createCommand()->update(self::model()->tableName(), array('filter' => $filter), 'id = :id', array(':id' => $contest['id']));
			}
		}
	}
    
    /**
     * Convert datetime string from Specific timezone to UTC timezone
     *
     * @param $dateTimeString string
     * @param $timezone string
     * @return string Y-m-d H:i:s format
     */
    public static function convertFromContestTimezoneToUTC($dateTimeString,$timezone = null, $targetTimezone = false){
        $contestTimezone = !empty($timezone) ? $timezone : Settings::get('timezone_default', 'UTC');

        $dateTime = new DateTime($dateTimeString . ' ' . $contestTimezone);
		if(!$targetTimezone){
			$dateTime->setTimezone(new DateTimeZone('UTC'));
		} else{
			$dateTime->setTimezone(new DateTimeZone($targetTimezone));
		}
        return $dateTime->format('Y-m-d H:i:s');
    }
}