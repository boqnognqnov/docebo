<?php

/**
 * This is the model class for table "gamification_badge_translation".
 *
 * The followings are the available columns in table 'gamification_badge_translation':
 * @property integer $id_badge
 * @property string $lang_code
 * @property string $name
 * @property string $description
 *
 * The followings are the available model relations:
 * @property GamificationBadge $gamificationBadge
 */
class GamificationBadgeTranslation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gamification_badge_translation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_badge, lang_code, name', 'required'),
			array('id_badge', 'numerical', 'integerOnly'=>true),
			array('lang_code', 'length', 'max'=>50),
			array('name', 'length', 'max'=>255),
			array('description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_badge, lang_code, name, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'gamificationBadge' => array(self::BELONGS_TO, 'GamificationBadge', 'id_badge'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_badge' => 'Id Badge',
			'lang_code' => Yii::t('admin_lang', '_LANG_CODE'),
			'name' => Yii::t('standard', '_TITLE'),
			'description' => Yii::t('standard', '_DESCRIPTION'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_badge',$this->id_badge);
		$criteria->compare('lang_code',$this->lang_code,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GamificationBadgeTranslation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getModelsList($id_badge = 0)
	{
		$languages = CoreLangLanguage::getActiveLanguages();
		$models = array();

		foreach($languages as $code => $lang)
		{
			if($id_badge === 0)
			{
				$models[$code] = GamificationBadgeTranslation::model();
				$models[$code]->name = '';
				$models[$code]->description = '';
				$models[$code]->lang_code = $code;
			}
			else
			{
				$model = GamificationBadgeTranslation::findByAttributes(array('id_badge' => $id_badge, 'lang_code' => $code));
				if($model)
					$models[$code] = $model;
				else
				{
					$models[$code] = GamificationBadgeTranslation::model();
					$models[$code]->name = '';
					$models[$code]->description = '';
					$models[$code]->lang_code = $code;
				}
			}
		}

		return $models;
	}
}
