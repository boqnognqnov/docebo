<?php

/**
 * This is the model class for table "gamification_badge".
 *
 * The followings are the available columns in table 'gamification_badge':
 * @property integer $id_badge
 * @property string $icon
 * @property integer $score
 * @property string $event_name
 * @property string $event_params
 *
 * The followings are the available model relations:
 * @property GamificationAssignedBadges[] $gamificationAssignedBadges
 * @property GamificationBadgeTranslation[] $gamificationBadgeTranslations
 * @property GamificationBadgeTranslation $badgeTranslation
 * @property GamificationBadgeTranslation $defaultTranslation
 */
class GamificationBadge extends CActiveRecord
{
	public $confirm;
    protected $url;

    public $search_input = null;

    /**
     * @var int
     */
    public $badgeCounter;
    /**
     * This will hold the sum of all counted badges' score
     * @var int
     */
    public $badgeCounterScore;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gamification_badge';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('icon, score', 'required'),
			array('score', 'numerical', 'integerOnly'=>true),
			array('icon', 'length', 'max'=>255),
			array('event_name', 'length', 'max'=>100),
			array('event_params', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_badge, icon, score, event_name, event_params', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'gamificationAssignedBadges' => array(self::HAS_MANY, 'GamificationAssignedBadges', 'id_badge'),
			'gamificationBadgeTranslations' => array(self::HAS_MANY, 'GamificationBadgeTranslation', 'id_badge'),
			'badgeTranslation' => array(self::HAS_ONE, 'GamificationBadgeTranslation', 'id_badge', 'condition'=>'lang_code="'.Lang::getCodeByBrowserCode(Yii::app()->getLanguage()).'"'),
            'defaultTranslation' => array(self::HAS_ONE, 'GamificationBadgeTranslation', 'id_badge', 'condition' => 'lang_code = "' . CoreLangLanguage::getDefaultLanguage() . '"')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_badge' => 'Id Badge',
			'icon' => 'Icon',
			'score' => Yii::t('standard', '_SCORE'),
			'event_name' => 'Event Name',
			'event_params' => 'Event Params',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_badge',$this->id_badge);
		$criteria->compare('icon',$this->icon,true);
		$criteria->compare('score',$this->score);
		$criteria->compare('event_name',$this->event_name,true);
		$criteria->compare('event_params',$this->event_params,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GamificationBadge the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Returns the url of the badge icon
     * @return string
     */
    public function getIconUrl() {
    	$this->url = CoreAsset::url((int) $this->icon);
    	
    	if (empty($this->url)) {
    		$this->url = Yii::app()->baseUrl.'/../plugins/GamificationApp/assets/images/badge_blank.png';
    	}
    	return  $this->url;
        
    }

    /**
     * Returns true if this badge has a user uploaded icon
     * @return bool
     */
    public function hasUserIcon() {
        return strpos($this->icon, 'badge_r') !== false;
    }

    /**
     * @param string $name
     * @param string $description
     * @param int $badgeCounter
     * @internal param $score
     * @return string
     */
    public function renderBadge($name = '', $description = '')
    {
        $iconUrl = $this->getIconUrl();
        $this->badgeCounter = intval($this->badgeCounter);

        $cssBadgeCounter = 'badge_counter';
        if ($this->badgeCounter > 99) {
            $cssBadgeCounter .= ' x-small';
        }

        $html = '
                <div class="preview_badge">
                    <img src="'.$iconUrl.'" alt="'.$name.'" title="'.$name.'" />
                    <img class="flag" src="'.Yii::app()->baseUrl.'/../plugins/GamificationApp/assets/images/badge_flag.png'.'" alt="'.$name.'" title="'.$name.'" />
                    <span class="flag_text">'.$this->badgeCounterScore.'</span>'
                    . (!empty($name) ? '<span class="badge_title">'.$name.'</span>' : '')
                    . (!empty($description) ? '<span class="badge_description">'.((strlen($description) > 100) ? substr($description,0,97).'...' : $description).'</span>' : '')
                    . (($this->badgeCounter) ? '<span class="'.$cssBadgeCounter.'">'.$this->badgeCounter.'</span>' : '')
                .'</div>';

        return $html;
    }

    /**
     * Default data provider for listing badges in admin view
     * @return CActiveDataProvider
     */
    public function dataProvider()
    {
        $search = '';
        if(!empty($_GET['GamificationBadgeTranslation']))
            if (isset($_GET['GamificationBadgeTranslation']['name']))
                $search = $_GET['GamificationBadgeTranslation']['name'];


	    if (isset($_POST['GamificationBadgeTranslation']['name']))
		    $search = $_POST['GamificationBadgeTranslation']['name'];

        // Count records
        $criteria = new CDbCriteria();
        $criteria->together = true;
        $criteria->with = array('gamificationBadgeTranslations' => array (
            'joinType' => 'LEFT JOIN',
            'alias' => 'badgeTranslation',
            'on' => 'lang_code="'.Lang::getCodeByBrowserCode(Yii::app()->getLanguage()).'"'
        ));

        if ($search != '') {
            $criteria->addCondition('badgeTranslation.name like :search');
            $criteria->params = array(':search' => '%'.$search.'%');
        }

        if(!PluginManager::isPluginActive('Share7020App')){
            $events = array(
                GamificationAppModule::$EVENT_APP7020_ASSET_REACHED_A_GOAL,
                GamificationAppModule::$EVENT_APP7020_ASSET_VIEWED_FOR_LONGEST_TIME,
                GamificationAppModule::$EVENT_APP7020_EXPERT_REACHED_A_GOAL,
                GamificationAppModule::$EVENT_APP7020_USER_REACHED_A_GOAL,
                GamificationAppModule::$EVENT_APP7020_USER_REACHED_A_GOAL_AS_TOP_CONTRIBUTOR
                );
            $criteria->addNotInCondition('event_name',$events);
        }


        $count = GamificationBadge::model()->count($criteria);

        // Select all badges
        $config = array();
        $sortAttributes = array();
        foreach ($this->attributeNames() as $attributeName) {
            $sortAttributes[$attributeName] = 't.'.$attributeName;
        }

        $config['criteria'] = $criteria;
        $config['sort']['attributes'] = $sortAttributes;
        $config['sort']['defaultOrder'] = 'name desc';
        $config['pagination'] = array (
            'pageSize' => Settings::get('elements_per_page', 10)
        );
        $config['totalItemCount'] = $count;

        return new CActiveDataProvider(get_class($this), $config);
    }

    /**
     * Renders the name of the badge
     * @return mixed
     */
    public function renderBadgeName()
    {
        if($this->badgeTranslation)
            return $this->badgeTranslation->name;
        elseif($this->defaultTranslation)
			return $this->defaultTranslation->name;
		else
            return '';
    }

    /**
     * Renders badge popover
     * @param string $size
     * @return string
     */
    public function renderBadgePopover()
    {
        $badgeName = $this->renderBadgeName();
        $badgeDescription = ($this->badgeTranslation->description) ? $this->badgeTranslation->description : (($this->defaultTranslation->description) ? $this->defaultTranslation->description : '');
        return CHtml::image($this->getIconUrl(), $badgeName, array(
            'style' => 'width: 30px',
            'id' => 'image_'.$this->id_badge,
            'class' => 'popover_selector popover-trigger',
            'data-content' => $this->renderBadge($badgeName, $badgeDescription)
        ));
    }

    /**
     * Renders badge points
     * @return int
     */
    public function renderBadgePoints()
    {
        return $this->score;
    }

    /**
     * Renders badge assignment icon
     * @return string
     */
    public function renderBadgeAssignment()
    {
        $criteria = new CDbCriteria();
        $criteria->distinct = true;
        $criteria->select = 'id_user';
        $criteria->addCondition('id_badge = :id_badge');
        $criteria->params = array(':id_badge' => $this->id_badge);
        $count = GamificationAssignedBadges::model()->count($criteria);

        if($count > 0) {
            $text = $count.' <span class="enrolled"></span>';
            $class = 'assign-badge-count';
        }
        else {
            $text = Yii::t('standard', '_ASSIGN');
            $class = 'assign-badge';
        }

        $res = CHtml::link($text, '?r=GamificationApp/GamificationApp/assign&id_badge='.$this->id_badge, array('class' => $class));

        return $res;
    }

    public function pkToString(){
        $id = implode('_', array(
            $this->id_badge,
            $this->icon,
            $this->score,
        ));

        return $id;
    }

    public function getAllBadgesWithScore($excludeFromUser = false){
		$criteria = new CDbCriteria();
		$criteria->select = self::model()->getTableAlias().'.*, score AS badgeCounterScore';

		if($excludeFromUser) {
			$userBadges = Yii::app()->db->createCommand()
				->select('id_badge')
				->from(GamificationAssignedBadges::model()->tableName())
				->where('id_user = :id_user', array(':id_user' => $excludeFromUser))
				->queryColumn();
			$criteria->addNotInCondition('id_badge', $userBadges);
		}

        if(!PluginManager::isPluginActive('Share7020App')){
            $events = array(
                GamificationAppModule::$EVENT_APP7020_ASSET_REACHED_A_GOAL,
                GamificationAppModule::$EVENT_APP7020_ASSET_VIEWED_FOR_LONGEST_TIME,
                GamificationAppModule::$EVENT_APP7020_EXPERT_REACHED_A_GOAL,
                GamificationAppModule::$EVENT_APP7020_USER_REACHED_A_GOAL,
                GamificationAppModule::$EVENT_APP7020_USER_REACHED_A_GOAL_AS_TOP_CONTRIBUTOR
            );
            $criteria->addNotInCondition('event_name',$events);
        }

		$criteria->index = 'id_badge';

		$data = $this->findAll($criteria);

        return $data;
    }

    public function sqlDataProvider($all = false){
        // SQL Parameters
        $params = array();

        // BASE
        $commandBase = Yii::app()->db->createCommand();
        $commandBase->from('gamification_badge t');
        $commandBase->join(GamificationBadgeTranslation::model()->tableName().' gbt','gbt.id_badge = t.id_badge AND gbt.lang_code="'.Lang::getCodeByBrowserCode(Yii::app()->getLanguage()).'"');


        // Also search by user related text
        if ($this->search_input != null) {
            $commandBase->andWhere("CONCAT(name) LIKE :search_query");
            $commandBase->orWhere("CONCAT(description) LIKE :search_query");
            $params[':search_query'] = "%" . $this->search_input . "%";
        }

        // ORDERING
        $commandBase->order = "";
        $sort = new CSort();
        $sort->attributes = array(
            'name' => array(
                'asc' 	=> 'name',
                'desc'	=> 'name DESC',
            ),
            'description' => array(
                'asc' 	=> 'description',
                'desc'	=> 'description DESC',
            ),
        );
        $sort->defaultOrder = array(
            'name'		=>	CSort::SORT_ASC,
        );

        // COUNTER
        $commandCounter = clone $commandBase;
        $commandCounter->select('count(t.id_badge)');
        $numRecords = $commandCounter->queryScalar($params);


        // DATA-II
//        $commandData->select("*");
        //$commandData->select("t.id_badge id_badge,t.icon icon,t.score points, if(gbt.name is null, (select name from gamification_badge_translation as gb2 where gb2.id_badge=t.id_badge and gb2.lang_code='english'),gbt.name) nameTrans, if(gbt.description is null, (select description from gamification_badge_translation as gb3 where gb3.id_badge=t.id_badge and gb3.lang_code='english'),gbt.description) descrTrans");
        $commandBase->select('t.id_badge id_badge,t.icon icon,t.score points,name,description');
        // END
        $pageSize = Settings::get('elements_per_page', 20);
        $config = array(
            'totalItemCount'	=>	$numRecords,
            'pagination' 		=> array(
                'pageSize' => $all ? $numRecords : $pageSize
            ),
            'keyField'			=> 'id_badge',
            'sort' 				=> $sort,
            'params'			=> $params,
        );

        // Pass dbCommand, not SQL text, to apply parameters binding !!!
        $dataProvider = new CSqlDataProvider($commandBase, $config);

        return $dataProvider;
    }

    public function beforeSave(){
        if($this->getIsNewRecord()){
            if(count($this->event_params) > 0){
                $params = CJSON::decode($this->event_params);

                //Set the date create flag
                $params['date_created'] = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
                $this->event_params = CJSON::encode($params);
            }
        }

        return parent::beforeSave();
    }

    public function afterSave(){
        if($this->isNewRecord){
            if($this->event_name == GamificationAppModule::$EVENT_APP7020_USER_REACHED_A_GOAL_AS_TOP_CONTRIBUTOR){
                $paramArray  = CJSON::decode($this->event_params);
                switch ($paramArray['goal']) {
                    case 0:
                        $timeframe = CoreJob::PERIOD_DAY;
                        break;
                    case 1:
                        $timeframe = CoreJob::PERIOD_WEEK;
                        break;
                    case 2:
                        $timeframe = CoreJob::PERIOD_MONTH;
                        break;
                }

            }

            $job = Yii::app()->scheduler->createJob('Badge', UserReachedAGoalAsTopContributorJobHandler::HANDLER_ID, CoreJob::TYPE_RECURRING,
                $timeframe,
                1,
                0,  // minute is always 0
                0,
                false,
                false,
                false,
                Yii::app()->localtime->getUTCNow(),
                array(
                    'badge_id'=>$this->id_badge,
                    'goal' => $paramArray['goal']
                ),
                false,
                false,
                false);
        }
    }
}
