<?php

/**
 * This is the model class for table "gamification_leaderboard".
 *
 * The followings are the available columns in table 'gamification_leaderboard':
 * @property integer $id
 * @property string $filter
 * @property integer $is_active
 * @property integer $default
 *
 * The followings are the available model relations:
 * @property GamificationLeaderboardTranslation[] $leaderboardTranslations
 * @property GamificationLeaderboardTranslation $translation
 * @property GamificationLeaderboardTranslation $defaultTranslation
 */
class GamificationLeaderboard extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gamification_leaderboard';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array('filter', 'required'),
				array('is_active, default', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
				array('id, filter, is_active, default', 'safe', 'on'=>'search'),
				array('filter', 'checkFilter', 'on' => 'filterChanged, insert')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automati
		//cally generated below.
		return array(
				'leaderboardTranslations' => array(self::HAS_MANY, 'GamificationLeaderboardTranslation', 'id_board'),
				'translation' => array(self::HAS_ONE, 'GamificationLeaderboardTranslation', 'id_board', 'condition'=>'lang_code="'.Lang::getCodeByBrowserCode(Yii::app()->getLanguage()).'"'),
				'defaultTranslation' => array(self::HAS_ONE, 'GamificationLeaderboardTranslation', 'id_board', 'condition' => 'lang_code = "' . CoreLangLanguage::getDefaultLanguage() . '"')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
				'id' => 'ID',
				'filter' => 'Filter',
				'is_active' => 'Is Active',
				'default' => 'Default',
		);
	}

	public function checkFilter(){
		$boards = self::model()->findAll();

		foreach($boards as $board){
			if($board->filter === $this->filter && $board->id !== $this->id){
				$this->addError('filter', Yii::t('gamification', 'You cannot create two identical leaderboards'));
			}
		}
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('filter',$this->filter,true);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('default',$this->default);

		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GamificationLeaderboard the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function dataProvider(){
		$search = '';
		if (isset($_POST['search_text']))
			$search = $_POST['search_text'];

		// Count records
		$criteria = new CDbCriteria();
		$criteria->together = true;
		$criteria->with = array('leaderboardTranslations' => array (
				'joinType' => 'LEFT JOIN',
				'alias' => 'leaderboardTranslations',
				'on' => 'lang_code="'.Lang::getCodeByBrowserCode(Yii::app()->getLanguage()).'"'
		));

		if ($search != '') {
			$criteria->addCondition('leaderboardTranslations.name like :search');
			$criteria->params = array(':search' => '%'.$search.'%');
		}

		$count = GamificationLeaderboard::model()->count($criteria);

		// Select all badges
		$config = array();
		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.'.$attributeName;
		}

		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 't.id asc';
		$config['pagination'] = array (
				'pageSize' => Settings::get('elements_per_page', 10)
		);
		$config['totalItemCount'] = $count;

		return new CActiveDataProvider(get_class($this), $config);
	}

	public function renderBoardName(){
        if(empty($this->translation))
            $this->translation = GamificationLeaderboardTranslation::model()->findByAttributes(array('id_board' => $this->id, 'lang_code' => Settings::get('default_language')));
        
		if($this->translation)
			return $this->translation->name;
		else
			return '';
	}

	public function renderFilter(){

		$filterText = '';

		if($this->default == 1){
			$filterText = Yii::t('standard', 'All branches and groups');
		} else{
			if($this->filter === 'all'){
				$text = Yii::t('standard', 'All branches and groups');
			} else{
				$filter = CJSON::decode($this->filter);
				$groups = $filter['groups'];
				$branches = $filter['branches'];
				$text = count($branches) . ' ' . Yii::t('standard', 'branches') . ' / ' . count($groups) . ' ' . Yii::t('standard', '_GROUPS');
			}

			$usersSelectorUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
					'type'  		=> UsersSelector::TYPE_GAMIFICATION_LEADERBOARD_NO_WIZARD,
					'idForm' 		=> 'leaderboar-edit-selector',
					'idGeneral' 	=> $this->id,
					'noyiixviewjs'	=> true,  // very important: main page already has widget(s) loading yiigridview.js, so, don't
					'name'          => 'usersSelector',
					'tp'            => 'plugin.GamificationApp.views.leaderboardWizard.users_top_panel',
					'collectorUrl'	=> Docebo::createAdminUrl('GamificationApp/LeaderboardWizard/editSelection')
			));

			$filterText = CHtml::link($text, $usersSelectorUrl, array(
					'class' => 'open-dialog leaderboard-filter',
					'data-dialog-title' => Yii::t('gamification', 'Edit Leaderboard'),
					'data-dialog-class' => 'users-selector',
					'data-dialog-id' => 'users-selector-leaderboard'
			));
		}


		return $filterText;
	}

	public function renderActivate(){
		if($this->is_active){
			return CHtml::link('<span class="i-sprite is-circle-check green">&nbsp;</span>', 'javascript:', array(
				'class' => 'activate-leaderboard',
				'data-leaderboard-id' => $this->id
			));
		} else{
			return CHtml::link('<span class="i-sprite is-circle-check gray">&nbsp;</span>', 'javascript:', array(
					'class' => 'activate-leaderboard',
					'data-leaderboard-id' => $this->id
			));
		}
	}

	public function renderEdit(){
		return CHtml::link('<span class="i-sprite is-edit">&nbsp;</span>', Docebo::createAdminUrl('GamificationApp/LeaderboardWizard/wizard', array(
			'id' => $this->id
		)), array(
			'class' => 'open-dialog',
			'data-dialog-title' => Yii::t('gamification', 'Edit Leaderboard'),
			'data-dialog-class' => 'leaderboard-wizard-step-name',
			'data-dialog-id' => 'leaderboard-dialog'
		));
	}

	public function renderView(){
		return CHtml::link('<i class="fa fa-search" style="font-size: 18px;"></i>', Docebo::createAdminUrl('GamificationApp/LeaderboardWizard/viewBoard', array(
			'id' => $this->id
		)), array(
			'class' => 'open-dialog view-leaderboard',
			'data-dialog-title' => Yii::t('gamification', 'Leaderboard'),
			'data-dialog-class' => 'leaderboard-view',
			'data-dialog-id' => 'leaderboard-view-' . $this->id
		));
	}

	public function renderDelete(){

		if($this->default){
			return '';
		} else{
			return CHtml::link('<span class="i-sprite is-remove red">&nbsp;</span>', Docebo::createAdminUrl('GamificationApp/LeaderboardWizard/deleteLeaderboard', array(
					'id' => $this->id
			)), array(
					'class' => 'open-dialog',
					'data-dialog-title' => Yii::t('gamification', 'Delete Leaderboard'),
					'data-dialog-class' => 'delete-leaderboar'
			));
		}
	}

	public function getSelectionFromFilter(){

		if($this->filter === 'all'){
			return $this->filter;
		}

		$filter = CJSON::decode($this->filter);

		$data = array(
			'groups' => $filter['groups'],
			'branches' => $filter['branches']
		);

		return $data;
	}

	public function hasUsersSelected(){
		$filter = $this->filter;

		if($filter === 'all'){
			return false;
		}

		return true;
	}

	public function isUserPartOfGroups($groups = array(), $userId){
		$command = Yii::app()->db->createCommand();
		/**
		 * @var $command CDbCommand
		 */

		$command->select('user.idst idUser');
		$command->from(CoreGroupMembers::model()->tableName() . ' gm');
		$command->join(CoreUser::model()->tableName() . ' user', 'gm.idstMember = user.idst');
		foreach($groups as $group){
			$command->orWhere('gm.idst = ' . $group . ' AND user.idst = ' . $userId);
		}

		$result = $command->queryAll();

		return !empty($result);
	}

	/**
	 * Removes a group $groupToRemove from the filter column
	 * @param int $groupToRemove
	 */
	public static function cleanUpGroups($groupToRemove) {
		$leaderboards = Yii::app()->db->createCommand()
			->select('id, filter')
			->from(self::model()->tableName())
			->where('filter LIKE :filter', array(':filter' => '%'.$groupToRemove.'%'))
			->queryAll();
		foreach($leaderboards as $leaderboard) {
			$tmpData = json_decode($leaderboard['filter'], true);
			if(isset($tmpData['groups']) && in_array($groupToRemove, $tmpData['groups'])) {
				$tmpData['groups'] = array_values(array_diff($tmpData['groups'], array($groupToRemove)));
				$filter = json_encode($tmpData);
				Yii::app()->db->createCommand()->update(self::model()->tableName(), array('filter' => $filter), 'id = :id', array(':id' => $leaderboard['id']));
			}
		}
	}

	/**
	 * Removes a branch $branchToRemove from the filter column
	 * @param int $branchToRemove
	 */
	public static function cleanUpBranches($branchToRemove) {
		$leaderboards = Yii::app()->db->createCommand()
			->select('id, filter')
			->from(self::model()->tableName())
			->where('filter LIKE :filter', array(':filter' => '%'.$branchToRemove.'%'))
			->queryAll();
		foreach($leaderboards as $leaderboard) {
			$tmpData = json_decode($leaderboard['filter'], true);
			if(isset($tmpData['branches'])) {
				foreach($tmpData['branches'] as $key => $branch) {
					if($branchToRemove == $branch['key']) {
						unset($tmpData['branches'][$key]);
						break;
					}
				}
				$tmpData['branches'] = array_values($tmpData['branches']);
				$filter = json_encode($tmpData);
				Yii::app()->db->createCommand()->update(self::model()->tableName(), array('filter' => $filter), 'id = :id', array(':id' => $leaderboard['id']));
			}
		}
	}
    
    public static function getActiveLeaderboards($userId = null, $leaderboardId = null, $showUnactive = false){
		$rootNode = CoreOrgChartTree::getOrgRootNode();
		$rootNode = $rootNode->idOrg;

		$command = Yii::app()->db->createCommand();
		/**
		 * @var $command CDbCommand
		 */
		$command->select('id, filter')
			->from(GamificationLeaderboard::model()->tableName());
		if(!$showUnactive){
			$command->where('is_active = 1');
		}
		if($leaderboardId !== null){
			$command->andWhere('id = ' . $leaderboardId);
		}
		$boards = $command->queryAll();

		$leaderboardUsers = array();
		$leaderboards = array();
		foreach($boards as $board){
			$leaderboardUsers[$board['id']] = array();
			$filter = $board['filter'];
			if($filter === 'all'){
				$leaderboards[] = $board['id'];
			} else{
				$filter = CJSON::decode($filter);
				$groups = is_array($filter['groups']) ? $filter['groups'] : array();
				$branches = $filter['branches'];

				if(!empty($branches)) {
					$nodeWithDescendatnsIds = array();
					$nodeIds = array();
					foreach($branches as $branch){
						if($branch['key'] === $rootNode && $branch['selectState'] == 2){
							$leaderboards[] = $board['id'];
							continue;
						} else if($branch['selectState'] == 2){
							$nodeWithDescendatnsIds[] = $branch['key'];
						} else{
							$nodeIds[] = $branch['key'];
						}
					}

					if(!empty($nodeWithDescendatnsIds)){
						$sql = "select gm.idstMember as user
from core_org_chart_tree t2
join (select t1.idOrg, t1.iLeft, t1.iRight, t1.idst_oc
from core_org_chart_tree t1
where t1.idOrg IN (" . implode(',', $nodeWithDescendatnsIds) .")) as org
join core_group_members gm ON gm.idst = t2.idst_oc  OR gm.idst = org.idst_oc
where t2.iLeft > org.iLeft AND t2.iRight < org.iRight GROUP BY user";						
						$command = Yii::app()->db->createCommand($sql);
						$nodeUsers = $command->queryAll();
						$users = array();
						foreach($nodeUsers as $user){
							$users[] = $user['user'];
						}

						if($userId){
							if(in_array($userId, $users)){
								$leaderboardUsers[$board['id']] = array_merge($leaderboardUsers[$board['id']], $users);
								$leaderboards[] = $board['id'];
							}
						} else{
							$leaderboardUsers[$board['id']] = array_merge($leaderboardUsers[$board['id']], $users);
							$leaderboards[] = $board['id'];
						}
					}

					if(!empty($nodeIds)){
						$sql = "select gm.idstMember as user
from core_org_chart_tree t1
join core_group_members gm ON gm.idst = t1.idst_oc
where t1.idOrg IN (" . implode(',', $nodeIds) .")";
						$command = Yii::app()->db->createCommand($sql);
						$nodeUsers = $command->queryAll();
						$users = array();
						foreach($nodeUsers as $user){
							$users[] = $user['user'];
						}

						if($userId){
							if(in_array($userId, $users)){
								$leaderboardUsers[$board['id']] = array_merge($leaderboardUsers[$board['id']], $users);
								$leaderboards[] = $board['id'];
							}
						} else{
							$leaderboardUsers[$board['id']] = array_merge($leaderboardUsers[$board['id']], $users);
							$leaderboards[] = $board['id'];
						}
					}
				}

				if(!empty($groups)){
					$command = Yii::app()->db->createCommand();
					$command->select('cgm.idstMember')
						->from('core_group_members cgm')
						->where('cgm.idst IN (' . implode(',', $groups) . ')');
					$groupUsers = $command->queryAll();

					$users = array();
					if(!empty($groupUsers)){
						foreach($groupUsers as $user){
							$users[] = $user['idstMember'];
						}
					}


					if($userId){
						if(in_array($userId, $users)){
							$leaderboardUsers[$board['id']] = array_merge($leaderboardUsers[$board['id']], $users);
							$leaderboards[] = $board['id'];
						}
					} else{
						$leaderboardUsers[$board['id']] = array_merge($leaderboardUsers[$board['id']], $users);
						$leaderboards[] = $board['id'];
					}
				}

			}
		}

		if(empty($leaderboards)){
			return false;
		}

		return array(
			'leaderboards' => $leaderboards,
			'users' => $leaderboardUsers
		);
	}
}