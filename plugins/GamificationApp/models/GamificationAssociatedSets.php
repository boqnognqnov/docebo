<?php

/**
 * This is the model class for table "gamification_associated_sets".
 *
 * The followings are the available columns in table 'gamification_associated_sets':
 * @property integer $id
 * @property integer $id_reward
 * @property integer $id_set
 *
 * The followings are the available model relations:
 * @property GamificationReward $rewardModel
 * @property GamificationRewardsSet $rewardsSetModel
 */
class GamificationAssociatedSets extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gamification_associated_sets';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_reward, id_set', 'required'),
			array('id, id_reward, id_set', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_reward, id_set', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rewardModel' => array(self::BELONGS_TO, 'GamificationReward', 'id_reward'),
			'rewardsSetModel' => array(self::BELONGS_TO, 'GamificationRewardsSet', 'id_set'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_reward' => 'Id Reward',
			'id_set' => 'Id Set',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_reward',$this->id_reward);
		$criteria->compare('id_set',$this->id_set);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GamificationAssociatedSets the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function dataProvider(){
		$search = '';
		$id = Yii::app()->request->getParam('id', false);

		// Get lang code
		$browserCode = Yii::app()->getLanguage();
		$language = Lang::getCodeByBrowserCode($browserCode);

		if (isset($_POST['search_text']))
			$search = $_POST['search_text'];

		// Count records
		$criteria = new CDbCriteria();
		$criteria->join = ' LEFT JOIN `'. GamificationReward::model()->tableName() . '` AS `gr` ON t.id_reward = gr.id';

		// Join with the Rewards translations db table
		$criteria->join = ' LEFT JOIN `'. GamificationRewardTranslation::model()->tableName() . '` AS `grtr` ON grtr.id_reward = t.id_reward';//gr.id';

		$criteria->addCondition('grtr.lang_code = :language');
		$criteria->params = array(':language' => $language);

		if ($id) {
			$criteria->addCondition('t.id = :id');
//			$criteria->params = array(':id' => $id);
			$criteria->params = array_merge($criteria->params, array(':id' => $id));
		}

		if ($search != '') {
			$criteria->addCondition('t.name like :search OR t.description LIKE :search');
//			$criteria->params = array(':search' => '%'.$search.'%');
			$criteria->params = array_merge($criteria->params, array(':search' => '%'.$search.'%'));
		}

		$count = GamificationAssociatedSets::model()->count($criteria);

		// Select all rewards
		$config = array();
		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.'.$attributeName;
		}

		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 't.id asc';
		$config['pagination'] = array (
				'pageSize' => Settings::get('elements_per_page', 10)
		);
		$config['totalItemCount'] = $count;

		return new CActiveDataProvider(get_class($this), $config);
	}

	public static function sqlDataProvider($onlyNotAssigned = false) {
		// id of the reward set
		$id = Yii::app()->request->getParam('id', false);

		// search term
		$term = Yii::app()->request->getParam('search_input', false);

		// Default LMS language
		$base_language = CoreLangLanguage::getDefaultLanguage();

		// Get lang code
		$browserCode = Yii::app()->getLanguage();
		$language = Lang::getCodeByBrowserCode($browserCode);

		$commandBase = Yii::app()->db->createCommand()
				->group('gr.id')
				->from('gamification_reward gr')
				->leftJoin('gamification_associated_sets t', 't.id_reward = gr.id')
				->leftJoin('gamification_reward_translation grtr', 'grtr.id_reward = gr.id AND grtr.lang_code = "' . $language.'"')
				->leftJoin('gamification_reward_translation grtr2', 'grtr2.id_reward = gr.id AND grtr2.lang_code = "' . $base_language.'"')
				->andWhere('(t.id_set = ' . $id . ' OR gr.association = "'. GamificationReward::ASSOCIATION_ALL .'")')
				->select('
						gr.id as id,
						gr.id as id_reward,
						t.id_set,

						(CASE
							WHEN grtr.name IS NULL THEN grtr2.name
							ELSE grtr.name
						END) AS reward_name,

						(CASE
							WHEN grtr.description IS NULL THEN grtr2.description
							ELSE grtr.description
						END) AS reward_description,


						gr.coins AS coins,
						gr.availability AS availability,
						gr.picture AS picture
					');

		if($term) {
			$commandBase->andWhere('grtr.name LIKE "%' . $term.'%"');
		}

		//For sorting the table by any of the given column
		$sort = new CSort();
		$sort->attributes = array(
				'name' => array(
						'asc' 	=> 'grtr.name',
						'desc'	=> 'grtr.name DESC',
				),
				'description' => array(
						'asc' 	=> 'grtr.description',
						'desc'	=> 'grtr.description DESC',
				),
		);
		$sort->defaultOrder = array(
				'name'		=>	CSort::SORT_ASC,
		);

		//To show "Total:[number]"
		$commandCounter = clone $commandBase;
		$commandCounter->select('t.id_set');

		$numRecords = count($commandCounter->queryColumn());


		$config = array(
						'totalItemCount' => $numRecords,
						'sort' => $sort
				);

		$dataProvider = new CSqlDataProvider($commandBase->getText(), $config);
		return $dataProvider;
	}

	public static function sqlDataProviderNotAssignedrewards($searchTerm = false) {
		// id of the reward set
		$id = Yii::app()->request->getParam('id', false);

		// Default LMS language
		$base_language = CoreLangLanguage::getDefaultLanguage();

		// search term
		if (!$searchTerm) {
			$term = Yii::app()->request->getParam('search_input', false);
		} else {
			$term = $searchTerm;
		}

		// Get lang code
		$browserCode = Yii::app()->getLanguage();
		$language = Lang::getCodeByBrowserCode($browserCode);

		$RewardsInSet = Yii::app()->db->createCommand()
				->from('gamification_associated_sets t')
				->andWhere('t.id_set = '.$id)
				->select('t.id_reward')
				->queryAll();


		$RewardsSetIds = array();
		$RewardsSetIdsStr = '';
		if(is_array($RewardsInSet) && count($RewardsInSet) > 0) {
			foreach($RewardsInSet as $value) {
				$RewardsSetIds[$value['id_reward']] = $value['id_reward'];
			}

			$RewardsSetIdsStr = implode(',', $RewardsSetIds);
		}

		$commandBase = Yii::app()->db->createCommand()
				->from('gamification_reward t')
				->leftJoin('gamification_reward_translation grtr', 'grtr.id_reward = t.id AND grtr.lang_code = "' . $language.'" ')
				->leftJoin('gamification_reward_translation grtr2', 'grtr2.id_reward = t.id AND grtr2.lang_code = "' . $base_language.'" ')
				->select('t.id,

						(CASE
							WHEN grtr.name IS NULL THEN grtr2.name
							ELSE grtr.name
						END) AS reward_name,

						(CASE
							WHEN grtr.description IS NULL THEN grtr2.description
							ELSE grtr.description
						END) AS reward_description,


						t.coins AS coins,
						t.availability AS availability,
						t.picture AS picture');


		if($term) {
			$commandBase->andWhere('grtr.name LIKE "%' . $term.'%"');
		}

		if(strlen($RewardsSetIdsStr) > 0) {
			$commandBase->andWhere('t.id NOT IN ('.$RewardsSetIdsStr.')');
		}

		$commandBase->andWhere('t.association = "'. GamificationReward::ASSOCIATION_SELECTION .'" OR t.association = "'. GamificationReward::ASSOCIATION_NONE .'"');

		//For sorting the table by any of the given column
		$sort = new CSort();
		$sort->attributes = array(
				'name' => array(
						'asc' 	=> 'grtr.name',
						'desc'	=> 'grtr.name DESC',
				),
				'description' => array(
						'asc' 	=> 'grtr.description',
						'desc'	=> 'grtr.description DESC',
				),
		);
		$sort->defaultOrder = array(
				'name'		=>	CSort::SORT_ASC,
		);

		//To show "Total:[number]"
		$commandCounter = clone $commandBase;
		$commandCounter->select('t.id');

		$numRecords = count($commandCounter->queryColumn());


		$config = array(
				'totalItemCount' => $numRecords,
				'sort' => $sort
		);

		$dataProvider = new CSqlDataProvider($commandBase->getText(), $config);
		return $dataProvider;
	}

	public static function unassignRewardFromSet($idReward, $idSet) {
		$RewardAssociationType = Yii::app()->db->createCommand()
				->from('gamification_reward t')
				->andWhere('t.id = '.$idReward)
				->select('t.association')
				->queryScalar();

		// If regular assignment
		if ($RewardAssociationType == GamificationReward::ASSOCIATION_SELECTION) {
            GamificationAssociatedSets::model()->deleteAllByAttributes(array(
		            'id_set' => $idSet,
		            'id_reward' => $idReward
            ));

            $setsAssociatedWithReward = Yii::app()->db->createCommand("SELECT COUNT(*) AS c FROM ". GamificationAssociatedSets::model()->tableName()." WHERE id_reward = '{$idReward}'")->queryScalar();
            if((int)$setsAssociatedWithReward == 0) {
                $reward = GamificationReward::model()->findByPk($idReward);
                if($reward) {
                    $reward->association = GamificationReward::ASSOCIATION_NONE;
                    $reward->save();
                }
            }

		} else {
			// If assigned to all rewards sets
			$rewardModel = GamificationReward::model()->findByPk($idReward);
			$rewardModel->association = GamificationReward::ASSOCIATION_SELECTION;
			$rewardModel->save(false);

			// Get all other reward set
			$allRewardSetsArray = Yii::app()->db->createCommand()
					->select('id')
					->from(GamificationRewardsSet::model()->tableName())
					->where('id != :id_set', array(':id_set' => $idSet))
					->queryAll();

			// If there are other reward sets, assign the reward to them
			if (count($allRewardSetsArray) > 0) {
				$sql = "INSERT INTO gamification_associated_sets (id_set, id_reward) values (:id_set, :id_reward)";

				foreach ($allRewardSetsArray as $row) {
					// Add
					$parameters = array(":id_set"=>$row['id'], ":id_reward"=> $idReward);
					Yii::app()->db->createCommand($sql)->execute($parameters);
				}
			}
		}
	}
}
