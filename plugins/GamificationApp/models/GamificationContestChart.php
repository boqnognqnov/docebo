<?php

/**
 * This is the model class for table "gamification_contest_chart".
 *
 * The followings are the available columns in table 'gamification_contest_chart':
 * @property integer $id
 * @property integer $id_contest
 * @property integer $rank
 * @property integer $id_user
 * @property string $full_name
 * @property integer $ranking_result
 * @property string $reward_data
 *
 * The followings are the available model relations:
 * @property GamificationContest $contest
 */
class GamificationContestChart extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gamification_contest_chart';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_contest, id_user, full_name, ranking_result', 'required'),
			array('id_contest, rank, id_user, ranking_result', 'numerical', 'integerOnly'=>true),
			array('full_name', 'length', 'max'=>255),
			array('reward_data', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_contest, rank, id_user, full_name, ranking_result, reward_data', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contest' => array(self::BELONGS_TO, 'GamificationContest', 'id_contest'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_contest' => 'Id Contest',
			'rank' => 'Rank',
			'id_user' => 'Id User',
			'full_name' => 'Full Name',
			'ranking_result' => 'Ranking Result',
			'reward_data' => 'Reward Data',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_contest',$this->id_contest);
		$criteria->compare('rank',$this->rank);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('full_name',$this->full_name,true);
		$criteria->compare('ranking_result',$this->ranking_result);
		$criteria->compare('reward_data',$this->reward_data,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GamificationContestChart the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function afterSave(){
		if($this->getScenario() == 'issueRewards'){
			Yii::app()->event->raise(EventManager::EVENT_USER_WINNED_CONTEST, new DEvent($this, array(
				'reward_params' => CJSON::decode($this->reward_data),
				'id_user' => $this->id_user,
				'rank' => $this->rank,
				'contest' => $this->contest
			)));
		}
	}

	protected function getUserModel(){
		$userModel = CoreUser::model()->findByPk($this->id_user);

		return $userModel;
	}

	public function getUserAvatar($onlySrc = true){
		$userModel = $this->getUserModel();

		/**
		 * @var $userModel CoreUser
		 */

		if($userModel){
			return $userModel->getAvatarImage($onlySrc);
		} else{
			return Yii::app()->theme->baseUrl .  '/images/standard/user.png';
		}
	}

	public function getAvatarImage($onlySrc = true){
		return $this->getUserAvatar($onlySrc);
	}

	public function getFullName(){
		$userModel = $this->getUserModel();

		if($userModel){
			return $userModel->getFullName();
		} else{
			return $this->full_name;
		}
	}

	public function getRewardData(){

		if(!$this->reward_data){
			return null;
		}

		$rewardData = CJSON::decode($this->reward_data);

		if($rewardData['type'] == GamificationContestReward::REWARD_TYPE_BADGE){
			return $this->getBadgeData();
		}
	}

	protected function getBadgeData(){
		$rewardData = CJSON::decode($this->reward_data);

		$data = array();
		if($rewardData['type'] == GamificationContestReward::REWARD_TYPE_BADGE){
			$badgeModel = GamificationBadge::model()->findByPk($rewardData['id_item']);

			if($badgeModel){
				$data['points'] = $badgeModel->score;
				$data['icon'] = $badgeModel->icon;
			} else{
				$rewardValue = $rewardData['value'];
				$data['points'] = $rewardValue['points'];
				$data['icon'] = $rewardValue['badge_icon'];
			}
		}

		return $data;
	}

	public function renderUser(){
		return '<img src="' . $this->getUserAvatar() . '" /> ' . $this->getFullName();
	}

	public function renderPoints(){
		$rewardData = $this->getRewardData();

		if($rewardData){
			return '<span class="gamification-sprite star-small"></span>&nbsp;' . $rewardData['points'];
		} else{
			return '';
		}
	}

	public function renderBadge(){
		$rewardData = $this->getRewardData();

		if($rewardData) {
			return '<img src = "' . CoreAsset::model()->findByPk($rewardData['icon'])->getUrl() . '" />';
		} else{
			return '';
		}
	}

	public function dataProvider($id_contest){
		$search = '';
		if (isset($_REQUEST['search_text']))
				$search = $_REQUEST['search_text'];

		// Count records
		$criteria = new CDbCriteria();
		$criteria->addCondition('id_contest = ' . $id_contest);

		if ($search != '') {
			$criteria->addCondition('full_name like :search');
			$criteria->params[':search'] = '%'.$search.'%';
		}

		$count = GamificationContestChart::model()->count($criteria);

		// Select all badges
		$config = array();
		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.'.$attributeName;
		}

		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 't.id asc';
		$config['pagination'] = array (
				'pageSize' => Settings::get('elements_per_page', 10)
		);
		$config['totalItemCount'] = $count;

		return new CActiveDataProvider(get_class($this), $config);
	}
}
