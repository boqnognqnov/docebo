<?php

/**
 * This is the model class for table "gamification_user_rewards".
 *
 * The followings are the available columns in table 'gamification_user_rewards':
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_reward
 * @property integer $status
 * @property string $data
 * @property integer $coins
 * @property string $date_created
 * @property string $date_modified
 * @property integer $modified_by
 *
 * The followings are the available model relations:
 * @property CoreUser $userModel
 * @property GamificationReward $rewardModel
 */
class GamificationUserRewards extends CActiveRecord
{
	public static $STATUS_PENDING = 0;
	public static $STATUS_APPROVED = 1;
	public static $STATUS_REJECTED = 2;
	public static $STATUS_CANCELED = 3;

	const REWARD_HANDLER_USER_ASKED_TO_REDEEM_REWARD = 'UserAskedToRedeemReward';
	const REWARD_HANDLER_SUPERADMINISTRATOR_APPROVED_REWARD_REQUEST = 'SuperadministratorApprovedRewardRequest';
	const REWARD_HANDLER_SUPERADMINISTRATOR_REJECTED_REWARD_REQUEST = 'SuperadministratorRejectedRewardRequest';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gamification_user_rewards';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user, id_reward', 'required'),
			array('id, id_user, id_reward, status, modified_by, coins', 'numerical', 'integerOnly'=>true),
			array('data, date_created, date_modified', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user, id_reward, status, data, coins, date_created, date_modified, modified_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userModel' => array(self::BELONGS_TO, 'CoreUser', 'id_user'),
			'rewardModel' => array(self::BELONGS_TO, 'GamificationReward', 'id_reward'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'id_reward' => 'Id Reward',
			'status' => 'Status',
			'data' => 'Data',
			'coins'	=> 'Coins',
			'date_created' => 'Date Created',
			'date_modified' => 'Date Modified',
			'modified_by' => 'Modified By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('id_reward',$this->id_reward);
		$criteria->compare('status',$this->status);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_modified',$this->date_modified,true);
		$criteria->compare('modified_by',$this->modified_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GamificationUserRewards the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function afterSave() {

        $eventFirst = true;
        if($this->status === self::$STATUS_APPROVED) {
            $eventName = CoreNotification::NTYPE_GAMIFICATION_REWARD_APPROVED;
        } elseif($this->status === self::$STATUS_REJECTED) {
            $eventName = CoreNotification::NTYPE_GAMIFICATION_REWARD_REJECTED;
        } else {
            $eventName = CoreNotification::NTYPE_GAMIFICATION_REWARD_PENDING;
            $eventFirst = empty($this->date_modified);
        }

        if($eventFirst) {
            Yii::app()->event->raise($eventName, new DEvent($this, array(
                'id_user' => $this->id_user,
                'id_reward' => $this->id_reward,
                'user_reward_request' => $this
            )));
        }
    }

	/**
	 * @return CActiveDataProvider
	 */
	public function dataProvider($idUser = false){
		$search = '';

		if(isset($_REQUEST['show_approved_rejected_requests']) && !empty($_REQUEST['show_approved_rejected_requests'])){
			$showMode = $_REQUEST['show_approved_rejected_requests'];
		}
		if (isset($_REQUEST['search_text']))
			$search = $_REQUEST['search_text'];
		elseif(isset($_REQUEST['term'])){
			$search = $_REQUEST['term'];
		}

		$criteria = new CDbCriteria();
		$criteria->together = true;
		$criteria->with = array(
				'userModel' => array (
						'alias' => 'user',
						'joinType' => 'LEFT JOIN',
				),
				'rewardModel' => array (
						'alias' => 'reward',
						'joinType' 	=> 'LEFT JOIN',
						'with' 		=>	array(
								'gamificationRewardTranslations' => array (
										'alias' => 'rewardTranslations',
										'on' => 'lang_code="'.Lang::getCodeByBrowserCode(Yii::app()->getLanguage()).'"'
								)
						)
				)
		);

		if($showMode) {
			switch ($showMode) {
				case 'pending':
					$criteria->addCondition('t.status = ' . self::$STATUS_PENDING);
					break;
				case 'approved':
					$criteria->addCondition('t.status = ' . self::$STATUS_APPROVED);
					break;
				case 'rejected':
					$criteria->addCondition('t.status = ' . self::$STATUS_REJECTED);
					break;
				case 'canceled':
					$criteria->addCondition('t.status = ' . self::$STATUS_CANCELED);
					break;
			}
		}

		if ($search != '') {
			$criteria->addCondition('REPLACE(REPLACE(REPLACE(REPLACE(t.data, "userid", ""),"firstname",""),"lastname",""),"name","") LIKE :search OR user.userid LIKE :search OR user.firstname LIKE :search OR user.lastname LIKE :search OR rewardTranslations.name LIKE :search OR reward.code LIKE :search');
			$criteria->params[':search'] = '%'.$search.'%';
		}

		if ($idUser) {
			$criteria->addCondition('t.id_user like :id_user');
			$criteria->params[':id_user'] = $idUser;
		}

		$config = array();
		$sortAttributes = array('name'=>'user.userid');
		$config['criteria'] = $criteria;
		$config['countCriteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 't.date_created desc';
		$config['pagination'] = array (
				'pageSize' => Settings::get('elements_per_page', 10)
		);

		return new CActiveDataProvider(get_class($this), $config);
	}


	public function userModelExists(){
		return CoreUser::model()->exists('idst='.$this->id_user);
	}

	public function rewardModelExists(){
		return GamificationReward::model()->exists('id='.$this->id_reward);
	}

	public function getUserModel(){
		return CoreUser::model()->findByPk($this->id_user);
	}
	public function getUserWallet(){
		if($this->userModelExists()){
			return GamificationUserWallet::model()->findByAttributes(array('id_user'=>$this->id_user));
		}
		else
			return false;
	}

	public function getRewardModel(){
		return GamificationReward::model()->findByPk($this->id_reward);
	}


	public function getUsersFullName(){
		if($this->userModelExists()){
			return CoreUser::getFullNameById($this->id_user);
		}else{
			$dataJson = CJSON::decode($this->data);
			return CoreUser::getFullnameByNames($dataJson['firstname'],$dataJson['lastname'],$dataJson['userid']);
		}
	}


	public function renderPicture(){
		$html = '';
		if($this->rewardModelExists())
			$html = GamificationReward::model()->findByPk($this->id_reward)->renderPicture();
		else
			$html = '<img src="'.Yii::app()->baseUrl.'/../themes/spt/images/course/course_nologo.png'.'" style="max-width:30px;max-height:30px;" />';
		return $html;
	}


	public function getRewardName(){
		if($this->rewardModelExists()){
			return GamificationReward::model()->findByPk($this->id_reward)->getName();
		}else{
			$dataJson = CJSON::decode($this->data);
			return $dataJson['name'];
		}
	}



	public function renderRequestedOn($dateOnly = false, $shortTime = false){
		if (!$dateOnly) {
			if (!$shortTime) {
				return Yii::app()->localtime->toLocalDateTime($this->date_created);
			} else {
				return Yii::app()->localtime->toLocalDateTime($this->date_created, LocalTime::DEFAULT_DATE_WIDTH, LocalTime::SHORT);
			}
		} else {
            $result = Yii::app()->localtime->toLocalDateTime($this->date_created);
            $result = explode(' ', $result);
            foreach($result as $i => $temp) {
                if(!preg_match("/\:/", $temp)) {
                    return $temp;
                }
            }
		}
	}


	public function renderStatus(){
		$html = '';
		switch($this->status){
			case self::$STATUS_APPROVED:
				$color = '#5EBE5D';
				$text = Yii::t('gamification','Approved');
				$class = 'approved';
				break;
			case self::$STATUS_REJECTED:
				$color = '#E84C3D';
				$text = Yii::t('gamification','Rejected');
				$class = 'rejected';
				break;
			case self::$STATUS_PENDING:
				$color = '#333333';
				$text = Yii::t('gamification','Pending');
				$class = 'pending';
				break;
			case self::$STATUS_CANCELED:
				$color = '#333333';
				$text = Yii::t('gamification','Canceled');
				$class = 'pending';
				break;
		}
		$html = '<div class="request-status-'.$class.'" style="background-color: '.$color.';padding: 0px 5px 1px; color:white; text-align: center; font-size: 12.5px;font-weight: 900;">'.strtoupper($text).'</div>';
		return $html;
	}


	public function renderEmailAction(){
		$html = '';
		if(/*$this->rewardModelExists() && */$this->userModelExists()) {
			$user =  $this->getUserModel();
			if($user->email) {
				$html = CHtml::link('<i class="fa fa-envelope-o deep_link_option" data-original-title="' . Yii::t('course', 'Send email') . '"></i>', Docebo::createAbsoluteUrl('//GamificationApp/Reward/sendEmail',array('userId'=>$this->id_user, 'requestId'=>$this->id)), array(
						'class' => 'send-email open-dialog',
						'data-dialog-title' => Yii::t('gamification', 'Send Message'),
						'data-dialog-class' => 'modal-send-email',
				));
			}
		}
		return $html;
	}


	public function renderActions(){
		$html = '';
		$btnResetActive = '<i id="reset-'.$this->id.'" class="fa fa-undo rewards-requests-management-action action-reset" data-original-title="'.Yii::t('gamification','Reset status').'" style="cursor:pointer;font-size:18px;"></i>';
		$btnResetInactiveAvailability = '<i id="none-'.$this->id.'" class="fa fa-undo action-inactive-reset" data-original-title="'.Yii::t('gamification','You cannot reset this reward request because there is not enough availability').'" style="cursor:not-allowed;font-size:18px;color:#D2D2D2;"></i>';
		$btnResetInactiveCoins = '<i id="none-'.$this->id.'" class="fa fa-undo action-inactive-reset" data-original-title="'.Yii::t('gamification','You cannot reset this reward request because the user doesn\'t have enough coins').'" style="cursor:not-allowed;font-size:18px;color:#D2D2D2;"></i>';
		$btnResetInactiveRewardMissing = '<i id="none-'.$this->id.'" class="fa fa-undo action-inactive-reset" data-original-title="'.Yii::t('gamification','You cannot reset this request because the reward has been deleted').'" style="cursor:not-allowed;font-size:18px;color:#D2D2D2;"></i>';
		$btnResetInactiveUserMissing = '<i id="none-'.$this->id.'" class="fa fa-undo action-inactive-reset" data-original-title="'.Yii::t('gamification','You cannot reset this request because the user is no more in the system').'" style="cursor:not-allowed;font-size:18px;color:#D2D2D2;"></i>';

		//
			switch($this->status){
				case self::$STATUS_PENDING:
					if($this->rewardModelExists() && $this->userModelExists()) {
						$html = '<i id="approve-' . $this->id . '" class="fa fa-check-circle-o rewards-requests-management-action deep_link_option action-approve" data-original-title="' . Yii::t('gamification', 'Approve request') . '" style="cursor:pointer;color:#5EBE5D;margin-right:6px;"></i><i id="reject-' . $this->id . '" class="fa fa-times-circle-o rewards-requests-management-action deep_link_option action-reject" data-original-title="' . Yii::t('gamification', 'Reject request') . '" style="cursor:pointer;color:#E84C3D;margin-left:6px;"></i>';
					}
					break;
				case self::$STATUS_APPROVED:
					if($this->rewardModelExists() && $this->userModelExists()) {
						$html = $btnResetActive;
					}else{
						if(!$this->rewardModelExists())
							$html = $btnResetInactiveRewardMissing;
						else if(!$this->userModelExists())
							$html = $btnResetInactiveUserMissing;
					}
					break;
				case self::$STATUS_REJECTED:
					$reward = $this->getRewardModel();
					$userWallet = $this->getUserWallet();
					if($this->rewardModelExists() && $this->userModelExists()) {
						if ($reward->availability < 1)
							$html = $btnResetInactiveAvailability;
						else if (!$userWallet || $userWallet->coins < $this->coins)
							$html = $btnResetInactiveCoins;
						else
							$html = $btnResetActive;
					}
					else{
						if(!$this->rewardModelExists())
							$html = $btnResetInactiveRewardMissing;
						else if(!$this->userModelExists())
							$html = $btnResetInactiveUserMissing;
					}
					break;
				case self::$STATUS_CANCELED:
					if(!$this->rewardModelExists())
						$html = $btnResetInactiveRewardMissing;
					else if(!$this->userModelExists())
						$html = $btnResetInactiveUserMissing;
					break;

		}

		return $html;
	}


}
