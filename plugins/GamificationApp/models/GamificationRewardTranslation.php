<?php

/**
 * This is the model class for table "gamification_reward_translation".
 *
 * The followings are the available columns in table 'gamification_reward_translation':
 * @property integer $id
 * @property integer $id_reward
 * @property string $lang_code
 * @property string $name
 * @property string $description
 *
 * The followings are the available model relations:
 * @property GamificationReward $rewardModel
 */
class GamificationRewardTranslation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gamification_reward_translation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_reward, lang_code', 'required'),
			array('id, id_reward', 'numerical', 'integerOnly'=>true),
			array('lang_code', 'length', 'max'=>50),
			array('name', 'length', 'max'=>255),
			array('description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_reward, lang_code, name, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rewardModel' => array(self::BELONGS_TO, 'GamificationReward', 'id_reward'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_reward' => 'Id Reward',
			'lang_code' => 'Lang Code',
			'name' => 'Name',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_reward',$this->id_reward);
		$criteria->compare('lang_code',$this->lang_code,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GamificationRewardTranslation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getModelsList($id_reward = 0)
	{
		$languages = CoreLangLanguage::getActiveLanguages();
		$models = array();

		foreach($languages as $code => $lang)
		{
			if($id_reward === 0)
			{
				$models[$code] = GamificationRewardTranslation::model();
				$models[$code]->name = '';
				$models[$code]->description = '';
				$models[$code]->lang_code = $code;
			}
			else
			{
				$model = GamificationRewardTranslation::findByAttributes(array('id_reward' => $id_reward, 'lang_code' => $code));
				if($model)
					$models[$code] = $model;
				else
				{
					$models[$code] = GamificationRewardTranslation::model();
					$models[$code]->name = '';
					$models[$code]->description = '';
					$models[$code]->lang_code = $code;
				}
			}
		}

		return $models;
	}
}
