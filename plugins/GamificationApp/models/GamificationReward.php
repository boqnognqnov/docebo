<?php

/**
 * This is the model class for table "gamification_reward".
 *
 * The followings are the available columns in table 'gamification_reward':
 * @property integer $id
 * @property string $code
 * @property integer $coins
 * @property integer $availability
 * @property string $picture
 * @property string $association
 * @property string $date_added
 *
 * The followings are the available model relations:
 * @property GamificationAssociatedSets[] $gamificationAssociatedSets
 * @property GamificationRewardTranslation[] $gamificationRewardTranslations
 * @property  GamificationRewardTranslation $defaultTranslation
 * @property GamificationRewardTranslation $translation
 * @property GamificationUserRewards[] $gamificationUserRewards
 */
class GamificationReward extends CActiveRecord
{

	const ASSOCIATION_SELECTION = 'selection';
	const ASSOCIATION_ALL = 'all';
	const ASSOCIATION_NONE = 'none';
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gamification_reward';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, coins, availability', 'numerical', 'integerOnly'=>true),
			array('picture', 'length', 'max'=>2000),
			array('association', 'length', 'max'=>50),
			array('code', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, code, coins, availability, picture, association, date_added', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'gamificationAssociatedSets' => array(self::HAS_MANY, 'GamificationAssociatedSets', 'id_reward'),
			'gamificationRewardTranslations' => array(self::HAS_MANY, 'GamificationRewardTranslation', 'id_reward'),
			'gamificationUserRewards' => array(self::HAS_MANY, 'GamificationUserRewards', 'id_reward'),
			'defaultTranslation' => array(self::HAS_ONE, 'GamificationRewardTranslation', 'id_reward', 'condition' => 'lang_code = "' . CoreLangLanguage::getDefaultLanguage() . '"'),
			'translation' => array(self::HAS_ONE, 'GamificationRewardTranslation', 'id_reward', 'condition'=>'lang_code="'.Lang::getCodeByBrowserCode(Yii::app()->getLanguage()).'"'),
			'englishTranslation' => array(self::HAS_ONE, 'GamificationRewardTranslation', 'id_reward', 'condition' => 'lang_code = "english"'),

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'code' => 'Code',
			'coins' => 'Coins',
			'availability' => 'Availability',
			'picture' => 'Picture',
			'association' => 'Association',
			'date_added' => 'Date Added'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('code',$this->code);
		$criteria->compare('coins',$this->coins);
		$criteria->compare('availability',$this->availability);
		$criteria->compare('picture',$this->picture,true);
		$criteria->compare('association',$this->association,true);
		$criteria->compare('date_added',$this->date_added,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GamificationReward the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function dataProvider(){
		$search = '';

		if (isset($_REQUEST['search_text']))
			$search = $_REQUEST['search_text'];
		elseif(isset($_REQUEST['term'])){
			$search = $_REQUEST['term'];
		}

		$criteria = new CDbCriteria();
		$criteria->together = true;
		$criteria->with = array('gamificationRewardTranslations' => array (
				'alias' => 'rewardTranslations',
				'on' => 'lang_code="'.Lang::getCodeByBrowserCode(Yii::app()->getLanguage()).'"'
		));

		if ($search != '') {
			$criteria->addCondition('rewardTranslations.name like :search');
			$criteria->params[':search'] = '%'.$search.'%';

			$criteria->addCondition('t.code like :code', 'OR');
			$criteria->params[':code'] = '%'.$search.'%';
		}
        
		$config = array();
		$sortAttributes = array('name'=>'rewardTranslations.name');
		$config['criteria'] = $criteria;
		$config['countCriteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 't.id desc';
		$config['pagination'] = array (
				'pageSize' => Settings::get('elements_per_page', 10)
		);

		return new CActiveDataProvider(get_class($this), $config);
	}

	public function getName($useFallback = true){
		if(!empty($this->translation)){
			return $this->translation->name;
		} else{
			if($useFallback){
				return $this->englishTranslation->name;
			} else{
				return $this->defaultTranslation->name;
			}
		}
	}

	public function getCode(){
        return $this->code;
	}

	public function getDescription($useFallback = true){
		if(!empty($this->translation)){
			return $this->translation->description;
		} else{
			if($useFallback){
				return $this->englishTranslation->description;
			} else{
				return $this->defaultTranslation->description;
			}
		}
	}

	public function getPicture(){
		if($this->picture)
			return CoreAsset::url((int) $this->picture);
		else
			return Yii::app()->baseUrl.'/../themes/spt/images/course/course_nologo.png';
	}

	public function renderPicture(){
		$html = '<img src="'.$this->getPicture().'" style="max-width:30px;max-height:30px;padding:3.52px;border:1px solid #e4e6e5;" />';
		return $html;
	}

	/**
	 * @return array of rewards sets models, that the reward is assigned to
	 */
	public function getAllRewardSets(){
		$result = array();
		$rewardSetsRecords = GamificationAssociatedSets::model()->findAllByAttributes(array('id_reward'=>$this->id));
		foreach($rewardSetsRecords as $rewardSetsRecord){
			$result[] = GamificationRewardsSet::model()->findByPk($rewardSetsRecord->id_set);
		}
		return $result;
	}

	/**
	 * Returns rewards sets, that the reward is associated(assigned to) - all sets or selection of sets!!!
	 * @return string
	 */
	public function renderRewardSets(){
		$html = '';
		// Is it assigned to all reward sets
		if($this->association == SELF::ASSOCIATION_ALL) {
			$html = Yii::t('gamification', 'All rewards sets');
		} elseif ($this->association == SELF::ASSOCIATION_NONE) {
            $html = Yii::t('gamification', 'No rewards sets');
        } else {
			// Or is it assigned to a selection of reward sets
			$rewardSets = $this->getAllRewardSets();
			foreach($rewardSets as $rewardSet){
				$html .= $rewardSet->name.';';
			}

			$html = substr($html,0,strlen($html)-1);
		}

		return $html;
	}

	public function renderActions(){
		$html = CHtml::link('<span class="i-sprite is-edit">&nbsp;</span>', Docebo::createAdminUrl('GamificationApp/Reward/edit', array(
				'id' => $this->id,
				'class' => 'reward-action-edit'
		)));
		$html .= CHtml::link('<span class="i-sprite is-remove red">&nbsp;</span>', Docebo::createAdminUrl('GamificationApp/Reward/delete', array(
				'id' => $this->id
		)), array(
				'class' => 'open-dialog reward-action-delete',
                'data-dialog-title' => Yii::t('gamification', 'Delete Reward'),
                'data-dialog-class' => 'delete-reward'
		));

		return $html;
	}

    public function updateStatus($id_reward = false) {
        $request = Yii::app()->request;
        $reward = $id_reward ? $this->findByPk($id_reward) : $this;
        $reward_assoc = GamificationAssociatedSets::model()->findAllByAttributes(array('id_reward' => $id_reward));
        $reward_set = GamificationRewardsSet::model()->findAll();
        $hasSets = !empty($reward_set);

        $count = count($reward_assoc);

        if($reward) {
            if($count == 0 && $reward->association == GamificationReward::ASSOCIATION_SELECTION) {
                $reward->association = GamificationReward::ASSOCIATION_NONE;
            } elseif($count == 0 && $reward->association == GamificationReward::ASSOCIATION_ALL && $hasSets) {
                $reward->association = GamificationReward::ASSOCIATION_ALL;
            } elseif($count == 0 && $reward->association == GamificationReward::ASSOCIATION_ALL && !$hasSets) {
                $reward->association = GamificationReward::ASSOCIATION_NONE;
            }

            $reward->save();
        }
    }

	public function afterDelete(){
		$pendingRequests = GamificationUserRewards::model()->findAllByAttributes(array('id_reward'=>$this->id, 'status'=>GamificationUserRewards::$STATUS_PENDING));

		foreach($pendingRequests as $pendingRequest){
			$pendingRequest->status = GamificationUserRewards::$STATUS_CANCELED;
			$wallet = GamificationUserWallet::model()->findByAttributes(array('id_user'=>$pendingRequest->id_user));

			if($wallet)
				$wallet->coins = $wallet->coins + $pendingRequest->coins;

			$transaction = Yii::app()->db->beginTransaction();
			try {
				if($wallet)
					$wallet->save();

				$pendingRequest->save();
				$transaction->commit();
			} catch (Exception $e) {
				$transaction->rollback();
			}
		}
		parent::afterDelete();
	}
}
