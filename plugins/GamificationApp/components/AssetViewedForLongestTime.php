<?php

/**
 * Class StudentCompletedCourseHandler
 * Handles the student completed LO event
 */
class AssetViewedForLongestTime extends CComponent implements GamificationEventHandler
{
    /**
     * Renders the view for the settings page
     * @param $params
     */
    public function renderEventSettingsView($params) {
        // Render the _setting_course view
        $params['handler'] = $this;
        return Yii::app()->controller->renderPartial('_App7020AssetViewedForLongestTime', $params, true);
    }

    /**
     * Checks whether this badge is to be assigned and returns event logging info
     * @param $badge GamificationBadge
     * @param $event_config array
     * @param $event_params array
     */
    public function shouldAssignBadge($badge, $event_config, $event_params) {
        $result = array('action' => false);

        // Fetch event params
        $user = $event_params['user']; /* @var $user CoreUser */
        $idContent = $event_params['id_content']; /* @var $idContent */
        $new_time = $event_params['new_time']; /* @var $new_time */

        $assigned = GamificationAssignedBadges::model()->findByAttributes(array('id_badge'=>$badge->id_badge, 'id_user'=>$user));
        $assigned_event_params = json_decode($assigned->event_params);

        $time = $event_config['hours']*3600 + $event_config['minutes']*60;

        if($new_time[0] >= $time && !$assigned){
            $result['action'] = true;
        }

        $result_event_key = 'Asset viewed for longest time';
        $result_event_params = array(
            'limit'=>$new_time[0]
        );

        // If badge is to be assigned, fill in logging info
        if($result['action']) {
            $result['id_user'] = $user;
            $result['event_module'] = 'gamification';
            $result['event_key'] = $result_event_key;
            $result['event_params'] = $result_event_params;
        }

        return $result;

    }
}