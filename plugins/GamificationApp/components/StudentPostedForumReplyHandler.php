<?php

/**
 * Class StudentPostedForumReplyHandler
 * Handles the student posted reply event
 */
class StudentPostedForumReplyHandler extends CComponent implements GamificationEventHandler
{
    /**
     * Renders the view for the settings page
     * @param $params
     */
    public function renderEventSettingsView($params)
    {
        if(isset($params['params']['courses']) && !empty($params['params']['courses']))
        {
            $criteria = new CDbCriteria();
            $criteria->select = 'idCourse, code, name';
            $criteria->addInCondition('idCourse', $params['params']['courses']);
            $params['courses'] = LearningCourse::model()->findAll($criteria);
        }
        else
            $params['courses'] = array();

		$totalCourses = Yii::app()->db->createCommand('SELECT COUNT(*) FROM '.LearningCourse::model()->tableName())->queryScalar();
		$params['totalCourses'] = $totalCourses;

        // Render the _setting_course view
        $params['handler'] = $this;
        return Yii::app()->controller->renderPartial('_event_setting_forum_reply', $params, true);
    }

    /**
     * Checks whether this badge is to be assigned and returns event logging info
     * @param $badge GamificationBadge
     * @param $event_config array
     * @param $event_params array
     */
    public function shouldAssignBadge($badge, $event_config, $event_params) {
        $result = array('action' => false);

        // Fetch event params
        $course = $event_params['course']; /* @var $course LearningCourse */
        $user = $event_params['user']; /* @var $user CoreUser */
        $thread = $event_params['thread']; /* @var $thread LearningForumthread */

        // Check course association mode
        $limit = (int) $event_config['limit'];
        $badgeDate = false;
        if(isset($event_config['date_created']) && !empty($event_config['date_created'])){
            $badgeDate = $event_config['date_created'];
        }
        switch($event_config['courses_association']) {
            case 'all':
                if(!$limit || ($this->countForumMessagesForUser($user, array(), $badgeDate) == $limit))
                    $result['action'] = true;
                break;
            case 'selection':
                $selected_courses = isset($event_config['courses']) ? $event_config['courses'] : array();
                if(in_array($course->idCourse, $selected_courses)) {
                    if(!$limit || ($this->countForumMessagesForUser($user, $selected_courses, $badgeDate) == $limit))
                        $result['action'] = true;
                }
                break;
        }

        // If badge is to be assigned, fill in logging info
        if($result['action']) {
            $result['id_user'] = $user->idst;
            $result['event_module'] = 'gamification';
            $result['event_key'] = 'Student has posted a forum reply in discussion "{discussion}" for course "{course_name}"';
            $result['event_params'] = array(0 => array(
                '{course_name}' => $course->name,
                '{discussion}' => $thread->title
            ));
        }

        return $result;
    }

    /**
     * Returns the number of forum replies left by the user in any course or in a specific set of courses
     *
     * @param $user CoreUser
     * @param $courseIds array
     * @param $from_date string|boolean
     *
     * @return int
     */
    private function countForumMessagesForUser($user, $courseIds = array(), $from_date = false) {
        $sql = 'SELECT COUNT(DISTINCT idMessage) FROM '.LearningForummessage::model()->tableName().' as msg
				INNER JOIN '.LearningForumthread::model()->tableName().' AS thread ON msg.`idThread` = thread.`idThread`
				INNER JOIN '.LearningForum::model()->tableName().' AS forum ON thread.idForum = forum.idForum
				WHERE msg.author = '.(int)$user->idst;

        if(!empty($courseIds))
            $sql .= ' AND forum.idCourse IN ('.implode(',', $courseIds).')';

        //Filter messages that are posted AFTER badge was created
        if($from_date !== false){
            $sql .= ' AND msg.posted > "' . $from_date . '"';
        }

        $res = Yii::app()->db->createCommand($sql)->queryScalar();

        return $res ? $res : 0;
    }

}