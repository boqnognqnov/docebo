<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 3.12.2015 г.
 * Time: 10:21
 */
class GoalJobHandler extends JobHandler implements IJobHandler
{

    const HANDLER_ID = 'plugin.GamificationApp.components.GoalJobHandler';

    const JOB_NAME = 'GamificationGoal';

    /**
     * Initialize the job handler component
     * @param array $params Array of arbitrary parameters
     */
    public function init($params = false)
    {
        // TODO: Implement init() method.
    }

    /**
     * Run the Job the handler is associated with (during creation)
     *
     * Called in a try/catch block, i.e. all exceptions thrown will be catched.
     * If the method ends with NO exception thrown, job is considered done with success.
     *
     * @param array $params Array of arbitrary parameters
     * @return void
     */
    public function run($params = false)
    {
        $job = $this->job;
        $jobParams = json_decode($job->params, true);

        if(!isset($jobParams['contest_id'])){
            Yii::app()->scheduler->deleteJob($this->job, true);
            throw new Exception('Goal handler called with invalid contest');
        }

        $contest = GamificationContest::model()->findByPk($jobParams['contest_id']);

        $localNow = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');

        $contestFromDate = GamificationContest::convertFromContestTimezoneToUTC($contest->from_date, $contest->timezone);


        if(Yii::app()->localtime->isGte($localNow, $contestFromDate)){
            if($jobParams['firstRun'] === true){
                Yii::app()->event->raise(CoreNotification::NTYPE_GAMIFICATION_CONTEST_START, new DEvent($this, array(
                    'id_contest' => $contest->id,
                )));
                $jobParams['firstRun'] = false;
            }

            /**
             * @var $contest GamificationContest
             */

            if(!$contest){
                Yii::app()->scheduler->deleteJob($this->job, true);
                throw new Exception('Could not find contest with id: ' . $jobParams['contest_id']);
            }

            Yii::log("Its time to calculating the results for contest {$contest->id}", 'debug');

            if($this->handleGoal($contest)){
                Yii::log('Contest finished successfully! Now issuing the rewards', 'debug');
                Yii::app()->scheduler->deleteJob($this->job, true);
            } else{
                Yii::log("Temporary results for the contest are generated!", 'debug');
            }

            $this->job->params = json_encode($jobParams);
            $this->job->save();
        } else{
            Yii::log("It's not time for contest {$contest->id}", 'debug');
        }
    }

    protected function handleGoal(GamificationContest $contest){
        $goalHandler = $contest->goal;

        $handler = new $goalHandler($contest->id);

        if($handler instanceof IGamificationGoalHandler){

            /**
             * @var $handler IGamificationGoalHandler
             */

            $handler->collectData();
            $issueRewards = false;
            $now = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
            $contestToDate = GamificationContest::convertFromContestTimezoneToUTC($contest->to_date, $contest->timezone);
            if(Yii::app()->localtime->isGt($now, $contestToDate)){
                $issueRewards = true;
            }

            if($handler->finishContest($issueRewards)){
                return true;
            } else{
                return false;
            }
        }
    }
   
    /**
     * Returns string identifier of the handler CLASS (handler id).
     * Must be unique LMS-wide, including among plugins and core handlers.
     *
     * Handler ID can be:
     *        1) Arbitrary/Random string, in which case, JobHandler::registerHandler(<handler-id>, <class-path>) must be used to
     *           map the ID to a class path run-time.
     *                Example (in plugin module init()):
     *                    JobHandler::registerHandler('myown_handler_id', 'plugins.my_plugin.MyJobHandler');
     *
     *    2) Class path (string), e.g. plugins.my_plugin.MyJobHandler, in which case the MyJobHandler.php will be used, no need to register.
     *
     * Handler ID is associated with jobs in the database, so it is the connection between Jobs and Handlers.
     *
     * @see ExampleJobHandler
     *
     * @param array $params Array of arbitrary parameters
     * @return string
     */
    public static function id($params = false)
    {
        return self::HANDLER_ID;
    }
}