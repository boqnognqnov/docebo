<?php

/**
 * Class StudentCompletedCourseHandler
 * Handles the student completed LO event
 */
class AssetReachedAGoal extends CComponent implements GamificationEventHandler
{
    /**
     * Renders the view for the settings page
     * @param $params
     */
	 public static $ASSET_RATED_FIVE_STARS = 0;
	 public static $ASSET_RECEIVES_VIEW = 1;
    public function renderEventSettingsView($params) {
        // Render the _setting_course view
        // Render the _setting_course view
		$goals = array(
			self::$ASSET_RATED_FIVE_STARS => "User's asset rated with five stars",
			self::$ASSET_RECEIVES_VIEW => "User's asset viewed"
		);
        $params['handler'] = $this;
		$params['goals'] = $goals;
        return Yii::app()->controller->renderPartial('_App7020AssetReachedAGoal', $params, true);
    }

    /**
     * Checks whether this badge is to be assigned and returns event logging info
     * @param $badge GamificationBadge
     * @param $event_config array
     * @param $event_params array
     */
    public function shouldAssignBadge($badge, $event_config, $event_params) {
        $result = array('action' => false);

        // Fetch event params
        $goal = $event_params['goal']; /* @var $lo LearningOrganization */
        $asset = $event_params['contentId']; /* @var $user CoreUser */


        $latestParams =  GamificationAssignedBadges::getLatestAssignedBadgeParams($event_params['userId'],$badge->id_badge);
        if(!count($latestParams) > 0){
            $latestParams['limit']= 0;
        }

        // $goal cases
        switch ($goal) {
            case self::$ASSET_RATED_FIVE_STARS:
				if($event_config["goal"] == self::$ASSET_RATED_FIVE_STARS) {
                $result_event_key = 'Asset rated with five stars';
				$countFiveStars = App7020ContentRating::model()->findAllByAttributes(array('idContent'=>$asset , 'rating' => 5),array('group'=>'idUser'));
                    $limit = count($countFiveStars);

                    if($event_config['limit'] > 0 ) {
                        if (count($countFiveStars) % $event_config["limit"] == 0 && $latestParams['limit'] < count($countFiveStars)) {
                            $result['action'] = true;
                        }
                    }
				}
                break;
            case self::$ASSET_RECEIVES_VIEW:
				if($event_config["goal"] == self::$ASSET_RECEIVES_VIEW) {
				$result_event_key = 'Asset receives view';	
				$countViews = App7020ContentHistory::model()->findAllByAttributes(array('idContent'=>$asset),array('group'=>'idUser'));
                    $limit = count($countViews);
                    if($event_config['limit'] > 0 ) {
                        if (count($countViews) % $event_config["limit"] == 0  && $latestParams['limit'] < count($countViews)) {
                            $result['action'] = true;
                        }
                    }
				}
                break;
		}
        // If badge is to be assigned, fill in logging info
        if($result['action']) {
            $result['id_user'] = $event_params['userId'];
            $result['event_module'] = 'gamification';
            $result['event_key'] = $result_event_key;
            $result['event_params'] = array(0 => array(
                'limit' => $limit,
                'goal' => $event_config["goal"]
            ));
        }

        return $result;
    }

    /**
     * Returns true if passed object has score assigned
     * @param $lo LearningOrganization
     */
    public function isObjectWithScore($lo) {
        return in_array($lo->objectType, array(LearningOrganization::OBJECT_TYPE_TEST, LearningOrganization::OBJECT_TYPE_DELIVERABLE,
            LearningOrganization::OBJECT_TYPE_SCORMORG, LearningOrganization::OBJECT_TYPE_TINCAN));
    }

    /** Returns the json array of learning objects
     * @param $objects array
     */
    public function getObjectsAsJson($objects) {
        $list = array();
        foreach($objects as $object) {
            $list[$object->idOrg] = $this->isObjectWithScore($object);
        }
        return CJSON::encode($list);
    }
}