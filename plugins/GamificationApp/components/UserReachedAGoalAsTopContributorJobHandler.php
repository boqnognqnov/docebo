<?php

class UserReachedAGoalAsTopContributorJobHandler extends JobHandler implements IJobHandler
{

	const HANDLER_ID = 'plugin.GamificationApp.components.UserReachedAGoalAsTopContributorJobHandler';

	const JOB_NAME = 'UserTopContributorGoal';


	/**
	 * Initialize the job handler component
	 * @param array $params Array of arbitrary parameters
	 */
	public function init($params = false)
	{
		// TODO: Implement init() method.
	}


	public function run($params = false)
	{
		$job = $this->job;
		$jobParams = json_decode($job->params, true);
		if (!isset($jobParams['badge_id'])) {
			Yii::app()->scheduler->deleteJob($this->job, true);
			throw new Exception('Goal handler called with invalid badge');
		}

		$badge = GamificationBadge::model()->findByPk($jobParams['badge_id']);

		if (!$badge) {
			Yii::app()->scheduler->deleteJob($this->job, true);
			throw new Exception('Could not find badge with id: ' . $jobParams['badge_id']);
		}

		switch ($jobParams['goal']) {
			case 0:
				$sql = "SELECT
                        idst,
                        count(*) as count_content
                        FROM core_user as cu
                        JOIN app7020_content as content
                        WHERE content.userId = cu.idst
                        AND content.created BETWEEN (NOW() - INTERVAL 1 DAY) AND NOW()
                        GROUP BY idst
                        ORDER BY count_content DESC
                        LIMIT 1
                    ";
				break;
			case 1:
				$sql = "SELECT
                        idst,
                        count(*) as count_content
                        FROM core_user as cu
                        JOIN app7020_content as content
                        WHERE content.userId = cu.idst
                        AND content.created BETWEEN (NOW() - INTERVAL 1 WEEK) AND NOW()
                        GROUP BY idst
                        ORDER BY count_content DESC
                        LIMIT 1
                    ";
				break;
			case 2:
				$sql = "SELECT
                        idst,
                        count(*) as count_content
                        FROM core_user as cu
                        JOIN app7020_content as content
                        WHERE content.userId = cu.idst
                        AND content.created BETWEEN (NOW() - INTERVAL 1 MONTH) AND NOW()
                        GROUP BY idst
                        ORDER BY count_content DESC
                        LIMIT 1
                    ";
				break;
		}
		/**
		 * Returns array
		 * [0] - > user id
		 * [1] - > count of uploaded assets for set time interval
		 * recurring
		 */
		$result = Yii::app()->db->createCommand($sql)->queryAll(false);

		if ($result) {
			Yii::app()->event->raise(GamificationAppModule::$EVENT_APP7020_USER_REACHED_A_GOAL_AS_TOP_CONTRIBUTOR, new DEvent($this, array('goal' => $jobParams['goal'], 'user' => $result[0][0])));
		}

	}

	/**
	 * Returns string identifier of the handler CLASS (handler id).
	 * Must be unique LMS-wide, including among plugins and core handlers.
	 *
	 * Handler ID can be:
	 *        1) Arbitrary/Random string, in which case, JobHandler::registerHandler(<handler-id>, <class-path>) must be used to
	 *           map the ID to a class path run-time.
	 *                Example (in plugin module init()):
	 *                    JobHandler::registerHandler('myown_handler_id', 'plugins.my_plugin.MyJobHandler');
	 *
	 *    2) Class path (string), e.g. plugins.my_plugin.MyJobHandler, in which case the MyJobHandler.php will be used, no need to register.
	 *
	 * Handler ID is associated with jobs in the database, so it is the connection between Jobs and Handlers.
	 *
	 * @see ExampleJobHandler
	 *
	 * @param array $params Array of arbitrary parameters
	 * @return string
	 */
	public static function id($params = false)
	{
		return self::HANDLER_ID;
	}
}