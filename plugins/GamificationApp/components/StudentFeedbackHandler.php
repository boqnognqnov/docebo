<?php

/**
 * Generic super class for feedback handlers
 * Class StudentFeedbackHandler
 */
abstract class StudentFeedbackHandler extends CComponent {

    /**
     * Fills in common params for feedback handler
     */
    protected function collectFeedbackParams(&$params) {

        // Generic "forum reply" target type
        $params['targets'][] = array(
            'value' => 'forum_reply',
            'label' => Yii::t('gamification', 'Forum reply')
        );

        // Add Blog-specific targets
        if(PluginManager::isPluginActive("MyBlogApp")) {
            $params['targets'][] = array(
                'value' => 'post',
                'label' => Yii::t('myblog', 'Blog Post')
            );

            $params['targets'][] = array(
                'value' => 'comment',
                'label' => Yii::t('myblog', 'Blog Comment')
            );
        }

        // Setting feedback defaults
        $params['feedback_values'] = $params['params']['feedback_values'];
        if(!isset($params['feedback_values']) || !is_array($params['feedback_values']))
            $params['feedback_values'] = array();

        // Check if values are allowed for the current type of feedback
        if($this instanceof StudentClickedHelpfulHandler)
            $params['feedback_values'] = array_intersect($params['feedback_values'], array(0,1));
        else
            $params['feedback_values'] = array_intersect($params['feedback_values'], array(1,2,3,4,5));

        // Values should not be empty -> default 1
        if(empty($params['feedback_values']))
            $params['feedback_values'][] = 1;
    }

    /**
     * Returns true if badge configuration options match the current event
     */
    protected function getBadgeResult($event_params, $event_config) {
        $result = false;

        // Fetch event params
        $value = $event_params['value'];
        $evaluator_user = $event_params['evaluator_user']; /* @var $evaluator_user CoreUser */
        $evaluated_user = $event_params['evaluated_user']; /* @var $evaluated_user CoreUser */
        $target_type = $event_params['target_type'];
        $target_element = $event_params['target_element'];

        // Check target type && feedback values
        if(!isset($event_config['target']) || !is_array($event_config['target']))
            $event_config['target'] = array();
        if(!isset($event_config['feedback_values']) || !is_array($event_config['feedback_values']))
            $event_config['feedback_values'] = array();

        if(in_array($target_type, $event_config['target']) && in_array($value, $event_config['feedback_values'])) {
            // Should we check for threashold?
            if(($event_config['action'] == 'receive_feedback') && ($event_config['use_threashold'] == 1) && ($event_config['threashold'] > 0)) {
                $model = null;

                // Filter using the feedback values and the type of feedback (helpful, set the tone)
                $attributes = array(
                    'value' => $event_config['feedback_values'], // IN condition
                    'type' => ($this instanceof StudentClickedHelpfulHandler) ? 'helpful' : 'tone'
                );

                // Find the right model...
                switch ($target_type) {
                    case 'post':
                        $model = MyblogPostVote::model();
                        if ($event_config['threashold_type'] == 'same_item')
                            $attributes['id_post'] = $target_element->id_post;
                        break;
                    case 'comment':
                        $model = MyblogCommentVote::model();
                        if ($event_config['threashold_type'] == 'same_item')
                            $attributes['id_comment'] = $target_element->id_comment;
                        break;
                    case 'forum_reply':
                        $model = LearningForummessageVotes::model();
                        if ($event_config['threashold_type'] == 'same_item')
                            $attributes['id_message'] = $target_element->idMessage;
                        break;
                }

                // ... do the counting
                if($model) {
                    $count = $model->countByAttributes($attributes);
                    if (($count+1) == $event_config['threashold']) // Is this the feedback that reaches the threashold?
                        $result = true;
                }
            } else
                $result = true;
        }

        return $result;
    }
} 