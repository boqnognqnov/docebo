<?php

/**
 * Class StudentCompletedCourseHandler
 * Handles the student completed LO event
 */
 class UserReachedAGoalAsTopContributor extends CComponent implements GamificationEventHandler

{
    public static $EVENT_TOP_DAILY = 0;
    public static $EVENT_TOP_WEEKLY = 1;
    public static $EVENT_TOP_MONTHLY = 2;

    const HANDLER_ID = 'plugin.GamificationApp.components.UserReachedAGoalAsTopContributor';

    const JOB_NAME = 'UserTopContributorGoal';

    /**
     * Renders the view for the settings page
     * @param $params
     */
    public function renderEventSettingsView($params) {
        // Render the _setting_course view
		$goals = array(
            self::$EVENT_TOP_DAILY=>"Daily",
            self::$EVENT_TOP_WEEKLY=>"Weekly",
            self::$EVENT_TOP_MONTHLY=>"Monthly"
		);
        $params['handler'] = $this;
		$params['events'] = $goals;
        return Yii::app()->controller->renderPartial('_App7020UserReachedAGoalAsTopContributor', $params, true);
    }

    /**
     * Checks whether this badge is to be assigned and returns event logging info
     * @param $badge GamificationBadge
     * @param $event_config array
     * @param $event_params array
     */
    public function shouldAssignBadge($badge, $event_config, $event_params) {
        $result = array('action' => false);

        // Fetch event params
        $goal = $event_params['goal'];
        $user = $event_params['user']; /* @var $user CoreUser */


        switch ($goal) {
            case self::$EVENT_TOP_DAILY:
                $result_event_key = 'User is top contributor for {date}';
                $result_event_params = array(
                    0=>array(
                        'date' => Yii::app()->localtime->getUTCNow(),
                    )
                );
                if($event_config['goal'] == self::$EVENT_TOP_DAILY){
                    $result['action'] = true;
                }
                break;
            case self::$EVENT_TOP_WEEKLY:

                $result_event_key = 'User is top contributor for {week} ';
                $result_event_params = array(
                    0=>array(
                        'week' => Yii::app()->localtime->getUTCNow(),
                    )
                );

                if($event_config['goal'] == self::$EVENT_TOP_WEEKLY){
                    $result['action'] = true;
                }
                break;
            case self::$EVENT_TOP_MONTHLY:
                $result_event_key = 'User is top contributor for {month}';
                $result_event_params = array(
                    0=>array(
                        'month' => Yii::app()->localtime->getUTCNow(),
                    )
                );

                if($event_config['goal'] == self::$EVENT_TOP_MONTHLY){
                    $result['action'] = true;
                }
                break;



        }



        // If badge is to be assigned, fill in logging info
        if($result['action']) {
            $result['id_user'] = $user;
            $result['event_module'] = 'gamification';
            $result['event_key'] = $result_event_key;
            $result['event_params'] = $result_event_params;
        }



        return $result;
    }
}