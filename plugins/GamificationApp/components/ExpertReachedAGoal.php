<?php

/**
 * Class StudentCompletedCourseHandler
 * Handles the student completed LO event
 */
class ExpertReachedAGoal extends CComponent implements GamificationEventHandler {

	/**
	 * Renders the view for the settings page
	 * @param $params
	 */
	public static $EXPERT_SATISFIES_REQUEST = 0;
	public static $EXPERT_ADDS_REVIEW = 1;
	public static $EXPERT_APPROVES_ASSET = 2;

	public function renderEventSettingsView($params) {
		// Render the _setting_course view
		// Render the _setting_course view
		$goals = array(
			self::$EXPERT_SATISFIES_REQUEST => "Expert satisfies request",
			self::$EXPERT_ADDS_REVIEW => "Expert adds peer review",
			self::$EXPERT_APPROVES_ASSET => "Expert approves asset",
		);
		$params['handler'] = $this;
		$params['goals'] = $goals;
		return Yii::app()->controller->renderPartial('_App7020ExpertReachedAGoal', $params, true);
	}

	/**
	 * Checks whether this badge is to be assigned and returns event logging info
	 * @param $badge GamificationBadge
	 * @param $event_config array
	 * @param $event_params array
	 */
	public function shouldAssignBadge($badge, $event_config, $event_params) {
		$result = array('action' => false);

		// Fetch event params
		$goal = $event_params['goal']; /* @var $lo LearningOrganization */
		$expertId = $event_params['userId']; /* @var $user CoreUser */
		$is_expert = App7020Experts::model()->findByAttributes(array('idUser'=>$expertId));

		$latestParams =  GamificationAssignedBadges::getLatestAssignedBadgeParams($expertId,$badge->id_badge);
		if(!count($latestParams) > 0){
			$latestParams['limit']= 0;
		}

		// $goal cases
		switch ($goal) {
			case self::$EXPERT_SATISFIES_REQUEST:
				if ($event_config["goal"] == self::$EXPERT_SATISFIES_REQUEST) {
					$result_event_key = 'Expert satisfies request';
					$Criteria = new CDbCriteria();
					$Criteria->condition = "idExpert = ".$expertId;
					$Criteria->condition = "idContent IS NOT NULL";
					$countSatisfiedRequests = App7020QuestionRequest::model()->findAll($Criteria);
					$limit = count($countSatisfiedRequests);
					if($event_config['limit'] > 0 ) {
						if (count($countSatisfiedRequests) % $event_config["limit"] == 0 && $is_expert && $latestParams['limit'] < count($countSatisfiedRequests)) {
							$result['action'] = true;
						}
					}
				}
				break;
			case self::$EXPERT_ADDS_REVIEW:
				if ($event_config["goal"] == self::$EXPERT_ADDS_REVIEW) {
					$result_event_key = 'Expert adds peer review';
					$countReviews = App7020ContentReview::model()->findAllByAttributes(array('idUser' => $expertId));
					$limit = count($countReviews);
					if($event_config['limit'] > 0 ) {
						if (count($countReviews) % $event_config["limit"] == 0 && $is_expert && $latestParams['limit'] < count($countReviews)) {
							$result['action'] = true;
						}
					}
				}
				break;
			case self::$EXPERT_APPROVES_ASSET:
				if ($event_config["goal"] == self::$EXPERT_APPROVES_ASSET) {
					$result_event_key = 'Expert approves asset';
					$countApproves = App7020ContentPublished::model()->findAllByAttributes(array('idUser' => $expertId , 'actionType'=> App7020ContentPublished::ACTION_TYPE_PUBLISH));
					$limit = count($countApproves);
					if($event_config['limit'] > 0 ) {
						if (count($countApproves) % $event_config["limit"] == 0 && $is_expert && $latestParams['limit'] < count($countApproves)) {
							$result['action'] = true;
						}
					}
				}
				break;
		}
		// If badge is to be assigned, fill in logging info
		if ($result['action']) {
			$result['id_user'] = $event_params['userId'];
			$result['event_module'] = 'gamification';
			$result['event_key'] = $result_event_key;
			$result['event_params'] = array(0 => array(
					'limit' => $limit,
					'goal' => $event_config["goal"]
			));
		}

		return $result;
	}

	/**
	 * Returns true if passed object has score assigned
	 * @param $lo LearningOrganization
	 */
	public function isObjectWithScore($lo) {
		return in_array($lo->objectType, array(LearningOrganization::OBJECT_TYPE_TEST, LearningOrganization::OBJECT_TYPE_DELIVERABLE,
			LearningOrganization::OBJECT_TYPE_SCORMORG, LearningOrganization::OBJECT_TYPE_TINCAN));
	}

	/** Returns the json array of learning objects
	 * @param $objects array
	 */
	public function getObjectsAsJson($objects) {
		$list = array();
		foreach ($objects as $object) {
			$list[$object->idOrg] = $this->isObjectWithScore($object);
		}
		return CJSON::encode($list);
	}

}
