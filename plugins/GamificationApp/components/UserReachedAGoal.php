<?php

/**
 * Class StudentCompletedCourseHandler
 * Handles the student completed LO event
 */
class UserReachedAGoal extends CComponent implements GamificationEventHandler
{

    public static $EVENT_ADDS_QUESTION = 0;
    public static $EVENT_RECEIVES_LIKE = 1;
    public static $EVENT_RECEIVES_DISLIKE = 2;
    public static $EVENT_BEST_ANSWER = 3;
    public static $EVENT_INVITES_PEOPLE = 4;
    public static $EVENT_UPLOADS_ASSET = 5;

    /**
     * Renders the view for the settings page
     * @param $params
     */
    public function renderEventSettingsView($params) {
        // Render the _setting_course view
		$goals = array(
			self::$EVENT_ADDS_QUESTION=>"User made a question",
			self::$EVENT_RECEIVES_LIKE=>"User's answer received like",
			self::$EVENT_RECEIVES_DISLIKE=>"User's answer received dislike",
			self::$EVENT_BEST_ANSWER=>"User's answer marked as Best Answer",
			self::$EVENT_INVITES_PEOPLE=>"User invited people to watch an asset",
			self::$EVENT_UPLOADS_ASSET=>"User uploaded asset"
		);
        $params['handler'] = $this;
		$params['goals'] = $goals;
        return Yii::app()->controller->renderPartial('_App7020UserReachedAGoal', $params, true);
    }

    /**
     * Checks whether this badge is to be assigned and returns event logging info
     * @param $badge GamificationBadge
     * @param $event_config array
     * @param $event_params array
     */
    public function shouldAssignBadge($badge, $event_config, $event_params) {
        $result = array('action' => false);

        // Fetch event params
        $goal = $event_params['goal']; 
        $user = $event_params['user']; /* @var $user CoreUser */


        $latestParams =  GamificationAssignedBadges::getLatestAssignedBadgeParams($user,$badge->id_badge);
        if(!count($latestParams) > 0){
            $latestParams['limit']= 0;
        }

        switch ($goal) {
            case self::$EVENT_ADDS_QUESTION:
                $questions = App7020Question::model()->findAllByAttributes(array('idUser'=>$event_params['user']));
                $result_event_key = 'User has published "{limit}" questions';
                $result_event_params = array(
                    0=>array(
                        'limit' => count($questions),
                    )
                );
                if($event_config['limit'] > 0 ) {
                    if (count($questions) % $event_config['limit'] == 0 && $event_config['goal'] == self::$EVENT_ADDS_QUESTION && $latestParams['limit'] < count($questions)) {
                        $result['action'] = true;

                    }
                }
                break;
            case self::$EVENT_RECEIVES_LIKE:
                $currentUser = $event_params['user'];
                $commandBase = Yii::app()->db->createCommand();
                $commandBase->select("
                    idQuestion,
                    SUM(CASE WHEN al.type = " . App7020AnswerLike::TYPE_LIKE . " THEN 1 ELSE 0 END) count_likes
                ");
                $commandBase->from(App7020Answer::model()->tableName() . ' a');
                $commandBase->leftJoin(App7020AnswerLike::model()->tableName() . ' al', 'a.id = al.idAnswer');
                $commandBase->andWhere("a.idUser = " . $currentUser);
                $commandBase->group('a.idQuestion');
                $resultArray = $commandBase->queryAll(true);


                $likes = App7020Helpers::arrSumByKey($resultArray, 'count_likes');
                $result_event_key = 'User has recived "{limit}" likes';
                $result_event_params = array(
                    0=>array(
                        'limit' => $likes,
                    )
                );
                if($event_config['limit'] > 0 ) {
                    if ($likes % $event_config['limit'] == 0 && $event_config['goal'] == self::$EVENT_RECEIVES_LIKE  && $latestParams['limit'] < $likes) {
                        $result['action'] = true;

                    }
                }
                break;
            case self::$EVENT_RECEIVES_DISLIKE:
                $currentUser = $event_params['user'];
                $commandBase = Yii::app()->db->createCommand();
                $commandBase->select("
                    idQuestion,
                    SUM(CASE WHEN al.type = " . App7020AnswerLike::TYPE_DISLIKE . " THEN 1 ELSE 0 END) count_dislikes
                ");
                $commandBase->from(App7020Answer::model()->tableName() . ' a');
                $commandBase->leftJoin(App7020AnswerLike::model()->tableName() . ' al', 'a.id = al.idAnswer');
                $commandBase->andWhere("a.idUser = " . $currentUser);
                $commandBase->group('a.idQuestion');
                $resultArray = $commandBase->queryAll(true);


                $dislikes = App7020Helpers::arrSumByKey($resultArray, 'count_dislikes');
                $result_event_key = 'User has recived "{limit}" dislikes';
                $result_event_params = array(
                    0=>array(
                        'limit' => $dislikes,
                    )
                );
                if($event_config['limit'] > 0 ) {
                    if ($dislikes % $event_config['limit'] == 0 && $event_config['goal'] == self::$EVENT_RECEIVES_DISLIKE  && $latestParams['limit'] < $dislikes) {
                        $result['action'] = true;

                    }
                }
                break;
            case self::$EVENT_BEST_ANSWER:
                $currentUser = $event_params['user'];
                $commandBase = Yii::app()->db->createCommand();
                $commandBase->select("
                    idQuestion,
                    SUM(CASE WHEN a.bestAnswer = " . App7020Answer::BEST_ANSWER . " THEN 1 ELSE 0 END) count_bestAnswers
                ");
                $commandBase->from(App7020Answer::model()->tableName() . ' a');
                $commandBase->leftJoin(App7020AnswerLike::model()->tableName() . ' al', 'a.id = al.idAnswer');
                $commandBase->andWhere("a.idUser = " . $currentUser);
                $commandBase->group('a.idQuestion');
                $resultArray = $commandBase->queryAll(true);

                $answer = App7020Helpers::arrSumByKey($resultArray, 'count_bestAnswers');
                $result_event_key = 'User has recived best answer status for "{limit}" answers';
                $result_event_params = array(
                    0=>array(
                        'limit' => $answer,
                    )
                );
                if($event_config['limit'] > 0 ) {
                    if ($answer % $event_config['limit'] == 0 && $event_config['goal'] == self::$EVENT_BEST_ANSWER  && $latestParams['limit'] < $answer) {
                        $result['action'] = true;

                    }
                }
                break;
            case self::$EVENT_INVITES_PEOPLE:
                $inv = $event_params['inv'];
                $result_event_key = 'User has send "{limit}" invitations';
                $result_event_params = array(
                    0=>array(
                        'limit' => $inv,
                    )
                );
                if($event_config['limit'] > 0 ) {
                    if ($inv % $event_config['limit'] == 0 && $event_config['goal'] == self::$EVENT_INVITES_PEOPLE  && $latestParams['limit'] < $inv) {
                        $result['action'] = true;

                    }
                }
                break;
            case self::$EVENT_UPLOADS_ASSET:
                $assets = $event_params['asset_count'];
                $result_event_key = 'User has uploaded "{limit}" assets';
                $result_event_params = array(
                    0=>array(
                        'limit' => $assets,
                    )
                );
                if($event_config['limit'] > 0 ) {
                    if($assets % $event_config['limit'] == 0 && $event_config['goal'] == self::$EVENT_UPLOADS_ASSET  && $latestParams['limit'] < $assets){
                        $result['action'] = true;

                    }
                }
                break;
        }



        // If badge is to be assigned, fill in logging info
        if($result['action']) {
            $result['id_user'] = $user;
            $result['event_module'] = 'gamification';
            $result['event_key'] = $result_event_key;
            $result['event_params'] = $result_event_params;
        }

        return $result;
    }
}