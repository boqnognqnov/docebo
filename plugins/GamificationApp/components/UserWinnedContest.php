<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 7.12.2015 г.
 * Time: 12:14
 */
class UserWinnedContest extends CComponent implements GamificationEventHandler
{

    public function renderEventSettingsView($params)
    {
        return false;
    }

    public function shouldAssignBadge($badge, $event_config, $event_params)
    {
        $rewardData = $event_params['reward_params'];
        $result = array('action' => false);

        $userId = $event_params['id_user'];

        $userModel = CoreUser::model()->findByPk($userId);

        if(!$userModel){
            return $result;
        }

        $badge = GamificationBadge::model()->findByPk($rewardData['id_item']);

        /**
         * @var $badge GamificationBadge
         */

        if(!$badge){
            return $result;
        }

        $contest = $event_params['contest'];
        /**
         * @var $contest GamificationContest
         */
        $result['action'] = true;
        $result['badge'] = $badge;
        $result['id_user'] = $userModel->idst;
        $result['event_module'] = 'gamification';
        $result['event_key'] = 'User has completed the contest "{name}" with rank #{rank}';
        $result['event_params'] = array(
            0 => array(
                '{name}' => !empty($contest->translation) ? $contest->translation->name : $contest->defaultTranslation->name,
                '{rank}' => $event_params['rank']
            )
        );

        return $result;
    }
}