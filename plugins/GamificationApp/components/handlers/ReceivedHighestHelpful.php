<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 3.12.2015 г.
 * Time: 11:58
 */
class ReceivedHighestHelpful extends GamificationGoalHandler implements IGamificationGoalHandler
{
    public function renderGoalSettings($params = array())
    {
        $coursesIds = $params['courses'];
        $courses = array();

        if(!empty($coursesIds)){
            $command = Yii::app()->db->createCommand();
            /**
             * @var $command CDbCommand
             */
            $command->select('idCourse, name, code')
                ->from(LearningCourse::model()->tableName())
                ->where('idCourse IN (' . implode(',', $coursesIds) . ')');
            $courses = $command->queryAll();
        }

        return Yii::app()->controller->renderPartial('goals_partials/received_highest_helpful', array(
            'params' => $params['params'],
            'courses' => $courses
        ), true, true);
    }

    public function collectData()
    {
        $command = Yii::app()->db->createCommand();
        /**
         * @var $command CDbCommand
         */
        $sql = "SELECT fv.id_user as idUser, COUNT(fv.value) as score
FROM `learning_forummessage_votes` `fv`
JOIN `learning_forummessage` `fm` ON fv.id_message = fm.idMessage AND (fm.posted >= '{$this->contest->from_date}' AND fm.posted <= '{$this->contest->to_date}')
WHERE fv.type = 'helpful'
GROUP BY `fv`.`id_user`
ORDER BY `score` DESC";
        $command->setText($sql);
        $rows = $command->queryAll();

        $users = array();
        $usrs = array();
        foreach($rows as $key => $row){
            $users[$key] = $row;
            $usrs[] = $row['idUser'];
        }

        if(empty($usrs)){
            return array();
        }

        $sql2 = "select t.id_user as idUser, sum(t.score) as score
from gamification_assigned_badges t
where t.id_user IN (" . implode(',', $usrs) . ")
GROUP BY t.id_user
ORDER BY score DESC";
        $command2 = Yii::app()->db->createCommand($sql2);
        $badgesUsers = $command2->queryAll();
        $badges = array();
        foreach($badgesUsers as $key => $user){
            $badges[$key] = $user;
        }

        $finalResult = array();

        foreach($users as $key => $user){
            $finalResult[] = array(
                'idUser' => $badges[$key]['idUser'],
                'score' => $user['score']
            );
        }

        $this->data = $finalResult;
    }
}