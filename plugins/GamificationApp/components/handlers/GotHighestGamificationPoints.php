<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 3.12.2015 г.
 * Time: 11:59
 */
class GotHighestGamificationPoints extends GamificationGoalHandler implements IGamificationGoalHandler
{
    public function renderGoalSettings($params = array())
    {

//        $coursesIds = $params['courses'];
//        $courses = array();
//
//        if(!empty($coursesIds)){
//            $command = Yii::app()->db->createCommand();
//            /**
//             * @var $command CDbCommand
//             */
//            $command->select('idCourse, name, code')
//                ->from(LearningCourse::model()->tableName())
//                ->where('idCourse IN (' . implode(',', $coursesIds) . ')');
//            $courses = $command->queryAll();
//        }
//
//        return Yii::app()->controller->renderPartial('goals_partials/got_highest_gamification_points', array(
//            'params' => $params['params'],
//            'courses' => $courses
//        ), true, true);
        return '';
    }

    public function collectData()
    {
        $command = Yii::app()->db->createCommand();
        /**
         * @var $command CDbCommand
         */

        $command->select('t.id_user as idUser, SUM(t.score) as score')
            ->from(GamificationAssignedBadges::model()->tableName() . ' t')
            ->where('t.issued_on >= "' . $this->contest->from_date . '" AND t.issued_on <= "' . $this->contest->to_date . '"')
            ->group('t.id_user')
            ->order('score DESC, t.issued_on ASC');
        $rows = $command->queryAll();

        $this->data = $rows;
    }

}