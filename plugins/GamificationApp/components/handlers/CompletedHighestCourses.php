<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 3.12.2015 г.
 * Time: 10:54
 */
class CompletedHighestCourses extends GamificationGoalHandler implements IGamificationGoalHandler
{

    public function renderGoalSettings($params = array())
    {

        $coursesIds = $params['courses'];;
        $courses = array();

        if(!empty($coursesIds)){
            $command = Yii::app()->db->createCommand();
            /**
             * @var $command CDbCommand
             */
            $command->select('idCourse, name, code')
                ->from(LearningCourse::model()->tableName())
                ->where('idCourse IN (' . implode(',', $coursesIds) . ')');
            $courses = $command->queryAll();
        }

        return Yii::app()->controller->renderPartial('goals_partials/complete_highest_courses', array(
            'params' => $params['params'],
            'courses' => $courses
        ), true, true);
        // TODO: Implement renderGoalSettings() method.
    }

    public function collectData()
    {

        $command = Yii::app()->db->createCommand();
        /**
         * @var $command CDbCommand
         */
        $command->select('idUser, COUNT(idCourse) as score, MAX(date_complete) as date')
            ->from(LearningCourseuser::model()->tableName());
        if($this->courses){
            $command->where('idCourse IN (' . implode(',', $this->courses) . ')');
        }
        $command->andWhere('status = ' . LearningCourseuser::$COURSE_USER_END)
            ->andWhere('date_complete >= "' . $this->contest->from_date . '" AND date_complete <= "' . $this->contest->to_date . '"')
            ->group('idUser')
            ->order('score DESC, date ASC');
        $rows = $command->queryAll();

        $this->data = $rows;
    }


}