<?php

class SuperadministratorRejectedRewardRequest extends NotificationHandler implements INotificationHandler    {


	public function getRecipientsData($params=false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// "AT" ?
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

            $userId   = $this->eventParams['id_user'];
            $rewardId = $this->eventParams['id_reward'];
            $rewardRequest = $this->eventParams['user_reward_request'];
            $newRewardRequest = GamificationUserRewards::model()->findByAttributes(array('id_user' => $userId, 'id_reward' => $rewardId, 'status' => GamificationUserRewards::$STATUS_REJECTED));

			$rewardModel     = GamificationReward::model()->findByPk($rewardId);
            $rewardI18nModel = GamificationRewardTranslation::model()->findByAttributes(array(
                'id_reward' => $rewardId,
                'lang_code'  => Settings::get('default_language', 'english')
            ));

			$userModel 		 = CoreUser::model()->findByPk($userId);

			$targetUsers 	 = $this->maskItems(self::MASK_USERS, array($userModel->idst));

            $recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, FALSE, self::PU_OF_USERSONLY);

            // ADD RELATED INFO
            if (!empty($recipCollection['users'])) {
                foreach ($recipCollection['users'] as $index => $recipient) {
                    $recipCollection['users'][$index]['first_name']             = $userModel->firstname;
                    $recipCollection['users'][$index]['last_name']              = $userModel->lastname;
                    $recipCollection['users'][$index]['username']               = $userModel->getUserNameFormatted();
                    $recipCollection['users'][$index]['reward_name']            = $rewardI18nModel->name;
                    $recipCollection['users'][$index]['reward_coins']           = $rewardModel->coins;
                    $recipCollection['users'][$index]['reward_request_date']    = Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $newRewardRequest->date_created);
                }
            }

		}

		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];

		return $response;

	}

    public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {

        $result[CoreNotification::SC_FIRSTNAME]                          = $recipient['first_name'];
        $result[CoreNotification::SC_LASTNAME]                           = $recipient['last_name'];
        $result[CoreNotification::SC_USERNAME]                           = $recipient['username'];
		$result[CoreNotification::SC_GAMIFICATION_REWARD_NAME]           = $recipient['reward_name'];
		$result[CoreNotification::SC_GAMIFICATION_REWARD_COINS]          = $recipient['reward_coins'];
		$result[CoreNotification::SC_GAMIFICATION_REWARD_REQUEST_DATE]   = $recipient['reward_request_date'];

		return $result;

	}


	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {

		$descriptor                         = new NotificationDescriptor();

		$descriptor->type 					= __CLASS__;
		$descriptor->title 					= Yii::t('gamification', 'Superadministrator rejected reward request');
		$descriptor->description 			= Yii::t('gamification', 'Superadministrator rejected reward request');
		$descriptor->allowedRecipientTypes 	= CoreNotification::$RECIP_USER_AND_GODADMIN;
		$descriptor->allowedScheduleTypes 	= CoreNotification::$SCHEDULE_PRESET_ONLY_AT;
        $descriptor->allowedProperties      = CoreNotification::$PROP_NO_COURSE;
		$descriptor->shortcodes             = array(
			CoreNotification::SC_FIRSTNAME,
			CoreNotification::SC_LASTNAME,
			CoreNotification::SC_USERNAME,
			CoreNotification::SC_GAMIFICATION_REWARD_NAME,
			CoreNotification::SC_GAMIFICATION_REWARD_COINS,
			CoreNotification::SC_GAMIFICATION_REWARD_REQUEST_DATE,
		);
		$descriptor->jobHandler             = NotificationJobHandler::HANDLER_ID;

		return $descriptor;

	}


}
