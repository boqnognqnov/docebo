<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 3.12.2015 г.
 * Time: 11:00
 */
class ScoredHighestInCourse extends GamificationGoalHandler implements IGamificationGoalHandler {

    public function renderGoalSettings($params = array())
    {

        $coursesIds = $params['courses'];
        $courses = array();

        if(!empty($coursesIds)){
            $command = Yii::app()->db->createCommand();
            /**
             * @var $command CDbCommand
             */
            $command->select('idCourse, name, code')
                ->from(LearningCourse::model()->tableName())
                ->where('idCourse IN (' . implode(',', $coursesIds) . ')');
            $courses = $command->queryAll();
        }

        return Yii::app()->controller->renderPartial('goals_partials/scored_highest_in_course', array(
            'params' => $params['params'],
            'courses' => $courses
        ), true, true);
        // TODO: Implement renderGoalSettings() method.
    }

    public function collectData()
    {
        $command = Yii::app()->db->createCommand();

        /**
         * @var $command CDbCommand
         */

        $command->select('lcu.idUser, SUM(lcu.score_given) as score')
            ->from(LearningCourseuser::model()->tableName() . ' lcu');
        if($this->courses){
            $command->andWhere('lcu.idCourse IN (' . implode(',', $this->courses) . ')');
        }
        $command->andWhere('lcu.status = ' . LearningCourseuser::$COURSE_USER_END)
            ->andWhere('lcu.date_complete >= "' . $this->contest->from_date . '" AND lcu.date_complete <= "' . $this->contest->to_date . '"')
            ->group('lcu.idUser')
            ->order('score DESC');

        $rows = $command->queryAll();

        $this->data = $rows;
    }
}