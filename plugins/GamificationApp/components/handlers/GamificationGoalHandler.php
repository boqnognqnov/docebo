<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 3.12.2015 г.
 * Time: 12:01
 */
class GamificationGoalHandler extends CComponent
{

    /**
     * @var $contest GamificationContest
     */
    public $contest;
    public $data = array();
    public $settings = array();
    public $courses;

    function __construct($idContest = null)
    {
        if($idContest){
            $this->contest = GamificationContest::model()->findByPk($idContest);
            $this->settings = $this->contest->getGoalSettings();

            if($this->settings == 'all'){
                $this->courses = false;;
            } else{
                $this->courses = $this->settings['courses'];
            }
        }
    }

    public function finishContest($issueRewards = false)
    {

        if(empty($this->data)){
            if($issueRewards){
                Yii::app()->event->raise(CoreNotification::NTYPE_GAMIFICATION_CONTEST_END, new DEvent($this, array(
                    'id_contest' => $this->contest->id,
                )));
                return true;
            } else {
                return false;
            }
        }


        $data = $this->filterUsers($this->data);

        $zeroStart = false;

        GamificationContestChart::model()->deleteAllByAttributes(array(
            'id_contest' => $this->contest->id
        ));

        $contestData = array();
        foreach($data as $rank => $user){
            //Skipping the zero sores
            if($user['score'] == 0){
                continue;
            }
            if($rank == 0 || $zeroStart){
                $rank++;
                $zeroStart = true;
            }
            $userModel = CoreUser::model()->findByPk($user['idUser']);

            /**
             * @var $userModel CoreUser
             */

            $chart = new GamificationContestChart();
            $chart->id_contest = $this->contest->id;

            $chart->rank = $rank;
            if($userModel){
                $chart->id_user = $userModel->idst;
                $chart->full_name = $userModel->getFullName();
            }
            $chart->ranking_result = intval($user['score']);

            $rewardsModel = GamificationContestReward::model()->findByAttributes(array(
                'id_contest' => $this->contest->id,
                'rank' => $rank
            ));

            /**
             * @var $rewardsModel GamificationContestReward
             */

            $rewardData = array();
            if($rewardsModel){
                switch($rewardsModel->type){
                    case GamificationContestReward::REWARD_TYPE_BADGE:
                        $badge = GamificationBadge::model()->findByPk($rewardsModel->id_item);
                        /**
                         * @var $badge GamificationBadge
                         */
                        if($badge){
                            $rewardData['id_item'] = $badge->id_badge;
                            $rewardData['type'] = GamificationContestReward::REWARD_TYPE_BADGE;
                            $rewardData['value'] = array(
                                'points' => $badge->score,
                                'badge_icon' => $badge->icon
                            );
                        }
                        break;
                }
            }
            $chart->reward_data = CJSON::encode($rewardData);

            if($chart->validate()){
                if($issueRewards){
                    $chart->setScenario('issueRewards');
                }
                $chart->save();
            } else{
            	Yii::log(var_export($chart->getErrors(),true), CLogger::LEVEL_ERROR);
            }
        }
        if($issueRewards){
            Yii::app()->event->raise(CoreNotification::NTYPE_GAMIFICATION_CONTEST_END, new DEvent($this, array(
                'id_contest' => $this->contest->id,
            )));
        }

        return $issueRewards;
    }

    protected function filterUsers($data = array()){
        if($this->contest->filter === 'all'){
            return $data;
        }

        $filteredData = array();
        $users = $this->contest->getUsersFromFilter();

        foreach($data as $item){
            if(in_array($item['idUser'], $users)){
                $filteredData[] = $item;
            }

	        foreach($users as $user){
		        if(is_array($user)){
			        if(in_array($item['idUser'], $user)) {
				        $filteredData[] = $item;
			        }
		        }
	        }
        }

        return $filteredData;
    }

}