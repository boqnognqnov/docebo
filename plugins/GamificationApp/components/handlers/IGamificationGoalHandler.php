<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 3.12.2015 г.
 * Time: 10:31
 */

interface IGamificationGoalHandler{
    public function renderGoalSettings($params = array());
    public function collectData();
}