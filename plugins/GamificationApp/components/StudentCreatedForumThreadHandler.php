<?php

/**
 * Class StudentCreatedForumThreadHandler
 * Handles the student created thread event
 */
class StudentCreatedForumThreadHandler extends CComponent implements GamificationEventHandler
{
    /**
     * Renders the view for the settings page
     * @param $params
     */
    public function renderEventSettingsView($params)
    {
        if(isset($params['params']['courses']) && !empty($params['params']['courses']))
        {
            $criteria = new CDbCriteria();
            $criteria->select = 'idCourse, code, name';
            $criteria->addInCondition('idCourse', $params['params']['courses']);
            $params['courses'] = LearningCourse::model()->findAll($criteria);
        }
        else
            $params['courses'] = array();

		$totalCourses = Yii::app()->db->createCommand('SELECT COUNT(*) FROM '.LearningCourse::model()->tableName())->queryScalar();
		$params['totalCourses'] = $totalCourses;

        // Render the _setting_course view
        $params['handler'] = $this;
        return Yii::app()->controller->renderPartial('_event_setting_forum_thread', $params, true);
    }

    /**
     * Checks whether this badge is to be assigned and returns event logging info
     * @param $badge GamificationBadge
     * @param $event_config array
     * @param $event_params array
     */
    public function shouldAssignBadge($badge, $event_config, $event_params) {
        $result = array('action' => false);

        // Fetch event params
        $course = $event_params['course']; /* @var $course LearningCourse */
        $user = $event_params['user']; /* @var $user CoreUser */
        $thread = $event_params['thread']; /* @var $thread LearningForumthread */

        // Check course association mode
        $limit = (int) $event_config['limit'];
        switch($event_config['courses_association']) {
            case 'all':
                if(!$limit || ($this->countForumThreadsForUser($user) == $limit))
                    $result['action'] = true;
                break;
            case 'selection':
                $selected_courses = isset($event_config['courses']) ? $event_config['courses'] : array();
                if(in_array($course->idCourse, $selected_courses)) {
                    if(!$limit || ($this->countForumThreadsForUser($user, $selected_courses) == $limit))
                        $result['action'] = true;
                }
                break;
        }

        // If badge is to be assigned, fill in logging info
        if($result['action']) {
            $result['id_user'] = $user->idst;
            $result['event_module'] = 'gamification';
            $result['event_key'] = 'Student has created a new discussion "{discussion}" for course "{course_name}"';
            $result['event_params'] = array(0 => array(
                '{course_name}' => $course->name,
                '{discussion}' => $thread->title
            ));
        }

        return $result;
    }

    /**
     * Returns the number of forum threads created by the user in any course or in a specific set of courses
     *
     * @param $user CoreUser
     * @param $courseIds array
     */
    private function countForumThreadsForUser($user, $courseIds = array()) {
        $sql = 'SELECT COUNT(DISTINCT idThread) FROM '.LearningForumthread::model()->tableName().' as thread
				INNER JOIN '.LearningForum::model()->tableName().' AS forum ON thread.idForum = forum.idForum
				WHERE thread.author = '.(int)$user->idst;

        if(!empty($courseIds))
            $sql .= ' AND forum.idCourse IN ('.implode(',', $courseIds).')';

        $res = Yii::app()->db->createCommand($sql)->queryScalar();

        return $res ? $res : 0;
    }

}