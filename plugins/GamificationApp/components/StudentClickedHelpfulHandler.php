<?php

/**
 * Class StudentClickedHelpfulHandler
 * Handles the student clicked on "Helpful" event
 */
class StudentClickedHelpfulHandler extends StudentFeedbackHandler implements GamificationEventHandler
{
    /**
     * Renders the view for the settings page
     * @param $params
     */
    public function renderEventSettingsView($params)
    {
        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/docebo-helpful.css');

        // Collect params
        $this->collectFeedbackParams($params);

        // override params
        $params['feedback_type'] = 'helpful';

        // Render the _setting_course view
        return Yii::app()->controller->renderPartial('_event_setting_feedback', $params, true, true);
    }

    /**
     * Checks whether this badge is to be assigned and returns event logging info
     * @param $badge GamificationBadge
     * @param $event_config array
     * @param $event_params array
     */
    public function shouldAssignBadge($badge, $event_config, $event_params) {
        $result = array('action' => false);

        // Fetch event params
        $evaluator_user = $event_params['evaluator_user']; /* @var $evaluator_user CoreUser */
        $evaluated_user = $event_params['evaluated_user']; /* @var $evaluated_user CoreUser */
        $target_type = $event_params['target_type'];

        $result['action'] = $this->getBadgeResult($event_params, $event_config);

        // If badge is to be assigned, fill in logging info
        if($result['action']) {
            $result['event_module'] = 'gamification';
            if(($event_config['action'] == 'give_feedback')) {
                $result['id_user'] = $evaluator_user->idst;
                $result['event_key'] = 'Student has clicked on "Helpful" on item of type "{target_type}"';
            } else {
                $result['id_user'] = $evaluated_user->idst;
                $result['event_key'] = 'Student received "Helpful" vote on item of type "{target_type}"';
            }

            $result['event_params'] = array(0 => array(
                '{target_type}' => $target_type
            ));
        }

        return $result;
    }
}