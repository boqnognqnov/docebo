<?php

/**
 * Class StudentCompletedCourseHandler
 * Handles the student completed course event
 */
class StudentCompletedCourseHandler extends CComponent implements GamificationEventHandler
{
    /**
     * Renders the view for the settings page
     * @param $params
     */
    public function renderEventSettingsView($params)
    {
        if(isset($params['params']['courses']) && !empty($params['params']['courses']))
        {
            $criteria = new CDbCriteria();
            $criteria->select = 'idCourse, code, name';
            $criteria->addInCondition('idCourse', $params['params']['courses']);
            $params['courses'] = LearningCourse::model()->findAll($criteria);
        }
        else
            $params['courses'] = array();

		$totalCourses = Yii::app()->db->createCommand('SELECT COUNT(*) FROM '.LearningCourse::model()->tableName())->queryScalar();
		$params['totalCourses'] = $totalCourses;

        // Render the _setting_course view
        $params['handler'] = $this;
        return Yii::app()->controller->renderPartial('_event_setting_courses', $params, true);
    }

    /**
     * Checks whether this badge is to be assigned and returns event logging info
     * @param $badge GamificationBadge
     * @param $event_config array
     * @param $event_params array
     */
    public function shouldAssignBadge($badge, $event_config, $event_params) {
        $result = array('action' => false);

        // Fetch event params
        $course = $event_params['course']; /* @var $course LearningCourse */
        $user = $event_params['user']; /* @var $user CoreUser */

        // Check course association mode
        switch($event_config['courses_association']) {
            case 'all':
                $result['action'] = true;
                break;
            case 'selection':
                $selected_courses = isset($event_config['courses']) ? $event_config['courses'] : array();
                if(in_array($course->idCourse, $selected_courses))
                    $result['action'] = true;
                break;
            case 'all_selection' :
                $selected_courses = isset($event_config['courses']) ? $event_config['courses'] : array();
                if( LearningCourseuser::checkCompletedCoursesByUser($user->idst, $selected_courses) && in_array($course->idCourse, $selected_courses) )
                    $result['action'] = true;
                break;
            case 'nth':
                $total = $this->getCompletedCoursesTotal($user);
                $limit = (int) $event_config['limit'];
                if($limit > 0 && ($total == $limit))
                    $result['action'] = true;
                break;
        }

        // If badge is to be assigned, fill in logging info
        if($result['action']) {
            $result['id_user'] = $user->idst;
            $result['event_module'] = 'gamification';
            if($event_config['courses_association'] == 'nth')
                $result['event_key'] = 'Student has completed his {number}-th course ("{course_name}")';
            else
                $result['event_key'] = 'Student has completed course "{course_name}"';

            $result['event_params'] = array(0 => array(
                '{course_name}' => $course->name,
                '{number}' => $event_config['limit']
            ));
        }

        return $result;
    }

    /**
     * Returns how many course the user has completed (until now)
     *
     * @param $position integer
     * @param $user CoreUser
     */
    private function getCompletedCoursesTotal($user) {
        // Count how many course the user has completed
        $completedCourses = LearningCourseuser::model()->countByAttributes(array('idUser' => $user->idst, 'status' => LearningCourseuser::$COURSE_USER_END));
        return $completedCourses;
    }
}