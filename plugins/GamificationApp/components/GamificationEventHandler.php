<?php
/**
 * Interface GamificationEventHandler
 * Defines the standard interface for a gamification event handlers
 */
interface GamificationEventHandler {
    public function renderEventSettingsView($params);
    public function shouldAssignBadge($badge, $event_config, $event_params);
} 