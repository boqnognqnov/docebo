<?php

/**
 * Class StudentCompletedCourseHandler
 * Handles the student completed LO event
 */
class StudentCompletedLoHandler extends CComponent implements GamificationEventHandler
{
    /**
     * Renders the view for the settings page
     * @param $params
     */
    public function renderEventSettingsView($params) {
        if(isset($params['params']['id_organization']) && !empty($params['params']['id_organization']))
        {
            $lo = LearningOrganization::model()->findByPk($params['params']['id_organization']);
            $params['selected_learning_object'] = $lo;
			if($lo){
				// Find all learning objects inside the course
				$criteria = new CDbCriteria();
				$criteria->select = 'idOrg, title, objectType';
				$criteria->condition = 'idCourse = '.$lo->idCourse.' AND visible = 1 AND objectType <> ""';
				$params['learning_objects'] = LearningOrganization::model()->findAll($criteria);
			}
        }

        if(!$params['selected_learning_object']) {
            $params['selected_learning_object'] = new LearningOrganization();
            $params['selected_learning_object']->course = new LearningCourse();
            $params['learning_objects'] = array();
        }

        // Render the _setting_course view
        $params['handler'] = $this;
        return Yii::app()->controller->renderPartial('_event_setting_lo', $params, true);
    }

    /**
     * Checks whether this badge is to be assigned and returns event logging info
     * @param $badge GamificationBadge
     * @param $event_config array
     * @param $event_params array
     */
    public function shouldAssignBadge($badge, $event_config, $event_params) {
        $result = array('action' => false);

        // Fetch event params
        $lo = $event_params['organization']; /* @var $lo LearningOrganization */
        $user = $event_params['user']; /* @var $user CoreUser */
        // Check course association mode
        $selected_lo = isset($event_config['id_organization']) ? $event_config['id_organization'] : null;
        if($selected_lo && ($selected_lo == $lo->idOrg)) {
            $min_score = (int) $event_config['min_score'];
            if ( $min_score > 0 ) {
                if($this->isObjectWithScore($lo) && $min_score > 0) {
                    // Get object score
                    $track = LearningCommontrack::model()->findByAttributes(array('idUser' => (int)$user->idst, 'idReference' => (int)$lo->idOrg));
                    if($track && ($track->score >= $min_score))
                        $result['action'] = true;
                } else
                    $result['action'] = true;
            } else {
                $under_score = (int) $event_config['under_score'];

                if($this->isObjectWithScore($lo) && $under_score > 0) {
                    // Get object score
                    $track = LearningCommontrack::model()->findByAttributes(array('idUser' => (int)$user->idst, 'idReference' => (int)$lo->idOrg));
                    if($track && ($track->score <= $under_score))
                        $result['action'] = true;
                } else
                    $result['action'] = true;
            }

        }

        // If badge is to be assigned, fill in logging info
        if($result['action']) {
            $result['id_user'] = $user->idst;
            $result['event_module'] = 'gamification';
            $result['event_key'] = 'Student has completed learning object "{object_title}" inside course "{course_name}"';
            $result['event_params'] = array(0 => array(
                '{course_name}' => $lo->course->name,
                '{object_title}' => $lo->title
            ));
        }

        return $result;
    }

    /**
     * Returns true if passed object has score assigned
     * @param $lo LearningOrganization
     */
    public function isObjectWithScore($lo) {
        return in_array($lo->objectType, array(LearningOrganization::OBJECT_TYPE_TEST, LearningOrganization::OBJECT_TYPE_DELIVERABLE,
            LearningOrganization::OBJECT_TYPE_SCORMORG, LearningOrganization::OBJECT_TYPE_TINCAN));
    }

    /** Returns the json array of learning objects
     * @param $objects array
     */
    public function getObjectsAsJson($objects) {
        $list = array();
        foreach($objects as $object) {
            $list[$object->idOrg] = $this->isObjectWithScore($object);
        }
        return CJSON::encode($list);
    }
}