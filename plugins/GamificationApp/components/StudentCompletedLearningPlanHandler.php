<?php

/**
 * Class StudentCompletedLearningPlanHandler
 * Handles the student completed course event
 */
class StudentCompletedLearningPlanHandler extends CComponent implements GamificationEventHandler
{
    /**
     * Renders the view for the settings page
     * @param $params
     */
    public function renderEventSettingsView($params)
    {
        if(isset($params['params']['learning_plan']) && !empty($params['params']['learning_plan']))
        {
            $criteria = new CDbCriteria();
            $criteria->select = 'id_path, path_code, path_name';
            $criteria->addInCondition('id_path', $params['params']['learning_plan']);
            $params['learning_plan'] = LearningCoursepath::model()->findAll($criteria);
        }
        else
            $params['learning_plan'] = array();

        $totalLearningPlans = Yii::app()->db->createCommand('SELECT COUNT(*) FROM '.LearningCoursepath::model()->tableName())->queryScalar();
        $params['totalLearningPlans'] = $totalLearningPlans;

        // Render the _setting_learning_plan view
        $params['handler'] = $this;
        return Yii::app()->controller->renderPartial('_event_setting_learning_plan', $params, true);
    }

    /**
     * Checks whether this badge is to be assigned and returns event logging info
     * @param $badge GamificationBadge
     * @param $event_config array
     * @param $event_params array
     */
    public function shouldAssignBadge($badge, $event_config, $event_params) {
        $result = array('action' => false);

        // Fetch event params
        $learningPlan = $event_params['learningPlan']; /* @var $course LearningCoursepath */
        $user         = $event_params['user'];          /* @var $user CoreUser */

        // Check course association mode
        switch($event_config['learning_plan_association']) {
            case 'all':
                $result['action'] = true;
                break;
            case 'selection':
                $selected_plans = (isset($event_config['learning_plan']) ? $event_config['learning_plan'] : array());
                if(in_array($learningPlan->id_path, $selected_plans))
                    $result['action'] = true;
                break;
            case 'all_selection' :
                $selected_learning_plan = isset($event_config['learning_plan']) ? $event_config['learning_plan'] : array();
                if( LearningCoursepathUser::getCompletedLearningPlansByUser($user->idst, $selected_learning_plan) && in_array($learningPlan->id_path, $selected_learning_plan) )
                    $result['action'] = true;
                break;
        }

        // If badge is to be assigned, fill in logging info
        if($result['action']) {
            $result['id_user']      = $user->idst;
            $result['event_module'] = 'gamification';
            $result['event_key']    = 'Student has completed the learning plan "{learning_plan_name}"';
            $result['event_params'] = array(0 => array(
                '{learning_plan_name}' => $learningPlan->path_name,
                '{number}' => $event_config['limit']
            ));
        }

        return $result;
    }

}