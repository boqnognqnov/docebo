<?php

/**
 * Yii DB Migration template.
 *
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 *
 */
class m160303_152105_CREATE_TABLE_aws_china_scorm_replications extends DoceboDbMigration {

	public function safeUp () {
		$this->createTable( 'aws_china_scorm_replications', array(
			'id'                   => 'pk',
			'hash'                 => 'string',
			'is_zip_replicated'    => 'boolean DEFAULT 0',
			'is_folder_replicated' => 'boolean DEFAULT 0',
		) );

		return TRUE;
	}

	public function safeDown () {
		// PUT YOUR MIGRATION-DOWN CODE HERE
		// DO NOT USE try/catch or DB transactions!

		// Internally, this method call is wrapped in a try/catch block and DB transaction
		// You can throw exceptions here to be catched internally.
		// If you wish to cancel the migration, return false
		return TRUE;
	}


}
