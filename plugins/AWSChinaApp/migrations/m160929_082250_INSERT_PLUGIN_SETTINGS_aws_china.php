<?php
/**
 * Yii DB Migration template.
 * 
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 * 
 */
class m160929_082250_INSERT_PLUGIN_SETTINGS_aws_china extends DoceboDbMigration {

	public function safeUp(){
		PluginSettings::save('send_notifications', "1", 'AWSChinaApp');
		return true;
	}

	public function safeDown(){
		return true;
	}
	
	
}
