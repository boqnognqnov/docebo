<?php
/**
 * @author Dzhuneyt <Dzhuneyt@dzhuneyt.com>
 * Date: 21.12.2015 г.
 * Time: 12:56 ч.
 */

class AWSChinaAppController extends Controller{

	public function actionUpdateChinaAccessibility(){
		$isUserInChina = Yii::app()->request->getParam('china_reached');

		$model = CoreSettingUser::model()->findByAttributes(array(
			'id_user'=>Yii::app()->user->getIdst(),
			'path_name'=>AWSChinaAppModule::CHINA_REACHABLE_SETTING_NAME
		));
		if(!$model){
			$model = new CoreSettingUser();
			$model->id_user = Yii::app()->user->getIdst();
			$model->path_name = AWSChinaAppModule::CHINA_REACHABLE_SETTING_NAME;
		}
		$model->value = $isUserInChina ? AWSChinaAppModule::CHINA_REACHABLE_YES : AWSChinaAppModule::CHINA_REACHABLE_NO;
		$model->save();
	}

}