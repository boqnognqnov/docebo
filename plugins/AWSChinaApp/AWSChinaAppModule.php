<?php
use GeoIp2\Database\Reader;

/**
 * Class AWSChinaAppModule
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 *
 */
class AWSChinaAppModule extends CWebModule {

	const CHINA_REACHABLE_SETTING_NAME = 'china_reachable';

	const CHINA_REACHABLE_YES = 'yes';
	const CHINA_REACHABLE_NO = 'no';

	private $chinaReplicableCollections = array(
		CFileStorage::COLLECTION_LO_TINCAN,
		CFileStorage::COLLECTION_LO_SCORM,
		CFileStorage::COLLECTION_LO_VIDEO,
	);

	public function init () {

		$this->setImport( array(
			'AWSChinaApp.models.*',
		) );

		// Register a handler to listen for file uploads to Amazon S3
		// and replicate the same file at the same location but to the
		// Amazon S3 China region
		Yii::app()->event->on( 'onFileUploadedToStorage', array( $this, 'onFileUploadedToStorage' ) );

		// When Video LO transcoding has been completed (Amazon SNS calls back the LMS with a message of "Success")
		Yii::app()->event->on( 'onAmazonTranscodingComplete', array( $this, 'onAmazonVideoTranscodingComplete' ) );

		/*
		 * This was removed for now because is out of scope of the SOW contract
		// App7020 contribution upload transcoding has completed
		Yii::app()->event->on('onApp7020TranscodingCompleted', array($this, 'onApp7020TranscodingCompleted'));
		*/

		/**
		 * On each login, clear the previously saved flag
		 * whether or not the user can reach the Amazon China S3 region,
		 * because he (the user) may have relocated countries since his last
		 * login. The next page request will do the check again for him (through JS/AJAX)
		 * @see _checkIfCurrentUserCanAccessAmazonS3China()
		 */
		Yii::app()->event->on( 'onAfterLogin', function ( DEvent $event ) {
			CoreSettingUser::model()->deleteAllByAttributes( array(
				'id_user'   => Yii::app()->user->getIdst(),
				'path_name' => self::CHINA_REACHABLE_SETTING_NAME,
			) );
		} );

		// We detect if the current user is located in China by
		// querying the MaxMind Geolite country DB via its IP address
		// Only if this flag is present, we start serving S3 content from
		// the  Amazon S3 China region instead of from Amazon S3 Global
		// for this user
		if( self::isCurrentUserInChina() ) {

			// Serve Amazon S3 file direct streams by Amazon S3 China
			Yii::app()->event->on( 'onBeforeSendFileFromStorage', array(
				$this,
				'sendFileFromChinaS3InsteadOfGlobalS3'
			) );

			// Sever Amazon S3 file URLs from Amazon S3 China
			Yii::app()->event->on( 'onBeforeGetFileUrlFromStorage', array(
				$this,
				'serveFileUrlFromChinaS3InsteadOfGlobalS3'
			) );

		}

		// If the current user was not yet checked whether or not he is
		// in China, do that check now
		$currentUserChinaReachableFlagChecked = CoreSettingUser::getSettingByUser( Yii::app()->user->getIdst(), self::CHINA_REACHABLE_SETTING_NAME );
		if( ! $currentUserChinaReachableFlagChecked ) {
			Yii::app()->event->on( 'AfterBodyTagOpen', array( $this, '_checkIfCurrentUserCanAccessAmazonS3China' ) );
		}

		self::validateConfiguration();
	}

	/**
	 * Check if this plugin was correctly configured
	 * in main-live/main-devel.php
	 */
	static public function validateConfiguration () {
		if( ! isset( Yii::app()->params['china_storage'] ) || empty( Yii::app()->params['china_storage'] ) ) {
			Yii::log( 'The "china_storage" param is not properly configured in the main.php configuration file. Amazon S3 China app (AWSChinaApp) will not work without it.', CLogger::LEVEL_ERROR );
		}
	}

	static public function isCurrentUserInChina () {
		$setting = CoreSettingUser::getSettingByUser( Yii::app()->user->getIdst(), self::CHINA_REACHABLE_SETTING_NAME );

		return ( $setting == self::CHINA_REACHABLE_YES );
	}

	/**
	 * Callback, triggered after ANY file is uploaded to ANY file storage
	 * through the FileStorage classes.
	 *
	 * This specific implementation will check that the file has been
	 * uploaded to any (non-Beijing) Amazon S3 Global Region and replicate it
	 * to the Amazon S3 Beijing region.
	 *
	 * Since S3 China is a special AWS S3 Region, it has its own credentials,
	 * and the Amazon cross-region replication service is not available automatically.
	 * It has to be done on PHP app level like we do here
	 *
	 * @see http://docs.amazonaws.cn/en_us/aws/latest/userguide/s3.html
	 * @see http://docs.aws.amazon.com/AmazonS3/latest/dev/crr.html
	 *
	 * @param \DEvent $event
	 */
	public function onFileUploadedToStorage ( DEvent $event ) {

		/* @see CFileStorage::TYPE_S3 */
		/* @see CFileStorage::TYPE_S3_CHINA */
		/* @see CFileStorage::TYPE_FILESYSTEM */
		$fileStorageType = $event->getParamValue( 'storageType' );

		if( $fileStorageType != CFileStorage::TYPE_S3 ) {
			return;
		}

		$collection      = $event->getParamValue( 'collection' );
		$sourcePath      = $event->getParamValue( 'sourcePath' );
		$customSubfolder = $event->getParamValue( 'customSubfolder' );
		$fileName        = $event->getParamValue( 'filename' );
		$storeParams     = $event->getParamValue( 'params' );

		if( ! in_array( $collection, $this->chinaReplicableCollections ) ) {
			return;
		}

		$jobParams = array(
			'originalPath'    => $sourcePath,
			'collection'      => $collection,
			'customSubfolder' => $customSubfolder,
			'fileName'        => $fileName,
			'storeParams'     => $storeParams,
		);

		Yii::log( 'Scheduling replicating of LO to Amazon China S3' );

        Yii::app()->scheduler->createImmediateJob( 'AWSChinaLoReplicatorJob', AWSChinaLoReplicatorJob::id(), $jobParams, FALSE, NULL, TRUE );
	}

	public function sendFileFromChinaS3InsteadOfGlobalS3 ( DEvent $event ) {
		if( $event->getParamValue( 'storageType' ) == CFileStorage::TYPE_S3 ) {
			// Override Amazon Global S3 file streaming
			// functionality to use Amazon China S3 resources

			$collection      = $event->getParamValue( 'collection' );
			$filename        = $event->getParamValue( 'filename' );
			$displayFileName = $event->getParamValue( 'displayFileName' );
			$customSubfolder = $event->getParamValue( 'customSubfolder' );

			if( ! in_array( $collection, $this->chinaReplicableCollections ) ) {
				return;
			}

			// Second parameter instantiates the S3 storage class with China S3 settings
			$storage = CFileStorage::getDomainStorage( $collection, 'china_storage' );
			$storage->sendFile( $filename, $displayFileName, $customSubfolder );

			return FALSE; // prevent default file sending functionality
		}
	}

	public function serveFileUrlFromChinaS3InsteadOfGlobalS3 ( DEvent $event ) {
		if( $event->getParamValue( 'storageType' ) == CFileStorage::TYPE_S3 ) {
			// Override Amazon Global S3 file URL generation
			// functionality to use Amazon China S3 resources

			$collection      = $event->getParamValue( 'collection' );
			$filename        = $event->getParamValue( 'filename' );
			$customSubfolder = $event->getParamValue( 'customSubfolder' );

			if( ! in_array( $collection, $this->chinaReplicableCollections ) ) {
				return;
			}

			// Second parameter instantiates the S3 storage class with China S3 settings
			$storage             = CFileStorage::getDomainStorage( $collection, 'china_storage' );
			$event->return_value = $storage->fileUrl( $filename, $customSubfolder );

			return FALSE; // prevent default file sending functionaly
		}
	}

	public static function _checkIfCurrentUserCanAccessAmazonS3China () {
		if( Yii::app()->user->getIsGuest() ) {
			return;
		}

		try {
			$clientIps = IPHelper::getRemoteIp(TRUE);
			// $clientIpsWithLoadBalancer = array('10.8.0.10', '116.236.216.116');
			// $clientIps = array('116.236.216.116'); // Sample Chinese IP from Shangai

			require_once(dirname(__FILE__) . '/vendor/autoload.php');

			// This creates the Reader object, which should be reused across lookups
			$reader = new Reader(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'GeoLite2-Country.mmdb');


			$isUserInChina = FALSE;
			foreach($clientIps as $ip) {
				try {
					$record = $reader->country($ip);
					if($record->country->isoCode == 'CN') {
						$isUserInChina = TRUE;
						break;
					}
				} catch (Exception $e) {
					Yii::log('Can not detect the country of IP ' . $ip.'. Reason: '.$e->getMessage(), CLogger::LEVEL_WARNING);
				}
			}


			$model = CoreSettingUser::model()->findByAttributes(array(
				'id_user' => Yii::app()->user->getIdst(),
				'path_name' => AWSChinaAppModule::CHINA_REACHABLE_SETTING_NAME
			));
			if(!$model) {
				$model = new CoreSettingUser();
				$model->id_user = Yii::app()->user->getIdst();
				$model->path_name = AWSChinaAppModule::CHINA_REACHABLE_SETTING_NAME;
			}
			$model->value = $isUserInChina ? AWSChinaAppModule::CHINA_REACHABLE_YES : AWSChinaAppModule::CHINA_REACHABLE_NO;
			$model->save();
		} catch ( Exception $e ) {
			Yii::log( 'Can not detect if user is in China or not', CLogger::LEVEL_ERROR );
			Yii::log( $e->getMessage(), CLogger::LEVEL_ERROR );
		}
	}

	/**
	 * Called after a Video LO is uploaded and Amazon has successfully transcoded it.
	 * (generated lower resolution versions and thumbnails, etc)
	 * Here we start a one time background job that downloads the original file
	 * and the derivative files and replicates them to Amazon S3 China
	 *
	 * @param DEvent $event
	 */
	public function onAmazonVideoTranscodingComplete ( DEvent $event ) {
		$marker     = $event->getParamValue( 'marker' ); // File name, without extension
		$pathToFile = $event->getParamValue( 'outputKeyPrefix' );

		$jobParams = array(
			'marker' => $marker,
		);

		Yii::log( 'Scheduling replicating of video LO and derivatives: ' . $marker . ' to Amazon China S3' );

		Yii::app()->scheduler->createImmediateJob( 'AWSChinaTranscodedVideoReplicatorJob', AWSChinaTranscodedVideoReplicatorJob::id(), $jobParams, FALSE, NULL, TRUE );
	}

	/**
	 * After a Video is uploaded to Coach & Share and its transcoding is completed
	 * We start a background job here, which replicates the original file and the
	 * derivatives (smaller resolutions, thumbnails) to Amazon S3 China
	 *
	 * @param DEvent $event
	 *
	 * @unused
	 * @deprecated Not paid by the client, this is why it's never called :)
	 */
	public function onApp7020TranscodingCompleted ( DEvent $event ) {
		$marker = $event->getParamValue( 'marker' ); // File URL, without extension

		$jobParams = array(
			'marker' => $marker,
		);

		Yii::log( 'Scheduling replicating of App7020 Contribution video ' . $marker . ' to Amazon S3 China' );

		Yii::app()->scheduler->createImmediateJob( 'AWSChinaApp7020ContributionReplicatorJob', AWSChinaApp7020ContributionReplicatorJob::id(), $jobParams, FALSE, NULL, TRUE );
	}
}
