<?php

/**
 * Allows for auto replicating Amazon S3 content to Amazon China S3
 *
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */

class AWSChinaApp extends DoceboPlugin {

	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'AWSChinaApp';

	public $is_mobile_capable = true;

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return DoceboPlugin Object
	 */
	public static function plugin($class_name = __CLASS__) {
		return parent::plugin($class_name);
	}

	public function activate() {
		parent::activate();
	}

	public function deactivate() {
		parent::deactivate();
	}

}
