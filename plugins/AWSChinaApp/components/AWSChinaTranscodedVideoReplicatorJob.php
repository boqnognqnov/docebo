<?php

/**
 * Class AWSChinaTranscodedVideoReplicatorJob
 *
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */
class AWSChinaTranscodedVideoReplicatorJob extends JobHandler implements IJobHandler {

	const ID = 'AWSChinaApp.components.AWSChinaTranscodedVideoReplicatorJob';

	private $sendNotifications = true;

	public function init ( $params = FALSE ) {

		$pSettings = PluginSettings::get('send_notifications', 'AWSChinaApp');

		if($pSettings <= 0){
			$this->sendNotifications = false;
		}
	}

	public function run ( $params = FALSE ) {
		$jobParams = CJSON::decode( $this->job->params );

		$marker = $jobParams['marker'];

		// Replicate the original file (not transcoded)
		$standardStorage = CFileStorage::getDomainStorage( CFileStorage::COLLECTION_LO_VIDEO );
		$chinaStorage    = CFileStorage::getDomainStorage( CFileStorage::COLLECTION_LO_VIDEO, 'china_storage' );

		$ds = DIRECTORY_SEPARATOR;

		$localTmpToStoreDerivativesAndOriginal = Docebo::getUploadTmpPath() . $ds . '' . 'aws-china-replicator' . $ds . $marker . $ds;

		if( ! is_dir( $localTmpToStoreDerivativesAndOriginal ) ) {
			mkdir( $localTmpToStoreDerivativesAndOriginal, 0777, TRUE );
		}

		// Download the original file (non transcoded)
		$originalFilename = $marker . '.' . VideoConverter::ET_VIDEO_EXTENSION;
		$standardStorage->downloadAs( $marker . '.' . VideoConverter::ET_VIDEO_EXTENSION, $localTmpToStoreDerivativesAndOriginal . $ds . $originalFilename );
		$chinaStorage->storeAs( $localTmpToStoreDerivativesAndOriginal . $ds . $marker . '.' . VideoConverter::ET_VIDEO_EXTENSION, $originalFilename );

		$_pathWithoutFilename = $standardStorage->getFileKey( '' );

		// Download all derivative files (thumbnails, lower resolution video, etc)
		$derivativeFiles = $standardStorage->findFiles( $marker );
		foreach ( $derivativeFiles as $derivativeFile ) {
			$pathToDerivativeFile = '/' . ltrim( $derivativeFile, '/' );
			$pathToDerivativeFile = str_replace( $_pathWithoutFilename, '', $pathToDerivativeFile );

			$pathInfo = pathinfo( $pathToDerivativeFile );

			$directory = $pathInfo['dirname'];
			$fileName  = $pathInfo['basename'];

			$localDirToFile = $localTmpToStoreDerivativesAndOriginal . $directory . $ds;

			if( ! is_dir( $localDirToFile ) ) {
				mkdir( $localDirToFile );
			}

			$standardStorage->downloadAs( $fileName, $localDirToFile . $fileName, $directory );
			$chinaStorage->storeAs( $localDirToFile . $fileName, $fileName, $directory );
		}

		// Clean up the temporary working dir
		if( !Docebo::deleteFolderRecursive( $localTmpToStoreDerivativesAndOriginal ) ) {
			Yii::log( 'Unable to delete temporary working folder: ' . $localTmpToStoreDerivativesAndOriginal, CLogger::LEVEL_ERROR );
		}

		$idOrg = Yii::app()->getDb()->createCommand()
					->select( 'org.idOrg' )
					->from( 'learning_organization org' )
					->join( 'learning_video v', 'org.objectType=:video AND org.idResource=v.id_video AND v.video_formats LIKE :search', array(
						':video'  => LearningOrganization::OBJECT_TYPE_VIDEO,
						':search' => '%' . $marker . '%',
					) )
					->queryScalar();
		if($this->sendNotifications){
			AWSChinaAfterReplicationEmailNotifier::notifyAfterReplication( $idOrg );
		}
	}

	public static function id ( $params = FALSE ) {
		return self::ID;
	}
}