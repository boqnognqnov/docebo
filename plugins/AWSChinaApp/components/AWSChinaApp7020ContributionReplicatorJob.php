<?php

/**
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */
class AWSChinaApp7020ContributionReplicatorJob extends JobHandler implements IJobHandler {

	const ID = 'AWSChinaApp.components.AWSChinaApp7020ContributionReplicatorJob';

	public function init($params = false) {
	}

	public function run($params = false) {
		$jobParams = CJSON::decode($this->job->params);

		$marker = $jobParams['marker'];

		// Replicate the original file (not transcoded)
		$standardStorage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020);
		$chinaStorage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020, 'china_storage');

		$ds = DIRECTORY_SEPARATOR;

		$localTmpToStoreDerivativesAndOriginal = Docebo::getUploadTmpPath() . $ds . '' . 'aws-china-replicator' . $ds . $marker . $ds;

		if(!is_dir($localTmpToStoreDerivativesAndOriginal)) {
			mkdir($localTmpToStoreDerivativesAndOriginal, 0777, true);
		}


		$_pathWithoutFilename = $standardStorage->getFileKey('video/');

		// Download all derivative files (thumbnails, lower resolution video, etc)
		$derivativeFiles = $standardStorage->findFiles('video/' . $marker);

		foreach($derivativeFiles as $derivativeFile) {
			$pathToDerivativeFile = '/' . ltrim($derivativeFile, '/');
			$pathToDerivativeFile = str_replace($_pathWithoutFilename, '', $pathToDerivativeFile);

			$pathInfo = pathinfo($pathToDerivativeFile);

			$directory = 'video/' . $pathInfo['dirname'];
			$fileName = $pathInfo['basename'];

			$localDirToFile = $localTmpToStoreDerivativesAndOriginal . $directory . $ds;

			if(!is_dir($localDirToFile)) {
				mkdir($localDirToFile, 0777, true);
			}

			$localDirToFile = realpath($localDirToFile) . $ds;

			$standardStorage->downloadAs($fileName, $localDirToFile . $fileName, $directory);
			$chinaStorage->storeAs($localDirToFile . $fileName, $fileName, $directory);
		}

		// Clean up the temporary working dir
		Docebo::deleteFolderRecursive($localTmpToStoreDerivativesAndOriginal);
	}

	public static function id($params = false) {
		return self::ID;
	}
}