<?php

/**
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 * Created: 23.2.2016 г. 14:38 ч.
 */
class AWSChinaLoReplicatorJob extends JobHandler implements IJobHandler {

	const ID = 'AWSChinaApp.components.AWSChinaLoReplicatorJob';

	private $sendNotifications = true;

	public function init ( $params = FALSE ) {

		$pSettings = PluginSettings::get('send_notifications', 'AWSChinaApp');

		if($pSettings <= 0){
			$this->sendNotifications = false;
		}
	}

	public function run ( $params = FALSE ) {
		$jobParams = CJSON::decode( $this->job->params );

		$originalPath = $jobParams['originalPath'];
		$collection      = $jobParams['collection'];
		$customSubfolder = $jobParams['customSubfolder'];
		$fileName        = $jobParams['fileName'];
		$storeParams     = $jobParams['storeParams'];
		
		$standardStorage = CFileStorage::getDomainStorage( $collection );
		$chinaStorage = CFileStorage::getDomainStorage( $collection, 'china_storage' );


		$ds      = DIRECTORY_SEPARATOR;

		if(stripos($originalPath, '.')){

		    // It's a file (e.g. the SCORM zip original)

			$localTmpToStoreDerivativesAndOriginal = Docebo::getUploadTmpPath() . $ds . '' . 'aws-china-replicator';


			if(!is_dir($localTmpToStoreDerivativesAndOriginal)) {
				mkdir($localTmpToStoreDerivativesAndOriginal, 0777, true);
			}

			$localTmpToStoreDerivativesAndOriginal = realpath($localTmpToStoreDerivativesAndOriginal).$ds.$fileName;

			$standardStorage->downloadAs($fileName,$localTmpToStoreDerivativesAndOriginal,$customSubfolder);
			$chinaStorage->storeAs( $localTmpToStoreDerivativesAndOriginal, $fileName, $customSubfolder, $storeParams );
		}else{
			// It is a folder

			$localTmpToStoreDerivativesAndOriginal = Docebo::getUploadTmpPath() . $ds . '' . 'aws-china-replicator' . $ds . $fileName . $ds;

			$subFolder = $fileName;

			Yii::log('Creating temporary local working folder for replicating contributions: ' . $localTmpToStoreDerivativesAndOriginal);

			if(!is_dir($localTmpToStoreDerivativesAndOriginal)) {
				mkdir($localTmpToStoreDerivativesAndOriginal, 0777, true);
			}

			$_pathWithoutFilename = $standardStorage->getFileKey($fileName);

			// Download all derivative files (thumbnails, lower resolution video, etc)
			$derivativeFiles = $standardStorage->findFiles($fileName);

			foreach ( $derivativeFiles as $derivativeFile ) {
				$pathToDerivativeFile = '/' . ltrim($derivativeFile, '/');
				$pathToDerivativeFile = str_replace($_pathWithoutFilename, '', $pathToDerivativeFile);

				$pathInfo = pathinfo($pathToDerivativeFile);

				$directory = $pathInfo['dirname'];
				$fileName = $pathInfo['basename'];

                //Yii::log('DEBUG CODE:', 'error');
                //Yii::log('$_pathWithoutFilename:'.$_pathWithoutFilename, 'error');
                //Yii::log('$pathToDerivativeFile:'.$pathToDerivativeFile, 'error');
                //Yii::log('Upload directory is: ' .$directory, 'error');
                //Yii::log('Upload file is: '.$fileName, 'error');
                //Yii::log('Subfolder is :'.$subFolder, 'error');

				$localDirToFile = $localTmpToStoreDerivativesAndOriginal.$directory;

				if(!is_dir($localDirToFile)) {
					mkdir($localDirToFile, 0777, true);
				}

				$localDirToFile = realpath($localDirToFile) . $ds;

				$standardStorage->downloadAs($subFolder.$pathToDerivativeFile, $localDirToFile.$fileName, $customSubfolder);
				$chinaStorage->storeAs($localDirToFile . $fileName, $pathToDerivativeFile, $subFolder);

				Yii::log('Replicated transcoded file China S3: ' . $directory .$ds. $fileName);
			}
		}


		if( ! Docebo::deleteFolderRecursive( $localTmpToStoreDerivativesAndOriginal ) ) {
			Yii::log( 'Can not clean up temporary working folder: ' . $localTmpToStoreDerivativesAndOriginal, CLogger::LEVEL_ERROR );
		}

		if( $collection == CFileStorage::COLLECTION_LO_SCORM ) {
			// Try to retrieve the idOrg of this SCORM
			$scormHash = explode( '.', $fileName );
			$scormHash = reset( $scormHash );

			$logModel = AwsChinaScormReplications::model()->findByAttributes( array( 'hash' => $scormHash ) );
			if( ! $logModel ) {
				$logModel       = new AwsChinaScormReplications();
				$logModel->hash = $scormHash;
			}

			if( stripos( $fileName, '.zip' ) === FALSE ) {
				// We are replicating the scorm's extracted folder right now
				$logModel->is_folder_replicated = 1;
				Yii::log('SCORM folder replicated', CLogger::LEVEL_INFO);
			} else {
				// We are replicating the scorm's original zip file right now
				$logModel->is_zip_replicated = 1;
				Yii::log('SCORM zip file replicated', CLogger::LEVEL_INFO);
			}

			$readyToEmail = false;

			if(!PluginManager::isPluginActive('OfflinePlayerApp')){
				if( $logModel->is_folder_replicated){
					$readyToEmail = true;
				}
			}else{
				if( $logModel->is_folder_replicated && $logModel->is_zip_replicated ) {
					$readyToEmail = true;
				}
			}

			if( $readyToEmail ) {

				if(!$logModel->getIsNewRecord()) {
					// No need for this log anymore
					$logModel->delete();
				}

				// ready to send the email,
				// both the origianal scorm zip and the extracted folder are replicated

				$idOrg = Yii::app()->getDb()->createCommand()
							->select( 'o.idOrg' )
							->from( LearningOrganization::model()->tableName() . ' o' )
							->join( LearningScormOrganizations::model()->tableName() . ' sco', 'o.objectType=:scorm AND o.idResource=sco.idscorm_organization', array(
								":scorm" => LearningOrganization::OBJECT_TYPE_SCORMORG
							) )
							->join( LearningScormPackage::model()->tableName() . ' scp', 'sco.idscorm_package=scp.idscorm_package AND scp.path=:search', array(
								':search' => $scormHash,
							) )
							->queryScalar();
				if($this->sendNotifications){
					AWSChinaAfterReplicationEmailNotifier::notifyAfterReplication( $idOrg );
				}

			} else {
				if( ! $logModel->save() ) {
					Yii::log(var_export($logModel->getErrors(),true), CLogger::LEVEL_ERROR);
				}
			}
		}
	}

	public static function id ( $params = FALSE ) {
		return self::ID;
	}
}