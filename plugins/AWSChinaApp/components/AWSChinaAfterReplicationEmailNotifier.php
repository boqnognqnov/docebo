<?php

/**
 * @author Dzhuneyt <Dzhuneyt@dzhuneyt.com>
 * Date: 3.3.2016 г.
 * Time: 14:00 ч.
 */
class AWSChinaAfterReplicationEmailNotifier {

	static public function notifyAfterReplication ( $idOrg ) {

		$details = Yii::app()->getDb()->createCommand()
					  ->select( 'c.name, c.code, org.title' )
					  ->from( LearningCourse::model()->tableName() . ' c' )
					  ->join( LearningOrganization::model()->tableName() . ' org', 'c.idCourse=org.idCourse AND org.idOrg=:idOrg', array(
						  ':idOrg' => $idOrg
					  ) )
					  ->queryRow();

		if(is_array($details))
		{
			$subject = Yii::t( 'aws_china', 'Learning object replica is ready' );
			$body    = Yii::t( 'aws_china', 'The replica of the {lo_name} object for course {course_name} ({course_code}) has been completed', array(
				'{lo_name}'     => $details['title'],
				'{course_name}' => $details['name'],
				'{course_code}' => $details['code'],
			) );
		}
		else
		{
			$org_name = Yii::app()->getDb()->createCommand()
						->select('title')
						->from(LearningOrganization::model()->tableName())
						->where('idOrg = :idOrg', array(':idOrg' => $idOrg))
						->queryScalar();

			$subject = Yii::t('aws_china', 'Learning object replica is ready');
			$body = Yii::t('aws_china', 'The replica of the {lo_name} object has been completed', array(
				'{lo_name}' => $details['title']
			));
		}

		$senderEmail = CoreSetting::get( 'sender_event', 'noreply@docebo.com' );

		$targetEmails = Yii::app()->user->getGodadminsEmails();
		$targetEmails = array_values( array_filter( $targetEmails ) );

		if( empty( $targetEmails ) ) {
			Yii::log( 'No godadmin emails found to notify', CLogger::LEVEL_ERROR );

			return;
		}

		try {
			$mm = new MailManager();
			$mm->mail( $senderEmail, $targetEmails, $subject, $body );
		} catch ( Exception $e ) {
			Yii::log( 'Can not notify godadmins due to: ' . $e->getMessage(), CLogger::LEVEL_ERROR );
		}
	}
}