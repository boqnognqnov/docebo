<?php

/**
 * Plugin for SkymeetingApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class SkymeetingApp extends DoceboPlugin {


	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'SkymeetingApp';

	/**
	 * Returns the settings page (admin app)
	 */
	public static function settingsUrl() {
		return Docebo::createAdminUrl('SkymeetingApp/skymeetingApp/settings');
	}

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {
		parent::activate();

		// activate table
		$sql = "CREATE TABLE IF NOT EXISTS `conference_skymeeting_meeting` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`id_room` int(11) NOT NULL COMMENT 'Id of the LMS room (local)',
				`name` varchar(255) NOT NULL,
				`session_id` bigint(20) unsigned NOT NULL COMMENT 'OnSync session id',
				`friendly_id` varchar(32) NOT NULL COMMENT 'Friendly string at the end of session URL',
				`logout_url` varchar(512) DEFAULT '',
				`max_participants` int(11) DEFAULT '-1',
				PRIMARY KEY (`id`),
				KEY `id_room` (`id_room`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE='utf8_general_ci';";
		$command = Yii::app()->db->createCommand($sql);
		$command->execute();
	}


	public function deactivate() {
		parent::deactivate();
	}


}
