<?php

/**
 * Docebo API documentor.
 * It produces Swagger 2.0 compliant JSON documentation.
 */
class DoceboSwaggerDocumentor extends CComponent
{

    /**
     * The table of all available API modules and actions
     * @var array
     */
    private $_apiTable = array();

    /**
     * Initializes the apis map.
     */
    public function _buildModuleTable()
    {
        // Build the table of apis
        // Phase 1. Discover core api modules
        $files = CFileHelper::findFiles(Yii::app()->getBasePath() . '/components', array('fileTypes' => array('php'), 'level' => 0));
        foreach ($files as $file) {
            include_once($file);
            if (preg_match('/(.*)\.php$/', basename($file), $matches)) {
                $className = $matches[1];
				$moduleName = strtolower(substr($className, 0, strpos($className, "ApiModule")));

                // Check if the api module has swagger documentation tags and add it to the table
                $this->_checkApiModuleDescription($className, $moduleName);
            }
        }

        // Phase 2. Collect api modules exposed by plugins plugin
        $pluginModules = array();
        Yii::app()->event->raise('PublishApiModules', new DEvent($this, array('plugin_modules' => &$pluginModules)));
        foreach ($pluginModules as $module) {
            if ($module instanceof ApiModule) {
				$className = get_class($module);
				$moduleName = strtolower(substr($className, 0, strpos($className, "ApiModule")));
                $this->_checkApiModuleDescription($className, $moduleName);
			}
        }
    }

    /**
     * Analyzes the given class and adds to module to the set of documented api modules
     * @param $className The class to analyze
     */
    private function _checkApiModuleDescription($className, $moduleName)
    {
        $reflector = new ReflectionClass($className);
        $doc = $reflector->getDocComment();
        if ($doc) {
            // Extract class tags from docblock
            if (preg_match_all('/@(\w+)\s+(.*)\r?\n/m', $doc, $matches)) {
                $result = array_combine($matches[1], $matches[2]);
                if ($result['description']) {
                    $this->_apiTable[$moduleName] = array(
						'path' => $moduleName,
                        'description' => $result['description'],
					);
                }
            }
        }
    }

    /**
     * Returns the response message for a "Resource Listing" request
     */
    public function resourceListing()
    {
        $response = array(
            'swagger' => "2.0",
			'basePath' => '/api',
			'tags' => array(),
			'info' => array(
				'title' => '',
				'version' => '2.0',
			),
			'paths' => array(),
			'securityDefinitions' => array(
				'docebo_oauth' => array(
					'type' => 'oauth2',
					'flow' => 'implicit', //application = client credentials
					'authorizationUrl' =>  Docebo::getRootBaseUrl(true) . '/oauth2/authorize',
					'scopes' => array(
						'api' => 'Common scope, used by the whole API',
					)
				),
			),
			'security' => array(
				array(
					'docebo_oauth' => array(
						'api',
					),
				)
			),
        );

        // Build the api modules table
        $this->_buildModuleTable();

		/**
		 * When parsing all the modules, the "definitions"(the old 'models' key) need to be set in $response, to avoid
		 * passing them through a few arrays, we are just going to use $GLOBALS
		 */
		$GLOBALS['definitions'] = array();

        // Return resource listing in Swagger format
        foreach ($this->_apiTable as $path => $module) {
			$response['paths'] = array_merge($response['paths'],$this->pathInformation($path));
			//$path matches the tag used in the action
			$response['tags'][] = array(
				'name' => $module['path'],
				'description' => $module['description'],
			);
        }

		$response['definitions'] = $GLOBALS['definitions'];
		unset($GLOBALS['definitions']);

		//final checkups, convert some arrays to objects
		foreach($response['definitions'] as $key => $definition){
			//in definitions, we can't have empty arrays, because CJSON::encode will turn them into plain arrays
			//but that is not acceptable for swagger 2.0
			if(!count($definition)){
				$response['definitions'][$key] = new StdClass();
			}
		}

        return $response;
    }

	/**
	 * Returns the 'paths' part of the response object for the specific module
	 */
	public function pathInformation($modulePath)
	{
		// Prepare the swagger response
		$pathInfo = array();

		// Load api modules
		$this->_getApiModuleActions($modulePath);

		// Loop through all actions of this module
		foreach ($this->_apiTable[$modulePath]['actions'] as $action) {
			//for now, the API uses POST only, so its hardcoded, since its not provided as a parameter
			$pathInfo["/" . $modulePath . "/" . $action['path']]['post'] = array(
				"tags" => array($modulePath),
				"operationId" => $modulePath . "." . $action['path'],
				"produces" => array($action['return_type']),
				"parameters" => is_array($action['parameters']) ? $action['parameters'] : array(),
				"summary" => $action['summary'],
				"description" => isset($action['notes']) ? $action['notes'] : '',
				"responses" => is_array($action['error_responses']) ? $action['error_responses'] : array(),
			);
		}

		return $pathInfo;
	}

    /**
     * Returns the model name from the give string
     * @param $name The string to convert (e.g. ext_field -> ExtField)
     */
    private function generateModelName($name) {
        $modelName = '';
        $parts = explode('_', $name);
        foreach($parts as $part)
            $modelName .= ucfirst($part);

        return $modelName;
    }

    /**
     * Retunrn the list of module actions from the given module
     * @param $modulePath The path of the module to load
     */
    private function _getApiModuleActions($modulePath) {

        // Trigger hook to let plugins return their own module for this api request
        $module = null;
		$event = new DEvent($this, array('moduleName' => $modulePath));
		Yii::app()->event->raise('LoadApiModule', $event);
		if(isset($event->return_value['targetApiComponent']) && $event->return_value['targetApiComponent'])
			$module = $event->return_value['targetApiComponent'];

        // If no plugin provided a component for the requested api,
        // try to load the default API module corresponding to the request action
        if (!$module) {
            $moduleName = ucfirst(strtolower($modulePath)) . "ApiModule";
            $module = Yii::createComponent(array('class' => $moduleName));
        }

        // Was the api module successfully loaded?
        if (!$module || !is_object($module) || !($module instanceof ApiModule))
            throw new CHttpException(404, 'Invalid API module loaded');

        // Start using reflection
        $className = get_class($module);
        $reflector = new ReflectionClass($className);

        // Find class names up the hierarchy until ApiModule is reached
        // (useful to filter methods)
        $parentClass = $reflector;
        $componentClassNames = array();
        $name = $className;
        while($name != "ApiModule") {
            $componentClassNames[] = $name;
            $parentClass = $parentClass->getParentClass();
            $name = $parentClass->getName();
        }

        // Use reflection to inspect module actions
        foreach($reflector->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
            if(in_array($method->class, $componentClassNames)) {
				$docBlock = $method->getDocComment();
				//skip if there is no @summary
				if(strpos($docBlock,'@summary') === false)
					continue; //skip this method, its probably not an API method; PS: move this to a better place to save time

                // Parse method docblock and extract swagger tags
				$actionName = preg_replace('/^action/i', '', $method->getName());
				$controllerName = preg_replace('/ApiModule$/i','',$reflector->name);
                $returnObjectModel = $this->generateModelName($controllerName . ucfirst($actionName)).'Response';

				$action = array(
                    'path' => $actionName,
                    'return_type' => 'application/json',
                );

				$action['error_responses']['200'] = array(
					'description' => 'Operation Successful',
					'schema' => array(
						'$ref' => '#/definitions/' . $returnObjectModel,
					)
				);

				$paramObject = array(
					"in" => "body",
					"name" => 'body', //for now hidden by css
					"description" => 'hidden', // for now hidden by css
					"required" => false,
					'schema' => array()
				);

				$schemaName = $this->generateModelName($method->class . "_" . $method->getName()) . "Schema";
				$GLOBALS['definitions'][$schemaName] = array(
					'type'=>'object',
				);
				$paramObject['schema'] = array('$ref' => '#/definitions/' . $schemaName);
				$action['parameters'][] = $paramObject;

				$modelsStack = array();
                if (preg_match_all('/@(\w+)[ \t]*(.*)/m', $docBlock, $matches)) {

					foreach($matches[1] as $index => $param){
						$param = strtolower($param);
						$paramLine = $matches[2][$index];
						switch($param){
							case 'summary': // Short summary of the api action
								$modelsStack = array();
								$action['summary'] = $matches[2][$index];
								break;
							case 'notes': // Extended notes on the action (can be multiline)
								$modelsStack = array();
								if(isset($action['notes']))
									$action['notes'] .= $matches[2][$index];
								else
									$action['notes'] = $matches[2][$index];
								break;
							case 'status': // Status code responses
								$modelsStack = array();
								$statusLine = $matches[2][$index];
								if(preg_match('/(\w+)[ \t]*(.*)$/', $statusLine, $statusMatches)){
									$action['error_responses'][(int)$statusMatches[1]] = array(
										'description' => $statusMatches[2]
									);
								}
								break;
							case 'response':
								$modelsStack = array();
								$this->buildModel($modelsStack, $returnObjectModel, $paramLine, $method);
								break;
							case 'parameter':
								$modelsStack = array();
								$this->buildModel($modelsStack, $schemaName, $paramLine, $method);
								break;
							case 'item':
								$currentModel = $modelsStack[count($modelsStack) - 1];
								$this->buildModel($modelsStack, $currentModel, $paramLine, $method);
								break;
							case 'end':
								array_pop($modelsStack);
						}
					}
				}

                if($action['summary']) {
                    $action['error_responses'][500] = array(
						'description' => 'Internal server error',
					);
                    $this->_apiTable[$modulePath]['actions'][] = $action;
                }

				//each method always returns a 'success' property at least
				$GLOBALS['definitions'][$returnObjectModel]['required'][] = 'success';
				$GLOBALS['definitions'][$returnObjectModel]['properties']['success'] = array(
					'type' => 'boolean',
					'description' => 'True if operation was successfully completed'
				);
            }
        }
    }



	/**
	 * Method is used to build both body '@parameter', '@item' and '@response' Objects/Models
	 *
	 * === Method not suitable for formData Parameter, only for BodyParameter & Response because they share the same structure ===
	 *
	 * @param $modelsStack
	 * @param $object
	 * @param $paramLine
	 * @param $method
	 */
	private function buildModel(&$modelsStack, $object, $paramLine, $method){

		if(preg_match('/(\w+)[ \t]*\[(\w+\(*\w+\)*),\s+(\w+)\]\s*(.*)$/', $paramLine, $responseMatches)){
			$fieldName = $responseMatches[1];
			$type = $this->parseType($responseMatches[2]);
			$required = $responseMatches[3];
			$description = trim($responseMatches[4]);
			$modelName = null;
			$arrayType = null;

			if($required == 'required')
				$GLOBALS['definitions'][$object]['required'][] = $fieldName;

			if(preg_match('/array\((\w+)\).*$/', $type, $typeMatches)){
				$arrayType = $typeMatches[1];

				$GLOBALS['definitions'][$object]['properties'][$fieldName] = array(
					'type' => 'array',
					'description' => $description,
					'items' => array(
						'type' => $arrayType
					)
				);
				return;
			}

			switch($type){
				case 'array':
					$modelName = $this->generateModelName($method->class . "_" . $method->getName() . "_" . $fieldName) . "Model";

					$GLOBALS['definitions'][$modelName] = array();

					$GLOBALS['definitions'][$object]['properties'][$fieldName] = array(
						'type' => 'array',
						'description' => $description,
						'items' => array(
							'$ref' => '#/definitions/' . $modelName,
						),
					);

					array_push($modelsStack, $modelName);
					break;
				case 'object':
					$modelName = $this->generateModelName($method->class . "_" . $method->getName() . "_" . $fieldName) . "Model";

					$GLOBALS['definitions'][$modelName] = array();

					$GLOBALS['definitions'][$object]['properties'][$fieldName] = array(
						'$ref' => '#/definitions/' . $modelName,
						'description' => $description, //produces a warning according to http://editor.swagger.io, but can be(and is) used by swagger UI
					);

					array_push($modelsStack, $modelName);
					break;
				case 'date':
				case 'datetime':
					//datetime can have its own case, but the format is not the same as the one we use, its format is: "2016-05-19T07:40:40.656Z"
					//based on RFC3339 ( http://xml2rfc.ietf.org/public/rfc/html/rfc3339.html#anchor14 )
					$GLOBALS['definitions'][$object]['properties'][$fieldName] = array(
						'type' => 'string',
						'format' => 'date',
						'description' => $description,
					);
					break;
                case 'float':
                    $GLOBALS['definitions'][$object]['properties'][$fieldName] = array(
                        'type' => 'number',
                        'format' => 'float',
                        'description' => $description,
                    );
                    break;
                case 'double':
                    $GLOBALS['definitions'][$object]['properties'][$fieldName] = array(
                        'type' => 'number',
                        'format' => 'double',
                        'description' => $description,
                    );
                    break;
				default:
					$GLOBALS['definitions'][$object]['properties'][$fieldName] = array(
						'type' => $type,
						'description' => $description,
					);
					break;
			}
		}
	}

	private function parseType($type){

		switch($type){
			case 'url':
			case 'time':
			case 'mixed':
				return 'string';

            case 'decimal':
                return 'double';

            case 'bool':
				return 'boolean';

			case 'int':
				return 'integer';

			case "file":
				return "string";
		}

		return $type;
	}
}