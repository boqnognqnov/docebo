<?php

/**
 * Main Swagger UI controller
 * Class DefaultController
 */
class DefaultController extends CController
{
    /**
     * Use module based layout
     */
    public function init()
    {
        $this->layout = '/layouts/base';
    }

    /**
     * Main Swagger UI controller
     */
    public function actionIndex()
    {
        $secret = Yii::app()->db->createCommand(
            'SELECT client_secret FROM ' . OauthClients::model()->tableName() . ' WHERE client_id = :clientId'
        )->queryScalar(array(
            ':clientId' => OauthClients::SWAGGER_CLIENT_ID
        ));
        $this->render('index', array(
            'clientSecret' => $secret,
        ));
    }

    /**
     * Ajax method that returns the json swagger doc
     */
    public function actionView()
    {
        $response = $this->getOpenApiV2Json();

        header('Content-Type: application/json; charset="UTF-8"');
        echo CJSON::encode($response);
        Yii::app()->end();
    }

    /**
     * Used by swagger UI 2.1.4
     * Returns a valid json based on OpenApi v2.0
     *
     * Use this to check the returned json: http://editor.swagger.io/#/
     */
    public function getOpenApiV2Json()
    {
        try{
            $documentor = new DoceboSwaggerDocumentor();
            return $documentor->resourceListing();
        }catch (Exception $e){
            return array('error' => $e->getMessage());
        }
    }

    /**
     * Authentication handler, parses the response from the return url
     */
    public function actionO2c(){
        $this->render("o2c");
    }
}