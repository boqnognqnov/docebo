<?php

/**
 * Main Swagger Module class
 * Class SwaggerModule
 */
class SwaggerModule extends CWebModule
{
    /**
     * @var string Path to assets for this module
     */
    private $_assetsUrl;

    /**
     * Load module components and models
     */
    public function init()
	{
		$this->setImport(array(
			'swagger.models.*',
			'swagger.components.*',
		));
	}

	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('swagger.assets'));
		}

		return $this->_assetsUrl;
	}
}
