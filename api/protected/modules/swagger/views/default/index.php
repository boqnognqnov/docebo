<div id="message-bar" class="swagger-ui-wrap">&nbsp;</div>
<div id="swagger-ui-container" class="swagger-ui-wrap"></div>

<script type="text/javascript">
    $(function () {
        window.swaggerUi = new SwaggerUi({
            validatorUrl: false,
            url: '<?=  Docebo::getRootBaseUrl(true); ?>/api/docs/view',
            dom_id: "swagger-ui-container",
            supportedSubmitMethods: ['get', 'post', 'put', 'delete', 'patch'],
            oauth2RedirectUrl: '<?=  Docebo::getRootBaseUrl(true); ?>/api/docs/o2c',
            onComplete: function (swaggerApi, swaggerUi) {
                if (typeof initOAuth == "function") {
                    initOAuth({
                        clientId: "<?= OauthClients::SWAGGER_CLIENT_ID ?>",
                        clientSecret: "<?= $clientSecret ?>",
                        realm: "resource",
                        appName: "Docebo Api",
                        scopeSeparator: ",",
                        additionalQueryStringParams: {}
                    });
                }

                //add button if not in iframe
                if(window.self === window.top) {
                    $('.info_title').after("<div class='input'><a id='authenticate' href='#'>Authenticate</a></div>");
                    $("#authenticate").click(function () {
                        handleLogin();
                    });
                }

                $('pre code').each(function (i, e) {
                    hljs.highlightBlock(e)
                });

            },
            onFailure: function (data) {
                log("Unable to Load SwaggerUI");
            },
            docExpansion: "none",
            jsonEditor: false,
            apisSorter: "alpha",
            defaultModelRendering: 'model', //or schema
            showRequestHeaders: false
        });

        window.swaggerUi.load();

        function log() {
            if ('console' in window) {
                console.log.apply(console, arguments);
            }
        }

        // === === === Quality of life improvements === === === //

        // submit form when ctrl+enter is pressed //
        $(document).on('keypress', ".sandbox textarea.body-textarea", function (event) {
            if ((event.which == 13 || event.which == 10) && event.ctrlKey) {
                $(event.target).closest("form").submit();
            }
        });
    })
</script>