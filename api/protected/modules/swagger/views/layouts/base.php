<?php /* @var $this DefaultController */
$assetsUrl = $this->module->assetsUrl;
?>
<!DOCTYPE html>
<html>
<head>
    <title>Docebo API documentation</title>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href='<?= $assetsUrl ?>/css/reset.css' media='screen' rel='stylesheet' type='text/css'/>
    <link href='<?= $assetsUrl ?>/css/screen.css' media='screen' rel='stylesheet' type='text/css'/>
    <link href='<?= $assetsUrl ?>/css/doceboCustom.css' rel='stylesheet' type='text/css'/>
    <script src='<?= $assetsUrl ?>/lib/jquery-1.8.0.min.js' type='text/javascript'></script>
    <script src='<?= $assetsUrl ?>/lib/jquery.slideto.min.js' type='text/javascript'></script>
    <script src='<?= $assetsUrl ?>/lib/jquery.wiggle.min.js' type='text/javascript'></script>
    <script src='<?= $assetsUrl ?>/lib/jquery.ba-bbq.min.js' type='text/javascript'></script>
    <script src='<?= $assetsUrl ?>/lib/handlebars-2.0.0.js' type='text/javascript'></script>
    <script src='<?= $assetsUrl ?>/lib/underscore-min.js' type='text/javascript'></script>
    <script src='<?= $assetsUrl ?>/lib/backbone-min.js' type='text/javascript'></script>
    <script src='<?= $assetsUrl ?>/lib/swagger-ui.min.js' type='text/javascript'></script>
    <script src='<?= $assetsUrl ?>/lib/highlight.7.3.pack.js' type='text/javascript'></script>
    <script src='<?= $assetsUrl ?>/lib/jsoneditor.min.js' type='text/javascript'></script>
    <script src='<?= $assetsUrl ?>/lib/marked.js' type='text/javascript'></script>
    <script src='<?= $assetsUrl ?>/lib/swagger-oauth.js' type='text/javascript'></script>
</head>
<body class="swagger-section">
<?= $content ?>
</body>
</html>