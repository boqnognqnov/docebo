<?php

/**
 * Pushnotification API Component
 * @description Set of APIs to manage push notifications
 */
class PushnotificationApiModule extends ApiModule {


	/**
	 * Returns the requested security level for the passed action
	 * @param $actionName The name of the action
	 * @return string The requested security level
	 */
	public function getSecurityLevel($actionName) {

		switch($actionName) {
			case "register" :
			case "unregister" :
			case "authorization" :
				return self::$SECURITY_LIGHT;
				break;
			default:
				return self::$SECURITY_STRONG;
		}
	}


	/**
	 * @summary Return device authorization info from a given user
	 *
	 * @parameter id_user [integer, required] idst of the user
	 *
	 * @response devices [array, optional] A list of devices registered for the current user
     *      @item os [string, required] Operating system
     *      @item device_token [string, required] Device token string
     *      @item registration_id [integer, required] Push notification registration id
	 *
	 * @status 201 Invalid input data provided or User not found
	 */
	public function get() {

		//read input
		$id_user = $this->readParameter('id_user');
		if (!$id_user) {
			throw new CHttpException(201, 'Invalid user input data provided.');
		}
		$user = CoreUser::model()->findByPk($id_user);
		if (!$user) {
			throw new CHttpException(201, 'User not found.');
		}
		$devices = MobilePushnotification::model()->findAllByAttributes(array(
			'id_user' => $id_user,
		));

		$output = array();
		foreach ($devices as $device) {
			$output['devices'][] = array(
				'os' => $device->os,
				'device_token' => $device->device_token,
				'registration_id' => $device->registration_id,
			);
		}
		return $output;
	}


	/**
	 * @summary Register notification info for specified user
	 *
	 * @parameter id_user [integer, required] idst of the user
	 * @parameter os [string, required] operative system used for request
	 * @parameter device_token [string, required] token code of the used device
	 *
     * @response sender_id [integer, required] Sender ID
     *
	 * @status 201 Invalid input data provided
	 */
	public function register() {

		//read input
		$idUser = $this->readParameter('id_user');
		$os = $this->readParameter('os');
		$deviceToken = $this->readParameter('device_token');

		//validate input
		if (!$idUser) {
			throw new CHttpException(201, 'Invalid user input data provided.');
		}
		$user = CoreUser::model()->findByPk($idUser);
		if (!$user) {
			throw new CHttpException(201, 'User not found.');
		}

		if (!MobilePushnotification::isValidOS($os)) {
			throw new CHttpException(201, 'Invalid os input data provided.');
		}

		if (!$deviceToken) {
			throw new CHttpException(201, 'Invalid device input data provided.');
		}

		//do stuff
		$ar = new MobilePushnotification();
		$ar->id_user = $idUser;
		$ar->os = $os;
		$ar->device_token = $deviceToken;
		if (!$ar->save()) {
			throw new CHttpException(500, 'Error while saving notification data.');
		}

		// needed for android registration
		$sender_id = ($os == MobilePushnotification::OS_ANDROID ? Yii::app()->params['gcm_sender_id'] : '');

		$output = array('sender_id' => (int)$sender_id);
		return $output;
	}


	/**
	 * @summary Unregister notification info for specified user
	 *
	 * @parameter id_user [integer, required] idst of the user
	 * @parameter os [string, required] operative system used for request
	 * @parameter device_token [string, required] token code of the used device
	 *
	 * @status 201 Invalid input data provided
	 */
	public function unregister() {

		//read input
		$idUser = $this->readParameter('id_user');
		$os = $this->readParameter('os');
		$deviceToken = $this->readParameter('device_token');

		//validate input
		if (!$idUser) {
			throw new CHttpException(201, 'Invalid user input data provided.');
		}
		$user = CoreUser::model()->findByPk($idUser);
		if (!$user) {
			throw new CHttpException(201, 'User not found.');
		}

		if (!MobilePushnotification::isValidOS($os)) {
			throw new CHttpException(201, 'Invalid os input data provided.');
		}

		if (!$deviceToken) {
			throw new CHttpException(201, 'Invalid device input data provided.');
		}

		//do stuff
		$ar = MobilePushnotification::model()->findByAttributes(array(
			'device_token' => $deviceToken,
			'os' => $os,
			'id_user' => $idUser
		));
		if (!$ar || !$ar->delete()) {
			throw new CHttpException(500, 'Error while performing operation.');
		}

		$output = array();
		return $output;
	}


	/**
	 * @summary Read device authorization string
	 *
	 * @parameter os [string, required] operative system used for request
	 * @parameter device_token [string, required] token code of the used device
	 * @parameter registration_id [string, required] registration token for the device
	 *
	 * @status 201 Invalid input data provided
	 */
	public function authorization() {

		//read input
		$os = $this->readParameter('os');
		$deviceToken = $this->readParameter('device_token');
		$registrationId = $this->readParameter('registration_id');

		//validate input
		if (!MobilePushnotification::isValidOS($os)) {
			throw new CHttpException(201, 'Invalid os input data provided.');
		}

		if (!$deviceToken || $registrationId) {
			throw new CHttpException(201, 'Invalid device input data provided.');
		}

		//do stuff: retrieve registration record and add device registration id
		$ar = MobilePushnotification::model()->findByAttributes(array(
			'device_token' => $deviceToken,
			'os' => $os
		));
		if (!$ar) {
			throw new CHttpException(500, 'Error while retrieving registration info.');
		}
		$ar->registration_id = $registrationId;
		if (!$ar->save()) {
			throw new CHttpException(500, 'Error while performing operation.');
		}

		$output = array();
		return $output;
	}

}
