<?php

/**
 * Base API Module class
 */
abstract class ApiModule extends CComponent {
	/* Available security levels */

	public static $SECURITY_NONE = 'none';
	public static $SECURITY_LIGHT = 'light';
	public static $SECURITY_STRONG = 'strong';

	const DEFAULT_COUNT_LIMIT = 500;

	/**
	 * @var CoreUser Currently logged in user
	 */
	protected $user;

	/**
	 * @var array This is filled with the datapassed to the api via json payload, if false the input wasn't a json
	 */
	protected $apiInput = false;

	/**
	 * API Component initialization module
	 * @param $user The currently logged in user (if it can be identified)
	 */
	public function init($user) {
		$this->user = $user;
		// to support json payload we are cheking for it and then use it for the params
		// this is done here because some params are needed for the
		$this->manageJsonPaylod();
	}


	/**
	 * This is
	 * @param CoreUser $user the model for the user to associate with the lms
	 */
	public function setUser($user) {
		$this->user = $user;
		if($user instanceof CoreUser) {
			if(Yii::app()->user->getIsGuest() || (Yii::app()->user->id !== $user->idst)) {
				$ui = new DoceboUserIdentity($user->userid, null);
				if ($ui->authenticateWithNoPassword())
					Yii::app()->user->login($ui);
			}
		}
	}


	/**
	 * This method is used in case of json payload (in http raw post data) in that case the raw is taken and decode
	 * and the result is then saved for later usage by the api modules
	 */
	protected function manageJsonPaylod() {

		$postdata = file_get_contents("php://input");
		if ($postdata) $this->apiInput = CJSON::decode($postdata);
	}

	/**
	 * This method will return the params returned from the json file
	 */
	public function getApiInput() {

		return $this->apiInput;
	}

	/**
	 * Returns the requested security level for the passed action
	 * @param $actionName The name of the action
	 * @return string The requested security level
	 */
	public function getSecurityLevel($actionName) {
		return self::$SECURITY_STRONG;
	}

	/**
	 * Returns the allow oauth2 scopes for a certain action name
	 * @param $actionName
	 */
	public function getOauth2Scopes($actionName) {
		return array('api');
	}

	/**
	 * Read input parameters by parameter name (key)
	 * @param string $key
	 * @param null $default
	 * @return mixed
	 */
	public function readParameter($key, $default = null) {

		// json payload
		if ($this->apiInput != false) {

			// return json decoded data or default if not set
			return isset($this->apiInput[$key]) ? $this->apiInput[$key] : $default;
		}
		// POST var result
		return Yii::app()->request->getParam($key, $default);
	}

    /**
     * Read input parameters
     * @return array
     */
    public function readParameters() {

        // json payload
        if ($this->apiInput != false) {

            // return json decoded data
            return $this->apiInput;
        }

        return $_POST;
    }

	/**
	 * Returns errors for AR models
	 * @param $model
	 * @return string
	 */
	protected function errorSummary($model) {
		$content='';
		if(!is_array($model))
			$model=array($model);

		foreach($model as $m) {
			foreach($m->getErrors() as $errors) {
				foreach($errors as $error) {
					if($error!='')
						$content.="$error - ";
				}
			}
		}
		
		if(rtrim($content, "- ")){
			$content = rtrim($content, "- ");
		}

		return  $content;
	}

	/**
	 * Reads the CSV file from the specified url or from the HTTP POST body
	 * @param $type string The type of the CSV file
	 * @return CUploadedFile
	 */
	protected function getCSVFile($type = 'users') {
		$file_url = $this->readParameter('file_url', null);
		$file_upload = $this->readParameter('file_upload', null);
		$file = null; /* @var $file CUploadedFile */
		if($file_url) {
			// Load remote file into local temporary upload directory with random tmp file name
			$localCSVFileName = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $type . '_api_update_' . time() . '.csv';
			$ch = curl_init($file_url);
			$fp = fopen($localCSVFileName, "w");
			if (!$ch || !$fp)
				throw new CHttpException(400, 'Error while reading remote CSV file');
			curl_setopt($ch, CURLOPT_FILE, $fp);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_exec($ch);
			curl_close($ch);
			fclose($fp);

			$file = new CUploadedFile($type.'.csv', $localCSVFileName, CFileHelper::getMimeType($localCSVFileName), filesize($localCSVFileName), UPLOAD_ERR_OK);

		} else if ($file_upload) {
			$file = CUploadedFile::getInstanceByName($file_upload);
			if(!$file)
				throw new CHttpException(400, 'Error while reading uploaded CSV file');
		} else
			throw new CHttpException(203, "Missing both file_url and file_upload params");

		return $file;
	}
}
