<?php

/**
 * App7020 API Component
 * @description Set of 70/20 API endpoints 
 */
class App7020ApiModule extends ApiModule
{

    /**
     * Returns the requested security level for the passed action
     *
     * @param $actionName The
     *            name of the action
     * @return string The requested security level
     */
    public function getSecurityLevel($actionName)
    {
        return self::$SECURITY_LIGHT;
    }

    /**
     * @summary Inform API that content material upload just has been completed
     *
     * @parameter type [string, required] Content type uploaded: video, image - required if status = 4 (create new content)
     * @parameter typeId [integer, required] Content type ID uploaded: 1 - video, 2- image, etc. - required if status = 4 (create new content)
     * @parameter filename [string, required] Randomized file name, original extension - required if status = 4 (create new content)
     * @parameter originalFilename [string, required] Full original filename - required if status = 4 (create new content)
     * @parameter title [string, required] Title of the content - required if status = 3 (update existing content)
     * @parameter description [string, required] Description of the content - required if status = 3 (update existing content)
     * @parameter topics [array, required] JSON encoded array of selected topic IDs the content to be assigned to - required if status = 3 (update existing content)
     * @parameter tags [array, optional] JSON encoded array of selected tag IDs the content to be assigned to
     * @parameter userId [integer, required] An ID of the logged user - required if status = 4 (create new content)
     * @parameter status [integer, required] 4 - if the content is sent without metadata; 3-if the content is sent with metadata and is ready for convertion
     * @parameter id [integer, optional] ID of the content which needs to be updated (from status 4 to status 3) and needs to be converted - required if status = 3 (update existing content)
     * @parameter idSource [integer, optional] ID of the source of the content (1-from web; 2-from Android; 3-from iOS) - required if status = 4 (create new content)
     *            
     *            @status 201 No filename specified
     *            @status 202 No original filename specified
     *            @status 203 No title specified
     *            @status 251 No description specified
     *            @status 205 No topics specified
     *            @status 252 No status specified
     *            @status 253 Invalid ststus specified
     *            @status 254 Invalid content ID
     *            @status 255 No user specified
     *            @status 256 Unknown content type
     *            @status 257 Unknown source type
     *            @status 206 Could not create model
     *            @status 207 Error while creating new content item
     *            @status 208 Error while updating the content item
     *            
     *@response contentId [integer, required] ID of the content in the LMS
     */
    public function uploadComplete()
    {
       
        $type = $this->readParameter('type', false);
        $typeId = $this->readParameter('typeId', false);
        $filename = $this->readParameter('filename', false);
        $originalFilename = $this->readParameter('originalFilename', false);
        $title = $this->readParameter('title', false);
        $description = $this->readParameter('description', false);
        $topics =json_decode($this->readParameter('topics', false));
        $tags = json_decode($this->readParameter('tags', false));
        $userId = $this->readParameter('userId', false);
        $status = $this->readParameter('status', false);
        $contentId = $this->readParameter('id', false);
        $sourceId = $this->readParameter('idSource', false);
        
        if (empty($filename) && $status == App7020Assets::CONVERSION_STATUS_FROM_MOBILE )
            throw new CHttpException(201, 'No filename specified');
        
        if (empty($originalFilename) && $status == App7020Assets::CONVERSION_STATUS_FROM_MOBILE)
            throw new CHttpException(202, 'No original filename specified');
        
        if (empty($title) && $status == App7020Assets::CONVERSION_STATUS_S3)
            throw new CHttpException(203, 'No title specified');
        
        if (empty($description) && $status == App7020Assets::CONVERSION_STATUS_S3)
            throw new CHttpException(251, 'No description specified');
        
        if (empty($topics) && $status == App7020Assets::CONVERSION_STATUS_S3)
            throw new CHttpException(205, 'No topics specified');
        
        if (empty($status))
            throw new CHttpException(252, 'No status specified');
        
        if ($status <> App7020Assets::CONVERSION_STATUS_FROM_MOBILE && $status != App7020Assets::CONVERSION_STATUS_S3)
            throw new CHttpException(253, 'Invalid status specified');
        
        if (empty($contentId) && $status == App7020Assets::CONVERSION_STATUS_S3)
            throw new CHttpException(254, 'Invalid content ID');
        
        if (empty($userId) && $status == App7020Assets::CONVERSION_STATUS_FROM_MOBILE)
            throw new CHttpException(255, 'No user specified');
        
        if ($typeId != App7020Assets::CONTENT_TYPE_VIDEO && $status == App7020Assets::CONVERSION_STATUS_FROM_MOBILE)
            throw new CHttpException(256, 'Unknown content type');
        
        if (empty($sourceId) && $status == App7020Assets::CONVERSION_STATUS_FROM_MOBILE)
            throw new CHttpException(257, 'Unknown source type');

        
        if($status == App7020Assets::CONVERSION_STATUS_FROM_MOBILE){
            //Create content and get the ID
            $contentObject = new App7020Assets();
            $contentObject->contentType = $typeId;
            $contentObject->conversion_status = $status;
            $contentObject->userId = $userId;
            $contentObject->filename = $filename;
            $contentObject->originalFilename = $originalFilename;
            $contentObject->idSource = $sourceId;
            $contentObject->viewCounter = 0;
            
            try{
                $contentObject->save(false);
            } catch (CHttpException $e){
                $response = array(
                    'contentId' => 0
                );
                return $response;
            }
            
            if (empty($contentObject->id) || $contentObject->hasErrors()) {
                if (empty($contentObject->id)) {
                    $message = 'Could not create content';
                    $errorId = 206;
                } else {
                    $message = 'Error while creating new content item';
                    $errorId = CLogger::LEVEL_ERROR;
                    Yii::log(var_export($content->errors, true), CLogger::LEVEL_ERROR);
                }
                Yii::log($message, CLogger::LEVEL_ERROR);
            
                //return an array with error message, error code and zero for ID
                $response = array(
                    'contentId' => 0,
                    'errorMessage' => $message,
                    'errorId' => $errorId
                );
                return $response;
            } else {
                $response = array(
                    'contentId' => $contentObject->id
                );
                
                return $response;
            }
        } 
        
        if($status == App7020Assets::CONVERSION_STATUS_S3){
            //update an existing content record
            $contentObject = App7020Assets::model()->findByPk($contentId);
            
            $contentObject->conversion_status = $status;
            $contentObject->title = $title;
            $contentObject->description = $description;
            $updateResult = $contentObject->update();
            
            
            
            if(!$updateResult){
                $message = 'Error while updating the content item';
                $errorId = 208;
                    
                Yii::log($message, CLogger::LEVEL_ERROR);
                
                //return an array with error message, error code and zero for ID
                $response = array(
                    'contentId' => 0,
                    'errorMessage' => $message,
                    'errorId' => $errorId
                );
                return $response;
            } 
            
            //Start Elastic service and proccessing data 
            $convertResult = self::_convertApiVideo($contentObject->id);
            
            if (!$convertResult){
                $response = array(
                    'contentId' => 0,
                    'errorMessage' => "Cannot convert the video",
                    'errorId' => 499
                );
                return $response;
            } 
            
            if(!empty($topics) && is_array($topics)){
            
                foreach ($topics as $topicId){
            
                    if(App7020TopicTranslation::model()->exists('idTopic = :topicId', array(':topicId' => $topicId))){
                        $topicContentObject = new App7020TopicContent();
                        $topicContentObject->idContent = $contentObject->id;
                        $topicContentObject->idTopic = $topicId;
                        $topicContentObject->save();
                    }
            
                }
            }
            
            //Connect tags with the newly created content
            if(!empty($tags) && is_array($tags)){
            
                foreach ($tags as $tagId){
                    if(App7020Tag::model()->exists('id = :tagId', array(':tagId' => $tagId))){
                        $tagsContentObject = new App7020TagLink();
                        $tagsContentObject->idContent = $contentObject->id;
                        $tagsContentObject->idTag = $tagId;
                        $tagsContentObject->save();
                    }
            
                }
            }
        }
        
        
       
        //Return positive response - the ID of the updated or newly created content
        $response = array(
            'contentId' => $contentObject->id
        );
            
        return $response;
        
        
        
        
    }

    /**
     * @summary Get Topics in the specified language as a Tree Array
     *
     * @parameter lang [string, optional] The Language in which topics to be returned ('en', 'it', 'bg',...)
     *            
     * @response topics [array, required] An array of topics in a node->children form
     */
    public function getTopics()
    {
        
        // Read language parameter and resolve the language to use
        $lang = $this->readParameter('lang', false);
        $defaultLanguage = Lang::getBrowserCodeByCode(Settings::get('default_language', 'english'));
        $lang = $lang ? $lang : $defaultLanguage;
        
        if (empty($lang)) {
            throw new CHttpException(201, 'Language not specified or not correctly resolved');
        }
        
        $response = array(
            'topics' => App7020TopicTree::getTopicTree($lang)
        );
        return $response;
    }

    /**
     * @summary Retrieve all available tags
     *
     * @parameter limit [integer, optional] Maximum number of tags to return
     *            
     *            @response tags [array, required]
     *            @item id [integer, required] Tag ID
     *            @item text [string, required] Tag Text
     */
    public function getTags()
    {
        $limit = $this->readParameter('limit', false);
        
        // Just get all tags
        $command = Yii::app()->db->createCommand()
            ->select('*')
            ->from('app7020_tag');
        
        if ($limit !== false) {
            $command->limit($limit);
        }
        
        $rows = $command->queryAll();
        
        $tags = array();
        foreach ($rows as $row) {
            $tags[] = array(
                'id' => $row['id'],
                'text' => $row['tagText']
            );
        }
        
        $response = array(
            'success' => true,
            'tags' => $tags
        );
        
        return $response;
    }

    /**
     * @summary Creates tag if not exist
     *
     * @paramstring tag Tag name
     *            
     * @return integer 0 (if tag exist), ID of the created tag
     */
    public function createTag()
    {
        $tagText = $this->readParameter('tag', false);
        
        if (empty($tagText))
            throw new CHttpException(209, 'Invalid tag');
        
        //Check for tagText duplication
        if(App7020Tag::model()->exists('tagText = :tagText', array(':tagText' => $tagText))){
            $response = array(
                'id' => 0, 
                'saved' => false
            );
            return $response;
        }
            
        
        $tagObject = new App7020Tag();
        $tagObject->tagText = $tagText;
        
        if (! $tagObject->save())
            throw new CHttpException(210, 'Cannot save the tag');
        
        //Return the ID of the newly created tag
        $response = array(
            'id' => $tagObject->id, 
            'saved' => true
        );
        
        return $response;
    }
    
    
    /**
     * @summary Converts video content and sets first thumbnail to active
     * 
     * @parameter id [integer, required] The ID of the video which needs to be converted
     * 
     * @return integer - 0 on fault, ID of the converted video on success
     */
    private static function _convertApiVideo($contentId){
        
           
        $contentObject = App7020Assets::model()->findByPk($contentId);
               
        if (empty($contentObject->id))
            throw new CHttpException(210, 'Cannot convert the content');

        //sent convert video command to Amazon
        $fileStorageObject = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020);
        $fileStorageObject->setKey($contentObject->filename);
        return $fileStorageObject->convertVideo($fileStorageObject->getKey());
        
    }
    
        
    /**
     * @summary Get the uploaded videos which are not finished yet and checks their status.
     *          If the video is right converted, update its status to "finished" and sets the first thumnail as active
     *          If something gone wrong, notify the caller that the video is not right converted   
     * @parameter contentIds [array, required] IDs of the contents with ststus 3
     * @response multitype positives - array of IDs of finished contents, negatives - array of IDs of contents which needs to be deleted
     */
    
    
    public function updateNonFinishedContent(){
        $contents = json_decode($this->readParameter('contentIds', false));
        
        if (!is_array($contents))
            throw new CHttpException(209, 'Invalid content array');
        
        $returnPositives = array();
        $returnNegatives = array();
        foreach ($contents as $contentId){
            $contentObject = App7020Assets::model()->findByPk($contentId);
            
            if($contentObject->id){
                //If the videos are right converted, get the first thumbnail, set it as active and mark the content as finished
                if($contentObject->conversion_status == App7020Assets::CONVERSION_STATUS_SNS_SUCCESS){
                    
                    $contentThumbs = $contentObject->thumbs;
                    
                    if(!empty($contentThumbs)){
                        $thumbModel = App7020ContentThumbs::model()->findByPk($contentThumbs[0]->id);
                        $thumbModel->is_active = 1;
                        $thumbModel->update();
                    }
                    
                    $contentObject->conversion_status = App7020Assets::CONVERSION_STATUS_FINISHED;
                    $contentObject->update();
                    $returnPositives[] = $contentId;
                }
                //If the videos are not converted successfully - delete the content files
                if($contentObject->conversion_status == App7020Assets::CONVERSION_STATUS_SNS_WARNING 
                    || $contentObject->conversion_status == App7020Assets::CONVERSION_STATUS_SNS_ERROR){
                    $returnNegatives[] = $contentId;
                    $a = self::_deleteContent($contentObject);
                    
                }
                        
            }
        }
        
        $response = array(
            'positives' => $returnPositives, 
            'negatives' => $returnNegatives
        );
        
        return $response;
                
    }
    
    
    /**
     * @summary Deletes a content files from Amazon server, local storage and DB
     *          
     * @parameter contentObject [object, required] App7020Content object
     * 
     */    
    private static function _deleteContent(App7020Assets $contentObject){
        //delete remote fiiles
        $storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020);
        preg_match('/^(.*)\.(.*)$/i', $contentObject->filename, $out);
        $storage->removeFolder($out[1]);
        //delete local files
        $file = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $contentObject->filename;
        @unlink($file);
        //delete the content object
        $contentObject->delete();
    }
    
    
    /**
     * @summary Sends an email to registered user with a link for recovery password proccess
     * @parameter email [string, required] The email of the user
     * @response integer 1 - if the email is sent successfully; 0 - if the email cannot be sent
     */
    public function resetPassword() {
        // Check input params
        $email = $this->readParameter('email', false);
        if (!$email || (filter_var($email, FILTER_VALIDATE_EMAIL) == false)) {
            $response = array(
                'result' => 0, 
                'errorMessage' => "Invalid email"
            );
            return $response;
        }
    
        // Try to load a user with the provided email
        $userModel = CoreUser::model()->findByAttributes(array('email' => $email));
        if (!$userModel) {
            $response = array(
                'result' => 0, 
                'errorMessage' => "The email not exists"
            );
            return $response;
        }
        
        $code = md5(mt_rand() . mt_rand());
        // Checking if the code generated already exists in core_pwd_recover
        $corePwdRecoverModel = CorePwdRecover::model()->findByPk($userModel->idst);
        
        if ($corePwdRecoverModel === NULL) {
            $corePwdRecoverModel = new CorePwdRecover();
            $corePwdRecoverModel->idst_user = $userModel->idst;
            $corePwdRecoverModel->random_code = $code;
            $corePwdRecoverModel->request_date = Yii::app()->localtime->toLocalDateTime();
            $corePwdRecoverModel->save();
        
        } else {
            $corePwdRecoverModel->random_code = $code;
            $corePwdRecoverModel->request_date = Yii::app()->localtime->toLocalDateTime();
            $corePwdRecoverModel->update();
        }
        
        
        
        // Get the link, but prevent Url manager to strip "release", if any
        $oldStripRelease = Yii::app()->urlManager->getStripRelease();
        
        Yii::app()->urlManager->setStripRelease(false);
        
        $link = Docebo::createAbsoluteLmsUrl('user/recoverPwd', array('code' => $code));
        
        Yii::app()->urlManager->setStripRelease($oldStripRelease);
        
        $fromAdminEmail = array(Settings::get('mail_sender', 'noreply@docebo.com'));
        $toEmail = array($userModel->email);
        $subject = Yii::t('register', '_LOST_PWD_TITLE');
        
        $body = Yii::t('register', '_LOST_PWD_MAILTEXT', array(
            '[link]' => $link,
            '[username]' => $userModel->getClearUserId()
        ));
        
        $mm = new MailManager();
        $mm->setContentType('text/html');
        
        try {
            $result = $mm->mail($fromAdminEmail, $toEmail, $subject, $body);
            $response = array(
                'result' => $result
            );
            return $response;
            
        } catch (Swift_RfcComplianceException $e) {
            $error = Yii::t('standard', '_OPERATION_FAILURE');
            $response = array(
                'result' => 0, 
                'errorMessage' => $error
            );
            return $response;
        }
    }
    
    
    /**
     * @summary Deletes the requested content 
     * @parameter id [integer, required] The ID of the requested content
     * @response integer The ID of the deleted content if the deletion proccess has been successfully done; 0 if something goes wrong with an error message
     */
    public function deleteApiContent(){
        
        $contentId = $this->readParameter('id', false);
        
        if (empty($contentId)){
            $response = array(
                'id' => 0,
                'errorMessage' => "Invalid content ID "
            );
            return $response;
        }
        
        $contentObject = App7020Assets::model()->findByPk($contentId);
        if(!$contentObject->id){
            $response = array(
                'id' => 0,
                'errorMessage' => "Invalid content"
            );
            return $response;
        }
        
        try {
            self::_deleteContent($contentObject);
            $response = array(
                'id' => $contentId,
                'errorMessage' => "Invalid content"
            );
            return $response;
        } catch (CHttpException $e) {
            $response = array(
                'id' => $contentId,
                'errorMessage' => "Cannot delete the requested content"
            );
            return $response;
        }
        
    }
}
