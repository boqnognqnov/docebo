<?php

/**
 * Course API Component
 * @description Set of APIs to manage courses of the LMS
 */
class CourseApiModule extends ApiModule {

    /**
     * Course user level mapping
     * @param $level
     * @return bool|int
     */
    protected static function _getCourseUserLevelId($level) {
		switch(strtolower($level)) {
			case 'guest': { return LearningCourseuser::$USER_SUBSCR_LEVEL_GUEST; } break;
			case 'ghost': { return LearningCourseuser::$USER_SUBSCR_LEVEL_GHOST; } break;
			case 'tutor': { return LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR; } break;
			case 'coach': { return LearningCourseuser::$USER_SUBSCR_LEVEL_COACH; } break;
			case 'mentor': { return LearningCourseuser::$USER_SUBSCR_LEVEL_MENTOR; } break;
			case 'instructor': { return LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR; } break;
			case 'administrator': { return LearningCourseuser::$USER_SUBSCR_LEVEL_ADMIN; } break;
			case 'student':
			default: { return LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT; } break;
		}
		return false;
	}

    /**
     * Enrollment status mapping
     * @param $status
     * @return bool|int
     */
    public static function _getCourseUserStatusId($status) {
		switch (strtolower($status)) {
			case 'ab-initio': { return LearningCourseuser::$COURSE_USER_SUBSCRIBED; } break;
			case 'in itinere': { return LearningCourseuser::$COURSE_USER_BEGIN; } break;
			case 'in progress': { return LearningCourseuser::$COURSE_USER_BEGIN; } break;
			case 'completed': { return LearningCourseuser::$COURSE_USER_END; } break;
			case 'suspended': { return LearningCourseuser::$COURSE_USER_SUSPEND; } break;
			case 'waitlisted': { return LearningCourseuser::$COURSE_USER_WAITING_LIST; } break;
		}
		return false;
	}

	/**
	 * Returns true if the provided course type is allowed
	 * @param $type
	 */
	private function _validateCourseType($type) {
		return in_array($type, array(LearningCourse::TYPE_CLASSROOM, LearningCourse::TYPE_ELEARNING));
	}

    /**
     * Date validation
     * @param $input
     * @param bool $strict
     * @return bool
     */
    public static function _validateDate(&$input, $strict = false) {
		$regExp = '/^([1-3][0-9]{3,3})-(0?[1-9]|1[0-2])-(0?[1-9]|[1-2][0-9]|3[0-1])$/';
		if (preg_match($regExp, $input)) {
			return true;
		} elseif (!$strict) {
			if (self::_validateDateTime($input, true)) {
				$input = substr($input, 0, 10);
				return true;
			}
		}
		return false;
	}

    /**
     * Datetime validation
     * @param $input
     * @param bool $strict
     * @return bool
     */
    public static function _validateDateTime(&$input, $strict = false) {
		$regExp = '/^([1-3][0-9]{3,3})-(0?[1-9]|1[0-2])-(0?[1-9]|[1-2][0-9]|3[0-1])\s([0-1][0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9])$/';
		$tmp = $input;
		if (preg_match($regExp, $tmp)) {
			return true;
		} elseif (!$strict) {
			if (self::_validateDate($tmp, true)) {
				$input = $tmp.' 00:00:00';
				return true;
			}
		}
		return false;
	}

	/**
	 * @summary [DEPRECATED, see course/courses] Retrieves a list of courses in the LMS
	 * @notes Resources may be filtered by category.
	 *
	 * @parameter category [integer, optional] Filter on course category
	 *
	 * @response courses [array, required] a list of retrieved courses
     *      @item course_info [object, required] Course info object
     *          @item course_id [integer, required] Course ID
     *          @item code [string, optional] Alphanumeric course code
     *          @item course_name [string, required] Course name/title
     *          @item course_description [string, required] Course description
	 * 			@item course_language [string, required] Course language (E.g. 'english', italian', 'french', ...)
     *          @item status [integer, required] Course status (0 = In preparation, 1 = Available, 2 = Effective)
     *          @item selling [integer, optional] Is this course on sale?
     *          @item price [float, optional] Course price (for the ecommerce cart)
     *          @item subscribe_method [integer, required] Subscription method in the catalog (0 = Admin only, 1 = Moderated, 2 = Free)
     *          @item course_edition [integer, required] Deprecated
     *          @item course_type [string, required] Course type (elearning, classroom)
     *          @item sub_start_date [datetime, optional] Subscription start date
     *          @item sub_end_date [datetime, optional] Subscription end date
     *          @item date_begin [date, required] Course validity begin date
     *          @item date_end [date, required] Course validity end date
     *          @item course_link [string, required] Course Link
     *          @end
	 */
	public function listCourses() {

		// Read input filter (category)
		$idCategory = (int) $this->readParameter('category');

		// Do search in DB
		$criteria = new CDbCriteria();
		if ($idCategory) {
			$criteria->addCondition("idCategory = :id_category");
			$criteria->params[':id_category'] = $idCategory;
		}
		$courses = LearningCourse::model()->findAll($criteria); /** @var $course LearningCourse */

		// Prepare output list of courses
		$output = array();
		foreach ($courses as $course) {
			$output[]['course_info'] = array(
				'course_id'				=> (int)$course->idCourse,
				'code'					=> str_replace('&', '&amp;', $course->code),
				'course_name'			=> str_replace('&', '&amp;', $course->name),
				'course_description'	=> str_replace('&', '&amp;', $course->description),
				'course_language'		=> $course->lang_code,
				'status'				=> (int)$course->status,
				'selling'				=> (int)$course->selling,
				'price'					=> $course->prize,
				'subscribe_method'		=> (int)$course->subscribe_method,
				'course_edition'		=> (int)$course->course_edition,
				'course_type'			=> $course->course_type,
				'sub_start_date'		=> $course->sub_start_date,
				'sub_end_date'			=> $course->sub_end_date,
				'date_begin'			=> $course->date_begin,
				'date_end'				=> $course->date_end,
				'course_link'			=> Docebo::createAbsoluteLmsUrl('player', array('course_id' => $course->idCourse))
			);
		}

		return $output;
	}

	/**
	 * @summary Retrieves a list of courses in the LMS
	 * @notes Resources may be filtered by category.
	 *
	 * @parameter category [integer, optional] Filter on course category
	 *
	 * @response courses [array, required] a list of retrieved courses
	 *      @item course_id [integer, required] Course ID
	 *      @item code [string, optional] Alphanumeric course code
	 *      @item course_name [string, required] Course name/title
	 *      @item course_description [string, required] Course description
	 * 		@item course_language [string, required] Course language (E.g. 'english', italian', 'french', ...)
	 *      @item status [integer, required] Course status (0 = In preparation, 1 = Available, 2 = Effective)
	 *      @item selling [integer, optional] Is this course on sale?
	 *      @item price [float, optional] Course price (for the ecommerce cart)
	 *      @item subscribe_method [integer, required] Subscription method in the catalog (0 = Admin only, 1 = Moderated, 2 = Free)
	 *      @item course_edition [integer, required] Deprecated
	 *      @item course_type [string, required] Course type (elearning, classroom)
	 *      @item sub_start_date [datetime, optional] Subscription start date
	 *      @item sub_end_date [datetime, optional] Subscription end date
	 *      @item date_begin [date, required] Course validity begin date
	 *      @item date_end [date, required] Course validity end date
	 *      @item course_link [string, required] Course Link
	 *      @end
	 */
	public function courses() {

		// Read input filter (category)
		$idCategory = (int) $this->readParameter('category');

		// Do search in DB
		$criteria = new CDbCriteria();
		if ($idCategory) {
			$criteria->addCondition("idCategory = :id_category");
			$criteria->params[':id_category'] = $idCategory;
		}
		$courses = LearningCourse::model()->findAll($criteria); /** @var $course LearningCourse */

		// Prepare output list of courses
		$output = array();
		foreach ($courses as $course) {
			$output[] = array(
				'course_id'				=> (int)$course->idCourse,
				'code'					=> str_replace('&', '&amp;', $course->code),
				'course_name'			=> str_replace('&', '&amp;', $course->name),
				'course_description'	=> str_replace('&', '&amp;', $course->description),
				'course_language'		=> $course->lang_code,
				'status'				=> (int)$course->status,
				'selling'				=> (int)$course->selling,
				'price'					=> $course->prize,
				'subscribe_method'		=> (int)$course->subscribe_method,
				'course_edition'		=> (int)$course->course_edition,
				'course_type'			=> $course->course_type,
				'sub_start_date'		=> $course->sub_start_date,
				'sub_end_date'			=> $course->sub_end_date,
				'date_begin'			=> $course->date_begin,
				'date_end'				=> $course->date_end,
				'course_link'			=> Docebo::createAbsoluteLmsUrl('player', array('course_id' => $course->idCourse))
			);
		}

		return array('courses'=>$output);
	}


	/**
	 * @summary Deletes a course by its ID
	 * @notes Only "elearning" courses can be deleted
	 *
	 * @parameter course_id [integer, required] The numerical ID of the course
	 *
	 * @status 201 Invalid specified course
	 *
	 */
	public function deleteCourse() {
		$idCourse = $this->readParameter('course_id');

		// validate input
		$course = LearningCourse::model()->findByPk($idCourse);
		if (!$course || !$this->_validateCourseType($course->course_type)) {
			throw new CHttpException(201, 'Invalid specified course');
		}

		// do stuff
		$transaction = Yii::app()->db->beginTransaction();
		try {
			$course->delete();
			$transaction->commit();
		} catch(Exception $e) {
			$transaction->rollback();
			throw new CHttpException(500, $e->getMessage());
		} catch(CHttpException $e) {
			$transaction->rollback();
			throw $e;
		}

		return array();
	}


    /**
     * @summary Creates a new course in the LMS
     *
     * @parameter course_code [string, optional] Course code
	 * @parameter course_type [string, optional] Course type ('elearning','classroom'). If not provided, defaults to 'elearning'.
     * @parameter course_name [string, required] Course name/title
     * @parameter course_descr [string, required] Course description
     * @parameter category_id [integer, optional] Course category ID
     * @parameter course_lang [string, optional] Course language
     * @parameter course_difficult [string, optional] Course difficulty ('veryeasy', 'easy', 'medium', 'difficult', 'verydifficult')
     * @parameter allow_overbooking [boolean, optional] Is overbooking allowed for this course?
     * @parameter can_subscribe [boolean, optional] Can the user enroll to this course?
     * @parameter date_begin [date, optional] Course validity begin date (in format yyyy-MM-dd)
     * @parameter date_end [date, optional] Course validity end date (in format yyyy-MM-dd)
     * @parameter course_medium_time [string, optional] Course medium time
     * @parameter course_price [string, optional] Course price in the shopping cart
     * @parameter course_sell [boolean, optional] Is this course on sale via the ecommerce?
     * @parameter course_show_rules [boolean, optional] Deprecated
     * @parameter course_status [integer, optional] Course status (0 = In preparation, 1 = Available, 2 = Effective)
     * @parameter credits [decimal, optional] Number of CEU for this course
     * @parameter max_num_subscribe [integer, optional] Maximum number of subscriptions for this course
     * @parameter sub_start_date [datetime, optional] Subscription start date
     * @parameter sub_end_date [datetime, optional] Subscription end date
     *
     * @status 201 Empty name, Empty description
     * @status 202 Error while creating course
     *
     * @response course_id [integer, required] The internal course ID of the created course
     */
    public function addCourse() {

		$purifier = new DoceboCHtmlPurifier();
		$transaction = Yii::app()->db->beginTransaction();
		try {

			//prepare new course AR
			$learningCourse = new LearningCourse();
			$learningCourse->setScenario('createApi');
			$learningCourse->setCourseDefaults();

			$param = $this->readParameter('course_type');
			if ($param && $this->_validateCourseType($param)) { $learningCourse->course_type = $param; }

			$param = $this->readParameter('course_code');
			if ($param) { $learningCourse->code = $param; }

			$param = $this->readParameter('course_name');
			if (empty($param)) { throw new CHttpException(201, 'Empty name'); }
			if ($param) { $learningCourse->name = $param; }

			$param = $this->readParameter('course_descr', "");
			if (empty($param)) { throw new CHttpException(201, 'Empty description'); }
			$learningCourse->description = $purifier->purify($param);

			$param = $this->readParameter('category_id', 0);
			if ($param) { $learningCourse->idCategory = $param; }

			$param = $this->readParameter('course_lang', CoreUser::getDefaultLangCode());
			if ($param) { $learningCourse->lang_code = $param; }

			$param = $this->readParameter('course_difficult');
			if (!array_key_exists($param, LearningCourse::difficultyList())) {
				$param = LearningCourse::DIFFICULTY_MEDIUM;
			}
			$learningCourse->difficult = $param;

			$param = $this->readParameter('allow_overbooking', 0) > 0;
			$learningCourse->allow_overbooking = ($param ? 1 : 0);

			$param = $this->readParameter('can_subscribe', 0) > 0;
			$learningCourse->can_subscribe = ($param ? 1 : 0);

			$param = $this->readParameter('date_begin');
			if(!$param)
				$param = $this->readParameter('course_date_begin');
			if ($param && self::_validateDate($param)) { $learningCourse->date_begin = Yii::app()->localtime->toLocalDate($param); }

			$param = $this->readParameter('date_end');
			if(!$param)
				$param = $this->readParameter('course_date_end');
			if ($param && self::_validateDate($param)) { $learningCourse->date_end = Yii::app()->localtime->toLocalDate($param); }

			$param = $this->readParameter('course_medium_time', 0);
			if ($param) { $learningCourse->mediumTime = $param; }

			$param = $this->readParameter('course_price');
			if ($param) { $learningCourse->prize = $param; }

			$param = $this->readParameter('course_sell', 0) > 0;
			$learningCourse->selling = ($param ? 1 : 0);

			$param = $this->readParameter('course_show_rules', 0) > 0;
			$learningCourse->show_rules = ($param ? 1 : 0);

			$param = (array_search($this->readParameter('course_status', 2), array(2,0)) ? $this->readParameter('course_status', 2) : 2);
			if ($param) { $learningCourse->status = $param; }

			$param = $this->readParameter('credits', 0);
			if ($param) { $learningCourse->credits = $param; }

			$param = $this->readParameter('max_num_subscribe', 0);
			if ($param) { $learningCourse->max_num_subscribe = $param; }

			$param = $this->readParameter('sub_start_date');
			if ($param && self::_validateDateTime($param)) { $learningCourse->sub_start_date = Yii::app()->localtime->toLocalDateTime($param); }

			$param = $this->readParameter('sub_end_date');
			if ($param && self::_validateDateTime($param)) { $learningCourse->sub_end_date = Yii::app()->localtime->toLocalDateTime($param); }

			if (!$learningCourse->validate() || !$learningCourse->saveCourse(true)) {
				throw new CHttpException(202, 'Error while creating course');
			}

			$transaction->commit();

		} catch (CHttpException $e) {
			$transaction->rollback();
			throw $e;
		} catch (Exception $e) {
			$transaction->rollback();
			throw new CHttpException(500, $e->getMessage());
		}

		return array('course_id' => $learningCourse->getPrimaryKey());
	}

    /**
     * @summary Updates course information with the provided data
     *
     * @parameter course_id [integer, required] Course ID
     * @parameter course_code [string, optional] Course code
     * @parameter course_name [string, optional] Course name/title
     * @parameter course_descr [string, optional] Course description
     * @parameter category_id [integer, optional] Course category ID
     * @parameter course_lang [string, optional] Course language
     * @parameter course_difficult [string, optional] Course difficulty ('veryeasy', 'easy', 'medium', 'difficult', 'verydifficult')
     * @parameter allow_overbooking [boolean, optional] Is overbooking allowed for this course?
     * @parameter can_subscribe [boolean, optional] Can the user enroll to this course?
	 * @parameter date_begin [date, optional] Course validity begin date (in format yyyy-MM-dd)
	 * @parameter date_end [date, optional] Course validity end date (in format yyyy-MM-dd)
     * @parameter course_medium_time [string, optional] Course medium time
     * @parameter course_price [string, optional] Course price in the shopping cart
     * @parameter course_sell [boolean, optional] Is this course on sale via the ecommerce?
     * @parameter course_show_rules [boolean, optional] Deprecated
     * @parameter course_status [integer, optional] Course status (0 = In preparation, 1 = Available, 2 = Effective)
     * @parameter credits [decimal, optional] Number of CEU for this course
     * @parameter max_num_subscribe [integer, optional] Maximum number of subscriptions for this course
     * @parameter sub_start_date [datetime, optional] Subscription start date
     * @parameter sub_end_date [datetime, optional] Subscription end date
     *
     * @status 201 Course ID cannot be empty
     * @status 202 Invalid specified course
     * @status 203 Error while updating course
     *
     * @response course_id [integer, required] The internal course ID of the updated course
     */
    public function updateCourse() {

		$purifier = new DoceboCHtmlPurifier();
		$transaction = Yii::app()->db->beginTransaction();
		try {

			$idCourse = $this->readParameter('course_id');
			if (empty($idCourse)) {
				throw new CHttpException(201, 'Course ID cannot be empty');
			}

			//retrieve corse AR
			$learningCourse = LearningCourse::model()->findByPk($idCourse);
			$learningCourse->setScenario('updateApi');
			if (!$learningCourse) {
				throw new CHttpException(202, 'Invalid specified course');
			}

			//assign new data
			//read input parameters and assign them to new course
			$param = $this->readParameter('course_code', null);
			if ($param !== null) { $learningCourse->code = $param; }

			$param = $this->readParameter('course_name', null);
			if ($param !== null) { $learningCourse->name = $param; }

			$param = $this->readParameter('course_descr', null);
			if ($param !== null) { $learningCourse->description = $purifier->purify($param); }

			$param = $this->readParameter('category_id', null);
			if ($param !== null) { $learningCourse->idCategory = $param; }

			$param = $this->readParameter('course_lang', null);
			if ($param !== null) { $learningCourse->lang_code = $param; }

			$param = $this->readParameter('course_difficult', null);
			if ($param !== null) {
				if (!array_key_exists($param, LearningCourse::difficultyList())) {
					$param = LearningCourse::DIFFICULTY_MEDIUM; //default difficulty value
				}
				$learningCourse->difficult = $param;
			}

			$param = $this->readParameter('allow_overbooking', 0) > 0;
			$learningCourse->allow_overbooking = ($param ? 1 : 0);

			$param = $this->readParameter('can_subscribe', 0) > 0;
			$learningCourse->can_subscribe = ($param ? 1 : 0);

			$param = $this->readParameter('date_begin');
			if(!$param)
				$param = $this->readParameter('course_date_begin');
			if ($param && self::_validateDate($param)) { $learningCourse->date_begin = Yii::app()->localtime->toLocalDate($param); }

			$param = $this->readParameter('date_end');
			if(!$param)
				$param = $this->readParameter('course_date_end');
			if ($param && self::_validateDate($param)) {
				$learningCourse->date_end = Yii::app()->localtime->toLocalDate($param);
			}

			$param = $this->readParameter('course_medium_time', 0);
			if ($param) {
				$learningCourse->mediumTime = $param;
			}

			$param = $this->readParameter('course_price');
			if ($param) {
				$learningCourse->prize = $param;
			}

			$param = $this->readParameter('course_sell', 0) > 0;
			$learningCourse->selling = ($param ? 1 : 0);

			$param = $this->readParameter('course_show_rules', 0) > 0;
			$learningCourse->show_rules = ($param ? 1 : 0);

			$param = (array_search($this->readParameter('course_status', 2), array(2, 0)) ? $this->readParameter('course_status', 2) : 2);
			if ($param) {
				$learningCourse->status = $param;
			}

			$param = $this->readParameter('credits', 0);
			if ($param) {
				$learningCourse->credits = $param;
			}

			$param = $this->readParameter('max_num_subscribe', 0);
			if ($param) {
				$learningCourse->max_num_subscribe = $param;
			}

			$param = $this->readParameter('sub_start_date');
			if ($param && self::_validateDateTime($param)) {
				$learningCourse->sub_start_date = Yii::app()->localtime->toLocalDateTime($param);
			}

			$param = $this->readParameter('sub_end_date');
			if ($param && self::_validateDateTime($param)) {
				$learningCourse->sub_end_date = Yii::app()->localtime->toLocalDateTime($param);
			}

			//update AR
			if (!$learningCourse->saveCourse(true)) {
				throw new CHttpException(203, 'Error while updating course');
			}

			$transaction->commit();

		} catch (CHttpException $e) {
			$transaction->rollback();
			throw $e;
		} catch (Exception $e) {
			$transaction->rollback();
			throw new CHttpException(500, $e->getMessage());
		}

		return array('course_id' => $learningCourse->getPrimaryKey());
	}


    /**
     * @summary Subscribes a user to a course
     *
     * @parameter id_user [integer, optional] Docebo ID for the user to be enrolled in the course
     * @parameter idst [integer, optional] DEPRECATED use id_user instead
     * @parameter course_id [integer, optional] Docebo ID for the course
     * @parameter course_code [string, optional] Course code
     * @parameter user_level [string, optional] User enrollment level (student, instructor, tutor)
     * @parameter date_begin_validity [datetime, optional] the enrollment validity start (in yyyy-MM-dd HH:mm:ss format, UTC timezone)
     * @parameter date_expire_validity [datetime, optional] the enrollment validity end (in yyyy-MM-dd HH:mm:ss format, UTC timezone)
	 * @parameter enrollment_fields [array, optional] array of additional enrollment fields. Key = ID of the field, value = value of the field
     *
     * @status 201 Invalid parameters
     * @status 202 Invalid specified course
     * @status 203 User already enrolled to the course
	 * @status 264 Invalid enrollment fields
     * @status 254 Error while enrolling user
     */
    public function addUserSubscription() {

		//read input
		$input = new stdClass();
		$input->idst = $this->readParameter('idst');
		if (!$input->idst) $input->idst = $this->readParameter('id_user');

		//validate user
		if (empty($input->idst) || (int)$input->idst <= 0) {
			throw new CHttpException(201, 'Invalid parameters');
		}
		$user = CoreUser::model()->findByPk($input->idst);
		if (empty($user)) {
			throw new CHttpException(201, 'Invalid parameters');
		}

		$input->course_id = $this->readParameter('course_id');
		$input->course_code = $this->readParameter('course_code');

		//validate course
		if ($input->course_id > 0) {
			$course = LearningCourse::model()->findByPk($input->course_id);
		} elseif ($input->course_code != '') {
			$course = LearningCourse::model()->findByAttributes(array('code' => $input->course_code));
		} else {
			$course = false;
		}
		if (empty($course)) {
			throw new CHttpException(202, 'Invalid specified course');
		}
		if($course->course_type == LearningCourse::TYPE_ELEARNING) {
			$input->date_begin_validity = $this->readParameter('date_begin_validity');
			$input->date_expire_validity = $this->readParameter('date_expire_validity');
		} else {
			$input->date_begin_validity = false;
			$input->date_expire_validity = false;
		}

		//check if user is already subscribed
		$courseUser = LearningCourseuser::model()->findByAttributes(array(
			'idUser' => $user->idst,
			'idCourse' => $course->idCourse
		));
		if (!empty($courseUser)) {
			throw new CHttpException(203, 'User already enrolled to the course');
		}
		
		$fields = $this->readParameter("enrollment_fields", array());

		$fieldsToValidate = $fields;
		if (!is_array($fields)) {
			$fieldsToValidate = json_decode($fields);
		}
		$validFields = LearningEnrollmentField::validateEnrollmentArray($fieldsToValidate, $input->course_id);



		if(empty($validFields['success'])){
			throw new CHttpException(264, 'Invalid enrollment fields');
		}

		//get level
		$input->level = $this->readParameter('user_level', 'student');
		$userLevel = self::_getCourseUserLevelId($input->level);

		//do enrollment
		$courseUser = new LearningCourseuser();
		if (!$courseUser->subscribeUser($user->idst, Yii::app()->user->id, $course->idCourse, 0, $userLevel, false, $input->date_begin_validity, $input->date_expire_validity, false, false, false, json_encode($fields)))
			throw new CHttpException(254, 'Error while enrolling user');

		return array();
	}

	/**
	 * @summary Updates the user subscription to a course
	 *
	 * @parameter id_user [integer, optional] Docebo ID for the user to be enrolled in the course
	 * @parameter idst [integer, optional] DEPRECATED use id_user instead
	 * @parameter course_id [integer, optional] Docebo ID for the course
	 * @parameter course_code [string, optional] Course code
	 * @parameter user_level [string, optional] User enrollment level (student, instructor, tutor)
	 * @parameter user_status [string, optional] User enrollment status (ab-initio, in progress, completed, suspended, waitlisted)
	 * @parameter enrollment_fields [json, optional] Json- list of additional enrollment fields. Key = ID of the field, value = value of the field. For example: {"21":"Field text","22":"75"}
	 *
	 * @parameter date_begin_validity [datetime, optional] the enrollment validity start (in yyyy-MM-dd HH:mm:ss format, UTC timezone)
	 * @parameter date_expire_validity [datetime, optional] the enrollment validity end (in yyyy-MM-dd HH:mm:ss format, UTC timezone)
	 * @status 201 Invalid specified user
	 * @status 203 Invalid specified course
	 * @status 254 Invalid enrollment
	 * @status 264 Invalid enrollment fields
	 * @status 205 Error while updating enrollment data
	 */
	public function updateUserSubscription()
	{

		//read and validate user input
		$idUser = $this->readParameter('idst');
		if (!$idUser) $idUser = $this->readParameter('id_user');
		$user = ((int)$idUser > 0 ? CoreUser::model()->findByPk((int)$idUser) : false);
		if (!$user) {
			throw new CHttpException(201, 'Invalid specified user');
		}

		$idCourse = $this->readParameter('course_id');
		$codeCourse = $this->readParameter('course_code');

		//read new user level and status
		$newUserLevel = $this->readParameter('user_level');
		$newUserStatus = $this->readParameter('user_status');
		//then "translate" them (if false, no update is requested)
		$userLevel = ($newUserLevel ? self::_getCourseUserLevelId($newUserLevel) : false);
		$userStatus = ($newUserStatus ? self::_getCourseUserStatusId($newUserStatus) : false);

		//validate course input
		if ((int)$idCourse > 0) {
			$course = LearningCourse::model()->findByPk((int)$idCourse);
		} elseif (!empty($codeCourse)) {
			$course = LearningCourse::model()->findByAttributes(array('code' => $codeCourse));
		} else {
			$course = false;
		}
		if (empty($course)) {
			throw new CHttpException(203, 'Invalid specified course');
		}

		//retrieve enroll record
		$enroll = LearningCourseuser::model()->findByAttributes(array(
			'idUser' => $user->idst,
			'idCourse' => $course->idCourse
		));
		if (empty($enroll)) {
			throw new CHttpException(254, 'Invalid enrollment');
		}

		$additionalFieldsToUpdate = $this->readParameter('enrollment_fields');

		$additionalFieldsToUpdate = (array)json_decode($additionalFieldsToUpdate);

		$fieldValidation = LearningEnrollmentField::validateEnrollmentArray($additionalFieldsToUpdate);
		Log::_($fieldValidation);

		if (sizeof($fieldValidation['missingFields']) == sizeof($additionalFieldsToUpdate)) {
			throw new CHttpException(264, 'Invalid enrollment fields');
		}


		$isAdditionalFields = false;
		$fieldValidation = [];
		if (!empty($additionalFieldsToUpdate && sizeof($additionalFieldsToUpdate) > 0)) {

			$newFieldsClearArr = [];

			$isAdditionalFields = true;

			foreach ($additionalFieldsToUpdate as $fieldId => $oneNewFieldVal) {


				$fieldExist = LearningEnrollmentField::model()->findByAttributes(array(
					'id' => (int)$fieldId
				));

				if (!$fieldExist) {
					$fieldValidation['not_existed_field_ids'][]=$fieldId;
					continue;
				}

				if ($fieldExist->type == LearningEnrollmentField::TYPE_FIELD_DROPDOWN) {
					$sql = 'SELECT 
									lefd.id
	  							FROM learning_enrollment_fields_dropdown as lefd
	  							WHERE lefd.id=' . $oneNewFieldVal . ' AND lefd.id_field=' . $fieldId;

					$connection = Yii::app()->db;
					$optionExist = $connection->createCommand($sql)->queryColumn();
					if (!$optionExist) {
						continue;
					}

				}
				$newFieldsClearArr[$fieldId] = $oneNewFieldVal;
				$fieldValidation['new_field_ids'][$fieldId]=$oneNewFieldVal;

			}

			$oldFields = json_decode($enroll->enrollment_fields);
			foreach ($oldFields as $oldFieldId => $oldField) {
				if (isset($newFieldsClearArr[$oldFieldId])) {
					continue;
				}
				$newFieldsClearArr[$oldFieldId] = $oldField;
			}



			$enroll->enrollment_fields = json_encode($newFieldsClearArr);

		}

		if ($course->course_type == LearningCourse::TYPE_ELEARNING) {
			$startDate = $this->readParameter('date_begin_validity');
			$endDate = $this->readParameter('date_expire_validity');
			if ($startDate !== null)
				$enroll->date_begin_validity = $startDate;
			if ($endDate !== null)
				$enroll->date_expire_validity = $endDate;
		} else {
			$enroll->date_begin_validity = null;
			$enroll->date_expire_validity = null;
		}
		//assign new values
		if ($userLevel !== false) {
			$enroll->level = $userLevel;
		}
		if ($userStatus !== false) {
			$enroll->status = $userStatus;
		}

		//do update
		if (!$enroll->save()) {
			throw new CHttpException(205, 'Error while updating enrollment data');
		}
		$data = [];
		if ($isAdditionalFields == true) {
			//$data['all_additional_fields'] = $newFieldsClearArr;

			if (isset($fieldValidation['not_existed_field_ids'])) {
				$data['not_existed_field_ids'] = $fieldValidation['not_existed_field_ids'];
			}

			if (isset($fieldValidation['new_field_ids'])) {
				$data['updated_field_ids'] = $fieldValidation['new_field_ids'];
			}

		}
		return $data;
	}

    /**
     * @summary Deletes the user subscription from a course
     *
     * @parameter id_user [integer, optional] Docebo ID for the user to be enrolled in the course
     * @parameter idst [integer, optional] DEPRECATED use id_user instead
     * @parameter course_id [integer, optional] Docebo ID for the course
     * @parameter course_code [string, optional] Course code
     *
     * @status 201 Invalid specified user
     * @status 202 Invalid specified course
     * @status 203 Invalid enrollment
     * @status 254 Error while unenrolling user
     */
	public function deleteUserSubscription() {

		//read input values
		$idUser = $this->readParameter('idst');
		if (!$idUser) $idUser = $this->readParameter('id_user');
		$user = ((int)$idUser > 0 ? CoreUser::model()->findByPk((int)$idUser) : false);
		if (!$user) {
			throw new CHttpException(201, 'Invalid specified user');
		}

		$input = new stdClass();
		$input->idst = $idUser;
		$input->course_id = $this->readParameter('course_id', false);
		$input->course_code = $this->readParameter('course_code', false);

		if ((int)$input->course_id > 0) {
			$course = LearningCourse::model()->findByPk($input->course_id);
		} elseif (!empty($input->course_code)) {
			$course = LearningCourse::model()->findByAttributes(array('code' => $input->course_code));
		} else {
			throw new CHttpException(202, 'Invalid specified course');
		}
		if (!$course) {
			throw new CHttpException(202, 'Invalid specified course');
		}

		//retrieve enrollment AR and unenroll the user
		$courseUser = LearningCourseuser::model() ->findByAttributes(array(
			'idUser' => $user->getPrimaryKey(),
			'idCourse' => $course->getPrimaryKey()
		));
		if (!$courseUser)
			throw new CHttpException(203, 'Invalid enrollment');

		if (PluginManager::isPluginActive('CurriculaApp')) {
			//check if user can effectively be unenrolled by the course or some LPs constraints are present
			$lpEnrollments = LearningCoursepath::checkEnrollment($input->idst, $input->course_id);
			if (!empty($lpEnrollments)) {
				throw new CHttpException(203, 'User is enrolled to learning plan(s) containing this course');
			}
		}

		//do physical unenrollment
		if (!$courseUser->unsubscribeUser($courseUser->idUser, $courseUser->idCourse)) {
			throw new CHttpException(254, 'Error while unenrolling user');
		}

		return array();
	}

    /**
     * @summary Subscribes a user using auto-registration codes
     * @notes The user is enrolled in all courses having the provided registration code with the student level
     *
     * @parameter id_user [integer, optional] Docebo ID for the user to be enrolled in the course
     * @parameter idst [integer, optional] DEPRECATED use id_user instead
     * @parameter reg_code_type [string, optional] Type of registration code
     * @parameter reg_code [string, required] Registration code
     *
     * @status 201 Invalid specified user
     * @status 202 Empty code or code type not specified
     * @status 203 Invalid provided autoregistration code
     * @status 254 Error while enrolling user
     */
	public function subscribeUserWithCode() {

		//read input values
        $idUser = $this->readParameter('idst');
        if (!$idUser) $idUser = $this->readParameter('id_user');
        $user = ((int)$idUser > 0 ? CoreUser::model()->findByPk((int)$idUser) : false);
        if (!$user) {
            throw new CHttpException(201, 'Invalid specified user');
        }

        $input = new stdClass();
        $input->idst = $idUser;

		//read registration code
		$input->reg_code_type = $this->readParameter('reg_code_type', 'course');
		$input->reg_code = $this->readParameter('reg_code', false);
		if (!empty($input->reg_code)) {
			$registrationCode = strtoupper($input->reg_code);
			$registrationCode = str_replace('-', '', $registrationCode);
		}
		if (empty($input->reg_code_type) || empty($registrationCode)) {
			throw new CHttpException(202, 'Empty code or code type not specified');
		}
		if ($input->reg_code_type == 'tree_course') {
			$registrationCode = substr($registrationCode, 10, 10);
		}

		//check registration code validity
		$courses = LearningCourse::model()->findAllByAttributes(array('autoregistration_code' => $registrationCode));
		if (empty($courses)) {
			throw new CHttpException(203, 'Invalid provided autoregistration code');
		}

		$transaction = Yii::app()->db->beginTransaction();
		try {
			//do enrollment(s)
			foreach ($courses as  $course) {
				//check if user is already subscribed to the course
				$courseUser = LearningCourseuser::model()->findByAttributes(array(
					'idUser' => $idUser,
					'idCourse' => $course->idCourse
				));

				if (empty($courseUser)) {
					//if not, then do enrollment
					$courseUser = new LearningCourseuser();
					if (!$courseUser->subscribeUser($idUser, Yii::app()->user->id, $course->idCourse, 0, self::_getCourseUserLevelId('student'))) {
						throw new CHttpException(254, 'Error while enrolling user');
					}
				}
			}

			$transaction->commit();

		} catch (CHttpException $e) {
			$transaction->rollback();
			throw $e;
		} catch (Exception $e) {
			$transaction->rollback();
			throw new CHttpException(500, $e->getMessage());
		}

		return array();
	}

	/**
	 * @summary [DEPRECATED, see course/listEnrolled] Retrieves the list of courses in the LMS that a user is enrolled to
	 *
	 * @parameter id_user [integer, required] The user's id
	 *
	 * @status 401 User not found
	 *
	 * @response courses [array, required] the list of enrolled courses
	 *      @item course_info [array, required] Course enrollment info
	 *          @item course_id [integer, required] Course ID
	 *          @item code [string, optional] Alphanumeric course code
	 *          @item course_name [string, required] Course name/title
	 *          @item credits [string, optional] Course credits
	 *          @item total_time [string, optional] Total time in course
	 * 	        @item enrollment_date [datetime, optional] Enrollment start date
	 * 	        @item first_access_date [datetime, optional] Course first access date
	 * 	        @item completion_date [datetime, optional] Course completion date
	 *          @item score [string, optional] User score for the course
	 *          @item status [string, required] Enrollment status
	 *          @end
	 *
	 */
	public function listEnrolledCourses() {

		$idUser = (int) $this->readParameter('id_user');

		// check if the user exists
		$user = CoreUser::model()->findByPk($idUser);
		if (!$user)
			throw new CHttpException(401, 'User not found');

		// Do search in DB
		$criteria = new CDbCriteria();
		$criteria->addCondition("idUser = :idUser");
		$criteria->params[':idUser'] = $idUser;
		$enrolledCourses = LearningCourseuser::model()->findAll($criteria);

		// Prepare output list of courses
		$output = array();
		foreach ($enrolledCourses as $row) {
			$output[]['course_info'] = array(
				'course_id' => (int)$row->idCourse,
				'code' => $row->course->code,
				'course_name' => $row->course->name,
				'credits' => $row->course->credits,
				'total_time' => $row->renderTimeInCourse(),
				'enrollment_date' => $row->date_inscr,
				'completion_date' => $row->date_complete,
				'first_access_date'	=> $row->date_first_access,
				'score'	=> LearningCourseuser::getLastScoreByUserAndCourse($row->idCourse, $row->idUser),
				'status' => LearningCourseuser::getStatusLabel($row->status)
			);
		}

		return $output;
	}

	/**
	 * @summary Starts a background report creation process and returns a unique JOB ID to track its progress
	 *
	 * @parameter user_ids [array, required] User IDs which to include in the report
	 * @parameter interactions [bool, optional] Whether or not to return also the user's interactions
	 *
	 * @status 401 Invalid "user_ids" parameter
	 * @status 402 No user IDs provided
	 * @status 403 Unable to schedule job
	 *
	 * @response job_id [string, required] The unique Job ID to check status later
	 */
	public function startUsersReport(){
		$userIds = $this->readParameter( 'user_ids' );
		/* @var $userIds array */

		$interactionsParam = $this->readParameter('interactions');
		if(strtolower($interactionsParam) == "true")
			$interactionsParam = true;
		else if(strtolower($interactionsParam) == "false")
			$interactionsParam = false;

		$withInteractions = (int) $interactionsParam;
		/* @var $withInteractions boolean */

		if( is_string( $userIds ) ) {
			if( stripos( $userIds, ',' ) !== FALSE ) {
				// Is a comma seaprated list
				$userIds = explode( ',', $userIds );
			} else {
				// Is just a single user ID
				$userIds = array( (int) $userIds );
			}
		} elseif( is_array( $userIds ) ) {
			// all good, user IDs list already provided as array
		} else {
			throw new CHttpException( 401, 'Invalid "user_ids" parameter' );
		}

		$userIds = array_filter($userIds); // remove empty elements

		// Clean up the input
		foreach ( $userIds as $key => $userId ) {
			if(((int) $userId) > 0)
				$userIds[ $key ] = (int) $userId;
		}
		$userIds = array_unique( $userIds );

		if( empty( $userIds ) ) {
			throw new CHttpException( 402, 'No user IDs provided' );
		}

		Yii::import( 'root.api.protected.components.helpers.GenerateCourseAPIUsersReportImmediateJobHandler' );

		$name    = "CourseApiModuleUsersReport";
		$handler = GenerateCourseAPIUsersReportImmediateJobHandler::HANDLER_ID;
		$params  = array(
			'input'  => $userIds,
			'output' => array(
				'data'=>array(),
				'failed_users'=>array(),
			),
			'with_interactions'=>(bool) $withInteractions,
			'status' => GenerateCourseAPIUsersReportImmediateJobHandler::STATUS_IN_PROGRESS, // in_progress|completed
			'offset' => 0, // will be increased on every self-call to the job by itself
			'total'  => count( $userIds )
		);

		try {
			// Create Core Job for all the chunks
			$job = Yii::app()->scheduler->createApiJob( $name, $handler, $params );
			/* @var $job CoreJob */
			$output = array(
				'job_id'  => $job->hash_id,
			);

			return $output;
		}catch(Exception $e){
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			// Failes when Scheduler+Redis are not correctly configured
			throw new CHttpException(403, "Unable to schedule job");
		}
	}

	/**
	 * @summary Checks the status of a background report job and returns the data if it's ready
	 *
	 * @parameter job_id [string, required] The report ID, received from the call to course/startUsersReport
	 *
	 * @status 401 No Job ID provided
	 * @status 402 Job not found
	 * @status 403 Invalid job state
	 *
	 * @response status [string, required] "running" or "completed"
	 * @response data [array, optional] Only returned when status is "completed".
	 * 		@item userid [integer, required] The ID of the user being processed
	 * 		@item total_questions [integer, required] Number of total interactions by this user
	 * 		@item total_correct [integer, required] Number of correct interactions by this user
	 * 		@item percent_correct [float, required] A weighted percentage between the total interactions and the correct interactions by this user
	 * 		@item courses [array, optional] Details for each of the courses this user is enrolled in (that contains a SCORM)
	 * 			@item course_id [integer, required] The course ID
	 * 			@item total_questions [integer, required] Number of total interactions in this course for this user
	 * 			@item total_correct [integer, required] Number of correct interactions in this course for this user
	 * 			@item percent_correct [integer, required] A weighted percentage between the total and correct interactiosn by this user in this course
	 * 			@item course_name [string, required] The course name
	 * 			@item courseuser_status [string, required] Enrollment status
	 * 			@item date_complete [datetime, optional] Course completion date in format yyyy-MM-dd HH:mm:ss (in UTC timezone)
	 * 			@item interactions [array, optional] A list of interactions by this user in this course
	 * 				@item id [string, required] The interaction name
	 * 				@item learner_response [mixed, optional] Either an array of interaction responses or string with the response of the user
	 * @response failed_users [array, optional] Only returned when status is "completed".
	 *       @item userid [integer, required] The ID of the user requested to process
	 *       @item error [string, required] Why this user failed to process, e.g. user doesn't exist
	 *       @end
	 *
	 */
	public function usersReportStatus(){
		Yii::import( 'root.api.protected.components.helpers.GenerateCourseAPIUsersReportImmediateJobHandler' );

		$jobId = $this->readParameter('job_id');

		if(!$jobId){
			throw new CHttpException( 401, 'No Job ID provided' );
		}

		$job = CoreJob::model()->findByAttributes(array('hash_id'=>$jobId));

		if(!$job){
			throw new CHttpException( 402, 'Job not found' );
		}

		$params = CJSON::decode($job->params);

		$output = array(
			'status'=>GenerateCourseAPIUsersReportImmediateJobHandler::STATUS_IN_PROGRESS, // default
		);

		if(isset($params['status'])){
			switch($params['status']){
				case GenerateCourseAPIUsersReportImmediateJobHandler::STATUS_COMPLETED:
					$output['status'] = GenerateCourseAPIUsersReportImmediateJobHandler::STATUS_COMPLETED;
					$output['data'] = $params['output']['data'];
					$output['failed_users'] = $params['output']['failed_users'];
					break;
				case GenerateCourseAPIUsersReportImmediateJobHandler::STATUS_IN_PROGRESS:
					$output['status'] = GenerateCourseAPIUsersReportImmediateJobHandler::STATUS_IN_PROGRESS;
					break;
				default:
					throw new CHttpException(403, "Invalid job state");
			}
		}else{
			throw new CHttpException(403, "Invalid job state");
		}

		return $output;
	}

	/**
	 * @summary Retrieves the list of courses in the LMS that a user is enrolled to
	 *
	 * @parameter id_user [integer, required] The user's id
	 *
	 * @status 401 User not found
	 *
	 * @response courses [array, required] the list of enrolled courses
	 *      @item course_id [integer, required] Course ID
	 *      @item code [string, optional] Alphanumeric course code
	 *      @item course_name [string, required] Course name/title
	 *      @item credits [string, optional] Course credits
	 *      @item total_time [string, optional] Total time in course
	 * 	    @item enrollment_date [datetime, optional] Enrollment start date
	 * 	    @item first_access_date [datetime, optional] Course first access date
	 * 	    @item completion_date [datetime, optional] Course completion date
	 *      @item score [integer, optional] User score for the course
	 *      @item status [string, required] Enrollment status
	 *      @end
	 *
	 */
	public function listEnrolled() {

		$idUser = (int) $this->readParameter('id_user');

		// check if the user exists
		$user = CoreUser::model()->findByPk($idUser);
		if (!$user)
			throw new CHttpException(401, 'User not found');

		// Do search in DB
		$criteria = new CDbCriteria();
		$criteria->addCondition("idUser = :idUser");
		$criteria->params[':idUser'] = $idUser;
		$enrolledCourses = LearningCourseuser::model()->findAll($criteria);

		// Prepare output list of courses
		$output = array();
		foreach ($enrolledCourses as $row) {
			$output[] = array(
				'course_id' => (int)$row->idCourse,
				'code' => $row->course->code,
				'course_name' => $row->course->name,
				'credits' => $row->course->credits,
				'total_time' => $row->renderTimeInCourse(),
				'enrollment_date' => $row->date_inscr,
				'completion_date' => $row->date_complete,
				'first_access_date'	=> $row->date_first_access,
				'score'	=> LearningCourseuser::getLastScoreByUserAndCourse($row->idCourse, $row->idUser),
				'status' => LearningCourseuser::getStatusLabel($row->status)
			);
		}

		return array('courses'=>$output);
	}

	/**
	 * @summary Retrieves course total enrollments count by its ID
	 *
	 * @status 401 No Course provided
	 * @status 402 Course not found
	 * @status 403 Group not found
	 *
	 * @parameter id_course [integer, required] The numerical ID of the course
	 * @parameter id_group [integer, optional] The numerical ID of the group
	 * @parameter exclude_suspended [boolean, optional] Show excluded users (default: true)
	 *
	 * @response count [integer, required] The total number of enrolled users for course
	 *
	 */
	public function getTotalUsersForCourse() {
		$idCourse = $this->readParameter('id_course');
		if(!$idCourse)
			throw new CHttpException( 401, 'No Course provided' );
		if(!LearningCourse::model()->isExistent($idCourse))
			throw new CHttpException( 402, 'Course not found: ' . $idCourse );

		$idGroup = $this->readParameter('id_group', false);
		$excludeSuspended = $this->readParameter('exclude_suspended', true);

		$criteria = new CDbCriteria();

		// If Group is passed
		if($idGroup){
			// Lets first check if the group exists
			if(!CoreGroup::model()->exists(array("condition" => 'idst = :group AND hidden = "false"', 'params' => array(":group" => $idGroup))))
				throw new CHttpException( 403, 'Group not found' );

			$criteria->join = ' JOIN ' . CoreGroupMembers::model()->tableName() . " cgm ON cgm.idstMember = t.idUser";
			$criteria->addCondition("cgm.idst = :group");
			$criteria->params[':group'] = $idGroup;
		}

		$criteria->addCondition("idCourse = :course");
		$criteria->params[":course"] = $idCourse;

		// Filter out the excluded user, i.e users with status != 3
		if($excludeSuspended)
			$criteria->addCondition("t.status <> 3");

		return array('count' => (int)LearningCourseuser::model()->count($criteria));
	}

	/**
	 * @summary Retrieves a list of users enrolled in course (optionally paginated)
	 *
	 * @status 401 No Course provided
	 * @status 402 Course not found
	 * @status 403 Group not found
	 *
	 * @parameter id_course [integer, required] The numerical ID of the course
	 * @parameter id_group [integer, optional] The numerical ID of the group
	 * @parameter exclude_suspended [boolean, optional] Show excluded users (default: true)
	 * @parameter page_size [integer, optional] Number of elements per page
	 * @parameter page_number [integer, optional] The number of the page to be shown
	 *
	 * @response report [array, optional] List of enrolled users
	 *     @item idst [integer, required]The ID of the user
	 *     @item status [integer, required] User status into the course (-2 = Waiting user, -1 = Subscription to confirm, 0 = Subscribed, 1 = In progress, 2 = Completed, 3 = Suspended, 4 = Overbooking)
	 *     @item enroll_date [datetime, required] The date when the user was enrolled into this course
	 *     @item date_first_access [datetime, required] The date when the user first accessed this course
	 *     @item date_complete [datetime, required] The date when the user completed this course
	 *     @item date_last_access [datetime, required] The date when the user last accessed this course
	 *     @item ext_type [string, required] External type (e.g. joomla, drupal)
	 *     @item ext_value [string, required] Value assigned for this external type
	 *     @item total_time [string, required] The total time the user has spent in the course
	 * @end
	 *
	 */
	public function reportCourseForUsers() {
		$idCourse = $this->readParameter('id_course');
		if(!$idCourse)
			throw new CHttpException( 401, 'No Course provided' );
		if(!LearningCourse::model()->isExistent($idCourse))
			throw new CHttpException( 402, 'Course not found' );

		$report = array();
		$idGroup = $this->readParameter('id_group');
		$excludeSuspended = $this->readParameter('exclude_suspended', true);
		$pageSize = $this->readParameter('page_size');
		$pageNumber = $this->readParameter('page_number');

		$query = Yii::app()->db->createCommand();
		$query->from(LearningCourseuser::model()->tableName(). ' lc');
		$query->join(CoreUser::model()->tableName(). " cu", "cu.idst = lc.idUser");
		$query->order(array("date_inscr DESC", "userid ASC"));
		// If the group is passed let's filter out the users, to users that are members to that group
		if($idGroup){
			// Lets first check if the group exists
			if(!CoreGroup::model()->exists(array("condition" => 'idst = :group AND hidden = "false"', 'params' => array(":group" => $idGroup))))
				throw new CHttpException( 403, 'Group not found' );

			$query->join(CoreGroupMembers::model()->tableName() . ' cgm', "cgm.idstMember = lc.idUser");
			$query->andWhere("cgm.idst = :group", array(":group" => $idGroup));
		}
		// Filter out the users, to users enrolled only in requested course
		$query->andWhere("idCourse = :course", array(":course" => $idCourse));

		// Filter out the excluded user, i.e users with status != 3
		if($excludeSuspended)
			$query->andWhere("lc.status <> 3");

		// Set pagination if needed
		if ($pageSize > 0) {
			if ($pageNumber < 0)
				$pageNumber = 0;  //force the first page

			$query->limit($pageSize);
			$query->offset($pageNumber * $pageSize);
		}

		// Get all the users as array of user's ids
		$usersList = $query->queryAll(true);

		// Get all the user's idst's only
		$usersIdst = array();
		foreach($usersList as $user)
			$usersIdst[] = $user['idUser'];

		$usersSettings = array();
		// Let's get user related data FROM CoreSettingUser
		$usersSettingsQuery = Yii::app()->db->createCommand()
			->from(CoreSettingUser::model()->tableName())
			->where(array('and', array('in', 'id_user', $usersIdst), array("like", "path_name", "ext.user.%")))
			->queryAll(true);
		// Let's convert this data
		foreach($usersSettingsQuery as $settings)
			$usersSettings[$settings['id_user']] = $settings;

		$usersTimes = array();
		// Let's get user related data from LearningTracksession
		$usersTimesQuery = Yii::app()->db->createCommand()
			->select(array("idUser", "SUM(ABS(UNIX_TIMESTAMP(lastTime) - UNIX_TIMESTAMP(enterTime))) AS total_time"))
			->from(LearningTracksession::model()->tableName())
			->where(array("and", "idCourse = :course", array('in', 'idUser', $usersIdst)), array( ":course" => $idCourse))
			->group("idUser")
			->queryAll(true);
		foreach($usersTimesQuery as $time)
			$usersTimes[$time['idUser']] = $time;

		// Let's build the list of users info
		foreach($usersList as $user){
			// Prepare the current user total time
			$total_time = isset($usersTimes[$user['idUser']]) ? (int)$usersTimes[$user['idUser']]['total_time'] : null;
			$hours = floor($total_time/3600);
			$minutes = floor(($total_time%3600)/60);
			$seconds = ($total_time%3600)%60;

			$report[] = array(
				'idst' => (int)$user['idUser'],
				'status' => (int)$user['status'],
				'enroll_date' => $user['date_inscr'],
				'date_first_access' => $user['date_first_access'],
				'date_complete' => $user['date_complete'],
				'date_last_access' => $user['date_last_access'],
				'ext_type' =>  isset($usersSettings[$user['idUser']]) ? str_replace('ext.user.', '', $usersSettings[$user['idUser']]['path_name']) : null,
				'ext_value' =>  isset($usersSettings[$user['idUser']]) ? str_replace('ext_user_', '', $usersSettings[$user['idUser']]['value']) : null,
				'total_time' => $hours."h ".$minutes."m ".$seconds."s"
			);
		}

		return array(
			'report' => $report
		);
	}
}
