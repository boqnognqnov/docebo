<?php

/**
 * Organization API Component
 * @description Set of APIs to manage course training materials
 */
class OrganizationApiModule extends ApiModule {

	/**
	 * Returns the requested security level for the passed action
	 * @param $actionName The name of the action
	 * @return string The requested security level
	 */
	public function getSecurityLevel($actionName)
	{
		switch ($actionName) {
			case 'listObjects':
			case 'play':
				return self::$SECURITY_LIGHT;
				break;
			default:
				return self::$SECURITY_STRONG;
		}
	}

	/**
	 * @summary Lists training materials of a passed course, with user status information
	 *
	 * @parameter id_course [integer, required] Internal ID of the course
	 * @parameter id_org [integer, optional] Training material folder id. If not passed, returns all materials under root.
     * @parameter id_scormitem [integer, optional] SCORM item (folder or sco) ID
	 * @parameter id_user [integer, optional] The id of the user for whom status is returned at the object level
	 *
	 * @response objects [array, required] Array of training materials found
	 *     @item type [string, required] The type of learning object returned (folder, test, scorm, etc)
	 *     @item id_org [integer, required] ID of the learning object
	 *     @item title [string, required] The title of the learning object
	 *     @item status [string, optional] The user status for this learning object
	 *     @item locked [boolean, optional] True of this learning object is locked (e.g. a prerequisite must be satisfied)
	 *     @item id_scormitem [integer, optional] The id of the scorm chapter (if type is scorm)
	 *
	 * @status 201 Missing required param "id_course"
	 * @status 202 Unable to list course objects
	 */
	public function listObjects()
	{
		$output = array('objects' => array());

		// Check mandatory params
		$id_course = $this->readParameter('id_course', false);
		if (!$id_course) $id_course = $this->readParameter('idCourse', false);
		if (!$id_course) {
			throw new CHttpException(201, 'Missing required param "id_course"');
		}

		//check if provided course is valid
		$course = LearningCourse::model()->findByPk($id_course);
		if (empty($course)) {
			throw new CHttpException(201, "Invalid specified course: course #$id_course does not exists");
		}

		$tree_params = array('userInfo' => true, 'scormChapters' => true, 'showHiddenObjects' => false, 'aiccChapters' => true);

		// Do we have a user id param?
		$id_user = $this->readParameter('id_user', null);
		if($id_user)
			$tree_params['targetUser'] = $id_user;
		else if ($this->user) // no requested user id -> use logged in API user
			$tree_params['targetUser'] = $this->user->idst;

		try {
			$tree = LearningObjectsManager::getCourseLearningObjectsTree($id_course, $tree_params);
		} catch(Exception $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			throw new CHttpException(202, "Unable to list course objects");
		}

		$id_org = $this->readParameter('id_org', false);
		$id_scormitem = $this->readParameter('id_scormitem', null);

		if (!$id_org) {

			$folder = $tree[0];
		} else {

			// manage id_org.id_scormitem param time
			$org_dotted = strpos($id_org, '.');
			if ($org_dotted !== false) {
				// extract the second part first or it will be erased by the second instruction
				$id_scormitem = substr($id_org, $org_dotted + 1);
				$id_org = substr($id_org, 0, $org_dotted);
			}

			$folder = $this->_findNodeByIdOrg($tree[0], $id_org, $id_scormitem);
		}

		if ($folder) {
			// Return all learning objects under found
			if (isset($folder['children'])) {
				foreach ($folder['children'] as $children) {
					$object = array(
						'id_org' => $children['idOrganization'],
						'title' => $children['title'],
					);

					if ($children['isFolder'])
						$object['type'] = "folder";
                    else {
						$object['type'] = $children['type'];
						$object['status'] = $children['status'];
						$object['locked'] = $children['locked'];
					}

                    if ($children['type'] == 'sco') {
						$object['id_scormitem'] = $children['idScormItem'];
						$object['id_org'] = $object['id_org'] . '.' . $object['id_scormitem'];
					}

					$output['objects'][] = $object;
				}
			}
		}

		return $output;
	}



	/**
	 * Go Depth First into the tree hierarchy of learning objects
	 * @param $current_node The current node to explore
	 * @param $id_org The id_org to search for
     * @param $id_scormitem Id of the scorm item (if any)
     *
	 * @return array The found node
	 */
	private function _findNodeByIdOrg($current_node, $id_org, $id_scormitem = null)
	{
		if (isset($current_node['children'])) {
			foreach ($current_node['children'] as $children) {
				if (($children['idOrganization'] == $id_org) && (!$id_scormitem || $id_scormitem == $children['idScormItem']))
					return $children;
                else {
					$node = $this->_findNodeByIdOrg($children, $id_org, $id_scormitem);
					if ($node)
						return $node;
				}
			}
		}

		return null;
	}




	/**
	 * @summary Gets the launch URL for a learning object
	 *
	 * @parameter id_org [integer, required] ID of the learning object to play
	 * @parameter id_scormitem [integer, optional] The id of the scorm chapter (if type is scorm)
	 * @parameter id_user [integer, optional] The id of the user that this url is generated for
	 *
	 * @response launch_url [string, required] The launch URL to play the passed learning object
	 *
	 * @status 201 Can't find learning object with the provided id_org, Missing required param "id_org"
	 * @status 202 Unsupported learning object type
	 * @status 203 Bad or missing resource id
	 * @status 204 Error while retrieving the launch URL for the requested resource
	 */
	public function play()
	{
		$id_org = $this->readParameter('id_org', false);
		if (!$id_org)
			throw new CHttpException(201, 'Missing required param "id_org"');

		// Load the passed learning object
		$id_org = $this->readParameter('id_org', 0);

		// Get SCORM Item ID, if any
		$id_scorm_item = (int)$this->readParameter('id_scormitem', false);

		// Get user id
		$id_user = $this->readParameter('id_user', null);

		// Manage dotted id_org
		$org_dotted = strpos($id_org, '.');
		if ($org_dotted !== false) {
			// extract the second part first or it will be erased by the second instruction
			$id_scorm_item = substr($id_org, $org_dotted + 1);
			$id_org = substr($id_org, 0, $org_dotted);
		}

		// Get Launch URL
		$result = Player::getLoAuthPlayUrl($id_org, $id_user ? $id_user : $this->user->idst, $id_scorm_item);

		if (!$result["success"]) {
			throw new CHttpException($result["err"], Player::$errorMessages[$result["err"]]);
		}

		return array('launch_url' => $result["url"]);
	}

	/**
	 * @summary Returns the list of SCORM interactions for a certain user in a SCO object
	 *
	 * @parameter id_user [integer, required] The id of the user whose interactions will be retrieved
	 * @parameter id_scormitem [integer, required] The id of the SCO item where the test/quiz is located
	 *
	 * @response interactions [array, required] Array of questions and user answers for every SCO interaction
	 * 		@item type [string, required] The type of interaction (e.g. 'true-false', 'choice', 'fill-in', 'long-fill-in', 'matching', 'numeric')
	 * 		@item id [string, required] Question ID (e.g. Question4_1)
	 * 		@item timestamp [string, required] Time at which the interaction was first made available to the learner for interaction and response (e.g. 15:49:13)
	 *		@item correct_responses [string, required] Array of correct responses
	 * 		@item weighting [integer, optional] Weight given to the interaction relative to other interactions
	 * 		@item learner_response [string, required] Array of responses given by the user for the current question
	 * 		@item result [string, required] Judgment of the correctness of the learner response (e.g. wrong)
	 * 		@item latency [string, optional] Time elapsed between the time the interaction was made available to the learner for response and the time of the first response (e.g 0000:00:13.15)
	 *
	 * @status 201 Missing required input parameters
	 * @status 202 Invalid id_user
	 * @status 203 Invalid id_scormitem
	 * @status 254 No SCORM tracking data available for the specified user
	 */
	public function getUserInteractions()
	{
		// Input parameters check
		$id_user = (int) $this->readParameter('id_user', null);
		$id_scorm_item = (int) $this->readParameter('id_scormitem', false);
		if(!$id_user || !$id_scorm_item)
			throw new CHttpException(201, "Missing required input parameters");

		// Check if user is present
		$user = CoreUser::model()->findByPk($id_user);
		if(!$user)
			throw new CHttpException(202, "Invalid id_user");

		// Check if SCO is present
		$scoItem = LearningScormItems::model()->findByPk($id_scorm_item);
		if(!$scoItem)
			throw new CHttpException(203, "Invalid id_scormitem");

		// Load the tracking model
		$trackingModel = LearningScormTracking::model()->findByAttributes(array(
			'idUser' => $id_user,
			'idscorm_item' => $id_scorm_item
		));
		if(!$trackingModel)
			throw new CHttpException(254, "No SCORM tracking data available for the specified user");

		// Returns the array of interactions
		return array('interactions' => $trackingModel->getLearnerInteractions(array($id_user)));
	}

}