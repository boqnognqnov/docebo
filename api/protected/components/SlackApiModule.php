<?php

/**
 * Slack API Component
 * @description Set of APIs to handle some Slack app related actions in the LMS
 */
class SlackApiModule extends ApiModule {


	/**
	 * Returns the requested security level for the passed action
	 * @param $actionName The name of the action
	 * @return string The requested security level
	 */
	public function getSecurityLevel($actionName) {

		switch($actionName) {
			case 'setToken':
				return self::$SECURITY_NONE;
				break;
			default:
				return self::$SECURITY_STRONG;
		}
	}




	/**
	 * @summary Set a token for a slack team
	 *
	 * @parameter token [string, required] The token value to be set
	 * @parameter team_name [string, required] The team name in which set the token
	 *
	 * @status 201 token or team value is invalid
	 *
	 * @throws CHttpException
	 */
	public function setToken() {
		//check parameters
		$token = $this->readParameter('token', null);
		//$teamId = $this->readParameter('team_id', '');
		$teamName = $this->readParameter('team_name', '');

		//validate token parameter
		if (empty($token)) {
			throw new CHttpException(201, "Invalid token");
		}

		//create the new team in the LMS
		$team = new SlackTeam();
		$team->domain = $teamName;
		$team->token = $token;
		$team->synch_info = -1;
		$team->enable_signin = 0;
		if (!$team->save()) {
			throw new CHttpException(201, "An error occurred while trying to save new team info");
		}

		//save token info in the team record
		$team->token = $token;
		$team->save();
	}

}
