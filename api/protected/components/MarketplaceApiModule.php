<?php

/**
 * Marketplace API Component
 * Set of APIs to manage marketplace
 */
class MarketplaceApiModule extends ApiModule {


	/**
	 * Returns list of courses in THIS LMS.
	 * Implements specific, marketplace related filtering
	 * Possible parameters in $params:
	 * 'fields' => 'coma,separated,list,of,fields,to, return' (see learning_course table)
	 * 'mpidCourse' => ###  Marketlace ID (integer) of a course to search for in LMS
	 */
	public function courses() {

		// Incoming parameters (associative array)
		$params = array(
			'fields' => trim($this->readParameter('fields')),
			'mpidCourse' => $this->readParameter('mpidCourse')
		);

		// Check if we have to filter by specific course id (MP course id!!!)
		$mpidCourse = ((int)$params['mpidCourse'] > 0 ? (int)$params['mpidCourse'] : null);

		// Check if we have to extract only a set of fields ?  This must be a comma separated list of field
		// Set to ALL (*) if no incoming request for this
		$fields = ($params['fields'] != '' ? $params['fields'] : ' * ');

		$output = array();
		try {

			//retrieve data from DB. Use CDbCommand in order to get arrays as records
			$command = Yii::app()->db->createCommand()
				->select($fields)
				->from(LearningCourse::model()->tableName());

			if ($mpidCourse !== null) {
				$command->where("mpidCourse = :mpid_course", array(':mpid_course' => $mpidCourse));
			}
			$rows = $command->queryAll();

			//prepare output info
			$output['message'] = 'Operation successful. Check "data"!';
			$output["data"] = $rows;

		} catch (CHttpException $e) {
			throw $e;
		} catch (Exception $e) {
			throw new CHttpException(500, $e->getMessage()/*'Unknown error occured in Marketplace API controller'*/);
		}

		return $output;
	}



	/**
	 * Change abs_max_subscription of a course in LMS.
	 * NOTE: this method should be moved into ErpApiController (main), but for test only: here
	 */
	public function absMaxSubscriptions() {

		// Incoming parameters (associative array)
		$params = array(
			'abs_max_subscriptions' => $this->readParameter('abs_max_subscriptions'),
			'mpidCourse' => $this->readParameter('mpidCourse'),
			'additive' => (bool)$this->readParameter('additive')
		);
		
		//validate course id  parameter (MP course id!!!)
		$mpidCourse = ((int)$params['mpidCourse'] > 0 ? (int)$params['mpidCourse'] : null);
		if ($mpidCourse === null) {
			throw new CHttpException(201, 'Invalid or not specified Course Id');
		}

		//validate abs_max_subscriptions  parameter
		$newValue = (!empty($params['abs_max_subscriptions']) ? (int)$params['abs_max_subscriptions'] : null);
		if ($newValue === null) {
			throw new CHttpException(201, 'Invalid or not specified number of subscription');
		}

		//retrieve course AR
		$course = LearningCourse::model()->findByAttributes(array('mpidCourse' => $mpidCourse));
		if (!$course) {
			throw new CHttpException(201, 'Course not found: ID = '.$mpidCourse);
		}

		// Are we adding subscriptions to existing or reseting ?
		if ($params['additive']) {
			$course->abs_max_subscriptions += $newValue;
		} else {
			$course->abs_max_subscriptions = $newValue;
		}

		//save new data
		if (!$course->save()) {
			throw new CHttpException(500, 'Error while updating course');
		}

		return array(
			'message' => 'Operation successful, abs_max_subscriptions upgraded',
			'id_course' => $course->idCourse
		);
	}




	/**
	 * Check if a Marketplace Course is installed and return feedback to API caller
	 */
	public function isMpCourseInstalled() {
		// Initial values
		$out = array('success' => false, 'message' => 'Unknown error occured in Marketplace API controller');
		
		// Incoming parameters (associative array)
		$params = array(
			'mpidCourse' => $this->readParameter('mpidCourse')
		);
		
		//validate course id  parameter (MP course id!!!)
		$mpidCourse = ((int)$params['mpidCourse'] > 0 ? (int)$params['mpidCourse'] : null);
		if ($mpidCourse === null) {
			throw new CHttpException(201, 'Invalid or not specified Course Id');
		}

		//retrieve course AR
		$output = array();
		$course = LearningCourse::model()->findByAttributes(array('mpidCourse' => $mpidCourse));
		if (!$course) {

			$output['message'] = 'Course Id '.$mpidCourse.'not installed';
			$output['data']['installed'] = false;

		} else {

			//count enrolled users
			$countEnrolled = Yii::app()->db->createCommand()
				->select("COUNT(*) AS num_enrolled")
				->from(LearningCourseuser::model()->tableName())
				->where("idCourse = :id_course", array(':id_course' => $course->idCourse))
				->queryScalar();

			$out['message'] = 'Course Id '.$mpidCourse.' is installed';
			$out['data']['installed'] = true;
			$out['data']['abs_max_subscriptions'] = (int)$course->abs_max_subscriptions;
			$out['data']['seats_taken'] = (int)$countEnrolled;

		}
		
		return $output;
	}


}
