<?php

/**
 * Learning Plan API Component
 * @description Set of APIs to manage Learning Plans
 */
class LearningplanApiModule extends ApiModule {

	const STATUS_NOT_STARTED	= 'not started';
	const STATUS_IN_PROGRESS	= 'in progress';
	const STATUS_COMPLETED 		= 'completed';

	/**
	 * @summary Enrolls user in a learning plan
	 * @notes This action will enroll the user to all courses of the LP. For session-based courses (i.e. ILT, Webinar) no session is selected.
	 *
	 * @parameter id_learningplan [integer, optional] ID of the learning plan to enroll
	 * @parameter learningplan_code [string, optional] Code of the learning plan to enroll (it can be passed instead of id_learningplan)
	 * @parameter id_user [integer, required] Docebo ID of the user to enroll
	 * @parameter enrollment_fields [array, optional] array of additional enrollment fields. Key = ID of the field, value = value of the field
	 *
	 * @status 201 Missing mandatory params
	 * @status 202 Invalid Learning Plan ID/code provided or Invalid User ID provided
	 * @status 203 User is already enrolled in this Learning Plan
	 * @status 264 Invalid enrollment fields
	 */
	public function enroll() {
		// Check mandatory params
		$idPlan = $this->readParameter('id_learningplan');
		$code = $this->readParameter('learningplan_code');
		$idUser = $this->readParameter('id_user');
		if((!$idPlan && !$code) || !$idUser)
			throw new CHttpException(201, "Missing mandatory params");

		// Check ID of the user & plan
		if($idPlan)
			$curriculaModel = LearningCoursepath::model()->findByPk($idPlan);
		else
			$curriculaModel = LearningCoursepath::model()->findByAttributes(array('path_code' => $code));
		if (!$curriculaModel)
			throw new CHttpException(202, "Invalid Learning Plan ID/code provided");
		else
			$idPlan = $curriculaModel->id_path;

		$userModel = CoreUser::model()->findByPk($idUser);
		if (!$userModel)
			throw new CHttpException(202, "Invalid User ID provided");

		// Check if user is already enrolled to this LP
		$enrollment = LearningCoursepathUser::model()->findByAttributes(array('idUser' => $idUser, 'id_path' => $idPlan));
		if($enrollment)
			throw new CHttpException(203, "User is already enrolled in this Learning Plan");


		$fields = $this->readParameter("enrollment_fields", array());

		Log::_($fields);

		
		$fieldsToValidate = $fields;
		if (!is_array($fields)) {
			$fieldsToValidate = json_decode($fields);
		}
		$validFields = LearningEnrollmentField::validateEnrollmentArray($fieldsToValidate);


		if(empty($validFields['success'])){
			throw new CHttpException(264, 'Invalid enrollment fields');
		}

		// Ok, Let's try to enroll the user in this LP
		$enrollment = new LearningCoursepathUser();
		$enrollment->id_path = $idPlan;
		$enrollment->idUser = $idUser;
		$enrollment->waiting = 0;
		$enrollment->course_completed = 0;
		$enrollment->date_assign = Yii::app()->localtime->getLocalNow();
		$enrollment->subscribed_by = Yii::app()->user->id;
		$enrollment->save();
		$curriculaModel->subscribeSingleUserToCourses($idUser, $fields);
		Yii::app()->event->raise(EventManager::EVENT_USER_ENROLLED_IN_LEARNING_PLAN, new DEvent($this, array('user_id' => $idUser, 'learning_plan' => $curriculaModel)));
	}

	/**
	 * @summary Unenrolls a user from a learning plan
	 * @notes This action will unenroll the user from all courses of the LP.
	 *
	 * @parameter id_learningplan [integer, optional] ID of the learning plan to unenroll
	 * @parameter learningplan_code [string, optional] Code of the learning plan to unenroll from (it can be passed instead of id_learningplan)
	 * @parameter id_user [integer, required] Docebo ID of the user to unenroll
	 *
	 * @status 201 Missing mandatory params
	 * @status 202 Invalid Learning Plan ID/code provided or Invalid User ID provided
	 * @status 203 User is not enrolled in this Learning Plan
	 */
	public function unenroll() {
		// Check mandatory params
		$idPlan = $this->readParameter('id_learningplan');
		$code = $this->readParameter('learningplan_code');
		$idUser = $this->readParameter('id_user');
		if((!$idPlan && !$code) || !$idUser)
			throw new CHttpException(201, "Missing mandatory params");

		// Check ID of the user & plan
		if($idPlan)
			$curriculaModel = LearningCoursepath::model()->findByPk($idPlan);
		else
			$curriculaModel = LearningCoursepath::model()->findByAttributes(array('path_code' => $code));
		if (!$curriculaModel)
			throw new CHttpException(202, "Invalid Learning Plan ID/code provided");
		else
			$idPlan = $curriculaModel->id_path;

		$userModel = CoreUser::model()->findByPk($idUser);
		if (!$userModel)
			throw new CHttpException(202, "Invalid User ID provided");

		// Check if user is already enrolled to this LP
		$enrollment = LearningCoursepathUser::model()->findByAttributes(array('idUser' => $idUser, 'id_path' => $idPlan));
		if(!$enrollment)
			throw new CHttpException(203, "User is not enrolled in this Learning Plan");

		// Ok, Let's try to unenroll the user from this LP
		$enrollment->delete();
		Yii::app()->event->raise(EventManager::EVENT_USER_UNENROLLED_FROM_LEARNING_PLAN, new DEvent($this, array(
			'learningPlan' => $enrollment->path,
			'user' => $enrollment->user,
		)));

		// Get ALL curricula courses and unsubscribe the user
		$courses = $curriculaModel->getCoursesIdList();
		LearningCourseuser::unSubscribeUsersFromCourses(array($idUser), $courses);
	}

	/**
	 * @summary API to list available learning plans
	 * @notes If you expect a significant number of records, we strongly recommend that you use pagination to retrieve results in chunks.
	 *
	 * @parameter from [integer, optional] (default=false) the offset to use for the pagination (starts from 0)
	 * @parameter count [integer, optional] (default=false) the number of records to return per each call. If passed as false, all records are returned.
	 *
	 * @response learning_plans [array, required] An array of one or more learning plan records
	 *      @item id_path [integer, required] The internal Docebo ID for the learning plan
	 *      @item path_code [string, optional] The alphanumeric code of the learning plan
	 *      @item path_credits [integer, optional] The number of credits provided by the plan
	 *      @item path_description [string, optional] The description of the learning plan
	 *      @item visible_in_catalogs [boolean, required] If the learning plan is visible in catalogs
	 *      @item on_sale [boolean, required] If the learning plan is available for sale
	 */
	public function listPlans() {
		$from 	= (int) $this->readParameter('from');
		$count 	= (int) $this->readParameter('count');

		// Prepare queries
		$command = Yii::app()->db->createCommand()
			->select("cu.*")
			->from(LearningCoursepath::model()->tableName() . " cu");
		if ($count > 0 && $from >= 0)
			$command->limit($count, $from);

		// Execute DB queries
		$list = $command->queryAll();
		$output = array();

		if (!empty($list)) {
			foreach ($list as $item) {
				$output[] = array(
					'id_path'				=> (int) $item['id_path'],
					'path_code'				=> $item['path_code'],
					'path_credits'			=> (int) $item['credits'],
					'path_description'      => $item['path_descr'],
					'visible_in_catalogs'   => (boolean) $item['visible_in_catalog'],
					'on_sale'           	=> (boolean) $item['is_selling']
				);
			}
		}

		return array('learning_plans' => $output);
	}

	/**
	 * @summary Retrieves a list of enrollment records for learning plans
	 * @notes At least one filter criteria should be selected OR if no filter is selected the pagination will be no more than 10.000 enrollment each time.
	 *
	 * @parameter from [integer, optional] (default=false) the offset to use for the pagination (starts from 0)
	 * @parameter count [integer, optional] (default=false) the number of records to return per each call. If passed as false, all records are returned.
	 * @parameter ids_path [string, optional] comma separated list of learning plan IDs, needed to filter the results.
	 * @parameter path_codes [string, optional] comma separated list of learning plan codes, needed to filter the results. This param is ignored if id_paths is passed non empty.
	 * @parameter user_ids [string, optional] comma separated user ids, needed to filter the results.
	 *
	 *
	 * @response enrollments [array, required] Array of enrollment records
	 *      @item id_path [integer, required] the id of the learning plan
	 *      @item path_code [string, optional] the code of the learning plan
	 *      @item id_user [integer, required] the enrolled user ID
	 *      @item first_name [string, optional] the enrolled user first name
	 *      @item last_name [string, optional] the enrolled user last name
	 *      @item subscription_date [datetime, required] the date the user is enrolled in the plan (in yyyy-MM-dd HH:mm:ss format, UTC timezone)
	 *      @item complete_date [datetime, optional] the date the user completed the plan (in yyyy-MM-dd HH:mm:ss format, UTC timezone)
	 *      @item status [string, required] the status of the enrollment (not started, in progress, completed)
	 *      @item percentage [integer, required] the completion percentage
	 *
	 *
	 * @status 201 Invalid pagination params provided
	 */
	public function enrollments() {
		$from 	= (int) $this->readParameter('from');
		$count 	= (int) $this->readParameter('count');

		$ids_path 	= $this->readParameter('ids_path');
		$path_codes = $this->readParameter('path_codes');
		$user_ids 	= $this->readParameter('user_ids');

		if ($count < 0 || $from < 0)
			throw new CHttpException(201, "Invalid pagination params provided");

		$columns = array (
			"plan.id_path   as id_path",
			"plan.path_code as path_code",
			"planUser.date_assign as subscription_date",
			"user.idst       as id_user",
			"user.firstname  as first_name",
			"user.lastname   as last_name",
		);

		$command = Yii::app()->db->createCommand()
			->select($columns)
			->from('learning_coursepath_user planUser')
			->join('learning_coursepath plan', 'plan.id_path = planUser.id_path')
			->join('core_user user', 'user.idst = planUser.idUser');

		if ($user_ids) {
			$userIdsArr = explode(",", $user_ids);
			if(!empty($userIdsArr))
				$command->andWhere(array('in', 'planUser.idUser', $userIdsArr));
		}

		if ($ids_path) {
			$pathIdsArr = explode(",", $ids_path);
			if(!empty($pathIdsArr))
				$command->andWhere(array('in', 'plan.id_path', $pathIdsArr));
		} else if (!empty($path_codes))  {
			$pathCodesArr = explode(",", $path_codes);
			$pathCodesArr = $this->addLikeSymbols($pathCodesArr);
			if(!empty($pathCodesArr))
				$command->andWhere(array('or like', 'path_code', $pathCodesArr));
		}

		if (!$count || $count > 10000)
			$count = 10000;

		$command->limit($count, $from);

		// Execute DB queries
		$list = $command->queryAll();
		$output = array();
		if (!empty($list)) {
			foreach ($list as $item) {
				$planCompleteDate = $this->getLastCompletionDate($item['id_path'], $item['id_user']);
				$percentage = $this->getPlanPercentageCompleted($item['id_path'], $item['id_user']);

				$status = self::STATUS_NOT_STARTED;
				if ($percentage > 0)
					$status = self::STATUS_IN_PROGRESS;
				if ($percentage == 100)
					$status = self::STATUS_COMPLETED;

				$output[] = array(
					'id_path'	=> (int) $item['id_path'],
					'path_code'	=> $item['path_code'],
					'id_user'	=> (int) $item['id_user'],
					'first_name'=> $item['first_name'],
					'last_name'	=> $item['last_name'],
					'subscription_date' => $item['subscription_date'],
					'complete_date'		=> ($percentage == 100) ? $planCompleteDate : "",
					'status'           	=> $status,
					'percentage'		=> $percentage
				);
			}
		}

		return array('learning_plans' => $output);
	}

	/**
	 * Inserts "%code%" symbols to an array of elements (to be used in mysql query)
	 * @param $pathCodesArr
	 * @return mixed
	 */
	private function addLikeSymbols($pathCodesArr) {
		for ($i=0; $i<count($pathCodesArr); $i++)
			$pathCodesArr[$i] = '%' . $pathCodesArr[$i] . '%';

		return $pathCodesArr;
	}

	/**
	 * Returns the completion percentage for a LP
	 * @param $planID
	 * @param $userID
	 * @return float|int
	 */
	private function getPlanPercentageCompleted($planID, $userID){
		$total = 0;

		$command = Yii::app()->db->createCommand()
			->select("COUNT(*)")
			->from('learning_courseuser courseUser')
			->join('learning_coursepath_courses planCourses', 'planCourses.id_item = courseUser.idCourse')
			->andWhere("courseUser.status = :status", array(":status" => LearningCourseuser::$COURSE_USER_END)) // completed
			->andWhere("courseUser.idUser = :idUser", array(":idUser" => $userID))
			->andWhere("planCourses.id_path = :id_path", array(":id_path" => $planID));

		$completed = $command->queryScalar();

		$command = Yii::app()->db->createCommand()
			->select("COUNT(*)")
			->from('learning_coursepath_courses planCourses')
			->where("(planCourses.id_path = :id_path) AND (planCourses.mode <> 'all')", array(":id_path" => $planID));

		$coursesWithOrPrerequisites = $command->queryScalar();

		if ($coursesWithOrPrerequisites > 0) {
			$command = Yii::app()->db->createCommand()
				->select("COUNT(*)")
				->from('learning_coursepath_courses planCourses')
				->join('learning_courseuser courseUser', 'planCourses.id_item = courseUser.idCourse')
				->where("(planCourses.id_path = :id_path) AND courseUser.idUser = :idUser", array(":id_path" => $planID, ":idUser" => $userID))
				->order('sequence DESC');

			$lastCourse = $command->queryRow();

			if ($lastCourse->status == LearningCourseuser::$COURSE_USER_END)
				$total = $completed;
		}

		if (!$total) {
			$command = Yii::app()->db->createCommand()
				->select("COUNT(*)")
				->from('learning_coursepath_courses planCourses')
				->where("planCourses.id_path = :id_path", array(":id_path" => $planID));

			$total = $command->queryScalar();
		}

		return ($completed == 0 ? 0 : round(($completed / $total) * 100, 0));
	}

	/**
	 * Determines the date when this LP was completing (using the highest course completion date)
	 * @param $planID
	 * @param $userID
	 * @return mixed
	 */
	private function getLastCompletionDate($planID, $userID){
		$command = Yii::app()->db->createCommand()
			->select("MAX(date_complete)")
			->from('learning_coursepath_courses planCourse')
			->join('learning_courseuser courseUser', 'planCourse.id_item = courseUser.idCourse')
			->andWhere("courseUser.date_complete IS NOT NULL")
			->andWhere("courseUser.idUser = :idUser", array(":idUser" => $userID))
			->andWhere("planCourse.id_path = :id_path", array(":id_path" => $planID));

		$result = $command->queryRow();
		return $result['MAX(date_complete)'];
	}
}
