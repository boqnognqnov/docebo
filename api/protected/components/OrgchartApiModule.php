<?php

/**
 * Orgchart API Component
 * @description Set of APIs to manage organization charts
 */
class OrgchartApiModule extends ApiModule {

	const MAX_SIZE = 100;

	/**
	 * Returns the requested security level for the passed action
	 * @param $actionName The name of the action
	 * @return string The requested security level
	 */
	public function getSecurityLevel($actionName)
	{
		switch ($actionName) {
			default:
				return self::$SECURITY_STRONG;
		}
	}

	/**
	 * @summary Creates an orgchart node with the passed information under the provided parent node.
	 *
	 * @parameter code [string, optional] alphanumeric code for the node
	 * @parameter translation [object, optional] Array of node name translations, indexed by language (e.g. translation[english] = "English name", translation[french] = "French name")
	 *     @item english [string, optional] Translation in English
	 *     @item italian [string, optional] Translation in Italian and so on...
	 * @parameter id_parent [integer, optional] ID of the parent node (defaults to root node)
	 *
	 * @response id_org [integer, required] The id of the newly created node
	 *
	 * @status 401 Missing or invalid required param "id_parent"
	 * @status 402 Missing or invalid required param "translation"
	 * @status 500 Error while saving new node
	 */
	public function createNode() {
		$code = $this->readParameter('code');
		$translations = $this->readParameter('translation');
		$idParent = $this->readParameter('id_parent', CoreOrgChartTree::getOrgRootNode()->idOrg);
		$activeLanguagesList = CoreLangLanguage::getActiveLanguages();

		$parent = CoreOrgChartTree::model()->findByPk($idParent);
		if (!$parent)
			throw new CHttpException(401, 'Missing or invalid required param "id_parent"');

		if (empty($translations))
			throw new CHttpException(402, 'Missing or invalid required param "translation"');

		$coreOrgChartTree = new CoreOrgChartTree();
		$coreOrgChartTree->code = $code;
		if ($coreOrgChartTree->validate()) {
			$coreOrgChartTree->appendTo($parent);

			// due to foreign key constraints, translations must be added AFTER tree node creation
			$orgChartId = $coreOrgChartTree->getPrimaryKey();
			if (!empty($orgChartId) && !empty($translations)) {
				foreach ($activeLanguagesList as $key => $lang) {
					$translation              = new CoreOrgChart();
					$translation->id_dir      = $orgChartId;
					$translation->lang_code   = $key;
					$translation->translation = isset($translations[$key]) ? $translations[$key] : '';
					$translation->save();
				}
			}
			// end of translations filling

			//handling of additional fields:
			// - if coming from root node then set all fields ON
			// - otherwise set only the parent folder fields without considering the root
			$rootNode = CoreOrgChartTree::getOrgRootNode();
			$additionalFields = array();
			if ($idParent != $rootNode->getPrimaryKey()) {
				//find all additional fields inherited from ancestor nodes
				//NOTE: parent node alone should already own all needed fields, but read from all ancestors the same to prevent possible errors
				$cmd = Yii::app()->db->createCommand("SELECT gf.id_field "
					." FROM ".CoreOrgChartTree::model()->tableName()." oct "
					." JOIN ".CoreGroupFields::model()->tableName()." gf ON (gf.idst = oct.idst_oc OR gf.idst = oct.idst_ocd) "
					." WHERE oct.iLeft <= :iLeft AND oct.iRight >= :iRight AND oct.idOrg <> :root");
				$reader = $cmd->query(array(
					':iLeft' => $parent->iLeft,
					':iRight' => $parent->iRight,
					':root' => $rootNode->getPrimaryKey()
				));
				while ($row = $reader->read()) {
					$additionalFields[] = $row['id_field'];
				}
				$additionalFields = array_unique($additionalFields);
			} else {
				//plain assign all additional fields
				$cmd = Yii::app()->db->createCommand("SELECT DISTINCT(id_field) FROM ".CoreUserField::model()->tableName());
				$reader = $cmd->query();
				while ($row = $reader->read()) { $additionalFields[] = $row['id_field']; }
			}

			//insert new values
			foreach ($additionalFields as $additionalField) {
				$assignment = new CoreGroupFields();
				$assignment->idst = $coreOrgChartTree->idst_ocd;
				$assignment->id_field = $additionalField;
				$assignment->mandatory = 'true';
				$rs = $assignment->save();
				if (!$rs) { throw new CException('Error while saving new field node assignments'); }
			}

			//end of additional fields assignment

			return array('id_org' => $orgChartId);
		} else
			throw new CHttpException(500, "Error while saving new node");
	}

	/**
	 * @summary Updates an orgchart node with the passed information.
	 *
	 * @parameter id_org [integer, required] node id
	 * @parameter code [string, optional] alphanumeric code for the node
	 * @parameter translation [object, optional] Array of node name translations, indexed by language (e.g. translation[english] = "English name", translation[french] = "French name")
	 *     @item english [string, optional] Translation in English
	 *     @item italian [string, optional] Translation in Italian and so on...
	 * @parameter new_parent [integer, optional] New parent node
	 *
	 * @status 401 Missing or invalid required param "id_org"
	 * @status 402 Missing or invalid required param "translation"
	 * @status 403 Invalid new parent node
	 */
	public function updateNode() {
		$code = $this->readParameter('code');
		$translations = $this->readParameter('translation');
		$idOrg = $this->readParameter('id_org');
		$newParentId = $this->readParameter('new_parent');

		/* @var $orgChartTree CoreOrgChartTree */
		$orgChartTree = CoreOrgChartTree::model()->findByPk($idOrg);
		if (!$orgChartTree)
			throw new CHttpException(401, 'Missing or invalid required param "id_org"');

		if (empty($translations))
			throw new CHttpException(402, 'Missing or invalid required param "translation"');

		$newParentNodeTree = null;
		if($newParentId) {
			$newParentNodeTree = CoreOrgChartTree::model()->findByPk($newParentId);
			if (!$newParentNodeTree)
				throw new CHttpException(403, 'Invalid new parent node');
		}

		if (!empty($code))
			$orgChartTree->code = $code;

		if ($orgChartTree->validate()) {
			$orgChartTree->saveNode();

			$dbTranslations = CoreOrgChart::model()->findAllByAttributes(array(
				'id_dir' => $idOrg,
			));

			$translationsByKey = array();
			if (!empty($dbTranslations)) {
				foreach ($dbTranslations as $row) {
					$translationsByKey[$row->lang_code] = $row;
				}
			}

			foreach ($translations as $key => $lang) {
				if(!isset($translationsByKey[$key])) {
					$translationsByKey[$key]              = new CoreOrgChart();
					$translationsByKey[$key]->id_dir      = $idOrg;
					$translationsByKey[$key]->lang_code   = $key;
				}
				$translationsByKey[$key]->translation = $lang;
				$translationsByKey[$key]->save();
			}

			if($newParentNodeTree)
				$this->_moveNodeUnderNewParent($orgChartTree, $newParentNodeTree);
		} else
			throw new CHttpException(500, "Error while saving node");
	}

	/**
	 * @summary Deletes the specified node from the orgchart node.
	 *
	 * @parameter id_org [integer, required] ID of the node to delete
	 *
	 * @status 401 Missing or invalid required param "id_org"
	 * @status 402 Cannot delete non-leaf nodes
	 */
	public function deleteNode() {
		$idOrg = $this->readParameter('id_org');

		$orgChartTree = CoreOrgChartTree::model()->findByPk($idOrg);

		if (!$orgChartTree)
			throw new CHttpException(401, 'Missing or invalid required param "id_org"');

		if ($orgChartTree->isLeaf())
			CoreOrgChartTree::model()->removeNode($orgChartTree);
		else
			throw new CHttpException(402, 'Cannot delete non-leaf nodes');
	}

	/**
	 * @param $srcOrgNode CoreOrgChartTree
	 * @param $dstOrgNode CoreOrgChartTree
	 */
	private function _moveNodeUnderNewParent($srcOrgNode, $dstOrgNode) {
		CoreOrgChartTree::model()->flushCache();
		if(!$srcOrgNode->moveAsFirst($dstOrgNode))
			throw new CHttpException(405, "Error while moving node");
		//make sure additional fields are properly inherited
		CoreGroupFields::propagateBranchFieldAssignments($dstOrgNode);
	}

	/**
	 * @summary Moves the specified node from its current position to another one in the orgchart.
	 *
	 * @parameter id_org [integer, required] node id
	 * @parameter dst_node_id [integer, required] destination parent node
	 *
	 * @status 401 Missing or invalid required param "id_org"
	 * @status 402 Missing or invalid required param "dst_node_id"
	 */
	public function moveNode() {
		$idOrg = $this->readParameter('id_org');
		$dstNodeId = $this->readParameter('dst_node_id');

		$srcOrgNode = CoreOrgChartTree::model()->findByPk($idOrg);
		if (!$srcOrgNode || !$srcOrgNode->isLeaf())
			throw new CHttpException(401, 'Missing or invalid required param "id_org"');

		$dstOrgNode = CoreOrgChartTree::model()->findByPk($dstNodeId);
		if (!$dstOrgNode)
			throw new CHttpException(402, 'Missing or invalid required param "dst_node_id"');

		$this->_moveNodeUnderNewParent($srcOrgNode, $dstOrgNode);
	}

	/**
	 * @summary Retrieves information about a node.
	 *
	 * @parameter id_org [integer, required] ID of the node to retrieve
	 *
	 * @response code [string, required] Alphanumeric code for the node
	 * @response translation [object, required] Array of translations in all system languages
	 * 			@item english [string, optional] Translation in English
	 * 			@item italian [string, optional] Translation in Italian and so on...
	 * 			@end
	 *
	 * @status 401 Missing or invalid required param "id_org"
	 * @status 402 No such branch found
	 */
	public function getNodeInfo() {
		$idOrg = $this->readParameter('id_org', null);
		if (is_null($idOrg))
			throw new CHttpException(401, 'Missing required param "id_org"');

		$orgChartTree = CoreOrgChartTree::model()->findByPk($idOrg);
		if ($orgChartTree) {
			$result = array('code' => $orgChartTree->code, 'translation' => array());

			$dbTranslations = CoreOrgChart::model()->findAllByAttributes(array('id_dir' => $idOrg));
			if (!empty($dbTranslations)) {
				foreach ($dbTranslations as $row)
					$result['translation'][$row->lang_code] = $row->translation;
			}

			$activeLanguagesList = CoreLangLanguage::getActiveLanguages();
			foreach ($activeLanguagesList as $key => $lang) {
				if(!isset($result['translation'][$key]))
					$result['translation'][$key] = '';
			}

			return $result;
		} else
			throw new CHttpException(402, "No such branch found");
	}

	/**
	 * @summary Retrieves information about a node using its code.
	 *
	 * @parameter code [string, required] Alphanumeric code of the node to retrieve
	 *
	 * @response id_org [integer, required] Internal ID of the found node (null if not found)
	 * @response translation [object, required] Array of translations in all system languages (not empty only if node was found)
	 * 			@item english [string, optional] Translation in English
	 * 			@item italian [string, optional] Translation in Italian and so on...
	 * 			@end
	 *
	 * @status 401 Missing or invalid required param "code"
	 */
	public function findNodeByCode() {
		$code = $this->readParameter('code', null);
		if (is_null($code))
			throw new CHttpException(401, 'Missing required param "code"');

		$orgChartTree = CoreOrgChartTree::model()->findByAttributes(array('code' => $code)); /* @var $orgChartTree CoreOrgChartTree */
		if ($orgChartTree) {
			$result = array('id_org' => $orgChartTree->idOrg, 'translation' => array());

			$dbTranslations = CoreOrgChart::model()->findAllByAttributes(array('id_dir' => $orgChartTree->idOrg));
			if (!empty($dbTranslations)) {
				foreach ($dbTranslations as $row)
					$result['translation'][$row->lang_code] = $row->translation;
			}

			$activeLanguagesList = CoreLangLanguage::getActiveLanguages();
			foreach ($activeLanguagesList as $key => $lang) {
				if(!isset($result['translation'][$key]))
					$result['translation'][$key] = '';
			}

			return $result;
		} else
			return array('id_org' => null, 'translation' => array());
	}

	/**
	 * @summary Retrieves the set of children nodes under a specified node.
	 *
	 * @parameter id_org [integer, required] Branch node id
	 *
	 * @response children [array, required] array of 0-N children nodes
	 * 		@item code [string, optional] alphanumeric code for the node (optional)
	 * 		@item id_org [integer, required] id of the child node
	 *     @item translation [object, required] Node labels in each available language
	 * 			@item english [string, optional] Translation in English
	 * 			@item italian [string, optional] Translation in Italian and so on...
	 * 			@end
	 * @end
	 *
	 * @status 401 Missing required param "id_org"
	 * @status 402 No such branch found
	 */
	public function getChildren() {
		$idOrg = $this->readParameter('id_org', null);
		if (is_null($idOrg))
			throw new CHttpException(401, 'Missing required param "id_org"');

		$orgChartTree = CoreOrgChartTree::model()->findByPk($idOrg);
		if ($orgChartTree) {
			$children = array();
			$childNodesIds = $orgChartTree->getImmediateChildren();
			$criteria = new CDbCriteria();
			$criteria->addInCondition('idOrg', $childNodesIds);
			$childNodes = CoreOrgChartTree::model()->findAll($criteria);
			if ($childNodes) {
				foreach ($childNodes as $childNode) {
					$data = array(
						'code' => $childNode->code,
						'id_org' => intval($childNode->idOrg),
						'translation' => array()
					);

					$dbTranslations = CoreOrgChart::model()->findAllByAttributes(array('id_dir' => $childNode->idOrg));
					if (!empty($dbTranslations)) {
						foreach ($dbTranslations as $row)
							$data['translation'][$row->lang_code] = $row->translation;
					}

					$children[] = $data;
				}
			}

			return array( 'children' => $children);
		} else
			throw new CHttpException(402, 'No such branch found');
	}

	/**
	 * @summary Returns the parent node of the passed node.
	 *
	 * @parameter id_org [integer, required] Node id
	 *
	 * @response code [string, required] Alphanumeric code for the node
	 * @response id_org [integer, required] id of the parent node
	 * @response translation [object, required] Array of translations in all system languages
	 * 			@item english [string, optional] Translation in English
	 * 			@item italian [string, optional] Translation in Italian and so on...
	 * 			@end
	 *
	 * @status 401 Missing or invalid required param "id_org"
	 * @status 402 No such branch found
	 */
	public function getParentNode() {
		$idOrg = $this->readParameter('id_org', null);
		if (is_null($idOrg))
			throw new CHttpException(401, 'Missing required param "id_org"');

		$orgChartTree = CoreOrgChartTree::model()->findByPk($idOrg);
		if ($orgChartTree) {
			$result = array();
			$parent = $orgChartTree->parent()->find();
			if ($parent) {
				$result['code'] = $parent->code;
				$result['id_org'] = intval($parent->idOrg);
				$result['translation'] = array();

				$dbTranslations = CoreOrgChart::model()->findAllByAttributes(array('id_dir' => $parent->idOrg));
				if (!empty($dbTranslations)) {
					foreach ($dbTranslations as $row)
						$result['translation'][$row->lang_code] = $row->translation;
				}
			}

			return $result;
		} else
			throw new CHttpException(402, 'No such branch found');
	}

	/**
	 * @summary Assigns a collection of user ids to the specified node.
	 *
	 * @parameter id_org [integer, required] node id
	 * @parameter user_ids [string, required] comma separated list of user ids
	 *
	 * @response assigned_users [string, required] comma separated list of users ids successfully assigned
	 *
	 * @status 401 Missing or invalid required param "id_org"
	 * @status 402 Missing or invalid required param "user_ids"
	 */
	public function assignUsersToNode() {
		$idOrg = $this->readParameter('id_org');
		$userIds = explode(',', $this->readParameter('user_ids'));

		$orgChartTree = CoreOrgChartTree::model()->findByPk($idOrg);
		if (!$orgChartTree)
			throw new CHttpException(401, 'Missing or invalid required param "id_org"');

		// Make sure idst array coming from user input
		// contains only valid user IDs from the platform
		$userIds = Yii::app()->getDb()->createCommand()
			->select('idst')
			->from(CoreUser::model()->tableName())
			->where(array('IN', 'idst', $userIds))
			->queryColumn();
		if (empty($userIds))
			throw new CHttpException(402, 'Missing or invalid required param "user_ids"');

		$assigned_users = array();
		foreach ($userIds as $idUser) {
			//check OC
			$exists = CoreGroupMembers::model()->find(array(
				'condition' => 'idst = :idst_oc AND idstMember = :userId',
				'params' => array(':idst_oc' => $orgChartTree->idst_oc, ':userId' => $idUser)
			));
			if (!$exists) {
				$groupMember = new CoreGroupMembers();
				$groupMember->idst = $orgChartTree->idst_oc;
				$groupMember->idstMember = $idUser;
				$groupMember->save(false);
			}

			//check OCD
			$exists = CoreGroupMembers::model()->find(array(
				'condition' => 'idst = :idst_ocd AND idstMember = :userId',
				'params' => array(':idst_ocd' => $orgChartTree->idst_ocd, ':userId' => $idUser)
			));
			if (!$exists) {
				$groupMember = new CoreGroupMembers();
				$groupMember->idst = $orgChartTree->idst_ocd;
				$groupMember->idstMember = $idUser;
				$groupMember->save(false);
			}

			$assigned_users[] = $idUser;
		}

		//update power users assignments, if needed
		if (PluginManager::isPluginActive('PowerUserApp')) {
			if (!empty($assigned_users)) {
				PowerUserAppModule::setUpdateJob($assigned_users);
			}
		}

		return array('assigned_users' => implode(',', $assigned_users));
	}

	/**
	 * @summary Returns the number of branches under a certain "parent" node (default = root).
	 *
	 * @parameter id_org [integer, optional] node id
	 * @parameter code [string, optional] the branch alphanumeric code
	 *
	 * @response count [integer, required] the number of orgchart nodes under the specified node
	 *
	 * @status 701 Invalid "id_org" param provided
	 * @status 702 Invalid "code" param provided
	 */
	public function count() {
		$idOrg = $this->readParameter('id_org');
		$code = $this->readParameter('code');

		// Check if the OrgChart ID AND OrgChart code has passed
		if($idOrg){
			// Well it seems that the API Caller has passed id_org so let's check if it's valid id_org
			if(!is_numeric($idOrg)  || !CoreOrgChartTree::model()->exists(array('condition' => 'idOrg = :id', 'params' => array(':id' => $idOrg))))
				throw new CHttpException(701, 'Invalid "id_org" param provided');

		}
		if(!$idOrg && $code){
			// Ok we got the node's code let's check if it's legit
			if( ! CoreOrgChartTree::model()->exists(array('condition' => 'code = :code', 'params' => array(':code' => $code))))
				throw new CHttpException(702, 'Invalid "code" param provided');
		}

		$criteria = new CDbCriteria();
		// If both params are passed the idOrg has priority
		if($idOrg){
			$criteria->addCondition("idOrg = :id");
			$criteria->params[':id'] = $idOrg;
		}elseif($code){
			$criteria->addCondition("code = :code");
			$criteria->params[':code'] = $code;
		}

		// Let's get he org_chart
		// note: the criteria will determinate which parameter will be used to get the org_chart
		// and we can be sure that both parameter should be correct because otherwise the previous code will throw exception
		$node = CoreOrgChartTree::model()->find($criteria);
		if($node === null)
			$node = CoreOrgChartTree::getOrgRootNode();
		$count = Yii::app()->db->createCommand()
				->select('COUNT("idOrg")')
				->from(CoreOrgChartTree::model()->tableName())
				->where('iLeft>:myLeft AND iRight<:myRight', array(
					':myLeft'=>$node->iLeft,
					':myRight'=>$node->iRight,
				))->queryScalar();

		return array('count' => (int)$count);
	}

	/**
	 * @summary Returns an array of branches under a certain node (default = root) with some aggregated statistics.
	 *
	 * @parameter id_org [integer, optional] node id
	 * @parameter code [string, optional] the branch alphanumeric code
	 * @parameter include_descendants [boolean, optional] (default=false) If true, the returned statistics will include all users from the selected branch and its descendants.
	 * @parameter from [integer, required] the offset to use for the pagination (starts from 0).
	 * @parameter count [integer, required] the number of records to return per each call.
	 *
	 * @response branches [array, required] Array of one (or more) items, each one related to one specific branch
	 * 		@item id_org [integer, required] The unique ID of the branch
	 * 		@item code [string, required] The branch alphanumeric code
	 *      @item translation [object, required] Node labels in each available language
	 *          @item english [string, optional] Translation in English
	 *          @item italian [string, optional] Translation in Italian and so on...
	 *      @end
	 *      @item stats [object, required] Object containing a set of stats for the selected branch
	 * 			@item total_users [integer, required] Number of users in the selected branch (optionally, including descendants)
	 * 			@item course_enrollments [integer, required] Number of course enrollments for users in the selected branch (optionally, including descendants)
	 * 			@item course_enrollments_not_started [integer, required] Number of course enrollments in "Not yet started" status for users in the selected branch (optionally, including descendants)
	 * 			@item course_enrollments_in_progress [integer, required] Number of course enrollments in "In progress" status for users in the selected branch (optionally, including descendants)
	 * 			@item course_enrollments_expired [integer, required] Number of course enrollments with expired deadline for users in the selected branch (optionally, including descendants)
	 * 			@item course_enrollments_waitlist [integer, required] Number of course enrollments in "In waitinglist" status for users in the selected branch (optionally, including descendants)
	 * 			@item course_enrollments_overbooking [integer, required] Number of course enrollments in "Overbooked" status for users in the selected branch (optionally, including descendants)
	 * 			@item course_enrollments_suspended [integer, required] Number of course enrollments in "suspended" status for users in the selected branch (optionally, including descendants)
	 * 			@item course_enrollments_to_be_confirmed [integer, required] Number of course enrollments in "To be confirmed" status for users in the selected branch (optionally, including descendants)
	 * 		@end
	 *
	 * @status 701 Invalid "id_org" param provided
	 * @status 702 Invalid "code" param provided
	 * @status 703 Invalid "from" param provided
	 * @status 704 Invalid "count" param provided
	 * @status 705 "count" param exceeds the maximum page size (=N)
	 */
	public function stats() {
		$idOrg = $this->readParameter('id_org');
		$code = $this->readParameter('code');
		$include_descendants = $this->readParameter('include_descendants');
		if ($include_descendants == 'true' || $include_descendants === true) {
			$include_descendants = true;
		} else {
			$include_descendants = false;
		}
		$from = $this->readParameter('from');
		$count = $this->readParameter('count');

		// Check if the OrgChart ID AND OrgChart code has passed
		if($idOrg){
			// Well it seems that the API Caller has passed id_org so let's check if it's valid id_org
			if( ! CoreOrgChartTree::model()->exists(array('condition' => 'idOrg = :id', 'params' => array(':id' => $idOrg))))
				throw new CHttpException(701, 'Invalid "id_org" param provided');

		}
		if($code){
			// Ok we got the node's code let's check if it's legit
			if( ! CoreOrgChartTree::model()->exists(array('condition' => 'code = :code', 'params' => array(':code' => $code))))
				throw new CHttpException(702, 'Invalid "code" param provided');
		}

		if ($from === null || !is_numeric($from) || $from != intval($from) || $from < 0)
			throw new CHttpException(703, 'Invalid "from" param provided');

		if ($count === null || !is_numeric($count) || $count != intval($count) || $count <= 0)
			throw new CHttpException(704, 'Invalid "count" param provided');

		$limit = self::MAX_SIZE;

		// check if the requested page count is bigger then the max size
		if ($count > $limit) {
			throw new CHttpException(705, '"count" param exceeds the maximum page size (' . $limit . ')');
		}

		$nodes = array();
		// If no idOrg or code has passed we assume that we should get the ROOT branch
		if(!$idOrg && !$code){
			$node = CoreOrgChartTree::getOrgRootNode();
			$nodes = Yii::app()->db->createCommand()
					->select('idst_oc')
					->from(CoreOrgChartTree::model()->tableName())
					->where('iLeft>=:myLeft AND iRight<=:myRight', array(
							':myLeft'=>$node->iLeft,
							':myRight'=>$node->iRight,
					))->queryColumn();
		}else{
			// If both params are passed the idOrg has priority
			if($idOrg){
				$iLeftRight = Yii::app()->db->createCommand()
						->select('iLeft, iRight')
						->from(CoreOrgChartTree::model()->tableName())
						->where('idOrg = :id', array(':id'=>$idOrg))->queryRow();
				$nodes = Yii::app()->db->createCommand()
						->select('idst_oc')
						->from(CoreOrgChartTree::model()->tableName())
						->where('iLeft>=:myLeft AND iRight<=:myRight', array(
								':myLeft'=>$iLeftRight['iLeft'],
								':myRight'=>$iLeftRight['iRight'],
						))->queryColumn();
			}elseif($code){
				$iLeftRightArray = Yii::app()->db->createCommand()
						->select('iLeft, iRight')
						->from(CoreOrgChartTree::model()->tableName())
						->where('code=:code', array(':code'=>$code))->queryAll();

				foreach($iLeftRightArray as $iLeftRight){
					$results = Yii::app()->db->createCommand()
							->select('idst_oc')
							->from(CoreOrgChartTree::model()->tableName())
							->where('iLeft>=:myLeft AND iRight<=:myRight', array(
									':myLeft'=>$iLeftRight['iLeft'],
									':myRight' => $iLeftRight['iRight'],
							))->queryColumn();
					$nodes = array_merge($nodes, $results);
				}
			}
		}

		// Let's get the branches we need
		$criteria = new CDbCriteria();
		$criteria->addInCondition('idst_oc', $nodes);

		$criteria->limit = $count;
		$criteria->offset = $from;
		$branches = CoreOrgChartTree::model()->findAll($criteria);
		$stats = array();
		foreach($branches as $children){
			$this->apiInput['id_org'] = $children->idOrg;
			$info = $this->getNodeInfo();

			// get id's of the users in the current branch
			$command = Yii::app()->db->createCommand()
					->select('cgm.idstMember')
					->from(CoreGroupMembers::model()->tableName().' cgm')
					->join(CoreOrgChartTree::model()->tableName().' coc','cgm.idst=coc.idst_oc');
			if($include_descendants === true) {
				$command->where('coc.`iLeft`>=:left AND coc.`iRight`<=:right', array(
						':left' => $children->iLeft,
						':right' => $children->iRight,
				));
			}else{
				$command->where('coc.idst_oc=:id', array(':id'=>$children->idst_oc));
			}
			$totalUsers = $command->queryColumn();
			$statistics = array(
					'total_users' => count($totalUsers),
					'course_enrollments' => (int)$this->courseEnrollments($totalUsers),
					'course_enrollments_not_started' => (int)$this->courseEnrollments($totalUsers, LearningCourseuser::$COURSE_USER_SUBSCRIBED),
					'course_enrollments_in_progress' => (int)$this->courseEnrollments($totalUsers, LearningCourseuser::$COURSE_USER_BEGIN),
					'course_enrollments_completed' => (int)$this->courseEnrollments($totalUsers, LearningCourseuser::$COURSE_USER_END),
					'course_enrollments_expired' => (int)$this->courseEnrollments($totalUsers, 'course_enrollments_expired'),
					'course_enrollments_waitlist' => (int)$this->courseEnrollments($totalUsers, LearningCourseuser::$COURSE_USER_WAITING_LIST),
					'course_enrollments_overbooking' => (int)$this->courseEnrollments($totalUsers, LearningCourseuser::$COURSE_USER_OVERBOOKING),
					'course_enrollments_suspended' => (int)$this->courseEnrollments($totalUsers, LearningCourseuser::$COURSE_USER_SUSPEND),
					'course_enrollments_to_be_confirmed' => (int)$this->courseEnrollments($totalUsers, LearningCourseuser::$COURSE_USER_CONFIRMED),
			);

			$stats[] = array(
				'id_org' => (int)$children->idOrg,
				'code' => $info['code'],
				'translation' => $info['translation'],
				'stats' => $statistics
			);
		}

		return array('branches' => $stats);
	}

	private function courseEnrollments($users, $status = false){
		$participants = array('in', 'idUser', $users);

		$command = Yii::app()->db->createCommand()
			->select(array('count(1)'))
			->from(LearningCourseuser::model()->tableName());

		if($status !== false) {
			if ($status === 'course_enrollments_expired') {
				$command->where(
					array('and',
						$participants,
						array('and', 'date_expire_validity < CURDATE()', 'date_expire_validity > "0000-00-00 00:00:00"', 'date_expire_validity IS NOT NULL'),
						'waiting=0',
					)
				);
			}else{
				$command->where(array('and', $participants, 'status = :status'), array(':status' => $status));
			}
		}
		else{
			$command->where($participants);
		}

		return $command->queryScalar();
	}

	/**
	 * @summary Unassigns users from an orgchart node
	 *
	 * @parameter id_org [integer, required] node id
	 * @parameter user_ids [string, required] comma separated list of user ids to unassign
	 *
	 * @response unassigned_users [string, required] comma separated list of users ids successfully unassigned
	 *
	 * @status 401 Missing or invalid required param "id_org"
	 * @status 402 Missing or invalid required param "user_ids"
	 * @status 403 Invalid node
	 */
	public function unassignUsersFromNode() {
		$idOrg = $this->readParameter('id_org');
		$userIds = $this->readParameter('user_ids');

		if (!$idOrg)
			throw new CHttpException(401, 'Missing or invalid required param "id_org"');

		if (!$userIds || $userIds == '')
			throw new CHttpException(402, 'Missing or invalid required param "user_ids"');


		$coreOrgCartTree = CoreOrgChartTree::model()->findByPk($idOrg);

		if (!$coreOrgCartTree)
			throw new CHttpException(403, 'Invalid node');

		$userIdsArr = explode(',',$userIds);
		$selectedGroups = array($coreOrgCartTree->idst_oc, $coreOrgCartTree->idst_ocd);
		$groups = implode(',',$selectedGroups);

		$deletedNodes = array();
		foreach($selectedGroups as $selectdeGroup){
			$deletedNodes[] = CoreOrgChartTree::getIdOrgByOcOrOcd($selectdeGroup);
		}
		$deletedNodes = array_unique($deletedNodes);
		if (($key = array_search(CoreOrgChartTree::getOrgRootNode()->idOrg, $deletedNodes)) !== false) {
			unset($deletedNodes[$key]);
		}

		$unassignedUserIds = array();
		foreach($userIdsArr as $index=>$userId){
			$numSucceded = CoreGroupMembers::model()->deleteAllByAttributes(array()," idstMember = ".$userId." AND idst IN (".$groups.") ");
			if($numSucceded>0) {
				$unassignedUserIds[] = $userId;
				foreach ($deletedNodes as $deletedNode) {
					Yii::app()->event->raise(CoreNotification::NTYPE_USER_REMOVED_FROM_BRANCH, new DEvent($this, array(
							'targetUser' => $userId,
							'BranchIdOrg' => $deletedNode
					)));
				}
			}
		}

		return array('unassigned_users' => implode(',',$unassignedUserIds));
	}

}