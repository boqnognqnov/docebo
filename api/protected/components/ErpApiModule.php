<?php

/**
 * Erp API Component
 * Set of APIs to manage ERP
 */
class ErpApiModule extends ApiModule {

	/**
	 * Set inside this installation the params related to the limits setted inside the erp
	 * @param array $params
	 * @return array with two key, success : true or false, message: will contain the error message if any
	 */
	public function setInstallationParams() {

		//read input parameters
		$params = array(
			'max_users' => $this->readParameter('max_users'),
			'expiry' => $this->readParameter('expiry'),
			'downgrade_check' => $this->readParameter('downgrade_check'),
			'grace_period' => $this->readParameter('grace_period'),
			'trial_expiration_date' => $this->readParameter('trial_expiration_date'),
			'subscription_begin_date' => $this->readParameter('subscription_begin_date'),
			'expired_period' => $this->readParameter('expired_period'),
			'enable_user_billing' => $this->readParameter('enable_user_billing'),
            'payment_method' => $this->readParameter('payment_method'),
		);


		if ($params['max_users'] !== null) {

			$setting = CoreSetting::model()->findByPk('max_users');
			if (!$setting) {
				$setting = new CoreSetting();
				$setting->param_name = 'max_users';
				$setting->param_value = (int)$params['max_users'];
				$setting->value_type = 'string';
				$setting->max_size = 255;
				$setting->pack = '0';
				$setting->regroup = 0;
				$setting->sequence = 0;
				$setting->param_load = 1;
				$setting->hide_in_modify = 1;
			} else {
				$setting->param_value = (int)$params['max_users'];
			}
			if (!$setting->save()) {
				throw new CHttpException(500, 'Error while creating/updating "max_users" setting');
			}

			// if it's a first install update the subscription begin_date
			$subscriptionBeginDate = Settings::get('subscription_begin_date');
			if (!$subscriptionBeginDate) {
				$setting = CoreSetting::model()->findByPk('subscription_begin_date');
				if (!$setting) {
					$setting = new CoreSetting();
					$setting->param_name = 'subscription_begin_date';
				}
				$setting->param_value = date("Y-m-d H:i:s");
				$setting->value_type = 'string';
				$setting->max_size = 255;
				$setting->pack = 'main';
				$setting->regroup = 0;
				$setting->sequence = 0;
				$setting->param_load = 1;
				$setting->hide_in_modify = 0;

				if (!$setting->save()) {
					throw new CHttpException(500, 'Error while updating "subscription_begin_date" setting');
				}
			}
		}

		//update expiry date, if set
		if ($params['expiry'] !== null) {
			$setting = CoreSetting::model()->findByPk('expiration_date');
			if ($setting) {
				$setting->param_value = $params['expiry'];
				if (!$setting->save()) {
					throw new CHttpException(500, 'Error while updating "expiration_date" setting');
				}
			}
		}

		//checking downgrade option
		if ($params['downgrade_check'] !== null) {

			$freeUsers = Settings::get('free_users', 5); //Get::sett('free_users', Get::pcfg('userlimit', 'free_users', 5)); ... ?!?
			$maxUsers = Settings::get('max_users') + $freeUsers;
			$activeUsers = Yii::app()->db->createCommand("SELECT COUNT(*) FROM ".CoreUser::model()->tableName()." WHERE userid <> '/Anonymous'")->queryScalar();

			if ($activeUsers > $maxUsers) { // force grace period mode:
				$params['grace_period'] = 1;
			}
		}


		if ($params['grace_period'] !== null) {
			$setting = CoreSetting::model()->findByPk('grace_period');
			if (!$setting) {
				$setting = new CoreSetting();
				$setting->param_name = 'grace_period';
				$setting->value_type = 'int';
				$setting->max_size = 2;
				$setting->pack = '0';
				$setting->regroup = 0;
				$setting->sequence = 0;
				$setting->param_load = 1;
				$setting->hide_in_modify = 1;
			}
			$setting->param_value = (int)$params['grace_period'];
			if (!$setting->save()) {
				throw new CHttpException(500, 'Error while creating/updating "grace_period" setting');
			}
		}


		if ($params['trial_expiration_date'] !== null) {
			$setting = CoreSetting::model()->findByPk('trial_expiration_date');
			if ($setting) {
				$setting->param_value = date('Y-m-d', strtotime($params['trial_expiration_date']));
				if (!$setting->save()) {
					throw new CHttpException(500, 'Error while updating "trial_expiration_date" setting');
				}
			} else {
				//TODO: throw an exception here ?!? ...
			}
		}


		if ($params['subscription_begin_date'] !== null) {
			$setting = CoreSetting::model()->findByPk('subscription_begin_date');
			if ($setting) {
				$setting->param_value = $params['subscription_begin_date'];
				if (!$setting->save()) {
					throw new CHttpException(500, 'Error while updating "trial_expiration_date" setting');
				}
			} else {
				//TODO: throw an exception here ?!? ...
			}
		}


		if ($params['expired_period'] !== null) {
			$setting = CoreSetting::model()->findByPk('expired_period');
			if (!$setting) {
				$setting = new CoreSetting();
				$setting->param_name = 'expired_period';
				$setting->param_value = (int)$params['expired_period'];
				$setting->value_type = 'int';
				$setting->max_size = 2;
				$setting->pack = '0';
				$setting->regroup = 0;
				$setting->sequence = 0;
				$setting->param_load = 1;
				$setting->hide_in_modify = 1;
			} else {
				$setting->param_value = (int)$params['expired_period'];
			}
			if (!$setting->save()) {
				throw new CHttpException(500, 'Error while creating/updating "expired_period" setting');
			}
		}


		if ($params['enable_user_billing'] !== null) {
			$value = (trim(strtolower($params['enable_user_billing'])) == 'on' ? 'on' : 'off');
			$setting = CoreSetting::model()->findByPk('enable_user_billing');
			if (!$setting) {
				$setting = new CoreSetting();
				$setting->param_name = 'enable_user_billing';
				$setting->param_value = $value;
				$setting->value_type = 'enum';
				$setting->max_size = 3;
				$setting->pack = 'main';
				$setting->regroup = 1;
				$setting->sequence = 0;
				$setting->param_load = 1;
				$setting->hide_in_modify = 1;
			} else {
				$setting->param_value = $value;
			}
			if (!$setting->save()) {
				throw new CHttpException(500, 'Error while creating/updating "enable_user_billing" setting');
			}
		}

		if ($params['core_version'] !== null) {
			$value = trim($params['enable_user_billing']);
			$setting = CoreSetting::model()->findByPk('core_version');
			if (!$setting) {
				$setting = new CoreSetting();
				$setting->param_name = 'core_version';
				$setting->param_value = $value;
				$setting->value_type = 'string';
				$setting->max_size = 255;
				$setting->pack = 0;
				$setting->regroup = 1;
				$setting->sequence = 0;
				$setting->param_load = 1;
				$setting->hide_in_modify = 1;
			} else {
				$setting->param_value = $value;
			}
			if (!$setting->save()) {
				throw new CHttpException(500, 'Error while creating/updating "core_version" setting');
			}
		}

        // A parameter for payment_method core setting
        /**
         * @todo Set an array of allowed values, if any other passed - ignore
         */
        if ($params['payment_method'] !== null) {
            $setting = CoreSetting::model()->findByPk('payment_method');
            if (empty($setting) === true) {
                $setting = new CoreSetting;
                $setting->param_name = 'payment_method';
            }

            $setting->param_value = $params['payment_method'];

            if (empty($setting->save()) === true) {
                throw new CHttpException(500, 'Error while updating "payment_method" setting');
            }
        }

		// reset cached settings
		//Docebo::cache()->delete('setting'); //TODO: how to do it in Yii, if still needed ?!?

		return array(); //no specifica data to return
	}



	public function createCourse() {

		//instantiate some utilities classes
		$root =LearningCourseCategoryTree::model()->roots()->find();
		$purifier = new DoceboCHtmlPurifier();

		//read and format input values
		$input = new stdClass();
		$input->idCategory = $this->readParameter('idCategory', $root->getPrimaryKey());
		$input->course_code = $this->readParameter('course_code', '');
		$input->mpidCourse = $this->readParameter('mpidCourse', 0);
		$input->abs_max_subscriptions = $this->readParameter('abs_max_subscriptions', 0);
		$input->prerequisites_policy = $this->readParameter('prerequisites_policy', 1);
		$input->course_name = $this->readParameter('course_name', '');
		$input->course_descr = $purifier->purify($this->readParameter('course_descr', ''));
		$input->course_lang = Lang::getCodeByBrowserCode($this->readParameter('course_lang', 'en'));
		$input->course_type = $this->readParameter('course_type', LearningCourse::TYPE_ELEARNING);
		$input->course_status = $this->readParameter('course_status', LearningCourse::$COURSE_STATUS_PREPARATION);
		$input->userStatusOp = $this->readParameter('userStatusOp', 0);
		$input->cours_progress = ((bool)$this->readParameter('course_progress', 0) ? 1 : 0);
		$input->course_time = ((bool)$this->readParameter('course_time', 0) ? 1 : 0);
		$input->show_who_online = ((bool)$this->readParameter('show_who_online', 0) ? 1 : 0);
		$input->course_advanced = ((bool)$this->readParameter('course_advanced', 0) ? 1 : 0);
		$input->course_show_rules = ((bool)$this->readParameter('course_show_rules', 0) ? 1 : 0);
		$input->direct_play = ((bool)$this->readParameter('direct_play', 0) ? 1 : 0);
		$input->selling = ((bool)$this->readParameter('selling', 0) ? 1 : 0);
		$input->course_price = $this->readParameter('course_price', 0);
		$input->allow_overbooking = ((bool)$this->readParameter('allow_overbooking', 0) ? 1 : 0);
		$input->can_subscribe = (int)$this->readParameter('can_subscribe', 1);
		$input->show_result = ((bool)$this->readParameter('show_result', 0) ? 1 : 0);
		$input->use_logo_in_courselist = ((bool)$this->readParameter('use_logo_in_courselist', 0) ? 1 : 0);
		$input->img_course = $this->readParameter('img_course', '');
		$input->auto_unsubscribe = ((bool)$this->readParameter('auto_unsubscribe', 0) ? 1 : 0);
		$input->course_medium_time = (int)$this->readParameter('course_medium_time', 0);
		$use_unsubscribe_date_limit = (bool)$this->readParameter('use_unsubscribe_date_limit', false);
		$input->unsubscribe_date_limit = ($use_unsubscribe_date_limit ? $this->readParameter('unsubscribe_date_limit') : null);

		// "'".Format::dateDb($params['unsubscribe_date_limit'], 'date')."'" : 'NULL')."";


		//prepare new course AR and set attributes
		$course = new LearningCourse();
		$course->idCategory = $input->idCategory;
		$course->mpidCourse = $input->mpidCourse;
		$course->code = $input->course_code;
		$course->name = $input->course_name;
		$course->description = $input->course_descr;
		$course->lang_code = $input->course_lang;
		$course->course_type = $input->course_type;
		$course->status = $input->course_status;
		$course->create_date = Yii::app()->localtime->toLocalDateTime();
		$course->userStatusOp = $input->userStatusOp;
		$course->show_progress = $input->course_progress;
		$course->show_time = $input->course_time;
		$course->show_who_online = $input->show_who_online;
		$course->show_extra_info = $input->course_advanced;
		$course->show_rules = $input->course_show_rules;
		$course->direct_play = $input->direct_play;
		$course->selling = $input->selling;
		$course->prize = $input->course_price;
		//$course->course_edition = ... //no more used
		$course->allow_overbooking 	= $input->allow_overbooking;
		$course->can_subscribe = $input->can_subscribe;
		$course->show_result = $input->show_result;
		$course->use_logo_in_courselist = $input->use_logo_in_courselist;
		$course->img_course = $input->img_course;
		$course->auto_unsubscribe = $input->auto_unsubscribe;
		$course->mediumTime = $input->course_medium_time;
		$course->unsubscribe_date_limit = $input->unsubscribe_date_limit;

		//physically create the course
		if (!$course->save()) {
			throw new CHttpException(500, 'Error while creating course');
		}

		//set output info
		$output = array();
		$output['id_course'] = $course->getPrimaryKey();

		return $output;
	}



	public function loadScorm() {

		//read input values
		$input = new stdClass();
		$input->id_course = $this->readParameter('id_course');
		$input->course_code = $this->readParameter('course_code');
		$input->location = $this->readParameter('location');	// http://qwedrdfdf.cloudfront.net
		$input->path = $this->readParameter('path');					// tincan/partners/34/courses/456/765/
		$input->title = $this->readParameter('title');
		$input->score = $this->readParameter('score');

		//validate input values
		if ($input->id_course) {
			$course = LearningCourse::model()->findByPk($input->id_course);
		} elseif ($input->course_code) {
			$course = LearningCourse::model()->findByAttributes(array('code' => $input->course_code));
		}
		if (!$course) {
			throw new CHttpException(201, 'Invalid specified course');
		}

		$objectName = $this->readParameter('obj_name', '');
		$objectAddress = $this->readParameter('obj_address', '');

		$transaction = Yii::app()->db->beginTransaction();

		//open package file and elaborate content
		$package = new LearningScormPackage();

		$o = explode('/scorm/', $objectAddress);
		$package->setIsRemote(true);
		$package->isRemote  = true;
		$package->path      = $o[1];
		$package->location  = str_replace('http://', '', $o[0]);;

		$package->idUser = Yii::app()->user->id;
		$package->setFile($objectAddress . '/imsmanifest.xml'); //TODO: is this right?
		$package->setIdCourse($course->getPrimaryKey());

		if (!$package->save()) {
			$transaction->rollback();
			//TO DO: revert file storing
			throw new CHttpException(500, 'Error while saving DB information');
		}

		if ($objectName) {
			//retrieve object from learning_organization and change object name
			//TODO: ...
			/*
			$object = LearningScormOrganizations::model()->findByAttributes(array(
				'idscorm_package' => $package->getPrimaryKey();
			));
			*/
		}

		//all operations were ok, commit changes to DB
		$transaction->commit();

		//set output info
		$output = array();
		$output['message'] = 'SCORM Object installed.';

		return $output;
	}



	public function loadTinCan() {

		//read input values
		$input = new stdClass();
		$input->id_course = $this->readParameter('id_course');
		$input->course_code = $this->readParameter('course_code');
		$input->location = $this->readParameter('location');	// http://qwedrdfdf.cloudfront.net
		$input->path = $this->readParameter('path');					// tincan/partners/34/courses/456/765/

		//validate input values
		if ($input->id_course) {
			$course = LearningCourse::model()->findByPk($input->id_course);
		} elseif ($input->course_code) {
			$course = LearningCourse::model()->findByAttributes(array('code' => $input->course_code));
		}
		if (!$course) {
			throw new CHttpException(201, 'Invalid specified course');
		}

		//do stuff

		$transaction = Yii::app()->db->beginTransaction();

		$object = new LearningTcAp();
		$object->registration = LearningTcAp::generateStatementUUID();
		$object->location = $input->location; //...
		$object->path = $input->path;

		//$object->setTitle($title);
		$object->setIdCourse($course->getPrimaryKey());

		$rs = $object->save();
		if (!$rs) {
			//TO DO: revert file storing
			Yii::log('Error while saving DB information; error(s): '.print_r($object->getErrors(), true), CLogger::LEVEL_ERROR);
			throw new CException('Error while saving DB information; error(s): '.print_r($object->getErrors(), true));
		}


		//set output info
		$output = array();
		$output['message'] = "Activities loaded into table. Count: " . count($activities);

		return $output;
	}



	public function loadTest() {

		//read input values
		$input = new stdClass();
		$input->id_course = $this->readParameter('id_course');
		$input->location = $this->readParameter('location');	// http://qwedrdfdf.cloudfront.net
		$input->path = $this->readParameter('path');					// tincan/partners/34/courses/456/765/
		$input->title = $this->readParameter('title');
		$input->score = $this->readParameter('score');

		//validate input values
		$course = LearningCourse::model()->findByPk($input->id_course);
		if (!$course) {
			throw new CHttpException(201, 'Invalid specified course');
		}

		$transaction = Yii::app()->db->beginTransaction();

		//physically creates the test object (most values are set by default)
		$test = new LearningTest();
		$test->setIdCourse($input->id_course);
		$test->title = $input->title;
		$test->point_required = $input->score;
		$test->description = '';
		$test->point_type = 0;
		$test->display_type = 0;
		$test->order_type = 0;
		$test->shuffle_answer = 0;
		$test->question_random_number = 0;
		$test->save_keep = 0;
		$test->mod_doanswer = 1;
		$test->can_travel = 1;
		$test->show_doanswer = 0;
		$test->show_solution = 0;
		$test->max_attempt = 0;
		$test->hide_info = 0;
		$test->use_suspension = 0;
		$test->suspension_num_attempts = 0;
		$test->suspension_num_hours = 0;
		$test->suspension_prerequisites = 0;
		$test->mandatory_answer = 0;

		if (!$test->save()) {
			$transaction->rollback();
			throw new CHttpException(500, 'Error while creating test');
		}

		//verify inserted data
		$learningObject = LearningOrganization::model()->findByAttributes(array(
			'idResource' => $test->getPrimaryKey(),
			'objectType' => LearningOrganization::OBJECT_TYPE_TEST,
			'idCourse' => $input->id_course
		));
		if (!$learningObject) {
			throw new CHttpException(500, 'Invalid learning object');
		}

		$idTest = $test->getPrimaryKey();
		$output = array();
		$output['scorm_id'] = (int)$idTest;

		//import the gift file
		$giftUrl = $input->location.$input->path;
		$giftText = '';

		if (!empty($giftUrl)) { $giftText = file_get_contents($giftUrl); }
		if (empty($giftText)) { throw new CHttpException(500, 'Invalid gift url / text given.'); }

		//read all gift file lines
		$file = file($giftUrl);

		$gift = new GiftFormatManager();
		$formatted = $gift->readQuestions($file);

		if (!empty($formatted)) {

			try {

				$sequence = $test->getMaxSequence() + 1;
				$page = $test->getMaxPage();
				if ($page <= 0) { $page = 1; }

				foreach ($formatted as $question) {

					/*if ((int)$idCategory > 0 && is_object($question)) {
						$question->id_category = (int)$idCategory;
					}*/

					$questionManager = CQuestionComponent::getQuestionManager($question->qtype);

					if ($questionManager) {
						$rs = $questionManager->importFromRaw($question, $idTest, $sequence, $page);
						if (!$rs) { throw new CException('Error while importing questions'); }
					}

					$sequence++; //increment question sequence at every imported row
				}

				//make sure that values for questions sequence and page are correct
				LearningTestquest::fixTestSequence($idTest);
				LearningTestquest::fixTestPages($idTest);

			} catch (CException $e) {

				$transaction->rollback();
				throw new CHttpException(500, 'Error while populating test questions');
			}

		}

		//all operations has been successful: commit DB changes
		$transaction->commit();

		return $output;
	}


	/**
	 * Return information about very specific bug we had: when password was changed, Branch were deassociated.
	 * This is a temporary method, should be removed later.
	 *
	 * @return array
	 */
	public function getLmsPassBranchMessInfo() {
		$output = array();
		$output['data'] = null;


		$oData = array();

		// Number of branches
		$command = Yii::app ()->db->createCommand ();
		$command->select('count(*)')->from('core_org_chart_tree');
		$count = $command->queryScalar();
		$oData['number_branches'] = (int) $count - 1;   // do not count the ROOT

		// Total number of users
		$command = Yii::app ()->db->createCommand ();
		$command->select('count(*)')->from('core_user');
		$count = $command->queryScalar();
		$oData['number_users'] = (int) $count;

		// First iteration: get list of users: created before 15 Sep and logged in after 15 Sep
		$command = Yii::app ()->db->createCommand ();
		$command->select('u.idst idst')->from('core_user u');
		$command->where("u.register_date <= '2014-09-15'");
		$command->andWhere("u.lastenter >= '2014-09-15'");
		$command->andWhere("u.pass LIKE '$2a%'");
		$rows = $command->queryAll(true);
		// So, list of users created... logged..
		$list1 = array();
		foreach ($rows as $row) {
			$list1[] = $row['idst'];
		}
		$oData['number_of_created_logged'] = count($list1);

		// Second: intersect the previos list with list of users NOT assigned to any branch
		$sql = "
				select t.idst
				from core_user t
				where
					t.idst NOT IN
					(
						SELECT distinct(u.idst)
						FROM core_user u
						JOIN core_group_members cgm ON u.idst=cgm.idstMember
						JOIN core_group cg ON (cgm.idst=cg.idst)
						WHERE
							groupid LIKE '/oc%' AND
							groupid <> '/oc_0' AND
							groupid <> '/ocd_0'
					)

					AND
					t.idst IN (" . implode(",", $list1) . ")

		";
		$command = Yii::app ()->db->createCommand ();
		$command->setText($sql);
		$rows = $command->queryAll(true);

		// So, list of users... created.. logged.. and NOT in a branch..
		$list2 = array();
		foreach ($rows as $row) {
			$list2[] = $row['idst'];
		}

		$oData['number_of_created_logged_notbranch'] = count($list2);
		$oData['users'] = $list2;


		$output['data'] = $oData;
		return $output;
	}



	public function getLmsLanguages(){
		$result = array();
		$success = false;
		$models = CoreLangLanguage::model()->findAll();
		$result['list'] = array();
		if($models){
			$success = true ;
			$list = CHtml::listData($models, 'lang_code', 'lang_description' );
			$result['list'] = $list;
		}


		$result['success'] = $success ;
		echo json_encode($result);
		die();
	}

	public function getLmsTranslations(){

		$result = array();
		$success = true;
		$languages = $this->readParameter('languages');
		$coreLangTextKeys = array();
		$resultArray = array();
		$tmpPath = Docebo::getUploadTmpPath();
		$result['filePath'] = null;
		$csvCoreLangLanguage = '"lang_code","lang_description","lang_browsercode","lang_direction","lang_active"';
		$csvCoreLangTranslation = '"id_text","lang_code","translation_text","translation_status","save_date","pid"';
		$csvCoreLangText = '"id_text","text_key","text_module","text_attributes","context"';

		if($languages){
			if($languages == 'all'){
				$coreLangLanguageModels = CoreLangLanguage::model()->findAll();

			}else{
				$languagesArray = explode(',' , $languages);
				$coreLangLanguageCriteria = new CDbCriteria();
				$coreLangLanguageCriteria->addInCondition('lang_code', $languagesArray);
				$coreLangLanguageModels =  CoreLangLanguage::model()->findAll($coreLangLanguageCriteria);
			}


			foreach($coreLangLanguageModels as $key=>$value){

				$csvCoreLangLanguage .= PHP_EOL."\"{$value->lang_code}\",\"{$value->lang_description}\",\"{$value->lang_browsercode}\",\"{$value->lang_direction}\",\"{$value->lang_active}\"";

				$coreLangTranslationModels = CoreLangTranslation::model()->findAllByAttributes(array('lang_code'=>$value->lang_code));
				foreach($coreLangTranslationModels as $keyTranslation=>$valueTranslation){
					if(!in_array($valueTranslation->id_text, $coreLangTextKeys)){
						$coreLangTextKeys[] = $valueTranslation->id_text;
					}

					$csvCoreLangTranslation .= PHP_EOL."\"{$valueTranslation->id_text}\",\"{$valueTranslation->lang_code}\",\"{$valueTranslation->translation_text}\",\"{$valueTranslation->translation_status}\",\"{$valueTranslation->save_date}\",\"{$valueTranslation->pid}\"";
				}
			}

			$coreLanguageTextCriteria = new CDbCriteria();
			$coreLanguageTextCriteria->addInCondition('id_text', $coreLangTextKeys);
			$coreLanguageTextModel = CoreLangText::model()->findAll($coreLanguageTextCriteria);


			foreach($coreLanguageTextModel as $keyText=>$valueText){

				$csvCoreLangText .= PHP_EOL."\"{$valueText->id_text}\",\"{$valueText->text_key}\",\"{$valueText->text_module}\",\"{$valueText->text_attributes}\",\"{$valueText->context}\"";
			}

			$resultArray['core_lang_language'] = $csvCoreLangLanguage;
			$resultArray['core_lang_text'] = $csvCoreLangText;
			$resultArray['core_lang_translation'] = $csvCoreLangTranslation;

			$zip = new ZipArchive();
			$zipName = $zipName = strtolower($_SERVER['HTTP_HOST']).'_'.'translation_'.'zipfile.zip';
			$zip->open($tmpPath . DIRECTORY_SEPARATOR .$zipName, ZipArchive::CREATE);

			foreach($resultArray as $key=>$val){
				$name = $tmpPath . DIRECTORY_SEPARATOR .$key.".csv";
				$fileHandle = fopen($name, 'w');
				fwrite($fileHandle, $val);
				fclose($fileHandle);

				if(!$zip->addFile($name, $key.'.csv')){
					$success = false;
					continue;
				}
			}

			$zip->close();
		}

		if($tmpPath . DIRECTORY_SEPARATOR .$zipName){
			$result['filePath'] = Docebo::getUploadTmpUrl().'/'.$zipName ;
			foreach($resultArray as $key=>$val){
				$name = $tmpPath . DIRECTORY_SEPARATOR .$key.".csv";
				if(file_exists($name)){
					unlink($name);
				}
			}
		}


		$result['success'] = $success ;
		echo json_encode($result);
		die();
	}

	/**
	 * API endpoint to update the info in the old LMS (non-sandbox)
	 * with details on where the sandbox was created (URL), its expiration date, etc
	 */
	public function updateSandboxInfo(){

		Yii::log('Sandbox created callback called. See passed parameters in the next log message', CLogger::LEVEL_INFO);

		$sandboxCreationStatus = $this->readParameter('status');
		$sandboxUrl = $this->readParameter('sandbox_url');
		$sandboxExpiration = $this->readParameter('sandbox_expiration');

		Yii::log("Updating local sandbox details to: url={$sandboxUrl}, expiration={$sandboxExpiration}, status={$sandboxCreationStatus}", CLogger::LEVEL_INFO);

		if($sandboxCreationStatus=='activated'){
			$raw = Settings::get('sandbox_details');
			if (!empty($raw)) {
				$json = CJSON::decode($raw);

				$idUserInitiator = isset($json['id_user']) ? (int)$json['id_user'] : false;

				$sandboxDetails = $json['sandbox_details'];

				// UTC timestamp (integer) when the sandbox was fully created and configured
				$sandboxDetails['date_finished'] = time();

				// UTC timestamp (integer) when the sandbox expires
				$sandboxDetails['date_expire'] = $sandboxExpiration;

				$sandboxDetails['sandbox_url'] = $sandboxUrl;

				$sandboxDetails['status'] = $sandboxCreationStatus;

				$json['sandbox_details'] = $sandboxDetails;

				Settings::save('sandbox_details', CJSON::encode($json));

				$userInitiator = CoreUser::model()->findByPk($idUserInitiator);
				if($userInitiator && $userInitiator->email) {
					// Notify the super admin that initiated the request
					// that his sandbox is ready
					$mm = new MailManager();
					$mm->mail(
						Docebo::getDefaultSenderEmail(), // From:
						$userInitiator->email, // To:

						// Subject:
						Yii::t('sandbox', 'Your sandbox is ready'),

						// Body:
						Yii::t('sandbox', 'Your sandbox was created and is available at {url}. It is available until {expiration}.', array(
								'{url}' => $sandboxUrl,
								'{expiration}' => date('Y-m-d', $sandboxExpiration),
							))
					);
				}
			}
		}else{
			// No other scenarios are currently possible
			// (E.g. deleting the sandbox creation status from the JSON details)
		}

	}


}
