<?php

/**
 * Stat API Component
 * @description Set of APIs to manage platform stats
 */
class StatApiModule extends ApiModule {

    /**
     * @summary Returns generic user stats
     * @response user_count [integer, required] Number of users
     * @response week_session_count [integer, required] Number of session in the last 7 days
     * @response user_session_count [integer, required] Number of user sessions
     * @response week_login_count [integer, required] Number of logins in the last 7 days
     */
	public function getGenericUserStat() {
		$params = array('user_stat_only' => true);
		return $this->getGenericPlatformStat($params);
	}


    /**
     * @summary Returns generic platform stats
     *
     * @parameter user_stat_only [boolean, optional] Whether to show only statistics related to users
     *
     * @response user_count [integer, required] Number of users
     * @response week_session_count [integer, required] Number of session in the last 7 days
     * @response user_session_count [integer, required] Number of user sessions
     * @response week_login_count [integer, required] Number of logins in the last 7 days
     * @response has_custom_logo [boolean, optional] Whether platform has custom logo
     * @response course_tot_count [integer, optional] Total number of courses
     * @response preinstalled_tot_count [integer, optional] Total Number of (old, before 5.1) pre-installed courses:
     * @response marketplace_course_count [integer, optional] Number of courses installed from marketplace:
     * @response user_course_count [integer, optional]  Number of courses created by the LMS users
     * @response learningobj_count [integer, optional] Number of learning objects
     * @response preinstalled_learningobj_count [integer, optional] Total Number of (old, before 5.1) pre-installed learning objects:
     * @response user_learningobj_count [integer, optional] Number of courses learning objects installed by user (not from marketplace):
     * @response course_subscription_count [integer, optional] Total Number of users subscribed to courses
     * @response last_platform_action [integer, optional] Last action in platform
     * @response training_time_12month [integer, optional] Training time in the last 12 months (in seconds)
     * @response last_platform_login [integer, optional] Last login in platform
     *
     */
    public function getGenericPlatformStat($params = false) {

		//read parameters from request if arguments are not specified
		if (empty($params) || !is_array($params) || (is_array($params) && !isset($params['user_stat_only']))) {
			$params = array(
				'user_stat_only' => (bool)$this->readParameter('user_stat_only')
			);
		}

		$output = array();

		// ------------ User stats:
		$userCount = Yii::app()->db->createCommand()
			->select("COUNT(*) AS tot")
			->from(CoreUser::model()->tableName())
			->where("userid <> :anonymous", array(':anonymous' => '/Anonymous'))
			->queryScalar();
		$output['user_count'] = (int)$userCount;

		// ------------------

		// Track session in the last 7 days:
		$trackCount = Yii::app()->db->createCommand()
			->select("COUNT(*) AS tot")
			->from(LearningTracksession::model()->tableName())
			->where("enterTime >= :enter_time", array(':enter_time' => date('Y-m-d H:i:s', strtotime('-7 days'))))
			->queryScalar();
		$output['week_session_count'] = (int)$trackCount;

		// ------------------

		// Total number of track session entries:
		$trackTotal = Yii::app()->db->createCommand()
			->select("COUNT(*) AS tot")
			->from(LearningTracksession::model()->tableName())
			->queryScalar();
		$output['user_session_count'] = (int)$trackTotal;

		// ------------------

		$loginCount = Yii::app()->db->createCommand()
			->select("COUNT(*) AS tot")
			->from(CoreUser::model()->tableName())
			->where("lastenter >= :lastenter", array(':lastenter' => date('Y-m-d H:i:s', strtotime('-7 days'))))
			->queryScalar();
		$output['week_login_count'] = (int)$loginCount;


		if (!$params['user_stat_only']) {

			// ------------ Platform stats:
			// Has custom logo:
			$setting = CoreSetting::model()->findByPk('company_logo');
			$output['has_custom_logo'] = ($setting && !empty($setting->param_value) ? 1 : 0);
			// ------------------


			// Total Number of courses:
			$courseCount = Yii::app()->db->createCommand()
				->select("COUNT(*) AS tot")
				->from(LearningCourse::model()->tableName())
				->queryScalar();
			$output['course_tot_count'] = (int)$courseCount;


			// Total Number of (old, before 5.1) pre-installed courses:
			$precourseCount = Yii::app()->db->createCommand()
				->select("COUNT(*) AS tot")
				->from(LearningCourse::model()->tableName())
				->where("create_date < :create_date AND idCourse <= :id_course", array(
					':create_date' => '2012-08-04 12:10:00',
					':id_course' => 22
				))
				->queryScalar();
			$output['preinstalled_tot_count'] = (int)$precourseCount;


			// Number of courses installed from marketplace:
			$mpcourseCount = Yii::app()->db->createCommand()
				->select("COUNT(*) AS tot")
				->from(LearningCourse::model()->tableName())
				->where("mpidCourse > 0")
				->queryScalar();
			$output['marketplace_course_count'] = (int)$mpcourseCount;


			// Number of courses created by the user:
			$output['user_course_count'] = $output['course_tot_count'] - $output['marketplace_course_count'] - $output['preinstalled_tot_count'];


			// Number of learning objects:
			$lobjCount = Yii::app()->db->createCommand()
				->select("COUNT(*) AS tot")
				->from(LearningOrganization::model()->tableName())
				->where("objectType <> ''")
				->queryScalar();
			$output['learningobj_count'] = (int)$lobjCount;


			// Total Number of (old, before 5.1) pre-installed learning objects:
			$prelobjCount = Yii::app()->db->createCommand()
				->select("COUNT(*) AS tot")
				->from(LearningOrganization::model()->tableName())
				->where("objectType <> ''")
				->andWhere("dateInsert < :date_insert AND idCourse <= :id_course", array(
					':date_insert' => '2012-08-04 12:10:00',
					':id_course' => 22
				))
				->queryScalar();
			$output['preinstalled_learningobj_count'] = (int)$prelobjCount;


			// Number of courses learning objects installed by user (not from marketplace):
			// (1 marketplace course = only 1 learning object)
			$output['user_learningobj_count'] = $output['learningobj_count'] - $output['marketplace_course_count'] - $output['preinstalled_learningobj_count'];


			// Total Number of users subscribed to courses:
			$enrollCount = Yii::app()->db->createCommand()
				->select("COUNT(*) AS tot")
				->from(LearningCourseuser::model()->tableName())
				->queryScalar();
			$output['course_subscription_count'] = (int)$enrollCount;


			// Last action in platform:
			$lastAction = Yii::app()->db->createCommand()
				->select("MAX(lastTime) AS last_enter")
				->from(LearningTracksession::model()->tableName())
				->queryScalar();
			$output['last_platform_action'] = $lastAction;

			// Training time in the last 12 months (in seconds):
			$trainingTime = 0;
			$trainingTimeRecords = Yii::app()->db->createCommand()
				->select("lastTime, enterTime")
				->from(LearningTracksession::model()->tableName())
				->where("lastTime > :last_time", array(':last_time' => date("Y-m-d H:i:s", strtotime("-12 MONTH"))))
				->queryAll();
			if (!empty($trainingTimeRecords)) {
				foreach ($trainingTimeRecords as $record) {
					$lastTime = strtotime($record['lastTime']);
					$enterTime = strtotime($record['enterTime']);
					$trainingTime += ($lastTime - $enterTime);
				}
			}
			$trainingTimeRecords = NULL; //free memory
			$output['training_time_12month'] = (int)$trainingTime;


			// Last login in platform:
			$lastLogin = $enrollCount = Yii::app()->db->createCommand()
				->select("MAX(lastenter) AS last_enter")
				->from(CoreUser::model()->tableName())
				->queryScalar();
			$output['last_platform_login'] = $lastLogin;

		}

		return $output;
	}

}
