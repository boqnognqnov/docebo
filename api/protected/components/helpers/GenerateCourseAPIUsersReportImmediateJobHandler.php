<?php

/**
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */
class GenerateCourseAPIUsersReportImmediateJobHandler extends JobHandler implements IJobHandler {

	const HANDLER_ID = 'root.api.protected.components.helpers.GenerateCourseAPIUsersReportImmediateJobHandler';

	const USERS_PER_PAGE = 10;

	const STATUS_IN_PROGRESS = 'running';
	const STATUS_COMPLETED = 'completed';

	/**
	 * Returns whether this handler allows for this "immediate"/"one-time"
	 * job to be reused.
	 *
	 * @return bool
	 */
	public function canReuseSameImmediateJob () {
		// Return true here to prevent this core_job from being ever deleted
		// by the JobExecutor after run. We keep it because we keep the
		// live processed data in the JSON array of the job
		return TRUE;
	}

	/**
	 * Initialize the job handler component
	 *
	 * @param array $params Array of arbitrary parameters
	 */
	public function init ( $params = FALSE ) {
		// TODO: Implement init() method.
	}

	/**
	 * Run the Job the handler is associated with (during creation)
	 *
	 * Called in a try/catch block, i.e. all exceptions thrown will be catched.
	 * If the method ends with NO exception thrown, job is considered done with success.
	 *
	 * @param array $params Array of arbitrary parameters
	 *
	 * @return void
	 */
	public function run ( $params = FALSE ) {
		// Already loaded at upper level
		$job = $this->job;
		/* @var $job CoreJob */

		$params = json_decode( $job->params, TRUE );

		Yii::log( 'Course API user generation Immediate Job Handler RUN started. Job hash: ' . $job->hash_id, CLogger::LEVEL_INFO );

		$users  = $params['input'];
		$output = isset( $params['output'] ) ? $params['output'] : array(
			'data'         => array(),
			'failed_users' => array(),
		);

		$withInteractions = $params['with_interactions'];

		$offset     = (int) $params['offset'];
		$totalCount = (int) $params['total'];

		$chunk = $this->processChunk( $users, $offset, self::USERS_PER_PAGE, $withInteractions );

		// Add the new output to the old output we already have
		// from previous iterations of the job
		$output['data']         = array_merge( $output['data'], $chunk['data'] );
		$output['failed_users'] = array_merge( $output['failed_users'], $chunk['failed_users'] );

		// Finally, set the merged output to the job params JSON
		$params['output'] = $output;

		$nextOffset = (int) ( $offset + self::USERS_PER_PAGE );

		$params['offset'] = $nextOffset;

		$batchJobEnded = $nextOffset > $totalCount;

		if( $batchJobEnded ) {
			// update the flag in the JSON params so
			// future API callers know that the job is finished
			$params['status'] = 'completed';
			$job->active = 0;
		}

		$job->params = CJSON::encode( $params );
		$job->save();

		if( ! $batchJobEnded ) {
			// Schedule the job to run one more time
			// (self-call, recursively increasing offset with each call)
			Yii::app()->scheduler->createApiJob( $job->name, $job->handler_id, $params, FALSE, $job );
		}
	}

	/**
	 * Returns string identifier of the handler CLASS (handler id).
	 * Must be unique LMS-wide, including among plugins and core handlers.
	 *
	 * Handler ID can be:
	 *        1) Arbitrary/Random string, in which case, JobHandler::registerHandler(<handler-id>, <class-path>) must be used to
	 *           map the ID to a class path run-time.
	 *                Example (in plugin module init()):
	 *                    JobHandler::registerHandler('myown_handler_id', 'plugins.my_plugin.MyJobHandler');
	 *
	 *    2) Class path (string), e.g. plugins.my_plugin.MyJobHandler, in which case the MyJobHandler.php will be used, no need to register.
	 *
	 * Handler ID is associated with jobs in the database, so it is the connection between Jobs and Handlers.
	 *
	 * @see ExampleJobHandler
	 *
	 * @param array $params Array of arbitrary parameters
	 *
	 * @return string
	 */
	public static function id ( $params = FALSE ) {
		return self::HANDLER_ID;
	}

	private function processChunk ( $allElements, $offset, $perPage, $withInteractions = true ) {
		$chunkUserIds = array_slice( $allElements, $offset, $perPage, TRUE );

		$validUserIds = Yii::app()->getDb()->createCommand()
							->select('idst')
							->from(CoreUser::model()->tableName())
							->andWhere(array('in', 'idst', $chunkUserIds))
							->queryColumn();

		$errors    = array();
		$successes = array();

		$getProgressPercentage = function($idUser, $idCourse){
			$tmpCourseModel = new LearningCourse();
			$tmpCourseModel->idCourse = $idCourse;
			$progressArr = $tmpCourseModel->getObjectsProgress($idUser, false);

			$objectsTotal = $progressArr['total'];
			$objectsCompleted = $progressArr['completed'];

			if($objectsTotal>0){
				return ($objectsCompleted/$objectsTotal*100);
			}else{
				return 0;
			}
		};

		foreach ( $chunkUserIds as $userId ) {
			try {

				if( ! in_array( $userId, $validUserIds ) ) {
					throw new Exception( 'User ID ' . intval( $userId ) . ' does not exist' );
				}

				$thisUserCourses = Yii::app()->getDb()->createCommand()
									  ->select( 'cu.idCourse, course.name, cu.date_complete, cu.status' )
									  ->from( LearningCourseuser::model()->tableName() . ' cu' )
									  ->join( LearningCourse::model()->tableName() . ' course', 'cu.idCourse=course.idCourse' )
									  ->join( LearningOrganization::model()->tableName() . ' o', 'course.idCourse=o.idCourse AND o.objectType=:scorm', array( ':scorm' => LearningOrganization::OBJECT_TYPE_SCORMORG ) )
									  ->where( 'cu.idUser=:idUser', array( ':idUser' => $userId ) )
									  ->group( 'cu.idUser, cu.idCourse' )
									  ->queryAll();

				if( empty( $thisUserCourses ) ) {
					throw new Exception( 'User ' . intval( $userId ) . ' is not enrolled in courses' );
				}

				$userCoursesDetails = array();

				$cntTotalQuestions   = 0;
				$cntCorrectQuestions = 0;

				foreach ( $thisUserCourses as $enrollment ) {
					$idCourse   = $enrollment['idCourse'];
					$courseName = $enrollment['name'];
					$courseStatus = LearningCourseuser::getStatusLabel($enrollment['status']);
					$dateComplete = $enrollment['date_complete'];
					$courseCompletionPercentage = (int) $getProgressPercentage($userId, $idCourse);

					$course = array(
						'course_id'       => (int)$idCourse,
						'course_name'	  => $courseName,
						'courseuser_status' => $courseStatus,
						'date_complete'     => $dateComplete,
						'course_progress'   => $courseCompletionPercentage,
						'total_questions'   => 0,
						'total_correct'     => 0,
						'percent_correct'   => 0,
						'interactions'      => array(),
					);

					$scormsInCourse = LearningOrganization::model()->findAllByAttributes( array(
						'idCourse'   => $idCourse,
						'objectType' => LearningOrganization::OBJECT_TYPE_SCORMORG,
					) );

					// Those 3 are organized on COURSE level, not SCORM level (a flat array, so to say, not nested)
					$interactionsInCourse           = array();
					$cntCorrectInteractionsInCourse = 0;
					$cntAllInteractionsInCourse     = 0;

					// Loop all SCORM LOs in THIS looped course
					foreach ( $scormsInCourse as $scormOrg ) {
						/* @var $scormOrg LearningOrganization */
						$scoItemsInScorm = Yii::app()->getDb()->createCommand()
											  ->select( 'idscorm_item' )
											  ->from( LearningScormItems::model()->tableName() )
											  ->where( 'idscorm_organization=:org', array( ':org' => $scormOrg->idResource ) )
											  ->andWhere( 'COALESCE(nChild,0)=0 AND COALESCE(nDescendant,0)=0' )// prevent SCO folders
											  ->queryAll();

						foreach ( $scoItemsInScorm as $sco ) {
							$scoTrackingModel = LearningScormTracking::model()->findByAttributes( array(
								'idUser'       => $userId,
								'idscorm_item' => $sco['idscorm_item'],
							) );
							if( $scoTrackingModel ) {
								$interactionsRaw = $scoTrackingModel->getLearnerInteractions($validUserIds);

								// Parse the interactions array and retrieve what we need
								if( is_array( $interactionsRaw ) ) {
									$cntAllInteractionsInCourse = $cntAllInteractionsInCourse + count( $interactionsRaw );

									foreach ( $interactionsRaw as $interactionRaw ) {
										$interactionId              = $interactionRaw['id'].'_'.$scormOrg->idOrg;
										$interactionType            = $interactionRaw['type'];
										$interactionLearnerResponse = $interactionRaw['learner_response'];
										$interactionResult          = $interactionRaw['result']; // wrong|correct
										$interactionWeighting       = $interactionRaw['weighting'];

										// Add this single interaction for this SCO
										// in the global (per-course) interactions array
										$interactionsInCourse[] = array(
											'id'               => $interactionId,
											'learner_response' => $interactionLearnerResponse,
										);

										if( $interactionResult == 'correct' ) {
											$cntCorrectInteractionsInCourse ++;
										}
									}
								}
							}
						}
					} // end of SCORM in single course loop

					if($withInteractions) {
						$course['interactions'] = $interactionsInCourse;
					}

					$course['total_questions'] = $cntAllInteractionsInCourse;
					$course['total_correct']   = $cntCorrectInteractionsInCourse;
					$course['percent_correct'] = $cntAllInteractionsInCourse > 0 ? ( $cntCorrectInteractionsInCourse / $cntAllInteractionsInCourse * 100 ) : 0;

					// Also increase the more global (per-user) counters with
					// the counters we got for this single course
					$cntTotalQuestions   = $cntTotalQuestions + $cntAllInteractionsInCourse;
					$cntCorrectQuestions = $cntCorrectQuestions + $cntCorrectInteractionsInCourse;

					$userCoursesDetails[] = $course;
				} // end of enrolled courses loop

				$userRow = array(
					'userid'          => $userId,
					'total_questions' => $cntTotalQuestions,
					'total_correct'   => $cntCorrectQuestions,
					'percent_correct' => $cntTotalQuestions > 0 ? ( $cntCorrectQuestions / $cntTotalQuestions * 100 ) : 0,
					'courses'         => $userCoursesDetails
				);

				$successes[] = $userRow;

			} catch ( Exception $e ) {
				$errors[] = array(
					'userid' => $userId,
					'error'  => $e->getMessage(),
				);
			}
		}

		return array(
			'data'         => $successes,
			'failed_users' => $errors,
		);
	}
}