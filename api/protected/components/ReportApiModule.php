<?php
/**
 * Report API Component
 * @description Set of APIs to manage reports
 */
class ReportApiModule extends ApiModule {

	const MAX_EXTRACT_ROWS = 500;

	/**
	 * Returns the requested security level for the passed action
	 * @param $actionName The name of the action
	 * @return string The requested security level
	 */
	public function getSecurityLevel($actionName)
	{
		switch ($actionName) {
			case 'user':
				return self::$SECURITY_LIGHT;
				break;
			default:
				return self::$SECURITY_STRONG;
		}
	}
    /**
     * @summary Lists all reports created in the system
     *
     * @parameter only_public [boolean, optional] Whether to show only public reports or not
     *
     * @response report_list [array, required] The array of reports
     *      @item id_filter [integer, required] The ID of the report
     *      @item report_type_id [integer, required] Type ID fpr this report
     *      @item author [integer, required] Author ID
     *      @item creation_date [datetime, required] Report creation date
     *      @item filter_name [string, required] Name of the filter
     *      @item is_public [boolean, required] Whether this is a public report
     *      @item views [integer, required] The number of views
     *      @item idst [integer, required] The ID of the author
     *      @item userid [string, required] Username of the author
     */
    public function listReports() {

		//read input
		$only_public = ($this->readParameter('only_public', 0) > 0);

		//retrieve data
		$command = Yii::app()->db->createCommand()
			->select("r.*, u.idst, u.userid")
			->from(LearningReportFilter::model()->tableName()." r")
			->leftJoin(CoreUser::model()->tableName()." u", "r.author = u.idst");
		if ($only_public) { $command->where("is_public = 1"); }
		$command->order("creation_date DESC, filter_name ASC");
		$list = $command->queryAll();

		//prepare output
		$output = array();
		if (!empty($list)) {
			foreach ($list as $item) {
				$output[] = array(
					'id_filter' => (int)$item['id_filter'],
					'report_type_id' => (int)$item['report_type_id'],
					'author' => (empty($item['author']) ? null : (int)$item['author']),
					'creation_date' => $item['creation_date'],
					'filter_name' => $item['filter_name'],
					'is_public' => (boolean)$item['is_public'],
					'views' => (int)$item['views'],
					'idst' => (empty($item['idst']) ? null : (int)$item['idst']), //same as "author" ... is it really necessary?!?
					'userid' => (empty($item['userid']) ? null : Yii::app()->user->getRelativeUsername($item['userid']))
				);
			}
		}

		return array('report_list' => $output);
	}


	/**
	 * @summary Return specially crafted array of data regarding particular User's Summary Report
	 *
	 * @parameter id_user [integer, required] User ID
	 *
	 * @status 201 Missing required param "id_user"
	 * @status 202 Invalid user
	 *
	 * @response html	[string, required] HTML string, no <b>BODY</b> tag, just rendered HTML, including inline Javascript
	 * @response js 	[array(string), required] Array of URLs of Javascript files
	 * @response css 	[array(string), required] Array of URLs to CSS files
	 */
	public function user() {
		$idUser = $this->readParameter('id_user', false);
		if (!$idUser) {
			throw new CHttpException(201, 'Missing required param "id_user"');
		}

		$userModel = CoreUser::model()->findByPk($idUser);
		if (!$userModel) {
			throw new CHttpException(202, 'Invalid user');
		}

		// Get speciall Mobile App specific 'widget' data.
		// Please carefully sturdy the widget and what it does.
		$widget = Yii::app()->controller->createWidget('mobileapp.widgets.UserPersonalSummary', array(
			'idUser' => $idUser,
		));

		$data = $widget->getData();

		// remove html \t and \n
		$data['html'] = str_replace(array("\t", "\n"), "", $data['html']);
		return $data;
	}

	/**
	 * @summary Returns the number of records matching the specified filters
	 * @note Usually needed for paginating report/extractRows. The number of rows depends on the custom report configuration and it should be properly set up inside the LMS Reports Management area.
	 * @parameter id_report [integer, required] internal custom report ID, as returned by report/listReports.
	 * @parameter date_from [datetime, optional] the start date & time used to additionally filter report rows (in yyyy-MM-dd HH:mm:ss format, UTC timezone). Used by the Audit Trail report.
	 * @parameter date_to [datetime, optional] the end date & time used to additionally filter report rows (in yyyy-MM-dd HH:mm:ss format, UTC timezone). Used by the Audit Trail report.
	 *
	 * @status 201 Missing or invalid param "id_report"
	 * @status 202 The "date_from" - "date_to" time interval cannot be larger than a month
	 *
	 * @response count [integer, required] the number of rows matching the specified filters.
	 */
	public function countRows(){
		$idReport = $this->readParameter('id_report');
		$dateFrom = strtotime($this->readParameter('date_from'));
		$dateTo = strtotime($this->readParameter('date_to'));

		$report = LearningReportFilter::model()->findByPk($idReport);

		if (!$report)
			throw new CHttpException(201, 'Missing or invalid param `id_report`');

		if($report->report_type_id = LearningReportType::AUDIT_TRAIL) {
			// AUDIT TRAIL report type
			// period is more than 1 month
			if ($dateFrom && $dateTo && ($dateTo - $dateFrom > 2592000))
				throw new CHttpException(202, 'The `date_from` - `date_to` time interval cannot be larger than a month');

			if ($dateFrom === false && $dateTo === false) {
				// no dates is provided, so we get the end date as current date, and start date one month behind
				$dateTo = strtotime(Yii::app()->localtime->getUTCNow());
				$dateFrom = $dateTo - 2592000;
			} elseif ($dateFrom === false && $dateTo) {
				// only end date is provided
				$dateFrom = $dateTo - 2592000;
			} elseif ($dateFrom && $dateTo === false) {
				// only start date is provided
				$dateTo = $dateFrom + 2592000;
			}

			$dateFrom = date('Y-m-d', $dateFrom);
			$dateTo = date('Y-m-d', $dateTo);
			$dataProvider = LearningReportFilter::getReportDataProviderInfo($idReport, false, array('audit_trail_date_from' => $dateFrom, 'audit_trail_date_to' => $dateTo))['dataProvider'];
			$result = $dataProvider->getTotalItemCount();
			return array('count' => (int)$result);
		} else {
			// some other report type
			$dataProvider = LearningReportFilter::getReportDataProviderInfo($idReport)['dataProvider'];
			$count = $dataProvider->getTotalItemCount();
			return array('count' => (int)$count);
		}
	}

	/**
	 * @summary Returns an array of rows extracted from a custom report and matching the specified filter.
	 * @note Use report/countRows to calculate the number of needed pages. The format of each row depends on the custom report configuration and it should be properly set up inside the LMS Reports Management area.
	 * @parameter id_report [integer, required] internal custom report ID, as returned by report/listReports.
	 * @parameter date_from [datetime, optional] the start date & time used to additionally filter report rows (in yyyy-MM-dd HH:mm:ss format, UTC timezone). Used by the Audit Trail report.
	 * @parameter date_to [datetime, optional] the end date & time used to additionally filter report rows (in yyyy-MM-dd HH:mm:ss format, UTC timezone). Used by the Audit Trail report.
	 * @parameter from [integer, optional] the offset to use for the pagination (starts from 0). If empty, default will be 0.
	 * @parameter count [integer, optional] the number of records to return per each call. If empty, default will be 500.
	 *
	 * @status 201 `count` cannot be larger than 500
	 * @status 202 Missing required param "id_report"
	 * @status 203 The "date_from" - "date_to" time interval cannot be larger than a month
	 *
	 * @response rows [array, required] the array of rows extracted from the custom report.
	 */
	public function extractRows(){
		$idReport = $this->readParameter('id_report');
		$dateFrom = strtotime($this->readParameter('date_from'));
		$dateTo = strtotime($this->readParameter('date_to'));
		$from = $this->readParameter('from', 0);
		$count = $this->readParameter('count', self::MAX_EXTRACT_ROWS);

		if(!$count)
			$count = self::MAX_EXTRACT_ROWS;

		if($count > self::MAX_EXTRACT_ROWS)
			throw new CHttpException(201, '`count` cannot be larger than ' . self::MAX_EXTRACT_ROWS);

		if (!$idReport)
			throw new CHttpException(202, 'Missing or invalid param `id_report`');

		$pagination = new DoceboCPagination();
		$pagination->setLimit($count);
		$pagination->setOffset($from);

		$report = LearningReportFilter::model()->findByPk($idReport);

		if($report->report_type_id == LearningReportType::AUDIT_TRAIL) {
			// AUDIT TRAIL report type

			// period is more than 1 month
			if ($dateFrom && $dateTo && ($dateTo - $dateFrom > 2592000))
				throw new CHttpException(203, 'The `date_from` - `date_to` time interval cannot be larger than a month');

			if ($dateFrom === false && $dateTo === false) {
				// no dates is provided, so we get the end date as current date, and start date one month behind
				$dateTo = strtotime(Yii::app()->localtime->getUTCNow());
				$dateFrom = $dateTo - 2592000;
			} elseif ($dateFrom === false && $dateTo) {
				// only end date is provided
				$dateFrom = $dateTo - 2592000;
			} elseif ($dateFrom && $dateTo === false) {
				// only start date is provided
				$dateTo = $dateFrom + 2592000;
			}

			$dateFrom = date('Y-m-d', $dateFrom);
			$dateTo = date('Y-m-d', $dateTo);

			$reportData = LearningReportFilter::getReportDataProviderInfo($idReport, $pagination, array('audit_trail_date_from' => $dateFrom, 'audit_trail_date_to' => $dateTo));
		} else {
			// some other report type
			$reportData = LearningReportFilter::getReportDataProviderInfo($idReport, $pagination);
		}

		$dataProvider = $reportData['dataProvider'];
		$gridColumns = $reportData['gridColumns'];
		$rawData = $dataProvider->getData();
		$result = array();
		$i = 0;
		foreach ($rawData as $data) {
			foreach ($gridColumns as $key => $gridColumn) {
				$cellValue = $data[$key];

				// Remove backslash from username (userid)
				if($key == "user.userid") {
					$cellValue = str_replace("/", "", $cellValue);
				}elseif(strpos($key, 'course_field_') === 0) {
					eval('$cellValue = ' . $gridColumn['value'] .';');
				}

				$result[$i][$key] = $cellValue;
			}
			$i++;
		}
		return array('rows' => $result);
	}
}
