<?php

/**
 * ILT API Component
 * @description Set of APIs to manage ilt course sessions of the LMS.
 * Please note that to use this API the ilt app must be enabled in the lms.
 */
class IltsessionsApiModule extends ApiModule {


	public function init($user) {

		parent::init($user);

		// Check if ilt is enabled, otherwise we should refuse api call
		if(!PluginManager::isPluginActive('ClassroomApp')) {
			// Launch an unathorized error
			throw new CHttpException("You need to enable the classroom app to use this api.", 401);
		}
	}

	/**
	 * @summary Return the list of scheduled ilt sessions
	 * @notes if you use the max_results parameter be carefull to keep it the same size for each page request in order to avoid possible missing data.
	 *
	 * @parameter id_course [integer, required] numeric ID of a course
	 * @parameter year [string, required] filter the course per year
	 * @parameter date_from [date, optional] filter by session start date from(in format yyyy-MM-dd, UTC timezone)
	 * @parameter date_to [date, optional] filter by session start date to(in format yyyy-MM-dd, UTC timezone)
	 * @parameter page [integer, optional] the page number
	 * @parameter max_results [integer, optional] the number of results per page, default 100, range (1..1000)
	 *
	 * @response sessions [array, required] The array of reports
	 *      @item id_session [integer, required] The ID of the date
	 *      @item name [string, required]
	 *      @item max_enroll [integer, required]
	 *      @item min_enroll [integer, optional]
	 *      @item date_begin [datetime, required] Note: this is going to be in UTC
	 *      @item date_end [datetime, required] Note: this is going to be in UTC
	 *      @item last_subscription_date [datetime, optional]
	 *      @item other_info [string, optional]
	 *      @item evaluation_type [integer, optional] 0 = online test, 1 = manual evaluation
	 *      @item score_base [integer, optional] This value is valid only if the evaluation_type is 1
	 *      @item dates [array, required]
	 *          @item day [date, required] Note: this will be expressed in the timezone
	 *          @item date_name [string, required]
	 *          @item time_begin [time, required] Note: this will be expressed in the timezone
	 *          @item time_end [time, required] Note: this will be expressed in the timezone
	 *          @item break_begin [time, required] Note: this will be expressed in the timezone
	 *          @item break_end [time, required] Note: this will be expressed in the timezone
	 *          @item timezone [string, required]
	 *          @item location_name [string. require] quick location info to avoid a lookup
	 *          @item location_address [string. require] quick location info to avoid a lookup
	 *          @item id_location [integer, required] id of the location, more info searching in the location table
	 *          @item id_classroom [integer, required] id of the classroom, more info searching in the location table
	 *      @end dates
	 *
	 * @status 400 Invalid date_from filter
	 * @status 400 Invalid date_to filter
	 * @status 402 Invalid id course provided
	 */
	public function listAction() {

		// Read input
		$input = new stdClass();
		$input->id_course   = $this->readParameter('id_course', false);
		$input->year        = $this->readParameter('year', false);
		$input->date_from	= $this->readParameter('date_from', false);
		$input->date_to		= $this->readParameter('date_to', false);

		$input->page        = $this->readParameter('page', 0);
		$input->max_results = $this->readParameter('max_results', 100);

		if ($input->max_results > 1000) $input->max_results = 1000;
		if ($input->max_results < 1) $input->max_results = 1;

		if (!$input->id_course) {
			throw new CHttpException(402, "Invalid id course provided");
		}

		// Is the course requested a classroom course?
		$course = LearningCourse::model()->findByPk($input->id_course);
		if (!$course || $course->course_type != LearningCourse::TYPE_CLASSROOM) {
			throw new CHttpException(402, "Invalid id course provided");
		}

		$cmd = Yii::app()->db->createCommand()
			->select('lcs.name as session_name, lcsd.name as date_name, lcs.id_session as session_id, lcsd.id_session as date_id , lcs.*, lcsd.*, ll.name AS location_name, ll.address AS location_address')
			->from(LtCourseSession::model()->tableName() . ' lcs')
			->leftJoin(LtCourseSessionDate::model()->tableName() . ' lcsd', 'lcs.id_session = lcsd.id_session')
			->leftJoin(LtLocation::model()->tableName() . ' ll', 'lcsd.id_location = ll.id_location')
			->where("course_id = :course_id", array(':course_id' => $input->id_course));

		if ($input->year != false) {

			$cmd->andWhere("( lcs.date_begin BETWEEN :date_begin AND :date_end OR lcs.date_end BETWEEN :date_begin AND :date_end )", array(
				':date_begin' =>  $input->year . '-01-01 00:00:00',
				':date_end' =>  $input->year . '-12-31 23:59:59',
			));
		}
		if ($input->date_from != false) {
			if(!CourseApiModule::_validateDate($input->date_from, true))
				throw new CHttpException(400, 'Invalid date_from filter');

			$cmd->andWhere("( lcs.date_begin >= :date_begin)", array(
				':date_begin' => $input->date_from.' 00:00:00',
			));
		}
		if ($input->date_to != false) {
			if(!CourseApiModule::_validateDate($input->date_to, true))
				throw new CHttpException(400, 'Invalid date_to filter');

			$cmd->andWhere("( lcs.date_begin <= :date_to)", array(
				':date_to' => $input->date_to.' 23:59:59',
			));
		}
		$cmd->offset($input->page * $input->max_results);
		$cmd->limit($input->max_results);
		$cmd->order("lcs.date_begin, lcsd.day");

		$oSessions = $cmd->queryAll();

		$sessions['sessions'] = array();
		foreach ($oSessions as $data) {

			if (!isset($sessions['sessions'][$data['session_id']])) {
				$datesArray = array();
				if(isset($data['date_id']) && $data['date_id']){
					$datesArray =	array(
						array(
							'day'              => $data['day'],
							'name'             => $data['date_name'],
							'time_begin'       => $data['time_begin'],
							'time_end'         => $data['time_end'],
							'break_begin'      => $data['break_begin'],
							'break_end'        => $data['break_end'],
							'timezone'         => $data['timezone'],
							'location_name'    => $data['location_name'],
							'location_address' => $data['location_address'],
							'id_location'      => $data['id_location'],
							'id_classroom'     => $data['id_classroom'],
						),
					);
				}

				$sessions['sessions'][$data['session_id']] = array(
					'id_session'             => (int)$data['session_id'],
					'id_course'              => (int)$data['course_id'],
					'name'                   => $data['session_name'],
					'date_begin'             => $data['date_begin'],
					'date_end'               => $data['date_end'],
					'max_enroll'             => (int)$data['max_enroll'],
					'min_enroll'             => (int)$data['min_enroll'],
					'last_subscription_date' => $data['last_subscription_date'],
					'other_info'             => $data['other_info'],
					'evaluation_type'        => (int)$data['evaluation_type'],
					'score_base'             => (int)$data['score_base'],
					'dates'                  => $datesArray,
				);


			} else {
				// main session already created, add a date
				if(isset($data['date_id']) && $data['date_id']){
					// main session already created, add a date
					$sessions['sessions'][$data['session_id']]['dates'][] = array(
						'day'              => $data['day'],
						'name'             => $data['date_name'],
						'time_begin'       => $data['time_begin'],
						'time_end'         => $data['time_end'],
						'break_begin'      => $data['break_begin'],
						'break_end'        => $data['break_end'],
						'timezone'         => $data['timezone'],
						'location_name'    => $data['location_name'],
						'location_address' => $data['location_address'],
						'id_location'      => $data['id_location'],
						'id_classroom'     => $data['id_classroom'],
					);
				}
			}

		}

		// Unfortunately the main array need to be without id_session or it will become a json object
		$sessions['sessions'] = array_values($sessions['sessions']);

		return $sessions;
	}


	/**
	 * @summary Creates a new Classroom course
	 *
	 * @parameter id_course [integer, required] numeric ID of a course
	 * @parameter name [string, required]
	 * @parameter max_enroll [integer, required]
	 * @parameter min_enroll [integer, optional]
	 * @parameter last_subscription_date [datetime, optional]
	 * @parameter other_info [string, optional]
	 * @parameter evaluation_type [integer, optional] 0 = online test, 1 = manual evaluation
	 * @parameter score_base [integer, optional] This value is valid only if the evaluation_type is 1
	 * @parameter dates [array, required]
	 *      @item day [date, required] Note: this must be expressed in the timezone
	 *      @item name [string, required]
	 *      @item time_begin [time, required] Note: this must be expressed in the timezone
	 *      @item time_end [time, required] Note: this must be expressed in the timezone
	 *      @item break_begin [time, required] Note: this must be expressed in the timezone
	 *      @item break_end [time, required] Note: this must be expressed in the timezone
	 *      @item timezone [string, required]
	 *      @item id_location [integer, required] id of the location, more info searching in the location table
	 *      @item id_classroom [integer, required] id of the classroom, more info searching in the location table
	 * @end dates
	 * @response id_session [integer, required] Internal Docebo ID for the new session. Used in subsequent calls to manage this group.
	 *
	 * @status 201 Missing mandatory param 'dates'
	 * @status 205 Validation errors: <list of errors>
	 * @status 206 Save error
	 *
	 */
	public function insert() {

		$cs = new LtCourseSession();
		$cs->course_id              = $this->readParameter('id_course', false);
		$cs->name                   = $this->readParameter('name', false);
		$cs->max_enroll             = $this->readParameter('max_enroll', false);
		$cs->min_enroll             = $this->readParameter('min_enroll', false);
		$cs->last_subscription_date = $this->readParameter('last_subscription_date', false);
		$cs->other_info             = $this->readParameter('other_info', false);
		$cs->evaluation_type        = $this->readParameter('evaluation_type', false);
		$cs->score_base             = $this->readParameter('score_base', false);

		$dates = $this->readParameter('dates', false);
		if (empty($dates)) {
			throw new CHttpException(201, "You need to specify at least one date");
		}

			// validate data received
		if (!$cs->validate()) {
			throw new CHttpException(205, "Validation errors: " . $this->errorSummary($cs));
		}

		// save the dates, this will also save the main record and update the correct columns
		try {
			$cs->saveWithDates($dates);
		} catch(Exception $e) {
			throw new CHttpException(206, "An error has been raised while saving the new session, please try again.");
		}

		return array(
			'id_session' => $cs->id_session
		);
	}


	/**
	 * @summary Updates an existing session in the LMS
	 *
	 * @parameter id_course [integer, required] numeric ID of a course
	 * @parameter id_session [integer, required] numeric ID of a course
	 * @parameter name [string, required]
	 * @parameter max_enroll [integer, required]
	 * @parameter min_enroll [integer, optional]
	 * @parameter last_subscription_date [datetime, optional]
	 * @parameter other_info [string, optional]
	 * @parameter evaluation_type [integer, optional] 0 = online test, 1 = manual evaluation
	 * @parameter score_base [integer, optional] This value is valid only if the evaluation_type is 1
	 * @parameter dates [array, required]
	 *      @item day [date, required] Note: this must be expressed in the timezone
	 *      @item name [string, required]
	 *      @item time_begin [time, required] Note: this must be expressed in the timezone
	 *      @item time_end [time, required] Note: this must be expressed in the timezone
	 *      @item break_begin [time, required] Note: this must be expressed in the timezone
	 *      @item break_end [time, required] Note: this must be expressed in the timezone
	 *      @item timezone [string, required]
	 *      @item id_location [integer, required] id of the location, more info searching in the location table
	 *      @item id_classroom [integer, required] id of the classroom, more info searching in the location table
	 * @end dates
	 * @response id_session [integer, required] Internal Docebo ID for the new session. Used in subsequent calls to manage this group.
	 *
	 * @status 201 Missing mandatory params
	 * @status 202 Invalid group ID provided
	 * @status 205 Validation errors: <list of errors>
	 * @status 206 Save error
	 */
	public function update() {

		$id_session = $this->readParameter('id_session', false);

		$cs = LtCourseSession::model()->findByPk($id_session);
		if(!$cs)
			throw new CHttpException(202, "Invalid session ID provided");

		$cs->course_id              = $this->readParameter('id_course', false);
		$cs->name                   = $this->readParameter('name', false);
		$cs->max_enroll             = $this->readParameter('max_enroll', false);
		$cs->min_enroll             = $this->readParameter('min_enroll', false);
		$cs->last_subscription_date = $this->readParameter('last_subscription_date', false);
		$cs->other_info             = $this->readParameter('other_info', false);
		$cs->evaluation_type        = $this->readParameter('evaluation_type', false);
		$cs->score_base             = $this->readParameter('score_base', false);

		$dates = $this->readParameter('dates', false);
		if (empty($dates)) {
			throw new CHttpException(201, "You need to specify at least one date");
		}

		// validate data received
		if (!$cs->validate()) {
			throw new CHttpException(205, "Validation errors: " . $this->errorSummary($cs));
		}

		// save the dates, this will also save the main record and update the correct columns
		try {
			$cs->saveWithDates($dates);
		} catch(Exception $e) {
			throw new CHttpException(206, "An error has been raised while saving the new session, please try again.");
		}

		return array(
			'id_session' => $cs->id_session
		);
	}



		/**
	 * @summary Update the status of a user enrolled in an ILT session
	 *
	 * @parameter id_user [integer, required]: numeric ID of an user
	 * @parameter id_session [integer, required]: numeric ID of an ILT session
	 * @parameter user_status [string, required]: user enrollment status. Possible values: "waiting users", "subscriptions to confirm", "in progress", "completed", "suspended", "waitlisted".
	 * @parameter evaluation_status [string, required]: Possible values: "passed", "not passed".
	 * @parameter evaluation_score [integer, optional]: score obtained by user during the evaluation (default = 0).
	 * @parameter evaluation_comment [string, optional]: Comments explaining the reason of the evaluation (default = "").
	 * @parameter evaluated_on [date, optional]: Evaluation date; if not passed, defaults to today (yyyy-mm-ddTHH:mm:ss+0000 format, UTC timezone).
	 * @parameter mark_all_as_present [boolean, optional]: if true marks the user as present in all sessions dates (default= false).
	 *
	 * @status 100 Missing parameter `id_user`
	 * @status 101 Missing parameter `id_session`
	 * @status 102 Missing parameter `user_status`
	 * @status 103 Missing parameter `evaluation_status`
	 * @status 104 User with current id enrolled in current session not exist
	 * @status 105 '`user_status` parameter value is impossible'
	 * @status 106 '`evaluation_status` parameter value is impossible'
	 * @status 107 `evaluated_on` Incorrect date format
	 * @status 108 Error to try to present current user in all session
	 */
	public function ilt_update_status()
	{
//		MANDATORY PARAMETERS
		$idUser = $this->readParameter('id_user');
		if (!$idUser)
			throw new CHttpException(100, 'Missing parameter `id_user`');

		$idSession = $this->readParameter('id_session');
		if (!$idSession)
			throw new CHttpException(101, 'Missing parameter `id_session`');

		$userStatus = $this->readParameter('user_status');
		if (!$userStatus)
			throw new CHttpException(102, 'Missing parameter `user_status`');

		$evaluationStatus = $this->readParameter('evaluation_status');
		if (!$evaluationStatus)
			throw new CHttpException(103, 'Missing parameter `evaluation_status`');


//		OPTIONAL PARAMETERS
		$evaluationScore = $this->readParameter('evaluation_score');
		$evaluationComment = $this->readParameter('evaluation_comment');
		$evaluatedOn = $this->readParameter('evaluated_on');
		$markAllAsPresent = $this->readParameter('mark_all_as_present');

		$user = null;

		$user = LtCourseuserSession::model()->findByAttributes(
			array('id_user' => $idUser, 'id_session' => $idSession)
		);

		if (!$user) {
			throw new CHttpException(104, 'User with id_user ' . $idUser . ' or id_session ' . $idSession . ' not exist');
		}

		$status = 0;

		switch ($userStatus) {
			case 'waiting users':
				$status = LtCourseuserSession::$SESSION_USER_WAITING_LIST;
				break;
			case 'subscriptions to confirm':
				$status = LtCourseuserSession::$SESSION_USER_CONFIRMED;
				break;
			case 'in progress':
				$status = LtCourseuserSession::$SESSION_USER_BEGIN;
				break;
			case 'completed':
				$status = LtCourseuserSession::$SESSION_USER_END;
				break;
			case 'suspended':
				$status = LtCourseuserSession::$SESSION_USER_SUSPEND;
				break;
			case 'waitlisted':
				$status = LtCourseuserSession::$SESSION_USER_SUBSCRIBED;
				break;
			default:
				throw new CHttpException(105, '`user_status` parameter value is impossible');
		}


		$evaluationStatusInt = 0;
		switch ($evaluationStatus) {
			case 'passed':
				$evaluationStatusInt = LtCourseuserSession::EVALUATION_STATUS_PASSED;
				break;
			case 'not passed':
				$evaluationStatusInt = LtCourseuserSession::EVALUATION_STATUS_FAILED;
				break;
			default:
				throw new CHttpException(106, '`evaluation_status` parameter value is impossible');
		}


		$user->status = $status;

		$user->evaluation_status = $evaluationStatusInt;

		if (empty($evaluationScore)) {
			$evaluationScore = 0;
		}
		$user->evaluation_score = $evaluationScore;

		$user->evaluation_text = $evaluationComment;

		if (!empty($evaluatedOn)) {
			if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $evaluatedOn)) {
				$user->evaluation_date = $evaluatedOn;
			} else {
				throw new CHttpException(107, 'Incorrect date format');
			}
		} else {
//				$evaluatedOn = date("yyyy-mm-ddTHH:mm:ss");
			$user->evaluation_date = date("Y-m-d");
		}


		if ($markAllAsPresent == 'true') {
			$status = $this->presentUserInAllSession($idSession, $idUser);
			if ($status == false)
				throw new CHttpException(108, 'Error to try to present current user in all session');
		}

		try {
			$user->save();

		} catch (Exception $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			return ['status' => 'error'];
		}


		return ['status' => 'User with id: ' . $idUser . ' updated successfully'];
	}

	private function presentUserInAllSession($sessionId, $userId)
	{
		try {
			$datesOfThisSession = Yii::app()->db->createCommand()
				->select('t.day')
				->from(LtCourseSessionDate::model()->tableName() . ' t')
				->where('t.id_session = :id_session', ['id_session' => $sessionId])
				->queryAll();

			LtCourseSessionDateAttendance::model()->deleteAll(
				'id_session = :id_session and id_user = :id_user',
				array('id_session' => $sessionId, 'id_user' => $userId)
			);

			foreach ($datesOfThisSession as $oneDate) {
				$record = new LtCourseSessionDateAttendance();
				$record->id_session = $sessionId;
				$record->day = $oneDate['day'];
				$record->id_user = $userId;
				$record->save();
			}
		} catch (Exception $e) {
			return false;
		}
		return true;
	}


	/**
	 * @summary Deletes a session in the LMS
	 *
	 * @parameter id_session [integer, required] ID of the session to delete
	 *
	 * @status 201 Missing mandatory params
	 * @status 202 Invalid group ID provided
	 */
	public function delete() {

		$id_session = $this->readParameter('id_session', null);
		if(!$id_session)
			throw new CHttpException(201, "Missing mandatory param");

		// Try loading group
		$session = LtCourseSession::model()->findByPk($id_session);
		if(!$session)
			throw new CHttpException(202, "Invalid session ID provided");

		// Delete the session, it will also delete associated resources
		$session->deleteSession();
	}


	/**
	 * @summary Assigns a user to a classroom course and session
	 *
	 * @parameter id_course [integer, required] ID of the course to assign to
	 * @parameter id_session [integer, required] ID of the session to assign to
	 * @parameter id_user [integer, required] ID of the user to assign
	 *
	 * @status 201 Missing mandatory params
	 * @status 202 Invalid course ID provided
	 * @status 203 Invalid user ID provided
	 * @status 205 User already assigned to this course
	 * @status 206 Save error
	 */
	public function enroll() {

		$id_course  = $this->readParameter('id_course', false);
		$id_session = $this->readParameter('id_session', false);
		$id_user    = $this->readParameter('id_user', false);

		$course = LearningCourse::model()->findByPk($id_course);
		if (!$course)
			throw new CHttpException(202, "Invalid course provided");

		$user = CoreUser::model()->findByPk($id_user);
		if(!$user)
			throw new CHttpException(203, "Invalid user ID provided");

		if (LtCourseuserSession::isSubscribed($id_user, $id_session)) {
			throw new CHttpException(205, "User already assigned to this course");
		}

		if (!LtCourseuserSession::enrollUser($id_user, $id_session)) {
			throw new CHttpException(206, "Save error");
		}
	}

	/**
	 * @summary Removes a user from the specified course session
	 *
	 * @parameter id_course [integer, required] ID of the course to assign to
	 * @parameter id_session [integer, required] ID of the session to assign to
	 * @parameter id_user [integer, required] ID of the user to assign
	 *
	 * @status 201 Missing mandatory params
	 * @status 202 Invalid course ID provided
	 * @status 203 Invalid user ID provided
	 * @status 206 Delete error
	 */
	public function unassign() {

		$id_course  = $this->readParameter('id_course', false);
		$id_session = $this->readParameter('id_session', false);
		$id_user    = $this->readParameter('id_user', false);

		$course = LearningCourse::model()->findByPk($id_course);
		if (!$course)
			throw new CHttpException(202, "Invalid course provided");
		$user = CoreUser::model()->findByPk($id_user);

		if(!$user)
			throw new CHttpException(203, "Invalid user ID provided");

		if (!LtCourseuserSession::unsubscribeUser($id_user, $id_session)) {
			throw new CHttpException(206, "Delete error");
		}
	}

	/**
	 * @summary Removes a user from the specified course session
	 *
	 * @parameter year [integer, optional] All the sessions that start in that year
	 * @parameter course_id [integer, optional] Numeric ID of a course
	 * @parameter from [integer, optional] limit the resultset by record offset
	 * @parameter count [integer, optional] limit the resultset by number of records
	 *
	 * @response ilt_course_status [array, required] The retrieved enrollments
	 * 		@item course_id [integer, required] Course ID
	 *      @item code [string, required] Alphanumeric course code
	 *      @item course_name [string, required] Course name/title
	 *      @item course_description [string, required] Course description
	 * 		@item course_language [string, required] Course language (E.g. 'english', italian', 'french', ...)
	 *      @item status [string, required] Course status ( In preparation, Available, ...)
	 *      @item selling [boolean, optional] Is this course on sale?
	 *      @item price [float, optional] Course price (for the ecommerce cart)
	 *      @item subscribe_method [string, required] Subscription method in the catalog (Admin only, Moderated, Free)
	 *      @item course_type [string, required] Course type (elearning, classroom)
	 *      @item sub_start_date [datetime, optional] Subscription start date
	 *      @item sub_end_date [datetime, optional] Subscription end date
	 *      @item date_begin [date, required] Course validity begin date
	 *      @item date_end [date, required] Course validity end dat
	 * 		@item course_link [string, required] URL the course
	 * 		@item waitinglist_users [integer, required] Users in course waiting list or not associated to any session
	 * 		@item maximum_enrollments [integer, required] Maximum enrollments in the course
	 * 		@item enrolled_user [integer, required] The number of users enrolled in the course
	 * 		@item completed_user [integer, required] The number of users enrolled in the course with the "completed" status
	 * 		@item course_teacher [array, required] List of all the users enrolled in the course with the "teacher" role
	 * 			@item username [string, required] Teacher's username
	 *		@end  course_teacher
	 * 		@item idSession [integer, required] Session ID
	 * 		@item session_name [string, required] Session name
	 *      @item session_start_date [datetime, required] Session Start date (in UTC)
	 *      @item session_end_date [datetime, required] Session End date (in UTC)
	 *      @item session_start_date_timezone [datetime, required] Session Start date Time Zone
	 *      @item session_start_end_timezone [datetime, required] Session End date Time Zone
	 *      @item session_location_list [string, required] All the different locations for the session dates
	 *      @item session_country_list [string, required] All the different countries for the session dates
	 *      @item session_teacher [array, required] All the users initials enrolled in the session with the "teacher" role
	 * 			@item username [string, required] Teacher's username
	 *      @end session_teacher
	 *      @item total_hours [double, required] Session duration time
	 *      @item session_max_enrollments [integer, required] Maxumum enrollments of the current session
	 *      @item enrolled_users [integer, required] The number of the users enrolled in the current session
	 *
	 * @status 201 Missing mandatory params
	 */
	public function ilt_course_status() {
		set_time_limit(60*5); //just in case, set the time limit to be 5 minutes
		$max_items = 5000;

		//read input
		$input = new stdClass();
		$input->year = $this->readParameter('year');
		$input->course_id = $this->readParameter('course_id');
		$input->from = $this->readParameter('from', 0);
		$input->count = $this->readParameter('count', $max_items);

		// If the count is bigger than the allowed max count, limit it.
		if(!$input->count || ($input->count > $max_items))
			$input->count = $max_items;

		// If no of the required fields is added them error
		if(!($input->year || $input->course_id))
			throw new CHttpException(201, "Missing mandatory params");

		$id_sessions = false;
		$id_courses = array();

		if($input->course_id)
		{
			$id_courses[] = $input->course_id;
			$id_sessions = Yii::app()->db->createCommand()
				->select('id_session')
				->from(LtCourseSession::model()->tableName())
				->where('course_id = :course_id', array(':course_id' => $input->course_id))
				->queryColumn();
		}

		if(is_array($id_sessions) && empty($id_sessions))
			return array('ilt_course_status' => array());

		if($input->year)
		{
			$command = Yii::app()->db->createCommand()
				->select('id_session')
				->from(LtCourseSession::model()->tableName());

			if($id_sessions !== false)
				$command->andWhere(array('in', 'id_session', $id_sessions));

			$command->andWhere("DATE_FORMAT(date_begin, '%Y') = :year", array(':year' => $input->year));

			$id_sessions = $command->queryColumn();
		}

		if(is_array($id_sessions) && empty($id_sessions))
			return array('ilt_course_status' => array());

		if(empty($id_courses))
		{

			$id_courses = Yii::app()->db->createCommand()
				->select('DISTINCT(course_id)')
				->from(LtCourseSession::model()->tableName())
				->andWhere(array('in', 'id_session', $id_sessions))
				->queryColumn();
		}

		$selectedColumns = array(
			"lc.idCourse",
			"lc.code AS courseCode",
			"lc.name AS courseName",
			"lc.description AS courseDesc",
			"lc.lang_code AS courseLang",
			"lc.status",
			"lc.selling",
			"lc.prize",
			"lc.date_begin as courseBegin",
			"lc.date_end as courseEnd",
			"lc.subscribe_method",
			"lc.max_num_subscribe",
			"lcs.id_session",
			"lcs.name as sessionName",
			"lcs.date_begin",
			"lcs.date_end",
			"lcs.total_hours",
			"lcs.max_enroll",
			'counts.waiting',
			'counts.enrolled',
			'counts.completed',
			'counts.teachers',
			'session.sessionTeacher',
			'session.sessionEnrolled',
			'location.locations',
			'location.countries',
		);

		$counts = Yii::app()->db->createCommand()
			->select(array(
				'idCourse'
				,'COUNT(IF(ilcu.status = -2, ilcu.idUser, NULL)) AS waiting'
				,'COUNT(IF((ilcu.status = 0 OR ilcu.status = 1 OR ilcu.status = 2 OR ilcu.status = -2), ilcu.idUser, NULL)) AS enrolled'
				,'COUNT(IF(ilcu.status = 2, ilcu.idUser, NULL)) AS completed'
				,"GROUP_CONCAT(IF(ilcu.level = 6, SUBSTR(icu.userid,2), NULL)  SEPARATOR ',') AS teachers"
			))
			->from(CoreUser::model()->tableName() . ' icu')
			->join(LearningCourseuser::model()->tableName() . ' ilcu', 'ilcu.idUser = icu.idst')
			->where(array('in', 'ilcu.idCourse', $id_courses))
			->group('idCourse')->text;

		$session = Yii::app()->db->createCommand()
			->select(array(
				'ilcs.course_id'
				,'ilcs.id_session'
				,"GROUP_CONCAT( DISTINCT IF(ilcu.level = 6, SUBSTR(icu.userid,2), NULL)  SEPARATOR ',') AS sessionTeacher"
				,'COUNT(IF((ilcu.status = 0 OR ilcu.status = 1 OR ilcu.status = 2 OR ilcu.status = -2), ilcu.idUser, NULL)) AS sessionEnrolled'
			))
			->from(LtCourseSession::model()->tableName() . ' ilcs')
			->join(LtCourseuserSession::model()->tableName() . ' ilcus', 'ilcus.id_session = ilcs.id_session')
			->join(LearningCourseuser::model()->tableName(). ' ilcu', 'ilcu.idCourse = ilcs.course_id AND ilcu.idUser = ilcus.id_user')
			->join(CoreUser::model()->tableName() . ' icu', 'icu.idst = ilcus.id_user')
			->where(array('in', 'ilcs.id_session', $id_sessions))
			->group('ilcs.course_id, ilcs.id_session')->text;

		$locations = Yii::app()->db->createCommand()
			->select(array(
				'lcsd.id_session'
				,'GROUP_CONCAT(loc.`name`  SEPARATOR ", ") AS locations'
				,'GROUP_CONCAT(cc.`name_country` SEPARATOR ", ") AS countries'
			))
			->from(LtCourseSessionDate::model()->tableName() . ' lcsd')
			->join(LtLocation::model()->tableName() . ' loc', 'loc.id_location = lcsd.id_location')
			->join(CoreCountry::model()->tableName() . ' cc', 'cc.id_country = loc.id_country')
			->where(array('in', 'lcsd.id_session', $id_sessions))
			->group('lcsd.id_session')->text;

		// Create the query to extracto all the data needed for this API
		$command = Yii::app()->db->createCommand()
			->select($selectedColumns)
			->from(LtCourseSession::model()->tableName() . ' lcs')
			->join(LearningCourse::model()->tableName(). ' lc', 'lc.idCourse = lcs.course_id')
			->leftJoin("(" . $counts . ") as `counts`", 'counts.idCourse = lc.idCourse')
			->leftJoin("(" . $session . ") as `session`", 'session.course_id = lc.idCourse AND session.id_session = lcs.id_session')
			->leftJoin("(" . $locations . ") as `location`", 'location.id_session = lcs.id_session');

		// Filter the query if the user has passed any filters
		$command->andWhere(array('in', 'lcs.id_session', $id_sessions));

		$timezones = array();
		$dates = Yii::app()->db->createCommand()
			->select('id_session, timezone')
			->from(LtCourseSessionDate::model()->tableName())
			->andWhere(array('in', 'id_session', $id_sessions))
			->order('day, time_begin')
			->queryAll();
		foreach($dates as $date)
		{
			$tz = $date['timezone'];
			$id = $date['id_session'];
			if(isset($timezones[$id]))
				$timezones[$id]['end'] = $tz;
			else
			{
				$timezones[$id]['begin'] = $tz;
				$timezones[$id]['end'] = $tz;
			}
		}

		if ((int)$input->count > 0) {
			$command->limit((int)$input->count, (int)$input->from);
		}
		//execute DB calculation
		$list = $command->queryAll();

		$output = array();
		if (!empty($list)) {
			$subscribtionMethods = LearningCourse::subscribeMethodList();
			// Loop through all the records
			foreach ($list as $item) {
				// Format the sub_start and sub_end dates of the course
				$subStartDate = ($item['sub_start_date'] === '0000-00-00 00:00:00' || !$item['sub_start_date']) ? null : Yii::app()->localtime->toLocalDateTime($item['sub_start_date'], LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, null, null, true, true ? true : false);
				$subEndDate = ($item['sub_end_date'] === '0000-00-00 00:00:00' || !$item['sub_end_date']) ? null : Yii::app()->localtime->toLocalDateTime($item['sub_end_date'], LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, null, null, true, true ? true : false);

				$cur = array(
					'id_course'						=> (int)$item['idCourse'],
					'course_code'					=> $item['courseCode'],
					'course_title'					=> $item['courseName'],
					'course_description'			=> $item['courseDesc'],
					'course_language'				=> $item['courseLang'],
					'status'						=> LearningCourse::getStatusLabelTrsnaslated($item['status']),
					'selling'						=> boolval($item['selling']),
					'price'							=> (float)$item['prize'],
					'subscribe_method'				=> $subscribtionMethods[$item['subscribe_method']],
					'course_type'					=> LearningCourse::CLASSROOM,
					'sub_start_date'				=> $subStartDate,
					'sub_end_date'					=> $subEndDate,
					'date_begin'					=> $item['courseBegin'],
					'date_end'						=> $item['courseEnd'],
					'course_link'					=> Docebo::getOriginalDomain() . Docebo::createLmsUrl('player/training/session', array('course_id' => $item['idCourse'], 'session_id' => $item['id_session'])),
					'waitinglist_users'				=> (int)$item['waiting'],
					'maximum_enrollments'			=> $item['max_num_subscribe'] ? (int)$item['max_num_subscribe'] : null,
					'enrolled_user'					=> (int)$item['enrolled'],
					'completed_user'				=> (int)$item['completed'],
					'course_teacher'				=> explode(',', $item['teachers']),
					'idSession'						=> (int)$item['id_session'],
					'session_name'					=> $item['sessionName'],
					'session_start_date'			=> Yii::app()->localtime->toUTC($item['date_begin']),
					'session_end_date'				=> Yii::app()->localtime->toUTC($item['date_end']),
					'session_start_date_timezone'	=> $timezones[(int)$item['id_session']]['begin'],
					'session_end_date_timezone'		=> $timezones[(int)$item['id_session']]['end'],
					'session_location_list'			=> $item['locations'],
					'session_country_list'			=> $item['countries'],
					'session_teacher'				=> explode(',', $item['sessionTeacher']),
					'total_hours'					=> (double)$item['total_hours'],
					'session_max_enrollments'		=> (int)$item['max_enroll'],
					'enrolled_users'				=> (int)$item['sessionEnrolled'],
				);

				$output[] = $cur;
			}
		}

		return array('ilt_course_status' => $output);
	}

	/**
	 * @summary Retrieves session cound
	 *
	 * @status 201 Missing mandatory params
	 *
	 * @parameter year [integer, optional] All the sessions that start in that year
	 * @parameter course_id [integer, optional] Numeric ID of a course
	 *
	 * @response ilt_course_status_count [integer, required] The total number of sessions
	 *
	 */
	public function ilt_course_status_count(){
		//read input
		$input = new stdClass();
		$input->year = $this->readParameter('year');
		$input->course_id = $this->readParameter('course_id');

		// If no of the required fields is added them error
		if(!($input->year || $input->course_id))
			throw new CHttpException(201, "Missing mandatory params");

		// Create the query to extracto all the data needed for this API
		$command = Yii::app()->db->createCommand()
			->select('count(1)')
			->from(LtCourseSession::model()->tableName() . ' lcs')
			->join(LearningCourse::model()->tableName(). ' lc', 'lc.idCourse = lcs.course_id');

		// Filter the query if the user has passed any filters
		if ($input->year) {$command->andWhere("DATE_FORMAT(lcs.date_begin, '%Y') = :year", array(':year' => $input->year));}
		if ($input->course_id) {$command->andWhere("lc.idCourse = :course", array(':course' => $input->course_id));}

		// Execute DB calculation
		$count = $command->queryScalar();
		return array('ilt_course_status_count' => (int)$count);
	}

}
