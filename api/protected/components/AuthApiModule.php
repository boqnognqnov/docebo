<?php
/**
 * Created by PhpStorm.
 * User: Dzhuneyt
 * Date: 14-8-11
 * Time: 13:05
 */

class AuthApiModule extends ApiModule {

	public function authenticate(){

		$username = $this->readParameter('username');
		$password = $this->readParameter('password');

		if ($username == false || $password === false) {
			//error: no login data provided
			throw new CHttpException(500, 'Invalid username or password');
		} else {
			$res = $this->_generateToken($username, $password);
			if ($res['success']) {
				$output = array(
					'success'=>true,
					'message'=>'You are authenticated.',
					'idst'=>$res['idst'],
					'token'=>$res['token'],
					'expire_at'=>$res['expire_at']
				);

				return $output;
			} else {
				throw new CHttpException(500, 'Invalid username or password');
			}
		}
	}

	private function _generateToken($username, $password){

		// Check if user exists
		$user = CoreUser::model()->findByAttributes(array(
			'userid'=>Yii::app()->user->getAbsoluteUsername($username)
		));
		// Validate password too
		if($user){
			if(false == Yii::app()->security->validatePassword($password, $user->pass) && $user->pass != md5($password)){
				$user = null;
			}
		}

		if($user){

			$timenow = time();
			$now = date("Y-m-d H:i:s", $timenow);
			$level = CoreGroup::model()->getGroupIdByLabel(Yii::app()->user->getHighestLevel($user->idst));
			if(!$level) $level = 0;
			$token = md5(uniqid(rand(), true) + $username);

			$lifetime = Settings::get('rest_auth_lifetime', 1);
			$expire = date("Y-m-d H:i:s", ($timenow + $lifetime)) ;

			// check if the user is already authenticated
			// If yes, refresh his token
			// If not, authenticate him and save his token in DB with specified lifetime
			$existingToken = CoreRestAuthentication::model()->findByAttributes(array(
				'id_user' => $user->idst,
			));
			if($existingToken){
				$existingToken->token = $token;
				$existingToken->generation_date = $now;
				$existingToken->last_enter_date = null;
				$existingToken->expiry_date = $expire;
				$existingToken->save();
			}else{
				$newToken = new CoreRestAuthentication();
				$newToken->token = $token;
				$newToken->generation_date = $now;
				$newToken->id_user = $user->idst;
				$newToken->user_level = $level;
				$newToken->expiry_date = $expire;
				$newToken->save();
			}

			$result = array('success'=>true, 'token'=>$token, 'expire_at' => $expire, 'idst' => $user->idst);
		}else{
			$result = array('success'=>false, 'message'=>'Error: invalid auth.');
		}

		return $result;
	}
} 