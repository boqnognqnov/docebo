<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2014 Docebo
 */

/**
 * Public API Component
 * @description Set of APIs to retrieve public info
 */
class PublicApiModule extends ApiModule {


	/**
	 * Returns the requested security level for the passed action
	 * @param $actionName The name of the action
	 * @return string The requested security level
	 */
	public function getSecurityLevel($actionName) {

		switch($actionName) {
			case 'getlmsinfo':
				return self::$SECURITY_NONE;
				break;
			default:
				return self::$SECURITY_STRONG;
		}
	}

	/**
	 * @summary Returns info of the lms to setup a remote site or app
     *
	 * @response logo [string, required] the url of the platform logo
	 */
	public function getlmsinfo() {

		// The theme setup for this part is skipped, so we have to do it
		// Set the correct path for the themes folder that is shared
		Yii::app()->themeManager->setBasePath(dirname(Yii::app()->getRequest()->getScriptFile()) . DIRECTORY_SEPARATOR . '../themes');

		// Using DIRECTORY_SEPARATOR for Paths is Ok, but NOT for URLs. (Windows)
		Yii::app()->themeManager->setBaseUrl(Docebo::getRootBaseUrl() . '/themes');

		$output = array(
			'logo' => Yii::app()->theme->getLogoUrl(),
		);
		return $output;
	}

}