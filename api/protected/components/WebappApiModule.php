<?php
/**
 * Class WebappApiModule
 * User API Component
 */
class WebappApiModule extends ApiModule {
	
	const REQUIRED_CLIENT_VERSION = '1.0.0';

	
	/**
	 * Returns the requested security level for the passed action
	 *
	 * @param  $actionName - Name of the action
	 * @return string - Requested security level
	 */
	public function getSecurityLevel($actionName) {
		switch ($actionName) {
			case 'connect':
			case 'forgot_password':
			case 'language':
			case 'check_version':
			case 'logout':
			case 'get_app_version':
				return self::$SECURITY_NONE;
			default:
				return self::$SECURITY_STRONG;
		}
	}

	/**
	 * Returns the allow oauth2 scopes for a certain action name
	 * @param $actionName
	 */
	public function getOauth2Scopes($actionName) {
		return array('webapp');
	}
	
	public function check_version(){
		$valid = 0;
		if(PluginManager::isPluginActive('WebApp')){
			$version = Yii::app()->request->getParam('clientVersion', '0');
			$valid = 1;
		/*	if($version != self::REQUIRED_CLIENT_VERSION){
				$valid = 0;
			}*/
		}
		return array(
			'valid' => $valid
		);
	}
	/**
	 * @return array
	 * Connect to the LMS and return array with configuration and
	 * private non sensitive information
	 */
	public function connect() {
		if (Docebo::isPlatformExpired()){
			throw new CHttpException(500, 'Cannot connect to lms server');
		}
		Yii::import('common.models.Flowplayer.FlowplayerMobile');
		$storageConfig = Yii::app()->params['s3Storages']['amazon'];
		$settings = MobileAppMultiDomain::getSettings();
		$bgColor = $settings['login_page_background'];
		$navColor = $settings['main_app_color'];
		$bgFontHex = $this->contrastColor2($bgColor);
		$navFontHex = $this->contrastColor2($navColor);
		$logoPath = $this->getLogo($settings['app_logo']);
		$settingsPrivateAssets = Settings::get('allow_private_assets');
		$privateAssets = $settingsPrivateAssets != null ? 1 : 0;
		$fs = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020);
		$fs->setKey("a.jpg");
		$s3StorageKeys = array(
			'videos' => str_replace('a.jpg', '', $fs->getKey()),
			'images' => str_replace('a.jpg', '', $fs->getOutputKey()),
		);
		$cloudfrontCookieUrl = Yii::app()->controller->widget('common.widgets.CloudfrontCookieBaker', array('onlyUrl' => true), TRUE);
		$output = array(
			'multidomain' => isset($settings['isMultidomain']) ? $settings['isMultidomain'] : false,
			'plugins' => array(
				'Coach7020App' => PluginManager::isPluginActive('Coach7020App'),
				'Share7020App' => PluginManager::isPluginActive('Share7020App'),
				'GooglessoApp' => PluginManager::isPluginActive('GooglessoApp') && Docebo::isCurrentLmsDomain(),
				'GoogleappsApp' => PluginManager::isPluginActive('GoogleappsApp'),
				'SimpleSamlApp' => PluginManager::isPluginActive('SimpleSamlApp'),
				'OktaApp' => PluginManager::isPluginActive('OktaApp')
			),
			'paths' => array(
				'amazon' => $s3StorageKeys,
				'embed_video_track' => Docebo::createLmsUrl('video/default/axTrack', array())
			),
			'configuration' => array(
				'flowplayer_license' => FlowplayerMobile::instance()->generateKey(),
				'https_redirect' => (!Yii::app()->request->isSecureConnection && Yii::app()->https->mustRedirect()),
				'cloudfrontCookie' => $cloudfrontCookieUrl,
				'privateAssetsAllowed' => $privateAssets,
				's3BucketName' => $storageConfig['bucket_name'],
				's3Region' => $storageConfig['region'],
				'logo_path' => $logoPath,
				'bg_color' => array(
					'hex' => $bgColor,
					'rgba' => $this->hexToRgba($bgColor),
					'bg_font' => array(
						'hex' => $bgFontHex,
						'rgba' => $this->hexToRgba($bgFontHex)
					)
				),
				'nav_color' => array(
					'hex' => $navColor,
					'rgba' => $this->hexToRgba($navColor),
					'nav_font' => array(
						'hex' => $navFontHex,
						'rgba' => $this->hexToRgba($navFontHex)
					)
				)
			)
		);
		return $output;
	}
	
	function logout(){
		unset(Yii::app()->request->cookies['docebo_session']);
	}

	/**
	 * This API calls directly to the 'lostPassword' func in coreUser model and
	 * always return true / success
	 * @return string
	 */
	public function forgot_password() {
		$payload = Yii::app()->request->getRawBody();
		$payloadDecoded = CJSON::decode($payload);
		$user = ($payloadDecoded['user'] != '' ? $payloadDecoded['user'] : $this->readParameter('user', false));
		CoreUser::model()->lostPassword($user);
		return array('success' => 'true');
	}

	/**
	 * @param $hex string ----  be a valid string HEX value
	 * @return array $rgba ---- of converted values from provided $hex
	 */
	private function hexToRgba($hex) {
		$rgba = array();
		$hex = ltrim($hex, '#');
		$rgba['red'] = (string) hexdec($hex[0] . $hex[1]);
		$rgba['green'] = (string) hexdec($hex[2] . $hex[3]);
		$rgba['blue'] = (string) hexdec($hex[4] . $hex[5]);
		$rgba['alpha'] = "1.0";
		return $rgba;
	}

	/**
	 * Get the mobile logo from Amazon S3.
	 * If the *logo* is empty take the default one from the LMS
	 *
	 * @param $logoName
	 * @return bool|mixed|string
	 * @throws CException
	 */
	private function getLogo($logoName) {
		$storage  = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_ASSETS);
		$logoPath = $storage->fileUrl($logoName);

		/* If there is invalid image from S3 load local LMS logo */
		if ( !$logoPath || !@getimagesize($logoPath)) {
			$logoPath   = __DIR__.'/webapp/images/docebo_logo.png'; // if cannot access AWS cloud logo return docebo logo
		}

		return $logoPath; // Url of the LOGO
	}

	/**
	 * Pass a HEX array color and get the desired font color
	 *
	 * @param $hex array of HEX colors
	 * @return string BLACK or WHITE font color depending on the passed HEX array
	 */
	private function contrastColor2($hex) {
		return (hexdec($hex) > 0xffffff / 2) ? '#000000' : '#ffffff';
	}

	/**
	 * @return array|string
	 * @throws CHttpException
	 */
	public function get_user_info() {
		if (empty($this->user))
			throw new CHttpException(201, 'Invalid user specification');
		else {
			$userInfo = CoreUser::getUserMobileInfo($this->user->idst);
		}

		return $userInfo;
	}

	/**
	 * Return translations for the language send over request if language doesn't exist fallbacks to default lms language
	 * @return array|string
	 */
	public function language() {
		$payload = Yii::app()->request->getRawBody();
		$payloadDecoded = CJSON::decode($payload);
		$lang = $payloadDecoded["language"] != "" ? $payloadDecoded["language"] : $this->readParameter('language');
		$defaultLang = Lang::getInstance()->getBrowserCodeByCode(Settings::get('default_language', 'english'));
		$dir = Yii::getPathOfAlias('common.messages.' . $lang);
		$suffix = DS . 'webapp.php';
		$filePath = realpath($dir) . $suffix;
		$filePath = file_exists($filePath) ? $filePath : Yii::getPathOfAlias('common.messages.' . $defaultLang) . $suffix;
		$translation = require_once($filePath);
		return $translation;
	}

	/**
	 * Pass a courseId and retrieve all information about the Learning Objects
	 * in this course for the current User and also the statuses
	 *
	 * This also UPDATE the user access status
	 *
	 * @id - Id of the course
	 * @throws CException
	 * return $response array with all LO for the current Course
	 */
	public function course() {
		$payloadDecoded = CJSON::decode(Yii::app()->request->getRawBody());
		$id             = $payloadDecoded['id'];
		$idUser         = ($this->user->idst ? $this->user->idst : '');
		$response       = array();
		if (!$id)
			return false;
		try {
			if ( isset($idUser)) { // Check user id
				$canEnter = $this->can_enter_course($id); // Is user able to enter the course
				if ( $canEnter['can'])  // If he can update the DB with user access
					LearningCourseuser::updateCourseUserAccess($idUser, $id);
				else throwException(new Exception('User can not enter course'));
			}
			$Course = WebappCourse::getCourseInfo($id); // array of materials
			$LoModel = new WebappLearningObject();
			$LoData = $LoModel->getLearningMaterials($id, $idUser);
			if (is_array($Course) && is_array($LoData)) {
				$response = array_merge( $LoData, $Course);
			}
		}
		catch (Exception $exc){
			return array('result' => false, 'errors' => $exc->getMessage());
		}
		return $response;
	}

	/**
	 * Return courses array based on Learning Plan Id and User Id
	 * Also return information about the Learning plan it self
	 * @return array|void
	 */
	public function get_courses_by_lp_id() {

		$payload         = Yii::app()->request->getRawBody();
		$payloadDecoded  = CJSON::decode($payload);
		$id_learningplan = $payloadDecoded['LpId'];
		$idUser          = ($this->user->idst ? $this->user->idst : '');

		if (!$id_learningplan || !$idUser)
			return;

		$command = Yii::app()->getDb()->createCommand()
				->select('lcc.id_path, lcp.path_name, lcp.path_descr, lc.idCourse, lc.name, lc.img_course, lcc.prerequisites, lcc.sequence, lc.name, lcu.status')
				->from(LearningCoursepathCourses::model()->tableName() . ' lcc')
				->leftJoin(LearningCoursepath::model()->tableName() . ' lcp', 'lcp.id_path = lcc.id_path')
				->leftJoin(LearningCourse::model()->tableName() . ' lc', 'lc.idCourse = lcc.id_item')
				->leftJoin(LearningCoursepathUser::model()->tableName() . ' lcpu', 'lcpu.idUser = "' . $idUser . '"')
				->leftJoin(LearningCourseuser::model()->tableName() . ' lcu', 'lcu.idUser = lcpu.idUser AND lcu.idCourse = lc.idCourse')
				->where('lcc.id_path = "' . $id_learningplan . '"')
				->group('lc.idCourse')
				->order('sequence');

		$response   = array();
		$percentage = LearningCoursepath::model()->getCoursepathPercentage($idUser, $id_learningplan);
		$result     = $command->queryAll();

		$response['learningPlan']['id_path'] = $result[0]['id_path'];
		$response['learningPlan']['path_name'] = $result[0]['path_name'];
		$response['learningPlan']['path_descr'] = strip_tags($result[0]['path_descr']);
		$response['learningPlan']['count'] = count($result);
		$response['learningPlan']['percentage'] = $percentage;
		foreach ($result as &$courses) {
			/* Set class for courses status */

			/* Check the prerequisites for the course. If any lock the course until they are all completed */
			if ( $courses['prerequisites']) {
				$prerequisites = explode(',', $courses['prerequisites']);
				$courses['locked'] = true;
				foreach ( $prerequisites as $id ) {
					$key = array_search($id, array_column($result, 'idCourse'));
					if (strpos($result[$key]['status'], 'icon-completed') === false) {
						$courses['locked'] = true;
						break;
					} else
						$courses['locked'] = false;
				}
			}
			/* Check does the current user can enter the Course */
			$can_enter = $this->can_enter_course($courses['idCourse']);
			$courses['locked'] = ( $can_enter['can'] == 1 ? false : true );

			$courses['status'] = LearningCourseuser::getStatusForMobileApp($courses['status']);
			/* Set img logo */
			if (empty($courses['img_course']))
				$courses['img_course'] = Docebo::getRootBaseUrl(true) . '/themes/spt/images/course/course_nologo.png';
			else {
				$img = trim($courses['img_course']);
				$courses['img_course'] = CoreAsset::url((int) $img);
			}
		}
		$response['learningPlanCourses'] = $result;

		return $response;
	}

	/**
	 * @param bool $courseId
	 * @return array|void  $canEnter['can'] = 1 if  user can enter the course. Empty if it is locked
	 */
	public function can_enter_course($id=false) {
		$payload        = Yii::app()->request->getRawBody();
		$payloadDecoded = CJSON::decode($payload);
		$id       = ($id != false ? $id : $payloadDecoded['courseId']);
		$idUser         = ($this->user->idst ? $this->user->idst : '');

		if (!$id || !$idUser)
			return;
		/* @var $enrollment LearningCourseuser */
		$enrollment = LearningCourseuser::model()->findByAttributes(array(
			'idCourse'  => $id,
			'idUser'    => $idUser
		));
		$canEnter = ($enrollment ? $enrollment->canEnterCourse() : array('can' => false));

		return $canEnter;
	}

	/**
	 * Implements AJAX data feed for channels dashboard
	 *
	 * @param $endpoint It can be "channels" or "channel_items" (or any other)
	 */
	public function data() {
		$payload = Yii::app()->request->getRawBody();
		$payloadDecoded = CJSON::decode($payload);
		$endpoint = $payloadDecoded['endpoint'];
		$params = $payloadDecoded['params'];
		$result = array("success" => true);

		try {
			$handler = RestData::factory($endpoint, $params);
			$returnValue = $handler->run();
			if(!is_array($returnValue))
				$returnValue = array("return" => $returnValue);

			$result = array_merge($returnValue, $result);
		} catch (Exception $e) {
			$result["success"] = false;
			$result["message"] = $e->getMessage();
		}

		return $result;
	}

	/**
	 * Return Learning object
	 * @param  $id - Learning object id
	 * @return array|void
	 */
	public function get_learning_object() {
		$payload = Yii::app()->request->getRawBody();
		$payloadDecoded = CJSON::decode($payload);

		$loId = $payloadDecoded['id'];
		$idUser = ($this->user->idst ? $this->user->idst : '');
		if (!$loId || !$idUser)
			return;
		$scormItemId = ($payloadDecoded['scormItemId'] != '' ? $payloadDecoded['scormItemId'] : '');

		return LearningOrganization::getLoInfo($loId, $idUser, $scormItemId);
	}
	
	/**
	 * Return Learning object
	 * @param  $id - Learning object id
	 * @return array|void
	 */
	public function play() {
		$payload        = Yii::app()->request->getRawBody();
		$payloadDecoded = CJSON::decode($payload);
		$allHeaders     = getallheaders();
		$bearer         = ($allHeaders['Authorization'] ? str_replace('Bearer ', '', $allHeaders['Authorization']) : null);

		Yii::app()->user->loginByAccessToken($bearer);
		$loId   = $payloadDecoded['id'];
		$authCode = ($payloadDecoded['auth_code'] ? $payloadDecoded['auth_code'] : false);
		$idUser = ($this->user->idst ? $this->user->idst : '');
		if (!$loId || !$idUser)
			return array('success' => false, 'err'  => 'There is error fetching Material ID or User ID');
		$scormItemId = ($payloadDecoded['scormItemId'] != '' ? $payloadDecoded['scormItemId'] : '');

		$launchUrl   = WebappLearningObject::getLaunchUrl($loId, $idUser, $scormItemId, $authCode);
		if ( $scormItemId )
			$launchUrl   = $launchUrl . '&auth_code=' . $authCode;
		$response = array(
			'url' => $launchUrl
		);
		if(is_array($launchUrl) && array_key_exists('isArray', $launchUrl)){
			unset($launchUrl['isArray']);
			$response = array(
				'slides' => $launchUrl['urls'],
				'url' => ''
			);
		}
		return $response; 
	}

	/**
	 * This API checks is there path_name='skip_webapp_walkthrough' for the current user
	 * If not it rights it in CoreSettingUser
	 */
	public function skip_walktrough() {
		$coreSettingUser = new CoreSettingUser();
		$coreSettingUser->value = 1;
		$coreSettingUser->id_user = $this->user->idst;
		if (!CoreSettingUser::model()->findByAttributes(array('path_name' => 'skip_walkthrough', 'id_user' => $this->user->idst))) {
			$coreSettingUser->path_name = 'skip_walkthrough';
			$coreSettingUser->save();
		}
		return array('success' => true);
	}

	/**
	 * Returns tags needed for publishing an asset
	 * 
	 * @return array
	 */
	public function asset_tags() {

		$command = Yii::app()->db->createCommand();
		$command->select("tagText");
		$command->from(App7020Tag::model()->tableName());
		$command->order("tagText");

		$tags = $command->queryColumn();


		return array('tags' => $tags, 'result' => true);
	}

	/**
	 * Returns channels according to logged user's visibility, 
	 * needed for publishing an asset
	 * 
	 * @return array
	 */
	public function asset_channels() {

		$channelsArray = App7020Channels::getUsersVisibleChannels(true, $this->user->idst, true);
		$channels = array();

		if ($channelsArray) {
			foreach ($channelsArray as $value) {
				$channelModel = App7020Channels::model()->findByPk($value['idChannel']);
				$channels[] = array(
					'idChannel' => $value['idChannel'],
					'channelName' => $channelModel->translation()
				);
			}
		}

		return array('channels' => $channels, 'result' => true);
	}

	/**
	 * @summary Get the uploaded app7020Assets which are not finished yet and checks their status.
	 *          If the asset is successfully converted, notify the caller that the asset is ready to be published
	 *          If something gone wrong, notify the caller that the asset is not successfully converted   
	 * @parameter assetIds [array, required] IDs of the assets with ststus 3 (sucessfully uploaded)
	 * @response array 
	 * 				positives - array of IDs of successfully converted assets, 
	 * 				negatives - array of IDs of assets which are not successfully converted
	 */
	public function get_upload() {

		$assetIds = $this->readParameter('assetIds', false);
		if (!is_array($assetIds))
			throw new CHttpException(209, 'Invalid array');
		$returnPositives = array();
		$returnNegatives = array();
		foreach ($assetIds as $idAsset) {
			$assetObject = App7020Assets::model()->findByPk($idAsset);

			if ($assetObject->id) {
				//If the asset is successfully converted
				if ($assetObject->conversion_status == App7020Assets::CONVERSION_STATUS_SNS_SUCCESS) {
					$returnPositives[] = $idAsset;
				}
				//If the asset is not converted successfully
				if ($assetObject->conversion_status == App7020Assets::CONVERSION_STATUS_SNS_WARNING || $assetObject->conversion_status == App7020Assets::CONVERSION_STATUS_SNS_ERROR) {
					$returnNegatives[] = $idAsset;
				}
			}
		}

		return array(
			'positives' => $returnPositives,
			'negatives' => $returnNegatives
		);
	}

	/**
	 * @summary Create DB record for uploaded asset 
	 *
	 * @parameter type [integer, required] Asset type ID uploaded: 1 - video, 2- image
	 * @parameter filename [string, required] Randomized file name, original extension
	 * @parameter originalFilename [string, required] Full original filename 
	 *            
	 *            @status 201 No filename specified
	 *            @status 202 No original filename specified
	 *            @status 256 Unknown content type
	 *            @status 206 Could not create the asset object
	 *            
	 * @response idAsset [integer] ID of the newly created asset in the DB
	 */
	public function new_upload() {
		Yii::import('app7020.protected.components.App7020VideoProceeder');
		Yii::import('app7020.protected.components.App7020ImageProceeder');
		$type = $this->readParameter('type', false);
		$filename = $this->readParameter('filename', false);
		$originalFilename = $this->readParameter('originalFilename', false);

		if (empty($filename)){
			$response = array(
				'errorMessage' => Yii::t('standard', 'No filename specified') ,
				'errorId' => 201, 
				'result' => false
			);
			
			return $response;
		}

		if (empty($originalFilename)){
			$response = array(
				'errorMessage' => Yii::t('standard', 'No original filename specified') ,
				'errorId' => 202, 
				'result' => false
			);
			
			return $response;
		}

		//This needs to be extended when the other type of assets needs to be uploaded
		if ($type != App7020Assets::CONTENT_TYPE_VIDEO && $type != App7020Assets::CONTENT_TYPE_IMAGE){
			$response = array(
				'errorMessage' => Yii::t('standard', 'Unknown content type') ,
				'errorId' => 256, 
				'result' => false
			);
			
			return $response;
		}

		//Create an asset and get the ID
		$assetObject = new App7020Assets();
		$assetObject->contentType = $type;
		$assetObject->conversion_status = App7020Assets::CONVERSION_STATUS_S3;
		$assetObject->userId = $this->user->idst;
		$assetObject->filename = $filename;
		$assetObject->originalFilename = $originalFilename;
		$assetObject->idSource = 2;
		$assetObject->viewCounter = 0;

		$assetObject->save(false);

		if (empty($assetObject->id)) {
			$response = array(
				'errorMessage' => Yii::t('standard', 'Could not create the asset object') ,
				'errorId' => 206, 
				'result' => false
			);
			
		} else {
			$assetObject->startProccessingAfterFileUploaded();
			$assetObject->save(false);
			$response = array(
				'idAsset' => $assetObject->id, 
				'result' => true
			);
		}

		return $response;
	}
	
	 /**
     * @summary Submits the App7020 asset and send it to peer review or approve it
     *
     * @parameter title [string, required] Title of the asset
     * @parameter description [string, required] Description of the content
     * @parameter channels [array, required] Array of selected channels IDs the asset to be assigned to - required if the asset is public
     * @parameter tags [string, optional] comma separated string of selected tags the asset to be assigned to
     * @parameter idAsset [integer, required] ID of the asset which needs to be submitted
	 * @parameter isPrivate [boolean, required] Boolean value which indicates if the asset is private or not
     *            
     *            
     *@response 
	  *		result [boolean, required] The result of the operation
	  *		idAsset [integer, optiona] ID of the submitted asset
	  *		assetStatus [integer, optional] If the status is 10, the asset is submitted for peer review. 
	  *										If the status is 20, the asset is directly published
	  *		errors [array, optional] If the result of the operation is FALSE - array with the errors
     */	
	public function update_upload() {
		
		Yii::import('app7020.protected.components.App7020VideoProceeder');
		Yii::import('app7020.protected.components.App7020ImageProceeder');
		$assetId = $this->readParameter('idAsset', false);
		$assetTitle = $this->readParameter('title', false);
		$assetDescription = $this->readParameter('description', false);
		$isPrivate = $this->readParameter('isPrivate', false);
		$channnelsCheckList = $this->readParameter('channels', array());
		$tagsToAssign = $this->readParameter('tags', false);
		$assetObject = App7020Assets::model()->findByPk($assetId);

		if (empty($assetId) || empty($assetObject)) {
			return	array(
				'result' => false,
				'errors' => array(Yii::t('app7020', 'The asset was not found!'))
			);
		}

		// If we have tags assigned, let's find new ones and create them
		$allTags = explode(",", $tagsToAssign);
		if(!empty($allTags)) {
			$existingTags = Yii::app()->db->createCommand()->select("tagText")->from("app7020_tag")
				->where("tagText IN ('".implode("','", $allTags)."')")->queryColumn();
			$newTags = array_diff($allTags, $existingTags);
			foreach($newTags as $newTag)
				App7020Tag::addNewTags($newTag);
		}

		$assetObject->setAttribute("tagsToAssign", $tagsToAssign);
		$assetObject->setAttribute("activeThumbnail", App7020ContentThumbs::model()->findByAttributes(array('idContent'=>$assetId, 'is_active'=>1))->id);
		$assetObject->setAttribute("title", $assetTitle);
		$assetObject->setAttribute("description", $assetDescription);
		$assetObject->setAttribute("is_private", !ShareApp7020Settings::isGlobalAllowPrivateAssets() ? App7020Assets::PRIVATE_STATUS_INIT : $isPrivate);
		$assetObject->setAttribute("relatedChannels", $channnelsCheckList);

		if ($assetObject->validate()) {
			//if validation is ok then save the asset
			try {
				if ($assetObject->conversion_status == App7020Assets::CONVERSION_STATUS_SNS_SUCCESS) {
					$assetObject->conversion_status = (int) $assetObject->is_private == App7020Assets::PRIVATE_STATUS_PRIVATE ? App7020Assets::CONVERSION_STATUS_APPROVED : App7020Assets::getFinalConversionStatus($channnelsCheckList);
					Yii::app()->event->raise('App7020NewContent', new DEvent($assetObject, array('contentId' => $assetObject->id)));
				}
				$assetObject->save();
				return array(
					'result' => true,
					'idAsset' => $assetObject->id,
					'assetStatus' => $assetObject->conversion_status
				);
			}
			//detect any problem with env
			catch (Exception $ex) {
				return array('result' => false, 'errors' => array(Yii::t('app7020', 'Something went wrong!')));
			}
		} else {
			return array('result' => false, 'errors' => App7020Helpers::convertErrorsToFlatArray($assetObject->getErrors()));
		}
	}
	
	/**
     * @summary Returns the URL of the asset's thumbnail
     *
     * @parameter idAsset [integer, required] ID of the asset
     */	
	public function asset_thumb(){
		$idAsset = $this->readParameter('idAsset', false);
		return array(
			'thumb' => App7020Assets::getAssetThumbnailUri($idAsset),
			'success' => true
		);
	}
	
	/**
     * @summary Deletes an asset
     *
     * @parameter idAsset [integer, required] ID of the asset
     */	
	public function delete_asset(){
		
		$idAsset = $this->readParameter('idAsset', false);
		$assetObject = App7020Assets::model()->findByPk($idAsset);

		
		if (!empty($assetObject) && $assetObject->userId == $this->user->idst) {
			CoreAsset::model()->deleteByPk($assetObject->idThumbnail);
			App7020Helpers::deleteCloudFile($assetObject);
			App7020Helpers::deleteLocalFile($assetObject);
			if($assetObject->delete()){
				return array(
					'result' => true,
					'idAsset' => $idAsset
				);
			} else {
				return array(
					'result' => false,
					'idAsset' => $idAsset,
					'error' => Yii::t('app7020', 'Unable to delete the requested asset')
				);
			}
		} else {
			return array(
				'result' => false,
				'idAsset' => $idAsset,
				'error' => Yii::t('app7020', 'You have no rights to delete the requested asset')
			);
		}
	}
	
	/**
     * @summary Get uploaded but not completed assets of the logged user
     *
     */	
	public function get_assets(){
		
		$returnArray = array();
		$assetsArray = App7020Assets::model()->findAllByAttributes(
				array('userId' => $this->user->idst, 'idSource' => 2), 
				'conversion_status = '.App7020Assets::CONVERSION_STATUS_S3.' OR conversion_status = '.App7020Assets::CONVERSION_STATUS_SNS_SUCCESS);

		if($assetsArray){
			foreach ($assetsArray as $value) {
				$returnArray['assets'][] = array(
					'idAsset' => $value->id,
					'thumbnail' => App7020Assets::getAssetThumbnailUri($value->id),
					'videoUrl' => $this->get_video_urls($value->id),
					'status' => $value->conversion_status
				);
			}
		}
		
		return $returnArray;
		
	}

	/**
	 * Returns paths to MP4 and HLS palylist in S3 storage
	 * @parameter idAsset [integer, required] ID of the asset
	 */
	public function get_video_urls($id = ''){
		$idAsset = empty($id) ? $this->readParameter('idAsset', false) : $id;
		if(!$idAsset){
			return array(
					'result' => false,
					'error' => Yii::t('app7020', 'Invalid data provided')
			);
		}

		if(!$assetObject = App7020Assets::model()->findByPk($idAsset)){
			return array(
					'result' => false,
					'error' => Yii::t('app7020', 'Invalid asset')
			);
		}

		if($assetObject->contentType <> App7020Assets::CONTENT_TYPE_VIDEO){
			return array(
					'result' => false,
					'error' => Yii::t('app7020', 'The requested asset is not a video')
			);
		}

		$storage = CS3Storage::getDomainStorage(CS3StorageSt::COLLECTION_7020);
		return CS3StorageSt::getPlayerSources($assetObject->filename, $storage);
	}

	/**
	 * @return bool
	 */
	public function track_user_lo_status(){
		$idUser = ($this->user->idst ? $this->user->idst : '');
		$idOrg   = $this->readParameter('idOrg', false);
		$objType = $this->readParameter('objType', false);

		$result = array();

		$user_track = LearningCommontrack::model()->findByAttributes(array('idUser' => $idUser, 'idReference' => $idOrg, 'objectType' => $objType));
		if ( $user_track && $user_track['status'] == 'completed') {
			$result['success'] = true;
			$result['status'] = $user_track['status'];
		}
		else {
			$result['success'] = false;
		}
		return $result;
	}
	/**
	 * @return array
	 */
	public function get_app_version(){
		$response = array();
		$storageConfig = Yii::app()->params['s3Storages']['amazon'];
		$response['manifest'] = "https://".$storageConfig['bucket_name'].".s3.amazonaws.com/manifest.json";//$fs->getOutputKey();
		$response['downloadPath'] = "https://".$storageConfig['bucket_name'].".s3.amazonaws.com/webapp.zip";
		return $response;
	}
	

	/**
	 * @return array array(object) structured as follows:
	 * [
	 *   'search' => searchWord,
	 *   'recentSearches' => previous searches for the current user (from DB),
	 *   'suggestions' => Actual suggestions from ES,
	 * ]
	 */
	public function search_suggestions(){
		$response = array();

		$search      = $this->readParameter('search', ''); //Search by this string
		$type        = $this->readParameter('type', false); // false == 'all'
		$from        = $this->readParameter('from', 0); // Start from 0
		$pageSize    = WebappGlobalSearch::maxSuggestionItems; // Size of the returned suggestions
		$minScore    = WebappGlobalSearch::minScore; // score for filtering hits in ES

		$suggestionClass = new SuggestionsParams();
		if ( $suggestionClass )
			$response = $suggestionClass->getResult($search, $type, $from, $pageSize, $minScore);

		return $response;
	}

	/**
	 * @return array
	 */
	public function search_results(){
		$response    = array(); // Hold the final API JSON response
		/* Get Logged in user */
		$currentUser = ($this->user->idst ? $this->user->idst : '');
		$search      = $this->readParameter('search', '');
		$type        = $this->readParameter('type', 'all');
		$from        = $this->readParameter('from', 0);
		$pageSize    = $this->readParameter('pageSize', WebappGlobalSearch::pageSize);
		$minScore    = WebappGlobalSearch::minScore;

		if ( $from === 0) CoreUser::updateRecentSearch($currentUser, $search); // Update user Recent Searches
		/* Important thing for LMS */
		$userCatalogs = array();
		if ( PluginManager::isPluginActive('CoursecatalogApp') ){
			$userCatalogs = LearningCatalogue::getUserCatalogs($currentUser);
		}
		$es = Yii::app()->es;
		$es->setUserCatalogs($userCatalogs);

		if ( !ResultParams::isTypeSupported($type)){
			return array(
				'result' => false,
				'error'  => 'The type: "'.$type.'" is not supported'
			);
		}
		$type    = ($type === 'all' ? $type = false : $type); // If 'All' $type = false

		$resultParams = new ResultParams();
		$results = $resultParams->getResult($search, $type, $from, $pageSize, $minScore);
		$attempts = 0;
		while(count($results) < WebappGlobalSearch::maxItems && $attempts < 3){
			$from = $from + WebappGlobalSearch::maxItems;
			$newResults = $resultParams->getResult($search, $type, $from, $pageSize, $minScore);
			$attempts++;
			if(count($newResults)){
				$results = array_merge($results, $newResults);
			}
		}
		$response['items'] = $results;
		$response['from'] = $from +  WebappGlobalSearch::maxItems;
		return $response;

	}

	/**
	 * @return array
	 */
	public function lo_download_status(){
		$idCourse           = $this->readParameter('courseId', 0);
		$webappObject       = new WebappLearningObject();
		$learningMaterials  = $webappObject->getLearningMaterials($idCourse, $this->user->idst);
		$webappStorage      = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_WEBAPP);
		$scheduler = Yii::app()->scheduler;

		if ( is_array($learningMaterials)){
			foreach ($learningMaterials['lo_metadata'] as $key => $material){
				$title = preg_replace('/\s+/', '_', $material[$key]['title']);
				$resourceKey = $title.'_'.$material[$key]['id_resource'].'.zip';
				if(!$webappStorage->fileExists($resourceKey)){
					$job = $scheduler->createImmediateJob(WebappArchiveCourses::JOB_NAME, WebappArchiveCourses::id(), array('courseId' => $idCourse, 'idUser' => $this->user->idst));
					return array("scheduler_run" => true, 'job_hash' => $job->hash_id);
				}
			}
		}
		return array('success' => false, 'scheduler_run' => false);
	}

	/**
	 * @return array
	 */
	public function materials_size_data(){
		$idCourse       = $this->readParameter('courseId', 0);
		$jobHashId      = $this->readParameter('jobHashId', false);
		$webappObject   = new WebappLearningObject();
		$learningMaterials = $webappObject->getLearningMaterials($idCourse, $this->user->idst);
		$webappStorage  = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_WEBAPP);
		$total          = 0;
		$loList         = array();
		$allDone        = true;

		if ( is_array($learningMaterials)){
			foreach ($learningMaterials['lo_metadata'] as $key => $material){
				$title = preg_replace('/\s+/', '_', $material['title']);
				$resourceKey = $title.'_'.$material['id_resource'].'.zip';

					if($webappStorage->fileExists($resourceKey)){

						$fileSize = $webappStorage->fileSize($resourceKey);

						$total = $total+$fileSize;
						$loList[$material['id_resource']]['size'] = $fileSize;
						$loList[$material['id_resource']]['path'] = $webappStorage->fileUrl($resourceKey);
					}else{
						$loList[$material['id_resource']]['size'] = 0;
						$loList[$material['id_resource']]['path'] = 'Unknown';
					}
			}
			if($jobHashId != false){
				$job = CoreJob::model()->findByAttributes(array('hash_id' => $jobHashId));
				if(is_null($job)){
					$allDone = true;
				}else{
					$allDone = false;
				}
			}else{
				$allDone = true;
			}
		}
		return array(
			'total' => $total,
			'loList' => $loList,
			'schedular_done' => $allDone
		);
	}
	/**
	 *
	 */
	public function trackMaterial(){
		$id          = $this->readParameter('id', 0);
		$idUser      = ($this->user->idst ? $this->user->idst : '');
		$params      = $this->readParameter('params', array());
		if($id == 0){
			$id = $this->readParameter('id_reference', 0);
			if($id != 0){
				$params['status'] = $this->readParameter('status');
				$params['bookmark'] = $this->readParameter('bookmark');
			}
		}
		$webappMaterial = new WebappLearningObject();
		$trackResponse  = $webappMaterial->setTrackMaterial($id, $idUser, $params);
	}
	
	public function sco_track_sync(){
		return WebappLearningObject::syncScormTracking();
	}
	
	public function get_lo_by_course_id() {
		$payload = Yii::app()->request->getRawBody();
		$payloadDecoded = CJSON::decode($payload);
		$idCourse = $payloadDecoded['courseId'];
		if (!$idCourse)
			return;
		$params = array('userInfo' => true, 'scormChapters' => true, 'showHiddenObjects' => false, 'downloadOnScormFoldersOnly' => false, 'aiccChapters' => true);
		$objectsTree = LearningObjectsManager::getCourseLearningObjectsTree($idCourse, $params);

		$items = LearningObjectsManager::flattenLearningObjectsTree($objectsTree, array('getFolders' => true));

		$countItems = 0;
		foreach ($items as $item) {
			switch ($item['status']) {
				case LearningCommontrack::STATUS_AB_INITIO:
					$item['status_class'] = "icon-play-circle gray";
					break;
				case LearningCommontrack::STATUS_ATTEMPTED:
					$item['status_class'] = "icon-play-circle yellow";
					break;
				case LearningCommontrack::STATUS_COMPLETED:
					$item['status_class'] = "icon-completed green";
					break;
				case LearningCommontrack::STATUS_FAILED:
					$item['status_class'] = "failed";
					break;
				case LearningCommontrack::STATUS_PASSED:
					$item['status_class'] = "icon-completed green";
					break;
				default:
					$item['status_class'] = "icon-play-circle gray";
			}
			switch ($item['self_prerequisite']){
				case LearningOrganization::SELF_PREREQ_ONCE:
					// If LO is only allowed to be played once and is already played

					$criteria = new CDbCriteria();
					$criteria->addCondition('idReference=:idReference');
					$criteria->params[':idReference'] = $item['idOrganization'];
					$criteria->addCondition('idUser=:idUser');
					$criteria->params[':idUser'] = Yii::app()->user->id;

					// Make sure the status is different from "not started" so we can assume
					// that the user actually started the LO
					$criteria->addNotInCondition('status', array(LearningCommontrack::STATUS_AB_INITIO));
					if(LearningCommontrack::model()->count($criteria)){
						// The user has played the LO already, so lock it now
						$item['locked'] = TRUE;
					}
					break;
				case LearningOrganization::SELF_PREREQ_INCOMPLETE:
					// If the LO can be played infinitely until completed

					$_condition = array(
						'idReference' => $item['idOrganization'],
						'idUser' => Yii::app()->user->id,
						'status' => LearningCommontrack::STATUS_COMPLETED
					);
					if(LearningCommontrack::model()->countByAttributes($_condition)){
						$item['locked'] = TRUE;
					}
					break;
				default: // Don't lock the LO. It can be played infinitely
			}
			if ($item['isFolder'] != true)
				$countItems ++;

			$response['items'][] = $item;
		}

		$response['countObjects'] = $countItems;

		$cmnd = Yii::app()->db->createCommand();
		$cmnd->select('idCourse, name, description, lang_code, status, mediumTime');
		$cmnd->from(LearningCourse::model()->tableName() . ' lc');
		$cmnd->where('idCourse = :idCourse', array(':idCourse' => $idCourse));

		$courseInfo = $cmnd->queryRow();

		$idUser = ($this->user->idst ? $this->user->idst : '');
		if ($idUser && $idUser != '') {
			$enrollment = LearningCourseuser::model()->findByAttributes(array(
					'idCourse'  => $idCourse,
					'idUser' 	=> $idUser
			));
			if ($enrollment)
				$canEnter = $enrollment->canEnterCourse();
			else
				$canEnter = array('can' => false);

			if ( $canEnter['can'])
				LearningCourseuser::updateCourseUserAccess($idUser, $idCourse);

		}

		if (is_array($courseInfo)) {
			$courseInfo['description'] = strip_tags($courseInfo['description']);
			$response['courseInfo'] = $courseInfo;
		}

		return $response;
	}
}
