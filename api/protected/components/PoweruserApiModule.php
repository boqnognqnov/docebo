<?php

/**
 * Poweruser API Component
 * @description Set of APIs to manage power users
 */
class PoweruserApiModule extends ApiModule {
	
	const ITEM_TYPE_ID_USER 				= 'id_user';
	const ITEM_TYPE_USERID 					= 'userid';
	const ITEM_TYPE_ID_GROUP 				= 'id_group';
	const ITEM_TYPE_GROUP_NAME 				= 'group_name';
	const ITEM_TYPE_ID_ORG 					= 'id_org';
	const ITEM_TYPE_BRANCH_NAME 			= 'branch_name';
	
	const COURSES_TYPE_ID_COURSE 			= 'id_course';
	const COURSES_TYPE_COURSE_CODE 			= 'course_code';
	const COURSES_TYPE_ID_LEARNINGPLAN 		= 'id_learningplan';
	const COURSES_TYPE_LEARNINGPLAN_CODE 	= 'learningplan_code';
	const COURSES_TYPE_ID_CATEGORY 			= 'id_category';
	
	/**
	 * Returns the requested security level for the passed action
	 * @param $actionName The name of the action
	 * @return string The requested security level
	 */
	public function getSecurityLevel($actionName)
	{
		switch ($actionName) {
			default:
				return self::$SECURITY_STRONG;
		}
	}

	/**
	 * @summary Makes a user a Power User with an assigned profile & orgchart nodes
	 *
	 * @parameter id_user [integer, required] ID of an existing non PU user account
	 * @parameter profile_name [string, optional] power user profile name to be assigned
	 * @parameter orgchart [string, optional] comma separated list of org chart node ids
	 *
	 * @status 401 Power User app is not enabled
	 * @status 402 Missing or invalid required param "id_user"
	 * @status 403 User is already a power user
	 * @status 404 Failed to assign the org nodes to the user
	 * @status 405 PU Profile not found
	 *
	 * @throws CHttpException
	 */
	public function add() {
		$idUser = $this->readParameter('id_user');
		$profileName = $this->readParameter('profile_name');
		$orgchart = explode(',', $this->readParameter('orgchart'));
		$user = CoreUser::model()->findByPk($idUser);

		if (!PluginManager::isPluginActive('PowerUserApp'))
			throw new CHttpException(401, 'Power User app is not enabled');

		if (!$user)
			throw new CHttpException(402, 'Missing or invalid required param "id_user"');

		// model for admin level
		$groupMember = CoreGroupMembers::model()->findAdminLevel($user->idst);
		if ($groupMember === null) {
			$groupMember = new CoreGroupMembers();
			$groupMember->idstMember = $user->idst;
		}
		else {
			// check if the user is already a power user
			if ($groupMember->group->groupid !== Yii::app()->user->level_user)
				throw new CHttpException(403, 'User is already a power user');
		}

		$transaction = Yii::app()->db->beginTransaction();

		try {
			/**
			 * Set user as power user
			 */
			$puLevel = CoreGroup::model()->find("groupid = :level_admin", array(':level_admin'=>Yii::app()->user->level_admin));
			$groupMember->scenario = 'update';
			$groupMember->idst = $puLevel->idst;
			$groupMember->save();
			// Sync the User Role with the Hydra RBAC
			HydraRBAC::manageUserToRole(array( "user" => $user->idst, "group" => $puLevel->idst ));

			/**
			 * Assign the PU profile
			 */
			if ($profileName) {
				$puProfile = CoreGroup::model()->findByAttributes(array('groupid' => '/framework/adminrules/' . $profileName));

				if(!$puProfile){
					throw new CHttpException(405, 'PU Profile not found');
				}

				$profileMemberModel = CoreGroupMembers::model()->getPowerUserProfile($user->idst);
				$profileMemberModel->idst = $puProfile->idst;
				$profileMemberModel->save();

				// Sync the User Role with the Hydra RBAC
				HydraRBAC::manageUserToRole(array( "user" => $user->idst, "group" => $puProfile->idst ), true);
			}

			/**
			 * Assign org nodes
			 */
			if (!empty($orgchart)) {
				// make sure that the ids match the ones in the db
				$criteria = new CDbCriteria();
				$criteria->addInCondition('idOrg', $orgchart);
				$orgChartNodes = CoreOrgChartTree::model()->findAll($criteria);
				if (!empty($orgChartNodes)) {
					$orgChartNodes = CHtml::listData($orgChartNodes, 'idst_oc', 'idst_oc');
					$result = CoreAdminTree::updatePowerUserSelection($user->idst, $orgChartNodes);
					if (!$result)
						throw new CHttpException(404, "Failed to assign the org nodes to the user");
				}
			}

			$transaction->commit();
		}
		catch (Exception $ex) {
			$transaction->rollback();
			throw $ex;
		}
	}
	
	/**
	 * @summary Reverts the passed PU account to the "regular user" level
	 * 
	 * @notes This API action does not delete the PU account. It simply changes his level and removes the PU Profile assigned to him.
	 *
	 * @parameter id_user [integer, required] ID of an existing PU account
	 *
	 * @status 401 Power User app is not enabled
	 * @status 402 Missing or invalid required param "id_user"
	 * @status 403 User is not a Power User
	 *
	 * @throws CHttpException
	 */
	public function delete(){
		$idUser = $this->readParameter('id_user');
		
		$user = CoreUser::model()->findByPk($idUser);
		
		if(!PluginManager::isPluginActive('PowerUserApp')){
			throw new CHttpException(401, 'Power User app is not enabled');
		}
		
		if(!$user){
			throw new CHttpException(402, 'Missing or invalid required param "id_user"');
		}			
		
		
		$groupMemeber = CoreGroupMembers::model()->findAdminLevel($user->idst);
				
		
		if($groupMemeber->group->groupid !== Yii::app()->user->level_admin){
			throw new CHttpException(403, 'User is not a Power User');
		}
		
		$groupUsers = CoreGroup::model()->find('groupid = :reg_user', array(
				':reg_user' => Yii::app()->user->level_user
		));

		$groupMemeber->idst = $groupUsers->idst;
		if($groupMemeber->save()){
			$criteria = new CDbCriteria();
			$criteria->with = array('group');
			$criteria->compare('group.groupid', '/framework/adminrules/', true);
			$criteria->compare('t.idstMember', $user->idst);
			
			$models = CoreGroupMembers::model()->findAll($criteria);
			
			foreach ($models as $model){
				$model->delete();
			}			
		}

		// Sync the User Role with the Hydra RBAC
		HydraRBAC::manageUserToRole(array( "user" => $user->idst, "group" => 6 ));
				
	}
	
	/**
	 * @summary Updates certain configuration settings for the passed PU	 	
	 *
	 * @parameter id_user [integer, required] ID of an existing PU account
	 * 
	 * @parameter profile_name [string, optional] the new PU Profile to assign to the PU account. This overwrites the old  PU Profile (if any)
	 * 
	 * @parameter reset_profile_if_empty [boolean, optional] If this input param is passed as "true", an empty value in the  "profile_name" param is interpreted as a reset command. Default is "false", i.e. do not change the PU Profile if "profile_name" is empty.
	 *
	 * @status 401 Power User app is not enabled
	 * @status 402 Missing or invalid required param "id_user"
	 * @status 403 User is not a Power User
	 * @status 404 PU Profile not found
	 *
	 * @throws CHttpException 
	 */
	public function update(){
		$idUser = $this->readParameter('id_user');
		$profileName = $this->readParameter('profile_name');
		$resetIfEmpty = $this->readParameter('reset_profile_if_empty', false);
		
		$user = CoreUser::model()->findByPk($idUser);
		
		if(!PluginManager::isPluginActive('PowerUserApp')){
			throw new CHttpException(401, 'Power User app is not enabled');
		}
		
		if(!$user){
			throw new CHttpException(402, 'Missing or invalid required param "id_user"');
		}
		
		
		$groupMemeber = CoreGroupMembers::model()->findAdminLevel($user->idst);
		
		
		if($groupMemeber->group->groupid !== Yii::app()->user->level_admin){
			throw new CHttpException(403, 'User is not a Power User');
		}
		
		if(!empty($profileName)){
			$puProfile = CoreGroup::model()->findByAttributes(array('groupid' => '/framework/adminrules/' . $profileName));
			
			if(!$puProfile){
				throw new CHttpException(404, 'PU Profile not found');
			}
			
			$profileMember = CoreGroupMembers::model()->getPowerUserProfile($user->idst);
			$profileMember->idst = $puProfile->idst;
			$profileMember->save();

			// Sync the User Role with the Hydra RBAC
			HydraRBAC::manageUserToRole(array( "user" => $user->idst, "group" => $puProfile->idst ), true);
		}
		
		if($resetIfEmpty){
			if(empty($profileName)){
				$criteria = new CDbCriteria();
				$criteria->with = array('group');
				$criteria->compare('group.groupid', '/framework/adminrules/', true);
				$criteria->compare('t.idstMember', $user->idst);
				
				$model = CoreGroupMembers::model()->find($criteria);
				
				$model->delete();

				// Sync the User Role with the Hydra RBAC
				HydraRBAC::manageUserToRole(array( "user" => $user->idst, "group" => 6 ));
			}
		}
	}
	
	/**
	 * @summary Returns the list of all PU profiles.
	 * 
	 * @response profiles [array, required] Array of profile records.
	 * 	   @item id [integer, required] The internal ID for the profile
	 *     @item name [string, required] The human readable name for the profile
	 *
	 * @status 401 Power User app is not enabled
	 *
	 * @throws CHttpException 
	 */
	public function listProfiles(){
		
		if(!PluginManager::isPluginActive('PowerUserApp')){
			throw new CHttpException(401, 'Power User app is not enabled');
		}
		
		$criteria = new CDbCriteria();		
		$criteria->compare('t.groupid', '/framework/adminrules/', true);
		
		$model = CoreGroup::model()->findAll($criteria);
		
		$output = array();
		foreach ($model as $puProfile){
			$output[] = array(
					'id' => $puProfile->idst,
					'name' => $puProfile->relativeProfileId()
			);
		}
		
		return array('profiles' => $output);
	}	
	
	/**
	 * @summary Adds the passed item (user, branch, group) to the set of users managed by the current PU.
	 *
	 * @notes When using the id_orgchart and branch_name values, the API will assume that the branch with all its descendants is to be assigned. Besides, the branch_name is to be intended in the default LMS language. At least one of the id_user or userid must be given! 
	 *
	 * @parameter id_user [integer, optional] Internal Docebo ID of an existing PU
	 * @parameter userid [string, optional] Login name of an existing PU account
	 * @parameter item_type [string, required] Can be one of the following values (id_user, userid, id_group, group_name, id_org, branch_name)
	 * @parameter item_value [string, required] Item value (e.g. the ID of the user, the name of the branch, etc.)
	 *
	 * @status 401 Power User app is not enabled
	 * @status 402 Missing or invalid required params
	 * @status 403 User is not a Power User	 
	 * @status 404 Missing id_user or userid
	 * @status 405 Invalid item_value for item_type
	 * @status 406 Failed assigning to PU
	 *
	 * @throws CHttpException 
	 */
	public function assignUser(){
		
		$idUser = $this->readParameter('id_user');
		$userid = $this->readParameter('userid');
		$itemType = $this->readParameter('item_type');
		$itemValue = $this->readParameter('item_value');
		
		if(!PluginManager::isPluginActive('PowerUserApp')){
			throw new CHttpException(401, 'Power User app is not enabled');
		}
		
		if(empty($idUser) && empty($userid)){
			throw new CHttpException(404,  'Missing id_user or userid');
		}
		
		if(empty($itemType) || !isset($itemValue) || $itemValue === ''){
			throw new CHttpException(402,  'Missing or invalid required params');
		}
		
		$puProfile = null;
		$user = null;
		
		if(!empty($idUser)){
			$user = CoreUser::model()->findByPk($idUser);
		} else if(!empty($userid)){
			$user = CoreUser::model()->find('userid LIKE :user', array(
					':user' => '/' . $userid
			));
		}
		
		$criteria = new CDbCriteria();
		$criteria->with = array('group');
		$criteria->compare('group.groupid', '/framework/level/admin', true);
		$criteria->compare('t.idstMember', $user->idst);
		
		$puProfile = CoreGroupMembers::model()->find($criteria);
		
		if(!$puProfile){
			throw new CHttpException(403, 'User is not a Power User');
		}
				
		$listToAdd = $this->getSelectedIds($itemType, $itemValue);
				
		$result = CoreAdminTree::updatePowerUserSelection($user->idst, $listToAdd);
		
		CoreOrgChartTree::model()->flushCache();
		
		if(!$result){
			throw new CHttpException(406, 'Failed assigning to PU');
		}
	}	
	
	/**
	 * @summary Removes the passed items (user, branch, group) from the set of users managed by currnet PU.	 	
	 *
	 * @parameter id_user [integer, optional] Internal Docebo ID of an existing PU
	 * @parameter userid [string, optional] Login name of an existing PU account
	 * @parameter item_type [string, required] Can be one of the following values (id_user, userid, id_group, group_name, id_org, branch_name)
	 * @parameter item_value [string, required] Item value (e.g. the ID of the user, the name of the branch, etc.)
	 *
	 * @status 401 Power User app is not enabled
	 * @status 402 Missing or invalid required params
	 * @status 403 User is not a Power User	 
	 * @status 404 Missing id_user or userid
	 * @status 405 Invalid item_value for item_type	 
	 *
	 * @throws CHttpException 
	 */
	public function unassignUsers(){
		$idUser = $this->readParameter('id_user');
		$userid = $this->readParameter('userid');
		$itemType = $this->readParameter('item_type');
		$itemValue = $this->readParameter('item_value');
		
		if(!PluginManager::isPluginActive('PowerUserApp')){
			throw new CHttpException(401, 'Power User app is not enabled');
		}
		
		if(empty($idUser) && empty($userid)){
			throw new CHttpException(404,  'Missing id_user or userid');
		}

        if(empty($itemType) || !isset($itemValue) || $itemValue === ''){
			throw new CHttpException(402,  'Missing or invalid required params');
		}
		
		$puProfile = null;
		$user = null;
		
		if(!empty($idUser)){
			$user = CoreUser::model()->findByPk($idUser);
		} else if(!empty($userid)){
			$user = CoreUser::model()->find('userid LIKE :user', array(
					':user' => '/' . $userid
			));
		}
		
		$criteria = new CDbCriteria();
		$criteria->with = array('group');
		$criteria->compare('group.groupid', '/framework/level/admin', true);
		$criteria->compare('t.idstMember', $user->idst);
		
		$puProfile = CoreGroupMembers::model()->find($criteria);
		
		if(!$puProfile){
			throw new CHttpException(403, 'User is not a Power User');
		}
		
		$listToRemove = $this->getSelectedIds($itemType, $itemValue);
		
		$condition = new CDbCriteria();
		$condition->addInCondition('idst', $listToRemove);
		$condition->addCondition('idstAdmin=:pu');
		$condition->params[':pu'] = $user->idst;
		
		CoreOrgChartTree::model()->flushCache();
		
		// Actually unassing the users/groups from the power user
		CoreAdminTree::model()->deleteAll($condition);
		//$this->updateUsersInReportFilters((int)$currentPowerUserId,$idsArr);
		// Recalculate cached tables for this power user
		CoreUserPU::model()->updatePowerUserSelection($user->idst);			
	}

	/**
	 * @summary Assigned the visibility of a certain items (courses, learning plans, categories) from the set of items managed by the current PU.
	 *
	 * @notes When passing id_category in items array, that means the category and its descendants will be removed
	 *
	 * @parameter id_user [integer, required] ID of an existing PU account
	 *
	 * @parameter items [object, required] Array of one or more items to assign from the current PU.
	 * 		@item id_course [array(integer), optional] Array of one or more course ids to assign
	 * 		@item course_code [array(string), optional] Array of one or more course codes to assign
	 * 		@item id_learningplan [array(integer), optional] Array of one or more learning plan ids to assign
	 * 		@item learningplan_code [array(string), optional] Array of one or more unique learning plan codes to assign
	 * 		@item id_category [array(integer), optional] Array of one or more categories ids to assign
	 * @end
	 *
	 * @response errors [array, required] Array of errors (if any) occured during the assigment process.
	 *
	 * @status 401 Power User app is not enabled
	 * @status 402 Missing or invalid required params
	 * @status 403 User is not a Power User
	 * @status 404 Invalid item
	 * @status 405 Curricula App is not enabled	 	
	 *
	 * @throws CHttpException 
	 */
	public function assignCourses(){		

		$idUser = $this->readParameter('id_user');
		$items = $this->readParameter('items');
		$user = null;
		$errors = array();
		
		if(!PluginManager::isPluginActive('PowerUserApp')){
			throw new CHttpException(401, 'Power User app is not enabled');
		}
		
		if(empty($idUser) || empty($items)){
			throw new CHttpException(402,  'Missing or invalid required params');
		}			
		
		if(!empty($idUser)){
			$user = CoreUser::model()->findByPk($idUser);
		}
		
		$criteria = new CDbCriteria();
		$criteria->with = array('group');
		$criteria->compare('group.groupid', '/framework/level/admin', true);
		$criteria->compare('t.idstMember', $user->idst);
		
		$puProfile = CoreGroupMembers::model()->find($criteria);
		
		if(!$puProfile){
			throw new CHttpException(403, 'User is not a Power User');
		}
		
		$result = $this->resolveCoursesForAssign($items);

		$coursesIDs = $result['coursesIds'];
		$lpIDs = $result['lpIDs'];
		$categoriesIDs = $result['categoriesIDs'];

		$transaction = Yii::app()->db->beginTransaction();
		try{
			foreach ($coursesIDs as $courseId) {

				$alreadyIn = CoreUserPuCourse::model()->find('puser_id = :user AND course_id = :course', array(
					':user' => $user->idst,
					':course' => $courseId
				));

				if(!$alreadyIn){
					$model = new CoreUserPuCourse();
					$model->puser_id = $user->idst;
					$model->course_id = $courseId;
					$model->save();
				}

				// TODO: CoreAdminCourse handling will be upgraded in the future

				$alreadyIn = CoreAdminCourse::model()->find('idst_user = :user AND id_entry = :course', array(
					':user' => $user->idst,
					':course' => $courseId
				));
				if(!$alreadyIn){
					$adminCourseModel = new CoreAdminCourse();
					$adminCourseModel->idst_user = $user->idst;
					$adminCourseModel->type_of_entry = CoreAdminCourse::TYPE_COURSE;
					$adminCourseModel->id_entry = $courseId;
					$adminCourseModel->save();
				}
			}
			$transaction->commit();
		} catch(CDbException $e){
			$transaction->rollback();
			$errors[] = $e->getMessage();
		}

		//Assigning Learning Plans
		if($transaction->getActive() === false){
			$transaction = Yii::app()->db->beginTransaction();
		}
		try{
			foreach ($lpIDs as $coursepathId) {

				$alreadyIn = CoreUserPuCoursepath::model()->find('puser_id = :user AND path_id = :path', array(
					':user' => $user->idst,
					':path' => $coursepathId
				));

				if(!$alreadyIn){
					$model = new CoreUserPuCoursepath();
					$model->puser_id = $user->idst;
					$model->path_id = $coursepathId;
					$model->save();
				}

				// TODO: CoreAdminCourse handling will be upgraded in the future
				$alreadyIn = CoreAdminCourse::model()->find('idst_user = :user AND id_entry = :course', array(
					':user' => $user->idst,
					':course' => $coursepathId
				));

				if(!$alreadyIn){
					$adminCourseModel = new CoreAdminCourse();
					$adminCourseModel->idst_user = $user->idst;
					$adminCourseModel->type_of_entry = CoreAdminCourse::TYPE_COURSEPATH;
					$adminCourseModel->id_entry = $coursepathId;
					$adminCourseModel->save();
				}
			}
			$transaction->commit();
		} catch(CDbException $e){
			$transaction->rollback();
			$errors[] = $e->getMessage();
		}

		//Assigning Categories
		if($transaction->getActive() === false){
			$transaction = Yii::app()->db->beginTransaction();
		}
		try{
			// First clear old category associations
			$deleteCriteria = new CDbCriteria();
			$deleteCriteria->compare('idst_user', $user->idst);
			$deleteCriteria->compare('type_of_entry', array(CoreAdminCourse::TYPE_CATEGORY, CoreAdminCourse::TYPE_CATEGORY_DESC));
			CoreAdminCourse::model()->deleteAll($deleteCriteria);

			foreach($categoriesIDs as $catSelection){
				$alreadyIn = CoreAdminCourse::model()->find('idst_user = :user AND id_entry = :course', array(
					':user' => $user->idst,
					':course' => $courseId
				));
				if(!$alreadyIn){
					$adminCourseModel = new CoreAdminCourse();
					$adminCourseModel->idst_user = $user->idst;
					$adminCourseModel->type_of_entry = CoreAdminCourse::TYPE_CATEGORY_DESC;
					$adminCourseModel->id_entry = $catSelection;
					$adminCourseModel->save();
				}
			}
			$transaction->commit();
		} catch(CDbException $e){
			$transaction->rollback();
			$errors[] = $e->getMessage();
		}

		return array(
			'errors' => $errors
		);
	}
	
	/**
	 * @summary Removes the visibility of certain items (courses, learning plans, categories) form the set of items managed by the current PU
	 * 
	 * @notes When passing id_category in items array, that means the category and its descendants will be removed	 	
	 *
	 * @parameter id_user [integer, required] ID of an existing PU account
	 * 
	 * @parameter items [array, required] Array of one or more items to unassign from the current PU.
	 * 		@item id_course [integer, optional]: ID of the course or array if course ids
	 * 		@item course_code [string, optional]: unique code of the course to assign or array of course codes
	 * 		@item id_learningplan [integer, optional]: ID of the learning plan to assign or array of learning plan ids
	 * 		@item learningplan_code [string, optional]: unique code of learning plan to assign or array of learning plan codes
	 * 		@item id_category [integer, optional]: ID of the category to assign or array of categories ids
	 * 
	 * @response errors [array, required] Array of errors (if any) occured during the unassigment process.
	 *
	 * @status 401 Power User app is not enabled
	 * @status 402 Missing or invalid required params
	 * @status 403 User is not a Power User
	 * @status 404 Invalid item
	 * @status 405 Curricula App is not enabled	 	
	 *
	 * @throws CHttpException 
	 */
	public function unassignCourses(){

		$idUser = $this->readParameter('id_user');
		$items = $this->readParameter('items');
		$user = null;
		$errors = array();
		
		if(!PluginManager::isPluginActive('PowerUserApp')){
			throw new CHttpException(401, 'Power User app is not enabled');
		}
		
		if(empty($idUser) || empty($items)){
			throw new CHttpException(402,  'Missing or invalid required params');
		}
		
		if(!empty($idUser)){
			$user = CoreUser::model()->findByPk($idUser);
		}
		
		$criteria = new CDbCriteria();
		$criteria->with = array('group');
		$criteria->compare('group.groupid', '/framework/level/admin', true);
		$criteria->compare('t.idstMember', $user->idst);
		
		$puProfile = CoreGroupMembers::model()->find($criteria);
		
		if(!$puProfile){
			throw new CHttpException(403, 'User is not a Power User');
		}
		
		$result = $this->resolveCoursesForAssign($items);
		
		$coursesIDs = $result['coursesIds'];
		$lpIDs = $result['lpIDs'];
		$categoriesIDs = $result['categoriesIDs'];
		
		$transaction = Yii::app()->db->beginTransaction();
		
		try{		
			//Unassigning Courses
			foreach ($coursesIDs as $courseId) {
				CoreUserPuCourse::model()->deleteAllByAttributes(array('course_id' => $courseId, 'puser_id' => $user->idst));
				//TO DO: CoreAdminCourse handling will be upgraded in the future
				CoreAdminCourse::model()->deleteAllByAttributes(array('id_entry' => $courseId, 'type_of_entry' => CoreAdminCourse::TYPE_COURSE, 'idst_user' => $user->idst));
			}
				
			//Unassigning Learning Plans
			foreach ($lpIDs as $coursepathId) {	
				CoreUserPuCoursepath::model()->deleteAllByAttributes(array('path_id' => $coursepathId, 'puser_id' => $user->idst));
                $this->updateLearningPlansInReportFilters($user->idst,$coursepathId);
				//TO DO: CoreAdminCourse handling will be upgraded in the future
				CoreAdminCourse::model()->deleteAllByAttributes(array('id_entry' => $coursepathId, 'type_of_entry' => CoreAdminCourse::TYPE_COURSEPATH, 'idst_user' => $user->idst));
			}
				
			//Unassigning Categories										
			foreach($categoriesIDs as $catSelection){
				$criteria = new CDbCriteria();
				$criteria->compare('idst_user', $user->idst);
				$criteria->compare('type_of_entry', CoreAdminCourse::TYPE_CATEGORY_DESC);
				$criteria->compare('id_entry', $catSelection);
				CoreAdminCourse::model()->deleteAll($criteria);

				// Refresh courses to make counters ok
				CoreUserPuCourse::model()->updatePowerUserSelection($user->idst);				
			}
				
			$transaction->commit();
		} catch(Exception $e){
			$transaction->rollback();
			$errors = $e->getMessage();
		}
		
		if(!empty($errors)){
			return array('errors' => $errors);
		}
	}
	
	/**
	 * @summary Massively updates PU profiles using an asynchronous job that processes a CSV file. CSV files can be uploaded either throught a publicly accessible URL or throught a multipart POST.
	 * 
	 * @notes Each row of CSV file must contains two columns:
	 * @notes <ul style="padding-left: 40px; list-style:circle;">
	 * @notes <li>1st column: ID of the PU to be assigned or his username (depending on user_column param)</li>
	 * @notes <li>2nd column: Name of the profile to assign (can be empty). </li>
	 * @notes </ul>
	 * 
	 * @parameter user_column [string, required] Whether the user column contains internal Docebo user ids or login names. Possible values [id_user or username]
	 * @parameter reset_profile_if_empty [boolean, optional] If this input param is passed as "true", an empty value in the "profile_name" column is interpreted as a reset command. Default is "false", i.e. do not change PU Profile if "profile_name" is empty.
	 * @parameter file_url [url, optional] Publicly accessible URL where the CSV file can be downloaded from.
	 * @parameter file_upload [file, optional] The name of the upload CSV file (via multipart POST).
	 * @response job_id [string, required] A unique ID identifying the scheduled job. User to get the status of the job.
	 *
	 * @status 401 Power User app is not enabled
	 * @status 402 Missing or invalid required params
	 * @status 403 User is not a Power User	 	
	 * @status 404 Api current not available on this system
	 * @status 405 Another synchronization is currently running 	 
	 * @status 406 Validation errors:
	 * @status 407 Could not start the user synch job 
	 *
	 * @throws CHttpException 
	 */
	public function bulkUpdate(){
		return $this->handleBulkTask(PoweruserUpdateForm::JOB_TYPE_UPDATE);
	}
	
	/**
	 * @summary Massively sets assigned users/groups/branches for PUs using an asynchronous job that processes a CSV file. CSV files can be uploaded either through a publicly accessible URL or throwgh a multipart POST.
	 *
	 * @notes Each row of CSV file must contains three columns:
	 * @notes <ul style="padding-left: 40px; list-style:circle;">
	 * @notes <li>1st column: ID of the PU to be assigned or his username (depending on puser_column param)</li>
	 * @notes <li>2nd column: Item type (can be one of the following values: <i>id_user, userid, id_group, group_name, id_org, branch_name</i>)</li>
	 * @notes <li>3rd column: Item value (e.g. the ID of the user, the name of the branch, etc.)</li>
	 * @notes </ul>
	 * 
	 * @parameter puser_column [string, required] Whether the user column contains internal Docebo user ids or login names. Possible values [id_user or username]
	 * @parameter reset_assignments [boolean, optional] Optional param that defines whether each PU in the file schould have his assignmens reset, before applying the new once. Possible values: [<i>none, users, branches, groups, all</i>]. A comma-separated combination of <i>users, branches</i> and <i>groups</i> is allowed (e.g. <i>users,branches</i>). Defaults to <i>"none"</i> i.e. merge new assignments with the existing ones without resitting anything.
	 * @parameter file_url [url, optional] Publicly accessible URL where the CSV file can be downloaded from.
	 * @parameter file_upload [file, optional] The name of the upload CSV file (via multipart POST).
	 * @response job_id [string, required] A unique ID identifying the scheduled job. User to get the status of the job.
	 *
	 * @status 401 Power User app is not enabled
	 * @status 402 Missing or invalid required params
	 * @status 403 User is not a Power User	 	
	 * @status 404 Api current not available on this system
	 * @status 405 Another synchronization is currently running 	 
	 * @status 406 Validation errors:
	 * @status 407 Could not start the user synch job 
	 *
	 * @throws CHttpException 
	 */
	public function bulkAssign(){
		return $this->handleBulkTask(PoweruserUpdateForm::JOB_TYPE_ASSIGN);
	}

	/**
	 * @summary Gets the current status of the bulk PU users assignment job.
	 *
	 * @notes Input Example:
	 * @notes <ul style="padding-left: 40px; list-style:circle;">
	 * @notes <li><i><u>job_id</u></i>: a73nsbx81nSSSsj38</li>
	 * @notes </ul>
	 * @notes <br>
	 * @notes Output Example:
	 * @notes <ul style="padding-left: 40px; list-style:circle;">
	 * @notes <li><i><u>success</u></i>: true</li>
	 * @notes <li><i><u>status</u></i>: "completed"</li>
	 * @notes <li><i><u>errors</u></i>: ["InvalidprofileID", "InvaliduserID"]</li>
	 * @notes </ul>
	 *
	 * @parameter job_id [string, required] ID of the scheduled job, as returned by poweruser/bulkUpdate.
	 *
	 * @response status [string, required] Job status
	 * @response processed_records [integer, required] Number of processed rows/records
	 * @response total_records [integer, required] Total number of rows/records
	 * @response errors [array, required] Errors Array
	 *     @item error [string, optional] Error information
	 * @end
	 *
	 * @status 401 Missing required param "job_id
	 * @status 402 Invalid job ID param
	 *
	 * @throws CHttpException
	 */
	public function bulkUpdateStatus(){
		return $this->handleBulkStatus();
	}
	
	/**
	 * @summary Gets the current status of the bulk PU users assignment job.
	 *
	 * @notes Input Example:
	 * @notes <ul style="padding-left: 40px; list-style:circle;">
	 * @notes <li><i><u>job_id</u></i>: a73nsbx81nSSSsj38</li>
	 * @notes </ul>
	 * @notes <br>
	 * @notes Output Example:
	 * @notes <ul style="padding-left: 40px; list-style:circle;">
	 * @notes <li><i><u>success</u></i>: true</li>
	 * @notes <li><i><u>status</u></i>: "completed"</li>
	 * @notes <li><i><u>errors</u></i>: ["InvalidprofileID", "InvaliduserID"]</li>
	 * @notes </ul>
	 *
	 * @parameter job_id [string, required] ID of the scheduled job, as returned by poweruser/bulkAssign.
	 *
	 * @response status [string, required] Job status
	 * @response processed_records [integer, required] Number of processed rows/records
	 * @response total_records [integer, required] Total number of rows/records
	 * @response errors [array, required] Errors Array
	 *     @item error [string, optional] Error information
	 * @end
	 *
	 * @status 401 Missing required param "job_id
	 * @status 402 Invalid job ID param
	 *
	 * @throws CHttpException
	 */
	public function bulkAssignStatus(){
		return $this->handleBulkStatus();
	}
	
	private function handleBulkStatus(){
		$job_id = $this->readParameter('job_id', null);
		
		if(!$job_id){
			throw new CHttpException('401', 'Missing required param "job_id"');
		}
		
		$job = CoreJob::model()->findByAttributes(array('hash_id' => $job_id));
		
		if(!$job){
			throw new CHttpException(402, 'Invalid job ID param');
		}
		
		$param = json_decode($job->params, true);
		$importForm = new PoweruserUpdateForm();
		foreach ($param['form'] as $attribute => $value){
			if(property_exists($importForm, $attribute)){
				$importForm->$attribute = $param['form'][$attribute];
			}
		}
		
		$result = array(
			'status' => 'running',
			'processed_records' => ($param['offset'] > $importForm->totalUsers) ? $importForm->totalUsers : $param['offset'],
			'total_records' => $importForm->totalUsers,
			'errors' => array()
		);
		
		if($job->active == 0){
			$result['status'] = 'completed';
			$logFileName = $job->id_job . '_failed_lines.csv';
			$localLogFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $logFileName;
			$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_ASSETS);
			if($storage->downloadAs($logFileName, $localLogFilePath)){
				$fh = fopen($localLogFilePath, 'r');
				if($fh != FALSE){
					while(($line = fgetcsv($fh)) !== FALSE){
						$result['errors'][] = array(
							'error' => implode("|", $line)
						);
					}
					fclose($fh);
				}
			}
		}
		
		return $result;
	}
	
	private function handleBulkTask($type){					
		$transaction = Yii::app()->db->beginTransaction();
		
		try{
			if (!PluginManager::isPluginActive('SchedulerApp') || !Yii::app()->scheduler){
				throw new CHttpException(404, 'Api current not available on this system');
			}
				
			$puUpdateForm = new PoweruserUpdateForm();
			$puUpdateForm->separator = $this->readParameter('separator', 'auto');
			$puUpdateForm->manualSeparator = $this->readParameter('manual_separator', '');
			$puUpdateForm->firstRowAsHeader = $this->readParameter('first_row_header', false);
			$puUpdateForm->charset = $this->readParameter('charset', 'UTF-8');
			$puUpdateForm->userColumn = $this->readParameter('user_column');
			$puUpdateForm->userColumn = $this->readParameter('puser_column', $puUpdateForm->userColumn);
			$puUpdateForm->file = $this->getCSVFile('pu');
			$puUpdateForm->jobType = $type;
			$puUpdateForm->resetAssigment = $this->readParameter('reset_assignments', 'none');
			$puUpdateForm->resetProfile = $this->readParameter('reset_profile_if_empty', false);
				
			if(!$puUpdateForm->userColumn){
				throw new CHttpException(402, 'Missing or invalid required params');
			}
				
			if($puUpdateForm->validate()){
				$puUpdateForm->originalFilename = Docebo::randomHash() . "." . $puUpdateForm->file->extensionName;
				$puUpdateForm->localFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $puUpdateForm->originalFilename;
				@copy($puUpdateForm->file->tempName, $puUpdateForm->localFile);
			} else{
				throw new CHttpException(406, 'Validation errors: ' . $this->errorSummary($puUpdateForm));
			}
				
			$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_ASSETS);
			$storage->store($puUpdateForm->localFile);
				
			$puUpdateForm->clearErrors();
				
			$csvFile = new QsCsvFile(array(
				'path' => $puUpdateForm->localFile,
				'charset' => $puUpdateForm->charset,
				'delimer' => UserImportForm::translateSeparatorName($puUpdateForm->separator),
			));
				
			$puUpdateForm->totalUsers = $csvFile->getTotalLines();
		
			$params = array(
				'limit' => BulkApiTask::MAX_USERS_PER_RUN,
				'offset' => 0,
				'form' => $puUpdateForm->getParams(),
				'form_type' => 'PoweruserUpdateForm'
			);
			$scheduler = Yii::app()->scheduler;					
			$job = $scheduler->createImmediateJob('ApiPoweruserUpdate', BulkApiTask::id(), $params);
				
				
			$transaction->commit();
				
			return array('job_id' => $job->hash_id);
		} catch (CHttpException $e){
			$transaction->rollback();
			throw $e;
		} catch (Exception $e){
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			$transaction->rollback();
			throw new CHttpException(407, 'Could not start the user synch job');
		}
	}
	
	private function updateLearningPlansInReportFilters($puID,$plans){
		// Clean up report filters: remove the user being deleted from all reports, that are created by this PU
		$customReports = LearningReportFilter::model()->findAllByAttributes(array(
			'author'=>$puID,
			'report_type_id'=>LearningReportType::USERS_COURSEPATH
		));
	
		if ($customReports){
			if (!is_array($customReports)) {
				$customReports = array($customReports);
			}
			foreach ($customReports as $report) {
				$report->cleanUpCoursepaths($plans);
			}
		}
	}
	
	private function resolveCoursesForAssign($items){
		$coursesIDs = array();
		$lpIDs = array();
		$categoriesIDs = array();

		foreach ($items as $key => $item){

			switch($key){
				case self::COURSES_TYPE_ID_COURSE:

					if(is_array($item)){
						foreach($item as $courseId){
							$course = LearningCourse::model()->findAllByPk($courseId);

							if(!$course){
								throw new CHttpException(404, 'Invalid item');
							}

							$coursesIDs[] = $courseId;
						}
					} else{
						$course = LearningCourse::model()->findAllByPk($item);

						if(!$course){
							throw new CHttpException(404, 'Invalid item');
						}

						$coursesIDs[] = $item;
					}
	
					break;
				case self::COURSES_TYPE_COURSE_CODE:

					if(is_array($item)){
						foreach($item as $courseCode){
							$course = LearningCourse::model()->find('code = :code', array(
								':code' => $courseCode
							));

							if(!$course){
								throw new CHttpException(404, 'Invalid item');
							}

							$coursesIDs[] = $course->idCourse;
						}
					} else{
						$course = LearningCourse::model()->find('code = :code', array(
							':code' => $item
						));

						if(!$course){
							throw new CHttpException(404, 'Invalid item');
						}

						$coursesIDs[] = $course->idCourse;
					}

					break;
				case self::COURSES_TYPE_ID_LEARNINGPLAN:
	
					if (!PluginManager::isPluginActive('CurriculaApp')) {
						throw new CHttpException(405, 'Curricula App is not enabled');
					}

					if(is_array($item)){
						foreach($item as $lpId){
							$lp = LearningCoursepath::model()->findByPk($lpId);

							if(!$lp){
								throw new CHttpException(404, 'Invalid item');
							}

							$lpIDs[] = $lpId;
						}
					} else{
						$lp = LearningCoursepath::model()->findByPk($item);

						if(!$lp){
							throw new CHttpException(404, 'Invalid item');
						}

						$lpIDs[] = $item;
					}
	
					break;
				case self::COURSES_TYPE_LEARNINGPLAN_CODE:
	
					if (!PluginManager::isPluginActive('CurriculaApp')) {
						throw new CHttpException(405, 'Curricula App is not enabled');
					}

					if(is_array($item)){
						foreach($item as $lpCode){
							$lp = LearningCoursepath::model()->find('path_code = :code', array(
								':code' => $lpCode
							));

							if(!$lp){
								throw new CHttpException(404, 'Invalid item');
							}

							$lpIDs[] = $lp->id_path;
						}
					} else{
						$lp = LearningCoursepath::model()->find('path_code = :code', array(
							':code' => $item
						));

						if(!$lp){
							throw new CHttpException(404, 'Invalid item');
						}

						$lpIDs[] = $lp->id_path;
					}
	
					break;
				case self::COURSES_TYPE_ID_CATEGORY:

					if(is_array($item)){
						foreach($item as $categoryId){
							$category = LearningCourseCategoryTree::model()->find('idCategory = :category', array(
								':category' => $categoryId
							));

							if(!$category){
								throw new CHttpException(404, 'Invalid item');
							}

							$categoriesIDs[] = $categoryId;
						}
					} else{
						$category = LearningCourseCategoryTree::model()->find('idCategory = :category', array(
							':category' => $item
						));

						if(!$category){
							throw new CHttpException(404, 'Invalid item');
						}

						$categoriesIDs[] = $item;
					}
	
					break;
			}
		}
	
		$result = array(
				'coursesIds' => $coursesIDs,
				'lpIDs' => $lpIDs,
				'categoriesIDs' => $categoriesIDs
		);
	
		return $result;
	}
	
	private function getSelectedIds($itemType, $itemValue){
	
		$list = array();
	
		switch($itemType){
			case self::ITEM_TYPE_ID_USER:
				$_user = CoreUser::model()->findByPk($itemValue);
	
				if(!$_user){
					throw new CHttpException(405, 'Invalid item_value for item_type');
				}
	
				$list[] = $_user->idst;
				break;
			case self::ITEM_TYPE_USERID:
				$_user = CoreUser::model()->find('userid LIKE :user', array(
				':user' => '/' . $itemValue
				));
	
				if(!$_user){
					throw new CHttpException(405, 'Invalid item_value for item_type');
				}
	
				$list[] = $_user->idst;
				break;
			case self::ITEM_TYPE_ID_GROUP:
				$group = CoreGroup::model()->findByPk($itemValue);
	
				if(!$group){
					throw new CHttpException(405, 'Invalid item_value for item_type');
				}
	
				$list[] = $group->idst;
				break;
			case self::ITEM_TYPE_GROUP_NAME:
				$group = CoreGroup::model()->find('groupid = :groupName', array(
				':groupName' => '/' . $itemValue
				));
	
				if(!$group){
					throw new CHttpException(405, 'Invalid item_value for item_type');
				}
	
				$list[] = $group->idst;
				break;
			case self::ITEM_TYPE_ID_ORG:
				$orgChart = CoreOrgChartTree::model()->findByPk($itemValue);

				if(!$orgChart){
					throw new CHttpException(405, 'Invalid item_value for item_type');
				}
	
				$orgChartTree = CoreOrgChartTree::model()->findByPk($itemValue);
	
				$list[] = $orgChartTree->idst_ocd;
				break;
			case self::ITEM_TYPE_BRANCH_NAME:
				$language = Lang::getNameByBrowserCode(Yii::app()->getLanguage());
	
				$orgChart = CoreOrgChart::model()->find('lang_code = :lang AND translation = :value', array(
					':lang' => strtolower ($language),
					':value' => $itemValue
				));
	
				if(!$orgChart){
					throw new CHttpException(405, 'Invalid item_value for item_type');
				}
	
				$orgChartTree = CoreOrgChartTree::model()->findByPk($orgChart->id_dir);
	
				$list[] = $orgChartTree->idst_ocd;
				break;
		}
	
		return $list;
	}
}