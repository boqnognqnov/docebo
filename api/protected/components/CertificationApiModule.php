<?php

/**
 * Certification API Component
 * @description Set of APIs to manage certifications & retraining
 */
class CertificationApiModule extends ApiModule {

	/**
	 * @summary Resets the trackings of a user in a course
	 * @notes <b>Beware: This action resets all tracking information for the passed user (e.g. played scorm trackings, test attempts, uploaded assignments..).</b>
	 *
	 * @parameter id_course [integer, required] Docebo ID for the course to reset
	 * @parameter id_user [integer, required] Docebo ID of the user to reset
	 *
	 * @status 201 Missing mandatory params
	 *
	 */
	public function resetCourseTrackings() {

		// Check mandatory params
		$id_course = $this->readParameter('id_course');
		$id_user = $this->readParameter('id_user');
		if(!$id_course || !$id_user)
			throw new CHttpException(201, "Missing mandatory params");

		// Check valid IDs
		$course = LearningCourse::model()->findByPk($id_course);
		if(!$course)
			throw new CHttpException(202, "Invalid course ID provided");
		$user = CoreUser::model()->findByPk($id_user);
		if(!$user)
			throw new CHttpException(202, "Invalid user ID provided");

		// Try resetting permissions
		LearningCourseuser::reset($id_course, $id_user);
	}
}
