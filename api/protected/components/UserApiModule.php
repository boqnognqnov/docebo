<?php

/**
 * User API Component
 * @description Set of APIs to manage users of the LMS
 */
class UserApiModule extends ApiModule {


	/**
	 * Returns the requested security level for the passed action
	 * @param $actionName The name of the action
	 * @return string The requested security level
	 */
	public function getSecurityLevel($actionName) {

		switch($actionName) {
			case 'lostpassword':
			case 'authenticate':
				return self::$SECURITY_NONE;
				break;
			case "profile" :
			case "userCourses" :
			case "getstat" :
			case "thumbnail" :
			case "logout" :
			case "checkToken" :
            case "get_profile_image" :
				return self::$SECURITY_LIGHT;
				break;
			default:
				return self::$SECURITY_STRONG;
		}
	}


	/**
	 * Internal utility method, retrieve an organization branch id from branch name
	 * @param string $like the branch name (case insensitive)
	 * @param bool|int $parent optional, the id of the parent branch
	 * @param bool|string $lang_code optional, the language code to search for
	 * @return mixed the numeric od of the branch, or false if no branch has been found
	 */
	private function _getBranch($like, $parent = false, $lang_code = false) {

		//check input
		if (!$like) {
			return false;
		}

		//prepare SQL command
		$command = Yii::app()->db->createCommand()
			->select("oct.idOrg")
			->from(CoreOrgChart::model()->tableName() . " oc")
			->join(CoreOrgChartTree::model()->tableName() . " oct", "oct.idOrg = oc.id_dir")
			->where("oc.translation LIKE :search", array(':search' => $like));

		//optional parameters
		if ($lang_code !== false) {
			$command->andWhere("oc.lang_code = :lang_code", array(':lang_code' => $lang_code));
		}
		if ($parent !== false) {
			$command->andWhere("oct.idParent = :idParent", array('id_parent' => (int)$parent));
		}

		//execute command
		$result = $command->queryAll();
		if (is_array($result) && !empty($result)) {
			return $result[0]['idOrg'];
		}

		//if no resultset from SQL, return false
		return false;
	}


	/**
	 * Return the path of an organization chart by its group idst
	 * @param integer $groupIdst the idst of the folder's group
	 * @param bool|string $language the language for the folder path
	 * @return string (false, if no path could be retrieved)
	 */
	private static function _getOrgchartPath($groupIdst, $language = false) {

		//initialize output
		$output = false;

		//retrieve node
		$node = CoreOrgChartTree::model()->findByAttributes(array('idst_oc' => $groupIdst));
		if ($node) {
			$output = $node->getNodePath('/');
		}

		return $output;
	}


	/**
	 * Resolve external user
	 * @param $externalUser
	 * @param $externalUserType
	 * @return type
	 */
	private static function _getExternalUserIdst($externalUser, $externalUserType) {

		//initialize output
		$idst = false;

		//check input and extract setting from DB
		if ($externalUser != '' && $externalUserType != '') {
			// search for the user with the external id
			$path = 'ext.user.' . $externalUserType;
			$value = 'ext_user_' . $externalUserType . "_" . $externalUser;

			//search the user in DB
			$queryParams = array(
				':path_name' => $path,
				':value' => $value
			);
			// NOTE: the MAX() is applied because sometimes -due to errors- the same external association is applied to more
			// than one user. This way only the highest ID -and consequently the most recent association- is selected.
			$cmd = Yii::app()->db->createCommand()
				->select("MAX(id_user) AS latest_id_user")
				->from(CoreSettingUser::model()->tableName())
				->where("path_name = :path_name AND value = :value");
			$reader = $cmd->query($queryParams);
			if ($reader) {
				$record = $reader->read();
				if ($record) {
					$idst = (int)$record['latest_id_user'];
				}
			}
		}

		return $idst;
	}

    /**
     * Returns the array of ext.user.* fields for the passed user
     * @param $idst Idst of the user
     * @return array Array of ext.user.*
     */
    private function _getExtUserSettingForUser($idst) {
        $values = array();

        $criteria = new CDbCriteria();
        $criteria->addCondition('path_name LIKE :path_name')
            ->addCondition('id_user = :idst');
        $criteria->params[':path_name'] = 'ext.user.%';
        $criteria->params[':idst'] = $idst;

        $models = CoreSettingUser::model()->findAll($criteria);
        foreach($models as $model) {
            /* @var $model CoreSettingUser */
            $type = substr($model->path_name, 9);
            $len = strlen("ext_user_".$type."_");
            $value = substr($model->value, $len);
            $values[] = array(
                "ext_type" => $type,
                "ext_user" => $value
            );
        }

        return $values;
    }

	/**
	 * Check if an user is a god admin or not
	 * @param integer $idUser the idst of the user to be checked
	 * @return boolean
	 */
	private static function _isGodAdmin($idUser) {
		$count = Yii::app()->db->createCommand()
			->select("COUNT(*) AS tot")
			->from(CoreGroupMembers::model()->tableName() . " gm")
			->join(CoreGroup::model()->tableName() . " g", "gm.idst = g.idst")
			->where("g.groupid = :groupid", array(':groupid' => '/framework/level/godadmin'))
			->andWhere("gm.idstMember = :id_user", array(':id_user' => $idUser))
			->queryScalar();
		return ($count > 0);
	}

	/**
	 * Reads additional fields from db
	 * @param $inputFields an array of fields using core_field::id_common as key and field content as value
	 * @param bool $fillAll if set to true, an array for ALL fields will be returned. Unrequested fields will be filled with NULL value.
	 * @return array an array of fields using core_field::idField as index and a sub-array with value and field type as content
	 */
	public function readAdditionalFields($inputFields, $fillAll = false)
	{
        $criteria = new CDbCriteria;
        $criteria->index = 'id_field';
        $_fieldsList = CoreUserField::model()->language()->findAll($criteria);
        $additionalFields = array();

        if (is_array($inputFields)) {
			foreach ($inputFields as $fieldId => $valueField) { // $fieldId is the core_user_field::id_field !!!
				if (isset($_fieldsList[$fieldId])) {
					$index = $_fieldsList[$fieldId]->getFieldId();
					switch ($_fieldsList[$fieldId]->getFieldType()) {
						case CoreUserField::TYPE_DROPDOWN: {
                            $optionId = CoreUserFieldDropdown::getOptionValueByLabel($valueField, $fieldId);
							if ($optionId) {
								$additionalFields[$index] = array(
									'value' => (int) $optionId,
									'type_field' => $_fieldsList[$fieldId]->getFieldType()
								);
							}
						} break;
						case CoreUserField::TYPE_COUNTRY: {
							$criteria = new CDbCriteria();
							$criteria->addCondition("name_country LIKE :translation");
							$criteria->params[':translation'] = $valueField;
							$son = CoreCountry::model()->find($criteria);
							if ($son) {
								$additionalFields[$index] = array(
									'value' => (int) $son->id_country,
									'type_field' => $_fieldsList[$fieldId]->getFieldType()
								);
							}
						} break;
						case CoreUserField::TYPE_UPLOAD: {
							//...
						} break;
						default: {
							$additionalFields[$index] = array(
								'value' => $valueField,
								'type_field' => $_fieldsList[$fieldId]->getFieldType()
							);
						} break;
					}
				} else {
					//TODO: is it appropriate launching an exception here?
				}
			}
		}

		if ($fillAll) {
			// Fill not passed fields
			foreach ($_fieldsList as $fieldId => $field) {
                /** @var CoreUserField $field */
				if (!isset($additionalFields[$fieldId])) {
					$additionalFields[$fieldId] = array(
						'type_field' => $field->getFieldType(),
						'value' => ''
					);
				}
			}
		}

		return $additionalFields;
	}

	/**
	 * A function to be called if you want to test
	 * if the API to this LMS works.
	 */
	public function apiResponseTest(){
		return array(
			'success' => '1',
			'api_status' => 'available'
		);
	}

    /**
     * @summary Checks if a user exists by its username (userid) or its email if also_check_as_email is true
     * @parameter userid [string, required] The username or email of a valid user in the LMS
     * @parameter also_check_as_email [boolean, optional] If true is passed, the userid param is checked against the user email
     *
     * @response idst [integer, required] Internal ID of the authenticated user
     * @response firstname [string, required] The user's firstname
     * @response lastname [string, required] The user's lastname
     * @response email [string, required] The user's email
     * @response ext_fields [array, optional] User's external identification fields
     *      @item ext_type [string, required] Type of external identification (e.g. joomla, drupal)
     *      @item ext_user [string, required] Value assigned for this external identification type
     *
     * @status 202 Invalid params passed
     * @status 201 User not found
     */
    public function checkUsername() {
        $userid = $this->readParameter('userid', null);
        $check_as_email = $this->readParameter('also_check_as_email', false);

        // Check input param
        if(!$userid)
            throw new CHttpException(202, "Invalid params passed");

        // Load user from DB
        $criteria = new CDbCriteria();
        $criteria->addCondition("userid = :user_id");
        if($check_as_email) {
            $criteria->addCondition("email = :email", 'OR');
			$criteria->params[':email'] = $userid;
		}

	   $criteria->params[':user_id'] = Yii::app()->user->getAbsoluteUsername($userid);


        $user = CoreUser::model()->find($criteria);
        if(!$user)
            throw new CHttpException(201, "User not found");

        $output = array(
            'idst' => (int) $user->idst,
            'firstname' => $user->firstname,
            'lastname' => $user->lastname,
            'email' => $user->email
        );

        // Add ext_user and ext_user_type to the returned results
        $extUserValues = $this->_getExtUserSettingForUser($user->idst);
        if(!empty($extUserValues))
            $output['ext_fields'] = $extUserValues;

        return $output;
    }

	/**
	 * @summary Check if valid username and password are provided and returns the authentication token required for single sign on
	 *
	 * @parameter username [string, required] username of the user that we want to authenticate
	 * @parameter password [string, required] password of the user that we want to authenticate
	 *
	 * @response id_user [integer, optional] Internal ID of the authenticated user
	 * @response token [string, optional] The generated token
	 *
	 * @status 201 Invalid login data provided
	 */
	public function authenticate() {
		$username = $this->readParameter('username', false);
		$password = $this->readParameter('password', false);

		// Check passed params
		if ($username == false || $password == false) {
			throw new CHttpException(201, 'Invalid login data provided.');
		}

		// Retrive user model
		$userModel = CoreUser::model()->findByAttributes(array('userid' => Yii::app()->user->getAbsoluteUsername($username)));
		if (!$userModel) {
			throw new CHttpException(201, 'Invalid login data provided.');
		}

		// Check password hash
		if($userModel->pass != md5($password) && !Yii::app()->security->validatePassword($password, $userModel->pass)){
			throw new CHttpException(201, 'Invalid login data provided.');
		}

		$token = $this->_generateToken($userModel->idst);
		if ($token) {
			$output = array(
				'id_user' => $userModel->idst,
				'token' => $token,
			);
		} else {
			throw new CHttpException(201, 'Invalid login data provided.');
		}

		return $output;
	}

	/**
	 * @summary Check if a token is valid for a user
	 *
	 * @parameter id_user [integer, required] Internal ID of the user
	 * @parameter auth_token [string, required] Token of the user that we want to check
	 *
	 * @response id_user [integer, required] Internal ID of the user
	 * @response auth_token [string, required] The token
	 * @response token_valid [bool, required] true if the token is valid for the user false otherwise
	 *
	 * @status 201 No id_user specified
	 * @status 202 No auth_token specified
	 */
	public function checkAuthentication()
	{
		$id_user = $this->readParameter('id_user', false);
		$auth_token = $this->readParameter('auth_token', false);

		if($id_user == false || $id_user == '')
			throw new CHttpException(201, 'No id_user specified');

		if($auth_token == false || $auth_token == '')
			throw new CHttpException(202, 'No auth_token specified');

		$current_token = $this->_recoverToken($id_user);

		$output = array(
				'id_user' => $id_user,
				'auth_token' => $auth_token
			);

		if($current_token == false || $current_token != $auth_token)
			$output['token_valid'] = false;
		else
			$output['token_valid'] = true;

		return $output;
	}

	/**
	 * @summary Sends the password recovery email to the user
	 *
     * @parameter email [string, required] the email of the user whose password must be recovered
     *
	 * @status 201 Error while performing the requested operation
	 */
	public function lostpassword() {
		// Check input params
		$email = $this->readParameter('email', false);
		if (!$email || (filter_var($email, FILTER_VALIDATE_EMAIL) == false)) {
			throw new CHttpException(201, "Error while performing the requested operation");
		}

		// Try to load a user with the provided email
		$userModel = CoreUser::model()->findByAttributes(array('email' => $email));
		if (!$userModel) {
			throw new CHttpException(201, "Error while performing the requested operation");
		}

		// If DomainBrandingApp is enabled, use the HTTP_HOST and ignore 'url' setting
		if (PluginManager::isPluginActive('DomainBrandingApp')) {
			$url = parse_url(Yii::app()->getBaseUrl(true), PHP_URL_HOST);
		} else {
			$url = Settings::get('url', '');
		}

		// Preparing email placeholders
		$body = Yii::t('register', '_LOST_USERID_MAILTEXT', array(
			'[date_request]' => Yii::app()->localtime->toLocalDateTime(),
			'[url]' => $url,
			'[userid]' => substr($userModel->userid, 1) // removing ACL_SEPARATOR
		));
		$subject = Yii::t('register', '_LOST_USERID_TITLE');
		$fromAdminEmail = array(Settings::get('mail_sender', 'noreply@docebo.com'));
		$toEmail = array($email);

		// Sending the email
		$mm = new MailManager();
		$mm->setContentType('text/html');
		$mm->mail($fromAdminEmail, $toEmail, $subject, $body);
	}


	/**
	 * @summary Logs out the current user and invalidates a token
     *
	 * @parameter id_user [integer, required] the idst of the user whose session is to be destroyed
     *
	 * @status 201 Error while performing the requested operation
	 */
	public function logout() {
		// Check input params
		$id_user = $this->readParameter('id_user', false);
		if (!$id_user) $id_user = $this->readParameter('idst', 0);
		if (!$id_user) $id_user = $this->readParameter('userid', 0);

		if (!$id_user) {
			throw new CHttpException(201, "Error while performing the requested operation");
		}

		// Try to delete tokens assigned to this user
		CoreRestAuthentication::model()->deleteAllByAttributes(array('id_user' => $id_user));
	}


	/**
	 * Return existing (non expired) OR generates  a new token for the passed user id
	 *
	 * @param $id_user The idst of the user
	 * @return bool|string The generated token
	 * @throws CHttpException
	 */
	protected function _generateToken($id_user) {

		// Create helper component
		$authToken = new AuthToken($id_user);

		// Lets do some cleanup -> remove expired tokens for all users
		$authToken->cleanup();

		// Get existing token; If it does not exist, create new one and return
		$token = $authToken->get(true);

		return $token;
	}

	/**
	 * Return existing (non expired) token for the passed user id
	 *
	 * @param $id_user The idst of the user
	 * @return bool|string The generated token
	 * @throws CHttpException
	 */
	protected function _recoverToken($id_user) {

		// Create helper component
		$authToken = new AuthToken($id_user);

		// Lets do some cleanup -> remove expired tokens for all users
		$authToken->cleanup();

		// Get existing token;
		$token = $authToken->get(false);

		return $token;
	}

	/**
	 * @summary Creates a new user with the provided account information
	 * @notes The newly created user will be able to login into the LMS using the provided credentials
	 *
	 * @parameter userid [string, required] the username of the user (used for the login)
	 * @parameter firstname [string, optional] User's firstname
	 * @parameter lastname [string, optional] User's firstname
	 * @parameter password [string, optional] Login password in clear text
	 * @parameter force_password_change [boolean, optional] Force users to change their password at the first sign in (defaults to false)
	 * @parameter email [string, optional] User's email
	 * @parameter valid [boolean, optional] False if this user should be suspended, true to unsuspend the user
	 * @parameter reg_code [string, optional] Registration code
	 * @parameter reg_code_type [string, optional] Registration code type
	 * @parameter ext_user_type [string, optional] Third party user type
	 * @parameter ext_user [string, optional] Third party user
	 * @parameter role [string, optional] User level (student, admin, etc)
	 * @parameter language [string, optional] User language code (e.g. simplified_chinese)
	 * @parameter orgchart [string, optional] List of orgchart nodes the user should be assigned to, separated by semicolon (e.g. Node 1; Node 2)
	 * @parameter id_group [integer, optional] ID of the group that the user should be assigned to (e.g. 47)
	 * @parameter fields [object, required] List of additional fields to set for the user, indexed by field ID (e.g. fields[12] = "New York City", fields[23] = "123")
	 * @parameter timezone [string, optional] The timezone to set for the user (e.g. Europe/Rome). See http://php.net/manual/en/timezones.php for a complete list.
	 * @parameter disableNotifications [boolean, optional] If set to true, the system will not trigger the "User has been created (by administrator)" notification (defaults to false).
	 *
     * @status 201 Empty userid
     * @status 202 Error while assigning user level
	 * @status 203 Cannot create godadmin users
     * @status 254 Cannot save user
     * @status 205 Group not found
     *
	 * @response idst [integer, required] The internal Docebo ID for the newly created user
	 */
	public function create() {

		// Read input data
		$input = new stdClass();

		$input->userid = trim($this->readParameter('userid'));
		$input->firstname = trim($this->readParameter('firstname'));
		$input->lastname = trim($this->readParameter('lastname'));
		$input->password = $this->readParameter('password');
		$input->force_change = ($this->readParameter('force_password_change', 0) > 0 ? 1 : 0);
		$input->email = trim($this->readParameter('email'));
		$input->language = $this->readParameter('language', CoreUser::getDefaultLangCode());
		$input->orgchart = $this->readParameter('orgchart');
		$input->id_group  = $this->readParameter('id_group');
		$input->role = $this->readParameter('role');
		$input->fields = $this->readParameter('fields', array()); //custom fields
		$input->reg_code = $this->readParameter('reg_code');
		$input->reg_code_type = $this->readParameter('reg_code_type');
		$input->ext_user = $this->readParameter('ext_user');
		$input->ext_user_type = $this->readParameter('ext_user_type');
		$input->valid = $this->readParameter('valid', false);
		$input->valid = $input->valid === true || $input->valid === 'true' ? 1 : $input->valid;
		$input->valid = $input->valid !== false && $input->valid !== 'false' ? ($input->valid > 0 ? 1 : 0) : 0;
		$input->timezone = $this->readParameter('timezone');
		$input->disableNotifications = $this->readParameter('disableNotifications', false);

		// Validate input data
		if($input->userid == '')
			throw new CHttpException(201, 'Empty userid');
        if($input->role == 'godadmin')
            throw new CHttpException(203, 'Cannot create godadmin users');

		// Let's see if the API Caller has passed id_group
		if($input->id_group){
			// If so and the group does not exists let's throw the propper error message
			if(!CoreGroup::model()->exists(array("condition" => 'idst = :group AND hidden = "false"', 'params' => array(":group" => $input->id_group))))
				throw new CHttpException( 205, 'Group not found' );
		}

		if ($input->password == '') {
			//if no specified password, then generate a random one
			$randomPassword = chr(mt_rand(65, 90))
				. chr(mt_rand(97, 122))
				. mt_rand(0, 9)
				. chr(mt_rand(97, 122))
				. chr(mt_rand(97, 122))
				. chr(mt_rand(97, 122))
				. mt_rand(0, 9)
				. chr(mt_rand(97, 122));
			$input->password = $randomPassword;
		}

		$transaction = Yii::app()->db->beginTransaction();

		try {
			$user = new CoreUser('createApi');
			$user->userid = $input->userid;
			$user->firstname = $input->firstname;
			$user->lastname = $input->lastname;
			$user->new_password = $input->password;
			$user->new_password_repeat = $input->password;
			$user->force_change = $input->force_change;
			$user->email = $input->email;
			if ($this->readParameter('valid', -1) != -1)
				$user->valid = $input->valid;

			// Additional fields
			$additionalFields = $this->readAdditionalFields($input->fields);

			// Set fields to user AR
			if (!empty($additionalFields))
				$user->setAdditionalFieldValues($additionalFields);

			// Set organization chart nodes
			if ($input->orgchart) {
				$branches = explode(";", $input->orgchart);
				if (is_array($branches)) {
					foreach ($branches as $branch) {
						$idOrg = $this->_getBranch($branch);
						if (!empty($idOrg))
							$user->chartGroups[] = (int)$idOrg;
					}
				}
			}

			//Check for registration code chart node
			$found = false;
			switch($input->reg_code_type)
			{
				case 'tree_man':
					$criteria = new CDbCriteria();
					$criteria->addCondition('code = :reg_code');
					$criteria->params[':reg_code'] = $input->reg_code;
					$found = CoreOrgChartTree::model()->findAll($criteria);
				break;
				case 'tree_drop':
					$criteria = new CDbCriteria();
					$criteria->addCondition('idOrg = :idOrg');
					$criteria->params[':idOrg'] = $input->reg_code;
					$found = CoreOrgChartTree::model()->findAll($criteria);
				break;
			}

			if($found)
				foreach($found as $node)
					$user->chartGroups[] = (int)$node->idOrg;

			//add organization chart root node by default
			$_roots = CoreOrgChartTree::model()->roots()->findAll();
			if (!empty($_roots)) {
				$rootNode = $_roots[0];
				$user->chartGroups[] = $rootNode->getPrimaryKey();
			}

			// Group members (user level group)
			$groupMember = new CoreGroupMembers('create');
			$criteria = new CDbCriteria();
			$criteria->compare('groupid', '/framework/level', true);
			$adminLevelModels = CoreGroup::model()->findAll($criteria);
			$adminLevels = array();
			foreach ($adminLevelModels as $adminLevelModel)
				$adminLevels[$adminLevelModel->groupid] = (int)$adminLevelModel->idst;
			if (empty($adminLevels))
				throw new CHttpException(202, 'Error while assigning user level');

			// Check input for level assignment (default: user level)
			switch($input->role) {
				case 'admin':
					$groupMember->idst = $adminLevels['/framework/level/admin'];
				    break;
				default:
				    $groupMember->idst = $adminLevels['/framework/level/user'];
				    break;
			}

			// Language setting
			$settingUser = new CoreSettingUser('create');
			$settingUser->value = $input->language;
			$settingUser->path_name = 'ui.language';

			// Save data in DB
			if (!$user->save())
				throw new CHttpException(254, "Cannot save user: " . $this->errorSummary($user));

			// Assign the user to the orgchart nodes provided.
			// NOTE: This call will also ensure user orgchart root node assignment
			$user->saveChartGroups();

			// Assign user to the "user level" group
			$groupMember->idstMember = $user->idst;
			if (!$groupMember->save())
				throw new CHttpException(254, "Cannot save user: " . $this->errorSummary($groupMember));

			// Sync the User Role with the Hydra RBAC
			HydraRBAC::manageUserToRole(array( "user" => $user->idst, "group" => $groupMember->idst ));

			// Let's see if the API caller has passed id_group, If so let's add the user to that group as well
			// NOTE: if the group is fake, non existing in DB we wouldn't be here
			if($input->id_group){
				$userGroup = new CoreGroupMembers();
				$userGroup->idst = $input->id_group;
				$userGroup->idstMember = $user->idst;
				$userGroup->save();
			}

			// Save settings
			$settingUser->id_user = $user->idst;
			if (!$settingUser->save())
				throw new CHttpException(254, "Cannot save user: " . $this->errorSummary($settingUser));

			// Save ext_user data
			if (isset($input->ext_user_type) && isset($input->ext_user)) {
				$path_name = 'ext.user.' . $input->ext_user_type;
				$value = 'ext_user_' . $input->ext_user_type . "_" . $input->ext_user;
				$coreSettingUser = new CoreSettingUser('create');
				$coreSettingUser->path_name = $path_name;
				$coreSettingUser->id_user = $user->idst;
				$coreSettingUser->value = $value;
				if (!$coreSettingUser->save())
					throw new CHttpException(254, "Cannot save user: " . $this->errorSummary($coreSettingUser));
			}

			// Save timezone setting (if provided)
			if($input->timezone) {
				$timezoneSettingUser = new CoreSettingUser('create');
				$timezoneSettingUser->path_name = 'timezone';
				$timezoneSettingUser->id_user = $user->idst;
				$timezoneSettingUser->value = $input->timezone;
				if (!$timezoneSettingUser->save())
					throw new CHttpException(254, "Cannot save user: " . $this->errorSummary($timezoneSettingUser));
			}

			// Raise an event (if notifications are not disabled)
			if(!$input->disableNotifications) {
				$user->passwordPlainText = $user->new_password;
				Yii::app()->event->raise('NewUserCreated', new DEvent($this, array('user' => $user)));
			} else {
				Yii::log("Skipping event NewUserCreated for user {$user->idst}", CLogger::LEVEL_ERROR);
			}

			$hasSelfPopulatingGroups = Yii::app()->db->createCommand("SELECT COUNT(*) FROM core_group WHERE assign_rules = 1")->queryScalar();
			if($hasSelfPopulatingGroups > 0)
				GroupAutoAssignTask::singleUserRun($user->idst);

			$transaction->commit();

		} catch(CHttpException $e) {
			$transaction->rollback();
			throw $e;
		} catch(Exception $e) {
			$transaction->rollback();
			throw new CHttpException(500, $e->getMessage());
		}

		return array (
			'idst' => (int)$user->getPrimaryKey()
		);
	}


	/**
	 * @summary Deletes an user
	 * @notes User to be deleted must exists, otherwise an exception is thrown
	 *
	 * @parameter id_user [integer, optional] Docebo ID for the user to be deleted
	 * @parameter idst [integer, optional] DEPRECATED user id_user instead
	 * @parameter ext_user [integer, optional] numeric user id on the external platform
	 * @parameter ext_user_type [string, optional] (for example: drupal, joomla, …)
	 *
	 * @status 210 Invalid user specification
	 * @status 211 Error in user deletion
	 *
	 * @response idst [integer, required] The internal Docebo ID for the deleted user
	 */
	public function delete() {
		// Read input
		$input = new stdClass();
		$input->idst  = $this->readParameter('idst', $this->readParameter('id_user'));
		$input->ext_user_type = $this->readParameter('ext_user_type', '');
		$input->ext_user = $this->readParameter('ext_user', '');

		if (!$input->idst) {
			//try to search for external user
			$input->idst = self::_getExternalUserIdst($input->ext_user, $input->ext_user_type);
		}

		// Validate input
		if (!$input->idst)
			throw new CHttpException(210, 'Invalid user specification');

		// Don't allow godadmins elimination
		if (self::_isGodAdmin($input->idst))
			throw new CHttpException(210, 'Invalid user specification');

		// do stuff
		$coreUser = CoreUser::model()->findByAttributes(array('idst' => $input->idst));
		if (empty($coreUser))
			throw new CHttpException(211, 'User not found');

		$transaction = Yii::app()->db->beginTransaction();
		try {
			$coreUser->delete();

			// Sync the User Role with the Hydra RBAC
			HydraRBAC::manageUserToRole(array( "user" => $coreUser->idst, "group" => null ));

			$transaction->commit();
		} catch(CHttpException $e) {
			$transaction->rollback();
			throw $e;
		} catch(Exception $ex) {
			$transaction->rollback();
			throw new CHttpException(500, $ex->getMessage() /*'Error while deleting user'*/);
		}

		// Return output
		return array('idst' => $input->idst);
	}


	/**
	 * @summary Updates an existing user with the provided account information
	 *
	 * @parameter id_user [integer, optional] Docebo ID for the user to be updated
	 * @parameter idst [integer, optional] DEPRECATED user id_user instead
	 * @parameter ext_user [integer, optional] numeric user id on the external platform
	 * @parameter ext_user_type [string, optional] (for example: drupal, joomla, …)
	 * @parameter userid [string, optional] the username of the user (used for the login)
	 * @parameter firstname [string, optional] User's firstname
	 * @parameter lastname [string, optional] User's lastname
	 * @parameter password [string, optional] Login password in clear text
	 * @parameter force_password_change [boolean, optional] Force users to change their password at the first sign in (defaults to false)
	 * @parameter email [string, optional] User's email
	 * @parameter valid [boolean, optional] False if this user should be suspended, true to unsuspend the user
	 * @parameter language [string, optional] User language code (e.g. simplified_chinese)
	 * @parameter fields [object, optional] List of additional fields to set for the user
	 * @parameter orgchart [string, optional] List of orgchart nodes the user should be assigned to, separated by semicolon (e.g. Node 1; Node 2). This overrides any previously assigned node.
	 * @parameter id_group [integer, optional] ID of the group that the user should be assigned to (e.g. 47)
	 * @parameter timezone [string, optional] The timezone to set for the user (e.g. Europe/Rome). See http://php.net/manual/en/timezones.php for a complete list.
	 * @parameter unenroll_deactivated [boolean, optional] Unenroll user from all future Classroom and Webinar sessions if valid is set to "false"
	 *
	 * @status 201 Invalid specified user
	 * @status 203 Error while updating user
	 * @status 254 Cannot save user
	 * @status 205 Group not found
	 *
	 * @response idst [integer, required] The internal Docebo ID for the updated user
	 */
	public function edit()
	{
		// Read and validate input
		$input = new stdClass();
		$input->idst = $this->readParameter('idst', $this->readParameter('id_user'));
		$input->ext_user_type = $this->readParameter('ext_user_type', '');
		$input->ext_user = $this->readParameter('ext_user', '');
		$input->userid = $this->readParameter('userid', null);
		$input->firstname = $this->readParameter('firstname', false);
		$input->lastname = $this->readParameter('lastname', false);
		$input->password = $this->readParameter('password', false);
		$input->force_change = $this->readParameter('force_password_change', -1);
		$input->email = $this->readParameter('email', false);
		$input->valid = $this->readParameter('valid', false);
		$input->valid = $input->valid === true || $input->valid === 'true' ? 1 : $input->valid;
		$input->valid = $input->valid !== false && $input->valid !== 'false' ? ($input->valid > 0 ? 1 : 0) : 0;
		$input->language = $this->readParameter('language');
		$input->fields = $this->readParameter('fields', array());
		$input->orgchart = $this->readParameter('orgchart');
		$input->id_group = $this->readParameter('id_group');
		$input->timezone = $this->readParameter('timezone');
		$input->unenroll_deactivated = $this->readParameter('unenroll_deactivated', false);
		$input->unenroll_deactivated = $input->unenroll_deactivated === true || $input->unenroll_deactivated === 'true' ? 1 : $input->unenroll_deactivated;
		$input->unenroll_deactivated = $input->unenroll_deactivated !== false && $input->unenroll_deactivated !== 'false' ? ($input->unenroll_deactivated > 0 ? 1 : 0) : 0;
		if (!$input->idst)
			$input->idst = self::_getExternalUserIdst($input->ext_user, $input->ext_user_type);

		if (!$input->idst)
			throw new CHttpException(201, 'Invalid specified user');

		if (self::_isGodAdmin($input->idst))
			throw new CHttpException(201, 'Cannot edit superadministrator users');

		$user = CoreUser::model()->findByPk($input->idst);
		if (!$user)
			throw new CHttpException(201, 'Invalid specified user');

		// Let's see if the API Caller has passed id_group
		if($input->id_group){
			// If so and the group does not exists let's throw the propper error message
			if(!CoreGroup::model()->exists(array("condition" => 'idst = :group AND hidden = "false"', 'params' => array(":group" => $input->id_group))))
				throw new CHttpException( 205, 'Group not found' );
		}

		$user->setScenario('updateApi');

		// Assign new attributes to user model
		if ($input->userid)
			$user->userid = Yii::app()->user->getAbsoluteUsername($input->userid);

		if ($input->firstname !== false)
			$user->firstname = $input->firstname;

		if ($input->lastname !== false)
			$user->lastname = $input->lastname;

		if ($input->password !== false)
			$user->pass = CoreUser::model()->encryptPassword($input->password);

		if($input->force_change !== -1)
		{
			if($input->force_change > 0)
				$user->force_change = 1;
			else
				$user->force_change = 0;
		}

		if ($input->email !== false)
			$user->email = $input->email;

		if ($this->readParameter('valid', -1) != -1)
			$user->valid = $input->valid;


		// Set organization chart nodes
		if ($input->orgchart) {
			$branches = explode(";", $input->orgchart);
			if (is_array($branches)) {
				foreach ($branches as $branch) {
					$idOrg = $this->_getBranch($branch);
					if (!empty($idOrg))
						$user->chartGroups[] = (int)$idOrg;
				}
			}
		}

		//add organization chart root node by default
		$_roots = CoreOrgChartTree::model()->roots()->findAll();
		if (!empty($_roots)) {
			$rootNode = $_roots[0];
			$user->chartGroups[] = $rootNode->getPrimaryKey();
		}

		// Event raised for YnY app to hook on to
		Yii::app()->event->raise('BeforeUserModified', new DEvent($this, array(
			'context' => $this,
			'user' => $user,
		)));

		$transaction = Yii::app()->db->beginTransaction();

		try {
			if (!$user->save()) {
				Yii::log('Received user attributes:', CLogger::LEVEL_ERROR);
				Yii::log(print_r($user->getAttributes(), true), CLogger::LEVEL_ERROR);
				Yii::log(print_r($user->getErrors(), true), CLogger::LEVEL_ERROR);

				throw new CHttpException(203, 'Error while updating user');
			}

			// Language setting
			if($input->language) {
				$languageSetting = CoreSettingUser::model()->findByAttributes(array(
					'path_name' => 'ui.language',
					'id_user' => $user->idst
				));

				if (!$languageSetting) {
					$languageSetting = new CoreSettingUser('create');
					$languageSetting->path_name = 'ui.language';
					$languageSetting->id_user = $user->idst;
				}

				$languageSetting->value = $input->language;
				if (!$languageSetting->save())
					throw new CHttpException(203, 'Error while updating user');
			}

			// Assign the user to the orgchart nodes provided
			if($input->orgchart)
				$user->saveChartGroups();

			// Let's see if the API caller has passed id_group, If so let's add the user to that group as well
			// NOTE: if the group is fake, non existing in DB we wouldn't be here
			if($input->id_group){
				$userGroup = new CoreGroupMembers();
				$userGroup->idst = $input->id_group;
				$userGroup->idstMember = $user->idst;
				$userGroup->save();
			}

			// Update ext_user data
			if ($input->ext_user_type != '' && $input->ext_user != '') {
				$path_name = 'ext.user.' . $input->ext_user_type;
				$path_value = 'ext_user_' . $input->ext_user_type . "_" . $input->ext_user;

				$settingUser = CoreSettingUser::model()->findByAttributes(array(
					'path_name' => $path_name,
					'id_user' => $user->idst
				));

				if ($settingUser)
					$settingUser->value = $path_value;
				else {
					$settingUser = new CoreSettingUser();
					$settingUser->value = $path_value;
					$settingUser->path_name = $path_name;
					$settingUser->id_user = $user->idst;
				}

				if (!$settingUser->save())
					throw new CHttpException(203, 'Error while updating user');
			}

			// Save timezone setting (if provided)
			if($input->timezone) {
				$timezoneSettingUser = CoreSettingUser::model()->findByAttributes(array(
					'path_name' => 'timezone',
					'id_user' => $user->idst
				));

				if ($timezoneSettingUser)
					$timezoneSettingUser->value = $input->timezone;
				else {
					$timezoneSettingUser = new CoreSettingUser('create');
					$timezoneSettingUser->path_name = 'timezone';
					$timezoneSettingUser->id_user = $user->idst;
					$timezoneSettingUser->value = $input->timezone;
				}

				if (!$timezoneSettingUser->save())
					throw new CHttpException(254, "Cannot save user: " . $this->errorSummary($timezoneSettingUser));
			}

			// Saving additional fields
			$additionalFieldsChanged = false;
			if (!empty($input->fields)) {
				$additionalFields = $this->readAdditionalFields($input->fields, false); //NOTE: uncompiled fields are left unchanged
				if (!empty($additionalFields)) {

                    // Loading user values
                    $userValues = CoreUserField::getValuesForUsers([$user->idst]);
                    if (!$userValues) {
                        $userValues = new CoreUserFieldValue;
                        $userValues->id_user = $user->idst;
                    } else {
                        $userValues = current($userValues);
                    }

                    foreach ($additionalFields as $idField => $additionalField) {
                        $column = 'field_' . $idField;

                        if (isset($userValues, $column) === true) {
                            if($userValues->{$column} != $additionalField['value']){
                                $additionalFieldsChanged = true;
                            }
                            $userValues->{$column} = $additionalField['value'];
                        }
                    }

                    if (!$userValues->save())
                    {
                        throw new CHttpException(203, 'Error while updating user');
                    }
				}
			}

			if($additionalFieldsChanged == true){
				$hasSelfPopulatingGroups = Yii::app()->db->createCommand("SELECT COUNT(*) FROM core_group WHERE assign_rules = 1")->queryScalar();
				if($hasSelfPopulatingGroups > 0) {
					$scheduler = Yii::app()->scheduler;
					$params = array(
						'user_idst' => $user->idst,
						'offset' => 0
					);
					$scheduler->createImmediateJob(GroupAutoAssignTask::JOB_NAME, GroupAutoAssignTask::id(), $params);
				}
			}

			//Unenroll the user if is Deactivated
			if($input->unenroll_deactivated && !$input->valid){
				$this->unenrollUserFromAllSessions($input->idst);
			}

			// Raise event
			Yii::app()->event->raise('UserModified', new DEvent($this, array(
				'user' => $user,
			)));

			$transaction->commit();
		} catch (CHttpException $e) {
			$transaction->rollback();
			throw $e;
		} catch (Exception $e) {
			$transaction->rollback();
			throw new CHttpException(500, $e->getMessage());
		}

		return array('idst' => (int)$user->idst);
	}


	/**
	 * @summary Retrieve additional fields list
	 * @notes Every returned field is composed by its numerical ID (id_common) and textual name translation
	 *
	 * @parameter language [string, optional] The language for fields names translations
	 *
	 * @response fields [array, required]
     *      @item id [integer, required] ID of the additional field
     *      @item name [string, required] Name (translated) of the field
	 */
	public function fields() {

		//detect language for fields translations
		$inputLanguage = $this->readParameter('language', false);
		$language = ($inputLanguage ? $inputLanguage : CoreUser::getDefaultLangCode());

		//retrieve database info
		$criteria = new CDbCriteria();
		$criteria->order = "translation ASC";
		$list = CoreUserField::model()->language($language, true)->findAll($criteria);

		//prepare output
		$output = array();
		if (!empty($list)) {
			foreach ($list as $item) {
                /** @var CoreUserField $item */
				$output[] = array(
					'id' => (int) $item->getFieldId(),
					'name' => $item->getTranslation($language)
				);
			}
		}

		return array('fields' => $output);
	}


	/**
	 * @summary Retrieves a list of all users in the platform
	 * @notes Users are sorted by userid in ascending order.
	 *
	 * @parameter from [integer, optional] pagination offset (default = 0)
	 * @parameter count [integer, optional] how many records are required for pagination page size (default = all records)
	 *
	 * @response users [array, required] list of users objects
     *      @item id_user [integer, required] Internal Docebo ID of the user
     *      @item userid [string, required] Username of the user
     *      @item firstname [string, required] Firstname of the user
     *      @item lastname [string, required] Lastname of the user
     *      @item email [string, required] Email of the user
     *      @item valid [boolean, required] Is this user active or suspended?
	 */
	public function listUsers() {

		// Read input parameters
		$from = $this->readParameter('from', null);
		$count = $this->readParameter('count', null);

		/* @var $db CDbConnection */
		/* @var $cmd CDbCommand */
		/* @var $webUser DoceboWebUser */
		$db = Yii::app()->db;
		$webUser = Yii::app()->user;

		//retrieve all godadmins
		$godAdmins = $webUser->getAllGodadmins(true);

		//prepare user retrieval query
		$params = array(':anonymous' => Yii::app()->user->getAbsoluteUsername('Anonymous'));
		$cmd = $db->createCommand()
			->select('u.idst, u.userid, u.firstname, u.lastname, u.email, u.valid')
			->from(CoreUser::model()->tableName()." u")
			->where("u.userid <> :anonymous")
			->order("u.userid ASC");
		if (!is_null($from) || !is_null($count)) {
			if (empty($count)) { $count = 0; } //just to be sure to have a valid value
			if ($count > 0) {
				if (empty($from)) { $from = null; } //just to be sure to have the right value
				$cmd->limit($count, $from);
			} elseif (is_numeric($from) && $from > 0) {
				$cmd->offset($from);
			}
		}
		$reader = $cmd->query($params);

		//fetch data and prepare output
		$output = array();
		while ($record = $reader->read()) {
			if (!empty($godAdmins) && !in_array($record['idst'], $godAdmins)) { //exclude god admins from the retrieval
				$output[] = array(
					'id_user' => (int)$record['idst'],
					'userid' => $webUser->getRelativeUsername($record['userid']),
					'firstname' => $record['firstname'],
					'lastname' => $record['lastname'],
					'email' => $record['email'],
					'valid' => $record['valid']
				);
			}
		}

		return array('users' => $output);
	}


	/**
	 * @summary Suspend a user
	 *
	 * @parameter id_user [integer, optional] Internal Docebo ID for the user to be suspended
	 * @parameter ext_user_type [integer, optional] User id on the external platform
	 * @parameter ext_user [string, optional] External authentication type (e.g. drupal, joomla, …)
	 * @parameter unenroll_deactivated [boolean, optional] Unenroll user from all future Classroom and Webinar sessions
	 *
	 * @status 210 Invalid user specification
     * @status 211 Error occurred while suspending user
	 *
	 * @response idst [integer] the idst of the user that was suspended
	 */
	public function suspend() {

		// Read input
		$input = new stdClass();
		$input->idst  = $this->readParameter('idst', $this->readParameter('id_user'));
		$input->ext_user_type = $this->readParameter('ext_user_type', '');
		$input->ext_user = $this->readParameter('ext_user', '');
		$input->unenroll_deactivated = $this->readParameter('unenroll_deactivated', false);
		$input->unenroll_deactivated = $input->unenroll_deactivated === true || $input->unenroll_deactivated === 'true' ? 1 : $input->unenroll_deactivated;
		$input->unenroll_deactivated = $input->unenroll_deactivated !== false && $input->unenroll_deactivated !== 'false' ? ($input->unenroll_deactivated > 0 ? 1 : 0) : 0;

		if (!$input->idst) {
			//try to search for external user
			$input->idst = self::_getExternalUserIdst($input->ext_user, $input->ext_user_type);
		}

		//validate input
		if (!$input->idst)
			throw new CHttpException(210, 'Invalid user specification');

		//validate input data
		$user = CoreUser::model()->findByPk($input->idst);
		if (!$user)
			throw new CHttpException(210, 'Invalid user specification');

		if (self::_isGodAdmin($input->idst))
			throw new CHttpException(210, 'Invalid user specification');

		//do operation
		try {
			$user->setScenario('deactivate');
			if($user->deactivate()){
				if($input->unenroll_deactivated){
					$this->unenrollUserFromAllSessions($user->idst);
				}
			}
		} catch (Exception $e) {
			throw new CHttpException(211, 'Error occurred while suspending user');
		}

		return array('idst' => $user->idst);
	}


	/**
	 * @summary Recovers a suspended user
	 *
     * @parameter id_user [integer, optional] Internal Docebo ID for the user to be recovered
     * @parameter ext_user_type [integer, optional] User id on the external platform
     * @parameter ext_user [string, optional] External authentication type (e.g. drupal, joomla, …)
     *
     * @status 210 Invalid user specification
     * @status 211 Error occurred while unsuspending user
	 *
	 * @response idst [integer] the idst of the user that has been recovered
	 */
	public function unsuspend() {
		// Read input
		$input = new stdClass();
		$input->idst  = $this->readParameter('idst', $this->readParameter('id_user'));
		$input->ext_user_type = $this->readParameter('ext_user_type', '');
		$input->ext_user = $this->readParameter('ext_user', '');

		if (!$input->idst) {
			//try to search for external user
			$input->idst = self::_getExternalUserIdst($input->ext_user, $input->ext_user_type);
		}

		// Validate input
		if (!$input->idst)
			throw new CHttpException(210, 'Invalid user specification');

		// Validate input data
		$user = CoreUser::model()->findByPk($input->idst);
		if (!$user)
			throw new CHttpException(210, 'Invalid user specification');

		if (self::_isGodAdmin($input->idst))
			throw new CHttpException(210, 'Invalid user specification');

		//do operation
		try {
			$user->setScenario('activate');
			$user->activate();
		} catch (Exception $e) {
			throw new CHttpException(211, 'Error occurred while unsuspending user');
		}

		return array('idst' => $user->idst);
	}


	/**
	 * @summary Retrieve the number of users present in the platform
	 * @notes Godadmins are not counted
	 *
	 * @parameter status [string, optional] Filter on user status. Possible values: [active|suspended|all]
	 *
	 * @response count [integer, required] The number of users in the system (filtered by status)
	 */
	public function count() {
		//read input
		$status = trim(strtolower($this->readParameter('status', 'all')));
        if(!in_array($status, array('active', 'suspended', 'all')))
            $status = 'all';

		// Retrieve all godadmins. These shouldn't be counted
		$godAdminsList = array();
		$criteria = new CDbCriteria();
		$criteria->addCondition("groupid LIKE :groupid");
		$criteria->params[':groupid'] = '/framework/level/godadmin';
		$list = CoreGroup::model()->findAll($criteria);
		if (!empty($list)) {
			foreach ($list as $item)
				$godAdminsList[] = (int)$item->idst;
		}

		// Count users
		$command = Yii::app()->db->createCommand()
			->select("COUNT(*) AS tot")
			->from(CoreUser::model()->tableName())
			->where("userid <> :anonymous", array(':anonymous' => Yii::app()->user->getAbsoluteUsername('Anonymous')));
		if (!empty($godAdminsList))
			$command->andWhere(array("NOT IN", 'idst', $godAdminsList));

		switch($status) {
			case 'active':
				$command->andWhere("valid = 1");
				break;
			case 'suspended':
				$command->andWhere("valid = 0");
				break;
		}
		$count = $command->queryScalar();

		return array('count' => (int)$count);
	}


	/**
	 * @summary Retrieve user profile info
	 * @notes Godadmins are not retrievable for strong authentication.<br />
	 * @notes Godadmins are retrievable for lightweight authentication when current user is itself (godadmin)
	 *
     * @parameter id_user [integer, optional] Internal Docebo ID for the user
     * @parameter ext_user_type [integer, optional] User id on the external platform
     * @parameter ext_user [string, optional] External authentication type (e.g. drupal, joomla, …)
	 *
     * @status 201 Invalid user specification
     *
	 * @response id_user [integer, required] Internal Docebo ID for the user
     * @response userid [string, required] Login username
     * @response firstname [string, required] User's firstname
     * @response lastname [string, required] User's lastname
     * @response email [string, required] Email address
     * @response signature [string, required] User's signature
     * @response valid [boolean, required] Is this user suspended or not?
     * @response register_date [datetime, required] The registration date
     * @response last_enter [datetime, required] The last login by this user
     * @response fields [array, required] The set of additional fields for the user
     *      @item id [integer, required] Internal fields ID
     *      @item name [string, required] Field name
     *      @item value [string, required] Field value
	 */
	public function profile() {

		//read input
		$input = new stdClass();
		$input->idst  = $this->readParameter('idst');
		if(!$input->idst)
			$input->idst = $this->readParameter('id_user');
		if(!$input->idst)
			$input->idst = $this->readParameter('login_user');

		$input->ext_user_type = $this->readParameter('ext_user_type', '');
		$input->ext_user = $this->readParameter('ext_user', '');

		if (!$input->idst) {
			//try to search for external user
			$input->idst = self::_getExternalUserIdst($input->ext_user, $input->ext_user_type);
		}

		//validate input
		if (!$input->idst)
			throw new CHttpException(201, 'Invalid user specification');

		$user = CoreUser::model()->findByPk($input->idst);
		if (!$user)
			throw new CHttpException(201, 'Invalid user specification');

		if (self::_isGodAdmin($input->idst)) {
			if (!$this->user && ($input->idst != $this->user))
				throw new CHttpException(201, 'Invalid user specification');
		}

		//prepare output data
		$output = array(
			'id_user' => $user->idst,
			'userid' => Yii::app()->user->getRelativeUsername($user->userid),
			'firstname' => $user->firstname,
			'lastname' => $user->lastname,
			'email' => $user->email,
			'signature' => $user->signature,
			'valid' => (bool)$user->valid,
			'register_date' => $user->register_date,
			'last_enter' => $user->lastenter,
		);

		// Event raised for YnY app to hook on to
		Yii::app()->event->raise('GetUserProfile', new DEvent($this, array('output' => &$output, 'user' => &$user)));

		//retrieve custom fields
		$output['fields'] = array();
		$userFields = $user->getAdditionalFields(true);
		if (!empty($userFields)) {
			foreach ($userFields as $i => $field) {
                /** @var CoreUserField $field */
                $output['fields'][] = array(
                    'id' => $field->getFieldId(),
                    'name' => $field->getTranslation(),
                    'value' => $field->userEntry
                );
			}
		}

		return $output;
	}


	/**
	 * @summary Retrieves user profile info
	 * @notes Godadmins are not retrievable.
	 *
	 * @parameter userid [string, required] Docebo username for the user to be profiled
	 * @parameter password [string, optional] the user password
	 * @parameter password_encoded [string, optional] user password already encrypted
	 *
     * @status 201 Empty userid or password
     * @status 202 Invalid specified user
     * @status 203 Invalid user specification
     *
	 * @response id_user [integer, required] Internal Docebo ID for the user
     * @response userid [string, required] Login username
     * @response firstname [string, required] User's firstname
     * @response lastname [string, required] User's lastname
     * @response email [string, required] Email address
     * @response signature [string, required] User's signature
     * @response valid [boolean, required] Is this user suspended or not?
     * @response register_date [datetime, required] The registration date
     * @response last_enter [datetime, required] The last login by this user
     * @response fields [array, required] The set of additional fields for the user
     *      @item id [integer, required] Internal fields ID
     *      @item name [string, required] Field name
     *      @item value [string, required] Field value
	 */
	public function profile_credential() {
		// Read input
		$input = new stdClass();
		$input->userid = $this->readParameter('userid');
		$input->password = $this->readParameter('password');
		$input->password_encoded = $this->readParameter('password_encoded');

		// Validate input
		if (!$input->userid || (!$input->password && !$input->password_encoded))
			throw new CHttpException(201, 'Empty userid or password');

		// Encrypt password
		if (!$input->password_encoded)
			$input->password_encoded = CoreUser::model()->encryptPassword($input->password);

		// Check user existence
		$user = CoreUser::model()->findByAttributes(array(
			'userid' => Yii::app()->user->getAbsoluteUsername($input->userid),
			'pass' => $input->password_encoded
		));

		if (!$user)
			throw new CHttpException(202, 'Invalid specified user');

		// Don't allow to retrieve data about godadmins
		if (self::_isGodAdmin($user->idst))
			throw new CHttpException(203, 'Invalid user specification');

		// Prepare output data
		$output = array(
			'id_user' => $user->idst,
			'userid' => Yii::app()->user->getRelativeUsername($user->userid),
			'firstname' => $user->firstname,
			'lastname' => $user->lastname,
			'email' => $user->email,
			'signature' => $user->signature,
			'valid' => (bool)$user->valid,
			'register_date' => $user->register_date,
			'last_enter' => $user->lastenter,
		);

		//retrieve custom fields
		$output['fields'] = array();
        $userFields = $user->getAdditionalFields(true);
        if (!empty($userFields)) {
            foreach ($userFields as $i => $field) {
                /** @var CoreUserField $field */
                $output['fields'][] = array(
                    'id' => $field->getFieldId(),
                    'name' => $field->getTranslation(),
                    'value' => $field->userEntry
                );
            }
        }

        return $output;
	}


	/**
	 * @summary Retrieve user profile image
	 * @notes Godadmins are not retrievable.
	 *
	 * @parameter id_user [integer, optional] Docebo internal ID of the user
	 * @parameter userid [string, optional] Docebo username for the user to be profiled
	 * @parameter email [string, optional] the user email
	 *
     * @status 201 Invalid user specification
     * @status 207 No profile image for user
     *
	 * @response image_url [string, required] the link to the user's photo
	 */
	public function profile_image() {

		//read input
		$input = new stdClass();
		$input->idst  = $this->readParameter('idst', $this->readParameter('id_user'));
		$input->userid = $this->readParameter('userid', false);
		$input->email = $this->readParameter('email', false);

		//retrieve user info
		$user = false;
		if ($input->idst !== false && $input->idst > 0) {
			$user = CoreUser::model()->findByPk($input->idst);
		} elseif ($input->userid !== false && $input->userid != '') {
			$user = CoreUser::model()->findByAttributes(array('userid' => Yii::app()->user->getAbsoluteUsername($input->userid)));
		} elseif ($input->email !== false && filter_var($input->email, FILTER_VALIDATE_EMAIL) !== false) {
			$user = CoreUser::model()->findByAttributes(array('email' => $input->email));
		}

		if (!$user) {
			throw new CHttpException(201, 'Invalid user specification');
		}

		//does the user have an avatar image?
		if (empty($user->avatar)) {
			throw new CHttpException(207, 'No profile image for user');
		}

		//return image url
		return array('image_url' => $user->getAvatarImage(true));
	}


	/**
	 * @summary [DEPRECATED, see user/enrollments] Returns the list of courses the user is enrolled in, with related information and stats.
	 *
	 * @parameter id_user [integer, optional] The internal Docebo ID for the user
     * @parameter ext_user_type [integer, optional] User id on the external platform
     * @parameter ext_user [string, optional] External authentication type (e.g. drupal, joomla, …)
	 * @parameter elearning [integer, optional] If set to 1 only elearning courses are returned
	 * @parameter classroom [integer, optional] If set to 1 only classroom courses are returned
	 *
	 * @status 210 Invalid user specification
	 *
	 * @response courses [array, required] The array of courses this user is enrolled in
     *      @item course_id [integer, required] The ID of the course
     *      @item course_name [string, required] The title of the course
     *      @item course_description [string, required] Course description (e.g. HTML)
     *      @item can_enter [object, required] If the user can access the course at the time of the call
     *          @item can [boolean, required] True if user can access course, false otherwise
     *          @item reason [string, required] The reason why the user cannot access. Possible values: 'prerequisites', 'waiting', 'course_date', 'course_valid_time', 'user_status', 'course_status', 'subscription_not_started', 'subscription_expired'
     *          @item expiring_in [datetime, required] The day remaining before the course expires
     *      @end can_enter
     *      @item course_link [string, required] A deep link to enter the course
     *      @item course_thumbnail [string, required] The URL to the course cover
     *      @item courseuser_status [integer, required] The course status (-2 waiting user, -1 subscription to confirm, 0 = not started, 1 = in progress, 2 = completed)
     *      @item course_type [string, required] The course type (elearning, platform)
     *      @item course_progress [decimal, required] The percentage of training materials completed by this user
	 *      @item date_enroll [datetime, required] The date the user was enrolled in the course (yyyy-mm-ddTHH:mm:ss+0000 format, UTC timezone)
	 *      @item first_access [datetime, required] The first access date for the user in this course (yyyy-mm-ddTHH:mm:ss+0000 format, UTC timezone)
	 *		@item last_access [datetime, required] The last access date for the user in this course (yyyy-mm-ddTHH:mm:ss+0000 format, UTC timezone)
	 *      @item date_complete [datetime, required] The completion date (if course is completed) for the user (yyyy-mm-ddTHH:mm:ss+0000 format, UTC timezone)
	 *      @item final_score [decimal, required] The user's final score in the course
	 * 		@item total_time [integer, required] Total time spent in course (in <b>x</b>h <b>y</b>m <b>z</b>s, where <b>x</b> is the number of hours, <b>y</b> is the number of minutes and <b>z</b> is the number of seconds)
	 */
	public function userCourses() {

		$idUser        = $this->readParameter('idst', $this->readParameter('id_user'));
		$ext_user      = $this->readParameter('ext_user');
		$ext_user_type = $this->readParameter('ext_user_type');
		$elearning     = $this->readParameter('elearning', false);
		$classroom     = $this->readParameter('classroom', false);


		//validate input
		if (!$idUser) {
			// Try to search for the user by the external user ID
			$idUser = self::_getExternalUserIdst($ext_user, $ext_user_type);
			if(!$idUser){
				Yii::log("userCourses api params:" . file_get_contents("php://input"), CLogger::LEVEL_ERROR);
				throw new CHttpException(210, 'Invalid user specification');
			}
		}

		$userModel = CoreUser::model()->findByPk($idUser);
		if(!$userModel) {
			// User not present in platform
			Yii::log("userCourses api params:" . file_get_contents("php://input"), CLogger::LEVEL_ERROR);
			throw new CHttpException(210, 'Invalid user specification');
		}

		//retrieve data
		$params = array(':id_user' => $idUser);
		if ($elearning)  $params[':elearning']  = LearningCourse::TYPE_ELEARNING;
		if ($classroom)  $params[':classroom']  = LearningCourse::TYPE_CLASSROOM;

        $courses = LearningCourse::model()->with('learningCourseusers')->findAll('learningCourseusers.idUser = :id_user '
	        . ( $elearning  ? ' AND course_type = :elearning'  : '' )
	        . ( $classroom  ? ' AND course_type = :classroom'  : '' ),
		$params);

		$output = array();
		foreach ($courses as $course) {
            /* @var $course LearningCourse */
			$link = Docebo::createAbsoluteLmsUrl('player', array('course_id' => $course->idCourse));
			$thumbnail = $course->getCourseLogoUrl();

			/* @var $enrollment LearningCourseuser */
			$enrollment =  LearningCourseuser::model()->findByAttributes(array('idCourse' => $course->idCourse, 'idUser' => $idUser));
			if($enrollment)
				$canEnter = $enrollment->canEnterCourse();
			else
				$canEnter = array('can' => false);

			$thumbnail = str_ireplace('http://http://', '//', $thumbnail);
			$thumbnail = str_ireplace('https://https://', '//', $thumbnail);

			$thumbnail = str_ireplace('http://', '//', $thumbnail);
			$thumbnail = str_ireplace('https://', '//', $thumbnail);

            // Find course progress
			$percentage = LearningOrganization::getProgressByUserAndCourse($course->idCourse, $idUser);

			// Find course session time for this user
			$row = Yii::app()->db->createCommand("
				SELECT SUM(UNIX_TIMESTAMP(lastTime) - UNIX_TIMESTAMP(enterTime)) AS sessionTime
				FROM learning_tracksession
				WHERE idCourse = :idCourse and idUser = :idUser
				GROUP BY idUser")
				->bindValue(':idCourse', $course->idCourse)
				->bindValue(':idUser', $idUser)
				->queryRow();

			$finalScore = LearningCourseuser::getLastScoreByUserAndCourse($course->idCourse, $idUser, false);
			if($finalScore=='-')
				$finalScore = 0;

			$output[]['course_info'] = array (
				'course_id' => (int) $course->idCourse,
				'course_name' => str_replace('&', '&amp;', $course->name),
				'course_description' => str_replace('&', '&amp;', $course->description),
				'can_enter' => $canEnter,
				'course_link' => $link,
				'course_thumbnail' => $thumbnail,
				'courseuser_status' => $enrollment ? $enrollment->status : 0,
				'course_type' => $course->course_type,
                'course_progress'   => $percentage,
				'date_enroll' => $enrollment->date_inscr ? Yii::app()->localtime->toUTC($enrollment->date_inscr) : null,
				'date_complete' => $enrollment->date_complete ? Yii::app()->localtime->toUTC($enrollment->date_complete) : null,
				'first_access' => $enrollment->date_first_access ? Yii::app()->localtime->toUTC($enrollment->date_first_access) : null,
				'last_access' => $enrollment->date_last_access ? Yii::app()->localtime->toUTC($enrollment->date_last_access) : null,
				'final_score'=>$finalScore,
				'total_time' => $row ? Yii::app()->localtime->hoursAndMinutes($row['sessionTime']) : null
			);
		}

		return $output;
	}

	/**
	 * @summary Returns the list of courses the user is enrolled in, with related information and stats.
	 *
	 * @parameter id_user [integer, optional] The internal Docebo ID for the user
	 * @parameter ext_user_type [integer, optional] User id on the external platform
	 * @parameter ext_user [string, optional] External authentication type (e.g. drupal, joomla, …)
	 * @parameter elearning [integer, optional] If set to 1 only elearning courses are returned
	 * @parameter classroom [integer, optional] If set to 1 only classroom courses are returned
	 *
	 * @status 210 Invalid user specification
	 *
	 * @response courses [array, required] The array of courses this user is enrolled in
	 *      @item course_id [integer, required] The ID of the course
	 *      @item course_name [string, required] The title of the course
	 *      @item course_description [string, required] Course description (e.g. HTML)
	 *      @item can_enter [object, required] If the user can access the course at the time of the call
	 *          @item can [boolean, required] True if user can access course, false otherwise
	 *          @item reason [string, required] The reason why the user cannot access. Possible values: 'prerequisites', 'waiting', 'course_date', 'course_valid_time', 'user_status', 'course_status', 'subscription_not_started', 'subscription_expired'
	 *          @item expiring_in [datetime, required] The day remaining before the course expires
	 *      @end can_enter
	 *      @item course_link [string, required] A deep link to enter the course
	 *      @item course_thumbnail [string, required] The URL to the course cover
	 *      @item courseuser_status [integer, required] The course status (-2 waiting user, -1 subscription to confirm, 0 = not started, 1 = in progress, 2 = completed)
	 *      @item course_type [string, required] The course type (elearning, platform)
	 *      @item course_progress [decimal, required] The percentage of training materials completed by this user
	 *      @item date_enroll [datetime, required] The date the user was enrolled in the course (yyyy-mm-ddTHH:mm:ss+0000 format, UTC timezone)
	 *      @item first_access [datetime, required] The first access date for the user in this course (yyyy-mm-ddTHH:mm:ss+0000 format, UTC timezone)
	 *		@item last_access [datetime, required] The last access date for the user in this course (yyyy-mm-ddTHH:mm:ss+0000 format, UTC timezone)
	 *      @item date_complete [datetime, required] The completion date (if course is completed) for the user (yyyy-mm-ddTHH:mm:ss+0000 format, UTC timezone)
	 *      @item final_score [decimal, required] The user's final score in the course
	 * 		@item total_time [integer, required] Total time spent in course (in <b>x</b>h <b>y</b>m <b>z</b>s, where <b>x</b> is the number of hours, <b>y</b> is the number of minutes and <b>z</b> is the number of seconds)
	 */
	public function enrollments(){
		$idUser        = $this->readParameter('idst', $this->readParameter('id_user'));
		$ext_user      = $this->readParameter('ext_user');
		$ext_user_type = $this->readParameter('ext_user_type');
		$elearning     = $this->readParameter('elearning', false);
		$classroom     = $this->readParameter('classroom', false);


		//validate input
		if (!$idUser) {
			// Try to search for the user by the external user ID
			$idUser = self::_getExternalUserIdst($ext_user, $ext_user_type);
			if(!$idUser){
				Yii::log("userCourses api params:" . file_get_contents("php://input"), CLogger::LEVEL_ERROR);
				throw new CHttpException(210, 'Invalid user specification');
			}
		}

		$userModel = CoreUser::model()->findByPk($idUser);
		if(!$userModel) {
			// User not present in platform
			Yii::log("userCourses api params:" . file_get_contents("php://input"), CLogger::LEVEL_ERROR);
			throw new CHttpException(210, 'Invalid user specification');
		}

		//retrieve data
		$params = array(':id_user' => $idUser);
		if ($elearning)  $params[':elearning']  = LearningCourse::TYPE_ELEARNING;
		if ($classroom)  $params[':classroom']  = LearningCourse::TYPE_CLASSROOM;

		$courses = LearningCourse::model()->with('learningCourseusers')->findAll('learningCourseusers.idUser = :id_user '
			. ( $elearning  ? ' AND course_type = :elearning'  : '' )
			. ( $classroom  ? ' AND course_type = :classroom'  : '' ),
		$params);

		$output = array();
		foreach ($courses as $course) {
			/* @var $course LearningCourse */
			$link = Docebo::createAbsoluteLmsUrl('player', array('course_id' => $course->idCourse));
			$thumbnail = $course->getCourseLogoUrl();

			/* @var $enrollment LearningCourseuser */
			$enrollment =  LearningCourseuser::model()->findByAttributes(array('idCourse' => $course->idCourse, 'idUser' => $idUser));
			if($enrollment)
				$canEnter = $enrollment->canEnterCourse();
			else
				$canEnter = array('can' => false);

			$thumbnail = str_ireplace('http://http://', '//', $thumbnail);
			$thumbnail = str_ireplace('https://https://', '//', $thumbnail);

			$thumbnail = str_ireplace('http://', '//', $thumbnail);
			$thumbnail = str_ireplace('https://', '//', $thumbnail);

			// Find course progress
			$percentage = LearningOrganization::getProgressByUserAndCourse($course->idCourse, $idUser);

			// Find course session time for this user
			$row = Yii::app()->db->createCommand("
				SELECT SUM(UNIX_TIMESTAMP(lastTime) - UNIX_TIMESTAMP(enterTime)) AS sessionTime
				FROM learning_tracksession
				WHERE idCourse = :idCourse and idUser = :idUser
				GROUP BY idUser")
								 ->bindValue(':idCourse', $course->idCourse)
								 ->bindValue(':idUser', $idUser)
								 ->queryRow();

			$finalScore = LearningCourseuser::getLastScoreByUserAndCourse($course->idCourse, $idUser, false);
			if($finalScore=='-')
				$finalScore = 0;

			$output[] = array (
				'course_id' => (int) $course->idCourse,
				'course_name' => str_replace('&', '&amp;', $course->name),
				'course_description' => str_replace('&', '&amp;', $course->description),
				'can_enter' => $canEnter,
				'course_link' => $link,
				'course_thumbnail' => $thumbnail,
				'courseuser_status' => $enrollment ? $enrollment->status : 0,
				'course_type' => $course->course_type,
				'course_progress'   => $percentage,
				'date_enroll' => $enrollment->date_inscr ? Yii::app()->localtime->toUTC($enrollment->date_inscr) : null,
				'date_complete' => $enrollment->date_complete ? Yii::app()->localtime->toUTC($enrollment->date_complete) : null,
				'first_access' => $enrollment->date_first_access ? Yii::app()->localtime->toUTC($enrollment->date_first_access) : null,
				'last_access' => $enrollment->date_last_access ? Yii::app()->localtime->toUTC($enrollment->date_last_access) : null,
				'final_score'=>$finalScore,
				'total_time' => $row ? Yii::app()->localtime->hoursAndMinutes($row['sessionTime']) : null
			);
		}

		return array('courses'=>$output);
	}


	/**
	 * @summary Retrieve user groups and organization chart folders associations
	 * @notes Groups and folders are returned in separate arrays
	 *
	 * @parameter id_user [integer, optional] numeric Docebo internal ID of the user
	 * @parameter userid [string, optional] Docebo username for the user to be profiled
	 * @parameter email [string, optional] the user email
	 * @parameter language [string, optional] translation language for organization chart folders (if not specified, default language is used)
	 *
     * @status 201 Invalid user specification
     *
	 * @response results [object, required] Result object
     *      @item groups [array(string), required] Array of group names the user is member of
     *      @item folders [array(string), required] Array of org chart folders the user is assigned to
	 */
	public function group_associations() {

		//read input
		$input = new stdClass();
		$input->idst  = $this->readParameter('idst', $this->readParameter('id_user'));
		$input->userid = $this->readParameter('userid', false);
		$input->email = $this->readParameter('email', false);
		$input->language = $this->readParameter('language', CoreUser::getDefaultLangCode());

		//retrieve user info
		$user = false;
		if ($input->idst !== false && $input->idst > 0) {
			$user = CoreUser::model()->findByPk($input->idst);
		} elseif ($input->userid !== false && $input->userid != '') {
			$user = CoreUser::model()->findByAttributes(array('userid' => Yii::app()->user->getAbsoluteUsername($input->userid)));
		} elseif ($input->email !== false && filter_var($input->email, FILTER_VALIDATE_EMAIL) !== false) {
			$user = CoreUser::model()->findByAttributes(array('email' => $input->email));
		}

		if (!$user)
			throw new CHttpException(201, 'Invalid user specification');

		//retrieve groups info
		$results = array('groups' => array(), 'folders' => array());
		$groupsList = Yii::app()->db->createCommand()
			->select("g.*")
			->from(CoreGroup::model()->tableName() . " g")
			->join(CoreGroupMembers::model()->tableName() . " gm", "gm.idst = g.idst AND gm.idstMember = :idst", array(':idst' => $user->idst))
			->where("g.hidden = 'false' OR g.groupid LIKE :oc_prefix", array(':oc_prefix' => '/oc_%'))
			->andWhere("g.groupid <> :oc0_prefix", array(':oc0_prefix' => '/oc_0'))
			->queryAll();

        foreach ($groupsList as $groupsItem) {
            if (strpos($groupsItem['groupid'], '/ocd_') === false) { //security check to prevent orgchart descendants groups
                if (strpos($groupsItem['groupid'], '/oc_') !== false) {
                    //orgchart folders
                    $results['folders'][] = $this->_getOrgchartPath($groupsItem['idst'], $input->language);
                } else {
                    //flat groups
                    $results['groups'][] = CoreGroup::model()->relativeId($groupsItem['groupid']);
                }
            }
        }

        return array('results' => $results);
	}


	/**
	 * @summary Checks if an user is logged into platform
	 *
	 * @parameter id_user [integer, optional] numeric Docebo internal ID of the user
	 * @parameter userid [string, optional] Docebo username for the user to be profiled
	 * @parameter email [string, optional] the user email
	 *
     * @status 201 Invalid user specification
     *
	 * @response logged_in [boolean, required] True if the user is logged in, false otherwise
	 */
	public function user_logged_in() {

		//read input
		$input = new stdClass();
		$input->idst  = $this->readParameter('idst', $this->readParameter('id_user'));
		$input->userid = $this->readParameter('userid', false);
		$input->email = $this->readParameter('email', false);

		//retrieve user info
		$user = false;
		if ($input->idst !== false && $input->idst > 0) {
			$user = CoreUser::model()->findByPk($input->idst);
		} elseif ($input->userid !== false && $input->userid != '') {
			$user = CoreUser::model()->findByAttributes(array('userid' => Yii::app()->user->getAbsoluteUsername($input->userid)));
		} elseif ($input->email !== false && filter_var($input->email, FILTER_VALIDATE_EMAIL) !== false) {
			$user = CoreUser::model()->findByAttributes(array('email' => $input->email));
		}

		if (!$user)
			throw new CHttpException(201, 'Invalid user specification');

		//do the check
		$time = time(); // this is UTC
		$check = Yii::app()->db->createCommand()
			->select("COUNT(*) AS tot")
			->from(LearningTracksession::model()->tableName())
			->where("lastFunction <> '_LOGOUT'")
			->andWhere("active > 0")
			->andWhere("idUser = :id_user", array(':id_user' => $user->idst))
			->andWhere("lastTime >= :last_time", array(':last_time' => date('Y-m-d H:i:s', ($time-5*60))))
			->queryScalar();

		return array('logged_in' => ($check > 0));
	}



	/**
	 * @summary Associate a list of external user id and type with the corresponding Docebo users
     * @notes Userdata is an array, where every item contains a user specification (idst, userid or email)
	 *
	 * @parameter userdata [array, required] list of users to be associated
     *     @item ext_user [string, optional] External user ID
     *     @item ext_user_type [string, optional] External identification tpe (joomla, drupal, ..)
     *     @item userid [string, optional] Docebo login name
     *     @item idst [integer, optional] Internal Docebo ID
	 * @parameter from_email [boolean, optional] specify if json data uses email to define users or not
	 *
	 * @response sync [array, required] for every user to be associated, a result data is provided
     *      @item idst [integer, required] ID of the synched user
     *      @item username [string, required] Username of the synched user
	 */
	public function associateExternalUsers() {

		//read input
		$input = new stdClass();
		$input->userdata = $this->readParameter('userdata', array());
		$input->from_email = (bool)$this->readParameter('from_email', true);

		//do stuff
		$output = array();
		$counter = 0;
		foreach ($input->userdata as $userInfo) {
			$idUser = self::_getExternalUserIdst($userInfo['ext_user'], $userInfo['ext_user_type']);

			// if the user is not yet in sync..
			if (!$idUser) {
				$user = false;
				if (!$input->from_email) {
					// we search the user by userid or idst:
					$idst = (!empty($userInfo['idst']) ? $userInfo['idst'] : false);
					$userid = (!empty($userInfo['userid']) ? $userInfo['userid'] : false);
					if ($idst)
						$user = CoreUser::model()->findByPk($idst);
					elseif ($userid) {
						$user = CoreUser::model()->findByAttributes(array(
							'userid' => Yii::app()->user->getAbsoluteUsername($userid)
						));
					}
				} else { // we search the user by its e-mail:
					$user = CoreUser::model()->findByAttributes(array(
						'email' => $userInfo['email']
					));
				}

				// if found, we link the account to the external one:
				if ($user) {
					try {
                        if (self::_isGodAdmin($user->idst))
                            throw new CHttpException(210, 'Invalid user specification');

						$ar = new CoreSettingUser();
						$ar->id_user = $user->idst;
						$ar->path_name = 'ext.user.' . $userInfo['ext_user_type'];
						$ar->value = 'ext_user_' . $userInfo['ext_user_type'] . "_" . (int)$userInfo['ext_user'];
						if ($ar->save()) {
							$output['sync_' . $user->idst] = Yii::app()->user->getRelativeUsername($user->userid);
							$counter++;
						}
					} catch(CException $e) {
						Yii::log(print_r($e->getMessage(),true), CLogger::LEVEL_ERROR);
					}
				}
			}
		}

		return $output;
	}



	/**
	 * @summary Synchronize wordpress external users to LMS users
	 * @notes Associate a list of external WordPress users with corresponding Docebo users.
	 * @notes User data is provided as JSON text. If found in the LMS (first matched by username, then by email), the external
	 * @notes user is linked to the Docebo user. If not found, the user is created and associated.
	 *
	 * @parameter users [string, required] JSON-serialized list of users to be synchronized
	 *
	 * @response result [array, required] List of users processed, with the outcome of the sync operation
	 *     @item wp_user_id [integer, required] IDST of the user synced
	 *     @item success [boolean, required] True = success, False = failure
	 *     @item message [string, required] Operation result
	 */
	public function syncWordpressUsers()
	{

		set_time_limit(60 * 60);
		$output = array();
		$tmp = $this->readParameter('users');
		$userdata = json_decode(stripslashes($tmp));

		$i = 0;
		foreach ($userdata as $user_info) {
			$singleUserSyncStatus = array();

			try {
				$user_info = get_object_vars($user_info);

				// We start preparing the data to be returned by the API
				// (count of how much users were synced and how much failed, etc)
				$singleUserSyncStatus = array(
					'wp_user_id' => $user_info['wp_user_id'],
					'status' => false, // Will be changed below (note: false also means that the user already exists)
					'message' => 'A generic error occurred', // Will be changed below
				);

				$pref_path = 'ext.user.' . $user_info['ext_user_type'];
				$pref_val = 'ext_user_' . $user_info['ext_user_type'] . "_" . (int)$user_info['ext_user'];

				$userEmail = ((isset($user_info['email']) && $user_info['email']) ? $user_info['email'] : false);
				$userName = ((isset($user_info['wp_username']) && !empty($user_info['wp_username'])) ? $user_info['wp_username'] : false);

				$userNotSynchronized = true;

				// Check if the user is already synchronized
				$existingExternalAssociation = CoreSettingUser::model()->findByAttributes(array(
					'path_name' => $pref_path,
					'value' => $pref_val
				));
				if (!empty($existingExternalAssociation)) {
					$userNotSynchronized = false;
				}

				// if the user is not yet in sync..
				if ($userNotSynchronized) {
					$userid = Yii::app()->user->getAbsoluteUsername($userName);
					$criteria = new CDbCriteria();
					// Search for username first, fallback to email
					$criteria->compare('userid', $userid);
					if ($userEmail)
						$criteria->compare('email', $userEmail, false, 'OR');
					$user = CoreUser::model()->find($criteria);

					// if found, we link the account to the external one:
					if ($user) {
						if (self::_isGodAdmin($user->idst))
							throw new CHttpException(500, 'Invalid assignment');

						$newSetting = new CoreSettingUser();
						$newSetting->id_user = $user->idst;
						$newSetting->path_name = $pref_path;
						$newSetting->value = $pref_val;
						if (!$newSetting->save())
							throw new CHttpException(500, 'Error while creating user setting');

						$doceboUsername = Yii::app()->user->getRelativeUsername($user->userid);
						$singleUserSyncStatus['success'] = true;
						$singleUserSyncStatus['message'] = 'User exists in LMS with username ' . $doceboUsername . '. Associated.';

					} else {
						// The account doesn't exist in Docebo, create the user
						$newUser = new CoreUser('externalUserSync');
						$newUser->userid = Yii::app()->user->getAbsoluteUsername($userName);
						$newUser->firstname = $user_info['first_name'];
						$newUser->lastname = $user_info['last_name'];
						$newUser->email = $userEmail;

                        // Set organization chart nodes based on the extra orgchatrs param
                        if ($user_info['orgchart']) {
                            $branches = explode(";", $user_info['orgchart']);
                            if (is_array($branches)) {
                                foreach ($branches as $branch) {
                                    $idOrg = $this->_getBranch($branch);
                                    if (!empty($idOrg))
                                        $newUser->chartGroups[] = (int)$idOrg;
                                }
                            }
                        }

						if (!$newUser->save()) {
							Yii::log('Unable to save user via api. Attributes for user follow below:', CLogger::LEVEL_ERROR);
							Yii::log(print_r($newUser->getAttributes(), true), CLogger::LEVEL_ERROR);
							Yii::log('Validation errors for user:', CLogger::LEVEL_ERROR);
							Yii::log(print_r($newUser->getErrors(), true), CLogger::LEVEL_ERROR);
							throw new CHttpException(500, 'Error while creating user: ' . $userName);
						}

						$id_user_created = ($newUser ? $newUser->idst : false);
						if ($id_user_created) {

							//handle orgchart folder assignments (NOTE: this will automatically assign user to orgchart root node)
							$newUser->saveChartGroups();

							// Subscribe him to the default users group
							try {
								$auth = Yii::app()->authManager;
								$auth->assign(Yii::app()->user->level_user, $newUser->idst);
							} catch (Exception $e) {
								Yii::log('Can not assign user to users group. Maybe he is already there?', CLogger::LEVEL_ERROR);
								Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
							}

							// Link the newly created account with the external one
							$newSetting = new CoreSettingUser();
							$newSetting->id_user = $id_user_created;
							$newSetting->path_name = $pref_path;
							$newSetting->value = $pref_val;
							if ($newSetting->save()) {
								$singleUserSyncStatus['success'] = true;
								$singleUserSyncStatus['message'] = 'LMS user created and linked.';
							} else {
								$singleUserSyncStatus['success'] = false;
								$singleUserSyncStatus['message'] = 'LMS user created, but can not link him with WordPress user';
							}

						} else {
							$singleUserSyncStatus['success'] = false;
							$singleUserSyncStatus['message'] = 'Unable to create LMS user.';
						}
					}
					$i++;
				} else { // User is already synchronized to LMS
					$singleUserSyncStatus['success'] = false;
					$singleUserSyncStatus['message'] = 'User already synchronized';
				}

			} catch (Exception $e) {
				$singleUserSyncStatus['success'] = false;
				$singleUserSyncStatus['message'] = $e->getMessage();
			} catch (CHttpException $e) {
				$singleUserSyncStatus['success'] = false;
				$singleUserSyncStatus['message'] = $e->getMessage();
			}

			$output[] = $singleUserSyncStatus;
		}

		return array('result' => $output);
	}

	/**
	 * @summary Return a list of user detail about courses grouped by user (default) or course
	 * @notes Return a detailed list of user course subscription grouped by user (default) or course.<br />
	 * @notes id_courses and all_courses are optional but you are required to set all_courses to 1 or to set a valid course list for id_courses.<br />
	 * @notes id_list and id_org_list are optional but at least one of them must be set.
     *
	 * @parameter id_courses [string, optional] A list of courses id, comma separated
	 * @parameter all_courses [integer, optional] 0 to get courses from the id_courses list (default)and 1 to take all courses
	 * @parameter id_list [string, optional] A list of user idst or group idst, comma separated
	 * @parameter id_org_list [string, optional] A list of folders id, comma separated
	 * @parameter status [string, optional] Filter on user course status (ab-initio, in progress, completed)
	 * @parameter date_inscr_from [datetime, optional] Filter on course subscription date from, can be also passed as a date instead of a datetime
	 * @parameter date_inscr_to [datetime, optional] Filter on course subscription date to, can be also passed as a date instead of a datetime
	 * @parameter date_complete_from [datetime, optional] Filter on course completion date from, can be also passed as a date instead of a datetime
	 * @parameter date_complete_to [datetime, optional] Filter on course completion date to, can be also passed as a date instead of a datetime
	 * @parameter group_by_course [integer, optional] 0 to group the results by user (default) and 1 to group the results by course
	 *
	 * @status 201 Invalid course specification, No course found
	 * @status 202 Invalid users specification, No user found
	 * @status 203 Invalid subscription status filter
	 * @status 254 Invalid subscription date start filter, Invalid subscription date end filter
	 * @status 205 Invalid completion date start filter, Invalid completion date end filter
	 * @status 206 Invalid output specification
	 * @status 207 Invalid all courses specification
	 *
	 * @response user_view [array, optional] Details view grouped by users
	 *		@item id_user [integer, required] The idst of the user
	 *		@item username [string, required] The username of the user
	 *		@item firstname [string, required] The forstname of the user
	 *		@item lastname [string, required] The lastname of the user
	 *		@item email [string, required] The e-mail of the user
	 *		@item total_courses [integer, required] Total course in which the user was registered
	 *		@item courses_details [array, required] The details about user course subscription
	 *			@item id_course [integer, required] The id of the course
	 *			@item code [string, required] The code of the course
	 *			@item name [string, required] The name of the course
	 *			@item status [string, required] The user status into the course
	 *			@item date_inscr [datetime, required] The subscription date of the user
	 *			@item date_complete [datetime, required] The completion date of the user
	 *			@item waiting [integer, required] The waiting status for the user
	 *
	 * @response course_view [array, optional] Details view grouped by courses
	 *		@item id_course [integer, required] The id of the course
	 *		@item code [string, required] The code of the course
	 *		@item name [string, required] The name of the course
	 *		@item total_users [integer, required] Total users found into the course
	 *		@item users_details [array, required] The details about user course subscription
	 *			@item id_user [integer, required] The idst of the user
	 *			@item username [string, required] The username of the user
	 *			@item firstname [string, required] The forstname of the user
	 *			@item lastname [string, required] The lastname of the user
	 *			@item email [string, required] The e-mail of the user
	 *			@item status [string, required] The user status into the course
	 *			@item date_inscr [datetime, required] The subscription date of the user
	 *			@item date_complete [datetime, required] The completion date of the user
	 *			@item waiting [integer, required] The waiting status for the user
	 */
	public function getstats()
	{
		$reg = '/[^0-9, ]/';

		$id_list = $this->readParameter('id_list', '');
		$id_orgs = $this->readParameter('id_org_list', '');
		$id_courses = $this->readParameter('id_courses', '');
		$all_courses = $this->readParameter('all_courses', 0);

		$status_filter = $this->readParameter('status', '');
		$date_inscr_begin_filter = $this->readParameter('date_inscr_from', '');
		$date_inscr_end_filter = $this->readParameter('date_inscr_to', '');
		$date_complete_begin_filter = $this->readParameter('date_complete_from', '');
		$date_complete_end_filter = $this->readParameter('date_complete_to', '');

		$group_by_course = $this->readParameter('group_by_course', 0);

		if($id_courses == '' && $all_courses == 0)
			throw new CHttpException(201, 'Invalid course specification');

		if(preg_match($reg, $id_courses))
			throw new CHttpException(201, 'Invalid course specification');

		if(preg_match($reg, $id_list) || preg_match($reg, $id_orgs))
			throw new CHttpException(202, 'Invalid users specification');

		if(CourseApiModule::_getCourseUserStatusId($status_filter) === false && $status_filter !== '')
			throw new CHttpException(203, 'Invalid subscription status filter');
		elseif($status_filter !== '')
			$status_filter = CourseApiModule::_getCourseUserStatusId($status_filter);

		if($date_inscr_begin_filter !== '')
		{
			if(CourseApiModule::_validateDate($date_inscr_begin_filter, true))
				$date_inscr_begin_filter .= ' 00:00:00';
			if(!CourseApiModule::_validateDateTime($date_inscr_begin_filter, true))
				throw new CHttpException(254, 'Invalid subscription date start filter');
		}

		if($date_inscr_end_filter !== '')
		{
			if(CourseApiModule::_validateDate($date_inscr_end_filter, true))
				$date_inscr_end_filter .= ' 23:59:59';
			if(!CourseApiModule::_validateDateTime($date_inscr_end_filter, true))
				throw new CHttpException(254, 'Invalid subscription date end filter');
		}

		if($date_complete_begin_filter !== '')
		{
			if(CourseApiModule::_validateDate($date_complete_begin_filter, true))
				$date_complete_begin_filter .= ' 00:00:00';
			if(!CourseApiModule::_validateDateTime($date_complete_begin_filter, true))
				throw new CHttpException(205, 'Invalid completion date start filter');
		}

		if($date_complete_end_filter !== '')
		{
			if(CourseApiModule::_validateDate($date_complete_end_filter, true))
				$date_complete_end_filter .= ' 23:59:59';
			if(!CourseApiModule::_validateDateTime($date_complete_end_filter, true))
				throw new CHttpException(205, 'Invalid completion date end filter');
		}

		if($group_by_course != 0 && $group_by_course != 1)
			throw new CHttpException(206, 'Invalid output specification');

		if($all_courses != 0 && $all_courses != 1)
			throw new CHttpException(207, 'Invalid all course specification');

		if($all_courses == 0)
		{
			$id_courses = explode(',', $id_courses);

			$query = "
				select idCourse
				from learning_course
				where idCourse in (".implode(',', $id_courses).")
				".(PluginManager::isPluginActive('ClassroomApp') ? '' : " and course_type <> 'classroom'");
			$id_courses = Yii::app()->db->createCommand($query)->queryColumn();

			if(!is_array($id_courses) || (is_array($id_courses) && empty($id_courses)))
				throw new CHttpException(201, 'No course found');
		}

		$id_list = explode(',', $id_list);

		if($id_orgs <> '')
		{
			$query = "
				select idst_oc
				from core_org_chart_tree
				where idOrg in (".addslashes($id_orgs).")";
			$folders_groups = Yii::app()->db->createCommand($query)->queryColumn();

			if(is_array($folders_groups) && !empty($folders_groups))
				$id_list = array_merge($id_list, $folders_groups);
			unset($folders_groups);
		}

		$query = "
			select idstMember
			from core_group_members
			where idst in ('".implode("','", $id_list)."')";
		$group_members = Yii::app()->db->createCommand($query)->queryColumn();

		if(is_array($group_members) && !empty($group_members))
			$id_list = array_merge($id_list, $group_members);
		unset($group_members);

		if(empty($id_list))
			throw new CHttpException(202, 'No user found');

		$query = "
			select idst
			from core_user
			where idst in ('".implode("','", $id_list)."')";
		$id_users = Yii::app()->db->createCommand($query)->queryColumn();
		unset($id_list);

		if(!is_array($id_users) || (is_array($id_users) && empty($id_users)))
			throw new CHttpException(202, 'Invalid user specification');

		$query = "
			select u.idst, substr(u.userid, 2) as username, u.firstname, u.lastname, u.email, c.idCourse, c.code, c.name, cu.status, cu.date_inscr, cu.date_complete, cu.waiting
			from learning_courseuser as cu
			join learning_course as c on c.idCourse = cu.idCourse
			join core_user as u on u.idst = cu.idUser
			where cu.idUser in (".implode(',', $id_users).")
			".($all_courses == 1 ? '' : " AND cu.idCourse in (".implode(',', $id_courses).")")."
			".($date_inscr_begin_filter !== '' ? " AND cu.date_inscr >= '".$date_inscr_begin_filter."'" : '')."
			".($date_inscr_end_filter !== '' ? " AND cu.date_inscr <= '".$date_inscr_end_filter."'" : '')."
			".($date_complete_begin_filter !== '' ? " AND cu.date_complete >= '".$date_complete_begin_filter."'" : '')."
			".($date_complete_end_filter !== '' ? " AND cu.date_complete <= '".$date_complete_end_filter."'" : '')."
			".($status_filter !== '' ? " AND cu.status = ".$status_filter : '');

		$reader = Yii::app()->db->createCommand($query)->query();
		$output = array();

		while ($row = $reader->read()) {
			if($group_by_course == 1)
			{
				if(!isset($output[$row['idCourse']])) {
					$output[$row['idCourse']] = array(
						'id_course' => $row['idCourse'],
						'code' => $row['code'],
						'name' => $row['name'],
						'total_users' => 0,
						'users_details' => array()
					);
				}
				$output[$row['idCourse']]['total_users']++;
				$output[$row['idCourse']]['users_details'][] = array(
					'id_user' => $row['idst'],
					'username' => $row['username'],
					'firstname' => $row['firstname'],
					'lastname' => $row['lastname'],
					'email' => $row['email'],
					'status' => LearningCourseuser::getStatusLabel($row['status']),
					'date_inscr' => $row['date_inscr'],
					'date_complete' => is_null($row['date_complete']) ? '' : $row['date_complete'],
					'waiting' => $row['waiting']
				);
			}
			else
			{
				if(!isset($output[$row['idst']])) {
					$output[$row['idst']] = array(
						'id_user' => $row['idst'],
						'username' => $row['username'],
						'firstname' => $row['firstname'],
						'lastname' => $row['lastname'],
						'email' => $row['email'],
						'total_courses' => 0,
						'courses_details' => array()
					);
				}
				$output[$row['idst']]['total_courses']++;
				$output[$row['idst']]['courses_details'][] = array(
					'id_course' => $row['idCourse'],
					'code' => $row['code'],
					'name' => $row['name'],
					'status' => LearningCourseuser::getStatusLabel($row['status']),
					'date_inscr' => $row['date_inscr'],
					'date_complete' => is_null($row['date_complete']) ? '' : $row['date_complete'],
					'waiting' => $row['waiting']
				);
			}
		}

		$mainOutput = array();
		if($group_by_course == 1) {
			$mainOutput['course_view'] = array_values($output);
		} else {
			$mainOutput['user_view'] = array_values($output);
		}

		return $mainOutput;
	}

	/**
	 * @summary Calculate attendance statistical information for a single user
	 * @notes Returns how much courses the user is enrolled in, if he has completed or is in progress.
     * @notes Additionally, information on the last visited course is returned.
     *
     * @parameter id_user [integer, optional] The internal Docebo ID for the us
	 * @parameter idst [integer, optional] DEPRECATED user id_user instead
     * @parameter ext_user_type [integer, optional] User id on the external platform
     * @parameter ext_user [string, optional] External authentication type (e.g. drupal, joomla, …)
	 *
	 * @status 210 Invalid user specification
     * @status 211 User not found
	 *
	 * @response user_course [object, required] Global statistics about all enrolled courses
     *      @item new_courses [integer, required] The number of not yet started courses
     *      @item in_progress [integer, required] The number of in progress courses
     *      @item completed [integer, required] The number of completed courses
	 * @response last_course [object, required] Specific statistics about last attended course
     *      @item course_title [string, required] The title of the course
     *      @item course_thumbnail [string, required] URL of the course cover
     *      @item course_progress [decimal, required] Percentage of completed training materials
	 * @response sessions_per_month [array, required] List of user sessions over the last 12 months
	 *      @item month  [integer, required] Numeric identifier for the month (1 = January, 2 = February... 12 = December)
	 *      @item sessions [integer, required] Number of sessions occurred for this month
	 * @response most_viewed_courses [array, required] Top 3 most viewed courses for this user
	 *      @item id_course [integer, required] Docebo ID for the course
	 *      @item course_code [string, required] Alphanumeric code for the course
	 *      @item course_name [string, required] Course title/name
	 *      @item time_spent [string, required] Time spent by the user in the course
	 *      @item courseuser_status [integer, required] The course status for this user (0 = not started, 1 = in progress, 2 = completed)
	 * @response user_performance [object, required] Various performance values for the current user.
	 *      @item scores_per_month [array, required] User scores taken over the last 12 months
	 *      	@item month [integer, required] Numeric identifier for the month (1 = January, 2 = February... 12 = December)
	 *      	@item sessions [integer, required] Total score taken for this month
	 * 		@end scores_per_month
	 *      @item highest_score_points [integer, required] User's highest test score in points
	 *      @item highest_score_points_max [integer, required] Maximum potential score, in points for the test, where the user got the highest score
	 *      @item lowest_score_points [integer, required] User's lowest test score in points
	 *      @item lowest_score_points_max [integer, required] Maximum potential score, in points for the test where the user got the lowest score
	 *      @item average_score_points [integer, required] User's average test score in points
	 *      @item average_score_points_max [integer, required] Average of potential maximum scores in points for tests where the user has score
	 *      @item highest_score_percentage [integer, required] User's highest test score in percentage
	 *      @item highest_score_percentage_max [integer, required] Maximum potential score, in percentage for the test, where the user got the highest score
	 *      @item lowest_score_percentage [integer, required] User's lowest test score in percentage
	 *      @item lowest_score_percentage_max [integer, required] Maximum potential score, in percentage for the test where the user got the lowest score
	 *      @item average_score_percentage [integer, required] User's average test score in percentage
	 *      @item average_score_percentage_max [integer, required] Average of potential maximum scores in percentage for tests where the user has score
	 */
	public function getstat() {

		// Read input
		$input = new stdClass();
		$input->idst  = $this->readParameter('idst', $this->readParameter('id_user'));
		$input->ext_user_type = $this->readParameter('ext_user_type', '');
		$input->ext_user = $this->readParameter('ext_user', '');

		if (!$input->idst) {
			//try to search for external user
			$input->idst = self::_getExternalUserIdst($input->ext_user, $input->ext_user_type);
		}

		if (!$input->idst)
			throw new CHttpException(210, 'Invalid user specification');

		// Prevent godamdmin requests
		if (self::_isGodAdmin($input->idst))
			throw new CHttpException(210, 'Invalid user specification');

		// do stuff
		$coreUser = CoreUser::model()->findByAttributes(array('idst' => $input->idst));
		if (empty($coreUser))
			throw new CHttpException(211, 'User not found');

		// prepare output data
		$output = array(
			'user_courses' => array(
				'new_courses' => 0,
				'in_progress' => 0,
				'completed' => 0,
			),
			'last_course' => null,
		);

		// find user course statistic
		$statusInfo = Yii::app()->db->createCommand()
			->select("status, COUNT(*) AS num_users")
			->from(LearningCourseuser::model()->tableName())
			->where("idUser = :id_user", array(':id_user' => $coreUser->getPrimaryKey()))
			->group("status")
			->queryAll();

		if (is_array($statusInfo) && !empty($statusInfo)) {
			foreach ($statusInfo as $record) {
				$count = $record['num_users'];
				switch ($record['status']) {
					case LearningCourseuser::$COURSE_USER_SUBSCRIBED: $output['user_courses']['new_courses'] = $count; break;
					case LearningCourseuser::$COURSE_USER_BEGIN		: $output['user_courses']['in_progress'] = $count; break;
					case LearningCourseuser::$COURSE_USER_END		: $output['user_courses']['completed'] = $count; break;
				}
			}
		}

		// find last course and related info
		$arrLastCourse = Yii::app()->db->createCommand()
			->select('idCourse')
			->from(LearningTracksession::model()->tableName())
			->order("enterTime DESC")
			->limit(1, 0)
			->queryAll();

		if (!empty($arrLastCourse)) {
			$lastCourse = $arrLastCourse[0];
			$course = LearningCourse::model()->findByPk($lastCourse['idCourse']);
			if ($course) {
				//course thumbnail
				$thumbnail = $course->getCourseLogoUrl();
				$thumbnail = str_ireplace('http://http://', 'http://', $thumbnail);
				$thumbnail = str_ireplace('https://https://', 'https://', $thumbnail);

                // Find course progress
				$percentage = LearningOrganization::getProgressByUserAndCourse($course->idCourse, $coreUser->getPrimaryKey());

				$output['last_course'] = array(
					'course_title' 		=> $course->name,
					'course_thumbnail' 	=> $thumbnail,
					'course_progress' 	=> $percentage
				);
			}
		}

		// Get the Session Information
		$output['sessions_per_month'] = $this->getstatUserSessionsPerMonth($coreUser->getPrimaryKey());
		// Get the Session Information

		// Get top 3 most viewed courses
		$output['most_viewed_courses'] = $this->getstatUserTopThreeCourses($coreUser->getPrimaryKey());
		// Get top 3 most viewed courses

		// User performance
		$output['user_performance'] = $this->getstatUserPerformance($coreUser->getPrimaryKey());
		// User performance

		return $output;
	}

	private function getstatUserSessionsPerMonth($idUser) {
		$userSessions = $userSessionsFormated = array();
		$date_begin = date('Y-m-d H:i:s', mktime(0,0,0,date('m') - 1, 1, date('Y') - 1));
		$date_end = date('Y-m-d H:i:s', mktime(23,59,59,date('m') + 1, -1, date('Y')));
		$userSessionsQuery = Yii::app()->db->createCommand()
			->select(array("MONTH(enterTime) as monthNumber", "COUNT(*) as sessionCount"))
			->from(LearningTracksession::model()->tableName())
			->where(array("and", "idUser = :user", "enterTime >= :date_begin", "enterTime <= :date_end"))
			->group("MONTH(enterTime)")
			->order("enterTime")
			->queryAll(true, array(":user" => $idUser, ":date_begin" => $date_begin , ':date_end' => $date_end));

		if($userSessionsQuery !== array()) {
			foreach($userSessionsQuery as $row){
				$userSessions[$row['monthNumber']] = $row['sessionCount'];
			}
		}

		$date = new DateTime();
		for($i=1; $i<=12; $i++){
			$number = $date->format("n");
			$userSessionsFormated[$i] = array(
				'month' => (int)$number,
				'sessions' => isset($userSessions[$number]) ? (int)$userSessions[$number] : 0
			);
			$date->sub( new DateInterval('P1M') );
		}

		return $userSessionsFormated;
	}

	private function getstatUserTopThreeCourses($idUser){
		$coursesTopTotalTime = array();
		$reader = Yii::app()->db->createCommand()
			->select("t.idCourse, SUM(UNIX_TIMESTAMP(t.lastTime) - UNIX_TIMESTAMP(t.enterTime)) AS course_time, c.code, c.name, cu.status")
			->from(LearningTracksession::model()->tableName()." t")
			->join(LearningCourse::model()->tableName()." c", "c.idCourse = t.idCourse")
			->join(LearningCourseuser::model()->tableName()." cu", "cu.idCourse = t.idCourse AND cu.idUser = t.idUser")
			->where("t.idUSer = :id_user", array(':id_user' => $idUser))
			->group("t.idCourse")
			->order("course_time DESC")
			->limit(3, 0)
			->query();
		while($row = $reader->read()) {
			$init = $row['course_time'];
			$hours = floor($init / 3600);
			$minutes = floor(($init / 60) % 60);
			$seconds = $init % 60;

			$coursesTopTotalTime[] = array(
				'id_course' => (int)$row['idCourse'],
				'course_code' => $row['code'],
				'course_name' => $row['name'],
				'time_spent' => $hours . "h " . $minutes . "m " . $seconds."s",
				'courseuser_status' => (int)$row['status'],
			);
		}
		return $coursesTopTotalTime;
	}

	private function getstatUserPerformance($idUser){
		$monthsTrans = array(
			'01' => Yii::t('calendar', '_JAN'),
			'02' => Yii::t('calendar', '_FEB'),
			'03' => Yii::t('calendar', '_MAR'),
			'04' => Yii::t('calendar', '_APR'),
			'05' => Yii::t('calendar', '_MAY'),
			'06' => Yii::t('calendar', '_JUN'),
			'07' => Yii::t('calendar', '_JUL'),
			'08' => Yii::t('calendar', '_AUG'),
			'09' => Yii::t('calendar', '_SEP'),
			'10' => Yii::t('calendar', '_OCT'),
			'11' => Yii::t('calendar', '_NOV'),
			'12' => Yii::t('calendar', '_DEC'),
		);
		$monthsLimits = array();
		$chartDates = array();
		$chartMonths = array();
		$localNow = Yii::app()->localtime->toLocalDateTime(null, 'short', 'medium', 'Y-m-d H:i:s');
		$localMonthBegin = date('Y-m').'-01 00:00:00';
		$monthsLimits[] = Yii::app()->localtime->toUTC($localNow);
		$monthsLimits[] = Yii::app()->localtime->toUTC($localMonthBegin);
		$chartDates[] = date('m/d/Y');
		$chartDates[] = date('m/01/Y');
		$chartMonths[] = date('m');
		$startTime = strtotime($localMonthBegin);
		for ($i=1; $i<=12; $i++) {
			$targetTime = strtotime('-'.$i.' month', $startTime);
			$monthsLimits[] = Yii::app()->localtime->toUTC(date('Y-m-d H:i:s', $targetTime));
			$chartDates[] = date('m/d/Y', $targetTime);
			$chartMonths[] = date('m', $targetTime);
		}
		//extract data from DB and prepare it to be displayed
		$performanceInfo = array(
			'scores_per_month' => array(), //chart data, this array will contain 12 cells, one for every month
			'highest_score' => false,
			'lowest_score' => false,
			'average_score' => false,
			'highest_score_percent' => false,
			'lowest_score_percent'  => false,
			'average_score_percent' => false
		);
		$dateBegin = $monthsLimits[12];
		$dateEnd = $monthsLimits[0];
		$reader = Yii::app()->db->createCommand()
			->select("ct.idReference, ct.score, ct.score_max, ct.dateAttempt, c.idCourse, c.code, c.name, o.objectType")
			->from(LearningCommontrack::model()->tableName()." ct")
			->join(LearningOrganization::model()->tableName()." o", "o.idOrg = ct.idReference")
			->join(LearningCourse::model()->tableName()." c", "c.idCourse = o.idCourse")
			->where("ct.idUser = :id_user", array(':id_user' => $idUser))
			->andWhere("ct.score IS NOT NULL AND ct.score_max IS NOT NULL AND ct.score_max > 0") //exclude all NULL values for scores
			->andWhere("ct.dateAttempt <= :date_end AND ct.dateAttempt >= :date_begin", array(':date_end' => $dateEnd, ':date_begin' => $dateBegin))
			->andWhere("o.objectType != :type", array(':type' => LearningOrganization::OBJECT_TYPE_TEST))
			->query();
		$readerTests = Yii::app()->db->createCommand()
			->select("ct.idReference, ct.score, ct.score_max, ct.dateAttempt, c.idCourse, c.code, c.name, o.objectType, lt.point_type, lt.idTest")
			->from(LearningCommontrack::model()->tableName()." ct")
			->join(LearningOrganization::model()->tableName()." o", "o.idOrg = ct.idReference")
			->join(LearningCourse::model()->tableName()." c", "c.idCourse = o.idCourse")
			->join(LearningTest::model()->tableName()." lt", "lt.idTest = o.idResource")
			->where("ct.idUser = :id_user", array(':id_user' => $idUser))
			->andWhere("ct.score IS NOT NULL AND ct.score_max IS NOT NULL AND ct.score_max > 0") //exclude all NULL values for scores
			->andWhere("ct.dateAttempt <= :date_end AND ct.dateAttempt >= :date_begin", array(':date_end' => $dateEnd, ':date_begin' => $dateBegin))
			->andWhere("o.objectType = :objectType", array(':objectType' => LearningOrganization::OBJECT_TYPE_TEST))
			->query();

		$count = $sumPercent = $sumScoreMax = $countPercent = $sumPercentTest = $sumScoreMaxPercent = 0;
		$monthsAverages = array();
		while($rowTest = $readerTests->read()){
			if(isset($rowTest['point_type']) && ($rowTest['point_type'] == LearningTest::POINT_TYPE_PERCENT)){
				$countPercent++;
				$percentScore = ($rowTest['score_max'] && $rowTest['score_max'] > 0) ? ($rowTest['score'] / $rowTest['score_max']) : 0;
				$sumPercentTest += $percentScore;
				$sumScoreMaxPercent += $rowTest['score_max'];

				//initialize months average values
				if (!isset($monthsAverages[$i])) {
					$monthsAverages[$i] = array( 'sum_percent' => 0, 'count' => 0);
				}

				//detect year's lowest score
				if ($performanceInfo['lowest_score_percent'] === false) {
					$performanceInfo['lowest_score_percent'] = array(
						'score' => $rowTest['score'],
						'score_max' => $rowTest['score_max'],
						'percent' => $percentScore
					);
				} else {
					if ($percentScore < $performanceInfo['lowest_score_percent']['percent'] || ($percentScore == $performanceInfo['lowest_score_percent']['percent'] && $performanceInfo['lowest_score_percent']['score'] > $rowTest['score'])) {
						$performanceInfo['lowest_score_percent']['score'] = $rowTest['score'];
						$performanceInfo['lowest_score_percent']['score_max'] = $rowTest['score_max'];
						$performanceInfo['lowest_score_percent']['percent'] = $percentScore;
					}
				}
				//detect year's highest score
				if ($performanceInfo['highest_score_percent'] === false) {
					$performanceInfo['highest_score_percent'] = array(
						'score' => $rowTest['score'],
						'score_max' => $rowTest['score_max'],
						'percent' => $percentScore
					);
				} else {
					if ($percentScore > $performanceInfo['highest_score_percent']['percent'] || ($percentScore == $performanceInfo['highest_score_percent']['percent'] && $performanceInfo['highest_score_percent']['score'] < $rowTest['score'])) {
						$performanceInfo['highest_score_percent']['score'] = $rowTest['score'];
						$performanceInfo['highest_score_percent']['score_max'] = $rowTest['score_max'];
						$performanceInfo['highest_score_percent']['percent'] = $percentScore;
					}
				}

			}else{
				if(isset($rowTest['idTest']) ){
					$testModel = LearningTest::model()->findByPk($rowTest['idTest']);
					if($testModel){
						$rowTest['score_max'] = $testModel->getMaxScore($idUser, true, false);
					}
				}

				$count++;
				$percentScore = ($rowTest['score_max'] && $rowTest['score_max'] > 0) ? ($rowTest['score'] / $rowTest['score_max']) : 0;
				$sumPercent += $percentScore;
				$sumScoreMax += $rowTest['score_max'];

				//initialize months average values
				if (!isset($monthsAverages[$i])) {
					$monthsAverages[$i] = array(
						'sum_percent' => 0,
						'count' => 0
					);
				}

				//detect year's lowest score
				if ($performanceInfo['lowest_score'] === false) {
					$performanceInfo['lowest_score'] = array(
						'score' => $rowTest['score'],
						'score_max' => $rowTest['score_max'],
						'percent' => $percentScore
					);
				} else {
					if ($percentScore < $performanceInfo['lowest_score']['percent'] || ($percentScore == $performanceInfo['lowest_score']['percent'] && $performanceInfo['lowest_score']['score'] > $rowTest['score'])) {
						$performanceInfo['lowest_score']['score'] = $rowTest['score'];
						$performanceInfo['lowest_score']['score_max'] = $rowTest['score_max'];
						$performanceInfo['lowest_score']['percent'] = $percentScore;
					}
				}
				//detect year's highest score
				if ($performanceInfo['highest_score'] === false) {
					$performanceInfo['highest_score'] = array(
						'score' => $rowTest['score'],
						'score_max' => $rowTest['score_max'],
						'percent' => $percentScore
					);
				} else {
					if ($percentScore > $performanceInfo['highest_score']['percent'] || ($percentScore == $performanceInfo['highest_score']['percent'] && $performanceInfo['highest_score']['score'] < $rowTest['score'])) {
						$performanceInfo['highest_score']['score'] = $rowTest['score'];
						$performanceInfo['highest_score']['score_max'] = $rowTest['score_max'];
						$performanceInfo['highest_score']['percent'] = $percentScore;
					}
				}
			}

			//detect time period for monthly stats
			$foundTest = false;
			for ($i=0; $i<12 && !$foundTest; $i++) {
				$limitBegin = $monthsLimits[12 - $i];
				$limitEnd = $monthsLimits[12 - ($i+1)];
				if ($rowTest['dateAttempt'] > $limitBegin && $rowTest['dateAttempt'] <= $limitEnd) {
					$found = true;
					$monthsAverages[$i]['sum_percent'] += $percentScore;
					$monthsAverages[$i]['count']++;
				}
			}
		}

		while ($row = $reader->read()) {

			$count++;
			if($row['score_max'] && $row['score_max'] > 0)
				$percentScore = ($row['score'] / $row['score_max']); //this will always be a float number between 0 and 1
			else
				$percentScore = 0;
			$sumPercent += $percentScore;
			$sumScoreMax += $row['score_max'];

			//initialize months average values
			if (!isset($monthsAverages[$i])) {
				$monthsAverages[$i] = array( 'sum_percent' => 0, 'count' => 0 );
			}

			//detect year's lowest score
			if ($performanceInfo['lowest_score'] === false) {
				$performanceInfo['lowest_score'] = array(
					'score' => $row['score'],
					'score_max' => $row['score_max'],
					'percent' => $percentScore
				);
			} else {
				if ($percentScore < $performanceInfo['lowest_score']['percent'] || ($percentScore == $performanceInfo['lowest_score']['percent'] && $performanceInfo['lowest_score']['score'] > $row['score'])) {
					$performanceInfo['lowest_score']['score'] = $row['score'];
					$performanceInfo['lowest_score']['score_max'] = $row['score_max'];
					$performanceInfo['lowest_score']['percent'] = $percentScore;
				}
			}
			//detect year's highest score
			if ($performanceInfo['highest_score'] === false) {
				$performanceInfo['highest_score'] = array(
					'score' => $row['score'],
					'score_max' => $row['score_max'],
					'percent' => $percentScore
				);
			} else {
				if ($percentScore > $performanceInfo['highest_score']['percent'] || ($percentScore == $performanceInfo['highest_score']['percent'] && $performanceInfo['highest_score']['score'] < $row['score'])) {
					$performanceInfo['highest_score']['score'] = $row['score'];
					$performanceInfo['highest_score']['score_max'] = $row['score_max'];
					$performanceInfo['highest_score']['percent'] = $percentScore;
				}
			}

			//detect time period for monthly stats
			$found = false;
			for ($i=0; $i<12 && !$found; $i++) {
				$limitBegin = $monthsLimits[12 - $i];
				$limitEnd = $monthsLimits[12 - ($i+1)];
				if ($row['dateAttempt'] > $limitBegin && $row['dateAttempt'] <= $limitEnd) {
					$found = true;
					$monthsAverages[$i]['sum_percent'] += $percentScore;
					$monthsAverages[$i]['count']++;
				}
			}
		}

		//set monthly stats for chart
		for ($i=0; $i<12; $i++) {
			//$index = $chartDates[12 - $i - 1];
			$index = $monthsTrans[''.$chartMonths[12 - $i - 1]];
			if (isset($monthsAverages[$i]) && $monthsAverages[$i]['count'] > 0) {
				$performanceInfo['scores_per_month'][$index] = $monthsAverages[$i]['sum_percent'] / $monthsAverages[$i]['count'];
			} else {
				$performanceInfo['scores_per_month'][$index] = 0;
			}
		}

		// FORMAT THE Score Per month
		$date = new DateTime();
		for($i=1; $i<=12; $i++){
			$numberNumber = $date->format("n");
			$monthIndex = $date->format("M");
			$currentState = $performanceInfo['scores_per_month'][$monthIndex];
			unset($performanceInfo['scores_per_month'][$monthIndex]);
			// Assign the new Format Value
			$performanceInfo['scores_per_month'][$i] = array( 'month' => (int) $numberNumber, 'sessions' => (int)round($currentState * 100));
			$date->sub( new DateInterval('P1M') );
		}

		//processing last data
		if ($count > 0) {
			$averagePercent = $sumPercent / $count;
			$averageScoreMax = $sumScoreMax / $count;
			$averageScore = $averagePercent * $averageScoreMax;
			$performanceInfo['average_score'] = array(
				'percent' => $averagePercent,
				'score' => $averageScore,
				'score_max' => $averageScoreMax
			);
		}

		if ($countPercent > 0) {
			$averagePercentTest = $sumPercentTest / $countPercent;
			$averageScoreMaxTest = $sumScoreMaxPercent / $countPercent;
			$averageScoreTest = $averagePercentTest * $averageScoreMaxTest;
			$performanceInfo['average_score_percent'] = array(
				'percent' => $averagePercentTest,
				'score' => $averageScoreTest,
				'score_max' => $averageScoreMaxTest
			);
		}

		return $this->getstatUserPerformanceClearData($performanceInfo);
	}

	private function getstatUserPerformanceClearData($performanceInfo) {
		$performance = array(
			'scores_per_month' => $performanceInfo['scores_per_month'],
			// Score
			'highest_score_points' => 0,
			'highest_score_points_max' => 0,
			'lowest_score_points' => 0,
			'lowest_score_points_max' => 0,
			'average_score_points' => 0,
			'average_score_points_max' => 0,
			// Percent
			'highest_score_percentage' => 0,
			'highest_score_percentage_max' => 0,
			'lowest_score_percentage' => 0,
			'lowest_score_percentage_max' => 0,
			'average_score_percentage' => 0,
			'average_score_percentage_max' => 0,
		);
		// Highest Score
		$performance['highest_score_points'] = isset($performanceInfo['highest_score']['score']) ? (int)$performanceInfo['highest_score']['score'] : 0;
		$performance['highest_score_points_max'] = isset($performanceInfo['highest_score']['score_max']) ? (int)$performanceInfo['highest_score']['score_max'] : 0;
		// Lowest Score
		$performance['lowest_score_points'] = isset($performanceInfo['lowest_score']['score']) ? (int)$performanceInfo['lowest_score']['score'] : 0;
		$performance['lowest_score_points_max'] = isset($performanceInfo['lowest_score']['score_max']) ? (int)$performanceInfo['lowest_score']['score_max'] : 0;
		// Average Score
		$performance['average_score_points'] = isset($performanceInfo['average_score']['score']) ? (int)$performanceInfo['average_score']['score'] : 0;
		$performance['average_score_points_max'] = isset($performanceInfo['average_score']['score_max']) ? (int)$performanceInfo['average_score']['score_max'] : 0;

		// Highest Percent
		$performance['highest_score_percentage'] = isset($performanceInfo['highest_score_percent']['score']) ? (int)$performanceInfo['highest_score_percent']['score'] : 0;
		$performance['highest_score_percentage_max'] = isset($performanceInfo['highest_score_percent']['score_max']) ? (int)$performanceInfo['highest_score_percent']['score_max'] : 0;
		// Lowest Percent
		$performance['lowest_score_percentage'] = isset($performanceInfo['lowest_score_percent']['score']) ? (int)$performanceInfo['lowest_score_percent']['score'] : 0;
		$performance['lowest_score_percentage_max'] = isset($performanceInfo['lowest_score_percent']['score_max']) ? (int)$performanceInfo['lowest_score_percent']['score_max'] : 0;
		// Average Percent
		$performance['average_score_percentage'] = isset($performanceInfo['average_score_percent']['score']) ? (int)$performanceInfo['average_score_percent']['score'] : 0;
		$performance['average_score_percentage_max'] = isset($performanceInfo['average_score_percent']['score_max']) ? (int)$performanceInfo['average_score_percent']['score_max'] : 0;

		return $performance;
	}

	/**
	 * Returns the array of count of completed courses and number of all courses for each user
	 * @parameter ext_user_type [string, optional] External identification type (joomla, drupal, ..)
	 * @return array
	 */
	public function getUsersCoursesInfo()
	{
		$extUserType = $this->readParameter('ext_user_type', '');

		$data = array();
		$results = Yii::app()->db->createCommand()
			->select('
				SUM(CASE WHEN learning_courseuser.status = '.LearningCourseuser::$COURSE_USER_END.' THEN 1 END) AS completed,
				COUNT(learning_courseuser.status) AS total,
				core_setting_user.value')
			->from('learning_courseuser learning_courseuser')
			->join('learning_course learning_course', 'learning_courseuser.idCourse = learning_course.idCourse')
			->join('core_setting_user core_setting_user', 'core_setting_user.id_user = learning_courseuser.idUser')
			->where('core_setting_user.path_name = "ext.user.'.$extUserType.'"')
			->group('learning_courseuser.idUser')
			->queryAll();

		foreach ($results as $row) {
			$data[$row['value']]['completed'] = $row['completed'];
			$data[$row['value']]['total'] = $row['total'];
		}

		return $data;
	}


	public function createHash(){

		$result = array();
		$success = false;
		$c = new CDbCriteria();
		$c->select = 't.idst';
		if (Yii::app()->request->getParam('email', false))
		{
			$c->addCondition("t.userid = '/" . Yii::app()->request->getParam('email', false) . "'");
		}
		else
		{
			$c->addCondition("groups.groupid='/framework/level/godadmin'");
			$c->addCondition("t.valid =".CoreUser::USER_STATUS_ACTIVE);
		}
		$godadmins = CoreUser::model()->with('groups')->findAll($c);

		if(isset($godadmins[0]) && $godadmins[0]->idst ){
			$auth = new AuthToken($godadmins[0]->idst);
			$success = true;
			$result['authToken'] = $auth->get();
			$result['userId'] = $godadmins[0]->idst;
		}
		$result['success'] = $success ;
		echo json_encode($result);
		die();
	}


	/**
	 * @summary Returns user ID and authentication token for single sign on. Requires key/secret API authentication
	 *
	 * @parameter username [string, required] username of the user that we want to authenticate
	 *
	 * @response id_user [integer, required] Internal ID of the authenticated user
	 * @response token [string, required] The generated token
	 *
	 * @status 201 Invalid login data provided
	 */
	public function getToken() {

		$username = $this->readParameter('username', false);

		// Check passed params
		if ($username == false) {
			throw new CHttpException(201, 'Invalid login data provided.');
		}

		// Retrive user model
		$userModel = CoreUser::model()->findByAttributes(array('userid' => Yii::app()->user->getAbsoluteUsername($username)));
		if (!$userModel) {
			throw new CHttpException(201, 'Invalid login data provided.');
		}

		$token = $this->_generateToken($userModel->idst);
		if ($token) {
			$output = array(
					'id_user' => $userModel->idst,
					'token' => $token,
			);
		} else {
			throw new CHttpException(201, 'Invalid login data provided.');
		}

		return $output;

	}

	/**
	 * @summary Starts a bulk user synchronization job using a CSV file
	 * @notes Massively updates users in the LMS using an asynchronous job that processes a CSV file. CSV files can be uploaded either through a publicly accessible URL or through a multipart POST.<br/>
	 * @notes Each row of the CSV file will be processed according to the mapping/order defined by the "importMap" input param. Allowed values for the columns array are:
	 * @notes <ul style="padding-left: 40px; list-style:circle;">
	 * @notes <li><span class="code">ignoredfield</span>: ignores this column</li>
	 * @notes <li><span class="code">userid</span>: Username</li>
	 * @notes <li><span class="code">firstname</span>: First name</li>
	 * @notes <li><span class="code">lastname</span>: Last name</li>
	 * @notes <li><span class="code">email</span>: E-mail</li>
	 * @notes <li><span class="code">new_password</span>: New password</li>
	 * @notes <li><span class="code">force_change</span>: Force users to change their password at the first sign in. Accepts ( true, false, 0, 1) as value. If empty it will fallback to the general option</li>
	 * @notes <li><span class="code">language</span>: Language</li>
	 * @notes <li><span class="code">user_level</span>: "poweruser" = Power User, empty (default) = regular user</li>
	 * @notes <li><span class="code">pu_profile</span>: The name of the Power User profile. Only used if "user_level" column is "poweruser"</li>
	 * @notes <li><span class="code">branch</span>: branch name in default LMS language </li>
	 * @notes <li><span class="code">branch_code</span>: Branch code</li>
	 * @notes <li><span class="code">suspend</span>: "true" = suspend user, "false" = unsuspend user, empty (default) = no action</li>
	 * @notes <li><span class="code">123</span>: Additional field with ID 123</li>
	 * @notes </ul>
	 *
	 * @parameter importMap [array(string), required] Array of columns identifying the columns of the CSV file. For all allowed column names, see notes above.
	 * @parameter file_url [string, optional] Publicly accessible URL where the CSV file can be downloaded from
	 * @parameter file_upload [file, optional] The name of the uploaded CSV file (via multipart POST)
	 * @parameter separator [string, optional] The type of CSV to use (can be one of the following values: "comma","semicolon","auto","manual"). Defaults to "auto".
	 * @parameter manual_separator [string, optional] If "separator" param is set to "manual", this param is required and it should contain the custom separator to use.
	 * @parameter first_row_header [boolean, optional] Set to true if the first row of the CSV file contains the header. Defaults to true.
	 * @parameter charset [string, optional] The charset to use for the CSV file. Defaults to "UTF-8".
	 * @parameter node [integer, optional] The id_org of the branch where all users are assigned by default. Defaults to the "root" node ID.
	 * @parameter password_changing [string, optional] If set to "yes", it forces imported users to change their password on their first login. Defaults to "no".
	 * @parameter insert_update [boolean, optional] Whether existing users should be updated or just skipped. Defaults to "false".
	 * @parameter unenroll_deactivated [boolean, optional] Unenroll user from all future Classroom and Webinar sessions if valid is set to "false"
	 * @parameter ignore_password_change_for_existing_users [boolean, optional] Ignore force password change for existing users
	 * @parameter auto_assign_branches [boolean, optional] Whether to auto assign branches to all power users . Defaults to true.
	 *
	 * @status 201 Api current not available on this system
	 * @status 202 Another user synchronization is currently running
	 * @status 203 Missing both file_url and file_upload params
	 * @status 400 "Error while reading remove CSV file" or "Error while reading uploaded CSV file"
	 * @status 205 Validation errors: <list of errors>
	 * @status 206 Could not start the user synch job
	 * @status 207 Import map empty or not valid
	 *
	 * @response job_id [string, required] A unique ID identifying the scheduled job. Used to get the status of the job
	 */
	public function startSynch() {

		$transaction = Yii::app()->db->beginTransaction();

		try {
			// This API relies on the scheduler app to work
			if(!PluginManager::isPluginActive("SchedulerApp") || !Yii::app()->scheduler)
				throw new CHttpException(201, "Api current not available on this system");

			// Check if there's another user synch job started (avoid duplicate calls)
			$activeJob = CoreJob::model()->findByAttributes(array('handler_id' => BulkApiTask::id(), 'active' => 1 , 'name'=>'ApiUserSynchronization'));
			if($activeJob) {
				$now = Yii::app()->localtime->getUTCNow();
				$lastFinished = Yii::app()->localtime->fromLocalDateTime($activeJob->last_finished);
				if((strtotime($now) - strtotime($lastFinished)) > 24*3600) { //24 hours
					$activeJob->active = 0;
					$activeJob->save();
					Yii::log('Deactivated job: ApiUserSynchronization (id_job = '.$activeJob->id_job.')', 'debug');
				} else {
					throw new CHttpException(202, "Another user synchronization is currently running");
				}
			}

			// Set up the user import form
			$userImportForm = new UserImportForm('step_one');
			$userImportForm->separator = $this->readParameter('separator', "auto");
			$userImportForm->manualSeparator = $this->readParameter('manual_separator', '');
			$userImportForm->firstRowAsHeader = $this->readParameter('first_row_header', true);
			$userImportForm->charset = $this->readParameter('charset', "UTF-8");
			$userImportForm->node = $this->readParameter('node', CoreOrgChartTree::getOrgRootNode()->idOrg);
			$userImportForm->passwordChanging = $this->readParameter('password_changing', 'no');
			$userImportForm->insertUpdate = $this->readParameter('insert_update', false);
			$userImportForm->ignorePasswordChangeForExistingUsers = $this->readParameter('ignore_password_change_for_existing_users', false);
			$userImportForm->file = $this->getCSVFile();
			$userImportForm->unenroll_deactivated = $this->readParameter('unenroll_deactivated', false);
			$userImportForm->unenroll_deactivated = $userImportForm->unenroll_deactivated === true || $userImportForm->unenroll_deactivated === 'true' ? 1 : $userImportForm->unenroll_deactivated;
			$userImportForm->unenroll_deactivated = $userImportForm->unenroll_deactivated !== false && $userImportForm->unenroll_deactivated !== 'false' ? ($userImportForm->unenroll_deactivated > 0 ? 1 : 0) : 0;
			$userImportForm->autoAssignBranches =  $this->readParameter('auto_assign_branches', true);
			if ($userImportForm->validate()) {
				// If uploaded via HTTP form, the file is in the /tmp dir... let's copy it in a safer location
				$userImportForm->originalFilename = Docebo::randomHash() . "." . $userImportForm->file->extensionName;
				$userImportForm->localFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $userImportForm->originalFilename;
				@copy($userImportForm->file->tempName, $userImportForm->localFile);
			} else
				throw new CHttpException(205, "Validation errors: ".$this->errorSummary($userImportForm));


			// Check on the import map params
			$userImportForm->scenario = 'step_two';
			$importMap = $this->readParameter('importMap', array());
			if(empty($importMap))
				throw new CHttpException(207, "Import map empty or not valid");
			else {
				foreach($importMap as $index => $value)
					if(!trim($value))
						throw new CHttpException(207, "Import map empty or not valid");
			}

			if(!in_array('new_password', $importMap))
				$importMap[] = 'new_password';
			$userImportForm->setImportMap($importMap);
			if(!$userImportForm->validate())
				throw new CHttpException(205, "Validation errors: ".$this->errorSummary($userImportForm));

			// Time to save the CSV file in S3 for later processing
			// Get the storage object
			$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_ASSETS);
			$storage->store($userImportForm->localFile);


			// Parse CSV file to calculate totals and save into serialized form
			$userImportForm->clearErrors();
			$userImportForm->failedLines = array();
			$userImportForm->successCounter = 0;
			$csvFile = new QsCsvFile(array(
				'path' => $userImportForm->localFile,
				'charset' => $userImportForm->charset,
				'delimiter' => UserImportForm::translateSeparatorName($userImportForm->separator),
				'firstRowAsHeader' => $userImportForm->firstRowAsHeader,
				'numLines' => 1, // excluding header, if any
			));
			$userImportForm->totalUsers = $csvFile->getTotalLines();

			// Create a new job to run the chunked user import
			$params = array('limit' => BulkApiTask::MAX_USERS_PER_RUN, 'offset' => 0, 'form' => $userImportForm->getParams());
			$scheduler = Yii::app()->scheduler; /* @var $scheduler SchedulerComponent */

			$job = $scheduler->createImmediateJob("ApiUserSynchronization", BulkApiTask::id(), $params, false, null, true);

			$currentTransaction =  Yii::app()->db->getCurrentTransaction();
			if($currentTransaction){
				$transaction = $currentTransaction;
			}else{
				$transaction = Yii::app()->db->beginTransaction();
			}

			$transaction->commit();

			// Now we can return job ID
			return array('job_id' => $job->hash_id);
		} catch (CHttpException $e) {
			$transaction->rollback();
			Yii::log('Rollback transaction', 'debug');
			throw $e;
		} catch (Exception $e) {
			// Something unforeseen happened..log it and return opaque error message
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			$transaction->rollback();
			Yii::log('Rollback transaction', 'debug');
			throw new CHttpException(206, 'Could not start the user synch job');
		}
	}

	/**
	 * @summary Gets the current status of the user synchronization job.
	 *
	 * @parameter job_id [string, required] ID of the scheduled job, as returned by user/startSynch.
	 *
	 * @status 201 Missing required param "job_id"
	 * @status 202 Invalid job ID param
	 *
	 * @response status [string, required] One of the following statuses: “running”, “completed”
	 * @response total_records [integer, required] The total number of records to process
	 * @response processed_records [integer, required] The number of records already processed
	 * @response errors [array, required] Array of error messages occurred while processing users records
	 * 		@item userid [string, required] The userid for whom the error is returned
	 * 		@item error [string, required] The error message generated while updating the user
	 */
	public function synchStatus() {
		// Check mandatory param
		$job_id = $this->readParameter('job_id', null);
		if(!$job_id)
			throw new CHttpException(201, 'Missing required param "job_id"');

		// Try loading the job
		/* @var $job CoreJob */
		$job = CoreJob::model()->findByAttributes(array('hash_id' => $job_id));
		if(!$job)
			throw new CHttpException(202, "Invalid job ID param");

		// Fetch job params
		$params = json_decode($job->params, true);
		$importForm = new UserImportForm(); /* @var $importForm UserImportForm */
		foreach ($params['form'] as $attribute => $value) {
			if(property_exists($importForm, $attribute))
				$importForm->$attribute = $params['form'][$attribute];
			else if($attribute == 'importMap')
				$importForm->setImportMap($value);
		}

		$result = array(
			'status' => 'running',
			'processed_records' => ($params['offset'] > $importForm->totalUsers) ? $importForm->totalUsers : $params['offset'],
			'total_records' => $importForm->totalUsers,
			'errors' => array()
		);

		// Job finished -> prepare error summary
		if($job->active == 0) {
			$result['status'] = 'completed';
			$useridKey = array_search("userid", $importForm->getImportMap());

			$logFileName = $job->id_job . "_failed_lines.csv";
			$localLogFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $logFileName;
			$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_ASSETS);
			if($storage->downloadAs($logFileName, $localLogFilePath)) {
				// Open log file
				$fh = fopen($localLogFilePath, "r");
				if ($fh !== FALSE) {
					while (($line = fgetcsv($fh)) !== FALSE) {
						$result['errors'][] = array(
							'userid' => $line[$useridKey + 1],
							'error' => $line[0]
						);
					}
					fclose($fh);
				}
			}
		}

		return $result;
	}

	/**
	 * @summary Check if a token is valid for a user
	 *
	 * @parameter id_user [string, required] Internal ID of the user
	 * @parameter auth_token [string, required] Token of the user that we want to check
	 *
	 * @response id_user [integer, required] Internal ID of the user
	 * @response auth_token [string, required] The token
	 * @response token_valid [bool, required] true if the token is valid for the user false otherwise
	 *
	 * @status 201 No id_user specified
	 * @status 202 No auth_token specified
	 */
	public function checkToken()
	{
		$id_user = $this->readParameter('id_user', false);
		$auth_token = $this->readParameter('auth_token', false);

		if($id_user == false || $id_user == '')
			throw new CHttpException(201, 'No id_user specified');

		if($auth_token == false || $auth_token == '')
			throw new CHttpException(202, 'No auth_token specified');

		$current_token = $this->_recoverToken($id_user);

		$output = array(
				'id_user' => $id_user,
				'auth_token' => $auth_token
		);

		if($current_token == false || $current_token != $auth_token)
			$output['token_valid'] = false;
		else
			$output['token_valid'] = true;

		return $output;
	}

    /**
     * It's the same as profile_image() but uses SECURITY_LIGHT
     *
     */
    public function get_profile_image() {
        return $this->profile_image();
    }

	/**
	 *
	 * Unenroll user from all Classroom and Webinar sessions
	 *
	 * @param $idst Idst of the LMS user
	 */
	private function unenrollUserFromAllSessions($idst){

		$classroomSessions = array();
		$webinarSessions = array();

		$params = array(
			':user' => $idst,
			':now' => Yii::app()->localtime->getUTCNow('Y-m-d H:i:s')
		);

		$classroomIds = Yii::app()->db->createCommand()
			->select('ls.id_session')
			->from(LtCourseSession::model()->tableName() . ' ls')
			->join(LtCourseuserSession::model()->tableName() . ' lsu', 'ls.id_session = lsu.id_session')
			->where('lsu.id_user = :user AND ls.date_end >= :now', $params)
			->queryAll();


		foreach($classroomIds as $sessId){
			$classroomSessions[] = $sessId['id_session'];
		}

		if(!empty($classroomSessions)){

			$condition = "id_session IN (" . implode(',', $classroomSessions) . ") AND id_user = :id_user";

			Yii::app()->db->createCommand()
				->delete(LtCourseuserSession::model()->tableName(), $condition, array(':id_user' => $idst));
		}

		$webinarIds = Yii::app()->db->createCommand()
			->select('ws.id_session')
			->from(WebinarSession::model()->tableName() . ' ws')
			->join(WebinarSessionUser::model()->tableName() . ' wsu', 'ws.id_session = wsu.id_session')
			->where('wsu.id_user = :user AND ws.date_end >= :now', $params)
			->queryAll();


		foreach($webinarIds as $sessId){
			$webinarSessions[] = $sessId['id_session'];
		}

		if(!empty($webinarSessions)){

			$condition = "id_session IN (" . implode(',', $webinarSessions) . ") AND id_user = :id_user";

			Yii::app()->db->createCommand()
				->delete(WebinarSessionUser::model()->tableName(), $condition, array(':id_user' => $idst));
		}
	}

}
