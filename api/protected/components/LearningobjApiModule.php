<?php


/**
 * LearningObj API Component
 * Set of APIs to manage learning objects of the LMS
 */
class LearningobjApiModule extends ApiModule {


	/**
	 * @summary Given a course ID, retrieve a list of course's learning objects
	 * @notes Folder objects are retrieved too
	 *
	 * @parameter course_id [integer, required] numeric ID of the course
	 *
	 * @status 201 Error in input data or invalid course specification
	 * @status 500 Error in data retrieval
	 *
	 * @response object_list [array, required] The list of the course learning objects, each one as a record of LO properties
	 */
	public function listCourseObjects() {

		//read and validate input parameters
		$idCourse = $this->readParameter('course_id');
		if (!$idCourse) {
			throw new CHttpException(201, 'Invalid specified course');
		}

		//prepare output variable
		$output = array();

		//retrieve data
		try {
			$criteria = new CDbCriteria();
			$criteria->addCondition("idCourse = :id_course");
			$criteria->params[':id_course'] = $idCourse;
			$criteria->order = "iLeft ASC";
			$objects = LearningOrganization::model()->findAll($criteria);
			if (!empty($objects)) {
				foreach ($objects as $object) {
					$output[ /*$object->idOrg*/] = array(
						'scorm_id' => (int)$object->idOrg,
						'parent_id' => (int)$object->idParent,
						'title' => $object->title,
						'object_type' => $object->objectType,
						'resource_id' => (int)$object->idResource,
						'category_id' => (int)$object->idCategory,
						'description' => $object->description,
						'language' => $object->language,
						'course_id' => (int)$object->idCourse,
						'prerequisites' => $object->prerequisites,
						'is_terminator' => $object->isTerminator,
						'publish_from' => $object->publish_from,
						'publish_to' => $object->publish_to
					);
				}
			}
		} catch(CHttpException $e) {
			throw $e;
		} catch(Exception $e) {
			throw new CHttpException(500, $e->getMessage());
		}

		return array('object_list' => $output);
	}


	/**
	 * @summary Given a learning object ID, edit some of its properties
	 * @notes "isTerminator" and "bookmark" are the only editable properties. The "scorm_id" parameter name is a
	 * @notes conventional name for learning objects numerical ID, but non-scorm objects are editable too.
	 *
	 * @parameter scorm_id [integer, required] numeric ID of the learning object
	 *
	 * @status 201 Error in input data or invalid learning object specification
	 * @status 500 Error in database operation
	 *
	 * @response none
	 */
	public function setObjectProperties() {

		//read and validate input parameters
		$scormId = $this->readParameter('scorm_id');
		if (!$scormId) {
			throw new CHttpException(201, 'Invalid scorm_id');
		}
		$object = LearningOrganization::model()->findByPk($scormId);
		if (!$object) {
			throw new CHttpException(201, 'Invalid scorm_id');
		}

		if ($object->objectType == '') {
			//folder objects are not editable
			throw new CHttpException(500, 'The selected object is a folder, unable to edit properties');
		}

		$isTerminator = $this->readParameter('is_terminator', null);
		$bookmark = $this->readParameter('bookmark', null);

		if ($isTerminator === null && $bookmark === null) {
			throw new CHttpException(201, 'Invalid input data');
		}

		try {

			//apply directly new properties to the LMS object
			if ($isTerminator !== null) {
				$object->isTerminator = ($isTerminator ? 1 : 0);
			}
			if ($bookmark !== null) {
				$object->milestone = $bookmark;
			}

			$object->saveNode();

		} catch(CHttpException $e) {
			throw $e;
		} catch(Exception $e) {
			throw new CHttpException(500, $e->getMessage());
		}

		return array(); //no specific data to return
	}


	/**
	 * @summary creation of a learning object of type "test"
	 * @notes at least one parameter "course_id" or "course_code" is required. If both are present, "course_id" is given precedence.
	 *
	 * @parameter course_id [integer, optional] numeric ID of the course
	 * @parameter course_code [string, optional] code property of the course
	 * @parameter obj_name [string, required] title of the test to be created
	 * @parameter description [string, optional] description property of the test
	 * @parameter point_type [integer, optional] point_type learning_test property, possible values are "percentage" or "numeric" (default = "numeric")
	 * @parameter point_required [double, optional] point_required learning_test property, score required to pass the test (default = 0)
	 * @parameter order_type [integer, optional] order_type learning_test property, possible values are 0, 1, 2 and 3 (default = 0)
	 * @parameter shuffle_answer [integer, optional] shuffle_answer learning_test property, possible values are 0 or 1 (default = 0)
	 * @parameter question_random_number [integer, optional] question_random_number learning_test property (default = 0)
	 * @parameter allow_resume [integer, optional] allow_resume learning_test property, possible values are 0 or 1 (default = 0)
	 * @parameter allow_edit_answer [integer, optional] allow_edit_answer learning_test property, possible values are 0 or 1 (default = 0)
	 * @parameter allow_page_navigation [integer, optional] allow_page_navigation learning_test property, possible values are 0 or 1 (default = 1)
	 * @parameter show_answers [integer, optional] show_answers learning_test property, possible values are 0 or 1 (default = 1)
	 * @parameter show_correct [integer, optional] show_correct learning_test property, possible values are 0 or 1 (default = 0)
	 * @parameter max_attempt [integer, optional] max_attempt learning_test property, if 0 no limit to possible test attempts (default = 0)
	 * @parameter hide_info [integer, optional] hide_info learning_test property, possible values are 0 or 1 (default = 1)
	 * @parameter use_suspension [integer [integer, optional] use_suspension learning_test property, possible values are0 or 1 (default = 0)
	 * @parameter suspension_num_attempts [integer, optional] suspension_num_attempts learning_test property (default = 0)
	 * @parameter suspension_num_hours [integer, optional] suspension_num_hours learning_test property (default = 0)
	 * @parameter mandatory_answer [integer, optional] mandatory_answer learning_test property, possible values are 0 or 1 (default = 0)
	 *
	 *
	 * @status 201 Error in input data or invalid course specification
	 * @status 500 Error in test creation
	 *
	 * @response scorm_id [integer, required] the learning_organization::idResource (learning_test::idTest) of the newly created test object
	 */
	public function createTest() {

		//read input data
		$input = new stdClass();

		$input->course_id = $this->readParameter('course_id');
		$input->course_code = $this->readParameter('course_code');
		$input->obj_name = $this->readParameter('obj_name');

		$input->description = $this->readParameter('description', "");
		$input->point_type = $this->readParameter('point_type');
		$input->point_required = $this->readParameter('point_required', 0);
		$input->display_type = $this->readParameter('display_type');
		$input->order_type = $this->readParameter('order_type');
		$input->shuffle_answer = $this->readParameter('shuffle_answer');
		$input->question_random_number = $this->readParameter('question_random_number');
		$input->allow_resume = $this->readParameter('allow_resume');
		$input->allow_edit_answer = $this->readParameter('allow_edit_answer');
		$input->allow_page_navigation = $this->readParameter('allow_page_navigation');
		$input->show_answers = $this->readParameter('show_answers');
		$input->show_correct = $this->readParameter('show_correct');
		$input->max_attempt = $this->readParameter('max_attempt');
		$input->hide_info = $this->readParameter('hide_info');
		$input->use_suspension = $this->readParameter('use_suspension');
		$input->suspension_num_attempts = $this->readParameter('suspension_num_attempts');
		$input->suspension_num_hours = $this->readParameter('suspension_num_hours');
		$input->suspension_prerequisites = $this->readParameter('suspension_prerequisites');
		$input->mandatory_answer = $this->readParameter('mandatory_answer');

		//validate input values
		if ((int)$input->course_id > 0) {
			$course = LearningCourse::model()->findByPk($input->course_id);
		} elseif ($input->course_code != '') {
			$course = LearningCourse::model()->findByAttributes(array('code' => $input->course_code));
		} else {
			$course = false;
		}
		if (!$course) {
			throw new CHttpException(201, 'Invalid specified course');
		}

		//do DB stuff
		$transaction = Yii::app()->db->beginTransaction();
		try {

			//"translate" some input values into DB real values
			switch($input->point_type) {
				case 'percentage':
					$point_type = 1;
					break;
				default:
					$point_type = 0;
					break;
			}

			//assign attributes to newly created test AR
			$test = new LearningTest();
			$test->title = $input->obj_name;
			$test->description = $input->description;
			$test->point_type = $point_type;
			$test->point_required = $input->point_required;
			$test->display_type = $input->display_type;
			$test->order_type = $input->order_type;
			$test->shuffle_answer = $input->shuffle_answer;
			$test->question_random_number = $input->question_random_number;
			$test->save_keep = $input->allow_resume;
			$test->mod_doanswer = $input->allow_edit_answer;
			$test->can_travel = $input->allow_page_navigation;
			$test->show_doanswer = $input->show_answers;
			$test->show_solution = $input->show_correct;
			$test->max_attempt = $input->max_attempt;
			$test->hide_info = $input->hide_info;
			$test->use_suspension = $input->use_suspension;
			$test->suspension_num_attempts = $input->suspension_num_attempts;
			$test->suspension_num_hours = $input->suspension_num_hours;
			$test->suspension_prerequisites = $input->suspension_prerequisites;
			$test->mandatory_answer = $input->mandatory_answer;

			//these are needed by learningObjects behavior component for LearningTest model
			$rootNode = LearningOrganization::model()->getRoot($course->idCourse);
			if (!$rootNode) {
				throw new CHttpException(500, 'Error while saving test');
			}
			$test->idCourse = $course->idCourse;
			$test->idParent = $rootNode->idOrg;

			if (!$test->save()) {
				throw new CHttpException(500, 'Error while saving test');
			}

			//retrieve LO organization ID
			$object = LearningOrganization::model()->findByAttributes(array(
				'idResource' => $test->getPrimaryKey(),
				'objectType' => LearningOrganization::OBJECT_TYPE_TEST,
				'idCourse' => $course->idCourse
			));
			if (!$object) {
				throw new CHttpException(500, 'Error while saving test');
			}

			$transaction->commit();

			//prepare output
			$output = array('scorm_id' => (int)$object->getPrimaryKey());

		} catch(CHttpException $e) {
			$transaction->rollback();
			throw $e;
		} catch(Exception $e) {
			$transaction->rollback();
			throw new CHttpException(500, $e->getMessage());
		}

		return $output;
	}


	/**
	 * @summary Import questions into a test through a gift text file content
	 * @notes the gift file can be linked via url or the entire text can be passed directly as an argument. At least
	 * @notes one parameter between "gift_url" and "gift_text" must be passed to the API.
	 *
	 * @parameter scorm_id [integer, required] numeric ID of the test
	 * @parameter gift_url [string, optional] the url link to a gift file
	 * @parameter gift_text [string, optional] the gift text content to be imported
	 *
	 * @status 201 Error in input data or invalid learning object specification
	 * @status 500 Error in internal operation
	 *
	 * @response imported [array, required] a list of imported questions
	 */
	public function importTestQuestions() {

		//read and validate input data
		$idObject = $this->readParameter('scorm_id');
		$giftUrl = $this->readParameter('gift_url');
		$giftText = $this->readParameter('gift_text');
		//$encoding = $this->readParameter('encoding', 'utf-8');

		if (!$idObject && (!$giftUrl && !$giftText)) {
			throw new CHttpException(201, 'Invalid input data');
		}

		$object = LearningOrganization::model()->findByPk($idObject);
		if (!$object || $object->objectType != LearningOrganization::OBJECT_TYPE_TEST) {
			throw new CHttpException(201, 'The given scorm_id does not correspond to a test');
		}

		$test = LearningTest::model()->findByPk($object->idResource);
		if (!$test) {
			throw new CHttpException(201, 'Invalid specified object');
		}

		//Read gift text content form URL, if specified.
		//Otherwise gift_text parameter already contains text data
		if (!empty($giftUrl)) {
			$giftText = file_get_contents($giftUrl);
		}
		if (empty($giftText)) {
			throw new CHttpException(500, 'Invalid gift url or gift text given');
		}

		$idTest = (int)$test->getPrimaryKey();
		$idCategory = (int)$this->readParameter('id_category', 0); //TODO: this parameter is not yet specified in API, default: 0

		//do import
		$output = array();
		$transaction = Yii::app()->db->beginTransaction();
		try {

			//read all gift file lines
			$fileLines = file($giftUrl);

			$gift = new GiftFormatManager();
			$formatted = $gift->readQuestions($fileLines);

			if (!empty($formatted)) {

				try {

					$sequence = $test->getMaxSequence()+1;
					$page = $test->getMaxPage();
					if ($page <= 0) {
						$page = 1;
					}

					foreach ($formatted as $question) {

						if ((int)$idCategory > 0 && is_object($question)) {
							$question->id_category = (int)$idCategory;
						}

						$questionManager = CQuestionComponent::getQuestionManager($question->qtype);

						if ($questionManager) {
							$rs = $questionManager->importFromRaw($question, $idTest, $sequence, $page);
							if (!$rs) {
								throw new CException('Error while importing questions');
							}
						}

						$sequence++; //increment question sequence at every imported row

						$output[] = array(
							'quest_type' => Yii::t('test', '_QUEST_' . strtoupper($question->qtype)),
							'success' => true //NOTE: this will always be true, in caso of any error an exception is being throwed and all process and DB changes rolled back
						);
					}

					//make sure that values for questions sequence and page are correct
					LearningTestquest::fixTestSequence($idTest);
					LearningTestquest::fixTestPages($idTest);

					$transaction->commit();

				} catch(CException $e) {

					throw new CHttpException(500, $e->getMessage());
				}

			}

		} catch(CHttpException $e) {
			$transaction->rollback();
			throw $e;
		} catch(Exception $e) {
			$transaction->rollback();
			throw new CHttpException(500, $e->getMessage());
		}

		return array('imported' => $output);
	}


	/**
	 * @summary create a ne SCORM object into a course
	 * @notes "course_id" and "course_code" are optional, but at least one of them is required.
	 *
	 * @parameter course_id [integer, optional] numeric ID of the target course
	 * @parameter course_code [string, optional] code of the target course
	 * @parameter obj_name [string, optional] learning object title. If not specified, the default name defined into package is being used.
	 * @parameter zip_address [string, required] an url to the SCORM package file
	 *
	 * @status 201 Error in input data or invalid course specification
	 * @status 500 Error in scorm loading and/or creation
	 *
	 * @response scorm_id [integer, required] the numeircal ID of the newly created learning object
	 * @response scorm_package_id [integer, required] the numerical ID of the imported package
	 */
	public function loadScorm() {

		//read input values
		$input = new stdClass();
		$input->id_course = $this->readParameter('course_id');
		$input->course_code = $this->readParameter('course_code');
		$input->obj_name = $this->readParameter('obj_name');
		$input->zip_address = $this->readParameter('zip_address');

		//validate input values
		if ($input->id_course) {
			$course = LearningCourse::model()->findByPk($input->id_course);
		} elseif ($input->course_code) {
			$course = LearningCourse::model()->findByAttributes(array('code' => $input->course_code));
		}
		if (!$course) {
			throw new CHttpException(201, 'Invalid specified course');
		}

		//load remote file into local temporary upload directory

		//generate random tmp file name
		$scormBaseFileName = 'scorm_tmp_' . time() . '.zip';
		$localZipFileName = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $scormBaseFileName;

		//stream zip url content into local file
		$ch = curl_init($input->zip_address);
		$fp = fopen($localZipFileName, "w");
		if (!$ch || !$fp) {
			throw new CHttpException(500, 'Error while reading scorm package file');
		}
		curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_exec($ch);
		curl_close($ch);
		fclose($fp);

		//starting DB operations
		$transaction = Yii::app()->db->beginTransaction();

		try {

			//retrieve current user idst
			$idUser = Yii::app()->user->id;
			if (!$idUser) {
				//if no current user is found then fallback to anonymous user
				$user = CoreUser::model()->findByAttributes(array('userid' => '/Anonymous'));
				$idUser = $user->getPrimaryKey();
			}

			//open package file and elaborate content
			$package = new LearningScormPackage();
			$package->idUser = $idUser;
			$package->setFile($scormBaseFileName);
			$package->setIdCourse($input->id_course);
			$package->setIdParent(LearningOrganization::model()->getRoot($input->id_course)->getPrimaryKey()); //load scorm object under root node by default
			if (!$package->save()) {
				//TO DO: revert file storing
				throw new CHttpException(500, 'Error while saving DB information');
			}

			//retrieve scorm object AR
			$idPackage = $package->getPrimaryKey();
			$scormObject = LearningScormOrganizations::model()->findByAttributes(array('idscorm_package' => $idPackage));
			if (!$scormObject) {
				throw new CHttpException(500, 'Error in SCORM organization');
			}

			if ($input->obj_name) {
				//retrieve object from learning_organization and change object name
				$object = LearningOrganization::model()->findByAttributes(array(
					'idResource' => $scormObject->getPrimaryKey(),
					'objectType' => LearningOrganization::OBJECT_TYPE_SCORMORG,
					'idCourse' => $input->id_course
				));
				if ($object) {
					$object->title = $input->obj_name;
					$object->saveNode();
				}
			}

			//all operations were ok, commit changes to DB
			$transaction->commit();

		} catch(CHttpException $e) {

			$transaction->rollback();
			throw $e;
		} catch(Exception $e) {

			$transaction->rollback();
			throw new CHttpException(500, $e->getMessage());
		}

		return array(
			'scorm_id' => $scormObject->getPrimaryKey(),
			'scorm_package_id' => $idPackage
		);
	}


	/**
	 * @summary create a ne SCORM object into a course based on a remote playing location
	 * @notes "course_id" and "course_code" are optional, but at least one of them is required.
	 *
	 * @parameter course_id [integer, optional] numeric ID of the target course
	 * @parameter course_code [string, optional] code of the target course
	 * @parameter obj_name [string, optional] learning object title. If not specified, the default name defined into package is being used.
	 * @parameter obj_address [string, required] an url to the SCORM manifest file
	 *
	 * @status 201 Error in input data or invalid course specification
	 * @status 500 Error in scorm loading and/or creation
	 */
	public function loadCloudScorm() {

		//read input values
		$input = new stdClass();
		$input->id_course 	= $this->readParameter('course_id');
		$input->course_code = $this->readParameter('course_code');
		$input->obj_name 	= $this->readParameter('obj_name');
		$input->obj_address = $this->readParameter('obj_address');

		//validate input values
		if ($input->id_course) {
			$course = LearningCourse::model()->findByPk($input->id_course);
		} elseif ($input->course_code) {
			$course = LearningCourse::model()->findByAttributes(array('code' => $input->course_code));
		}
		if (!$course) {
			throw new CHttpException(201, 'Invalid specified course');
		}

		// Starting DB operations
		$transaction = Yii::app()->db->beginTransaction();

		try {

			//retrieve current user idst
			$idUser = Yii::app()->user->id;
			if (!$idUser) {
				//if no current user is found then fallback to anonymous user
				$user = CoreUser::model()->findByAttributes(array('userid' => '/Anonymous'));
				$idUser = $user->getPrimaryKey();
			}

			//open package file and elaborate content
			$package = new LearningScormPackage();
			$package->idUser = $idUser;
			$package->location = 'cloudfront';
			$package->setFile( $input->obj_address );
			
			// Very important: We are loading/injecting a remote content! @see ScormBehavior
			$package->isRemote = true;
			
			$package->setIdCourse($input->id_course);
			$package->setIdParent(LearningOrganization::model()->getRoot($input->id_course)->getPrimaryKey()); //load scorm object under root node by default
			if (!$package->save()) {
				//TO DO: revert file storing
				throw new CHttpException(500, 'Error while saving DB information');
			}

			//retrieve scorm object AR
			$idPackage = $package->getPrimaryKey();
			$scormObject = LearningScormOrganizations::model()->findByAttributes(array('idscorm_package' => $idPackage));
			if (!$scormObject) {
				throw new CHttpException(500, 'Error in SCORM organization');
			}

			if ($input->obj_name) {
				//retrieve object from learning_organization and change object name
				$object = LearningOrganization::model()->findByAttributes(array(
					'idResource' => $scormObject->getPrimaryKey(),
					'objectType' => LearningOrganization::OBJECT_TYPE_SCORMORG,
					'idCourse' => $input->id_course
				));
				if ($object) {
					$object->title = $input->obj_name;
					$object->location = 'cloudfront';
					$object->saveNode();
				}
			}

			//all operations were ok, commit changes to DB
			$transaction->commit();

		} catch(CHttpException $e) {

			$transaction->rollback();
			throw $e;
		} catch(Exception $e) {

			$transaction->rollback();
			throw new CHttpException(500, $e->getMessage());
		}

		return array(
			'success' => true,
		);

	}

}
