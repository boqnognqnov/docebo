<?php

/**
 * Group API Component
 * @description Set of APIs to manage LMS groups
 */
class GroupApiModule extends ApiModule {

	/**
	 * @summary Creates a new LMS group
	 *
	 * @parameter name [string, required] Unique group name (max 255 characters)
	 * @parameter description [string, optional] Extended group description
	 *
	 * @response id_group [integer, required] Internal Docebo ID for the new group. Used in subsequent calls to manage this group.
	 *
	 * @status 201 Missing mandatory param 'name'
	 * @status 205 Validation errors: <list of errors>
	 *
	 */
	public function insert() {
		// Check params
		$name = $this->readParameter('name', null);
		$description = $this->readParameter('description', '');
		if(!$name)
			throw new CHttpException(201, "Missing mandatory param 'name'");

		// Try creation
		$coreGroup = new CoreGroup();
		$coreGroup->description = $description;
		$coreGroup->groupid = $coreGroup->absoluteId($name);
		if ($coreGroup->validate()) {
			$coreGroup->show_on_platform = 'framework,lms,';
			$coreGroup->save();
		} else
			throw new CHttpException(205, "Validation errors: " . $this->errorSummary($coreGroup));

		return array('id_group' => (int)$coreGroup->idst);
	}

	/**
	 * @summary Updates an existing group in the LMS
	 *
	 * @parameter id_group [integer, required] ID of the group to update
	 * @parameter name [string, required] Unique group name (max 255 characters)
	 * @parameter description [string, optional] Extended group description
	 *
	 * @status 201 Missing mandatory params
	 * @status 202 Invalid group ID provided
	 * @status 205 Validation errors: <list of errors>
	 */
	public function update() {
		// Check params
		$id_group = $this->readParameter('id_group', null);
		$name = $this->readParameter('name', null);
		$description = $this->readParameter('description', '');
		if(!$id_group || !$name)
			throw new CHttpException(201, "Missing mandatory params");

		// Try loading group
		$coreGroup = CoreGroup::model()->findByPk($id_group);
		if(!$coreGroup || ($coreGroup->hidden == 'true'))
			throw new CHttpException(202, "Invalid group ID provided");

		// Save group info
		$coreGroup->description = $description;
		$coreGroup->groupid = $coreGroup->absoluteId($name);
		if ($coreGroup->validate())
			$coreGroup->save();
		else
			throw new CHttpException(205, "Validation errors: " . $this->errorSummary($coreGroup));
	}

	/**
	 * @summary Deletes a group in the LMS
	 *
	 * @parameter id_group [integer, required] ID of the group to update
	 *
	 * @status 201 Missing mandatory params
	 * @status 202 Invalid group ID provided
	 */
	public function delete() {
		// Check params
		$id_group = $this->readParameter('id_group', null);
		if(!$id_group)
			throw new CHttpException(201, "Missing mandatory param");

		// Try loading group
		$coreGroup = CoreGroup::model()->findByPk($id_group);
		if(!$coreGroup || ($coreGroup->hidden == 'true'))
			throw new CHttpException(202, "Invalid group ID provided");

		// Delete the group
		$coreGroup->delete();
	}

	/**
	 * @summary Lists all groups currently in the LMS
	 *
	 * @response groups [array, required] Array of group records
	 * 		@item id_group [integer, required] ID of the group
	 * 		@item name [string, required] Group name
	 * 		@item description [description optional] Extended group description
	 */
	public function listGroups() {
		$sql = "SELECT idst as id_group, substr(groupid, 2) as groupid, description
			FROM core_group
			WHERE `hidden` = 'false' AND `type` LIKE '%free%'";

		$records = Yii::app()->db->createCommand($sql)->queryAll();
		$result = array('groups' => array());
		foreach($records as $group)
			$result['groups'][] = array(
				'id_group' => $group['id_group'],
				'name' => $group['groupid'],
				'description' => $group['description']
			);

		return $result;
	}

	/**
	 * @summary Assigns a user to the specified group
	 *
	 * @parameter id_group [integer, required] ID of the group to assign to
	 * @parameter id_user [integer, required] ID of the user to assign
	 *
	 * @status 201 Missing mandatory params
	 * @status 202 Invalid group ID provided
	 * @status 203 Invalid user ID provided
	 * @status 205 User already assigned to this group
	 */
	public function assign() {
		// Check params
		$id_group = $this->readParameter('id_group', null);
		$id_user = $this->readParameter('id_user', null);
		if(!$id_group || !$id_user)
			throw new CHttpException(201, "Missing mandatory params");

		// Try loading group
		$coreGroup = CoreGroup::model()->findByPk($id_group);
		if(!$coreGroup || ($coreGroup->hidden == 'true'))
			throw new CHttpException(202, "Invalid group ID provided");

		// Try loading user
		$coreUser = CoreUser::model()->findByPk($id_user);
		if(!$coreUser)
			throw new CHttpException(203, "Invalid user ID provided");

		// Check if user is already assigned to this group
		$existingGroupMember = CoreGroupMembers::model()->findByAttributes(array('idst' => $coreGroup->idst, 'idstMember' => $id_user));
		if($existingGroupMember)
			throw new CHttpException(205, "User already assigned to this group");

		// Ok, let's do the assignment
		$newGroupMember = new CoreGroupMembers();
		$newGroupMember->idst = $coreGroup->idst;
		$newGroupMember->idstMember = $id_user;
		$newGroupMember->save();
	}

	/**
	 * @summary Removes a user from the specified group
	 *
	 * @parameter id_group [integer, required] ID of the group to unassign from
	 * @parameter id_user [integer, required] ID of the user to unassign
	 *
	 * @status 201 Missing mandatory params
	 * @status 202 Invalid group ID provided
	 * @status 203 Invalid user ID provided
	 * @status 205 User is not assigned to this group
	 */
	public function unassign() {
		// Check params
		$id_group = $this->readParameter('id_group', null);
		$id_user = $this->readParameter('id_user', null);
		if(!$id_group || !$id_user)
			throw new CHttpException(201, "Missing mandatory params");

		// Try loading group
		$coreGroup = CoreGroup::model()->findByPk($id_group);
		if(!$coreGroup || ($coreGroup->hidden == 'true'))
			throw new CHttpException(202, "Invalid group ID provided");

		// Try loading user
		$coreUser = CoreUser::model()->findByPk($id_user);
		if(!$coreUser)
			throw new CHttpException(203, "Invalid user ID provided");

		// Check if user is already assigned to this group
		$existingGroupMember = CoreGroupMembers::model()->findByAttributes(array('idst' => $coreGroup->idst, 'idstMember' => $id_user));
		if(!$existingGroupMember)
			throw new CHttpException(205, "User is not assigned to this group");

		// Delete the group membership
		$existingGroupMember->delete();
	}

	/**
	 * @summary Returns the list of users assigned to a group
	 *
	 * @parameter id_group [integer, required] ID of the group to select
	 *
	 * @response users [array, required] Array of "short" user records
	 * 		@item id_user [integer, required] Internal Docebo ID for the user
	 * 		@item username [string, required] User login name
	 *
	 * @status 201 Missing mandatory params
	 * @status 202 Invalid group ID provided
	 */
	public function members() {
		// Check params
		$id_group = $this->readParameter('id_group', null);
		if(!$id_group)
			throw new CHttpException(201, "Missing mandatory param");

		// Try loading group
		$coreGroup = CoreGroup::model()->findByPk($id_group);
		if(!$coreGroup || ($coreGroup->hidden == 'true'))
			throw new CHttpException(202, "Invalid group ID provided");

		// Run a direct SQL query (for optimization reasons)
		$sql = 'SELECT `t`.`idstMember` AS `id_user`, SUBSTR(`users`.`userid`, 2) AS `username`
		FROM `core_group_members` `t`
		LEFT OUTER JOIN `core_user` `users` ON (`t`.`idstMember`=`users`.`idst`)
		WHERE (t.idst=:id_group)';
		$records = Yii::app()->db->createCommand($sql)->bindValue(':id_group', $id_group)->queryAll();
		$results = array('users' => array());
		foreach($records as $record){
			$record['id_user'] = (int)$record['id_user'];
			$results['users'][] = $record;
		}

		return $results;
	}

	/**
	 * Shared logic between the two bulkAssign & bulkUnassign tasks
	 * @param $type The type of task (assign or unassign)
	 */
	private function handleBulkGroupTask($type) {
		$transaction = Yii::app()->db->beginTransaction();

		try {
			// This API relies on the scheduler app to work
			if(!PluginManager::isPluginActive("SchedulerApp") || !Yii::app()->scheduler)
				throw new CHttpException(201, "Api current not available on this system");

			// Check if there's another user synch job started (avoid duplicate calls)
			$activeJob = CoreJob::model()->findByAttributes(array('handler_id' => BulkApiTask::id(), 'active' => 1, 'name'=>'ApiGroupSynchronization'));
			if($activeJob) {
				$now = Yii::app()->localtime->getUTCNow();
				$lastFinished = Yii::app()->localtime->fromLocalDateTime($activeJob->last_finished);
				if((strtotime($now) - strtotime($lastFinished)) > 24*3600) { //24 hours
					$activeJob->active = 0;
					$activeJob->save();
					Yii::log('Deactivated job: ApiGroupSynchronization (id_job = '.$activeJob->id_job.')', 'debug');
				} else {
					throw new CHttpException(202, "Another  group synchronization is currently running");
				}
			}

			// Set up the import form
			$groupImportForm = new GroupImportForm('step_one');
			$groupImportForm->operationType = $type;
			$groupImportForm->separator = $this->readParameter('separator', "auto");
			$groupImportForm->manualSeparator = $this->readParameter('manual_separator', '');
			$groupImportForm->firstRowAsHeader = $this->readParameter('first_row_header', true);
			$groupImportForm->charset = $this->readParameter('charset', "UTF-8");
			$groupImportForm->file = $this->getCSVFile('groups');
			if ($groupImportForm->validate()) {
				// If uploaded via HTTP form, the file is in the /tmp dir... let's copy it in a safer location
				$groupImportForm->originalFilename = Docebo::randomHash() . "." . $groupImportForm->file->extensionName;
				$groupImportForm->localFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $groupImportForm->originalFilename;
				@copy($groupImportForm->file->tempName, $groupImportForm->localFile);
			} else
				throw new CHttpException(205, "Validation errors: ".$this->errorSummary($groupImportForm));


			// Check on the fields params
			$groupImportForm->scenario = 'step_two';
			$importMap = array(
				'user_column' => $this->readParameter('user_column'),
				'group_column' => $this->readParameter('group_column')
			);
			$groupImportForm->setImportMap($importMap);
			if(!$groupImportForm->validate())
				throw new CHttpException(205, "Validation errors: ".$this->errorSummary($groupImportForm));

			// Time to save the CSV file in S3 for later processing
			// Get the storage object
			$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_ASSETS);
			$storage->store($groupImportForm->localFile);

			// Parse CSV file to calculate totals and save into serialized form
			$groupImportForm->clearErrors();
			$groupImportForm->failedLines = array();
			$groupImportForm->successCounter = 0;
			$csvFile = new QsCsvFile(array(
				'path' => $groupImportForm->localFile,
				'charset' => $groupImportForm->charset,
				'delimiter' => UserImportForm::translateSeparatorName($groupImportForm->separator, $groupImportForm->manualSeparator),
				'firstRowAsHeader' => $groupImportForm->firstRowAsHeader,
				'numLines' => 1, // excluding header, if any
			));
			$groupImportForm->totalItems = $csvFile->getTotalLines();

			// Create a new job to run the chunked user import
			$params = array('limit' => BulkApiTask::MAX_GROUPS_PER_RUN, 'offset' => 0, 'form' => $groupImportForm->getParams(), 'form_type' => "GroupImportForm");
			$scheduler = Yii::app()->scheduler; /* @var $scheduler SchedulerComponent */
			$job = $scheduler->createImmediateJob("ApiGroupSynchronization", BulkApiTask::id(), $params, false, null, true);

			$currentTransaction =  Yii::app()->db->getCurrentTransaction();
			if($currentTransaction){
				$transaction = $currentTransaction;
			}else{
				$transaction = Yii::app()->db->beginTransaction();
			}

			$transaction->commit();

			// Now we can return job ID
			return array('job_id' => $job->hash_id);
		} catch (CHttpException $e) {
			$transaction->rollback();
			Yii::log('Rollback transaction', 'debug');
			throw $e;
		} catch (Exception $e) {
			// Something unforeseen happened..log it and return opaque error message
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			$transaction->rollback();
			Yii::log('Rollback transaction', 'debug');
			throw new CHttpException(206, 'Could not start the group synch job');
		}
	}

	/**
	 * @summary Massively assigns users to groups using an asynchronous job that processes a CSV file.
	 *
	 * @notes CSV files can be uploaded either through a publicly accessible URL or through a multipart POST. Each row of the CSV file must contain two columns:
	 * @notes <ul style="padding-left: 40px; list-style:circle;">
	 * @notes <li>1st column: ID of the user to be assigned or username (depending on user_column param)</li>
	 * @notes <li>2nd column: ID of the group to assign or group name (depending on group_column param).</li>
	 * @notes </ul>
	 *
	 * @parameter user_column [string, required] Whether the user column contains internal Docebo user ids or login names. Possible values: <i>id_user</i> or <i>username].
	 * @parameter group_column [string, required] Whether the group column contains internal Docebo group ids or names. Possible values: [id_group or  groupname].
	 * @parameter file_url [string, optional] Publicly accessible URL where the CSV file can be downloaded from.
	 * @parameter file_upload [file, optional] The name of the uploaded CSV file (via multipart POST).
	 * @parameter separator [string, optional] The type of CSV to use (can be one of the following values: "comma","semicolon","auto","manual"). Defaults to "auto".
	 * @parameter manual_separator [string, optional] If "separator" param is set to "manual", this param is required and it should contain the custom separator to use.
	 * @parameter first_row_header [boolean, optional] Set to true if the first row of the CSV file contains the header. Defaults to true.
	 * @parameter charset [string, optional] The charset to use for the CSV file. Defaults to "UTF-8".
	 *
	 * @response job_id [string, required] A unique ID identifying the scheduled job. Used to get the status of the job
	 *
	 * @status 201 Api current not available on this system
	 * @status 202 Another user synchronization is currently running
	 * @status 203 Missing both file_url and file_upload params
	 * @status 400 "Error while reading remove CSV file" or "Error while reading uploaded CSV file"
	 * @status 205 Validation errors: <list of errors>
	 * @status 206 Could not start the group synch job

	 * @status ---
	 */
	public function bulkAssign() {
		return $this->handleBulkGroupTask('assign');
	}

	/**
	 * @summary Massively unassigns users from groups using an asynchronous job that processes a CSV file.
	 *
	 * @notes CSV files can be uploaded either through a publicly accessible URL or through a multipart POST. Each row of the CSV file must contain two columns:
	 * @notes <ul style="padding-left: 40px; list-style:circle;">
	 * @notes <li>1st column: ID of the user to be un-assigned or username (depending on user_column param)</li>
	 * @notes <li>2nd column: ID of the group to un-assign from or group name (depending on group_column param).</li>
	 * @notes </ul>
	 *
	 * @parameter user_column [string, required] Whether the user column contains internal Docebo user ids or login names. Possible values: <i>id_user</i> or <i>username].
	 * @parameter group_column [string, required] Whether the group column contains internal Docebo group ids or names. Possible values: [id_group or  groupname].
	 * @parameter file_url [string, optional] Publicly accessible URL where the CSV file can be downloaded from.
	 * @parameter file_upload [file, optional] The name of the uploaded CSV file (via multipart POST).
	 * @parameter separator [string, optional] The type of CSV to use (can be one of the following values: "comma","semicolon","auto","manual"). Defaults to "auto".
	 * @parameter manual_separator [string, optional] If "separator" param is set to "manual", this param is required and it should contain the custom separator to use.
	 * @parameter first_row_header [boolean, optional] Set to true if the first row of the CSV file contains the header. Defaults to true.
	 * @parameter charset [string, optional] The charset to use for the CSV file. Defaults to "UTF-8".
	 *
	 * @response job_id [string, required] A unique ID identifying the scheduled job. Used to get the status of the job
	 *
	 * @status 201 Api current not available on this system
	 * @status 202 Another user synchronization is currently running
	 * @status 203 Missing both file_url and file_upload params
	 * @status 400 "Error while reading remove CSV file" or "Error while reading uploaded CSV file"
	 * @status 205 Validation errors: <list of errors>
	 * @status 206 Could not start the group synch job
	 */
	public function bulkUnassign() {
		return $this->handleBulkGroupTask('unassign');
	}

	/**
	 * @summary Gets the current status of the bulk group assignment/unassignment job.
	 *
	 * @parameter job_id [string, required] ID of the scheduled job, as returned by group/bulkAssign or group/bulkUnassign.
	 *
	 * @status 201 Missing required param "job_id"
	 * @status 202 Invalid job ID param
	 *
	 * @response status [string, required] One of the following statuses: “running”, “completed”
	 * @response total_records [integer, required] The total number of records to process
	 * @response processed_records [integer, required] The number of records already processed
	 * @response errors [array, required] Array of error messages occurred while processing group records
	 * 		@item error [string, required] The error message generated while synching a group
	 */
	public function synchStatus() {
		// Check mandatory param
		$job_id = $this->readParameter('job_id', null);
		if(!$job_id)
			throw new CHttpException(201, 'Missing required param "job_id"');

		// Try loading the job
		/* @var $job CoreJob */
		$job = CoreJob::model()->findByAttributes(array('hash_id' => $job_id));
		if(!$job)
			throw new CHttpException(202, "Invalid job ID param");

		// Fetch job params
		$params = json_decode($job->params, true);
		$importForm = new GroupImportForm(); /* @var $importForm GroupImportForm */
		foreach ($params['form'] as $attribute => $value) {
			if(property_exists($importForm, $attribute))
				$importForm->$attribute = $params['form'][$attribute];
			else if($attribute == 'importMap')
				$importForm->setImportMap($value);
		}

		$result = array (
			'status' => 'running',
			'processed_records' => ($params['offset'] > $importForm->totalItems) ? $importForm->totalItems : $params['offset'],
			'total_records' => $importForm->totalItems,
			'errors' => array()
		);

		// Job finished -> prepare error summary
		if($job->active == 0) {
			$result['status'] = 'completed';
			$logFileName = $job->id_job . "_failed_lines.csv";
			$localLogFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $logFileName;
			$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_ASSETS);
			if($storage->downloadAs($logFileName, $localLogFilePath)) {
				// Open log file
				$fh = fopen($localLogFilePath, "r");
				if ($fh !== FALSE) {
					while (($line = fgetcsv($fh)) !== FALSE) {
						$result['errors'][] = array(
							'error' => implode("|", $line)
						);
					}
					fclose($fh);
				}
			}
		}

		return $result;
	}
}
