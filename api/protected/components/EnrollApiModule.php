<?php

/**
 * Enroll API Component
 * @description Set of APIs to manage enrollments of the LMS
 */
class EnrollApiModule extends ApiModule {


	/**
	 * @summary Counts the number of enrolled users, eventually filtered by some user-defined criteria
	 * @notes all filter parameters are optional. If none is specified, all enrolled users for all courses are counted.
	 *
	 * @parameter id_user [integer, optional] numeric ID of an user
	 * @parameter id_course [string, optional] numeric ID of a course
	 * @parameter updated_from [string, optional] additional filter on enrollments, date filter on "last_access" property
	 * @parameter user_ext_type [string, optional] external user type
	 * @parameter user_ext [string, optional] external user ID
	 * @parameter completed_from [datetime, optional] the start date & time used to filter enrollments based on the completion date (in yyyy-MM-dd HH:mm:ss format, UTC timezone)
	 * @parameter completed_to [datetime, optional]  the end date & time used to filter enrollments based on the completion date (in yyyy-MM-dd HH:mm:ss format, UTC timezone).
	 *
	 * @response count [integer, required] the number of counted users
	 */
	public function count() {

		//read input
		$input = new stdClass();
		$input->id_user = $this->readParameter('id_user');
		$input->id_course = $this->readParameter('id_course');
		$input->updated_from = $this->readParameter('updated_from');

		$input->ext_user_type = $this->readParameter('ext_user_type');
		$input->ext_user = $this->readParameter('ext_user');
		$input->completed_from = $this->readParameter('completed_from');
		$input->completed_to = $this->readParameter('completed_to');

		//do elaboration
		$command = Yii::app()->db->createCommand()
			->select("COUNT(*) AS tot")
			->from(LearningCourseuser::model()->tableName()." cu");
		
		$statuses = array(
			LearningCourseuser::$COURSE_USER_SUBSCRIBED,
			LearningCourseuser::$COURSE_USER_BEGIN,
			LearningCourseuser::$COURSE_USER_END
		);

		if (empty($input->ext_user_type)) {
			$command->where(array("IN", "cu.status", $statuses));
		} elseif (!empty($input->ext_user_type) && empty($input->ext_user)) {
			$command->join(CoreSettingUser::model()->tableName()." su", "cu.idUser = su.id_user");
			$command->where(array("IN", "cu.status", $statuses));
			$command->andWhere("su.path_name = :path_name", array(':path_name' => 'ext.user.'.$input->ext_user_type));
		} else {
			$command->join(CoreSettingUser::model()->tableName()." su", "cu.idUser = su.id_user");
			$command->where(array("IN", "cu.status", $statuses));
			$command->andWhere("su.path_name = :path_name", array(':path_name' => 'ext.user.'.$input->ext_user_type));
			$command->andWhere("su.value = :value", array(':value' => 'ext_user_'.$input->ext_user_type.'_'.$input->ext_user));
		}
		
		if ($input->updated_from) {
			$command->andWhere("(cu.date_inscr >= :update_from
				OR cu.date_first_access >= :update_from
				OR cu.date_complete >= :update_from
				OR cu.date_last_access >= :update_from)", array(':update_from' => $input->updated_from));
		}

		if ($input->id_user) { $command->andWhere("cu.idUser = :id_user", array(':id_user' => $input->id_user)); }
		if ($input->id_course) { $command->andWhere("cu.idCourse = :id_course", array(':id_course' => $input->id_course)); }
		if ($input->completed_from) $command->andWhere('cu.date_complete>=:dateFrom', array(':dateFrom' => $input->completed_from));
		if ($input->completed_to) $command->andWhere('cu.date_complete<=:dateTo', array(':dateTo' => $input->completed_to));

		//execute DB calculation
		$count = $command->queryScalar();

		return array('count' => (int)$count);
	}

	/**
	 * @summary Lists the enrolled users, eventually filtered by some user-defined criteria
	 * @notes All filter parameters are optional. If none of them is specified, all enrolled users for all courses are counted.
	 *
	 * @parameter id_user [integer, optional] numeric ID of an user
	 * @parameter id_course [string, optional] nuemric ID of a course
	 * @parameter updated_from [string, optional] additional filter on enrollments, date filter on "last_access" property
	 * @parameter user_ext_type [string, optional] external user type
	 * @parameter user_ext [string, optional] external user ID
	 * @parameter from [integer, optional] limit the resultset by record offset
	 * @parameter count [integer, optional] limit the resultset by number of records
	 * @parameter start_date [datetime, optional] limit the resultset by date (yyyy-MM-dd HH:mm:ss format, UTC timezone)
	 * @parameter end_date [datetime, optional] limit the resultset by date (yyyy-MM-dd HH:mm:ss format, UTC timezone)
	 * @parameter username [string, optional] limit the resultset by username
	 * @parameter completed_from [datetime, optional] the start date & time used to filter enrollments based on the completion date (in yyyy-MM-dd HH:mm:ss format, UTC timezone).
	 * @parameter completed_to [datetime, optional] the end date & time used to filter enrollments based on the completion date (in yyyy-MM-dd HH:mm:ss format, UTC timezone).
	 *
	 *
	 * @response enrollments [array, required] The retrieved enrollments
     *      @item id_user [integer, required] The ID of the user
	 *      @item username [string, required] The login name of the user
     *      @item id_course [integer, required] ID of the enrolled course
     *      @item course_code [string, optional] Code of the enrolled course
     *      @item level [integer, required] Level ID
     *      @item status [integer, required] Status ID
     *      @item date_enrollment [datetime, required] Enrollment datetime
     *      @item date_first_access [datetime, required] Course first access datetime
     *      @item date_complete [datetime, required] Completion datetime
     *      @item date_last_access [datetime, required] Lastaccess datetime
	 *      @item active_from [datetime, required] Start date after which the user can access the course
	 *      @item active_until [datetime, required] Last available date for the user to access the course
	 *      @item first_name [string, optional] User first name
	 *      @item last_name [string, optional] User last name
	 *      @item email [string, required] User email
	 *      @item course_link [string, optional] the course link (see course/courses)
	 *      @item course_name [string, required] The full title of the course
	 *      @item course_fields [array, optional] The set of additional fields for this course
	 *      	@item id [integer, required] Internal fields ID
	 *      	@item name [string, required] Field name
	 *      	@item value [string, required] Field name
	 *		@item enrollment_fields [array, optional] The set of additional fields for this enrollment
	 *      	@item id [integer, required] Internal fields ID
	 *      	@item name [string, required] Field name
	 *      	@item value [string, required] Field name
	 *			@item option_name [string, optional] Name of the option if the field is from dropdown type
	 *      @end course_fields
	 *      @item total_time [string, required] Total time spent in course (in xh ym zs, where x is the number of hours, y is the number of minutes and z is the number of seconds)
	 */

	public function enrollments() {

		//read input
		$input = new stdClass();
		$input->id_user = $this->readParameter('id_user');
		$input->id_course = $this->readParameter('id_course');
		$input->updated_from = $this->readParameter('updated_from');

		$input->ext_user_type = $this->readParameter('ext_user_type');
		$input->ext_user	= $this->readParameter('ext_user');
		
		$input->from = $this->readParameter('from');
		$input->count = $this->readParameter('count');
		$input->completed_from = $this->readParameter('completed_from');
		$input->completed_to = $this->readParameter('completed_to');

		$input->start_date = $this->readParameter('start_date');
		$input->end_date = $this->readParameter('end_date');

		$input->username = $this->readParameter('username');

		//do elaboration
		$command = Yii::app()->db->createCommand()
			->select("cu.*, SUBSTR(userid,2) AS userid, c.code, c.name, u.firstname, u.lastname, email")
			->from(LearningCourseuser::model()->tableName()." cu")
            ->join(LearningCourse::model()->tableName()." c", "c.idCourse = cu.idCourse")
			->join(CoreUser::model()->tableName()." u", "u.idst = cu.idUser");
		
		$statuses = array(
			LearningCourseuser::$COURSE_USER_SUBSCRIBED,
			LearningCourseuser::$COURSE_USER_BEGIN,
			LearningCourseuser::$COURSE_USER_END
		);

		if (empty($input->ext_user_type)) {
			$command->where(array("IN", "cu.status", $statuses));
		} elseif (!empty($input->ext_user_type) && empty($input->ext_user)) {
			$command->join(CoreSettingUser::model()->tableName()." su", "cu.idUser = su.id_user");
			$command->where(array("IN", "cu.status", $statuses));
			$command->andWhere("su.path_name = :path_name", array(':path_name' => 'ext.user.'.$input->ext_user_type));
		} else {
			$command->join(CoreSettingUser::model()->tableName()." su", "cu.idUser = su.id_user");
			$command->where(array("IN", "cu.status", $statuses));
			$command->andWhere("su.path_name = :path_name", array(':path_name' => 'ext.user.'.$input->ext_user_type));
			$command->andWhere("su.value = :value", array(':value' => 'ext_user_'.$input->ext_user_type.'_'.$input->ext_user));
		}
		
		if ($input->updated_from) {
			$command->andWhere("(cu.date_inscr >= :update_from
				OR cu.date_first_access >= :update_from
				OR cu.date_complete >= :update_from
				OR cu.date_last_access >= :update_from)", array(':update_from' => $input->updated_from));
		}

		if ($input->id_user) { $command->andWhere("cu.idUser = :id_user", array(':id_user' => $input->id_user)); }
		if ($input->id_course) { $command->andWhere("cu.idCourse = :id_course", array(':id_course' => $input->id_course)); }

		// date filtering /if any/
		if ($input->completed_from) $command->andWhere("cu.date_complete >= :dateFrom", array(':dateFrom' => $input->completed_from));
		if ($input->completed_to) $command->andWhere("cu.date_complete <= :dateTo", array(':dateTo' => $input->completed_to));

		if ($input->start_date) {
			$command->andWhere("cu.date_inscr > :startDate", array(':startDate' => $input->start_date));
		}

		if ($input->end_date) {
			$command->andWhere("cu.date_inscr < :endDate", array(':endDate' => $input->end_date));
		}

		if ($input->username) { $command->having("userid = :username", array(':username' => $input->username)); }

		$command->order("cu.date_inscr ASC");

		if ((int)$input->count > 0) {
			$command->limit((int)$input->count, (int)$input->from);
		}

		//execute DB calculation
		$list = $command->queryAll();

		$learningCourseModel = LearningCourse::model();
		$additionalFields = $learningCourseModel->getCourseAdditionalFields();

		$output = array();
		if (!empty($list)) {
			foreach ($list as $item) {
				$output[] = array(
					'id_user'				=> (int)$item['idUser'],
					'username'				=> $item['userid'],
					'id_course'				=> (int)$item['idCourse'],
                    'course_code'           => $item['code'],
					'level'					=> (int)$item['level'],
					'status'				=> (int)$item['status'],
					'date_enrollment'		=> $item['date_inscr'],
					'date_first_access'		=> $item['date_first_access'],
					'date_complete'			=> $item['date_complete'],
					'date_last_access'		=> $item['date_last_access'],
					'active_from' 			=> $item['date_begin_validity'],
					'active_until' 			=> $item['date_expire_validity'],
					'first_name' 			=> $item['firstname'],
					'last_name' 			=> $item['lastname'],
					'email' 				=> $item['email'],
					'course_name' 			=> $item['name'],
					'course_fields' 		=> $this->_getCourseAdditionalFields($item['idCourse'], $additionalFields),
					'enrollment_fields'		=> $this->_getEnrollmentAdditionalFields($item['enrollment_fields']),
					'total_time' 			=> LearningTracksession::model()->getUserTotalTime($item['idUser'], $item['idCourse'], 'G\h i\m s\s'),
                    'course_link' 			=> Docebo::createAbsoluteLmsUrl('player', array('course_id' => (int)$item['idCourse']))
				);
			}
		}

		return array('enrollments' => $output);
	}

	private function _getCourseAdditionalFields($idCourse, $additionalFields){
		static $courseAdditionalFields = array();

		if (!isset($courseAdditionalFields[$idCourse])){
			$courseFields = array();
			foreach ($additionalFields as $field) {
				$value = $this->_getCourseAdditioanlFieldValue($idCourse, $field['id_field'], true);
				$courseFields[] = array(
					'id' => (int)$field['id_field'],
					'name' => $field->getTranslation($field['id_field']),
					'value' => $value
				);
			}

			$courseAdditionalFields[$idCourse] = $courseFields;
		}

		return $courseAdditionalFields[$idCourse];
	}
	
	private function _getEnrollmentAdditionalFields($jsonFields){
		
		$fieldsArray = json_decode($jsonFields, true);
		Log::_($fieldsArray);
		if(!is_array($fieldsArray) || empty($fieldsArray))
			return array();
		
		$defaultLanguage = Lang::getBrowserCodeByCode(Settings::get('default_language', 'english'));
		// Resolve requested language
		$language = Yii::app()->getLanguage();
		$returnArray = array();
		foreach ($fieldsArray as $key => $value) {
			$command = Yii::app()->db->createCommand();
			$command->select('IF(JSON_UNQUOTE(translation->"$.'.$language.'") IS NULL OR JSON_UNQUOTE(translation->"$.'.$language.'") = "", JSON_UNQUOTE(translation->"$.'.$defaultLanguage.'"), JSON_UNQUOTE(translation->"$.'.$language.'")) as title, '
					. 'IF(type = 2, (SELECT IF(JSON_UNQUOTE(translation->"$.'.$language.'") IS NULL OR JSON_UNQUOTE(translation->"$.'.$language.'") = "", JSON_UNQUOTE(translation->"$.'.$defaultLanguage.'"), JSON_UNQUOTE(translation->"$.'.$language.'")) '
					. 'FROM learning_enrollment_fields_dropdown WHERE id=\''.$value.'\'), "") AS optionTitle');
			$command->from('learning_enrollment_fields');
			$command->where('id = :fieldId', array(':fieldId' => $key));
			$result = $command->queryRow();
			$returnArray[] = array(
				'id' => $key,
				'name' => $result['title'],
				'value' => $value,
				'option_name' => $result['optionTitle']
			);
			
		}
		return $returnArray;
	}

	private function _getCourseAdditioanlFieldValue($courseId, $fieldId, $dropdownKeyInsteadOfValue = false){
		$fieldType = Yii::app()->db->createCommand()
			->select('type')
			->from(LearningCourseField::model()->tableName())
			->where('id_field = '.((int)$fieldId))
			->queryScalar();

		$cid = ($courseId)? $courseId : $this->idCourse;
		$result = Yii::app()->db->createCommand()
			->select('field_'.$fieldId.' v')
			->from(LearningCourseFieldValue::model()->tableName().' fv')
			->where('id_course = '.$cid);

		$result = $result->queryScalar();
		if($fieldType==LearningCourseField::TYPE_FIELD_DROPDOWN && $result) {
			if($dropdownKeyInsteadOfValue)
				return $result;
			else {
				$translation = Yii::app()->db->createCommand()
					->select('translation')
					->from(LearningCourseFieldDropdownTranslations::model()->tableName())
					->where('id_option = ' . $result . ' AND lang_code = "' . Lang::getCodeByBrowserCode(Yii::app()->getLanguage()) . '"')
					->queryScalar();
				if (!$translation)
					$translation = Yii::app()->db->createCommand()
						->select('translation')
						->from(LearningCourseFieldDropdownTranslations::model()->tableName())
						->where('id_option = ' . $result . ' AND lang_code = "' . Settings::get('default_language') . '"')
						->queryScalar();

				$result = $translation;
			}
		}
		return $result;
	}

    /**
     * @summary Lists the enrolled users, eventually filtered by some user-defined criteria
     * @notes All filter parameters are optional. If none of them is specified, all enrolled users for all courses are counted.
     *
     * @parameter id_user [integer, optional] numeric ID of an user
     * @parameter id_course [string, optional] nuemric ID of a course
     * @parameter updated_from [string, optional] additional filter on enrollments, date filter on "last_access" property
     * @parameter user_ext_type [string, optional] external user type
     * @parameter user_ext [string, optional] external user ID
     * @parameter from [integer, optional] limit the resultset by record offset
     * @parameter count [integer, optional] limit the resultset by number of records
     * @parameter include_waitlisted [boolean, optional] Whether or not to include waitlisted users in the returned records (default = false)
     *
     * @response iltenrollments [array, required] The retrieved enrollments
     *      @item id_user [integer, required] The ID of the user
     *      @item username [string, required] The login name of the user
     *      @item firstname [string, required] The first name of the user
     *      @item lastname [string, required] The last name of the user
     *      @item id_course [integer, required] ID of the enrolled course
     *      @item level [integer, required] Level ID
     *      @item status [string, required] Enrollment status label
     *      @item status_id [integer, required] Status ID ('-2' - 'Waiting list', '0' - 'Subscibed', '1' - 'In progress', '2' - 'Completed')
     *      @item date_enrollment [datetime, required] Enrollment datetime
     *      @item date_first_access [datetime, required] Course first access datetime
     *      @item date_complete [datetime, required] Completion datetime
     *      @item date_last_access [datetime, required] Lastaccess datetime
     *      @item idSession [integer, required] Session ID
     *      @item session_start_date [datetime, required] Session Start date (in UTC)
     *      @item session_end_date [datetime, required] Session End date (in UTC)
     *      @item total_hours [datetime, required] Session duration time
     *      @item dates [array, required] An array of the dates for each session
     *      @item date [datetime, required] The date in a give day for certain session
     *      @item date_name [string, required] The name of the date
     *      @item date_starting_time [datetime, required] Time of beginning of the date
     *      @item date_ending_time [datetime, required] End time of the date
     *      @item date_timezone [string, required] The timezone of the date
     *      @item date_user_attendance [datetime, required] They day of the user date attendance
     *      @item date_location_name [string, required] The location name of the date
     *      @item date_room_name [string, required] The room name of the date
     *      @item date_location_country [string, required] The country name of the location
     *      @item date_location_address [string, required] The address of the date's location
     */
    public function iltenrollments() {

        //read input
        $input = new stdClass();
        $input->id_user = $this->readParameter('id_user');
        $input->id_course = $this->readParameter('id_course');
        $input->updated_from = $this->readParameter('updated_from');

        $input->ext_user_type = $this->readParameter('ext_user_type');
        $input->ext_user	= $this->readParameter('ext_user');

        $input->from = $this->readParameter('from');
        $input->count = $this->readParameter('count');
        $input->include_waitlisted = $this->readParameter('include_waitlisted');

		if($input->include_waitlisted == 'true' || $input->include_waitlisted === true || intval($input->include_waitlisted) === 1){
			$input->include_waitlisted = true;
		}else{
			$input->include_waitlisted = false;
		}

        $selectedColumns = array(
            "idst",
            "SUBSTR(userid,2) AS userid",
            "u.firstname",
            "u.lastname",
            "lc.idCourse",
            "lcu.level",
            "cus.status",
            "cus.date_subscribed",
            "lcu.date_first_access",
            "lcu.date_complete",
            "lcu.date_last_access",
            "lcs.id_session",
            "lcs.date_begin",
            "lcs.date_end",
            "lcs.total_hours"
        );

        //do elaboration
        $command = Yii::app()->db->createCommand()
            ->select($selectedColumns)
            ->from(LtCourseuserSession::model()->tableName()." cus")
            ->join(CoreUser::model()->tableName()." u", "u.idst = cus.id_user")
            ->leftJoin(LtCourseSession::model()->tableName() . ' lcs', 'lcs.id_session = cus.id_session')
            ->leftJoin(LearningCourse::model()->tableName(). ' lc', 'lc.idCourse = lcs.course_id')
            ->leftJoin(LearningCourseuser::model()->tableName(). ' lcu', 'lcu.idCourse = lc.idCourse AND lcu.idUser = u.idst');

        $statuses = array(
            LearningCourseuser::$COURSE_USER_SUBSCRIBED,
            LearningCourseuser::$COURSE_USER_BEGIN,
            LearningCourseuser::$COURSE_USER_END
        );
		if ($input->include_waitlisted === true) {
			$statuses[] = LtCourseuserSession::$SESSION_USER_WAITING_LIST;
		}

        if (empty($input->ext_user_type)) {

            $command->where(array("IN", "cus.status", $statuses));

        } elseif (!empty($input->ext_user_type) && empty($input->ext_user)) {

            $command->join(CoreSettingUser::model()->tableName()." su", "cus.id_user = su.id_user");
            $command->where(array("IN", "cus.status", $statuses));
            $command->andWhere("su.path_name = :path_name", array(':path_name' => 'ext.user.'.$input->ext_user_type));

        } else {

            $command->join(CoreSettingUser::model()->tableName()." su", "cus.id_user = su.id_user");
            $command->where(array("IN", "cus.status", $statuses));
            $command->andWhere("su.path_name = :path_name", array(':path_name' => 'ext.user.'.$input->ext_user_type));
            $command->andWhere("su.value = :value", array(':value' => 'ext_user_'.$input->ext_user_type.'_'.$input->ext_user));

        }

        // This field is only for the Normal courses and not for ILT Courses
        if ($input->updated_from) {
            $command->andWhere("(lcu.date_inscr >= :update_from
				OR lcu.date_first_access >= :update_from
				OR lcu.date_complete >= :update_from
				OR lcu.date_last_access >= :update_from)", array(':update_from' => $input->updated_from));
        }

        if ($input->id_user) { $command->andWhere("cus.id_user = :user", array(':user' => $input->id_user)); }
        if ($input->id_course) {$command->andWhere("lc.idCourse = :course", array(':course' => $input->id_course));}

        $command->order("lcu.date_inscr ASC");

        if ((int)$input->count > 0) {
            $command->limit((int)$input->count, (int)$input->from);
        }
        //execute DB calculation
        $list = $command->queryAll();

        $output = array();
        if (!empty($list)) {
            foreach ($list as $item) {

                $datesColumns = array(
                    "t.day              as date",
                    "t.name             as date_name",
                    "t.time_begin       as date_starting_time",
                    "t.time_end         as date_ending_time",
                    "t.timezone         as date_timezone",
                    "ua.day             as date_user_attendance",
                    "dr.name            as date_room_name",
                    "dl.name            as date_location_name",
                    "dlc.name_country   as date_location_country",
                    "dl.address         as date_location_address",
                );
                $dates = Yii::app()->db->createCommand()
                    ->select($datesColumns)
                    ->from(LtCourseSessionDate::model()->tableName() . ' t')
					->leftJoin(LtCourseSessionDateAttendance::model()->tableName() . ' ua', 'ua.id_session = t.id_session and t.day = ua.day and ua.id_user = ' . $item['idst'])
                    ->leftJoin(LtLocation::model()->tableName() . ' dl', 'dl.id_location = t.id_location')
                    ->leftJoin(CoreCountry::model()->tableName() . ' dlc', 'dlc.id_country = dl.id_country')
                    ->leftJoin(LtClassroom::model()->tableName() . ' dr', 'dr.id_classroom = t.id_classroom')
                    ->where('t.id_session = :session' , array(':session' => $item['id_session']) )
                    ->queryAll();

				foreach($dates as $key => $curDate){
					$dates[$key]['date_user_attendance'] = (bool)$dates[$key]['date_user_attendance'];
				}

                $output[] = array(
                    'id_user'				=> (int)$item['idst'],
                    'username'				=> $item['userid'],
                    'firstname'				=> $item['firstname'],
                    'lastname'				=> $item['lastname'],
                    'id_course'				=> (int)$item['idCourse'],
                    'level'					=> $item['level'] ? Yii::t("levels", "_LEVEL_".$item['level']) : '',
                    'status'				=> LearningCourseuser::getStatusLabel($item['status']),
                    'Status_ID'				=> $item['status'],
                    'date_enrollment'		=> $item['date_subscribed'],
                    'date_first_access'		=> $item['date_first_access'],
                    'date_complete'			=> $item['date_complete'],
                    'date_last_access'		=> $item['date_last_access'],
                    'idSession'     		=> (int)$item['id_session'],
                    'session_start_date'	=> Yii::app()->localtime->toUTC($item['date_begin']),
                    'session_end_date'	    => Yii::app()->localtime->toUTC($item['date_end']),
                    'total_session_hours'	=> $item['total_hours'],
                    'dates'                 => $dates
                );
            }
        }

        return array('iltenrollments' => $output);
    }

	/**
	 * @summary Lists the enrolled users, eventually filtered by some user-defined criteria
	 *
	 * @parameter year [integer, optional] All the sessions that start in that year
	 * @parameter course_id [integer, optional] Numeric ID of a course
	 * @parameter user_id [integer, optional] Numeric ID of a user
	 * @parameter from [integer, optional] limit the resultset by record offset
	 * @parameter count [integer, optional] limit the resultset by number of records
	 *
	 * @response iltenrollments_status [array, required] The retrieved enrollments
	 *      @item id_user [integer, required] The ID of the user
	 *      @item username [string, required] The login name of the user
	 *      @item firstname [string, required] The first name of the user
	 *      @item lastname [string, required] The last name of the user
	 *      @item course_id [integer, required] ID of the enrolled course
	 *      @item course_code [string, required] Code of the enrolled course
	 *      @item course_title [string, required] Title of the enrolled course
	 *      @item course_description [string, required] Description of the enrolled course
	 *      @item course_language [string, required] Language of the enrolled course
	 *      @item course_type [string, required] Type of the enrolled course
	 *      @item course_link [string, required] URL the enrolled course
	 *      @item level [string, required] The level of the user
	 *      @item status [string, required] Enrollment status label
	 *      @item date_enrollment [datetime, required] Enrollment datetime
	 *      @item date_first_access [datetime, required] Course first access datetime
	 *      @item date_complete [datetime, required] Completion datetime
	 *      @item date_last_access [datetime, required] Lastaccess datetime
	 *      @item can_enter [boolean, required] If the user can access the course at the time of the call
	 *      @item progress [integer, required] The progress of the user into the course
	 *      @item final_score [string, required] Final score of the user
	 *      @item idSession [integer, required] Session ID
	 *      @item session_start_date [datetime, required] Session Start date
	 *      @item session_start_date_timezone [string, required] Session Start date timezone
	 *      @item session_end_date [datetime, required] Session End date
	 *      @item session_end_date_timezone [string, required] Session End date timezone
	 *      @item total_hours [datetime, required] Session duration time
	 *      @item session_teachers [array, required] All the users initials enrolled in the course with the "teacher" role
	 * 			@item username [string, required] Teacher's username
	 *      @end session_teachers
	 *      @item session_coaches [array, required] All the users initials enrolled in the course with the "coaches" role
	 * 			@item username [string, required] Coaches username
	 *      @end session_coaches
	 *      @item session_tutors [array, required] All the users initials enrolled in the course with the "tutors" role
	 * 			@item username [string, required] Tutor's username
	 *      @end session_tutors
	 *      @item dates [array, required] An array of the dates for each session
	 *      	@item date [datetime, required] The date in a give day for certain session
	 *      	@item date_name [string, required] The name of the date
	 *      	@item date_starting_time [datetime, required] Time of beginning of the date
	 *      	@item date_ending_time [datetime, required] End time of the date
	 *      	@item date_timezone [string, required] The timezone of the date
	 *      	@item date_user_attendance [datetime, required] They day of the user date attendance
	 *      	@item date_location_name [string, required] The location name of the date
	 *      	@item date_room_name [string, required] The room name of the date
	 *      	@item date_location_country [string, required] The country name of the location
	 *      	@item date_location_address [string, required] The address of the date's location
	 *      @end dates
	 *
	 * @status 201 Missing mandatory params
	 */
	public function iltenrollments_status() {
		set_time_limit(60*5); //just in case, set the time limit to be 5 minutes
		$max_items = 5000;

		//read input
		$input = new stdClass();
		$input->year = $this->readParameter('year');
		$input->course_id = $this->readParameter('course_id');
		$input->user_id = $this->readParameter('user_id');
		$input->from = $this->readParameter('from', 0);
		$input->count = $this->readParameter('count', $max_items);

		// If the count is bigger than the allowed max count, limit it.
		if(!$input->count || ($input->count > $max_items))
			$input->count = $max_items;

		// If no of the required fields is added them error
		if(!($input->year || $input->course_id || $input->user_id))
			throw new CHttpException(201, "Missing mandatory params");

		$progress = "(SELECT ROUND(COUNT(IF(ilct.status = 'completed', 1, NULL)) * 100 / COUNT(1))
			FROM learning_organization it
			LEFT JOIN learning_commontrack ilct ON (ilct.idReference=it.idOrg)
			WHERE (ilct.idUser=lcu.idUser) AND (it.idCourse = lcu.idCourse) AND (it.visible) AND (it.objectType != '')) AS progress";
		$managers = "(SELECT CONCAT_WS(':',GROUP_CONCAT(IF(ilcu.level = 6, SUBSTR(userid,2), NULL)  SEPARATOR ','), GROUP_CONCAT(IF(ilcu.level = 4, SUBSTR(userid,2), NULL)  SEPARATOR ','))
			FROM `lt_course_session` `ilcs`
			JOIN `learning_courseuser` `ilcu` ON ilcu.idCourse = ilcs.course_id
			JOIN `core_user` `icu` ON icu.idst = ilcu.idUser
			WHERE (ilcs.course_id = lcu.`idCourse`) AND (ilcs.id_session = lcs.`id_session`) AND (ilcu.`level` = 4 OR ilcu.level = 6)) AS managers";

		$selectedColumns = array(
			"idst",
			"SUBSTR(userid,2) AS userid",
			"u.firstname",
			"u.lastname",
			"lc.idCourse",
			"lc.code AS courseCode",
			"lc.name AS courseName",
			"lc.description AS courseDesc",
			"lc.lang_code AS courseLang",
			"lcu.level",
			"cus.status",
			"lcu.score_given as finalScore",
			"cus.date_subscribed",
			"lcu.date_first_access",
			"lcu.date_complete",
			"lcu.date_last_access",
			"lcs.id_session",
			"lcs.name as sessionName",
			"lcs.date_begin",
			"lcs.date_end",
			"lcs.total_hours",
			$progress,
			$managers,
		);

		// Create the query to extracto all the data needed for this API
		$command = Yii::app()->db->createCommand()
			->select($selectedColumns)
			->from(LtCourseuserSession::model()->tableName()." cus")
			->join(CoreUser::model()->tableName()." u", "u.idst = cus.id_user")
			->leftJoin(LtCourseSession::model()->tableName() . ' lcs', 'lcs.id_session = cus.id_session')
			->leftJoin(LearningCourse::model()->tableName(). ' lc', 'lc.idCourse = lcs.course_id')
			->leftJoin(LearningCourseuser::model()->tableName(). ' lcu', 'lcu.idCourse = lc.idCourse AND lcu.idUser = u.idst');

		// Get Subscription for all the user statuses
		$statuses = array(
			LearningCourseuser::$COURSE_USER_SUBSCRIBED,
			LearningCourseuser::$COURSE_USER_BEGIN,
			LearningCourseuser::$COURSE_USER_END,
			LtCourseuserSession::$SESSION_USER_WAITING_LIST
		);
		$command->where(array("IN", "cus.status", $statuses));

		// Filter the query if the user has passed any filters
		if ($input->year) {$command->andWhere("DATE_FORMAT(lcs.date_begin, '%Y') = :year", array(':year' => $input->year));}
		if ($input->user_id) { $command->andWhere("cus.id_user = :user", array(':user' => $input->user_id)); }
		if ($input->course_id) {$command->andWhere("lc.idCourse = :course", array(':course' => $input->course_id));}

		// Order the data by session date creation
		$command->order("lcu.date_inscr ASC");

		if ((int)$input->count > 0) {
			$command->limit((int)$input->count, (int)$input->from);
		}
		//execute DB calculation
		$list = $command->queryAll();

		$output = array();
		if (!empty($list)) {
			// Loop through all the records
			foreach ($list as $item) {
				$instructors = array();
				$tutors = array();
				$managers = explode(':', $item['managers']);
				if(isset($managers[0]) && $managers[0])
					$instructors = explode(',', $managers[0]);
				if(isset($managers[1]) && $managers[1])
					$tutors = explode(',', $managers[1]);
				// Get the dates of the current session
				$dateColumns = array(
					"t.day              as date",
					"t.name             as date_name",
					"t.time_begin       as date_starting_time",
					"t.time_end         as date_ending_time",
					"t.timezone         as date_timezone",
					"ua.day				as date_user_attendance",
					"dr.name            as date_room_name",
					"dl.name            as date_location_name",
					"dlc.name_country   as date_location_country",
					"dl.address         as date_location_address"
				);
				$dates = Yii::app()->db->createCommand()
					->select($dateColumns)
					->from(LtCourseSessionDate::model()->tableName() . ' t')
					->leftJoin(LtCourseSessionDateAttendance::model()->tableName() . ' ua', 'ua.id_session = t.id_session and t.day = ua.day and ua.id_user = ' . $item['idst'])
					->leftJoin(LtLocation::model()->tableName() . ' dl', 'dl.id_location = t.id_location')
					->leftJoin(CoreCountry::model()->tableName() . ' dlc', 'dlc.id_country = dl.id_country')
					->leftJoin(LtClassroom::model()->tableName() . ' dr', 'dr.id_classroom = t.id_classroom')
					->where('t.id_session = :session' , array(':session' => $item['id_session']) )
					->queryAll();

				foreach($dates as $key => $curDate){
					$dates[$key]['date_user_attendance'] = (bool)$dates[$key]['date_user_attendance'];
				}

				$sessionStartDate = Yii::app()->db->createCommand()
					->select('id_session, CONCAT(day, " ", time_begin) date_begin, timezone')
					->from(LtCourseSessionDate::model()->tableName())
					->where('id_session = :session' , array(':session' => $item['id_session']))
					->order('date_begin ASC')
					->limit(1)
					->queryRow();

				$sessionEndDate = Yii::app()->db->createCommand()
					->select('id_session, CONCAT(day, " ", time_end) date_end, timezone')
					->from(LtCourseSessionDate::model()->tableName())
					->where('id_session = :session' , array(':session' => $item['id_session']))
					->order('date_end DESC')
					->limit(1)
					->queryRow();

				$cur = array(
					'id_user'				=> (int)$item['idst'],
					'username'				=> $item['userid'],
					'firstname'				=> $item['firstname'],
					'lastname'				=> $item['lastname'],
					'id_course'				=> (int)$item['idCourse'],
					'course_code'			=> $item['courseCode'],
					'course_title'			=> $item['courseName'],
					'course_description'	=> $item['courseDesc'],
					'course_language'		=> $item['courseLang'],
					'course_type'			=> LearningCourse::CLASSROOM,
					'course_link'			=> Docebo::getOriginalDomain() . Docebo::createLmsUrl('player/training/session', array('course_id' => $item['idCourse'], 'session_id' => $item['id_session'])),
					'level'					=> $item['level'] ? Yii::t("levels", "_LEVEL_".$item['level']) : '',
					'status'				=> LearningCourseuser::getStatusLabel($item['status']),
					'date_enrollment'		=> $item['date_subscribed'],
					'date_first_access'		=> $item['date_first_access'],
					'date_complete'			=> $item['date_complete'],
					'date_last_access'		=> $item['date_last_access'],
					'can_enter'				=> !in_array($item['status'], array(LearningCourseuser::$COURSE_USER_END, LearningCourseuser::$COURSE_USER_SUSPEND, LearningCourseuser::$COURSE_USER_WAITING_LIST)),
					'progress'				=> ($item['progress'] ? $item['progress'] : '0'). '%',
					'final_score'			=> floatval($item['finalScore']),
					'idSession'     		=> (int)$item['id_session'],
					'session_name'     		=> $item['sessionName'],
					'session_start_date'	=> $sessionStartDate['date_begin'],
					'session_start_date_timezone' => $sessionStartDate['timezone'],
					'session_end_date'		=> $sessionEndDate['date_end'],
					'session_end_date_timezone' => $sessionEndDate['timezone'],
					'total_session_hours'	=> (float)$item['total_hours'],
					'session_teachers'		=> $instructors,
					'session_coaches'		=> array(), // Coaches are only available for E-Learning Courses
					'session_tutors'		=> $tutors,
					'dates'                 => $dates
				);

				$output[] = $cur;
			}
		}

		return array('iltenrollments_status' => $output);
	}

	/**
	 * @summary Retrieves course total enrollments
	 *
	 * @status 201 Missing mandatory params
	 *
	 * @parameter year [integer, optional] All the sessions that start in that year
	 * @parameter course_id [integer, optional] Numeric ID of a course
	 * @parameter user_id [integer, optional] Numeric ID of a user
	 *
	 * @response iltenrollments_status_count [integer, required] The total number of enrollments
	 *
	 */
	public function iltenrollments_status_count(){
		//read input
		$input = new stdClass();
		$input->year = $this->readParameter('year');
		$input->course_id = $this->readParameter('course_id');
		$input->user_id = $this->readParameter('user_id');

		// If no of the required fields is added them error
		if(!($input->year || $input->course_id || $input->user_id))
			throw new CHttpException(201, "Missing mandatory params");

		// Create the query to extracto all the data needed for this API
		$command = Yii::app()->db->createCommand()
			->select('count(1)')
			->from(LtCourseuserSession::model()->tableName()." cus")
			->join(CoreUser::model()->tableName()." u", "u.idst = cus.id_user")
			->leftJoin(LtCourseSession::model()->tableName() . ' lcs', 'lcs.id_session = cus.id_session')
			->leftJoin(LearningCourse::model()->tableName(). ' lc', 'lc.idCourse = lcs.course_id')
			->leftJoin(LearningCourseuser::model()->tableName(). ' lcu', 'lcu.idCourse = lc.idCourse AND lcu.idUser = u.idst');

		// Get Subscription for all the user statuses
		$statuses = array(
			LearningCourseuser::$COURSE_USER_SUBSCRIBED,
			LearningCourseuser::$COURSE_USER_BEGIN,
			LearningCourseuser::$COURSE_USER_END,
			LtCourseuserSession::$SESSION_USER_WAITING_LIST
		);
		$command->where(array("IN", "cus.status", $statuses));

		// Filter the query if the user has passed any filters
		if ($input->year) {$command->andWhere("DATE_FORMAT(lcs.date_begin, '%Y') = :year", array(':year' => $input->year));}
		if ($input->user_id) { $command->andWhere("cus.id_user = :user", array(':user' => $input->user_id)); }
		if ($input->course_id) {$command->andWhere("lc.idCourse = :course", array(':course' => $input->course_id));}

		//execute DB calculation
		$count = $command->queryScalar();

		return array('iltenrollments_status_count' => (int)$count);
	}

}
