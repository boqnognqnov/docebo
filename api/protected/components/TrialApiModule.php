<?php

/**
 * Trial API Component
 */
class TrialApiModule extends ApiModule {



	// Method to set the extension type (admin or all) and expiration date for the trial
	public function setExpirationDateAndType(){

		//read and validate input parameters
		$date = $this->readParameter('date');
		$extensionType = $this->readParameter('trial_extension_type');

		//TODO: check date format validity
		if (!$date || !$extensionType) {
			throw new CHttpException(201, 'Expiration date or extension type not specified');
		}
		$timestamp = strtotime($date);
		if (!$timestamp){
			throw new CHttpException(201, 'Invalid expiration date provided');
		}

		//set the trial extension type
		switch ($extensionType) {
			case 'admin':
			case 'all': {
				$setting = CoreSetting::model()->findByAttributes(array(
					'param_name' => 'trial_extension_type',
				));
				if (!$setting) {
					$setting = new CoreSetting();
					$setting->param_name = 'trial_extension_type';
				}
				$setting->param_value = $extensionType;
				if (!$setting->save()) {
					throw  new CHttpException(500, 'Unable to update trial extension type');
				}

			} break;
			default: {
				throw  new CHttpException(500, 'Invalid extension type provided: valid values "admin" and "all"');
			} break;
		}

		// Set the new expiration date to DB param
		$setting = CoreSetting::model()->findByPk('trial_expiration_date');
		if (!$setting) { //trial expiration date MUST be set in settings table
			throw new CHttpException(500, 'Unable to update trial expiration date');
		}
		$setting->param_value = date('Y-m-d H:i:s', $timestamp);
		if (!$setting->save()) {
			throw new CHttpException(500, 'Unable to update trial expiration date');
		}

		return array('message' => 'Expiration date and extension type changed successfully');
	}



	// An API method to set the trial expiration date for the platform
	public function setExpirationDate(){

		//read and validate input parameters
		$date = $this->readParameter('date');
		$maxUsers = (int)$this->readParameter('max_users', 0);

		//TODO: check date format validity
		if (!$date) {
			throw new CHttpException(201, 'Expiration date not specified');
		}
		$timestamp = strtotime($date);
		if (!$timestamp){
			throw new CHttpException(201, 'Invalid expiration date provided');
		}

		//calculate remaining trial time
		$now = time();
		$trialExpirationDate = Settings::get('trial_expiration_date');
		$trialExpirationDatetime = date('Y-m-d', strtotime($trialExpirationDate))." 23:59:59";
		$trialRemainingTime = strtotime($trialExpirationDatetime) - $now;
		$trialRemainingDays = -1;  // meaning, it passed (expired)
		if ($trialRemainingTime > 0) {
			$trialRemainingDays = (int) floor($trialRemainingTime / 86400);
		}
		$inTrial = ($maxUsers == 0 && $trialRemainingDays >= 0);

		//do DB stuff
		$output = array();
		if ($timestamp) {

			$setting = CoreSetting::model()->findByPk('trial_expiration_date');
			if (!$setting) {
				throw new CHttpException(500, 'Unable to update trial expiration date');
			}
			$setting->param_value = date('Y-m-d', $timestamp);
			if (!$setting->save()) {
				throw new CHttpException(500, 'Unable to update trial expiration date');
			}
			$output['message'] = "Expiration date changed successfully";
		}

		return $output;
	}




	private static function _addToDate($date, $add) {
		if (!strtotime($date)) return FALSE;
		$date = strtotime($date);
		return strtotime($add, $date);
	}




	// Api method to see active users for a given period
	// POST param 'period' acccepts the following values:
	// --- 'this' for user activity in the current monthly cycle
	// --- 'previous' for activity in the last monthly cycle
	// --- 'YYYY-MM' for activity in the period that STARTED in the specified month
	// Note: Months are not solar. They do not necessary start on the 1st and end on the 31st.
	// They are based on the starting subscription date. For example, if subscription started April 14th,
	// the first period will be April 14 to May 14, the second is from May 15 to June 14 and so on.
	public function activeUsersByPeriod() {

		//read input parameter
		$period = $this->readParameter('period');

		//prepare output variable
		$output = array();

		//validate input and do some other checkings
		if (!$period) {
			throw new TrialApiException('The period parameter is empty. Please, pass "this", "previous" or "YYYY-MM" to see active user count for the specified period.', 500);
			return false;
		}

		$subscriptionBeginDate = Settings::get('subscription_begin_date');
		if (!$subscriptionBeginDate || !strtotime($subscriptionBeginDate)) {
			throw new CHttpException(500, 'The platform has no valid subscription date. Can not calculate the period starting/ending dates.');
		}

		if (strtotime($subscriptionBeginDate) > strtotime('now')) {
			throw new CHttpException(500, 'The platform subscription date must be in the past.');
		}

		//retrieve all monthly periods' beginning/ending date starting from the subscription date up to the currently active period
		$periods = Billing::getSubscriptionPeriodDates();
		if (empty($periods) || !is_array($periods)) {
			$output['users_count'] = 0;
			return $output;
		}

		switch ($period) {

			case 'this': {
				// Get only the latest (currently active) period
				$currentPeriod = end($periods);
				$count = UserLimit::countActiveUsers($currentPeriod['from'], $currentPeriod['to']);
				if ($count === false) { throw new CHttpException(500, 'Error while retrieving active users'); }
				$output['users_count'] = ($count > 0 ? (int)$count : 0);
			} break;

			case 'previous': {
				// Get only the previous period starting/ending dates
				$previousPeriod = array_slice($periods, -2, 1);
				if (empty($previousPeriod)) {
					throw new CHttpException(500, 'The previous period does not exists');
				}
				$previousPeriod = $previousPeriod[0]; // array_slice still returns it as one-element array
				if (empty($previousPeriod)) {
					throw new CHttpException(500, 'The previous period does not exists');
				}
				$count = UserLimit::countActiveUsers($previousPeriod['from'], $previousPeriod['to']);
				if ($count === false) { throw new CHttpException(500, 'Error while retrieving active users'); }
				$output['users_count'] = ($count > 0 ? (int)$count : 0);
			} break;

			default: {

				//check input validity
				if (!preg_match('/^(\d{4})-(\d{2})$/', $period)) {
					throw new CHttpException(500, 'The passed period is not valid. Please, pass "this", "previous", or a date in YYYY-MM format to see the active users for the specified period');
				}

				// Month and year are passed
				$timePeriod = strtotime($period);
				if ($timePeriod) {
					$provided_date = date('Y-m-01 00:00:00', $timePeriod); //force the first day and time of the month, because only YYYY-MM is accepted
					$provided_time = strtotime($provided_date);

					$first_period = array_slice($periods, 0, 1);
					$first_period = $first_period[0];

					// Check if the period provided is before the platform activation subscription/date
					$first_period_time = strtotime($first_period['from']);
					if ($first_period_time > $provided_time) {
						//we still need to check if we are in the activation month (which is valid) or not
						$chk1 = date('Y-m', $first_period_time);
						$chk2 = date('Y-m', $provided_time);
						if ($chk1 != $chk2) {
							//we are requesting a month prior to activation month, so the request is invalid
							throw new CHttpException('A period this far in the past does not exists. You requested: ' . date('Y-m', $provided_time), 500);
						}
					}

					// The provided date is valid (after the subscription start date)
					$matched_period = false;
					if (!empty($periods)) {
						foreach ($periods as $per) {
							if (strtotime($per['from']) > $provided_time) {
								$matched_period = $per;
								break;
							}
						}
					}

					if ($matched_period && is_array($matched_period)) {
						$count = UserLimit::countActiveUsers($matched_period['from'], $matched_period['to']);
						if ($count === false) { throw new CHttpException(500, 'Error while retrieving active users'); }
						$output['users_count'] = ($count > 0 ? (int)$count : 0);
					} else {
						throw new CHttpException(500, 'An unknown error occurred. Please, check the passed parameters.');
					}

				} else {
					throw new CHttpException(500, 'The passed period is not valid. Please, pass "this", "previous", or a date in YYYY-MM format to see the active users for the specified period');
				}
			} break;
		}

		return $output;
	}

}
