<?php

/**
 * Created by PhpStorm.
 * User: Asen Nikolov
 * Date: 06-Jan-16
 * Time: 5:28 PM
 * Location API Component
 * @description Set of APIs to manage locations of the LMS
 */
class LocationApiModule extends ApiModule {

	const MAX_SIZE = 100;

	/**
	 * @summary Returns number of locations matching the specified criteria.
	 * @notes Use this method to implement pagination for location/listLocations.
	 * @parameter name [string, optional] location name
	 * @parameter country [string, optional] country name
	 *
	 * @response count [integer, required] The number of records matching the specified (if any) criteria
	 *
	 * @status 701 Invalid country name provided
	 */
	public function count(){
		$name = $this->readParameter('name');
		$countryName = $this->readParameter('country');
		$command = Yii::app()->db->createCommand()
			->select('COUNT(id_location)')
			->from(LtLocation::model()->tableName().' l');

		if (!empty($name)) {
			$command->where(array('LIKE', 'name', '%' . $name . '%'));
		}
		if (!empty($countryName)) {
			$countryName = strtolower($countryName);
			$countryIdFromDb = Yii::app()->db->createCommand()->select('id_country')
				->from(CoreCountry::model()->tableName())
				->where('LOWER(name_country) = :name', array(':name' => $countryName))->queryScalar();
			if(!$countryIdFromDb)
				throw new CHttpException(701, 'Invalid country name provided');

			$command->andWhere('l.id_country=:countryId', array(':countryId' => $countryIdFromDb));
		}

		$count = $command->queryScalar();
		return array('count' => (int)$count);
	}

	/**
	 * @summary  Returns an array of ILT locations matching the specified criteria.
	 * @notes Use the location/count endpoint to calculate the number of pages and the page size.
	 *
	 * @parameter name [string, optional] location name
	 * @parameter country [string, optional] country name
	 * @parameter from [integer, optional] the offset to use for the pagination (starts from 0)
	 * @parameter count [integer, optional] the number of records to return per each call
	 *
	 * @response locations [array, required]
	 *      @item id_location [integer, required] the internal Docebo ID for the location
	 *	    @item name [string, required] the name of the location
	 *	    @item address [string, optional] street address for the location
	 *	    @item telephone [string, optional] the telephone number for the location
	 *      @item email [string, optional] email contact for the location
	 *	    @item reaching_info [string, optional] Directions to reach/find the location
	 *	    @item accomodations [string, optional] Accommodation information for people attending the course in the location
	 *	    @item other_info [string, optional] Additional details about the location
	 *	    @item country [string, required] unique name of the country for the location (e.g. Bulgaria, Italy)
	 *
	 * @status 701 Invalid country name provided
	 * @status 702 Invalid from param provided
	 * @status 703 Invalid count param provided
	 * @status 704 "count" param exceeds the maximum page size (=N)
	 */
	public function listLocations(){
		$name = $this->readParameter('name');
		$countryName = $this->readParameter('country');
		$from = $this->readParameter('from', false);
		$count = $this->readParameter('count', false);

		if ($from === false || $from != intval($from) || $from < 0)
			throw new CHttpException(702, 'Invalid from param provided');

		if ($count === false || $count != intval($count) || $count <= 0)
			throw new CHttpException(703, 'Invalid count param provided');

		if ($count > self::MAX_SIZE)
			throw new CHttpException(704, '"count" param exceeds the maximum page size (' . self::MAX_SIZE . ')');

		$command = Yii::app()->db->createCommand()
				->select('loc.*, cc.name_country as country')
				->from(LtLocation::model()->tableName() . ' loc')
				->join(CoreCountry::model()->tableName() . ' cc', 'loc.id_country = cc.id_country');
		if(!empty($countryName)){
			$countryId = Yii::app()->db->createCommand()
					->select('id_country')
					->from(CoreCountry::model()->tableName())
					->where('LOWER(name_country) = :name', array(':name' => $countryName))->queryScalar();
			if(!$countryId)
				throw new CHttpException(701, 'Invalid country name provided');
			$command->andWhere('loc.id_country = :id', array(':id' => $countryId));
		}

		if(!empty($name)){
			$command->andWhere(array('LIKE', 'loc.name', '%' . $name . '%'));
		}

		$command->limit($count, $from);
		$locations = $command->queryAll();
		foreach ($locations as &$location) {
			unset($location['id_country']);

			//set proper types
			$location['id_location'] = (int)$location['id_location'];
		}

		return array('locations' => $locations);
	}

	/**
	 * @summary  Returns the total number of ILT sessions whose location matches the passed location ID.
	 * @notes Use this method to implement pagination for location/findSessionsByLocation.
	 *
	 * @parameter id_location [integer, required] internal Docebo ID of an existing location
	 *
	 * @response count [integer, required] The number of records matching the specified criteria
	 *
	 * @status 701 Invalid id_location param provided
	 * @status 702 Missing required param id_location
	 */
	public function countSessionsByLocation(){
		$locationId = $this->readParameter('id_location');

		if($locationId === null)
			throw new CHttpException(702, 'Missing required param id_location');

		$locationModel = LtLocation::model()->findByPk($locationId);
		if (!$locationModel)
			throw new CHttpException(701, 'Invalid id_location param provided');

		$count = Yii::app()->db->createCommand()
				->select('COUNT(id_session)')
				->from(LtCourseSessionDate::model()->tableName())
				->where('id_location = :id', array(':id' => $locationId))->queryScalar();

		return array('count' => (int)$count);
	}

	/**
	 * @summary  Returns an array of ILT sessions whose location matches the passed location ID (with related course info).
	 * @notes Use the location/countSessionsByLocation endpoint to calculate the number of pages and the page size.
	 *
	 * @parameter id_location [integer, required] internal Docebo ID of an existing location
	 * @parameter from [integer, optional] (default=false) the offset to use for the pagination (starts from 0)
	 * @parameter count [integer, optional] (default=false) the number of records to return per each call.
	 *
	 * @response sessions [array, required]
	 * 		@item id_session [integer, required] the internal Docebo ID for the ILT session
	 * 		@item session_name [string, required] the name of the ILT session
	 * 		@item session_date_begin [datetime, required] ILT session start date (in yyyy-MM-dd HH:mm:ss format, UTC timezone)
	 * 		@item session_date_end [datetime, required] ILT session end date (in yyyy-MM-dd HH:mm:ss format, UTC timezone)
	 * 		@item id_course [integer, required] the internal Docebo ID for the course the ILT session belongs to
	 * 		@item course_code [string, optional] the alphanumeric code for the course the ILT session belongs to
	 * 		@item course_name [string, required] the title of the course the ILT session belongs to
	 * 		@item course_description [string, required] the description of the course the ILT session belongs to
	 *
	 * @status 701 Invalid id_location param provided
	 * @status 702 Missing required param id_location
	 * @status 703 "count" param exceeds the maximum page size (=N)
	 * @status 704 Invalid from param provided
	 * @status 705 Invalid count param provided
	 */
	public function findSessionsByLocation(){
		$locationId = $this->readParameter('id_location');
		$from = $this->readParameter('from');
		$count = $this->readParameter('count');

		if($locationId === null)
			throw new CHttpException(702, 'Missing required param id_location');

		$locationModel = LtLocation::model()->findByPk($locationId);
		if (!$locationModel)
			throw new CHttpException(701, 'Invalid id_location param provided');

		if ($from === null|| $from != intval($from) || $from < 0)
			throw new CHttpException(704, 'Invalid from param provided');

		if ($count === null || $count != intval($count) || $count <= 0)
			throw new CHttpException(705, 'Invalid count param provided');

		if($count> self::MAX_SIZE)
			throw new CHttpException(703, '"count" param exceeds the maximum page size (' . self::MAX_SIZE . ')');

		$sessions = Yii::app()->db->createCommand()
				->select('lts.id_session, lts.name as session_name, lts.date_begin as session_date_begin, lts.date_end as session_date_end,
					lc.idCourse as id_course, lc.name as course_name, lc.code as course_code, lc.description as course_description')
				->from(LtCourseSession::model()->tableName() . ' lts')
				->join(LearningCourse::model()->tableName() . ' lc', 'lts.course_id = lc.idCourse')
				->join(LtCourseSessionDate::model()->tableName() . ' ltsd', 'lts.id_session = ltsd.id_session')
				->where('ltsd.id_location = :id', array(':id'=>$locationId))
				->group('lc.idCourse')->queryAll();

		//converting the returned data to proper type
		foreach($sessions as $key => $session){
			$sessions[$key]['id_session'] = (int)$session['id_session'];
			$sessions[$key]['id_course'] = (int)$session['id_course'];
		}

		return array('sessions' => $sessions);
	}

	/**
	 * @summary  Creates a new ILT location with the given details.
	 * @notes You can refer to the Country list in ILT locations management for the complete list of country names.
	 *
	 * @parameter name [string, required] the name of the location
	 * @parameter address [string, optional] street address for the location
	 * @parameter country [string, required] unique name of the country for the location (e.g. Bulgaria, Italy)
	 * @parameter telephone [string, optional] the telephone number for the location
	 * @parameter email [string, optional] email contact for the location
	 * @parameter reaching_info [string, optional] Directions to reach/find the location
	 * @parameter accomodations [string, optional] Accommodation information for people attending the course in the location
	 * @parameter other_info [string, optional] Additional details about the location
	 *
	 * @response id_location [integer, required] The internal ID for the newly created location
	 *
	 * @status 701 Missing or empty location name
	 * @status 702 Missing or empty country name
	 * @status 703 Invalid country name provided
	 */

	public function create(){
		$name = $this->readParameter('name');
		$country = strtolower($this->readParameter('country'));
		$address = $this->readParameter('address');
		$telephone = $this->readParameter('telephone');
		$email = $this->readParameter('email');
		$reaching_info = $this->readParameter('reaching_info');
		$accomodations = $this->readParameter('accomodations');
		$other_info = $this->readParameter('other_info');

		if(empty($name))
			throw new CHttpException(701, 'Missing or empty location name');

		$idCountry = $this->getCountryId($country);

		$location = new LtLocation();
		$location->name = $name;
		$location->address = $address;
		$location->accomodations = $accomodations;
		$location->email = $email;
		$location->telephone = $telephone;
		$location->reaching_info = $reaching_info;
		$location->other_info = $other_info;
		$location->id_country = $idCountry;
		$location->save();

		return array('id_location' => (int)$location->id_location);
	}

	/**
	 * @summary Updates the provided ILT location with the given details.
	 * @notes You can refer to the Country list in ILT locations management for the complete list of country names.
	 *
	 * @parameter id_location [integer, required]: the internal Docebo ID for the location
	 * @parameter name [string, required] the name of the location
	 * @parameter address [string, optional] street address for the location
	 * @parameter country [string, required] unique name of the country for the location (e.g. Bulgaria, Italy)
	 * @parameter telephone [string, optional] the telephone number for the location
	 * @parameter email [string, optional] email contact for the location
	 * @parameter reaching_info [string, optional] Directions to reach/find the location
	 * @parameter accomodations [string, optional] Accommodation information for people attending the course in the location
	 * @parameter other_info [string, optional] Additional details about the location
	 *
	 * @status 701 Missing or empty location name
	 * @status 702 Missing or empty country name
	 * @status 703 Invalid country name provided
	 * @status 704 Missing or empty id_location param
	 * @status 705 Invalid id_location param provided
	 */
	public function update(){
		$idLocation = $this->readParameter('id_location');
		$name = $this->readParameter('name');
		$country = strtolower($this->readParameter('country'));
		$address = $this->readParameter('address');
		$telephone = $this->readParameter('telephone');
		$email = $this->readParameter('email');
		$reaching_info = $this->readParameter('reaching_info');
		$accomodations = $this->readParameter('accomodations');
		$other_info = $this->readParameter('other_info');

		if(empty($idLocation))
			throw new CHttpException(704, 'Missing or empty id_location param');

		$locationModel = LtLocation::model()->findByPk($idLocation);
		if(!$locationModel)
			throw new CHttpException(705, 'Invalid id_location param provided');

		if(empty($name))
			throw new CHttpException(701, 'Missing or empty location name');

		$idCountry = $this->getCountryId($country);
		$locationModel->id_country = $idCountry;
		$locationModel->name = $name;
		$locationModel->address = $address;
		$locationModel->telephone = $telephone;
		$locationModel->email = $email;
		$locationModel->reaching_info = $reaching_info;
		$locationModel->accomodations = $accomodations;
		$locationModel->other_info = $other_info;
		$locationModel->save();

		return array();
	}

	/**
	 * @summary Deletes an existing ILT location.
	 *
	 * @parameter id_location [integer, required] the internal Docebo ID for the location
	 *
	 * @status 704 Missing or empty id_location param
	 * @status 705 Invalid id_location param provided
	 */
	public function delete(){
		$idLocation = $this->readParameter('id_location');
		if(empty($idLocation))
			throw new CHttpException(704, 'Missing or empty id_location param');

		$locationModel = LtLocation::model()->findByPk($idLocation);
		if(!$locationModel)
			throw new CHttpException(705, 'Invalid id_location param provided');

		$locationModel->delete();
		return array();
	}

	private function getCountryId($country){
		if(empty($country))
			throw new CHttpException(702, 'Missing or empty country name');

		$listOfCountries = Yii::app()->db->createCommand()
				->select('LOWER(name_country) as name')
				->from(CoreCountry::model()->tableName())->queryColumn();

		if(!in_array($country, $listOfCountries))
			throw new CHttpException(703, 'Invalid country name provided');

		// all checks are passed, ready for create new location
		$idCountry = Yii::app()->db->createCommand()
				->select('id_country')
				->from(CoreCountry::model()->tableName())
				->where('name_country=:name', array(':name' => strtoupper($country)))->queryScalar();

		return $idCountry;
	}
}