<?php

/**
 * EcommerceApiModule API Component
 * @description Set of APIs to manage e-commerce transactions
 */
class EcommerceApiModule extends ApiModule {

	/**
	 * Returns the requested security level for the passed action
	 * @param $actionName The name of the action
	 * @return string The requested security level
	 */
	public function getSecurityLevel($actionName)
	{
		switch ($actionName) {
			default:
				return self::$SECURITY_STRONG;
		}
	}

	/**
	 * @summary List e-commerce transactions
	 *
	 * @parameter username [string, optional] Filter transactions based on one username
	 * @parameter created_from [datetime, optional] the start date & time used to filter transactions based on the creation date (in yyyy-MM-dd HH:mm:ss format, UTC timezone).
	 * @parameter created_to [datetime, optional] the end date & time used to filter transactions based on the creation date (in yyyy-MM-dd HH:mm:ss format, UTC timezone).
	 * @parameter activated_from [datetime, optional] the start date & time used to filter transactions based on the activation/confirmation date (in yyyy-MM-dd HH:mm:ss format, UTC timezone).
	 * @parameter activated_to [datetime, optional] the end date & time used to filter transactions based on the activation/confirmation date (in yyyy-MM-dd HH:mm:ss format, UTC timezone).
	 * @parameter id_user [integer, optional] if provided, returns only transactions created by the passed used (internal Docebo ID).
	 * @parameter status [string, optional] filters transactions using their current status (possible values: "accepted" or "declined").
	 * @parameter from [integer, required] limit the results set by record offset.
	 * @parameter count [integer, required] limit the results set by number of records.
	 * @parameter show_items [boolean, optional] if true is passed, the report will include also the transaction items details in the "items" field (default = false).
	 *
	 * @response transactions [array, required] The full list of e-commerce transactions
	 * 		@item id [integer, required] The internal ID of the transaction
	 * 		@item username [string, required] Login name of the buyer
	 * 		@item firstname [string, required] First name of the buyer
	 * 		@item lastname [string, required] Last name of the buyer
	 * 		@item email [string, required] Email address of the buyer
	 * 		@item total [string, required] The total amount of the transaction
	 * 		@item payment_method [string, required] The payment gateway used
	 * 		@item transaction_id [string, required] The transaction id
	 * 		@item date_created [datetime, required] The date when the transaction was activated
	 * 		@item date_activated [datetime, required] The date when the transaction was confirmed/activated
	 *		@item status [string, required] The status of the transaction (possible values: "accepted" or "declined")
	 * 		@item company_name [string, required] The company name
	 * 		@item vat [string, required] VAT identification number
	 * 		@item address_1 [string, required] First address of the buyer
	 * 		@item address_2 [string, required] Second address of the buyer
	 * 		@item city [string, required] City of the buyer
	 * 		@item state [string, required] State of the buyer
	 * 		@item zip [string, required] Zip of the buyer
	 *		@item items [array, optional] array of transaction items for the current transaction. Each transaction item contains:
	 * 			@item name [string, required] The name of the purchased item
	 * 			@item item_id [integer, required] The internal ID of the purchased item (course ID or learning plan ID)
	 * 			@item price [string, required] The calculated price for the purchased item
	 * 			@item status [string, required] The status of the transaction item (possible values: "activated" or "pending")
	 * 			@item type [string, required] The type of item (possible values: "course", "coursepath", "courseseats")
	 * 			@item item_code [string, required] The alphanumeric code of the purchased item (course ID or learning plan ID)
	 * 		@end items
	 *		@item id_gateway_transaction [string, optional] The gateway specific transaction ID (as returned by the specific payment gateway)
	 *		@item id_user [integer, required] Unique Docebo ID for the user that bought the item
	 *		@item unit [string, required] currency set for the e-commerce app
	 *		@item coupon_code [string, optional] Coupon applied to purchase this item (if applicable)
	 *		@item discount [string, optional] The discount value (fixed price or percentage) for the applied coupon (if applicable)
	 *
	 * @status 701 Invalid value for parameter "status"
	 * @status 702 No "from" parameter provided
	 * @status 703 No "count" parameter provided
	 * @status 704 Invalid "from" parameter
	 * @status 705 Invalid "count" parameter
	 */
	public function listTransactions() {
		$model = new EcommerceTransaction('search');
		$input = array();

		$status = $this->readParameter('status');
		if ($status == 'accepted') {
			$input['paid'] = 1;
		} elseif ($status == 'declined') {
			$input['paid'] = 0;
		} elseif(!empty($status)){
			throw new CHttpException(701, 'Invalid value for parameter "status"');
		}

		// getting input parameters
		$input['from'] = $this->readParameter('from', null);
		$input['count'] = $this->readParameter('count', null);
		$input['date_creation']['from'] = $this->readParameter('created_from');
		$input['date_creation']['to'] = $this->readParameter('created_to');
		$input['date_activated']['from'] = $this->readParameter('activated_from');
		$input['date_activated']['to'] = $this->readParameter('activated_to');
		$input[EcommerceTransaction::USERNAME_ALIAS] = $this->readParameter('username');
		$input['u.idst'] = $this->readParameter('id_user');

		if(is_null($input['from'])){
			throw new CHttpException( 702, 'No "from" parameter provided' );
		}

		if(is_null($input['count'])){
			throw new CHttpException( 703, 'No "count" parameter provided' );
		}

		//check if from/count are digits (the strings contain digits)
		if(is_string($input['from']) && !ctype_digit($input['from'])){
			throw new CHttpException( 704, 'Invalid "from" parameter' );
		}
		if( (is_string($input['count']) &&  !ctype_digit($input['count'])) || $input['count'] <= 0){
			throw new CHttpException( 705, 'Invalid "count" parameter' );
		}

		if($input['count'] > ApiModule::DEFAULT_COUNT_LIMIT ){
			$input['count'] = ApiModule::DEFAULT_COUNT_LIMIT;
		}

		$show_items = $this->readParameter('show_items', false);
		if ($show_items == 'true' || $show_items === true || $show_items == 1) $show_items = true;
		else $show_items = false;

		$params = array();
		foreach ($input as $name => $item) {
			if (is_array($item)) {
				if(empty($item['from'])){
					// set initial date, according to db default value date
					$item['from'] = '0000-00-00 00:00:00';
				}
				if(empty($item['to'])){
					// get current date
					$item['to'] = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
				}
				$params[$name] = $item;
			} elseif (!empty($item) || ((int)$item === 0 && ($name == 'from' || $name == 'count' || $name == 'paid'))) {
				$params[$name] = $item;
			}
		}

		$dataArr = $model->transactionsManageDataProvider(false, $params);
		foreach ($dataArr as $key => $data) {
            $coupon_code = '';
            $discount = '';
            if ($data['id_coupon']) {
                $couponModel = EcommerceCoupon::model()->findByPk($data['id_coupon']);
                $coupon_code = $couponModel->code;
                $discount = ($couponModel->discount_type == 'percent') ?
                    number_format($couponModel->discount, 2) . ' %' :
                    number_format($couponModel->discount, 2) . ' ' . $data['payment_currency'];
            }

			$transactions[$key] = array(
                'id' => (int)$data['id_trans'],
                'username' => (substr($data['userid'], 0, 1) == '/' ? substr($data['userid'], 1) : $data['userid']),
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'email' => $data['email'],
                'total' => floatval(EcommerceTransaction::renderTotalPaidFromData($data)),
                'payment_method' => strtoupper($data['payment_type']),
                'transaction_id' => isset($data['payment_txn_id']) ? $data['payment_txn_id'] :  '' ,
                'date_created' => $data['date_creation'],
                'date_activated' => $data['date_activated'],
                'status' => $data['paid'] ? 'accepted' : 'declined',
                'company_name' =>  isset($data['bill_company_name']) ? $data['bill_company_name'] :  '' ,
                'vat' => isset($data['bill_vat_number']) ? $data['bill_vat_number'] :  '' ,
                'address_1' =>  isset($data['bill_address1']) ? $data['bill_address1'] :  '' ,
                'address_2' =>  isset($data['bill_address2']) ? $data['bill_address2'] :  '' ,
                'city' => isset($data['bill_city']) ? $data['bill_city'] :  '' ,
                'state' => isset($data['bill_state']) ? $data['bill_state'] :  '' ,
                'zip' =>  isset($data['bill_zip']) ? $data['bill_zip'] :  '' ,
                'id_gateway_transaction' => isset($data['payment_txn_id']) ? $data['payment_txn_id'] :  '',
                'id_user' => isset($data['id_user']) ? (int)$data['id_user'] :  '' ,
                'unit' => isset($data['payment_currency']) ? $data['payment_currency'] :  '',
                'coupon_code' => $coupon_code,
                'discount' => $discount
			);

			if($show_items === true) {
				$items = Yii::app()->db->createCommand()
						->select('item_id,id_course,id_path,code as item_code,name,price,activated,item_type as type')->from(EcommerceTransactionInfo::model()->tableName())
						->where('id_trans=:id', array(':id' => (int)$data['id_trans']))->queryAll();
				foreach($items as $item){
					$itemId = '';
					switch($item['type']){
						case EcommerceTransactionInfo::TYPE_COURSE:
						case EcommerceTransactionInfo::TYPE_COURSESEATS:
							$itemId = $item['id_course'];
							break;
						case EcommerceTransactionInfo::TYPE_COURSEPATH:
							$itemId = $item['id_path'];
							break;
					}
					$item['item_id'] = (int)$itemId;
					$item['status'] = $item['activated']==1 ? 'activated' : 'pending';
					unset($item['activated']);
					unset($item['id_course']);
					unset($item['id_path']);
					$item['price'] = floatval($item['price']);
					$transactions[$key]['items'][] = $item;
				}
			}
		}

		return array('transactions' => $transactions);
	}

	/**
	 * @summary Returns the items list of a specified transaction
	 *
	 * @parameter id_transaction [integer, required] The id of the transaction to retrieve
	 *
	 * @response items [array, required] An array containing the transaction items
	 *		@item name [string, required] The name of the purchased item
	 *		@item item_id [integer, required] The ID of the purchased item (course ID or learning plan ID)
	 * 		@item price [string, required] The calculated price for the purchased item
	 * 		@item status [string, required] The status of the transaction item (possible values: "activated" or "pending")
	 * 		@item type [string, required] The type of item (possible values: "course", "coursepath", "courseseats")
	 * 		@item username [string, required] The user id of the associated with the transaction user
	 * 		@item firstname [string, required] First name of the associated with the transaction user
	 * 		@item lastname [string, required] Last name of the associated with the transaction user
	 * 		@item email [string, required] Email of the associated with the transaction user
	 * 		@item payment_method [string, required] Transaction payment method
	 * 		@item transaction_id [string, required] Transaction id
	 * 		@item company_name [string, required] Billing company name
	 * 		@item vat [string, required] Billing company VAT number
	 * 		@item address_1 [string, required] Billing company address 1
	 * 		@item address_2 [string, required] Billing company address 2
	 * 		@item city [string, required] Billing company city
	 * 		@item state [string, required] Billing company address
	 * 		@item zip [string, required] Billing company zip code
	 * 		@item item_code [string, required] The alphanumeric code of the purchased item (course ID or learning plan ID)
	 * 		@item item_description [string, required] Course or learning plan description
	 *
	 * @status 401 Transaction not found
	 */
	public function transactionInfo() {
		$idTransaction = $this->readParameter('id_transaction');
		$transaction = EcommerceTransaction::model()->findByPk($idTransaction);
		if (!$transaction)
			throw new CHttpException(401, 'Transaction not found');

		$items = array();
		$itemsModel = new EcommerceTransactionInfo();
		$itemsModel->id_trans = $idTransaction;

		/** @var CActiveDataProvider $transactionCourses */
		$transactionCourses = $itemsModel->searchByTransactionId();
		if ($transactionCourses->itemCount > 0) {
			foreach ($transactionCourses->getData() as $transactionInfo) {
				$items[] = array(
					'name' => $transactionInfo->name,
					'item_id' => $transactionInfo['item_type'] == 'coursepath' ?  (int)$transactionInfo->id_path : (int)$transactionInfo->id_course,
					'price' => EcommerceTransaction::renderTotalPaidFromData(array(
						'totalPrice' => $transactionInfo['price'],
						'discount' => 0,
						'payment_currency' => $transaction['payment_currency']
					)),
					'status' => $transactionInfo['activated'] ? 'activated' : 'pending',
					'type' => $transactionInfo['item_type'],
					'username' => isset($transactionInfo->transaction->user->userid) ? (substr($transactionInfo->transaction->user->userid, 0, 1) == '/' ? substr($transactionInfo->transaction->user->userid, 1) : $transactionInfo->transaction->user->userid) : '',
					'firstname' =>  isset($transactionInfo->transaction->user->firstname) ? $transactionInfo->transaction->user->firstname : '',
					'lastname' => isset($transactionInfo->transaction->user->lastname) ? $transactionInfo->transaction->user->lastname : '',
					'email' => isset($transactionInfo->transaction->user->email) ? $transactionInfo->transaction->user->email : '',
					'payment_method' => isset($transactionInfo->transaction->payment_type) ? strtoupper($transactionInfo->transaction->payment_type) : '',
					'transaction_id' => isset($transactionInfo->transaction->payment_txn_id) ? $transactionInfo->transaction->payment_txn_id : '',
					'company_name' => isset($transactionInfo->transaction->billingInfo->bill_company_name) ? $transactionInfo->transaction->billingInfo->bill_company_name : '',
					'vat' =>  isset($transactionInfo->transaction->billingInfo->bill_vat_number) ? $transactionInfo->transaction->billingInfo->bill_vat_number : '',
					'address_1' => isset($transactionInfo->transaction->billingInfo->bill_address1) ? $transactionInfo->transaction->billingInfo->bill_address1 : '',
					'address_2' => isset($transactionInfo->transaction->billingInfo->bill_address2) ? $transactionInfo->transaction->billingInfo->bill_address2 : '',
					'city' => isset($transactionInfo->transaction->billingInfo->bill_city) ? $transactionInfo->transaction->billingInfo->bill_city : '',
					'state' =>  isset($transactionInfo->transaction->billingInfo->bill_state) ? $transactionInfo->transaction->billingInfo->bill_state : '',
					'zip' => isset($transactionInfo->transaction->billingInfo->bill_zip) ? $transactionInfo->transaction->billingInfo->bill_zip : '',
					'item_code' => $transactionInfo['code'],
					'item_description' => $this->_getDescription($transactionInfo)

				);
			}
		}

		return array('items' => $items);
	}

	/**
	 * @summary Returns the number of counted transactions by specified criteria
	 *
	 * @parameter created_from [datetime, optional] the start date & time used to filter transactions based on the creation date (in yyyy-MM-dd HH:mm:ss format, UTC timezone).
	 * @parameter created_to [datetime, optional] the end date & time used to filter transactions based on the creation date (in yyyy-MM-dd HH:mm:ss format, UTC timezone).
	 * @parameter activated_from [datetime, optional] the start date & time used to filter transactions based on the activation/confirmation date (in yyyy-MM-dd HH:mm:ss format, UTC timezone).
	 * @parameter activated_to [datetime, optional] the end date & time used to filter transactions based on the activation/confirmation date (in yyyy-MM-dd HH:mm:ss format, UTC timezone).
	 * @parameter id_user [integer, optional] if provided, returns only transactions created by the passed used (internal Docebo ID)
	 * @parameter status [string, optional]  filters transactions using their current status (possible values: "accepted" or "declined")
	 *
	 * @response count [integer, required] the number of counted transactions (to be used for paginating with ecommerce/listTransactions).
	 *
	 * @status 701 Invalid parameter "status"!
	 */
	public function countTransactions(){
		$createdFrom = $this->readParameter('created_from');
		$createdTo = $this->readParameter('created_to');
		$activatedFrom = $this->readParameter('activated_from');
		$activatedTo = $this->readParameter('activated_to');
		$idUser = $this->readParameter('id_user');
		$status = $this->readParameter('status');
		if(!empty($status)) {
			if ($status == 'accepted') {
				$status = 1;
			} elseif ($status == 'declined') {
				$status = 0;
			} else {
				throw new CHttpException(701, 'Invalid parameter "status"!');
			}
		}else $status = null;

		$command = Yii::app()->db->createCommand();
		$command->select('COUNT(id_trans)')->from(EcommerceTransaction::model()->tableName());
		if (!empty($createdFrom)) $command->andWhere('date_creation >= :start', array(':start' => $createdFrom));
		if (!empty($createdTo)) $command->andWhere('date_creation <= :end', array(':end' => $createdTo));
		if (!empty($activatedFrom)) $command->andWhere('date_activated >= :start', array(':start' => $activatedFrom));
		if (!empty($activatedTo)) $command->andWhere('date_activated <= :end', array(':end' => $activatedTo));
		if (!empty($idUser)) $command->andWhere('id_user = :user', array(':user' => $idUser));
		if (!empty($status) || $status === 0) $command->andWhere('paid = :status', array(':status' => $status));
		$count = $command->queryScalar();

		return array('count' => (int)$count);
	}

	private function _getDescription($transactionInfo){
		if ($transactionInfo['item_type'] == 'coursepath'){
			if ($transactionInfo->coursepath){
				return $transactionInfo->coursepath->path_descr;
			}else {
				$itemData = json_decode($transactionInfo->item_data_json, false);
				return $itemData->position_snapshot->path_descr;
			}
		} else {
			if ($transactionInfo->course){
				return $transactionInfo->course->description;
			}else {
				$itemData = json_decode($transactionInfo->item_data_json, false);
				return $itemData->position_snapshot->description;
			}
		}
	}
}