<?php
// This is the main Web application configuration. First include the $main configuration array
$main = require(dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../../common/config/main.php');

// Disable unused parts
$main['preload'] = array('log');
$main['import'][] = 'common.components.oauth.*';

return ConfigHelper::merge(
	$main,
	array(
		'name' => 'api',
		'basePath'=> realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..'),
			
        'catchAllRequest' => array('site/index'), // This is to redirect all "module" request to site controller
        'components' => array(
            'request' => array(
                'enableCsrfValidation' => false,
            ),
            'assetManager'=>array(
                'basePath'=> _lmsassets_,
                'baseUrl' => '../lms/assets/',
            ),
			'user' => array (
				'allowAutoLogin' => true
			)
        ),
        'modules' => array(
            'swagger' => array()
        ),
		'params' => array (
			'erpIpAddresses' => array (
				'54.154.191.62', 	// live
				'54.165.243.191'	// test
			)
		)
	)
);