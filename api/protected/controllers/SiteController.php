<?php

/**
 * Main Api controller.
 * Get all requests for this application and checks for permissions to call API.
 *
 * Class SiteController
 */
class SiteController extends CController {

	/**
	 * The name assigned to the swagger api
	 */
	public static $SWAGGER_API_PATH = 'docs';

	/**
	 * @var string Name of the API module that will process the request
	 */
	private $apiModuleName;

	/**
	 * @var string Name of the API action that will run this request
	 */
	private $apiActionName;

	/**
	 * @var ApiModule The loaded module for the requested API
	 */
	private $apiComponent;

	/**
	 * @var string The requested security level for the invoked action
	 */
	private $securityLevel;

	/**
	 * @var string The requested OAuth2 scopes for the invoked action
	 */
	private $scopes;

	/**
	 * @var array The map to convert from deprecated APIs to the new format
	 */
	private $deprecatedApis = array(
		'public/authenticate' => 'user/authenticate'
	);

	/**
	 * @var CoreUser The currently logged in user
	 */
	private $user;

	/**
	 * Sets the filters to invoke before each action
	 * @return array
	 */
	public function filters() {
		return array(
			'apiRequest', // It checks the passed param and (if valid) stores apiModule, apiAction in this controller
			'apiAuthorization', // It invokes x-authorization header check
		);
	}

	/**
	 * The filter method for 'apiRequest' filter.
	 * Preprocesses the HTTP request
	 *
	 * @param CFilterChain $filterChain the filter chain that the filter is on.
	 * @throws CHttpException
	 * @throws CHttpException
	 * @throws Exception
	 */
	public function filterApiRequest($filterChain) {
		if (Yii::app()->errorHandler->error) // This is the error action -> no security needed
			$this->securityLevel = ApiModule::$SECURITY_NONE;
		else {
			// === TODO: IS THIS REALLY NEEDED ????????????? ====
			if (!empty($_GET['d'])) {
				session_set_cookie_params(0);
				session_name('docebo_session');
				session_start();
				$domain_code = preg_replace('/(\W)/is', '_', $_GET['d']);
				$_SESSION['domain_code'] = $domain_code;
				$_SESSION['domain'] = $_GET['d'];
				session_write_close();
			}
			// ==================================================
			// Parse request param
			$request = Yii::app()->request; /* @var $request CHttpRequest */
			$r = $request->getParam('r');
			if (!$r)
				throw new CHttpException(101, "Missing params for the current request");

			// Check if a deprecated URL is invoked and convert it to the new REST Api
			if (isset($this->deprecatedApis[$r]))
				$r = $this->deprecatedApis[$r];

			// Check URL path components
			$apiCall = explode('/', $r);
			if (count($apiCall) < 1)
				throw new CHttpException(101, "Malformed params given");

			// Extract api module name and action
			$this->apiModuleName = preg_replace('/[^a-zA-Z0-9\-\_]/i', '', $apiCall[0]);

			// Handle special request for the swagger module
			if ($this->apiModuleName == self::$SWAGGER_API_PATH) {
				$this->securityLevel = ApiModule::$SECURITY_NONE;
				$module = Yii::app()->getModule('swagger');
				if ($module) { // Check if swagger is enabled and forward to the default swagger controller
					$this->forward('swagger/default/' . (isset($apiCall[1]) ? $apiCall[1] : 'index'));
					return;
				} else
					throw new CHttpException(404, "Api Documentation not available");
			} else {
				// Standard API call -> we need an action to invoke
				if (count($apiCall) < 2)
					throw new CHttpException(101, "Malformed params given");

				// Extract method name
				$this->apiActionName = preg_replace('/[^a-zA-Z0-9\-\_]/i', '', $apiCall[1]);

				// Load api module
				try {
					// Trigger hook to let plugins return their own component for this api request
					$this->apiComponent = null;
                    $event = new DEvent($this, array('moduleName' => $this->apiModuleName));
					Yii::app()->event->raise('LoadApiModule', $event);
                    if(isset($event->return_value['targetApiComponent']) && $event->return_value['targetApiComponent'])
                        $this->apiComponent = $event->return_value['targetApiComponent'];

					// If no plugin provided a component for the requested api,
					// load the default API module corresponding to the request action
					if (!$this->apiComponent) {
						$moduleName = ucfirst(strtolower($this->apiModuleName)) . "ApiModule";
						$this->apiComponent = Yii::createComponent(array('class' => $moduleName));
					}

					// Was the api module successfully loaded?
					if (!$this->apiComponent || !is_object($this->apiComponent) ||
						(!method_exists($this->apiComponent, $this->apiActionName) && !method_exists($this->apiComponent, $this->apiActionName.'Action')) ||
						!($this->apiComponent instanceof ApiModule)) {
						throw new CHttpException(404, 'The requested action was not found');
					}

					// Init the api module
					$this->apiComponent->init($this->user);

					// Get the requested security level & scope from the loaded module
					$this->securityLevel = $this->apiComponent->getSecurityLevel($this->apiActionName);
					$this->scopes = $this->apiComponent->getOauth2Scopes($this->apiActionName);
				} catch (CHttpException $e) {
					throw $e;
				} catch (Exception $ex) {
					throw new CHttpException(500, $ex->getMessage());
				}
			}

			$filterChain->run();
		}
	}

	/**
	 * The filter method for 'apiAuthorization' filter.
	 * This filter throws an exception (CHttpException with code 400) if the applied action is receiving a non valid HTTP api request.
	 *
	 * @param CFilterChain $filterChain the filter chain that the filter is on.
	 * @throws CHttpException if the current request is not a valid API request.
	 */
	public function filterApiAuthorization($filterChain) {
		try {

			if($this->securityLevel !== ApiModule::$SECURITY_NONE) {
				// Check oauth2 authorization
				if (!$this->_checkOauthAuthorization()) {

					// Fallback to legacy Docebo custom authentication
					if ($this->securityLevel == ApiModule::$SECURITY_STRONG) {
						// Patch headers if only HTTP_AUTHORIZATION is passed (HTTP_X_AUTHORIZATION are filtered by some proxy/firewall)
						if (!empty($_SERVER['HTTP_AUTHORIZATION']) && !isset($_SERVER['HTTP_X_AUTHORIZATION']))
							$_SERVER['HTTP_X_AUTHORIZATION'] = $_SERVER['HTTP_AUTHORIZATION'];
					}


					// Strong authorization also will log the first god admin with valid
                    // email address!
                    $firstGodAdmin = CoreUser::getGodAdmins(true);

                    // Two authorization headers supported
                    // 1. X-UserAuthorization (strong authorization)
                    // 2. X-Authorization (light authorization)
					if ($this->securityLevel == ApiModule::$SECURITY_STRONG){
                        $this->_checkStrongAuthorization();
                        $this->apiComponent->setUser($firstGodAdmin);
                    } else {
						if (isset($_SERVER['HTTP_X_AUTHORIZATION'])){
                            $this->_checkStrongAuthorization();
                            $this->apiComponent->setUser($firstGodAdmin);
                        } else {
							$this->_checkLightAuthorization();

							// the light athorization chan have changed the logged in user (in the api)
							// we need to reset it also in the component
							$this->apiComponent->setUser($this->user);
						}

					}
				}
			}
		} catch (Exception $exc) {
			Yii::log($exc->statusCode . ' - ' . $exc->getMessage() . ' header: ' . (isset($_SERVER['HTTP_X_USERAUTHORIZATION']) ? $_SERVER['HTTP_X_USERAUTHORIZATION'] : 'missing' ), CLogger::LEVEL_INFO, 'api');
			echo CJSON::encode(array(
				'success' => false,
				'message' => $exc->getMessage(),
				'code' => $exc->statusCode,
			));
			Yii::app()->end();
		}

		// If check succeded, then go on with the invocation of the api action
		$filterChain->run();
	}

	/**
	 * Checks whether the current request is authenticated through OAuth2 or legacy Docebo header
	 *
	 * @return true if successfully authenticated, false if no oauth2 authentication is present
	 *
	 * @throws CException
	 */
	private function _checkOauthAuthorization() {
		// Check if the current request has the Oauth2 header or access_token GET param
		$isOauth2Request = Yii::app()->request->getParam('access_token', false) || (stripos($_SERVER['HTTP_AUTHORIZATION'], 'Bearer ') === 0);
		if($isOauth2Request) {
			$oauthServer = DoceboOauth2Server::getInstance();
			$oauthServer->init();

			// Check if the provided token is enabled for at least one of the requested scopes
			$isAuthorized = false;
			foreach($this->scopes as $scope) {
				if ($oauthServer->verifyResourceRequest($scope)) {
					$isAuthorized = true;
					break;
				}
			}
			// Token is not valid for the required scopes
			if(!$isAuthorized) {
				$oauthServer->sendResponse(); // Send oauth2 style response
				Yii::app()->end();
			}

			// Set the current user
			$this->user = $oauthServer->getAuthorizedUser();
			$this->apiComponent->setUser($this->user);

			// If the current user was not yet checked whether or not he is
			// in China, do that check now
			if(PluginManager::isPluginActive("AWSChinaApp")) {
				$currentUserChinaReachableFlagChecked = CoreSettingUser::getSettingByUser(Yii::app()->user->getIdst(), AWSChinaAppModule::CHINA_REACHABLE_SETTING_NAME);
				if (!$currentUserChinaReachableFlagChecked) {
					AWSChinaAppModule::_checkIfCurrentUserCanAccessAmazonS3China();
				}
			}

			return true;
		}

		return false;
	}

	/**
	 * Checks for a valid strong authorization header
	 */
	private function _checkStrongAuthorization() {
		// Check if we are allowed to use the legacy header
		$erpServers   = isset(Yii::app()->params['erpIpAddresses']) ? Yii::app()->params['erpIpAddresses'] : array();
		$ips          = IPHelper::getRemoteIp(true);
		$intersect    = array();
		if (is_array($ips) && is_array($erpServers)) {
		    $intersect = array_intersect($ips, $erpServers);
		}
//		if(empty($intersect) && (Settings::get('legacy_api_authentication', 'on') == 'off')) {
//			throw new CHttpException(103, "Legacy API authentication disabled. You should use OAuth2 to access this API");
//		}

		// Check authorization header
		if (!$_SERVER['HTTP_X_AUTHORIZATION'])
			throw new CHttpException(103, 'Missing authorization header');

		// Extract hash from x-authorization header
		$requestHash = trim(base64_decode(str_replace('Docebo ', '', $_SERVER['HTTP_X_AUTHORIZATION'])));
		if (!$requestHash)
			throw new CHttpException(103, 'Malformed authorization header');

		// Load api keys
		$apiKey = Settings::getCfg('api_key');
		$apiSecret = Settings::getCfg('api_secret');
		if (empty($apiKey) || empty($apiSecret))
			throw new CHttpException(102, "Bad server API configuration");

		// Calculate the params hash locally
		$paramsToCheck = $this->_getParamsToCheck($_POST);
		$hashSha1 = sha1(stripslashes(implode(',', $paramsToCheck)) . ',' . $apiSecret);

		// Check if the two hashes match each other
		if ($requestHash != $apiKey . ':' . $hashSha1)
			throw new CHttpException(104, "Authorization header value doesn't match");
	}

	/**
	 * Checks for a valid light authorization header
	 */
	private function _checkLightAuthorization() {

		// key, in light authorization this will be the username
		$received_api_key 		= $this->apiComponent->readParameter('key', '');

		// token, in light authorization this will be the rest token
		$received_api_token 	= $this->apiComponent->readParameter('token', '');

		if (empty($received_api_key) || empty($received_api_token))
			throw new CHttpException(102, "Authorization parameters are missing");

		$this->user = CoreUser::model()->findByAttributes(array('userid' => Yii::app()->user->getAbsoluteUsername($received_api_key)));
		if (!$this->user)
			throw new CHttpException(103, "Invalid username provided");
		
		// Retrieve the token for this user
		$criteria = new CDbCriteria();
		$criteria->condition = 'id_user = :id_user';
		$criteria->params = array(':id_user' => (int) $this->user->idst);
		$restAuthModel = CoreRestAuthentication::model()->find($criteria);
		if (!$restAuthModel)
			throw new CHttpException(104, "No valid token for given user");

		// Check token expiration
		$utcExpiryDate = Yii::app()->localtime->fromLocalDateTime($restAuthModel->expiry_date);
		if (!strtotime($utcExpiryDate) || (strtotime($utcExpiryDate) < strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'))))
			throw new CHttpException(105, "User token expired");

		// Extend token validity
		$utcExpiryDate = date("Y-m-d H:i:s", strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s')) + 1800);
		$restAuthModel->expiry_date = Yii::app()->localtime->toLocalDateTime($utcExpiryDate);
		$restAuthModel->save();

		// Check if the received token is the same has the one saved in database
		if ($received_api_token != $restAuthModel->token) {
			throw new CHttpException(106, "The token used is not valid");
		}
	}

	/**
	 * Sends a response as a JSON object
	 * @param $response
	 */
	private function _sendResponse($response) {
		if(!$response['success'] && $response['error'] && $response['message']) {
			$status_header = 'HTTP/1.1 ' . $response['error'] . ' ' . $response['message'];
			header($status_header);
		}

		header('Content-Type: application/json; charset="UTF-8"');
		echo CJSON::encode($response);
	}

	/**
	 * Decodes urlencoded params in the $_POST variable
	 */
	private function _decodeUrlEncodedParams(&$params) {
		foreach ($params as $key => &$value) {
			if (is_array($value))
				$this->_decodeUrlEncodedParams($value);
			else
				$params[$key] = rawurldecode($value);
		}
	}

	/**
	 * Creates the theme object
	 */
	private function resolveTheme() {
		// The theme setup for this part is skypped, so we have to do it
		// Set the correct path for the themes folder that is shared
		Yii::app()->themeManager->setBasePath(dirname(Yii::app()->getRequest()->getScriptFile()) . DIRECTORY_SEPARATOR . '../themes');

		// Using DIRECTORY_SEPARATOR for Paths is Ok, but NOT for URLs. (Windows)
		Yii::app()->themeManager->setBaseUrl(Docebo::getRootBaseUrl() . '/themes');
	}

	/**
	 * This is the default 'index' action that is invoked for the api controller.
	 */
	public function actionIndex() {
		try {
			// Resolve language
			Docebo::resolveLanguage();

			// Resolve theme (used if api calls event to generate a new pdf course certificate)
			$this->resolveTheme();

			// Start buffering output
			ob_start();

			// Urldecode param values before they're processed by api module
			$this->_decodeUrlEncodedParams($_POST);

			// Trigger an event before the API call
			$event = new DEvent($this, array(
				'api_component' => $this->apiComponent,
				'action' => $this->apiActionName
			));
			Yii::app()->event->raise('OnBeforeApiCall', $event);

			// Do the actual call
			if (method_exists($this->apiComponent, $this->apiActionName)) {
				$action = $this->apiActionName;
			} else {
				$action = $this->apiActionName.'Action';
			}

			$response = $this->apiComponent->$action();

			// Trigger an event after the API call
			$event = new DEvent($this, array(
				'api_component' => $this->apiComponent,
				'action' => $this->apiActionName,
				'response' => $response
			));
			Yii::app()->event->raise('OnAfterApiCall', $event);
			if (!$event->shouldPerformAsDefault() && !empty($event->return_value['response']))
				$response = $event->return_value['response'];

			// Clear debug messages and clean buffer for output
			ob_clean();

			if (!$response)
				$response = array();
			$response['success'] = true;
			$this->_sendResponse($response);

			// Flush buffer
			ob_end_flush();
		} catch (CHttpException $e) {
			ob_end_clean();
			Yii::log($e->statusCode . ' - ' . $e->getMessage() . ' input: ' .  file_get_contents("php://input"), CLogger::LEVEL_INFO, 'api');
			$this->_sendResponse(array(
				'success' => false,
				'error' => $e->statusCode,
				'message' => $e->getMessage()
			));
		} catch (Exception $ex) {
			ob_end_clean();
			Yii::log($ex->statusCode . ' - ' . $ex->getMessage() . ' input: ' .  file_get_contents("php://input"), 'error', 'api');
			throw new CHttpException(500, $ex->getMessage());
		}
	}

	/**
	 * Default error handler
	 */
	public function actionError() {
		$response = array('success' => false);
		if ($error = Yii::app()->errorHandler->error) {
			$response['error'] = $error['code'];
			if ($error['code'] == 500) {
				Yii::log($error['message']);
				$response['message'] = 'Error while processing the request';
			} else
				$response['message'] = $error['message'];
		} else {
			$response['error'] = 400;
			$response['message'] = 'Invalid request';
		}

		$this->_sendResponse($response);
	}

	/**
	 * Internal function that recursively loads all POST param values
	 * @param $params
	 * @param array $params_to_check
	 * @return array
	 */
	private function _getParamsToCheck($params, &$params_to_check = array()) {
		foreach ($params as $val) {
			if (is_array($val))
				$this->_getParamsToCheck($val, $params_to_check);
			else
				$params_to_check[] = $val;
		}

		return $params_to_check;
	}

}
