<?php

/**
 * Class SiteController
 */

class MreportController extends MController {


	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {

		return array(
			'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {

		return array(

			array('allow',
				'users' => array('@'),
			),

			array('deny',
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
			),

		);
	}


	public function actionIndex() {

		$user_id = Yii::app()->user->id;

		$widget = Yii::app()->controller->createWidget('mobileapp.widgets.UserPersonalSummary', array(
			'idUser' => $user_id,
		));

		$data = $widget->getData();

		// remove html \t and \n
		$data['html'] = str_replace(array("\t", "\n"), "", $data['html']);

		foreach ($data['css'] as $css) {

			Yii::app()->getClientScript()->registerCssFile($css);
		}

		foreach ($data['js'] as $js) {

			//Yii::app()->getClientScript()->registerScriptFile($js, CClientScript::POS_END);
		}

		$this->render('index', array(
			'html' => $data['html'],
			'css' => $data['css'],
			'js' => $data['js'],
 		));
	}

}
