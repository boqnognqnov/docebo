<?php

/**
 * Class SiteController
 */

class MplayerController extends MController {


	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {

		return array(
			'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {

		return array(

			array('allow',
				'users' => array('@'),
			),

			array('deny',
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
			),

		);
	}


	public function actionIndex() {

		$user_id   = Yii::app()->user->id;
		$course_id = Yii::app()->request->getParam('course_id', '');
		$id_org    = Yii::app()->request->getParam('id_org', false);
		$m_course = LearningCourse::model()->findByPk($course_id);
		LearningCourseuser::updateCourseUserAccess($user_id, $course_id);

		if(!$m_course) {
			// User not present in platform
			throw new CHttpException(210, 'Invalid course specification');
		}

		$enrollment =  LearningCourseuser::model()->findByAttributes(array('idCourse' => $course_id, 'idUser' => $user_id));
		if($enrollment) {
			$canEnter = $enrollment->canEnterCourse();
			if(!$canEnter['can']) {
				$this->redirect(Yii::app()->createAbsoluteUrl('site/index', array('error' => 1)));
			}
		} else {
			throw new CHttpException(210, 'Invalid course specification');
		}

		// retrive course content

		$this->render('index', array(
			'id_org' => $id_org,
			'm_course' => $m_course,
			'objects' => $this->listObjects($course_id, $id_org)
		));
	}

	protected function listObjects($id_course, $id_org = false, $id_scormitem = false) {

		$output = array();

		// Check mandatory params

		$tree_params = array(
			'userInfo' => true,
			'scormChapters' => true,
			'showHiddenObjects' => false,
			'targetUser' => Yii::app()->user->id,
			'aiccChapters' => true
		);

		try {
			$tree = LearningObjectsManager::getCourseLearningObjectsTree($id_course, $tree_params);
		} catch(Exception $e) {
			throw new CHttpException(202, "Unable to list course objects");
		}

		if (!$id_org) {

			$folder = $tree[0];
		} else {

			// manage id_org.id_scormitem param time
			$org_dotted = strpos($id_org, '.');
			if ($org_dotted !== false) {
				// extract the second part first or it will be erased by the second instruction
				$id_scormitem = substr($id_org, $org_dotted + 1);
				$id_org = substr($id_org, 0, $org_dotted);
			}

			$folder = $this->_findNodeByIdOrg($tree[0], $id_org, $id_scormitem);
		}

		if ($folder) {
			// Return all learning objects under found
			if (isset($folder['children'])) {
				foreach ($folder['children'] as $children) {
					$object = array(
						'id_org' => $children['idOrganization'],
						'title' => $children['title'],
					);

					if ($children['isFolder'])
						$object['type'] = "folder";
					else {
						$object['type'] = $children['type'];
						$object['status'] = $children['status'];
						$object['locked'] = $children['locked'];
					}

					if ($children['type'] == 'sco') {
						$object['id_scormitem'] = $children['idScormItem'];
						$object['id_org'] = $object['id_org'] . '.' . $object['id_scormitem'];
					}

					if($children['type'] == 'file'){
						$object['id_resource'] = $children['idResource'];
					}

					$output[] = $object;
				}
			}
		}

		return $output;
	}



	/**
	 * Go Depth First into the tree hierarchy of learning objects
	 * @param $current_node The current node to explore
	 * @param $id_org The id_org to search for
	 * @param $id_scormitem Id of the scorm item (if any)
	 *
	 * @return array The found node
	 */
	protected function _findNodeByIdOrg($current_node, $id_org, $id_scormitem = null)
	{
		if (isset($current_node['children'])) {
			foreach ($current_node['children'] as $children) {
				if (($children['idOrganization'] == $id_org) && (!$id_scormitem || $id_scormitem == $children['idScormItem']))
					return $children;
				else {
					$node = $this->_findNodeByIdOrg($children, $id_org, $id_scormitem);
					if ($node)
						return $node;
				}
			}
		}

		return null;
	}




	/**
	 * @summary Gets the launch URL for a learning object
	 *
	 * @parameter id_org [integer, required] ID of the learning object to play
	 * @parameter id_scormitem [integer, optional] The id of the scorm chapter (if type is scorm)
	 * @parameter id_user [integer, optional] The id of the user that this url is generated for
	 *
	 * @response launch_url [string, required] The launch URL to play the passed learning object
	 *
	 * @status 201 Can't find learning object with the provided id_org, Missing required param "id_org"
	 * @status 202 Unsupported learning object type
	 * @status 203 Bad or missing resource id
	 * @status 204 Error while retrieving the launch URL for the requested resource
	 */
	public function actionPlay() {

		$course_id = Yii::app()->request->getParam('course_id', '');
		$id_org = Yii::app()->request->getParam('id_org', '');
		if (!$id_org)
			throw new CHttpException(201, 'Missing required param "id_org"');


		// Get SCORM Item ID, if any
		$id_scorm_item = Yii::app()->request->getParam('id_scormitem', '');

		// Get user id
		$id_user = Yii::app()->user->id;

		// Manage dotted id_org
		$org_dotted = strpos($id_org, '.');
		if ($org_dotted !== false) {
			// extract the second part first or it will be erased by the second instruction
			$id_scorm_item = substr($id_org, $org_dotted + 1);
			$id_org = substr($id_org, 0, $org_dotted);
		}

		// Get Launch URL
		$result = Player::getLoAuthPlayUrl($id_org, $id_user, $id_scorm_item);

		if (!$result["success"]) {
			if($result['err'] != Player::PLAYER_ERR_INVALID_LO_TYPE)
				throw new CHttpException($result["err"], Player::$errorMessages[$result["err"]]);
		}

		switch ($result['loModel']->objectType) {
			case LearningOrganization::OBJECT_TYPE_ELUCIDAT:
				$this->render('iframe_elucidat', array(
					'course_id' => $course_id,
					'id_org' => $result['loModel']->idOrg,
					'title' => $result['loModel']->title,
					'loModel' => $result['loModel'],
					'url' => str_replace('$course_id', $course_id, str_replace('/mobile/', '/lms/', $result["url"]))
				));
				break;
			case LearningOrganization::OBJECT_TYPE_VIDEO:
			case LearningOrganization::OBJECT_TYPE_FILE: {

				Yii::app()->request->redirect(str_replace('/mobile/', '/lms/', $result["url"]));
			};break;

			case LearningOrganization::OBJECT_TYPE_AUTHORING :
			case LearningOrganization::OBJECT_TYPE_SCORMORG:
			case LearningOrganization::OBJECT_TYPE_HTMLPAGE:
			case LearningOrganization::OBJECT_TYPE_TEST:
			case LearningOrganization::OBJECT_TYPE_TINCAN: {
				$this->render('iframe', array(
					'course_id' => $course_id,
					'id_org' => $result['loModel']->idOrg,
					'title' => $result['loModel']->title,
					'loModel' => $result['loModel'],
					'url' => str_replace('/mobile/', '/lms/', $result["url"])
				));
			};break;
			default : {
				$this->render('unsupported', array(
					'course_id' => $course_id,
					'id_org' => $result['loModel']->idOrg,
					'title' => $result['loModel']->title,
					'loModel' => $result['loModel'],
				));
			}
		}

		// return array('launch_url' => $result["url"]);
	}

}
