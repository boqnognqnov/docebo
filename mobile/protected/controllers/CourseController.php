<?php

/**
 * Class SiteController
 */

class CourseController extends MController {


	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {

		return array(
			'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {

		return array(

			array('allow',
				'users' => array('@'),
			),

			array('deny',
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
			),

		);
	}


	public function actionIndex() {

		$user_id = Yii::app()->user->id;

		$userModel = CoreUser::model()->findByPk($user_id);
		if(!$userModel) {
			// User not present in platform
			throw new CHttpException(210, 'Invalid user specification');
		}

		//retrieve data
		$m_courses = LearningCourse::model()->with('learningCourseusers')->findAll(
			'learningCourseusers.idUser = :id_user AND ( course_type = :elearning OR course_type = :smartphone )',
			array(
				':id_user' => $user_id,
				':elearning' => LearningCourse::TYPE_ELEARNING,
				':smartphone' => LearningCourse::TYPE_MOBILE,
			));

		$courses = array();
		foreach ($m_courses as $course) {
			/* @var $course LearningCourse */
			$link = Docebo::createAbsoluteLmsUrl('player', array('course_id' => $course->idCourse));
			$thumbnail = $course->getCourseLogoUrl();

			/* @var $enrollment LearningCourseuser */
			$enrollment =  LearningCourseuser::model()->findByAttributes(array('idCourse' => $course->idCourse, 'idUser' => $user_id));
			if($enrollment)
				$canEnter = $enrollment->canEnterCourse();
			else
				$canEnter = array('can' => false);

			$thumbnail = str_ireplace('http://http://', '//', $thumbnail);
			$thumbnail = str_ireplace('https://https://', '//', $thumbnail);

			$thumbnail = str_ireplace('http://', '//', $thumbnail);
			$thumbnail = str_ireplace('https://', '//', $thumbnail);

			// Find course progress
			$percentage = LearningOrganization::getProgressByUserAndCourse($course->idCourse, $user_id);

			// Find course session time for this user

			$courses[] = array (
				'course_id' => (int) $course->idCourse,
				'course_name' => str_replace('&', '&amp;', $course->name),
				'course_description' => str_replace('&', '&amp;', $course->description),
				'can_enter' => $canEnter,
				'course_link' => $link,
				'course_thumbnail' => $thumbnail,
				'courseuser_status' => $enrollment ? $enrollment->status : 0,
				'course_type' => $course->course_type,
				'course_progress'   => $percentage,
				'lang_code'   => $course->lang_code,
				'date_complete' => $enrollment->date_complete ? Yii::app()->localtime->toUTC($enrollment->date_complete) : null,
				'first_access' => $enrollment->date_first_access ? Yii::app()->localtime->toUTC($enrollment->date_first_access) : null,
				'last_access' => $enrollment->date_last_access ? Yii::app()->localtime->toUTC($enrollment->date_last_access) : null,
			);
		}

		$this->render('index', array(
			'courses' => $courses,
		));
	}

	/**
	 * Send file to user. Only for files stored in local file system!
	 *
	 * @throws CException
	 */
	public function actionSendFile() {

		$transaction = Yii::app()->db->beginTransaction();

		try {

			$idFile = (int)Yii::app()->request->getParam('id_file', 0);
			$idCourse = (int)Yii::app()->request->getParam('course_id', 0);

			$file = LearningMaterialsLesson::model()->findByPk($idFile);
			if (!$file) {
				throw new CException('Invalid file ID: '.$idFile);
			}

			$criteria = new CDbCriteria();
			$criteria->addCondition('idResource=:idFile');
			$criteria->addCondition('objectType=:objType');
			$criteria->addCondition('idCourse=:idCourse');
			$criteria->params = array(
				':idCourse' => $idCourse,
				':idFile' => $idFile,
				':objType' => LearningOrganization::OBJECT_TYPE_FILE,
			);
			$object = LearningOrganization::model()->find($criteria);

			if (!$object) {
				throw new CException('Invalid object ID: '.$idFile);
			}


			//track the page
			$idUser = Yii::app()->user->id;
			$track = LearningMaterialsTrack::model()->findByAttributes(array('idReference' => $object->idOrg, 'idUser' => $idUser));
			if (!$track) {
				//the object has not been tracked yet for this user: create the record now
				$track = new LearningMaterialsTrack();
				$track->idUser = (int)$idUser;
				$track->idReference = (int)$object->getPrimaryKey();
				$track->idResource = $idFile;
			}
			$track->setStatus(LearningCommontrack::STATUS_COMPLETED);
			$track->setObjectType(LearningOrganization::OBJECT_TYPE_FILE);
			if (!$track->save()) {
				throw new CException("Error while saving track data");
			}
			UserLimit::logActiveUserAccess($idUser);

			//finish action
			$transaction->commit();

			// Use file storage manage
			$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_FILE);
			$file->original_filename = Sanitize::fileName($file->original_filename,true);
			$storageManager->sendFile($file->path, $file->original_filename);
		} catch (CException $e) {

			$transaction->rollback();
			echo $e->getMessage();
		}
	}



}
