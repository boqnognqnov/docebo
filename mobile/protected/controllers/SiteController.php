<?php

/**
 * Class SiteController
 */

class SiteController extends MController {


	/**
	 * (non-PHPdoc)
	 * @see CController::filters()
	 */
	public function filters() {

		return array(
			'accessControl',
		);
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::accessRules()
	 */
	public function accessRules() {

		return array(

			array('allow',
				'actions' => array('error', 'login', 'logout', 'loginRest', 'hybridauthEndpoint', 'index', 'colorschemeCss', 'getUserPage', 'privacy', 'sso', 'axUploadFile', 'axApproveLastTos', 'autoLogin'),
				'users' => array('*'),
			),

			array('allow',
				'users' => array('@'),
			),

			array('deny',
				'users' => array('*'),
				'deniedCallback' => array($this, 'accessDeniedCallback'),
			),

		);
	}


	public function actionIndex($login_userid = '') {

		if (!Yii::app()->user->isGuest) {
			$this->redirect(Yii::app()->createAbsoluteUrl('course/index', array()));
		}

		$langs = Lang::getLanguages(true);

		$this->render('index', array(
			'langs'        => $langs,
			'login_userid' => $login_userid,
			'logo'         => Yii::app()->theme->getLogoUrl(),
			'home_image'   => Yii::app()->theme->getLoginImageUrl(CoreAsset::VARIANT_ORIGINAL),
		));
	}

    public function actionHybridauthEndpoint(){
        Yii::import('common.vendors.hybridauth.Hybrid.Auth',1);
        Yii::import('common.vendors.hybridauth.Hybrid.Endpoint',1);
        Hybrid_Endpoint::process();
    }

	public function actionLogin() {

		$username = Yii::app()->request->getParam('login_userid', '');
		$password = Yii::app()->request->getParam('login_pwd', '');

		if (!$username || !$password) {
			if(!$username)
				unset(Yii::app()->session['login_userid']);
			else
				Yii::app()->session['login_userid'] = $username;
			// Username or password not entered
			$this->redirect(Yii::app()->createAbsoluteUrl('site/index', array('error' => 1)));
		}

		$ui = new DoceboUserIdentity($username, $password);

		if ($ui->isUserHacker()) {
			unset(Yii::app()->session['login_userid']);
			$this->redirect(Yii::app()->createAbsoluteUrl('site/index', array('error' => 3)));
		}

		if ($ui->authenticate()) {
			if($ui->checkUserAlreadyLoggedIn())
				$this->redirect(Yii::app()->createAbsoluteUrl('site/index', array('error' => 4)));

			Yii::app()->user->login($ui);
		} else {
			Yii::app()->session['login_userid'] = $username;

			$ui->logFailedLoginAttempt();

			// Unable to authenticate, wrong username or password
			$this->redirect(Yii::app()->createAbsoluteUrl('site/index', array('error' => 2)));
		}

		if(isset(Yii::app()->session['login_userid']))
			unset(Yii::app()->session['login_userid']);

		LearningTracksession::model()->updateAll(array('active' => 0), 'active = 1 AND idUser = '.Yii::app()->user->id);

		$this->redirect(Yii::app()->createAbsoluteUrl('course/index', array()));
	}


	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout() {

		if (Yii::app()->user->id && !Yii::app()->user->getIsGuest()) {
			LearningTracksession::model()->updateAll(array('active' => 0), 'active = 1 AND idUser = '.Yii::app()->user->id);
		}

		$previouslyLoggedUser = Yii::app()->user->loadUserModel();
		Yii::app()->user->logout();

		// Trigger event for plugins post-logout processing. This call may never return.
		Yii::app()->event->raise("UserLogout", new DEvent($this, array('user' => $previouslyLoggedUser)));

		if(Settings::get('user_logout_redirect') == 'on' && Settings::get('user_logout_redirect_url')){
			$url = Settings::get('user_logout_redirect_url');
			if(strpos($url , 'http') === false)
				$url = 'http://' . $url;

			$this->redirect($url);
		}

		$this->redirect( $this->createUrl('site/index') );
	}

	/**
	 * Return the css with that overrides the css colors
	 */
	public function actionColorschemeCss() {
		header("Content-type: text/css");
		echo CoreScheme::generateMobileCss();
		Yii::app()->end();
	}

	/**
	 * Action that handles sso with token
	 * Example URL: /mobile/?r=site/sso&login_user=XXXX&time=12321321321&token=AERQWETWERTR
	 * [optional] &id_course=1234
	 * [optional] &destination=[mycourse|catalog|learningplan]
	 */
	public function actionSso()
	{

		try {
			// Prepare a SSO dispatch table, to determine which app will handle the current request
			$sso_dispatch_table = array();
			Yii::app()->event->raise('RegisterSSOHandlers', new DEvent($this, array('map' => &$sso_dispatch_table)));
			if (empty($sso_dispatch_table))
				throw new Exception("No SSO handlers registered. Aborting SSO request.");

			// Get the sso type of the current request
			$sso_type = Yii::app()->request->getParam('sso_type', 'token');

			// Dispatch the request to the proper handler
			if (isset($sso_dispatch_table[$sso_type]) && !is_null($sso_dispatch_table[$sso_type])) {
				/* @var $handler ISsoHandler */
				$handler = $sso_dispatch_table[$sso_type];
				$userid = $handler->login();
				if (!$userid)
					throw new Exception("No valid user logged in");

				// Search for the user in DB and authenticate him without password
				$ui = new DoceboUserIdentity($userid, null);
				if ($ui->authenticateWithNoPassword()) {
					if($ui->checkUserAlreadyLoggedIn())
						throw new Exception(Yii::t('login', '_TWO_USERS_LOGGED_WITH_SAME_USERNAME'), 4);

					Yii::app()->user->login($ui);

					// Login is NOT enough to say that user is Active;
					// As of Mid December'13 User is counted as active when he/she enters a course
					// UserLimit::logActiveUserLogin();

					// if the authentication goes well we need to check for a suspended installation process
					Docebo::resumeSaasInstallation();

					// Determine which URL to jump to
					$redirect_url = $handler->getLandingURL();
					if (!$redirect_url)
						$redirect_url = Yii::app()->createAbsoluteUrl('site/index', array('login' => 1));

					// Finally, redirect!!
					$this->redirect($redirect_url);

				} else {
					$ui->logFailedLoginAttempt();
					throw new Exception("Invalid username or user suspended", 2);
				}

			} else
				throw new Exception("Wrong or unknown sso handler.");
		} catch (Exception $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			$this->redirect(Yii::app()->createAbsoluteUrl('site/index', array('error' => $e->getCode())), true, 302, true);
		}
	}

}
