<!-- Start of login : #courses -->
<div data-role="page" id="play-<?= $id_org; ?>" class="play-page">


	<div data-role="header">
		<a href="<?= $this->createUrl('mplayer/index', array('course_id' => $course_id)); ?>"
		   data-ajax="false" class="ui-btn ui-icon-carat-l ui-nodisc-icon ui-btn-icon-notext ui-corner-all ui-btn-transparent"><?= Yii::t('standard', '_BACK'); ?></a>
		<h1><?= $title; ?></h1>
	</div>
	<!-- /header -->

	<div role="main" id="scroller" class="iframe-scroller ui-content wrapper-iframe">

		<iframe class="content-iframe" src="<?= $url; ?>"></iframe>

	</div>
	<!-- /content -->

</div><!-- /page login -->
<script>
	// $('#scroller').css({'overflow' : 'auto', '-webkit-overflow-scrolling' : 'touch'});
</script>