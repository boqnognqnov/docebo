<?php
/** @var LearningCourse $m_course */?><!-- Start of login : #course-[n] -->
<div data-role="page" id="course-<?= $m_course->idCourse; ?>" class="courses-page">


	<div data-role="header" data-position="fixed">
		<a href="<?= $this->createUrl('course/index'); ?>" class="ui-btn ui-icon-carat-l ui-nodisc-icon ui-btn-icon-notext ui-corner-all ui-btn-transparent"><?= Yii::t('standard', '_BACK'); ?></a>
		<h1><?= $id_org; ?><?= $m_course->name; ?></h1>
	</div>
	<!-- /header -->

	<div role="main" class="ui-content ui-objects">

		<ul data-role="listview">
			<?php foreach($objects as $lo) : ?>
				<?php
				if($lo['locked'])
				{
					$img = 'lock';
					$link =  '';
				}
				else
				{
					switch ($lo['status']) {
						case 'completed' : $userstatus = 'completed';break;
						case 'attempted' : $userstatus = 'inprogress';break;
						case 'failed' : $userstatus = 'failed';break;
						default : $userstatus = 'notstarted';
					}

					$img = $lo['type'] . $userstatus;

					if ($lo['type'] == 'folder') {
						$link = $this->createUrl('mplayer/index', array('course_id' => $m_course->idCourse, 'id_org' => $lo['id_org']));
					}elseif ($lo['type'] == 'file'){

						$link = $this->createUrl('course/SendFile', array('course_id' => $m_course->idCourse , 'id_file' => $lo['id_resource']));
					}else {
						$link = $this->createUrl('mplayer/play', array('course_id' => $m_course->idCourse, 'id_org' => $lo['id_org'], 'id_scormitem' => $lo['id_scormitem']));
					}
				}
				?>
				<li>
					<a <?= ( $lo['type'] == 'video' || $lo['type'] == 'false' || $lo['type'] == 'file' ? ' data-ajax="false"' : '' ); ?> <?= ( $lo['type'] == 'file' ? ' class = download-link' : '' ); ?> href="<?= $link; ?>"
																																																		   onclick="window.location.href = this.href; event.preventDefault();"
						>
						<img class="ui-li-icon" src="<?= Yii::app()->theme->baseUrl . '/images/mobile/' . $img . '.png'; ?>" />
						<h2><?= $lo['title']; ?></h2>
					</a>
				</li>

			<?php endforeach; ?>
		</ul>

	</div>
	<!-- /content -->

</div><!-- /page login -->

<script type="text/javascript">
	$(function(){
		$(".download-link").on("click", function(){
			// Arena.showQuickNav();
			// After click on the Download link,
			// give 5 secs to Download process to start and do the FILE COMPLETED Tracking (in the controller action)
			setTimeout(function(){
				location.reload();
			}, 10000);
		});
	});
</script>