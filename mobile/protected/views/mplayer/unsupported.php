
!-- Start of login : #courses -->
<div data-role="page" id="play-<?= $id_org; ?>" class="play-page">


	<div data-role="header">
		<a href="<?= $this->createUrl('mplayer/index', array('course_id' => $course_id)); ?>"
		   data-ajax="false" class="ui-btn ui-icon-carat-l ui-nodisc-icon ui-btn-icon-notext ui-corner-all ui-btn-transparent"><?= Yii::t('standard', '_BACK'); ?></a>
		<h1><?= $title; ?></h1>
	</div>
	<!-- /header -->

	<div role="main" id="scroller" class="ui-content wrapper-iframe">

		<?= Yii::t('mobile', 'This training material is not available on a smartphone device.'); ?>

	</div>
	<!-- /content -->

</div><!-- /page login -->