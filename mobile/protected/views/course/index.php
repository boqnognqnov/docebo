<!-- Start of login : #courses -->
<div data-role="page" id="courses" class="courses-page">

	<div class="main-menu" data-role="panel" id="main-menu" data-position-fixed="true">
		<?= $this->renderPartial('//common/_menu'); ?>
	</div><!-- /panel -->

	<div data-role="header" data-position="fixed">
		<a href="#main-menu" class="ui-btn ui-icon-bars ui-nodisc-icon ui-btn-icon-notext ui-corner-all ui-btn-transparent"><?= Yii::t('adminrules', '_ADMIN_MENU'); ?></a>
		<h1><?= Yii::t('menu_over', '_MYCOURSES'); ?></h1>
	</div>
	<!-- /header -->

	<div role="main" class="ui-content">
		<form class="ui-filterable">
			<input id="search-course-input" data-type="search" placeholder="<?= Yii::t('standard', '_SEARCH'); ?>">
		</form>
		<ul data-role="listview" data-filter="true" data-input="#search-course-input">
		<?php foreach($courses as $course) : ?>

			<?php if ($course['can_enter']['can']) : ?>

				<li><a href="<?= $this->createUrl('mplayer/index', array('course_id' => $course['course_id'])); ?>">
					<img src="<?= $course['course_thumbnail']; ?>">
					<h2><?= $course['course_name']; ?></h2>
					<p><?= '<span class="course_status">' . Yii::t("subscribe", LearningCourseuser::getStatusLangLabel($course['courseuser_status'])) . '</span>'; ?></p></a>
				</li>

			<?php else : ?>

				<li>
					<img src="<?= $course['course_thumbnail']; ?>">
					<span class="course-locked"></span>
					<h2><?= $course['course_name']; ?></h2>
					<p><?= '<span class="course_status">' . Yii::t("subscribe", LearningCourseuser::getStatusLangLabel($course['courseuser_status'])) . '</span>'; ?></p>
				</li>

			<?php endif; ?>

		<?php endforeach; ?>
		</ul>

	</div>
	<!-- /content -->

</div><!-- /page login -->