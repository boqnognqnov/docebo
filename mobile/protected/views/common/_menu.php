<?php
$langs = Lang::getLanguages(true);
$currentLang = Yii::app()->getLanguage();
?>
<div class="ui-panel-inner">

	<ul data-role="listview" data-icon="false">
		<li>
			<a data-ajax="false" href="<?= $this->createUrl('course/index') ?>" class="ui-btn ui-icon-home ui-btn-icon-left ui-btn-transparent">
				<?= Yii::t('menu_over', '_MYCOURSES'); ?></a>
		</li>
		<li>
			<a data-ajax="false" href="<?= $this->createUrl('mreport/index') ?>" class="ui-btn ui-icon-user ui-btn-icon-left ui-btn-transparent">
				<?= Yii::t('course', 'My Activities'); ?></a>
		</li>
		<?php if(!empty($langs)){ ?>
		<li class="langMenuLi">
			<span class="langMenuItem ui-btn ui-btn-icon-left ui-btn-transparent"><i class="fa fa-globe"></i>
			<?= Yii::t('standard', '_LANGUAGE'); ?></span>
			<div class="second-menu">
				<ul>
				<?php foreach($langs as $lang){
					$addClass = ($lang['browsercode'] == $currentLang)?" class='currentLang'":"";
					print "<li".$addClass."><a href='".$this->createUrl('site/index', array('lang' => $lang['browsercode']))."'>".$lang['description']."</a></li>";
				} ?>
				</ul>
			</div>
		</li>
		<?php } ?>
		<li>
			<a data-ajax="false" href="<?= $this->createUrl('site/logout') ?>" class="ui-btn ui-icon-power ui-btn-icon-left ui-btn-transparent">
				<?= Yii::t('standard', '_LOGOUT'); ?></a>
		</li>
	</ul>

</div>