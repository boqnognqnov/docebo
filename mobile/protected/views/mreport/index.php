<!-- Start of login : #myactivities -->
<div data-role="page" id="myactivities" class="report-page">

	<div class="main-menu" data-role="panel" id="main-menu" data-position-fixed="true">
		<?= $this->renderPartial('//common/_menu'); ?>
	</div><!-- /panel -->

	<div data-role="header" data-position="fixed">
		<a href="#main-menu" class="ui-btn ui-icon-bars ui-nodisc-icon ui-btn-icon-notext ui-corner-all ui-btn-transparent"><?= Yii::t('adminrules', '_ADMIN_MENU'); ?></a>
		<h1><?= Yii::t('course', 'My Activities'); ?></h1>
	</div>
	<!-- /header -->

	<div role="main" class="ui-content">

		<?= $html; ?>

	</div>
	<!-- /content -->

</div><!-- /page login -->