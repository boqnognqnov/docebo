<!-- Start of login : #login -->
<div data-role="page" id="login" class="login-page">

	<?php if (!empty($langs)) : ?>
	<div class="language" data-role="panel" id="language" data-position-fixed="true">
		<div class="ui-panel-inner">

			<ul data-role="listview" data-icon="false">
			<?php foreach ($langs as $lang) : ?>

				<li>
					<a data-ajax="false" href="<?= $this->createUrl('site/index', array('lang' => $lang['browsercode'])) ?>"><?= $lang['description']; ?></a>
				</li>

			<?php endforeach; ?>
			</ul>

		</div>
	</div><!-- /panel -->
	<?php endif; ?>

	<div data-role="header" data-position="fixed">
		<?php if (!empty($langs)) : ?>
			<a href="#language" id="handler-language" data-role="none"><i class="fa fa-globe"></i></a>
		<?php endif; ?>
		<h1><?= Yii::t('login', '_LOGIN'); ?></h1>
	</div><!-- /header -->

	<div role="main" class="ui-content ui-login">

		<?php $this->renderPartial('_login', array(
			'login_userid' => $login_userid,
			'logo'         => $logo,
			'home_image'   => $home_image,
		)); ?>

	</div><!-- /content -->

</div><!-- /page login -->

<!-- Start of recover password : #recover-page -->
<div data-role="page" id="recover-page" class="login-page">

	<div role="main" class="ui-content">

	</div><!-- /content -->

</div><!-- /page recover-page -->

<?php
// We need to redirect back to the correct page if it's not a smartphone based on the resolution
if (Yii::app()->user->isGuest) : ?>
	<script type="text/javascript">
		<!--
		var maxWidthSmartphone = <?= (isset(Yii::app()->params['max_width_smartphone']) ? (int) Yii::app()->params['max_width_smartphone'] : 767) ?>;
		var w = $(window).width();
		var h = $(window).height();
		var ww = Math.max(w,h);

		if (ww > maxWidthSmartphone) {
			// Redirect back to home, passing GET parameter to warn the application controller this is NOT a smartphone device
			window.location.replace('<?= Docebo::createAbsoluteLmsUrl('//mobileapp/default/index', array('not_smartphone' => 1)); ?>');
		}
		//-->
	</script>
<?php endif; ?>