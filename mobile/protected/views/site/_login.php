

<div class="login-logo">
	<img src="<?= $logo ?>" alt="<?= Yii::t('login', '_HOMEPAGE'); ?>" />
</div>

<div class="login-wrapper">

	<div class="login-bg">
		<img class="homepage" src="<?= $home_image ?>" alt="<?= Yii::t('login', '_HOMEPAGE'); ?>" />
	</div>

	<?= CHtml::beginForm($this->createUrl('site/login'), 'post', array(
		'id' => 'login-form',
	)) ?>

		<div id="loginError">
			<?= DoceboUI::getLoginError(); ?>
		</div>

		<?php if (Yii::app()->event->raise('BeforeLoginFormRender', new DEvent($this, array()))) : ?>

			<div class="input-wrapper">
				<input style="height: 34px;" data-role="none" type="text" id="login_userid" name="login_userid" value="<?= $login_userid; ?>" placeholder="<?= Yii::t('standard', '_USERNAME'); ?>" />

				<input style="height: 34px;" data-role="none" type="password" id="login_pwd" name="login_pwd" placeholder="<?= Yii::t('standard', '_PASSWORD'); ?>" />
			</div>

			<button type="submit" name="login" class="btn-login">
				<?= Yii::t('login', '_LOGIN'); ?>
			</button>
            <!-- START Google SSO App -->
            <?php if ( PluginManager::isPluginActive('GooglessoApp')) : ?>
                <div class="row">
                    <a class="google-login-link" rel="external" title="Google Login" href="<?=Docebo::createLmsUrl('GooglessoApp/GooglessoApp/login')?>">
                        <span class="span-logo" title="Google Login" class="pull-left">
                            <?= CHtml::image(Yii::app()->theme->baseUrl . "/images/loginexternal/google-25.png") ?>
                        </span>
                        <span class="google-text"><?= Yii::t('login', 'Sign in with Gmail'); ?></span>
                    </a>
                </div>
            <?php endif; ?>
            <!-- END Google SSO -->
            <!-- START Google Apps -->
            <?php if ( PluginManager::isPluginActive('GoogleappsApp')) : ?>
                <div class="row">
                    <a class="google-apps-link" rel="external" title="Google Apps" href="<?=Docebo::createLmsUrl('GoogleappsApp/GoogleappsApp/gappsLogin')?>">
                        <span class="span-apps-logo" title="Google Apps" class="pull-left">
                            <?= CHtml::image(Yii::app()->theme->baseUrl . "/images/loginexternal/google-25.png") ?>
                        </span>
                        <span class="google-text"><?= Yii::t('login', 'Sign in with Google Apps');?></span>
                    </a>
                </div>
            <?php endif; ?>
            <!-- END Google App -->

			<? Yii::app()->event->raise('afterMobileLoginPageSocialIcons', new DEvent($this, array())) ?>

        <?php endif; ?>

	<?= CHtml::endForm(); ?>

</div>
