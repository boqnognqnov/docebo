#!/bin/sh
#
# Create required folders and set permissions
# Since LMS R 6.x
#
# It is a good idea to put the owner of Web root directories into Apache user group (www-data for Linux/Ubuntu).
# That way you don't have to run this file as root (because below folders usualy have some files inside, created by Apache)  
#
#

# Create directories
mkdir admin/assets 
mkdir admin/protected/runtime 
mkdir lms/assets
mkdir lms/protected/runtime 
mkdir common/extensions/JsTrans/assets 
mkdir authoring/assets 
mkdir authoring/protected/runtime 
mkdir themes/spt/assets
mkdir lmstmp
mkdir lmslogs
mkdir files
mkdir config


# Set permissions
chmod -R 777 admin/assets 
chown -R www-data.www-data admin/assets
chmod -R 777 admin/protected/runtime 
chown -R www-data.www-data admin/protected/runtime
chmod -R 777 lms/assets 
chown -R www-data.www-data lms/assets
chmod -R 777 lms/protected/runtime 
chown -R www-data.www-data lms/protected/runtime
chmod -R 777 common/extensions/JsTrans/assets 
chown -R www-data.www-data common/extensions/JsTrans/assets
chmod -R 777 authoring/assets 
chown -R www-data.www-data authoring/assets
chmod -R 777 authoring/protected/runtime 
chown -R www-data.www-data authoring/protected/runtime
chmod -R 777 themes/spt/assets
chown -R www-data.www-data themes/spt/assets
chmod -R 777 lmstmp
chown -R www-data.www-data lmstmp
chmod -R 777 lmslogs
chown -R www-data.www-data lmslogs
chmod -R 777 files
chown -R www-data.www-data files
chmod -R 777 config
chown -R www-data.www-data config
