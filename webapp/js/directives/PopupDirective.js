/**
 * Created by kyuchukovv on 4/4/2016.
 */
/**
 * This is used to create alert like popup messages
 *
 * You need to define params object in your controller
 *
 * Call example: <div popup params="paramsFromScope">Open Popup</div>
 * where : paramsFromScope = { title, message, buttonText }
 */
'use strict';
define(function(){
   function Popup(){
       return {
           restrict: 'EA',
           scope: {
               params: '='
           },
           link: function(scope, element, attr) {
               angular.element(element).on('click', function() {
                   var modalParams = scope.params;
                   var webApp = new Framework7({
                       material: true,
                       modalTitle: modalParams.title,
                       modalButtonOk: modalParams.buttonText,
                       modalButtonCancel: modalParams.buttonCancel,
                       modalCloseByOutside: true,
                       popupCloseByOutside: true,
                       modalActionsCloseByOutside: true
                   });
                   if (modalParams.buttonCancel){
                       webApp.confirm(modalParams.message);
                   }else{
                       webApp.alert(modalParams.message);
                   }


                   /**
                    * close popup on click
                    * @param evt
                    */
                   function closePopup(evt) {
                       evt.preventDefault();
                       webApp.closeModal();
                       angular.element(".modal-button").trigger('click');
                   }

                   var el = document.getElementsByClassName("modal-overlay-visible")[0];
                   el.addEventListener("touchend", closePopup, false);
               });

           }
       };
   }
    Popup.$inject = [];
    return Popup;
});