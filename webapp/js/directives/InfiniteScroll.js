/* global angular */
'use strict';
define(function () {
	function InfiniteScroll(Utils, $window) {
		return {
			restrict: 'A',
			link: function (scope, elem, attr) {

				var timeElapsed = 0;
				var interval;

				angular.element($window).bind('touchmove', function () {
					if(timeElapsed == 0) {
						timeElapsed += 100;
						interval = setInterval(refresh, 100);
					}
				});
				var refresh = function () {
					timeElapsed += 100;
					if(timeElapsed <= 1200) {
						var isOnEnd = Utils.isElementBottomVisible(elem[0]);
						if (isOnEnd) {
							timeElapsed = 0;
							clearInterval(interval);
							scope.$broadcast('infiniteScroll', {element: elem[0]});
						}
					} else {
						timeElapsed = 0;
						clearInterval(interval);
					}
				};
			}
		};
	}
	InfiniteScroll.$inject = ['Utils', '$window'];//Injecting dependencies
	return InfiniteScroll;//Returns Class definition
});