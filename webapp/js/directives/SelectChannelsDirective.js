/**
 * Created by Mitko on 3/31/2016.
 */
'use strict';
define(function(){
    function SelectChannelsDirective($rootScope, ChannelsData, $compile) {
        return {
            restrict: 'AEC',
            scope: {
                channels:"=channelsModel"
            },
            templateUrl: 'views/directives/select_channels.html',
            link: function(scope, elem, attr, compile) {

                scope.$on("modalLength", function(event, data){
                    scope.channels = data;
                });

                elem.bind('click', function() {
                    var el = $compile( "<modalchanel channels-model='channelItems'>" )($rootScope);
                    angular.element("body").append( el );
                    var $div = $('modalchanel');
                    $div.not(':last').remove();
                });
            }
        };
    }
    SelectChannelsDirective.$inject = ['$rootScope', 'ChannelsData', "$compile"];
    return SelectChannelsDirective;
});