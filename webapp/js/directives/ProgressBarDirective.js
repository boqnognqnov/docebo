/**
 * Created by Mitko on 20.10.2016 г..
 */

'use strict';
define([], function () {
    function ProgressBarDirective(){
        return {
            restrict: 'AE',
            templateUrl: 'views/directives/progress_bar.html',
            scope: {
                currentProgress: '=progressValue'
            },
            link: function(scope, elem, attr){
                scope.startStorageProgress = function () {
                    var progress = angular.element('.progress-load');
                    progress.css('width', scope.currentProgress);
                };

                scope.$on("progressValue", function (event, data) {
                    scope.currentProgress = data;
                    scope.startStorageProgress();
                });
            }
        };
    }
    ProgressBarDirective.$inject = []; // Injecting dependencies
    return ProgressBarDirective; // Returns class definition
});