/* global angular */
'use strict';
define([], function(){
   function DoceboAutocomplete(){
       return {
           templateUrl: 'views/directives/autocomplete.html',
           restrict: 'AE',
           scope: {
                suggestions: '=tagsList',
                tagsModel: '=binding'
           },
           link: function(scope, element, attr){
				scope.showSuggestions = false;
				scope.suggestionsModel = '';
				scope.newTag = '';

				/*
				 * Checks whether tag exists in suggestions and return index in the array if exists
				 * @param {String} value
				 * @return {Int} Index
				 */
				scope.checkInSuggestions = function(value){
					var lowerVal = value.toLowerCase();
					var countSuggestions = scope.suggestions.length;
					for(var i=0; i < countSuggestions; i++){
						if(scope.suggestions[i].toLowerCase() === lowerVal){
							return i;
						}
					}
					return -1;
				};
				/*
				 * Filter for which suggestions to show by input field text
				 * @param {String} value
				 * @return {Void}
				 */
				scope.tagsFilter = function (actual) {
					var expected = scope.suggestionsModel;
					var lowerStr = (actual + "").toLowerCase();
					return lowerStr.indexOf(expected.toLowerCase()) === 0;
				};
				/*
				 * Append tag to model and removes it from suggestions
				 * @param {String} value
				 * @return {Void}
				 */
				scope.appendToModel = function(value){
					scope.suggestionsModel = '';
					var index = scope.checkInSuggestions(value);
					if(index !== -1){
						scope.suggestions.splice(index, 1);
					}
					if(typeof scope.tagsModel === 'undefined'){
						scope.tagsModel = [];
					}
					scope.tagsModel.push(value);
				};

				/*
				 * Removes value from the input model and returns them to suggestions
				 * @param {String} value
				 * @return {Void}
				 */
				scope.removeFromModel = function(value){
					angular.element('#suggestion_'+value).hide();
					var index = scope.tagsModel.indexOf(value);
					scope.tagsModel.splice(index, 1);
					scope.suggestions.push(value);
				};
			   /**
				* When enter is pressed add the tag in the tagsModel
				* @param keyEvent - event on keypress
                */
			   scope.detectEnter = function(keyEvent){
				   if ( keyEvent.keyCode == '13'){
					   var newTag = scope.cleanString(angular.element("#autocomplete-dropdown").val());
					   scope.appendToModel(newTag);
				   }
			   };
			   /**
				* Returns a clean string for tags insertion
				* @param string
                * @returns {*}
                */
			   scope.cleanString = function(string){
				   return string.replace(/\,|<\/?[^>]+(>|$)/g, "");
			   };

				scope.$watch('suggestionsModel', function(nv){
					if(nv !== ''){
						//Add's new tag if not exists
						var lastCharacter = nv.substr(nv.length - 1);
						var string = nv.replace(',', '');
						var cleanString = scope.cleanString(string);

						if(lastCharacter === ',' && scope.tagsModel.indexOf(cleanString) === -1 && cleanString !== ''){
								scope.appendToModel(cleanString);
								scope.suggestionsModel = '';
								scope.newTag = cleanString;
								return;
						}
						scope.showSuggestions = true;
					}else{
						scope.showSuggestions = false;
					}
					scope.suggestion = '';
				});
           }
       };
   }
    DoceboAutocomplete.$inject = [];
    return DoceboAutocomplete;
});