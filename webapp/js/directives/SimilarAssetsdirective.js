'use strict';
define(function () {
	function SimilarAssetsDirective($timeout, $location, $routeParams, CSAsset) {
		return {
			restrict: 'E',
			templateUrl: 'views/directives/similar_assets.html',
			scope: true,
			link: function(scope, elem, attr){
				//Parsing initial slides
				attr.$observe('data', function(value){
					scope.items = JSON.parse(attr.data); //Parsing initial slides
					scope.lastItemID = scope.items.length;//Last item used for requesting more by infinite scroll
				 });  
				 attr.$observe('similar_total', function(value){
					scope.similar_total = attr.similar_total; //Parsing initial slides
				 });  
				scope.chunkSize = 5;//Chunk size for slides
				scope.inifiniteScroll = attr.infiniteScroll;//Infinite scroll function definition
				scope.disableInfinite = false;//Flag should prevent infinite scroll request
				scope.endpoint = attr.endpoint;//End point passed in attributes
				scope.elementId = attr.id;//Element id
				
				scope.show = false;
				var minItems = typeof attr['minimumItems'] !== 'undefined' ? attr['minimumItems'] : 5;
				angular.element(elem).on('mouseenter', function(){
					scope.show = true;
					scope.$digest();
				});
				angular.element(elem).on('mouseleave', function(){
					scope.show = false;
					scope.$digest();
				});
				
				/**
				 * Executes ajax request to parse more items for current carousel
				 * @param {Void} updateCallback
				 * @returns {Void}
				 */
						
				scope.infiniteScrollHorizontal = function(updateCallback){
					if(scope.disableInfinite === true){
						//updateCallback();
						return;
					}
					
					var promise = CSAsset.getRelatedAssets($routeParams.id, scope.lastItemID, scope.chunkSize);
					promise.then(function(response){
						if (response ) {
							angular.forEach(response, function (item) {
								if(typeof item === 'object'){
									scope.items.push(item);
								}
							});
							if(scope.items.length === scope.similar_total){ 
								scope.disableInfinite = true;
							}
							scope.lastItemID = scope.items.length; 
							if(!scope.$$phase){ 
								scope.$digest();
							}
							updateCallback();
						}
					});
				
				};
				var initiliaze = function(){ //Function used to init/reinit swiper carousel on angular $digest done
					$timeout(function(){
						new Swiper('#'+scope.elementId, {
							slidesPerView: 'auto',
							nextButton: '.swiper-button-next',
							prevButton: '.swiper-button-prev',
							//freeMode: true,
							onReachEnd: function($self){
								//Executes when swiper reach end
								if(scope.items.length < minItems){ //If items less than 12 dont execute infinite carousel this means there are no more items on server
									return;
								}
								
								if(!scope.disableInfinite){
									scope.infiniteScrollHorizontal(function(){//Trigger request to the server
										$timeout(function(){
											//$self.update(true);//Update swiper so can remove unneccessary placeholders
											$self.slideNext(false);
										});
									});
								}
							},
							onSlideChangeEnd: function($self){
								$self.update(true);
							}
						});
					});
				};
				//Calling swiper initially
				initiliaze();
			}				
		};
	}
	SimilarAssetsDirective.$inject = ['$timeout', '$location', '$routeParams', 'CSAsset'];
	return SimilarAssetsDirective;
});