'use strict';
define(function () {
	function AssetPlaydirective($timeout, $rootScope) {
		return {
			restrict: '',
			templateUrl: 'views/directives/asset_play.html',
			link:  function (scope, element, attr, ctrl) {
			}
		};
	}
	AssetPlaydirective.$inject = ['$timeout', '$rootScope'];
	return AssetPlaydirective;
});