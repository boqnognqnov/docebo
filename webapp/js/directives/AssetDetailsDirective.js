'use strict';
define(function () {
	function AsssetDetailsDirective($timeout) {
		return {
			restrict: '',
			templateUrl: 'views/directives/asset_details.html',
			link: function(scope, elem, attr){
				
			}				
		};
	}
	AsssetDetailsDirective.$inject = ['$timeout'];
	return AsssetDetailsDirective;
});