/* global angular */
'use strict';
define(function () {
	/**
	 * Lazy loading directive class definition
	 * @param {Service} $window
	 * @returns {Class}
	 */
	function DoceboScroll($window){
		return {
			scope: false,
			restrict: 'A',
			link: function(scope, elem, attr){
				var dom = angular.element;
				dom($window).on('touchmove', function(e){
					dom(this).trigger('scroll');
				});
			}
		};
	}
	DoceboScroll.$inject = ['$window'];//Injecting dependencies
	return DoceboScroll;//Returns Class definition
});