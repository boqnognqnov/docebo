/**
 * Created by Mitko on 3/30/2016.
 */
'use strict';
define(function(){
    function FlowPlayerDirective($rootScope, $timeout, Config) {
        return {
            restrict: 'AEC',
            scope: {
                instance: "@fpInstance"
            },
            templateUrl: 'views/directives/flowplayer.html',
            link: function(scope, elem, attr) {
                $rootScope.$on('playVideoObject', function(event, uploadedItem) {
                    if(uploadedItem.instance !== scope.instance)
                        return;
                    scope.flowplayerInstance = null;
                    var isPlayerStop = true;
                    var container = angular.element("#flowplayer-"+scope.instance);
                    var videoPlayer = angular.element(angular.element('#video-player-'+scope.instance)[0]);
                    if(videoPlayer.length == 1)
                        return;
                    var fpObject = {};

					container.append('<div id="video-player-'+scope.instance+'" class="learning-object-container"></div>');
                    videoPlayer = angular.element('#video-player-'+scope.instance);

                    fpObject = flowplayer(videoPlayer, {
						key: Config.fpLicense(),
						autoPlay: true,
                        native_fullscreen: true,
                        splash: true,
                        fullscreen: true,
						clip: {
							sources: [
								{
									autoPlay: true,
                                    native_fullscreen: true,
                                    fullscreen: true,
									type: "video/mp4",
									src: uploadedItem.url
								}
							]
						}
					});
                    /* Enter the video in fullscreen Mode */
                    videoPlayer[0].addEventListener('click', function(event){
                        fpObject.fullscreen();
                    });

                });

                scope.$on('rightPanelClosed', function(){
                    var container = angular.element("#flowplayer-"+scope.instance);
                    container.empty();
                    //container.remove();
                });
                scope.$on('LearningObjectClosed', function(){
                    var container = angular.element(document.querySelector("#flowplayer-"+scope.instance));
                    container.empty();
                    //container.remove();
                });

                $rootScope.$on('destroyFlowPlayer', function(){
                    var container = angular.element( document.querySelector("#flowplayer-"+scope.instance));
                    container.empty();
                    //container.remove();
                });


                /**
                 * align center - play button, loading icon
                 */

                $timeout(function(){
                    var orientationResult = '';
                    orientationResult = $(".asset-video-preview").height() + "px";
                    $(".flowplayer .fp-player").height(orientationResult);

                    window.addEventListener("orientationchange", function() {
                        orientationResult = $(".asset-video-preview").height() + "px";
                        $(".flowplayer .fp-player").height(orientationResult);
                    }, false);
                });
            }
        };
    }
    FlowPlayerDirective.$inject = ['$rootScope', '$timeout', 'Config'];//Injecting dependencies
    return FlowPlayerDirective;
});