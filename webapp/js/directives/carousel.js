/* global angular, channelsDirectory, $user_id */
'use strict';
define(function () {
	/**
	 * Carousel directive class definition
	 * @param {Service} $timeout
	 * @param {Service} ChannelsData
	 * @returns {Class}
	 */

	function Carousel($timeout, ChannelsData, $route, $window, $compile, $rootScope){
		
		return {
			scope: {
				infiniteScrollHorizontal: '&infiniteScrollHorizontal',
				disableInfinite: '=disableInfiniteBind'
			},
			restrict: 'E',
			link: function(scope, elem, attr){
				scope.items = JSON.parse(attr.data); //Parsing initial slides
				scope.updateNext = true;
			
				if(typeof attr.infiniteScrollHorizontal != 'undefined' || typeof attr.typechannel != 'undefined'){
				
					attr.$observe('data', function(value){
						scope.items = JSON.parse(value);
						if(typeof scope.swiper != 'undefined' && typeof scope.swiper.update == 'function'){
							scope.swiper.update(true);
							scope.updateNext = false;
						}
					});
				
				}
				scope.chunkSize = 12;//Chunk size for slides
				scope.elementId = attr.id;//Element id
				scope.channelID = attr.id.split("-")[1];//Channeld id
				scope.inifiniteScroll = attr.infiniteScroll;//Infinite scroll function definition
				scope.lastItemID = scope.items.length;//Last item used for requesting more by infinite scroll
				scope.disableInfinite = false;//Flag should prevent infinite scroll request
				scope.endpoint = attr.endpoint;//End point passed in attributes
				scope.show = false;
				scope.allowVerticalSwiper = attr.verticalSwiper; //Allow vertical swiper for tablet view
				var minItems = typeof attr['minimumItems'] !== 'undefined' ? attr['minimumItems'] : 6;
				
				/* Re-initialize open and close functions because of isolated scope */
				scope.open = $rootScope.open;
				/* Close three dots link menu */
				scope.close = $rootScope.close;
				/* Deeplink functionality (copy to clipboard) */
				scope.getShareurl = function($event){
					var selector = angular.element($event.currentTarget).parent().find('.copy-url');
					var selectorInitial = angular.element($event.currentTarget);
					var selectorCopied = angular.element($event.currentTarget).parent().find('.copied-success');
					scope.textToCopy = selector.attr('href');

					scope.success = function () {
						selectorInitial.hide();
						selectorCopied.show();
						$timeout(function() {
							selectorInitial.show();
							selectorCopied.hide();
						}, 2000);
					};

				};
				
				/* Change swiper direction based on width of screen and carousel type */
				scope.screenWidth = $window.innerWidth;
				
				scope.$on('$routeChangeStart', function(next, current) { 
					scope.screenWidth = $window.innerWidth;
				 });
 
				angular.element($window).bind('resize', function(){
				   scope.screenWidth = $window.innerWidth;
				});
				
				scope.$watch('screenWidth', function(newValue, oldValue){
					if(newValue > 768 && scope.allowVerticalSwiper){
						scope.swiperDirection = 'vertical';
					}else{
						scope.swiperDirection = 'horizontal';
					}
				});
				
				
				
				
				if(typeof attr.infiniteScrollHorizontal === 'undefined'){
					/**
					 * Executes ajax request to parse more items for current carousel
					 * @param {Void} updateCallback
					 * @returns {Void}
					 */
					scope.infiniteScrollHorizontal = function(updateCallback){
						if(scope.disableInfinite === true){
							//updateCallback();
							return;
						}
						ChannelsData.getChannelsData({
							endpoint: (scope.endpoint ? scope.endpoint : ChannelsData.ENUM_ENDPOINT.ITEMS),
							id_channel: scope.channelID,
							from: scope.lastItemID,
							id_user: scope.idUser,
							successCallback: function($result){
								if($result.items.length > 0){ //if there any results
									var i = 0;
									while($result.items[i]){
										scope.items.push($result.items[i]); //Push recieved items from server into existing stack
										for ( var index = 0, itemsLength = scope.items.length; index < itemsLength; ++index) {
											if ( $result.items[i].id === scope.items[index].id){
												scope.disableInfinite = true;
											}
										}
										i++;
									}
									if(scope.items.length === $result.items_count){ //If response items_count variable is equal to current items count should prevent next infinite scroll
										scope.disableInfinite = true;
									}
									scope.lastItemID = scope.items.length; //Update items count for future request
									if(!scope.$$phase){ //If scope not currently in loop trigger it
										//scope.$digest();
									}
									updateCallback();//Call the passed callback
									scope.$emit('infiniteScroll:horizontal', {channelID: scope.channelID, items: scope.items});//Emit event that infintie scroll was triggered for outside purpose's
								}else{ //Otherwsie call the passed callback and disable future infintie scroll
									updateCallback();
									scope.disableInfinite = true;
								}
							}
						});
					};
				}
				var initiliaze = function(){ //Function used to init/reinit swiper carousel on angular $digest done
					$timeout(function(){
						angular.element('[data-toggle="tooltip"]').tooltip({ trigger: "click" }); //Reinitiliazing bootstrap tooltips everytime angular $digest over
						//angular.element('[data-toggle="popover"]').popover(popoverConfig);
						angular.element('.lazyload-swiper').hide(); //Hiding lazyloading placeholders
						
						//Creating instance of the Swiper for params information check http://idangero.us/swiper/api/#.Vt_YypN95cA
						scope.swiper = new Swiper('#'+scope.elementId, {
							slidesPerView: 'auto',
							nextButton: '.swiper-button-next',
							prevButton: '.swiper-button-prev',
							//freeMode: true,
							direction: scope.swiperDirection,
							runCallbacksOnInit: false,
							observer: false,
							onResize: function(e){ return false; },
							onReachEnd: function($self){
								$self.onResize = function(){
									return false;
								}
								if($self.activeIndex == 0 && typeof attr.infiniteScrollHorizontal != 'undefined'){
									return;
								}
								//Executes when swiper reach end
								if(scope.items.length < minItems){ //If items less than 12 dont execute infinite carousel this means there are no more items on server
									return;
								}
								if(!scope.disableInfinite){
									//var lazyLoadSwipes = angular.element($self.wrapper[0]).find('.lazyload-swiper'); //Find lazy load placeholders
									//lazyLoadSwipes.show();//Show lazy load placeholders on request start
									//$self.update(true); //Update swiper so can show newly showed placeholders
									var updateCallback = function(){//Trigger request to the server
										//lazyLoadSwipes.hide();//When done hide lazy load placeholders
										$timeout(function(){
											$self.update(true);//Update swiper so can remove unneccessary placeholders
											angular.element('[data-toggle="tooltip"]').tooltip({ trigger: "click" }); //Reinitiliazing bootstrap tooltips everytime angular $digest over
											if(scope.updateNext){
												$self.slideNext(false);
											}
										});
									};
									if(typeof attr.infiniteScrollHorizontal === 'undefined'){
										scope.infiniteScrollHorizontal(function(){
											updateCallback();
										});
									}else{
										scope.infiniteScrollHorizontal({updateCallback: updateCallback});
									}
								}
							},
							onSlideChangeEnd: function($self){
								$self.update(true);
							}
						});
						scope.swiper.onResize = function(){
							return false;
						};
					});
				};
				//Calling swiper initially
				angular.element(document).ready(function(){
					initiliaze();


					//Calling swiper everytime infiniteScroll is triggered because its not native angular library and not catching dom modifications made by angular
					scope.$on('infiniteScroll:vertical', function(){
						initiliaze();
					});
				});
				scope.iCanEnter = function(item){
					return $rootScope.iCanEnter(item);
				};
				scope.$on("$destroy", function(){
					if(typeof scope.swiper.destroy === 'function'){
						scope.swiper.destroy();
					}
					elem.remove();
				});
			},
			templateUrl: function(elem,attrs) {
				return attrs.templateUrl || 'views/directives/carousel.html'
			}
		};
	}
	Carousel.$inject = ['$timeout', 'ChannelsData', '$route', '$window', '$compile', '$rootScope'];//Injecting dependencies
	return Carousel;//Returns class definition
});