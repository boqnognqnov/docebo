/* global angular */
'use strict';
define(function () {
	function SwitchView($rootScope, $location, $route, NativeAPI){
		return {
			scope: false,
			restrict: 'A',
			link: function(scope, elem, attr){
				$rootScope.bodyClass = 'prelogin-layout';
				$rootScope.showNavBar = true;
				$rootScope.ClassLeftMenu = false;
				$rootScope.ClassViews = 'prelogin-layout1';


				$rootScope.$on( "$routeChangeStart", function(event, next, current) {
				var nextRoute    = ( next ? next.$$route : '');
				var currentRoute = ( current ? current.$$route : '');
				if(typeof nextRoute != 'undefined'){
					
					switch ( nextRoute.templateUrl ) {
					case 'views/login.html'    :
						$rootScope.bodyClass = 'login-layout' + (NativeAPI.isMobile() ? ' hidden-none': '');
						$rootScope.ClassViews = 'login-layout1';
						$rootScope.showNavBar   = false;
						$rootScope.ClassLeftMenu = false;
						break;
				//    case 'views/channels.html'     :
						
					case 'views/course.html'   :
						$rootScope.showNavBar = true;
						$rootScope.ClassLeftMenu = true;
						$rootScope.bodyClass = 'courses-page';
						$rootScope.ClassViews = 'courses-layout1';
						break;
					case 'views/forgot_pass.html' :
						$rootScope.showNavBar = false;
						$rootScope.bodyClass = 'page-password';
						$rootScope.ClassViews = 'forgot-layout1';
						$rootScope.ClassLeftMenu = false;
						break;
					case 'views/channels.html' :
					case 'views/myChannel.html' :
					case 'views/upload_queue.html' :
					case 'views/upload_history.html' :
						$rootScope.showNavBar = true;
						$rootScope.ClassViews = 'home-layout1';
						$rootScope.bodyClass = 'with-tabs';
						$rootScope.ClassLeftMenu = false;
						break;
					default : 
						$rootScope.showNavBar = true;
						$rootScope.ClassViews = 'home-layout1';
						$rootScope.bodyClass = 'home-page';
						$rootScope.ClassLeftMenu = false;
						break;
				}
				}
			});
		}
		};
	}
	SwitchView.$inject = ['$rootScope', '$location', '$route', 'NativeAPI'];//Injecting dependencies
	return SwitchView;//Returns Class definition
});