'use strict';
define(function () {
	function NavbarDirective($rootScope, NativeAPI, Storage, $location, GlobalSearch, URLManager, PlayerStore){
		return {
			restrict: 'AE',
            controller: 'AppController',
			templateUrl: URLManager.createViewUrl('views/directives/navbar.html'),
			link: function(scope, elem, attr){
				/* open/close left sidebar menu */
				scope.$onButton1Click = function() {
					$(".panel-overlay").css('z-index', '-9999');
					scope.el_sidemenu =  angular.element( angular.element( '.hidden-left-menu' )[0] );
					scope.el_view = angular.element(  angular.element( '.views' )[0] );
					scope.el_sidemenu.removeClass('hidden');
					scope.el_view.toggleClass('right-slide');
					NativeAPI.dispatch('rightPanelState', "open");
					if ( Storage.get('mobile_info') !== null && typeof Storage.get("mobile_info") != 'undefined' ) {
						scope.$emit('checkWalkthrough', scope.skipWalkthrough);
						$rootScope.$broadcast('mobileInfoSaved');
					}
				};

				/**
				 * check is search icon to be visible
				 * @returns {boolean}
                 */

				scope.$isSearchIconVisible = function () {
					if( PlayerStore.isReady()  || $location.$$url.indexOf("/search") >= 0){
						return false;
					} else {
						return true;
					}
				};

				/**
				 * open search field
				 */

				scope.$openGlobalSearch = function() {
					document.getElementById("search-wrapper").style.display = 'block';
					document.getElementById("search-autocomplete-dropdown").focus();
                    NativeAPI.dispatch('isGlobalSearchLoaded', "true");
				};
			}
		};
	}
	NavbarDirective.$inject = ['$rootScope', 'NativeAPI', 'LocalStorage', '$location', 'GlobalSearch', 'URLManager', 'PlayerStore'];
	return NavbarDirective;
});