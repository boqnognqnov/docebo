'use strict';
define([], function () {
	function ModalDirective($timeout){
		return {
			restrict: 'AE',
			templateUrl: 'views/directives/modal_window.html',
			scope: {},
			controller: "@",
			name: "controllerName",
			link: function(scope, elem, attr){
				var Modals = new Framework7();
				var $$ = Dom7;
				
				var $modalWin = $$('.view-asset-page .modal-popup').detach();
				scope.$on('ModalOpened', function(event, asset_id) {
					scope.idAsset = asset_id;
					$$('body').append($modalWin);
					Modals.popup('.modal-window');
					angular.element('.modal-overlay').removeClass('modal-overlay-visible');
				});
				
				scope.$on('ModalClosed', function(event) {
					Modals.closeModal('.modal-window');
				});
			}
		};
	};
	ModalDirective.$inject = ['$timeout'];//Injecting dependencies
	return ModalDirective;//Returns class definition
});