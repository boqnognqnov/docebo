'use strict';
define([], function () {
   function PercentageTracker($timeout, $interval) {
       return {
           templateUrl: 'views/directives/percentage_tracker.html',
           restrict: 'AE',
           scope:   {
                value:    '@percentage',
                showText: '@' //@string if (true) show text inside the circle
           },
           link: function (scope, element, attr) {
               /**
                * Calculate the visual part of the circle percentage based on the value
                * @param progress
                */
           scope.renderPercentage = function (progress) {
               if ( progress === 100 ){
                   $interval.cancel(intervalId);
               }
               var angle = 0;
               var $$ = angular.element;

               progress = Math.floor(progress);
               if(progress<25){
                   angle = -90 + (progress/100)*360;
                   $$(".animate-0-25-b").css("transform","rotate("+angle+"deg)");
               }
               else if(progress>=25 && progress<50){
                   angle = -90 + ((progress-25)/100)*360;
                   $$(".animate-0-25-b").css("transform","rotate(0deg)");
                   $$(".animate-25-50-b").css("transform","rotate("+angle+"deg)");
               }
               else if(progress>=50 && progress<75){
                   angle = -90 + ((progress-50)/100)*360;
                   $$(".animate-25-50-b, .animate-0-25-b").css("transform","rotate(0deg)");
                   $$(".animate-50-75-b").css("transform","rotate("+angle+"deg)");
               }
               else if(progress>=75 && progress<=100){
                   angle = -90 + ((progress-75)/100)*360;
                   $$(".animate-50-75-b, .animate-25-50-b, .animate-0-25-b")
                       .css("transform","rotate(0deg)");
                   $$(".animate-75-100-b").css("transform","rotate("+angle+"deg)");
               }
                /* Show the text inside the green percentage circle*/
                if ( scope.showText === 'true' ){
                    $$(".text").html(progress);
                    $$(".percentage-single").html("%");
                    $$(".percentage-multi").html("%");
                }
           };
               /**
                * Fire the rendering of the percentage circle
                */
            angular.element(document).ready(function () {
                $timeout(function () {
                   scope.renderPercentage(scope.value);
                }, 300);
            });

               var intervalId = $interval(function () {
                   scope.value = scope.value.replace(/[^\w\s]/gi, '');
                   if ( scope.value == 100) {
                        $interval.cancel(intervalId);
                   }
                   if ( parseInt(scope.value) != parseInt(0)){
                       scope.renderPercentage(scope.value);
                   }

               }, 300);

               scope.$watch(scope.value, function (nv, ov) {
                   if ( parseInt(nv) === parseInt(100)){
                       $interval.cancel(intervalId);
                   }
                  if ( nv != ov){
                      if (nv === 0){
                          $interval.cancel(intervalId);
                      }
                      scope.value = nv;
                  }
               });
               element.on('$destroy', function() {
                   $interval.cancel(intervalId);
               });
               scope.$on('rightPanelClosed', function(){
                   $interval.cancel(intervalId);
               });

           }

       };
   }
   PercentageTracker.$inject = ['$timeout', '$interval'];
    return PercentageTracker;
});