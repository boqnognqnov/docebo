/**
 * Created by kyuchukovv on 3/30/2016.
 */
'use strict';
define(function(){
    function TableView($timeout, $location) {
        return {
            templateUrl: 'views/directives/table_view.html',
            restrict: 'EA',
            scope: {
                items: '=',
                tapAction: '&onTap',
				rowView: '@subView'
            },
			link: function(scope, elem){
				scope.$on('scrollTableViewBottom', function(){
                    var pageContent = angular.element('.page-content');
                    pageContent[0].scrollTop = pageContent[0].scrollHeight;
				});
			}
        };
    }
    TableView.$inject = ['$timeout', '$location'];
    return TableView;
});