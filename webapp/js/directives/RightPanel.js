/* global angular, channelsDirectory, $user_id */
'use strict';
define([], function () {
	/**
	 * Swiping walkthrough directive class definition
	 * @param {Service} $timeout
	 * @returns {Class}
	 */
	function RightPanel($timeout, $rootScope, $location, NativeAPI){
		return {
			restrict: 'AE',
			templateUrl: 'views/directives/rightPanel.html',
			scope: {},
			controller: "@",
			name: "controllerName",
			link: function(scope, elem, attr){
				
				/* close rigth panel - workaround, as default framewrok7 function not working properly */
				var bodyElem = $('body');
				scope.closePanels = function(){
					if(bodyElem.hasClass('with-panel-right-cover')){
						bodyElem.removeClass('with-panel-right-cover');
					}
					scope.$broadcast('rightPanelClosed');
				};

				/**
				 * set visibility to navBar rightPanel
				 * @returns {boolean}
				 */

				scope.$isSearchIconVisiblePanel = function () {
					if( $location.$$url.indexOf("/learning_object") > 0 || $location.$$url.indexOf("/uploadQueue") == 0
						|| $location.$$url.indexOf("/search") >= 0 || $location.$$url.indexOf("/course") >= 0 ) {
						return false;
					} else {
						return true;
					}
				};

				/**
				 * open search field
				 */

				scope.$openGlobalSearchPanel = function() {
					scope.$emit('enableLoading');
					document.getElementById("search-wrapper").style.display = 'block';
					document.getElementById("search-autocomplete-dropdown").focus();
					NativeAPI.dispatch('isGlobalSearchLoaded', "true");
				};

				scope.$on("closeSearchFilter", function (event, data) {
					scope.closePanels();
				});
				
				//Fixed header bar
				angular.element('.content-asset-block').css('height', angular.element(window).height()-52);
			}
		};
	};
	RightPanel.$inject = ['$timeout', '$rootScope', '$location', 'NativeAPI'];//Injecting dependencies
	return RightPanel;//Returns class definition
});