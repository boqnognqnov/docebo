/* global angular */
'use strict';
define(function () {
	/**
	 * Lazy loading directive class definition
	 * @param {Service} $timeout
	 * @param {Service} $document
	 * @param {Service} $window
	 * @returns {Class}
	 */
	function LazyLoadChannel($timeout, $document, $window){
		return {
			templateUrl: 'views/directives/lazyload.html',
			scope: true,
			restrict: 'E',
			link: function(scope, elem, attr){
				var bootstrap = function(){
					var document = $document[0];
					var hasVScroll = document.body.scrollHeight < document.body.clientHeight;
					if(!hasVScroll){
						angular.element($window).trigger('scroll');
					}
				};
				bootstrap();
				scope.$on('infiniteScroll:vertical', function(){
					bootstrap();	
				});
				scope.type = attr.type;//Binding type of lazyloader to scope
				scope.$on('showLazyLoad', function(){//Event listener to show lazy loading placeholders
					angular.element(elem).show();
					angular.element('.page-content').animate({scrollTop: elem.offset().bottom}, "slow");
				});
				scope.$on('hideLazyLoad', function(){//Event listener to hide lazy loading placeholders
					$timeout(function(){
						angular.element(elem).hide();
					});
				});
			}
		};
	}
	LazyLoadChannel.$inject = ['$timeout', '$document', '$window'];//Injecting dependencies
	return LazyLoadChannel;//Returns Class definition
});