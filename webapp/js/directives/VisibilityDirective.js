/* global angular */
'use strict';
define(function () {
	/**
	 * Visibility checboxes directive class definition
	 * @param {Service} $timeout
	 * @returns {Class}
	 */
	function VisibilityDirective($timeout){
		return {
			templateUrl: 'views/directives/visibility.html',
			restrict: 'E',
			scope: {
				items: '=',
				visible: '=visibilityModel'
			},
			controller: "@",
			name: "controllerName",
			link: function(scope, elem, attr) {
				/* Add class selected on active radio button (visibility) */
				$timeout(function(){
					var selectedElem = angular.element('input[type=radio]:checked');
					selectedElem.parent().addClass('selected');
				});
				
				/* on row click check/uncheck radio button */
				scope.$selectVisibility = function($event){
					var selector = angular.element($event.currentTarget);
					var data = selector[0]['attributes']['data']['value'];
					scope.visible = 0;

					if ( data.indexOf("item_1") > -1 ) {
						scope.visible = 1;
					} else {
						scope.visible = 0;
					}
					$('.details-asset li').removeClass('selected');
					selector.addClass('selected');
				};
			}
		};
	};
	VisibilityDirective.$inject = ['$timeout'];//Injecting dependencies
	return VisibilityDirective;//Returns class definition
});