'use strict';
define(function () {
	function TabsbarDirective($route, $parse, NativeAPI, URLManager){
		return {
			restrict: 'E',
			templateUrl: URLManager.createViewUrl('views/directives/tabsbar.html'),
			scope: {
				items: "="
			},
			link: function(scope, elem, attr){
				scope.isIphone = NativeAPI.device() == NativeAPI.OS.iOS ? true : false;
				scope.$active = 0;
				scope.$route = $route;
				scope.$watch("$route.current.$$route.activeTab", function(v){
					if(typeof v != 'undefined'){
						scope.$active = v;
					}
				});
				scope.$on('$routeChangeSuccess', function(event, next){
					if(typeof next.$$route != 'undefined' && typeof next.$$route.activetab != 'undefined'){
						scope.$active = next.$$route.activetab;
					}
				});
			}
		 };
	}
	TabsbarDirective.$inject = ['$route', '$parse', 'NativeAPI', 'URLManager'];
	return TabsbarDirective;
});