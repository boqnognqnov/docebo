'use strict';
define(function () {
    function LoadingDirective($http) {
        return {
         restrict: 'E',
		 template: '<div class="loading-spinner-background"><div class="material-icon"><div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active is-upgraded"></div></div></div>',
            link: function (scope, elm, attrs) {
				scope.disable = false;
				elm.css({"display" : "block"});
				/* Events listeners */
				scope.$on("loading-started", function(e) {
					//console.log("HERE");
					elm.css({"display" : "block"});
				});

				scope.$on("loading-complete", function(e) {
					elm.css({"display" : "none"});
				});
				
				scope.$on('disableLoading', function(){
					scope.disable = true;
					scope.$broadcast('loading-complete');
				});
				
				scope.$on('enableLoading', function(){
					scope.disable = false;
				//	scope.$broadcast('loading-started');
				});
				scope.isLoading = function () {
                    return $http.pendingRequests.length > 0;
                };
				
				 scope.$watch(scope.isLoading, function (v) {
					if(!scope.disable){
						if (v) {
							scope.$broadcast('loading-started');
						} else {
							scope.$broadcast('loading-complete');
						}
					}
                });
            }
        };
    }
	LoadingDirective.$inject = ['$http'];//Injecting dependencies
    return LoadingDirective;
});