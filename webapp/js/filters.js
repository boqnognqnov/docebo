'use strict';
define(function (require) {
	var module = require('angular').module('DoceboMobile.filters', []);
	module.filter('range', function() {
		return function(input, total) {
		  total = parseInt(total);

		  for (var i=0; i<total; i++) {
			input.push(i);
		  }
		  return input;
		};
	})
	.filter('cut', function(){
		return function (value, wordwise, max, tail) {
			if (!value) return '';
			max = parseInt(max, 10);
			if (!max) return value;
			if (value.length <= max) return value;
			value = value.substr(0, max);
			if (wordwise) {
				var lastspace = value.lastIndexOf(' ');
				if (lastspace !== -1) {
					value = value.substr(0, lastspace);
				}
			}
			return value + (tail || ' …');
		};
	})
	.filter('html', ['$sce', function ($sce) { 
		return function (text) {
			return $sce.trustAsHtml(text);
		};
	}]);
	return module;
});