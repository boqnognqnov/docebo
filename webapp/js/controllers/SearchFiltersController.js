/* global angular */
define(function () {
	/**
	 *
     * @param $scope
	 * @param $rootScope
	 * @param $translate
	 * @param GS
	 *
	*/
	function SearchFilterController($scope, $rootScope, $translate, GS, NativeAPI){
		$scope.view = 'views/search_filters.html';
		$scope.headertitle = '';
		$scope.translations = {};
		$scope.options = [];
		$scope.filterTypeIds = [];
		$scope.filterTypeIdsCopy = [];

		$translate(['Courses', 'Learning Plans', 'Knowledge Assets', 'Learning Objects', 'Filter search results']).then(function (result) {
			$scope.translations = result;
			$scope.headertitle  = $scope.translations['Filter search results'];
			$scope.options = [
				{name: $scope.translations['Courses'], value: GS.ENUM_TYPES.EsAgentCourse},
				{name: $scope.translations['Learning Plans'], value: GS.ENUM_TYPES.EsAgentPlan},
				{name: $scope.translations['Learning Objects'], value: GS.ENUM_TYPES.EsAgentLo},
				{name: $scope.translations['Knowledge Assets'], value: GS.ENUM_TYPES.EsAgentKnowledgeAsset}
			];
		});

        /**
		 * mark all items
		 * @param items
         */

		$scope.markAllItems = function (items) {
			if ($scope.filterTypeIds.length == 0 || $scope.filterTypeIds.length > 0 && $scope.filterTypeIds.length < $scope.options.length) {
				$scope.filterTypeIds = [];
				for (var i = 0; i < items.length; i++) {
					if (items[i].name != null) {
						$scope.filterTypeIds.push((items[i].value));
						document.getElementById('mark-all-search').className = 'row-search-selected';
					}
				}
			} else {
				$scope.filterTypeIds = [];
				document.getElementById('mark-all-search').className = 'row-search-not-selected';
			}
		};


		/**
		 * mark selected element
		 * @param value
		 * @returns {boolean}
		 */

		$scope.isChecked = function (value) {
			var index = $scope.filterTypeIds.indexOf((value));
			if(index > -1){
				return true;
			}
			return false;
		};


		/**
		 * mark single item
		 * @param index
		 */

		$scope.markSelectedItem = function (index) {
			var value = angular.element("#select_search_"+index).attr('data-value'); // get id on current item
			$scope.filterTypeIds = [];
			$scope.filterTypeIds.push((value));
			document.getElementById('mark-all-search').className = 'row-search-not-selected';
		};


		/**
		 * save current type ids and close the modal
         */

		$scope.applyBtn = function () {
			$scope.filterTypeIdsCopy = [];
			$scope.filterTypeIdsCopy = $scope.filterTypeIds.concat();
			$rootScope.$broadcast("filterTypeIds", $scope.filterTypeIds);
			$scope.closePanels();
		};


		/**
		 * didn't save filterTypeIds if user didn't clicked on confirm button
		 */

		$scope.closeFilterPage = function () {
			$scope.filterTypeIds = [];
			$scope.filterTypeIds = $scope.filterTypeIdsCopy.concat();

			//check state of allTypes button
			if ($scope.options.length == $scope.filterTypeIds.length) {
				document.getElementById('mark-all-search').className = 'row-search-selected';
			} else {
				document.getElementById('mark-all-search').className = 'row-search-not-selected';
			}
		};


		/**
		 * check if user is clicked on back button and trigger $scope.closeFilterPage
         */

		$scope.$on('rightPanelClosed', function(){
			$scope.closeFilterPage();
		});

		/**
		 * clear filter when user is not in search functionality
		 */

		$scope.$on('clearSearchFilter', function(){
			$scope.filterTypeIds = [];
			$scope.filterTypeIdsCopy = [];
		});

		NativeAPI.registerFunc('closePanels', function() {
			$scope.closePanels();
		});
	}
	SearchFilterController.$inject = ['$scope', '$rootScope', '$translate', 'GlobalSearch', 'NativeAPI'];//Injecting dependencies
	return SearchFilterController;//Return Class definition
});