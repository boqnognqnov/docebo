/* 
 * This is controller for testing things here
 */


/* global angular */
define(function () {
	function TestController($scope, URLManager, NativeAPI){}
	TestController.$inject = ['$scope', 'URLManager', 'NativeAPI'];//Injecting dependencies
	return TestController;//Return Class definition
});