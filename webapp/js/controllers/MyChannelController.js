define(function () {

	function MyChannelController($scope, $q, $http, ChannelsData, HTMLDispatcher, Storage, $rootScope, $translate) {
		var translateTitle = 'My channel';
		$translate(translateTitle).then(function (result) {
			translateTitle = result;
			HTMLDispatcher.ChangeTitle(translateTitle);
		});
		
		$scope.channelMyContribution = [];
		$scope.channelMyInReview = [];
		$scope.channelData = null;
		$scope.canRequestMore = true;
		/* My Contributions */
		$scope.getMyChannelData = function () {
			ChannelsData.getChannelsData({
				endpoint: ChannelsData.ENUM_ENDPOINT.USER_CHANNELS,
				count: {
					inReviewCount: 8,
					myContributionsCount: 12
				},
				successCallback: function ($result) {
					$scope.channelData = $result.channel;
					/* Update current user avatar */
					$scope.channelData.other_fields.avatar = Storage.get('mobile_info').avatar;
					
					$result.channel.items.forEach(function(data){
						if(data.other_fields.status === 10 || data.other_fields.status === 15){
							$scope.channelMyInReview.push(data);
						}
						
						if(data.other_fields.status === 18 || data.other_fields.status === 20){
							$scope.channelMyContribution.push(data);
						}
					});
				}
			});
		};
		
		$scope.showDescription = function(){
			$('#descriptionInput').show();
		};
		
		$scope.updateDescription = function($event, oldDescription){
			var keyCode = $event.which || $event.keyCode;
			var el = $($event.currentTarget);
			if (keyCode === 13 || $event.type === 'blur') {
				if ( el.html() === oldDescription ){
					return;
				}
				$event.preventDefault();
				el.html(el.text());
				el.attr('contenteditable', 'false');

				ChannelsData.updatePersonalChannelDescription(el.text());
			}
		};
		$scope.switchToEditable = function($event){
			var el = $($event.currentTarget);
			$scope.oldDescription = el.html();
			if(el.attr('contenteditable') == 'false'){
				el.attr('contenteditable', 'true');
				el.focus();
			}
		};

		$scope.init = function () {
			HTMLDispatcher.ChangeTitle('My Channel');
			$scope.getMyChannelData();
		};
		
		$scope.infiniteScrollVertical = function(){
			if($scope.canRequestMore === false){
				return;
			}
			ChannelsData.getChannelsData({
				endpoint: ChannelsData.ENUM_ENDPOINT.USER_CHANNEL_MY_CONTRIBUTION,
				from: $scope.channelMyContribution.length,
				count: 12,
				successCallback: function($response){
					$response.items.forEach(function(item){
						$scope.channelMyContribution.push(item);
					});
					if($response.items_count === $scope.channelMyContribution.length || $response.items.length === 0){
						$scope.canRequestMore = false;
					}
				}
			});
			$scope.$emit("infiniteScroll:vertical");
		};
		$translate(['All Channels', 'My Channel']).then(function(translations){
			$scope.$tabs = [
				{title: translations['All Channels'], action: '#/all', icon: 'icon-d_thumbs'},
				{title: translations['My Channel'], action: '#/myChannel', icon: 'icon-d_user'}
			];
		});
		$rootScope.getTabs = function(){
			return $scope.$tabs;
		};
	}
	MyChannelController.$inject = ['$scope', '$q', '$http', 'ChannelsData', 'HTMLDispatcher', 'LocalStorage', '$rootScope', '$translate'];
	return MyChannelController;
});