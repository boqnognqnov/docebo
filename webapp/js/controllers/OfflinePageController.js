define(function () {
	var statusCheck = false;
	var opened = false; // OfflinePage right panel is opened

    /**
     *
     * @param $scope
     * @param $translate
     * @param $rootScope
     * @param NativeAPI
     * @param Queue
     * @param OfflineQueueItem
     * @param OfflinePlayerService
     * @param $interval
     * @param $http
     * @param Utils
     * @constructor
     * @param CacheService
	 * @param CourseStore - Course store init with idCourse
     *
     * Logic HERE for Offline Course Download Page
     */
    function OfflinePageController($scope, $translate, $rootScope, NativeAPI, Queue, OfflineQueueItem, OfflinePlayerService, $interval, $http, Utils, CacheService, CourseStore) {
        /*const*/
        var STOP_DOWNLOAD_BUTTON  = 'stop-download-offline-courses';
        var START_DOWNLOAD_BUTTON = 'download-offline-courses';
        var CHAPTER_AVAILABLE     = 'available';
        var CHAPTER_DOWNLOADING   = 'downloading';
        var CHAPTER_WAITING       = 'waiting';
        var CHAPTER_DOWNLOADED    = 'downloaded';
        var CHAPTER_UNAVAILABLE   = 'unavailable';

        var STOP_DOWNLOAD_TEXT    = '';
        var START_DOWNLOAD_TEXT   = '';

        var $$ = Dom7; // Init Dom7 lib
        var multipleDownload;
        var intervalId;
        //Assign a view to display in rightPanel.html
        $scope.view = 'views/offlinePage.html';
        //Access navbar color and font color from rootScope
        $scope.navColor = $rootScope.navColor;
        $scope.navFont = $rootScope.navFont;

        $scope.offline = [];
        $scope.headertitle = '';
        $scope.idCourse    = 0;
        $scope.courseName  = '';
		$scope.queue = Queue.getQueue();
		$scope.checkInterval = null;
        $scope.checkProgress = null;
		$scope.jobHashId = '';
        /* TRANSLATE */
        $translate(['Download', 'Stop Download', 'Download all course chapters']).then(function (res) {
           $scope.headertitle   = res['Download'];
            STOP_DOWNLOAD_TEXT  = res['Stop Download'];
            START_DOWNLOAD_TEXT = res['Download all course chapters']
        });
        $translate(['Warning!', 'If you stop the download, you cannot resume it later, and you will have to start over.', 'Cancel', 'Stop Download', 'Remove downloaded object', 'Open downloaded object']).then(function(translations){
            $scope.popupActions = {
                title: 		translations['Warning!'],
                message:    translations['If you stop the download, you cannot resume it later, and you will have to start over.'],
                buttonCancel: translations['Cancel'],
                buttonText: translations['Stop Download']
            };
            $scope.removeObjectText = translations['Remove downloaded object'];
            $scope.openObjectText   = translations['Open downloaded object'];
        });
        /* END TRANSLATE */
        /**
         * Event listener for opened offline page
         * @data is an object with {
         * (string)     courseName,
         * (int)        idCourse,
         * (object)     courseChapters
         */
        $scope.$on('offlinePageOpened', function (event, data){
        	opened = true;
			$scope.offlineChapters = {};
			Queue.clearQueue();
			$scope.initQueue(data.courseChapters);
			$scope.queue = Queue.getQueue();
			statusCheck = false;
				$scope.$watch(function(){ return window["getDownloadedObjects"] }, function(v){
					if(typeof v != 'undefined' && opened){
						$scope.courseName       = data.courseName;
						$scope.idCourse         = data.idCourse;
						$scope.assignActions(data.courseChapters);
						$scope.downloadedChapters = JSON.parse(v).split(", ").map(Number);

						$scope.initQueue(data.courseChapters);
						$scope.queue = Queue.getQueue();
						if ( typeof $scope.offlineChapters === 'undefined' || (typeof $scope.offlineChapters === 'object' && Object.keys($scope.offlineChapters).length === 0)){
							$scope.offlineChapters  = $scope.queue;
							$scope.offlineChapters.sumSize = -1;
						}

						$scope.$emit('disableLoading');
						if(!statusCheck && CourseStore.id() === $scope.idCourse){
							OfflinePlayerService.checkDownloadStatus($scope.idCourse).then(function(data){
								if(data.scheduler_run == true){
									if(typeof data.job_hash != 'undefined'){
										$scope.jobHashId = data.job_hash;
									}
									if ($scope.checkInterval === null) {
										$scope.checkInterval = $interval(function () {
											$scope.checkArchiveStatus();
										}, 5000);
									}
								}else{
									$scope.checkArchiveStatus();
								}

							});
						}
						statusCheck = true;
					}
				});
        });
		$scope.initQueue = function(chapters){
			if(chapters.length > 0){
				angular.forEach(chapters, function(obj, index){
                    var item = OfflineQueueItem.create();
					item.id = obj.id;
					item.id_org = obj.id_org;
                    item.idScormItem = obj.idScormItem ? obj.idScormItem: 0;
					item.state = obj.state;
					item.title = obj.title;
					item.size = obj.size;
                    item.progress = (obj.progress ? parseInt(obj.progress) : parseInt(0));
					item.actions = obj.actions;
                    if ( item.state === item.STATES.AVAILABLE && Queue.getQueue().length < chapters.length){
						if(typeof $scope.downloadedChapters !== 'undefined' && $scope.downloadedChapters.indexOf(item.id_org) != -1){
							item.state = item.STATES.DOWNLOADED;
						}
                        Queue.push(item);
                    }
				});
			}
		};
		$rootScope.$on("logout", function(){
			Queue.clearQueue();
			$scope.offlineChapters = [];
			statusCheck = false;
		});
		$scope.checkArchiveStatus = function(){
			OfflinePlayerService.materialsDataPolling($scope.idCourse, $scope.jobHashId).then(function(data){
				//if(data.allDone == true){
				if(typeof data.loList != 'undefined'){
					var $total = 0;
					angular.forEach($scope.offlineChapters, function (obj, index) {
						var current = data.loList[obj.id];
						$scope.offlineChapters[index]['size'] = Utils.humanFileSize(current['size']);
						$scope.offlineChapters[index]['path'] = current['path'];
						$total = $total+current['size'];
					});
					if(data.schedular_done == true){
						$scope.offlineChapters.sumSize = Utils.humanFileSize($total);
						delete $total;
						if($scope.checkInterval != null){
							$interval.cancel($scope.checkInterval);
						}
					}
				}
			});
		};
        /**
         *
         */
        $scope.stopBulkDownload = function(){
            var stopDownloadButton = document.getElementById('stop-download-offline-courses');
            if (stopDownloadButton){
                stopDownloadButton.id = START_DOWNLOAD_BUTTON;
                stopDownloadButton.innerText = START_DOWNLOAD_TEXT;

                if ( multipleDownload){
                    clearInterval(multipleDownload);
                }
                changeChaptersState(1, 0); // Stop all downloading chapters
                changeChaptersState(2, 0); // Stop all waiting chapters

                NativeAPI.dispatch("cancelLoDownload", ""); // stop current download from native device
                $scope.clearDownloadedProgress();
            }
        };
        /**
         *
         * @param event
         */
        $scope.startBulkDownload = function (event){
            var button = event.currentTarget;
            var id = button.id;
            if (id && id === START_DOWNLOAD_BUTTON){
                event.currentTarget.id = STOP_DOWNLOAD_BUTTON;
                button.innerText = STOP_DOWNLOAD_TEXT;
                changeChaptersState(0, 2); // SET ALL AVAILABLE ELEMENTS TO WAITING
                $scope.$broadcast('offlineDownloadStarted');
                $scope.clearDownloadedProgress();
            }
            if (id && id === STOP_DOWNLOAD_BUTTON){
                angular.element('#stop-chapters-download').trigger('click');
                var stopDownloadModal = document.getElementsByClassName("modal-button-bold")[0];
                stopDownloadModal.addEventListener('click', $scope.stopBulkDownload, false);
                $scope.clearDownloadedProgress();
            }

            if ( !Queue.isProccessing()){
                $scope.process();
            }
        };
        /**
         * Change the states of all chapters if they are matching the current state
         *
         * @param state - current state
         * @param newState - desired state
         */
        var changeChaptersState = function(state, newState){
                for ( var i=0; $scope.offlineChapters.length; i++ ){
                    currentChapter = $scope.offlineChapters[i];
                    if (typeof currentChapter === 'undefined'){ break; }
                    /* Change the states */
                    if (currentChapter.state === state ){
                        currentChapter.state = newState;
                    }
                }
        };
        /**
         *
         */
        $scope.$on('offlineDownloadStopped', function () {

        });
        /**
         *
         * @param state
         * @returns {*}
         */
        $rootScope.getChapterIcon = function(state){
            if ( state.length < 2){
                return '';
            }
            switch (state){
                case CHAPTER_AVAILABLE: return 'icon-download'; break;
                case CHAPTER_DOWNLOADED: return 'icon-check'; break;
                case CHAPTER_DOWNLOADING: return 'icon-close'; break;
                case CHAPTER_WAITING: return 'icon-close'; break;
                default: return ''; break;
            }
        };
        /**
         * show free storage from native device
         */
        $scope.$on("freeStorage", function (event, data){
            $scope.freeStorage = data;
        });
        /**
         *
         * @param index
         */
        $scope.openMenu = function (index) {
            var chapters = $scope.offlineChapters;
            $scope.targetChapter = chapters[Object.keys(chapters)[index]];
            $scope.clearDownloadedProgress();
            switch ($scope.targetChapter.state){
                case $scope.targetChapter.STATES.AVAILABLE:
                    $scope.targetChapter.state = $scope.targetChapter.STATES.DOWNLOADING;

                    var item = OfflineQueueItem.create();
                    var downloadedFile = '';
                    item.id          = $scope.targetChapter.id;
                    item.id_org      = $scope.targetChapter.id_org;
                    item.idScormItem = $scope.targetChapter.idScormItem ? $scope.targetChapter.idScormItem: 0;
                    item.state       = $scope.targetChapter.state;
                    item.title       = $scope.targetChapter.title;
                    item.size        = $scope.targetChapter.size;
                    item.progress    = ($scope.targetChapter.progress ? parseInt($scope.targetChapter.progress) : parseInt(0));
                    item.actions     = $scope.targetChapter.actions;

                    // Queue.push(item);
                    if ( !Queue.isProccessing()){
                        NativeAPI.dispatch('startDownload', {
                            targetPath: $scope.targetChapter.path,
                            courseId: $scope.idCourse,
                            itemId: item.id_org,
                            loId: item.id,
                            idScormItem: item.idScormItem,
							userId: $rootScope.userIdst,
							title: item.title
                        });
                    }
                    intervalId = setInterval(function () {
						var progress = 0;
                       if(NativeAPI.device() != NativeAPI.OS.iOS){
						   progress = NativeAPI.dispatch('getDownloadProgress', '');
						}else{
							NativeAPI.dispatch('getDownloadProgress', '');
						   progress = NativeAPI.getBridgeResponse('getDownloadProgress');
						}
						if ( typeof progress != 'undefined' && progress != 'undefined')
							$scope.targetChapter.progress = JSON.parse(progress);
                    }, 300);
					$scope.$watch('targetChapter.progress', function(v){
						if(NativeAPI.device() != NativeAPI.OS.iOS){
							var progress = typeof v != 'undefined' ? v : 0;
						}else{
							var progress = typeof v != 'undefined' ? JSON.parse(v) : 0;
						}

						if ( parseInt(progress) >= 100){
                            $scope.targetChapter.state = item.STATES.DOWNLOADED;
                            $scope.targetChapter.progress = "0";
                            $scope.clearDownloadedProgress();
                            downloadedFile = Queue.getItemByCriteria('id', item.id);

                            // Queue.removeItem(downloadedFile);
                            Queue.setFreeOfProcessing();
                            clearInterval(intervalId);
                        } else if(parseInt(progress) == 0)  {
                            $scope.clearDownloadedProgress();
                        }
					});

                    break;
                case $scope.targetChapter.STATES.DOWNLOADING:
                case $scope.targetChapter.STATES.WAITING:
                    $scope.targetChapter.progress = "0";
                    $scope.clearDownloadedProgress();
                    NativeAPI.dispatch("cancelLoDownload", ""); // stop current download from native device
                    $scope.targetChapter.state = $scope.targetChapter.STATES.AVAILABLE;
                    break;
                case $scope.targetChapter.STATES.DOWNLOADED:
                    console.log("DOWNLOADED");
                    break;
                case 'locked' :
                    console.log("locked");
                    break;
                case $scope.targetChapter.STATES.NOT_DOWNLOADABLE :
                    $scope.targetChapter.state = $scope.targetChapter.STATES.NOT_DOWNLOADABLE; break;
            }

        };
        /**
         *
         * @param item
         * @returns {Array}
         */
        $scope.getActions = function(){
            var actions = [];
            var actionText = $scope.removeObjectText;

            actions.push({
                text: $scope.openObjectText,
                color: 'green',
                onClick: viewObject
            });
            actions.push({
                text: actionText,
                color: 'red',
                onClick: removeObject
            });

            return actions;
        };
        var viewObject = function(){
          console.log('lol');
        };
        var removeObject = function () {
            console.log("deleted");
        };
        /**
         *
         * @param chapters
         */
        $scope.assignActions = function(chapters){
            var actions = $scope.getActions();
            for (var i = 0; i < chapters.length; i++){
                chapters[i].actions = actions;
            }

            return chapters;
        };
		/**QUEUE**/
		$scope.pushInQueue = function(object){
			Queue.push(object);
			if(Queue.getQueue().length > 8){
				$scope.$broadcast('scrollTableViewBottom');
			}
			$scope.$digest();
		};
		$scope.removeFromQueue = function (index) {
            Queue.remove(index);
        };

		$scope.process = function() {
            if (!Queue.isProccessing()) {
				var nextFromQueue = Queue.pullNextFile();
				if (typeof nextFromQueue !== 'undefined' && nextFromQueue) {
					$scope.targetChapter = nextFromQueue;
				    Queue.setToBusy();

                    var itemId = $scope.targetChapter.id_org;
                    var itemPath = $scope.targetChapter.path;
                    var itemLoId =  $scope.targetChapter.id;
                    var idScormItem = $scope.targetChapter.idScormItem ? $scope.targetChapter.idScormItem: 0;
                    var title = $scope.targetChapter.title;

                    NativeAPI.dispatch('startDownload', {
                        targetPath: itemPath,
                        courseId: $scope.idCourse,
                        itemId: itemId,
                        loId: itemLoId,
                        idScormItem: idScormItem,
                        userId: $rootScope.userIdst,
                        title: title
                    });


                    multipleDownload = setInterval(function () {
						if(NativeAPI.device() != NativeAPI.OS.iOS){
							$scope.targetChapter.progress = NativeAPI.dispatch('getDownloadProgress', '');
						}else{
							NativeAPI.dispatch('getDownloadProgress', '');
							$scope.targetChapter.progress = NativeAPI.ios.iosResponse('getDownloadProgress');
						}
                        if(typeof $scope.targetChapter.progress != 'undefined') {
                            $scope.targetChapter.progress = $scope.targetChapter.progress.replace(/[^\w\s]/gi, '');
                        }

                        if(NativeAPI.device() != NativeAPI.OS.iOS){
                            var progress = typeof $scope.targetChapter.progress != 'undefined' ? $scope.targetChapter.progress : 0;
                        }else{
                            var progress = typeof $scope.targetChapter.progress != 'undefined' ? JSON.parse($scope.targetChapter.progress) : 0;
                        }
                        if ( parseInt(progress) >= 100){
                            $scope.targetChapter.state = $scope.targetChapter.STATES.DOWNLOADED;
                            Queue.setFreeOfProcessing();
                            $scope.targetChapter.progress = "0";
                            clearInterval(multipleDownload);
                            $scope.clearDownloadedProgress();
                            $scope.process();
                        }
                    }, 300);
				} else {
				    if(typeof multipleDownload != 'undefined') {
                        $scope.stopBulkDownload();
                    }
                }
			} else {
				$timeout(function () {
                    clearInterval(multipleDownload);
                    $scope.stopBulkDownload();
				}, 2000);
				return;
			}
		};

		$scope.$watchCollection("queue", function(nv, ov){
			if(nv !== ov){
				$scope.process();
			}
		});
		/**
		 * Close Right Panel
		 */
		$scope.$on('rightPanelClosed', function(){
			$scope.$emit('enableLoading');
			clearInterval($scope.checkInterval);
			statusCheck = false;
			opened = false;
		});
		/**
		 * Cancel the downloading process
		 */
        $scope.cancelProgress = function(){
          $interval.cancel(promise);
        };

        /**
         * clear progress on every new download
         */

        $scope.clearDownloadedProgress = function() {
            var $$ = angular.element;
            $$(".animate-0-25-b").css("transform","rotate(180deg)");
            $$(".animate-25-50-b").css("transform","rotate(180deg)");
            $$(".animate-50-75-b").css("transform","rotate(180deg)");
            $$(".animate-75-100-b").css("transform","rotate(180deg)");
            $$(".loader-bg .text").text('');
            $$(".loader-bg .percentage-multi").text('');
            $$(".loader-bg .percentage-single").text('');
        }
    }
    OfflinePageController.$inject = ['$scope', '$translate', '$rootScope', 'NativeAPI', 'Queue', 'OfflineQueueItem',
        'OfflinePlayerService', '$interval', '$http', 'Utils', 'CacheService', 'CourseStore']; // Inject dependencies
    return OfflinePageController; // Return Class Definiton
});
