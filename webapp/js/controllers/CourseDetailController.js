/**
 * Created by vladimir on 18.12.2015 г..
 */

'use strict';
define(function () {


    function CourseDetailController(Utils, $scope, $window, $routeParams, $rootScope, Course, HTMLDispatcher, Storage, $translate, NativeAPI, StorageType) {

        var $$ = Dom7;
        NativeAPI.dispatch('isControllerLoaded', "false");
        if ($routeParams && typeof $routeParams.courseId !== 'undefined'){
            var courseId = $routeParams.courseId;
            Course.canEnterCourse(courseId, function(data){
                if ( data.success === true && data.can === false){
                    $window.location.href = '#/all';
                    return;
                }
            },
                function(){
                    $window.location.href = '#/all';
                    return;
                });

			/* Wait loding of meta details before showing the container */
			$scope.courseDetailsLoaded = false;
            /**
             * Are we going to show the offline button
             * Show it only if it is in mobile application and there is content do download
             * @returns {boolean}
             */
			$scope.isButtonAvailable = function () {
                var offlineItems = extractChapters($scope.learningObjects.items);

                return (NativeAPI.isMobile() && offlineItems.length > 0);
            };
            Course.getLoInfo(courseId,
                function(data){
                    $scope.learningObjects = data;
                    $scope.courseInfo = data.courseInfo;
                    $scope.langCode = data.courseInfo.lang_code;
                    $translate([$scope.langCode]).then(function(translations){
                        $scope.langCode = translations[$scope.langCode];
                    });
					/* Wait loding of meta details before showing the container */
					$scope.courseDetailsLoaded = true;
                    $$('.white-title-folder').css('color', 'white');
                    $scope.setTitle();
                    Storage.set('loInfo', data.items);
            }, function(error){
                    if ( error.statusText == 'Unauthorized') {
                        $window.location.href = '#/login';
                    }
                console.log("ERROR: ");
                console.log(error);
             });
        }


        /**
         * set title in navbar
         */

        $scope.setTitle = function() {
            if ( typeof $scope.courseInfo.name !== 'undefined') {
                HTMLDispatcher.ChangeTitle($scope.courseInfo.name); // Change the title of the course
            }
        };

        /**
         *
         */
        $scope.expand = function() {
            var growDiv = document.getElementById('course-description');

            if ( growDiv.clientHeight > 40 ) {
                growDiv.style.height = '40px';
            } else {
                var wrapper = document.getElementById('mobile-wrapper');
                growDiv.style.height = wrapper.clientHeight + "px";
            }
            document.getElementById("more-arrow").className = document.getElementById("more-arrow").className == 'icon-angle-down' ? 'icon-angle-up' : 'icon-angle-down';
        };

        /**
         * check objectType is valid
         * @param objectType
         * @returns mObjectType - is valid or not
         */

        $scope.checkObjectTypes = function(objectType) {

            var mObjectType;

            switch (objectType) {
                case "htmlpage":
                case "sco":
                case "test":
                case "video":
                case "tincan":
                case "aicc":
                case "elucidat":
                case "slides":
                case "authoring":
				case "googledrive":
                case "file":
                    mObjectType = true;
                    break;
                default :
                    mObjectType =  false;
            }
            return mObjectType;
        };

        $scope.goToObject = function(object, idCourse) {
            if ( object.type == 'sco' && object.idScormItem != ''){
                $rootScope.scormId = object.idScormItem;
            }
            if ( object.locked !== true){
                $window.location.href = '#/course/'+idCourse+'/learning_object/'+object.idOrganization;
            }
        };

        /**
         * Basically remove the nasty bounce lagish IOS Bounce (Rubber) Effect
         * by blocking the body to be scrollable and make the
         * Target Div(scrollTarget) scrollable
         *
         * @param scrollTarget is scrollable div (must be valid HTML element)
         */
        $scope.removeIOSBounceEffect = function(scrollTarget){
            scrollTarget.addEventListener('touchstart', function(event){
                var top             = this.scrollTop,
                    totalScroll     = this.scrollHeight,
                    currentScroll   = top + this.offsetHeight;

                if ( top === 0){
                    this.scrollTop = 1;
                }
                if ( top < 0 ) {
                    event.preventDefault();
                } else if ( currentScroll === totalScroll ) {
                    this.scrollTop = top - 1;
                    event.preventDefault();
                }
            });

            scrollTarget.addEventListener('touchmove', function(){
                var mTop             = this.scrollTop,
                    mTotalScroll     = this.scrollHeight,
                    mCurrentScroll   = mTop + this.offsetHeight;

                if ( top < 0 || top===0 && mTotalScroll - mCurrentScroll < 1){
                    event.preventDefault();
                }
            });
        };
        /* Initiate the code bellow when the page(document) is ready */
        angular.element(document).ready(function () {
            /* Get the scrolling div */
            var PAGE = document.getElementById("scrollable");
            /* Pass it to the function to fix the Bounce IOS */
            $scope.removeIOSBounceEffect(PAGE);
        });
        
        //prevent hiding navBar onOrientationChange(landscape) from LearningObjectController
        var navBar = angular.element('#top-bar');
        navBar.show();

        /**
         * Extract chapter data from learningObjects
         */
        var extractChapters = function (object) {
            var result = [];
            var sum = 0;
            angular.forEach(object, function (obj, index) {
                if ( !obj.isFolder && Utils.isTypeSupportedOffline(obj.type)){
                    this.push({
                        id:         obj.idResource,
                        title:       obj.title,
                        description: obj.description,
                        size: {
							value: -1,
							unit: 'b'
						},
                        type:       obj.type,
                        status:     obj.status,
                        state:      0
                    });
                }
            },result);
            result.sumSize = -1;

            return result;
        };
    }
    CourseDetailController.$inject = ['Utils', '$scope',  '$window', '$routeParams', '$rootScope', 'Course', 'HTMLDispatcher', 'LocalStorage', '$translate', 'NativeAPI', 'StorageType'];
    return CourseDetailController;
});