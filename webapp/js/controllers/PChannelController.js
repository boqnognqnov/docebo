define(function () {
	/**
	 * 
	 * @param {Service} $scope
	 * @param {Service} ChannelsData
	 * @param {Array} $routeParams
	 * @returns {Class}
	 */
	function PChannelController($scope, ChannelsData, $routeParams) {
		$scope.channelUserContribution = [];
		$scope.channelData = null;
		$scope.idUser = $routeParams.id; //Setting id of the requested user from query string
		$scope.getUserChannelData = function () {
			ChannelsData.getChannelsData({
				endpoint: ChannelsData.ENUM_ENDPOINT.OTHER_PERSONAL_CHANNEL,
				id_user: $scope.idUser,
				count: 12,
				successCallback: function ($result) {

					$scope.channelData = $result.channel;
					$scope.channelUserContribution = $result.channel.items;
				}
			});
		};//Get channel data by current id from the ChannelsData service

		/**
		 * Loading new elements from the server for the grid
		 * @returns {Void}
		 */
		$scope.infiniteScrollVertical = function () {
			ChannelsData.getChannelsData({
				endpoint: ChannelsData.ENUM_ENDPOINT.OTHER_PERSONAL_CHANNEL,
				from: $scope.channelUserContribution.length,
				count: 12,
				id_user: $scope.idUser,
				successCallback: function ($response) {
					$response.channel.items.forEach(function (item) {
						$scope.channelUserContribution.push(item);
					});
				}
			});
			$scope.$emit("infiniteScroll:vertical");
		};

		$scope.init = function () {
			$scope.getUserChannelData();
			if($('.modal.experts .modal-body').length){
				$('.modal.experts .modal-body').dialog2('close');
			}
		};
	}
	;
	PChannelController.$inject = ['$scope', 'ChannelsData', '$routeParams'];//Injecting dependencies
	return PChannelController;//Return Class definition
});
