/**
 * Created by mitko on 29.2.2016 г..
 */
'use strict';
define(function () {
    function LearningPlanController($scope, $routeParams, $location, $translate, Course, HTMLDispatcher) {

        if (typeof $routeParams.learning_plan_id !== 'undefined'){

            Course.getLearningPlan($routeParams.learning_plan_id,
                function(data){
                    $scope.learningPlanCourses = data.learningPlanCourses;
                    $scope.learningPlan = data.learningPlan;
                    // set current progress of the user in percentage
                    $scope.percentage = $scope.learningPlan.percentage.percentage;
                    $scope.setTitle();
                }, function(error){
                    if ( error.statusText == 'Unauthorized') {
                        $location.path('/login');
                    }
                    console.log("ERROR: ");
                    console.log(error);
                });
        }

        /**
         * set title in navbar
         */
        $scope.setTitle = function() {
            if ( typeof $scope.learningPlan.path_name !== 'undefined') {
                HTMLDispatcher.ChangeTitle($scope.learningPlan.path_name); // Change the title of the course
            }
        };

        /**
         * Expand the course description based on the content
         */
        $scope.expand = function() {
            var growDiv = document.getElementById('course-description');

            if ( growDiv.clientHeight > 40 ) {
                growDiv.style.height = '40px';
            } else {
                var wrapper = document.getElementById('mobile-wrapper');
                growDiv.style.height = wrapper.clientHeight + "px";
            }
            document.getElementById("more-arrow").className = document.getElementById("more-arrow").className == 'icon-angle-down' ? 'icon-angle-up' : 'icon-angle-down';
        };

    }
    LearningPlanController.$inject = ['$scope', '$routeParams', '$location', '$translate', 'Course', 'HTMLDispatcher'];
    return LearningPlanController;
});