/* global angular */
var shouldRequestMore = true;//Flag should do more request against the server if there are no more items this flag is setted to false
var preventInfinite = true; //Flag to prevent infinite scroll further its true by default so when no loaded items ot not retrigger
define(function () {
	function Channels($scope, $timeout, ChannelsData, $routeParams, HTMLDispatcher, $translate){
		$scope.channelID = $routeParams.id; //Setting current channel id from query string
		$scope.channel = ChannelsData.getChannelById($scope.channelID); //Get channel data by current id from the ChannelsData service
		var translateTitle = 'Dashboard';
		$translate(translateTitle).then(function (result) {
			translateTitle = result;
			HTMLDispatcher.ChangeTitle(translateTitle);
		});

		/* Load bootstrap functions */
		$scope.ScriptsInit = function(){
			$timeout(function(){
				angular.element('[data-toggle="tooltip"]').tooltip({ trigger: "click" }); //Reinitiliazing bootstrap tooltips everytime angular $digest over
			});
		};
		
		if($scope.channel === false){//If the result for retrieving channel from cache is false show lazy loading and retrieve new data from the server and removes preventing infinite scroll
			$scope.$broadcast('showLazyLoad');
			ChannelsData.getSingleChannel({
					id_channel: $routeParams.id,
					single_channel: 1,
				successCallback: function(response){
					$scope.channel = response.channel;
					$scope.itemsCount = response.channel.items_count;
					preventInfinite = false;
					$scope.$broadcast('hideLazyLoad');
					$scope.ScriptsInit();
				}
			});
		}else{//Otherwise set the count of items and remove preventing infinite scroll
			 $scope.itemsCount = $scope.channel.items.length;
			 preventInfinite = false;
			 $scope.ScriptsInit();
		}
		/**
		 * Loading new elements from the server for the grid
		 * @returns {Void}
		 */
		$scope.infiniteScrollVertical = function(){
			if(!shouldRequestMore || preventInfinite){ //If should prevent infinite scroll or shouldnt do more request stop execution of this method
				$scope.$broadcast('hideLazyLoad');//Hiding lazyload placeholders if they shown
				return;
			}else{
				$scope.$broadcast('showLazyLoad');//Show lazyload placeholders if they hidden
			}
			ChannelsData.getChannelsData({
				endpoint: ChannelsData.ENUM_ENDPOINT.ITEMS,
				id_channel: $scope.channelID,
				from: $scope.itemsCount,
				count: ChannelsData.itemsChunk,
				successCallback: function($result){
					$scope.$broadcast('hideLazyLoad');
					if(parseInt($result.items_count) === $scope.itemsCount){
						shouldRequestMore = false;
						return;
					}
					$scope.channel = ChannelsData.getChannelById($routeParams.id);
					$scope.itemsCount = $scope.channel.items.length;
					$scope.ScriptsInit();
				}
			});
		};
	}
	function LearningPlans($scope, $timeout, Course, $routeParams, HTMLDispatcher, $translate){
		$scope.from = 0;
		$scope.count = 12;
		$scope.disableInfinite = false;
		Course.getUserLearningPlans($scope.from, $scope.count).then(function(data){
			$scope.channel = data;
			$translate(['My Learning Plans']).then(function (result) {
				var translateTitle = result['My Learning Plans'];
				HTMLDispatcher.ChangeTitle(translateTitle);
				$scope.channel.name = result['My Learning Plans'];
			});

			/* Load bootstrap functions */
			$scope.ScriptsInit = function(){
				$timeout(function(){
					angular.element('[data-toggle="tooltip"]').tooltip({ trigger: "click" }); //Reinitiliazing bootstrap tooltips everytime angular $digest over
				});
			};
			$scope.from = $scope.from+$scope.count;
			/**
			* Loading new elements from the server for the grid
			* @returns {Void}
			*/
			$scope.infiniteScrollVertical = function(){
				if(!$scope.disableInfinite){
					$scope.$broadcast('showLazyLoad');//Show lazyload placeholders if they hidden
					$scope.from = $scope.from+$scope.count;
					Course.getUserLearningPlans($scope.from, $scope.count).then(function(data){
						if(data.items != null && data.items.length > 0){  /////
							for(var i=0;i<data.items.length;i++){
								$scope.channel.items.push(data.items[i]);
							}
						}else{
							$scope.disableInfinite = true;
						}
						$scope.ScriptsInit();
						$scope.$broadcast('hideLazyLoad');
					});
				}
			};
		});
	}
	
	function Courses($scope, $timeout, Course, $routeParams, HTMLDispatcher, $translate){
		$scope.from = 0;
		$scope.count = 12;
		$scope.disableInfinite = false;
		Course.getUserCourses($scope.from, $scope.count).then(function(data){
			$scope.channel = data;
			$translate(['My Courses']).then(function (result) {
				var translateTitle = result['My Courses'];
				HTMLDispatcher.ChangeTitle(translateTitle);
				$scope.channel.name = result['My Courses'];
			});

			/* Load bootstrap functions */
			$scope.ScriptsInit = function(){
				$timeout(function(){
					angular.element('[data-toggle="tooltip"]').tooltip({ trigger: "click" }); //Reinitiliazing bootstrap tooltips everytime angular $digest over
				});
			};
			/**
			* Loading new elements from the server for the grid
			* @returns {Void}
			*/
			$scope.infiniteScrollVertical = function(){
				if(!$scope.disableInfinite){
					$scope.$broadcast('showLazyLoad');//Show lazyload placeholders if they hidden
					$scope.from = $scope.from+$scope.count;
					Course.getUserCourses($scope.from, $scope.count).then(function(data){
						if(data.items != null && data.items.length > 0){ ////
							for(var i=0;i<data.items.length;i++){
								$scope.channel.items.push(data.items[i]);
							}
						}else{
							$scope.disableInfinite = true;
						}
						$scope.ScriptsInit();
						$scope.$broadcast('hideLazyLoad');
					});
				}
			};
		});
	}
	/**
	 * 
	 * @param {Service} $scope
	 * @param {Service} $timeout
	 * @param {Service} ChannelsData
	 * @param {Array} $routeParams
     * @param NativeAPI
	 * @returns {Class}
	 */
	function ChannelsSingleController($scope, $timeout, ChannelsData, $routeParams, HTMLDispatcher, $translate, Course){
		$scope.viewType = $routeParams.type;
		switch($scope.viewType){
			case 'lp':
					LearningPlans($scope, $timeout, Course, $routeParams, HTMLDispatcher, $translate);
				break;
			case 'courses':
					Courses($scope, $timeout, Course, $routeParams, HTMLDispatcher, $translate);
				break;
			case '':
			default:
				Channels($scope, $timeout, ChannelsData, $routeParams, HTMLDispatcher, $translate);
				break;
		}
	};
	ChannelsSingleController.$inject = ['$scope', '$timeout', 'ChannelsData', '$routeParams', 'HTMLDispatcher', '$translate', 'Course'];//Injecting dependencies
	return ChannelsSingleController;//Return Class definition
});
