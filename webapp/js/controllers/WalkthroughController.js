/* global angular */
define(function () {
	/**
	 *
	 * @param $scope
	 * @param $rootScope
	 * @param $window
	 * @param $route
	 * @param HTMLDispatcher
	 * @param Storage
	 * @param Cards
     * @param Config
     * @constructor
     */
	function WalkthroughController($scope, $rootScope, $window, $route, HTMLDispatcher, Storage, Cards, Config, $translate){
		if ( Storage.get('mobile_info').skip_walkthrough ) {
			$window.location.href = '#/uploadQueue';
		}

		var translateTitle = '';
		$translate(['Walkthrough: 1 of 3']).then(function (result) {
			translateTitle = 'Walkthrough: 1 of 3';
			HTMLDispatcher.ChangeTitle(translateTitle);
		});
		var $$ = Dom7;
		var cards = Cards.getCardsByType('walkthrough');
		/**
		 * Return array of cards for the walkthrough intro for the directive
		 * @returns {*}
         */
		$scope.getCards = function(){
			return cards;
		};

		/**
		 *
		 * @param cardId
         */
		

		$scope.customFunction = function(cardId) {
			var allCards = $scope.getCards().length;
			var currentId = parseInt(cardId.id) + 1;
			
			$translate(['Walkthrough', 'of']).then(function(translations){
				HTMLDispatcher.ChangeTitle(translations['Walkthrough'] + currentId + translations['of'] + allCards);
			});
		};
		
		/**
		 * Either you SWIPE a card or Click 'Next' this event is targeted
		 */
		$rootScope.$on('swipeCards:next', function(event, args){
			var allCards = $scope.getCards().length;
			var currentId = allCards + 1 - parseInt(args);
			var doNotShow = $(".checbox-lastslide");
			var card = angular.element('[data-pane='+args+']');
			var cardNext = angular.element('[data-pane='+(args-1)+']');
			var cardLast = angular.element('[data-pane='+(args-2)+']');
			
			cardNext.removeClass('next');
			cardNext.addClass('next1');
			cardLast.addClass('next1');
			if(cardNext.hasClass('last')){
				cardNext.removeClass('last');
			}
			card.hide();
			// Here you are clicking the 'confirm' button for the last card
			//if ( currentId > allCards && !Storage.get('mobile_info').skip_walkthrough) {
			if ( currentId > allCards ) {
				currentId = allCards;
				$window.location.href = '#/uploadQueue';
				// Here you are looking on the last card, but still not clicked
				doNotShow.hide();
				
				/* Skip walkthrough page if checkbox is checked */
				if ( $( "input#not-show:checked").length > 0) {
					Config.skipWalkthrough(
							function(data){
								if ( data.success ) {
									var mobileInfo = Storage.get('mobile_info');
									mobileInfo.skip_walkthrough = 1;
									Storage.set('mobile_info', mobileInfo);
								}
							}, function(error){
								console.log("Error: " + error);
							}
					);
				}
			} else if ( currentId == allCards ) {
				doNotShow.show();
			}

			
			/* Change the navbar title depending on cards progress */
			if($route.current.controller !== 'UploadQueueController'){
				$translate(["Walkthrough", 'of']).then(function(translations){
					HTMLDispatcher.ChangeTitle(translations["Walkthrough"] + ' ' + currentId + ' ' +translations["of"] + ' ' + allCards);
				});
			}
		});
	}
	WalkthroughController.$inject = ['$scope', '$rootScope', '$window', '$route', 'HTMLDispatcher', 'LocalStorage', 'Cards', 'Config', '$translate'];//Injecting dependencies
	return WalkthroughController;//Return Class definition
});
