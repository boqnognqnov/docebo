/* global angular */
define(function () {
	/**
	 * ready for invitie
	 * @type Number
	 */
	var INVITE_STATUS_FOR_INVITE = 0;
	/**
	 * Already invited
	 * @type Number
	 */
	var INVITE_STATUS_INVITED = 1;
	/**
	 * All new invitations
	 * @type Number
	 */
	var INVITE_STATUS_NEW_INVITATIONS = 3;
	/**
	 * Invite related for channels
	 * @type Number
	 */
	var INVITE_TYPE_CHANNEL = 1;
	/**
	 * Invite related to users
	 * @type Number
	 */
	var INVITE_TYPE_USERS = 2;
	
	function InviteWatchController($scope, $rootScope, $timeout, $translate, $filter, CSAsset, $routeParams, Storage){
		
		var Framework7Popup = new Framework7();
				
		$scope.assetID = $routeParams.id;
		$scope.invited_items;
		$scope.req = 0;
		$scope.name = '';
		$scope.onItemSelected = function () { // this gets executed when an item is selected
			$scope.name = '';
		};
		
		/* Get all users for the current asset */
		$scope.setCurrentAsset = function () {
			var promise = CSAsset.assetInvitations($routeParams.id);
			promise.then(function(response){
				$scope.dataIsLoaded = true;
				$scope.req = 1;
				
				$scope.invited_items = [];
				var i=0;
				angular.forEach(response, function(element) {
					$scope.invited_items[i] = element;
					i++;
				});
				
			});
			
		};
		
		$scope.$on('loadInvitations', function() {
			$scope.setCurrentAsset();
		});
		
		$scope.$watch('req', function(value){
			$scope.reqUpdated = value;
			if(value === 1){
				angular.element('.loading-spinner-background').hide();
				//Add max-height to the scrollable modal content
				angular.element(".invited_items").css('max-height', angular.element('.popup.modal-in').height()-228);
			}
		});
		
		$scope.invitedPeople = 0;
		$scope.inviteToWatchForm = null;
		$scope.inviteForm = false;
		$scope.loadingContent = true;
		$scope.showLoadingF = true;
		
		/**
		 * Call invite users and proccess whole flow
		 * @returns {inviteToWatch_L2.inviteController.$scope}
		 */
		$scope.inviteUsers = function () {
			$scope.inviteForm = true;
			$scope.loadingContent = false;
			$scope.showLoadingF = true;
			$scope.invitedPeople = 0;
			angular.element('.modal-popup').addClass('inviting');
			
			var userID = Storage.get('mobile_info').idst;
			var itemsArray = [];
			var i=0;
			angular.forEach($scope.invited_items, function(element) {
				if(element.invited == '3'){
					/* Remove channel elemes and only add uids to the array */
					if(element.type == '1'){
						var Ids = element.userIds;
						var temp = new Array();
						temp = Ids.split(",");
					}else{
						itemsArray.push(element.uid);
					}
				}
				i++;
			});
			var promise = CSAsset.assetSendInvitations($routeParams.id, itemsArray, userID);
			promise.then(function(response){
				$scope.inviteSuccess(response);
			}, function (error) {
				$scope.inviteFail(error);
			});
			return this;
		};

		$scope.keypress = function($event){
			if ($event.keyCode == 13) {
				$event.preventDefault();
			}
		};
		
		/**
		 * Flow when invited users are successfull done 
		 * @param {Object} data
		 * @returns {inviteToWatch_L2.inviteController.$scope}
		 */
		$scope.inviteSuccess = function (data) {
			$scope.inviteForm = true;
			$scope.loadingContent = true;
			$scope.showLoadingF = false;
			$scope.invitedPeople = data.invitedPeople;
			
			/* Close the success dialog */
			$timeout(function(){
				Framework7Popup.closeModal();
			}, 3000);
			
			/* Reset popup settings */
			$timeout(function(){
				$scope.inviteForm = false;
				$scope.loadingContent = true;
				$scope.showLoadingF = true;
				angular.element('.modal-popup').removeClass('inviting');
			}, 4000);
			
			return this;
		}
		
		/**
		 * Notify the usser for an error
		 * @param {text} errorThrown
		 * @returns {inviteToWatch_L2.inviteController.$scope}
		 */
		$scope.inviteFail = function (errorThrown) {
			alert(errorThrown);
			$scope.$broadcast('ModalClosed');
			return this;
		}
		/**
		 * 
		 * @param {type} selectedItem
		 * @param {type} invited_items
		 * @returns {inviteToWatch_L33.$scope}
		 */
		$scope.handleSelection = function (selectedItem, invited_items) {
			if (selectedItem.type == INVITE_TYPE_CHANNEL) {
				$scope.req = 0;
				$scope.tempArr = [];
				var ids = selectedItem.userIds.split(',');
				angular.forEach(invited_items, function (value, key) {
					angular.forEach(ids, function (value2, key) {
						if (value.id == value2 && value.invited != 1 && value.invited != INVITE_STATUS_NEW_INVITATIONS) {
							value.invited = INVITE_STATUS_NEW_INVITATIONS;
							value.chanTrack = 1;
						}
					});
					$scope.tempArr.push(value);
					if(invited_items.length == $scope.tempArr.length) {
						$scope.req = 1;
						$scope.tempArr = [];
					}
				});
			}
			selectedItem.invited = INVITE_STATUS_NEW_INVITATIONS;
			$scope.model = '';
			$scope.current = 0;
			$scope.selected = true;
			angular.element('#selection .person').show();
			
			return this;
		};
		$scope.showForm = function () {
			return $scope.inviteForm;
		}
		$scope.showLoading = function () {
			return $scope.loadingContent;
		}
		$scope.showComplete = function () {
			return $scope.showLoadingF;
		}

		/**
		 * 
		 * @param {type} selectedItem
		 * @param {type} invited_items
		 * @returns {inviteToWatch_L33.$scope}
		 */
		$scope.unhandleSelection = function (selectedItem, invited_items) {
			if (selectedItem.type == INVITE_TYPE_CHANNEL) {
				$scope.req = 0;
				$scope.tempArr = [];
				var ids = selectedItem.userIds.split(',');
				angular.forEach(invited_items, function (value, key) {
					angular.forEach(ids, function (value2, key) {
						if (value.id == value2 && value.invited != INVITE_STATUS_INVITED && value.chanTrack == 1) {
							value.invited = 0;
							value.chanTrack = 0;
						}
					});
					$scope.tempArr.push(value);
					if(invited_items.length == $scope.tempArr.length) {
						$scope.req = 1;
						$scope.tempArr = [];
					}
				});
			}
			selectedItem.invited = 0;
			$scope.model = '';
			$scope.current = 0;
			$scope.selected = true;

			return this;
		};
		/**
		 * 
		 * @param {type} invited_items
		 * @returns {inviteToWatch_L33.$scope}
		 */
		$scope.unhandleAll = function (invited_items) {
			angular.forEach(invited_items, function (value, key) {
				if (value.invited == INVITE_STATUS_NEW_INVITATIONS) {
					value.invited = 0;
					value.chanTrack = 0;
				}
			});
			$scope.model = '';
			$scope.current = 0;
			$scope.selected = true;

			return this;
		};
		$scope.current = 0;
		$scope.selected = true; // hides the list initially

		/**
		 * 
		 * @param {type} index
		 * @returns {Boolean}
		 */
		$scope.isCurrent = function (index) {
			return $scope.current == index;
		};
		/**
		 * Set index to current 
		 * @param {type} index
		 * @returns {inviteToWatch_L33.$scope}
		 */
		$scope.setCurrent = function (index) {
			$scope.current = index;
			return this;
		};
		
		$scope.filterInvited = function(element){
			if(element.invited == 3 && element.chanTrack == 0){
				return true;
			}
			return false;
		};
		$scope.isSelected = function(){
			var result = false;
			if ( typeof $scope.invited_items != 'undefined'){
				var selected = $filter('filter')($scope.invited_items, {invited:3,type:2});
				result = (selected.length > 0 ? true : false);
				$scope.countSelected = selected.length;
			}
			return result;
		}
    }
	InviteWatchController.$inject = ['$scope', '$rootScope', '$timeout', '$translate', '$filter', 'CSAsset', '$routeParams',  'LocalStorage'];//Injecting dependencies
	return InviteWatchController;//Return Class definition
});