define(function () {
	/**
	 * 
	 * @param {Service} $scope
	 * @param {Service} $http
	 * @param {Service} ChannelsData
	 * @returns {Class}
	 */
	function DashboardController($scope, $http, ChannelsData, $route, HTMLDispatcher, $rootScope, $translate, NativeAPI){
		$scope.$route = $route;
		$scope.channels = [];//Channels scope object
		$scope.noChannels = false;//Show message on dashboard if true
		$scope.init = function(){//Called on page initialisation
			var channels = ChannelsData.getChannels();//Retrieve channels data from the service
			if(!channels){//If channels data from the service is empty contact the server to get the data
				ChannelsData.getChannelsData({
					items_per_channel: 6,
					successCallback: function(){
						$('lazyload').show();
						$scope.channels = ChannelsData.getChannels();
						$scope.$broadcast('hideLazyLoad');
						if($scope.channels == ''){
							$scope.noChannels = true;
						}
						NativeAPI.dispatch('isDashboardLoaded', "true");
					}
				});
			}else{//otherwise set channels object to service data
				$scope.channels = channels;
				if($scope.channels == ''){
					$scope.noChannels = true;
				}
			}
			
			var translateTitle = 'Dashboard';
			$translate(translateTitle).then(function (result) {
				translateTitle = result;
				HTMLDispatcher.ChangeTitle(translateTitle);
			});
			/* Reset channels */
			ChannelsData.reviveChannels();
		};
		$scope.infiniteScrollVertical = function(){//Called on infinite scroll
			if(ChannelsData.canRequestMoreChannels() === true){//Checks whether can reqeust more channels or not
				$('lazyload').show();
				$scope.$broadcast('showLazyLoad');//fire event for showing lazyload
			}
			ChannelsData.getChannelsData({
				from: ChannelsData.next_from,
				successCallback: function(){
					$scope.channels = ChannelsData.getChannels();//Setting the scope channels
					$scope.$broadcast('hideLazyLoad');//Hiding lazy load
				}
			});
			$scope.$emit("infiniteScroll:vertical");//Emit infiniteScroll event for outside use purpose's
		};
		$translate(['All Channels', 'My Channel']).then(function(translations){
			$scope.$tabs = [
				{title: translations['All Channels'], action: '#/all', icon: 'icon-d_thumbs'},
				{title: translations['My Channel'], action: '#/myChannel', icon: 'icon-d_user'}
			];
		});
		$rootScope.getTabs = function(){
			return $scope.$tabs;
		};
		
		$scope.$on('$destroy', function(){
			ChannelsData.cleanToFirstChunk();
		});


		/**
		 * change status of the view expert icon, according to the orientation of the view
		 * node: this is temporarily *hide* of the view expert icon
		 */

		$scope.$watch(function () {
			return document.body.innerHTML;
		}, function(val) {
			//TODO: write code here, slit wrists, etc. etc.

			if (window.matchMedia("(orientation: portrait)").matches) {
				//PORTRAIT mode
				$('.experts-mobile').hide(); //show
				$(".experts").hide();
			}

			if (window.matchMedia("(orientation: landscape)").matches) {
				//LANDSCAPE mode
				$(".experts").hide(); //show
				$('.experts-mobile').hide();
			}
		});

		window.addEventListener("orientationchange", function() {
			if (screen.height > screen.width) {
				//PORTRAIT mode
				$('.experts-mobile').hide(); //show
				$(".experts").hide();

			} else {
				//LANDSCAPE mode
				$(".experts").hide(); //show
				$('.experts-mobile').hide();
			}
		}, false);

	}
	DashboardController.$inject = ['$scope', '$http', 'ChannelsData', '$route', 'HTMLDispatcher', '$rootScope', '$translate', 'NativeAPI'];//Injecting dependencies
	return DashboardController;//Returning class definition
});