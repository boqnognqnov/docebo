/**
 * Created by vladimir on 5.1.2016 г..
 */
//'use strict';
define(['require', 'translate', 'sanitize'], function (require) {
    function AppController($scope, $location, $routeParams, NativeAPI, $rootScope, HTMLDispatcher, $timeout,
						   $window, Config, User, Storage, Course, TrackUser, Cards, StorageType, DeviceOrientation, PlayerStore) {
		var Panels = new Framework7();
		$rootScope.panelController = 'PublishPageController';
        var $$ = Dom7;
		Cards.getCards();
		$scope.$showNextArrow = false;
        /**
         * On refresh page apply the colors from the API request earlier..
         */
        $scope.$refreshNavbar = function(){
			if(window.isiPhone == 1){
				User.getUserInfo(
					function(data){
						Storage.set('mobile_info', data);
						/* GET CURRENT USER ID */
						$rootScope.userIdst = Storage.get('mobile_info').idst;
					}, function(error){
						if ( error.statusText === 'Unauthorized') {
							$location.path('/login');
						}
						console.log(error);
						console.log("Cannot retrieve User info from LMS.");
				});
				$scope.isIphone = 'true';
			} else if (window.isAndroid == 1) {
				User.getUserInfo(
					function(data){
						Storage.set('mobile_info', data);
						/* GET CURRENT USER ID */
						$rootScope.userIdst = Storage.get('mobile_info').idst;
					}, function(error){
						if ( error.statusText === 'Unauthorized') {
							$location.path('/login');
						}
						console.log(error);
						console.log("Cannot retrieve User info from LMS.");
					});
			}
        };

        if ( Storage.get('learning_plan_id') != null ) {
            $scope.learningPlanId = Storage.get('learning_plan_id');
        }

        $timeout(function(){
            $scope.$refreshNavbar();
            if ( Storage.get('learning_plan_id') != null ) {
                $scope.learningPlanId = Storage.get('learning_plan_id');
            }
            if ( typeof $routeParams === 'object' ) {
                //$scope.idOrg    = $routeParams.learningObjectId;
                $scope.idCourse = $routeParams.courseId;
            }
        });
		
		/* Open three dots link menu */
		$rootScope.open = function($event){
			var selector = angular.element($event.currentTarget).parent().parent().find('.dots-menu');
			selector.addClass('open');
		};
		/* Close three dots link menu */
		$rootScope.close = function($event) {
			var selector = angular.element($event.currentTarget).parent().closest('.dots-menu');
			selector.removeClass('open');
		};
		
		/* Close dots menu on clicking outside of the tile */
		$(document).on('click', function (e) {
			if( typeof event != 'undefined' && $(event.target).parents('.col-tile').length  === 0 ) {
                $(e.target).find(".dots-menu").removeClass('open');
            }
		});
		
		/* Deeplink functionality (copy to clipboard) */
		$scope.getShareurl = function($event){
			var selector = angular.element($event.currentTarget).parent().find('.copy-url');
			var selectorInitial = angular.element($event.currentTarget);
			var selectorCopied = angular.element($event.currentTarget).parent().find('.copied-success');
			$scope.textToCopy = selector.attr('href');
			
			$scope.success = function () {
				selectorInitial.hide();
				selectorCopied.show();
				$timeout(function() {
					selectorInitial.show();
					selectorCopied.hide();
				}, 2000);
			};

		};

        /**
         * Return you in the previous page
         */
        $scope.$back = function() {
			var pageAddress = TrackUser.popRoute();
			$rootScope.$broadcast("go:back", {route: pageAddress});
			//HTMLDispatcher.back();
        };
        /**
         * If user comes from a learning plan be send him again there when he is
         * going back from a course.
         * @param id = learning plan id
         */
       $scope.$backToLP = function(id) {
			var id = ''; 
			if(Storage.get('learning_plan_id') != null) {
				id = Storage.get('learning_plan_id');
			}
		   HTMLDispatcher.backToLP(id);
        };

        $scope.$backToGlobalSearch = function() {
			$scope.$back();
			document.getElementById("search-wrapper").style.display = 'block';
			document.getElementById("search-autocomplete-dropdown").focus();
			NativeAPI.dispatch('isGlobalSearchLoaded', "true");
		};

        $scope.$hasLearningPlanId = function(){
            return ( Storage.get('learning_plan_id') != null ? true : false );
        };
		
        angular.element(document).ready(function() {
			StorageType.invokeStorageCheck();
            if ( Storage.get('learning_plan_id') != null ) {
                $scope.learningPlanId = Storage.get('learning_plan_id');
            }
		});
			//var animateJs = new require('animate');
			//var translateJs = new require('translate');
			//var sanitizeJS = new require('sanitize');

		$scope.$hasTabs = function(){
			var loc = function(needle){
				return $location.$$url.indexOf(needle);
			};
			if (loc('/all') == 0 || loc('/myChannel') == 0 || loc('/uploadQueue') == 0 || loc('/uploadHistory') == 0) {
				return true;
			}
			return false;
		};

		NativeAPI.registerFunc('$back', function() {
			$scope.$back();
		});

		NativeAPI.registerFunc('back', function() {
			HTMLDispatcher.back();
		});

		NativeAPI.registerFunc('backToLP', function() {
			$scope.$backToLP();
		});

		NativeAPI.registerFunc('rightPanelState', function() {
			$scope.rightPanelState();
		});

		NativeAPI.registerFunc('$backToGlobalSearch', function() {
			$scope.$backToGlobalSearch();
		});

		NativeAPI.registerFunc('controllerLoaded', function() {
			NativeAPI.dispatch('isControllerLoaded', "false");
		});

		NativeAPI.registerFunc('triggerOfflineBtn', function() {
			angular.element('#offline-mode-check').prop('checked', true);
			angular.element('.label-switch.label-switch-offline-mode-menu').click();
		});

		/**
		 * If you are in course return true to set Back Button in Menu header
		 * @param {String} name
		 * @param {String|Object|Int} data
		 * @returns {boolean}
		 */
		$rootScope.callFunc = function (name, data){
			NativeAPI.callFunc(name, data);
		};
		/**
		 *
		 * @returns {boolean}
		 */
		$scope.$returnEnteredPage = function() {
			$scope.$showNextArrow = false;
			if( $location.$$url.indexOf("search") > 0 ) {
				return 'search';
			} else if ( !PlayerStore.isReady() && $location.$$url.indexOf("course/") > 0 && $location.$$url.indexOf("learning") < 1 ) {
				return 'course';
			} else if ( PlayerStore.isReady() ) {
				$scope.$showNextArrow = true;
				return 'learning_object';
			} else if ($location.$$url.indexOf("channel") > 0 ){
				return 'viewAllChannels';
			} else if ( $location.$$url.indexOf("learning_plan") > 0 ) {
				return 'learning_plan';
			} else if ( $location.$$url.indexOf("assets") > 0 ){
				return 'viewAllChannels';
			} else if ( $location.$$url.indexOf("my/lp") > 0 ){
				return 'myLp';
			} else if ( $location.$$url.indexOf("my/courses") > 0 ){
				return 'myCourses';
			}
			else {
				return 'home';
			}
		};
		
		/* Reveal rightside panel framework7 */
		$scope.$openRightPanel = function(uploadedItem) {
			if ( typeof uploadedItem !== 'undefined' ) {
				var objectToUpload = {url: uploadedItem.uploadFileUrl, instance: 'publish-asset'};
				$scope.$broadcast('RightPanelOpened', uploadedItem);
				$rootScope.$broadcast('RightPanelVideo', objectToUpload);
			}
			Panels.openPanel('right');
			$scope.bodyElem = $('body');
			if(!$scope.bodyElem.hasClass('with-panel-right-cover')){
				$scope.bodyElem.addClass('with-panel-right-cover');
			}
		};

		/* Close rightside panel framework7 */
		$scope.$closeRightPanel = function() {
			Panels.closePanel('right');
		};

		/**
		 * Return true if the OS viewing the APP is DESKTOP
		 * @returns {boolean}
		 */
		$scope.$isDesktop = function() {
		/*	var OS = NativeAPI.device();
			if ( OS === NativeAPI.OS.Desktop ) {
				return true;
			}*/
			return true;
		};
		$scope.$isLocked = function(course) {
			var status = course.status;
		};
		/**
		 * The values of the tab filter values
		 * @type {string[]}
		 */

		$scope.menuItems = [
			'All Courses',
			'Unlocked Courses',
			'Locked Courses',
			'Completed Courses'
		];
		/**
		 * Default Active Menu Tab in the Navigation Bar
		 * @type {string}
		 */
		$scope.activeMenu = 'All Courses';
		/**
		 * Set Active tab in the navigation bar by passing a string
		 * @param menuItem
		 */
		$scope.setActive = function(menuItem) {
			$scope.activeMenu = menuItem;
		};
		
		/**
		 * Get back to the Course(list of learning objects)
		 */
		$scope.$returnToCourse = function() {
			if(!PlayerStore.isReady()){
				$scope.$broadcast('LearningObjectClosed');
				$location.path('/course/' + $routeParams.courseId);
				TrackUser.popRoute();
				if ($rootScope.$root.$$phase != '$apply' && $rootScope.$root.$$phase != '$digest') {
					$rootScope.$apply();
				}
			}else{
				$scope.$broadcast('closePlayer');
			}

		};
		NativeAPI.registerFunc('returnToCourse', $scope.$returnToCourse);

		/**
		 * check objectType is valid
		 * @param objectType
		 * @returns mObjectType - is valid or not
		 */

		$scope.checkLoObjectTypes = function(objectType) {

			var mObjectType;

			switch (objectType) {
				case "htmlpage":
				case "sco":
				case "test":
				case "video":
				case "tincan":
				case "aicc":
				case "elucidat":
				case "slides":
				case "authoring":
				case "file":
				case "googledrive":
					mObjectType = true;
					break;
				default :
					mObjectType =  false;
			}
			return mObjectType;
		};

		if ( typeof $routeParams.courseId != 'undefined') {
			Course.getLoInfo($routeParams.courseId,
					function(data){
						$scope.learningObjects = data;
						$scope.courseInfo = data.courseInfo;

						/* Wait loding of meta details before showing the container */
						$scope.courseDetailsLoaded = true;

						$$('.white-title-folder').css('color', 'white');

						Storage.set('loInfo', data.items);
					}, function(error){
						if ( error.statusText == 'Unauthorized') {
							$location.path('/login');
						}
						console.log("ERROR: ");
						console.log(error);
					});
		}

		/* Watch for unlocking the next course */
		$scope.$watch('isLoCompleted', function(newVar, oldVar){
			if ( typeof newVar != 'undefined' && newVar != '' ){
				$scope.isLoCompleted = newVar;
			}
		});
		/* if isLoCompleted is true show next arrow, if false -> show locked*/
		$scope.isLoCompleted = false;
		$scope.$on('learningObjectCompleted', function(event, data){
			$scope.isLoCompleted = true;
		});
		$scope.$on('learningObjectStarted', function(){
			$scope.isLoCompleted = false;
		});
		/* if lastLo is true then hide any arrows*/
		$scope.lastLo = false;
		$scope.$on('lastLearningObjectReached', function(){
			$scope.lastLo = true;
		});
		$scope.$on('NotlastLearningObjectReached', function(){
			$scope.lastLo = false;
		});

		/* Get transaltions */
		if ( !NativeAPI.isMobile()) {
			Config.getLanguage(
			function (data) {
				Storage.set('translationStrings', data);
			}, function (error) {
				console.log(error);
			});
		}

		$scope.avatar = '';
		$scope.firstname = '';
		$scope.lastname = '';
		$scope.email = '';
		/**
		 * This check are you able to enter a learning object. If it is an asset you are able to enter with no check :)
		 * @param item
		 * @returns {boolean|*}
         */
		$rootScope.iCanEnter = function(item){
			var result;
			result = (item.can_enter || item.type_id === 'knowledge_asset' ? true : false);
			
			return result;
		};
		$scope.isOfflineChecked = Storage.get('offline-mode');
		/**
		 * Watches the on/off switch in the left menu and raise event
		 * is the offline mode is ON or OFF and pass the click event
		 * of the element
		 *
		 * If isChecked == false the device is in offline mode
         */
		$scope.setOfflineStatus = function () {
			if(isOnline()){
				setOffline();
				NativeAPI.dispatch('setOfflineMode', "");
			}else{
				setOnline();
				NativeAPI.dispatch('setOnlineMode', "");
			}
			$rootScope.offlineMode = !isOnline(); // global state of offline mode
		};

		$scope.isOffline = function(){
			return NativeAPI.isMobile() && !isOnline();
		};
		/**
		 * Open the right panel and raise event for opened offline page
		 *
		 * @param idCourse
		 * @param courseName
		 * @param courseChapters - All learning objects names, sizes, statuses
		 */
		$scope.$openOfflinePage = function(idCourse, courseName, courseChapters){
			$rootScope.panelName = 'OfflinePageController';
			Panels.openPanel('right');
			$scope.$broadcast('offlinePageOpened', {
					idCourse  : idCourse,
					courseName: courseName,
				courseChapters: courseChapters
				});

			$scope.bodyElem = $('body');
			if(!$scope.bodyElem.hasClass('with-panel-right-cover')){
				$scope.bodyElem.addClass('with-panel-right-cover');
			}
		};

		/**
		 * get native device storage
		 */

	//	$scope.$on("getMobileStorage", function (event, data) {

	//	});


		/**
		 * resolve double scroll on right panel
		 */

        window.addEventListener("orientationchange", function () {
			var orientation = DeviceOrientation.getDeviceOrientation();
            if(orientation == DeviceOrientation.ORIENTATION.PORTRAIT || orientation == DeviceOrientation.ORIENTATION.UPSIDE_DOWN){
                angular.element('.content-asset-block').css('height', window.screen.height-52); // Fixed header bar
            }else{
                angular.element('.content-asset-block').css('height', window.screen.width-52); // Fixed header bar
            }
        });

		/**
		 *
		 * @returns {Array} of links to images all over the DOM
         */
		$rootScope.getAllImages = function(){
			var url, imgs = [], elements = document.getElementsByTagName('*');
			elements = imgs.slice.call(elements, 0, elements.length);
			while( elements.length ){
				url = document.deepCss(elements.shift(), 'background');
				if(url)
					url = /url\(['"]?([^")]+)/.exec(url) || [];
				url = url[1];
				if( url && imgs.indexOf(url)== -1)
					imgs[imgs.length]= url;
			}
			return imgs;
		};
		/**
		 * Searchin element in the DOM by style property
		 * @param who - reference to the element
		 * @param css - style property we search for
         * @returns {*}
         */
		document.deepCss= function(who, css){
			if(!who || !who.style) return '';
			var sty= css.replace(/\-([a-z])/g, function(a, b){
				return b.toUpperCase();
			});
			if(who.currentStyle){
				return who.style[sty] || who.currentStyle[sty] || '';
			}
			var dv = document.defaultView || window;
			return who.style[sty] ||
				dv.getComputedStyle(who,"").getPropertyValue(css) || '';
		};
		/**
		 * Search in array
		 * @type {any}
         */
		Array.prototype.indexOf = Array.prototype.indexOf ||
			function(what, index){
				index= index || 0;
				var L= this.length;
				while(index< L){
					if(this[index]=== what) return index;
					++index;
				}
				return -1;
			};

		$rootScope.$on('$viewContentLoaded', function () {
			angular.element(document).ready(function () {
					var imagesArr = $rootScope.getAllImages();
					imagesArr.map(function (link) {
						testImage(link)
							.then(function () {//do nothing - image is fine
								 },
								function () {

							});
					});

				MutationObserver = window.MutationObserver || window.WebKitMutationObserver;


				var observer = new MutationObserver(function(mutations, observer) {
					// fired when a mutation occurs
					mutations.map(function (elem) {
						if (elem.target.className === 'thumb-img'){
							var thumbUrl = elem.target.style.backgroundImage.replace('url(','').replace(')','').replace(/\"/gi, "");
							testImage(thumbUrl)
								.then(function(success){
									//do nothing - image is fine
								}, function (e) {
									elem.target.className = 'thumb-img backup-image fa fa-camera';
								});
						}
					});
				});
				// define what element should be observed by the observer
				// and what types of mutations trigger the callback
				var config = {
					subtree: true,
					attributes: true
				};
				observer.observe(document, config); // Start to observe for changes in 'thumb-img'
			});
		});
		/**
		 * Check an image is it accessible
		 * @param url
         * @returns {Promise}
         */
		function testImage(url) {
			return new Promise(function (resolve, reject) {
				var timeout = 5000;
				var timer, img = new Image();
				img.onerror = img.onabort = function () {
					clearTimeout(timer);
					reject("error");
				};
				img.onload = function () {
					clearTimeout(timer);
					resolve("success");
				};
				timer = setTimeout(function () {
					// reset .src to invalid URL so it stops previous
					// loading, but doesn't trigger new load
					img.src = "//!!!!/test.jpg";
					reject("timeout");
				}, timeout);
				img.src = url;
			});
		}
    }
    AppController.$inject = ['$scope', '$location', '$routeParams', 'NativeAPI', '$rootScope', 'HTMLDispatcher', '$timeout',
		'$window', 'Config', 'User', 'LocalStorage', 'Course', 'TrackUser', 'Cards', 'StorageType', 'DeviceOrientation', 'PlayerStore'];
    return AppController;
});