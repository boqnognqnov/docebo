'use strict';
/* global angular */
define(function () {
	SlidePlayer.$inject = ['Config'];
	function SlidePlayer(Config) {
		return {
			scope:{
				slides: '=slides'
			},
			templateUrl: view('CoursePlayer.authoring.slide_player')
		};
	}
	return SlidePlayer;
});