'use strict';
define(function () {
	function NextArrow(CourseStore, PlayerStore, PlayerActions) {
		return {
			templateUrl: view('CoursePlayer.nextArrow.next_arrow'),
			link: function (scope) {
				/* Declare variables*/
				var items = {}; // Array of objects ( all LOs for Course )
				var keys = {}; // Array of index -> id pair
				var currentIndex = 0; // Index of the currentItem in array
				var currentObject = {}; // Current LO Metadata
				var nextIndex = 0;
				var nextItem = {}; // Next object metadata
				/**
				 * Construct method that init PlayerStore to be ready
				 */
				var construct = function () {
					scope.lastLo = false; // Is the current LO last in the array / lock arrow /
					scope.isLoCompleted = false; // is status completed / unlock arrow /
					scope.loaded = PlayerStore.isReady(); // TRUE if _store is ready

					if ( scope.loaded ){
						items 		  = CourseStore.items();
						currentObject = PlayerStore.getLo();
						/* Is current object completed and unlocked if so TRUE */
						scope.isLoCompleted = ( !currentObject['locked'] && currentObject['status'] == PlayerStore.STATUS.COMPLETED ? true : false);
						keys = Object.keys(items).map(function (key) {
							return items[key];
						});
						keys.sort(sortById); // Sort the array by id_organization
						/* Get the index in the array of the current item */
						currentIndex = keys.findIndex(function (key) {
							return key['key'] === currentObject['key'];
						});
						/* Check is the current index of the item is last in the array */
						if ( currentIndex + 1 == keys.length ){
							scope.lastLo = true; // Last in the array
						}
						nextIndex = (currentIndex > -1 ? currentIndex + 1 : false);
						nextItem = ( nextIndex != false ? keys[nextIndex] : false);
						if ( nextItem != false || typeof nextItem != 'undefined'){
							if ( !isItemAccessible(nextItem)){
								scope.isLoCompleted = false;
							}
						}
					}
				};
				/**
				 * Redirect ot next LO if it is unlocked and current LO is completed
				 */
				scope.nextLearningObject = function(){
					if ( isItemAccessible(nextItem)){
						PlayerActions.requestPlay(nextItem['key'], nextItem.idScormItem, nextItem);
					}
				};
				/**
				 * Return *true* if you are in learning object
				 */
				scope.showArrow = function () {
					if ( typeof scope.$showNextArrow != 'undefined' && scope.$showNextArrow){
						return scope.$showNextArrow;
					} else {
						return false;
					}
				};
				/**
				 * Small sorting function to sort the array of items as in the ItemsList
				 * @param a - first param
				 * @param b - second param
				 * @returns {number} sorted number
				 */
				var sortById = function (a, b) {
					if ( a.id_organization < b.id_organization)
						return -1;
					else
						return 1;
					return 0;
				};
				/**
				 * Check is Learning Object accessible for user
				 * @param item Metadata of the Learning Object
				 * @returns {boolean} true if user can enter
				 */
				var isItemAccessible = function(item){
					if ( typeof item != 'undefined')
						return (item != false && item['locked'] == false);
				};
				/* Init PlayerStore */
				PlayerStore.register(function () {
					construct();
				});
			}
		};
	}
	NextArrow.$inject = ['CourseStore', 'PlayerStore', 'PlayerActions'];
	return NextArrow;
});