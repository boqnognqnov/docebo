'use strict';
define(function () {
	function CourseHeader(CourseStore, HTMLDispatcher, NativeAPI, ExtractService) {
		return {
			templateUrl: view('CoursePlayer.courseHeader.course_header'),
			link: function(scope){
				scope.course = CourseStore.course();
				scope.items = CourseStore.items();
				CourseStore.register(function(){
					scope.course = CourseStore.course();
					HTMLDispatcher.ChangeTitle(scope.course.name);
				});


				/**
				 * Are we going to show the offline button
				 * Show it only if it is in mobile application and there is content do download
				 * @returns {boolean}
				 */
				scope.isButtonAvailable = function () {
					var offlineItems = ExtractService.extractChapters(scope.items);
					return (NativeAPI.isMobile() && offlineItems.length > 0) && isOnline();
				};

				/**
				 * download course from native app  implement lo download in course player
				 */
				scope.downloadCourse = function () {
					var chapters = ExtractService.extractChapters(scope.items);
					scope.$openOfflinePage(scope.course.idCourse, scope.course.name, chapters);
					NativeAPI.dispatch('isControllerLoaded', "true");
				};
			}
		};
	}
	CourseHeader.$inject = ['CourseStore', 'HTMLDispatcher', 'NativeAPI', 'ExtractService'];
	return CourseHeader;
});