'use strict';
/* global angular */
define(function () {
	EmbedPlayer.$inject = ['$sce', 'EmbedLinkParser', 'PlayerStore', 'CourseStore', "URLManager", 'NativeAPI', '$compile'];
	function EmbedPlayer($sce, EmbedLinkParser, PlayerStore, CourseStore, URLManager, NativeAPI, $compile) {
		return {
			scope:{
				playbackUrl: '@playbackUrl',
				videoType: '@videoType',
				courseId: '@courseId',
				idReference: '@idReference'	
			},
			templateUrl: view('CoursePlayer.embedPlayer.embed_player'),
			link: function(scope, elem){
				scope.isInitialized = false;
				var trackUrl = URLManager.createApiUrl("trackMaterial");
				var LinkGenerator = {
					youtube: EmbedLinkParser.parseYT,
					wistia: EmbedLinkParser.parseWistia,
					vimeo: EmbedLinkParser.parseVimeo
				};
				scope.url = $sce.trustAsResourceUrl(LinkGenerator[scope.videoType](scope.playbackUrl));
				var viewPath = view('CoursePlayer.player.embedPlayer', true);
				scope.view = function(viewName){
					var result = view('CoursePlayer.embedPlayer.'+viewName);
					return result;
				};
				var VIDEO_TYPE = {
					YOUTUBE: 'youtube',
					WISTIA: 'wistia',
					VIMEO: 'vimeo'
				};
				scope.$on('closePlayer', function(){
					switch(scope.videoType){
						case VIDEO_TYPE.YOUTUBE:
								$('#youtube').remove();
							break;
						case VIDEO_TYPE.WISTIA:
								$('#wistia').remove();
							break;
						case VIDEO_TYPE.VIMEO:
								$('#vimeo').remove();
							break;
					};
					scope.videoType = undefined;
					scope.isInitialized = false;
				});
				var construct = function(type){
					if(!scope.isInitialized){
						var track =  CourseStore.getUserTrack(scope.idReference);
						var bookmark = track.data !== "" ? track.data.bookmark : 0;
						switch(type){
							case VIDEO_TYPE.YOUTUBE:
								var urlSplit = scope.playbackUrl.split('v=');
								new YoutubeCustomPlayer({
									trackUrl: trackUrl,
									course_id: scope.courseId,
									idReference: scope.idReference,
									statusAttempted: PlayerStore.STATUS.IN_PROGRESS,
									statusIntro: PlayerStore.STATUS.NOT_STARTED,
									statusCompleted: PlayerStore.STATUS.COMPLETED,
									videoId: urlSplit[1],
									bookmark: bookmark,
									headers: {
										'Authorization': NativeAPI.dispatch('getAuthData', '')
									}
								});
								break;
							case VIDEO_TYPE.WISTIA:
								new WistiaCustomPlayer({
									trackUrl: trackUrl,
									course_id: scope.courseId,
									idReference: scope.idReference,
									statusAttempted: PlayerStore.STATUS.IN_PROGRESS,
									statusIntro: PlayerStore.STATUS.NOT_STARTED,
									statusCompleted: PlayerStore.STATUS.COMPLETED,
									videoId: scope.url,
									bookmark: bookmark,
									url: scope.playbackUrl,
									headers: {
										'Authorization': NativeAPI.dispatch('getAuthData', '')
									}
								});
								break;
							case VIDEO_TYPE.VIMEO:
								new VimeoCustomPlayer({
									trackUrl: trackUrl,
									course_id: scope.courseId,
									idReference: scope.idReference,
									statusAttempted: PlayerStore.STATUS.IN_PROGRESS,
									statusIntro: PlayerStore.STATUS.NOT_STARTED,
									statusCompleted: PlayerStore.STATUS.COMPLETED,
									videoId: scope.url,
									bookmark: bookmark,
									url: scope.playbackUrl,
									headers: {
										'Authorization': NativeAPI.dispatch('getAuthData', '')
									}
								});
								break;
						}
					}
				};
				PlayerStore.register(function(){
					var lo = PlayerStore.getLo();
					var isListHidden = angular.element('#course_list').css('display') != 'block';
					if( typeof lo !='undefined' && typeof lo.video_type != 'undefined' && isListHidden){
						scope.videoType = lo.video_type;
						scope.playbackUrl = PlayerStore.launchUrl();
						construct(lo.video_type);
						scope.isInitialized = true;
					}
				});
				scope.$watch('videoType', function(type){
					if(typeof type != 'undefined'){
						construct(type);
					}
				});
			}
		};
	}
	return EmbedPlayer;
});