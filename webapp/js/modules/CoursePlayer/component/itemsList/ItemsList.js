'use strict';
define(function () {
	function ItemList(CourseStore, PlayerActions, PlayerStore, NativeAPI) {
		return {
			templateUrl: view('CoursePlayer.itemsList.item_list'),
			link: function(scope){

				scope.items = CourseStore.items();
				scope.count = CourseStore.itemsCount();
				scope.isAccessible = function(type) {
					var result = false;
					var accessible = [
						"htmlpage",
						"sco",
						"test",
						"video",
						"tincan",
						"elucidat",
						"slides",
						"authoring",
						"googledrive",
						"file",
						"aicc"
					];
					if(accessible.indexOf(type) !== -1){
						result = true;
					}
					return result;
				};
				scope.enterPlayer = function(object) {
					PlayerActions.requestPlay(object.key, object.idScormItem, object);
				};
				/**
				 * Check if the item has prerequisites, is it locked by any way
				 * @param item metadata of the object
				 */
				scope.isLocked = function (item) {
					var answer = false;
					if ( !item) // Invalid Item - Lock it
						answer = true;
					if ( item.locked ) // If it is locked - Lock it
						answer = true;
					/* If this item have prerequisite and is not completed lock the item */
					if ( item.type != PlayerStore.TYPE.SCORM && item.prerequisites && prerequisitesCompleted(item.prerequisites)){
						answer = false; // if it is not scorm chapter and all prerequisites are completed - Unlock it
					} else if (item.type != PlayerStore.TYPE.SCORM && item.prerequisites && !prerequisitesCompleted(item.prerequisites))
						answer = true; // if it is not scorm chapter and at least one prerequisite is not complete - Lock it
					if ( item.type === PlayerStore.TYPE.SCORM && item.locked){
						answer = true; // if it is locked scorm - Lock it
					}
					if ( item.prerequisites == '' || !item.locked )
						answer = false; // if no prerequisites and unlocked - Unlock it
					if ( item.type === PlayerStore.TYPE.TEST ){ // if is test
						if ( item.self_prerequisite === "NULL" && (item.status_class === PlayerStore.STATUS_CLASS.COMPLETED || item.status_class === PlayerStore.STATUS_CLASS.FAILED)){ // if allow enter only once
							answer = true;
							// If allowed enter only until completed
						} else if ( item.self_prerequisite === 'incomplete' && item.status_class === PlayerStore.STATUS_CLASS.COMPLETED){
							answer = true;
						}
					}
					return answer;
				};
				/**
				 * return true if all are true
				 * @param prerequisites can be one ID or multiple comma separated
				 */
				var prerequisitesCompleted = function(prerequisites){
					if ( !prerequisites || typeof prerequisites != 'string')
						return;
					var array = prerequisites.split(',');
					if ( array.length > 1){
						for ( var i = 0; i < array.length; i++){
							if ( !isLoCompleted(array[i])){
								return false;
							}
						}
					} else {
						if ( !isLoCompleted(prerequisites))
							return false;
					}
					return true;
				};
				/**
				 *
				 * @param id - id_organization of the Learning Object
				 * @returns {boolean} true if status is completed
				 */
				var isLoCompleted = function(id){
					if ( CourseStore.getLoStatus(id) !== PlayerStore.STATUS_CLASS.COMPLETED )
						return false;
					return true;
				};
				/**
				 * Register function
				 */
				CourseStore.register(function(){
					scope.items = CourseStore.items();
					scope.count = CourseStore.itemsCount();
					scope.items = Object.keys(scope.items).map(function (key) {
						return scope.items[key];
					});
					if(NativeAPI.isMobile()){
						var data = {
							idCourse: CourseStore.id(),
							storeData: CourseStore.getFullStore()
						};
						NativeAPI.dispatch('cacheCourseStore', data);
					}
				});
			}
		};
	}
	ItemList.$inject = ['CourseStore', 'PlayerActions', 'PlayerStore', 'NativeAPI'];
	return ItemList;
});