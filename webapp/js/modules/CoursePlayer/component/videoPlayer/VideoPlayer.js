'use strict';
/* global angular */
define(function () {
	VideoPlayer.$inject = ['Config', 'TrackingService', 'CourseStore', 'PlayerStore'];
	function VideoPlayer(Config, TrackingService, CourseStore, PlayerStore) {
		return {
			scope:{
				playbackUrl: '@playbackUrl',
				thumbUrl: '@thumbUrl',
				loId: '@loId'
			},
			templateUrl: view('CoursePlayer.videoPlayer.video_player'),
			link: function(scope, elem){
				TrackingService.fp = {};
				var userTrack = CourseStore.getUserTrack(scope.loId);
				var bookmark = userTrack.data.bookmark;
				var subtitles = PlayerStore.getSubtitles();
				var fp = null;
				var target = angular.element('#flowplayer_container');
				scope.intervalId = null;
				scope.$watch('playbackUrl', function(nv, ov){
					if(nv != ''){
						if(fp != null ){
							fp.shutdown();
						}
						var fpSubs = [];
						if(typeof subtitles != 'undefined'){
							fpSubs = [{ 
								"default": true,
								kind: "subtitles", 
								srclang: subtitles.lang_code, 
								label: "English",
								src:  subtitles.url
							}];
						}
						fp = flowplayer(target, {
							key: Config.fpLicense(),
							autoplay: false,
							nativesubtitles: true,
							native_fullscreen: true,
							splash: true,
							fullscreen: true,
							seekable: true,
							clip: {
								subtitles: fpSubs,
								sources: [
									{
										autoplay: false,
										native_fullscreen: true,
										fullscreen: true,
										type: "video/mp4",
										src: scope.playbackUrl
									}
								]
							}
						});
						TrackingService.setFlowPlayer(fp);
						fp.on('ready', function () {
							TrackingService.setVideoPlayed(true);
							var currentTime = fp.video.time;
							if ( parseInt(bookmark) !== 0 && typeof bookmark != 'undefined'){
								fp.seek(bookmark, function () {
									var percentage = ( currentTime / fp.video.duration) * 100;
									angular.element('.fp-progress').css('width', percentage+'%');
								});
							}
							scope.intervalId = window.setInterval(function () {
								TrackingService.setVideoStatus();
								scope.$emit('disableLoading');
							}, 5000);
						});
						fp.on('pause', function () {
							TrackingService.setVideoStatus();
							closeInterval(scope.intervalId);
							scope.$emit('disableLoading');
						});
						fp.on('stop', function () {
							closeInterval(scope.intervalId);
						});
						angular.element('.top-close-object').on('click', function (e) {
							closeInterval(scope.intervalId);
							if(fp){
								fp.unload();
							}
							TrackingService.setVideoPlayed(false);
							e.preventDefault();
						});
						fp.on('resume', function () {
							scope.intervalId = window.setInterval(function () {
								TrackingService.setVideoStatus();
								scope.$emit('disableLoading');
							}, 5000);
						});
					}
				});
				/**
				 * Check is such ID and is the windwo object exist
				 * @param intervalId - id of the interval that has to be closed
                 */
				var closeInterval = function (intervalId) {
					if ( intervalId !== false && window){
						clearInterval( intervalId );
                        intervalId = null;
					}
				};
			}
		};
	}
	return VideoPlayer;
});