'use strict';
define(function () {
	function FallbackMessage(CourseStore) {
		return {
			scope: {
				headMessage: '=headMessage',
				secondaryMessage: '=secondaryMessage'
			},
			templateUrl: view('CoursePlayer.fallbackMessage.fallback_message'),
			link: function(scope){
				scope.show = CourseStore.itemsCount() > 0;
				CourseStore.register(function(){
					scope.show = CourseStore.itemsCount() > 0;
				});
			}
		};
	}
	FallbackMessage.$inject = ['CourseStore'];
	return FallbackMessage;
});