'use strict';
/* global angular */
define(function () {
	LoPlayer.$inject = ['Utils', 'PlayerStore', 'HTMLDispatcher', 'PlayerActions', 'NativeAPI', '$sce', '$window', 'DeviceOrientation', 'CourseActions', 'CourseStore', 'TrackingService', 'PromiseService', '$rootScope', '$timeout', 'URLManager'];
	function LoPlayer(Utils, PlayerStore, HTMLDispatcher, PlayerActions, NativeAPI, $sce, $window, DeviceOrientation, CourseActions, CourseStore, TrackingService, PromiseService, $rootScope, $timeout, URLManager) {
		return {
			templateUrl: view('CoursePlayer.player.lo_player'),
			link: function(scope){
				var viewPath = view('CoursePlayer.player.lo_player', true);
				
				scope.view = function(viewName){
					return viewPath + '_partials/' + viewName;
				};
				scope.downloadFile = function(downloadPath) {
					NativeAPI.dispatch('FileLoAction', downloadPath);
					PlayerActions.trackObject(PlayerStore.STATUS.COMPLETED);
					setLoStatus(PlayerStore.STATUS.COMPLETED);
				};
				$(window).on('updateCourseStore', function(e){
					var statusClassKey = Utils.getKeyByValue(e.status, PlayerStore.STATUS);
					var statusClass = PlayerStore.STATUS_CLASS[statusClassKey];
					if ( CourseStore.getLoStatus(scope.metadata['key']) === PlayerStore.STATUS_CLASS.COMPLETED)
						statusClass = PlayerStore.STATUS_CLASS.COMPLETED;
					PlayerActions.updateEmbedVideoTrack(e.idReference, e.status, e.bookmark, statusClass);
				});
				scope.$on('closePlayer', function(e){
					if ( PlayerStore.getType())
						$(window).trigger('loPlayerClosed', { type: PlayerStore.getType()});
					if(PlayerStore.getType() != PlayerStore.TYPE.AUTHORING) {
						PlayerActions.closePlayer(scope.metadata);
						setLoStatus(scope.metadata['status']);
					} else {
						TrackingService.setAuthoringStatus(scope.metadata, scope.currentSlide);
						PlayerActions.trackObject(scope.metadata.status, scope.metadata.status_class);
						PlayerActions.updateSlide(scope.metadata.key, scope.currentSlide);
					}
					PlayerActions.empty();
					$window.removeEventListener('orientationchange', scope.rotate, false);
					scope.loStatus = CourseStore.getLoStatus(scope.metadata.key);
					CourseActions.applyStatus(scope.loStatus, scope.metadata.key); // update lo status
					NativeAPI.dispatch('isCoursePlayer', "true"); // set flag to native device(Android) is course player active
				});
				scope.rotate = function(){
					var orientation = DeviceOrientation.getDeviceOrientation();
					var navBar = angular.element('#top-bar');
					var frame = angular.element('.frame-wrapper iframe');
					var loPlayer = angular.element('#lo_player');
					var userAgent = window.navigator.userAgent;
					var fileAuthoring = angular.element("#file-authoring .swiper-wrapper .swiper-slide .col-file-item img");
					if ( fileAuthoring.length ){
						if (orientation === DeviceOrientation.ORIENTATION.LANDSCAPE || orientation === DeviceOrientation.ORIENTATION.LANDSCAPE_LEFT){
							fileAuthoring.css({
								'object-fit' : 'contain'
							});
						}else{
							fileAuthoring.css({
								'object-fit' : 'fill'
							});
						}
					}
					if ( NativeAPI.device() === NativeAPI.OS.iOS){
						if(orientation === DeviceOrientation.ORIENTATION.PORTRAIT || orientation === DeviceOrientation.ORIENTATION.UPSIDE_DOWN){
							navBar.show('slow');
							frame.css('height', 'calc(100% - 72px)');
							loPlayer.css({
								'margin-top' : '72px',
								'height'	 : 'calc(100% - 72px)'
							});
						}else{
							navBar.hide('slow');
							frame.css('padding-top', '30px');
							frame.css('height', '100%');
							loPlayer.css({
								'width' 	 : '100%',
								'height'	 : '100%',
								'margin-top' : '0px'
							});
						}
					}else if((userAgent.match(/iPad/i) || userAgent.match(/iPhone/i))){
						if(orientation == DeviceOrientation.ORIENTATION.PORTRAIT || orientation == DeviceOrientation.ORIENTATION.UPSIDE_DOWN){
							navBar.show('slow');
							frame.css('padding-top', '52px');
							frame.css('height', 'calc(100% - 72px)');
						}else{
							navBar.hide('slow');
							frame.css('padding-top', '30px');
							frame.css('height', '100%');
						}
					} else {
						if(orientation == DeviceOrientation.ORIENTATION.PORTRAIT || orientation == DeviceOrientation.ORIENTATION.UPSIDE_DOWN){
							navBar.show('slow');
							frame.css('padding-top', '52px');
							frame.css('height', 'calc(100% - 52px)');
						}else{
							navBar.hide('slow');
							frame.css('padding-top', '0px');
							frame.css('height', '100%');
						}
					}

				};
				/**
				 *
				 * @param index - current slide when user last viewed
				 */
				var setAuthoringStatus = function (index) {
					scope.currentSlide = index;
					var current = scope.metadata.status;
					var slidesCount = ( scope.slides ? scope.slides.length : false);
					if ( slidesCount === false) $window.location.href = '#/course/'+scope.courseId;
					if(index == slidesCount) {
						TrackingService.setTrackStatus(scope.metadata);
						scope.metadata.status 	    = PlayerStore.STATUS.COMPLETED;
						scope.metadata.status_class = PlayerStore.STATUS_CLASS.COMPLETED;
					} else if ( index < slidesCount && scope.metadata.status != PlayerStore.STATUS.COMPLETED){
						scope.metadata.status	    = PlayerStore.STATUS.IN_PROGRESS;
						scope.metadata.status_class = PlayerStore.STATUS_CLASS.IN_PROGRESS;
					}
					if(current == PlayerStore.STATUS.COMPLETED){
						scope.metadata.status 	    = PlayerStore.STATUS.COMPLETED;
						scope.metadata.status_class = PlayerStore.STATUS_CLASS.COMPLETED;
					}
				};
				/**
				 * Update metadata with current status of LO and if completed raise event
				 * @param status - constants from STATUS in PlayerStore
				 * @returns {boolean} - false if no or invalid status is passed
				 */
				var setLoStatus = function(status){
					if ( !status || typeof scope.metadata === 'undefined') return;
					switch (status){
						case PlayerStore.STATUS.IN_PROGRESS:{
							scope.metadata.status       = PlayerStore.STATUS.IN_PROGRESS;
							scope.metadata.status_class = PlayerStore.STATUS_CLASS.IN_PROGRESS;
							CourseActions.applyStatus(PlayerStore.STATUS_CLASS.IN_PROGRESS, scope.metadata.key);
						}break;
						case PlayerStore.STATUS.COMPLETED:{
							scope.metadata.status       = PlayerStore.STATUS.COMPLETED;
							scope.metadata.status_class = PlayerStore.STATUS_CLASS.COMPLETED;
							CourseActions.applyStatus(PlayerStore.STATUS_CLASS.COMPLETED, scope.metadata.key);
							scope.$emit('learningObjectCompleted', scope.metadata);
						}break;
						case PlayerStore.STATUS.FAILED:{
							scope.metadata.status       = PlayerStore.STATUS.FAILED;
							scope.metadata.status_class = PlayerStore.STATUS_CLASS.FAILED;
							CourseActions.applyStatus(PlayerStore.STATUS_CLASS.FAILED, scope.metadata.key);
						}break;
					}
					return true;
				};
				/**
				 *
				 */
				var updateTestStatus = function () {
					$window.addEventListener('message', function (event) {
						if ( event.data && event.data.type === 'testStatus'){
							switch ( event.data.status){
								case 'in_progress':
									setLoStatus(PlayerStore.STATUS.IN_PROGRESS);
									break;
								case true:
									setLoStatus(PlayerStore.STATUS.COMPLETED);
									break;
								case false:
									setLoStatus(PlayerStore.STATUS.FAILED);
									break;
							}
						}
						PlayerActions.trackObject(scope.metadata.status, scope.metadata.status_class);
					});
				};
				/**
				 * set slide converter current page
				 */
				var updateSlideStatus = function(){
					scope.slides = PlayerStore.getSlides();
					var track = CourseStore.getUserTrack(scope.metadata.id_organization);

					scope.currentSlide = (track && track.data !== '' ? track.data.slideNumber : false);
					if (typeof scope.currentSlide == false || scope.currentSlide < 1) {
						scope.currentSlide = 1;
					}
					setAuthoringStatus(scope.currentSlide);

					$timeout(function(){
						$rootScope.$broadcast('setCurrentSlide', scope.currentSlide);
					}, 200);

					scope.$on('onChangeSlider', function(event, index) {
						setAuthoringStatus(index);
					});
				};

				scope.$on('nextLearningObject', function () {
					var items = CourseStore.items();
				});
				/**
				 *  This message is received from mobileapp/test
				 *  When you click on buttons and navigate trough the test object
				 */
				var attachLoaderEvent = function(){
					window.addEventListener('message', function (event) {
						if ( event && event.data === 'enable'){
							scope.$emit('enableLoading');
						}
						if ( event.data.event_id === 'disable'){
							if ( event.data.data.url != 'empty'){
								var iframe = document.getElementById('learning-object-iframe');
								iframe.src = event.data.data.url;
							}
							scope.$emit('disableLoading');
						}
					}, false);
				};

				var construct = function(){
					scope.rotate();
					var loType = PlayerStore.getType();
					scope.show = PlayerStore.isReady();
					scope.metadata = PlayerStore.getLo();
					scope.launchUrl = ( loType === PlayerStore.TYPE.FILE ? PlayerStore.launchUrl() : $sce.trustAsResourceUrl(PlayerStore.launchUrl()));
					scope.courseId = CourseStore.id();
					switch(loType){
						case PlayerStore.TYPE.AUTHORING:
							updateSlideStatus();
							break;
						case PlayerStore.TYPE.FILE:
							// Wait until file is downloaded
							break;
						case PlayerStore.TYPE.HTML:
							setLoStatus(PlayerStore.STATUS.COMPLETED);
							break;
						case PlayerStore.TYPE.TEST:
							attachLoaderEvent();
							updateTestStatus();
							break;
						case PlayerStore.TYPE.GOOGLEDRIVE:
							setLoStatus(PlayerStore.STATUS.COMPLETED);
							break;
						default:
							setLoStatus(PlayerStore.STATUS.COMPLETED);
							break;
					}
					if(scope.metadata && scope.metadata.title){
						HTMLDispatcher.ChangeTitle(scope.metadata.title);
						scope.thumbUrl = scope.metadata.video_thumb;
						scope.loId = scope.metadata.id_organization;
					}
					if(scope.show){
						angular.element('#course_list').hide();
					}else{
						angular.element('#course_list').show();
					}
					$window.addEventListener('orientationchange', scope.rotate, false);
				};
				PlayerStore.register(function(){
					construct();
				});
				construct();
			}
		};
	}
	return LoPlayer;
});