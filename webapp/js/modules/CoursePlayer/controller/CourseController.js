define(function () {
	CourseController.$inject = ['$scope', 'PromiseService', '$routeParams', 'CourseActions', 'PlayerActions', 'CourseStore', 'NativeAPI'];
	function CourseController($scope, PromiseService, $routeParams, CourseActions, PlayerActions, CourseStore, NativeAPI){
		$scope.courseId = $routeParams.courseId;
		NativeAPI.dispatch('isCoursePlayer', "true"); // set flag to native device(Android) is course player active
		$scope.init = function(){
			if(CourseStore.id() !== $scope.courseId){
				CourseActions.init($scope.courseId);
			}
			if ( NativeAPI.isMobile()){
				setTimeout(function(){
					NativeAPI.dispatch('getDownloadedObjects', { courseId:$routeParams.courseId });
				}, 500);
			}
		};
	};
	return CourseController;
});