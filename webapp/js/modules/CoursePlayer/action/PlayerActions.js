define(function () {
	PlayerActions.$inject = ['dispatcher', 'PromiseService', 'TrackingService', 'CourseActions', '$routeParams', '$timeout', 'PlayerStore', 'NativeAPI', 'CourseStore'];
	function PlayerActions(dispatcher, PromiseService, TrackingService, CourseActions, $routeParams, $timeout, PlayerStore, NativeAPI, CourseStore){
		return {
			empty: function(){
				dispatcher.emit({
					actionType: 'EMPTY'
				});
			},
			requestPlay: function(loid, scormId, lo){ // , lo.type
				var params = {};
				var launchParams = CourseStore.getLaunchParams(lo.key);
				if(lo.type === PlayerStore.TYPE.SCORM){
					params.scormVersion = CourseStore.getScoVersion(loid);
					if(!isOnline()){
						params.entryPoint = CourseStore.getScormEntryPoint(loid);
						//params.initialCMI = encodeURIComponent(CourseStore.getScormCMI(loid));
						NativeAPI.dispatch('setInitialCMI', encodeURIComponent(CourseStore.getScormCMI(loid)));
					}
				}
				PromiseService.create('play', {id: loid, scormItemId: scormId, loType: lo.type, auth_code: launchParams['auth_code'], params: params}).then(function(data){
					NativeAPI.dispatch('isCoursePlayer', "false"); // set flag to native device(Android) is course player active
					var slides = typeof data.slides != 'undefined' ? data.slides : [];
					dispatcher.emit({
						actionType: 'REQUEST_PLAY',
						scormItemId: scormId,
						launchUrl: data.url,
						slides: slides,
						loMetadata: lo,
						loid: loid
					});
				});
			},
			closePlayer: function(metadata){
				// INVOKE SERVICE TRACKING
				var actionDispatcher = function () {
					TrackingService.setTrackStatus(metadata)
						.then(function(res){
							dispatcher.emit({
								actionType: 'CLOSE_PLAYER',
								loMetadata: metadata
							});
						});
				};
				if ( metadata.type === 'video' && metadata.video_type == 'upload'){
					TrackingService.setTrackStatus( metadata ).then(function () {
						actionDispatcher();
					});
				} else if(metadata.type === PlayerStore.TYPE.SCORM || metadata.type === PlayerStore.TYPE.AICC
					|| metadata.type === PlayerStore.TYPE.TINCAN){
					$timeout(function () {
						CourseActions.init($routeParams.courseId);
					});
					actionDispatcher();
				} else {
					actionDispatcher();
				}
			},
			/**
			 *
			 * @param status
             * @param statusClass
             */
			trackObject: function(status, statusClass){
				dispatcher.emit({
					actionType: 'TRACK_OBJECT',
					status: status,
					status_class: statusClass
				});
			},
			updateSlide: function(id, slide){
				dispatcher.emit({
					actionType: 'UPDATE_SLIDE',
					id: id,
					slideNumber: slide
				});
			},
			updateEmbedVideoTrack: function(idReference, status, bookmark, statusClass){
				dispatcher.emit({
					actionType: 'UPDATE_EMBED_VIDEO_TRACK',
					id: idReference,
					bookmark: bookmark,
					status: status,
					statusClass: statusClass
				});
			}
		};
	};
	return PlayerActions;
});