define(function () {
	CourseActions.$inject = ['dispatcher', 'PromiseService'];
	function CourseActions(dispatcher, PromiseService){
		return {
			init: function(id){
				PromiseService.create('course', {id: id}).then(function(data){
					dispatcher.emit({
						actionType: 'INITIALIZE',
						data: data,
						courseId: id
					});
				});
            },
			unauthorized: function(){
				dispatcher.emit({
                    actionType: 'UNAUTHORIZED'
                });
			},
			applyStatus: function (status, id) {
				dispatcher.emit({
					actionType: 'APPLY_STATUS',
					loStatus: status,
					loId: id
				});
			}
		};
	};
	return CourseActions;
});