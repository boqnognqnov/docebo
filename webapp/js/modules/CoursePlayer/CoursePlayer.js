'use strict';
var path = 'js/modules/CoursePlayer/bootstrap/';
require([path+'routing.js', path+'components.js', path+'actions.js', path+'stores.js', path+'controllers.js', path+'../component/embedPlayer/embed_lib.js'], function(routing) {
    var module = angular.module('CoursePlayer', ['CoursePlayer.Components', 'CoursePlayer.Stores', 'CoursePlayer.Actions', 'CoursePlayer.Controllers']);
	module.config(routing);
});