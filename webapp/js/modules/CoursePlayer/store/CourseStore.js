'use strict';
define(function () {
	function CourseStore(dispatcher, PlayerStore) {
		var _store = this;
		dispatcher.register(function(action){
            switch (action.actionType){
                case 'INITIALIZE':
					_store.data = {};
					_store.init();
					_store.cid = action.courseId;
					_store.data = action.data;
					_store.emitChange();
					break;
				case 'CLOSE_PLAYER':
					var id = action['loMetadata']['key'];
					if ( action['loMetadata']['type'] === 'video' && action['loMetadata']['video_type'] == 'upload'){
						if(typeof _store.data['lo_user_track'][id].data == "string"){
						 _store.data['lo_user_track'][id].data = {};
						}
						_store.data['lo_user_track'][id].data.bookmark = action.loMetadata.bookmark;
					}
					_store.data['lo_status'][id] = action.loMetadata.status;
					_store.data['lo_metadata'][id]['status'] = action.loMetadata.status;
					_store.data['lo_metadata'][id]['status_class'] = action.loMetadata.status_class;
					_store.emitChange();
					break;
				case 'APPLY_STATUS'://learningObjectCompleted
					_store.data.lo_metadata[action.loId].status_class = action.loStatus;
					_store.emitChange();
					break;
				case 'UPDATE_SLIDE':
					if ( _store.data.lo_user_track[action.id].data != '')
						_store.data.lo_user_track[action.id].data.slideNumber = action.slideNumber;
					_store.emitChange();
					break;
				case 'UPDATE_EMBED_VIDEO_TRACK':
					if(typeof _store.data['lo_user_track'][action.id].data == "string"){
						_store.data['lo_user_track'][action.id].data = {};
					}
					_store.data.lo_user_track[action.id].data.bookmark = action.bookmark;
					_store.data.lo_user_track[action.id].data.status = action.status;
					_store.data.lo_metadata[action.id].status = action.status;
					_store.data.lo_metadata[action.id].status_class = action.statusClass;
					_store.emitChange();
					break;
			}
		});

		return {
			register:function(fn){ _store.register(fn); },
            items:function(){ return _store.data.lo_metadata; },
			course: function(){ return _store.data.course_metadata; },
			itemsCount: function(){ return _store.data.count; },
			tracking: function(){ return _store.data.lo_status; },
			getUserTrack: function(id){ return _store.data['lo_user_track'][id]; },
			id: function(){ return typeof _store.cid !== 'undefined' ? _store.cid: 0; },
			getLoStatus: function(id){ return  _store.data['lo_metadata'][id].status_class;},
			getFullStore: function(){ return _store.data; },
			getLaunchParams: function(id){ return _store.data['lo_launch_params'][id]; },
			getScoVersion: function(id){ return _store.data['lo_metadata'][id].scorm_version; },
			getScormCMI: function(id){ return _store.data['lo_user_track'][id].data.xmldata; },
			getScormEntryPoint: function(id){ return _store.data['lo_metadata'][id].entry_point; },
			getScormItems: function(id){ return _store.data['lo_metadata'][id].scorm_items; },
			getScormPrerequisites: function(id){ return _store.data['lo_metadata'][id].adlcp_prerequisites; }
		};
	}
	CourseStore.$inject = ['dispatcher', 'PlayerStore'];
	return CourseStore;
});