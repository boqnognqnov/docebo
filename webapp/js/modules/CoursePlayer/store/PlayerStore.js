'use strict';
define(function () {
	function PlayerStore(dispatcher) {
		var _store = this;
		dispatcher.register(function(action){
            switch (action.actionType){
				case 'EMPTY':
					_store.data = {};
					_store.emitChange();
					break;
				case 'REQUEST_PLAY':
					_store.data['lo_id'] = action.loid;
					_store.data['scormItemId'] = action.scormItemId;
					_store.data['launchUrl'] = action.launchUrl;
					_store.data['slides'] = action.slides;
					_store.data['ready'] = true;
					_store.data['loMetadata'] = action.loMetadata;
					_store.emitChange();
					break;
                case 'CLOSE_PLAYER':
                    _store.data['loMetadata'] = action.loMetadata;
                    _store.emitChange();
                    break;
                case 'TRACK_OBJECT':
                    _store.data['loMetadata']['status'] = action.status;
					_store.data['loMetadata']['status_class'] = action.status_class;
                    _store.emitChange();
                    break;
            }
		});

		return {
			TYPE: {
				SCORM: 		'sco',
				TEST: 		'test',
				HTML: 		'htmlpage',
				VIDEO: 		'video',
				AUTHORING: 	'authoring',
				FILE: 		'file',
				AICC: 		'aicc',
				TINCAN: 	'tincan',
				GOOGLEDRIVE:'googledrive',
				ELUCIDAT: 	'elucidat'
			},
			STATUS: {
				NOT_STARTED: 'ab-initio',
				IN_PROGRESS: 'attempted',
				COMPLETED:	 'completed',
				FAILED:		 'failed'
			},
			STATUS_CLASS: {
				NOT_STARTED: 'icon-play-circle gray',
				IN_PROGRESS: 'icon-play-circle yellow',
				COMPLETED: 	 'icon-completed green',
				FAILED:		 'icon-play-circle red'
			},
			register:function(fn){ _store.register(fn); },
            isReady: function(){ return typeof _store.data['ready'] !== 'undefined' ? _store.data['ready'] : false; },
			launchUrl: function(){ return _store.data['launchUrl']; },
			getLo: function(){ return _store.data['loMetadata']; },
			getLoId: function(){ return _store.data['lo_id']; },
			getSlides: function(){ return _store.data['slides']; },
			getType: function(){ return _store.data['loMetadata'] ? _store.data['loMetadata']['type'] : ''; },
			getSubtitles: function(){ return _store.data['loMetadata']['subtitles'];  }
		};
	}
	PlayerStore.$inject = ['dispatcher'];
	return PlayerStore;
});