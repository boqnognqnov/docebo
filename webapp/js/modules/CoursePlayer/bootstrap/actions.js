'use strict';
define(function (require) {
	var module = require('angular').module('CoursePlayer.Actions', ['CoursePlayer.Stores']);
	// Add action the same way <module.action('MyAction', 'action/filename');>
	module.action('CourseActions', require('modules/CoursePlayer/action/CourseActions'));
	module.action('PlayerActions', require('modules/CoursePlayer/action/PlayerActions'));
	return module;
});