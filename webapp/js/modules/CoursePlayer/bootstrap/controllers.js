'use strict';
define(function (require) {
	var module = require('angular').module('CoursePlayer.Controllers', ['CoursePlayer.Stores', 'CoursePlayer.Actions', 'DoceboMobile.services']);
	// Add controllers the same way <module.controller('MyController', 'controller/filename');>
	module.controller('CourseController', require('modules/CoursePlayer/controller/CourseController'));
	return module;
});