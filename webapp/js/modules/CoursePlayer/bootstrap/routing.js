'use strict';
/*
 * Here we're going to define module routing
 *  
 */
define([], function () {
	function CoursePlayerRouting($routeProvider, $injector) {
		$routeProvider.when('/course/:courseId', {
			templateUrl:    view('CoursePlayer.course'),
			controller:     'CourseController'
		});
	}
	CoursePlayerRouting.$inject = ['$routeProvider', '$injector'];//Injecting dependencies
	return CoursePlayerRouting;
});