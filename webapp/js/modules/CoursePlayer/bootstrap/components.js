'use strict';
define(function (require) {
	var module = require('angular').module('CoursePlayer.Components', ['ngRoute', 'ngSanitize', 'CoursePlayer.Stores', 'CoursePlayer.Actions']);
	// Add components the same way <module.directive('MyComponent', 'component/filename');>
	module.directive('courseHeader', require('modules/CoursePlayer/component/courseHeader/CourseHeader'));
	module.directive('itemsList', require('modules/CoursePlayer/component/itemsList/ItemsList'));
	module.directive('fallbackMessage', require('modules/CoursePlayer/component/fallbackMessage/FallbackMessage'));
	module.directive('loPlayer', require('modules/CoursePlayer/component/player/LoPlayer'));
	module.directive('videoPlayer', require('modules/CoursePlayer/component/videoPlayer/VideoPlayer'));
	module.directive('slidePlayer', require('modules/CoursePlayer/component/authoring/SlidePlayer'));
	module.directive('embedPlayer', require('modules/CoursePlayer/component/embedPlayer/EmbedPlayer'));
	module.directive('nextArrow', require('modules/CoursePlayer/component/nextArrow/NextArrow'))
	return module;
});