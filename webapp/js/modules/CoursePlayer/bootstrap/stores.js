'use strict';
define(function (require) {
	var module = require('angular').module('CoursePlayer.Stores', []);
	module.store('CourseStore', require('modules/CoursePlayer/store/CourseStore'));
	module.store('PlayerStore', require('modules/CoursePlayer/store/PlayerStore'));
	// Add store the same way <module.store('MyStore', 'store/filename');>
	return module;
});