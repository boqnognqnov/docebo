'use strict';
define(['angular', 'angularRoute'], function() {
    angular.module('DoceboMobile', [
		'infinite-scroll',
		'DoceboMobile.controllers',
		'ngRoute',
		'DoceboMobile.services',
		'DoceboMobile.directives',
		'DoceboMobile.filters',
		'DoceboMobile.providers',
		'CoursePlayer'
	]);
});