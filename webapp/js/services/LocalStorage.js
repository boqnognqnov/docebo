'use strict';
define(function () {
    function LocalStorage(){
		var factory = {};
		/*
		 * Reference to local storage object
		 */
		factory._storageObject = window.localStorage;
		factory._privateModeStorageObject = {};
		/**
		* Retrieve object from local storage
		* @param key {String} if $name is empty return the JSON object from localStorage
		* @return {String|Object|Array|Bool|Int|Null}
		*/
		factory.get = function(key){
			var result;
			if(this.isLocalStorageSupported() !== false){
				result = this._storageObject.getItem(key);
			}else{
				result = this._privateModeStorageObject[key];
			}
			if(this.isJson(result)){
				return JSON.parse(result);
			}
			return result;
		};
		/**
		* Checks whether provided string is and parseable JSON
		* @param key {String} if $name is empty return the JSON object from localStorage
		* @Param data {String|Object|Array|Bool|Int} the data you wish to persistent store
		* @return {Void}
		*/
		factory.set = function(key, data){
			if(this.isLocalStorageSupported() !== false){
				if(typeof data === "object"){
					data = JSON.stringify(data);
				}
				this._storageObject.setItem(key, data);
			}else{
				this._privateModeStorageObject[key] = data;
			}
		};
		
		factory.remove = function(key){
			this.set(key, null);
		};
		/**
		* Checks whether provided string is and parseable JSON
		* @param str {String} if $name is empty return the JSON object from localStorage
		* @return {Bool}
		*/
		factory.isJson = function(str){
			try {
				JSON.parse(str);
			} catch (e) {
				return false;
			}
			return true;
		};
		/**
		* Get array piece from Array stored in the local storage
		* @param localItem {String} if $name is empty return the JSON object from localStorage
		* @param name {String} is the name of the value of the objct that should return
		* @return {String|Bool|Int|Object}
		*/
		factory.getItemFromArray = function(localItem, name) {
			if ( typeof localItem === 'string' && JSON.parse(window.localStorage.getItem(localItem)) !== null ) {
				var localObject = JSON.parse(window.localStorage.getItem(localItem));
				if ( typeof name === 'undefined') {
					return localObject;
				} else {
					localObject.name;
				}
			} else {
				return false;
			}
		};
		
		factory.isLocalStorageSupported = function(){
			
			if(window.localStorage.length > 0)
			{
				return true;
			} 
			else 
			{
				return false;
			}
		};
		return factory;
    }
    LocalStorage.$inject = [];
    return LocalStorage;
});