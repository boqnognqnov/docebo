/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/*
BEGIN
:
1
CONFIRMED
:
-1
END
:
2
OVERBOOKING
:
4
SUBSCRIBED
:
0
SUSPENDED
:
3
WAITING_LIST
:
-2*/

'use strict';
define(function() {
	function CourseStatus() {
		var self = this;
		self.STATUS = {
			BEGIN: 1,
			CONFIRMED: -1,
			END: 2,
			OVERBOOKING: 4,
			SUBSCRIBED: 0,
			SUSPENDED: 3,
			WAITING_LIST: -2	
		};
		return self;
	}
});