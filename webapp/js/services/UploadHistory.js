'use strict';
define(function() {
	function UploadHistory(Storage) {
		return{
			items: Storage.get('uploadHistory'), //Our data bind for upload history array
			/*
			 * Clear all items from upload history
			 * @return {Void}
			 */
			clearAll: function(){
				this.items = [];
				Storage.set('uploadHistory', this.items);
			},
			/* Removes item by asset id
			 * @param {Int} idAsset
			 * @return {Void}
			 */
			removeItem: function(idAsset){
				var found = this.items.filter(function(obj){
					if(obj.idAsset === idAsset){
						return true;
					}
				});
				if(found.length > 0){
					var index = this.items.indexOf(found[0]);
					this.items.splice(index, 1);
					this.save();
				}
			},
			/* Push object into upload history and saves the items array to upload history
			 * @param {Object} obj
			 * @return {Void}
			 */
			push: function(obj){
				if(this.items === null){
					this.items = [];
				}
				if(typeof obj.time === 'undefined'){
					obj.time = new Date().getTime();
				}
				this.items.push(obj);
				this.save();
			},
			/* Save items to local storage
			 * @return {Void}
			 */
			save: function(){
				Storage.set('uploadHistory', this.items);
				this.items = Storage.get('uploadHistory');
			},
			/* Getter for the items
			 * @return {Array}
			 */
			getItems: function(){
				if(this.items !== null){
					return this.items;
				}
				return [];
			}
		};
	};
	UploadHistory.$inject = ['LocalStorage'];
	return UploadHistory;
});