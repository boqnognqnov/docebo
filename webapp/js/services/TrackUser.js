/**
 * Created by kyuchukovv on 09-Jun-16.
 */
'use strict';
define(function() {
    function TrackUser($http){
        return {
            routeStack: [],
            /**
             *
             * @param route
             */
            pushRoute: function(route){
                if(route !== null){
                    this.routeStack.push(route);
                }
            },
            /**
             *
             * @returns {boolean}
             */
            popRoute: function(){
                var lastVisited = false;
                if (this.routeStack.length > 0){
                    this.routeStack.pop();
                    lastVisited = (this.routeStack.pop());
                    if ( typeof lastVisited === 'undefined' || lastVisited == ''){
                        return '/all';
                    }
                    while ( lastVisited.indexOf('/learning_object/') > -1){
                        lastVisited = this.routeStack.pop();
                    }
                }
                return lastVisited;
            },
            getRoutes: function(){
                if (this.routeStack !== null){
                    return this.routeStack;
                }
                return [];
            }

        };
    }
    TrackUser.$inject = ['$http'];
    return TrackUser;
});