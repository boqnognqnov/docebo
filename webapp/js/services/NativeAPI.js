'use strict';
define(function () {
    function NativeAPI(IOSDispatcher, HTMLDispatcher){
        return{
            OS: {
                Android: 'Android',
                iOS: 'iOS',
                Desktop: 'Desktop',
                Unknown: 'Unknown'
            },
            android: (typeof AndroidFunction != "undefined" ? AndroidFunction : {}), //Android Object reference
            desktop: HTMLDispatcher, // Desktop(HTML) reference
            ios: IOSDispatcher,//IOS Object reference
            _handlerList: {}, //List of available functions in the bridge
            /**
             * Initialisation of the native api bridge
             *
             * @returns {void}
             */
            init: function(){
                if(this.device() === this.OS.iOS){
                    this.ios.init();
                }
            },
            /**
             * Determine the mobile operating system.
             * This function either returns 'iOS', 'Android' or 'unknown'
             *
             * @returns {String}
             */
            device: function() {
                var userAgent = navigator.userAgent || navigator.vendor || window.opera;
                if( typeof window['isiPhone'] != 'undefined' && window['isiPhone'] == 1 ){
                    return this.OS.iOS;
                }else if( typeof window['isAndroid'] != 'undefined' && window['isAndroid'] == 1 ){
                    return this.OS.Android;
                }else{
                    return this.OS.Desktop;
                }
            },
            /**
             * Dispatch function to native api.
             * @param func Function name {String}
             * @param data Data that will be invoked in function {Object}
             * @returns {Void}
             * @throws {Exception} if the device OS is not Android neither IOS
             */
            dispatch: function(func, data){
                var os = this.device();
                if(typeof data === "object"){
                    data = JSON.stringify(data);
                }
                if(os === this.OS.Android){
                    return this._dispatchAndroid(func, data);
                }else if(os === this.OS.iOS){
                    return this._dispatchIOS(func, data);
                } else if ( os === this.OS.Desktop) {
                    return this._dispatchDesktop(func, data);
                }
                else{
                    throw "Unrecognized OS";
                }
            },
            /**
             * Dispatch function to android device.
             * @param func Function name {String}
             * @param data Data that will be invoked in function {Object}
             * @returns {Void}
             */
            _dispatchAndroid: function(func, data){
                if(typeof data != 'undefined'){
                    if (typeof data === 'object') {
                        data = JSON.parse(data);
                    }
					window[func] = this.android[func](data);
                    return window[func];
                }else{
					window[func] = this.android[func]();
                    return window[func];
                }
            },
            /**
             * Dispatch function to ios device.
             * @param func Function name {String}
             * @param data Data that will be invoked in function {Object}
             * @returns {Void}
             */
            _dispatchIOS: function(func, data){
                return this.ios.callNative(func, data);
            },
            /**
             * Dispatch function to javascript on the desktop browser
             * @param func - the name of the function
             * @param data
             * @private
             * @returns {Void}
             */
            _dispatchDesktop: function(func, data) {
                if (typeof data === 'object') {
                    data = JSON.parse(data);
                }
                return this.desktop[func](data);
            },
            /**
             * Register function that will be allowed to execute throught native environment.
             * @param name Handler name {String}
             * @param func Func definition {Callable}
             * @returns {Void}
             * @throws {Expection} if second parameter is not callable
             */
            registerFunc: function(name, func){
                if(typeof func === 'function'){
                    this._handlerList[name] = func;
                }else{
                    throw "NativeAPI::registerFunc second parameter must be callable function";
                }
            },
            /**
             * Call javascript function from native environment.
             * @param name Handler name {String}
             * @param data Function param {object}
             * @returns {Void}
             * @throws {Expection} if cannot find invoked function
             */
            callFunc: function(name, data){
                var handler = this._handlerList[name];
                if(typeof handler !== 'undefined'){
                    console.log("Handler with name "+name+" found");
                    handler(data);
                }else{
                    throw "NativeAPI::callFunc cannot find function with name "+ name;
                }
            },
            /**
             * Check is the device using the web view mobile
             * @returns {boolean}
             */
            isMobile: function(){
                var OS = this.device();
                return ( OS == this.OS.Android || OS == this.OS.iOS );
            },
			getBridgeResponse: function(name){
				return window[name];
			},
            /**
             * Provide the full path URL to the native application
             * to download it locally
             *
             * @param link S3 URL to the training Material
             */
			startDownload: function(link){
			    console.log(link);
            },
            cacheRequest: function(link){

            },
            cancelLoDownload: function () {

            },
            isCoursePlayer: function () {

            }
        };
    }
    NativeAPI.$inject = ['IOSDispatcher', 'HTMLDispatcher'];
    return NativeAPI;
});