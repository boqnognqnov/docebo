'use strict';
define(function () {
    function PromiseService($http, NativeAPI, URLManager, $q, CacheService) {
		this.create = function(endpoint, params, cache){
			if(typeof params == 'undefined'){
				params = {};
			}
			if(typeof cache == 'undefined'){
				cache = true;
			}
			var qInstance = $q.defer();
			$http.post(
				URLManager.createApiUrl(endpoint),
                params,
                {
					headers: {
						'Authorization': NativeAPI.dispatch('getAuthData', '')
					}
				}
            )
			.success(function(data){
				if(NativeAPI.isMobile() && !isOnline() && params.loType == "authoring"){
					data.slides = data.slides.split(",");
				}
				qInstance.resolve(data);
			})
			.error(function(error){
				qInstance.reject("Error resolving request:" + error);
			});
			if(cache && NativeAPI.isMobile() && isOnline() || cache && NativeAPI.isMobile() && endpoint == "trackMaterial"){
				qInstance.promise.then(function(response){
					CacheService.cacheRequest(endpoint, params, response);
				});
			}
			return qInstance.promise;
		};
	}
	PromiseService.$inject = ['$http','NativeAPI', 'URLManager', '$q', 'CacheService'];
	return PromiseService;
});