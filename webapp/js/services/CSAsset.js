'use strict';
define(function() {
	/* Coach and share asset ajax calls */
	function CSAsset($rootScope, $q, $http, NativeAPI, URLManager) {
		var rootContext = this; //Reference to this context
		/*
		 * Creates request for new asset
		 * @param {Int} type Type of the asset by defined constants in QueueItem.js
		 * @param {String} Filename New file name of the file
		 * @param {String} Original file name of the file
		 * @return {Promise}
		 */
		this.newAsset = function(type, filename, originalFilename){
			var qInstance = $q.defer();
			$http.post(
				URLManager.createApiUrl('new_upload'),
                {
					type: type,
					filename: filename,
					originalFilename: originalFilename
				},
                {
					headers: {
						'Authorization': NativeAPI.dispatch('getAuthData', '')
					}
				}
            )
			.success(function(data){
				qInstance.resolve(data);
			})
			.error(function(error){
				qInstance.reject("Error:" + error);
			});
			return qInstance.promise;
		};
		/*
		 * Check conversion state of uploaded asset(s) by their id's
		 * @param {Array} Array of integer's of the asset id's
		 * @return {Promise}
		 */
		this.checkConversionStatus = function(assetIds){
			$rootScope.$broadcast('stateLoader', {stateLoader: false} );
			$rootScope.$emit('disableLoading');
			var qInstance = $q.defer();
			$http.post(
                URLManager.createApiUrl('get_upload'),
                {
					assetIds: assetIds
				},
                {
					headers: {
						'Authorization': NativeAPI.dispatch('getAuthData', '')
					}
				}
            )
			.success(function(data){
				qInstance.resolve(data);
			})
			.error(function(error){
				qInstance.reject("Error:" + error);
			});
			return qInstance.promise;
		};
		/*
		 * Deletes asset from the database by its ID
		 * @param {Int} ID of the asset in the database
		 * @return {Promise}
		 */
		this.deleteAsset = function(idAsset){
			var qInstance = $q.defer();
			$http.post(
               URLManager.createApiUrl('delete_asset'),
                {
					idAsset: idAsset
				},
                {
					headers: {
						'Authorization': NativeAPI.dispatch('getAuthData', '')
					}
				}
            )
			.success(function(data){
				if(data.result == true){
					qInstance.resolve(data);
				}else{
					qInstance.reject("Error:" + data.error);
				}
			})
			.error(function(error){
				qInstance.reject("Error:" + error);
			});
			return qInstance.promise;
		};
		/*
		 * Gets thumbnail for asset by its ID
		 * @param {Int} ID of the asset in the database
		 * @return {Promise}
		 */
		this.getThumb = function(idAsset){
			var qInstance = $q.defer();
			$http.post(
                URLManager.createApiUrl('asset_thumb'),
                {
					idAsset: idAsset
				},
                {
					headers: {
						'Authorization': NativeAPI.dispatch('getAuthData', '')
					}
				}
            )
			.success(function(data){
				qInstance.resolve(data);
			})
			.error(function(error){
				qInstance.reject("Error:" + error);
			});
			return qInstance.promise;
		};



		/*
		 * Gets video for asset by its ID
		 * @param {Int} ID of the asset in the database
		 * @return {Promise}
		 */

		this.getVideoUrl = function(id) {
			$rootScope.$broadcast('stateLoader', {stateLoader: true} );
			$rootScope.$emit('enableLoading');
			var qInstance = $q.defer();
			$http.post(URLManager.createApiUrl('get_video_urls'),
					{
						'idAsset': id
					},
					{
						headers: {
							'Authorization': NativeAPI.dispatch('getAuthData', '')
						}
					})
					.success(function (data) {
						qInstance.resolve(data);
					})
					.catch(function (error) {
						qInstance.reject("Error:" + error);
					});
			return qInstance.promise;
		};


		/*
		 * Get all assets from the server which are uploaded by mobile device and are unpublished yet
		 * @return {Promise}
		 */
		this.getUnpublished = function(){
			var qInstance = $q.defer();
			$http.post(
                URLManager.createApiUrl('get_assets'),
                {},
                {
					headers: {
						'Authorization': NativeAPI.dispatch('getAuthData', '')
					}
				}
            )
			.success(function(data){
				qInstance.resolve(data);
			})
			.error(function(error){
				qInstance.reject("Error:" + error);
			});
			return qInstance.promise;
		};

		this.updateAsset = function(idAsset, assetTitle, assetDescription, isPrivate, channnelsCheckList, tagsToAssign){
			var qInstance = $q.defer();
			$http.post(
					URLManager.createApiUrl('update_upload'),
					{
						idAsset: 	 idAsset,
						title: 		 assetTitle,
						description: assetDescription,
						isPrivate: 	 isPrivate,
						channels: 	 channnelsCheckList,
						tags: 		 tagsToAssign
					}, {
						headers: {
							'Authorization': NativeAPI.dispatch('getAuthData', '')
						}
					}
			)
					.success(function(data){
						qInstance.resolve(data);
					})
					.error(function(error){
						qInstance.reject("Error: " + error);
					});
			return qInstance.promise;
		};
		
		/*
		 * Gets asset details by asset ID
		 * @param {Int} ID of the asset in the database
		 * @return {Promise}
		 */

		this.getAssetDetails = function(id, src) {
			var qInstance = $q.defer();
			$http.post(URLManager.createApiUrl('data'),
					{
						endpoint: 'asset_data',
						params: {
							id_asset: id,
							onlySrc: src
						}
					},
					{
						headers: {
							'Authorization': NativeAPI.dispatch('getAuthData', '')
						}
					})
					.success(function (data) {
						qInstance.resolve(data);
					})
					.catch(function (error) {
						qInstance.reject("Error:" + error);
					});
			return qInstance.promise;
		};
		
		this.getRelatedAssets = function(id, length, pageSize) {
			var qInstance = $q.defer();
			$http.post(URLManager.createApiUrl('data'),
					{
						endpoint: 'asset_similar',
						params: {
							id_asset: id,
							from: length,
							count: pageSize
						}
					},
					{
						headers: {
							'Authorization': NativeAPI.dispatch('getAuthData', '')
						}
					})
					.success(function (data) {
						qInstance.resolve(data);
					})
					.catch(function (error) {
						qInstance.reject("Error:" + error);
					});
			return qInstance.promise;
		};
		
		this.assetRating = function(id, ratingIndex, userID) {
			var qInstance = $q.defer();
			$http.post(URLManager.createApiUrl('data'),
					{
						endpoint: 'rate_asset',
						params: {
							id_asset: id,
							rating: ratingIndex,
							userId: userID
						}
					},
					{
						headers: {
							'Authorization': NativeAPI.dispatch('getAuthData', '')
						}
					})
					.success(function (data) {
						qInstance.resolve(data);
					})
					.catch(function (error) {
						qInstance.reject("Error:" + error);
					});
			return qInstance.promise;
		};
		
		this.assetInvitations = function(id) {
			var qInstance = $q.defer();
			$http.post(URLManager.createApiUrl('data'),
					{
						endpoint: 'invite_get_users',
						params: {
							id_asset: id
						}
					},
					{
						headers: {
							'Authorization': NativeAPI.dispatch('getAuthData', '')
						}
					})
					.success(function (data) {
						qInstance.resolve(data);
					})
					.catch(function (error) {
						qInstance.reject("Error:" + error);
					});
			return qInstance.promise;
		};
		this.assetSendInvitations = function(id, items, inviterID) {
			var qInstance = $q.defer();
			$http.post(URLManager.createApiUrl('data'),
					{
						endpoint: 'invite_set_users',
						params: {
							id_asset: id,
							invited_items: items,
							inviter: inviterID
						}
					},
					{
						headers: {
							'Authorization': NativeAPI.dispatch('getAuthData', '')
						}
					})
					.success(function (data) {
						qInstance.resolve(data);
					})
					.catch(function (error) {
						qInstance.reject("Error:" + error);
					});
			return qInstance.promise;
		};

	};
	CSAsset.$inject = ["$rootScope", "$q", "$http", 'NativeAPI', 'URLManager'];
	return CSAsset;
});