'use strict';
define(['aws'], function(AWS) {
	function AmazonS3(Config, $q, DoceboFile, $rootScope, Storage) {
		var rootContext = this; //Reference to this context
		/* Object representation of the file types */
		this.TYPES = {
			VIDEO: 'video',
			IMAGE: 'image'
		};
		var cfg = Storage.get('appSettings');
		cfg = cfg.configuration;
		this.uploadProcess = false; //Instance of the upload process to amazon
		this.bucketName = cfg.s3BucketName; //S3 bucket name
		this.paths = Config.getAmazonStoragePath(); //S3 Storage paths
		AWS.config.region = cfg.s3Region; //S3 Region
		AWS.config.credentials = new AWS.CognitoIdentityCredentials({
			IdentityPoolId: Config.identityPool
		});
		/*
		 * Gets asset type from constant representation and return their amazon path for upload
		 * @param {String}  Object representation of the file type
		 * return {String|Null}
		 */
		this.getAssetType = function(type){
			switch(type){
				case rootContext.TYPES.VIDEO:
					return rootContext.paths['videos'];
				case rootContext.TYPES.IMAGE:
					return rootContext.paths['images'];
				default:
					return null;
			}
		};
		/*
		 * Gets asset type from constant representation and return their amazon path for upload
		 * @param {String}  Object representation of the file type
		 * return {String|Null}
		 */
		this.generateAssetName = function(file){
			var filename = DoceboFile.hashName(15);
			var extension = DoceboFile.exportExtension(file.name);
			return filename+'.'+extension;
		};
		/*
		 * Starts upload to amazon
		 * @param {Blob} File you want to upload
		 * @param {String} File name on the amazon you wish
		 * return {Promise}
		 */
		this.upload = function(file, newFileName){
			var type = DoceboFile.getFileType(file.type);
			var key = rootContext.getAssetType(type);
			return $q(function(resolve, reject){
				var params = {
					Key: key+newFileName,
					Body: file,
					Bucket: rootContext.bucketName
				};
				var defer = $q.defer();
				rootContext.uploadProcess = new AWS.S3.ManagedUpload({params: params, leavePartsOnError: true});
				rootContext.uploadProcess.addListener('httpUploadProgress', function(data){
					$rootScope.$broadcast('AmazonUploadProgress', {progress: data, file: file});
				});
				rootContext.uploadProcess.send(function(err, data) {
					if(err){
						reject(err);
					}else{
						resolve(data);
					}
				 });
			});
		};
		/*
		 * Cancel current upload to amazon and all concurent
		 * return {Void}
		 */
		this.cancel = function(){
			if(this.uploadProcess !== false){
				rootContext.uploadProcess.abort(rootContext.uploadProcess);
				$rootScope.$broadcast('AmazonUploadCancel', {file: rootContext.uploadProcess.body});
			}
		};
	};
	AmazonS3.$inject = ['Config', '$q', 'DoceboFile', '$rootScope', 'LocalStorage'];
	return AmazonS3;
});