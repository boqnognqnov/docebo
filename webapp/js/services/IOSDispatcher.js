'use strict';
define(function () {
	function IOSDispatcher(Config, $timeout, $rootScope){
		return {
			_protocol: Config.iosProtocolName,//IOSDispatcher our main object class
			bridge: document.createElement('iframe'),//protocol name,
			_queue: [],
			/**
			* Appending iFrame to dom
			*
			* @returns {Void}
			*/
			init: function(){
				var self = this;
				this.bridge.style.display = 'none';
				this.bridge.setAttribute("id", "bridgeFrame");
				document.documentElement.appendChild(this.bridge);
			},
			/**
			* Dispatch function to ios device.
			* @param name Function name {String}
			* @param data Data that will be invoked in function {Object}
			* @returns {Void}
			*/
			callNative: function(name, data){
				if(name == "getAuthData"){
					return window.iosBridgeResponse;
				}
				var self = this;
				var isBusy = typeof window.iosQueueBusy == 'undefined' ? false : window.iosQueueBusy;
				if(!isBusy){
					window.iosQueueBusy = true;
					var uri = self._protocol+"://"+name+"/"+encodeURIComponent(data);
					self.bridge.src = uri;
				}else{
					setTimeout(function(){
						self.callNative(name, data);
					});
				}
				//return this.iosResponse(name);
			},
			iosResponse: function(key){
				return window[key];
			}
		};
	}
	IOSDispatcher.$inject = ['Config', '$timeout', '$rootScope'];
	return IOSDispatcher;
});