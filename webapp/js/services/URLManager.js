/**
 * Created by vladimir on 2.2.2016 г..
 */
'use strict';

define(function () {
    function URLManager($location) {
		
		this.PATHS = {
			API: 'api/',
			WEBAPP: 'webapp/',
			OFFLINE_API: 'http://localhost:3000/'
		};
		this.SLASH = '/';
		
		this.checkIsNative = function(){
			if((typeof window['isiPhone'] != 'undefined' 
					&& window['isiPhone'] == 1) || (typeof window['isAndroid'] != 'undefined' && window['isAndroid'] == 1)){
                return true;
			}
			return false;
		};

		this.checkOS = function(){
			if((typeof window['isiPhone'] != 'undefined' && window['isiPhone'] == 1)) {
				return "IOS";
			} else if((typeof window['isAndroid'] != 'undefined' && window['isAndroid'] == 1)) {
				return "Android"
			}
		};

		/*
		 * Creating absolute url with current domain(this function is safe e.g. if you pass slash first character in the path function will automaticly remove it)
		 * @param {String} path - Path you want after domain
		 * @return {String}
		 */
		this.createAbsoluteUrl = function(path){
			var current = null;
			var absolute = null;
			if(!this.checkIsNative()){
				current =  'webapp/#'+$location.$$path;
				absolute = decodeURIComponent($location.$$absUrl).replace(current, '');
			}else{
				absolute = window.LMSUrl;
			}
			absolute = absolute.split('?')[0].replace(this.PATHS.WEBAPP, '');
			var cleanPath = this.cleanFrontSlash(path);
			var result = absolute + cleanPath;
			return result;
		};
		/*
		 * Creates app url
		 * @param {String} path - Path you want after domain
		 * @return {String}
		 */
		this.createAppUrl = function(path){
			var appPath = this.PATHS.WEBAPP;
			var cleanPath = '#/'+this.cleanFrontSlash(path);
			var result = this.createAbsoluteUrl(appPath+cleanPath);
			return result;
		};
		
		this.createOfflineApiUrl = function(path){
			var result = this.PATHS.OFFLINE_API + path;
			return result;
		};
		/*
		 * Creates url to api server
		 * @param {String} path - Path you want after domain
		 * @return {String}
		 */
		this.createApiUrl = function(path){
			var apiPath, cleanPath, result;
			if(isOnline()){
				apiPath = this.PATHS.API + this.PATHS.WEBAPP;
				cleanPath = this.cleanFrontSlash(path);
				result = this.createAbsoluteUrl(apiPath+cleanPath);
			}else{
				if (this.checkOS() == "IOS") {
					cleanPath = this.cleanFrontSlash(path);
					result = this.createOfflineApiUrl(cleanPath);
				} else if(this.checkOS() == "Android") {
					cleanPath = this.cleanFrontSlash(path);
					result = this.createOfflineApiUrl(cleanPath) + "?path=" + cleanPath;
				}
			}
			return result;
		};
		
		/*
		 * Creates relative path to view
		 * @param {String} path - Path you want after domain
		 * @return {String}
		 */
		this.createViewUrl = function(path){
			var current = null;
			var cleanPath = this.cleanFrontSlash(path);
			if(!this.checkIsNative()){
				current =  '/webapp/'+cleanPath;
			}else{
				current = '/'+cleanPath;
			}
			return current;
		};
		/*
		 * Clean first symbol from path if its slash(/)
		 * @param {String} path - Path you want after domain
		 * @return {String}
		 */
		this.cleanFrontSlash = function(path){
			var slash = path.substring(0,1);
			if(slash === this.SLASH){
				path = path.substr(1);
			}
			return path;
		};
		/**
		 * Return the root absolute path to the webapp directory
		 * @returns {string}
         */
		this.getWebappRoot = function(){
			return $location.protocol() + "://" + $location.host() + this.SLASH + this.PATHS.WEBAPP;
		};
    }
	URLManager.$inject = ['$location'];
    return URLManager;
});