'use strict';
define(function() {
	function QueueItemException(message){
		this.message = message;
		this.name = "QueueItemException";
	};
	function ArrayIndexOutOfRange(message){
		this.message = message;
		this.name = "ArrayIndexOutOfRange";
	};
	function Queue() {
		var rootContext = this; //Reference to this context
		this._queue = []; //Array holding queue
		this._isProccessing = false; //Flag to check does queue is doing operation
		
		/*
		 * Pulling item from queue for processing and changing his state if there is no more items in queue return false
		 * @return {Object|Bool}
		 */
		this.pullNextItem = function(){
			var forProcessing = rootContext._queue.filter(function(obj){
				return obj.state === obj.STATES.Waiting;
			});
			if(forProcessing.length > 0){
				var index = rootContext._queue.indexOf(forProcessing[0]);
				rootContext._queue[index].state = forProcessing[0].STATES.Processing;
				return rootContext.getItemByIndex(index);
			}
			return false;
		};
		
		/*
		 * Pushing item at the end of the queue for processing
		 * @param {Object}
		 * @return {Void}
		 */
		this.push = function(object){
			rootContext._queue.push(object);
		};
		
		/*
		 * Reference(getter) for qeueue arrray
		 * @return {Array}
		 */
		this.getQueue = function(){
			return rootContext._queue;
		};
		
		/*
		 * Clear whole queue
		 * @return {Void}
		 */
		this.clearQueue = function(){
			rootContext._queue = [];
		};
		
		/*
		 * Remove item from queue by item
		 * @param {Object} Item
		 * @throws ArrayIndexOutOfRange if there is no item with that index
		 * @return {Void}
		 */
		this.removeItem = function(item){
			var index = this._queue.indexOf(item);
			if (index > -1) {
				this.remove(index);
			}
		};
		
		/*
		 * Remove item from queue by index
		 * @param {Int} index of the item you wish to remove
		 * @throws ArrayIndexOutOfRange if there is no item with that index
		 * @return {Void}
		 */
		this.remove = function(index){
			if (index > -1) {
				rootContext._queue.splice(index, 1);
			}else{
				throw new ArrayIndexOutOfRange("Index of Queue item out of range in remove method");
			}
		};
		
		/*
		 * Gets item from queue by index
		 * @param {Int} index of the item you wish to get
		 * @return {Object}
		 */
		this.getItemByIndex = function(index){
			var item = rootContext._queue[index];
			if(typeof item !== 'undefined'){
				return item;
			}
			return {};
		};
		
		/*
		 * Search for item in queue by property and value to that property
		 * @param {String} name of the property you want to compare
		 * @param {Any} value of the property you looking for
		 * @return {Object}
		 */
		this.getItemByCriteria = function(paramName, paramValue){
			var item = rootContext._filterByCriteria(paramName, paramValue).first();
			return item;
		};
		
		/*
		 * Check does queue is busy with a operation
		 * @return {Bool}
		 */
		this.isProccessing = function(){
			return rootContext._isProccessing;
		};
		
		/*
		 * Sets queue to busy state
		 * @return {Void}
		 */
		this.setToBusy = function(){
			rootContext._isProccessing = true;
		};
		
		/*
		 * Sets queue to free state
		 * @return {Void}
		 */
		this.setFreeOfProcessing = function(){
			rootContext._isProccessing = false;
		};
		
		/*
		 * Returns the index of the current proccessed index
		 * @return {Object|Bool}
		 */
		this.getCurrentProcessedIndex = function(){
			var running = rootContext._queue.filter(function(obj){
				return obj.state === obj.STATES.Processing;
			});
			if(running.length > 0){
				var index = rootContext._queue.indexOf(running[0]);
				return index;
			}
			return false;
		};
		
		/*
		 * Filter object by property name and value
		 * @return {Prototype}
		 */
		this._filterByCriteria = function(paramName, paramValue){
			var parent = this;
			this._queryResult = rootContext._queue.filter(function(obj){
				return obj[paramName] === paramValue;
			});
			/*
			* Return first item in the array found
			* @return {Object}
			*/
			this.first = function(){
				if(parent._queryResult.length > 0){
					return parent._queryResult[0];
				}
				return {};
			};
			/*
			* Return the result of the filter
			* @return {Object}
			*/
			this.getResult = function(){
				return parent._queryResult;
			};
			return this;
		};
		this.pullNextFile = function(){
			var forProcessing = rootContext._queue.filter(function(obj){
				return obj.state === obj.STATES.WAITING;
			});
			if(forProcessing.length > 0){
				var index = rootContext._queue.indexOf(forProcessing[0]);
				rootContext._queue[index].state = forProcessing[0].STATES.DOWNLOADING;
				return rootContext.getItemByIndex(index);
			}
			return false;
		};
		return this;
	};
	Queue.$inject = [];
	return Queue;
});