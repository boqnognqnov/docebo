'use strict';
define(function() {
    function TrackingService(PromiseService, PlayerStore, $q){
        var fp            = {}; // Hold the flowPlayerObject
        var metadata      = {}; // Hold metadata of Learning Object
		var isVideoPlayed = false;
		
		this.setVideoPlayed = function(value){
			isVideoPlayed = value;
		};
        /**
         * Set the flow player object when playing/closing video
         * @param obj - FlowPlayer object
         */
        this.setFlowPlayer = function (obj) {
            fp = (typeof obj === 'object' ? obj : {});
        };
        /**
         *
         * @param params
         * @returns {*}
         */
        this.setTrackStatus = function(params){
            metadata   = params;
            var loType = metadata.type;
            if ( loType )
                switch ( loType ){
                    case PlayerStore.TYPE.VIDEO : {
						if(isVideoPlayed){
							return this.setVideoStatus( );
						}
						return $q.when("");
                    }break;
                    case PlayerStore.TYPE.FILE:
                    case PlayerStore.TYPE.HTML:
                    case PlayerStore.TYPE.AICC:
                    case PlayerStore.TYPE.SCORM:
                    case PlayerStore.TYPE.GOOGLEDRIVE:
                    case PlayerStore.TYPE.AUTHORING:
                    case PlayerStore.TYPE.ELUCIDAT:{
                        return this.setGenericStatus();
                    }break;
                    case PlayerStore.TYPE.TEST:{
                        return this.setTestStatus();
                    }
                    default:
                        return this.setGenericStatus();
                        break;
                } // end switch

            return true;
        };
        /**
         *
         * @returns {*}
         */
        this.setVideoStatus = function( ){
			var currentStatus = metadata.status;
            if ( !metadata || typeof metadata != 'object') return false;
            metadata.status       = PlayerStore.STATUS.IN_PROGRESS;
            metadata.status_class = PlayerStore.STATUS_CLASS.IN_PROGRESS;
            var bookmark = fp.video.time;
            var duration = fp.video.duration;
            if ( parseInt(bookmark) === 0 || typeof bookmark === 'undefined'){
                bookmark = 0;
                metadata.status       = PlayerStore.STATUS.NOT_STARTED;
                metadata.status_class = PlayerStore.STATUS_CLASS.NOT_STARTED;
            } else if ( parseInt(bookmark) < parseInt(duration)){
                metadata.status       = PlayerStore.STATUS.IN_PROGRESS;
                metadata.status_class = PlayerStore.STATUS_CLASS.IN_PROGRESS;
            } else {
                bookmark = duration;
                metadata.status       = PlayerStore.STATUS.COMPLETED;
                metadata.status_class = PlayerStore.STATUS_CLASS.COMPLETED;
            }
			if(currentStatus == PlayerStore.STATUS.COMPLETED){
				metadata.status       = PlayerStore.STATUS.COMPLETED;
                metadata.status_class = PlayerStore.STATUS_CLASS.COMPLETED;
			}
            metadata.bookmark = bookmark;
            /*** Make API call to trackMaterial endpoint*/
            var trackPromise = PromiseService.create('trackMaterial', {id: metadata.id_organization, params: metadata});
			
            return trackPromise;
        };
        /**
         *
         * @returns {boolean}
         */
        this.setGenericStatus = function(){
            var trackPromise = PromiseService.create('trackMaterial', {
                id: metadata.id_organization,
                params: metadata
            });
            return trackPromise;
        };
        /**
         *
         * @returns {trackMaterial}
         */
        this.setTestStatus = function(){
            var trackPromise = PromiseService.create('trackMaterial', {
                id: metadata.id_organization,
                params: metadata
            });
            return trackPromise;
        };


        this.setAuthoringStatus = function(metadata, currentSlide){
            var trackPromise = PromiseService.create('trackMaterial', {
                id: metadata.id_organization,
                params: {
                    status: metadata.status,
                    currentSlide: currentSlide
                }
            });
            return trackPromise;
        };
    }
    TrackingService.$inject = ['PromiseService', 'PlayerStore', '$q'];
    return TrackingService;
});