'use strict';
define(function () {
    function ExtractService(Utils) {
        return {

            /**
             * Extract chapter data from learningObjects
             */
            extractChapters: function (object) {
                var result = [];
                var sum = 0;
                angular.forEach(object, function (obj, index) {
                    if (!obj.isFolder && Utils.isTypeSupportedOffline(obj)) {
                        this.push({
                            id: obj.id_resource,
                            id_org: obj.id_organization,
                            idScormItem: obj.idScormItem,
                            title: obj.title,
                            description: obj.description,
                            size: {
                                value: -1,
                                unit: 'b'
                            },
                            type: obj.type,
                            status: obj.status,
                            state: 0
                        });
                    }
                }, result);
                result.sumSize = -1;

                return result;
            }
        };
    }
    ExtractService.$inject = ['Utils'];
    return ExtractService;
});