/**
 * Created by kyuchukovv on 4/8/2016.
 */
'use strict';
define(function(){
   function Tags($q, $http, NativeAPI, URLManager) {
       /**
        * Returns tags called with API call
        * @returns { array }
        */
       self.getApiTagsList = function () {
           var defer = $q.defer();
           var tokens = NativeAPI.dispatch('getAuthData', '');

           $http.post(URLManager.createApiUrl('asset_tags'), {},
               {
                   headers: {
                       'Authorization': tokens
                   }
               })
               .success(function (data) {
                   defer.resolve(data);
               })
               .catch(function (error) {
                   defer.reject("Error getting tags items");
                   console.log("errorTags = ", error);
               });

           return defer.promise;
       };

       return self;
   }
    Tags.$inject = ['$q', '$http', 'NativeAPI', 'URLManager'];
    return Tags;
});