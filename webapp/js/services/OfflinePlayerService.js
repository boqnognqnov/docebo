'use strict';
define(function() {
	/* Coach and share asset ajax calls */
	function OffliePlayerService($rootScope, $q, $http, NativeAPI, URLManager) {
		var rootContext = this; //Reference to this context
		this.checkDownloadStatus = function(courseId){
			var qInstance = $q.defer();
			$http.post(
                URLManager.createApiUrl('lo_download_status'),
                {
					courseId: courseId
				},
                {
					headers: {
						'Authorization': NativeAPI.dispatch('getAuthData', '')
					}
				}
            )
			.success(function(data){
				qInstance.resolve(data);
			})
			.error(function(error){
				qInstance.reject("Error:" + error);
			});
			return qInstance.promise;
		};
		this.materialsDataPolling = function(courseId, jobHashId){
			var qInstance = $q.defer();
			$http.post(
                URLManager.createApiUrl('materials_size_data'),
                {
					courseId: courseId,
					jobHashId: jobHashId
				},
                {
					headers: {
						'Authorization': NativeAPI.dispatch('getAuthData', '')
					}
				}
            )
			.success(function(data){
				qInstance.resolve(data);
			})
			.error(function(error){
				qInstance.reject("Error:" + error);
			});
			return qInstance.promise;
		};
		return this;
	};
	OffliePlayerService.$inject = ["$rootScope", "$q", "$http", 'NativeAPI', 'URLManager'];
	return OffliePlayerService;
});