'use strict';
define(function() {
    function OfflineQueueItem() {
        return {
            create: function(){
                return {
                    /* Available states for the OfflineQueueItem */
                    STATES: {
                        AVAILABLE: 0,
                        DOWNLOADING: 1,
                        WAITING: 2,
                        DOWNLOADED: 3,
						NOT_DOWNLOADABLE: 4
                    },
					state: '',
					id: '',
					id_org: '',
					title: '',
					progress: 0,
					size: {
						value: -1,
						unit: 'b'
					}
                };
            }
        };
    }
    OfflineQueueItem.$inject = [];
    return OfflineQueueItem;
});