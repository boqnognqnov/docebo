define(function (require) {
	function HTMLDispatcher( $window, Storage ){
        var $$ = Dom7;
		return {
			
			ChangeTitle: function(title){
				if ( $$("#topTitle").length )
				$$("#topTitle").html(title);
			},
			back: function(){
				window.location.href = '#/all';
			},
            backToLP: function(id){
                if ( id != false || typeof id == 'undefined') {
                    window.location.href = '#/learning_plan/' + id;
                } else {
                    window.location.href = '#/all';
                }
            },
            isDesktop: function() {
                var OS = NativeAPI.device();
                if (OS == NativeAPI.OS.Desktop || OS == 'Desktop') {
                    return true;
                }
                return false;
            },
			logout: function(){
                if ( Storage.get('tokens') !== null ){
                   Storage.remove('tokens');
                }
                $window.location.href = '#/';
			},
            getAuthData: function() {
                /* Check is there any existing tokens in local storage then get them */
                if (typeof Storage.get('tokens') != 'undefined' && Storage.get('tokens') !== null) {
                    var tokens = Storage.get('tokens');
                    return tokens.token_type + ' ' + tokens.access_token;
                } else {
                    return false;
                }
            },
            sendCurrentPage: function(flag){},

            isNextLo: function(nextLo){},

            nativeLogout: function(flag) {
				this.logout();
            },

            rightPanelState: function(state) {},

            /**
             * get flag - dashboard is loaded
             * this is needed for android preloader
             * @param isLoaded
             */
            isDashboardLoaded: function(isLoaded) {},

            /**
             * set flag - search is loaded
             * @param isSearchLoaded
             */

            isGlobalSearchLoaded: function (isSearchLoaded) {},

            /**
             * set flag - search filter is loaded
             * @param isControllerLoaded
             */

            isControllerLoaded: function (isControllerLoaded) {},

            /**
             * Handle the downloading of the file
             * @param path
             * @constructor
             */
            FileLoAction: function(path){
                $window.open(path, '_blank', '');
            },


            /**
             * get free size of native device local storage
             * @param freeStorage - return the amount of free storage
             * @returns {*}
             */

            getFreeSizeStorage: function (freeStorage) {
                return freeStorage;
            },


            /**
             * get total amount of storage of mobile phone
             * @param storage
             * @returns {*}
             */

             getTotalStorage: function (storage) {
                 return storage;
             },

            /**
             * get download progress from native device
             * @param progress
             * @returns {*}
             */

            getDownloadProgress: function (progress) {
                return progress;
            },
            /**
             * Fake function not breaking desktop version
             */
            cacheRequest: function(){

            },
            /**
             * Fake function not breaking desktop version
             */
            startDownload: function () {

            },
            /**
             * Fake function not breaking desktop version
             */
            cancelLoDownload: function (link) {

            },
            isCoursePlayer: function () {

            }
		};
	}
	HTMLDispatcher.$inject = ['$window', 'LocalStorage'];
	return HTMLDispatcher;
});

