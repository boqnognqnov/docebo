'use strict';
define(['framework7'], function (Framework7) {
	function User($http, Storage, NativeAPI, $rootScope, $window, URLManager, Request, $location, $q){
		return{
            /**
             * Retrieve simple mobile username information from the LMS
             * @param callback
             * @param errorCallback
             */
            getUserInfo: function(callback, errorCallback) {
                var tokens = NativeAPI.dispatch('getAuthData', '');
                $http.get(URLManager.createApiUrl('get_user_info'),
                    {
                        headers : {
                            'Authorization' : tokens
                        }
                }).success(function(data){
                    // CURRENT USER DATA
                    callback(data);
                }).catch(function(error){
                    if ( error.status != 200 ) {
                        if ( typeof errorCallback === "function") errorCallback(error);
                    }
                });
            },
			onUserInfoRetrieved: function(data){
				Storage.set('mobile_info', data);
				$rootScope.$broadcast('mobileInfoSaved');
				/* GET CURRENT USER ID */
				$rootScope.userIdst = data.idst;
				var url = $window.location.href.split('?');
				var clean = url[0].split('#');
				$window.location.href = clean[0]+'#/all';
			},
			logout: function(){
				var qInstance = $q.defer();
				$http.post(
					URLManager.createApiUrl('logout'),
					{},
					{
						headers: {
							'Authorization': NativeAPI.dispatch('getAuthData', '')
						}
					}
				)
				.success(function(data){
					qInstance.resolve(data);
				})
				.error(function(error){
					qInstance.reject("Error:" + error);
				});
				return qInstance.promise;
			}
		};
	}
	User.$inject = ['$http', 'LocalStorage', 'NativeAPI', '$rootScope', '$window', 'URLManager', 'Request', '$location', '$q'];
	return User;
});