/**
 * Created by Mitko on 24.10.2016 г..
 */

'use strict';
 define(function () {
     function StorageType(NativeAPI, $rootScope, Utils, $timeout) {
         return {

             /**
              * get free storage from native device
              */
			 _isSet: false,
             getFreeStorage: function () {
                 var freeStorage;
				 NativeAPI.dispatch('getFreeSizeStorage', '');
				 //setTimeout(function(){
				 $rootScope.$watch(function(){
					return NativeAPI.getBridgeResponse('getFreeSizeStorage');
				 }, function(val){
					if(typeof val != 'undefined'){ 
						var freeStorageObj = Utils.parseJSON(val);
						if (freeStorageObj.GB >= 1) {
							freeStorage = freeStorageObj.GB;
							freeStorage = parseFloat(freeStorage).toFixed(1) + " GB";
						} else if (freeStorageObj.GB < 1) {
							freeStorage = freeStorageObj.MB;
							freeStorage = parseFloat(freeStorage).toFixed(0) + " MB";
						} else if (freeStorageObj.MB < 1) {
							freeStorage = freeStorageObj.KB;
							freeStorage = parseFloat(freeStorage).toFixed(0) + " KB";
						} else if (freeStorageObj.KB < 1) {
							freeStorage = freeStorageObj.BYTE;
							freeStorage = parseFloat(freeStorage).toFixed(0) + " BYTE";
						}
						$rootScope.$broadcast("freeStorage", freeStorage);
					}
				 });
					
				 //}, 2000);
             },

             /**
              * get available space in percentage
              */

             getStoragePercentage: function () {
                 var progressValue;
                 //NativeAPI.dispatch('getFreeSizeStorage', '');
				$rootScope.$on("freeStorage", function(){
					NativeAPI.dispatch('getTotalStorage', '');
					$rootScope.$watch(function(){
						return NativeAPI.getBridgeResponse('getTotalStorage');
					 }, function(val){
						if(typeof val != 'undefined'){
							var freeStorageObj = Utils.parseJSON(NativeAPI.getBridgeResponse('getFreeSizeStorage'));
							var totalStorageObj = Utils.parseJSON(val);
							var usedStorage = totalStorageObj.GB - freeStorageObj.GB;
							var percentage = usedStorage / totalStorageObj.GB * 100;
							progressValue = Math.round(percentage) + "%";

							$timeout(function(){
								$rootScope.$broadcast("progressValue", progressValue);
							});
						}
					 });
				});
                
             },
			 invokeStorageCheck: function(){
				var $this = this;
				$rootScope.$on("progressValue", function(){
					$this._isSet = true;
				});
				if (NativeAPI.isMobile() && !this._isSet) {
					this.getFreeStorage();
					this.getStoragePercentage();
				}
			 }
         };
     }
     StorageType.$inject = ['NativeAPI', '$rootScope', 'Utils', '$timeout'];
     return StorageType;
 });