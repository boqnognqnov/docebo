'use strict';
define(function() {
	/* Coach and share asset ajax calls */
	function EmbedLinkParser() {
		var self = this; //Reference to this context
		
		var _baseYoutube = "http://www.youtube.com/embed/";
		var _baseWistia = "//fast.wistia.net/embed/iframe/";
		var _baseVimeo = "https://player.vimeo.com/video/";
		
		self.parseYT = function(link){
			var linkArray = link.split('?v=');
			if(linkArray.length > 1){
				return _baseYoutube+linkArray[1];
			}
		};
		self.parseWistia = function(link){
			var linkArray = link.split('medias/');
			if(linkArray.length > 1){
				return _baseWistia+linkArray[1]+"?videoFoam=true";
			}
		};
		self.parseVimeo = function(link){
			var newLink = link.replace("https://vimeo.com/", "");
			return _baseVimeo+newLink;
		};
		
		return self;
	};
	EmbedLinkParser.$inject = [];
	return EmbedLinkParser;
});