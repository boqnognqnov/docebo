/**
 * Created by vladimir on 14.1.2016 г..
 */
'use strict';
define([], function() {
    function Utils($window, $location) {
		var $this = this;
		$this.isElementBottomVisible = function(el) {
			var elemTop = el.getBoundingClientRect().top;
			var elemBottom = el.getBoundingClientRect().bottom;
			var isVisible = (elemTop <= 0) && (elemBottom <= window.innerHeight);
			return isVisible;
		};
		$this.isInt = function(value) {
			return !isNaN(value) && 
				   parseInt(Number(value)) == value && 
				   !isNaN(parseInt(value, 10));
		};
		$this.getKeyByValue = function(value, object){
			for( var prop in object ) {
				if( object.hasOwnProperty( prop ) ) {
					 if( object[ prop ] === value )
						 return prop;
				}
			}	
		};
		$this.isTypeSupportedOffline = function(obj){
			var allowedTypes = ['sco', 'video', 'htmlpage', 'file', 'authoring'];
			if(allowedTypes.indexOf(obj.type) !== -1){
				if ( obj.type === 'video' && obj.video_type !== 'upload'){
					return false;
				}
				return true;
			}
			return false;
		};
		$this.parseJSON = function(jsonString){
			var $object = JSON.parse(jsonString);
			if(typeof $object != 'object'){
				$object = JSON.parse($object);
			}
			return $object;
		};
		$this.humanFileSize = function(bytes){
			var result = {
				value: -1,
				unit: 'B'
			};
			if(bytes == 0){
				result.value = 0;
				return result;
			}
			bytes = parseFloat(bytes);
			var arBytes = [
				{
					'UNIT': 'TB', 
					'VALUE': Math.pow(1024, 4)
				},
				{
					'UNIT': 'GB', 
					'VALUE': Math.pow(1024, 3)
				},
				{
					'UNIT': 'MB', 
					'VALUE': Math.pow(1024, 2)
				},
				{
					'UNIT': 'KB', 
					'VALUE': 1024
				},
				{
					'UNIT': 'B', 
					'VALUE': 1
				}
			];
			for(var i=0;i<arBytes.length;i++){
				var obj = arBytes[i];
				if(bytes > obj.VALUE){
					result.value = parseFloat(bytes/obj.VALUE).toFixed(2);
					result.unit = obj.UNIT;
					break;
				}
			}
			return result;
		};
		return $this;
	};
    Utils.$inject = [];
    return Utils;
});