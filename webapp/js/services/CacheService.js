'use strict';
define(function() {
	/* Coach and share asset ajax calls */
	function CacheService($rootScope, NativeAPI) {
		var self = this; //Reference to this context
		
		self.cacheRequest = function(endpoint, requestParams, response){
			if(isOnline() || endpoint == "trackMaterial"){
				NativeAPI.dispatch('cacheRequest', {
					endpoint: endpoint, 
					requestParams: requestParams, 
					response: response
				});
			}
		};
		
		self.downloadRequestItems = function(url){
			NativeAPI.dispatch('downloadCacheItems', {
				toDownload: url
			});
		};
		
		return self;
	};
	CacheService.$inject = ['$rootScope', 'NativeAPI'];
	return CacheService;
});