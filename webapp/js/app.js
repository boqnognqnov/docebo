function isOnline() {

	// offline native message - set condition of the connection from native app
	if ((typeof window['setOfflineNative'] != 'undefined' && window['setOfflineNative'] == 'true')) {
        setOffline();
		window['setOfflineNative'] = '';
	}

	if (typeof localStorage == 'undefined') return true;
	var offlineMode = localStorage.getItem('offline-mode');
	if (offlineMode == 'true') {
		return false;
	}
	return true;
}

function setOffline(){
	if(typeof localStorage != 'undefined'){
		localStorage.setItem('offline-mode', 'true');
	}
}

function setOnline(){
	if(typeof localStorage != 'undefined'){
		localStorage.setItem('offline-mode', 'false');
	}
}


/*
 * Helper to point routing views in modules
 * @TODO Extend it for directives
 */
function view(module, onlyPath){
	if(typeof onlyPath == 'undefined'){
		onlyPath = false;
	}
	var DS = '/';
	var templatesDir = 'template';
	var componentsDir = 'component';
	var base = 'js'+DS+'modules'+DS;
	var pathSliced = module.split(".");
	var moduleName = pathSliced.shift();
	if(pathSliced.length > 1){
		var moduleFullPath = moduleName + DS + componentsDir + DS + pathSliced[0] + DS + 'view' + DS;// + pathSliced[1] +'.html';
		if(!onlyPath){
			moduleFullPath = moduleFullPath + pathSliced[1] +'.html';
		}
	}else{
		var moduleFullPath = moduleName + DS + templatesDir + DS;// + pathSliced.join('/') +'.html';
		if(!onlyPath){
			moduleFullPath = moduleFullPath + pathSliced.join('/') +'.html';
		}
	}
	return base+moduleFullPath;
}

require.config({
	baseUrl: 'js',
	waitSeconds: 0,
	paths: {
		angular: 'lib/angular',
		angularRoute: 'lib/angular-router',
		bootstrap: 'lib/bootstrap.min',
		framework7: 'lib/framework7',
		translate : 'lib/angular-translate.min',
		sanitize: 'lib/angular-sanitize.min',
		infiniteScroll: 'lib/ng-infinite-scroll.min',
		angularClipboard: 'lib/angular-clipboard',
		aws: 'lib/aws-sdk.min',
		flux: 'lib/flux-angular',
        app: 'app',
		cp: 'modules/CoursePlayer/CoursePlayer'
    },
	shim: { 
		'angular': {
			exports: 'angular'
		}, 
		'angularRoute': { deps: ['angular'] },
		'bootstrap':  {
			'exports' : 'bootstrap'
		},
		'framework7': {
			exports: 'Framework7'
		},
		'translate' : {
			exports: 'translate'
		},
		'sanitize' : {
			exports: 'sanitize'
		},
		'infiniteScroll':  {
			'exports' : 'infinite-scroll',
			'deps': ['angular']
		},
		'flux': {
			deps: ['angular']
		},
		'cp': {
			deps: ['flux']
		},
		'angularClipboard':  {
			'exports' : 'angularClipboard'
		},
		'aws': {
			'exports': 'AWS'
		},
		'app': {
			deps: [
				'angular', 
				'angularRoute',
				'bootstrap',
				'framework7',
				'infiniteScroll',
				'angularRoute',
				'framework7',
				'services',
				'filters',
				'directives',
				'controllers',
				'providers',
				'flux',
				'modules/DoceboMobile',
				'services/JSBridge',
				'angularClipboard',
				
			]
		}
	}
});

define(['angular', 'angularRoute', 'framework7', 'modules/DoceboMobile', 'infiniteScroll', 'bootstrap', 'flux', 'cp'], 
    function(angular) {
        // tell angular to wait until the DOM is ready
        // before bootstrapping the application
		//setTimeout(function(){
			require([
				'services',
				'filters',
				'directives',
				'controllers',
				'providers',
				'angularClipboard'
			],
			function() {
				try{
					var isNative = ((typeof window['isiPhone'] != 'undefined' && window['isiPhone'] == 1) || (typeof window['isAndroid'] != 'undefined' && window['isAndroid'] == 1));
					if((isNative && typeof window.LMSUrl !='undefined') || !isNative){
						angular.bootstrap(document, ['DoceboMobile']);
					}else{
						var authData = "";
						if(typeof AndroidFunction != 'undefined'){
							authData = AndroidFunction.getAuthData("token");
							window.location.href = '/webapp/v1/#/all?isIphone='+window['isiPhone']+'&isAndroid='+window['isAndroid']+"&authData="+authData.replace(" ", "_");
						}else{
							authData =  window.iosBridgeResponse;
							window.location.href = '/webapp/v1/#/all?isIphone='+window['isiPhone']+'&isAndroid='+window['isAndroid']+"&authData="+authData;
						}
					}
				}catch(e){
					console.log(e);
				}
				//});
			});
		//}, 4000);
});