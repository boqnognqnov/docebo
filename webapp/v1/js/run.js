/* global $scope, angular */

define(function(){
	function AngularRun($rootScope, $window, $location, NativeAPI, TokenStorage, ConnectAPI, Request, TrackUser){
		
		var isPreLoaderActive = null;
		var isIphone = getQueryVariable("isIphone");
		var isAndroid = getQueryVariable("isAndroid");
		var authData = getQueryVariable("authData");
		if(parseInt(isIphone) === 1){
			window['isiPhone'] = 1;
		}
		if(authData != false && parseInt(isIphone) == 1){
			window['iosBridgeResponse'] = authData;
		}
		if(parseInt(isAndroid) === 1){
			window['isAndroid'] = 1;
		}
		if(authData != false && parseInt(isAndroid) == 1){
			authData = authData.replace("_", " ");
			window['AndroidFunction']['getAuthData'] = function() {
				return authData;
			};
		}
		$rootScope.$on( "$routeChangeStart", function(event, next, current) {
			if(typeof current !== 'undefined'){
				if(typeof current.scope !== 'undefined'){
					current.scope.$destroy();
				}
			}
			/* Raise  event for destroying Flow Player object */
			$rootScope.$broadcast('destroyFlowPlayer');
			//reset scroll to top on every new page
			/*::TODO This must be refactor(causing strange effect on switching pages scrolling to top before next page is loaded)*/
			$('.page-content').scrollTop(0);

			NativeAPI.init(); // Initialize the Native API for all device
			ConnectAPI.applyAppSettings();
			var oauthResponse = Request.getParam('oauth2_response');
		//	if(TokenStorage.isExpired() && oauthResponse === false && !NativeAPI.isMobile() && $rootScope.entryPoint !== true){
		//		$window.location.href = '#/login';
		//	}
			var nextRoute    = ( next ? next.$$route : '');

			if(typeof nextRoute !== 'undefined' && typeof nextRoute.controller !== 'undefined'){
				NativeAPI.dispatch('sendCurrentPage', nextRoute.controller);

				if(isPreLoaderActive == false) {
					$rootScope.$emit('enableLoading');
				}
			}
			/**
			 * Track the user route and send the next object(page that he landed)
			 */
			trackUserRoute(next);
		});
		/**
		 * Get the route of the user and write it in TrackRoute service
		 * @param route
         */
		var trackUserRoute = function(route){
			if (typeof route === 'object'){
				if (typeof route.$$route === 'object' && route.activetab){
					TrackUser.pushRoute(route.$$route.originalPath);
				}
				else{
					TrackUser.pushRoute($location.path());
				}
			}
		};

		/* Spinner icon - material design */
			var mdlUpgradeDom = false;
			setInterval(function() {
			  if (mdlUpgradeDom) {
				componentHandler.upgradeDom();
				mdlUpgradeDom = false;
			  }
			}, 200);

			var observer = new MutationObserver(function () {
			  mdlUpgradeDom = true;
			});
			observer.observe(document.body, {
				childList: true,
				subtree: true
			});

		$rootScope.$on('stateLoader', function(event, args) {
			isPreLoaderActive = args.stateLoader;
		});
	}
	AngularRun.$inject = ['$rootScope', '$window', '$location', 'NativeAPI', 'TokenStorage', 'ConnectAPI', 'Request', 'TrackUser'];
	return AngularRun;
});


