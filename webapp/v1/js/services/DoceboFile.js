'use strict';
define(function () {
	function DoceboFile(){
		/*
		 * Creates a random hash that can be used for file name
		 * @param {Int} length - length of the hash default is 5
		 * @return {String}
		 */
		this.hashName = function(length){
			if(typeof length === 'undefined'){
				length = 5;
			}
			var text = "";
			var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

			for( var i=0; i < length; i++ )
				text += possible.charAt(Math.floor(Math.random() * possible.length));

			return text;
		};
		/*
		 * Returns file extension of the file by its file name
		 * @param {String} Name of the file you wish to get extension
		 * @return {String}
		 */
		this.exportExtension = function(filename){
			return filename.split('.').pop();
		};
		/*
		 * Get the file mime type image, video, audio etc.
		 * @param {String} File type example: image/jpeg
		 * @return {String}
		 */
		this.getFileType = function(fileType){
			return fileType.split('/').shift();
		};
		/*
		 *	@TODO
		 *	This function is not used because is not working properly but will be improved in future to work ^^
		 */
		this.videoThumb = function(file) {
			var video = document.createElement('video');
			var url = URL.createObjectURL(file);
			video.src = url;
			var canvas = document.createElement('canvas');
			canvas.height = video.videoHeight;
			canvas.width = video.videoWidth;
			var ctx = canvas.getContext("2d");
			ctx.drawImage(video, 50, 50, canvas.width, canvas.height);
			canvas.style.width = 'inherit';
			canvas.style.height = 'inherit';
			return canvas.toDataURL();
		};
	}
	DoceboFile.$inject = [];
	return DoceboFile;
});