/**
 * Created by vladimir on 4.1.2016 г..
 */
'use strict';
define(function () {
    function Course($http, $translate, NativeAPI, URLManager, $q) {
        return{
            _courses: {},
            /**
             *
             * @param courseId
             * @param callback
             * @param errorCallback
             */
            getLoInfo: function(courseId, callback, errorCallback) {
                var tokensAuth = NativeAPI.dispatch('getAuthData', '');

                $http.post(
					URLManager.createApiUrl('get_lo_by_course_id'),
                    {
                        'courseId' : courseId
                    },
                    {
                        headers : {
                            'Authorization' : tokensAuth
                        }
                    })
                    .success(function(data){
                        // CURRENT USER DATA
                        callback(data);
                    })
                    .catch(function(error){
                        if ( error.status != 200 ) {
                            if ( typeof errorCallback === "function") errorCallback(error);
                        }
                    });
            },
            /**
             *
             * @param courseId
             * @param callback
             * @param errorCallback
             */
            canEnterCourse: function(courseId, callback, errorCallback) {
                var tokensAuth = NativeAPI.dispatch('getAuthData', '');

                $http.post(URLManager.createApiUrl('can_enter_course'),
                    {
                        'courseId': courseId
                    },
                    {
                        headers : {
                            'Authorization' : tokensAuth
                        }
                    })
                    .success( function(data){
                        callback(data);
                    })
                    .catch( function(error){
                        if ( error.status != 200 && typeof errorCallback === "function")
                            errorCallback(error);
                    });
            },
            /**
             *
             * @param LpId
             * @param callback
             * @param errorCallback
             */
            getLearningPlan: function( LpId, callback, errorCallback) {
                var tokensAuth = NativeAPI.dispatch('getAuthData','');
                $http.post(URLManager.createApiUrl('get_courses_by_lp_id'),
                    {
                        'LpId'    : LpId
                    }, {
                        headers : {
                            'Authorization' : tokensAuth
                        }})
                    .success(function(data){
                        callback(data);
                    })
                    .catch(function(error){
                        if ( error.status != 200 && typeof errorCallback === "function")
                            errorCallback(error);
                    });
            },

            getLearningObject: function( id, scormItemId, callback, errorCallback){
                var tokensAuth = NativeAPI.dispatch('getAuthData','');

                $http.post(
					URLManager.createApiUrl('get_learning_object'),
                    {
                        'id'          : id,
                        'scormItemId' : scormItemId
                    },
                    {
                        headers : {
                            'Authorization' : tokensAuth
                        }
                    }
                )
                    .success(function(data){
                        callback(data);
                    })
                    .catch(function(error){
                        errorCallback(error);
						throw new Exception("Learning object fetch failed");
                    });
            },

            trackUserLoStatus: function(idOrg, objType, callback, errorCallback){
                var tokensAuth = NativeAPI.dispatch('getAuthData','');

                $http.post(
                    URLManager.createApiUrl('track_user_lo_status'),
                    {
                        'idOrg'  : idOrg,
                        'objType': objType
                    },{
                        headers : {
                            'Authorization' : tokensAuth
                        }
                    }
                )
                    .success(function(data){
                        callback(data);
                    })
                    .catch(function(error){
                        errorCallback(error);
                    });
            },
			
			getUserLearningPlans: function(from, count){
				var qInstance = $q.defer();
				var requestParams = {
					endpoint: "my/lp",
					params:{
						from: from,
						count: count
					}
				};
				$http.post(
					URLManager.createApiUrl('data'),
					requestParams,
					{
						headers: {
							'Authorization': NativeAPI.dispatch('getAuthData', '')
						}
					}
				)
				.success(function(data){
					qInstance.resolve(data);
				})
				.error(function(error){
					qInstance.reject("Error:" + error);
				});
				return qInstance.promise;
			},
			
			getUserCourses: function(from, count){
				var qInstance = $q.defer();
				var requestParams = {
					endpoint: "my/courses",
					params:{
						from: from,
						count: count
					}
				};
				$http.post(
					URLManager.createApiUrl('data'),
					requestParams,
					{
						headers: {
							'Authorization': NativeAPI.dispatch('getAuthData', '')
						}
					}
				)
				.success(function(data){
					qInstance.resolve(data);
				})
				.error(function(error){
					qInstance.reject("Error:" + error);
				});
				return qInstance.promise;
			}
        };
    }
    Course.$inject = ['$http', '$translate', 'NativeAPI', 'URLManager', '$q'];
    return Course;
});