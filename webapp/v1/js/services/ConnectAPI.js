/**
 * Created by Violeta on 29.3.2016.
 */
 
'use strict';
define(function () {
    function ConnectAPI($window, Config, $rootScope, Storage, NativeAPI, User){
		var rootCtx = this;
		this.applyAppSettings = function(){
			if(typeof $rootScope.appSettings == 'undefined' || $rootScope.appSettings == null){
				$rootScope.appSettings = Storage.get('appSettings');
			}

			if(NativeAPI.isMobile()){
				if($rootScope.appSettings == null || typeof $rootScope.appSettings == 'undefined'){
					$window.location = '#/login';
				}
				
			}
			$rootScope.$watch('appSettings', function(appSettings){
				if(typeof appSettings != 'undefined' && appSettings != null && appSettings.success != false){
					Config.appSettings = appSettings;
					$rootScope.mainLogo   = Config.appSettings.configuration.logo_path;
					$rootScope.logo           = Config.appSettings.configuration.logo_path;
					$rootScope.navColor   = Config.appSettings.configuration.nav_color.hex;
					$rootScope.navFont    = Config.appSettings.configuration.nav_color.nav_font.hex;
					$rootScope.isGoogleSsoActive = Config.appSettings.plugins.GooglessoApp;
					$rootScope.isGoogleAppsActive = Config.appSettings.plugins.GoogleappsApp;
					$rootScope.isOktaActive = Config.appSettings.plugins.OktaApp;
					$rootScope.isSamlActive = Config.appSettings.plugins.SimpleSamlApp;
					$rootScope.https_redirect = Config.appSettings.configuration.https_redirect;
					var bodyColor         = Config.appSettings.configuration.bg_color.hex;
					var bgFont            = Config.appSettings.configuration.bg_color.bg_font.hex;
					$rootScope.bgFont     = bgFont;
					$rootScope.bodyColor  = bodyColor;
					/* Get cloudfront cookie src url from the widget*/
					$rootScope.cloudfrontCookie = appSettings.configuration.cloudfrontCookie;
				}
			});
		};
		this.connect = function(callback){
			//Config.getLanguage();
			$rootScope.appSettings = null;
			 /*** If there is no response from the configuration API call */
			 if ( $rootScope.appSettings === null || typeof $rootScope.appSettings === 'undefined' ) {
				 Config.lmsConnect(function(data){
					 $rootScope.appSettings = Storage.get('appSettings');
					 rootCtx.applyAppSettings();
					 if(typeof callback === 'function')
						callback(data);
				 },function() {
					/*if(!NativeAPI.isMobile())
						$window.location.href = '#/login';
					else
						User.getUserInfo(function(data) {
							User.onUserInfoRetrieved(data);
						});*/
					$rootScope.$broadcast("error:Connect2Server");
				 });
			 } else {
				this.applyAppSettings();
				if(typeof callback === 'function')
					callback();
			 }
		 };
    }
    ConnectAPI.$inject = ['$window', 'Config', '$rootScope', 'LocalStorage', 'NativeAPI', 'User'];
    return ConnectAPI;
});