/**
 * Created by vladimir on 14.1.2016 г..
 */
'use strict';
define([], function() {
    function Utils($window, $location) {
		var $this = this;
		$this.isElementBottomVisible = function(el) {
			var elemTop = el.getBoundingClientRect().top;
			var elemBottom = el.getBoundingClientRect().bottom;
			var isVisible = (elemTop <= 0) && (elemBottom <= window.innerHeight);
			return isVisible;
		};
		return $this;
	};
    Utils.$inject = [];
    return Utils;
});