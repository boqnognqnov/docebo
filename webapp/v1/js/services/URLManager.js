/**
 * Created by vladimir on 2.2.2016 г..
 */
'use strict';

define(function () {
    function URLManager($location) {
		this.PATHS = {
			API: 'api/',
			WEBAPP: 'webapp/v1/'
		};
		this.SLASH = '/';
		/*
		 * Creating absolute url with current domain(this function is safe e.g. if you pass slash first character in the path function will automaticly remove it)
		 * @param {String} path - Path you want after domain
		 * @return {String}
		 */
		this.createAbsoluteUrl = function(path){
			var current =  this.PATHS.WEBAPP+'#'+$location.$$path;
			var absolute = decodeURIComponent($location.$$absUrl).replace(current, '');
			absolute = absolute.split('?')[0].replace(this.PATHS.WEBAPP, '');
			var cleanPath = this.cleanFrontSlash(path);
			var result = absolute + cleanPath;
			return result;
		};
		/*
		 * Creates app url
		 * @param {String} path - Path you want after domain
		 * @return {String}
		 */
		this.createAppUrl = function(path){
			var appPath = this.PATHS.WEBAPP;
			var cleanPath = '#/'+this.cleanFrontSlash(path);
			var result = this.createAbsoluteUrl(appPath+cleanPath);
			return result;
		};
		/*
		 * Creates url to api server
		 * @param {String} path - Path you want after domain
		 * @return {String}
		 */
		this.createApiUrl = function(path){
			var apiPath = this.PATHS.API + this.PATHS.WEBAPP.replace("v1/", "");
			var cleanPath = this.cleanFrontSlash(path);
			var result = this.createAbsoluteUrl(apiPath+cleanPath);
			return result;
		};
		/*
		 * Clean first symbol from path if its slash(/)
		 * @param {String} path - Path you want after domain
		 * @return {String}
		 */
		this.cleanFrontSlash = function(path){
			var slash = path.substring(0,1);
			if(slash === this.SLASH){
				path = path.substr(1);
			}
			return path;
		};
    }
	URLManager.$inject = ['$location'];
    return URLManager;
});