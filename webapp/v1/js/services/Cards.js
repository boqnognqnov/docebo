/**
 * Created by kyuchukovv on 3/29/2016.
 */
'use strict';
define(function() {
   function Cards($http, $translate) {
       var OkGotIt='', GotIt='', Next='', Desc11='', Desc12='',Desc13='',Desc21='',Desc22='',Desc23='',Desc31='',Desc32='',Desc33='',Desc34='',Desc41='',Desc42='',Desc43='',Desc44='';
       var cards = [];
	   $translate(['OK, got it!', 'Got it!', 'Next', 'Once details are added, it\'s time to', 'Publish', 'your video!', 'Add', 'assets details', 'hit Publish', 'Tap on your videos', 'add details', 'and', '(title, dexription, topics, tags)', 'Select', 'assets you want to upload or', 'record', 'a new video'])
           .then(function(translations){
               OkGotIt = translations['OK, got it!'];
               GotIt = translations['Got it!'];
               Next = translations['Next'];
               Desc11 = translations['Once details are added, it\'s time to'];
               Desc12 = translations['Publish'];
               Desc13 = translations['your video!'];
               Desc21 = translations['Add'];
               Desc22 = translations['assets details'];
               Desc23 = translations['hit Publish'];
               Desc31 = translations['Tap on your videos'];
               Desc32 = translations['and'];
               Desc33 = translations['add details'];
               Desc34 = translations['(title, dexription, topics, tags)'];
               Desc41 = translations['Select'];
               Desc42 = translations['assets you want to upload or'];
               Desc43 = translations['record'];
               Desc44 = translations['a new video'];

				cards = [
                   { id: 3, image: 'images/wt_upload_step3.png', description: '<div>'+Desc11+' <span class="bold"> '+Desc12+' </span> '+ Desc13+'</div>' , button_text: OkGotIt, card_type: 'walkthrough'},
                   { id: 4, image: 'images/wt_upload_step2.png', description: "<div><span class='bold black'>"+Desc21+" </span>"+ Desc22+" <span class='bold black'> "+Desc23+" </span></div>" , button_text: GotIt, card_type: 'first_video'},
                   { id: 2, image: 'images/wt_upload_step2.png', description: "<div><span class='bold'>"+Desc31+" </span>"+ Desc32 +" <span class='bold'> "+Desc33+" </span> "+Desc34+" </div>", button_text: Next, card_type: 'walkthrough'},
                   { id: 1, image: 'images/upload_record1.png', description: "<div><span class='bold'>"+Desc41+" </span>"+ Desc42 +" <span class='bold'>"+Desc43+"</span> "+ Desc44+" </div>", button_text: Next, card_type: 'walkthrough'}
               ];
            });


       return {

           getCards: function() {
             return cards;
           },
           getCardsByType: function(type) {
               var cards = this.getCards().filter(function(cards){
                  if (cards.card_type == type) {
                      return true;
                  }
                   return false;
               });
               return cards;
           }
       };
   }
    Cards.$inject = ['$http', '$translate'];
    return Cards;
});