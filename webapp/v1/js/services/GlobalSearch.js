/**
 * Created by Mitko on 16.8.2016 г..
 */

'use strict';
define(function () {
    function GlobalSearch($q, $http, NativeAPI, URLManager) {

        return{
			ENUM_TYPES: {
				EsAgentAll: 'all',
				EsAgentCourse: 'EsAgentCourse',
				EsAgentKnowledgeAsset: 'EsAgentKnowledgeAsset',
				EsAgentLo: 'EsAgentLo',
				EsAgentPlan: 'EsAgentPlan'
			},
			elasticSearch: function(keyword, from, to, type){
				type = typeof type == 'undefined' ? this.ENUM_TYPES.EsAgentAll : type;
				var defer = $q.defer();
                var tokensAuth = NativeAPI.dispatch('getAuthData', '');
				$http.post(
                    URLManager.createApiUrl('search_results'),
                    {
                        'search' : keyword,
						'from': from,
						'to': to,
						'type': type
                    },
                    {
					headers : {
						'Authorization' : tokensAuth
					}
				})
				.success(function(data){
					defer.resolve(data);
				})
				.catch(function(error){
					defer.reject("Error getting search results");
				});
                return defer.promise;
			},
            gsData: function(suggestion) {
                var defer = $q.defer();
                var tokensAuth = NativeAPI.dispatch('getAuthData', '');

                $http.post(
                    URLManager.createApiUrl('search_suggestions'),
                    {
                        'search' : suggestion
                    },
                    {
                        headers : {
                            'Authorization' : tokensAuth
                        }
                    })
                    .success(function(data){
                        //console.log("data = ", data);
                        defer.resolve(data);
                    })
                    .catch(function(error){
                        defer.reject("Error getting suggestions");
                    });

                return defer.promise;
            }
			
        };
   }
    GlobalSearch.$inject = ['$q', '$http', 'NativeAPI', 'URLManager'];
    return GlobalSearch;
});