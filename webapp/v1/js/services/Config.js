'use strict';
define(['framework7'], function (Framework7) {
	function Config($q, $http, Storage, URLManager){
		return{
			//Used by IOS bridge
			s3BucketName: '',//'lmsfiles-test',
			s3Region: '',//'eu-west-1',
			identityPool: 'eu-west-1:a4fe8dc4-576d-4b6c-b092-57d47e160005',
			iosProtocolName: "docebo",
			//Framework7 instance
			fw7: {
				app : new Framework7({
                    material: false,
				    animateNavBackIcon: true,
					swipeBackPage: false,
					swipePanel: false,
					swipePanelOnlyClose: false,
					swipePanelCloseOpposite: false,
					panelsCloseByOutside: false,
					swipeout: false,
					materialRipple:false,
					fastClicks: false
				}),
				options : {
				  dynamicNavbar: true,
				  domCache: true
				},
				views : []
			},
            /** Empty Object, but ...
             ** Will be filled with data from the API call to LMS webapp/connect ***/
            appSettings: {},
            /**
             *
             * @param callback called on Success and redirect user to Login page
             * @param errorCallback called on Error
             */
            lmsConnect: function( callback, errorCallback ) {
                var that = this;

                $http.post(URLManager.createApiUrl('connect'))
                    .success(function (data) { // Data is JSON
                        Storage.set('appSettings', data);
						Config.s3BucketName = data.configuration.s3BucketName;
						Config.s3Region = data.configuration.s3Region;
                        Config.appSettings = Storage.get('appSettings');
						callback(data);
                    })
                    .catch(function (err) {
                        if ( err.status !== 200 ) {
                            if ( typeof errorCallback === "function") errorCallback();
                        }
                    });
            },
            /**
             *
             * @returns {string}
             */
			detectLanguage: function() {
				/*var lang1 = Locale.getDefault().getISO3Language(); */
                var lang = navigator.language || navigator.userLanguage;
				return lang;
			},
			getLanguage: function() {
				
				/*var langBrowser = navigator.language || navigator.userLanguage;
				langBrowser = langBrowser.split('-')[0];
				var langCached = Storage.get('lang');
				if(langBrowser !== langCached){
					console.log("here");
					$http.post(URLManager.createApiUrl('language'),
                        {
                            'language': langBrowser
                        })
				   .success(function(data){
						Storage.set('lang', langBrowser);
						Storage.set('translationStrings', data);
					}).catch(function(error){
						console.log("Error getting language: "+ error);
					});
			    }*/

            },
            relativeUrl: function(URL) {
                return window.location.protocol + '//' + window.location.host + URL;
            },
            /**
             *
             * @param localItem if $name is empty return the JSON object from localStorage
             * @param name is the name of the value of the objct that should return
             */
            getLocalItem: function(localItem, name) {
                Storage.getItemFromArray(localItem, name)
            },
            skipWalkthrough: function(callback, errorCallback) {
                var tokens = Storage.get('tokens');
                $http.get(URLManager.createApiUrl('skip_walktrough'),
                    {
                        headers : {
                            'Authorization' : tokens.token_type + ' ' + tokens.access_token
                        }
                    })
                    .success(function(data){
                        callback(data);
                    })
                    .catch(function(error){
                        if ( error.status != 200 ) {
                            if ( typeof errorCallback === "function") errorCallback(error);
                        }
                });
            },
			getAmazonStoragePath: function(){
				var paths = Storage.get('appSettings').paths;
				var result = {};
				for(var key in paths.amazon){
					result[key] = paths.amazon[key].slice(1);
				}
				return result;
			},
            forgotPassword: function(user){
                var qInstance = $q.defer();
                $http.post(
                    URLManager.createApiUrl('forgot_password'),
                    {
                        user : user
                    }
                )
                    .success(function(data){
                        qInstance.resolve(data);
                })
                    .error(function(error){
                        qInstance.reject("Error: " + error);
                });
                return qInstance.promise;
            }

		};
	}
	Config.$inject = ['$q', '$http', 'LocalStorage', 'URLManager'];
	return Config;
});