/**
 * Created by vladimir on 2.2.2016 г..
 */
'use strict';

define(function () {
    function TokenStorage($http, Storage, URLManager) {
        return {
            _tokenConfig : {},
            _accessToken : {},
            /**
             *
             */
            getConfig: function(refresh) {
				var grant_type = 'refresh_token';
				if ( !refresh ) {
					grant_type = 'password';
				}
				this.setTokenConfig(
					{
						client_id       : 'mobile_app',
						client_secret   : 'e5998664b33f599e8bd633af081e30e1aa23f665',
						grant_type      :  grant_type,
						scope           : 'webapp'
					}
				);
            },
            /**
             *
             * @param config
             */

            setTokenConfig: function( config ) {
                this._tokenConfig = config;
            },
			/**
			 *
			 * @param refresh true for *refresh_token*,
			 **************** false for *password* token
			 * @returns {*}
			 */
            getTokenConfig: function (refresh) {
                this.getConfig(refresh);
                return this._tokenConfig;
            },
            /**
             *
             * @param token
             */
            setAccessToken : function(token) {
                this._accessToken = token;
            },
            /**
             *
             * @returns {*}
             */
            getAccessToken : function() {
                return this._accessToken;
            },
            /**
             * use this function with valid LMS credentials to receive an access token
             * for authentication in the LMS system
             *
             * @param username - Must be a valid username in the *current* LMS
             * @param password - Must be a valid password in the *current* LMS
             * @param callback if ajax post is successful
             * @param errorCallback if has error
             */
            getToken: function( username, password, callback, errorCallback) {
                var that = this;
                var TokenConf = this.getTokenConfig(false); // Holds the token configuration

                $http.post(
                    URLManager.createAbsoluteUrl('/oauth2/token'), {
                        client_id       : TokenConf.client_id,
                        client_secret   : TokenConf.client_secret,
                        grant_type      : TokenConf.grant_type,
                        scope           : TokenConf.scope,
                        username        : username,
                        password        : password
                    }
                ).success( function(data) {
                        callback(data);
                    }
                ).catch( function(err) {
                        if ( err.status != 200 ) {
                            if ( typeof errorCallback === "function") errorCallback(err);
                        }
                    });
            },
			/**
			 *
			 */
			refreshToken: function( refreshToken, callback, errorCallback ) {
				var that = this;
				var TokenConf = this.getTokenConfig(true);

				$http.post(
                    URLManager.createAbsoluteUrl('/oauth2/token'), {
						client_id		: TokenConf.client_id,
						client_secret	: TokenConf.client_secret,
						grant_type		: TokenConf.grant_type,
						scope			: TokenConf.scope,
						refresh_token	: refreshToken
					}
				).success( function(data) {
					callback(data);
					}
				).catch(function(err) {
					if ( err.status !== 200 ) {
						if ( typeof errorCallback === "function" )
							errorCallback(err);
					}}
				);
			},
            relativeUrl: function(URL) {
                return window.location.protocol + '//' + window.location.host + URL;
            },
			/**
			 * Check is the access token in "tokens" object expired ?
			 * @returns {boolean} | return true if is expired / we need false /
             */
			isExpired: function(){
				var token = Storage.get('tokens');
				var currentTime = new Date().getTime();
				if(typeof token === 'undefined' || token === null || currentTime >= token.expire_at){
					return true;
				}
				return false;
			},
			/**
			 * Parse token from query string and store's it in local storage
			 * @param {String} queryStringParam
			 * @returns {Void}
             */
			parseTokenFromUrl: function(queryStringParam){
				try{
					var response = JSON.parse(queryStringParam);
				}catch(e){
					console.log(e);
					return false;
				}
				response.success = null;
				this.storeToken(response);
				return true;
			},
			/**
			 * Stores oauth2 object to local storage
			 * @param {Object} oauth2Object
			 * @returns {Void}
             */
			storeToken: function(oauth2Object){
				oauth2Object = oauth2Object.token;
				oauth2Object['expire_at'] = new Date().getTime() + (oauth2Object.expires_in*1000);
				Storage.set('tokens', oauth2Object);
			}
        };
    }
	TokenStorage.$inject = ['$http', 'LocalStorage', 'URLManager'];
    return TokenStorage;
});