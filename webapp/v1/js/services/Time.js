/**
 * Created by vladimir on 2.2.2016 г..
 */
'use strict';

define(function () {
    Time.$inject = [];
    function Time() {
		this.UNITS = {
			Seconds: 0,
			Minutes: 1,
			Hours: 2,
			Days: 3,
			Months: 4,
			Years: 5
		};
		this.since =  function(date) {
			var seconds = Math.floor((new Date() - date) / 1000);
			var interval = Math.floor(seconds / 31536000);
			if (interval > 1) {
				return {time: interval, unit: this.UNITS.Years};
			}
			interval = Math.floor(seconds / 2592000);
			if (interval > 1) {
				return {time: interval, unit: this.UNITS.Months};
			}
			interval = Math.floor(seconds / 86400);
			if (interval > 1) {
				return {time: interval, unit: this.UNITS.Days};
			}
			interval = Math.floor(seconds / 3600);
			if (interval > 1) {
				return {time: interval, unit: this.UNITS.Hours};
			}
			interval = Math.floor(seconds / 60);
			if (interval > 1) {
				return {time: interval, unit: this.UNITS.Minutes};
			}
			return {time: Math.floor(seconds), unit: this.UNITS.Seconds};
		};
		this.sinceTimestamp = function(timestamp){
			var date = this.timestampToDate(timestamp);
			return this.since(date);
		};
		this.timestampToDate = function(timestamp){
			var date = new Date(timestamp);
			return date;
		};
    }
    return Time;
});