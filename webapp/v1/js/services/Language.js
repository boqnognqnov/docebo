/* global angular */
"use strict";

define(function(){
    /**
     * Return angular service for parsing channels data from server
     * @param {Service} $q
     * @param {Service} $http
	 * @param {Service} Storage
     * @param {Service} URLManager
     */
    function Language($q, $http, Storage, URLManager){
		return function(options) {
			var deferred = $q.defer();
			var langBrowser = Storage.get('langBrowser');
			var langCached = Storage.get('lang');
			if(langBrowser !== langCached){
				$http.post(URLManager.createApiUrl('language'), {'language': langBrowser})
			   .success(function(data){
					// CURRENT USER DATA
					 deferred.resolve(data);
				}).error(function(error){
					if ( error.status !== 200 ) {
						deferred.reject(error);
					}
				});
			}else{
				var dataLang = Storage.get('translationStrings');
				deferred.resolve(dataLang);
			}

		  return deferred.promise;
		};
    };
    Language.$inject = ['$q','$http', 'LocalStorage', 'URLManager']; //Injecting dependencies
    return Language; //Returns class definition
});
