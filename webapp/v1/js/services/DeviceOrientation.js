'use strict';
define(function() {
	/* Handling device orientation */
	function DeviceOrientation($window) {
		var rootContext = this; //Reference to this context
		/*
		 * Orientations struct for easier access
		 */
		rootContext.ORIENTATION = {
			PORTRAIT: 0,
			LANDSCAPE: 90,
			UPSIDE_DOWN: 180,
			LANDSCAPE_LEFT: -180
		};
		/**
		 * Returns window current orientation
		 * @returns {Int}
		 */
		rootContext.getDeviceOrientation = function(){
			var curOrientation = window.orientation;
			if(typeof curOrientation == 'undefined'){
				return rootContext.ORIENTATION.PORTRAIT;
			}
			return curOrientation;
		};
	};
	DeviceOrientation.$inject = ["$window"];
	return DeviceOrientation;
});