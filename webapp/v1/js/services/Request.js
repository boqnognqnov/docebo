/**
 * Created by vladimir on 14.1.2016 г..
 */
'use strict';
define([], function() {
    function Request($window, $location) {
		var $this = this;
		$this._queryString = $window.location.search;
		$this.getParam = function(name) {
			var needle = $this.getQueryString();
			needle = needle.replace(name+'=', '');
			//$window.location.search.replace(name+"="+needle, '')
			needle = decodeURIComponent(needle);
			if(typeof needle !== 'undefined' && needle != ''){
				return needle;
			}
			return false;
        };
		$this.getQueryString = function(){
			var queryString = $this._queryString.replace("?", '');
			return queryString;
		};
	};
    Request.$inject = ['$window', '$location'];
    return Request;
});