'use strict';
define(function() {
	function QueueItem($translate) {
		return {
			create: function(){
				return {
					/* Available states for the QueueItem */
					STATES: {
						Waiting: 0,
						Processing: 1,
						Failed: 2,
						Canceled: 3,
						Complete: 4,
						Converted: 5,
						Published: 6
					},
					/* Available types for the QueueItem */
					TYPES: {
						Video: 1,
						Image: 7
					},
					state: '', 
					file: '',
					name: '',
					originalFilename: '',
					uploadFileUrl: '',
					progress: 0,
					preview: '',
					type: '',
					idAsset: 0,
					actions: [] //Array of actions
				};
			}
		};
	};
	QueueItem.$inject = ['$translate'];
	return QueueItem;
});