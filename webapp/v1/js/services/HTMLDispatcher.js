define(function (require) {
	function HTMLDispatcher( $window, Storage ){
        var $$ = Dom7;
		return {
			
			ChangeTitle: function(title){
				if ( $$("#topTitle").length )
				$$("#topTitle").html(title);
			},
			back: function(){
				window.location.href = '#/all';
			},
            backToLP: function(id){
                if ( id != false || typeof id == 'undefined') {
                    window.location.href = '#/learning_plan/' + id;
                } else {
                    window.location.href = '#/all';
                }
            },
            isDesktop: function() {
                var OS = NativeAPI.device();
                if (OS == NativeAPI.OS.Desktop || OS == 'Desktop') {
                    return true;
                }
                return false;
            },
			logout: function(){
                if ( Storage.get('tokens') !== null ){
                   Storage.remove('tokens');
                }
                $window.location.href = '#/';
			},
            getAuthData: function() {
                /* Check is there any existing tokens in local storage then get them */
                if (typeof Storage.get('tokens') != 'undefined' && Storage.get('tokens') !== null) {
                    var tokens = Storage.get('tokens');
                    return tokens.token_type + ' ' + tokens.access_token;
                } else {
                    return false;
                }
            },
            sendCurrentPage: function(flag){},

            isNextLo: function(nextLo){},

            nativeLogout: function(flag) {
				this.logout();
            },

            rightPanelState: function(state) {},

            /**
             * get flag - dashboard is loaded
             * this is needed for android preloader
             * @param isLoaded
             */
            isDashboardLoaded: function(isLoaded) {},

            /**
             * set flag - search is loaded
             * @param isSearchLoaded
             */

            isGlobalSearchLoaded: function (isSearchLoaded) {},

            /**
             * set flag - search filter is loaded
             * @param isFilterLoaded
             */

            isSearchFiltersLoaded: function (isFilterLoaded) {},

            /**
             * Handle the downloading of the file
             * @param path
             * @constructor
             */
            FileLoAction: function(path){
                $window.open(path, '_blank', '');
            }
		};
	}
	HTMLDispatcher.$inject = ['$window', 'LocalStorage'];
	return HTMLDispatcher;
});

