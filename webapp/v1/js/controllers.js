'use strict';
define(function (require) {
	var module = require('angular').module('DoceboMobile.controllers', ['ngRoute', 'DoceboMobile.services', 'pascalprecht.translate', 'ngSanitize', 'DoceboMobile.providers']);
    // Add controllers the same way <module.controller('YourControllerName', 'Path/To/Controller');>
    module.controller('AppController',              require('controllers/AppController'));
    module.controller('LearningObjectController',   require('controllers/LearningObjectController'));
    module.controller('CourseDetailController',     require('controllers/CourseDetailController'));
    module.controller('LearningPlanController',     require('controllers/LearningPlanController'));
    module.controller('LoginController',            require('controllers/LoginController'));
    module.controller('ForgotPassController',       require('controllers/ForgotPassController'));
	module.controller('DashboardController',		require('controllers/DashboardController'));
	module.controller('ChannelsSingleController',	require('controllers/ChannelsSingleController'));
	module.controller('MyChannelController',	require('controllers/MyChannelController'));
	module.controller('UploadQueueController',	require('controllers/UploadQueueController'));
	module.controller('WalkthroughController',	require('controllers/WalkthroughController'));
	module.controller('PublishPageController',	require('controllers/PublishPageController'));
    module.controller('UploadHistoryController',	require('controllers/UploadHistoryController'));
	module.controller('TestController',	require('controllers/TestController'));
	module.controller('ViewAssetController',	require('controllers/ViewAssetController'));
	module.controller('InviteWatchController',	require('controllers/InviteWatchController'));
	module.controller('SearchController',	require('controllers/SearchController'));
	module.controller('SearchFiltersController',	require('controllers/SearchFiltersController'));
	module.run(require('run'));
	module.config(require('config'));
	return module;
});