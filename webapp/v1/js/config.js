'use strict';
define([], function () {
	function Config($routeProvider, $translateProvider, $sceDelegateProvider, $compileProvider, $sceProvider) {
		var Storage = window.localStorage;

		//$sceProvider.enabled(false);
		$compileProvider.debugInfoEnabled(true);
		$compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|file|tel|blob|data):/);
		$sceDelegateProvider.resourceUrlWhitelist([
			// Allow same origin resource loads.
			'self',
			// Allow loading from our assets domain.  Notice the difference between * and **.
			'http://lmsfiles-test.s3.amazonaws.com/**',
			'http://ec2-54-220-117-43.eu-west-1.compute.amazonaws.com/**',
			'//*.cloudfront.net/**',
			'http://*.cloudfront.net/**',
			'**.cloudfront.net/**',
			'*.cloudfront.net/**',
			'http://dcd.docebosandbox.com/**'

		]);
		$routeProvider
			.when('/login', {
                templateUrl:    'views/login.html',
                controller:     'LoginController',
				reloadOnSearch: false
            })
            .when('/all', {
                templateUrl:    'views/channels.html',
                controller:     'DashboardController',
				activetab: 'all'
            })
			.when('/channel/:id', {
                templateUrl:    'views/channelSingle.html',
                controller:     'ChannelsSingleController'
            })
			.when('/my/:type', {
                templateUrl:    'views/channelSingle.html',
                controller:     'ChannelsSingleController'
            })
			 .when('/myChannel', {
                templateUrl:    'views/myChannel.html',
                controller:     'MyChannelController',
				activetab: 'myChannel'
            })
            .when('/course/:courseId', {
                templateUrl:    'views/course.html',
                controller:     'CourseDetailController'
            })
            .when('/course/:courseId/learning_object/:learningObjectId', {
                templateUrl:    'views/learning_object.html',
                controller:     'LearningObjectController'
            })
            .when('/learning_plan/:learning_plan_id', {
                templateUrl:    'views/learning_plan.html',
                controller:     'LearningPlanController'
            })
            .when('/forgot', {
                templateUrl:    'views/forgot_pass.html',
                controller:     'ForgotPassController'
            })
			.when('/uploadQueue', {
                templateUrl:    'views/upload_queue.html',
                controller:     'UploadQueueController',
				activetab: 'uploadQueue',
				reloadOnSearch: false,
				resolve: {
					tags: function(Tags){
						return Tags.getApiTagsList();
					},
					channels: function (ChannelsData) {
						return ChannelsData.getApiChannelsList();
					}
				}
            })
			.when('/uploadHistory', {
				templateUrl:    'views/upload_history.html',
				controller:     'UploadHistoryController',
				activetab: 'uploadHistory'
			})
			.when('/walkthrough', {
                templateUrl:    'views/walkthrough.html',
                controller:     'WalkthroughController',
				resolve: {
					channels: function (ChannelsData) {
						return ChannelsData.getApiChannelsList();
					}
				}
            })
			.when('/test-controller', {
				controller:     'TestController',
				templateUrl:    'views/testController.html'
			})
			.when('/assets/view/:id', {
                templateUrl:    'views/view_asset.html',
                controller:     'ViewAssetController'
            })
			.when('/search/:result', {
                templateUrl:    'views/search_results.html',
                controller:     'SearchController'
            })
			.otherwise({
                redirectTo: '/login'
            });
			var langBrowser = null;
			var navigatorLang = navigator.language || navigator.userLanguage;
			try{
				langBrowser = Storage.getItem('langBrowser');
			}catch(e){
				langBrowser = navigatorLang.split('-')[0];
			}
			var translatorLang = typeof langBrowser !== 'undefined' || langBrowser !== null ? langBrowser : 'en';
			try{
				Storage.setItem('langBrowser', translatorLang);
			}catch(e){
				//console.log("You are using private mode, if you find");
			}
			$translateProvider.useLoader('Language', {});
			$translateProvider.useLoaderCache(false);
			$translateProvider.useSanitizeValueStrategy('escaped');
			$translateProvider.preferredLanguage(translatorLang);
			
	}
	Config.$inject = ['$routeProvider', '$translateProvider', '$sceDelegateProvider', '$compileProvider', '$sceProvider'];//Injecting dependencies
	return Config;
});