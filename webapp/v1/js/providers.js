'use strict';
define(function (require) {
	var module = require('angular').module('DoceboMobile.providers', ['DoceboMobile.services']);
	// Add controllers the same way <module.directive('NavbarDirective', 'Path/To/Filter');>
	return module;
});