function getQueryVariable(variable) {
    var query = window.location.hash.split("?")[1];
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
	console.log('Query variable %s not found', variable);
	return false;
}

require.config({
	baseUrl: 'js',
	waitSeconds: 35,
	paths: {
		angular: 'lib/angular',
		angularRoute: 'lib/angular-router',
		bootstrap: 'lib/bootstrap.min',
		framework7: 'lib/framework7',
		translate : 'lib/angular-translate.min',
		sanitize: 'lib/angular-sanitize.min',
		infiniteScroll: 'lib/ng-infinite-scroll.min',
		angularClipboard: 'lib/angular-clipboard',
		aws: 'lib/aws-sdk.min',
        app: 'app'
    },
	shim: { 
		'angular': {
			exports: 'angular'
		}, 
		'angularRoute': { deps: ['angular'] },
		'bootstrap':  {
			'exports' : 'bootstrap'
		},
		'framework7': {
			exports: 'Framework7'
		},
		'translate' : {
			exports: 'translate'
		},
		'sanitize' : {
			exports: 'sanitize'
		},
		'infiniteScroll':  {
			'exports' : 'infinite-scroll',
			'deps': ['angular']
		},
		'angularClipboard':  {
			'exports' : 'angularClipboard'
		},
		'aws': {
			'exports': 'AWS'
		},
		'app': {
			deps: [
				'angular', 
				'angularRoute',
				'bootstrap',
				'framework7',
				'infiniteScroll',
				'angularRoute',
				'framework7',
				'services',
				'filters',
				'directives',
				'controllers',
				'providers',
				'modules/DoceboMobile',
				'services/JSBridge',
				'angularClipboard'
			]
		}
	}
});

define(['angular', 'angularRoute', 'framework7', 'modules/DoceboMobile', 'infiniteScroll', 'bootstrap'], 
    function(angular) {
        // tell angular to wait until the DOM is ready
        // before bootstrapping the application
        require([
				'services',
				'filters',
				'directives',
				'controllers',
				'providers',
				'angularClipboard'
			],
			function() {
				angular.bootstrap(document, ['DoceboMobile']);
			});

});