/* global angular */
'use strict';
define(function () {
	/**
	 * Lazy loading directive class definition
	 * @returns {Class}
	 */
	function ImageError(){
		return {
			link: function(scope, element, attrs) {
				element.bind('error', function() {
					angular.element(element).hide();
				});
		   }
		};
	}
	ImageError.$inject = [];//Injecting dependencies
	return ImageError;//Returns Class definition
});