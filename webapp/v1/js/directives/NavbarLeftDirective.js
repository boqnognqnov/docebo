'use strict';
define(function () {
    function NavLeftDirective(Storage, NativeAPI, HTMLDispatcher, $window, $timeout, ChannelsData, User, Queue, $rootScope, $location){
        return {
            restrict: 'AE',
            controller: 'AppController',
			templateUrl: '/webapp/views/directives/navbar_left.html',
            link: function( scope, elem, $watch ) {
				/**
				 * Set inital values to scope parameters
				 * @type {string}
                 */
				scope.firstname = '';
				scope.lastname = '';
				scope.email = '';
				scope.avatar = '';

				scope.setUserInfo = function(name){
					if ( typeof Storage.get(name) !== 'undefined' && Storage.get(name) !== null){
						var userInfo 	= Storage.get(name);
						scope.firstname = userInfo.firstname;
						scope.lastname  = userInfo.lastname;
						scope.email 	= userInfo.email;
						scope.avatar 	= userInfo.avatar;
					}
				};

				scope.setUserInfo('mobile_info');
				scope.$on('mobileInfoSaved', function () {
					scope.setUserInfo('mobile_info');
				});
				
				scope.$on('checkWalkthrough', function(event, value){
					scope.skipWalkthrough = Storage.get('mobile_info').skip_walkthrough;
				});
				
				scope.$watch('appSettings', function(appSettings) {
					if(typeof appSettings != 'undefined' && appSettings != null) {
						scope.isShare7020AppActive = appSettings.plugins.Share7020App;
					}
				});
				
				/**
				* Close main left menu
				*/
			    scope.$Menuclose = function($link) {
					scope.el_sidemenu = $('.hidden-left-menu');
					scope.el_view = $('.views');
					scope.el_view.toggleClass('right-slide');
					scope.el_sidemenu.addClass('hidden');
					$window.location.href = $link;
					NativeAPI.dispatch('rightPanelState', "close");
				};

				/**
				 * Close main left menu without redirect
				 */
				scope.$topCloseButton = function() {
					scope.el_sidemenu = $('.hidden-left-menu');
					scope.el_view = $('.views');
					scope.el_view.toggleClass('right-slide');
					scope.el_sidemenu.addClass('hidden');
					NativeAPI.dispatch('rightPanelState', "close");
				};

				/**
				* Logout the user from the webapp and redirect him into the login screen
				*/
			   scope.$logout = function(){
					User.logout().then(function(data){
						NativeAPI.dispatch('nativeLogout', "logout");
						HTMLDispatcher.logout();
						scope.el_sidemenu =  $( '.hidden-left-menu' );
						scope.el_menu = $( '.top-nav-new' );
						scope.el_view = $( '.views' );

						scope.el_menu.addClass('hide-menu');
						scope.el_view.toggleClass('right-slide');
						scope.el_sidemenu.toggleClass('hidden');
						if(Storage.get('remember') !== null){
							Storage.remove('remember');
						}

						if ( Storage.get('mobile_info') !== null){
							Storage.remove('mobile_info');
						}

						if( Storage.get('tokens') !== null ){
							Storage.remove('tokens');
						}
						ChannelsData.clear();
						$rootScope.$broadcast('logout');
					});
			   };
			   
			   /* Detect active menu item and add class 'active' */
			   scope.isActive = function (viewLocation) {
					var active = (viewLocation === $location.path());
					return active;
			   };
            }
        };
    }
	NavLeftDirective.$inject = ['LocalStorage', 'NativeAPI', 'HTMLDispatcher', '$window', '$timeout', 'ChannelsData', 'User', 'Queue', '$rootScope', '$location'];//Injecting dependencies
	return NavLeftDirective;//Returns class definition
});