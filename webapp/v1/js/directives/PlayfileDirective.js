/* global angular, channelsDirectory, $user_id */
'use strict';
define(function () {
	/**
	 * Play file directive class definition
	 * @param {Service} $timeout
	 * @returns {Class}
	 */

	function PlayfileDirective($timeout, $window){
		
		return {
			scope: true,
			restrict: 'E',
			link: function(scope, elem, attr){
				scope.items = JSON.parse(attr.data); //Parsing initial slides

				var initiliaze = function(){ //Function used to init/reinit swiper carousel on angular $digest done
					var slideIndex = 1;
					var swiperOptions = {
						slidesPerView: 1,
						spaceBetween: 0,
						direction: 'horizontal',
						preloadImages: false,
						lazyLoading: true,
						lazyLoadingInPrevNext: true,
						setWrapperSize: true,
						onSlideChangeEnd : function() {
							slideIndex = scope.swiperFile.activeIndex+1;
							angular.element('.active-file-slide').text(slideIndex);
						}
					};
					$timeout(function(){
						//Creating instance of the Swiper for params information check http://idangero.us/swiper/api/#.Vt_YypN95cA
						scope.swiperFile = new Swiper(elem, swiperOptions);
						scope.totalSlides = scope.swiperFile.slides.length;
					});
				};
				//Calling swiper initially
				initiliaze();
				
				// Expand swiper carousel
				var swiperWrapper = angular.element('.play-asset-file');
				var expandIcon = angular.element('.expand-carousel');
				var shrinkIcon = angular.element('.shrink-carousel');
				var hammerJs;

				if($window.isiPhone == 1){
					scope.isIphone = 'true';
				}
				scope.$expandCarousel = function(){

					scope.$zoomInAsset(elem[0]);
					swiperWrapper.addClass('expanded');
					angular.element('.play-asset-file .files-wrap').css('height', angular.element(window).height()-56);
					angular.element('.play-asset-file.ios .files-wrap').css('height', angular.element(window).height()-73);
					angular.element('.margin-top52.view-asset-page').addClass('expanded-preview');
					shrinkIcon.show();
					expandIcon.hide();
				};
				scope.$shrinkCarousel = function(){

					scope.$zoomOutAsset(elem[0]);
					swiperWrapper.removeClass('expanded');
					expandIcon.show();
					shrinkIcon.hide();
					angular.element('.files-wrap').css('height', '100%');
					angular.element('.play-asset-file.ios .files-wrap').css('height', '100%');
					angular.element('.margin-top52.view-asset-page').removeClass('expanded-preview');
				};


				/**
				 * zoom in view asset
				 * @param elem
                 */

				scope.$zoomInAsset = function(elem){

					hammerJs = new Hammer(elem, {});
					hammerJs.get('pinch').set({
						enable: true
					});
					var posX = 0,
						posY = 0,
						scale = 1,
						last_scale = 1,
						last_posX = 0,
						last_posY = 0,
						max_pos_x = 0,
						max_pos_y = 0,
						transform = "",
						el = elem;

					hammerJs.on('doubletap pan pinch panend pinchend', function(ev) {

							if (ev.type == "doubletap") {
								transform =
									"translate3d(0, 0, 0) " +
									"scale3d(2, 2, 1) ";
								scale = 2;
								last_scale = 2;
								try {
									if (window.getComputedStyle(el, null).getPropertyValue('-webkit-transform').toString() != "matrix(1, 0, 0, 1, 0, 0)") {
										transform =
											"translate3d(0, 0, 0) " +
											"scale3d(1, 1, 1) ";
										scale = 1;
										last_scale = 1;
									}
								} catch (err) {
								}
								el.style.webkitTransform = transform;
								transform = "";
							}

							//pan
							if (scale != 1) {
								posX = last_posX + ev.deltaX;
								posY = last_posY + ev.deltaY;
								max_pos_x = Math.ceil((scale - 1) * el.clientWidth / 2);
								max_pos_y = Math.ceil((scale - 1) * el.clientHeight / 2);
								if (posX > max_pos_x) {
									posX = max_pos_x;
								}
								if (posX < -max_pos_x) {
									posX = -max_pos_x;
								}
								if (posY > max_pos_y) {
									posY = max_pos_y;
								}
								if (posY < -max_pos_y) {
									posY = -max_pos_y;
								}
							}

							//pinch
							if (ev.type == "pinch") {
								scale = Math.max(.999, Math.min(last_scale * (ev.scale), 4));
							}
							if (ev.type == "pinchend") {
								last_scale = scale;
							}

							//panend
							if (ev.type == "panend") {
								last_posX = posX < max_pos_x ? posX : max_pos_x;
								last_posY = posY < max_pos_y ? posY : max_pos_y;
							}

							if (scale != 1) {
								transform =
									"translate3d(" + posX + "px," + posY + "px, 0) " +
									"scale3d(" + scale + ", " + scale + ", 1)";
							}

							if (transform) {
								el.style.webkitTransform = transform;
							}

						//lock swiper on zoom detected
						//swiper is locked for better UX
						if(scale > 1) {
							scope.swiperFile.lockSwipes();
						} else {
							scope.swiperFile.unlockSwipes();
						}
					});
				};


				/**
				 * zoom out view asset
				 * @param elem
				 */

				scope.$zoomOutAsset = function(elem){
					var transform;
					var el = elem;

					transform = "translate3d(0, 0, 0)";
					el.style.webkitTransform = transform;

					hammerJs.off('doubletap pan pinch panend pinchend');
					hammerJs = new Hammer(elem, {});
					hammerJs.get('pinch').set({
						enable: false
					});
				};

				document.getElementById("file-play").addEventListener('touchstart',function() {
					scope.swiperFile.lockSwipes();
				},false);

				document.getElementById("file-play").addEventListener('touchend',function() {
					scope.swiperFile.unlockSwipes();
				},false);



				//On rotate screen
				angular.element($window).bind('resize', function () {
					if(swiperWrapper.hasClass('expanded')){
						angular.element('.files-wrap').css('height', angular.element(window).height()-56);
					}
				});
			},
			templateUrl: function(elem,attrs) {
				return 'views/directives/play_files.html'
			}
		};
	}
	PlayfileDirective.$inject = ['$timeout', '$window'];//Injecting dependencies
	return PlayfileDirective;//Returns class definition
});