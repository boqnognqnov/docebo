/**
 * Created by Mitko on 16.8.2016 г..
 */
'use strict';
define([], function () {
    function SearchAutoComplete($rootScope , $window, GlobalSearch, NativeAPI){
        return {
            restrict: 'AE',
            templateUrl: 'views/directives/search_autocomplete.html',
            scope: {},
            link: function(scope, elem, attr){

                scope.showSuggestions = false;
                scope.suggestionsModel = '';
                scope.suggestions = {};
                var typingTimer;

                scope.checkOS = function () {
                    if (NativeAPI.device() == NativeAPI.OS.iOS) {
                        return true;
                    }
                    return false;
                };


                /**
                 * close search overlay
                 */

                scope.closeSearch = function () {
                    document.getElementById("search-wrapper").style.display = 'none';
                    scope.resetField();
                    NativeAPI.dispatch('isGlobalSearchLoaded', "false");
                    $rootScope.$broadcast("clearSearchFilter");
                };


                /**
                 * onClick X reset input field
                 */

                document.getElementById("reset-input-id").addEventListener("touchstart", function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    document.getElementById('search-autocomplete-dropdown').focus();
                    scope.resetField();
                }, false);

                scope.resetField = function () {
                    var dropdown = $('.search-overlay-suggestions');
                    dropdown.hide();

                    document.getElementById("search-autocomplete-dropdown").value = "";
                    scope.suggestionsModel = '';
                    scope.suggestions = [];
                    document.getElementById("reset-input-id").style.display = 'none';
                };


                /**
                 * check suggestion is empty
                 * @param value
                 * @returns {boolean}
                 */

                scope.isEmpty = function (value) {
                    if(value == "") {
                        return false;
                    }
                    return true;
                };

                /*
                 * send to Global Search
                 * @param {String} value
                 * @return {Void}
                 */
                scope.searchTrigger = function(value){
                    $window.location.href = '#/search/' + value;
                    scope.closeSearch();
                };

                /**
                 * send value to Global Search via enter key pressed
                 * @param e
                 */

                scope.enterSearchPressed = function(e){
                    if(e.keyCode === 13){
                        var searchResult =  document.getElementById("search-autocomplete-dropdown").value;
                        scope.searchTrigger(searchResult);
                    }
                };

                scope.$watch('suggestionsModel', function(nv) {
                    var dropdown = $('.search-overlay-suggestions');
                    dropdown.show();

                    if (nv !== '') {
                        clearTimeout(typingTimer);
                        typingTimer = setTimeout(
                            function(){
                                scope.getApiSuggestions(nv); //send value to GlobalSearch service
                                scope.showSuggestions = true;
                            },
                            500
                        );
                        document.getElementById("reset-input-id").style.display = 'block';
                    } else {
                        clearTimeout(typingTimer);
                        document.getElementById("reset-input-id").style.display = 'none';
                        scope.showSuggestions = false;
                        document.getElementById("search-autocomplete-dropdown").value = "";
                        scope.suggestions = [];
                    }
                });


                /**
                 * get api data from suggestions
                 * @param suggestions
                 */

                scope.getApiSuggestions = function(suggestions){
                    var promise =  GlobalSearch.gsData(suggestions);
                    promise.then(
                        function(data) {
                            scope.suggestions = Object.keys(data.suggestions).map(function (key) {
                                var htmlText = data.suggestions[key] ? String(data.suggestions[key]).replace(/<[^>]+>/gm, '') : '';
                                return htmlText
                            });
                        },
                        function(error) {
                            console.log(error);
                        },
                        function(progress) {
                        });
                };

                NativeAPI.registerFunc('closeSearchAutoComplete', function() {
                    scope.closeSearch();
                });

                scope.$on('clearSuggestions', function() {
                    scope.suggestions = [];
                });


                /**
                 * remove scroll on black overlay - this issue is just on ios
                 */

                document.addEventListener('touchmove', function(e) {
                    if(document.getElementById("search-wrapper").style.display == 'block' &&  scope.suggestions.length == 0) {
                        e.preventDefault()
                    } else {
                        return true;
                    }
                }, false);
            }
        };
    }
    SearchAutoComplete.$inject = ['$rootScope', '$window', 'GlobalSearch', 'NativeAPI'];//Injecting dependencies
    return SearchAutoComplete;//Returns class definition
});