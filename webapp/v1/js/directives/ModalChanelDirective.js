/**
 * Created by Mitko on 4/4/2016.
 */
'use strict';
define(function(){

    function ModalChanelDirective($rootScope, $location,  ChannelsData, $timeout) {
        return {
            restrict: 'AEC',
            scope: {
                storeChannelIds:"=channelsModel"
            },
            templateUrl: 'views/directives/modal_chanel.html',
            link: function(scope, elem, attr) {

                scope.channelItemsFull = ChannelsData.getChannelList();
                scope.channelItems = scope.channelItemsFull.channels;
                var allChannelSize = [];

                //get all valid items - is used to check if user is clicked all items one by one
                for (var i = 0; i < scope.channelItems.length; i++) {
                    if (scope.channelItems[i].channelName.name != null) {
                        allChannelSize.push(parseInt(scope.channelItems[i].idChannel))
                    }
                }

                if(scope.storeChannelIds == null || scope.storeChannelIds == 'undefined'){
                    scope.storeChannelIds = [];
                    $rootScope.channelIdsCopy = [];
                }

                if(allChannelSize.length == scope.storeChannelIds.length) {
                    document.getElementById('mark-all-items').className = 'row-channel-selected';
                } else {
                    document.getElementById('mark-all-items').className = 'row-channel-not-selected';
                }


                //add animation
                var myApp = new Framework7();
                var $$ = Dom7;
                myApp.popup('.popup-about');


                /**
                 * set all ids in array
                 * @param channelItems
                 */

                scope.markAllItems = function (channelItems) {

                    if (scope.storeChannelIds.length == 0) {
                        scope.storeChannelIds = [];
                        for (var i = 0; i < channelItems.length; i++) {
                            if (channelItems[i].channelName.name != null) {
                                scope.storeChannelIds.push(parseInt(channelItems[i].idChannel));
                                document.getElementById('mark-all-items').className = 'row-channel-selected';
                            }
                        }

                    } else if (scope.storeChannelIds.length > 0 && scope.storeChannelIds.length < allChannelSize.length) {

                        scope.storeChannelIds = [];
                        for (var i = 0; i < channelItems.length; i++) {
                            if (channelItems[i].channelName.name != null) {
                                scope.storeChannelIds.push(parseInt(channelItems[i].idChannel));
                                document.getElementById('mark-all-items').className = 'row-channel-selected';
                            }
                        }
                    } else {
                        scope.storeChannelIds = [];
                        document.getElementById('mark-all-items').className = 'row-channel-not-selected';
                    }
                };


                /**
                 * mark selected element
                 * @param value
                 * @returns {boolean}
                 */

                scope.isChecked = function(value) {
                    var index = scope.storeChannelIds.indexOf(parseInt(value));
                    if(index > -1){
                        return true;
                    }
                    return false;
                };

                /**
                 * set selected ids in array
                 * @param index
                 */

                scope.markSelectedChannel = function (index) {
                    //get id on current item
                    var value = angular.element("#select_option_"+index).attr('data-value');
                    //check if the id is add it to array
                    var arrIndex = scope.storeChannelIds.indexOf(parseInt(value));
                    if(arrIndex > -1){
                        scope.storeChannelIds.splice(arrIndex, 1);
                    } else {
                        scope.storeChannelIds.push(parseInt(value));
                    }

                    if(allChannelSize.length == scope.storeChannelIds.length){
                        document.getElementById('mark-all-items').className = 'row-channel-selected';
                    } else {
                        document.getElementById('mark-all-items').className = 'row-channel-not-selected';
                    }
                };
				
				//Add max-height to the scrollable modal content
				angular.element(".ch-modal").css('height', angular.element('.popup.modal-in').height()-110);
				
				//Submit chosen channels and close modal
				scope.submitChannels = function(){
                    $rootScope.channelIdsCopy = [];
                    $rootScope.channelIdsCopy = scope.storeChannelIds.concat();

                    $rootScope.$broadcast("modalLength", scope.storeChannelIds);
					myApp.closeModal();
				};


                /**
                 * didn't save channel ids if user didn't clicked on confirm button
                 */

                scope.closeChannelModal = function () {

                    if($rootScope.channelIdsCopy.length != scope.storeChannelIds.length) {
                        scope.storeChannelIds = [];
                        scope.storeChannelIds = $rootScope.channelIdsCopy.concat();
                        $rootScope.$broadcast("modalLength", scope.storeChannelIds);
                    }
                };

            }
        };
    }
    ModalChanelDirective.$inject = ['$rootScope', '$location', 'ChannelsData', '$timeout'];
    return ModalChanelDirective;
});