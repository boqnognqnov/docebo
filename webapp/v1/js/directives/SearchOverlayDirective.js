/**
 * Created by Mitko on 12.8.2016 г..
 */
'use strict';
define([], function () {
    function SearchOverlayDirective(NativeAPI, $rootScope){
        return {
            restrict: 'AE',
            templateUrl: 'views/directives/search_overlay.html',
            scope: {},
            link: function(scope, elem, attr){

                var isFieldClicked = false;

                document.getElementById("search-input-wrapper-id").addEventListener('click', function () {
                    isFieldClicked = true;
                }, false);
				document.onkeyup=function(e) {
					if(e.which === 13){
						$('#search-autocomplete-dropdown').blur();
						return false;
					}
				};

                scope.closeSearch = function () {
                    if(!isFieldClicked) {
                        elem[0].style.display = 'none';
                        this.resetField();
                        NativeAPI.dispatch('isGlobalSearchLoaded', "false");
                        $rootScope.$broadcast("clearSearchFilter");
                    } else {
                        isFieldClicked = false;
                    }
                };
                scope.resetField = function () {
                    var dropdown = $('.search-overlay-suggestions');
                    dropdown.hide();

                    document.getElementById("search-autocomplete-dropdown").value = "";
                    $rootScope.$broadcast('clearSuggestions');
                };
            }
        };
    }
    SearchOverlayDirective.$inject = ['NativeAPI', '$rootScope'];//Injecting dependencies
    return SearchOverlayDirective;//Returns class definition
});