/* global angular, channelsDirectory, $user_id */
'use strict';
define([], function () {
	/**
	 * Swiping walkthrough directive class definition
	 * @param {Service} $timeout
	 * @returns {Class}
	 */
	function SwipingCards($timeout, $rootScope, $route, Storage){
		return {
			templateUrl: 'views/directives/swipecards.html',
			restrict: '	EA',
			scope: {
				items: '=',
				buttonHandler: '&nextButtonHandler'
			},
			link: function(scope, elem, attr) {
				scope.pluginLoaded = false;
				scope.buttonHandler = function(cardId) {
					$rootScope.$broadcast('swipeCards:next', cardId);
				};

					if($route.current.controller === 'UploadQueueController'){
						angular.element('.cardbutton-wrapper').on('touchstart', function(e){
							//$(".tinderslide").jTinder('like').jTinder('like');
							Storage.set('isFirstUpload', 1); // Do not show after click on the button
							$("body.home-page").removeClass('with-panel-right-cover');
						});
					}
			}
		};
	}
	SwipingCards.$inject = ['$timeout', '$rootScope', '$route', 'LocalStorage'];//Injecting dependencies
	return SwipingCards;//Returns class definition
});