/**
 * Created by Ignatov on 29.03.2016
 */
'use strict';
define(function(){
    function NativePicker($timeout) {
        return {
            restrict: 'A',
			templateUrl: 'views/directives/native_picker.html',
			scope: {
				pickFile: "&onUploadChanged"
			},
			link: function(scope, elem, attr){
				var dom = function(elem){
					return angular.element(elem);
				};
				var inputUpload = dom(elem).children()[0];
				$timeout(function() {
					dom(inputUpload).click();
				});
				dom(elem).on('click', function(){
					return $timeout(function() {
						dom(inputUpload).click();
					});
				});
				dom(inputUpload).on('change', function(){
					var fileList = inputUpload.files;
					var file = fileList[0];
					scope.pickFile({file: file});
					inputUpload.value = '';
				});
			}
        };
    }
	NativePicker.$inject = ['$timeout'];
    return NativePicker;
});