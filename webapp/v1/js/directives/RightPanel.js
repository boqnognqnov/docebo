/* global angular, channelsDirectory, $user_id */
'use strict';
define([], function () {
	/**
	 * Swiping walkthrough directive class definition
	 * @param {Service} $timeout
	 * @returns {Class}
	 */
	function RightPanel($timeout, $rootScope){
		return {
			restrict: 'AE',
			templateUrl: 'views/directives/rightPanel.html',
			scope: {},
			controller: "@",
			name: "controllerName",
			link: function(scope, elem, attr){
				
				/* close rigth panel - workaround, as default framewrok7 function not working properly */
				var bodyElem = $('body');
				scope.closePanels = function(){
					if(bodyElem.hasClass('with-panel-right-cover')){
						bodyElem.removeClass('with-panel-right-cover');
					}
					scope.$broadcast('rightPanelClosed');
				};
				
				
				
				//Fixed header bar
				angular.element('.content-asset-block').css('height', angular.element(window).height()-52);
			}
		};
	};
	RightPanel.$inject = ['$timeout', '$rootScope'];//Injecting dependencies
	return RightPanel;//Returns class definition
});