/* global angular, channelsDirectory, $user_id */
'use strict';
define(function () {
	/**
	 * Embeded video directive class definition
	 * @returns {Class}
	 */

	function EmbededVideoDirective($timeout, $compile){
		return {
			scope: true,
			restrict: 'E',
			template: '<div class="iframe-wrapper" ></div>',
			link: function(scope, elem, attr){
				scope.youtubeLink = '';
				scope.vimeoLink = '';
				scope.wistiaLink = '';
				
				switch (attr.type) {
					case 'youtube':
						scope.youtubeLink = 'https://www.youtube.com/embed/'+attr.hash;
						elem.html($compile(renderYoutube())(scope));
						break;

					case 'vimeo':
						scope.vimeoLink = 'https://player.vimeo.com/video/'+attr.hash;
						elem.html($compile(renderVimeo())(scope));
						break;

					case 'wistia':
						scope.wistiaLink = attr.hash;
						elem.html($compile(renderWistia())(scope));
						break;
				};
				
				function renderYoutube() {
					return '<iframe class="em-vid" frameborder="0" allowfullscreen="1" width="640" height="360" ng-src="'+scope.youtubeLink+'"></iframe>';
				}

				function renderVimeo() {
					return '<iframe class="em-vid" frameborder="0" width="640" height="360" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true" ng-src="'+scope.vimeoLink+'"></iframe>';
				}

				function renderWistia() {
					return '<iframe src="//fast.wistia.net/embed/iframe/'+scope.wistiaLink+'?videoFoam=true" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed em-vid" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="640" height="360"></iframe>';
				}
				
			}
		};
	}
	EmbededVideoDirective.$inject = ['$timeout', '$compile'];//Injecting dependencies
	return EmbededVideoDirective;//Returns class definition
});