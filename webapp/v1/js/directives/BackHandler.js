'use strict';
define(function () {
	function BackHandler($route, $window, TrackUser) {
		return {

			restrict: '',
			link:  function (scope, element) {
				scope.$on('go:back', function(event, argument){
					var redirectUrl = ['#',argument.route].join('');
					$window.location.href = redirectUrl;
				});
			}
		};
	}
	BackHandler.$inject = ['$route', '$window', 'TrackUser'];
	return BackHandler;
});