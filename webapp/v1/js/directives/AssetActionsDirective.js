'use strict';
define(function () {
	function AsssetActionsDirective($timeout) {
		
		var menus = new Array();
		menus['download'] = {
			'class': 'fa-download',
			'key': 'download',
			'href': 'http://dir.bg'
		};
		menus['invite'] = {
			'class': 'icon-d_invite',
			'key': 'invite'
		};
		menus['report'] = {
			'class': 'fa-exclamation-triangle',
			'key': 'report'
		};
		menus['watch_later'] = {
			'class': 'fa-history',
			'key': 'watch_later'
		};
		menus['sharing'] = {
			'class': 'fa-share-alt',
			'key': 'sharing'
		};
		return {
			restrict: '',
			templateUrl: 'views/directives/asset_actions.html',
			link:  function (scope, element, attr, ctrl) {
			scope.getMenuClass = function (param) {
				
					if (typeof (menus[param]) != 'undefined') {
						return menus[param].class;
					} else {
						return false;
					}
				}

				scope.getKey = function (param) {
					if (param == 1) {
						return menus['invite'].key;
					} else {
						return 'undefined';
					}
				}

			}
		};
	}
	AsssetActionsDirective.$inject = ['$timeout'];
	return AsssetActionsDirective;
});