/* global angular */

'use strict';
define(function(){
   function ActionSheet(){
       return {
           restrict: 'EA',
           scope: {
               actions:    '='
           },
           link: function(scope, element, attr){
			   scope.elem_id = attr.id;
               angular.element(element).on('click', function() {
					var webApp = new Framework7({
                        modalCloseByOutside: true,
                        material: true,
                        modalActionsCloseByOutside: true
                    });
					webApp.actions(scope.actions);


                   /**
                    * close popup on click
                    * @param evt
                    */
                   function closePopup(evt) {
                       evt.preventDefault();
                       webApp.closeModal();
                       angular.element(".modal-button").trigger('click');
                   }

                   var el = document.getElementsByClassName("modal-overlay-visible")[0];
                   el.addEventListener("touchend", closePopup, false);
               });
           }
       };
   }
    ActionSheet.$inject = [];
    return ActionSheet;
});