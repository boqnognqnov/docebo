'use strict';
define(function () {
	function AsssetRatingDirective($timeout, $routeParams, CSAsset, Storage, $translate) {
		return {
			restrict: '',
			templateUrl: 'views/directives/asset_rating.html',
			link: function(scope, elem, attr){
				scope.stars = [];
				$translate(['Thank you!']).then(function(translations){
					scope.successMessage = translations['Thank you!']
				});
				scope.updateStars = function() {
					
					//if(scope.details.canrate){
						scope.max = 5;
						for (var i = 0; i < scope.max; i++) {
							if (scope.ratingValue1 % 1 !== 0) {
								var valFloating = scope.ratingValue;
								var decimalsValue = (valFloating % 1).toFixed(2);
							}
							scope.stars[i] = {
								filled: i < scope.ratingValue1,
								half: i == Math.floor(scope.ratingValue1) && scope.ratingValue1 % 1 !== 0 && decimalsValue <= '0.75' && decimalsValue >= '0.25',
								empty: i == Math.floor(scope.ratingValue1) && scope.ratingValue1 % 1 !== 0 && decimalsValue < '0.25'
							};
						}
					//}
				};
				
				/* Rate the asset */
				scope.toggle = function (index) {
					if(!scope.details.canrate){
						return false;
					}
					var promise = CSAsset.assetRating($routeParams.id, index, Storage.get('mobile_info').idst);
					promise.then(function(response){
						if (response) {
							scope.ratingValue1 = response.return;
							scope.addSuccessMessage(scope.successMessage, elem[0].parentElement);
						/*	if (!angular.element('.rating-directive').hasClass('readonly')) {
								scope.ratingValue = index + 1;
								scope.updateStars();
							}*/
						}
					});
						
					scope.$emit('disableLoading');
				};
				/**
				 * After voting on the rating add success message for the User
				 * @param messageText - The text which display after the ElementToAppend
				 * @param elementToAppend - Append the messageElem after this element
                 */
				scope.addSuccessMessage = function (messageText, elementToAppend) {
					if (angular.element('#vote-success-message').length > 0){ return; }
					var messageElem = document.createElement('span');
					messageElem.id = 'vote-success-message';
					messageElem.appendChild(document.createTextNode(messageText));
					elementToAppend.appendChild(messageElem);
					$timeout(function(){
						angular.element('#vote-success-message').hide('slow');
					}, 3000);
				};
				/**
                 * Watch the rating value
				 * @param ratingValue1 - when the value is changed update the stars
				 */
				scope.$watch('ratingValue1', function (value1) {
					if (typeof value1 !== 'undefined') {

						scope.updateStars();
					}
				});
			}				
		};
	}
	AsssetRatingDirective.$inject = ['$timeout', '$routeParams', 'CSAsset', 'LocalStorage', '$translate'];
	return AsssetRatingDirective;
});