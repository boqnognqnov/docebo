'use strict';
define(function () {
	function TabsbarDirective($route, $parse, NativeAPI){
		return {
			restrict: 'E',
			templateUrl: '/webapp/views/directives/tabsbar.html',
			scope: {
				items: "="
			},
			link: function(scope, elem, attr){
				scope.isIphone = NativeAPI.device() == NativeAPI.OS.iOS ? true : false;
				scope.$active = 0;
				var current = $route.current.$$route.activetab;
				if(typeof current !== 'undefined'){
					scope.$active = current;
				}
				scope.$on('$routeChangeSuccess', function(event, next){
					if(typeof next.$$route != 'undefined'){
						scope.$active = next.$$route.activetab;
					}
				});
			}
		 };
	}
	TabsbarDirective.$inject = ['$route', '$parse', 'NativeAPI'];
	return TabsbarDirective;
});