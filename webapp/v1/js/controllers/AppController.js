/**
 * Created by vladimir on 5.1.2016 г..
 */
//'use strict';
define(['require', 'translate', 'sanitize'], function (require) {
    function AppController($scope, $location, $routeParams, NativeAPI, $rootScope, HTMLDispatcher, $timeout,
						   $window, Config, User, Storage, Course, TrackUser, Cards) {
		var Panels = new Framework7();
		$rootScope.panelController = 'PublishPageController';
        var $$ = Dom7;
		Cards.getCards();
        /**
         * On refresh page apply the colors from the API request earlier..
         */
        $scope.$refreshNavbar = function(){
			/*if(Storage.get('appSettings') == null){
				Config.lmsConnect();
			}*/
			if(window.isiPhone == 1){
				User.getUserInfo(
					function(data){
						Storage.set('mobile_info', data);
						/* GET CURRENT USER ID */
						$rootScope.userIdst = Storage.get('mobile_info').idst;

					}, function(error){
						if ( error.statusText === 'Unauthorized') {
							$location.path('/login');
						}
						console.log(error);
						console.log("Cannot retrieve User info from LMS.");
				});
				$scope.isIphone = 'true';
			}
        };

        if ( Storage.get('learning_plan_id') != null ) {
            $scope.learningPlanId = Storage.get('learning_plan_id');
        }

        $timeout(function(){
            $scope.$refreshNavbar();
            if ( Storage.get('learning_plan_id') != null ) {
                $scope.learningPlanId = Storage.get('learning_plan_id');
            }
            if ( typeof $routeParams === 'object' ) {
                //$scope.idOrg    = $routeParams.learningObjectId;
                $scope.idCourse = $routeParams.courseId;
            }
        });
		
		/* Open three dots link menu */
		$rootScope.open = function($event){
			var selector = angular.element($event.currentTarget).parent().parent().find('.dots-menu');
			selector.addClass('open');
		};
		/* Close three dots link menu */
		$rootScope.close = function($event) {
			var selector = angular.element($event.currentTarget).parent().closest('.dots-menu');
			selector.removeClass('open');
		};
		
		/* Close dots menu on clicking outside of the tile */
		$(document).on('click', function (e) {
			if( typeof event != 'undefined' && $(event.target).parents('.col-tile').length  === 0 ) {
                $(e.target).find(".dots-menu").removeClass('open');
            }
		});
		
		/* Deeplink functionality (copy to clipboard) */
		$scope.getShareurl = function($event){
			var selector = angular.element($event.currentTarget).parent().find('.copy-url');
			var selectorInitial = angular.element($event.currentTarget);
			var selectorCopied = angular.element($event.currentTarget).parent().find('.copied-success');
			$scope.textToCopy = selector.attr('href');
			
			$scope.success = function () {
				selectorInitial.hide();
				selectorCopied.show();
				$timeout(function() {
					selectorInitial.show();
					selectorCopied.hide();
				}, 2000);
			};

		};

        /**
         * Return you in the previous page
         */
        $scope.$back = function() {
			var pageAddress = TrackUser.popRoute();
			$rootScope.$broadcast("go:back", {route: pageAddress});
			//HTMLDispatcher.back();
        };
        /**
         * If user comes from a learning plan be send him again there when he is
         * going back from a course.
         * @param id = learning plan id
         */
       $scope.$backToLP = function(id) {
			var id = ''; 
			if(Storage.get('learning_plan_id') != null) {
				id = Storage.get('learning_plan_id');
			}
		   HTMLDispatcher.backToLP(id);
        };

        $scope.$backToGlobalSearch = function() {
			//$scope.$back();
			HTMLDispatcher.back();
			document.getElementById("search-wrapper").style.display = 'block';
			document.getElementById("search-autocomplete-dropdown").focus();
			NativeAPI.dispatch('isGlobalSearchLoaded', "true");
		};

        $scope.$hasLearningPlanId = function(){
            return ( Storage.get('learning_plan_id') != null ? true : false );
        };
		
        angular.element(document).ready(function() {
            if ( Storage.get('learning_plan_id') != null ) {
                $scope.learningPlanId = Storage.get('learning_plan_id');
            }
		});
			//var animateJs = new require('animate');
			//var translateJs = new require('translate');
			//var sanitizeJS = new require('sanitize');

		$scope.$hasTabs = function(){
			var loc = function(needle){
				return $location.$$url.indexOf(needle);
			};
			if (loc('/all') == 0 || loc('/myChannel') == 0 || loc('/uploadQueue') == 0 || loc('/uploadHistory') == 0) {
				return true;
			}
			return false;
		};

		NativeAPI.registerFunc('$back', function() {
			$scope.$back();
		});

		NativeAPI.registerFunc('back', function() {
			HTMLDispatcher.back();
		});

		NativeAPI.registerFunc('backToLP', function() {
			$scope.$backToLP();
		});

		NativeAPI.registerFunc('rightPanelState', function() {
			$scope.rightPanelState();
		});

		NativeAPI.registerFunc('$backToGlobalSearch', function() {
			$scope.$backToGlobalSearch();
		});

		NativeAPI.registerFunc('searchFiltersLoaded', function() {
			NativeAPI.dispatch('isSearchFiltersLoaded', "false");
		});

		/**
		 * If you are in course return true to set Back Button in Menu header
		 * @param {String} name
		 * @param {String|Object|Int} data
		 * @returns {boolean}
		 */
		$rootScope.callFunc = function (name, data){
			NativeAPI.callFunc(name, data);
		};
		/**
		 *
		 * @returns {boolean}
		 */
		$scope.$returnEnteredPage = function() {
			if( $location.$$url.indexOf("search") > 0 ) {
				return 'search';
			} else if ( $location.$$url.indexOf("my/courses") > 0){
				return 'myCourses';
			} else if ( $location.$$url.indexOf("course") > 0 && $location.$$url.indexOf("learning") < 1 ) {
				return 'course';
			} else if ( $location.$$url.indexOf("learning_obj") > 0 ) {
				return 'learning_object';
			} else if ($location.$$url.indexOf("channel") > 0 ){
				return 'viewAllChannels';
			} else if ( $location.$$url.indexOf("learning_plan") > 0 ) {
				return 'learning_plan';
			} else if ( $location.$$url.indexOf("assets") > 0 ){
				return 'viewAllChannels';
			}
			else {
				return 'home';
			}
		};
		
		/* Reveal rightside panel framework7 */
		$scope.$openRightPanel = function(uploadedItem) {
			if ( typeof uploadedItem !== 'undefined' ) {
				var objectToUpload = {url: uploadedItem.uploadFileUrl, instance: 'publish-asset'};
				$scope.$broadcast('RightPanelOpened', uploadedItem);
				$rootScope.$broadcast('RightPanelVideo', objectToUpload);
			}
			Panels.openPanel('right');
			$scope.bodyElem = $('body');
			if(!$scope.bodyElem.hasClass('with-panel-right-cover')){
				$scope.bodyElem.addClass('with-panel-right-cover');
			}
		};

		/* Close rightside panel framework7 */
		$scope.$closeRightPanel = function() {
			Panels.closePanel('right');
		};

		/**
		 * Return true if the OS viewing the APP is DESKTOP
		 * @returns {boolean}
		 */
		$scope.$isDesktop = function() {
		/*	var OS = NativeAPI.device();
			if ( OS === NativeAPI.OS.Desktop ) {
				return true;
			}*/
			return true;
		};
		$scope.$isLocked = function(course) {
			var status = course.status;
		};
		/**
		 * The values of the tab filter values
		 * @type {string[]}
		 */

		$scope.menuItems = [
			'All Courses',
			'Unlocked Courses',
			'Locked Courses',
			'Completed Courses'
		];
		/**
		 * Default Active Menu Tab in the Navigation Bar
		 * @type {string}
		 */
		$scope.activeMenu = 'All Courses';
		/**
		 * Set Active tab in the navigation bar by passing a string
		 * @param menuItem
		 */
		$scope.setActive = function(menuItem) {
			$scope.activeMenu = menuItem;
		};
		
		/**
		 * Get back to the Course(list of learning objects)
		 */
		$scope.$returnToCourse = function() {
			$scope.$broadcast('LearningObjectClosed');
			$location.path('/course/' + $routeParams.courseId);
			TrackUser.popRoute();
			if ($rootScope.$root.$$phase != '$apply' && $rootScope.$root.$$phase != '$digest') {
				$rootScope.$apply();
			}

		};
		NativeAPI.registerFunc('returnToCourse', $scope.$returnToCourse);

		/**
		 * Go to the next Learning Object (if it is unlocked),
		 * Skip unsupported Learning Objects
		 */
		$scope.$nextLearningObject = function() {

			if(Storage.get('loInfo') != null && Storage.get('loInfo') !== "undefined"
					&& typeof $rootScope.nextLearningObjectId != "undefined") {

				var loInfo = Storage.get('loInfo');
				var isLoValid;
				var nextLoId;
				var currentItemPosition;

				//get current position
				for (var itemPosition in loInfo) {
					if (loInfo.hasOwnProperty(itemPosition)) {
						nextLoId = loInfo[itemPosition].idOrganization;
						if (nextLoId == $rootScope.nextLearningObjectId) {
							currentItemPosition = parseInt(itemPosition);
							break;
						}
					}
				}

				//translate to next learning object
				for (currentItemPosition; currentItemPosition < loInfo.length; currentItemPosition++) {
					if (loInfo.hasOwnProperty(currentItemPosition)) {
						isLoValid = $scope.checkLoObjectTypes(loInfo[currentItemPosition].type);
						nextLoId = loInfo[currentItemPosition].idOrganization;

						if (isLoValid) {
							if(loInfo[currentItemPosition].type == "sco") {

								if(nextLoId == $rootScope.nextLearningObjectId){
									if(loInfo.length - 1 == currentItemPosition){
										$window.location.href = '#/course/' + $routeParams.courseId + '/learning_object/' + nextLoId;
										break
									}
									continue;
								}else {
									$window.location.href = '#/course/' + $routeParams.courseId + '/learning_object/' + nextLoId;
									break;
								}
							} else {
								$window.location.href = '#/course/' + $routeParams.courseId + '/learning_object/' + nextLoId;

								if ($rootScope.$root.$$phase != '$apply' && $rootScope.$root.$$phase != '$digest') {
									$rootScope.$apply();
								}
								break;
							}
						}
					}

				}
			}
		};

		/**
		 * check objectType is valid
		 * @param objectType
		 * @returns mObjectType - is valid or not
		 */

		$scope.checkLoObjectTypes = function(objectType) {

			var mObjectType;

			switch (objectType) {
				case "htmlpage":
				case "sco":
				case "test":
				case "video":
				case "tincan":
				case "aicc":
				case "elucidat":
				case "slides":
				case "authoring":
				case "file":
				case "googledrive":
					mObjectType = true;
					break;
				default :
					mObjectType =  false;
			}
			return mObjectType;
		};

		if ( typeof $routeParams.courseId != 'undefined') {
			Course.getLoInfo($routeParams.courseId,
					function(data){
						$scope.learningObjects = data;
						$scope.courseInfo = data.courseInfo;

						/* Wait loding of meta details before showing the container */
						$scope.courseDetailsLoaded = true;

						$$('.white-title-folder').css('color', 'white');

						Storage.set('loInfo', data.items);
					}, function(error){
						if ( error.statusText == 'Unauthorized') {
							$location.path('/login');
						}
						console.log("ERROR: ");
						console.log(error);
					});
		}

		/* Watch for unlocking the next course */
		$scope.$watch('isLoCompleted', function(newVar, oldVar){
			if ( typeof newVar != 'undefined' && newVar != '' ){
				$scope.isLoCompleted = newVar;
			}
		});
		/* if isLoCompleted is true show next arrow, if false -> show locked*/
		$scope.isLoCompleted = false;
		$scope.$on('learningObjectCompleted', function(event, data){
			$scope.isLoCompleted = (data.status == 'completed' ? true : false);
		});
		/* if lastLo is true then hide any arrows*/
		$scope.lastLo = false;
		$scope.$on('lastLearningObjectReached', function(){
			$scope.lastLo = true;
		});
		$scope.$on('NotlastLearningObjectReached', function(){
			$scope.lastLo = false;
		});

		/* Get transaltions */
		if ( !NativeAPI.isMobile()) {
			Config.getLanguage(
			function (data) {
				Storage.set('translationStrings', data);
			}, function (error) {
				console.log(error);
			});
		}

		$scope.avatar = '';
		$scope.firstname = '';
		$scope.lastname = '';
		$scope.email = '';
		/**
		 * This check are you able to enter a learning object. If it is an asset you are able to enter with no check :)
		 * @param item
		 * @returns {boolean|*}
         */
		$rootScope.iCanEnter = function(item){
			var result;
			result = (item.can_enter || item.type_id === 'knowledge_asset' ? true : false);
			
			return result;
		};
    }
    AppController.$inject = ['$scope', '$location', '$routeParams', 'NativeAPI', '$rootScope', 'HTMLDispatcher', '$timeout',
		'$window', 'Config', 'User', 'LocalStorage', 'Course', 'TrackUser', 'Cards'];
    return AppController;
});