/* global angular */
define(function () {
	/**
	 *
	 * @param $scope
	 * @param $rootScope
	 * @param $window
	 * @param NativeAPI
	 * @param Storage
	 * @param Cards
     * @param Config
     * @constructor
     */
	function ViewAssetController($scope, $rootScope, $window, Config, $routeParams, CSAsset, HTMLDispatcher, ChannelsData, $timeout, $translate, NativeAPI) {

		/**
		 * set title in navbar
		 */

		$scope.setTitle = function () {
			if (typeof $scope.details.title !== 'undefined') {
				HTMLDispatcher.ChangeTitle($scope.details.title); // Change the title of the course
			}
		};
		var buttonLeave = 'Leave';
		var buttonStay = 'Stay here';
		$translate(buttonLeave).then(function (result) {
			buttonLeave = result;
		});
		$translate(buttonStay).then(function (result) {
			buttonStay = result;
		});
		var framework7Instance = new Framework7({
			modalButtonOk: buttonLeave,
			modalButtonCancel: buttonStay
		});

		var $$ = Dom7;

		/* Force restart assetData when */
		var assetData = false;
		var isRequestRunning = false;
		if (assetData.asset_id != $routeParams.id)
			assetData = false;

		/* This flag is used for recognize the loading status after xHR */
		$scope.dataIsLoaded = false;

		/* Assets' details */
		$scope.details = {
			last_edit_by: false,
			last_edit_datetime: false,
			published_by: false,
			published_datetime: false,
			status: false,
			avatar: false,
			title: false,
			description: false,
			owner: false,
			owner_channel_url: false,
			tags: [],
			channels: [],
			uploaded_date: false
		};

		/* All Possible actions for an asset */
		$scope.actions = [];

		/* All current Peer Reviews */
		$scope.peer_reviews = {};

		/* All questions for an asset */
		$scope.questions = [];

		/* First Chunk with similar videos */
		$scope.similar = [];
		$scope.similar_total = 0;

		/* All current tooltips */
		$scope.tooltips = [];

		$scope.rating1 = '';

		$scope.playerHtml = '';
		$scope.lastItemID = 0;

		$scope.onlySrc = true;
		/* On view change check if asset exists */
		if ($routeParams && typeof $routeParams.id !== 'undefined') {
			/* Assets details data */
			$scope.loadAsset = function () {
				var promise = CSAsset.getAssetDetails($routeParams.id, $scope.onlySrc);
				promise.then(function (response) {
					if (!response.error) {
						$scope.dataIsLoaded = true;
						assetData = response;
						//Loading data into Object
						$scope.assetId = assetData.asset_id;
						$scope.details = assetData.details;
						$scope.actions = assetData.actions;
						$scope.peer_reviews = assetData.peer_reviews;
						$scope.similar = assetData.similar;
						$scope.similar_total = assetData.similar_total;
						$scope.tooltips = assetData.tooltips;
						$scope.ratingValue1 = $scope.details.rating;
						$scope.playerHtml = assetData.playerHtml;
						$scope.lastItemID = assetData.similar.length;
						$scope.assetIMG = assetData.details.image;
						$scope.urlIMG = angular.element($scope.assetIMG).attr('src');
						$scope.disableInfinite = false;//Flag should prevent infinite scroll request
						$scope.filename = $scope.details.filename;
						$scope.downloadLink = $scope.details.download_link;
						$scope.player = assetData.player;
						$scope.setTitle();

						/* Emit event upside for listener ouside ngView */
						$scope.$emit('loaded.details', $scope.details);
					} else {
						$window.location.href = '#/all';
					}
				});
			};
		}

		/* Broadcast video url to flowplayer directive */
		$scope.$watch('details.type', function (value) {
			if (value == 1) {
				$scope.assetVideo = assetData.details.sources.mediaUrlMp4;

				$timeout(function () {
					$rootScope.$broadcast('playVideoObject', {url: $scope.assetVideo, instance: 'asset-play'});
				});
			}
		});

		/* Obtain related assets data */
		$scope.RelatedAsssets = function (updateCallback) {
			if (isRequestRunning) {
				return;
			}
			if ($scope.disableInfinite === true) {
				//updateCallback();
				return;
			}
			isRequestRunning = true;
			var promise = CSAsset.getRelatedAssets($routeParams.id, $scope.lastItemID, $scope.chunkSize);
			promise.then(function (response) {
				if (typeof response.error == 'undefined') { //if there any results
					var i = 0;
					while (response[i]) {
						$scope.similar.push(response[i]); //Push recieved items from server into existing stack
						i++;
					}
					if ($scope.similar.length === $scope.similar_total) { //If response items_count variable is equal to current items count should prevent next infinite scroll
						$scope.disableInfinite = true;
					}
					$scope.lastItemID = $scope.similar.length;
					if (!$scope.$$phase) { //If scope not currently in loop trigger it
						$scope.$digest();
					}
					if (typeof updateCallback === 'function') {
						updateCallback();//Call the passed callback
					}
					$scope.$broadcast('infiniteScroll:vertical');
					isRequestRunning = false;
				} else { //Otherwsie call the passed callback and disable future infintie scroll
					if (typeof updateCallback === 'function') {
						updateCallback();//Call the passed callback
					}
					$scope.disableInfinite = true;
					isRequestRunning = false;
				}
			});
		};

		/* Reveal modal panel framework7 */
		$scope.$openModal = function (asset_id) {
			if (typeof asset_id !== 'undefined') {
				$scope.$broadcast('ModalOpened', asset_id);
				$scope.$broadcast('loadInvitations');
			}
		};

		/* Invite to watch dialog */
		$scope.modalPartialView = 'views/dialogs/invite_to_watch.html';
		$scope.hideHeader = true; //Hide the header in modal directive and use it instead in the partial view

		/* IShow alert when user tries to close modal window (invitations) */
		$scope.$openAlert = function (countSelected) {
			if (countSelected < 1){return;}
			angular.element('body').addClass('invitation-page');
			$translate(["You haven't invited the selected people yet. Are you sure you want to leave this page?", 'Warning!']).then(function(translations){
				framework7Instance.prompt(translations["You haven't invited the selected people yet. Are you sure you want to leave this page?"], translations['Warning!'],
					function () {
						angular.element('.close-popup').trigger('click');
						$scope.$broadcast('ModalClosed');
						angular.element('body').removeClass('invitation-page');
						angular.element('.modal-overlay').removeClass('modal-overlay-visible');
						angular.element('#selection .person').hide();
					},
					function () {
						//Do nothing, close alert
						angular.element('body').removeClass('invitation-page');
					}
				);
			});
		};
		
		 /**
        * Function to download a file
        * @param downloadPath ( URL of the file )
        */
       $scope.downloadFile = function(downloadPath) {
		   NativeAPI.dispatch('FileLoAction', downloadPath);
       };
	   /*$scope.$on('$destroy', function(){
		   ChannelsData.clear();
	   });*/
	}
	ViewAssetController.$inject = ['$scope', '$rootScope', '$window', 'Config', '$routeParams', 'CSAsset', 'HTMLDispatcher', 'ChannelsData', '$timeout', '$translate', 'NativeAPI'];//Injecting dependencies
	return ViewAssetController;//Return Class definition
});
