/* global angular */
define(function () {
	/**
	 *
     * @param $scope
	 * @param $rootScope
	 * @param $translate
	 * @param CSAsset
	 *
	*/
	function PublishPageController($scope, $rootScope, $translate, CSAsset){
		$scope.view = 'views/publishPage.html';

		$scope.headertitle = '';
		$scope.assetType='';
		$translate(['Add details', 'Image']).then(function (result) {
			$scope.headertitle  = result['Add details'];
			$scope.assetType 	= result['Image'];
		});
		$scope.videoUrl = "";
		$scope.isEnterPressed = false;
		$scope.visibilities = [];
		/**
		 * Translations for public/private visibilities
		 */
		$translate(['You\'ll have to select a channel where you\'d like to publish your asset', 'Public', 'This asset will be visible only for me and to people I decide to invite', 'Private']).then(function(translations){
			$rootScope.$watch('appSettings', function(settings){
				if(typeof settings != 'undefined' && settings != null){
					if(settings.configuration.privateAssetsAllowed == 1){
						$scope.visibilities.push({ id: 1, icon: 'icon-d_lock', title: translations['Private'], description: translations['This asset will be visible only for me and to people I decide to invite'], type: '1'});
					}
				}
			});
			$scope.visibilities.push({ id: 2, icon: 'icon-d_world', title: translations['Public'], description: translations['This asset will be visible only for me and to people I decide to invite'], type: '0'});
		});

		/* Define the Empty form fields */
		$scope.publishForm = {};
		$scope.publishForm.title = '';
		$scope.publishForm.description = '';
		$scope.publishForm.visibility = 0;
		$scope.publishForm.tags = [];
		$scope.publishForm.channels = [];

		/**
		 * disable enter key publish button
		 * @param e - get current key event
         */

		$scope.enterPressed = function(e){
			if(e.keyCode === 13){
				$scope.isEnterPressed = true;
			} else {
				$scope.isEnterPressed  = false;
			}
		};

		/**
		 * Clear the publishForm object when open PublishPage or submit the form
		 */
		$scope.clearForm = function(){
			$scope.publishForm.title = '';
			$scope.publishForm.description = '';
			$scope.publishForm.visibility = 0;
			$scope.publishForm.tags = [];
			$scope.publishForm.channels = [];
		};
		/**
		 * Check is there a video file to play for the flowplayer
		 * @returns {boolean}
         */
		$scope.isVideoFileUploaded = function(){
			var result = ($scope.uploadFileUrl ? true : false);

			return result;
		};

		$scope.panelOpened = false;
		/* Listen */
		$scope.$on('RightPanelOpened', function(event, uploadedItem) {
			$scope.panelOpened  = true;
			$scope.uploadedItem = uploadedItem;
			$scope.idAsset 		= uploadedItem.idAsset;
			$scope.type 		= uploadedItem.type;
			$scope.thumb   		= uploadedItem.preview;
			$scope.uploadFileUrl= uploadedItem.uploadFileUrl;
			/* Raise event for playing video */
			$rootScope.$broadcast('playVideoObject', {url: $scope.uploadFileUrl, instance: 'publish-asset'});

			$scope.publishForm = {};
			$scope.clearForm();
			/**
			 * Trigger the click to make the public by default.
			 */
			var publicInput = angular.element("#radio_2");
			publicInput.trigger('click');
			
			//Access navbar color from rootScope
			$scope.navColor = $rootScope.navColor;
			$scope.navFont = $rootScope.navFont;
		});

		var bodyElem = angular.element('body');
		$scope.closePanels = function(){
			if(bodyElem.hasClass('with-panel-right-cover')){
				bodyElem.removeClass('with-panel-right-cover');
			}
		};

		$scope.$watch('publishForm', function(nv, ov){
			$scope.publishForm.visibility = nv.visibility;
		}, true);

		/* Get visiblity radio button value */
		$scope.changeVisibility = $scope.publishForm.visibility;
		$scope.getVal = function(){
			$scope.changeVisibility = $scope.publishForm.visibility;
			//check visibility of channels
			if($scope.changeVisibility == 0) {
				$(document).ready(function() {
					$('.channels-menu').show();
				});
			} else {
				$(document).ready(function() {
					$('.channels-menu').hide();
				});
			}
		};
		/* Submit publish form */
		$scope.submitForm = function() {
			if($scope.isEnterPressed == false) {

				if ($scope.publishForm.title && $scope.publishForm.description && ($scope.publishForm.visibility == 1 || $scope.publishForm.visibility == 0 && $scope.publishForm.channels.length != 0)) {

					var tags = $scope.publishForm.tags.join(",");
					/* Make the API call */
					var promise = CSAsset.updateAsset($scope.idAsset, $scope.publishForm.title, $scope.publishForm.description, $scope.publishForm.visibility, $scope.publishForm.channels, tags);
					/* Raise event for published asset */
					promise.then(function () {
						$rootScope.$broadcast('assetIsPublished', {
							idAsset: $scope.idAsset,
							publishData: $scope.publishForm
						});
						/* Close the Right Panel */
						$scope.closePanels();
						/* empty the data holder*/
						dataObject = {};
						$scope.clearForm();
					});
				} else {
					angular.element("#publish-page-warning-modal").trigger('click');
				}
			}
			$scope.isEnterPressed = false;
		};

		$scope.suggestionTags = [];
		$rootScope.$watch('tagsList', function(newVal, oldVal){
			$scope.suggestionTags = newVal;
		});

		$translate(['Warning!', 'To publish your video you must add the mandatory details: title, description, visibility (and channels, if set to public)', 'OK, GOT IT!']).then(function(translations){
			$scope.popupActions = {
				title: 		translations['Warning!'],
				message:    translations['To publish your video you must add the mandatory details: title, description, visibility (and channels, if set to public)'],
				buttonText: translations['OK, GOT IT!']
			};
		});
    }
	PublishPageController.$inject = ['$scope', '$rootScope', '$translate', 'CSAsset'];//Injecting dependencies
	return PublishPageController;//Return Class definition
});