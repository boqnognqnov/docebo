/* global angular */
'use strict';
define(function () {
	/**
	 *
	 * @param {Service} $scope
	 * @param {Service} $rootScope
     * @param {Service} NativeAPI
	 * @param {Service} $translate
	 * @param {Service} Storage
	 * @param {Service} Cards
	 * @param {Service} AmazonS3
	 * @param {Service} Queue
	 * @param {Service} QueueItem
	 * @param {Service} $timeout
	 * @param {Service} DoceboFile
	 * @param {Service} CSAsset
	 * @param {Service} $interval
	 * @param {Promise} tags - list of 7020 tags
	 * @param {Service} UploadHistory
	 * @returns {Class}
	 */
	function UploadQueueController($scope, $rootScope,  HTMLDispatcher, $translate, Storage, Cards, AmazonS3, Queue, QueueItem, $timeout, DoceboFile, CSAsset, $interval, tags, UploadHistory){
		$rootScope.panelName = 'PublishPageController';
		var translateTitle = 'Uploading Queue';
		$translate(translateTitle).then(function (result) {
			translateTitle = result;
			HTMLDispatcher.ChangeTitle(translateTitle);
		});
		$scope.$emit('disableLoading');
		$rootScope.$on("logout", function(){
			Queue.clearQueue();
			$scope.queue = [];
			$rootScope.isDeleteItem = false;
		});
		$scope.$on('$destroy', function(){
			$scope.$emit('enableLoading');
		});
		/* Get Tags from API call from tags service*/
		$rootScope.tagsList = tags.tags;
		$scope.queue = Queue.getQueue();			
		$scope.requestCounter = 0;
		var REQUEST_CONVERSION_LIMIT = 60;
		$scope.init = function(){
			if($scope.queue.length === 0){
				var promise = CSAsset.getUnpublished();
				promise.then(function(unpublished){
					angular.forEach(unpublished.assets, function(obj, index){
						var item = QueueItem.create();
						if(obj.status == "6"){
							item.preview = obj.thumbnail;
						}else{
							item.preview = Storage.get('preview_'+obj.idAsset);
						}
						if(typeof item.preview == 'undefined' || item.preview == '' || item.preview == null){
							item.preview = false;
						}
						if(typeof obj.videoUrl.error != 'undefined'){
							item.type = item.TYPES.Image
						}else{
							item.type = item.TYPES.Video;
						}
						if(typeof obj.videoUrl != 'undefined'){
							item.uploadFileUrl = obj.videoUrl['mediaUrlMp4'];
						}else{
							item.uploadFileUrl = '';
						}
						item.idAsset = obj.idAsset;
						item.state = obj.status == "6" ? item.STATES.Converted : item.STATES.Complete;
						item.progress = 100;
						item.icon = 'icon-ellipsis_v';
						item.actions = $scope.getActions(item);
						Queue.push(item);
					});
				});
				if ($scope.checkInterval === null) {
					$scope.checkInterval = $interval(function () {
						$scope.checkConversionStatus();
					}, 5000);
				}
			}
		};
		$scope.clickedItem = '';
		$scope.checkInterval = null;
		$scope.assetIds = [];
		$scope.STATES = null;
		$scope.pushInQueue = function(object){
			Queue.push(object);
			if(Queue.getQueue().length > 8){
				$scope.$broadcast('scrollTableViewBottom');
			}
			$scope.$digest();
		};
		/* 
		 * Creates new QueueItem Object with the passed file from the input field and sends it to the Queue for uploading
		 * @return {Bool}
		 */
		$scope.fileUpload = function(file){
			var type = DoceboFile.getFileType(file.type);
			if(type !== "image" && type !== "video"){
				return;
			}
			var item = QueueItem.create();
			$scope.STATES = item.STATES;
			item.state = item.STATES.Waiting;
			item.file = file;
			item.originalFilename = file.name;
			item.name = AmazonS3.generateAssetName(file);
			item.icon = 'icon-ellipsis_v';
			item.actions = $scope.getActions(item);
			if(type === "image"){
				item.type = item.TYPES.Image;
			}else{
				item.type = item.TYPES.Video;
			}
			var reader = new FileReader();
			reader.onload = function(event) {
				if(item.type === item.TYPES.Image){
					item.preview = event.target.result;
				}
				$scope.pushInQueue(item);
			};
			// when the file is read it triggers the onload event above.
			var ftype = DoceboFile.getFileType(file.type);
			if(item.type === item.TYPES.Image){
				reader.readAsDataURL(file);
			}else{
				reader.readAsArrayBuffer(file);
			}
			
			/* Show swipe card if it is a first-time upload */
			$timeout(function(){
				$('.last').removeClass('last').addClass('next');
				$('.next').removeClass('next').addClass('active');
				var card = $('.walkthrough-page');
				var textCardImage = $translate.instant('Your videos are uploading...');
					$('.cardimage-wrapper').append('<div class="video-upload-text">'+textCardImage+'</div>');
				if (Storage.get('isFirstUpload') === null) {
					$("body.home-page").addClass('with-panel-right-cover black-overlay');
					$(".panel-overlay").addClass('no-menu-revealed');
					card.detach().addClass('modal-card');
					$("body").append(card);
				}
			});
		};
		
		$scope.process = function() {
			if (!Queue.isProccessing()) {
				var nextFromQueue = Queue.pullNextItem();
				if (typeof nextFromQueue !== 'undefined' && nextFromQueue) {
					Queue.setToBusy();
					var promise = AmazonS3.upload(nextFromQueue.file, nextFromQueue.name);
				}
			} else {
				$timeout(function () {
					$scope.process();
				}, 2000);
				return;
			}
		};
		
		$scope.checkConversionStatus = function(){
			$scope.assetIds = [];
			if($scope.requestCounter == REQUEST_CONVERSION_LIMIT){
				$interval.cancel($scope.checkInterval);
				$scope.requestCounter = 0;
				return;
			}
			var forConversion = $scope.queue.filter(function(obj){
				if($scope.STATES == null){
					$scope.STATES = obj.STATES;	
				}
				if(obj.state === obj.STATES.Complete){
					if($scope.assetIds.indexOf(obj.idAsset) === -1){
						$scope.assetIds.push(obj.idAsset);
						return true;
					}
				}
			});
			if($scope.assetIds.length > 0){
				var promise = CSAsset.checkConversionStatus($scope.assetIds);
				$scope.requestCounter++;
				promise.then(function(data){
					var positives = data.positives;
					var negatives = data.negatives;
					var procceedConverted = function(assetIds, state){
						for(var index in assetIds){
							var id = assetIds[index];
							var removeIndex = $scope.assetIds.indexOf(id);
							$scope.assetIds.splice(removeIndex, 1);
							var item = Queue.getItemByCriteria('idAsset', id);
							item.state = state;
							if(item.state === item.STATES.Converted){
								item.actions = $scope.getActions(item);
								CSAsset.getThumb(item.idAsset).then(function(response){
									item.preview = response.thumb;
								});
								CSAsset.getVideoUrl(item.idAsset).then(function(response){
									item.uploadFileUrl = response.mediaUrlMp4;
								});
							}
						}
					};
					if(typeof positives != 'undefined' && positives.length > 0){
						procceedConverted(positives, $scope.STATES.Converted);
					}
					if(typeof negatives != 'undefined' && negatives.length > 0){
						procceedConverted(negatives, $scope.STATES.Failed);
					}
				});
			} else{
				$interval.cancel($scope.checkInterval);
				$scope.checkInterval = null;
			}
		};
		
		$scope.removeFromQueue = function(index){
			Queue.remove(index);
		};
		
		$scope.$watchCollection("queue", function(nv, ov){
			if(nv !== ov){
				$scope.process();
			}
		});
		
		$scope.$on('AmazonUploadProgress', function(event, args){
			var item = Queue.getItemByCriteria('file', args.file);
			var progress = args.progress;
			var fileProgress = 100-(((progress.total - progress.loaded) / progress.total) * 100);
			item.progress = parseInt(fileProgress);
			if(fileProgress === 100) {
				$timeout(function () {
					item.state = item.STATES.Complete;
					Queue.setFreeOfProcessing();
					var promise = CSAsset.newAsset(item.type, item.name, item.originalFilename);
					promise.then(function (response) {
						item.idAsset = response.idAsset;
						item.state = item.STATES.Complete;
						/* temp fix */
						// Storage.set('preview_'+item.idAsset, item.preview);
						//if user cancel the asset, when http request is pending will cancel it
						if ($rootScope.isDeleteItem) {
							$rootScope.isDeleteItem = false;
							CSAsset.deleteAsset(item.idAsset);
						}
						if ($scope.checkInterval === null) {
							$scope.checkInterval = $interval(function () {
								$scope.checkConversionStatus();
							}, 5000);
						}
					});
				}, 2000);
			}
		});
		
		$scope.$on('AmazonUploadCancel', function(event, args){
			var current = Queue.getCurrentProcessedIndex();
			Queue.remove(current);
		});
		
		$scope.$on('assetIsPublished', function(event, args){
			var item = Queue.getItemByCriteria('idAsset', args.idAsset);
			UploadHistory.push({idAsset: item.idAsset, thumbnail: item.preview, title: args.publishData.title});
			Queue.removeItem(item);
 		});

		/* 
		 * Returns whether user enters for first time in upload queue
		 * @return {Bool}
		 */
		$scope.isFirstTime = function(){
			var isFirstTime = Storage.get('isFirstUpload');
			if(isFirstTime !== null){
				return false;
			}
			return true;
		};
		
		$scope.$tabs = [];
		//Tabs
		$translate(['Uploading Queue', 'Uploaded Items']).then(function(translations){
			$scope.$tabs = [
				{title: translations['Uploading Queue'], action: '#/uploadQueue', icon: 'icon-list'},
				{title: translations['Uploaded Items'], action: '#/uploadHistory', icon: 'icon-completed_outline'}
			];
		});
		$rootScope.getTabs = function(){
			return $scope.$tabs;
		};
		/**
		 * Return array of cards for the Upload Queue intro for the directive
		 * @returns {*}
         */
		var cards = Cards.getCardsByType('first_video');
		$scope.getCards = function() {
			return cards;
		};
		$scope.cardsClick = function(){
			if ( $(".tinderslide li.swipe-card.active")) {
				Storage.set('isFirstUpload', 1); // Do not show after click on the button
				$("body.home-page").removeClass('with-panel-right-cover');
			}
		};

		$rootScope.$on('swipeCards:next', function(event, args){
			if ( $(".tinderslide li.swipe-card.active")) {
				Storage.set('isFirstUpload', 1); // Do not show after click on the button
				var card = angular.element('[data-pane='+args+']');
				card.hide();
				$("body.home-page").removeClass('with-panel-right-cover');
			}
		});
		$translate(['Add details and publish', 'Cancel upload', 'Remove file']).then(function(translations){
			$scope.cancel = translations['Cancel upload'];
			$scope.addDetails = translations['Add details and publish'];
			$scope.removeFileText = translations['Remove file'];
		});
		
		angular.element(document).on('click', '.item-after', function(){
			var elemId = this.id;
			var parts = elemId.split('_');
			var queueIndex = parts.pop();
			$scope.clickedItem = queueIndex;
		});
		
		var actionSheetPublish = function(){
			var item = Queue.getItemByIndex($scope.clickedItem);
			if ( item.idAsset > 0){
				$scope.$openRightPanel(item);
			}
		};
		
		var actionSheetCancel = function(){
			var clickedIndex = $scope.clickedItem;
			var proccessedIndex =  Queue.getCurrentProcessedIndex();

			if(parseInt(clickedIndex) === parseInt(proccessedIndex)){
				$scope.removeFromQueue(clickedIndex);
				Queue.setFreeOfProcessing();
				return AmazonS3.cancel();
			}else{
				var item = Queue.getItemByIndex(clickedIndex);
				if(item.idAsset > 0){
					CSAsset.deleteAsset(item.idAsset);
					$scope.removeFromQueue(clickedIndex);
				} else {
					$rootScope.isDeleteItem = true;
					$scope.removeFromQueue(clickedIndex);
				}
			}
		};
	
		/**
		 * Decide actions for item according to his status
		 * @param item {Object}
         */
		$scope.getActions = function(item){
			var actions = [];
			if(typeof item !== 'undefined'){
				if(item.state === item.STATES.Converted){
					actions.push({ text: $scope.addDetails, color: 'green', onClick: actionSheetPublish});
				}
				var actionText;
				if(item.state === item.STATES.Converted){
					actionText = $scope.removeFileText;
				}else{
					actionText = $scope.cancel;
				}
				actions.push({ 
					text: actionText, 
					color: 'red', 
					onClick: actionSheetCancel
				});
			}
			return actions;
		};

	};
	UploadQueueController.$inject = ['$scope', '$rootScope', 'HTMLDispatcher', '$translate', 'LocalStorage', 'Cards', 'AmazonS3', 'Queue', 'QueueItem', '$timeout', 'DoceboFile', 'CSAsset', '$interval', 'tags', 'UploadHistory'];//Injecting dependencies
	return UploadQueueController;//Return Class definition
});
