/**
 * Created by vladimir on 14.1.2016 г..
 */
'use strict';
define([], function() {
    function LoginController ( $scope, $window, $rootScope, $translate, Request, TokenStorage, User, Storage, $timeout, ConnectAPI, NativeAPI, URLManager) {
		/* Placeholders color based on the calculated font color */
		var $$ = Dom7;
		$scope.showError = function(){
			document.getElementById('main-warning').style.visibility = "visible";
			$(".close-warning").click(function(){
				document.getElementById('main-warning').style.visibility = "hidden";
			});
		};
		$scope.errorMessage = '';
		$scope.connectFailed = false;
		ConnectAPI.connect(function(data){
			if(window.location.protocol === "http:" && data.configuration.https_redirect == true){
				var restOfUrl = window.location.href.substr(5);
				window.location = "https:" + restOfUrl;
				return;
			}
			var oauthResponse = Request.getParam('oauth2_response');
			if(oauthResponse !== false && oauthResponse !== ''){
				var res = TokenStorage.parseTokenFromUrl(oauthResponse);
				if(res !== false){
					User.getUserInfo(function(data){
						User.onUserInfoRetrieved(data);
					});
				}
			}
			$rootScope.$watch('appSettings', function(value){
				if(typeof value !== 'undefined' && value !== null){
					if(value.configuration.bg_color.bg_font.hex == '#ffffff'){
						$$('input').addClass('custom-white');
					}else{
						$$('input').addClass('custom-black');
					}
				}
			});

			var rememberMe = Storage.get('remember');
			if( rememberMe !== null && rememberMe === true){
				if(TokenStorage.isExpired()){
					var refreshToken = Storage.get('tokens').refresh_token;
					TokenStorage.refreshToken(refreshToken, function( data ) {
						var tokenObj = data;
						tokenObj['expire_at'] = new Date().getTime() + (tokenObj.expires_in*1000);
						Storage.set('tokens', tokenObj);
						User.onUserInfoRetrieved(data);
					});
				}else{
					$window.location.href = '#/all';
				}
			} else {
				if(NativeAPI.isMobile() || !TokenStorage.isExpired()) {
					User.getUserInfo(function(data){
						User.onUserInfoRetrieved(data);
					});
				}
			}
		});
		$rootScope.entryPoint = false;
		$scope.loginFormModel = {
			username: '',
			password: '',
			rememberMe: ''
		};
		$scope.submitForm = function(){
			if($scope.connectFailed){
				return;
			}
			TokenStorage.getToken($scope.loginFormModel.username, $scope.loginFormModel.password,function(data){
				TokenStorage.setAccessToken(data.access_token);
				var tokenObj = data;
				tokenObj['expire_at'] = new Date().getTime() + (tokenObj.expires_in*1000);
				Storage.set('tokens', tokenObj);
				User.getUserInfo(function(data){
					Storage.set('remember', $scope.loginFormModel.rememberMe);
					User.onUserInfoRetrieved(data);
				});
			},
			function(error){
				$translate(['Invalid username and/or password']).then(function (result) {
					$scope.errorMessage  = 'Invalid username and/or password';
				});
				$scope.showError();
			});
		
		};
		$scope.linkRedirect = function(link){
			if($scope.connectFailed){
				return false;
			}
			$window.open(URLManager.createAbsoluteUrl(link), '_self');
		};
		$scope.$on("error:Connect2Server", function(){
			$scope.connectFailed = true;
			$translate(['Cannot connect to lms server']).then(function (result) {
				$scope.errorMessage  = 'Cannot connect to lms server';
			});
			$scope.showError();
			$$('#forgot-password').on('click', function(e){
				e.preventDefault();
			});
		});
		$scope.$watch('loginFormModel.rememberMe', function(nv){
			console.log(nv);
		});
		angular.element('.label-switch').on('click', function(e){
			var checkbox = angular.element(this)[0].children[0].checked;
			if(!checkbox){
				$scope.loginFormModel.rememberMe = true;
			}else{
				$scope.loginFormModel.rememberMe = false;
			}
		});
	}
    LoginController.$inject = [ '$scope', '$window', '$rootScope', '$translate', 'Request', 'TokenStorage', 'User', 'LocalStorage', '$timeout', 'ConnectAPI', 'NativeAPI', 'URLManager'];
    return LoginController;
});