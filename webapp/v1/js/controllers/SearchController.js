/* global angular */
define(function () {
	function SearchController($scope, $rootScope, $translate, $timeout, HTMLDispatcher, GlobalSearch,routeParams, Utils, NativeAPI, Course, Storage, $window) {
		var searchKeyword = routeParams.result;
		var CURRENT = 0;
		var PER_PAGE = 10;
		var type;
		$rootScope.hasTabs = false;
		$rootScope.panelName = 'SearchFiltersController';
		$scope.filter = [];
		$scope.items = [];
		$scope._counter = 0;
		$scope.showLoadingPlaceholder = false;
		$scope.preventLoadMore = false;
		$scope.isRequestRunning = false;
		NativeAPI.dispatch('isSearchFiltersLoaded', "false");
		$scope._search = function () {
			return GlobalSearch.elasticSearch(searchKeyword, CURRENT, (CURRENT + PER_PAGE), type);
		};
		$scope._incrementCurrent = function (added) {
			if (typeof added == 'undefined') {
				CURRENT = CURRENT + PER_PAGE;
			} else {
				CURRENT = added;
			}
		};
		$scope.updateCounter = function (number) {
			$scope._counter = number;
		};
		$translate(['E-Learning', 'Learning Plan', 'Knowledge Asset', 'Learning Object', 'You cannot enter this object', 'Load more results']).then(function (result) {
			$scope.translations = result;
			$scope.types = {
				'EsAgentCourse': $scope.translations['E-Learning'],
				'EsAgentKnowledgeAsset': $scope.translations['Knowledge Asset'],
				'EsAgentLo': $scope.translations['Learning Object'],
				'EsAgentPlan': $scope.translations['Learning Plan']
			};
		});

		$scope.checkOS = function () {
			if (NativeAPI.device() == NativeAPI.OS.iOS) {
				return true;
			}
			return false;
		};

		$scope.initFilter = function () {
			NativeAPI.dispatch('isSearchFiltersLoaded', "true");
		};

		$scope._search().then(function (result) {
			HTMLDispatcher.ChangeTitle(searchKeyword);
			if (result.success) {
				result.items.map(function (object) {
					$scope.items.push(object);
				});
				$scope.updateCounter($scope.items.length);
				$scope._incrementCurrent(result.from);
				$scope.locked_message = $scope.translations["You cannot enter this object"];
				$scope.loadResultsMsg = $scope.translations["Load more results"];
				$timeout(function () {
					angular.element('.with-tooltip').tooltip({trigger: "click"});
				}, 100);
			}
		});

		$scope.$on("filterTypeIds", function (event, data) {
			CURRENT = 0;
			$scope.items = [];
			$scope.filter = data;
			$scope.setType(data);
			$scope._search().then(function (result) {
				if (result.success) {
					result.items.map(function (object) {
						$scope.items.push(object);
					});
					$scope.updateCounter($scope.items.length);
					$scope._incrementCurrent(result.from);
				}
			});
		});

		$scope.setType = function (data) {
			if(data.length == 4) {
				type = "all";
			} else {
				data.filter(function (item) {
					type = item;
				});
			}
		};
		
		$scope.courseMaterialLink = function (item) {
			return '#/course/' + item.other_fields.course_id;
		};

		$scope.allMaterialLink = function (item) {
			switch (item.type) {
				case GlobalSearch.ENUM_TYPES.EsAgentCourse:
					$window.location.href = '#/course/' + item.id;
					break;
				case GlobalSearch.ENUM_TYPES.EsAgentKnowledgeAsset:
					$window.location.href = '#/assets/view/' + item.id;
					break;
				case GlobalSearch.ENUM_TYPES.EsAgentLo:
					$window.location.href = '#/course/' + item.other_fields.course_id + '/learning_object/' + item.id;
					break;
				case GlobalSearch.ENUM_TYPES.EsAgentPlan:
					$window.location.href = '#/learning_plan/' + item.id;
					break;
			}
		};

		/**
		 * onClick load more search results
		 */

		$scope.loadMoreResults = function () {
				$scope.showLoadingPlaceholder = true;
				$('html, body').animate({
					scrollTop: $('.infinite-placeholder').offset().top + 'px'
				}, 'fast');
				if (!$scope.isRequestRunning) {
					$scope.isRequestRunning = true;
					$scope._search().then(function (result) {
						if (result.success) {
							result.items.map(function (object) {
								$scope.items.push(object);
							});
							$scope.updateCounter($scope.items.length);
							$scope._incrementCurrent(result.from);

							$timeout(function () {
								angular.element('.with-tooltip').tooltip({trigger: "click"});
							});
						}
						$scope.showLoadingPlaceholder = false;
						$scope.isRequestRunning = false;
					});
				}
		};


		/**
		 * Get scormId from API loSearchInfo for resolving redirect to CourseDetail
		 * @param courseId
		 * @param itemId
		 * @param item
		 */

		$scope.loSearchInfo = function (item, courseId, itemId) {
			Course.getLoInfo(courseId,
				function (data) {
					Storage.set('loInfo', data.items);
					for (var i = 0; i < data.items.length; i++) {
						if (data.items[i].idOrganization == itemId) {
							if (data.items[i].type == 'sco' && data.items[i].idScormItem != '') {
								$rootScope.scormId = data.items[i].idScormItem;
								break;
							}
						}
					}
					$scope.allMaterialLink(item);

				}, function (error) {
					console.log("ERROR: ");
					console.log(error);
				});
		};
		document.addEventListener('touchstart', function (e) {
			$('.tooltip').remove();
		});
	}
	SearchController.$inject = ['$scope', '$rootScope', '$translate', '$timeout', 'HTMLDispatcher', 'GlobalSearch', '$routeParams', 'Utils', 'NativeAPI', 'Course', 'LocalStorage', '$window'];//Injecting dependencies
	return SearchController;//Return Class definition
});