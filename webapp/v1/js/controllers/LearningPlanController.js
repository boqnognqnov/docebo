/**
 * Created by mitko on 29.2.2016 г..
 */
'use strict';
define(function () {
    function LearningPlanController($scope, $routeParams, $location, $translate, Course, HTMLDispatcher) {

        var $$ = Dom7;
   //     $scope.courseObject = {};

        if (typeof $routeParams.learning_plan_id !== 'undefined'){

        //    $scope.courseObject = Course.getCourseById($routeParams.learning_plan_id);
            Course.getLearningPlan($routeParams.learning_plan_id,
                function(data){
                    $scope.learningPlanCourses = data.learningPlanCourses;
                    $scope.learningPlan = data.learningPlan;
                   // console.log($scope.learningPlanCourses);
                    //set current progress of the user in percentage
                    renderProgress($scope.learningPlan.percentage.percentage);
                    $scope.setTitle();
                }, function(error){
                    if ( error.statusText == 'Unauthorized') {
                        $location.path('/login');
                    }
                    console.log("ERROR: ");
                    console.log(error);
                });
        }

        /**
         * set title in navbar
         */

        $scope.setTitle = function() {
            if ( typeof $scope.learningPlan.path_name !== 'undefined') {
                HTMLDispatcher.ChangeTitle($scope.learningPlan.path_name); // Change the title of the course
            }
        };

        /**
         *
         */
        $scope.expand = function() {
            var growDiv = document.getElementById('course-description');

            if ( growDiv.clientHeight > 40 ) {
                growDiv.style.height = '40px';
            } else {
                var wrapper = document.getElementById('mobile-wrapper');
                growDiv.style.height = wrapper.clientHeight + "px";
            }
            document.getElementById("more-arrow").className = document.getElementById("more-arrow").className == 'icon-angle-down' ? 'icon-angle-up' : 'icon-angle-down';
        };


        /**
         * listen for user progress,
         * circle progress animation
         */

        function renderProgress(progress)
        {
            progress = Math.floor(progress);
            if(progress<25){
                var angle = -90 + (progress/100)*360;
                $(".animate-0-25-b").css("transform","rotate("+angle+"deg)");
            }
            else if(progress>=25 && progress<50){
                var angle = -90 + ((progress-25)/100)*360;
                $(".animate-0-25-b").css("transform","rotate(0deg)");
                $(".animate-25-50-b").css("transform","rotate("+angle+"deg)");
            }
            else if(progress>=50 && progress<75){
                var angle = -90 + ((progress-50)/100)*360;
                $(".animate-25-50-b, .animate-0-25-b").css("transform","rotate(0deg)");
                $(".animate-50-75-b").css("transform","rotate("+angle+"deg)");
            }
            else if(progress>=75 && progress<=100){
                var angle = -90 + ((progress-75)/100)*360;
                $(".animate-50-75-b, .animate-25-50-b, .animate-0-25-b")
                    .css("transform","rotate(0deg)");
                $(".animate-75-100-b").css("transform","rotate("+angle+"deg)");
            }

            $(".text").html(progress);
            $(".percentage-single").html("%");
            $(".percentage-multi").html("%");
        }
    }
    LearningPlanController.$inject = ['$scope', '$routeParams', '$location', '$translate', 'Course', 'HTMLDispatcher'];
    return LearningPlanController;
});