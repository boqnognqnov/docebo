/**
 * Created by kyuchukovv_lenovo on 3/9/2016.
 */
'use strict';
define(function () {
   function LearningObjectController($scope, $routeParams, $rootScope, $window, $timeout, Course, HTMLDispatcher, NativeAPI, ChannelsData, $sce, DeviceOrientation, $http) {

       var iPhone    = (NativeAPI.device() == NativeAPI.OS.iOS);
       var Android   = (NativeAPI.device() === NativeAPI.OS.Android);
       var Desktop   = (NativeAPI.device() == NativeAPI.OS.Desktop);
       var userAgent = window.navigator.userAgent;
       /**
        * Pass an external URL and validate it for angular to enable XSS problem
        * @param src
        * @returns {*} Clean Url string
        */
       $scope.trustSrc = function(src) {
           return $sce.trustAsResourceUrl(src);
       };

       if ( typeof $routeParams == 'object' ) {
           $scope.idOrg    = $routeParams.learningObjectId;
           $scope.idCourse = $routeParams.courseId;
           $scope.idScorm  = $rootScope.scormId;
       }
       $scope.currentLoStatus = '';
       $scope.$watch('currentLoStatus', function(newVar, oldVar) {
           if ( typeof newVar != 'undefined' && newVar != '' ){
               $rootScope.$emit('learningObjectCompleted', {status : newVar});
           }
       });

       /**
        * Call API to get the info for current Learning Object
        */
       Course.getLearningObject($scope.idOrg, $scope.idScorm,
           function(data){
               if ( data.success == true && !data.err ) {
                   $scope.learningObject = data;
                   $scope.learningObject.url = ($scope.learningObject.loModel.objectType !== 'file') ? $sce.trustAsResourceUrl($scope.learningObject.url) : $scope.learningObject.url;
                   $rootScope.nextLearningObjectId = data.next.idOrganization;
                   $rootScope.currentLearningObjectId = data.loModel.idOrg;
                   $rootScope.loStatus = data.next.status;
                   $rootScope.loLocked = data.next.locked;
                   $rootScope.loPrerequisites = data.next.prerequisites;
                   $scope.currentLoStatus = data.completed;
                   $scope.$emit('learningObjectCompleted', {status : data.completed});
                   HTMLDispatcher.ChangeTitle(data.loModel.title);
				   checkMode();
                   if ( $scope.learningObject.loModel.objectType == 'video' )
                   {
                   $scope.videoType = $scope.learningObject.type;
                    switch ($scope.learningObject.type){
                        case 'youtube':
                        case 'vimeo' :
                            $scope.videoId = JSON.parse($scope.learningObject.video_formats).id;
                            break;
                        case 'wistia' :
                            $scope.videoId = JSON.parse($scope.learningObject.video_formats).id.hashedId;
                            break;
                        case 'upload' :
                            $rootScope.$broadcast('playVideoObject', {url: $scope.learningObject.url, instance: 'lo'});
                            break;
                    } // end switch
                   }
                   /* Get the clean Url(stringify) and look is it a google drawing link  */
                   var cleanUrl = ( data.url ? data.url.toString() : '');
                   var match = (cleanUrl.length > 0 ? cleanUrl.match(/[/]drawings[/]{1}/g) : null );

                   $timeout(function(){
                       /* On iOS open google drawings in safari browser */
                       if ( $scope.learningObject.loModel.objectType === 'googledrive' && match != null && match.length > 0 ){
                           if ( iPhone ){
                               /* Load the course before to be ready when you come back */
                               $window.location.href = '#/course/'+$scope.idCourse;
                               /* Actual redirecting to Safari new tab */
                               $scope.openInNewTab(cleanUrl);
                           }else if (Desktop && userAgent.match(/iPad/i) || userAgent.match(/iPhone/i)){
                               $window.location.href = '#/course/'+$routeParams.courseId;
                               $timeout(function () {
                                   $window.location.href = cleanUrl;
                               },1300);
                               return;
                           }

                        }

                       Course.trackUserLoStatus($scope.idOrg, $scope.learningObject.loModel.objectType,
                           function(data){
                               if ( data.success && data.status ){
                                   $scope.currentLoStatus = data.status;
                                   $scope.$emit('learningObjectCompleted', {status : data.status});
                               }
                           }, function(error){
                               console.log(error);
                           });
                   });

                   if ( data.next === false ){
                       $scope.$emit('lastLearningObjectReached');
                   } else {
                       $scope.$emit('NotlastLearningObjectReached');
                   }
				   var objectType = $scope.learningObject.loModel.objectType;
					if(objectType == "test" || objectType == "htmlpage" || objectType == "tincan"){
						$scope.scrollFixTest();
					}
					if($scope.learningObject.type == 'upload'){
						if (window.matchMedia("(orientation: portrait)").matches) {
							document.getElementById("center-video").className = "center-video-portrait";
						} else if (window.matchMedia("(orientation: landscape)").matches) {
							document.getElementById("center-video").className = "center-video-landscape";
						}
					}else{
						document.getElementById("center-video").remove();
					}

				   window.addEventListener("orientationchange", function() {
					   var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;

					   if (NativeAPI.device() == NativeAPI.OS.iOS) {
						   if (Math.abs(window.orientation) === 90) {
							   document.getElementById("center-video").className = "center-video-landscape-ios";
						   } else {
							   document.getElementById("center-video").className = "center-video-portrait";
						   }
					   } else if(NativeAPI.device() == NativeAPI.OS.Desktop && isSafari) {
						   if (Math.abs(window.orientation) === 90) {
							   document.getElementById("center-video").className = "center-video-landscape";
						   } else {
							   document.getElementById("center-video").className = "center-video-portrait";
						   }
					   } else {
						   if (screen.height > screen.width) {
							   document.getElementById("center-video").className = "center-video-portrait";
						   } else {
							   document.getElementById("center-video").className = "center-video-landscape";
						   }
					   }
				   }, false);
               }else{
                   $window.location.href = '#/course/'+$scope.idCourse;
               }
           },
           function(error){
               if ( error.statusText == 'Unauthorized') {
                   $window.location.href = '#/login';
               }
           });
		
		$scope.scrollFixTest = function(){
			$('.content-iframe').on('load', function(){
				$(this).contents().find('html').css({height: '100%', overflow: 'hidden'});
				$(this).contents().find('body').css({height: '100%', overflow: 'auto', '-webkit-overflow-scrolling': 'touch' });
			});
		};
		
       /**
        * Function to download a file
        * @param downloadPath ( URL of the file )
        */
       $scope.downloadFile = function(downloadPath) {
           NativeAPI.dispatch('FileLoAction', downloadPath);

           $timeout(function(){
               Course.trackUserLoStatus($scope.idOrg, $scope.learningObject.loModel.objectType,
                   function(data){
                       if ( data.success ){
                           $scope.currentLoStatus = data.status;
                           $scope.$emit('learningObjectCompleted', {status : data.status});
                       }
                   }, function(error){
                   });
           }, 1000);
       };
       /**
        * This will be used only on iOS
        *
        * @param link to redirect the user in new tab in safari
        */
       $scope.openInNewTab = function (link) {
           NativeAPI.dispatch('FileLoAction', link);
       };

       /**
        * hide LO top bar on landscape mode
        */
       var checkMode = function(){
           var orientation = DeviceOrientation.getDeviceOrientation();
           var navBar = angular.element('#top-bar');
           var frame = angular.element('.frame-wrapper iframe');
			var userAgent = window.navigator.userAgent;
			if (NativeAPI.device() == NativeAPI.OS.iOS) {
               if(orientation == DeviceOrientation.ORIENTATION.PORTRAIT || orientation == DeviceOrientation.ORIENTATION.UPSIDE_DOWN){
                   navBar.show('slow');
                   frame.css('height', 'calc(100% - 72px)');
               }else{
                   navBar.hide('slow');
                   frame.css('padding-top', '30px');
                   frame.css('height', '100%');
               }
           }else if((userAgent.match(/iPad/i) || userAgent.match(/iPhone/i))){
				if(orientation == DeviceOrientation.ORIENTATION.PORTRAIT || orientation == DeviceOrientation.ORIENTATION.UPSIDE_DOWN){
					navBar.show('slow');
					frame.css('padding-top', '52px');
					frame.css('height', 'calc(100% - 72px)');
               }else{
                   navBar.hide('slow');
                   frame.css('padding-top', '30px');
                   frame.css('height', '100%');
			   }
			} else {
               if(orientation == DeviceOrientation.ORIENTATION.PORTRAIT || orientation == DeviceOrientation.ORIENTATION.UPSIDE_DOWN){
                   navBar.show('slow');
				   frame.css('padding-top', '52px');
                   frame.css('height', 'calc(100% - 52px)');
               }else{
                   navBar.hide('slow');
                   frame.css('padding-top', '0px');
                   frame.css('height', '100%');
               }
           }
       };
       /* Attach the function to the device rotation custom event */
	   $window.addEventListener('orientationchange', checkMode, false);
        /* Closing the Learning Object */
       $scope.$on('$destroy', function(){
           /* Clear the channel data */
           ChannelsData.clear();
           /* Destroy the event listener attached to the window object */
           $window.removeEventListener('orientationchange', checkMode, false);
       });


       /**
        * add on orientation change width in wistia and vimeo player
        */

       window.addEventListener("orientationchange", function() {
          var width = window.screen.availWidth + 'px';
           var videoWrapper = $(".asset-video-wrapper iframe, .asset-video-wrapper object, .asset-video-wrapper embed");
               if (Math.abs(window.orientation) === 90) {
                   videoWrapper.width('100%').height('100%');
               } else {
                   videoWrapper.width(width).height('100%');
               }
       }, false);
       /**
        * Return to Course
        */
       $scope.$returnToCourse = function() {
           $scope.$broadcast('LearningObjectClosed');
           $window.location.href = '#/course/' + $routeParams.courseId;
           if ($rootScope.$root.$$phase != '$apply' && $rootScope.$root.$$phase != '$digest') {
               $rootScope.$apply();
           }

       };

       /**
        * center flowplayer video in portrait and landscape
        * */

       $timeout(function(){
           if ( $http.pendingRequests.length > 0){
               $scope.$emit('disableLoading');
           }
           /**
            *  This message is received from mobileapp/test
            *  When you click on buttons and navigate trough the test object
            */
           window.addEventListener('message', function (event) {
			   if(event && event.data === "closePlayer"){
				   $scope.$returnToCourse()
			   }
               if ( event && event.data === 'enable'){
                   $scope.$emit('enableLoading');
               }
               if ( event.data.event_id === 'disable'){
                   if ( event.data.data.url != 'empty'){
                       var iframe = document.getElementById('learning-object-iframe');
                       iframe.src = event.data.data.url;
                   }
				   $scope.$emit('disableLoading');
               }
           }, false);

           if (window.matchMedia("(orientation: portrait)").matches) {
               document.getElementById("center-video").className = "center-video-portrait";
           } else if (window.matchMedia("(orientation: landscape)").matches) {
               document.getElementById("center-video").className = "center-video-landscape";
           }

           window.addEventListener("orientationchange", function() {
               var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;

               if (NativeAPI.device() == NativeAPI.OS.iOS) {
                   if (Math.abs(window.orientation) === 90) {
                       document.getElementById("center-video").className = "center-video-landscape-ios";
                   } else {
                       document.getElementById("center-video").className = "center-video-portrait";
                   }
               } else if(NativeAPI.device() == NativeAPI.OS.Desktop && isSafari) {
                   if (Math.abs(window.orientation) === 90) {
                       document.getElementById("center-video").className = "center-video-landscape";
                   } else {
                       document.getElementById("center-video").className = "center-video-portrait";
                   }
               } else {
                   if (screen.height > screen.width) {
                       document.getElementById("center-video").className = "center-video-portrait";
                   } else {
                       document.getElementById("center-video").className = "center-video-landscape";
                   }
               }
           }, false);
           /**
            * Handle clicking 'back' button in the browser
            * @param event
            */
           var handleBackClick = function(event){
               if ( event.data === 'message')
                    $scope.$returnToCourse();
           };
		   /**
            * This will call NATIVE API bridge function only for ANDROID
            * to open the link in new tab in Chrome/Default browser
			* @param link
			*/
		   var openExternalLink = function (link) {
			   NativeAPI.dispatch('openExternalLink', link);
		   };
           window.addEventListener('message', handleBackClick, false);
           /**
            * This is fix for Test learning object
            * When you complete the test, but fails it this will stop the user to enter
            * one more time to re-complete the test
            *
            * @param iframe
            */
           document.getElementById('learning-object-iframe').onload = function(iframe) {
               var objectIframe = document.getElementById('learning-object-iframe');
               var intervalId   = '';
               if ( objectIframe ){
				   objectIframe.contentDocument.addEventListener('click', function (e) {
                      if ( e.target.tagName === 'A'){ // if element is link ( <a> )
                          e.preventDefault(); // Disable link clicking
                          e.target.setAttribute('target', '_blank'); // Add target='blank' attribute
                          var link = e.target.origin + e.target.pathname; // Generate link
                          if ( iPhone || Android){
							  $scope.openInNewTab(link); // Open link in new tab in Safari
						  } else {
							  var newWindow = window.open(link); // Default open link
							  newWindow.focus();
                          }
                      }
				   }, false);
               }
               /**
                * Accept the settings from LMS to the mobile app
                *
                * @type {number}
                */
               intervalId = setInterval(function () {
				   try{
						if ( typeof objectIframe.contentDocument != 'undefined' || objectIframe.contentDocument != null){
							if ( objectIframe === null || objectIframe.contentDocument === null || objectIframe.contentDocument.getElementById('restart') === null){
								clearInterval(intervalId); //stop looping
							} else {
								var restart = objectIframe.contentDocument.getElementById('restart'); // the restart button
							}

							if ( restart != null && typeof restart != 'undefined' ){
								var passed = objectIframe.contentDocument.getElementsByClassName('lo-passed-icon-big');
								if ( $scope.learningObject.loModel.self_prerequisite == 'incomplete' && (passed.length > 0 && passed != null)){
									restart.style.display = 'none';
									objectIframe.contentDocument.getElementById('restart').style.display = 'none';
									clearInterval(intervalId); //stop looping
								}
								if ( $scope.learningObject.loModel.self_prerequisite == 'null' || ($scope.currentLoStatus === 'completed' && $scope.learningObject.loModel.self_prerequisite == 'incomplete')){
									restart.style.display = 'none'; // Hide the restart button
									objectIframe.contentDocument.getElementById('restart').style.display = 'none';
									clearInterval(intervalId); //stop looping
								}
							} else {
								clearInterval(intervalId); //stop looping
							}
						}else{
							clearInterval(intervalId); //stop looping
						}
					}catch(e){
						console.log(e);
						clearInterval(intervalId);
					}
               }, 100);
           };
       });
   }
    LearningObjectController.$inject = ['$scope', '$routeParams', '$rootScope', '$window', '$timeout', 'Course', 'HTMLDispatcher', 'NativeAPI', 'ChannelsData', '$sce', 'DeviceOrientation', '$http'];
    return LearningObjectController;
});