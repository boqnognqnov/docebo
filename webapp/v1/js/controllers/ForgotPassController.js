/**
 * Created by vladimir on 4.2.2016 г..
 */
'use strict';
define([], function() {
	function ForgotPassController (  $scope, $rootScope, $timeout, $translate, $window, Config) {
        var fw7 = new Framework7();
		var $$ = Dom7;
		
		$timeout(function(){
			/* SET FONT COLOR BASED ON BACKGROUND COLOR */
			$$(".forgot-password-description").css('color', $rootScope.bgFont);
			$$(".back-arrow").css('color', $rootScope.bgFont);
			
			if($rootScope.bgFont == '#ffffff'){
				$$('input').addClass('custom-white');
			}else{
				$$('input').addClass('custom-black');
			}
		
			$$('.item-content.password-input').css({
				'bottom-border-color' : $rootScope.bgFont,
				'color' : $rootScope.bgFont
			});
			$$("#recover").css('color' , $rootScope.bgFont);
			/* SET FONT COLOR BASED ON BACKGROUND COLOR END */
		});

		$scope.addSuccess = function(){
			document.getElementById('main-success').style.visibility = "visible";
			document.getElementById('back-arrow').style.visibility = "hidden";
			document.getElementById('pass-progress').style.visibility = "visible";

		};
		 $scope.addError = function(){
			 document.getElementById('main-warning').style.visibility = "visible";

			 $(".close-warning").click(function(){
				 document.getElementById('main-warning').style.visibility = "hidden";
				 document.getElementById('back-arrow').style.visibility = "visible";
			 });


		};
		$scope.submit = function() {
			try {
				if ($scope.user.email.length > 0) {
					var promise = Config.forgotPassword($scope.user.email);
					promise.then(function (data) {
						$scope.addSuccess();
						$timeout(function () {
							$window.location.href = '#/login';
						}, 3000);
						$('.success-progress').animate({ width: '100%' }, 3000);
					}, function (error) {
						$scope.addError();
					});
				}
			} catch (error){
				$scope.addError();
				document.getElementById('back-arrow').style.visibility = "hidden";
			}
		};
	}
	ForgotPassController.$inject = [ '$scope', '$rootScope', '$timeout', '$translate', '$window', 'Config'];

	return ForgotPassController;
});