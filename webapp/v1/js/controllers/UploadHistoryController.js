/**
 * Created by Mitko on 4/8/2016.
 */

/* global angular */
define(function () {
	/**
	 *
	 * @param $scope
	 * @param $rootScope
	 * @param HTMLDispatcher
	 * @param $translate
	 * @param UploadHistory
	 * @param Time
	 * @param $window
	 * @param $timeout
     * @constructor
     */
    function UploadHistoryController($scope, $rootScope,  HTMLDispatcher, $translate, UploadHistory, Time, $window, $timeout) {
       
		var translateTitle = 'Uploaded items';
		$translate(translateTitle).then(function (result) {
			translateTitle = result;
			HTMLDispatcher.ChangeTitle(translateTitle);
		});
		$timeout(function(){
			$scope.items = UploadHistory.getItems();
			$scope.items.map(function(obj){
				if(typeof obj.icon === 'undefined'){
					obj.icon = 'icon-d_right';
				}
				if(typeof obj.time !== 'object'){
					obj.time = Time.sinceTimestamp(obj.time);
				}
			});
		});

        //Tabs
        $translate(['Uploading Queue', 'Uploaded Items']).then(function(translations){
            $scope.$tabs = [
                {title: translations['Uploading Queue'], action: '#/uploadQueue', icon: 'icon-list'},
				{title: translations['Uploaded Items'], action: '#/uploadHistory', icon: 'icon-completed_outline'}
            ]
        });
        $rootScope.getTabs = function(){
            return $scope.$tabs;
        };
		/**
		 * Clear all(/published items/) items from the UploadHistory service
		 */
		$scope.clearAll = function(){
			UploadHistory.clearAll();
			$timeout(function(){
				$scope.items = UploadHistory.getItems();
			});
		};
		/**
		 * Redirect the user to the uploaded item(asset) page
		 * @param index
         */
		$scope.goToAsset = function(index){
			var item = $scope.items[index];
			$window.location.href = '#/assets/view/'+item.idAsset;
		}
    }
    UploadHistoryController.$inject = ['$scope', '$rootScope', 'HTMLDispatcher', '$translate', 'UploadHistory', 'Time', '$window', '$timeout'];//Injecting dependencies
    return UploadHistoryController;//Return Class definition
});
