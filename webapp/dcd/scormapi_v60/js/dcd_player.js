/* ======================================================================== \
 | 	DOCEBO 																	|
 | 																			|
 | 	Copyright (c) 2014 (Docebo)												|
 | 	http://www.docebo.com													|
 \ ======================================================================== */

var $injector = window.parent.angular.element(window.parent.document.body).injector();
var NativeAPI = $injector.get('NativeAPI');
var $rootScope = $injector.get('$rootScope'); 
var $window = $injector.get('$window');
var $timeout = $injector.get('$timeout');
/**
 * Main class for the player, this will regulate it
 * @param config
 * @constructor
 */
var DCDPlayer = function (config) {
	this._fullscreen = config.fullscreen || false;

	this._size_diff = config.size_diff || false;

	if (config.viewport_width > config.viewport_height) {
		this._size_diff += this._height - parseInt(config.viewport_height);
	} else {
		this._size_diff += this._width - parseInt(config.viewport_height);
	}

	if (config.init_url !== undefined) {
		this.initFromUrl(config.init_url);
	}
	// ipad viewport fix, related to
	try {
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
		if (isiPad) {
			setTimeout("window.dcd_player.ipadInit();", 200);
		} else {
			setTimeout("window.dcd_player.desktopInit();", 200);
		}

		setTimeout("window.dcd_player.initWebapp();", 200);

	} catch(e) {}

	try {
		$(window).on('unload', $.proxy(this.browserClose, this));
		$(window).on('beforeunload', $.proxy(this.browserClose, this));
	} catch(e) {}
};

DCDPlayer.prototype = {

	/**
	 * url from the main frame or window that open the launcher
	 */
	_parent_url: "",

	_r: {},

	_fullscreen: false,

	/**
	 * This is used to understand if we have already managed the browser close window
	 */
	_browser_close_managed: false,

	/**
	 * query params readed from the url, we are expecting the following
	 * _id_user       : the unique identifier of the user
	 * _id_course     : the course from which the user has arrived
	 * _id_reference  : unique identifier of the resource inside a course
	 * _id_resource   : unique identifier of the original resource
	 * _id_item       : this id of the scorm chapter
	 * _scorm_version : the scorm version can be 1.2 and 1.3
	 * _auth_code     : authorization code
	 * _launch_type   : the open mode 'inline', 'lightbox', 'fullscreen', 'popup'
	 * * Preview Mode
	 * _preview_mode  : parameter passed in order to prevent the tracking when passed to the LMS API
	 * close          : if true the scounload will close the player no matther the next_to_play returned
	 */
	_id_user:       false,
	_id_course:     false,
	_id_reference:  false,
	_id_resource:   false,
	_id_item:       false,
	_scorm_version: false,
	_auth_code:     false,
	_launch_type:   'lightbox',
	_close:         false,
	_debug:         false,
	// Preview
	_preview_mode:  false,

	_width : 1024,
	_height: 768,
	_size_diff:     0,

	/**
	 * Caller Lms url, our main server, will be used to retrive the data for the scorm initializazion trough jsonp
	 */
	_lms_url: "",

	/**
	 * This will be the proxy for the tracking callback to the main lms
	 */
	_proxy_url: "",

	next_to_play: 'about:blank',

	/**
	 * Performs some initialization for the Mobile App (scrolling issue fix)
     */
	initWebapp: function() {
		if(this._launch_type == 'webapp') {
			$('#wrapper')
				.css({
					'overflow': 'auto',
					'-webkit-overflow-scrolling': 'touch'
				})
				.on('touchstart', function (event) {});
		}
	},

	initFromUrl: function(url) {

		this._parent_url	= url;

		// parse the get params for info
		this._r = $.url(url).param();
		this._id_user       = this._r.id_user;
		this._id_course     = this._r.id_course;
		this._id_reference  = this._r.id_reference;
		this._id_resource   = this._r.id_resource;
		this._id_item       = this._r.id_item;
		this._scorm_version = this._r.scorm_version;
		this._auth_code     = this._r.auth_code; // this is now returned by the loadremoteSco after the session has been validate, it's reading it only temporarly
		this._entry_point   = this._r.entry_point;
		this._launch_type   = "webapp";
		this._debug         = this._r.debug || false;
		//Preview
		this._preview_mode  = this._r.preview_mode || false;

		// retrieve, from the url the lms that called this place and from the "distribution"
		this._lms_url 		= '' + this._r.host + '';
		this._proxy_url 	= '' + $.url().attr('host');
		if (this._lms_url == '') {
			// if no host is passed we assume that everything is going to happen locally
			this._lms_url = this._proxy_url;
		}
	},

	_add_log: function( msg ) {
		try{
			if (this._debug) {
				if (console.log !== undefined) {
					console.log(msg);
				}
			}
		} catch(e) {}
	},

	launch: function() {

		var context = this;
		//var scoResponse = {};

		NativeAPI.dispatch('scoLaunch', {
			id_user: this._id_user,
			id_resource: this._id_resource,
			id_item: this._id_item,
			id_reference: this._id_reference,
			auth_code: this._auth_code,
			preview_mode: this._preview_mode
		});

		/**
		 * listen for response from native device
		 */
		var scoWatcher = function() {
			$rootScope.$watch(function () {
				return NativeAPI.getBridgeResponse('scoLaunch');
			}, function (data) {
				console.log(typeof data);
				if(typeof data != 'undefined'){
					console.log(data);
					try{
						var cleanJsonString = JSON.parse(data.replace("\\\'", "\'").replace("\\\"", "\"").replace("\\n", "\n"));
					}catch(e){
						console.log(e);
					}
					if (typeof cleanJsonString != 'undefined') {
						var scoResponse = cleanJsonString;
						scoResponse['initialize'] = JSON.parse(decodeURIComponent(scoResponse['initialize']));
						console.log(scoResponse['initialize']);
						scoResponse["launch_url"] = scoResponse["launch_url"]+context._entry_point;
						context.scoload(scoResponse);
					}
				}
			});
		};
		scoWatcher();
		$timeout(function () {
			scoWatcher();
		}, 1000);
	},

	localLaunch: function() {
		this.launch();
	},

	getApi: function() {

		if(this._scorm_version == '1.3') return window.API_1484_11;
		else return window.API;
	},

	scoload: function(data) {
		if (!data.success) {
			$('#error').html("There was an error in the intialization of the player, please try reopening the course again.<br/>"
			+ "Info:" + data.message);
			$('#sco').remove();
			return;
		}

		this._add_log('loading: ' + data.launch_url );

		// Initialize the auth_code for the others calls
		if(data.auth_code) this._auth_code = data.auth_code;

		// Initialize the scorm api with the data readed
		var api_config = {
			server_url: this._proxy_url,
			lms_url: this._lms_url,
			id_user: this._id_user,
			id_item: this._id_item,
			id_reference: this._id_reference,
			id_resource: this._id_resource,
			auth_code: this._auth_code,
			debug: this._debug,
			preview_mode: this._preview_mode
		}

		if(this._scorm_version == '1.3')  window.API_1484_11 = new ScormApi2004(api_config);
		else window.API = new ScormApi12(api_config);

		try {
			this.getApi().loadInitialtracking(data.initialize);
		} catch(e) {
			this._add_log("There was an issue loading the tracking data");
		}
		console.log(data);
		console.log(data.launch_url);
		// in data.launch_url we have the url of the sco path, we can now load it inside the sco iframe
		$('#sco').attr('src', data.launch_url);

		// start the keep-alive to keep session open
		this.keep_alive();

		if (data.title !== undefined) {
			try {
				document.title = data.title;
			} catch(e) {}
			var h1_title = $('.mynavbar h1');
			if (h1_title) h1_title.html(data.title);

            try {
                var frame_title = window.parent.$('.fancybox-docebo-title span');
                if (frame_title) frame_title.html(data.title);
            } catch(e) {}
        }

	},

	keep_alive: function() {

	},

	keep_alive_success: function() {
		// jsonp callback, doing nothing for now
	},

	ipadInit: function() {

		try {
			var html = $('html');
			// this will set the viewport corect size and solve a lot of resize

			if (html.width() == this._width) {
				// landscape
				html.width(this._width)
					.height(this._height - this._size_diff)
			} else {
				//portrait
				html.width(this._height)
					.height(this._width - this._size_diff);
			}
			// this is needed to have iframe scrollable on ipad
			if (/iPhone|iPod|iPad/.test(navigator.userAgent)) {
				// set the correct overflow and for the io7 bug apply the listener workaround
				$('#wrapper')
					.css({
						'overflow-y': 'scroll',
						'-webkit-overflow-scrolling': 'touch'
					})
					.on('touchstart', function(event){});;
			}

			window.addEventListener("orientationchange", function(e) {
				// Announce the new orientation number
				var html = $('html');
				// this will set the viewport corect size and solve a lot of resize
				if (html.width() < html.height()) {
					// landscape
					html.width(window.dcd_player._width)
						.height(window.dcd_player._height - window.dcd_player._size_diff)
				} else {
					//portrait
					html.width(window.dcd_player._height)
						.height(window.dcd_player._width - window.dcd_player._size_diff);
				}
			}, false);
		} catch(e){}
	},

	desktopInit: function() {

		if (this._fullscreen) {
			try {
				// this will set the viewport correct size in case of fullscreen
				var html_height = $('html').height();
				$('#sco').height(html_height - 35);
			} catch(e){}
		}
	},

	attachCloseListener: function(element_selector) {

		$(element_selector).on('click', function(e) {
			e.preventDefault();
			// To close the player just unload the scorm, it will call the finish and this will lead us back to
			// the normal close
			window.dcd_player._close = true;
			$('#sco').attr('src', 'about:blank');
		});
	},

	/**
	 * Action that need to be performed when a finish is called and we need either to open a new one or not
	 */
	scounload: function() {

		// if we have to close the player then the next_to_play will contain r=scormorg/default/closePlayer
		var regexp = /scormorg\/default\/closePlayer/gi;
		if ( (regexp.test(this.next_to_play) || this._close == true) && !this._preview_mode) {
			// let's close the player, how to do it depend on how it was launched
			switch (this._launch_type) {
				case "fullscreen" : {
					// We need to go back on a fixed url that is:
					window.location =  '//' + this._lms_url + '/lms/index.php?r=player&course_id=' + this._id_course + '&launch_type=' + this._launch_type;
				} break;
				case "popup" : {
					// This is a bit more complex, we need a special close url and refresh the opener
					// i will redirect to original domain with extra param for refresh
					window.location = this.next_to_play + '&course_id=' + this._id_course + '&launch_type=' + this._launch_type + '&refresh_opener=1';
				} break;
				case "inline" :
				case "lightbox" :
				default : {
					// We have received the original domain close link, it's fine to be used as it is
					$('#sco').attr('src', this.next_to_play + '&course_id=' + this._id_course + '&launch_type=' + this._launch_type);
				} break;
			} // end switch

		} else {
			// set the next object to play
			try {
				var url = this.next_to_play;
				if(!this._preview_mode){
					url += '&id_course=' + this._id_course
				}
				// $('#sco').attr('src', this.next_to_play + '&launch_type=' + this._launch_type);
				window.location = url;
			} catch(e) {}
		}

	},

	/**
	 * Action triggered on browser close
	 * @param event
	 */
	browserClose: function(event) {

		if (this._browser_close_managed) return;
		this._browser_close_managed = true;
		//force the iframe of the sco to blank to try a better trigger of the object unload
		$('#sco').attr('src', 'about:blank');
		try {
			if (this._launch_type == 'popup' && window.opener) window.opener.location.reload();
		} catch(e) {}
	}

}
