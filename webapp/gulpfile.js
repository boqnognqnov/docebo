/* HERE INCLUDE ALL NEEDED NODE MODULES */
var gulp  = require('gulp');
var zip   = require('gulp-zip');
var hashsum = require("gulp-hashsum");
var del = require('del');
var runSequence = require('run-sequence');
/*Amazon Docebo test credentials */
var accessKeyId = process.env.AMAZON_S3_KEY;
var secretAccessKey = process.env.AMAZON_SECRET;
var region = process.env.AMAZON_REGION;
var bucket = process.env.AMAZON_BUCKET;

var aws = {
    accessKeyId     : typeof accessKeyId != 'undefined' ? accessKeyId : "AKIAILJJZ4DJHM2M4A2A",
    secretAccessKey : typeof secretAccessKey != 'undefined' ? secretAccessKey : "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",
    Region          : typeof region != 'undefined' ? region : "eu-west-1"
};
var s3    = require('gulp-s3-upload')(aws); // AWS Upload plugin
var dirs = ['./**', '!./node_modules/**', '!./node_modules', '!./package.json', '!./gulpfile.js', '!./config.rb', '!./scss/**', '!./webapp.zip', '!./scss', '!./manifest.json'];
/* Enter Gulp tasks */
gulp.task('default', function(){
	runSequence( 'hashsum', 'zip', 'manifest-publish');
});
/**
 * Zipping the whole webapp directory and upload it to amazon
 */
gulp.task('zip', function () {
    gulp.src(dirs)
	.pipe(zip('webapp.zip'))
	.pipe(gulp.dest('./'))
	.pipe(s3({
		Bucket  : typeof bucket != 'undefined' ? bucket : "lmsfiles-test",
		ACL     : "public-read",
		onChange: function(key){
			del(['./webapp.zip']);
		}
	}));
});

gulp.task('hashsum', function(){
	gulp.src(dirs)
		.pipe(hashsum({filename: "manifest.json", json: true}));
});

gulp.task('manifest-publish', ['hashsum'], function(){
	gulp.src('./manifest.json')
	.pipe(s3({
		Bucket  : "lmsfiles-test",
		ACL     : "public-read",
		onChange: function(key){
			del(['./manifest.json']);
		}
	}));
});