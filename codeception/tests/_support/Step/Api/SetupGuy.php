<?php
namespace Step\Api;

use Yii;
use CoreOrgChart;
use CoreOrgChartTree;
use CoreLangLanguage;
use CoreGroupFields;
use CoreUserField;

use CException;

/**
 * Purpose of this class is to act as a helper for all the setup for the tests
 *
 * Class SetupGuy
 * @package Step\Api
 */
class SetupGuy extends \ApiGuy{

	/**
	 * @param $translations
	 * @param bool|false $code
	 * @param bool|false $idParent
	 * @return array
	 * @throws CException
	 * @throws CHttpException
	 */
	public function createBranch($translations, $code = false, $idParent = false){
		$I = $this;
		$I->amGoingTo('create a branch/node');

		if(!$idParent)
			$idParent = CoreOrgChartTree::getOrgRootNode()->idOrg;

		$activeLanguagesList = CoreLangLanguage::getActiveLanguages();

		$parent = CoreOrgChartTree::model()->findByPk($idParent);

		$coreOrgChartTree = new CoreOrgChartTree();
		$coreOrgChartTree->code = $code;
		if ($coreOrgChartTree->validate()) {
			$coreOrgChartTree->appendTo($parent);

			// due to foreign key constraints, translations must be added AFTER tree node creation
			$orgChartId = $coreOrgChartTree->getPrimaryKey();
			if (!empty($orgChartId) && !empty($translations)) {
				foreach ($activeLanguagesList as $key => $lang) {
					$translation              = new CoreOrgChart();
					$translation->id_dir      = $orgChartId;
					$translation->lang_code   = $key;
					$translation->translation = isset($translations[$key]) ? $translations[$key] : '';
					$translation->save();
				}
			}
			// end of translations filling

			//handling of additional fields:
			// - if coming from root node then set all fields ON
			// - otherwise set only the parent folder fields without considering the root
			$rootNode = CoreOrgChartTree::getOrgRootNode();
			$additionalFields = array();
			if ($idParent != $rootNode->getPrimaryKey()) {
				//find all additional fields inherited from ancestor nodes
				//NOTE: parent node alone should already own all needed fields, but read from all ancestors the same to prevent possible errors
				$cmd = Yii::app()->db->createCommand("SELECT gf.id_field "
					." FROM ".CoreOrgChartTree::model()->tableName()." oct "
					." JOIN ".CoreGroupFields::model()->tableName()." gf ON (gf.idst = oct.idst_oc OR gf.idst = oct.idst_ocd) "
					." WHERE oct.iLeft <= :iLeft AND oct.iRight >= :iRight AND oct.idOrg <> :root");
				$reader = $cmd->query(array(
					':iLeft' => $parent->iLeft,
					':iRight' => $parent->iRight,
					':root' => $rootNode->getPrimaryKey()
				));
				while ($row = $reader->read()) {
					$additionalFields[] = $row['id_field'];
				}
				$additionalFields = array_unique($additionalFields);
			} else {
				//plain assign all additional fields
                $cmd = Yii::app()->db->createCommand("SELECT DISTINCT(id_field) FROM ".CoreUserField::model()->tableName());
                $reader = $cmd->query();
                while ($row = $reader->read()) { $additionalFields[] = $row['id_field']; }
			}

			//insert new values
			foreach ($additionalFields as $additionalField) {
				$assignment = new CoreGroupFields();
				$assignment->idst = $coreOrgChartTree->idst_ocd;
				$assignment->id_field = $additionalField;
				$assignment->mandatory = 'true';
				$rs = $assignment->save();
				if (!$rs) { throw new CException('Error while saving new field node assignments'); }
			}

			//end of additional fields assignment

			return $orgChartId;
		} else
			throw new CException("Error while saving new node");
	}
}