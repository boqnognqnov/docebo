<?php

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = null)
 *
 * @SuppressWarnings(PHPMD)
 */

class AcceptanceTester extends \Codeception\Actor {

	use _generated\AcceptanceTesterActions;
	use AcceptanceTraits\BackgroundJob;

	private $_id;
	private $username;

	/**
	 * @param $selector
	 * @param $modal the selector of the modal when is loaded completely
	 */
	public function openModal($selector, $modal = null) {
		$this->click($selector);

		if($modal == null)
			$modal = \Page\Common::$modalElement . \Page\Common::$modalLoaded;

		$this->waitForElement($modal);
	}

	public function closeModal() {
		$this->click(\Page\Common::$modalElement . ' ' . \Page\Common::$modalCloseElement);
	}

	/**
	 * @param string $username
	 * @param string $password
	 * @param string $goToHomePage - if true, should redirect to original home page - without multidomain for example, use FALSE, if you are in multidomain environment
	 */
	public function login($username, $password, $goToHomePage = true) {
		$page = new \Page\Home($this);
		$page->login($username, $password, $goToHomePage);

		//save the username after a successful login, in case we need it later ( for id retrieval in getId() )
		$this->username = $username;
	}

	/**
	 * @param string $username
	 * @param string $password
	 * @param string $goToHomePage - if true, should redirect to original home page - without multidomain for example, use FALSE, if you are in multidomain environment
	 */
	public function loginFail($username, $password, $goToHomePage = true){
		$page = new \Page\Home($this);
		$page->loginFail($username, $password, $goToHomePage);
	}

	public function logout(){
		$page = new \Page\Common($this);
		$page->logout();
	}

	public function checkLandingPage() {
		$this->amGoingTo("check the landing page");

		/* === Catalog landing page === */
		$catalogSetting = $this->grabFromDatabase('core_setting', 'param_value', array('param_name' => 'first_catalogue'));
		if ($catalogSetting == 'on') {
			$this->seeInCurrentUrl('opt=catalog');
			return; //no need for further checking
		}

		/* === Learning Plan landing page  === */
		$learningPlanSetting = $this->grabFromDatabase('core_setting', 'param_value', array('param_name' => 'curricula_is_home_page'));
		//we don't care what column we get, just use idUser, all we want is to see if such a record exists
		$userHasLearningPlan = $this->grabFromDatabase('learning_coursepath_user', 'idUser', array('idUser' => $this->getId()));

		//the setting might be on BUT, if there are no courses, we still would go to the dashboard
		if ($learningPlanSetting == 'on' && $userHasLearningPlan) {
			$this->seeInCurrentUrl('curricula/show');
			return;
		}

		/* In case both options are disabled - we must land on the dashboard */
		$this->seeInCurrentUrl("site/index");
	}

	/**
	 * Get id from the database based on the username ( if not already set in _id )
	 */
	public function getId() {
		if ($this->_id) {
			return $this->_id;
		}

		if (!$this->username) {
			return false;
		}

		$this->_id = $this->grabFromDatabase('core_user', 'idst', array('userid' => "/$this->username"));

		return $this->_id;
	}

	/**
	 * Go to specific URL using the side menu
	 *
	 * @param $url
	 */
	public function goToPageUsingMenu($url){

		//wait for the menu to appear if there is any animation
		$this->waitForElementVisible(\Page\SideMenu::$menu);
		$this->moveMouseOver(\Page\SideMenu::getMenuSelector($url));

		//WAIT for menu to be visible, sometimes the execution is too quick and it will fail to click on the link afterwards
		$this->waitForElementVisible(\Page\SideMenu::$openedSecondMenu);
		$this->click(\Page\SideMenu::getMenuLinkSelector($url));
	}

	public function seeDoceboSuccessFlash(){
		$this->seeElement(\Page\Common::$docebo_success_flash);
	}

	/**
	 * @param $element
	 */
	public function seeStyledCheckboxIsChecked($element){
		$this->seeElementInDOM($element, array('checked' => true));
	}

	/**
	 * @param $element
	 */
	public function seeStyledRadioIsChecked($element){
		$this->seeElementInDOM($element, array('checked' => true));
	}

	/**
	 * @param $checkStyledOption
	 */
	public function checkStyledRadio($checkStyledOption){
		$this->checkOption($checkStyledOption . ' + span.jq-radio');
	}

	/**
	 * @param $element
	 */
	public function dontSeeStyledCheckboxIsChecked($element){
		$this->seeElementInDOM($element, array('checked' => false));
	}

	/**
	 * @param $element
	 */
	public function checkStyledOption($checkStyledOption){
		$this->checkOption($checkStyledOption . ' + span.jq-checkbox');
	}

	/**
	 * @param $element
	 * For some reason pure uncheckOption doesn't work, so we use click event here
	 */
	public function unCheckStyledOption($checkStyledOption){
		$this->click($checkStyledOption . ' + span.jq-checkbox');
	}

	/**
	 * @param $element
	 * @param $value mixed (string | array)
	 */
	public function seeFBKCompleteHasSelected($element, $values){
		$items = is_string($values) ? array($values) : $values;
		foreach($items as $value)
			$this->see($value, $element . ' + ul.holder');
	}

	/**
	 * @param $element
	 * @param $value
	 */
	private function fillRteEditor($selector, $content) {
		$this->executeInSelenium(
			function (\Facebook\WebDriver\Remote\RemoteWebDriver $webDriver) use ($selector, $content) {
				$webDriver->switchTo()->frame($webDriver->findElement($selector));

				$webDriver->executeScript(
					'arguments[0].innerHTML = "' . addslashes($content) . '"',
					[$webDriver->findElement(\Facebook\WebDriver\WebDriverBy::tagName('body'))]
				);

				$webDriver->switchTo()->defaultContent();
			});
	}

	/**
	 * @param $attribute
	 * @param $value
	 * @param $content
	 */
	private function fillTinyMceEditor($attribute, $value, $content) {
		$this->fillRteEditor(
			\Facebook\WebDriver\WebDriverBy::xpath(
				'//textarea[@' . $attribute . '=\'' . $value . '\']/../div[contains(@class, \'mce-tinymce\')]//iframe'
			),
			$content
		);
	}

	/**
	 * @param $id
	 * @param $content
	 */
	public function fillTinyMceEditorById($id, $content) {
		$this->fillTinyMceEditor('id', $id, $content);
	}

	public function scrollToBottomOfPage(){
		$this->executeJS('window.scrollTo(0, document.body.scrollHeight)');
	}

	public function fillFCBKAutocomplete($element, $values){
		if(is_string($values))
			$values = array($values);
		// For each domain we want to add in to the allowed domains
		$input = "select#".$element." + ul li.bit-input input";
		foreach($values as $value){
			$this->fillField($input, $value);
			$this->pressKey($input, WebDriverKeys::ENTER);
		}
	}

	public function removeFCBKAutocompleteElement($element, $value){
		$xpath = array(
			'xpath' => '//select[@id="'.$element.'"]/following-sibling::ul[1]//li[text()="'.$value.'"]//a'
		);
		$this->click($xpath);
	}

}
