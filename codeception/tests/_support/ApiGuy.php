<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = null)
 *
 * @SuppressWarnings(PHPMD)
 */
class ApiGuy extends \Codeception\Actor{
	use _generated\ApiGuyActions;

	//these traits are created mostly so that we can have logically separate the functionalities
	use makeSafe;

	public function grabUsers($limit = 3, $models = false){
		if($models){
			$users = CoreUser::model()->findAll(array(),array('limit'=>$limit));
		}else{
			$users = Yii::app()->db->createCommand()
				->select('idst')
				->from(CoreUser::model()->tableName())
				->where('valid = ' . CoreUser::USER_STATUS_ACTIVE)
				->limit($limit)
				->queryColumn();
		}

		return $users;
	}

	public function createFakeErollmentFields($limit = 3)
	{
		$fieldIds = [];
		for ($i = 0; $i < $limit; $i++) {
			$tempErollField = new LearningEnrollmentField();
			$fieldType = mt_rand(1, 4);
			$tempErollField->type = $fieldType;
			$tempErollField->translation = '[{"en": "en text", "fr": "fr text", "fr2": "fr text"}]';

			$tempErollField->save(false);

			if ($tempErollField->type == LearningEnrollmentField::TYPE_FIELD_DROPDOWN) {
				$fieldIds[$tempErollField->id] = $this::createFakeEnrollDoropdown(3, $tempErollField->id);
			}else{
				$fieldIds[$tempErollField->id] = 50;
			}
		}

		return $fieldIds;
	}

	public function createFakeEnrollDoropdown($limit = 3, $fieldId)
	{
		$tempDropdown = null;

		for ($i = 0; $i < $limit; $i++) {
			$tempDropdown = new LearningEnrollmentFieldDropdown();
			$tempDropdown->id_field = $fieldId;
			$tempDropdown->translation = '[{"en": "en bla223432"}, {"de": "de bla2432432"}]';
			$tempDropdown->save(false);
		}

		return $tempDropdown->id;
	}

	/**
	 * Get the top $limit users with most courses
	 *
	 * @param int $limit
	 * @return mixed
	 */
	public function grabUsersWithCourses($limit = 3){

		$users = Yii::app()->db->createCommand()
			->select('idUser')
			->from(LearningCourseuser::model()->tableName())
			->group('idUser')
			->order('count(1) DESC')
			->limit($limit)
			->queryColumn();

		return $users;
	}

	/**
	 * @return CoreUser
	 * @throws Exception
	 */
	public function grabNewUser(){
		$u = new CoreUser('createApi');
		$u->userid = 'ApiTmpUser - ' . substr(Docebo::randomHash(), 0, 10);
		$u->firstname = 'Api';
		$u->lastname = 'User';
		$u->new_password = CoreUser::model()->encryptPassword('changeme');
		$u->new_password_repeat = $u->new_password;
		$u->email = 'api@test.com';

		//add organization chart root node by default
		$_roots = CoreOrgChartTree::model()->roots()->findAll();
		if (!empty($_roots)) {
			$rootNode = $_roots[0];
			$u->chartGroups[] = $rootNode->getPrimaryKey();
		}

		if(!$u->save())
			throw new Exception ("Can't create new user:" . CJSON::encode($u->getErrors()));

		// Keep the user in the proper org_charts
		$u->saveChartGroups();

		// Keep the user as normal user in the system
		$groupMember = new CoreGroupMembers('create');
		$groupMember->idst = 6; // leaving it hardcoded for now (6 = normal user)
		$groupMember->idstMember = $u->idst;
		$groupMember->save();

		return $u;
	}

	/**
	 * @param int $limit
	 * @param bool|false $models
	 * @param null $type
	 * @return static[]
	 */
	public function grabCourses($limit = 3, $models = false, $type = null){
		if($models){
			if($type !== null) {
				$criteria = new CDbCriteria();
				$criteria->limit = $limit;
				$criteria->addCondition('course_type=:type');
				$criteria->params[':type'] = $type;

				$result = LearningCourse::model()->findAll($criteria);
			} else {
				$result = LearningCourse::model()->findAll(array(), array('limit' => $limit));
			}
		} else {
			$command = Yii::app()->db->createCommand()
				->select('idCourse')
				->from(LearningCourse::model()->tableName());
			if($type !== null){
				$command->where('course_type = :type', array(':type' => $type));
			}
			$result = $command->limit($limit)->queryColumn();
		}

		return $result;
	}

	/**
	 * @param $level
	 * @param bool|false $returnModel
	 * @return mixed|static
	 */
	public function grabUserByLevel($level, $returnModel = false){
		$levelId = array_search($level, Yii::app()->user->getUserLevelIds());

		//check for such a record in the database
		$this->seeInDatabase('core_group_members', array('idst' => $levelId));

		//in the case when we want a powerUser, try to provide a PU with as much assigned users/courses as possible
		//this way we will avoid ( when possible ) the return of a PU with only himself as assigned user
		if($level == Yii::app()->user->level_admin){
			$idst = Yii::app()->db->createCommand()
				->select('cgm.idstMember')
				->from('core_group_members as cgm')
				->join('core_user_pu cup', 'cup.puser_id = cgm.idstMember')
				->join('core_user_pu_course cupc', 'cupc.puser_id = idstMember')
				->where('cgm.idst = :level')
				->group('cgm.idstMember')
				->order('COUNT(cgm.idstMember) DESC')
				->queryScalar(array(
					':level' => $levelId
				));
		}else{
			//in the case when we want a superAdmin or a normal user, we just get the first record we find
			$idst = $this->grabFromDatabase('core_group_members', 'idstMember', array('idst' => $levelId));
		}

		if($returnModel){
			$coreUser = CoreUser::model()->findByPk($idst);
			return $coreUser;
		}else{
			return $idst;
		}
	}

	public function grabPowerUserProfile(){
		$profile = Yii::app()->db->createCommand()
			->select('groupid')
			->from(CoreGroup::model()->tableName())
			->where('groupid LIKE "%/framework/adminrules/%"')
			->queryScalar();

		\PHPUnit_Framework_Assert::assertNotFalse($profile);

		//return only the profile name
		return substr($profile, strlen("/framework/adminrules/"));
	}

	public function grabGroup(){
		$coreGroup = CoreGroup::model()->findByAttributes(array('hidden' => 'false'));
		\PHPUnit_Framework_Assert::assertNotFalse($coreGroup);

		return $coreGroup;
	}

	public function grabCoreOrgChartTree(){
		$coreOrgChartTree = CoreOrgChartTree::model()->find();
		PHPUnit_Framework_Assert::assertNotFalse($coreOrgChartTree);

		return $coreOrgChartTree;
	}

	public function grabUserAssignedToPU($powerUserId){
		//ignore the actual PU
		$coreUserPu = CoreUserPU::model()->find('puser_id = :powerUserId AND user_id != :powerUserId', array(
			':powerUserId' => $powerUserId
		));
		$alreadyAssignedUser = $coreUserPu->user;

		if(!$alreadyAssignedUser)
			PHPUnit_Framework_Assert::fail("No users were found that are assigned the Power User with ID {$powerUserId}");

		return $alreadyAssignedUser;
	}

	/**
	 * @param $type
	 * @param int $limit
	 * @param bool|false $powerUserId - the course will be from this Power User
	 * @param bool|false $alreadyAssigned
	 * @return array|mixed|null
	 */
	public function grabCoursesByType($type, $powerUserId, $limit = 5, $alreadyAssigned = false){

		$this->makeSafeCourseAssignments($powerUserId, $type);

		$criteria = new CDbCriteria();
		$criteria->addCondition('course_type = :courseType');
		$criteria->addCondition('code > ""'); //ignore records that don't have a course code ( empty strings and NULL values are both ignored )
		$criteria->with = array('coreUserPuCourse');
		$criteria->together = true;
		$criteria->group = "t.idCourse";

		if($alreadyAssigned)
			$criteria->addCondition('idCourse IN (SELECT course_id FROM core_user_pu_course WHERE puser_id = :powerUserId)');
		else
			$criteria->addCondition('idCourse NOT IN (SELECT course_id FROM core_user_pu_course WHERE puser_id = :powerUserId)');

		$criteria->limit = $limit; //uncomment later after testing
		$criteria->params = array(
			':powerUserId' => $powerUserId,
			':courseType' => $type,
		);

		$courses = LearningCourse::model()->findAll($criteria);

		if(!$courses){
			$assignedOrNot = $alreadyAssigned ? "assigned" : "not assigned";

			PHPUnit_Framework_Assert::fail("No courses were found that are of type {$type} and are {$assignedOrNot} to PU {$powerUserId}");
		}

		return $courses;
	}

	public function grabLearningPlan($powerUserId, $limit = 3, $alreadyAssigned = false){

		$this->makeSafeLearningPlanAssignments($powerUserId);

		$criteria = new CDbCriteria();
		$criteria->join = "LEFT JOIN core_user_pu_coursepath cupcp ON cupcp.path_id = t.id_path";

		if($alreadyAssigned)
			$criteria->addCondition('t.id_path IN (SELECT path_id FROM core_user_pu_coursepath WHERE puser_id = :powerUserId)');
		else
			$criteria->addCondition('t.id_path NOT IN (SELECT path_id FROM core_user_pu_coursepath WHERE puser_id = :powerUserId)');

		$criteria->limit = $limit;
		$criteria->group = "t.id_path";
		$criteria->params = array(
			':powerUserId' => $powerUserId,
		);

		$learningPlans = LearningCoursepath::model()->findAll($criteria);

		if(!$learningPlans){
			$assignedOrNot = $alreadyAssigned ? "assigned" : "not assigned";

			PHPUnit_Framework_Assert::fail("No learning plans were found that are {$assignedOrNot} to PU {$powerUserId}");
		}

		return $learningPlans;
	}

	public function grabCategory($powerUserId, $limit = 3, $alreadyAssigned = false){

		$this->makeSafeCategoryAssignments($powerUserId);

		$criteria = new CDbCriteria();
		$criteria->join = "LEFT JOIN core_admin_course cac ON t.idCategory = cac.id_entry";

		if($alreadyAssigned)
			$criteria->addCondition('t.idCategory IN ( SELECT id_entry FROM core_admin_course WHERE idst_user = :powerUserId AND type_of_entry = :type)');
		else
			$criteria->addCondition('t.idCategory NOT IN ( SELECT id_entry FROM core_admin_course WHERE idst_user = :powerUserId AND type_of_entry = :type)');

		$criteria->limit = $limit;
		$criteria->group = "t.idCategory";
		$criteria->order = 'idCategory DESC'; //this way the root will be last, so we can mostly avoid assigning him
		$criteria->params = array(
			':powerUserId' => $powerUserId,
			':type' => CoreAdminCourse::TYPE_CATEGORY_DESC,
		);

		$category = LearningCourseCategory::model()->findAll($criteria);

		if(!$category){
			$assignedOrNot = $alreadyAssigned ? "assigned" : "not assigned";

			PHPUnit_Framework_Assert::fail("No categories were found that are {$assignedOrNot} to PU {$powerUserId}");
		}

		return $category;
	}

	public function buildItemsArray($itemsMultiArr){
		$returnArr = array();

		//populate the array
		foreach($itemsMultiArr as $type => $items){
			foreach($items as $item){

				//the $type can be mapped as a public property in the class and evaluated before the second foreach to speed things up
				//but since tests won't be run that often, there is no need. Keeping it like this for better readability
				switch($type){
					case PoweruserApiModule::COURSES_TYPE_ID_COURSE:
						$returnArr[$type][] = $item->idCourse;
						break;

					case PoweruserApiModule::COURSES_TYPE_COURSE_CODE:
						if($item->code)
							$returnArr[$type][] = $item->code;

						break;

					case PoweruserApiModule::COURSES_TYPE_ID_LEARNINGPLAN:
						$returnArr[$type][] = $item->id_path;
						break;

					case PoweruserApiModule::COURSES_TYPE_LEARNINGPLAN_CODE:
						$returnArr[$type][] = $item->path_code;
						break;

					case PoweruserApiModule::COURSES_TYPE_ID_CATEGORY:
						$returnArr[$type][] = $item->idCategory;
						break;

					default:
						//skipping (for now) if the type doesn't match
						break;
				}

			}
		}

		return $returnArr;
	}

	public function grabScheduledJob($active = false){

//		$this->makeSafeScheduledJobs(); //not yet finished

		return CoreJob::model()->find();
	}

	public function getUsersIdsInCourse($courseId, $limit = 1)
	{
		$usersIds = Yii::app()->db->createCommand()
			->select('idUser')
			->from(LearningCourseuser::model()->tableName())
			->where('idCourse = :id', array(':id' => $courseId))
			->limit($limit)->queryColumn();
		return $usersIds;
	}

	public function grabRandomLocationId(){
		$idsLocations = Yii::app()->db->createCommand()
				->select('id_location')
				->from(LtLocation::model()->tableName())->queryColumn();
		$key = array_rand($idsLocations, 1);
		$idLocation = $idsLocations[$key];

		return $idLocation;
	}

	public function getRandomBranchId($lev = null)
	{
		$command = Yii::app()->db->createCommand()
				->select('idOrg')
				->from(CoreOrgChartTree::model()->tableName());
		if ($lev !== null) {
			$command->where('lev = :lev', array(':lev' => $lev));
		}

		$branchIds = $command->queryColumn();
		\PHPUnit_Framework_Assert::assertNotEmpty($branchIds, 'No branches found');

		$id = $branchIds[array_rand($branchIds, 1)];
		return $id;
	}

	/**
	 * Check that the response doesn't contain other course types, except the one provided
	 * @param $courseType
	 */
	public function seeJsonResponseContainsOnlyOneCourseType($courseType) {
		$this->amGoingTo("check that the response doesn't contain other course types except " . $courseType);

		$courseTypes = \LearningCourse::typesList();
		unset($courseTypes[$courseType]);

		//confirm all OTHER types of course are not in the response
		foreach($courseTypes as $typeKey => $courseTypeName){
			$this->dontSeeResponseContainsJson(array(
				"course_type"=>$typeKey
			));
		}
	}


}
