<?php
namespace Helper;
use Yii;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use CoreUser;
use LearningCourse;
use LearningCourseuser;
use CHttpException;

class Course extends \Codeception\Module
{
	/**
	 * Returns an Course Object
	 *
	 * @param $courseCode
	 *
	 * @return $learningCourse Object
	 */
	public function createElearningCourseWithCode($courseCode) {
		// Create brand new Course with passed CODE
		$learningCourse = new LearningCourse();
		$learningCourse->setScenario('create');
		$learningCourse->setCourseDefaults();
		$learningCourse->name = $courseCode;
		$learningCourse->code = $courseCode;
		$learningCourse->description = $courseCode;
		$learningCourse->saveCourse();
		return $learningCourse;
	}

	/**
	 * @param $assignmentType
	 * @param $user
	 * @param $course
	 */
	public function assignUserAs($assignmentType = false, CoreUser $user, LearningCourse $course){
		$assignmentType = $assignmentType ? $assignmentType : LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT;
		//if not, then do enrollment
		$courseUser = new LearningCourseuser();
		if (!$courseUser->subscribeUser($user->getPrimaryKey(), Yii::app()->user->id, $course->idCourse, 0, $assignmentType)) {
			throw new CHttpException(254, 'Error while enrolling user');
		}
	}

    /**
     * @param $user
     * @param $course
     * @param $time
     */
    public function setEnrollmentDateOfUser(CoreUser $user, LearningCourse $course, $time) {
        $courseUserParams = array(
            'idUser' => $user->getPrimaryKey(),
            'idCourse' => $course->getPrimaryKey()
        );
        $courseUser = LearningCourseuser::model()->findByAttributes($courseUserParams);
        if(!$courseUser) {
            throw new CException(Yii::t('error', 'Couldn\'t find LearningCourseUser with user_id: {user} and course_id: {course} ', array('{user}' => $user->getPrimaryKey(), '{course}' => $course->getPrimaryKey())));
        }
        $courseUser->date_inscr = $time;
        if(!$courseUser->save()) {
            throw new CException(Yii::t('error', 'Couldn\'t change the date of LearningCourseuser with id: {id}', array('{id}' => $courseUser->getPrimaryKey())));
        } else {
            return true;
        }
    }

    
}
