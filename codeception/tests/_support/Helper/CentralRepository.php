<?php

namespace Helper;
use Yii;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use CoreUser;
use LearningCourse;
use LearningCourseuser;
use CHttpException;
use LearningCourseCategoryTree;
use LearningRepositoryObject;
use LearningRepositoryObjectVersion;
use LearningObjectsManager;
use LearningCommontrack;
use LearningOrganization;


class CentralRepository extends  \Codeception\Module
{
    /**
     * Returns an LearningObjectsCategory
     *
     * @param $name
     * @param $parent
     *
     * @return $cat Object
     */
    public function createCategory($name , $parent = null) {

        if($parent === null){
            $root = LearningObjectsCategory::model()->roots()->find();
        }else{
            $root = LearningObjectsCategory::model()->findByPk($parent);
        }

        // Create brand new Course with passed CODE
        $cat = new LearningObjectsCategory();
        $cat->title = $name;
        $cat->appendTo($root);
        return $cat;
    }


    /**
     * Returns an LearningRepositoryObject
     *
     * @param $name
     * @param $type
     * @param $category
     *
     * @return $lo Object
     */
    public function createLO($name, $type, $category = null){
        if($category === null){
            $root = LearningCourseCategoryTree::model()->roots()->find();
            $category = $root->getPrimaryKey();
        }
        $lo = new LearningRepositoryObject();
        $lo->title = $name;
        $lo->object_type = $type;
        $lo->id_category = $category;
        $lo->save();
        return $lo;
    }

    public function activateElucidatPlugin(){
        \PluginManager::activateAppByCodename('Elucidat');
    }

    public function createVersion($idObject, $idResource, $objectType, $author, $version = 1, $name = 'V1', $description = 'AcceptanceTestVersion'){
        $versionModel = new LearningRepositoryObjectVersion();
        $versionModel->id_object = $idObject;
        $versionModel->id_resource = $idResource;
        $versionModel->object_type = $objectType;
        $versionModel->version_name = $name;
        $versionModel->version_description = $description;
        $versionModel->author = $author;
        $versionModel->version = $version;
        $versionModel->create_date = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
        if(!$versionModel->save()){
            return false;
        }

        return $versionModel;
    }

    public function createCourse($courseName = 'C1', $description = 'AcceptanceTestCourse', $type = LearningCourse::TYPE_ELEARNING, $category = 1){
        $course = new LearningCourse();
        $course->name = $courseName;
        $course->description = $description;
        $course->idCategory = $category;
        $course->lang_code = 'english';
        $course->course_type = $type;
        $course->save();

        return $course;
    }

    public function pushToCourse($idResource, $objectType, $idCourse, $title, $idObject){
        $loManager = new LearningObjectsManager($idCourse);
        $res = $loManager->addLearningObject($objectType, $idResource, $title, false, array(), $idObject);

        return $res;
    }

    public function subscribeUserToCourse($idCourse, $idUser, $level = 3){
        $courseUser = new LearningCourseuser();
        $courseUser->idUser = $idUser;
        $courseUser->idCourse = $idCourse;
        $courseUser->level = $level;

        return $courseUser->save();
    }

    public function completeLO($idReference, $idUser, $idResource){
        $lo = LearningOrganization::model()->findByPk($idReference);

        $videoTrack = new \LearningVideoTrack();
        $videoTrack->idReference = $lo->idOrg;
        $videoTrack->idResource = $idResource;
        $videoTrack->idUser = $idUser;
        $videoTrack->status = LearningCommontrack::STATUS_COMPLETED;

        return $videoTrack->save();
    }
    
    public function putLoInProgress($idReference, $idUser, $idResource){
        $lo = LearningOrganization::model()->findByPk($idReference);

        $videoTrack = new \LearningVideoTrack();
        $videoTrack->idReference = $lo->idOrg;
        $videoTrack->idResource = $idResource;
        $videoTrack->idUser = $idUser;
        $videoTrack->status = LearningCommontrack::STATUS_ATTEMPTED;

        return $videoTrack->save();
    }

}