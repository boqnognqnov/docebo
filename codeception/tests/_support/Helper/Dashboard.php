<?php
/**
 * Created by PhpStorm.
 * User: NikVas
 * Date: 24-Feb-16
 * Time: 10:16
 */
namespace Helper;
// here you can define custom actions
// all public methods declared in helper class will be available in $I

use Yii;
use Docebo;
use DashboardLayout;
use DashboardCell;
use DashboardRow;
use Exception;
//use CJSON;

class Dashboard extends \Codeception\Module
{

   /**
    * @param $position
    * @param string $layoutName
    * @param bool|false $makeItDefault
    * @param bool|false $isFallback
    * @param int $numberOfRows
    * @return int
    */
   public function createLayout($position, $layoutName = '', $makeItDefault = false, $isFallback = false,$numberOfRows = 1){
      $dashboardLayout = new DashboardLayout();
      $dashboardLayout->name = ($layoutName!='')? $layoutName : 'Test Layout';
      $dashboardLayout->default = ($makeItDefault)? 1 : 0;
      $dashboardLayout->position = (int)$position;
      $dashboardLayout->is_fallback = ($isFallback)? 1 : 0;
      try{
         $result = array();
         $dashboardLayout->save();
         $result['id'] = $dashboardLayout->id;
         $rowIds = array();
         for($i = 1;$i<=$numberOfRows;$i++){
            $layoutRow = new DashboardRow();
            $layoutRow->id_layout = $dashboardLayout->id;
            $layoutRow->position = $i;
            $layoutRow->save();
            $rowIds[] = $layoutRow->id;
            for($y = 1; $y<=3; $y++){
               $layoutCell = new DashboardCell();
               $layoutCell->row = $layoutRow->id;
               $layoutCell->position = $y;
               $layoutCell->width = 4;
               $layoutCell->save();
            }

         }
         $result['rows'] = $rowIds;
         return $result;
      }
	  catch(Exception $e){
		Yii::log('Creation of dashboard layout failed: '.$e->getMessage(), CLogger::LEVEL_ERROR);
      }
   }
}