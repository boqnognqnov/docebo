<?php

namespace Helper\apps;

use Yii;
use Docebo;
use CoreUser;
use CoreOrgChartTree;
use Exception;
use CJSON;
use CoreGroupMembers;
use PluginManager;
use CoreMultidomain;
use MultidomainManager;

class Multidomain  extends \Codeception\Module
{
    public function createClient($data){
        // Get current model and save it, used to compare old/new model attributes later, if required
        $model = new CoreMultidomain();
        // Create one more instance of the model
        $oldModel = clone $model;
        $model->attributes = $data;
        if ($model->validate()) {
            if ($this->updateLmsConfiguration($model, $oldModel)) {
                $model->save();
                return $model;
            }
        }
        return null;
    }

    /**
     * Update client LMS configuration: Redis, File, etc.
     *
     * @param CoreMultidomain $model
     * @param CoreMultidomain $oldModel
     */
    protected function updateLmsConfiguration(CoreMultidomain $model, CoreMultidomain $oldModel) {

        // NEW/CUSTOM DOMAIN ?
        if (in_array($model->domain_type, array(CoreMultidomain::DOMAINTYPE_CUSTOMDOMAIN, CoreMultidomain::DOMAINTYPE_SUBDOMAIN))) {
            return MultidomainManager::updateClientDomain($model->getClientUri(), $oldModel->getClientUri());
        }

        return true;

    }
}