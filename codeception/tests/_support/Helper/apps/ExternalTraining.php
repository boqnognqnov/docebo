<?php
namespace Helper\apps;
// here you can define custom actions
// all public methods declared in helper class will be available in $I

use Yii;
use Docebo;
use CoreUser;
use CoreOrgChartTree;
use Exception;
use CJSON;
use CoreGroupMembers;
use TranscriptsField;
use TranscriptsFieldDropdown;
use TranscriptsFieldDropdownTranslations;
use TranscriptsRecord;
use CHttpException;
use PluginManager;
use CertificationItem;

class ExternalTraining extends \Codeception\Module
{
    /**
     * @param string $type
     * @param string $title
     * @param array $options Dropdown Options
     * @return TranscriptsField $af
     */
    public function createAdditionalField($type, $title, $options = array()){
        $langCode = CoreUser::getDefaultLangCode();
        // Create new Transcripts field
        $af                      = new TranscriptsField();
        $af->setFieldTranslations(array($langCode => $title));
        $af->type                = $type;
        $af->sequence            = TranscriptsField::model()->getNextSequence();
        $af->settings            = CJSON::encode(array());
        // Let's save the additional field
        if ($af->save()) {
            // If the tpye of the field is dropdown then let's create hi's options
            if($type == TranscriptsField::TYPE_FIELD_DROPDOWN){
                $option_sequence = 0;
                foreach($options as $option){
                    $ddo = new TranscriptsFieldDropdown();
                    $ddo->id_field = $af->id_field;
                    $ddo->sequence = $option_sequence;
                    $ddo->save();
                    // Let's create translation for each options
                    $ddot = new TranscriptsFieldDropdownTranslations();
                    $ddot->id_option = (int) $ddo->id_option;
                    $ddot->lang_code = $langCode;
                    $ddot->translation = $option;
                    $ddot->save();
                    // Increment the counter
                    $option_sequence++;
                }
            }
        }else{
            throw new \Exception(CJSON::encode($af->getErrors()));
        }
        return $af;
    }

    public function createRecord($settings){
        $ext = new TranscriptsRecord();

        $inputData = $settings;
        $ext->attributes = $inputData;

        //check if a certificate file has been uploaded
        if (isset($_FILES['certificate']) && ($_FILES['certificate']['name'] != '')) {
            $ext->attachCertificateFile($_FILES['certificate']['tmp_name'], $_FILES['certificate']['name']);
        }

        if($ext->validate()) {
            if (!$ext->save()) { throw new CHttpException(500, 'Error while saving activity data'); }

            if (isset($settings['certification']) && PluginManager::isPluginActive('CertificationApp')) {
                // Save Certification association (retraining)
                CertificationItem::assignItemToCertification(CertificationItem::TYPE_TRANSCRIPT, $ext->getPrimaryKey(), intval($settings['certification']));
            }

            // Update additional fields
            if(isset($inputData['additionalFields']))
                $ext->updateAdditionalFiled($inputData['additionalFields']);

            return $ext;
        }
        else{
            throw new Exception($ext->getErrors());
        }
    }
}