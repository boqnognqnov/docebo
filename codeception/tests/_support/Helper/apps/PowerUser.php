<?php

namespace Helper\apps;

use CoreUser;
use CoreAdminTree;
use Yii;

class PowerUser extends \Codeception\Module
{
    public function assignUsersToPU(CoreUser $pu, $users){
        if(!is_array($users)){
            $users = array($users);
        }

        foreach($users as $user){
            $associationPU = new CoreAdminTree();
            $associationPU->idstAdmin = $pu->idst;
            $associationPU->idst = $user;
            $associationPU->save();
        }
    }

    public function promoteUserToPU($userId){
        $puGroupId = Yii::app()->db->createCommand()
            ->select('idst')
            ->from(\CoreGroup::model()->tableName())
            ->where('groupid=:id', array(':id' => \CoreGroup::GROUP_PU))->queryScalar();
        $userGroupId = Yii::app()->db->createCommand()
            ->select('idst')
            ->from(\CoreGroup::model()->tableName())
            ->where('groupid=:id', array(':id' => \CoreGroup::GROUP_USERS))->queryScalar();
        $oldRecord = Yii::app()->db->createCommand()
            ->select()
            ->from(\CoreGroupMembers::model()->tableName())
            ->where('idst=:id AND idstMember=:user',
                array(':id' => \CoreGroup::GROUP_USERS, ':user' => $userId))->queryScalar();
        if (empty($oldRecord)) {
            Yii::app()->db->createCommand()
                ->insert(\CoreGroupMembers::model()->tableName(), array(
                    'idst' => $puGroupId,
                    'idstMember' => $userId
                ));
        }else{
            Yii::app()->db->createCommand()
                ->update(\CoreGroupMembers::model()->tableName(), array('idst' => $puGroupId), 'idstMember=:user',
                    array(':user' => $userId));
        }
    }

    public function assignBranchesToPU($puserId, $branches)
    {
        foreach ($branches as $branchId) {
            $groupId = Yii::app()->db->createCommand()
                ->select('idst_oc')
                ->from(\CoreOrgChartTree::model()->tableName())
                ->where('idOrg=:id', array(':id' => $branchId))->queryScalar();
            $model = CoreAdminTree::model()->findByAttributes(array('idst' => $groupId, 'idstAdmin' => $puserId));
            if (!$model) {
                $model = new CoreAdminTree();
            }
            $model->idst = $groupId;
            $model->idstAdmin = $puserId;
            $model->save();
        }
    }
}