<?php
namespace Helper;
use Yii;
use LearningTestquest;
use LearningTestquestanswer;
use LearningCourse;
use CoreUser;
use LearningTest;
use LearningTesttrack;
use CException;
use LearningTesttrackAnswer;
use LearningTesttrackQuest;
use LearningPoll;
use LearningPollquest;
use LearningPollquestanswer;
use LearningPollLikertScale;
use LearningPolltrack;
use LearningPolltrackAnswer;
use LearningVideo;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Lo extends \Codeception\Module
{
	/**
	 * Returns an Lo Object
	 *
	 * @param $idCourse
	 * @param $title
	 *
	 * @return $lo Object
	 */
	public function createTest($idCourse, $title) {
		// Create brand new Corse with passed CODE
		//prepare AR object to be saved
		$testObj = new LearningTest();
		$testObj->author = Yii::app()->user->getIdst();
		$testObj->title = $title;

		//these will be needed by learning organization table
		$testObj->setIdCourse($idCourse);
		$testObj->setIdParent(0);
		//do actual saving
		if (!$testObj->save()) {
			throw new CException('Error while saving the test.');
		}

		return $testObj;
	}

	public function createTestQuestion($id, $type, $title, $settings){
		$lqt = new LearningTestquest();
		$lqt->idTest = $id;
		$lqt->type_quest = $type;
		$lqt->title_quest = $title;
		$lqt->idCategory = 0;
		$lqt->difficult = LearningTestquest::DIFFICULTY_MEDIUM;
		$lqt->time_assigned = 0;
		$lqt->sequence = 1;
		$lqt->page = 1;
		$lqt->shuffle = 0;
		$lqt->settings = $settings;
		$lqt->save();

		return $lqt;
	}

	public function createTestQuestionAnswers($idQuestion, $answer = null, $comment = null, $sequence = 1,$correct = null, $incorect = null, $settings = array()){
		$ltqa = new LearningTestquestanswer();
		$ltqa->idQuest = $idQuestion;
		$ltqa->answer = $answer;
		$ltqa->comment = $comment;
		$ltqa->sequence = $sequence;
		$ltqa->score_correct = $correct;
		$ltqa->score_incorrect = $incorect;
		$ltqa->settings = $settings;
		$ltqa->save();

		return $ltqa;
	}

	public function createFakeTestResults(LearningCourse $course, CoreUser $user, LearningTest $test, $idOrg){
		// Create TestTrack
		$testTrack = new LearningTesttrack();
		$testTrack->idReference = $idOrg;
		$testTrack->idUser = $user->idst;
		$testTrack->idTest = $test->idTest;
		$testTrack->score_status = LearningTesttrack::STATUS_VALID;
		if (!$testTrack->save(false)) {
			throw new CException(Yii::t('error', 'Error while saving test tracking data'));
		}

		foreach($test->questions as $key => $question){
			// Create track for the question as well
			$trackQuestion = new LearningTesttrackQuest();
			$trackQuestion->idTrack = $testTrack->getPrimaryKey();
			$trackQuestion->idQuest = $question->getPrimaryKey();
			$trackQuestion->page = 0; //this may be incorrect depending on test options, but it shouldn't be important here
			$trackQuestion->save();
			// Let's create track for each question's answers
			foreach($question->answers as $iKey => $answer){
				$ar = new LearningTesttrackAnswer();
				$ar->idTrack = $testTrack->idTrack;
				$ar->idQuest = $answer->idQuest;
				$ar->idAnswer = $answer->idAnswer;
				$ar->score_assigned = 1;
				$ar->more_info = $key . '_' . $iKey;
				$ar->user_answer = 1;
				$ar->save();
			}
		}
	}

	public function createSurvey($idUser, $idCourse, $title){
		//prepare AR object to be saved
		$pollObj = new LearningPoll();
		$pollObj->author = $idUser;
		$pollObj->title = $title;
		$pollObj->description = '';
		//these will be needed by learning organization table
		$pollObj->setIdCourse($idCourse);
		$pollObj->setIdParent(0);
		//do actual saving
		if (!$pollObj->save()) {
			throw new CException('Error while saving the survey.');
		}

		return $pollObj;
	}

	public function createSurveyQuestion($id_poll, $type, $title, $sequence = 1, $page = 1){
		$lpq = new LearningPollquest();
		$lpq->id_poll = $id_poll;
		$lpq->id_category = 0;
		$lpq->type_quest = $type;
		$lpq->title_quest = $title;
		$lpq->sequence = $sequence;
		$lpq->page = $page;
		if (!$lpq->save()){
			throw new CException('Error while saving the survey question.');
		}
		return $lpq;
	}

	public function createSurveyQuestionAnswers($idQuestion, $answer, $sequence = 1){
		$lpqa = new LearningPollquestanswer();
		$lpqa->id_quest = $idQuestion;
		$lpqa->answer = $answer;
		$lpqa->sequence = $sequence;
		$lpqa->save();
		if (!$lpqa->save()){
			throw new CException('Error while saving the survey question answer.');
		}
		return $lpqa;
	}

	public function createSurveyLikerScale($id_poll, $scale, $sequence = 1){
		$ar = new LearningPollLikertScale();
		$ar->id_poll = $id_poll;
		$ar->title = $scale;
		$ar->sequence = $sequence;
		if (!$ar->save()) {
			throw new CException('Error while saving the survey likert scale');
		}
		return $ar;
	}

	public function createFakeSurveyResults($idOrg, CoreUser $user, LearningPoll $poll, $answers = array()){
		// Create TestTrack
		$pollTrack = new LearningPolltrack();
		$pollTrack->id_user = $user->idst;
		$pollTrack->id_reference = $idOrg;
		$pollTrack->id_poll = $poll->id_poll;
		if (!$pollTrack->save(false)) {
			throw new CException(Yii::t('error', 'Error while saving poll tracking data'));
		}

		if(!empty($answers)){
			// Let's create track for each question's answers
			foreach($answers as $iKey => $answer){
				// If the needed Data is not present then continue
				if(empty($answer) || !isset($answer['id_quest']) || !isset($answer['id_answer']) || !isset($answer['more_info'])) continue;
				// If we are all good let's create the dummy track
				$ar = new LearningPolltrackAnswer();
				$ar->id_track = $pollTrack->id_track;
				$ar->id_quest = $answer['id_quest'];
				$ar->id_answer = $answer['id_answer'];
				$ar->more_info = $answer['more_info'];
				$ar->save();
			}
		}
	}

	public function createVideo($title = 'Video1', $description = 'AcceptanceTestVideo', $videoFormats = array()){
		$video = new LearningVideo();
		$video->title = $title;
		$video->description = $description;
		$video->video_formats = \CJSON::encode($videoFormats);
		if($video->save()){
			return $video;
		}

		return false;
	}
}
