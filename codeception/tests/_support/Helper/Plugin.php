<?php

namespace Helper;

use PluginManager;

class Plugin extends \Codeception\Module
{
    /**
     * @param $codename
     */
    public function activatePluginByCodeName($codename){
        // Activate the plugin by its code anem
        PluginManager::activateAppByCodename($codename);
        // When the plugin is active load it into the Yii component
        $plugin_name = ucfirst($codename) . 'App';
        PluginManager::loadPlugin($plugin_name, true);
    }
}