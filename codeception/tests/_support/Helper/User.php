<?php
namespace Helper;
// here you can define custom actions
// all public methods declared in helper class will be available in $I

use Yii;
use Docebo;
use CoreUser;
use CoreOrgChartTree;
use Exception;
use CJSON;
use CoreGroupMembers;

class User extends \Codeception\Module
{
	public $defaultPass = 'changeme';
	/**
	 * Returns an array of username and password from the config of DoceboHelper
	 */
	public function getDefaultSuperadminAccount() {
		$doceboModuleConfig = $this->getModule('DoceboHelper')->config;
		return array(
				'username' => $doceboModuleConfig['godadmin_username'],
				'password' => $doceboModuleConfig['godadmin_password']
		);
	}

	/* * Returns an User Object
	 *
	 * @param $userLevel
	 *
	 * @return $u Object
	 */
	public function createUser($userLevel = false, $perm = '*',$firstname = false, $lastname = false){
		$u = new CoreUser('createApi');
		$u->userid = 'AcceptanceTmpUser' . substr(Docebo::randomHash(), 0, 10);
		$u->firstname = ($firstname)? $firstname : 'Acceptance';
		$u->lastname = ($lastname)? $lastname : 'User';
		$u->new_password = $this->defaultPass;
		$u->new_password_repeat = $this->defaultPass;
		$u->email = 'acceptance'.substr(Docebo::randomHash(), 0, 10).'@test.com';

		//add organization chart root node by default
		$_roots = CoreOrgChartTree::model()->roots()->findAll();
		if (!empty($_roots)) {
			$rootNode = $_roots[0];
			$u->chartGroups[] = $rootNode->getPrimaryKey();
		}

		if(!$u->save())
			throw new Exception ("Can't create new user:" . CJSON::encode($u->getErrors()));

		// Keep the user in the proper org_charts
		$u->saveChartGroups();

		$normalUser = Yii::app()->db->createCommand()
			->select('idst')
			->from(\CoreGroup::model()->tableName())
			->where('groupid = :group', array(':group' => $userLevel ? $userLevel : Yii::app()->user->getUserLevellabel()))
			->queryScalar();

		// Keep the user as normal user in the system
		$groupMember = new CoreGroupMembers('create');
		$groupMember->idst = $normalUser; // leaving it hardcoded for now (6 = normal user)
		$groupMember->idstMember = $u->idst;
		$groupMember->save();

		// If the user is PU
		if($userLevel == Yii::app()->user->getAdminLevelLabel()){
			$data = array();
			$permissions = array();
			// If the permissions is different than the sign for all permission, then filter the permissions too
			if($perm != '*'){
				if(is_string($perm))
					$perm = array($perm);

				$permissions = Yii::app()->db->createCommand()
					->select('idst')
					->from(\CoreRole::model()->tableName())
					->where(array('in', 'roleid', $perm))
					->queryColumn();
			}else{
				$permissions = Yii::app()->db->createCommand()
					->select('idst')
					->from(\CoreRole::model()->tableName())
					->queryColumn();

			}
			foreach($permissions as $permission)
				$data[] = array('idst' => $permission,'idstMember' => $u->idst);

			Yii::app()->getDb()->getCommandBuilder()->createMultipleInsertCommand(\CoreRoleMembers::model()->tableName(), $data)->execute();
		}

		return $u;
	}

	public function getDefaultPass(){
		return $this->defaultPass;
	}
}
