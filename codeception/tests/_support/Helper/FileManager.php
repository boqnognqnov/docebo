<?php

namespace Helper;

use Yii;
use Docebo;

class FileManager extends \Codeception\Module
{
    public function getFileContent($filePath){
        $content = file_get_contents($filePath);
        return $content;
    }

    public function getNewlineNumbers($filePath){
        $count = 0;
        $fp = fopen($filePath, 'r');

        while (!feof($fp)) {
            fgets($fp);
            $count++;
        }

        fclose($fp);
        return $count;
    }
}