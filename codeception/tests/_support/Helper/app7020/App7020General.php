<?php

namespace Helper\App7020;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use Yii;
use Docebo;
use CoreUser;
use CorePlugin;
use CoreOrgChartTree;
use Exception;
use CJSON;
use CoreGroupMembers;
use App7020Assets;
use App7020Content;
use App7020ContentPublished;
use App7020Question;
use App7020Invitations;
use App7020Answer;
use App7020Experts;
use App7020Channels;

class App7020General extends \Codeception\Module {
	
	CONST APP_NAME = 'app7020';

	

	/**
	 * Add content 
	 * @param str $userId
	 * @param str $title
	 * @param str $description
	 * @param str $fileName
	 * @param str $orFilename
	 * @param str $contentType
	 * @param str $idSource
	 * @param str $conversion_status
	 */
	
	/**
	 * Add content 
	 * @param int $userId
	 * @param int $conversion_status
	 * @param str $title
	 * @param str $description
	 * @param str $fileName
	 * @param str $orFilename
	 * @param int $contentType
	 * @param int $idSource
	 * @return int - content id
	 */
	public function addContent(
	$userId, $conversion_status = App7020Assets::CONVERSION_STATUS_APPROVED, $title = 'my content title', $description = 'my content description', $fileName = 'video.mpg4', $orFilename = 'originalVideoName.mpg4', $contentType = App7020Assets::CONTENT_TYPE_VIDEO, $idSource = 1
	) {

		$asset = new App7020Assets();
		$asset->title = $title;
		$asset->description = $description;
		$asset->filename = $fileName;
		$asset->originalFilename = $orFilename;
		$asset->contentType = $contentType;
		$asset->idSource = $idSource;
		$asset->conversion_status = $conversion_status;
		$asset->userId = $userId; //Yii::app()->user->getId()
		$asset->save(false);

		// The record to table "app7020_content_published" is made by afterSave() of model App7020Assets
		// BUT userId is empty, so we have to add it
		if ($conversion_status == \App7020Assets::CONVERSION_STATUS_APPROVED) {
			App7020ContentPublished::model()->updateAll(array('idUser' => $userId), "idContent=$asset->id AND actionType=1");
		}
		return $asset->id;
	}

	/**
	 * Make user expert
	 * @param int $userId
	 */
	function makeUserExpert($userId) {

		$expertModel = new App7020Experts();
		$expertModel->idAdmin = $userId;
		$expertModel->idUser = $userId;
		$expertModel->save(false);
	}

	/**
	 * Enable some Plugin
	 * @param str $pluginName
	 * @param str $pluginTitle
	 * @param str $pluginSettings
	 */
	public function enablePlugin($pluginName, $pluginTitle, $pluginSettings = NULL) {
		$plugin = Yii::app()->db->createCommand()
				->select('is_active')
				->from(\CorePlugin::model()->tableName())
				->where('plugin_name = :plugin_name', array(':plugin_name' => $pluginName))
				->queryRow();

		if (empty($plugin)) {
			Yii::app()->db->createCommand()->insert('core_plugin', array(
				'plugin_name' => $pluginName,
				'plugin_title' => $pluginTitle,
				'is_active' => 1,
				'settings' => $pluginSettings
			));
		} elseif ($plugin['is_active'] == 0) {
			Yii::app()->db->createCommand()
					->update(
							'core_plugin', array('is_active' => 1), 'plugin_name = :plugin_name', array(':plugin_name' => $pluginName)
			);
		}
	}

	/**
	 * Enable App7020 plugins
	 */
	public function enableApp7020() {
		$this->enablePlugin('CoachingApp', 'CoachingApp', '[]');
		$this->enablePlugin('SeventyTwentyApp', 'SeventyTwentyApp');
		$this->enablePlugin('Coach7020App', 'CoachingApp', '[]');
		$this->enablePlugin('Share7020App', 'Share7020App', '[]');
	}

	/**
	 * Add Invitations to content
	 * @param int $idContent
	 * @param obj $userHelper
	 * @param bool $yesterday (if you want all invitations to be with yesterday date)
	 */
	public function addInvitations($idContent, $userHelper, $yesterday = false) {
		$coreUserModel = CoreUser::model()->findAll(array('select' => 'idst'));
		// If no users in DB insert one
		if (empty($coreUserModel)) {
			$userHelper->createUser();
			$coreUserModel = CoreUser::model()->findAll(array('select' => 'idst'));
		}
		$yesterdayDate = date("Y-m-j", time() - 60 * 60 * 24) . ' 03:33:33';
		$inviteArr = array();
		$i = 0;
		foreach ($coreUserModel as $user) {
			if ($i == 0) {
				// First user in DB will be Inviter
				$idInviter = $user['idst'];
			}
			if ($yesterday) {
				$inviteArr[] = array('idInvited' => $user['idst'], 'idInviter' => $idInviter, 'idContent' => $idContent, 'created' => $yesterdayDate);
			} else {
				$inviteArr[] = array('idInvited' => $user['idst'], 'idInviter' => $idInviter, 'idContent' => $idContent);
			}
			$i++;
		}
		// Add Invitations to DB
		$builder = Yii::app()->db->schema->commandBuilder;
		$command = $builder->createMultipleInsertCommand(\App7020Invitations::model()->tableName(), $inviteArr);
		$command->execute();
	}

	/**
	 * Delete all invitations
	 */
	public function deleteInvitations() {
		$q = 'DELETE FROM app7020_invitations';
		$cmd = Yii::app()->db->createCommand($q);
		$cmd->execute();
	}
	
	/**
	 * Delete all Not predifined channels
	 */
	public function deleteNotPredifinedChannels() {
		$q = 'DELETE FROM app7020_channels WHERE type = 2';
		$cmd = Yii::app()->db->createCommand($q);
		$cmd->execute();
	}
	
	/**
	 * Close Modal2
	 * @param AcceptanceTester $I
	 */
	public function closeModal($I) {
		$I->waitForElementNotVisible(\Page\Common::$modalHeaderElement . \Page\Common::$modalLoaded, 30);
		$I->waitForElementVisible(\Page\Common::$modalElement . ' ' . \Page\Common::$modalHeaderElement . ' ' . \Page\Common::$modalCloseElementVisible);
		$I->click(\Page\Common::$modalElement . ' ' . \Page\Common::$modalHeaderElement . ' ' . \Page\Common::$modalCloseElementVisible);
		$I->waitForElementNotVisible(\Page\Common::$modalElement, 30);
	}
	
	
	/**
	 * This method is to trigger click, or just to open dropdown menu, generated by widget "DropdownControls"
	 * @param AcceptanceTester $I
	 * @param str $container - container (row if is cList/cGrid view) "dots" to be shown
	 * @param str $clickToAction - on which action to click (ex. 'delete', 'edit', etc.)
	 */
	public function dropDownWidgetRun($I, $container, $clickToAction='') {
		$I->waitForElementVisible($container, 30);
		$I->moveMouseOver($container);
		$I->moveMouseOver($container . ' ' . \Page\App7020\Channels::$dropDownDots);
		$I->wait(1);
		if ($clickToAction) {
			$clickElement = $container . ' .link[data-method='. $clickToAction . ']';
			$I->waitForElementVisible($clickElement, 30);
			$I->moveMouseOver($clickElement);
			$I->wait(1);
			$I->click($clickElement);
			$I->waitForJS("return $.active == 0;", 60);
		}
	}
	
	/**
	 * Get channel info
	 * @param int $idChannel
	 * @param bool $getPreselected
	 * @return array
	 */
	public function getChannelInfo($idChannel, $getPreselected=false) {
		$data['trans'] = App7020Channels::getChannelTranslationsById($idChannel);
		$data['channel'] = App7020Channels::model()->findByPk($idChannel);
		if ($getPreselected) {
			$data['preselected'] = App7020ChannelVisibility::getAxSelectedGroupsAndBranches($idChannel);
		}
		return $data;
	}
	
	/**
	 * Delete all from 'app7020_experts' table
	 * @param type $param
	 */
	public function deleteAllExperts() {
		$q = 'DELETE FROM ' . \App7020Experts::model()->tableName();
		$cmd = Yii::app()->db->createCommand($q);
		$cmd->execute();
	}
	

}
