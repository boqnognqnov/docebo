<?php

namespace Helper\App7020;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use Yii;
use Docebo;
use CoreUser;
use CorePlugin;
use CoreOrgChartTree;
use Exception;
use CJSON;
use CoreGroupMembers;
use App7020Assets;
use App7020Content;
use App7020ContentPublished;
use App7020Question;
use App7020Invitations;
use App7020Answer;
use App7020Experts;


class ManageInvitationsHelper extends App7020General {
	
	/**
	 * By this method through I browse to LMS
	 * @param AcceptanceTester $I
	 * @param int $idUser
	 * @param int $idContent
	 * @param obj $userHelper
	 */
	public function assetJustPublishedBrowse($I, $idUser, $idContent, $userHelper) {
		// Log in
		$I->login(Yii::app()->user->getRelativeUsername($idUser), $userHelper->getDefaultPass());
		// Go to page "guruDashboard/index"
		$guruDashboardRoute = \Page\Common::route(\Page\App7020\GuruDashboard::MAIN_ROUTE, array(), false, App7020General::APP_NAME);
		$I->amOnPage($guruDashboardRoute);
//		// Click on Top nav tab "Assets to review/approve"
		$I->click(\Page\App7020\GuruDashboard::$btnActivityAssetsReview);
//		// Click on tab "Review in progress"
		$I->click(\Page\App7020\GuruDashboard::$tabReviewInProgress);
//		// Click on video title link to be redirected to page "knowledgeLibrary/editItem"
		$I->waitForElementVisible(\Page\App7020\GuruDashboard::getAssetTitleLink($idContent), 60);
		$I->click(\Page\App7020\GuruDashboard::getAssetTitleLink($idContent));
//		// Check if I am on page "knowledgeLibrary/editItem"
		$I->seeCurrentUrlEquals(\Page\Common::route(\Page\App7020\KnowledgeLibrary::ROUTE_EDIT, array('id'=>$idContent), false, App7020General::APP_NAME));
//		// Click to Publish Asset
		$I->click(\Page\App7020\KnowledgeLibrary::$publishAssetSwitch);
//		// Check if Dialog ".modal.app7020-confirm-invite-modal-dialog" is opened
		$I->waitForElementVisible(\Page\App7020\KnowledgeLibrary::$modalInviteBtnClose, 30);
//		// Click in just opened modal on "No Thanks" button
		$I->click(\Page\App7020\KnowledgeLibrary::$modalInviteBtnClose);
//		// Check if I am on page "knowledgeLibrary/item"
		$I->seeCurrentUrlEquals(\Page\Common::route(\Page\App7020\KnowledgeLibrary::ROUTE_ITEM, array('id'=>$idContent), false, App7020General::APP_NAME));
//		// Click on Analytics button
		$I->waitForElementVisible(\Page\App7020\KnowledgeLibrary::$analyticsButton, 30);
		$I->click(\Page\App7020\KnowledgeLibrary::$analyticsButton);
//		// Check if I am on page "analytics/index"
		$I->seeCurrentUrlEquals(\Page\Common::route(\Page\App7020\Analytics::MAIN_ROUTE, array('id'=>$idContent), false, App7020General::APP_NAME));
//		// Check if ComboListView returns empty state
		$I->waitForElementVisible(\Page\App7020\KnowledgeLibrary::$comboListViewEmpty, 30);
//		// Check if Blank state hint is there
		$I->waitForElementVisible(\Page\App7020\Analytics::$blankStateHint, 30);
//		// Log Out before start new scenario
		$I->logout();
	}
	
	/**
	 * By this method through I browse to LMS. The finale page is "analytics/index"
	 * @param AcceptanceTester $I
	 * @param str $userName
	 * @param str $userPass
	 * @param int $idContent
	 */
	public function trackInvitationsForPublishedAssetHelper($I, $userName, $userPass, $idContent) {
//		// Log in
		$I->login($userName, $userPass);
//		// Go to page "knowledgeLibrary/item"
		$I->amOnPage(\Page\Common::route(\Page\App7020\KnowledgeLibrary::ROUTE_ITEM, array('id'=>$idContent), false, App7020General::APP_NAME));
//		// Click on Analytics button
		$I->waitForElementVisible(\Page\App7020\KnowledgeLibrary::$analyticsButton, 30);
		$I->click(\Page\App7020\KnowledgeLibrary::$analyticsButton);
//		// Check if I am on page "analitics/index"
		$I->seeCurrentUrlEquals(\Page\Common::route(\Page\App7020\Analytics::MAIN_ROUTE, array('id'=>$idContent), false, App7020General::APP_NAME));
//		// Check if button "Invite people to watch" is there
		$I->waitForElementVisible(\Page\App7020\Analytics::$btnInvitePeopleToWatch, 30);
//		// Check if statistic "Total Invited People" is there
		$I->waitForElementVisible(\Page\App7020\Analytics::$totalInvitedPeople, 30);
//		// Check if statistic "Global Watch Rate" percentage is there
		$I->waitForElementVisible(\Page\App7020\Analytics::$globalWatchRate, 30);
//		// Check if statistic "Average Reaction time" is there
		$I->waitForElementVisible(\Page\App7020\Analytics::$averageReactionTime, 30);
//		// Check if statistic "Global Invitations breakdown" is there
		$I->waitForElementVisible(\Page\App7020\Analytics::$GlobalInvitationsBreakdown, 30);
//		// Log Out before start new scenario
		$I->logout();
	}
	
	
	/**
	 * By this method through I browse to LMS. The finale page is "analytics/index" and confimr that there are reuslt in GridView table
	 * @param AcceptanceTester $I
	 * @param str $userName
	 * @param str $userPass
	 * @param int $idContent
	 */
	public function manageInvitationsPublishedAssetHelper($I, $userName, $userPass, $idContent, $logout=true) {
//		// Log in
		$I->login($userName, $userPass);
//		// Go to page "analitics/index"
		$I->amOnPage(\Page\Common::route(\Page\App7020\Analytics::MAIN_ROUTE, array('id'=>$idContent), false, App7020General::APP_NAME));
//		// Check if there is content in GridView table
		$I->waitForElementVisible(\Page\App7020\Analytics::$analyticsGridViewTableUserName, 30);
		$I->waitForElementVisible(\Page\App7020\Analytics::$analyticsGridViewTableFullName, 30);
		$I->waitForElementVisible(\Page\App7020\Analytics::$analyticsGridViewTableSentWatched, 30);
		if ($logout) {
			// Log Out before start new scenario
			$I->logout();
		}
	}
	
	/**
	 * By this method through I browse throgh LMS. The finale page is "analytics/index" and confimr that there are reuslt in GridView table. 
	 * I check if after click "Re-sent" button, "SENT" coloumn will be updated with current date (for 1st row only)
	 * @param AcceptanceTester $I
	 * @param str $userName
	 * @param str $userPass
	 * @param int $idContent
	 */
	public function singleResendInvitationPublishedAssetHelper($I, $userName, $userPass, $idContent) {
//		// Browse through LMS, the finale page is "analytics/index" and confimr that there are reuslt in GridView table
		$this->manageInvitationsPublishedAssetHelper($I, $userName, $userPass, $idContent, false);
//		// Click re-invite btn
		$I->click(\Page\App7020\Analytics::$reinviteBtn);
//		// Waiting for confirm modal oppening
		$I->waitForElementVisible(\Page\App7020\Analytics::$btnConfirmModal, 30);
//		// Make checked "confirm" checkbox 
		$I->click(\Page\App7020\Analytics::$checkboxConfirm);
//		// Click in modal on "Confirm" btn
		$I->click(\Page\App7020\Analytics::$btnConfirmModal);
//		// Wait to see if "SENT" will be updated with current date
		$I->waitForText(date("Y-m-j", time()), 30, \Page\App7020\Analytics::$sentFirst);
		// Log Out before start new scenario
		$I->logout();
	}

}
