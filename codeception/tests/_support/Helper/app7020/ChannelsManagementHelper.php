<?php

namespace Helper\App7020;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use Yii;
use Docebo;
use CoreUser;
use CorePlugin;
use CoreOrgChartTree;
use Exception;
use CJSON;
use CoreGroupMembers;
use App7020Assets;
use App7020Content;
use App7020ContentPublished;
use App7020Question;
use App7020Invitations;
use App7020Answer;
use App7020Experts;

class ChannelsManagementHelper extends App7020General {

	private $channelTitle = '';

	/**
	 * Step 1 for creating new channel
	 * @param AcceptanceTester $I
	 */
	public function createNewChannelStep1($I) {
		// Click on "New Channel" button
		$I->waitForElementVisible(\Page\App7020\Channels::$newChannelBtn, 30);
		$I->click(\Page\App7020\Channels::$newChannelBtn);
		//Check if select for choosing language when create new channel is there
		$I->waitForElementVisible(\Page\App7020\Channels::$modalNewChannel . ' ' . \Page\App7020\Channels::$channelSelectLang, 30);
		//Fill in "Channel Title"
		$I->waitForElementVisible(\Page\App7020\Channels::$modalNewChannel . ' ' . \Page\App7020\Channels::$channelTitle, 30);
		$this->channelTitle = 'My channel_' . rand(1, 10000000);
		$I->fillField(\Page\App7020\Channels::$modalNewChannel . ' ' . \Page\App7020\Channels::$channelTitle, $this->channelTitle);
		// Insert some text in "Chanel description" (it's TinyMCE)
		$I->waitForJS("return tinyMCE.activeEditor.setContent('My description');", 30);
		$I->waitForJS("return $('#langArr_en_description').val('My description');", 30);
		// Click on "Next" button to go to the next step
		$I->click(\Page\App7020\Channels::$btnNext);
	}

	/**
	 * Step 2 for creating new channel
	 * @param AcceptanceTester $I
	 */
	public function createNewChannelStep2($I) {
		// Check if "Previous" button works
		$this->createNewChannelGoBack($I);
		// click on Button to show popover with available icons for channel
		$I->waitForElementVisible(\Page\App7020\Channels::$modalNewChannel . ' ' . \Page\App7020\Channels::$channelIconPicker, 30);
		$I->click(\Page\App7020\Channels::$modalNewChannel . ' ' . \Page\App7020\Channels::$channelIconPicker);
		// Select new icon and check if is applyed in the preview
		$I->waitForElementVisible(\Page\App7020\Channels::$iconPickerPopover, 30);
		$I->click(\Page\App7020\Channels::$btnAnchor);
		$I->waitForElementVisible(\Page\App7020\Channels::$previewTile . ' ' . \Page\App7020\Channels::$faAnchor, 30);
		// Select new icon color and backgroun color and check if is applyed in the preview
		$I->fillField(\Page\App7020\Channels::$inputIconColor, '#f12727'); // red color
		$I->waitForElementVisible(\Page\App7020\Channels::$previewIconSetColor, 30);
		$I->fillField(\Page\App7020\Channels::$inputIconBgColor, '#54f22b'); // green color
		$I->waitForElementVisible(\Page\App7020\Channels::$previewIconSetBgColor, 30);
		// Click on "Next" button to go to the next step
		$I->click(\Page\App7020\Channels::$btnNext);
	}

	/**
	 * Step 3 for creating new channel
	 * @param AcceptanceTester $I
	 */
	public function createNewChannelStep3($I) {
		// Check if "Previous" button works
		$this->createNewChannelGoBack($I);
		// Check if needed info is there
		$this->createNewChannelStep3CorectInfo($I);
		// Click on "Next" button to go to the next step
		$I->waitForElementVisible(\Page\App7020\Channels::$btnNextStep3, 30);
		$I->click(\Page\App7020\Channels::$btnNextStep3);
	}

	/**
	 * Step 4 for creating new channel
	 * @param AcceptanceTester $I
	 */
	public function createNewChannelStep4($I) {
		// Check if "Previous" button works
		$this->createNewChannelGoBack($I, true);
		// Check if needed info is there
		$this->createNewChannelStep4CorectInfo($I);
		// Click on "Next" button to go to the next step
		$I->click(\Page\App7020\Channels::$btnNext);
	}

	/**
	 * Step 5 for creating new channel
	 * @param AcceptanceTester $I
	 */
	public function createNewChannelStep5($I) {
		// Check if "Previous" button works
		$this->createNewChannelGoBack($I);
		// Check if "Add a new channel" button is there
		$I->waitForElementVisible(\Page\App7020\Channels::$addNewChannelBtn, 30);
		// Check if "Assign experts" button is there
		$I->waitForElementVisible(\Page\App7020\Channels::$assignExpertsBtn, 30);
		// Check if "Clone" button is there
		$I->waitForElementVisible(\Page\App7020\Channels::$cloneBtn, 30);
		// Check if "Finish" button is there
		$I->waitForElementVisible(\Page\App7020\Channels::$finishBtn, 30);
		// Save all info
		$I->click(\Page\App7020\Channels::$finishBtn);
		// Wait up to 60 seconds for all jQuery AJAX requests to finish
		$I->waitForJS("return $.active == 0;", 60);
		// Check if new channel is addes successfully
		$I->waitForText($this->channelTitle, 30, \Page\App7020\Channels::$lastAddedChannel);
	}

	/**
	 * Clicks to 'Previous' button on every step and go back to current step
	 * @param @param AcceptanceTester $I
	 * @param bool $step3
	 */
	public function createNewChannelGoBack($I, $step3 = false) {
		$I->waitForElementVisible(\Page\App7020\Channels::$previousBtn, 30);
		$I->click(\Page\App7020\Channels::$previousBtn);
		if (!$step3) {
			$I->waitForElementVisible(\Page\App7020\Channels::$btnNext, 30);
			$I->wait(3);
			$I->click(\Page\App7020\Channels::$btnNext);
		} else {
			$I->waitForJS("return $.active == 0;", 60);
			$I->waitForElementVisible(\Page\App7020\Channels::$btnNextStep3, 30);
			$I->wait(3);
			$I->click(\Page\App7020\Channels::$btnNextStep3);
		}
	}

	/**
	 * Check Channel info
	 * @param AcceptanceTester $I
	 * @param str $channelRow
	 * @param str $channelIcon
	 * @param str $isPredefinedChannel
	 * 
	 */
	public function checkChannelsListHelper($I, $channelRow, $channelIcon, $isPredefinedChannel = true) {
		// Check if Channel Icon is there
		$I->waitForElementVisible($channelRow . ' ' . \Page\App7020\Channels::$columnName . ' ' . $channelIcon, 30);
		// Check if Channel "Name" is there
		$I->grabTextFrom($channelRow . ' ' . \Page\App7020\Channels::$columnName);
		// Check if Channel "Visibility" is there
		$I->grabTextFrom($channelRow . ' ' . \Page\App7020\Channels::$columnVisibility);
		if ($isPredefinedChannel) {
			// Check if Channel "Upload Permissions" IS Empty (it have to be)
			$I->waitForJS("return $('" . $channelRow . " " . \Page\App7020\Channels::$columnUploadPermissions . "').text() == '';", 30);
			// Check if Channel "Experts" IS Empty (it have to be)
			$I->waitForJS("return $('" . $channelRow . " " . \Page\App7020\Channels::$columnExperts . "').text() == '';", 30);
		} else {
			// Check if Channel "Upload Permissions" IS Empty (it don't have to be)
			$I->waitForJS("return $('" . $channelRow . " " . \Page\App7020\Channels::$columnUploadPermissions . "').text() != '';", 30);
			// Check if Channel "Experts" IS Empty (it don't have to be)
			$I->waitForJS("return $('" . $channelRow . " " . \Page\App7020\Channels::$columnExperts . "').text() != '';", 30);
		}
		// Check if icon for "Show" column is there
		$I->waitForElementVisible($channelRow . ' ' . \Page\App7020\Channels::$columnShow . ' ' . \Page\App7020\Channels::$showIcon);
		// Open drop down menu
		$I->moveMouseOver($channelRow . ' ' . \Page\App7020\Channels::$dropDownContainer);
		$I->moveMouseOver($channelRow . ' ' . \Page\App7020\Channels::$dropDownContainer . ' ' . \Page\App7020\Channels::$dropDownDots);
		$I->wait(2);
		// Check if "Edit" menu item is available (it have to be)
		$I->waitForElementVisible($channelRow . ' ' . \Page\App7020\Channels::$dropDownContainer . ' ' . \Page\App7020\Channels::$dropDownDots . ' ' . \Page\App7020\Channels::$editMenu, 30);
		if ($isPredefinedChannel) {
			// Check if "Clone" menu item is available (it don't have to be)
			$I->waitForElementNotVisible($channelRow . ' ' . \Page\App7020\Channels::$dropDownContainer . ' ' . \Page\App7020\Channels::$dropDownDots . ' ' . \Page\App7020\Channels::$cloneMenu, 30);
			// Check if "Delete" menu item is available (it don't have to be)
			$I->waitForElementNotVisible($channelRow . ' ' . \Page\App7020\Channels::$dropDownContainer . ' ' . \Page\App7020\Channels::$dropDownDots . ' ' . \Page\App7020\Channels::$deleteMenu, 30);
		} else {
			// Check if "Clone" menu item is available (it have to be)
			$I->waitForElementVisible($channelRow . ' ' . \Page\App7020\Channels::$dropDownContainer . ' ' . \Page\App7020\Channels::$dropDownDots . ' ' . \Page\App7020\Channels::$cloneMenu, 30);
			// Check if "Delete" menu item is available (it have to be)
			$I->waitForElementVisible($channelRow . ' ' . \Page\App7020\Channels::$dropDownContainer . ' ' . \Page\App7020\Channels::$dropDownDots . ' ' . \Page\App7020\Channels::$deleteMenu, 30);
		}
	}

	/**
	 * Step 3 for creating new channel - It check if all needed info is there 
	 * @param AcceptanceTester $I
	 */
	public function createNewChannelStep3CorectInfo($I) {
		// Wait to load main container
		$I->waitForElementVisible(\Page\App7020\Channels::$step3ModalContainer, 30);
		// Check if radio button "All users, groups and branches" is there
		$I->seeElementInDOM(\Page\App7020\Channels::$step3ModalContainer . ' ' . \Page\App7020\Channels::$allUsersGroupsBranches);
		// Check if radio button "Select users, groups and branches" is there
		$I->seeElementInDOM(\Page\App7020\Channels::$step3ModalContainer . ' ' . \Page\App7020\Channels::$selectUsersGroupsBranches);
		//Check if click on "Select users, groups and branches" if additional container for choosing users/branches/groups is there
		$I->waitForElementVisible(\Page\App7020\Channels::$step3ModalContainer . ' ' . \Page\App7020\Channels::$labelSelectUsersGroupsBranches);
		$I->click(\Page\App7020\Channels::$step3ModalContainer . ' ' . \Page\App7020\Channels::$labelSelectUsersGroupsBranches);
		$I->waitForElementVisible(\Page\App7020\Channels::$step3ModalContainer . ' ' . \Page\App7020\Channels::$additionalSelectUsersGroupsBranches, 30);
		// Wait up to 60 seconds for all jQuery AJAX requests to finish
		$I->waitForJS("return $.active == 0;", 60);
	}

	/**
	 * Step 4 for creating new channel - It check if all needed info is there 
	 * @param AcceptanceTester $I
	 */
	public function createNewChannelStep4CorectInfo($I) {
		// Check if radio button "Everyone" is there
		$I->waitForElementVisible(\Page\App7020\Channels::$radioBtnEveryone, 30);
		// Check if radio button "Everyone,with Experts peer review" is there
		$I->waitForElementVisible(\Page\App7020\Channels::$radioBtnEveryoneWithExperts, 30);
		// Check if radio button "Experts only" is there
		$I->waitForElementVisible(\Page\App7020\Channels::$radioBtnExpertsOnly, 30);
	}

	/**
	 * Create new Not predifined channel using LMS interface
	 * @param AcceptanceTester $I
	 */
	public function addNewNotPredifinedChannel($I) {
		// Step 1
		$this->createNewChannelStep1($I);
		// Step 2
		$this->createNewChannelStep2($I);
		// Step 3
		$this->createNewChannelStep3($I);
		// Step 4
		$this->createNewChannelStep4($I);
		// Step 5
		$this->createNewChannelStep5($I);
	}

	/**
	 * Show or Hide Channel
	 * @param AcceptanceTester $I
	 * @param bool $hide
	 */
	public function showOrHideChannel($I, $hide=true) {
		// Wait for loading "Show" icon in 1st Not predifined channel
		$showIcon = \Page\App7020\Channels::$notPredifinedChannelRow . ' ' . \Page\App7020\Channels::$columnShow . ' ' . \Page\App7020\Channels::$showIcon;
		// Click and check if is correct result
		$I->waitForElementVisible($showIcon, 30);
		$I->click($showIcon);
		$I->waitForJS("return $.active == 0;", 60);
		$I->wait(3);
		if ($hide) {
			$I->waitForElementVisible($showIcon . \Page\App7020\Channels::$showIconDisabled, 30);
		} else {
			$I->waitForElementNotVisible($showIcon . \Page\App7020\Channels::$showIconDisabled, 30);
		}
	}

}
