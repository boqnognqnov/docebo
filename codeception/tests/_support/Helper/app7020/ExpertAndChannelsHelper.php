<?php

namespace Helper\App7020;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use Yii;
use Docebo;
use CoreUser;
use CorePlugin;
use CoreOrgChartTree;
use Exception;
use CJSON;
use CoreGroupMembers;
use App7020Assets;
use App7020Content;
use App7020ContentPublished;
use App7020Question;
use App7020Invitations;
use App7020Answer;
use App7020Experts;

class ExpertAndChannelsHelper extends App7020General {

  /**
   * Create new Expert using interface of LMS
   * @param AcceptanceTester $I
   */
	public function createExpert($I, $openModal=true) {
		if ($openModal) {
			// Open modal to make Experts
			$I->waitForElementVisible(\Page\App7020\ExpertsAndChannels::$selectExpersBtn, 30);
			$I->click(\Page\App7020\ExpertsAndChannels::$selectExpersBtn);
		} 
		// Select checkbox of first user in modal
		$firstUserModal = \Page\App7020\ExpertsAndChannels::$tabUsers . ' ' . \Page\App7020\ExpertsAndChannels::$usersTable . ' ' . \Page\App7020\ExpertsAndChannels::$firstUserCheckbox;
		$I->waitForElementVisible($firstUserModal, 30);
		$I->checkOption($firstUserModal);
		// Get info for selected user
		$I->waitForElementVisible(\Page\App7020\Channels::$firstUserFirstName, 30);
		$firstRowGrid = \Page\App7020\ExpertsAndChannels::$tabUsers . ' ' . \Page\App7020\ExpertsAndChannels::$firstRowGrid;
		$selectedUserName = $I->grabTextFrom($firstRowGrid . ' ' . \Page\App7020\ExpertsAndChannels::$userName);
		$selectedFirstName = $I->grabTextFrom($firstRowGrid . ' ' . \Page\App7020\ExpertsAndChannels::$firstName);
		$selectedLastName = $I->grabTextFrom($firstRowGrid . ' ' . \Page\App7020\ExpertsAndChannels::$lastName);
		$selectedEmail = $I->grabTextFrom($firstRowGrid . ' ' . \Page\App7020\ExpertsAndChannels::$email);
		// Click to "Next" button
		$I->waitForElementVisible(\Page\App7020\ExpertsAndChannels::$btnNext, 30);
		$I->click(\Page\App7020\ExpertsAndChannels::$btnNext);
		// Next Step with Progress bar
		$I->waitForJS("return $.active == 0;", 60);
		$I->waitForElementVisible(\Page\App7020\ExpertsAndChannels::$btnCancel, 30);
		$I->click(\Page\App7020\ExpertsAndChannels::$btnCancel);
		$I->waitForJS("return $.active == 0;", 60);
		// Check if user is added and info for this user is OK
		$firstRowGridMainPage = \Page\App7020\ExpertsAndChannels::$gridViewId . ' ' . \Page\App7020\ExpertsAndChannels::$firstRowGrid;
		// Check user name
		$I->waitForText($selectedUserName, 30, $firstRowGridMainPage . ' ' . \Page\App7020\ExpertsAndChannels::$userNameMainPage); 
		// Check EXPERT FULL NAME
		$I->waitForText($selectedFirstName . ' ' . $selectedLastName, 30, $firstRowGridMainPage . ' ' . \Page\App7020\ExpertsAndChannels::$fullNameMainPage); 
		// Check E-MAIL
		$I->waitForText($selectedEmail, 30, $firstRowGridMainPage . ' ' . \Page\App7020\ExpertsAndChannels::$emailMainPage); 
		// Check ASSIGNED CHANNELS
		$I->waitForElementVisible($firstRowGridMainPage . ' ' . \Page\App7020\ExpertsAndChannels::$assignedChannelsMainPage . ' ' . \Page\App7020\ExpertsAndChannels::$counterAC, 30);
		$I->waitForElementVisible($firstRowGridMainPage . ' ' . \Page\App7020\ExpertsAndChannels::$assignedChannelsMainPage . ' ' . \Page\App7020\ExpertsAndChannels::$iconAC, 30);
		// Check Delete
		$I->waitForElementVisible($firstRowGridMainPage . ' ' . \Page\App7020\ExpertsAndChannels::$deleteMainPage . ' ' . \Page\App7020\ExpertsAndChannels::$deleteIcon, 30);
	}
 

}
