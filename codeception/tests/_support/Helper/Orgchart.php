<?php
namespace Helper;

use Yii;
use CoreOrgChartTree;
use CoreOrgChart;
use CoreGroupMembers;
use CoreLangLanguage;
// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Orgchart extends \Codeception\Module
{
	/**
	 * Returns an array of branches with non-empty codes
	 *
	 * @param $parentBranch Id of the parent branch
	 *
	 * @return array
	 */
	public function getBranchesWithNonEmptyCodes($parentBranch = null) {
		$params = array();
		/* @var $cmd \CDbCommand */
		$cmd = Yii::app()->db->createCommand()
				->select("idOrg")
				->from("core_org_chart_tree")
				->where("code IS NOT NULL and code <> ''");

		if($parentBranch) {
			$cmd->andWhere("iLeft <= :iLeft AND iRight >= :iRight");
			$params[':iLeft'] = $parentBranch['iLeft'];
			$params[':iRight'] = $parentBranch['iRight'];
		}

		return $cmd->queryColumn($params);
	}

	public function createNode($code, $title, $parent = null){
		// If there is no passed parent then we assume that the parent is the root
		$parentNode = $parent !== null ? $parent : CoreOrgChartTree::getOrgRootNode();

		$activeLanguagesList = CoreLangLanguage::getActiveLanguages();
		$node = new CoreOrgChartTree();
		$node->code = $code;
		if ($node->validate()) {
			$node->appendTo($parentNode);
			$nodeId = $node->getPrimaryKey();
			// Let's handle with the node translations
			if(is_array($title)){
				foreach ($title as $key => $translation) {
					$translation 				= new CoreOrgChart();
					$translation->id_dir 		= $nodeId;
					$translation->lang_code 	= $key;
					$translation->translation 	= $translation;
					$translation->save();
				}
			}else{
				foreach ($activeLanguagesList as $key => $lang) {
					$translation              = new CoreOrgChart();
					$translation->id_dir      = $nodeId;
					$translation->lang_code   = $key;
					$translation->translation = $title;
					$translation->save();
				}
			}
			return $node;
		}
		return null;
	}

	public function assignUserToNode($userId, $id_org)
	{
		$oct = CoreOrgChartTree::model()->findByPk($id_org);
		$params1 = array($oct->idst_oc,$userId);
		$params2 = array($oct->idst_ocd,$userId);
		Yii::app()->getDb()->getCommandBuilder()
				->createSqlCommand('INSERT IGNORE INTO '.CoreGroupMembers::model()->tableName().' (`idst`, `idstMember`) VALUES ('.implode(',', $params1).")")
				->execute();

		Yii::app()->getDb()->getCommandBuilder()
				->createSqlCommand('INSERT IGNORE INTO '.CoreGroupMembers::model()->tableName().' (`idst`, `idstMember`) VALUES ('.implode(',', $params2).")")
				->execute();
	}
}
