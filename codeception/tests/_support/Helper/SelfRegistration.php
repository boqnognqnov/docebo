<?php
namespace Helper;
// here you can define custom actions
// all public methods declared in helper class will be available in $I

use Yii;
use Settings;

class SelfRegistration extends \Codeception\Module
{
	/* === Registration code types === */
	const REGISTRATION_CODE_TYPE_NONE = 0;
	const REGISTRATION_CODE_TYPE_MANUAL = 'tree_man';
	const REGISTRATION_CODE_TYPE_DROPDOWN = 'tree_drop';
	const REGISTRATION_CODE_TYPE_COURSE = 'tree_course';

	/**
	 * Sets the Advanced Settings -> Registration code usage to "Ask for a tree code chosen by the user from a list in a dropdown menu"
	 */
	public function haveRegistrationCodeWithDropdownSelection() {
		Settings::save('registration_code_type', self::REGISTRATION_CODE_TYPE_DROPDOWN);
	}
}
