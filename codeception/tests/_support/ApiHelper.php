<?php
namespace Codeception\Module;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class ApiHelper extends \Codeception\Module
{
	/**
	 * The json decoded result object
	 */
	private $result = null;
	private static $_accessToken;
	private static $_authenticatedAt;
	private static $_expiresIn;

	/**
	 * Returns the singleton result object
	 */
	private function getResultObject() {
		if(!$this->result) {
			$restModule = $this->getModule('REST'); /* @var $restModule REST */
			$this->result = json_decode($restModule->grabResponse());
		}

		return $this->result;
	}

	/**
	 * Internal function that recursively loads all POST param values
	 * @param $params
	 * @param array $params_to_check
	 * @return array
	 */
	private function _getParamsToCheck($params, &$params_to_check = array()) {
		foreach ($params as $val) {
			if (is_array($val))
				$this->_getParamsToCheck($val, $params_to_check);
			else
				$params_to_check[] = $val;
		}

		return $params_to_check;
	}

	/**
	 * Returns the header for the authentication
	 *
	 * @param $params
	 * @return string
	 */
	private function buildAuthenticationHeader($params)
	{
		$sha1_params = $this->_getParamsToCheck($params);

		\PHPUnit_Framework_Assert::assertArrayHasKey('api_secret', $this->config, 'Config has API secret');
		\PHPUnit_Framework_Assert::assertArrayHasKey('api_key', $this->config, 'Config has API key');

		$sha1 = sha1(implode(',', $sha1_params) . ',' . $this->config['api_secret']); // Encryption of the data
		$code = base64_encode($this->config['api_key'] . ':' . $sha1); // Generation of the code for the call
		return 'Docebo ' . $code;
	}

	/**
	 * Performs lightweight authentication call
	 * @param $url
	 * @param $data_params
	 * @param $content_type
	 *
	 * @return mixed
	 */
	private function call($url, $data_params, $content_type = 'multipart/form-data')
	{
		$curl = curl_init();
		$http_header = array (
			"Content-Type: ".$content_type
		);

		$opt = array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_HTTPHEADER => $http_header,
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => ($content_type == 'multipart/form-data') ? $data_params : http_build_query($data_params),
			CURLOPT_CONNECTTIMEOUT => 5, // Timeout to 5 seconds
		);

		curl_setopt_array($curl, $opt);

		// $output contains the output string
		$output = curl_exec($curl);

		$this->debug($output);

		// it closes the session
		curl_close($curl);

		return $output;
	}

	/**
	 * Performs the strong authentication for APIs
	 * @param $params The params needed to build the header
	 */
	public function doStrongAuthentication($params, $custom_header = 'X-Authorization')
	{
		$restModule = $this->getModule('REST');
		$restModule->haveHttpHeader($custom_header, $this->buildAuthenticationHeader($params));
		$this->result = null;
	}

	/**
	 * @param $params
	 */
	public function doOauth2Authentication($scope = 'api') {
		\PHPUnit_Framework_Assert::assertArrayHasKey('oauth2_consumer_key', $this->config, 'Config has no OAuth2 consumer key');
		\PHPUnit_Framework_Assert::assertArrayHasKey('oauth2_consumer_secret', $this->config, 'Config has no OAuth2 consumer secret');
		\PHPUnit_Framework_Assert::assertArrayHasKey('username', $this->config, 'Config has no API username');
		\PHPUnit_Framework_Assert::assertArrayHasKey('password', $this->config, 'Config has no API password');
        $accessToken = $this->getAccessToken();

		if(!$accessToken){
			\PHPUnit_Framework_Assert::assertArrayHasKey('oauth2_consumer_key', $this->config, 'Config has no OAuth2 consumer key');
			\PHPUnit_Framework_Assert::assertArrayHasKey('oauth2_consumer_secret', $this->config, 'Config has no OAuth2 consumer secret');
			\PHPUnit_Framework_Assert::assertArrayHasKey('username', $this->config, 'Config has no API username');
			\PHPUnit_Framework_Assert::assertArrayHasKey('password', $this->config, 'Config has no API password');

			$arr = $this->call($this->config['oauth2_server_url'].'token', array(
				'client_id' => $this->config['oauth2_consumer_key'],
				'client_secret' => $this->config['oauth2_consumer_secret'],
				'grant_type' => 'password',
				'username' => $this->config['username'],
				'password' => $this->config['password'],
				'scope' => 'api'
			), 'application/x-www-form-urlencoded');

			// Validate results
			\PHPUnit_Framework_Assert::assertJson($arr, "Oauth2 authentication returned an invalid JSON response");
			$arr = json_decode($arr);
			\PHPUnit_Framework_Assert::assertObjectHasAttribute('access_token', $arr, "Oauth2 authentication: no valid token returned");

			//save token
			$accessToken = $arr->access_token;
			$this->setAccessToken($accessToken,$arr->expires_in);
		}

		// Prepare the Bearer API request header
		$restModule = $this->getModule('REST');
		$restModule->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');
		$restModule->haveHttpHeader('Authorization', 'Bearer '.$accessToken);
		$this->result = null;
	}

	/**
	 * Performs the lightweight authentication for APIs
	 * @param $params The params needed to build the header
	 */
	public function doLightweightAuthentication($params)
	{
		// Call authenticate API method
		\PHPUnit_Framework_Assert::assertArrayHasKey('username', $this->config, 'Config has no API username');
		\PHPUnit_Framework_Assert::assertArrayHasKey('password', $this->config, 'Config has no API password');
		$arr = $this->call($this->config['url'].'user/authenticate', array(
			'username' => $this->config['username'],
			'password' => $this->config['password']
		));

		// Validate results
		\PHPUnit_Framework_Assert::assertJson($arr, "Lightweight authentication returned an invalid JSON response");
		$arr = json_decode($arr);
		\PHPUnit_Framework_Assert::assertObjectHasAttribute('token', $arr, "Lightweight authentication: no valid token returned");

		// Prepare the real API request header
		$restModule = $this->getModule('REST');
		$this->result = null;

		return array(
			'key' => $this->config['username'],
			'token' => $arr->token
		);
	}

	/**
	 * Asserts API response is success
	 */
	public function seeApiIsSuccess() {
		$restModule = $this->getModule('REST'); /* @var $restModule REST */
		$restModule->seeResponseIsJson();
		$response = $this->getResultObject();
		\PHPUnit_Framework_Assert::assertNotNull($response, "Response cannot be decoded");
		\PHPUnit_Framework_Assert::assertObjectHasAttribute('success', $response, 'Response has no success property');
		\PHPUnit_Framework_Assert::assertAttributeEquals(true,'success', $response, "Success must be true");
	}

	/**
	 * Asserts API response is failure
	 */
	public function seeApiIsFailure() {
		$restModule = $this->getModule('REST'); /* @var $restModule REST */
		$restModule->seeResponseIsJson();
		$response = $this->getResultObject();
		\PHPUnit_Framework_Assert::assertNotNull($response, "Response cannot be decoded");
		\PHPUnit_Framework_Assert::assertObjectHasAttribute('success', $response, 'Response has no success property');
		\PHPUnit_Framework_Assert::assertAttributeEquals(false,'success', $response, "Success must be false");
	}

	/**
	 * Returns the result object
	 */
	public function getApiResult() {
		$restModule = $this->getModule('REST'); /* @var $restModule REST */
		$restModule->seeResponseIsJson();
		$response = $this->getResultObject();
		return $response;
	}

	/**
	 * Asserts that error message equals the passed value
	 * @param $message mixed The message to check
	 */
	public function seeErrorMessageIs($message) {
		$response = $this->getResultObject();
		\PHPUnit_Framework_Assert::assertObjectHasAttribute('message', $response, 'Response has no message inside');
		\PHPUnit_Framework_Assert::assertTrue($message == $response->message, 'Error message is not equal to "'.$message.'"');
	}

	/**
	 * Checks that response contains
	 * @param $attribute
	 */
	public function seeResponseContainsAttribute($attribute) {
		$response = $this->getResultObject();
		\PHPUnit_Framework_Assert::assertObjectHasAttribute($attribute, $response, 'Response has no "'.$attribute.'" attribute');
	}

	/**
	 * Check attributes is array
	 * @param $attribute
	 */
	public function seeResponseAttributeIsArray($attribute) {
		$response = $this->getResultObject();
		\PHPUnit_Framework_Assert::assertObjectHasAttribute($attribute, $response, 'Response has no "'.$attribute.'" attribute');
		\PHPUnit_Framework_Assert::assertTrue(is_array($response->$attribute), 'Attribute "'.$attribute.'" is not an array');
	}

	private function getAccessToken(){
		if(!self::$_accessToken){
			codecept_debug('No token yet, request a new one');
			return null;
		}

		//request a new token,  before the current token expires (currently that is 5 minutes before current token expires)
		//this might need to be changed if the oauth server config is changed ( currently the token expiration is 3600s in AccessToken class )
		$tokenExpirationTime = self::$_authenticatedAt + self::$_expiresIn - 60*5;
		if($tokenExpirationTime < time()){
			codecept_debug('token is about to expire');
			return null;
		}

		return self::$_accessToken;
	}

	private function setAccessToken($token,$expiresIn){
		self::$_accessToken = $token;
		self::$_authenticatedAt = time();
		self::$_expiresIn = $expiresIn;
	}
}