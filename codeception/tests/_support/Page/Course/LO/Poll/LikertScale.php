<?php

namespace Page\Course\LO\Poll;

use Page\Course\LO\Survey;
use Yii;

class LikertScale extends Survey
{
    // Common
    public static $question_title_textarea_id = 'question-text';
    public static $delete_modal = 'div.bootbox.modal.fade.in';
    public static $confirm_delete_modal = 'div.bootbox.modal.fade.in div.modal-footer a.big.green';

    public static $save_changes = 'div.form-actions input.big.green';

    // Management
    // #############################################################################################################
    // Questions elements
    public static $question_table = 'table#questions-table';
    public static $add_new_question_input = 'input#addQuestionInput';
    public static $add_new_question_button = 'a.add-question-button';

    public static $question_edit_button_template = '//tr[.//td[text()="{text}"]]//a[contains(@id,"update-question")]';
    public static $question_edit_field = 'input#questionEditInput';
    public static $question_edit_save_button = 'input#questionEditInput + input[type="button"]';

    // Scales elements
    public static $scale_table = 'table#scales-table';
    public static $add_new_scale_input = 'input#addScaleInput';
    public static $add_new_scale_button = 'a.add-scale-button';

    public static $scale_edit_button_template = '//tr[.//td[text()="{text}"]]//a[contains(@id,"update-scale")]';
    public static $scale_edit_field = 'input#scaleEditInput';
    public static $scale_edit_save_button = 'input#scaleEditInput + input[type="button"]';
    public static $scale_delete_button_template = '//tr[.//td[text()="{text}"]]//a[contains(@id,"delete-scale")]';
    // #############################################################################################################

    // Player
    // #############################################################################################################
    public static $play_scale_question_block = 'div[data-question-index="{n}"] + div.answers-container';
    public static $play_scale_question_row_tmp = 'div[data-question-index="{n}"] + div.answers-container table.playLikertScale tbody tr';
    public static $play_scale_checkbox_template = 'input[id="question-{q}-{r}-{s}"]';
    public static $play_scale_submit_form = 'button#test-show-result';
    public static $play_scale_completed = 'div.l-testpoll.review';
    // #############################################################################################################

    public function addQuestionTitle($title){
        $this->acceptanceTester->fillTinyMceEditorById(self::$question_title_textarea_id, $title);
    }

    public function addNewQuestion($items){
        if(!is_array($items)){
            $items = array($items);
        }

        foreach($items as $item){
            $this->acceptanceTester->fillField(self::$add_new_question_input, $item);
            $this->acceptanceTester->click(self::$add_new_question_button);
        }
    }

    public function editQuestionItem($oldValue, $newValue){
        $button = str_replace('{text}' , $oldValue, self::$question_edit_button_template);
        $this->acceptanceTester->click($button);

        $this->acceptanceTester->fillField(self::$question_edit_field, $newValue);
        $this->acceptanceTester->click(self::$question_edit_save_button);
    }

    public function addNewLikertScale($items){
        if(!is_array($items)){
            $items = array($items);
        }

        foreach($items as $item){
            $this->acceptanceTester->fillField(self::$add_new_scale_input, $item);
            $this->acceptanceTester->click(self::$add_new_scale_button);
        }
    }

    public function editScaleItem($oldValue, $newValue){
        $button = str_replace('{text}' , $oldValue, self::$scale_edit_button_template);
        $this->acceptanceTester->click($button);

        $this->acceptanceTester->fillField(self::$scale_edit_field, $newValue);
        $this->acceptanceTester->click(self::$scale_edit_save_button);
    }

    public function deleteScaleItem($items){
        if(!is_array($items)){
            $items = array($items);
        }

        foreach($items as $item){
            $delete = str_replace('{text}' , $item, self::$scale_delete_button_template);
            $this->acceptanceTester->openModal($delete, self::$delete_modal);
            $this->acceptanceTester->click(self::$confirm_delete_modal);
            $this->acceptanceTester->wait(1);
        }
    }

    public function checkRowQuestionScales($idQuestion, $n){
        $row = str_replace('{n}' , $idQuestion, self::$play_scale_question_row_tmp);
        $this->acceptanceTester->seeNumberOfElements($row, $n);
    }

    public function checkQuestionContainScales($idQuestion, $scales){
        if(!is_array($scales)){
            $scales = array($scales);
        }
        $question = str_replace('{n}' , $idQuestion, self::$play_scale_question_block);
        foreach($scales as $scale){
            $this->acceptanceTester->see($scale,$question);
        }
    }

    public function checkOnScaleQuestion($question, $row, $scale){
        $input = str_replace(array('{q}','{r}','{s}') , array($question, $row, $scale), self::$play_scale_checkbox_template);
        $this->acceptanceTester->checkStyledRadio($input);
    }
}