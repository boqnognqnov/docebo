<?php
namespace Page\Course\LO;

use Page\Common;
use Yii;
use Docebo;

class Survey extends Common
{
    CONST TYPE_LIKERT_SCALE = 'likert_scale';

    public static $URL = 'poll/default/edit';

    public static $params = array();

    // Survey
    public static $pollTypes = 'div.dropdown-docebo.dropdown.pull-right > a';

    public static $templateType = 'div.dropdown-docebo.dropdown.pull-right > a+ul a[href*="question_type={type}"]';

    public static $poll_questions_table = 'table#poll-editor-question-list';

    public static $poll_questions_table_loading = 'div#poll-editor-question-list_processing';

    public static $poll_question_table_edit_button_template = '//tr[.//td[text()="{text}"]]//a[contains(@id,"question-update")]';


    public static function route($action = false, $params = array(), $overrideDefaultParameters = false, $app = false){
        //merge parameters with default params if no override, otherwise use only $params
        if(!$overrideDefaultParameters){
            $params = array_merge($params, static::$params);
        }

        if($action) {
            if (!$app) {
                return Yii::app()->createUrl($action, $params);
            } else {
                return Docebo::createAppUrl($app.':'.$action, $params);
            }

        }

        return Yii::app()->createUrl(static::$URL, $params);
    }
}