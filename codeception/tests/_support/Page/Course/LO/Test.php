<?php
namespace Page\Course\LO;

use Page\Common;
use Yii;

class Test extends Common
{

    CONST TYPE_FITB = 'fill_in_the_blank';

    // include url of current page
    public static $URL = 'test/default/edit';

    public static $testTypes = 'div.dropdown-docebo.dropdown.pull-right > a';

    public static $templateType = 'div.dropdown-docebo.dropdown.pull-right > a+ul a[href*="question_type={type}"]';

    public static $test_questions_table = 'table#test-editor-question-list';

    public static $test_questions_table_row = 'table#test-editor-question-list tbody tr[id*="questions-list"]';

    public static $test_questions_table_edit_first_row = 'table#test-editor-question-list tbody tr[id*="questions-list"] a[id*="question-update"]';

    public static function __callStatic($method, $arguments) {
        if (preg_match('/^create[A-Z][\w]*Url$/', $method)) {
            $appName = lcfirst(preg_replace(array('/^create/', '/Url$/'), '', $method));
            return Yii::app()->createUrl('test/default/' . $appName, $arguments[0]);
        }
    }
}
?>