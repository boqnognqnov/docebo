<?php

namespace Page\Course\LO\Test;


use Page\Common;
use Yii;

class Question extends Common
{
    CONST TYPE_FITB = 'fill_in_the_blank';

    // Question TYPE FITB
    public static $fitb_answers_input = 'input.maininput';

    public static $fitb_answers_select = 'select#new_correct_answers';

    public static $fitb_answers_case_sensitive = 'input#answers_are_case_sensitive';

    public static $fitb_answers_count_column = 'table#answers-table tbody tr';
    // Question TYPE FITB

    // Normal Selectors on Question/Create page
    public static $question_title_textarea_id = 'question-text';

    public static $question_save_changes = 'input[name="save"]';

    public static $no_answers = 'div#no-answers-placeholder';

    public static $consider_correct_answer = 'input#consider_correct_if_all_answers_are_correct';

    public static $add_answer = 'div#add-answer-link-container > a#toggle-add-answer';

    public static $add_answer_sub_form = 'div#form-box-container';

    public static $new_answer_score_correct = 'input#new-answer-score_correct';

    public static $new_answer_score_incorrect = 'input#new-answer-score_incorrect';

    public static $new_answer_comment = 'textarea#new-answer-comment';

    public static $new_answer_add = 'input#add-answer-button';

    public static $new_answer_undo = 'input#undo-add-answer-button';

    public static $answers_table_row = 'table#answers-table tbody tr';

    public static $answers_list_edit_first_answer = 'a[id*="update-answer-1"]';

    public static $answers_list_delete_first_answer = 'a[id*="delete-answer-1"]';

    public static $answer_modal = 'div.bootbox.modal.fade.in';

    public static $answer_modal_cancel = 'div.bootbox a.btn-docebo.black';

    //EDIT ANSWER
    public static $mod_answer_add = 'input#mod-answer-button';

    public static $mod_answer_undo = 'input#undo-edit-answer-button';

    // DELETE ANSWER

    public static $answer_delete_confirm = 'div.bootbox a.btn-docebo.green';

    public static function __callStatic($method, $arguments) {
        if (preg_match('/^create[A-Z][\w]*Url$/', $method)) {
            $appName = lcfirst(preg_replace(array('/^create/', '/Url$/'), '', $method));
            return Yii::app()->createUrl('test/question/' . $appName, $arguments[0]);
        }
    }


}
