<?php
/**
 * Created by PhpStorm.
 * User: NikVas
 * Date: 24-Feb-16
 * Time: 11:47
 */

namespace Page;


class Dashboard extends Common
{
    /* ---------------- PAGE ---------------- */
    public static $dashboardLayoutsManagementPage = 'lms/index.php?r=mydashboard/dash/admin';
    public static $dashboardLayoutManagementPageById = 'lms/index.php?r=mydashboard/dash/adminDashlets&id={layoutId}';
    /* --------------- ELEMENT -------------- */
    public static $dashboardLayoutAddCellBtn = 'a.add-dashlet-button';
    public static $listOfDashletTypes = 'ul.dashlet-type-boxes.thumbnails';
    public static $rewardsShopDashletBtn = 'ul.dashlet-type-boxes.thumbnails li.dl-type-box[data-dashlet-handler="plugin.GamificationApp.widgets.DashletRewardShop"]';
    public static $rewardsShopDashletBtnDescription = 'ul.dashlet-type-boxes.thumbnails li.dl-type-box[data-dashlet-handler="plugin.GamificationApp.widgets.DashletRewardShop"] p.dl-type-description';
    public static $dahletSelectionNextBtn = 'a.btn.btn-docebo.jwizard-nav';
    public static $rewardsShopDashletModalTitle = 'div.modal.create-dashlet-wizard div.modal-header h3';
    public static $rewardsShopDashletModalTitleInput = 'div#create-dashlet-wizard.modal-body.opened input#dashlet_instance_title';
    public static $rewardsShopDashletModalMaxItemsInput = 'div#create-dashlet-wizard.modal-body.opened input#max_items';
    public static $rewardsShopDashletModalRewardHistoryCheckbox = 'div#create-dashlet-wizard.modal-body.opened input#show_reward_history[type="checkbox"]';
    public static $rewardsShopDashletModalRewardShopLinkCheckbox = 'div#create-dashlet-wizard.modal-body.opened input#show_go_to_reward_shop[type="checkbox"]';
    public static $rewardsShopDashletModalConfirmBtn = 'div.modal.create-dashlet-wizard div.modal-footer a.btn.btn-docebo.jwizard-nav.jwizard-finish';
    public static $rewardsShopDashletInLayout = 'li.dashlet.plugin\.GamificationApp\.widgets\.DashletRewardShop';

}