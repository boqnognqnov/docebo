<?php
namespace Page\apps\Transcripts;

use Page\Common;
use Yii;
use Docebo;

class TranscriptsManagement extends Common
{
    public static $URL = 'TranscriptsApp/TranscriptsManagement/activitiesList';
    public static $APP = 'admin';

    public static $main_actions = 'div.main-actions';

    public static $main_actions_info = 'div.main-actions div.info';

    public static $additional_fields_management_button = 'div.main-actions li a.additional-fields';

    public static $assign_user_input = 'input#username-input';
    public static $newActivityButton = '.activities-main-actions-container.main-actions #create-new-activity';
    public static $manageCoursesInstitutesButton = '.activities-main-actions-container.main-actions #manage-courses-and-institues-button';

    // modal elements for new training activity
    public static $newActivityModalSelectInstitute = '.modal-body #edit-activity-form select#TranscriptsRecord_training_institute';
    public static $newActivityModalSelectCourse = '.modal-body #edit-activity-form select#TranscriptsRecord_course_name';

    // modal element when course and institute is not required, but can be entered freely by the user
    public static $newActivityModalInputInstitute = '.modal-body #edit-activity-form input#TranscriptsRecord_training_institute';
    public static $newActivityModalInputCourse = '.modal-body #edit-activity-form input#TranscriptsRecord_course_name';

    public static function __callStatic($method, $arguments) {
        if (preg_match('/^create[A-Z][\w]*Url$/', $method)) {
            $appName = lcfirst(preg_replace(array('/^create/', '/Url$/'), '', $method));
            return Docebo::createAdminUrl('/TranscriptsApp/TranscriptsManagement/' .$appName, isset($arguments[0]) ? $arguments[0] : array() );
        }
    }
}