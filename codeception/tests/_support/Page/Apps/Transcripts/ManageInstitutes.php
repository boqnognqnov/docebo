<?php
namespace Page\Apps\Transcripts;

class ManageInstitutes extends \Page\Common
{
	public static $URL = 'TranscriptsApp/TranscriptsManagement/manageInstitutes';
	public static $params = array();

	public static $createNewInstituteButton = 'div.activities-main-actions-container.main-actions #create-new-institute';
	public static $instituteNameField = 'div.modal-body div.form.transcripts-edit-institute input#TranscriptsInstitute_institute_name';
	public static $confirmButton = 'div.modal-footer a.btn.btn-docebo.big.institute_confirm_button';
	public static $backButton = 'div.main-actions .back-button a';

}
