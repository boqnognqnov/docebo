<?php
namespace Page\Apps\Transcripts;

class TranscriptsSettings extends \Page\Common
{
	public static $URL = 'TranscriptsApp/TranscriptsApp/settings';
	public static $params = array();


	public static $checkboxRequireUserSelectTrainingInstitute = 'div#external-training-settings #TranscriptsAppSettingsForm_transcripts_require_defined_list';
	public static $checkboxAllowUsersToAddActivity = 'div#external-training-settings #TranscriptsAppSettingsForm_transcripts_allow_users';
	public static $confirmChangesButton = 'input[type="submit"].btn.btn-docebo.big';

}
