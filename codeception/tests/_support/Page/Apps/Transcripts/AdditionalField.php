<?php
namespace Page\apps\Transcripts;

use Page\Common;
use Yii;
use Docebo;
use AcceptanceTester;
use WebDriverKeys;

class AdditionalField extends TranscriptsManagement
{

    public static $add_new_field = 'input.create-additional-field';

    public static $main_actions_info = 'div.main-actions div.info';

    public static $additional_fields_management_button = 'div.main-actions li a.additional-fields';

    public static $additional_fields_types_dropdown = 'select#field-type-select';

    public static $language_list = 'select#TranscriptsFieldTranslation_lang_code';

    public static $default_language_input = 'input.default_lang';

    public static $modal_confirm = 'div.modal-footer a.confirm-btn';

    public static $fields_table = 'div.grid-view table.items tbody';

    public static $fields_table_loading = 'div.grid-view.grid-view-loading';

    public static $fields_table_row_template = 'div.grid-view table.items tbody tr:nth-child({n})';

    public static $fields_table_last_edit_button = 'div.grid-view table.items tbody tr:last-child a.edit-field';

    public static $fields_table_last_delete_button = 'div.grid-view table.items tbody tr:last-child a.delete-action';

    public static $fields_table_move_button_template = 'div.grid-view table.items tbody tr:nth-child({n}) a.order-field';

    public static $filed_type_dropdown_entry_template = 'div#field-options-inputs-{lang} > input:nth-child({n})';

    public static $search_in_grid_input = 'input#search-additional-fields-grid';

    // Delete Additional field Checkbox confirm
    public static $delete_field_checkbox = 'div.delete-message input#confirm';

    public static $delete_field_confirm_button = 'div.modal-footer a.btn.green.yes';

    public static function __callStatic($method, $arguments) {
        if (preg_match('/^create[A-Z][\w]*Url$/', $method)) {
            $appName = lcfirst(preg_replace(array('/^create/', '/Url$/'), '', $method));
            return Docebo::createAdminUrl('/TranscriptsApp/additionalFields/' .$appName, isset($arguments[0]) ? $arguments[0] : array() );
        }
    }

    public static function waitForModalToCloseAndGirdToLoad(AcceptanceTester $I){
        // Wait till the modal disappear
        $I->waitForElementNotVisible(self::$modalHeaderElement);
        $I->waitForElementNotVisible(self::$fields_table_loading);
    }

    public static function filterTable(AcceptanceTester $I, $filter){
        // Let's filter the table results to show the NEW date filed element
        $I->executeJS("$('" . self::$search_in_grid_input ."').val('".$filter."')");
        $I->pressKey(self::$search_in_grid_input, WebDriverKeys::ENTER);
        // Wait till the grid loads
        self::waitForModalToCloseAndGirdToLoad($I);
    }
}