<?php
namespace Page\Apps\Transcripts;

class ManageCoursesAndInstitutes extends \Page\Common
{
	public static $URL = 'TranscriptsApp/TranscriptsManagement/manageCoursesAndInstitutes';
	public static $params = array();

	public static $createNewCourseButton = 'div.activities-main-actions-container.main-actions #edit-course';
	public static $manageInstitutesButton = 'div.activities-main-actions-container.main-actions #manage-institues-button';
	public static $courseNameField = 'div.modal-body form#new-course-form #TranscriptsCourse_course_name';
	public static $confirmButtonModal = 'div.modal-footer a.btn.btn-docebo.big';
	public static $assignInstituteLink = 'table.items a.institutesLink';
	public static $assignInstituteDropdownVisible = array('css' => 'table.items select.institutesLinkSelect');
	public static $searchFieldGrid = '#advanced-search-transcripts-by-text';
	public static $loadingGrid = 'div#transcripts-course-management-grid.grid-view-loading';
}
