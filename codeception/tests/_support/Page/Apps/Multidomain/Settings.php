<?php
namespace Page\Apps\Multidomain;

use Docebo;

class Settings extends \Page\Common
{
    public static $URL = 'MultidomainApp/main/settings';

    public static $tab_tmp = 'ul.nav-tabs li a[data-target*="{tab}"]';

    public static $enable_self_registration_settings = '#multidomain-self-registration-form input[id*="enable_custom_settings"]';

    public static $self_registration_settings_disabled = '#multidomain-self-registration-form div.blockable div.blockUI';

    public static $self_registration_registration_type_tmp = "#multidomain-self-registration-form input#CoreMultidomain_register_type_{n}";

    public static $self_registration_allowed_domains = 'allowed_domains';

    public static $self_registration_allowed_domains_disabled = '#multidomain-self-registration-form div.allowed_domains_container div.blockUI';

    public static $save_changes_button = "#multidomain-self-registration-form div.text-right input.green.big";

    public function saveChanges(){
        $this->acceptanceTester->click(self::$save_changes_button);
    }

    public static function route($action = false, $params = array(), $overrideDefaultParameters = false){
        //merge parameters with default params if no override, otherwise use only $params
        if(!$overrideDefaultParameters){
            $params = array_merge($params, static::$params);
        }

        if($action)
            return Docebo::createAdminUrl($action, $params);

        return Docebo::createAdminUrl(static::$URL, $params);
    }
}