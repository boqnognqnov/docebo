<?php
namespace Page\Apps\Multidomain;

use Docebo;

class GlobalSettings extends \Page\Common
{
    public static $URL = 'MultidomainApp/main/mainSettings';

    public static $loginRestrictionCheckbox = 'div.mainMultidomainSettings span#user_login_check-styler';
    public static $confirmChangesButton = 'div.mainMultidomainSettings input[name="save_multidomain_main_settings"]';
}