<?php
namespace Page\Player;

use Page\Common;

class Report extends Common
{
    public static $user_statistics_tab = 'div#report-navigation div:nth-child(1)';

    public static $training_materials_tab = 'div#report-navigation div:nth-child(2)';

    public static $assignments_tab = 'div#report-navigation div:nth-child(3)';

    // This is FOR LO TEST
    public static $tm_lo_grid_element_tmpl = 'div#report-objects-grid div.gridItemsContainer a[onclick*="{attr}"]';
    // This is FOR LO SURVEY
    public static $tm_lo_grid_element_tmpl_survey = 'div#report-objects-grid div.gridItemsContainer a[href*="{attr}"]';

    public static $player_reports_test_users_table = 'div#player-reports-test-users';

    public static $player_reports_test_users_table_row_tmpl = 'div#player-reports-test-users table.items tbody tr:nth-child({n})';

    public static $user_statistic_row_tmpl = 'div#player-reports a[href*="{attr}"]';

    public static $user_player_objects_row_tmp = 'div#player-objects a[href*="organizationId={org}"]';

    public static $user_modal_test_results = 'div.fancybox-opened';

    public static $user_modal_view_answers = 'div.review-answers';

    // Survey Liker Scale Question
    public static $player_report_survey_likert_scale_table = 'table.playLikertScale';
    public static $player_report_survey_likert_scale_table_row = 'table.playLikertScale tbody tr.id-{question}';
    public static $player_report_survey_likert_scale_table_row_scale = 'table.playLikertScale tbody tr.id-{question} td.id-{scale}';

}