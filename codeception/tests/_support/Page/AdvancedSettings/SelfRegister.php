<?php

namespace Page\AdvancedSettings;

class SelfRegister extends \Page\AdvancedSettings
{
    public static $params = array(
        '#' => 'register',
    );

    public static $registration_type_tmp = 'input#AdvancedSettingsRegisterForm_register_type_{n}';

    public static $allowed_domains_dom_id = 'allowed_domains';

    public static $allowed_domains_first_element = 'div.allowed_domains_container li.bit-box';

    public static $allowed_domains_blocked = 'div.allowed_domains_container div.blockUI';

    public static $save_settings = 'div.actions.right-buttons input.btn-save';

    public function saveSettings(){
        $this->acceptanceTester->click(self::$save_settings);
    }
}