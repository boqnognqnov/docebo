<?php
namespace Page;

/**
 * Class SideMenu
 *
 * Used to mainly map elements that are part of the side menu
 */
class SideMenu {

	/* ===================== MAP ==================== */
	public static $sidebar = '.sidebar-menu';
	public static $menu = '.menu';
	public static $menuAdmin = '.sidebar-tile-admin';
	public static $menuUser = '.tile.default';
	public static $menuCloseBtn = 'div#close-bar #close-button';

	// Gamification=====
	public static $menuRewards = 'div.menu div#gamification a[href="/admin/index.php?r=GamificationApp/Reward/index"]';
	//=================

	/* ================== TEMPLATES ================= */
	//used to find the proper menu to hover over for the {href} that is provided that is in sub menu
	private static $menuXpathSelectorTemplate = '//*[@id="menu"]//li[.//*[@class="item"]//a[@href="{href}"]]';
	private static $menuLinkSelectorTemplate = '.item a[href="{href}"]';

	public static $openedSecondMenu = '.second-menu.open';

	/* ========= PAGE SPECIFIC FUNCTIONS =========== */
	public static function getMenuSelector($url){
		$xpath = str_replace('{href}', $url, self::$menuXpathSelectorTemplate);
		return array(
			'xpath' => $xpath
		);
	}

	public static function getMenuLinkSelector($url){
		$css = str_replace('{href}', $url, self::$openedSecondMenu . ' ' . self::$menuLinkSelectorTemplate);
		return array(
			'css' => $css,
		);
	}
}
