<?php

namespace Page;

use Docebo;
use Yii;
use AcceptanceTester;

class ReportManagement extends Common
{
    CONST CONTROLLER = "/reportManagement";

    public static $new_custom_report_button = 'a[rel="custom-report-wizard"]';

    public static $new_custom_report_filter_name = 'input#filter_name';

    public static $new_custom_report_wizard_page_user_selector = 'div.modal.users-selector';

    public static $new_custom_report_wizard_page_user_selector_select_all_users = 'div#users a.select-all';

    public static $new_custom_report_wizard_field_page = 'div.modal.report-wizard-step-4';


    // EXTERNAL TRAINING REPORT FIELDS
    public static $report_ext_training_field_template = 'input#external_trainings_{id}';


    // REPORT LAST PAGE
    public static $new_custom_report_wizard_last_page_schedule = 'input[name="next_button"]';

    public static $new_custom_report_wizard_last_page_saveNShow = 'input[name="finish_wizard_show"]';

    public static $new_custom_report_wizard_last_page_saveNBack = 'input[name="finish_wizard_list"]';

    // PREVIEW CUSTOM REPORT
    public static $view_report_grid = 'div#custom-report-grid';

    public static function route($page, $params = array()){
        return Docebo::createAdminUrl(self::CONTROLLER . '/' . $page, $params);
    }

    public static function newReportFillStep1(AcceptanceTester $I, $type, $name){
        $title = Yii::app()->db->createCommand()
            ->select('title')
            ->from(\LearningReportType::model()->tableName())
            ->where('id=:id')
            ->queryScalar(array(':id' => $type));

        $element = $I->executeJs("return $('label:contains(\"".Yii::t('report', $title)."\")').find('input').attr('id')");

        $I->checkStyledRadio('input#'.$element);

        $I->fillField(self::$new_custom_report_filter_name, $name);
    }


    public static function wizardNextStep(AcceptanceTester $I){
        $I->click(".modal a.btn.big.next-page");
    }
}