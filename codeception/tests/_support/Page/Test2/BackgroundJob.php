<?php
namespace Page\Test2;
use Page\Common;

class BackgroundJob extends Common
{

	// include url of current page
	public static $URL = '/test2/background';

	public static $headerJobNotificationElement = "#header .logout-holder>span#defaultJob";
	public static $headerJobNotificationElementIcon = "#header .logout-holder>span#defaultJob .fa.fa-bell.fa-lg";
	public static $headerJobNotificationElementCounter = "#header .logout-holder>span#defaultJob .count";
	public static $headerJobNotificationElementTooltip = "#header .logout-holder>span#defaultJob .unread-tooltip.text-left";
	public static $headerProgressDiv = "#header .logout-holder>span#defaultJob .unread-tooltip.text-left #jobsContainer .progressDiv";
	public static $headerNotifyLink = "#header .logout-holder>span#defaultJob .unread-tooltip.text-left #jobsContainer .progressDiv .infoDiv .notifyLink";
	public static $headerNotificationSubscribed = "#header .logout-holder>span#defaultJob .unread-tooltip.text-left #jobsContainer .progressDiv .infoDiv .notificationSubscribed";
	public static $headerJobPercent = "#header .logout-holder>span#defaultJob .unread-tooltip.text-left #jobsContainer .progressDiv .percentElement";
	public static $headerJobErrors = "#header .logout-holder>span#defaultJob .unread-tooltip.text-left #jobsContainer .progressDiv .errorElement";
	public static $headerJobErrorsLink = "#header .logout-holder>span#defaultJob .unread-tooltip.text-left #jobsContainer .progressDiv .errorElement a";
	public static $headerNotificationStopLink = "#header .logout-holder>span#defaultJob .unread-tooltip.text-left #jobsContainer .progressDiv .stopJob";
	public static $headerNotificationViewAllLink = "#header .logout-holder>span#defaultJob .unread-tooltip.text-left #jobsContainer .view-all";


}
