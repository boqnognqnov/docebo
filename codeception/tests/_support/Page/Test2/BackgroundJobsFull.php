<?php
namespace Page\Test2;

class BackgroundJobsFull extends BackgroundJob
{
	// include url of current page
	public static $URL = '/test2/viewAllJobs';

	public static $pageTabPaneElements = "#maincontent #content #backgroundJobList ul.nav.nav-tabs";
	public static $pageTabPaneElementsFirstTab = "#maincontent #content #backgroundJobList ul.nav.nav-tabs li:first-of-type";
	public static $pageTabPaneElementsSecondTab = "#maincontent #content #backgroundJobList ul.nav.nav-tabs li:nth-of-type(2)";
	public static $pageSingleJobContainer = "#maincontent #content #backgroundJobList .progressDiv";
	public static $pageNotifyLink = "#maincontent #content #backgroundJobList .progressDiv .notifyLink";
	public static $pageNotificationSubscribed = "#maincontent #content #backgroundJobList .progressDiv .notificationSubscribed";
	public static $pageJobPercent = "#maincontent #content #backgroundJobList .progressDiv .percentElement";
	public static $pageNotificationStopLink = "#maincontent #content #backgroundJobList .progressDiv .stopJob";
	public static $pageJobErrors = "#maincontent #content #backgroundJobList .progressDiv .errorElement";
	public static $pageJobErrorsLink = "#maincontent #content #backgroundJobList .progressDiv .errorElement a";
}
