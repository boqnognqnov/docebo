<?php
namespace Page;

use Docebo;

class LocalizationTool extends \Page\Common
{
	public static $URL = 'langManagement/index';

	//overridden because of admin url
	public static function route($action = false, $params = array(), $overrideDefaultParameters = false){
		//merge parameters with default params if no override, otherwise use only $params
		if(!$overrideDefaultParameters){
			$params = array_merge($params, static::$params);
		}

		if($action)
			return Docebo::createAdminUrl($action, $params);

		return Docebo::createAdminUrl(static::$URL, $params);
	}
}
