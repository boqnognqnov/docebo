<?php

namespace Page;

use Page\Common;
use Docebo;
use AcceptanceTester;

//TODO: move to Profile folder (jordan)
class MyActivities extends Common
{
    public static $URL = 'myActivities/index';
    public static $params = array();

    public static $tab_external_activities = 'external-activities';

    public static $new_activity = 'a#create-new-activity';

    public static $modal_edit_activity = 'div.modal-edit-activity';

    public static $tabs_template = 'a[data-tab="{tab}"]';

    public static $date_additional_field_template = 'input#date_{id}';

    public static $additional_field_template = '#Transcripts_additional_{id}_value';

    public static $confirm_activity_modal = 'div.modal-footer a.btn.green.big';

    public static $external_training_course_name = 'input#TranscriptsRecord_course_name';

    public static $external_training_to_date = 'input#to-date-input';

    public static $external_training_table_last_row = 'div#transcripts-management-grid table tbody tr:last-child';

    public static $external_training_table_edit_last_row = 'div#transcripts-management-grid table tbody tr:last-child td.edit-column > a';

    public static $tabExternalTrainingLink = 'div.myactivities-sidebar a.external-activities';

    public static function __callStatic($method, $arguments) {
        if (preg_match('/^create[A-Z][\w]*Url$/', $method)) {
            $appName = lcfirst(preg_replace(array('/^create/', '/Url$/'), '', $method));
            return Docebo::createLmsUrl('/myActivities/' .$appName, isset($arguments[0]) ? $arguments[0] : array() );
        }
    }


    public static function waitForModalToCloseAndGirdToLoad(AcceptanceTester $I){
        // Wait till the modal disappear
        $I->waitForElementNotVisible(self::$modalHeaderElement);
        $I->waitForElementNotVisible(self::$table_loading);
    }
}