<?php
namespace Page;

use Docebo;
use Yii;
use AcceptanceTester;

class Login extends Common
{
    public static $URL = '/';

    public static $register_button = 'a[data-dialog-class="register"]';

    public static $register_form_username = "div.modal.register input#username";

    public static $register_form_email = "div.modal.register input#email";

    public static $register_form_password = "div.modal.register input#password";

    public static $register_form_re_password = "div.modal.register input#password_retype";

    public static $register_form_accept_checkbox = "div.modal.register input#privacy";

    public static $register_form_confirm_button = '.modal.register input[name="register_button"]';

    public static $register_form_successful_registration = ".modal.register div.registration-success";
}