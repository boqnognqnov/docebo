<?php
namespace Page;

class Home extends Common
{
	// include url of current page
	public static $URL = '/';

	public static $formElement = "form[action$='site/login']";
	public static $usernameField = 'login_userid';
	public static $passwordField = 'login_pwd';
	public static $forgotPasswordElement = "a[href*=axLostdata]";

	//lost password
	public static $modalLostPasswordForm = '#lostpwd-form';
	public static $modalUsernameField = 'username';

	//self register
	public static $modalSelfRegisterOpenLink = "[data-dialog-class=register]";
	public static $registrationCodesDropdow = "select#reg_code";

	/**
	 * @param string $username
	 * @param string $password
	 * @param string $goToHomePage - if true, should redirect to original home page - without multidomain for example, use FALSE, if you are in multidomain environment
	 */
	public function login($username, $password, $goToHomePage = true) {
		$I = $this->acceptanceTester;

		if($goToHomePage)
			$I->amOnPage(self::$URL);

		$I->submitForm(self::$formElement, array(
			self::$usernameField => $username,
			self::$passwordField => $password,
		));

		$I->seeElement(Common::$loginSuccessElement);
	}

	/**
	 * @param string $username
	 * @param string $password
	 * @param string $goToHomePage - if true, should redirect to original home page - without multidomain for example, use FALSE, if you are in multidomain environment
	 */
	public function loginFail($username, $password, $goToHomePage = true) {
		$I = $this->acceptanceTester;

		if($goToHomePage)
			$I->amOnPage(self::$URL);

		$I->submitForm(self::$formElement, array(
			self::$usernameField => $username,
			self::$passwordField => $password,
		));

		$I->dontSeeElement(Common::$loginSuccessElement);
	}
}
