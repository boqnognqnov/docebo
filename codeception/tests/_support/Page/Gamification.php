<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 3.2.2016 г.
 * Time: 15:28
 */

namespace Page;


class Gamification extends Common{
    /* ---------------- PAGE ---------------- */
    public static $rewardsHomePage = 'admin/index.php?r=GamificationApp/Reward/index';
    public static $rewardsEditPage = 'admin/index.php?r=GamificationApp/Reward/edit';
    public static $rewardsSetHomePage = 'admin/index.php?r=GamificationApp/RewardsSet/index';
    public static $rewardsSetAssignedRewardsPage = '#\/admin\/index\.php\?r\=GamificationApp\/RewardsSet\/assignRewards\&id\=(\d+)#';
    public static $rewardsRequestsPage = 'admin/index.php?r=GamificationApp/Reward/requestsManagement';

    /* ------------------- GRIDS -------------------------- */
    public static $rewardsSetGridViewRow = '#rewards-set-grid table tbody tr td';
    public static $rewardsSetAssignRewardsGrid = '#assign-rewards-management-grid';

    /* ----------------- AUTOCOMPLETE AND SEARCH STUFF ----------------- */
    public static $autocompleteDropdown = '.typeahead.dropdown-menu';
    public static $rewardsSetSearchField = 'input#advanced-search-rewards-set';

    /* ----------------- MODALS ------------------ */
    public static $newRewardsSetModal = '.modal.modal-new-set';
    public static $editRewardsSetModal = '.modal.modal-edit-set';
    public static $deleteRewardsSetModal = '.modal.modal-delete-set';
    public static $usersSelectorModal = '.modal.users-selector';
    public static $rewardsSetAssignRewardsModal = '.modal.modal-assign-rewards-to-set';
    public static $rewardsSetUnassignRewardModal = '.modal.modal-delete-set-reward';
    public static $rewardsSetUnassignRewardsModal = '.modal.modal-delete-set-reward';
    public static $rewardsSendMessageModal = '.modal.modal-reward-send-message';

    /* ----------------- GAMIFICATION SETTINGS ------------------ */
    public static $rewardsSettingEnableShop = 'span#enable_reward_shop-styler';//'input#enable_reward_shop';
    public static $rewardsSettingEnableShopInput = 'input#enable_reward_shop';
    public static $settingShowNotAwarded = 'span#show_not_awarded-styler';//'input#show_not_awarded';
    public static $settingShowNotAwardedInput = 'input#show_not_awarded';

    /* --------------- SELECTORS ----------------------- */
    public static $rewardsSetSuccessPage = 'h3.rewards-set-title';
    public static $rewardsSetAssignedRewardsCount = 'div#rewards-set-grid td.assigned-rewards span.assign-rewards-count';
    public static $rewardsSetAssignedRewardListItem = '#associated-sets-management-list div.combo-list-item';
    public static $rewardsSetAssignedRewardListView = '#associated-sets-management-list';
    public static $rewardsSetAssignRewardsBtn = '.reward-set-assignment-actions .assign-rewards-to-set';
    public static $rewardsSetUnassignMassAction = '#combo-list-view-container-associated-sets-management-list select option[value="delete"]';
    public static $rewardsSetUnassignMassActionDropDown = '#combo-list-view-container-associated-sets-management-list select';
    public static $learnerMenuItem = 'ul#menu li div.tile a[href="#home"]';
    public static $rewardShopLink = 'ul#menu li div.gamification-app-user-menu a#new-user-menu-view-reward-shop';
    public static $myCoinsCounter = 'ul#menu li div.gamification-app-user-menu #my-coins-counter';
    public static $viewAllBadgesPoints = 'ul#menu li div.profile-info-container a#new-user-menu-view-all';
    public static $rewardsTab = 'ul.nav.nav-tabs.advanced-sidebar li a[href="#mybadges-tab-rewards"]';
    public static $rewardsTabRow = 'div.gamification-mybadges-tabs.tabbable div#my-rewards-grid tr td';

    /* --------------- SELECTORS Rewards Management ----------------------- */
    public static $rewardsManagementTableNameTD = '#rewards-grid table.items td.reward-name';
    public static $rewardsManagementAutocomplete = 'input#advanced-search-reward';
    public static $rewardsRequestsManagementBtn = 'li#manage_rewards_requests a';
    public static $rewardsRequestsTableUserNameTD = '#rewards-requests-grid table.items td.user-fullname';
    public static $rewardsRequestsTableRewardNameTD = '#rewards-requests-grid table.items td.reward-name';
    public static $rewardsRequestsFilterAll     = 'span#show-all-requests-styler';
    public static $rewardsRequestsFilterPending = 'span#show-pending-requests-styler';
    public static $rewardsRequestsFilterApproved = 'span#show-approved-requests-styler';
    public static $rewardsRequestsFilterRejected = 'span#show-rejected-requests-styler';
    public static $rewardsRequestsAutocomplete = 'input#advanced-search-rewards-requests';
    public static $rewardsRequestsEmailDialog = 'div.modal.modal-send-email';
    public static $rewardsRequestsEmailDialogMessage = 'reward-request-email-body';
    public static $rewardsRequestsEmailDialogSendBtn = 'a.btn.btn-docebo.big.green.confirm';
    public static $rewardsRequestsUserRewardStatusXPath = '//div[@id="rewards-requests-grid"]//table[contains(@class,"items")]//tr[td[contains(@class,"user-fullname")]="{userName}" and td[contains(@class,"reward-name")]="{rewardName}" and td[contains(@class,"request-status")]//div[contains(@class,"{requestStatus}")]]';
    public static $rewardsRequestsUserRewardSendEmailXPath = '//div[@id="rewards-requests-grid"]//table[contains(@class,"items")]//tr[td[contains(@class,"user-fullname")]="{userName}" and td[contains(@class,"reward-name")]="{rewardName}" and td[contains(@class,"request-email")]]//a[contains(@class,"send-email")]';
    public static $rewardsRequestsUserRewardApproveXPath = '//div[@id="rewards-requests-grid"]//table[contains(@class,"items")]//tr[td[contains(@class,"user-fullname")]="{userName}" and td[contains(@class,"reward-name")]="{rewardName}"]//td[contains(@class,"requests-actions-td")]//i[contains(@class,"action-approve")]';
    public static $rewardsRequestsUserRewardRejectXPath = '//div[@id="rewards-requests-grid"]//table[contains(@class,"items")]//tr[td[contains(@class,"user-fullname")]="{userName}" and td[contains(@class,"reward-name")]="{rewardName}"]//td[contains(@class,"requests-actions-td")]//i[contains(@class,"action-reject")]';

    /* ------------------- Reward Shop ---------------------------------- */
    public static $rewardShowWidget = '.w-rewards-shop';
    public static $rewardShopUserCoins = '.usercoins span strong';
    public static $rewardShopSetSelector = '.rewards-sets-selector .item';
    public static $rewardShopSearchButton = "//a[@id='search-link']";
    public static $rewardShopSearchField = ['name' => 'search_input'];
    public static $rewardShopFilterButton = "//a[@id='filter-button']";

}