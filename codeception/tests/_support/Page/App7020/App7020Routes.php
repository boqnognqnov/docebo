<?php

namespace Page\App7020;



class App7020Routes extends \Page\Common {
	
	// Index page of "Assets"
	CONST ROUTE_INDEX = 'assets/index';
	CONST ROUTE_ASK_EXPERT = 'askTheExpert/index';
	CONST ROUTE_ASSET = 'site/index#/assets/view/';
	CONST ROUTE_ASSET_EDIT = 'site/index#/assets/peerReview/';
}