<?php

namespace Page\App7020;



class Channels extends \Page\Common {
	
	// Index page of "Channel"
	CONST ROUTE_INDEX = 'channelsManagement/index';
	// "Expert" page of "Channel" controller
	CONST ROUTE_EXPERTS = 'channels/experts';
	
	// Selectors on INDEX Page ******** START ***************
	// "New Channel" button
	public static $newChannelBtn = '#app7020ChannelsContainer #openNewChannelDialog';
	
	// *** Selectors in modal for creating "New channel" *** START ****
	// Modal 
	public static $modalNewChannel = '.modal.app7020-edit-channel-dialog';
	// Select for choosing language 
	public static $channelSelectLang = '#channelLangSelect';
	// Step 1: Channel title
	public static $channelTitle = 'input#channelTitle';
	// Step 1: hidden field for Channel description in English
	public static $channelDescription = 'input[type=hidden]#langArr_en_description';
	// Button "Next" for steps 1, 2, 4  btn save-dialog
	public static $btnNext = '.btn.save-dialog';
	// Button "Next" for step 3
	public static $btnNextStep3 = 'a.btn.user-selector-confirm.btn-docebo.green.big';
	// Button "Previous"
	public static $previousBtn = "a.btn.prev-button";
	// Step 2: Button to show popover with available icons for channel
	public static $channelIconPicker = '#channelIconPicker';
	// Step 2: popover with icons
	public static $iconPickerPopover = '.iconpicker-popover';
	// Step 2: button with value "fa-anchor]" in popover for choosing icon
	public static $btnAnchor = "button[value=fa-anchor]";
	// Step 2: Preview of title
	public static $previewTile = '.preview-tile';
	// Step 2: icon "fa-anchor"
	public static $faAnchor = 'i.fa-anchor';
	// Step 2: input field for icon color
	public static $inputIconColor = '#color';
	// Step 2: preview icon 'fa-anchor' with set red color
	public static $previewIconSetColor = '.preview-tile i.fa-anchor[style="color: rgb(241, 39, 39);"]';
	// Step 2: input field for icon background color
	public static $inputIconBgColor = '#background-color';
	// Step 2: preview icon 'fa-anchor' with set green background color
	public static $previewIconSetBgColor = '.preview-tile[style="background-color: rgb(84, 242, 43);"]';
	// Step 3: modal content container
	public static $step3ModalContainer = '#users-selector-container';
	// Step 3: radio button "All users, groups and branches" !!NB It is hidden element
	public static $allUsersGroupsBranches = 'input#public[type=radio]';
	// Step 3: radio button "Select users, groups and branches" !!NB It is hidden element
	public static $selectUsersGroupsBranches = 'input#selection[type=radio]';
	// Step 3: LABEL for radio button "Select users, groups and branches"
	public static $labelSelectUsersGroupsBranches = 'label[for=selection]';
	// Step 3: additional container for choosing users/branches/groups
	public static $additionalSelectUsersGroupsBranches = '.tabbable.main-selector-area';
	// Step 4: radio button "Everyone"
	public static $radioBtnEveryone = 'label[for=permissions_0]';
	// Step 4: radio button "Everyone,with Experts peer review"
	public static $radioBtnEveryoneWithExperts = 'label[for=permissions_1]';
	// Step 4: radio button "Experts only"
	public static $radioBtnExpertsOnly = 'label[for=permissions_2]';
	// Step 5: "Add a new channel" button
	public static $addNewChannelBtn = 'input[name=save_new]';
	// Step 5: "Assign experts" button
	public static $assignExpertsBtn = 'input[name=save_assign]';
	// Step 5: "Clone" button
	public static $cloneBtn = 'input[name=save_clone]';
	// Step 5: "Finish" button
	public static $finishBtn = 'a.btn.btn-docebo.green.big';
	// Step 5: Last added channel
	public static $lastAddedChannel = '#channelsGridView table.items tr:last-child td.channel_name';
	// *** Selectors in modal for creating "New channel" *** END ****
	
	// *** Selectors in modal "Delete channel" *** START ****
	// Modal container
	public static $deleteModal = '.modal.app7020-delete-channel-dialog';
	// Statistic container
	public static $deleteStatisticContainer = '.designChannelInner';
	// Statistic "Assets assigned to this channel only"
	public static $deleteStatisticLeft = '.left';
	// Statistic "Assets assigned also to other channels"
	public static $deleteStatisticRight = '.right';
	// Checkbox "Yes I want to proceed!"
	public static $proceedCheckbox = 'label[for=confirmDelete]';
	// *** Selectors in modal "Delete channel" *** END ****
	
	// Selectors for "Channel" page - GridView *** START ***
	// Column "Channel Name"
	public static $columnName = 'td.channel_name';
	// Column "Channel Visibility"
	public static $columnVisibility = 'td.channel_visibility';
	// Link "Channel Visibility"
	public static $visibilityLink = 'td.channel_visibility span';
	// Column "Upload permissions"
	public static $columnUploadPermissions = 'td.channel_upload_permissions';
	// Link :"Upload permissions"
	public static $UploadPermissionsLink = 'td.channel_upload_permissions span';
	// Column "Experts"
	public static $columnExperts = 'td.channel_experts';
	// Icon "Experts"
	public static $expertsIcon = '.fa-users';
	// Column "Show"
	public static $columnShow = 'td.channel_show';
	// Icon for column "Show"
	public static $showIcon = 'i.fa-check-circle-o';
	// Icon for column "Show" Disabled class
	public static $showIconDisabled = '.disabled';
	// Channel Id data attr
	public static $channelidDataAttr = 'data-channel-id';
	// Column Drag&Drop
	public static $columnDragAndDrop = 'td.channel_dnd';
	// Icon Drag&Drop
	public static $dragAndDropIcon = 'i.fa-arrows';
	// Last row of grid
	public static $gridLastRow = '#channelsGridView table.items tr:last-of-type';
	// Row grid
	public static $gridRow = '#channelsGridView table.items tr';
	// GridView ID
	public static $gridViewId = '#channelsGridView';

	// Widget "DropDownMenu" *** START ***
	// Main container
	public static $dropDownContainer = 'td.channel_dropdownMenu';
	// Dots
	public static $dropDownDots = '.awesomeIcon-dots';
	// "Edit" menu item
	public static $editMenu = '.link[data-method=edit]';
	// "Clone" menu item
	public static $cloneMenu = '.link[data-method=clone]';
	// "Delete" menu item
	public static $deleteMenu = '.link[data-method=delete]';
	// Widget "DropDownMenu" *** END ***


	// Channel "Invitations" - table row 
	public static $invitationsRow = '#channelsGridView table.items tr:first-child';
	// Channel "Invitations" - icon
	public static $invitationsIcon = '.fa-user-plus';
	// Channel "My courses and learning plans" - table row
	public static $myCoursesRow = '#channelsGridView table.items tr:nth-child(2)';
	// Channel "My courses and learning plans" - icon
	public static $myCoursesIcon = '.fa-book';
	// Channel "My active courses and learning plans" - table row
	public static $myActiveCoursesRow = '#channelsGridView table.items tr:nth-child(3)';
	// Channel "My active courses and learning plans" - icon
	public static $myActiveCoursesIcon = '.fa-play-circle-o';
	// Channel "Continue watching and learning" - table row
	public static $continueWatchingRow = '#channelsGridView table.items tr:nth-child(4)';
	// Channel "Continue watching and learning" - icon
	public static $continueWatchingIcon = '.fa-clock-o';
	// Channel "Watch Later" - table row
	public static $watchLaterRow = '#channelsGridView table.items tr:nth-child(5)';
	// Channel "Watch Later" - icon
	public static $watchLaterIcon = '.fa-history';
	// Channel "Recent Contribution" - table row
	public static $recentContributionRow = '#channelsGridView table.items tr:nth-child(6)';
	// Channel "Recent Contribution" - icon
	public static $recentContributionIcon = '.fa-list';
	// 1st Not predifined channel - table row 
	public static $notPredifinedChannelRow = '#channelsGridView table.items tr:nth-child(7)';
	// 3d Not predifined channel - table row 
	public static $thirdNotPredifinedChannelRow = '#channelsGridView table.items tr:nth-child(9)';
	// Channel "Not predifined channel" - icon
	public static $notPredifinedChannelIcon = '.fa';
	// Selectors for "Channel" page - GridView *** END ***
	// Selectors on INDEX Page ******** END ***************
	
	
	
	// Selectors on EXPERTS Page ******** START ***************
	// Experts Page main container
	public static $expertsMainContainer = '#expertsManagement';
	// On page GridView id
	public static $expertsPgGridViewId = '#expertsComboListView';
	// "Assign Experts" button
	public static $assignExpertsBtnExpertPage = 'a[data-dialog-id=app7020-assign-experts-dialog]';
	// Dialog2 "Assign experts"
	// Dialog2 ** start ***
	public static $assignExpertsDialog = '.modal.app7020-assign-experts-dialog';
	// Tab "Users" in dialog2
	public static $tabUsers = '#users';
	// User's table
	public static $usersTable = 'table.items';
	// Checkbox of first user
	public static $firstUserCheckbox = 'tr.odd:first-of-type input[type=checkbox]';
	// first name of 1st user
	public static $firstUserFirstName = 'tr.odd:first-of-type td:nth-of-type(3)';
	// last name of 1st user
	public static $firstUserLastName = 'tr.odd:first-of-type td:nth-of-type(4)';
	// last name of 1st user
	public static $firstUserEmail = 'tr.odd:first-of-type td:nth-of-type(5)';
	// Dialog2 ** end ***
	// Selectors on EXPERTS Page ******** END *****************
	

}
