<?php

namespace Page\App7020;

use Docebo;
use Yii;
use AcceptanceTester;

class Analytics extends \Page\Common {

	CONST MAIN_ROUTE = "analytics/index";
	// Analytics Main Container
	public static $analyticsMainContainer = '#analytics_manageInvitations';
	// Blank state hint
	public static $blankStateHint = '#analytics_manageInvitations .app7020-hint-userInvitationGridBlankState.app7020-hint.show';
	// Table for result (cGridView)
	public static $analyticsGridViewTable = '#analytics_manageInvitations .inivitationsGridView .userman-users-grid-wrapper table.items';
	// Table for result (cGridView) column "User name"
	public static $analyticsGridViewTableUserName = '#analytics_manageInvitations .inivitationsGridView .userman-users-grid-wrapper table.items .td-username';
	// Table for result (cGridView) column "Full name"
	public static $analyticsGridViewTableFullName = '#analytics_manageInvitations .inivitationsGridView .userman-users-grid-wrapper table.items .td-fullname';
	// Table for result (cGridView) columns "SENT" and "WATCHED"
	public static $analyticsGridViewTableSentWatched = '#analytics_manageInvitations .inivitationsGridView .userman-users-grid-wrapper table.items .td-email';
	// Button "Invite people to watch"
	public static $btnInvitePeopleToWatch = '#analytics_manageInvitations .invitePeopleSection #showInviteToWatch';
	// statistic "Total Invited People"
	public static $totalInvitedPeople = '#analytics_manageInvitations #manageInvitationStatistic .totalInvitedPeople';
	// statistic "Global Watch Rate" percentage
	public static $globalWatchRate = '#analytics_manageInvitations #manageInvitationStatistic .globalWatchRate';
	// statistic "Average Reaction time" 
	public static $averageReactionTime = '#analytics_manageInvitations #manageInvitationStatistic .avgReactionTime';
	// statistic "Global Invitations breakdown"
	public static $GlobalInvitationsBreakdown = '#analytics_manageInvitations #manageInvitationStatistic .globalInvitationsBreakdown';
	// re-invite btn
	public static $reinviteBtn = '#analytics_manageInvitations .inivitationsGridView .userman-users-grid-wrapper table.items tr:nth-child(1) td.td-reinvite a';
	// confirm modal button
	public static $btnConfirmModal = '.app7020-confirm-invitation-dialog .modal-footer .btn.save-dialog';
	// Checkbox "confirm" (for modal)
	public static $checkboxConfirm = '.app7020-confirm-invitation-dialog label[for=confirm]';
	// ComboGridView table of results cloumn "SENT" - first row
	public static $sentFirst = '#analytics_manageInvitations .inivitationsGridView .userman-users-grid-wrapper table.items tr:nth-child(1) td:nth-child(3)';


}
