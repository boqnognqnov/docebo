<?php

namespace Page\App7020;

use Docebo;
use Yii;
use AcceptanceTester;

class GuruDashboard extends \Page\Common {
	
	CONST MAIN_ROUTE = "guruDashboard/index";
	// Button in top nav tab "Assets to review/approve"
	public static $btnActivityAssetsReview = "a[href='#activityAssetsReview']";
	// tab "Review in progress"
	public static $tabReviewInProgress = '#activityAssetsReview #app7020TabsPlus ul.app7020BootstrapTabsExtended li:nth-child(2)';

	/**
	 * Get Asset Title Link
	 * @param int $idContent
	 * @return string
	 */
	public static function getAssetTitleLink($idContent) {
		$linktoEditItem = "#combo-list-view-container-reviewAssetsComboListView a[href$='/app7020/index.php?r=knowledgeLibrary/editItem&id=" . $idContent . "']";
		return $linktoEditItem;
	}


}
