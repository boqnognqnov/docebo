<?php

namespace Page\App7020;

use Docebo;
use Yii;
use AcceptanceTester;

class KnowledgeLibrary extends \Page\Common {
	
	CONST ROUTE_ITEM = "knowledgeLibrary/item";
	CONST ROUTE_EDIT = "knowledgeLibrary/editItem";
	// Switch to publish Asset
	public static $publishAssetSwitch = '#App7020KLeditPanel #switchPublishUnpublish .published';
	// Modal "Invite" button "Close" ("No, thanks")
	public static $modalInviteBtnClose = '.modal.app7020-confirm-invite-modal-dialog .modal-footer .close-dialog';
	// Analytics button
	public static $analyticsButton = '#knowledgeLibrarySingle #App7020KLinfoPanel.readToPublish .tools .fa-bar-chart';
	// ComboListView empty state
	public static $comboListViewEmpty = '#analytics_manageInvitations .inivitationsGridView .userman-users-grid-wrapper table.items td.empty';


}
