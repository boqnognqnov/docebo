<?php

namespace Page\App7020;



class ExpertsAndChannels extends \Page\Common {
	
	// Index page of "Experts&Channels"
	CONST ROUTE_INDEX = 'expertsChannels/index';
	// Page 'Channels' of "Experts&Channels"
	CONST ROUTE_CHANNELS = 'expertsChannels/channels';
	
	// Main page selectors *** START ****
	// Hint for empty GridView - "Select users and make them Experts!"
	public static $hintStep1 = '.app7020-experts-dialog-action-step1';
	// Hint for empty GridView - "Select Experts and assign them to one or more channels"
	public static $hintStep2 = '.app7020-experts-dialog-action-step2';
	// GridView in main page
	public static $gridViewId = '#userman-users-management-grid';
	// GridView table in main page
	public static $gridViewTable = '.items';
	// First row of GridView table in main page
	public static $firstRowGrid = '.items tbody tr:first-of-type';
	// Grid view - empty class
	public static $gridEmpty = 'td:first-of-type.empty';
	// USERNAME cell in grid
	public static $userNameMainPage = '.td-username';
	// EXPERT FULL NAME cell in grid
	public static $fullNameMainPage = '.td-expertnames';
	//E-MAIL cell in grid
	public static $emailMainPage = '.td-email';
	//ASSIGNED CHANNELS cell in grid
	public static $assignedChannelsMainPage = '.td-cchannels';
	// Expert's channel link
	public static $expertChannelLink = '.app7020_expert_topics';
	// Data attribute to get 1st expert id in grid
	public static $expertIdDataAttr = 'data-expert-id';
	// counter in ASSIGNED CHANNELS cell in grid
	public static $counterAC = '.count';
	// TV icon in ASSIGNED CHANNELS cell in grid
	public static $iconAC = '.fa-tv';
	// Button "Select Experts"
	public static $selectExpersBtn = '#selectExperts';
	//'Delete' (last table cell) cell in grid
	public static $deleteMainPage = '.td-cdelete';
	// Delete icon 
	public static $deleteIcon = '.delete';
	// "Multiple assign experts to channels" button
	public static $multiAssignExperts = '#multipleAssign';


	// Main page selectors *** END ****
	
	// Dialogs "Select Experts"
	// Tab "Users" in dialog2
	public static $tabUsers = '#users';
	// User's table
	public static $usersTable = 'table.items';
	// Checkbox of first user
	public static $firstUserCheckbox = 'tr.odd:first-of-type input[type=checkbox]';
	// User name cell
	public static $userName = 'td:nth-of-type(2)';
	// User name cell
	public static $firstName = 'td:nth-of-type(3)';
	// Last name cell
	public static $lastName = 'td:nth-of-type(4)';
	// Email cell
	public static $email = 'td:nth-of-type(5)';
	// Button next
	public static $btnNext = 'a.user-selector-confirm';
	// Button "Cancel"
	public static $btnCancel = 'a.close-dialog';
	
	// Dialog "Multiple Assign Experts" 
	// Dialog Id
	public static $dialogMultiAssignExpertsId = '#app7020-multipleAssign-dialog';
	// Empty Channels container
	public static $emptyChannels = '.app7020-dialogEmptyChannels';
	// "Create Channel" button AND "Select Experts"
	public static $createChannelBtn = '.action a';
	// Empty Experts container
	public static $emptyExperts = '.app7020-dialogEmptyExperts';
	// First channel checkbox
	public static $firstChannelCheckbox = '.items tbody tr:first-of-type input[type=checkbox]';
	// Next button
	public static $nextBtn = 'a.save-dialog';
	// Close button
	public static $closeBtn = 'a.close-dialog';
	
	// Selector of page "expertsChannels/channels"
	// Delete control for channel
	public static $deleteChannel = '.singleListItem .customControls .fa-times';
	// Checkbox "Confirm deletion"
	public static $confirmDeletion = 'input[type=checkbox]#confirm';
	// Empty grid
	public static $channelsEmptyGrid = '.items .empty';

}
