<?php

namespace Page;


use Page\Common;
use Yii;

class Player extends Common
{
    // include url of current page
    public static $play = 'a#common-launchpad-play-object';

    public static $launchpad = 'div.player-launchpad-content';

    public static $player_arena = 'div.player-arena-test-inline';

    public static $begin_test = 'input#begin';

    public static $test_play_container = 'div.test-play-container';

    public static $test_results = 'button#test-show-result';

    public static $review_score = 'div.review-scores';

    public static function __callStatic($method, $arguments) {
        if (preg_match('/^create[A-Z][\w]*Url$/', $method)) {
            $appName = lcfirst(preg_replace(array('/^create/', '/Url$/'), '', $method));
            return Yii::app()->createUrl('player/' .$appName, $arguments[0]);
        }
    }
}