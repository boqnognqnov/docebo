<?php

namespace Page;

use Docebo;
class AdvancedSettings extends \Page\Common
{
    public static $URL = 'advancedSettings/index';

    public static $params = array(
        '#' => 'register',
    );

    // Self Registration


    public static $tab_self_registration = "div.advanced-sidebar ul li a[data-tab='register']";

    public static function route($action = false, $params = array(), $overrideDefaultParameters = false){
        //merge parameters with default params if no override, otherwise use only $params
        if(!$overrideDefaultParameters){
            $params = array_merge($params, static::$params);
        }

        if($action)
            return Docebo::createAdminUrl($action, $params);

        return Docebo::createAdminUrl(static::$URL, $params);
    }
}