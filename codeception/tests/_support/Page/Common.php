<?php
namespace Page;


use Yii;
use \Docebo;

/**
 * Class Common
 * @package Page
 *
 * ==================================================================================
 * This Page class is used to describe the selector for common elements between pages
 * ==================================================================================
 */
class Common
{
	//any params that the page might have by default
	//example: My Courses page has "opt=fullmycourses"
	//which would translate to array('opt'=>'fullmycourses')
	public static $params = array();
	public static $APP = false;

	//modal
	public static $modalElement = '.modal';
	public static $modalHeaderElement = 'div.modal-header';
	public static $modalLoaded = ':not(.loading)';
	public static $modalCloseElement = '.close';
	public static $modalCloseElementVisible = '.close:not([style="display: none;"])';
	//public static $modalLoaderElement = '';

	//logout
	public static $logoutButton = 'span.logout a';
	public static $logoutSuccessElement = '#loginBlock';
	//login
	public static $loginSuccessElement = '#header .logout';

	//validation errors
	public static $modalError = ".span12.alert.alert-error";

	//errors
	public static $siteError = '.site-error';
	public static $phpNotice = 'span';
	public static $phpNoticeText = 'PHP Notice';
	public static $callStack = '.call-stack';

	//Table
	public static $table_loading = 'div.grid-view.grid-view-loading';
	
	//App7020
	public static $customDropdownWidgetContainer = '.customDropdown-widget';
	// customDropdownWidget - icon for opening of menu
	public static $customDropdownOpenIcon = '.awesomeIcon-dots';

	public static $docebo_success_flash = 'div#dcb-feedback > div.alert.alert-success';

	/* ================ END OF MAPPING ================ */

	protected $acceptanceTester;

	public function __construct(\AcceptanceTester $I) {
		$this->acceptanceTester = $I;
	}

	public function logout() {
		$I = $this->acceptanceTester;

		$I->wantTo("Logout");
		$I->click(self::$logoutButton);
		$I->seeElement(self::$logoutSuccessElement);
	}

	public static function route($action = false, $params = array(), $overrideDefaultParameters = false, $app = false){
		//merge parameters with default params if no override, otherwise use only $params
		if(!$overrideDefaultParameters){
			$params = array_merge($params, static::$params);
		}

		if(!$action)
			$action = static::$URL;

		if(!$app)
			$app = static::$APP;

		if (!$app) {
			return Yii::app()->createUrl($action, $params);
		} else {
			return Docebo::createAppUrl($app.':'.$action, $params);
		}
	}

}