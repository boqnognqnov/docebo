<?php

namespace Page;

use Page\Common;
use Yii;
use Docebo;
use LearningOrganization;

class CentralRepository extends Common
{
    public static $URL = 'centralrepo/centralRepo/index';

    public static $params = array();

    // CATEGORY
    public static $categoryTree = 'div#repo-tree-wrapper';
    public static $categoryTreeRowTemplate = '//div[contains(@class, "ajaxLinkNode") and .//div[contains(@class, "label-for-radio") and text() = "{item}"]]';
    public static $categoryTreeRowSelectedTemplate = '//div[contains(@class, "ajaxLinkNode checked-node") and .//div[contains(@class, "label-for-radio") and text() = "{item}"]]';
    public static $categoryTreeRowCollapseTemplate = '//div[contains(@class, "ajaxLinkNode") and .//div[contains(@class, "label-for-radio") and text() = "{item}"]]//span[contains(@class, "collapse-node")]';
    public static $categoryTreeRowOptionsTemplate = '//div[contains(@class, "ajaxLinkNode") and .//div[contains(@class, "label-for-radio") and text() = "{item}"]]//a[contains(@class, "popover-trigger")]';
    public static $categoryTreeRowOptionsEditTemplate = '//div[contains(@class, "ajaxLinkNode") and .//div[contains(@class, "label-for-radio") and text() = "{item}"]]//a[contains(@class, "node-edit")]';
    public static $categoryTreeRowOptionsMoveTemplate = '//div[contains(@class, "ajaxLinkNode") and .//div[contains(@class, "label-for-radio") and text() = "{item}"]]//a[contains(@class, "node-move")]';
    public static $categoryTreeRowOptionsDeleteTemplate = '//div[contains(@class, "ajaxLinkNode") and .//div[contains(@class, "label-for-radio") and text() = "{item}"]]//a[contains(@class, "delete-node")]';
    // Move Selected Row
    public static $moveCategoryTreeSelectedCategory = '//div[contains(@class, "repo-nodeTree-selectNode") and .//div[contains(@class, "label-for-radio") and text() = "{item}"]]';
    public static $confirmDelete = 'div.deleteRepoCategory input[type="checkbox"]';
    //Main Actions
    public static $addNewCategory = 'div.main-actions a.new-category';
    public static $addNewCategoryInput = 'div.orgChart_translationsList input.orgChart_translation:visible';
    public static $addNewCategoryDescribeParentName = 'span.parentCategoryName';
    // Manage Category Submit Button
    public static $manageCategorySubmitButton = 'div[id*="modal-"] div.modal-footer input.confirm-btn';
    // CATEGORY

    // LOS
    public static $loTypeFilterInput = 'select#filter_by_type';
    public static $loContainChildrenInput = 'input#LearningRepositoryObject_containChildren';
    public static $loSearchInput = 'input#repo-search-input';
    public static $losGrid = 'div#repo-objects-grid';

    public static $importCentralLo = 'a.lo-upload-centralrepo';

    public static $addLoButton = 'div#central-lo-manage-add-lo-button';
    public static $addLoButtonCourse = 'div#player-manage-add-lo-button';

    public $lo_types = array(
        LearningOrganization::OBJECT_TYPE_SCORMORG,
        LearningOrganization::OBJECT_TYPE_AICC,
        LearningOrganization::OBJECT_TYPE_TINCAN,
        LearningOrganization::OBJECT_TYPE_VIDEO,
        LearningOrganization::OBJECT_TYPE_FILE,
        LearningOrganization::OBJECT_TYPE_ELUCIDAT,
        LearningOrganization::OBJECT_TYPE_AUTHORING,
        LearningOrganization::OBJECT_TYPE_HTMLPAGE,
        LearningOrganization::OBJECT_TYPE_POLL,
        LearningOrganization::OBJECT_TYPE_TEST
    );

    public static $centralRepoContainer = 'div#player-centralrepo-uploader';
    public static $fileUploader = 'input[type=file]';
    public static $addLoSubmitButton = '#upload-lo-form input[type=submit]';

    public static $versionControlElement = '.version-control';
    public static $keepVersionOption = '#keep-version';
    public static $overrideVersionOption = '#add-version';
    public static $newVersionName = 'input[name=version_name]';
    public static $newVersionDescription = 'input[name=version_description]';

    public static $objectTitle = '#file-title';
    public static $objectShortDescription = '#short_description';

    // Files located in "codeception/tests/_data"
    public static $video1 = 'video1.avi';
    public static $video2 = 'video2.avi';
    // AICC
    public static $aicc1 = 'aicc1.zip';
    public static $aicc2 = 'aicc2.zip';
    public static $aicc3 = 'aicc3.zip';

    public static $centralRepoTitle = 'Central Repository';

    public static $usersSelector = '.modal.users-selector';
    public static $usageDialog = '.modal.material-usage-dialog';
    public static $gridViewItem = '#selectedObjectIds_';
    public static $changeVersionModal = '.modal.version-change-modal';

    public static $alertSuccess = '.alert-success';

    // Reports
    public static $courseStatisticsUrl = 'player/report';
    public static $courseTrainingMaterialsUrl = 'player/training';
    public static $courseReportHeader = 'Course report';
    public static $userReportHeader = 'User report for: ';

    // Preview LO Selectors
    public static $preview_object_versions = "select#object_version";
    public static $preview_object_TOC = "div#player-lonav";

    public static function route($action = false, $params = array(), $overrideDefaultParameters = false, $app = false){
        //merge parameters with default params if no override, otherwise use only $params
        if(!$overrideDefaultParameters){
            $params = array_merge($params, static::$params);
        }

        if($action) {
            if (!$app) {
                return Yii::app()->createUrl($action, $params);
            } else {
                return Docebo::createAppUrl($app.':'.$action, $params);
            }

        }

        return Yii::app()->createUrl(static::$URL, $params);
    }

    public function checkCategoryContainsCategory($parents, $child){
        if(!is_array($parents))
            $parents = array($parents);

        foreach($parents as $parent){
            $item = str_replace('{item}', $parent, self::$categoryTreeRowCollapseTemplate);
            $this->acceptanceTester->click($item);
        }

        $this->acceptanceTester->see($child, self::$categoryTree);
    }

    public function addNewCategory($name){
        $this->acceptanceTester->openModal(self::$addNewCategory);
        $this->acceptanceTester->fillField(self::$addNewCategoryInput, $name);
        $this->acceptanceTester->click(self::$manageCategorySubmitButton);
        $this->acceptanceTester->waitForElementNotVisible(self::$manageCategorySubmitButton);
        $this->acceptanceTester->wait(1);// Wait One second till the tree is reloaded
    }

    public function addNewCategoryUnder($parent, $new){
        $oldItem = str_replace('{item}', $parent, self::$categoryTreeRowTemplate);
        $this->acceptanceTester->click($oldItem);
        $this->acceptanceTester->openModal(self::$addNewCategory);
        $this->acceptanceTester->see($parent, self::$addNewCategoryDescribeParentName);
        $this->acceptanceTester->fillField(self::$addNewCategoryInput, $new);
        $this->acceptanceTester->click(self::$manageCategorySubmitButton);
        $this->acceptanceTester->waitForElementNotVisible(self::$manageCategorySubmitButton);
        $this->acceptanceTester->wait(1);// Wait One second till the tree is reloaded
    }

    public function editCategory($old, $new){
        $oldItem = str_replace('{item}', $old, self::$categoryTreeRowTemplate);
        $this->acceptanceTester->moveMouseOver($oldItem);
        $oldItem = str_replace('{item}', $old, self::$categoryTreeRowOptionsTemplate);
        $this->acceptanceTester->click($oldItem);
        $edit = str_replace('{item}', $old, self::$categoryTreeRowOptionsEditTemplate);
        $this->acceptanceTester->wait(1);
        $this->acceptanceTester->openModal($edit);
        $this->acceptanceTester->seeInField(self::$addNewCategoryInput, $old);
        $this->acceptanceTester->fillField(self::$addNewCategoryInput, $new);
        $this->acceptanceTester->click(self::$manageCategorySubmitButton);
        $this->acceptanceTester->waitForElementNotVisible(self::$manageCategorySubmitButton);
        $this->acceptanceTester->wait(1);// Wait One second till the tree is reloaded
    }

    public function moveCategoryUnder($under, $node){
        $oldItem = str_replace('{item}', $node, self::$categoryTreeRowTemplate);
        $this->acceptanceTester->moveMouseOver($oldItem);
        $oldItem = str_replace('{item}', $node, self::$categoryTreeRowOptionsTemplate);
        $this->acceptanceTester->click($oldItem);
        $move = str_replace('{item}', $node, self::$categoryTreeRowOptionsMoveTemplate);
        $this->acceptanceTester->wait(1);
        $this->acceptanceTester->openModal($move);
        $select = str_replace('{item}', $under, self::$moveCategoryTreeSelectedCategory);
        $this->acceptanceTester->click($select);
        $this->acceptanceTester->click(self::$manageCategorySubmitButton);
        $this->acceptanceTester->waitForElementNotVisible(self::$manageCategorySubmitButton);
        $this->acceptanceTester->wait(1);// Wait One second till the tree is reloaded
    }

    public function deleteCategory($name){
        $oldItem = str_replace('{item}', $name, self::$categoryTreeRowTemplate);
        $this->acceptanceTester->moveMouseOver($oldItem);
        $oldItem = str_replace('{item}', $name, self::$categoryTreeRowOptionsTemplate);
        $this->acceptanceTester->click($oldItem);
        $delete = str_replace('{item}', $name, self::$categoryTreeRowOptionsDeleteTemplate);
        $this->acceptanceTester->wait(2);
        $this->acceptanceTester->openModal($delete);
        $this->acceptanceTester->checkStyledOption(self::$confirmDelete);
        $this->acceptanceTester->click(self::$manageCategorySubmitButton);
        $this->acceptanceTester->waitForElementNotVisible(self::$manageCategorySubmitButton);
        $this->acceptanceTester->wait(1);// Wait One second till the tree is reloaded
    }

    public static function getLoCreateUrl($type){
        return 'a[data-lo-type=' . $type. ']';
    }

    public function seeAllLOs(){
        foreach ($this->lo_types as $type){
            $this->acceptanceTester->seeElement( self::getLoCreateUrl($type) );
        }
    }

    public function addAICC($file){
        $this->acceptanceTester->waitForElement(self::$addLoButton);
        $this->acceptanceTester->click(self::$addLoButton . ' a');
        $this->acceptanceTester->click(self::getLoCreateUrl(LearningOrganization::OBJECT_TYPE_AICC));
        $this->acceptanceTester->waitForText("AICC", 2, self::$centralRepoContainer);
        $this->acceptanceTester->attachFile(self::$fileUploader, $file);
        $this->acceptanceTester->wait(2);
        $this->acceptanceTester->click(self::$addLoSubmitButton);
        $this->acceptanceTester->wait(2);
    }

    public function addAICCVersion($to = false, $file, $newVersion = false, $newVersionName, $newVersionDesc){

        // If there is defined element on which we should add NEW version then do it
        // If there is not .... then get the first element in the GRID..... what else ???
        if($to !== false){
            $this->acceptanceTester->click($to);
        } else {
            $this->acceptanceTester->click(array('link' => 'Edit'), self::$losGrid);
        }
        $this->acceptanceTester->wait(3);
        $this->acceptanceTester->waitForText("AICC", 3, self::$centralRepoContainer);
        $this->acceptanceTester->attachFile(self::$fileUploader, $file);
        $this->acceptanceTester->wait(2);
        if($newVersion){
            $this->acceptanceTester->checkStyledRadio(self::$overrideVersionOption);
            $this->acceptanceTester->fillField(self::$newVersionName, $newVersionName);
            $this->acceptanceTester->fillField(self::$newVersionDescription, $newVersionDesc);
        }
        $this->acceptanceTester->click(self::$addLoSubmitButton);
        $this->acceptanceTester->wait(2);
    }
}