<?php
namespace Codeception\Module;

use Yii;

class DoceboHelper extends \Codeception\Module
{
    /**
     * Returns a single column from a database (using CdbConnection)
     */
    public function grabFromDatabase($table, $column, $criteria = array()) {
        $cmd = Yii::app()->db->createCommand(); /* @var $cmd \CDbCommand */
        $cmd->select($column)
            ->from($table);

        foreach($criteria as $name => $value)
            $cmd->andWhere($name." = '".$value."'");

        return $cmd->queryScalar();
    }

    public function seePluginIsActive($plugin_name){
        $active = \PluginManager::isPluginActive($plugin_name);
        \PHPUnit_Framework_Assert::assertNotFalse($active, 'Plugin "' . $plugin_name . '" is not active');
    }

    public function seeInDB($table, $column, $value, $condition = false){
        $query = Yii::app()->db->createCommand()
            ->select($column)
            ->from($table);
        if($condition)
            $query->where($condition);

        $dbValue = $query->queryScalar();

        \PHPUnit_Framework_Assert::assertNotFalse($dbValue == $value, "Can't find '$value' into Database");
    }
}