<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/4/2015
 * Time: 11:01 AM
 *
 * MakeSafe functions are used to ensure that we have the needed data for the test we are going to do
 *
 * The logic below is used in all(for now :]) makeSafe methods
 * Yes, we can check the user if the model(pu,user,..) has assigned the proper data and assign(or unassign only some of
 * them, but its easier(and potentially faster) to just create a few new models and assign half of them to the model.
 *
 * Also note that models are used because we can't always know what logic is tied in with the creation process of some
 * particular records. Models handle that mostly in after/before save methods
 */
trait makeSafe{

	/**
	 * Ensure that the provided PU has some courses of type $type that are assigned to him, and some that are not
	 *
	 * @param $powerUserId
	 * @param $type
	 */
	public function makeSafeCourseAssignments($powerUserId, $type){
		$insertCounter = 1;

		while($insertCounter <= 10){
			$courseModel = new LearningCourse();
			$courseModel->name = "codeception{$insertCounter}_" . time();
			$courseModel->description = "Automatically inserted by codeception";
			$courseModel->code = "auto{$insertCounter}_" . time();
			$courseModel->course_type = $type;
			$courseModel->save(false);

			//assign half of the courses to the PU
			if($insertCounter > 5){
				$model = new CoreUserPuCourse();
				$model->puser_id = $powerUserId;
				$model->course_id = $courseModel->idCourse;
				$model->save(false);
			}

			$insertCounter++;
		}
	}

	/**
	 * Ensure that the provided PU has some LP that are assigned to him, and some that are not
	 *
	 * @param $powerUserId
	 */
	public function makeSafeLearningPlanAssignments($powerUserId){
		$insertCounter = 1;

		while($insertCounter <= 10){
			$lpModel = new LearningCoursepath();
			$lpModel->path_name = "codeception{$insertCounter}_" . time();
			$lpModel->path_descr = 'Automatically inserted by codeception';
			$lpModel->path_code = "auto{$insertCounter}_" . time();
			//assign some random cert
			$lpModel->certificate = new LearningCertificateCoursepath();
			$lpModel->certificate->id_certificate = LearningCertificate::model()->find()->id_certificate;
			$lpModel->save(false);

			//assign half of the PLs to the PU
			if($insertCounter > 5){
				$assignModel = new CoreUserPuCoursepath();
				$assignModel->puser_id = $powerUserId;
				$assignModel->path_id = $lpModel->id_path;
				$assignModel->save(false);
			}

			$insertCounter++;
		}
	}

	/**
	 * Ensure that the provided PU has some categories that are assigned to him, and some that are not
	 *
	 * @param $powerUserId
	 */
	public function makeSafeCategoryAssignments($powerUserId){
		//get root
		$root = LearningCourseCategoryTree::model()->roots()->find();

		$insertCounter = 1;
		while($insertCounter <= 10){

			$learningCategory = new LearningCourseCategory();
			$learningCategory->lang_code = 'english'; //we don't really care about the language here
			$learningCategory->translation = 'codeceptionCategory_' . time();
			$learningCategory->save(false);

			//assign to the tree
			$learningCourseCategoryTree = new LearningCourseCategoryTree();
			$learningCourseCategoryTree->idCategory = $learningCategory->idCategory;
			$learningCourseCategoryTree->appendTo($root);

			//assign half of the categories to the PU
			if($insertCounter > 5){
				$coreAdminCourse = new CoreAdminCourse();
				$coreAdminCourse->idst_user = $powerUserId;
				$coreAdminCourse->id_entry = $learningCategory->idCategory;
				$coreAdminCourse->type_of_entry = CoreAdminCourse::TYPE_CATEGORY_DESC;
				$coreAdminCourse->save(false);
			}

			$insertCounter++;
		}
	}

	/**
	 * Ensure that there is at least one scheduled job in the DB
	 */
	public function makeSafeScheduledJobs(){ //TODO: work in progress
		//we only need to insert 1 record for now.
		//maybe at some point we should insert also an active/running record, but a talk with Raffaele is needed

		$scheduledJob = new CoreJob();
		$scheduledJob->hash_id = sha1(time());
		$scheduledJob->name = 'codeceptionDummyJob';
		$scheduledJob->active = 1;
		$scheduledJob->is_running = 0;
		$scheduledJob->save(false);

	}
}