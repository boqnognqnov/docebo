<?php
namespace AcceptanceTraits;
/**
 * Created by PhpStorm.
 * User: asen
 * Date: 12-Feb-16
 * Time: 3:42 PM
 */

trait BackgroundJob
{
	public function clickNotifyLinkPage(){
		$this->click(\Page\Test2\BackgroundJobsFull::$pageNotifyLink);
		$this->dontSeeElement(\Page\Test2\BackgroundJobsFull::$pageNotifyLink);
		$this->seeElement(\Page\Test2\BackgroundJobsFull::$pageNotificationSubscribed);
	}

	public function clickNotifyLinkHeader(){
		$this->click(\Page\Test2\BackgroundJob::$headerNotifyLink);
		$this->dontSeeElement(\Page\Test2\BackgroundJob::$headerNotifyLink);
		$this->seeElement(\Page\Test2\BackgroundJob::$headerNotificationSubscribed.':first');
	}

	public function openBackgroundTooltip(){
		$this->click(\Page\Test2\BackgroundJob::$headerJobNotificationElementIcon);
		$this->waitForElementVisible(\Page\Test2\BackgroundJob::$headerJobNotificationElementTooltip, 5);
	}

	public function openBackgroundTooltipAndClickOnViewAllJobs(){
		$this->openBackgroundTooltip();
		$this->click(\Page\Test2\BackgroundJob::$headerNotificationViewAllLink);
		$this->amOnPage(\Page\Test2\BackgroundJobsFull::$URL);
	}

	public function startBackgroundJobWithSuccess($shouldInterrupt = false){
		$this->amOnPage(\Page\Test2\BackgroundJob::route());
		$this->seeElement(\Page\Test2\BackgroundJob::$headerJobNotificationElement);
		$this->seeElement(\Page\Test2\BackgroundJob::$headerJobNotificationElementCounter);

		// get the initial counter for active jobs
		$counter = $this->grabTextFrom(\Page\Test2\BackgroundJob::$headerJobNotificationElementCounter);
		$this->click('#jobStart');

//		 we expect number to increase with one after the new job is created
		$counter++;

		$_this = clone $this;
		$this->waitForElementChange(\Page\Test2\BackgroundJob::$headerJobNotificationElementCounter, function() use ($counter, &$_this) {
			return ($_this->grabTextFrom(\Page\Test2\BackgroundJob::$headerJobNotificationElementCounter) == $counter);
		}, 2);

		// open a tooltip with first 5 jobs
		$this->openBackgroundTooltip();

		// only one active job should be
		$this->seeNumberOfElements(\Page\Test2\BackgroundJob::$headerProgressDiv, 1);
		$this->clickNotifyLinkHeader($this);

		$this->seeElement(\Page\Test2\BackgroundJob::$headerNotificationStopLink);

		if ($shouldInterrupt === true) {
			$this->click(\Page\Test2\BackgroundJob::$headerNotificationStopLink);
			$this->dontSeeElement(\Page\Test2\BackgroundJob::$headerNotificationStopLink);
		} else {
			// wait for the job to complete
			$this->waitForElementChange(\Page\Test2\BackgroundJob::$headerJobPercent, function () use (&$_this) {
				return ($_this->grabTextFrom(\Page\Test2\BackgroundJob::$headerJobPercent) == '100%');
			}, 110);
		}

		// see 3 errors from the job and try to download the csv file to see them
		$this->seeElement(\Page\Test2\BackgroundJob::$headerJobErrorsLink, array('text'=>'(3)'));
		$this->click(\Page\Test2\BackgroundJob::$headerJobErrorsLink);

		// check number of rows
		// TODO check for three rows in the file
		$filePath = Docebo::getUploadTmpUrl('test.csv');
		$lineNumbers = $this->fileManager->getNewlineNumbers($filePath);
		PHPUnit_Framework_Assert::assertEquals(3, $lineNumbers, 'There should be exact 3 lines!');
	}

	public function startBackgroundJobWithError(){
		$this->amOnPage(\Page\Test2\BackgroundJob::route());
		$this->dontSeeElement(\Page\Test2\BackgroundJob::$headerJobNotificationElement);
		$this->click('#jobStart');
		$this->seeElement('.alert.alert-error');
	}

	public function clickToDownloadErrors(){
		$this->click(\Page\Test2\BackgroundJobsFull::$pageJobErrors);
		// TODO check if the file is downloaded

	}

	public function adminSeeJobsAndInterruptTheOneWhichIsActive(){
		$this->seeElement(\Page\Test2\BackgroundJobJob::$headerJobNotificationElementCounter, array('text' => 3));
		$this->openBackgroundTooltipAndClickOnViewAllJobs();

		// only 3 total jobs, one should be active
		$this->seeNumberOfElements(\Page\Test2\BackgroundJobsFull::$pageSingleJobContainer, 3);
		$this->seeNumberOfElements(\Page\Test2\BackgroundJobsFull::$pageNotifyLink, 1);
		$this->seeElement(\Page\Test2\BackgroundJobsFull::$pageJobErrorsLink, array('text' => 3));
		$this->click(\Page\Test2\BackgroundJobsFull::$pageNotificationStopLink);
		$this->dontSeeElement(\Page\Test2\BackgroundJobsFull::$pageNotificationStopLink);

		// see error icon with 3 errors
		$this->seeElement(\Page\Test2\BackgroundJobsFull::$pageJobErrorsLink, array('text' => 3));
		$this->clickToDownloadErrors();
		// todo file download check
		$filePath = Docebo::getUploadTmpUrl('file.csv');
		$numberOfErrors = $this->fileManager->getNewlineNumbers($filePath);
		PHPUnit_Framework_Assert::assertEquals(3, $numberOfErrors, 'Number of errors in csv file is different than 3!');
	}
	
	public function createNotificationInboxMsg(){
		PluginManager::activateAppByCodename('Notification');
		Yii::app()->db->createCommand()
				->insert(CoreNotification::model()->tableName(), array(
						'type' => 'NewUserCreated',
						'recipient' => 'user',
						'active' => 1,
						'schedule_type' => 'at',
						'schedule_time' => 0,
						'from_name' => 'batanatoli',
						'from_email' => 'batanatoli@celtis.net',
						'schedule_shift_period' => 'day',
						'schedule_shift_number' => 1,
						'job_handler_id' => 'plugin.NotificationApp.components.NotificationJobHandler',
						'id_job' => null,
						'id_author' => 13006,
						'ufilter_option' => null,
						'cfilter_option' => null,
						'notify_type' => 'email_and_inbox',
						'puProfileId' => 0,
				));
		Yii::app()->db->createCommand()
				->insert(InboxAppNotification::model()->tableName(), array(
						'type'=>'newsletter',
						'timestamp'=>'2015-03-27 14:21:44',
						'json_data'=>'{"subject":"Test","body":"<p>This a test!<\/p>"}'
				));
		Yii::app()->db->createCommand()
				->insert(InboxAppNotificationDelivered::model()->tableName(), array(
						'notification_id' => 1,
						'user_id' => 13006,
						'is_read' => 0,
				));
	}
}