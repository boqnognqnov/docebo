<?php
namespace Codeception\Module;

use Facebook\WebDriver\WebDriverBy;

class DoceboWebDriver extends \Codeception\Module\WebDriver
{
	/**
	 * Checks that the specified selector contains all specified values
	 *
	 * @param $selector
	 * @param $values
	 */
	public function seeDropdownContainsAllValues($selector, $values) {
		$el = $this->findField($selector);
		$this->assertNotNull($el, "Cannot find element with selector '".$selector."'");
		$this->assertEquals("select", $el->getTagName(), "Element with selector '".$selector."' is not a dropdown");
		foreach($values as $value) {
			$xpath = './/option[@value = "'.$value.'"]';
			$option = $el->findElements(WebDriverBy::xpath($xpath));
			$this->assertNotEmpty($option);
		}
	}
}
