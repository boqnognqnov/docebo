<?php

class CCMigration  extends \Codeception\Module
{
    public static function beforeRun($config){
        // If the user want to apply the migrations into the test database
        if(isset($config['DoceboHelper']['apply_migrations']) && $config['DoceboHelper']['apply_migrations']){
            $runner      = new CConsoleCommandRunner();
            $commandPath = Yii::getFrameworkPath() . DIRECTORY_SEPARATOR . 'cli' . DIRECTORY_SEPARATOR . 'commands';
            $runner->addCommands( $commandPath );
            $args = array(
                'yiic-domain ' . $config['DoceboHelper']['server_name'],
                'migrate',
                // Everything else is optional parameters, appended to the migrate command:
                "--migrationPath=common.migrations",
                "--interactive=0",
            );
            $runner->run( $args );

            /*
             * Preventing the dump.sql from Updating....
             * Till we figure it out how to remove the DEFINER from the dump
             */
//            // If the developer wants to update the test dump
//            if(isset($config['DoceboHelper']['update_dump']) && $config['DoceboHelper']['update_dump']){
//                // Find the database
//                preg_match('/dbname=(.*)$/', $config['Db']['dsn'], $dbMatches);
//                if(!isset($dbMatches[1]))
//                    throw new Exception('Cannot read database');
//
//                $database = $dbMatches[1];
//                $dump = (__DIR__ . '/../../'. $config['Db']['dump']);
//                $file = (__DIR__ . "/../test_db.cnf");
//                $cmd = "mysqldump --defaults-file=" . $file . " " . $database . " > " . $dump;
//                exec($cmd);
//            }
            }
        }
}