<?php
$params = array (
	'LpId' => 1,
	'userId' => 18154,
);

$I = new ApiGuy($scenario);
$I->wantTo('call webapp api to retrieve courses for a LP');
$I->doOauth2Authentication('webapp');
$I->sendPOST('webapp/get_courses_by_lp_id', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();