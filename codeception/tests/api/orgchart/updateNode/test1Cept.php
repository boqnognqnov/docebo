<?php
$params = array (
	'id_org' => 16,
	'code' => 'azienda1 modificato',
	'translation[english]' => 'Azienda 1 english via api',
	'translation[italian]' => 'Azienda 1 italian via api'
);

$I = new ApiGuy($scenario);
$I->wantTo('update an existing node');
$I->doStrongAuthentication($params);
$I->sendPOST('orgchart/updateNode', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();