<?php
$params = array (
	'code' => 'API-1',
	'translation[english]' => 'English node',
	'translation[french]' => 'French node'
);

$I = new ApiGuy($scenario);
$I->wantTo('create a new node under the root');
$I->doStrongAuthentication($params);
$I->sendPOST('orgchart/createNode', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();