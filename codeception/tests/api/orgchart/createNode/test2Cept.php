<?php
$params = array (
	'id_parent' => 16,
	'code' => 'API-2',
	'translation[english]' => 'English node 2',
	'translation[french]' => 'French node 2'
);

$I = new ApiGuy($scenario);
$I->wantTo('create a new node under an existing node');
$I->doStrongAuthentication($params);
$I->sendPOST('orgchart/createNode', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();