<?php
$params = array (
	'code' => 'test-import'
);

$I = new ApiGuy($scenario);
$I->wantTo('find a node by code');
$I->doStrongAuthentication($params);
$I->sendPOST('orgchart/findNodeByCode', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();