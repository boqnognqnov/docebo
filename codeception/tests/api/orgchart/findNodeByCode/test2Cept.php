<?php
$params = array (
	'code' => 'wrong node'
);

$I = new ApiGuy($scenario);
$I->wantTo('find a non existing node by code');
$I->doStrongAuthentication($params);
$I->sendPOST('orgchart/findNodeByCode', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();