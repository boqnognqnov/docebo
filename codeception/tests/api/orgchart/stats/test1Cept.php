<?php
//@group orgchart_stats
//@group DOCEBO-1011

$params = array (
	'id_org' => -2,
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling orgchart/stats with an invalid "id_org"');
$I->doOauth2Authentication();
$I->sendPOST('orgchart/stats', $params);
$I->seeResponseCodeIs(701);
$I->seeErrorMessageIs('Invalid "id_org" param provided');
$I->seeApiIsFailure();