<?php
//@group orgchart_stats
//@group DOCEBO-1011

$params = array (
	'from' => 2,
	'count' => OrgchartApiModule::MAX_SIZE + 1,
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling orgchart/stats with a "count" bigger than max size');
$I->doOauth2Authentication();
$I->sendPOST('orgchart/stats', $params);
$I->seeResponseCodeIs(705);
$I->seeErrorMessageIs('"count" param exceeds the maximum page size (' . OrgchartApiModule::MAX_SIZE . ')');
$I->seeApiIsFailure();