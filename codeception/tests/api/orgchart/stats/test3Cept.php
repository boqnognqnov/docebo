<?php
//@group orgchart_stats
//@group DOCEBO-1011

$params = array ();

$I = new ApiGuy($scenario);
$I->wantTo('call orgchart/stats without "from" parameter');
$I->doOauth2Authentication();
$I->sendPOST('orgchart/stats', $params);
$I->seeResponseCodeIs(703);
$I->seeErrorMessageIs('Invalid "from" param provided');
$I->seeApiIsFailure();


$params = array (
	'from' => -2.3,
);
$I->amGoingTo('call orgchart/stats with an invalid "from" parameter');
//no need to do authorization again
$I->sendPOST('orgchart/stats', $params);
$I->seeResponseCodeIs(703);
$I->seeErrorMessageIs('Invalid "from" param provided');
$I->seeApiIsFailure();