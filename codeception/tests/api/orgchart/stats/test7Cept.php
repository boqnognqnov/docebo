<?php
//@group orgchart_stats
//@group DOCEBO-1011

$I = new ApiGuy($scenario);
$idOrg = $I->getRandomBranchId();

$params = array(
	'id_org' => $idOrg,
	'from' => 0,
	'count' => 10,
);

$I->wantTo('API client calling orgchart/stats with a valid id_org matching a single branch');
$I->doOauth2Authentication();
$I->sendPOST('orgchart/stats', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

$I->seeResponseMatchesJsonType(array(
	'branches' => array(
		array(
			'id_org' => 'integer',
			'code' => 'string|null',
			'translation' => 'array',
			'stats' => array(
				'total_users' => 'integer',
				'course_enrollments' => 'integer',
				'course_enrollments_not_started' => 'integer',
				'course_enrollments_in_progress' => 'integer',
				'course_enrollments_completed' => 'integer',
				'course_enrollments_expired' => 'integer',
				'course_enrollments_waitlist' => 'integer',
				'course_enrollments_overbooking' => 'integer',
				'course_enrollments_suspended' => 'integer',
				'course_enrollments_to_be_confirmed' => 'integer',
			),
		),
	),
	'success' => 'boolean',
));