<?php
//@group orgchart_stats
//@group DOCEBO-1011

$I = new ApiGuy($scenario);
$idsOrgs = Yii::app()->db->createCommand()
		->select('idOrg')
		->from(CoreOrgChartTree::model()->tableName())
		->where('(iRight-iLeft)>1 and idOrg <> 0')->queryColumn();

foreach ($idsOrgs as $idOrg) {
	$model = CoreOrgChartTree::model()->findByPk($idOrg);
	$users = $model->getBranchUsers();
	if (!empty($users)) {
		break;
	}
}

$params = array(
	'id_org' => $model->idOrg,
	'from' => 0,
	'count' => 10,
	'include_descendants' => true,
);

$I->wantTo('API client calling orgchart/stats with "include_descendants" = true');
$I->doOauth2Authentication();
$I->sendPOST('orgchart/stats', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

//TODO: we need some hardcoded values for the stats + to achieve that, we must add some users to the branches to have predictable results
$I->seeResponseMatchesJsonType(array(
	'branches' => array(
		array(
			'id_org' => 'integer',
			'code' => 'string|null',
			'translation' => 'array',
			'stats' => array(
				'total_users' => 'integer',
				'course_enrollments' => 'integer',
				'course_enrollments_not_started' => 'integer',
				'course_enrollments_in_progress' => 'integer',
				'course_enrollments_completed' => 'integer',
				'course_enrollments_expired' => 'integer',
				'course_enrollments_waitlist' => 'integer',
				'course_enrollments_overbooking' => 'integer',
				'course_enrollments_suspended' => 'integer',
				'course_enrollments_to_be_confirmed' => 'integer',
			),
		),
	),
	'success' => 'boolean',
));