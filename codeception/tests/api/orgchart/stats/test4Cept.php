<?php
//@group orgchart_stats
//@group DOCEBO-1011

$params = array (
	'from' => 2,
);

$I = new ApiGuy($scenario);
$I->wantTo('call orgchart/stats without "count" parameter');
$I->doOauth2Authentication();
$I->sendPOST('orgchart/stats', $params);
$I->seeResponseCodeIs(704);
$I->seeErrorMessageIs('Invalid "count" param provided');
$I->seeApiIsFailure();

$params = array (
	'from' => 2,
	'count' => -2,
);
$I->amGoingTo('call orgchart/stats with an invalid "count" parameter');
//no need to do authorization again
$I->sendPOST('orgchart/stats', $params);
$I->seeResponseCodeIs(704);
$I->seeErrorMessageIs('Invalid "count" param provided');
$I->seeApiIsFailure();