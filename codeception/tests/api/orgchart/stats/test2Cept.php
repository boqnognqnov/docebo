<?php
//@group orgchart_stats
//@group DOCEBO-1011

$params = array (
	'code' => '-2ksrgo23k',
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling orgchart/stats with an invalid "code"');
$I->doOauth2Authentication();
$I->sendPOST('orgchart/stats', $params);
$I->seeResponseCodeIs(702);
$I->seeErrorMessageIs('Invalid "code" param provided');
$I->seeApiIsFailure();