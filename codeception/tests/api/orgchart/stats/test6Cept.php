<?php
//@group orgchart_stats
//@group DOCEBO-1011

$params = array(
	'from' => 0,
	'count' => 10,
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling orgchart/stats with no specified node');
$I->doOauth2Authentication();
$I->sendPOST('orgchart/stats', $params);

$I->seeResponseContainsAttribute('branches');
$result = json_decode($I->grabResponse());

$branchIdsInDb = Yii::app()->db->createCommand()
	->select('idOrg')
	->from(CoreOrgChartTree::model()->tableName())->limit($params['count'])
	->where('lev = 2')->queryColumn();

if (!empty($branchIdsInDb)) {
	// we have some branches in db, so we expect result from api call
	PHPUnit_Framework_Assert::assertTrue(!empty($result->branches), 'Branches list is empty!');
} else {
	// we don't have branches in db, so we don't expect result from api call
	PHPUnit_Framework_Assert::assertTrue(empty($result->branches), 'Branches list is filled!');
}

$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

$I->seeResponseMatchesJsonType(array(
	'branches' => array(
		array(
			'id_org' => 'integer',
			'code' => 'string|null',
			'translation' => 'array',
			'stats' => array(
				'total_users' => 'integer',
				'course_enrollments' => 'integer',
				'course_enrollments_not_started' => 'integer',
				'course_enrollments_in_progress' => 'integer',
				'course_enrollments_completed' => 'integer',
				'course_enrollments_expired' => 'integer',
				'course_enrollments_waitlist' => 'integer',
				'course_enrollments_overbooking' => 'integer',
				'course_enrollments_suspended' => 'integer',
				'course_enrollments_to_be_confirmed' => 'integer',
			),
		),
	),
	'success' => 'boolean',
));