<?php
//@group orgchart_stats
//@group DOCEBO-1011

$I = new ApiGuy($scenario);

$idOrg = Yii::app()->db->createCommand()
	->select('co.idParent')
	->from(LearningCourseuser::model()->tableName() . ' lcu')
	->join(CoreGroupMembers::model()->tableName() . ' cgm', 'lcu.idUser=cgm.idstMember')
	->join(CoreOrgChartTree::model()->tableName() . ' co', 'cgm.idst=co.idst_oc')
	->where('lcu.status=:status AND co.idParent!=0', array(':status' => LearningCourseuser::$COURSE_USER_WAITING_LIST))
	->limit(1)->queryScalar();

PHPUnit_Framework_Assert::assertNotFalse($idOrg, 'No waitlist users found in the system');

$params = array(
	'id_org' => $idOrg,
	'from' => 0,
	'count' => 100,
);

$I->wantTo('API client calling orgchart/stats with at least one user in waitlist of the course');
$I->doOauth2Authentication();
$I->sendPOST('orgchart/stats', $params);

//TODO: rework to use exact data and not to search for it (v6.9)
$I->seeResponseContainsAttribute('branches');
$response = json_decode($I->grabResponse());
$isFound = false;
foreach ($response->branches as $branch) {
	if ($branch->stats->waiting_users > 0) {
		$isFound = true;
		break;
	}
}
PHPUnit_Framework_Assert::assertTrue($isFound, 'No waitlist user found, but should be!');
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

//TODO: we need some hardcoded values for the stats + to achieve that, we must add some users to the branches to have predictable results(v6.9)
$I->seeResponseMatchesJsonType(array(
	'branches' => array(
		array(
			'id_org' => 'integer',
			'code' => 'string|null',
			'translation' => 'array',
			'stats' => array(
				'total_users' => 'integer',
				'course_enrollments' => 'integer',
				'course_enrollments_not_started' => 'integer',
				'course_enrollments_in_progress' => 'integer',
				'course_enrollments_completed' => 'integer',
				'course_enrollments_expired' => 'integer',
				'course_enrollments_waitlist' => 'integer',
				'course_enrollments_overbooking' => 'integer',
				'course_enrollments_suspended' => 'integer',
				'course_enrollments_to_be_confirmed' => 'integer',
			),
		),
	),
	'success' => 'boolean',
));