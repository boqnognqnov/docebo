<?php
//@group orgchart_stats
//@group DOCEBO-1011

$I = new ApiGuy($scenario);
$code = Yii::app()->db->createCommand()
	->select('code')
	->from(CoreOrgChartTree::model()->tableName().' cc')
	->where('cc.code IS NOT NULL')
		->group('cc.code')->having('COUNT(cc.`code`)=1')->queryColumn()[0];

//just to be sure, check if a code is found
PHPUnit_Framework_Assert::assertNotNull($code, 'No codes match only one branch!');

$params = array(
	'code' => $code,
	'from' => 0,
	'count' => 10,
);

$I->wantTo('API client calling orgchart/stats with a valid code matching a single branch');
$I->doOauth2Authentication();
$I->sendPOST('orgchart/stats', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

//TODO: we need some hardcoded values for the stats + to achieve that, we must add some users to the branches to have predictable results
$I->seeResponseMatchesJsonType(array(
	'branches' => array(
		array(
			'id_org' => 'integer',
			'code' => 'string|null',
			'translation' => 'array',
			'stats' => array(
				'total_users' => 'integer',
				'course_enrollments' => 'integer',
				'course_enrollments_not_started' => 'integer',
				'course_enrollments_in_progress' => 'integer',
				'course_enrollments_completed' => 'integer',
				'course_enrollments_expired' => 'integer',
				'course_enrollments_waitlist' => 'integer',
				'course_enrollments_overbooking' => 'integer',
				'course_enrollments_suspended' => 'integer',
				'course_enrollments_to_be_confirmed' => 'integer',
			),
		),
	),
	'success' => 'boolean',
));