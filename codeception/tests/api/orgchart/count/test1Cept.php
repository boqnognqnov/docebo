<?php
//@group orgchart_count
//@group DOCEBO-1011

$params = array (
	'id_org' => -2,
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling orgchart/count with an invalid "id_org"');
$I->doOauth2Authentication();
$I->sendPOST('orgchart/count', $params);
$I->seeResponseCodeIs(701);
$I->seeErrorMessageIs('Invalid "id_org" param provided');
$I->seeApiIsFailure();