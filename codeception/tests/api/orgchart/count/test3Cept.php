<?php
//@group orgchart_count
//@group DOCEBO-1011

$params = array ();

$I = new ApiGuy($scenario);
$I->wantTo('API client calling orgchart/count without any parameters');
$I->doStrongAuthentication($params);
$I->sendPOST('orgchart/count', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

$I->seeResponseMatchesJsonType(array(
	'count' => 'integer',
	'success' => 'boolean',
));