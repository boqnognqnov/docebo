<?php
$params = array (
	'id_org' => 16
);

$I = new ApiGuy($scenario);
$I->wantTo('get information about a node');
$I->doStrongAuthentication($params);
$I->sendPOST('orgchart/getNodeInfo', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();