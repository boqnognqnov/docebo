<?php
//@group unassignUsersFromNode

class UnassignUsersFromNodeCest{
	protected $userHelper;
	protected $orgChartHelper;

	public function _inject(\Helper\User $user, \Helper\Orgchart $orgChart) {
		$this->userHelper = $user;
		$this->orgChartHelper = $orgChart;
	}



	public function scenarioUnassignUsersFromNode(ApiGuy $I) {
		$userA 	= $this->userHelper->createUser();
		$userB 	= $this->userHelper->createUser();
		$node 	= $this->orgChartHelper->createNode('G1','Group1');
		$this->orgChartHelper->assignUserToNode($userA->idst, $node->idOrg);
		$this->orgChartHelper->assignUserToNode($userB->idst, $node->idOrg);

		$params = array (
				'id_org' => $node->idOrg,
				'user_ids' =>implode(',',array($userA->idst, $userB->idst)),//'niki'
		);

		//$I = new ApiGuy($scenario);
		$I->wantTo('Unassign users from a node');
		$I->doOauth2Authentication();
		$I->sendPOST('orgchart/unassignUsersFromNode', $params);
		$I->seeResponseCodeIs(200);
		$I->seeApiIsSuccess();
		$I->seeResponseMatchesJsonType(array(
				'unassigned_users' => 'string',
				'success' => 'boolean'
		));

	}

	public function scenarioUnassignUsersFromNodeMissingUserId(ApiGuy $I) {
		$userA 	= $this->userHelper->createUser();
		$userB 	= $this->userHelper->createUser();
		$node 	= $this->orgChartHelper->createNode('G1','Group1');//$orgChartHelper->createGroup('G1');

		$this->orgChartHelper->assignUserToNode($userA->idst, $node->idOrg);
		$this->orgChartHelper->assignUserToNode($userB->idst, $node->idOrg);

		$params = array (
				'id_org' => $node->idOrg,
		);


		//$I = new ApiGuy($scenario);
		$I->wantTo('Unassign users from a node');
		$I->doOauth2Authentication();
		$I->sendPOST('orgchart/unassignUsersFromNode', $params);
		$I->seeResponseCodeIs(402);
//		$I->seeApiIsSuccess();
		$I->seeErrorMessageIs('Missing or invalid required param "user_ids"');
	}

	public function scenarioUnassignUsersFromNodeMissingIdOrg(ApiGuy $I) {
		$userA 	= $this->userHelper->createUser();
		$userB 	= $this->userHelper->createUser();
		$node 	= $this->orgChartHelper->createNode('G1','Group1');//$orgChartHelper->createGroup('G1');

		$this->orgChartHelper->assignUserToNode($userA->idst, $node->idOrg);
		$this->orgChartHelper->assignUserToNode($userB->idst, $node->idOrg);

		$params = array (
				'user_ids' => implode(',',array($userA->idst, $userB->idst)),
		);


		//$I = new ApiGuy($scenario);
		$I->wantTo('Unassign users from a node');
		$I->doOauth2Authentication();
		$I->sendPOST('orgchart/unassignUsersFromNode', $params);
		$I->seeResponseCodeIs(401);
//		$I->seeApiIsSuccess();
		$I->seeErrorMessageIs('Missing or invalid required param "id_org"');
	}

	public function scenarioUnassignUsersFromNodeInvalidNode(ApiGuy $I) {
		$userA 	= $this->userHelper->createUser();
		$userB 	= $this->userHelper->createUser();
		$node 	= $this->orgChartHelper->createNode('G1','Group1');//$orgChartHelper->createGroup('G1');

		$this->orgChartHelper->assignUserToNode($userA->idst, $node->idOrg);
		$this->orgChartHelper->assignUserToNode($userB->idst, $node->idOrg);

		$params = array (
				'id_org' => 99999,//$node->idOrg,
				'user_ids' => implode(',',array($userA->idst, $userB->idst)),
		);


		//$I = new ApiGuy($scenario);
		$I->wantTo('Unassign users from a node');
		$I->doOauth2Authentication();
		$I->sendPOST('orgchart/unassignUsersFromNode', $params);
		$I->seeResponseCodeIs(403);
//		$I->seeApiIsSuccess();
		$I->seeErrorMessageIs('Invalid node');
	}

}