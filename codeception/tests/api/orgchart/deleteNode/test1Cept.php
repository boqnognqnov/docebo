<?php
$params = array (
	'id_org' => 17
);

$I = new ApiGuy($scenario);
$I->wantTo('delete an existing node');
$I->doStrongAuthentication($params);
$I->sendPOST('orgchart/deleteNode', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();