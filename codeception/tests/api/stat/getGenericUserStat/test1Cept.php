<?php
$params = array(
    'user_stat_only' => 0
);

$I = new ApiGuy($scenario);
$I->wantTo('get generic platform stats');
$I->doStrongAuthentication($params);
$I->sendPOST('stat/getGenericPlatformStat', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();