<?php
$params = array(
);

$I = new ApiGuy($scenario);
$I->wantTo('get generic user stats');
$I->doStrongAuthentication($params);
$I->sendPOST('stat/getGenericUserStat', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();