<?php
$params = array(
	'name' => "Test group from api"
);

$I = new ApiGuy($scenario);
$I->wantTo('create a new group with no description');
$I->doStrongAuthentication($params);
$I->sendPOST('group/insert', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();