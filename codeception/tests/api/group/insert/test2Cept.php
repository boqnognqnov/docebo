<?php
$params = array(
	'name' => "Another Test group from api",
	"description" => "Funny description"
);

$I = new ApiGuy($scenario);
$I->wantTo('create a new group with a description');
$I->doStrongAuthentication($params);
$I->sendPOST('group/insert', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();