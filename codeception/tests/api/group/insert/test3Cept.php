<?php
$params = array(
	'name' => "Group C = Divya + Deb Units",
);

$I = new ApiGuy($scenario);
$I->wantTo('create a new group with a description containing special characters');
$I->doStrongAuthentication($params);
$I->sendPOST('group/insert', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();