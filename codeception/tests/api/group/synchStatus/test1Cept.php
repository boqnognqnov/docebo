<?php
$params = array(
    'job_id' => '93f9108906a9318de43e1e2063feb949bea312f9'
);

$I = new ApiGuy($scenario);
$I->wantTo('get bulk group synch status');
$I->doStrongAuthentication($params);
$I->sendPOST('group/synchStatus', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();