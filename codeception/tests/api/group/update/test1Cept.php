<?php
$params = array(
	"id_group" => 15966,
	'name' => "Updated name from API",
	"description" => 'Hello world'
);

$I = new ApiGuy($scenario);
$I->wantTo('update a group with name and description');
$I->doStrongAuthentication($params);
$I->sendPOST('group/update', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();