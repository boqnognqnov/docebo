<?php
$params = array(
	'id_user' => 15448,
	'id_group' => 15968
);

$I = new ApiGuy($scenario);
$I->wantTo('assign a user to a group he is already part of');
$I->doStrongAuthentication($params);
$I->sendPOST('group/assign', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();