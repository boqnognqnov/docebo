<?php
$params = array(
	'id_user' => 15448,
	'id_group' => 15968
);

$I = new ApiGuy($scenario);
$I->wantTo('unassign a user from a group');
$I->doStrongAuthentication($params);
$I->sendPOST('group/unassign', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();