<?php
$path = dirname(__FILE__)."/../../../../../../import_groups_2.csv";

$params = array(
	'user_column' => 'username',
	'group_column' => 'groupname',
	'file_upload' => 'import_groups'
);

$I = new ApiGuy($scenario);
$I->wantTo('start bulk group synch with file upload & with names');
$I->doStrongAuthentication($params);
$I->sendPOST('group/bulkAssign', $params, array('import_groups' => $path));
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();