<?php
$params = array(
	'user_column' => 'id_user',
	'group_column' => 'id_group',
	'file_url' => 'http://docebo66-devel.localhost/import_groups_1.csv'
);

$I = new ApiGuy($scenario);
$I->wantTo('start bulk group synch with URL & with both IDs');
$I->doStrongAuthentication($params);
$I->sendPOST('group/bulkAssign', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();