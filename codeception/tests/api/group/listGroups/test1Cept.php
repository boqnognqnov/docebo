<?php
$params = array(
);

$I = new ApiGuy($scenario);
$I->wantTo('list all groups in the LMS');
$I->doStrongAuthentication($params);
$I->sendPOST('group/listGroups', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();