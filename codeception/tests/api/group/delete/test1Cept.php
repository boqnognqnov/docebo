<?php
$params = array(
	'id_group' => 15966
);

$I = new ApiGuy($scenario);
$I->wantTo('delete an existing group');
$I->doStrongAuthentication($params);
$I->sendPOST('group/delete', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();