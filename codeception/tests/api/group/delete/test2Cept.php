<?php
$params = array(
	'id_group' => 39616
);

$I = new ApiGuy($scenario);
$I->wantTo('delete an non existing group');
$I->doStrongAuthentication($params);
$I->sendPOST('group/delete', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();