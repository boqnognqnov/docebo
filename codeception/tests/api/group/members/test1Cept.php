<?php
$params = array(
	'id_group' => 15965
);

$I = new ApiGuy($scenario);
$I->wantTo('list all members of a group');
$I->doStrongAuthentication($params);
$I->sendPOST('group/members', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();