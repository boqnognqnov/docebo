<?php
$params = array(
	'user_column' => 'username',
	'group_column' => 'groupname',
	'file_url' => 'http://docebo66-devel.localhost/import_groups_2.csv'
);

$I = new ApiGuy($scenario);
$I->wantTo('start bulk group unassign with URL & with names');
$I->doStrongAuthentication($params);
$I->sendPOST('group/bulkUnassign', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();