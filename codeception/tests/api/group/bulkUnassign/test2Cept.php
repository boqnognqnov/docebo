<?php
$path = dirname(__FILE__)."/../../../../../../import_groups_1.csv";

$params = array(
	'user_column' => 'id_user',
	'group_column' => 'id_group',
	'file_upload' => 'import_groups'
);

$I = new ApiGuy($scenario);
$I->wantTo('start bulk group unassign with upload & with both IDs');
$I->doStrongAuthentication($params);
$I->sendPOST('group/bulkUnassign', $params, array('import_groups' => $path));
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();