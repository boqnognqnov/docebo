<?php
$params = array(
);

$I = new ApiGuy($scenario);
$I->wantTo('get lms info via the public API');
$I->doStrongAuthentication($params);
$I->sendPOST('public/getlmsinfo', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();