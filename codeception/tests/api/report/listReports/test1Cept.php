<?php
$params = array(
);

$I = new ApiGuy($scenario);
$I->wantTo('get the list of reports');
$I->doStrongAuthentication($params);
$I->sendPOST('report/listReports', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();