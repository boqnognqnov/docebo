<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUser = $I->grabUserByLevel(Yii::app()->user->level_admin);

$params = array(
	'id_user' => $powerUser,
	'item_type' => 'branch_name',
);

$I->wantTo('unassign branch from PU with a missing param (item_value)');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/unassignUsers', $params);
$I->seeResponseCodeIs(402);
$I->seeApiIsFailure();