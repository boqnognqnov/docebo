<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random branch
$orgChartTree = $I->grabCoreOrgChartTree();

$params = array(
	'item_type' => 'branch_name',
	'item_value' => $orgChartTree->coreOrgChartTranslatedDefaultLang->translation,
);

$I->wantTo('unassign branch from PU with missing id_user and userid');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/unassignUsers', $params);
$I->seeResponseCodeIs(404);
$I->seeApiIsFailure();