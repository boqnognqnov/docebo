<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUser = $I->grabUserByLevel(Yii::app()->user->level_admin);

$params = array(
	'id_user' => $powerUser,
	'item_type' => 'branch_name',
	//unexisting PK
	'item_value' => '2123456789',
);

$I->wantTo('unassign branch from PU with invalid branch_name value');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/unassignUsers', $params);
$I->seeResponseCodeIs(405);
$I->seeApiIsFailure();