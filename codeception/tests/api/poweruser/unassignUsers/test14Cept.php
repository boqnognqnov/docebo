<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUser = $I->grabUserByLevel(Yii::app()->user->level_admin);
//grab some random branch
$orgChartTree = $I->grabCoreOrgChartTree();

$params = array(
	'id_user' => $powerUser,
	'item_type' => 'branch_name',
	'item_value' => $orgChartTree->coreOrgChartTranslatedDefaultLang->translation,
);

//disable PU app if needed
$enabledPlugin = false;
if(PluginManager::isPluginActive("PowerUserApp")){
	PluginManager::deactivateAppByCodename("PowerUser");
	$enabledPlugin = true;
}

$I->wantTo('unassign branch from PU using id_user and branch_name while PU app is not active');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/unassignUsers', $params);
$I->seeResponseCodeIs(401);
$I->seeApiIsFailure();

//after test is done, we can enable the app again, if it was disabled beforehand
if($enabledPlugin){
	PluginManager::activateAppByCodename("PowerUser");
}