<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUser = $I->grabUserByLevel(Yii::app()->user->level_admin);
//grab some random group
$group = $I->grabGroup();

$params = array(
	'id_user' => $powerUser,
	'item_type' => 'id_group',
	'item_value' => $group->idst,
);

$I->wantTo('unassign group from PU using id_user and id_group');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/unassignUsers', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();