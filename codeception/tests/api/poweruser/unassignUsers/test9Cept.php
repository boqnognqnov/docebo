<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUser = $I->grabUserByLevel(Yii::app()->user->level_admin, true);
//grab some random group
$group = $I->grabGroup();

$params = array(
	'userid' => $powerUser->getClearUserId(),
	'item_type' => 'group_name',
	'item_value' => substr($group->groupid, 1),
);

$I->wantTo('unassign group from PU using userid and group_name');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/unassignUsers', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();