<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUser = $I->grabUserByLevel(Yii::app()->user->level_admin);

$params = array(
	'id_user' => $powerUser,
	'item_type' => 'id_user',
	//unexisting PK
	'item_value' => '2123456789',
);

$I->wantTo('unassign user from PU with invalid id_user value');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/unassignUsers', $params);
$I->seeResponseCodeIs(405);
$I->seeApiIsFailure();