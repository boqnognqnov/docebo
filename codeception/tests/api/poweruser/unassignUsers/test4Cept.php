<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUserId = $I->grabUserByLevel(Yii::app()->user->level_admin);
//grab some user that is assigned to this PU
$alreadyAssignedUser = $I->grabUserAssignedToPU($powerUserId);

$params = array(
	'id_user' => $powerUserId,
	'item_type' => 'userid',
	'item_value' => $alreadyAssignedUser->getClearUserId(),
);

$I->wantTo('unassign user from PU using id_user and userid');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/unassignUsers', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();