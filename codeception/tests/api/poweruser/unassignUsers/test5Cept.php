<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUser = $I->grabUserByLevel(Yii::app()->user->level_admin, true);
//grab some user that is assigned to this PU
$alreadyAssignedUser = $I->grabUserAssignedToPU($powerUser->idst);

$params = array(
	'userid' => $powerUser->getClearUserId(),
	'item_type' => 'userid',
	'item_value' => $alreadyAssignedUser->getClearUserId(),
);

$I->wantTo('unassign user from PU using userid and userid');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/unassignUsers', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();