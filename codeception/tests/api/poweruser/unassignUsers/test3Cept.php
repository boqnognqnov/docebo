<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random userid of a PU
$powerUser = $I->grabUserByLevel(Yii::app()->user->level_admin, true);
//grab some user that is assigned to this PU
$alreadyAssignedUser = $I->grabUserAssignedToPU($powerUser->idst);

$params = array(
	'id_user' => $powerUser->idst,
	'userid' => $powerUser->getClearUserId(),
	'item_type' => 'id_user',
	'item_value' => $alreadyAssignedUser->idst,
);

$I->wantTo('unassign user from PU using id_user+userid and id_user');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/unassignUsers', $params);
$I->seeResponseCodeIs(200);
//$I->seeApiIsSuccess();