<?php
//@group powerUser

$I = new ApiGuy($scenario);

$params = array(
	'job_id' => 'someRandomUnexistingJobID',
);

$I->wantTo('Get bulkAssign job status using a invalid job_id');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/bulkAssignStatus', $params);
$I->seeResponseCodeIs(402);
$I->seeApiIsFailure();