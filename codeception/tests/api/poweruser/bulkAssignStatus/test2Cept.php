<?php
//@group powerUser

$I = new ApiGuy($scenario);

$params = array(
);

$I->wantTo('Get bulkAssign job status without a job_id');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/bulkAssignStatus', $params);
$I->seeResponseCodeIs(401);
$I->seeApiIsFailure();