<?php
//@group powerUser

$I = new ApiGuy($scenario);

$params = array();

$I->wantTo('list PU profiles');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/listProfiles', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();