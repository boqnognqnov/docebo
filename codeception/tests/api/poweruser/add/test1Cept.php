<?php //TODO: fix this test, currently its failing
//@group powerUser

$params = array(
	'id_user' => 12356,
	'profile_name' => 'Line manager',
	'orgchart' => '16'
);

$I = new ApiGuy($scenario);
$I->wantTo('create a PU with an assigned profile & node');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/add', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();