<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some non existing userId
$powerUserId = -982;

$params = array(
	'id_user' => $powerUserId,
);

$I->wantTo('delete a non existing PU');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/delete', $params);
$I->seeResponseCodeIs(402);
$I->seeApiIsFailure();