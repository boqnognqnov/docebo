<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random normal user
$powerUserId = $I->grabUserByLevel(Yii::app()->user->level_user);

$params = array(
	'id_user' => $powerUserId,
);

$I->wantTo('delete a normal user');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/delete', $params);
$I->seeResponseCodeIs(403);
$I->seeApiIsFailure();