<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random user that is a PU
$powerUserId = $I->grabUserByLevel(Yii::app()->user->level_admin);
$params = array(
	'id_user' => $powerUserId,
);

//disable PU app if needed
$enabledPlugin = false;
if(PluginManager::isPluginActive("PowerUserApp")){
	PluginManager::deactivateAppByCodename("PowerUser");
	$enabledPlugin = true;
}

//Both tests are in the same file to ensure we use the same powerUserId
$I->wantTo('delete PU while PU app is not active');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/delete', $params);
$I->seeResponseCodeIs(401);
$I->seeApiIsFailure();

//after test is done, we can enable the app again, if it was disabled beforehand
if($enabledPlugin){
	PluginManager::activateAppByCodename("PowerUser");
}