<?php
//@group powerUser

$I = new ApiGuy($scenario);

$params = array();

$I->wantTo('delete PU without sending an ID');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/delete', $params);
$I->seeResponseCodeIs(402);
$I->seeApiIsFailure();