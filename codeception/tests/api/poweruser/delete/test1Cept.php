<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random user that is a PU
$powerUserId = $I->grabUserByLevel(Yii::app()->user->level_admin);

$params = array(
	'id_user' => $powerUserId,
);

$I->wantTo('delete a PU');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/delete', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();