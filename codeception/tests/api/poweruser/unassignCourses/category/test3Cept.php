<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUserId = $I->grabUserByLevel(Yii::app()->user->level_admin);
//grab categories
$category = $I->grabCategory($powerUserId, 3, true);

$params = array(
	'id_user' => $powerUserId,
	'items' => $I->buildItemsArray(array(
		PoweruserApiModule::COURSES_TYPE_ID_CATEGORY => $category
	)),
);

$I->wantTo('unassign multiple learning plan to PU using id_category ( assigned already )');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/unassignCourses', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();