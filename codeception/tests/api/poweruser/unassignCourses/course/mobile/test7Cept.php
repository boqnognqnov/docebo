<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUserId = $I->grabUserByLevel(Yii::app()->user->level_admin);
//grab a course from type mobile
$course = $I->grabCoursesByType(LearningCourse::TYPE_MOBILE, $powerUserId, 5, true);

$params = array(
	'id_user' => $powerUserId,
	'items' => $I->buildItemsArray(array(
		PoweruserApiModule::COURSES_TYPE_COURSE_CODE => $course
	)),
);

$I->wantTo('unassign multiple mobile courses from PU using course code ( assigned already )');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/unassignCourses', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();