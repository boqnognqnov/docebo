<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUserId = $I->grabUserByLevel(Yii::app()->user->level_admin);
//grab a course from type classroom
$course = $I->grabCoursesByType(LearningCourse::TYPE_CLASSROOM, $powerUserId, 1, true);

$params = array(
	'id_user' => $powerUserId,
	'items' => $I->buildItemsArray(array(
		PoweruserApiModule::COURSES_TYPE_ID_COURSE => $course
	)),
);

$I->wantTo('unassign one classroom course from PU using id_course ( assigned already )');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/unassignCourses', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();