<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUserId = $I->grabUserByLevel(Yii::app()->user->level_admin);
//grab a course from type webinar
$course = $I->grabCoursesByType(LearningCourse::TYPE_WEBINAR, $powerUserId);

$params = array(
	'id_user' => $powerUserId,
	'items' => $I->buildItemsArray(array(
		PoweruserApiModule::COURSES_TYPE_COURSE_CODE => $course
	)),
);

$I->wantTo('unassign multiple webinar courses from PU using course code ( not yet assigned )');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/unassignCourses', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();