<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUserId = $I->grabUserByLevel(Yii::app()->user->level_admin);
//grab a course from type elearning
$course = $I->grabCoursesByType(LearningCourse::TYPE_ELEARNING, $powerUserId, 5, true);

$params = array(
	'id_user' => $powerUserId,
	'items' => $I->buildItemsArray(array(
		PoweruserApiModule::COURSES_TYPE_ID_COURSE => $course
	)),
);

$I->wantTo('unassign multiple elearning courses from PU using id_course ( assigned already )');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/unassignCourses', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();