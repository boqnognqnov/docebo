<?php
//@group powerUser


$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUserId = $I->grabUserByLevel(Yii::app()->user->level_admin);

$params = array(
	'id_user' => $powerUserId,
);

$I->wantTo('assign one webinar course to PU using id_course ( assigned already ) with missing items parameter');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/assignCourses', $params);
$I->seeResponseCodeIs(402);
$I->seeApiIsFailure();