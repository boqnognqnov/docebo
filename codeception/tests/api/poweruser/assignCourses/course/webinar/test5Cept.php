<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUserId = $I->grabUserByLevel(Yii::app()->user->level_admin);
//grab a course from type webinar
$course = $I->grabCoursesByType(LearningCourse::TYPE_WEBINAR, $powerUserId, 1, true);

$params = array(
	'id_user' => $powerUserId,
	'items' => $I->buildItemsArray(array(
		PoweruserApiModule::COURSES_TYPE_COURSE_CODE => $course
	)),
);

$I->wantTo('assign one webinar course to PU using course code ( assigned already )');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/assignCourses', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();