<?php
//@group powerUser


$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUserId = $I->grabUserByLevel(Yii::app()->user->level_admin);
//grab a course from type webinar
$course = $I->grabCoursesByType(LearningCourse::TYPE_WEBINAR, $powerUserId, 1, true);

$params = array(
	'id_user' => '',
	'items' => $I->buildItemsArray(array(
		PoweruserApiModule::COURSES_TYPE_ID_COURSE => $course
	)),
);

$I->wantTo('assign one webinar course to PU using id_course ( assigned already ) with empty id_user parameter');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/assignCourses', $params);
$I->seeResponseCodeIs(402);
$I->seeApiIsFailure();