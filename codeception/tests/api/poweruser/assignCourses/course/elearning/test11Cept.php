<?php
//@group powerUser


$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUserId = $I->grabUserByLevel(Yii::app()->user->level_admin);
//grab a course from type elearning
$course = $I->grabCoursesByType(LearningCourse::TYPE_ELEARNING, $powerUserId, 1, true);

$params = array(
	'id_user' => '',
	'items' => $I->buildItemsArray(array(
		PoweruserApiModule::COURSES_TYPE_ID_COURSE => $course
	)),
);

$I->wantTo('assign one elearning course to PU using id_course ( assigned already ) with empty id_user parameter');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/assignCourses', $params);
$I->seeResponseCodeIs(402);
$I->seeApiIsFailure();