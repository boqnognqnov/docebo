<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUserId = $I->grabUserByLevel(Yii::app()->user->level_admin);
//grab a category
$category = $I->grabCategory($powerUserId, 1, true);

$params = array(
	'id_user' => $powerUserId,
	'items' => $I->buildItemsArray(array(
		PoweruserApiModule::COURSES_TYPE_ID_CATEGORY => $category
	)),
);

//disable PU app if needed
$enabledPlugin = false;
if(PluginManager::isPluginActive("PowerUserApp")){
	PluginManager::deactivateAppByCodename("PowerUser");
	$enabledPlugin = true;
}

$I->wantTo('assign a category to PU while PU app is not active');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/assignCourses', $params);
$I->seeResponseCodeIs(401);
$I->seeApiIsFailure();

//after test is done, we can enable the app again, if it was disabled beforehand
if($enabledPlugin){
	PluginManager::activateAppByCodename("PowerUser");
}