<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUserId = $I->grabUserByLevel(Yii::app()->user->level_admin);

$params = array(
	'id_user' => '',
);

$I->wantTo('assign a category to PU with missing items parameter');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/assignCourses', $params);
$I->seeResponseCodeIs(402);
$I->seeApiIsFailure();