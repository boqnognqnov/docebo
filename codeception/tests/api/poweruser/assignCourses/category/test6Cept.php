<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUserId = $I->grabUserByLevel(Yii::app()->user->level_admin);
//grab categories
$category = $I->grabCategory($powerUserId, 3);

$params = array(
	'items' => $I->buildItemsArray(array(
		PoweruserApiModule::COURSES_TYPE_ID_CATEGORY => $category
	)),
);

$I->wantTo('assign a category to PU with missing id_user parameter');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/assignCourses', $params);
$I->seeResponseCodeIs(402);
$I->seeApiIsFailure();