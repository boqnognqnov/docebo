<?php
//@group powerUser


$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUserId = $I->grabUserByLevel(Yii::app()->user->level_admin);
//grab a learning plan
$learningPlan = $I->grabLearningPlan($powerUserId, 1, true);

$params = array(
	'id_user' => $powerUserId,
	'items' => $I->buildItemsArray(array(
		PoweruserApiModule::COURSES_TYPE_ID_LEARNINGPLAN => $learningPlan
	)),
);

//disable PU app if needed
$enabledPlugin = false;
if(PluginManager::isPluginActive("PowerUserApp")){
	PluginManager::deactivateAppByCodename("PowerUser");
	$enabledPlugin = true;
}

$I->wantTo('assign a learning plan to PU while PU app is not active');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/assignCourses', $params);
$I->seeResponseCodeIs(401);
$I->seeApiIsFailure();

//after test is done, we can enable the app again, if it was disabled beforehand
if($enabledPlugin){
	PluginManager::activateAppByCodename("PowerUser");
}