<?php
//@group powerUser


$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUserId = $I->grabUserByLevel(Yii::app()->user->level_admin);
//grab a learning plan
$learningPlan = $I->grabLearningPlan($powerUserId, 1, true);

$params = array(
	'items' => $I->buildItemsArray(array(
		PoweruserApiModule::COURSES_TYPE_ID_LEARNINGPLAN => $learningPlan
	)),
);

$I->wantTo('assign one learning plan to PU using id_learningPlan ( assigned already ) with missing id_user parameter');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/assignCourses', $params);
$I->seeResponseCodeIs(402);
$I->seeApiIsFailure();