<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random normal user
$powerUserId = $I->grabUserByLevel(Yii::app()->user->level_user);
//grab some random PU profile
$powerUserProfile = $I->grabPowerUserProfile();

$params = array(
	'id_user' => $powerUserId,
	'profile_name' => $powerUserProfile
);

$I->wantTo('update a normal user');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/update', $params);
$I->seeResponseCodeIs(403);
$I->seeApiIsFailure();