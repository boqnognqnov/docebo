<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some non existing userId
$powerUserId = -982;
//grab some random PU profile
$powerUserProfile = $I->grabPowerUserProfile();

$params = array(
	'id_user' => $powerUserId,
	'profile_name' => $powerUserProfile
);

$I->wantTo('update a non existing PU');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/update', $params);
$I->seeResponseCodeIs(402);
$I->seeApiIsFailure();