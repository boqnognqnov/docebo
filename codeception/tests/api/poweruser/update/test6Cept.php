<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random normal user
$powerUserId = $I->grabUserByLevel(Yii::app()->user->level_admin);
//grab some non existing PU profile
$powerUserProfile = "nonExistingProfile1379";

$params = array(
	'id_user' => $powerUserId,
	'profile_name' => $powerUserProfile
);

$I->wantTo('update a normal user');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/update', $params);
$I->seeResponseCodeIs(404);
$I->seeApiIsFailure();