<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random user that is a PU
$powerUserId = $I->grabUserByLevel(Yii::app()->user->level_admin);
//grab some random PU profile
$powerUserProfile = $I->grabPowerUserProfile();

$params = array(
	'id_user' => $powerUserId,
	'profile_name' => $powerUserProfile
);

$I->wantTo('update a PU');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/update', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();