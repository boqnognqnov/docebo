<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random PU profile
$powerUserProfile = $I->grabPowerUserProfile();

$params = array(
	'profile_name' => $powerUserProfile
);

$I->wantTo('update PU without sending an ID');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/update', $params);
$I->seeResponseCodeIs(402);
$I->seeApiIsFailure();