<?php
//@group powerUser

$I = new ApiGuy($scenario);

$params = array(
);

$I->wantTo('Get bulkUpdate job status without a job_id');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/bulkUpdateStatus', $params);
$I->seeResponseCodeIs(401);
$I->seeApiIsFailure();