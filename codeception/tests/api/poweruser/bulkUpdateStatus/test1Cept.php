<?php
//@group powerUser

//test is always failing with err 500, no time to debug it now, best guess is that the files are missing locally
//TODO: a sample file is needed and we can complete the makeSafe function by mimicking such a file

$I = new ApiGuy($scenario);

$coreJob = $I->grabScheduledJob();

$params = array(
	'job_id' => $coreJob->hash_id,
);

$I->wantTo('Get bulkUpdate job status using a real job_id');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/bulkUpdateStatus', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();