<?php
//@group powerUser

$I = new ApiGuy($scenario);

$params = array(
	'job_id' => 'someRandomUnexistingJobID',
);

$I->wantTo('Get bulkUpdate job status using a invalid job_id');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/bulkUpdateStatus', $params);
$I->seeResponseCodeIs(402);
$I->seeApiIsFailure();