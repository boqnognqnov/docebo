<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUser = $I->grabUserByLevel(Yii::app()->user->level_admin, true);
//grab some random group
$group = $I->grabGroup();

$params = array(
	'userid' => $powerUser->getClearUserId(),
	'item_type' => 'id_group',
	'item_value' => $group->idst,
);

$I->wantTo('assign group to PU using userid and id_group');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/assignUser', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();