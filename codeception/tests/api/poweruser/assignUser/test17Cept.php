<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random normal user id
$normalUserId = $I->grabUserByLevel(Yii::app()->user->level_user);
//grab some random branch
$orgChartTree = $I->grabCoreOrgChartTree();

$params = array(
	'id_user' => $normalUserId,
	'item_type' => 'branch_name',
	'item_value' => $orgChartTree->coreOrgChartTranslatedDefaultLang->translation,
);

$I->wantTo('assign branch to a normal user');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/assignUser', $params);
$I->seeResponseCodeIs(403);
$I->seeApiIsFailure();