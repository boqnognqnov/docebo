<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUser = $I->grabUserByLevel(Yii::app()->user->level_admin, true);
//grab some random branch
$orgChartTree = $I->grabCoreOrgChartTree();

$params = array(
	'userid' => $powerUser->getClearUserId(),
	'item_type' => 'branch_name',
	'item_value' => $orgChartTree->coreOrgChartTranslatedDefaultLang->translation,
);

$I->wantTo('assign branch to PU using userid and branch_name');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/assignUser', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();