<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUser = $I->grabUserByLevel(Yii::app()->user->level_admin);
//grab some random branch
$orgChartTree = $I->grabCoreOrgChartTree();

$params = array(
	'id_user' => $powerUser,
	'item_type' => 'id_org',
	'item_value' => $orgChartTree->idOrg,
);

$I->wantTo('assign branch to PU using id_user and id_org');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/assignUser', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();