<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUser = $I->grabUserByLevel(Yii::app()->user->level_admin);

$model = CoreOrgChartTree::model()->findByPk("205498798");

$params = array(
	'id_user' => $powerUser,
	'item_type' => 'id_org',
	//unexisting PK
	'item_value' => '2123456789',
);

$I->wantTo('assign branch to PU with invalid id_org value');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/assignUser', $params);
$I->seeResponseCodeIs(405);
$I->seeApiIsFailure();