<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUser = $I->grabUserByLevel(Yii::app()->user->level_admin);
//grab some random branch
$orgChartTree = $I->grabCoreOrgChartTree();

$params = array(
	'id_user' => $powerUser,
	'item_value' => $orgChartTree->coreOrgChartTranslatedDefaultLang->translation,
);

$I->wantTo('assign branch to PU with a missing param (item_type)');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/assignUser', $params);
$I->seeResponseCodeIs(402);
$I->seeApiIsFailure();