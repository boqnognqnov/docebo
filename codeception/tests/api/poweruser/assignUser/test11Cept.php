<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUser = $I->grabUserByLevel(Yii::app()->user->level_admin, true);
//grab some random branch
$orgChartTree = $I->grabCoreOrgChartTree();

$params = array(
	'userid' => $powerUser->getClearUserId(),
	'item_type' => 'id_org',
	'item_value' => $orgChartTree->idOrg,
);

$I->wantTo('assign branch to PU using userid and id_org');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/assignUser', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();