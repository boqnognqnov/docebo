<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUserId = $I->grabUserByLevel(Yii::app()->user->level_admin);
//grab some random id user of a normal user
$normalUser = $I->grabUserByLevel(Yii::app()->user->level_user, true);

$params = array(
	'id_user' => $powerUserId,
	'item_type' => 'userid',
	'item_value' => $normalUser->getClearUserId(),
);

$I->wantTo('assign user to PU using id_user and userid');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/assignUser', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();