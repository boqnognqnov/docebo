<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUser = $I->grabUserByLevel(Yii::app()->user->level_admin);

$params = array(
	'id_user' => $powerUser,
	'item_type' => 'id_group',
	//unexisting PK
	'item_value' => '2123456789',
);

$I->wantTo('assign group to PU with invalid id_group value');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/assignUser', $params);
$I->seeResponseCodeIs(405);
$I->seeApiIsFailure();