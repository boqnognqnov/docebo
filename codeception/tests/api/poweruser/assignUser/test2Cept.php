<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random userid of a PU
$powerUser = $I->grabUserByLevel(Yii::app()->user->level_admin, true);
//grab some random id user of a normal user
$normalUserId = $I->grabUserByLevel(Yii::app()->user->level_user);

$params = array(
	'userid' => $powerUser->getClearUserId(),
	'item_type' => 'id_user',
	'item_value' => $normalUserId,
);

$I->wantTo('assign user to PU using userid and id_user');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/assignUser', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();