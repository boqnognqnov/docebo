<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUser = $I->grabUserByLevel(Yii::app()->user->level_admin, true);
//grab some random id user of a normal user
$normalUser = $I->grabUserByLevel(Yii::app()->user->level_user, true);

$params = array(
	'userid' => $powerUser->getClearUserId(),
	'item_type' => 'userid',
	'item_value' => $normalUser->getClearUserId(),
);

$I->wantTo('assign user to PU using userid and userid');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/assignUser', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();