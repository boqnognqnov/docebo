<?php
//@group powerUser

$I = new ApiGuy($scenario);

//grab some random id user of a PU
$powerUserId = $I->grabUserByLevel(Yii::app()->user->level_admin);
//grab some random id user of a normal user
$normalUserId = $I->grabUserByLevel(Yii::app()->user->level_user);

$params = array(
	'id_user' => $powerUserId,
	'item_type' => 'id_user',
	'item_value' => $normalUserId,
);

$I->wantTo('assign user to PU using id_user and id_user');
$I->doStrongAuthentication($params);
$I->sendPOST('poweruser/assignUser', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();