<?php
$params = array(
    'id_course' => 90
);

$I = new ApiGuy($scenario);
$I->wantTo('browse first-level learning objects (Course must contain just the Maritime SCORM)');
$params = array_merge($params, $I->doLightweightAuthentication($params));
$I->sendPOST('organization/listObjects', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContainsAttribute("objects");
$I->seeResponseAttributeIsArray("objects");
$I->seeResponseContains('"type":"folder"');