<?php
$params = array(
    'id_course' => 90,
    'id_org' => 285 // Maritime Navigation folder id
);

$I = new ApiGuy($scenario);
$I->wantTo('test listing subfolders of "Maritime Navigation"');
$params = array_merge($params, $I->doLightweightAuthentication($params));
$I->sendPOST('organization/listObjects', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContainsAttribute('objects');
$I->seeResponseAttributeIsArray("objects");
$I->seeResponseContains('"type":"folder"');
$I->seeResponseContains('"id_scormitem":');