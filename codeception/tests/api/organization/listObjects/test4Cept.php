<?php
$params = array(
    'id_course' => 90,
    'id_org' => 285,
    'id_scormitem' => 526
);

$I = new ApiGuy($scenario);
$I->wantTo('prove that there are no object under sco "References and Lesson Objective"');
$params = array_merge($params, $I->doLightweightAuthentication($params));
$I->sendPOST('organization/listObjects', $params);
$I->seeApiIsSuccess();
$I->seeResponseContainsAttribute("objects");
$I->seeResponseAttributeIsArray("objects");