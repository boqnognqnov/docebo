<?php
$params = array(
    'id_course' => 90,
    'id_org' => 285,
    'id_scormitem' => 523
);

$I = new ApiGuy($scenario);
$I->wantTo('test listing items under "Inland Rules of the Road (HTML Format)"');
$params = array_merge($params, $I->doLightweightAuthentication($params));
$I->sendPOST('organization/listObjects', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContainsAttribute("objects");
$I->seeResponseAttributeIsArray("objects");
$I->seeResponseContains('"id_scormitem":');