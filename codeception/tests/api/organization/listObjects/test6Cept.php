<?php
$params = array(
    'id_course' => 90222, // non existing course
);

$I = new ApiGuy($scenario);
$I->wantTo('prove that for invalid course ID 90222 returns an error');
$params = array_merge($params, $I->doLightweightAuthentication($params));
$I->sendPOST('organization/listObjects', $params);
$I->seeResponseCodeIs(202);
$I->seeApiIsFailure();
$I->seeErrorMessageIs("Unable to list course objects");