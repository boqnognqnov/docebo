<?php
$params = array(
    'id_user' => 12303,
	'id_scormitem' => 652
);

$I = new ApiGuy($scenario);
$I->wantTo('retrieve the list of SCO interactions for user');
$I->doStrongAuthentication($params);
$I->sendPOST('organization/getUserInteractions', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContainsAttribute("interactions");
$I->seeResponseAttributeIsArray("interactions");