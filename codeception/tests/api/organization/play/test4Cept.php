<?php
$params = array(
    'id_org' => 285,
    'id_scormitem' => 526
);

$I = new ApiGuy($scenario);
$I->wantTo('get the launch URL for scorm LO');
$params = array_merge($params, $I->doLightweightAuthentication($params));
$I->sendPOST('organization/play', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContains('"launch_url":');