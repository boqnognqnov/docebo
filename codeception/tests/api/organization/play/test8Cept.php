<?php
$params = array(
    'id_org' => 59,
	'id_scormitem' => 45,
	'id_user' => 12303
);

$I = new ApiGuy($scenario);
$I->wantTo('get the launch URL for scorm LO with id_user');
$I->doStrongAuthentication($params);
$I->sendPOST('organization/play', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContainsAttribute('launch_url');