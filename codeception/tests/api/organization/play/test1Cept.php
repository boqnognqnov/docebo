<?php
$params = array(
);

$I = new ApiGuy($scenario);
$I->wantTo('get the launch URL for LO without passing the id_org param');
$params = array_merge($params, $I->doLightweightAuthentication($params));
$I->sendPOST('organization/play', $params);
$I->seeResponseCodeIs(201);
$I->seeApiIsFailure();
$I->seeErrorMessageIs('Missing required param "id_org"');