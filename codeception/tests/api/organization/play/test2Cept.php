<?php
$params = array(
    'id_org' => 12345
);

$I = new ApiGuy($scenario);
$I->wantTo('get the launch URL for an invalid LO');
$params = array_merge($params, $I->doLightweightAuthentication($params));
$I->sendPOST('organization/play', $params);
$I->seeResponseCodeIs(201);
$I->seeApiIsFailure();
$I->seeErrorMessageIs("Can't find learning object with the provided id_org");