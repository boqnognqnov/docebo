<?php
$params = array(
    'id_org' => 285
);

$I = new ApiGuy($scenario);
$I->wantTo('get the launch URL for scorm LO without passing id_scormitem');
$params = array_merge($params, $I->doLightweightAuthentication($params));
$I->sendPOST('organization/play', $params);
$I->seeResponseCodeIs(203);
$I->seeApiIsFailure();
$I->seeErrorMessageIs("Bad or missing resource id");