<?php
$params = array(
    'id_user' => 15448,
	'id_course' => 98
);

$I = new ApiGuy($scenario);
$I->wantTo('reset the tracking of a user in a course');
$I->doStrongAuthentication($params);
$I->sendPOST('certification/resetCourseTrackings', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();