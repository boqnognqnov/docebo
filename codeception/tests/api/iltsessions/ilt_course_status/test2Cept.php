<?php
//@group enroll_iltenrollments_status
$params = array (
	'year' => date("Y"),
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling iltsessions/ilt_course_status with current year as a parameters');
$I->doOauth2Authentication();
$I->sendPOST('iltsessions/ilt_course_status', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

$I->seeResponseMatchesJsonType(array(
	'ilt_course_status' => array(
		array(
			'id_course' => 'integer',
			'course_code' => 'string',
			'course_title' => 'string',
			'course_description' => 'string',
			'course_language' => 'string',
			'status' => 'string',
			'selling' => 'boolean',
			'price' => 'float|integer',
			'subscribe_method' => 'string',
			'course_type' => 'string',
			'sub_start_date' => 'string|null',
			'sub_end_date' => 'string|null',
			'date_begin' => 'string',
			'date_end' => 'string',
			'course_link' => 'string',
			'waitinglist_users' => 'integer',
			'maximum_enrollments' => 'integer|null',
			'enrolled_user' => 'integer',
			'completed_user' => 'integer',
			'course_teacher' => 'array',
			'idSession' => 'integer',
			'session_name' => 'string',
			'session_start_date' => 'string',
			'session_end_date' => 'string',
			'session_location_list' => 'string',
			'session_country_list' => 'string',
			'session_teacher' => 'array',
			'total_hours' => 'integer',
			'session_max_enrollments' => 'integer',
			'enrolled_users' => 'integer'
		),
	),
	'success' => 'boolean'
));