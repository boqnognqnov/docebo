<?php
//@group enroll_iltenrollments_status
$params = array ();

$I = new ApiGuy($scenario);
$I->wantTo('API client calling iltsessions/ilt_course_status without any parameters');
$I->doOauth2Authentication();
$I->sendPOST('iltsessions/ilt_course_status', $params);
$I->seeResponseCodeIs(201);
$I->seeErrorMessageIs("Missing mandatory params");
$I->seeApiIsFailure();