<?php
$params = array(
	'id_course' => 9,
	'year' => 2016
);

$I = new ApiGuy($scenario);
$I->wantTo('list all ILT sessions for 2014');
$I->doOauth2Authentication();
$I->sendPOST('iltsessions/listAction', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();