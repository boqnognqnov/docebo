<?php
//@group iltsessions_ilt_course_status_count
$params = array ();

$I = new ApiGuy($scenario);
$I->wantTo('API client calling iltsessions/ilt_course_status_count without any parameters');
$I->doOauth2Authentication();
$I->sendPOST('iltsessions/ilt_course_status_count', $params);
$I->seeResponseCodeIs(201);
$I->seeErrorMessageIs("Missing mandatory params");
$I->seeApiIsFailure();