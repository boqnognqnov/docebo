<?php
//@group iltsessions_ilt_course_status_count
$params = array (
	'year' => date("Y"),
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling iltsessions/ilt_course_status_count with current year as a parameters');
$I->doOauth2Authentication();
$I->sendPOST('iltsessions/ilt_course_status_count', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

$I->seeResponseMatchesJsonType(array(
	'ilt_course_status_count' => 'integer',
	'success' => 'boolean'
));