<?php
//@group user_enrollments
//@group DOCEBO-241

$I = new ApiGuy($scenario);

$params = array(
    'id_user' => false,
);

$I = new ApiGuy($scenario);
$I->wantTo('get user courses with blank id_user');
$I->doStrongAuthentication($params);
$I->sendPOST('user/enrollments', $params);
$I->seeResponseCodeIs(210);
$I->seeErrorMessageIs('Invalid user specification');
$I->seeApiIsFailure();