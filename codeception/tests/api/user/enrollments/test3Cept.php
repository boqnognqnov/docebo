<?php
//@group user_enrollments
//@group DOCEBO-241

$I = new ApiGuy($scenario);

// Grab a already existing user from the DB
$userId = $I->grabUsersWithCourses(1);

$params = array(
    'id_user' => $userId[0],
	'classroom' => 1
);

$I = new ApiGuy($scenario);
$I->wantTo('get user courses for an existing users - only classroom');
$I->doStrongAuthentication($params);
$I->sendPOST('user/enrollments', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

$I->seeJsonResponseContainsOnlyOneCourseType(LearningCourse::TYPE_CLASSROOM);
