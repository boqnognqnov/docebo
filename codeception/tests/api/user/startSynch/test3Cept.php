<?php
$path = dirname(__FILE__)."/../../../../../../import_users.csv";

$params = array(
    'importMap[0]' => 'userid',
	'importMap[1]' => 'firstname',
	'importMap[2]' => 'lastname',
	'importMap[3]' => 'email',
	'importMap[4]' => 'language',
	'importMap[5]' => 'suspend',
	'separator'	   => 'comma',
	'first_row_header' => true,
	'file_upload' => 'file_upload_file',
	'insert_update' => true
);

$I = new ApiGuy($scenario);
$I->wantTo('start bulk user synch with URL');
$I->doStrongAuthentication($params);
$I->sendPOST('user/startSynch', $params, array('file_upload_file' => $path));
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();