<?php
$params = array(
	'importMap[0]' => 'userid',
	'importMap[1]' => 'firstname',
	'importMap[2]' => 'lastname',
	'importMap[3]' => 'email',
	'importMap[4]' => '497',
	'importMap[5]' => 'branch_code',
	'file_url' => 'http://docebo65-devel.localhost/import_users.csv',
	'insert_update' => true
);

$I = new ApiGuy($scenario);
$I->wantTo('start bulk user synch with URL');
$I->doStrongAuthentication($params);
$I->sendPOST('user/startSynch', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();