<?php
$params = array(
    'id_user' => 12303
);

$I = new ApiGuy($scenario);
$I->wantTo('get user courses for an existing user');
$I->doStrongAuthentication($params);
$I->sendPOST('user/userCourses', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
