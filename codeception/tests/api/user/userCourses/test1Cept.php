<?php
$params = array(
    'id_user' => 33333
);

$I = new ApiGuy($scenario);
$I->wantTo('get user courses for a non existing user');
$I->doStrongAuthentication($params);
$I->sendPOST('user/userCourses', $params);
$I->seeResponseCodeIs(210);
$I->seeApiIsFailure();
$I->seeErrorMessageIs('Invalid user specification');