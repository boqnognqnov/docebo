<?php
$I = new ApiGuy($scenario);
$params = array(
    'id_user' => $I->grabUserByLevel(Yii::app()->user->level_user),
);

$I->wantTo('get user stats for an existing regular user');
$I->doStrongAuthentication($params);
$I->sendPOST('user/getstat', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$monthsArray = array(
    1 => array( 'month' => 'integer', 'sessions' => 'integer' ),
    2 => array( 'month' => 'integer', 'sessions' => 'integer' ),
    3 => array( 'month' => 'integer', 'sessions' => 'integer' ),
    4 => array( 'month' => 'integer', 'sessions' => 'integer' ),
    5 => array( 'month' => 'integer', 'sessions' => 'integer' ),
    6 => array( 'month' => 'integer', 'sessions' => 'integer' ),
    7 => array( 'month' => 'integer', 'sessions' => 'integer' ),
    8 => array( 'month' => 'integer', 'sessions' => 'integer' ),
    9 => array( 'month' => 'integer', 'sessions' => 'integer' ),
    10 => array( 'month' => 'integer', 'sessions' => 'integer' ),
    11 => array( 'month' => 'integer', 'sessions' => 'integer' ),
    12 => array( 'month' => 'integer', 'sessions' => 'integer' ),
);
$I->seeResponseMatchesJsonType(array(
    'sessions_per_month' => $monthsArray,
    'most_viewed_courses' => array(
        0 => array( 'id_course' => 'integer', 'course_code' => 'string', 'course_name' => 'string', 'time_spent' => 'string', 'courseuser_status' => 'integer'),
        1 => array( 'id_course' => 'integer', 'course_code' => 'string', 'course_name' => 'string', 'time_spent' => 'string', 'courseuser_status' => 'integer'),
        2 => array( 'id_course' => 'integer', 'course_code' => 'string', 'course_name' => 'string', 'time_spent' => 'string', 'courseuser_status' => 'integer'),
    ),
    'user_performance' => array(
        'scores_per_month' => $monthsArray,
        'highest_score_points' => 'integer',
        'highest_score_points_max' => 'integer',
        'lowest_score_points' => 'integer',
        'lowest_score_points_max' => 'integer',
        'average_score_points' => 'integer',
        'average_score_points_max' => 'integer',
        'highest_score_percentage' => 'integer',
        'highest_score_percentage_max' => 'integer',
        'lowest_score_percentage' => 'integer',
        'lowest_score_percentage_max' => 'integer',
        'average_score_percentage' => 'integer',
        'average_score_percentage_max' => 'integer',
    ),
    'success' => 'boolean'
));