<?php
$params = array(
    'id_user' => 12799
);

$I = new ApiGuy($scenario);
$I->wantTo('get user stats for an existing regular user');
$I->doStrongAuthentication($params);
$I->sendPOST('user/getstat', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContainsAttribute("user_courses");
$I->seeResponseContainsAttribute("last_course");