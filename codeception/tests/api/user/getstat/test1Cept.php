<?php
$params = array(

);

$I = new ApiGuy($scenario);
$I->wantTo('get user stats with missing param id_user');
$I->doStrongAuthentication($params);
$I->sendPOST('user/getstat', $params);
$I->seeResponseCodeIs(210);
$I->seeApiIsFailure();
$I->seeErrorMessageIs('Invalid user specification');