<?php
$params = array(
    'id_user' => 12302
);

$I = new ApiGuy($scenario);
$I->wantTo('get user stats for a godadmin user');
$I->doStrongAuthentication($params);
$I->sendPOST('user/getstat', $params);
$I->seeResponseCodeIs(210);
$I->seeApiIsFailure();
$I->seeErrorMessageIs('Invalid user specification');