<?php
$params = array(
    'userid' => 'petter.enholm@snapper.no'
);

$I = new ApiGuy($scenario);
$I->wantTo('check a username that actually exists');
$I->doStrongAuthentication($params);
$I->sendPOST('user/checkUsername', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();