<?php
$params = array(
    'userid' => 'petter.enholm@snapper.no',
	'also_check_as_email'  => true
);

$I = new ApiGuy($scenario);
$I->wantTo('check a user by email');
$I->doStrongAuthentication($params);
$I->sendPOST('user/checkUsername', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
