<?php
$params = array(
    'userid' => 'staff.docebo',
	'also_check_as_email'  => true
);

$I = new ApiGuy($scenario);
$I->wantTo('check a user by email using oauth2 authorization');
$I->doOauth2Authentication();
$I->sendPOST('user/checkUsername', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
