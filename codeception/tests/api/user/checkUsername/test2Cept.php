<?php
$params = array(
    'userid' => 'nonexisting.user'
);

$I = new ApiGuy($scenario);
$I->wantTo('check a username that does not exist');
$I->doStrongAuthentication($params);
$I->sendPOST('user/checkUsername', $params);
$I->seeResponseCodeIs(201);
$I->seeApiIsFailure();
$I->seeErrorMessageIs("User not found");
