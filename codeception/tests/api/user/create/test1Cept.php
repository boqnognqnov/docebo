<?php
//@group user_create

$params = array(
	'userid' 	=> 'API_CreateUser - ' . substr(Docebo::randomHash(), 0, 10),
	'firstname'	=> 'testfirstname',
	'lastname'	=> 'testlastname',
	'password'	=> 'password',
	'email'		=> 'carmine.pizza@docebo.com',
	'valid'		=> '1',
	//'reg_code'=> 'Zh-gr', // orgchart branch Code
	//'reg_code_type'=> 'tree_man',// code -> orgchart branch
	//'role'=> 'user', //Docebo ROLE
	//'orgchart'=> 'Chinese',// Alternative method to insert user in the orgchart branch by name
	'fields' 	=> array (
		//'125' => 'om',
		'1' => 'Male',
		//'32' => '12/11/14',
	),
	'timezone' => 'Europe/Lisbon'

);

$I = new ApiGuy($scenario);
$I->wantTo('Create a user with some additional fields');
$I->doStrongAuthentication($params);
$I->sendPOST('user/create', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContainsAttribute("idst");