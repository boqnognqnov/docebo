<?php
//@group user_create

$params = array(
	'userid' 	=> 'API_CreateUser - ' . substr(Docebo::randomHash(), 0, 10),
	'firstname'	=> 'testfirstname',
	'lastname'	=> 'testlastname',
	'password'	=> '135poiuytrewq',
	'email'		=> 'carmine.pezza@docebo.com',
	'valid'		=> '1',
	'timezone' 	=> 'Europe/Lisbon',
	'id_group' 	=> -5,
	'disableNotifications' => 1
);

$I = new ApiGuy($scenario);
$I->wantTo('Create a user with reverse-keyboard-sequential letters for password and PASS INCORRECT id_group (force 205 response)');
$I->doStrongAuthentication($params);
$I->sendPOST('user/create', $params);
$I->seeResponseCodeIs(205);
$I->seeApiIsFailure();
$I->seeResponseContains('"message":"Group not found"');