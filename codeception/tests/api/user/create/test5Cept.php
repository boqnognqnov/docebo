<?php
//@group user_create

$params = array(
	'userid' 	=> 'API_CreateUser - ' . substr(Docebo::randomHash(), 0, 10),
	'firstname'	=> 'testfirstname',
	'lastname'	=> 'testlastname',
	'password'	=> '135abcdefgh',
	'email'		=> 'carmine.pezza@docebo.com',
	'valid'		=> '1',
	'timezone' => 'Europe/Lisbon',
	'disableNotifications' => 1
);

$I = new ApiGuy($scenario);
$I->wantTo('Create a user with sequential letters for password');
$I->doStrongAuthentication($params);
$I->sendPOST('user/create', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContainsAttribute("idst");