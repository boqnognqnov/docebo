<?php
//@group LB-3541

//test is related to bug: https://docebo.atlassian.net/browse/LB-3446
//TL;DR - reg_code + reg_code_type didn't work

// === setup ===
$time = time();
$code = 'codeceptionApiBranchCode_' . $time;

$setupGuy = new \Step\Api\SetupGuy($scenario);
$branchId = $setupGuy->createBranch(array(
	'english' => 'codeceptionApiBranch_' . $time,
), $code);

// === actual test ===
$I = new ApiGuy($scenario);

$params = array(
	'userid' 	=> 'codeceptionApiUser_' . $time,
	'firstname'	=> 'cc_firstname',
	'password'	=> 'cc_pwd',
	'reg_code' => $code,
	'reg_code_type' => 'tree_man',
	'valid'		=> '1',
);

$I->wantTo('create a user and assign him to a branch, using reg_code');
$I->doStrongAuthentication($params);
$I->sendPOST('user/create', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContainsAttribute("idst");

$userId = $I->getApiResult()->idst;

$I->amGoingTo('check the database to confirm that the user is assigned to the proper branch');
$user = CoreUser::model()->findByPk($userId);

PHPUnit_Framework_Assert::assertContains($branchId,$user->getOrgChartGroupsList(), 'User is not assigned to the proper node.');