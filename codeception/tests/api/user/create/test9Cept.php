<?php
//@group user_create

$I = new ApiGuy($scenario);
$group = $I->grabGroup();

$params = array(
	'userid' 	=> 'API_CreateUser - ' . substr(Docebo::randomHash(), 0, 10),
	'firstname'	=> 'testfirstname',
	'lastname'	=> 'testlastname',
	'password'	=> '135poiuytrewq',
	'email'		=> 'carmine.pezza@docebo.com',
	'valid'		=> '1',
	'timezone' 	=> 'Europe/Lisbon',
	'id_group' 	=> $group->idst,
	'disableNotifications' => 1
);

$I = new ApiGuy($scenario);
$I->wantTo('Create a user with reverse-keyboard-sequential letters for password and ADD him into group');
$I->doStrongAuthentication($params);
$I->sendPOST('user/create', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContainsAttribute("idst");