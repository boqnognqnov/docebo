q<?php
//@group user_edit

$I = new ApiGuy($scenario);

// Grab a already existing user from the DB
$user = $I->grabUsers(1);

$params = array(
	'id_user' 	=> implode("", $user),
    'password'  => '135qwertyuiop',
);
$I->wantTo('Edit a created user by changing the password with keyboard sequential letters');
$I->doStrongAuthentication($params);
$I->sendPOST('user/edit', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();