<?php
//@group user_edit

$I = new ApiGuy($scenario);

// Grab a already existing user from the DB
$user = $I->grabUsers(1);

$params = array(
	'id_user' 	=> implode("", $user),
	'fields' 	=> array (
		'1' => 'Senior Tester'
	)
);

$I->wantTo('Edit a created user with some additional fields and see if orgchart node is preserved');
$I->doStrongAuthentication($params);
$I->sendPOST('user/edit', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();