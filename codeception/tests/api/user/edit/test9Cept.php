<?php
$params = array(
	'userid' 	=> 'test_doceboxxx_9',
	'firstname'	=> 'testfirstname',
	'lastname'	=> 'testlastname',
	'password'	=> 'password',
	'email'		=> 'carmine.pezza@docebo.com',
	'valid'		=> '1',
	'timezone' 	=> 'Europe/Rome',
	'language'	=> 'english'
);

$I = new ApiGuy($scenario);
$I->wantTo('create a user');
$I->doStrongAuthentication($params);
$I->sendPOST('user/create', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContainsAttribute("idst");

$result = $I->getApiResult();
$params = array(
    'id_user' 	=> $result->idst,
	'language'	=> 'french'
);
$I->wantTo('edit a created user and changing his language to french');
$I->doStrongAuthentication($params);
$I->sendPOST('user/edit', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();