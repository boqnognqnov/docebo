<?php
//@group user_edit

$I = new ApiGuy($scenario);

// Grab a already existing user from the DB
$user = $I->grabUsers(1);

$params = array(
	'id_user' 	=> implode("", $user),
    'force_password_change'  => 0,
);
$I->wantTo('Edit a created user by making him, not to change his password at first login');
$I->doStrongAuthentication($params);
$I->sendPOST('user/edit', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();