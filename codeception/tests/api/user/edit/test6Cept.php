<?php
//@group user_edit

$I = new ApiGuy($scenario);

// Grab a already existing user from the DB
$user = $I->grabUsers(1);

$params = array(
    'id_user' 	=> implode("", $user),
    'password'  => '135gfedcba',
);
$I->wantTo('Edit a created user with changing the password with reversed sequential letters');
$I->doStrongAuthentication($params);
$I->sendPOST('user/edit', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();