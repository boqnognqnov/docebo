<?php
//@group user_edit

$I = new ApiGuy($scenario);

// Grab a already existing user from the DB
$user = $I->grabUsers(1);

$params = array(
    'id_user' 	=> implode("", $user),
	'id_group' 	=> -9,
);
$I->wantTo('Edit a created user by assigning him to non existing group (force response 205)');
$I->doStrongAuthentication($params);
$I->sendPOST('user/edit', $params);
$I->seeResponseCodeIs(205);
$I->seeApiIsFailure();
$I->seeResponseContains('"message":"Group not found"');