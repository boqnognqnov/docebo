<?php
//@group user_edit

$I = new ApiGuy($scenario);

// Grab a already existing user from the DB
$user = $I->grabUsers(1);

$params = array(
	'id_user' 	=> implode("", $user),
	'orgchart'  => 'Test2;Test3',
	'timezone' => 'Europe/Lisbon'
);
$I->wantTo('Edit a created user changing orgchart node');
$I->doStrongAuthentication($params);
$I->sendPOST('user/edit', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();