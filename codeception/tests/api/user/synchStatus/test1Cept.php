<?php
$params = array(
    'job_id' => '1f143c0079d4b8164b77caf4f45cd76185295623'
);

$I = new ApiGuy($scenario);
$I->wantTo('get bulk user synch status');
$I->doStrongAuthentication($params);
$I->sendPOST('user/synchStatus', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();