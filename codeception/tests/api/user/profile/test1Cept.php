<?php
$params = array(
    'id_user' => '18066'
);

$I = new ApiGuy($scenario);
$I->wantTo('get user profile for godamin');
$I->doStrongAuthentication($params);
$I->sendPOST('user/profile', $params);
$I->seeResponseCodeIs(201);
$I->seeApiIsFailure();