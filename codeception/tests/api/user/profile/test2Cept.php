<?php
$params = array(
    'id_user' => '17992'
);

$I = new ApiGuy($scenario);
$I->wantTo('get user profile for regular user');
$I->doStrongAuthentication($params);
$I->sendPOST('user/profile', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();