<?php
$params = array(
	'language' => 'simplified_chinese'
);

$I = new ApiGuy($scenario);
$I->wantTo('list all fields in the simplified chinese language');
$I->doStrongAuthentication($params);
$I->sendPOST('user/fields', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContainsAttribute("fields");