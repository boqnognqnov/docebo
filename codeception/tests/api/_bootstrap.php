<?php
// This is global bootstrap for autoloading

/* @var $this Codeception\Subscriber\Bootstrap */
/* @var $settings array */

/* Mockup most of the $_SERVER data */
$_SERVER['SERVER_NAME'] = $settings['modules']['config']['DoceboHelper']['server_name'];
$_SERVER['SCRIPT_FILENAME'] = dirname(__FILE__).'/../../../api/index.php';
$_SERVER['SCRIPT_NAME'] = '/lms/index.php';
$_SERVER['HTTP_HOST'] = $settings['modules']['config']['DoceboHelper']['server_name'];
$_SERVER['REMOTE_ADDR'] = '127.0.0.1'; //set some addr for the auditTrailListeners

/**
 *
 * This block must be the first one in all Yii1 App index.php files, e.g. admin/index.php, api/index.php, codeception's _bootstrap.php etc.
 *
 */
/***************************************************************************/
// Configuration helper
require_once(__DIR__. "/../../../common/config/config_helper.php");

// Include the Yii1 App specific main config!
// In its turn it loads common/config/main.php
$config = require(dirname(__FILE__).'/../../../api/protected/config/main.php');

// Include yii (must be before loading "alias" file below!)
$yii = dirname(__FILE__).'/../../../yii/yii.php';
require_once($yii);

// Define Yii aliases
require_once(_base_ . '/common/config/parts/alias.php');
/***************************************************************************/


// Manage cross origin headers
if ( strtolower($_SERVER["REQUEST_METHOD"]) == "options" ) {
	// options pre-call management
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET,PUT,DELETE,POST,OPTIONS');
	header('Access-Control-Allow-Credentials: true');
	header("Content-Type: text/plain");
	exit;
} else {
	// normal request that need cross-origin anyway
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET,PUT,DELETE,POST, OPTIONS');
	header('Access-Control-Allow-Credentials: true');
}
// end: Manage cross origin headers

// Create the web app
try {
	Yii::createWebApplication($config);
} catch (CDbException $e) {
	// Check DB connection and show maintenance page if failed
	// Failure could be because of bad host, bad username/password, port, etc.
	header('Content-Type: application/json; charset="UTF-8"');
	echo CJSON::encode(array('error' => 599, 'message' => 'Uh-Oh... This was not supposed to happen!'));
	die();
}
// --- Before Run custom operations ---

// Set Yii assets to the dedicated folder: <root>/lms/assets
// We need Absolute (!) Base URL, returned by Docebo::getRootBaseUrl(true)
// That's why we can't set this in main.php config file (Docebo is not yet created)
Yii::app()->assetManager->basePath 	= Yii::getPathOfAlias('lms.assets');
Yii::app()->assetManager->baseUrl 	= Docebo::getRootBaseUrl(true) . "/lms/assets";

// 1. Force timezone and date formats (UTC and mysql format)
date_default_timezone_set("UTC");
Yii::app()->user->forceTimezone("UTC");
Yii::app()->localtime->forceMysqlDatetimeFormats();

// 2. Resolve the platform languageds
Docebo::resolveLanguage();

// 3. Load the plugins (as Yii Modules)
PluginManager::loadActivePlugins();

//problem on windows is resolved using this row. Otherwise you can get this error
//include(PHP_Invoker.php): failed to open stream: No such file or directory
YiiBase::$enableIncludePath=false;
//another known problem: https://github.com/Codeception/Codeception/issues/2124