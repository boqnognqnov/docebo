<?php
$I = new ApiGuy($scenario);


$sql='SELECT t.* FROM learning_courseuser as t WHERE t.enrollment_fields IS NOT NULL';
$connection=Yii::app()->db;
$learningCourseUser=$connection->createCommand($sql)->queryRow();
codecept_debug($learningCourseUser);

$params = array(
    'id_user' => $learningCourseUser['idUser'],
    'course_id' => $learningCourseUser['idCourse']
);


$I->wantTo('unsubscribe user from a course using a course ID');
$I->doStrongAuthentication($params);
$I->sendPOST('course/deleteUserSubscription', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();