<?php
$params = array(
    'id_user' => '12302',
    'course_code' => 'prova_api_update'
);

$I = new ApiGuy($scenario);
$I->wantTo('unsubscribe user from a course using a course code');
$I->doStrongAuthentication($params);
$I->sendPOST('course/deleteUserSubscription', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();