<?php
$params = array(
    'course_code' => 'prova_api_004',
    'course_name' => 'prova_api_004',
    'course_descr' => 'descrizione',
    'category_id'  => 3
);

$I = new ApiGuy($scenario);
$I->wantTo('create a new course and assign it to a category');
$I->doStrongAuthentication($params);
$I->sendPOST('course/addCourse', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContainsAttribute('course_id');