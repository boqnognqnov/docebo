<?php
$params = array(
    'course_code' => 'prova_api_005'
);

$I = new ApiGuy($scenario);
$I->wantTo('create a new course without the required params');
$I->doStrongAuthentication($params);
$I->sendPOST('course/addCourse', $params);
$I->seeResponseCodeIs(201);
$I->seeApiIsFailure();