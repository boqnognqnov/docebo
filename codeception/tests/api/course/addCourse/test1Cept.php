<?php
$params = array(
    'course_code' => 'prova_api_003',
    'course_name' => 'prova_api_003',
    'course_descr' => 'descrizione',
    'sub_start_date' => '2014-05-20 00:00:00',
    'sub_end_date' => '2014-05-29 10:00:00'
);

$I = new ApiGuy($scenario);
$I->wantTo('create a new course');
$I->doStrongAuthentication($params);
$I->sendPOST('course/addCourse', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContainsAttribute('course_id');