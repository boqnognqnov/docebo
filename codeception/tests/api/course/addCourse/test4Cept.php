<?php
$params = array(
    'course_code' => 'prova_api_classroom',
	'course_name' => 'Classroom course via api',
	'course_descr' => 'descrizione classroom',
	'course_type' => 'classroom'
);

$I = new ApiGuy($scenario);
$I->wantTo('create a new classroom course');
$I->doStrongAuthentication($params);
$I->sendPOST('course/addCourse', $params);
$I->seeResponseCodeIs(201);
$I->seeApiIsFailure();