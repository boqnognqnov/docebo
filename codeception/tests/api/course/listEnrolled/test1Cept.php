<?php
//@group course_listEnrolled
//@group DOCEBO-241

$I = new ApiGuy($scenario);
$userId = $I->grabUsersWithCourses(1)[0];
$params = array(
	'id_user' => $userId
);

$I->wantTo('get a list of courses that the user is enrolled to');
$I->doStrongAuthentication($params);
$I->sendPOST('course/listEnrolled', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContains('{"courses":');

$I->amGoingTo("confirm these are courses that the user is really enrolled it");
$allCoursesFromApiResponse = $I->getApiResult()->courses;
$dbCourseIds = Yii::app()->db->createCommand(
	'SELECT idCourse FROM ' . LearningCourseuser::model()->tableName() . ' WHERE idUser = ' . $userId
)->queryColumn();

$I->amGoingTo("check the number of returned courses");
PHPUnit_Framework_Assert::assertCount(count($dbCourseIds), $allCoursesFromApiResponse, "Number of returned courses doesn't match expected number of courses from this category");

$apiCourseIds = array();
$I->amGoingTo("check all returned courses have the user assigned");
foreach($allCoursesFromApiResponse as $course){
	$apiCourseIds[] = $course->course_id;
}
//compare arrays, since they are the same count, then if one is subset of the other, then they are exactly the same
PHPUnit_Framework_Assert::assertArraySubset($dbCourseIds, $apiCourseIds, false, "Count of returned courses and courses from database, doesn't match");