<?php
//@group course_listEnrolled
//@group DOCEBO-241

//get some course and use its category
$course = LearningCourse::model()->findByAttributes(array(),'idCategory <> 0');
$idCategory = $course->idCategory;

$params = array(
	'id_user' => '',
);

$I = new ApiGuy($scenario);
$I->wantTo('get a list of courses that the user is enrolled to, providing a blank user id parameter');
$I->doStrongAuthentication($params);
$I->sendPOST('course/listEnrolled', $params);
$I->seeResponseCodeIs(401);
$I->seeApiIsFailure();