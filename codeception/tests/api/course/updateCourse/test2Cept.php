<?php
$params = array(
    'course_id' => 94,
    'course_code' => 'prova_api_update',
    'course_name' => 'prova_api_update',
    'course_descr' => 'descrizione updated',
    'category_id'  => 3
);

$I = new ApiGuy($scenario);
$I->wantTo('create a new course and assign it to a category');
$I->doStrongAuthentication($params);
$I->sendPOST('course/updateCourse', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContainsAttribute('course_id');