<?php
$params = array(
    'course_id' => 2222,
    'course_code' => 'prova_api_005'
);

$I = new ApiGuy($scenario);
$I->wantTo('update a non existing course');
$I->doStrongAuthentication($params);
$I->sendPOST('course/updateCourse', $params);
$I->seeResponseCodeIs(202);
$I->seeApiIsFailure();
$I->seeErrorMessageIs('Invalid specified course');