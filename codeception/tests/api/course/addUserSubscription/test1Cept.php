<?php

$I = new ApiGuy($scenario);


$newUser = $I->grabNewUser();
$idCourse= $I->grabCourses(1)[0];

$fieldData=$I->createFakeErollmentFields(3);


$params = array(
    'id_user' => $newUser['idst'],
    'course_id'=>$idCourse,
    'enrollment_fields'=>json_encode($fieldData)
);
codecept_debug($params);


$I->wantTo('subscribe user to a course using an ID');
$I->doStrongAuthentication($params);
$I->sendPOST('course/addUserSubscription', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();