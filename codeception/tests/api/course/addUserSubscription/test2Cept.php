<?php
$params = array(
    'id_user' => '12302',
    'course_code' => 'prova_api_update'
);

$I = new ApiGuy($scenario);
$I->wantTo('subscribe user to a course using an ID');
$I->doStrongAuthentication($params);
$I->sendPOST('course/addUserSubscription', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();