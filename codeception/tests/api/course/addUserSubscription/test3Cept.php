<?php
//@group course_addUserSubscription
$I = new ApiGuy($scenario);
$userId = $I->grabNewUser()->idst;
$courseId = $I->grabCourses(1, false, LearningCourse::TYPE_ELEARNING)[0];

$params = array(
	'id_user' => $userId,
	'course_id' => $courseId,
	'date_begin_validity' => '2015-12-30 11:11:11',
	'date_expire_validity' => '2016-01-30 11:11:11', // Date format yyyy-MM-dd HH:mm:ss
);

$I->wantTo('subscribe user to a course using an ID');
$I->doStrongAuthentication($params);
$I->sendPOST('course/addUserSubscription', $params);

$savedDates = Yii::app()->db->createCommand()
	->select('date_begin_validity as begin, date_expire_validity as end')
	->from(LearningCourseuser::model()->tableName())
	->where('idUser = :user AND idCourse = :course', array(':user' => $userId, ':course' => $courseId))->queryRow();

PHPUnit_Framework_Assert::assertTrue($savedDates['begin'] == $params['date_begin_validity']
	&& $savedDates['end'] == $params['date_expire_validity'], 'Dates are not saved correctly!');

$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
