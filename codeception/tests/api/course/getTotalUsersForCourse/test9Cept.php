<?php
//@group course_getTotalUsersForCourse

$I = new ApiGuy($scenario);
$courses = $I->grabCourses(1);

$params = array(
    'id_course' => implode(',', $courses),
    'exclude_suspended' => 0,
);

$I = new ApiGuy($scenario);
$I->wantTo('Get count of enrolled users in course (id_course PASSED, exclude_suspended PASSED as false)');
$I->doStrongAuthentication($params);
$I->sendPOST('course/getTotalUsersForCourse', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContains('"count":');