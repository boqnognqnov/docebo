<?php
//@group course_getTotalUsersForCourse

$params = array(
);

$I = new ApiGuy($scenario);
$I->wantTo('Get count of enrolled users in course, NO DATA PASSED (force error 401)');
$I->doStrongAuthentication($params);
$I->sendPOST('course/getTotalUsersForCourse', $params);
$I->seeResponseCodeIs(401);
$I->seeApiIsFailure();
$I->seeResponseContains('"message":"No Course provided"');