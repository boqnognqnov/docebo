<?php
//@group course_getTotalUsersForCourse

$I = new ApiGuy($scenario);
$courses = $I->grabCourses(1);

$params = array(
    'id_course' => implode(',', $courses),
);

$I = new ApiGuy($scenario);
$I->wantTo('Get count of enrolled users in course (id_course PASSED correctly)');
$I->doStrongAuthentication($params);
$I->sendPOST('course/getTotalUsersForCourse', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContains('"count":');