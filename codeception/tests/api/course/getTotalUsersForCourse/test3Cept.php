<?php
//@group course_getTotalUsersForCourse

$params = array(
    'id_course' => -5,
);

$I = new ApiGuy($scenario);
$I->wantTo('Get count of enrolled users in course (id_course PASSED incorrect, force 402 response)');
$I->doStrongAuthentication($params);
$I->sendPOST('course/getTotalUsersForCourse', $params);
$I->seeResponseCodeIs(402);
$I->seeApiIsFailure();
$I->seeResponseContains('"message":"Course not found"');