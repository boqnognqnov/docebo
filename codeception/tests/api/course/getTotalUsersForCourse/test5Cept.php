<?php
//@group course_getTotalUsersForCourse

$params = array(
    'exclude_suspended' => 0,
);

$I = new ApiGuy($scenario);
$I->wantTo('Get count of enrolled users in course (exclude_suspended PASSED as false, force 401 response)');
$I->doStrongAuthentication($params);
$I->sendPOST('course/getTotalUsersForCourse', $params);
$I->seeResponseCodeIs(401);
$I->seeApiIsFailure();
$I->seeResponseContains('"message":"No Course provided"');