<?php
//@group course_getTotalUsersForCourse

$I = new ApiGuy($scenario);
$courses = $I->grabCourses(1);
$group = $I->grabGroup();

$params = array(
    'id_course' => implode(',', $courses),
    'id_group' => $group->idst,
);

$I = new ApiGuy($scenario);
$I->wantTo('Get count of enrolled users in course (id_course and id_group are PASSED)');
$I->doStrongAuthentication($params);
$I->sendPOST('course/getTotalUsersForCourse', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContains('"count":');