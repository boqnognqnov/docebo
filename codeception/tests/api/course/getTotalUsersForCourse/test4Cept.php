<?php
//@group course_getTotalUsersForCourse

$I = new ApiGuy($scenario);
$group = $I->grabGroup();

$params = array(
    'id_group' => $group->idst,
);

$I = new ApiGuy($scenario);
$I->wantTo('Get count of enrolled users in course (id_group PASSED, force 401 response)');
$I->doStrongAuthentication($params);
$I->sendPOST('course/getTotalUsersForCourse', $params);
$I->seeResponseCodeIs(401);
$I->seeApiIsFailure();
$I->seeResponseContains('"message":"No Course provided"');