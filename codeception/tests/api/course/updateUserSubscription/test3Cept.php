<?php
//@group course_updateUserSubscription
$I = new ApiGuy($scenario);
$courseId = $I->grabCourses(1)[0];
$userId = $I->getUsersIdsInCourse($courseId)[0];

$params = array(
    'id_user' => $userId,
    'course_id' => $courseId,
    'user_level' => 'instructor',
	'date_begin_validity' => '2015-12-30 22:22:11',
	'date_expire_validity' => '2016-01-30 22:22:11', // Date format yyyy-MM-dd HH:mm:ss
);

$I->wantTo('update a user subscription setting level to instructor and setting enrollment validity dates');
$I->doStrongAuthentication($params);
$I->sendPOST('course/updateUserSubscription', $params);

$savedDates = Yii::app()->db->createCommand()
		->select('date_begin_validity as begin, date_expire_validity as end')
		->from(LearningCourseuser::model()->tableName())
		->where('idUser = :user AND idCourse = :course', array(':user' => $userId, ':course' => $courseId))->queryRow();

PHPUnit_Framework_Assert::assertTrue($savedDates['begin'] == $params['date_begin_validity']
		&& $savedDates['end'] == $params['date_expire_validity'], 'Dates are not saved correctly!');

$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();