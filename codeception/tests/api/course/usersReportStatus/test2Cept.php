<?php
//@group usersReportStatus

/**
 * Things here are a bit more complex. We need to create a job, check its status while its active.
 * After the job finishes we need to check again to see that the job is complete + check some of the returned data
 */

$I = new ApiGuy($scenario);
$I->wantTo('Checks the statuses of a user interactions report using OAuth2 authentication');

//========================================================================================================
$I->amGoingTo('create a job');
$userIds = $I->grabUsers(150); //using 150 users so the job can take a while longer and we can catch it while its running
$params = array(
	'user_ids' => implode(',', $userIds),
	'interactions'=>'1',
);

$I->doOauth2Authentication();
$I->sendPOST('course/startUsersReport', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

//handle the response
$I->seeResponseContainsAttribute('job_id');
$response = json_decode($I->grabResponse());

//========================================================================================================
$I->amGoingTo('check if the job we just created is running');
sleep(2); //wait a bit so the job has a chance to start running
$params = array(
	'job_id' => $response->job_id,
);

$I->doOauth2Authentication();
$I->sendPOST('course/usersReportStatus', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

$I->seeResponseContainsAttribute('status');
$I->seeResponseContains('"status":"running"');

//========================================================================================================
$numberOfChecks = 3;
$I->amGoingTo('continue checking the job until it finishes');
do{
	$I->amGoingTo('wait 3 seconds');
	sleep(3); //wait a while before each check

	//using the same $params as the job is the same
	$I->doOauth2Authentication();
	$I->sendPOST('course/usersReportStatus', $params);
	$I->seeResponseCodeIs(200);
	$I->seeApiIsSuccess();

	$response = json_decode($I->grabResponse());
	$numberOfChecks--;

	if(!$numberOfChecks){
		\PHPUnit_Framework_Assert::fail('The job was not completed within 30 seconds - either its too slow, or its bugged at always running status!');
	}
}while($response->status == 'running' && $numberOfChecks);

$I->seeResponseAttributeIsArray('data');
$I->seeResponseAttributeIsArray('failed_users');