<?php
//@group usersReportStatus

// Cause a failing api call by passing a INVALID job_id param to the API

$params = array(
	 'job_id' => 'do_not_change_me', // <- pass some random jibberish here, make sure is not a valid JOB id
);

$I = new ApiGuy($scenario);
$I->wantTo('Cause an intentional failure by not passing a job_id to the course/usersReportStatus api call');
$I->doStrongAuthentication($params);
$I->sendPOST('course/usersReportStatus', $params);
$I->seeResponseCodeIs(402);
$I->seeApiIsFailure();

$I->seeErrorMessageIs("Job not found");