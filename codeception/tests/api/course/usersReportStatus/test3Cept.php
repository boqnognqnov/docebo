<?php
//@group usersReportStatus

// Cause a failing api call by not passing a job_id param to the API

$params = array(
	// 'job_id' => 'XXXXXXX', // <- intentionally not passed to cause failure
);

$I = new ApiGuy($scenario);
$I->wantTo('Cause an intentional failure by not passing a job_id to the course/usersReportStatus api call');
$I->doStrongAuthentication($params);
$I->sendPOST('course/usersReportStatus', $params);
$I->seeResponseCodeIs(401);
$I->seeApiIsFailure();

$I->seeErrorMessageIs("No Job ID provided");