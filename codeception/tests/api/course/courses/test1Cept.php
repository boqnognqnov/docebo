<?php
//@group course_courses
//@group DOCEBO-241

$params = array(
);

$I = new ApiGuy($scenario);
$I->wantTo('list courses without category filter');
$I->doStrongAuthentication($params);
$I->sendPOST('course/courses', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContains('{"courses":');