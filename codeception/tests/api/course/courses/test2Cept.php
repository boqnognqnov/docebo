<?php
//@group course_courses
//@group DOCEBO-241

//get some course and use its category
$course = LearningCourse::model()->findByAttributes(array(),'idCategory <> 0');
$idCategory = $course->idCategory;

$params = array(
    'category' => $idCategory
);

$I = new ApiGuy($scenario);
$I->wantTo('list courses with category filter');
$I->doStrongAuthentication($params);
$I->sendPOST('course/courses', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContains('{"courses":');

$I->amGoingTo("confirm the returned course");
$allCoursesFromApiResponse = $I->getApiResult()->courses;
$dbCourseIds = Yii::app()->db->createCommand(
	'SELECT idCourse FROM ' . LearningCourse::model()->tableName() . ' WHERE idCategory = ' . $idCategory
)->queryColumn();

$I->amGoingTo("check the number of returned courses");
PHPUnit_Framework_Assert::assertCount(count($dbCourseIds), $allCoursesFromApiResponse, "Number of returned courses doesn't match expected number of courses from this category");

$apiCourseIds = array();
$I->amGoingTo("check all returned courses from the proper category");
foreach($allCoursesFromApiResponse as $course){
	$apiCourseIds[] = $course->course_id;
}
//compare arrays, since they are the same count, then if one is subset of the other, then they are exactly the same
PHPUnit_Framework_Assert::assertArraySubset($dbCourseIds, $apiCourseIds, false, "Count of returned courses and courses from database, doesn't match");