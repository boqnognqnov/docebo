<?php
$params = array(
    'course_id' => 95333
);

$I = new ApiGuy($scenario);
$I->wantTo('delete a course with an invalid ID');
$I->doStrongAuthentication($params);
$I->sendPOST('course/deleteCourse', $params);
$I->seeResponseCodeIs(201);
$I->seeErrorMessageIs('Invalid specified course');