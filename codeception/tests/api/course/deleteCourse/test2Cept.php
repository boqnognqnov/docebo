<?php
$params = array(
    'course_id' => 95
);

$I = new ApiGuy($scenario);
$I->wantTo('delete a course with a valid ID');
$I->doStrongAuthentication($params);
$I->sendPOST('course/deleteCourse', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();