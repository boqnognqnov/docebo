<?php
$params = array(
	'course_id' => 142
);

$I = new ApiGuy($scenario);
$I->wantTo('delete a classroom course');
$I->doStrongAuthentication($params);
$I->sendPOST('course/deleteCourse', $params);
$I->seeResponseCodeIs(201);
$I->seeApiIsFailure();
$I->seeErrorMessageIs('Invalid specified course');