<?php
$params = array(
);

$I = new ApiGuy($scenario);
$I->wantTo('delete a course with a missing ID');
$I->doStrongAuthentication($params);
$I->sendPOST('course/deleteCourse', $params);
$I->seeResponseCodeIs(201);
$I->seeApiIsFailure();
$I->seeErrorMessageIs('Invalid specified course');