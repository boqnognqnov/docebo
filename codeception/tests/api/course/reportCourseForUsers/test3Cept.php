<?php
//@group course_reportCourseForUsers

$params = array(
    'id_course' => -5,
);

$I = new ApiGuy($scenario);
$I->wantTo('Get report of enrolled users in course (id_course PASSED incorrect, force 402 response)');
$I->doStrongAuthentication($params);
$I->sendPOST('course/reportCourseForUsers', $params);
$I->seeResponseCodeIs(402);
$I->seeApiIsFailure();
$I->seeResponseContains('"message":"Course not found"');