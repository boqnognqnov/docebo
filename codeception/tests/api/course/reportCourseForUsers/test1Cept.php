<?php
//@group course_reportCourseForUsers

$params = array(
);

$I = new ApiGuy($scenario);
$I->wantTo('Get report of enrolled users in course, (NO DATA PASSED, force error 401)');
$I->doStrongAuthentication($params);
$I->sendPOST('course/reportCourseForUsers', $params);
$I->seeResponseCodeIs(401);
$I->seeApiIsFailure();
$I->seeResponseContains('"message":"No Course provided"');