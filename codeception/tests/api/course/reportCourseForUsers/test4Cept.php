<?php
//@group course_reportCourseForUsers

$I = new ApiGuy($scenario);
$group = $I->grabGroup();

$params = array(
    'id_group' => $group->idst,
);

$I = new ApiGuy($scenario);
$I->wantTo('Get report of enrolled users in course (id_group PASSED, force 401 response)');
$I->doStrongAuthentication($params);
$I->sendPOST('course/reportCourseForUsers', $params);
$I->seeResponseCodeIs(401);
$I->seeApiIsFailure();
$I->seeResponseContains('"message":"No Course provided"');