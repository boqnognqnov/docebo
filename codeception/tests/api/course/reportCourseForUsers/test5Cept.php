<?php
//@group course_reportCourseForUsers

$params = array(
    'exclude_suspended' => 0,
);

$I = new ApiGuy($scenario);
$I->wantTo('Get report of enrolled users in course (exclude_suspended PASSED as false, force 401 response)');
$I->doStrongAuthentication($params);
$I->sendPOST('course/reportCourseForUsers', $params);
$I->seeResponseCodeIs(401);
$I->seeApiIsFailure();
$I->seeResponseContains('"message":"No Course provided"');