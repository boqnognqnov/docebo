<?php
//@group course_reportCourseForUsers

$I = new ApiGuy($scenario);
$courses = $I->grabCourses(1);

$params = array(
    'id_course' => implode(',', $courses),
    'id_group' => -5,
);

$I = new ApiGuy($scenario);
$I->wantTo('Get report of enrolled users in course (id_course PASSED id_group PASSED INCORRECT, force 402 response )');
$I->doStrongAuthentication($params);
$I->sendPOST('course/reportCourseForUsers', $params);
$I->seeResponseCodeIs(403);
$I->seeApiIsFailure();
$I->seeResponseContains('"message":"Group not found"');