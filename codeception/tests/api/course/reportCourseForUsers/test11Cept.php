<?php
//@group course_reportCourseForUsers

$I = new ApiGuy($scenario);
$courses = $I->grabCourses(1);
$group = $I->grabGroup();

$params = array(
    'id_course' => implode(',', $courses),
    'id_group' => $group->idst,
    'exclude_suspended' => 1,
);

$I = new ApiGuy($scenario);
$I->wantTo('Get report of enrolled users in course (id_course PASSED, id_group PASSED, exclude_suspended PASSED as true)');
$I->doStrongAuthentication($params);
$I->sendPOST('course/reportCourseForUsers', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContains('"report":');