<?php
$params = array(
    'id_user' => '12302',
    'reg_code' => 'abcde'
);

$I = new ApiGuy($scenario);
$I->wantTo('subscribe a user using an autoregistration code');
$I->doStrongAuthentication($params);
$I->sendPOST('course/subscribeUserWithCode', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();