<?php
$params = array(
);

$I = new ApiGuy($scenario);
$I->wantTo('list courses without category filter');
$I->doStrongAuthentication($params);
$I->sendPOST('course/listCourses', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContains('"0":{"course_info":');