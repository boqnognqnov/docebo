<?php
$params = array(
    'category' => 2
);

$I = new ApiGuy($scenario);
$I->wantTo('list courses with category filter');
$I->doStrongAuthentication($params);
$I->sendPOST('course/listCourses', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseContains('"0":{"course_info":');