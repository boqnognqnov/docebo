<?php
//@group startUsersReport

$I = new ApiGuy($scenario);
$userIds = $I->grabUsers(1);

$params = array(
	'user_ids' => implode(',', $userIds),
	'interactions'=>'1',
);

$I->wantTo('Start async report creation for user interactions in SCORM using OAuth2 authentication and only one user');
$I->doOauth2Authentication();
$I->sendPOST('course/startUsersReport', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

// Validate response from API
$I->seeResponseContainsAttribute('job_id');