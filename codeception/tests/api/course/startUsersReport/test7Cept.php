<?php
//@group startUsersReport

$params = array(
	 'user_ids' => '',
);

$I = new ApiGuy($scenario);
$I->wantTo('Start a 100% failing async report creation for user interactions in SCORM, using the old legacy header authentication, using a blank user_ids parameter');
$I->doStrongAuthentication($params);
$I->sendPOST('course/startUsersReport', $params);
$I->seeResponseCodeIs(402);
$I->seeApiIsFailure();

$I->seeErrorMessageIs('No user IDs provided');