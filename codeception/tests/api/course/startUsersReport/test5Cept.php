<?php
//@group startUsersReport

$params = array(

);

$I = new ApiGuy($scenario);
$I->wantTo('Start a 100% failing async report creation for user interactions in SCORM, using the old legacy header authentication, without providing a parameter');
$I->doStrongAuthentication($params);
$I->sendPOST('course/startUsersReport', $params);
$I->seeResponseCodeIs(401);
$I->seeApiIsFailure();

$I->seeErrorMessageIs('Invalid "user_ids" parameter');