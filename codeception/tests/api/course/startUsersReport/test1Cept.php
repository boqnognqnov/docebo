<?php
//@group startUsersReport

$I = new ApiGuy($scenario);
$userIds = $I->grabUsers(1);

$params = array(
	'user_ids' => implode(',', $userIds),
	'interactions'=>'1',
);

$I->wantTo('Start async report creation for user interactions in SCORM using the old legacy header authentication and only one user');
$I->doStrongAuthentication($params);
$I->sendPOST('course/startUsersReport', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

// Validate response from API
$I->seeResponseContainsAttribute('job_id');