<?php
//@group learningplan_listPlans

$params = array(
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling learningplan/listPlans with no parameters');
$I->doOauth2Authentication();
$I->sendPOST('learningplan/listPlans', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

$I->seeResponseContains('"id_path":');
$I->seeResponseContains('"path_code":');
$I->seeResponseContains('"path_credits":');
$I->seeResponseContains('"path_description":');
$I->seeResponseContains('"visible_in_catalogs":');
$I->seeResponseContains('"on_sale":');