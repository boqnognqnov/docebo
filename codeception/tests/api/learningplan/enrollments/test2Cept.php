<?php
//@group learningplan_enrollments

$params = array (
    'ids_path' => "8,10"
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling learningplan/enrollments using learning plan filters');
$I->doOauth2Authentication();
$I->sendPOST('learningplan/enrollments', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();