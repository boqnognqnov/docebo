<?php
//@group learningplan_enrollments

$params = array();

$I = new ApiGuy($scenario);
$I->wantTo('API client calling learningplan/enrollments with no parameters');
$I->doOauth2Authentication();
$I->sendPOST('learningplan/enrollments', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();