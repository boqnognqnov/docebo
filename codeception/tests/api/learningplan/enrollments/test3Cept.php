<?php
//@group learningplan_enrollments

$params = array(
    'path_codes' => '001,002'
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling learningplan/enrollments using learning plan filters');
$I->doOauth2Authentication();
$I->sendPOST('learningplan/enrollments', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();