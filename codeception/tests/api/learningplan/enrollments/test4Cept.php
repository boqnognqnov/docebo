<?php
//@group learningplan_enrollments

$params = array(
    'user_ids' => '14070,14117'
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling learningplan/enrollments using users filters');
$I->doOauth2Authentication();
$I->sendPOST('learningplan/enrollments', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();