<?php
$I = new ApiGuy($scenario);
$newUser = $I->grabNewUser();

$fieldData=$I->createFakeErollmentFields(3);

$params = array(
    'id_user' => $newUser['idst'],
	'id_learningplan' => 110,
	'enrollment_fields'=>json_encode($fieldData)
);
$I->doStrongAuthentication($params);
$I->sendPOST('learningplan/enroll', $params);


$I->wantTo('unenroll a user from a learning plan');
$I->doStrongAuthentication($params);
$I->sendPOST('learningplan/unenroll', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();