<?php
//@group ecommerce_countTransactions
$params = array (
	'status'=>'accepted'
);

$I = new ApiGuy($scenario);
$I->wantTo('Get count of paid transactions');
$I->doOauth2Authentication();
$I->sendPOST('ecommerce/countTransactions', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

$result = json_decode($I->grabResponse())->count;
$expectedCount = Yii::app()->db->createCommand()->select('COUNT(id_trans)')
	->from(EcommerceTransaction::model()->tableName())
	->where('paid = 1')->queryScalar();
PHPUnit_Framework_Assert::assertEquals($expectedCount,$result);

$I->seeResponseMatchesJsonType(array(
	'count' => 'integer',
	'success' => 'boolean',
));