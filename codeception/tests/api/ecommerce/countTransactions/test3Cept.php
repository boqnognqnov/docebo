<?php
//@group ecommerce_countTransactions
$params = array (
	'status'=>'some invalid value'
);

$I = new ApiGuy($scenario);
$I->wantTo('Get count of transactions, with invalid "status parameter"');
$I->doOauth2Authentication();
$I->sendPOST('ecommerce/countTransactions', $params);
$I->seeResponseCodeIs(701);
$I->seeApiIsFailure();

$I->seeErrorMessageIs('Invalid parameter "status"!');