<?php
//@group ecommerce_transactionInfo
$transactionInfo = EcommerceTransactionInfo::model()->find('code != "" AND item_type = :type', array(
	':type' => EcommerceTransactionInfo::TYPE_COURSE,
));
$params = array (
	'id_transaction' => $transactionInfo->id_trans
);

$I = new ApiGuy($scenario);
$I->wantTo('see details of a specified ecommerce transaction for course');
$I->doOauth2Authentication();
$I->sendPOST('ecommerce/transactionInfo', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

$result = json_decode($I->grabResponse())->items[0];
PHPUnit_Framework_Assert::assertEquals($transactionInfo->code,$result->item_code);