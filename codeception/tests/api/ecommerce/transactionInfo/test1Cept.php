<?php
$transactionInfo = EcommerceTransactionInfo::model()->find();
$params = array (
	'id_transaction' => $transactionInfo->id_trans
);

$I = new ApiGuy($scenario);
$I->wantTo('see details of a specified ecommerce transaction');
$I->doOauth2Authentication();
$I->sendPOST('ecommerce/transactionInfo', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

$I->seeResponseMatchesJsonType(
	array(
		'items' => array(
			array(
				'name' => 'string',
				'item_id' => 'integer',
				'price' => 'string',
				'status' => 'string',
				'type' => 'string',
				'username' => 'string',
				'firstname' => 'string',
				'lastname' => 'string',
				'email' => 'string',
				'payment_method' => 'string',
				'transaction_id' => 'string',
				'company_name' => 'string',
				'vat' => 'string',
				'address_1' => 'string',
				'address_2' => 'string',
				'city' => 'string',
				'state' => 'string',
				'zip' => 'string',
				'item_code' => 'string',
			)
		),
		'success' => 'boolean'
	)
);