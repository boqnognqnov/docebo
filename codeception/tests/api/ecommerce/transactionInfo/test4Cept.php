<?php
//@group ecommerce_transactionInfo
$params = array (
	'id_transaction' => 'unexistingggg123'
);

$I = new ApiGuy($scenario);
$I->wantTo('force error 401');
$I->doOauth2Authentication();
$I->sendPOST('ecommerce/transactionInfo', $params);
$I->seeResponseCodeIs(401);
$I->seeApiIsFailure();

$I->seeErrorMessageIs("Transaction not found");