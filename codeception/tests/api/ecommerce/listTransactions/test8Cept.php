<?php
$params = array (
	'from' => -100,
	'count' => 235,
);

$I = new ApiGuy($scenario);
$I->wantTo('force error 704');
$I->doOauth2Authentication();
$I->sendPOST('ecommerce/listTransactions', $params);
$I->seeResponseCodeIs(704);
$I->seeApiIsFailure();

$I->seeErrorMessageIs('Invalid "from" parameter');