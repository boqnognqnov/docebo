<?php
$params = array (
	'from' => 0,
	'count' => -100,
);

$I = new ApiGuy($scenario);
$I->wantTo('force error 705');
$I->doOauth2Authentication();
$I->sendPOST('ecommerce/listTransactions', $params);
$I->seeResponseCodeIs(705);
$I->seeApiIsFailure();

$I->seeErrorMessageIs('Invalid "count" parameter');