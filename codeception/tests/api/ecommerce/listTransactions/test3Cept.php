<?php
//@group ecommerce_llistTransactions
$params = array(
	'from' => 0,
	'count' => 100,
);

$I = new ApiGuy($scenario);
$I->wantTo('list all ecommerce transactions without their items');
$I->doOauth2Authentication();
$I->sendPOST('ecommerce/listTransactions', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();
$I->seeResponseMatchesJsonType(array(
	'transactions' => array(
		array(
			'id' => 'integer',
			'username' => 'string',
			'firstname' => 'string|null',
			'lastname' => 'string|null',
			'email' => 'string',
			'total' => 'float|integer',
			'payment_method' => 'string',
			'transaction_id' => 'string|null',
			'date_created' => 'string',
			'date_activated' => 'string',
			'status' => 'string',
			'company_name' => 'string|null',
			'vat' => 'string|null',
			'address_1' => 'string|null',
			'address_2' => 'string|null',
			'city' => 'string|null',
			'state' => 'string|null',
			'zip' => 'string|null',
		),
	),
	'success' => 'boolean'
));