<?php
$params = array (
	"count" => 5,
);

$I = new ApiGuy($scenario);
$I->wantTo('force error 702');
$I->doOauth2Authentication();
$I->sendPOST('ecommerce/listTransactions', $params);
$I->seeResponseCodeIs(702);
$I->seeApiIsFailure();

$I->seeErrorMessageIs('No "from" parameter provided');