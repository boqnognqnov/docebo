<?php
$params = array (
	'from' => 0,
);

$I = new ApiGuy($scenario);
$I->wantTo('force error 703');
$I->doOauth2Authentication();
$I->sendPOST('ecommerce/listTransactions', $params);
$I->seeResponseCodeIs(703);
$I->seeApiIsFailure();

$I->seeErrorMessageIs('No "count" parameter provided');