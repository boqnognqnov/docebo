<?php
$params = array (
	"from" => 0,
	"count" => 5,
	'status' => 'invalid status',
);

$I = new ApiGuy($scenario);
$I->wantTo('force error 701');
$I->doOauth2Authentication();
$I->sendPOST('ecommerce/listTransactions', $params);
$I->seeResponseCodeIs(701);
$I->seeApiIsFailure();

$I->seeErrorMessageIs('Invalid value for parameter "status"');