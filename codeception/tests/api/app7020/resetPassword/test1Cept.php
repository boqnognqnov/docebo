<?php
//@group reset_password

$params = array(
    'email' => 'test@mail.com'
);

$I = new ApiGuy($scenario);

$I->wantTo('Send an email for resetting password');
$params = array_merge($params, $I->doLightweightAuthentication($params));
$I->sendPOST('app7020/resetPassword', $params);
$I->seeResponseCodeIs(200);
$I->seeResponseContains('"result":1');
