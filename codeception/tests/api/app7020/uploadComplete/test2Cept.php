<?php
//@group upload_complete

$params = array(
    'type' => 'video',
    'typeId'=>1, 
    'filename' => '1ngk1dgo76g851rsqdgqgr6big.mp4',
    'originalFilename' => 'myfile.mp4',
    'title' => 'Testcontent',
    'description' => 'Description of test content',
    'topics' => '[3,4,5]',
    'tags' => '[29,30]',
    'userId' => 12301,
    'status' => 3,
    'id' => 24,
    'idSource' => 2
);

$I = new ApiGuy($scenario);

$I->wantTo('Update created content record to DB and sent it for conversion');
$params = array_merge($params, $I->doLightweightAuthentication($params));
$I->sendPOST('app7020/uploadComplete', $params);
$I->seeResponseCodeIs(200);
$I->seeResponseContains('"contentId"');
$I->cantSeeResponseContains('"contentId":0');
