<?php
//@group upload_complete

$params = array(
    'type' => 'video',
    'typeId'=>1, 
    'filename' => '',
    'originalFilename' => 'myfile.mp4',
    'title' => 'Testcontent',
    'description' => 'Description of test content',
    'topics' => '[3,4,5]',
    'tags' => '[29,30]',
    'userId' => 12301,
    'status' => 3,
    'id' => 24,
    'idSource' => 2
);

$I = new ApiGuy($scenario);

$I->wantTo('Create content record to DB with no filename provided');
$params = array_merge($params, $I->doLightweightAuthentication($params));
$I->sendPOST('app7020/uploadComplete', $params);
$I->seeResponseCodeIs(201);

