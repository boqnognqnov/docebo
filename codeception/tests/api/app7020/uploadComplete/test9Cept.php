<?php
//@group upload_complete

$params = array(
    'type' => 'video',
    'typeId'=>1, 
    'filename' => '1ngk1dgo76g851rsqdgqgr6big.mp4',
    'originalFilename' => 'myfile.mp4',
    'title' => 'Testcontent',
    'description' => 'Description of test content',
    'topics' => '[1,2]',
    'tags' => '[29,30]',
    'userId' => 12301,
    'status' => 5,
    'id' => '',
    'idSource' => 2
);

$I = new ApiGuy($scenario);

$I->wantTo('Create/Update content record with wrong status provided');
$params = array_merge($params, $I->doLightweightAuthentication($params));
$I->sendPOST('app7020/uploadComplete', $params);
$I->seeResponseCodeIs(2053);

