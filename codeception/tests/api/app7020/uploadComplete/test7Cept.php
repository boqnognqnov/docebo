<?php
//@group upload_complete

$params = array(
    'type' => 'video',
    'typeId'=>1, 
    'filename' => '1ngk1dgo76g851rsqdgqgr6big.mp4',
    'originalFilename' => 'myfile.mp4',
    'title' => 'Testcontent',
    'description' => 'Description of test content',
    'topics' => '',
    'tags' => '[29,30]',
    'userId' => 12301,
    'status' => 4,
    'id' => '',
    'idSource' => 2
);

$I = new ApiGuy($scenario);

$I->wantTo('Update content record to DB with no topics provided');
$params = array_merge($params, $I->doLightweightAuthentication($params));
$I->sendPOST('app7020/uploadComplete', $params);
$I->seeResponseCodeIs(205);

