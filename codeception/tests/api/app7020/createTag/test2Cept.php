<?php
//@group app7020_create_tag

$params = array(
    'tag' => 'brandly new tag'
);

$I = new ApiGuy($scenario);

$I->wantTo('Duplicate tag to database');
$params = array_merge($params, $I->doLightweightAuthentication($params));
$I->sendPOST('App7020/createTag', $params);
$I->seeResponseCodeIs(200);
$I->seeResponseContains('"id":0');