<?php
//@group app7020_create_tag

$params = array(
    
);

$I = new ApiGuy($scenario);

$I->wantTo('Create tag to database with wrong or missing parameter provided');
$params = array_merge($params, $I->doLightweightAuthentication($params));
$I->sendPOST('App7020/createTag', $params);
$I->seeResponseCodeIs(209);