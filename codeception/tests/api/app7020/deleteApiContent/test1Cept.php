<?php
//@group delete_content

$params = array(
    'id' => '4'
);

$I = new ApiGuy($scenario);

$I->wantTo('Delete content');
$params = array_merge($params, $I->doLightweightAuthentication($params));
$I->sendPOST('app7020/deleteApiContent', $params);
$I->seeResponseCodeIs(200);
$I->cantSeeResponseContains('"errorMessage"');
