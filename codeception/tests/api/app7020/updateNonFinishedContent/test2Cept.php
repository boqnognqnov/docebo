<?php
//@group update_non_finished_content

$params = array(
    'ids' => '[3,4,5]'
);

$I = new ApiGuy($scenario);

$I->wantTo('Update non finished content with wrong or missing parameter recieved');
$params = array_merge($params, $I->doLightweightAuthentication($params));
$I->sendPOST('app7020/updateNonFinishedContent', $params);
$I->seeResponseCodeIs(209);
