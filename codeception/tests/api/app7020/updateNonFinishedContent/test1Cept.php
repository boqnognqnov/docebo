<?php
//@group update_non_finished_content

$params = array(
    'contentIds' => '[3,4,5]'
);

$I = new ApiGuy($scenario);

$I->wantTo('Update non finished content');
$params = array_merge($params, $I->doLightweightAuthentication($params));
$I->sendPOST('app7020/updateNonFinishedContent', $params);
$I->seeResponseCodeIs(200);
$I->seeResponseContains('"positives"');
