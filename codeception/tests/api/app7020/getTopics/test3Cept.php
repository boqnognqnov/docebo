<?php
//@group get_topics

$params = array(
 
);

$I = new ApiGuy($scenario);

$I->wantTo('Get topic tree if lang parameter is not provided');
$params = array_merge($params, $I->doLightweightAuthentication($params));
$I->sendPOST('app7020/getTopics', $params);
$I->seeResponseCodeIs(200);
$I->seeResponseContains('"success":true');
$I->seeResponseContains('"idTopic":');