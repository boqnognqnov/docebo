<?php
//@group get_topics

$params = array(
    'lang' => 'de'
);

$I = new ApiGuy($scenario);

$I->wantTo('Get topic tree if lang parameter is not existing language');
$params = array_merge($params, $I->doLightweightAuthentication($params));
$I->sendPOST('app7020/getTopics', $params);
$I->seeResponseCodeIs(200);
$I->seeResponseContains('"success":true');
$I->seeResponseContains('"idTopic":');