<?php
//@group get_tags

$params = array(
    'limit' => '10'
);

$I = new ApiGuy($scenario);

$I->wantTo('Get all tags');
$params = array_merge($params, $I->doLightweightAuthentication($params));
$I->sendPOST('app7020/getTags', $params);
$I->seeResponseCodeIs(200);
$I->seeResponseContains('"success":true');
$I->seeResponseContains('"tags":');