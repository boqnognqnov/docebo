<?php
//@group enroll_count
$I = new ApiGuy($scenario);
$idUser = $I->grabUsers(1)[0];
$params = array(
	'id_user' => $idUser,
);

$I = new ApiGuy($scenario);
$I->wantTo('count the number of enrollments of a user');
$I->doOauth2Authentication();
$I->sendPOST('enroll/count', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

$I->seeResponseMatchesJsonType(array(
	'count' => 'integer',
	'success' => 'boolean',
));