<?php
//@group enroll_count
$I = new ApiGuy($scenario);
$idUser = $I->grabUsers(1)[0];
$params = array(
    'id_user' => $idUser,
    'completed_from' => '2016-05-10 19:00:00',
    'completed_to' => '2016-05-16 19:00:00',
);

$I->wantTo('count the number of enrollments of a user when passing the completed_from and completed_to parameters');
$I->doOauth2Authentication();
$I->sendPOST('enroll/count', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

$result = json_decode($I->grabResponse());
$countFromAPI = $result->count;
$expectedCount = Yii::app()->db->createCommand()
	->select('COUNT(idUser)')
	->from(LearningCourseuser::model()->tableName().' lcu')
	->where('lcu.date_complete BETWEEN :start AND :end AND idUser=:id',
		array(':start' => $params['completed_from'], ':end' => $params['completed_to'], ':id' => $idUser))->queryScalar();
PHPUnit_Framework_Assert::assertEquals($expectedCount,$countFromAPI);

$I->seeResponseMatchesJsonType(array(
	'count' => 'integer',
	'success' => 'boolean',
));