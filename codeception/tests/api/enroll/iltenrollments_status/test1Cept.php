<?php
//@group enroll_iltenrollments_status
$params = array ();

$I = new ApiGuy($scenario);
$I->wantTo('API client calling enroll/iltenrollments_status without any parameters');
$I->doOauth2Authentication();
$I->sendPOST('enroll/iltenrollments_status', $params);
$I->seeResponseCodeIs(201);
$I->seeErrorMessageIs("Missing mandatory params");
$I->seeApiIsFailure();