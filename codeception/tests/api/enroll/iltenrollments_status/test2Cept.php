<?php
//@group enroll_iltenrollments_status
$params = array (
	'year' => date("Y"),
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling enroll/iltenrollments_status with current year as a parameters');
$I->doOauth2Authentication();
$I->sendPOST('enroll/iltenrollments_status', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

$I->seeResponseMatchesJsonType(array(
	'iltenrollments_status' => array(
		array(
			'id_user' => 'integer',
			'username' => 'string',
			'firstname' => 'string',
			'lastname' => 'string',
			'id_course' => 'integer',
			'course_code' => 'string',
			'course_title' => 'string',
			'course_description' => 'string',
			'course_language' => 'string',
			'course_type' => 'string',
			'course_link' => 'string', //CHECK IF WE CAN USE URL type
			'level' => 'string',
			'status' => 'string',
			'date_enrollment' => 'string',
			'date_first_access' => 'string|null',
			'date_complete' => 'string|null',
			'date_last_access' => 'string|null',
			'can_enter' => 'boolean',
			'progress' => 'string', //CHECK IF WE CAN SOMEHOW CHECK FOR '%'
			'final_score' => 'float|integer', //in some cases, the score is integer
			'idSession' => 'integer',
			'session_name' => 'string',
			'session_start_date' => 'string', //CHECK IF WE CAN USE DATE (probably not, or it won't be easy)
			'session_end_date' => 'string',
			'total_session_hours' => 'float|integer',
			'session_teachers' => 'array',
			'session_coaches' => 'array',
			'session_tutors' => 'array',
			'dates' => array(
				array(
					'date' => 'string', //CHECK IF WE CAN USE DATE
					'date_name' => 'string',
					'date_starting_time' => 'string',
					'date_ending_time' => 'string',
					'date_timezone' => 'string',
					'date_user_attendance' => 'boolean',
					'date_room_name' => 'string|null',
					'date_location_name' => 'string',
					'date_location_country' => 'string',
					'date_location_address' => 'string',
				)
			),
		),
	),
	'success' => 'boolean'
));