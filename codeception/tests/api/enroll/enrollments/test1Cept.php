<?php
//@group enroll_enrollments
$I = new ApiGuy($scenario);
$idUser = $I->grabUsers(1)[0];
$params = array(
    'id_user' => $idUser,
);

$I = new ApiGuy($scenario);
$I->wantTo('get all the enrollments of a user');
$I->doOauth2Authentication();
$I->sendPOST('enroll/enrollments', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

$I->seeResponseMatchesJsonType(array(
    'enrollments' => array(
        array(
            'id_user' => 'integer',
            'username' => 'string',
            'id_course' => 'integer',
            'course_code' => 'string',
            'date_enrollment' => 'string',
            'date_first_access' => 'string|null',
            'date_complete' => 'string|null',
            'date_last_access' => 'string|null',
            'active_from' => 'string|null',
            'active_until' => 'string|null',
            'course_name' => 'string',
            'enrollment_fields' => 'array',
			'status' => 'string',
			'firstname' => 'string',
			'lastname' => 'string',
			'course_link' => 'string',
		),
	),
	'success' => 'boolean',
));