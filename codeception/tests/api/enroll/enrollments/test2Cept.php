<?php
//@group enroll_enrollments
$I = new ApiGuy($scenario);
$userData = Yii::app()->db->createCommand()
		->select('idUser, idCourse, date_complete')->from(LearningCourseuser::model()->tableName())
		->where('date_complete IS NOT NULL')->order('date_complete DESC')->limit(1)->queryRow();
$idUser = $userData['idUser'];
$idCourse = $userData['idCourse'];
$targetDate = $userData['date_complete'];
$params = array(
    'id_user' => $idUser,
	'completed_from' => date('Y-m-d H:i:s', strtotime($targetDate) - 1),
    'completed_to' => $targetDate,
);

$I->wantTo('get all the enrollments of a user when passing the completed_from and completed_to parameters');
$I->doOauth2Authentication();
$I->sendPOST('enroll/enrollments', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

$result = json_decode($I->grabResponse());
$responseData = $result->enrollments[0];
$expectedResponse = Yii::app()->db->createCommand()
	->select()->from(LearningCourseuser::model()->tableName().' lcu')
	->where('lcu.date_complete BETWEEN :start AND :end AND idUser=:id AND idCourse=:course',
		array(':start' => $params['completed_from'], ':end' => $params['completed_to'], ':id' => $idUser, ':course' => $idCourse))->queryRow();
PHPUnit_Framework_Assert::assertTrue($expectedResponse['date_complete'] == $responseData->date_complete);
PHPUnit_Framework_Assert::assertTrue($expectedResponse['idUser'] == $responseData->id_user);
PHPUnit_Framework_Assert::assertTrue($expectedResponse['idCourse'] == $responseData->id_course);

$I->seeResponseMatchesJsonType(array(
	'enrollments'=>array(
		array(
			'id_user' => 'integer',
			'username' => 'string',
			'id_course' => 'integer',
			'course_code' => 'string',
			'status' => 'string',
			'date_enrollment' => 'string',
			'date_first_access' => 'string|null',
			'date_complete' => 'string|null',
			'date_last_access' => 'string|null',
			'active_from' => 'string|null',
			'active_until' => 'string|null',
			'firstname' => 'string',
			'lastname' => 'string',
			'course_name' => 'string',
			'course_link' => 'string',
		),
	),
	'success' => 'boolean',
));