<?php
// @group iltenrollment
$courseIdWithWaitList = Yii::app()->db->createCommand()
	->select('DISTINCT(course_id) as id')
	->from(LtCourseSession::model()->tableName() . ' ltcs')
	->join(LtCourseuserSession::model()->tableName() . ' ltcus', 'ltcs.id_session=ltcus.id_session')
	->where('ltcus.status = :status', array(':status' => LtCourseuserSession::$SESSION_USER_WAITING_LIST))
	->limit(1)
	->queryScalar();

$params = array(
	'id_course' => $courseIdWithWaitList,
	'include_waitlisted' => true
);

$I = new ApiGuy($scenario);
$I->wantTo('Client calling API with include_waitlisted = true');
$I->doStrongAuthentication($params);
$I->sendPOST('enroll/iltenrollments', $params);
$I->seeResponseContainsAttribute('iltenrollments');
$I->seeResponseContainsJson(array('Status_ID' => LtCourseuserSession::$SESSION_USER_WAITING_LIST));
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();