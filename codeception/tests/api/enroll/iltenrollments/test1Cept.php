<?php
// @group iltenrollment
$ids = Yii::app()->db->createCommand()
	->select('DISTINCT(course_id) as id')
	->from(LtCourseSession::model()->tableName() . ' ltcs')
	->join(LtCourseuserSession::model()->tableName() . ' ltcus', 'ltcs.id_session=ltcus.id_session')
	->where('ltcus.status = :status', array(':status' => LtCourseuserSession::$SESSION_USER_WAITING_LIST))
	->queryColumn();
$courseIdWithoutWaitList = Yii::app()->db->createCommand()
	->select('course_id')
	->from(LtCourseSession::model()->tableName() . ' ltcs')
	->join(LtCourseuserSession::model()->tableName() . ' ltcus', 'ltcs.id_session=ltcus.id_session')
	->where(array('NOT IN', 'course_id', $ids))->limit(1)->queryScalar();

$params = array(
	'id_course' => $courseIdWithoutWaitList
);

$I = new ApiGuy($scenario);
$I->wantTo('Client calling API without include_waitlisted or with include_waitlisted = false');
$I->doStrongAuthentication($params);
$I->sendPOST('enroll/iltenrollments', $params);
$I->seeResponseContainsAttribute('iltenrollments');
$I->dontSeeResponseContainsJson(array('Status_ID' => LtCourseuserSession::$SESSION_USER_WAITING_LIST));

$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();