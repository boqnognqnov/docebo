<?php
//@group enroll_iltenrollments_status_count
$params = array (
	'year' => date("Y"),
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling enroll/iltenrollments_status_count with current year as a parameters');
$I->doOauth2Authentication();
$I->sendPOST('enroll/iltenrollments_status_count', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

$I->seeResponseMatchesJsonType(array(
	'iltenrollments_status_count' => 'integer',
	'success' => 'boolean'
));