<?php
//@group location_create
$params = array(
	'name' => 'test name'
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling location/create with missing country');
$I->doStrongAuthentication($params);
$I->sendPOST('location/create', $params);
$I->seeResponseCodeIs(702);
$I->seeErrorMessageIs("Missing or empty country name");
$I->seeApiIsFailure();