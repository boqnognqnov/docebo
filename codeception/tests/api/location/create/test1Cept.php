<?php
//@group location_create
$params = array(
	'name' => null
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling location/create with an empty name');
$I->doStrongAuthentication($params);
$I->sendPOST('location/create', $params);
$I->seeResponseCodeIs(701);
$I->seeErrorMessageIs("Missing or empty location name");
$I->seeApiIsFailure();