<?php
//@group location_create
$params = array(
	'name' => 'test name',
	'country'=>'Bulgaria'
);

$I = new ApiGuy($scenario);
$I->wantTo('API client successfully creating a new location');
$I->doStrongAuthentication($params);
$I->sendPOST('location/create', $params);
$I->seeResponseContainsAttribute('id_location');
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

$I->seeResponseMatchesJsonType(array(
	'id_location' => 'integer',
	'success'=>'boolean'
));