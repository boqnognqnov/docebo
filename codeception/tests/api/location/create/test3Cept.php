<?php
//@group location_create
$params = array(
	'name' => 'test name',
	'country'=>'fancy country'
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling location/create with an invalid country');
$I->doStrongAuthentication($params);
$I->sendPOST('location/create', $params);
$I->seeResponseCodeIs(703);
$I->seeErrorMessageIs("Invalid country name provided");
$I->seeApiIsFailure();