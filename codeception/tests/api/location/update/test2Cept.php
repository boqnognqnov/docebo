<?php
//@group location_update
$params = array(
	'name' => 'test name',
	'country'=>'fancy country',
	'id_location' => -1
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling location/update with invalid id_location');
$I->doStrongAuthentication($params);
$I->sendPOST('location/update', $params);
$I->seeResponseCodeIs(705);
$I->seeErrorMessageIs("Invalid id_location param provided");
$I->seeApiIsFailure();