<?php
//@group location_update
$idLocation = Yii::app()->db->createCommand()
		->select('id_location')
		->from(LtLocation::model()->tableName())
		->limit(1)->queryScalar();
$params = array(
	'name' => 'Location 2',
	'country'=>'Bulgaria',
	'telephone'=>'5678',
	'id_location'=>$idLocation
);

$I = new ApiGuy($scenario);
$I->wantTo('API client successfully updating an existing location');
$I->doStrongAuthentication($params);
$I->sendPOST('location/update', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();