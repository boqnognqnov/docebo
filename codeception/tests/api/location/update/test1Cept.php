<?php
//@group location_update
$params = array(
	'name' => 'test name',
	'country'=>'fancy country'
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling location/update with empty id_location');
$I->doStrongAuthentication($params);
$I->sendPOST('location/update', $params);
$I->seeResponseCodeIs(704);
$I->seeErrorMessageIs("Missing or empty id_location param");
$I->seeApiIsFailure();