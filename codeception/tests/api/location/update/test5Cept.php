<?php
//@group location_update
$idLocation = Yii::app()->db->createCommand()
		->select('id_location')
		->from(LtLocation::model()->tableName())
		->limit(1)->queryScalar();
$params = array(
	'id_location' => $idLocation,
	'name' => 'test name',
	'country'=>'fancy country'
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling location/update with an invalid country');
$I->doStrongAuthentication($params);
$I->sendPOST('location/update', $params);
$I->seeResponseCodeIs(703);
$I->seeErrorMessageIs("Invalid country name provided");
$I->seeApiIsFailure();