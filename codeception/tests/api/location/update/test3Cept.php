<?php
//@group location_update
$idLocation = Yii::app()->db->createCommand()
		->select('id_location')
		->from(LtLocation::model()->tableName())
		->limit(1)->queryScalar();
$params = array(
	'id_location' => $idLocation
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling location/update with empty name');
$I->doStrongAuthentication($params);
$I->sendPOST('location/update', $params);
$I->seeResponseCodeIs(701);
$I->seeErrorMessageIs("Missing or empty location name");
$I->seeApiIsFailure();