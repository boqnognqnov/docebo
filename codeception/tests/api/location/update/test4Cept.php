<?php
//@group location_update
$idLocation = Yii::app()->db->createCommand()
		->select('id_location')
		->from(LtLocation::model()->tableName())
		->limit(1)->queryScalar();
$params = array(
	'id_location' => $idLocation,
	'name' => 'test name',
	'country'=>''
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling location/update with missing country');
$I->doStrongAuthentication($params);
$I->sendPOST('location/update', $params);
$I->seeResponseCodeIs(702);
$I->seeErrorMessageIs("Missing or empty country name");
$I->seeApiIsFailure();