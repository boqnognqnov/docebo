<?php
//@group location_delete
$params = array(
	'id_location' => null
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling location/delete with empty id_location');
$I->doStrongAuthentication($params);
$I->sendPOST('location/delete', $params);
$I->seeResponseCodeIs(704);
$I->seeErrorMessageIs("Missing or empty id_location param");
$I->seeApiIsFailure();