<?php
//@group location_delete
$idLocation = Yii::app()->db->createCommand()
		->select('id_location')
		->from(LtLocation::model()->tableName())
		->limit(1)->queryScalar();
$params = array(
	'id_location' => $idLocation
);

$I = new ApiGuy($scenario);
$I->wantTo('API client successfully deleting a location');
$I->doStrongAuthentication($params);
$I->sendPOST('location/delete', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();