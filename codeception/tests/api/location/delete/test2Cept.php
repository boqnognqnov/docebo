<?php
//@group location_delete
$params = array(
	'id_location' => -3
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling location/delete with invalid id_location');
$I->doStrongAuthentication($params);
$I->sendPOST('location/delete', $params);
$I->seeResponseCodeIs(705);
$I->seeErrorMessageIs("Invalid id_location param provided");
$I->seeApiIsFailure();