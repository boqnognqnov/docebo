<?php
//@group location_countSessionsByLocation
$params = array (
	'id_location' => -25
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling location/countSessionsByLocation with an invalid id_location');
$I->doStrongAuthentication($params);
$I->sendPOST('location/countSessionsByLocation', $params);
$I->seeResponseCodeIs(701);
$I->seeErrorMessageIs('Invalid id_location param provided');
$I->seeApiIsFailure();