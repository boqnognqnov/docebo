<?php
//@group location_countSessionsByLocation
$I = new ApiGuy($scenario);
$id = $I->grabRandomLocationId();

$params = array (
	'id_location' => $id
);

$I->wantTo('API client calling location/countSessionsByLocation with valid id_location');
$I->doStrongAuthentication($params);
$I->sendPOST('location/countSessionsByLocation', $params);
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

$I->seeResponseMatchesJsonType(array(
	'count' => "integer",
	'success'=>'boolean'
));
