<?php
//@group location_countSessionsByLocation
$params = array ();

$I = new ApiGuy($scenario);
$I->wantTo('API client calling location/countSessionsByLocation with missing id_location');
$I->doStrongAuthentication($params);
$I->sendPOST('location/countSessionsByLocation', $params);
$I->seeResponseCodeIs(702);
$I->seeErrorMessageIs('Missing required param id_location');
$I->seeApiIsFailure();