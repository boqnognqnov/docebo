<?php
//@group location_findSessionsByLocation
$params = array ();

$I = new ApiGuy($scenario);
$I->wantTo('API client calling location/countSessionsByLocation with a missing id_location');
$I->doStrongAuthentication($params);
$I->sendPOST('location/findSessionsByLocation', $params);
$I->seeResponseCodeIs(702);
$I->seeErrorMessageIs('Missing required param id_location');
$I->seeApiIsFailure();