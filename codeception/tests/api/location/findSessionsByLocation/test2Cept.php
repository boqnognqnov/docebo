<?php
//@group location_findSessionsByLocation
$I = new ApiGuy($scenario);
$id = $I->grabRandomLocationId();
$params = array(
	'count' => -5,
	'from' => 0,
	'id_location' => $id
);

$I->wantTo('API client calling location/findSessionsByLocation with invalid "count"');
$I->doStrongAuthentication($params);
$I->sendPOST('location/findSessionsByLocation', $params);
$I->seeResponseCodeIs(705);
$I->seeErrorMessageIs('Invalid count param provided');
$I->seeApiIsFailure();