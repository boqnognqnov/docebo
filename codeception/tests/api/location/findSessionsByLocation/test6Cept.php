<?php
//@group location_findSessionsByLocation
//@group testingNow
$I = new ApiGuy($scenario);
$id = $I->grabRandomLocationId();

$params = array (
	'id_location' => $id,
	'from' => 0,
	'count' => 10
);

$I->wantTo('API client successfully retrieving sessions in a given location');
$I->doStrongAuthentication($params);
$I->sendPOST('location/findSessionsByLocation', $params);
$I->seeResponseContainsAttribute('sessions');
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

$I->seeResponseMatchesJsonType(array(
	'sessions' => array(
		array(
			'id_session' => 'string',
			'session_name' => 'string',
			'session_date_begin' => 'string', //possibility for date regex
			'session_date_end' => 'string', //possibility for date regex
			'id_course' => 'string',
			'course_name' => 'string',
			'course_code' => 'string',
			'course_description' => 'string'
		),
	),
	'success'=>'boolean'
));
