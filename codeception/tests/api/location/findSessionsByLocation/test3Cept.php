<?php
//@group location_findSessionsByLocation
$I = new ApiGuy($scenario);
$id = $I->grabRandomLocationId();
$params = array (
	'count' => 150,
	'from' => 0,
	'id_location' => $id,
);

$I->wantTo('API client calling location/findSessionsByLocation with a "count" bigger than max size');
$I->doStrongAuthentication($params);
$I->sendPOST('location/findSessionsByLocation', $params);
$I->seeResponseCodeIs(703);
$I->seeErrorMessageIs('"count" param exceeds the maximum page size (' . LocationApiModule::MAX_SIZE . ')');
$I->seeApiIsFailure();