<?php
//@group location_findSessionsByLocation
$params = array (
	'id_location' => -5
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling location/countSessionsByLocation with an invalid id_location');
$I->doStrongAuthentication($params);
$I->sendPOST('location/findSessionsByLocation', $params);
$I->seeResponseCodeIs(701);
$I->seeErrorMessageIs('Invalid id_location param provided');
$I->seeApiIsFailure();