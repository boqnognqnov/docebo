<?php
//@group location_findSessionsByLocation
$I = new ApiGuy($scenario);
$id = $I->grabRandomLocationId();
$params = array(
	'from' => -5,
	'count' => 10,
	'id_location' => $id
);

$I->wantTo('API client calling location/findSessionsByLocation with invalid "from"');
$I->doStrongAuthentication($params);
$I->sendPOST('location/findSessionsByLocation', $params);
$I->seeResponseCodeIs(704);
$I->seeErrorMessageIs('Invalid from param provided');
$I->seeApiIsFailure();