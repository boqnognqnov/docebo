<?php
//@group location_count
$params = array (
	'country' => 'fancy country'
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling location/count with an invalid country');
$I->doStrongAuthentication($params);
$I->sendPOST('location/count', $params);
$I->seeResponseCodeIs(701);
$I->seeErrorMessageIs("Invalid country name provided");
$I->seeApiIsFailure();