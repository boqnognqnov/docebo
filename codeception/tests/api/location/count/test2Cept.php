<?php
//@group location_count
$params = array (
	'country' => 'Bulgaria'
);

$I = new ApiGuy($scenario);
$I->wantTo('API client successfully counting locations');
$I->doStrongAuthentication($params);
$I->sendPOST('location/count', $params);

$countryName = strtolower($params['country']);
$countryIdFromDb = Yii::app()->db->createCommand()->select('id_country')
		->from(CoreCountry::model()->tableName())
		->where('LOWER(name_country) = :name', array(':name' => $countryName))->queryScalar();
$countFromDb = Yii::app()->db->createCommand()
		->select('COUNT(id_location)')
		->from(LtLocation::model()->tableName())
		->where('id_country=:id', array(':id' => $countryIdFromDb))->queryScalar();
$resultIdFromApi = json_decode($I->grabResponse());

PHPUnit_Framework_Assert::assertTrue($resultIdFromApi->count == $countFromDb, 'Wrong count is returned');
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

$I->seeResponseMatchesJsonType(array(
	'count' => "integer",
	'success'=>'boolean'
));