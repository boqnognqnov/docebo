<?php
//@group location_listLocations

$params = array(
	'from' => 0,
	'count' => 120,
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling location/listLocations with a "count" bigger than max size');
$I->doStrongAuthentication($params);
$I->sendPOST('location/listLocations', $params);
$I->seeResponseCodeIs(704);
$I->seeErrorMessageIs('"count" param exceeds the maximum page size (' . LocationApiModule::MAX_SIZE . ')');
$I->seeApiIsFailure();
