<?php
//@group location_listLocations

$params = array(
	'from' => 0,
	'count' => 10,
);

$I = new ApiGuy($scenario);
$I->wantTo('API client successfully retrieving locations');
$I->doStrongAuthentication($params);
$I->sendPOST('location/listLocations', $params);
$I->seeResponseContainsAttribute('locations');
$I->seeResponseCodeIs(200);
$I->seeApiIsSuccess();

$I->seeResponseMatchesJsonType(array(
	'locations' => array(
		array(
			'id_location' => 'integer',
			'name' => 'string',
			'address' => 'string',
			'country' => 'string',
			'telephone' => 'string',
			'email' => 'string',
			'reaching_info' => 'string',
			'accomodations' => 'string',
			'other_info' => 'string',
		)
	),
	'success'=>'boolean'
));