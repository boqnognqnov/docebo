<?php
//@group location_listLocations

//first test case with negative param
$params = array(
	'from' => -1,
	'count' => 10,
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling location/listLocations with invalid "from"');
$I->doStrongAuthentication($params);
$I->sendPOST('location/listLocations', $params);

$I->seeResponseCodeIs(702);
$I->seeApiIsFailure();

//second test case with non integer param
$params = array(
	'from' => 2.3,
	'count' => 10,
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling location/listLocations with invalid "from"');
$I->doStrongAuthentication($params);
$I->sendPOST('location/listLocations', $params);
$I->seeResponseCodeIs(702);
$I->seeErrorMessageIs("Invalid from param provided");
$I->seeApiIsFailure();