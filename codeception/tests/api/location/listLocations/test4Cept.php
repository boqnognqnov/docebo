<?php
//@group location_listLocations

$params = array(
	'from' => 0,
	'count' => 10,
	'country' => 'Fancy non existing country'
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling location/listLocations with an invalid "country"');
$I->doStrongAuthentication($params);
$I->sendPOST('location/listLocations', $params);
$I->seeResponseCodeIs(701);
$I->seeErrorMessageIs("Invalid country name provided");
$I->seeApiIsFailure();
