<?php
//@group location_listLocations

//first test case with negative param
$params = array(
	'from' => 0,
	'count' => -2,
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling location/listLocations with invalid "count"');
$I->doStrongAuthentication($params);
$I->sendPOST('location/listLocations', $params);

$I->seeResponseCodeIs(703);
$I->seeApiIsFailure();

//second test case with non integer param
$params = array(
	'from' => 0,
	'count' => 2.5,
);

$I = new ApiGuy($scenario);
$I->wantTo('API client calling location/listLocations with invalid "count"');
$I->doStrongAuthentication($params);
$I->sendPOST('location/listLocations', $params);
$I->seeResponseCodeIs(703);
$I->seeErrorMessageIs("Invalid count param provided");
$I->seeApiIsFailure();