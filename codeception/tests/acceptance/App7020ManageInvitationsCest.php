<?php

// Autoload the DoceboCest superclass
require_once "DoceboCest.php";

/**
 * Class ExampleDoceboCest
 *
 * Put here all test cases belonging to the "example_docebo" category.
 */
class App7020ManageInvitationsCest extends DoceboCest {

	protected $userHelper;
	protected $orgChartHelper;
	protected $app7020Helper;
	protected $manageInvitationsHelper;
	protected $defaultUserPass;

	public function _inject(\Helper\User $user, \Helper\Orgchart $orgChart, \Helper\App7020\App7020General $app7020General, \Helper\App7020\ManageInvitationsHelper $manageInvitationsHelper) {
		$this->userHelper = $user;
		$this->orgChartHelper = $orgChart;
		$this->app7020Helper = $app7020General;
		$this->manageInvitationsHelper = $manageInvitationsHelper;
		$this->defaultUserPass = $this->userHelper->getDefaultPass();
	}

	/**
	 * The following code will execute before codeception starts running each test case.
	 * Put something here only if you need to add "before" actions, otherwise delete the method from this subclass
	 */
	public function _before(AcceptanceTester $I) {
		// Do not delete this to allow superclass to do its "before" actions
		parent::_before($I);

		// Add your actions here...
	}

	/**
	 * The following code will execute after codeception runs each test case.
	 * Put something here only if you need to add "after" actions, otherwise delete the method from this subclass
	 */
	public function _after(AcceptanceTester $I) {
		// Do not delete this to allow superclass to do its "after" actions
		parent::_after($I);

		// Add your actions here...
	}
	
	/**
	 * Each method is a test case/scenario.
	 *
	 * @scenario Scenario track and send invitation for an asset just published 
	 *
	 * @group PR7020-1505
	 *
	 * @param AcceptanceTester $I
	 */
	public function assetJustPublished(AcceptanceTester $I) {
		//Activate App7020 Plugin
		$this->app7020Helper->enableApp7020();
		// Insert some topics
		$this->app7020Helper->addTopics();
		// Create user who will upload asset
		$user = $this->userHelper->createUser();

		$I->comment('****** Scenario I am Super Admin *** START **************************');
		// Add content
		$idContent1 = $this->app7020Helper->addContent($user->idst, App7020Assets::CONVERSION_STATUS_FINISHED);
		// Create "God admin" user 
		$userAdmin = $this->userHelper->createUser(Yii::app()->user->getGodadminLevelLabel());
		// Make "God admin" expert for all avalable topics topic
		$this->app7020Helper->makeUserExpert($userAdmin->idst);
		// Call method by which I browse through the LMS
		$this->manageInvitationsHelper->assetJustPublishedBrowse($I, $userAdmin->userid, $idContent1, $this->userHelper);
		$I->comment('****** Scenario I am Super Admin *** END **************************');
		 
		$I->comment('****** Scenario I am Expert *** START **************************');
		// Add content
		$idContent2 = $this->app7020Helper->addContent($user->idst, App7020Assets::CONVERSION_STATUS_FINISHED);
		// Create user for 2nd scenario 
		$user2 = $this->userHelper->createUser();
		// Make user expert for all avalable topics
		$this->app7020Helper->makeUserExpert($user2->idst);
		// Call method by which I browse through the LMS
		$this->manageInvitationsHelper->assetJustPublishedBrowse($I, $user2->userid, $idContent2, $this->userHelper);
		$I->comment('****** Scenario I am Expert *** END **************************');
	}
	
	
	/**
	 * Each method is a test case/scenario.
	 *
	 * @scenario Scenario track and send invitation for an asset just published 
	 *
	 * @group PR7020-1506
	 *
	 * @param AcceptanceTester $I
	 */
	public function trackInvitationsForPublishedAsset(AcceptanceTester $I) {
		//Activate App7020 Plugin
		$this->app7020Helper->enableApp7020();
		// Insert some topics
		$this->app7020Helper->addTopics();
		// Create user who will upload asset (contributor)
		$contributor = $this->userHelper->createUser();
		// Add content
		$idContent = $this->app7020Helper->addContent($contributor->idst, App7020Assets::CONVERSION_STATUS_APPROVED);
		
		$I->comment('****** Scenario I am Contributor *** START **************************');
		// Call method by which I browse through the LMS
		$this->manageInvitationsHelper->trackInvitationsForPublishedAssetHelper($I, Yii::app()->user->getRelativeUsername($contributor->userid), $this->defaultUserPass, $idContent);
		$I->comment('****** Scenario I am Contributor *** END **************************');
		
		$I->comment('****** Scenario I am Super Admin *** START **************************');
		// Create "God admin" user 
		$userAdmin = $this->userHelper->createUser(Yii::app()->user->getGodadminLevelLabel());
		// Call method by which I browse through the LMS
		$this->manageInvitationsHelper->trackInvitationsForPublishedAssetHelper($I, Yii::app()->user->getRelativeUsername($userAdmin->userid), $this->defaultUserPass, $idContent);
		$I->comment('****** Scenario I am Super Admin *** END **************************');
		
		$I->comment('****** Scenario I am Expert *** START **************************');
		// Create Expert
		$expert = $this->userHelper->createUser();
		// Make user expert for all avalable topics
		$this->app7020Helper->makeUserExpert($expert->idst);
		// Call method by which I browse through the LMS
		$this->manageInvitationsHelper->trackInvitationsForPublishedAssetHelper($I, Yii::app()->user->getRelativeUsername($expert->userid), $this->defaultUserPass, $idContent);
		$I->comment('****** Scenario I am Expert *** END **************************');
	}
	
	
	/**
	 * Each method is a test case/scenario.
	 *
	 * @scenario Scenario manage invitations for a published asset  
	 *
	 * @group PR7020-1507
	 *
	 * @param AcceptanceTester $I
	 */
	public function manageInvitationsPublishedAsset(AcceptanceTester $I) {
		//Activate App7020 Plugin
		$this->app7020Helper->enableApp7020();
		// Insert some topics
		$this->app7020Helper->addTopics();
		// Create user who will upload asset (contributor)
		$contributor = $this->userHelper->createUser();
		// Add content
		$idContent = $this->app7020Helper->addContent($contributor->idst, App7020Assets::CONVERSION_STATUS_APPROVED);
		// Add some Invitations to content
		$this->app7020Helper->addInvitations($idContent, $this->userHelper);
		
		$I->comment('****** Scenario I am Contributor *** START **************************');
		// Go through the LMS to reach page "analytics/index"
		$this->manageInvitationsHelper->manageInvitationsPublishedAssetHelper($I, Yii::app()->user->getRelativeUsername($contributor->userid), $this->defaultUserPass, $idContent);
		$I->comment('****** Scenario I am Contributor *** END **************************');
		
		$I->comment('****** Scenario I am Expert *** START **************************');
		// Create Expert
		$expert = $this->userHelper->createUser();
		// Make user expert for all avalable topics
		$this->app7020Helper->makeUserExpert($expert->idst);
		// Go through the LMS to reach page "analytics/index"
		$this->manageInvitationsHelper->manageInvitationsPublishedAssetHelper($I, Yii::app()->user->getRelativeUsername($expert->userid), $this->defaultUserPass, $idContent);
		$I->comment('****** Scenario I am Expert *** END **************************');

		$I->comment('****** Scenario I am Super Admin *** START **************************');
		// Create "God admin" user 
		$userAdmin = $this->userHelper->createUser(Yii::app()->user->getGodadminLevelLabel());
		// Go through the LMS to reach page "analytics/index"
		$this->manageInvitationsHelper->manageInvitationsPublishedAssetHelper($I, Yii::app()->user->getRelativeUsername($userAdmin->userid), $this->defaultUserPass, $idContent);
		$I->comment('****** Scenario I am Super Admin *** END **************************');
	}
	
	/**
	 * Each method is a test case/scenario.
	 *
	 * @scenario Scenario manage invitations for a published asset  
	 *
	 * @group PR7020-1508
	 *
	 * @param AcceptanceTester $I
	 */
	public function singleResendInvitationPublishedAsset (AcceptanceTester $I) {
		//Activate App7020 Plugin
		$this->app7020Helper->enableApp7020();
		// Insert some topics
		$this->app7020Helper->addTopics();
		// Create user who will upload asset (contributor)
		$contributor = $this->userHelper->createUser();
		// Add content
		$idContent = $this->app7020Helper->addContent($contributor->idst, App7020Assets::CONVERSION_STATUS_APPROVED);
		
		$I->comment('****** Scenario I am Contributor *** START **************************');
		// Add some Invitations to content
		$this->app7020Helper->addInvitations($idContent, $this->userHelper, true);
		// Go through the LMS to reach page "analytics/index"
		$this->manageInvitationsHelper->singleResendInvitationPublishedAssetHelper($I, Yii::app()->user->getRelativeUsername($contributor->userid), $this->defaultUserPass, $idContent);
		// Delete all invitations after all
		$this->app7020Helper->deleteInvitations();
		$I->comment('****** Scenario I am Contributor *** END **************************');
		
		$I->comment('****** Scenario I am Expert *** START **************************');
		// Add some Invitations to content
		$this->app7020Helper->addInvitations($idContent, $this->userHelper, true);
		// Create Expert
		$expert = $this->userHelper->createUser();
		// Make user expert for all avalable topics
		$this->app7020Helper->makeUserExpert($expert->idst);
		// Go through the LMS to reach page "analytics/index"
		$this->manageInvitationsHelper->singleResendInvitationPublishedAssetHelper($I, Yii::app()->user->getRelativeUsername($expert->userid), $this->defaultUserPass, $idContent);
		// Delete all invitations after all
		$this->app7020Helper->deleteInvitations();
		$I->comment('****** Scenario I am Expert *** END **************************');
		
		$I->comment('****** Scenario I am Super Admin *** START **************************');
		// Add some Invitations to content
		$this->app7020Helper->addInvitations($idContent, $this->userHelper, true);
		// Create "God admin" user 
		$userAdmin = $this->userHelper->createUser(Yii::app()->user->getGodadminLevelLabel());
		// Go through the LMS to reach page "analytics/index"
		$this->manageInvitationsHelper->singleResendInvitationPublishedAssetHelper($I, Yii::app()->user->getRelativeUsername($userAdmin->userid), $this->defaultUserPass, $idContent);
		// Delete all invitations after all
		$this->app7020Helper->deleteInvitations();
		$I->comment('****** Scenario I am Super Admin *** END **************************');
	}
	

}
