<?php

// Autoload the DoceboCest superclass
require_once "DoceboCest.php";


/**
 * Class ExampleDoceboCest
 *
 * Put here all test cases belonging to the "example_docebo" category.
 */
class App7020ChannelsManagementCest extends DoceboCest {

	protected $usersHelper;
	protected $orgChartHelper;
	protected $app7020Helper;
	protected $channelsManagementHelper;
	protected $defaultUserPass;

		public function _inject(\Helper\User $user, \Helper\Orgchart $orgChart, \Helper\App7020\App7020General $app7020General, \Helper\App7020\ChannelsManagementHelper $channelsManagementHelper) {
		$this->usersHelper = $user;
		$this->orgChartHelper = $orgChart;
		$this->app7020Helper = $app7020General;
		$this->channelsManagementHelper = $channelsManagementHelper;
		$this->defaultUserPass = $this->usersHelper->getDefaultPass();
	}

	/**
	 * The following code will execute before codeception starts running each test case.
	 * Put something here only if you need to add "before" actions, otherwise delete the method from this subclass
	 */
	public function _before(AcceptanceTester $I) {
		// Do not delete this to allow superclass to do its "before" actions
		parent::_before($I);

		// Add your actions here...
		$this->login($I);
		$I->amOnPage(\Page\Common::route(\Page\App7020\Channels::ROUTE_INDEX, array(), false, \Helper\App7020\App7020General::APP_NAME));
	}

	/**
	 * The following code will execute after codeception runs each test case.
	 * Put something here only if you need to add "after" actions, otherwise delete the method from this subclass
	 */
	public function _after(AcceptanceTester $I) {
		// Do not delete this to allow superclass to do its "after" actions
		parent::_after($I);

		// Add your actions here...
		// Logout after all
		$I->logout();
	}
	
	/**
	 * Each method is a test case/scenario.
	 *
	 * @scenario Create New Channel - all steps
	 *
	 * @group PR7020-1635
	 *
	 * @param AcceptanceTester $I
	 */
	public function scenarioCreateNewChannel(AcceptanceTester $I) {
		// Delete all Not predifined channels to be shore for the envirnoment
		$this->app7020Helper->deleteNotPredifinedChannels();
		// Create new Not predifined channel
		$this->channelsManagementHelper->addNewNotPredifinedChannel($I);
	}

	/**
	 * Each method is a test case/scenario.
	 *
	 * @scenario Predefined channels list
	 * @group PR7020-1636
	 *
	 * @param AcceptanceTester $I
	 */
	public function scenarioPredefinedChannelsList(AcceptanceTester $I) {
		// Check Predifined channel "Invitations" 
		$this->channelsManagementHelper->checkChannelsListHelper($I,\Page\App7020\Channels::$invitationsRow,\Page\App7020\Channels::$invitationsIcon);
		// Check Predifined channel "My courses and learning plans" 
		$this->channelsManagementHelper->checkChannelsListHelper($I,\Page\App7020\Channels::$myCoursesRow,\Page\App7020\Channels::$myCoursesIcon);
		// Check Predifined channel "My active courses and learning plans" 
		$this->channelsManagementHelper->checkChannelsListHelper($I,\Page\App7020\Channels::$myActiveCoursesRow,\Page\App7020\Channels::$myActiveCoursesIcon);
		// Check Predifined channel "Continue watching and learning" 
		$this->channelsManagementHelper->checkChannelsListHelper($I,\Page\App7020\Channels::$continueWatchingRow,\Page\App7020\Channels::$continueWatchingIcon);
		// Check Predifined channel "Watch Later" 
		$this->channelsManagementHelper->checkChannelsListHelper($I,\Page\App7020\Channels::$watchLaterRow,\Page\App7020\Channels::$watchLaterIcon);
	}
	
	/**
	 * Each method is a test case/scenario.
	 *
	 * @scenario Predefined channels list
	 * @group PR7020-1637
	 *
	 * @param AcceptanceTester $I
	 */
	public function scenarioNotPredefinedChannelsList(AcceptanceTester $I) {
		// Check NOT predifined channel info 
		$this->channelsManagementHelper->checkChannelsListHelper($I,\Page\App7020\Channels::$notPredifinedChannelRow,\Page\App7020\Channels::$notPredifinedChannelIcon, false);
	}
	
	
	/**
	 * Each method is a test case/scenario.
	 *
	 * @scenario Change the channel's visibility
	 * @group PR7020-1655
	 *
	 * @param AcceptanceTester $I
	 */
	public function scenarioChangeChannelVisibility(AcceptanceTester $I) {
		// Open modal for changing "Visibility"
		$I->waitForElementVisible(\Page\App7020\Channels::$notPredifinedChannelRow . ' ' . \Page\App7020\Channels::$visibilityLink, 30);
		$I->click(\Page\App7020\Channels::$notPredifinedChannelRow . ' ' . \Page\App7020\Channels::$visibilityLink);
		// Check if info in modal is OK
		$this->channelsManagementHelper->createNewChannelStep3CorectInfo($I);
		// Close modal
		$this->app7020Helper->closeModal($I);
	}
	/**
	 * Each method is a test case/scenario.
	 *
	 * @scenario Change the channel's visibility
	 * @group PR7020-1657
	 *
	 * @param AcceptanceTester $I
	 */
	public function scenarioChangeChannelUploadPermissions(AcceptanceTester $I) {
		// Open modal for changing "Permissions"
		$I->waitForElementVisible(\Page\App7020\Channels::$notPredifinedChannelRow . ' ' . \Page\App7020\Channels::$UploadPermissionsLink, 30);
		$I->click(\Page\App7020\Channels::$notPredifinedChannelRow . ' ' . \Page\App7020\Channels::$UploadPermissionsLink);
		// Check if info in modal is OK
		$this->channelsManagementHelper->createNewChannelStep4CorectInfo($I);
		// Close modal
		$this->app7020Helper->closeModal($I);
	}
	/**
	 * Each method is a test case/scenario.
	 *
	 * @scenario Drag & drop on entire channels list
	 * @group PR7020-1660
	 *
	 * @param AcceptanceTester $I
	 */
	public function scenarioDragAndDropChannels(AcceptanceTester $I) {
		// Create 2 new Not predifined channel
		$this->channelsManagementHelper->addNewNotPredifinedChannel($I);
		$this->channelsManagementHelper->addNewNotPredifinedChannel($I);
		// Wait for loading 1st Not predifined channel
		$I->waitForElementVisible(\Page\App7020\Channels::$notPredifinedChannelRow . ' ' . \Page\App7020\Channels::$columnDragAndDrop, 30);
		// Wait for loading 3d Not predifined channel
		$I->waitForElementVisible(\Page\App7020\Channels::$thirdNotPredifinedChannelRow . ' ' . \Page\App7020\Channels::$columnDragAndDrop, 30);
		// Show Drag&Drop icon
		$I->moveMouseOver(\Page\App7020\Channels::$notPredifinedChannelRow);
		$I->moveMouseOver(\Page\App7020\Channels::$notPredifinedChannelRow . ' ' . \Page\App7020\Channels::$columnDragAndDrop . ' ' . \Page\App7020\Channels::$dragAndDropIcon);
		// Drag&Drop operation
		$I->wait(5);
		$I->dragAndDrop(\Page\App7020\Channels::$notPredifinedChannelRow . ' ' . \Page\App7020\Channels::$columnDragAndDrop, \Page\App7020\Channels::$thirdNotPredifinedChannelRow . ' ' . \Page\App7020\Channels::$columnDragAndDrop);
		$I->wait(5);
	}
	/**
	 * Each method is a test case/scenario.
	 *
	 * @scenario Clone an admin generated channel
	 * @group PR7020-1663
	 *
	 * @param AcceptanceTester $I
	 */
	public function scenarioCloneAdminGeneratedChannel(AcceptanceTester $I) {
		// Wait for loading 1st Not predifined channel
		$I->waitForElementVisible(\Page\App7020\Channels::$notPredifinedChannelRow . ' ' . \Page\App7020\Channels::$columnDragAndDrop, 30);
		// Get 1st Not predifined channel id from data attr
		$idChannel = $I->grabAttributeFrom(Page\App7020\Channels::$notPredifinedChannelRow . ' ' . Page\App7020\Channels::$columnShow . ' ' . Page\App7020\Channels::$showIcon , Page\App7020\Channels::$channelidDataAttr);
		// Get all needed info for Channel which will be cloned
		$channelInfo = $this->app7020Helper->getChannelInfo($idChannel);
		// Open drop&down menu and click on "clone"
		$this->app7020Helper->dropDownWidgetRun($I, \Page\App7020\Channels::$notPredifinedChannelRow, 'clone');
		
		// Check step 1 info
		// Wait modal to be loaded
		$I->waitForElementVisible(\Page\App7020\Channels::$modalNewChannel, 60);
		// Check if "Channel name" in modal for cloned channel is == parent channel name + "(2)"
		$I->seeInField(Page\App7020\Channels::$channelTitle, $channelInfo['trans'][0]['name'] . ' (2)');
		// Check if "Channel description" in modal for cloned channel is == parent "Channel description"
		$I->seeInField(\Page\App7020\Channels::$channelDescription, $channelInfo['trans'][0]['description']);
		// Go next step
		$I->waitForJS("return $.active == 0;", 60);
		$I->waitForElementVisible(\Page\App7020\Channels::$btnNext, 60);
		$I->wait(3);
		$I->click(\Page\App7020\Channels::$btnNext);
		
		// Check step 2 info
		// Check if dropdown with icons is preselected
		$I->waitForElementVisible(\Page\App7020\Channels::$channelIconPicker . ' i.' . $channelInfo['channel']['icon_fa'], 60);
		// Check if text field "Icon color" is prefilled
		$I->seeInField(Page\App7020\Channels::$inputIconColor, $channelInfo['channel']['icon_color']);
		// Check if text field "Icon background color" is prefilled
		$I->seeInField(Page\App7020\Channels::$inputIconBgColor, $channelInfo['channel']['icon_bgr']);
		// Confirm "Clone"
		$I->waitForElementVisible(\Page\App7020\Channels::$btnNext, 60);
		$I->click(\Page\App7020\Channels::$btnNext);
		
		// Check if new Channel is added by counting rows in GridView. !!! The actual count is sum of channels + 1 for table header
		$I->wait(5);
		$I->seeNumberOfElements(\Page\App7020\Channels::$gridRow, 11);
	}
	
	/**
	 * Each method is a test case/scenario.
	 *
	 * @scenario Delete an admin generated channel
	 * @group PR7020-1672
	 *
	 * @param AcceptanceTester $I
	 */
	public function scenarioDeleteAdminGeneratedChannel(AcceptanceTester $I) {
		// Wait for loading 1st Not predifined channel
		$I->waitForElementVisible(\Page\App7020\Channels::$notPredifinedChannelRow . ' ' . \Page\App7020\Channels::$columnDragAndDrop, 30);
		// Open drop&down menu and click on "delete"
		$this->app7020Helper->dropDownWidgetRun($I, \Page\App7020\Channels::$notPredifinedChannelRow, 'delete');
		// Check modal info
		$I->waitForElementVisible(\Page\App7020\Channels::$deleteModal, 30);
		// Check if Statistic "Assets assigned to this channel only" is there
		$I->waitForElementVisible(\Page\App7020\Channels::$deleteStatisticLeft, 30);
		// Check if Statistic "Assets assigned also to other channels" is there
		$I->waitForElementVisible(\Page\App7020\Channels::$deleteStatisticRight, 30);
		// Confirm checkbox "Yes I want to proceed!"
		$I->waitForElementVisible(\Page\App7020\Channels::$proceedCheckbox, 30);
		$I->click(\Page\App7020\Channels::$proceedCheckbox);
		// Confirm Deletion
		$I->waitForElementVisible(\Page\App7020\Channels::$btnNext, 30);
		$I->click(\Page\App7020\Channels::$btnNext);
		$I->wait(5);
		// Check if new Channel is deleted by counting rows in GridView. !!! The actual count is sum of channels + 1 for table header
		$I->seeNumberOfElements(\Page\App7020\Channels::$gridRow, 10);
	}
	
	/**
	 * Each method is a test case/scenario.
	 *
	 * @scenario  Show an admin generated channel
	 * @group PR7020-1675
	 *
	 * @param AcceptanceTester $I
	 */
	public function scenarioHideAdminGeneratedChannel(AcceptanceTester $I) {
		// Go to "Channel" page
		$I->amOnPage(\Page\Common::route('channels/index', array(), false, \Helper\App7020\App7020General::APP_NAME));
		// Hide channel
		$this->channelsManagementHelper->showOrHideChannel($I);
	}
	
	/**
	 * Each method is a test case/scenario.
	 *
	 * @scenario  Show an admin generated channel
	 * @group PR7020-1674
	 *
	 * @param AcceptanceTester $I
	 */
	public function scenarioShowAdminGeneratedChannel(AcceptanceTester $I) {
		// Go to "Channel" page
		$I->amOnPage(\Page\Common::route('channels/index', array(), false, \Helper\App7020\App7020General::APP_NAME));
		// Hide channel
		$this->channelsManagementHelper->showOrHideChannel($I, false);
	}
	
	
	/**
	 * Each method is a test case/scenario.
	 *
	 * @scenario  Assign channel's experts
	 * @group PR7020-1659
	 *
	 * @param AcceptanceTester $I
	 */
	public function scenarioAssignChannelExperts(AcceptanceTester $I) {
		// Go to "Channel" page
		$I->amOnPage(\Page\Common::route('channels/index', array(), false, \Helper\App7020\App7020General::APP_NAME));
		
		// Get channel id
		$showIcon = Page\App7020\Channels::$notPredifinedChannelRow . ' ' . Page\App7020\Channels::$columnShow . ' ' . Page\App7020\Channels::$showIcon;
		$I->waitForElementVisible($showIcon, 30);
		$idChannel = $I->grabAttributeFrom($showIcon, Page\App7020\Channels::$channelidDataAttr);
		// Wait for loading "Experts" icon in 1st Not predifined channel and click it
		$expertsIcon = \Page\App7020\Channels::$notPredifinedChannelRow . ' ' . \Page\App7020\Channels::$columnExperts . ' ' . \Page\App7020\Channels::$expertsIcon;
		$I->waitForElementVisible($expertsIcon, 30);
		$I->click($expertsIcon);
		// Check if I am on correct page
		$I->seeCurrentUrlEquals(\Page\Common::route(\Page\App7020\Channels::ROUTE_EXPERTS, array('id'=>$idChannel), false, \Helper\App7020\App7020General::APP_NAME));
		
		// Page "Expert" tests
		// Open modal for assign expert
		$assignExpertBtn = \Page\App7020\Channels::$assignExpertsBtnExpertPage;
		$I->waitForElementVisible($assignExpertBtn, 30);
		$I->click($assignExpertBtn);
		// Select checkbox of first user in modal
		$firstUserModal = \Page\App7020\Channels::$tabUsers . ' ' . \Page\App7020\Channels::$usersTable . ' ' . \Page\App7020\Channels::$firstUserCheckbox;
		$I->waitForElementVisible($firstUserModal, 30);
		$I->checkOption($firstUserModal);
		// Get info for selected user
		$I->waitForElementVisible(\Page\App7020\Channels::$firstUserFirstName, 30);
		$selectedFirstName = $I->grabTextFrom(\Page\App7020\Channels::$firstUserFirstName);
		$I->waitForElementVisible(\Page\App7020\Channels::$firstUserLastName, 30);
		$selectedUserLastName = $I->grabTextFrom(\Page\App7020\Channels::$firstUserLastName);
		$I->waitForElementVisible(\Page\App7020\Channels::$firstUserEmail, 30);
		$selectedUserEmail = $I->grabTextFrom(\Page\App7020\Channels::$firstUserEmail);
		// Confirm modal
		$I->waitForElementVisible(\Page\App7020\Channels::$btnNextStep3, 30);
		$I->click(\Page\App7020\Channels::$btnNextStep3);
		$I->waitForJS("return $.active == 0;", 60);
		// Check if user is added
		$I->waitForText($selectedFirstName . ' ' . $selectedUserLastName . ' (' . $selectedUserEmail . ')' , 30, \Page\App7020\Channels::$expertsPgGridViewId); 
	}
	

}
