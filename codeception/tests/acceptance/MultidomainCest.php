<?php
// Autoload the DoceboCest superclass
require_once "DoceboCest.php";

/**
 * Created by PhpStorm.
 * User: asen
 * Date: 31-Aug-16
 * Time: 2:48 PM
 */
class MultidomainCest extends DoceboCest
{
    private $domainName = 'test';
    private $domainTitle = 'First client';
    private $isDbPopulated = false;

    /* @var $multidomainHelper \Helper\apps\Multidomain */
    private $multidomainHelper;

    /* @var $pluginHelper \Helper\Plugin */
    protected $pluginHelper;

    /* @var $orgchartHelper \Helper\Orgchart */
    private $orgchartHelper;

    /* @var $userHelper \Helper\User */
    protected $userHelper;

    /* @var $puserHelper \Helper\apps\PowerUser */
    protected $puserHelper;

    /* @var $userModel CoreUser*/
    protected $userModel;
    protected $targetBranchId;

    public function _inject(
        \Helper\apps\Multidomain $multidomainHelper,
        \Helper\Plugin $pluginHelper,
        \Helper\Orgchart $orgchartHelper,
        \Helper\User $userHelper,
        \Helper\apps\PowerUser $puserHelper
    ) {
        $this->multidomainHelper = $multidomainHelper;
        $this->pluginHelper = $pluginHelper;
        $this->orgchartHelper = $orgchartHelper;
        $this->userHelper = $userHelper;
        $this->puserHelper = $puserHelper;
    }

    private function createMultidomain(AcceptanceTester $I)
    {
        if ($this->isDbPopulated === false) {
            // Let's activate the Multidomain
            $this->pluginHelper->activatePluginByCodeName('Multidomain');
            Settings::save('url', Docebo::getOriginalDomain());
            // We want to create Multidomain client but we will need a org chart so let's create it
            $node = $this->orgchartHelper->createNode(404, 'Domain Branch');
            $this->targetBranchId = $node->idOrg;
            $userModel = $this->userHelper->createUser();
            $this->userModel = $userModel;
            $this->orgchartHelper->assignUserToNode($userModel->idst, $node->idOrg);
            $data = array(
                'name' => $this->domainTitle,
                'domain' => $this->domainName,
                'domain_type' => CoreMultidomain::DOMAINTYPE_SUBFOLDER,
                'org_chart' => $node->idOrg
            );
            $this->multidomainHelper->createClient($data);
            $this->isDbPopulated = true;
        }
    }

    /**
     * @before createMultidomain
     * @before login
     */
    public function godaminSetGlobalRestrictionLoginAllMultidomains(AcceptanceTester $I)
    {
//        extract($this->userHelper->getDefaultSuperadminAccount());
        $I->waitForElementVisible(\Page\SideMenu::$menuAdmin);
        $I->goToPageUsingMenu(Page\Apps\Multidomain\GlobalSettings::route(false, array(), false, 'admin'));
        $I->dontSeeCheckboxIsChecked(\Page\Apps\Multidomain\GlobalSettings::$loginRestrictionCheckbox);
        $I->checkOption(\Page\Apps\Multidomain\GlobalSettings::$loginRestrictionCheckbox);
        $I->click(\Page\Apps\Multidomain\GlobalSettings::$confirmChangesButton);
    }

    // === When global setting for login check is ON, custom settings disabled ===

    /**
     * @before createMultidomain
     * @depends godaminSetGlobalRestrictionLoginAllMultidomains
     */
    public function userLoginInAllowedDomainWithGlobalSettingOn(AcceptanceTester $I)
    {
        $I->amOnPage('/' . $this->domainName);
        $I->login($this->userModel->getClearUserId(), $this->userHelper->getDefaultPass(), false);
    }

    /**
     * @before createMultidomain
     * @depends godaminSetGlobalRestrictionLoginAllMultidomains
     */
    public function userLoginInRestrictedDomainWithGlobalSettingOn(AcceptanceTester $I)
    {
        $I->amOnPage('/' . $this->domainName);
        $userModel = $this->userHelper->createUser();
        $I->loginFail($userModel->getUserNameFormatted(), $this->userHelper->getDefaultPass(), false);
    }

    /**
     * @before createMultidomain
     * @depends godaminSetGlobalRestrictionLoginAllMultidomains
     */
    public function powerUserLoginInRestrictedDomainWithGlobalSettingOn(AcceptanceTester $I)
    {
        // PU is not assigned to any branch
        $I->amOnPage('/' . $this->domainName);
        $userModel = $this->userHelper->createUser();
        $this->puserHelper->promoteUserToPU($userModel->idst);
        $I->loginFail($userModel->getUserNameFormatted(), $this->userHelper->getDefaultPass(), false);
    }

    /**
     * @before createMultidomain
     * @depends godaminSetGlobalRestrictionLoginAllMultidomains
     */
    public function powerUserLoginInAllowedDomainWithGlobalSettingOn(AcceptanceTester $I)
    {
        // PU is assigned to the target branch
        $I->amOnPage('/' . $this->domainName);
        $userModel = $this->userHelper->createUser();
        $this->puserHelper->promoteUserToPU($userModel->idst);
        $this->puserHelper->assignBranchesToPU($userModel->idst, array($this->targetBranchId));
        $I->login($userModel->getUserNameFormatted(), $this->userHelper->getDefaultPass(), false);
    }

    /**
     * @before createMultidomain
     * @depends godaminSetGlobalRestrictionLoginAllMultidomains
     */
    public function godAdminLoginInRestrictedDomain(AcceptanceTester $I)
    {
        $I->amOnPage('/' . $this->domainName);
        extract($this->userHelper->getDefaultSuperadminAccount());
        /* @var $username*/
        /* @var $password*/
        $I->login($username, $password, false);
    }
    // === END When global setting for login check is ON, custom settings disabled ===

    // === When global setting for login check is ON, custom settings enabled, setting OFF for current multidomain ===

    /**
     * @before createMultidomain
     * @depends godaminSetGlobalRestrictionLoginAllMultidomains
     */
    private function enableCustomSettingsTurnOFFLoginSetting(){
        $cm = CoreMultidomain::model()->findByAttributes(array(
            'name' => $this->domainTitle,
            'org_chart' => $this->targetBranchId
        ));
        $cm->signInPageLayoutEnabled = 1;
        $cm->enableLoginRestriction = 0;
        $cm->save();
    }

    /**
     * @before enableCustomSettingsTurnOFFLoginSetting
     * @depends godaminSetGlobalRestrictionLoginAllMultidomains
     */
    public function userLoginInRestrictedDomainWithGlobalSettingOnCustomSettingOFF(AcceptanceTester $I)
    {
        $I->amOnPage('/' . $this->domainName);
        $userModel = $this->userHelper->createUser();
        $I->login($userModel->getUserNameFormatted(), $this->userHelper->getDefaultPass(), false);
    }

    /**
     * @before enableCustomSettingsTurnOFFLoginSetting
     * @depends godaminSetGlobalRestrictionLoginAllMultidomains
     */
    public function powerUserLoginInRestrictedDomainWithGlobalSettingOnCustomSettingOFF(AcceptanceTester $I)
    {
        // PU is not assigned to any branch
        $I->amOnPage('/' . $this->domainName);
        $userModel = $this->userHelper->createUser();
        $this->puserHelper->promoteUserToPU($userModel->idst);
        $I->login($userModel->getUserNameFormatted(), $this->userHelper->getDefaultPass(), false);
    }

    // === END When global setting for login check is ON, custom settings enabled, setting OFF for current multidomain ===

    // === When global setting for login check is OFF, custom settings enabled, setting ON for current multidomain ===

    /**
     * @before createMultidomain
     */
    private function enableCustomSettingsTurnONLoginSettingTurnOFFGlobalSetting(){
        Settings::save('user_login_check', 0);
        $cm = CoreMultidomain::model()->findByAttributes(array(
            'name' => $this->domainTitle,
            'org_chart' => $this->targetBranchId
        ));
        $cm->signInPageLayoutEnabled = 1;
        $cm->enableLoginRestriction = 1;
        $cm->save();
    }

    /**
     * @before enableCustomSettingsTurnONLoginSettingTurnOFFGlobalSetting
     */
    public function userLoginInRestrictedDomain(AcceptanceTester $I){
        $I->amOnPage('/' . $this->domainName);
        $userModel = $this->userHelper->createUser();
        $I->loginFail($userModel->getUserNameFormatted(), $this->userHelper->getDefaultPass(), false);
    }

    /**
     * @before enableCustomSettingsTurnONLoginSettingTurnOFFGlobalSetting
     */
    public function userLoginInAllowedDomain(AcceptanceTester $I){
        $I->amOnPage('/' . $this->domainName);
        $userModel = $this->userModel;
        $I->login($userModel->getUserNameFormatted(), $this->userHelper->getDefaultPass(), false);
    }

    /**
     * @before enableCustomSettingsTurnONLoginSettingTurnOFFGlobalSetting
     */
    public function powerUserLoginInRestrictedDomain(AcceptanceTester $I){
        $I->amOnPage('/' . $this->domainName);
        $userModel = $this->userHelper->createUser();
        $this->puserHelper->promoteUserToPU($userModel->idst);
        $I->loginFail($userModel->getUserNameFormatted(), $this->userHelper->getDefaultPass(), false);
    }

    /**
     * @before enableCustomSettingsTurnONLoginSettingTurnOFFGlobalSetting
     */
    public function powerUserLoginInAllowedDomain(AcceptanceTester $I){
        $I->amOnPage('/' . $this->domainName);
        $userModel = $this->userHelper->createUser();
        $this->puserHelper->promoteUserToPU($userModel->idst);
        $this->puserHelper->assignBranchesToPU($userModel->idst, array($this->targetBranchId));
        $I->login($userModel->getUserNameFormatted(), $this->userHelper->getDefaultPass(), false);
    }

    /**
     * @before enableCustomSettingsTurnONLoginSettingTurnOFFGlobalSetting
     */
    public function godAdminLoginInRestrictedDomainCustomSettingOn(AcceptanceTester $I){
        $I->amOnPage('/' . $this->domainName);
        extract($this->userHelper->getDefaultSuperadminAccount());
        $I->login($username, $password);
    }
    // === END When global setting for login check is OFF, custom settings enabled, setting ON for current multidomain ===

}