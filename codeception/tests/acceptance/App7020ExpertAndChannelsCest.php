<?php

// Autoload the DoceboCest superclass
require_once "DoceboCest.php";


/**
 * Class ExampleDoceboCest
 *
 * Put here all test cases belonging to the "example_docebo" category.
 */
class App7020ExpertAndChannelsCest extends DoceboCest {

	protected $userHelper;
	protected $orgChartHelper;
	protected $app7020Helper;
	protected $channelsManagementHelper;
	protected $expertsAndChannelsHelper;
	protected $defaultUserPass;

		public function _inject(\Helper\User $user, \Helper\Orgchart $orgChart, \Helper\App7020\App7020General $app7020General, \Helper\App7020\ExpertAndChannelsHelper $expertsAndChannelsHelper, \Helper\App7020\ChannelsManagementHelper $channelsManagementHelper) {
		$this->userHelper = $user;
		$this->orgChartHelper = $orgChart;
		$this->app7020Helper = $app7020General;
		$this->defaultUserPass = $this->userHelper->getDefaultPass();
		$this->channelsManagementHelper = $channelsManagementHelper;
		$this->expertsAndChannelsHelper = $expertsAndChannelsHelper;
	}

	/**
	 * The following code will execute before codeception starts running each test case.
	 * Put something here only if you need to add "before" actions, otherwise delete the method from this subclass
	 */
	public function _before(AcceptanceTester $I) {
		// Do not delete this to allow superclass to do its "before" actions
		parent::_before($I);

		// Add your actions here...
		$this->login($I);
//		$I->amOnPage(\Page\Common::route(\Page\App7020\ExpertsAndChannels::ROUTE_INDEX, array(), false, \Helper\App7020\App7020General::APP_NAME));
	}

	/**
	 * The following code will execute after codeception runs each test case.
	 * Put something here only if you need to add "after" actions, otherwise delete the method from this subclass
	 */
	public function _after(AcceptanceTester $I) {
		// Do not delete this to allow superclass to do its "after" actions
		parent::_after($I);

		// Add your actions here...
		// Logout after all
		$I->logout();
	}
	
	/**
	 * Each method is a test case/scenario.
	 *
	 * @scenario Scenario Expert's creation
	 *
	 * @group PR7020-1734
	 *
	 * @param AcceptanceTester $I
	 */
	public function scenarioExpertCreation(AcceptanceTester $I) {
		// Remove all experts
		$this->app7020Helper->deleteAllExperts();
		// Go to "Experts&Channels" Page
		$I->amOnPage(\Page\Common::route(\Page\App7020\ExpertsAndChannels::ROUTE_INDEX, array(), false, \Helper\App7020\App7020General::APP_NAME));
		
		$I->comment('Scenario empty assignment *** START ***');
		// Check if Hint 1 is there
		$I->waitForElementVisible(\Page\App7020\ExpertsAndChannels::$hintStep1, 30);
		// Check if Hint 2 is there
		$I->waitForElementVisible(\Page\App7020\ExpertsAndChannels::$hintStep2, 30);
		// Check grid si empty
		$I->waitForElementVisible(\Page\App7020\ExpertsAndChannels::$gridViewId . ' ' . \Page\App7020\ExpertsAndChannels::$firstRowGrid . ' ' . \Page\App7020\ExpertsAndChannels::$gridEmpty, 30);
		$I->comment('Scenario empty assignment *** END ***');
		
		$I->comment('Scenario experts creation *** START ***');
		// Create new Expert
		$this->expertsAndChannelsHelper->createExpert($I);
		$I->comment('Scenario experts creation *** END ***');
	}

	/**
	 * Each method is a test case/scenario.
	 *
	 * @scenario Multiple Assign Expert to channels
	 *
	 * @group PR7020-1735
	 *
	 * @param AcceptanceTester $I
	 */
	public function scenarioMultipleAssignExpertChannels(AcceptanceTester $I) {
		// Remove all experts
		$this->app7020Helper->deleteAllExperts();
		// Delete all Not predifined channels to be sure for the envirnoment
		$this->app7020Helper->deleteNotPredifinedChannels();
		// Go to "Experts&Channels" Page
		$I->amOnPage(\Page\Common::route(\Page\App7020\ExpertsAndChannels::ROUTE_INDEX, array(), false, \Helper\App7020\App7020General::APP_NAME));
		
		$I->comment('Scenario Multiple assignment without any Predifined Channels *** START ***');
		// Open modal for a multiple assign of experts
		$I->waitForElementVisible(\Page\App7020\ExpertsAndChannels::$multiAssignExperts, 30);
		$I->click(\Page\App7020\ExpertsAndChannels::$multiAssignExperts);
		// Check blank state in case of "No predifined channles"
		$I->waitForElementVisible(\Page\App7020\ExpertsAndChannels::$emptyChannels, 30);
		// Click to button "Create Channels"
		$I->waitForElementVisible(\Page\App7020\ExpertsAndChannels::$createChannelBtn, 30);
		$I->click(\Page\App7020\ExpertsAndChannels::$createChannelBtn);
		// Check if I am on page "channelsManagement/index"
		$I->seeCurrentUrlEquals(\Page\Common::route(\Page\App7020\Channels::ROUTE_INDEX, array(), false, \Helper\App7020\App7020General::APP_NAME));
		// Add new Predifined Channel for next sub scenario
		$this->channelsManagementHelper->addNewNotPredifinedChannel($I);
		$I->comment('Scenario Multiple assignment without any Predifined Channels *** END ***');
		
		$I->comment('Scenario Multiple assignment without experts *** START ***');
		$I->amOnPage(\Page\Common::route(\Page\App7020\ExpertsAndChannels::ROUTE_INDEX, array(), false, \Helper\App7020\App7020General::APP_NAME));
		// Open modal for a multiple assign of experts
		$I->waitForElementVisible(\Page\App7020\ExpertsAndChannels::$multiAssignExperts, 30);
		$I->click(\Page\App7020\ExpertsAndChannels::$multiAssignExperts);
		// Check blank state in case of "No Experts"
		$I->waitForElementVisible(\Page\App7020\ExpertsAndChannels::$emptyExperts, 30);
		// Click to button "Select Experts"
		$I->waitForElementVisible(\Page\App7020\ExpertsAndChannels::$createChannelBtn, 30);
		$I->click(\Page\App7020\ExpertsAndChannels::$createChannelBtn);
		// Create new Expert
		$this->expertsAndChannelsHelper->createExpert($I, false);
		$I->comment('Scenario Multiple assignment without experts *** END ***');
		
		$I->comment('Scenario Multiple assignment there IS Experts and Channels *** START ***');
		// Open modal for a multiple assign of experts
		$I->waitForElementVisible(\Page\App7020\ExpertsAndChannels::$multiAssignExperts, 30);
		$I->click(\Page\App7020\ExpertsAndChannels::$multiAssignExperts);
		// Select checkbox of first user in modal
		$firstUserModal = \Page\App7020\ExpertsAndChannels::$tabUsers . ' ' . \Page\App7020\ExpertsAndChannels::$usersTable . ' ' . \Page\App7020\ExpertsAndChannels::$firstUserCheckbox;
		$I->waitForElementVisible($firstUserModal, 30);
		$I->checkOption($firstUserModal);
		// Click to "Next" button
		$I->waitForElementVisible(\Page\App7020\ExpertsAndChannels::$btnNext, 30);
		$I->click(\Page\App7020\ExpertsAndChannels::$btnNext);
		// Select Channel
		$firstChannelCheckbox = \Page\App7020\ExpertsAndChannels::$dialogMultiAssignExpertsId . ' ' . \Page\App7020\ExpertsAndChannels::$firstChannelCheckbox;
		$I->waitForJS("return $.active == 0;", 60);
		$I->waitForElementVisible($firstChannelCheckbox, 30);
		$I->checkOption($firstChannelCheckbox);
		// Go to next step
		$I->waitForElementVisible(\Page\App7020\ExpertsAndChannels::$nextBtn, 30);
		$I->click(\Page\App7020\ExpertsAndChannels::$nextBtn);
		// Finish last step
		$I->waitForJS("return $.active == 0;", 60);
		$I->waitForElementVisible(\Page\App7020\ExpertsAndChannels::$closeBtn, 30);
		$I->click(\Page\App7020\ExpertsAndChannels::$closeBtn);
		$I->waitForJS("return $.active == 0;", 60);
		// Check if channel is added to expert
		$I->waitForText('1', 30, \Page\App7020\ExpertsAndChannels::$firstRowGrid . ' ' . \Page\App7020\ExpertsAndChannels::$assignedChannelsMainPage);
		$I->comment('Scenario Multiple assignment there IS Experts and Channels *** END ***');
	}
	
	/**
	 * Each method is a test case/scenario.
	 *
	 * @scenario Delete assignment Expert to channel
	 *
	 * @group PR7020-1736
	 *
	 * @param AcceptanceTester $I
	 */
	public function scenarioDeleteAssignmentExpertToChannel(AcceptanceTester $I) {
		// Go to "Experts&Channels" Page
		$I->amOnPage(\Page\Common::route(\Page\App7020\ExpertsAndChannels::ROUTE_INDEX, array(), false, \Helper\App7020\App7020General::APP_NAME));
		// Click on ASSIGNED CHANNELS icon
		$expertChannelLink = \Page\App7020\ExpertsAndChannels::$firstRowGrid . ' ' . \Page\App7020\ExpertsAndChannels::$assignedChannelsMainPage . ' ' . \Page\App7020\ExpertsAndChannels::$expertChannelLink;
		$I->waitForElementVisible($expertChannelLink, 30);
		// get expert id
		$expertId = $I->grabAttributeFrom($expertChannelLink, \Page\App7020\ExpertsAndChannels::$expertIdDataAttr);
		$I->click($expertChannelLink);
		// Check if I am on right page
		$I->seeCurrentUrlEquals(\Page\Common::route(\Page\App7020\ExpertsAndChannels::ROUTE_CHANNELS, array('expertId' => $expertId), false, \Helper\App7020\App7020General::APP_NAME));
		// Delete channel
		$I->waitForElementVisible(Page\App7020\ExpertsAndChannels::$deleteChannel, 30);
		$I->click(Page\App7020\ExpertsAndChannels::$deleteChannel);
		$I->waitForJS("return $.active == 0;", 60);
		$I->waitForElementVisible(Page\App7020\ExpertsAndChannels::$confirmDeletion, 30);
		$I->checkOption(Page\App7020\ExpertsAndChannels::$confirmDeletion);
		$I->waitForElementVisible(Page\App7020\ExpertsAndChannels::$nextBtn, 60);
		$I->click(Page\App7020\ExpertsAndChannels::$nextBtn);
		// Check if channel is deleted
		$I->waitForJS("return $.active == 0;", 60);
		$I->waitForElementVisible(Page\App7020\ExpertsAndChannels::$channelsEmptyGrid, 30);
	}
	
	

}
