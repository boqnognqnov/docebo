<?php
// Here you can initialize variables that will be available to your tests
/* @var $this Codeception\Subscriber\Bootstrap */
/* @var $settings array */

/* Mockup most of the $_SERVER data */
$_SERVER['SERVER_NAME'] = $settings['modules']['config']['DoceboHelper']['server_name'];
$_SERVER['SCRIPT_FILENAME'] = dirname(__FILE__).'/../../../lms/index.php';
$_SERVER['SCRIPT_NAME'] = '/lms/index.php';
$_SERVER['HTTP_HOST'] = $settings['modules']['config']['DoceboHelper']['server_name'];
$_SERVER['REMOTE_ADDR'] = '127.0.0.1'; //set some addr for the auditTrailListeners

/**
 *
 * This block must be the first one in all Yii1 App index.php files, e.g. admin/index.php, api/index.php, codeception's _bootstrap.php etc.
 *
 */
/***************************************************************************/
// Configuration helper
require_once(__DIR__. "/../../../common/config/config_helper.php");

// Include the Yii1 App specific main config!
// In its turn it loads common/config/main.php
$config = require(dirname(__FILE__).'/../../../lms/protected/config/main.php');

// Include yii (must be before loading "alias" file below!)
$yii = dirname(__FILE__).'/../../../yii/yii.php';
require_once($yii);

// Define Yii aliases
require_once(_base_ . '/common/config/parts/alias.php');
/***************************************************************************/


// Create the web app
try {
	Yii::createWebApplication($config);
} catch (CDbException $e) {
	echo $e->getMessage();
	// Check DB connection and show maintenance page if failed
	// Failure could be because of bad host, bad username/password, port, etc.
	die();
}

// Before Run custom operations
Docebo::beforeRun('lms');

CCMigration::beforeRun($settings['modules']['config']);

//problem on windows is resolved using this row. Otherwise you can get this error
//include(PHP_Invoker.php): failed to open stream: No such file or directory
YiiBase::$enableIncludePath=false;
//another known problem: https://github.com/Codeception/Codeception/issues/2124