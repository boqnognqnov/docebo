<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 2.2.2016 г.
 * Time: 16:05
 */

require_once "DoceboCest.php";

/**
 * Class GamificationRewardsCest
 * @group gamificationRewards
 */
class GamificationRewardsCest extends DoceboCest{

    protected $userHelper;
    public static $urlSettings = '/admin/index.php?r=GamificationApp/GamificationApp/settings';

    /**
     * @var $gamificationHelper \Helper\Gamification
     */
    protected $gamificationHelper;

    /**
     * @var $orgChartHelper \Helper\Orgchart
     */
    protected $orgChartHelper;

    public function _inject(\Helper\User $user, \Helper\Gamification $gamification, \Helper\Orgchart $orgChart){
        $this->userHelper = $user;
        $this->gamificationHelper = $gamification;
        $this->orgChartHelper = $orgChart;
    }


    /**
     * Superadministrator managing list of rewards for the marketplace - scenario 1
     *
     * @scenario Superadministrator not seeing new Rewards management page in admin area
     *
     * @group DOCEBO-1140
     *
     * @param AcceptanceTester $I
     */
    public function notSeeingRewardsManagementAdminMenu(AcceptanceTester $I){
        $user = $this->userHelper->createUser(Yii::app()->user->getGodadminLevelLabel());
        $this->gamificationHelper->enableGamificationApp($I);
        \PluginSettings::save('enable_reward_shop', 'off', 'GamificationApp');

        $I->login(Yii::app()->user->getRelativeUsername($user->userid), $this->userHelper->getDefaultPass());

        $I->seePluginIsActive('GamificationApp');
        $I->waitForElementVisible(\Page\SideMenu::$menuAdmin, 2);
        $I->moveMouseOver(\Page\SideMenu::$menuAdmin);
        $I->dontSeeElement(\Page\SideMenu::$menuRewards);
        if(!Settings::exists('enable_legacy_menu') || Settings::get('enable_legacy_menu') == 'off' ) {
            $I->click(\Page\SideMenu::$menuCloseBtn);
        }
        // Logout from the system
        $page = new \Page\Common($I);
        $page->logout();
    }

    /**
     * Superadministrator managing list of rewards for the marketplace - scenario 2
     *
     * @scenario Superadministrator seeing new Rewards management page in admin area
     *
     * @group DOCEBO-1140
     *
     * @param AcceptanceTester $I
     */
    public function seeingRewardsManagementAdminMenu(AcceptanceTester $I){
        $user = $this->userHelper->createUser(Yii::app()->user->getGodadminLevelLabel());
        $this->gamificationHelper->enableGamificationApp($I);
        \PluginSettings::save('enable_reward_shop', 'on', 'GamificationApp');

        $rewards = array(
            array(
                'translations' => array(
                    'english'   => array('name'=>'Reward test 1', 'description'=>'Reward test 1 description'),
                ),
            ),array(
                'translations' => array(
                    'english'   => array('name'=>'Reward 2', 'description'=>'Reward 2 description'),
                ),
            )
        );
        $this->gamificationHelper->createDummyRewards($rewards);

        $I->login(Yii::app()->user->getRelativeUsername($user->userid), $this->userHelper->getDefaultPass());

        $I->seePluginIsActive('GamificationApp');
        $I->waitForElementVisible(\Page\SideMenu::$menuAdmin, 2);
        $I->moveMouseOver(\Page\SideMenu::$menuAdmin);
        $I->seeElement(\Page\SideMenu::$menuRewards);
        $I->click(\Page\SideMenu::$menuRewards);
        $I->seeCurrentUrlEquals('/'.\Page\Gamification::$rewardsHomePage);
        $I->wait(5);
        $I->seeNumberOfElements(\Page\Gamification::$rewardsManagementTableNameTD, 2);
        $I->see('Reward test 1',\Page\Gamification::$rewardsManagementTableNameTD);
        $I->see('Reward 2',\Page\Gamification::$rewardsManagementTableNameTD);
        $I->seeElement(\Page\Gamification::$rewardsManagementAutocomplete);
        $I->fillField(\Page\Gamification::$rewardsManagementAutocomplete, 'test');
        $I->pressKey(\Page\Gamification::$rewardsManagementAutocomplete,WebDriverKeys::ENTER);
        $I->wait(10);
        $I->seeNumberOfElements(\Page\Gamification::$rewardsManagementTableNameTD, 1);
        $I->see('Reward test 1',\Page\Gamification::$rewardsManagementTableNameTD);
        // Logout from the system
        $page = new \Page\Common($I);
        $page->logout();
    }





    /**
     * Superadministrator configuring reward sets - scenario 1
     *
     * @scenario Superadministrator opening the rewards set management page
     *
     * @group DOCEBO-1269
     *
     * @param AcceptanceTester $I
     */
    public function openRewardsSetManagementPage(AcceptanceTester $I){
        $user = $this->userHelper->createUser(Yii::app()->user->getGodadminLevelLabel());

        $I->login(Yii::app()->user->getRelativeUsername($user->userid), $this->userHelper->getDefaultPass());

        $this->gamificationHelper->enableGamificationApp($I);

        $I->seePluginIsActive('GamificationApp');

        $this->gamificationHelper->createDummyRewardsSets(array(
            array(
                'name' => 'Set 1'
            ),
            array(
                'name' => 'Catalogue 2'
            ),
        ));

        $I->amOnPage(\Page\Gamification::$rewardsHomePage);
        $I->dontSee('', '.text-error');

        $I->click('a.rewards-sets');

        $I->wait(10);

        $I->see('Set 1', \Page\Gamification::$rewardsSetGridViewRow . '.name');
        $I->see('Catalogue 2', \Page\Gamification::$rewardsSetGridViewRow . '.name');

        $this->_searchForRewardsSet($I, 'Catalogue');

        $I->seeElement(\Page\Gamification::$autocompleteDropdown . ' li', ['data-value' => 'Catalogue 2']);

        // Logout from the system
        $page = new \Page\Common($I);
        $page->logout();
    }

    /**
     * Superadministrator configuring reward sets - scenario 2
     *
     * @scenario Superadministrator creating and editing a new reward set
     *
     * @group DOCEBO-1269
     *
     * @param AcceptanceTester $I
     */
    public function creatingAndEditingNewRewardSet(AcceptanceTester $I){
        $user = $this->userHelper->createUser(Yii::app()->user->getGodadminLevelLabel());

        $I->login(Yii::app()->user->getRelativeUsername($user->userid), $this->userHelper->getDefaultPass());

        $this->gamificationHelper->enableGamificationApp($I);

        $I->seePluginIsActive('GamificationApp');

        $I->amOnPage(\Page\Gamification::$rewardsSetHomePage);
        $I->dontSee('', '.text-error');

        $I->openModal('a.new-set', '.modal.modal-new-set');

        $I->wait(2);

        $I->fillField(['name' => 'GamificationRewardsSet[name]'], 'My Set');
        $I->fillTinyMceEditorById('description-textarea', 'Dummy set description');

        $I->switchToWindow();

        $I->click(\Page\Gamification::$newRewardsSetModal . ' a.confirm');

        $I->wait(3);

        $I->see('My Set', \Page\Gamification::$rewardsSetGridViewRow . '.name');
        $I->see('All branches & groups', \Page\Gamification::$rewardsSetGridViewRow . '.filter a');

        $I->openModal(\Page\Gamification::$rewardsSetGridViewRow . '.edit a.edit-set', \Page\Gamification::$editRewardsSetModal);

        $I->wait(15);

        $I->seeInFormFields('#new-rewards-set-from', array(
            'GamificationRewardsSet[name]' => 'My Set'
        ));

        $I->wait(3);

        $I->fillTinyMceEditorById('description-textarea', 'Final description');

        $I->click(\Page\Gamification::$editRewardsSetModal . ' a.confirm');

        $I->wait(5);

        $I->see('Final description', \Page\Gamification::$rewardsSetGridViewRow . '.description');

        // Logout from the system
        $page = new \Page\Common($I);
        $page->logout();
    }

    /**
     * Superadministrator configuring reward sets - scenario 3
     *
     * @scenario Superadministrator deleting an existing reward set
     *
     * @group DOCEBO-1269
     *
     * @param AcceptanceTester $I
     */
    public function deletingExistingRewardSet(AcceptanceTester $I){
        $user = $this->userHelper->createUser(Yii::app()->user->getGodadminLevelLabel());

        $I->login(Yii::app()->user->getRelativeUsername($user->userid), $this->userHelper->getDefaultPass());

        $this->gamificationHelper->enableGamificationApp($I);

        $this->gamificationHelper->createDummyRewardsSets(array(
            array(
                'name' => 'My Set',
                'filter' => 'all'
            )
        ));

        $I->seePluginIsActive('GamificationApp');

        $I->amOnPage(\Page\Gamification::$rewardsSetHomePage);
        $I->dontSee('', '.text-error');

        $I->wait(2);

        $I->see('My Set', \Page\Gamification::$rewardsSetGridViewRow . '.name');

        $I->openModal(\Page\Gamification::$rewardsSetGridViewRow . '.delete a.delete-set', '.modal.modal-delete-set');

        $I->wait(2);

        $I->checkStyledOption('input#deleteConfirmed');

        $I->click(\Page\Gamification::$deleteRewardsSetModal . ' a.confirm-button');

        $I->wait(3);

        $I->seeElement(\Page\Gamification::$rewardsSetGridViewRow . '.empty');

        // Logout from the system
        $page = new \Page\Common($I);
        $page->logout();
    }

    /**
     * Superadministrator configuring reward sets - scenario 4
     *
     * @scenario Superadministrator changing visibility for reward set
     *
     * @group DOCEBO-1269     
     *
     * @param AcceptanceTester $I
     */
    public function changingVisibilityForRewardSet(AcceptanceTester $I){
        $user = $this->userHelper->createUser(Yii::app()->user->getGodadminLevelLabel());

        $I->login(Yii::app()->user->getRelativeUsername($user->userid), $this->userHelper->getDefaultPass());

        $this->gamificationHelper->enableGamificationApp($I);

        $this->gamificationHelper->createDummyRewardsSets(array(
            array(
                'name' => 'My Set',
                'filter' => 'all'
            )
        ));

        $branchId = $this->orgChartHelper->createOrgChart('B1');
        \PHPUnit_Framework_Assert::assertNotFalse($branchId !== null, 'Cannot create branch');
        $groupId = $this->orgChartHelper->createGroup('G1');
        \PHPUnit_Framework_Assert::assertNotFalse($groupId !== null, 'Cannot create group');

        $I->seePluginIsActive('GamificationApp');

        $I->amOnPage(\Page\Gamification::$rewardsSetHomePage);
        $I->dontSee('', '.text-error');

        $I->see('My Set', \Page\Gamification::$rewardsSetGridViewRow . '.name');
        $I->see('All branches & groups', \Page\Gamification::$rewardsSetGridViewRow . '.filter');

        $I->openModal(\Page\Gamification::$rewardsSetGridViewRow . '.filter a.rewards-set-filter', '.users-selector');

        $I->wait(2);
        $I->checkStyledRadio('input#custom-selection');

        $I->executeJS("$('input.select-on-check:first').click();");
        $I->wait(1);
        $I->executeJS("$('.tabbable.main-selector-area ul#selector-tabs a[href=\"#orgchart\"]').click();");
        $I->wait(1);
        $I->executeJS("$('li#orgcharts-fancytree-node-$branchId span.fancytree-checkbox').click();");
        $I->wait(1);
        $I->click(\Page\Gamification::$usersSelectorModal . ' a.btn.btn-docebo.green.big');

        $I->wait(5);

        $I->see('My Set', \Page\Gamification::$rewardsSetGridViewRow . '.name');
        $I->see('1 Branches / 1 Groups', \Page\Gamification::$rewardsSetGridViewRow . '.filter');

        $I->openModal(\Page\Gamification::$rewardsSetGridViewRow . '.filter a.rewards-set-filter', '.users-selector');
        $I->wait(2);
        $I->seeCheckboxIsChecked('input.select-on-check');
        $I->executeJS("$('.tabbable.main-selector-area ul#selector-tabs a[href=\"#orgchart\"]').click();");
        $I->wait(3);
        $I->seeElement('.modal.users-selector li#orgcharts-fancytree-node-' . $branchId . ' span.fancytree-node.fancytree-lastsib.fancytree-partsel.fancytree-exp-nl.fancytree-ico-c');
        $I->closeModal();

        // Logout from the system
        $page = new \Page\Common($I);
        $page->logout();
    }

    private function _searchForRewardsSet(AcceptanceTester $I, $name){
        $I->seeElement(\Page\Gamification::$rewardsSetSearchField);

        $I->fillField(\Page\Gamification::$rewardsSetSearchField, $name);
        $I->wait(2);

        $I->seeNumberOfElements(\Page\Gamification::$autocompleteDropdown . ' li', 1);
    }

    /**
     * Superadministrator clicking on Admin -> Gamification -> Settings menu - scenario 1
     *
     * @scenario Superadministrator enable/disable Gamification reward shop
     *
     * @group DOCEBO-1264
     *
     * @param AcceptanceTester $I
     */
    public function changingEnableRewardShopSettings(AcceptanceTester $I){
        $user = $this->userHelper->createUser(Yii::app()->user->getGodadminLevelLabel());

        // Make sure we are using the new menu
        \Settings::save('enable_legacy_menu', 'off');

        // Login to LMS
        $I->login(Yii::app()->user->getRelativeUsername($user->userid), $this->userHelper->getDefaultPass());

        // Enable 'GamificationApp' plugin
        $this->gamificationHelper->enableGamificationApp($I);

        // Turn off the Gamification settings
        // Set 'show not awarded' and 'enable reward shop' to off !!!
        \PluginSettings::save('show_not_awarded', 'off', 'GamificationApp');
        \PluginSettings::save('enable_reward_shop', 'off', 'GamificationApp');

        // Check is the plugin active
        $I->seePluginIsActive('GamificationApp');

        // I'm on Gamification settings
//        $I->amOnPage(self::$urlSettings);

        $I->wait(3);

        // Click admin menu icon
        $I->click('div.menu > ul > li > div.tile > a[href="#admin"]');

        $I->wait(3);

        // Click on the Gamification settings link
        $I->click('div.menu div#gamification a[href="/admin/index.php?r=GamificationApp/GamificationApp/settings"]');

        $I->wait(3);

        // No errors in loading page
        $I->dontSee('', '.text-error');

        $I->wait(3);

        // See show not awarded checkbox
        $I->seeElement(\Page\Gamification::$settingShowNotAwarded);
        $I->dontSee('', \Page\Gamification::$settingShowNotAwarded . ':checked');

        // See enable reward shop
        $I->seeElement(\Page\Gamification::$rewardsSettingEnableShop);

        // Click show not awarded checkbox
        $I->click(\Page\Gamification::$settingShowNotAwarded);

        // Click enable shop checkbox
        $I->click(\Page\Gamification::$rewardsSettingEnableShop);

        // Click save
        $I->click('div.gamification-settings input[type="submit"].green');

        // Wait for page to be fully loaded
        $I->wait(3);

        // There should be no errors
        $I->dontSee('', '#dcb-feedback .alert-error');

        // load the same page again and make sure, the checkboxes are checked
        $I->amOnPage(self::$urlSettings);

        // Wait for page to be fully loaded
        $I->wait(3);

        // See show not awarded checkbox is checked
        $I->seeElement(\Page\Gamification::$settingShowNotAwardedInput . ':checked');

        // See enable reward shop is checked
        $I->seeElement(\Page\Gamification::$rewardsSettingEnableShopInput . ':checked');

        // Logout from the system
        $page = new \Page\Common($I);
        $page->logout();
    }

    /**
     * Superadministrator creating/editing/deleting a reward - scenario 1
     *
     * @scenario Superadministrator creating and editing a reward
     *
     * @group DOCEBO-1267
     *
     * @param AcceptanceTester $I
     */
    public function creatingAndEditingAReward(AcceptanceTester $I){

        $I->amGoingTo('create or edit a reward');
        $I->wait(5);

        $user = $this->userHelper->createUser(Yii::app()->user->getGodadminLevelLabel());

        $I->login(Yii::app()->user->getRelativeUsername($user->userid), $this->userHelper->getDefaultPass());

        $this->gamificationHelper->enableGamificationApp($I);

        \PluginSettings::save('enable_reward_shop', 'on', 'GamificationApp');

        $I->amOnPage('/admin/index.php?r=GamificationApp/Reward/index');

        $I->amGoingTo('Check if this is a clean index');

        $I->seeElement('li#create_reward > div > a');

        $I->click('li#create_reward > div > a');

        $I->amGoingTo('create a reward');

        $I->seeCurrentUrlEquals('/admin/index.php?r=GamificationApp/reward/edit');

        $this->checkFieldsForRewardEditPage($I);
        $I->comment('Check Fields for reward edit page is successful');

        $I->selectOption('select[name=lang_code]', 'english');
        $I->fillField('input[name=title_current]', 'New Reward 1');
        $I->fillTinyMceEditorById('description_current', 'New Reward 1 description');
        $I->fillField('input[name=RewardCoins]', 100);
        $I->fillField('input[name=RewardQuantity]', 20);
        $I->seeOptionIsSelected('input[type=radio][name=association_mode]', 'all');
        
        $I->click('input[type=submit]#save');

        $I->seeCurrentUrlEquals('/admin/index.php?r=GamificationApp/Reward/index');

        $I->dontSeeElement(\Page\Common::$modalError);

        $I->dontSeeElement('div#contests-grid > table > tbody > tr > td.empty > span.empty');

        $I->see('New Reward 1', 'div#rewards-grid > table td:nth-child(2)');
        $I->see('100', 'div#rewards-grid > table td:nth-child(3)');
        $I->see('20', 'div#rewards-grid > table td:nth-child(4)');
        $I->seeElement('div#rewards-grid > table td span.i-sprite.is-edit');
        $I->seeElement('div#rewards-grid > table td span.i-sprite.is-remove');

        $I->click('div#rewards-grid > table td span.i-sprite.is-edit');

        $I->seeCurrentUrlMatches('#/admin/index\.php\?r\=GamificationApp/Reward/edit\&id\=(\d+)#');

        $this->checkFieldsForRewardEditPage($I);

        $I->seeInField('input#title_current', 'New Reward 1');

        $newReward = array(
            'input#title_current' => 'Reward 1 Changed'
        );
        $I->seeOptionIsSelected('input[type=radio][name=association_mode]', 'all');
        
        $I->fillField('input[name=title_current]', 'Reward 1 Changed');

        $I->click('input[type=submit]#save');

        $I->seeCurrentUrlEquals('/admin/index.php?r=GamificationApp/Reward/index');

        $I->dontSeeElement(\Page\Common::$modalError);

        // Logout from the system
        $page = new \Page\Common($I);
        $page->logout();
    }
    
    private function checkFieldsForRewardEditPage(AcceptanceTester $I) {
            // There is an select input for changing the language for the reward
            $I->seeElement('select[name=lang_code]');

            // There is an input for inserting/editing the reward name
            $I->seeElement('input[name=title_current]');

            // There is an input for inserting/editing the reward description
            $I->seeElementInDOM('textarea[name=description_current]');

            // There is an input for inserting/editing the reward coins price
            $I->seeElement('input[name=RewardCoins]');

            // There is an input for inserting/editing the reward available quantity
            $I->seeElement('input[name=RewardQuantity]');

            // There is an input for attaching picture
            $I->seeElementInDOM('input[type=hidden][name=RewardPicture]');

            // There is an input for associating to all sets
            $I->seeElementInDOM('input[type=radio]#association_mode_0');

            // There is an input for associating sets manually
            $I->seeElementInDOM('input[type=radio]#association_mode_1');

            // There is an input for associating sets 
            $I->seeElementInDOM('select#sets');
    }

    /**
     * Superadministrator creating/editing/deleting a reward - scenario 2
     *
     * @scenario Superadministrator editing a reward and selecting one or more reward sets
     *
     * @group DOCEBO-1267
     *
     * @param AcceptanceTester $I
     */
    public function editingARewardAndSelectingOneOrMoreRewardSets(AcceptanceTester $I){

        $I->amGoingTo('edit a reward');
        $I->wait(5);
        $user = $this->userHelper->createUser(Yii::app()->user->getGodadminLevelLabel());

        $I->login(Yii::app()->user->getRelativeUsername($user->userid), $this->userHelper->getDefaultPass());

        $this->gamificationHelper->enableGamificationApp($I);

        \PluginSettings::save('enable_reward_shop', 'on', 'GamificationApp');

        $this->gamificationHelper->createDummyRewardsSets();
        $I->comment('I created dummy rewards sets');
        
        $rewards = array(
            array(
                'translations' => array(
                    'english'   => array('name'=>'Reward 2', 'description'=>'Reward 1 description'),
                ),
            )
        );
        
        $dummyRewardIds = $this->gamificationHelper->createDummyRewards($rewards);
        $I->comment('I created dummy rewards with ids: ' . print_r($dummyRewardIds, true));

        $I->amOnPage("/admin/index.php?r=GamificationApp/Reward/edit&id={$dummyRewardIds[0]}");

        $I->seeCurrentUrlMatches('#/admin/index\.php\?r\=GamificationApp/Reward/edit\&id\=(\d+)#');

        $I->seeCheckboxIsChecked('input[type=radio]#association_mode_0');

        $I->click('span#association_mode_1-styler');

        $I->seeElement('#sets_selector input[type=text]');

        $I->fillField('#sets_selector input[type=text]', 'Set 1');

        $I->click('#sets_selector input[type=text]');

        $I->wait('2');
        
        $I->seeElement('ul#sets_feed');

        $I->click('ul#sets_feed li:nth-of-type(1)');

        $I->click('form#reward-create-form input[type=submit]');

        $I->seeCurrentUrlEquals('/admin/index.php?r=GamificationApp/Reward/index');

        $I->dontSeeElement(\Page\Common::$modalError);

        // Get the row index of the record where the reward name is "Reward 1"
        $rowIndex = intval($I->executeJS('return $("div#rewards-grid > table > tbody > tr > td").filter(function() {return $(this).text() == "Reward 2";}).closest("tr").index();')) + 1;

        $I->see('Set 1', "div#rewards-grid > table > tbody > tr:nth-child({$rowIndex}) > td:nth-child(5)");
        
        // Logout from the system
        $page = new \Page\Common($I);
        $page->logout();
    }

    /**
     * Superadministrator creating/editing/deleting a reward - scenario 3
     *
     * @scenario Superadministrator deleting an existing reward
     *
     * @group DOCEBO-1267
     *
     * @param AcceptanceTester $I
     */
    public function deletingAnExistingReward(AcceptanceTester $I){

        $I->amGoingTo('create or edit a reward');

        $user = $this->userHelper->createUser(Yii::app()->user->getGodadminLevelLabel());

        $I->login(Yii::app()->user->getRelativeUsername($user->userid), $this->userHelper->getDefaultPass());

        $this->gamificationHelper->enableGamificationApp($I);

        \PluginSettings::save('enable_reward_shop', 'on', 'GamificationApp');

        $this->gamificationHelper->createDummyRewardsSets();

        $rewards = array(
            array(
                'translations' => array(
                    'english'   => array('name'=>'Reward 1', 'description'=>'Reward 1 description'),
                ),
            )
        );
        
        $dummyRewardIds = $this->gamificationHelper->createDummyRewards($rewards);

        $I->amOnPage("/admin/index.php?r=GamificationApp/Reward/index");

        $I->see('Reward 1', 'div#rewards-grid > table > tbody > tr > td:nth-child(2)');

        // Get the row index of the record where the reward name is "Reward 1"
        $rowIndex = intval($I->executeJS('return $("div#rewards-grid > table > tbody > tr > td").filter(function() {return $(this).text() == "Reward 1";}).closest("tr").index();')) + 1;

        $I->seeElement("div#rewards-grid > table tbody > tr:nth-child({$rowIndex}) > td:nth-child(6) span.i-sprite.is-remove.red");

        $I->click("div#rewards-grid > table tbody > tr:nth-child({$rowIndex}) > td:nth-child(6) span.i-sprite.is-remove.red");
        
        $I->wait(5);

        $I->seeElement('div.modal.delete-reward');

        $I->click('div.modal.delete-reward span#deleteConfirmed-styler');

        $I->click('div.modal.delete-reward a.confirm-button:not(.hidden)');

        $I->wait(5);
        
        $I->dontSee('Reward 2', "div#rewards-grid > table tbody > tr:nth-child({$rowIndex}) > td:nth-child(2)");
    }

    /**
     * Superadministrator assigning/unassigning rewards to a set
     *
     * @scenario Superadministrator assigning/unassigning rewards to a set
     *
     * @group DOCEBO-1270
     * @group DOCEBO-1270-s1
     *
     * @param AcceptanceTester $I
     */
    public function assignUnassignRewards(AcceptanceTester $I)
    {
        // Enable the rewards settings
        \PluginSettings::save('show_not_awarded', 'on', 'GamificationApp');
        \PluginSettings::save('enable_reward_shop', 'on', 'GamificationApp');

        // Create test user
        $user = $this->userHelper->createUser(Yii::app()->user->getGodadminLevelLabel());

        // Login in LMS
        $I->login(Yii::app()->user->getRelativeUsername($user->userid), $this->userHelper->getDefaultPass());

        // Enable Gamification Plugin
        $this->gamificationHelper->enableGamificationApp($I);

        // Verify is the Gamification plugin active
        $I->seePluginIsActive('GamificationApp');

        // Create Dummy rewards sets
        $this->gamificationHelper->createDummyRewardsSets(array(
            array(
                'name' => 'My Set',
                'filter' => 'all'
            ),
            array(
                'name' => 'My Set 2',
                'filter' => 'all'
            )
        ));

        // Create Dumm rewards
        $rewards = array(
            array(
                'coins'=>10,
                'availability'=>10,
                'translations' => array(
                    'english'=> array(
                        'name'=>'Reward 1',
                        'description'=>'Autogenerated Auto-Test Reward #1',
                ))),
            array(
                'coins' => 100,
                'availability' => 2,
                'translations' => array(
                    'english' => array(
                        'name' => 'Reward 2',
                        'description' => 'Autogenerated Auto-Test Reward #2',
            ))),
            array(
                'coins' => 1,
                'availability' => 5,
                'translations' => array(
                    'english' => array(
                        'name' =>'Reward 3',
                        'description' => 'Autogenerated Auto-Test Reward #3',
            ))),
        );
        $dummyRewardIds = $this->gamificationHelper->createDummyRewards($rewards);

        // Ger reward models
        $reward1Model = \GamificationReward::model()->findByPk($dummyRewardIds[0]);
        $reward2Model = \GamificationReward::model()->findByPk($dummyRewardIds[1]);
        $reward3Model = \GamificationReward::model()->findByPk($dummyRewardIds[2]);

        // Set rewards association to "selection" !!!
        $reward1Model->association = "selection";
        $reward1Model->save(false);
        $reward2Model->association = "selection";
        $reward2Model->save(false);
        $reward3Model->association = "selection";
        $reward3Model->save(false);

        // Get id of My Set
        $idMySet = $this->gamificationHelper->getSetIdByName('My Set');

        // Assign Reward 1 and Reward 2 to the My Set with id 1 -
        $this->gamificationHelper->assignDummyRewardToSet($dummyRewardIds[0], $idMySet);
        $this->gamificationHelper->assignDummyRewardToSet($dummyRewardIds[1], $idMySet);

        // I'm on rewards set home page
        $I->amOnPage(\Page\Gamification::$rewardsSetHomePage);
        $I->dontSee('', '.text-error');

        $I->wait(2);

        // Is there the row with the My Set data (use id of the set - 1)
        $I->seeElement(\Page\Gamification::$rewardsSetAssignedRewardsCount . ".assign-rewards-" . $idMySet);

        // Get assigned rewards count
        $rewardsCount = $I->grabTextFrom(\Page\Gamification::$rewardsSetAssignedRewardsCount . '.assign-rewards-' . $idMySet);
        \PHPUnit_Framework_Assert::assertNotFalse($rewardsCount !== 2, 'There should be two rewards assigned to My Set');


        // And I click on the rewards counter link
        $I->click(\Page\Gamification::$rewardsSetAssignedRewardsCount . '.assign-rewards-' . $idMySet);


        //And I am redirected to the Assigned Rewards page for "My set"
        $I->seeCurrentUrlMatches(\Page\Gamification::$rewardsSetAssignedRewardsPage);

        // Get associated set ids for "Reward 1" and "Reward 2"
        // Using ids we already have: $idMySet, $dummyRewardIds[0], $dummyRewardIds[1]
        $associatedSetId1 = $this->gamificationHelper->getAssociatedSetId($idMySet, $dummyRewardIds[0]);
        $associatedSetId2 = $this->gamificationHelper->getAssociatedSetId($idMySet, $dummyRewardIds[1]);

        $I->wait(3);

        //And I see two items listed "Reward 1" and "Reward 2"
        $I->seeElement(\Page\Gamification::$rewardsSetAssignedRewardListItem.'[data-key="'.$dummyRewardIds[0].'"]');
        $I->seeElement(\Page\Gamification::$rewardsSetAssignedRewardListItem.'[data-key="'.$dummyRewardIds[1].'"]');

        //And I click on the "Assign Rewards" button
        $I->openModal(\Page\Gamification::$rewardsSetAssignRewardsBtn, \Page\Gamification::$rewardsSetAssignRewardsModal);

        $I->wait(2);

        //And I see a modal window (with rewards not yet assigned to this set) -  i.e. in this case - Reward 3
        //And I see a row for "Reward 3"
        $I->seeElement(\Page\Gamification::$rewardsSetAssignRewardsModal.' '. \Page\Gamification::$rewardsSetAssignRewardsGrid . ' tr[class="sorted[]_'.$dummyRewardIds[2].'"] td.name');
        $I->dontSeeElement(\Page\Gamification::$rewardsSetAssignRewardsModal.' '. \Page\Gamification::$rewardsSetAssignRewardsGrid . ' tr[class="sorted[]_'.$dummyRewardIds[0].'"] td.name');
        $I->dontSeeElement(\Page\Gamification::$rewardsSetAssignRewardsModal.' '. \Page\Gamification::$rewardsSetAssignRewardsGrid . ' tr[class="sorted[]_'.$dummyRewardIds[1].'"] td.name');

        //And I select "Reward 3"
        $I->checkOption(\Page\Gamification::$rewardsSetAssignRewardsModal.' '. \Page\Gamification::$rewardsSetAssignRewardsGrid . ' tr[class="sorted[]_'.$dummyRewardIds[2].'"] td input.select-on-check');

        //And I click on "Confirm"
        $I->click(\Page\Gamification::$rewardsSetAssignRewardsModal. ' a.confirm-btn');

        $I->wait(8);

        // Get Associated Set Id for Reward 3
        $associatedSetId3 = $this->gamificationHelper->getAssociatedSetId($idMySet, $dummyRewardIds[2]);

        //And I see "Reward 3" is added to the list
        $I->seeElement(\Page\Gamification::$rewardsSetAssignedRewardListItem.'[data-key="'.$dummyRewardIds[2].'"]');

        //And I click on the X icon for "Reward 1"
        //And I see a deletion confirmation modal
        $I->openModal('a.delete-set-reward-'.$dummyRewardIds[0], \Page\Gamification::$rewardsSetUnassignRewardModal);

        $I->wait(5);

        //And I check "Yes, I want to proceed"
        $I->checkOption(\Page\Gamification::$rewardsSetUnassignRewardModal . ' #deleteConfirmed');

        //And I click on "Confirm"
        $I->click(\Page\Gamification::$rewardsSetUnassignRewardModal . ' a.confirm-button');

        $I->wait(5);

        //And I see "Reward 1" is no more in the list
        $I->dontSeeElement(\Page\Gamification::$rewardsSetAssignedRewardListItem.'[data-key="'.$dummyRewardIds[0].'"]');

        //And I check the items for "Reward 2" and "Reward 3"
        $I->checkStyledOption(\Page\Gamification::$rewardsSetAssignedRewardListView . ' #combo-list-item-checkbox-'.$dummyRewardIds[1]);
        $I->checkStyledOption(\Page\Gamification::$rewardsSetAssignedRewardListView . ' #combo-list-item-checkbox-'.$dummyRewardIds[2]);

        $I->wait(3);

        //And I select the dropdown "Choose an action"
        //And I select option "Unassign rewards"
        $option = $I->grabTextFrom(\Page\Gamification::$rewardsSetUnassignMassAction);
        $I->selectOption(\Page\Gamification::$rewardsSetUnassignMassActionDropDown, $option);

        $I->wait(5);

        //And I see a deletion confirmation modal with the message "You are about to unassign 2 rewards from "My set""
        //And I check "Yes, I want to proceed"
        $I->checkOption(\Page\Gamification::$rewardsSetUnassignRewardsModal . ' #deleteConfirmed');

        //And I click on "Confirm"
        $I->click(\Page\Gamification::$rewardsSetUnassignRewardsModal . ' a.confirm-button');

        $I->wait(5);

        //And I see the list of rewards is now empty
        $I->dontSeeElement(\Page\Gamification::$rewardsSetAssignedRewardListItem.'[data-key="'.$dummyRewardIds[0].'"]');
        $I->dontSeeElement(\Page\Gamification::$rewardsSetAssignedRewardListItem.'[data-key="'.$dummyRewardIds[1].'"]');
        $I->dontSeeElement(\Page\Gamification::$rewardsSetAssignedRewardListItem.'[data-key="'.$dummyRewardIds[2].'"]');

        // Logout from the system
        $page = new \Page\Common($I);
        $page->logout();
    }

    /**
     * User going to the Rewards Shop
     *
     * @scenario User opening Rewards shop and browsing the catalog
     *
     * @group DOCEBO-1272
     *
     * @param AcceptanceTester $I
     */
    public function userOpeningRewardsShop(AcceptanceTester $I){
        $user = $this->userHelper->createUser();       
        
        $this->gamificationHelper->enableGamificationApp($I);
        
        \PluginSettings::save('enable_reward_shop', 'on', 'GamificationApp');
        
        $rewardsSets = $this->gamificationHelper->createDummyRewardsSets(array(
        		array(
        				'name' => 'Reward Set 1',
        				'filter' => 'all'
        		),
        		array(
        				'name' => 'Reward Set 2',
        				'filter' => 'all'
        		)
        ));
        
        $now = Yii::app()->localtime->getUTCNow();
        
        $threeDaysBeforeNow = Yii::app()->localtime->subInterval($now, 'P3D');
        
        $rewards = array(
        		array(
        				'translations' => array(
        						'english'   => array('name'=>'Apple Watch', 'description'=>'Reward 1 description'),
        				),
        				'setId' => $rewardsSets['Reward Set 1'],
        				'date_added' => $threeDaysBeforeNow,
        				'coins' => 300,
        				'association' => 'selection'
        		),
        		array(
        				'translations' => array(
        						'english'   => array('name'=>'Regular Watch', 'description'=>'Reward 1 description'),
        				),
        				'setId' => $rewardsSets['Reward Set 2'],
        				'coins' => 100,
        				'availability' => 3,
        				'association' => 'selection'
        		),
        		array(
        				'translations' => array(
        						'english'   => array('name'=>'Special Watch', 'description'=>'Reward 1 description'),
        				),
        				'setId' => $rewardsSets['Reward Set 2'],
        				'coins' => 250,
        				'availability' => 0,
        				'association' => 'selection'
        		)
        );
        
        $dummyRewardIds = $this->gamificationHelper->createDummyRewards($rewards);
        
        $this->gamificationHelper->addUserCoins($user->idst, 350);

        $I->login(Yii::app()->user->getRelativeUsername($user->userid), $this->userHelper->getDefaultPass());

        $I->wait(3);
   
        $I->waitForElementVisible(\Page\SideMenu::$menuUser, 2);
        $I->moveMouseOver(\Page\SideMenu::$menuUser);

        $I->click(Yii::t('gamification', 'Go to Reward Shop'));
        
        $I->wait(5);
        
        $I->seeElement(\Page\Gamification::$rewardShowWidget);
        
        $I->see('350', \Page\Gamification::$rewardShopUserCoins);
        
        $I->see('All', \Page\Gamification::$rewardShopSetSelector);
        $I->see('Reward Set 1', \Page\Gamification::$rewardShopSetSelector);
        $I->see('Reward Set 2', \Page\Gamification::$rewardShopSetSelector);
        $I->click('Reward Set 1');
        $I->wait(5);
        $I->see('Apple Watch');
        $I->dontSee('Regular Watch');
        
        $I->click('All');
        $I->wait(5);
        $I->see('Apple Watch');
        $I->see('Regular Watch');
        $I->see('Special Watch');
        $I->see('NEW', '.new-reward');
        
        $I->click(\Page\Gamification::$rewardShopSearchButton);
        
        $I->fillField(\Page\Gamification::$rewardShopSearchField, 'Regular');
        $I->wait(5);
        $I->click(\Page\Gamification::$rewardShopSearchButton);
        $I->wait(5);
        $I->see('Regular Watch');
        $I->dontSee('Apple Watch');
        $I->dontSee('Special Watch');
        
        $I->click(\Page\Gamification::$rewardShopFilterButton);
        
        $I->seeElement('#filter_new_0 + span.jq-radio');

        $I->executeJS("$('input[name=price_filter_max]').val(200)");
        
        $I->click('Apply');

        $I->wait(5);

        $I->see('Regular Watch');
        $I->dontSee('Apple Watch');
        $I->dontSee('Special Watch');

        $I->click('Regular Watch');
        
        $I->wait(5);
        
        $I->seeInCurrentUrl('/buy');
        
        $I->openModal('a.redeem', '.redeem-reward');
        $I->wait(5);
        
        $I->click('a.confirm');
        $I->wait(5);
        $I->seeElement('.redeem-success');
        $I->click('a.cancel');
        $I->wait(7);
        $I->seeElement('.redeem.disabled');
        $I->see('250', \Page\Gamification::$rewardShopUserCoins);
        $I->click('Back');
        $I->wait(7);
        $I->click('Special Watch');
        $I->wait(5);
        $I->seeInCurrentUrl('/buy');
        $I->wait(5);
        $I->seeElement('.redeem.disabled');
        $I->click('Back');
        $I->wait(5);
        $I->click('Apple Watch');
        $I->wait(5);
        $I->seeInCurrentUrl('/buy');
        $I->seeElement('.redeem.disabled');
        $I->click('My reward request history');
        $I->wait(5);
        $I->see('PENDING');

        // Logout from the system
        $page = new \Page\Common($I);
        $page->logout();
    }


    /**
     * User going to the My Rewards
     *
     * @scenario User seeing "Go to Rewards Shop" and My coins in the sidebar menu
     *
     * @group DOCEBO-1273
     * @group DOCEBO-1273-s1
     *
     * @param AcceptanceTester $I
     *
     */
    public function userGoingToTheMyRewardsScenario1(AcceptanceTester $I) {
        // 1. Given I am an LMS user
        // 2. And Gamification app is active
        // 3. And the Rewards shop flag is active

        // Enable Gamification Plugin
        $this->gamificationHelper->enableGamificationApp($I);

        // Enable the rewards settings
        \PluginSettings::save('show_not_awarded', 'on', 'GamificationApp');
        \PluginSettings::save('enable_reward_shop', 'on', 'GamificationApp');

        // Verify is the Gamification plugin active
        $I->seePluginIsActive('GamificationApp');

        // 4. And I have at least one Reward set visible
        $rewardsSets = $this->gamificationHelper->createDummyRewardsSets(array(
            array(
                'name' => 'Reward Set 1',
                'filter' => 'all'
            )
        ));

        // Create test user
        $user = $this->userHelper->createUser();

        // Login in LMS
        $I->login(Yii::app()->user->getRelativeUsername($user->userid), $this->userHelper->getDefaultPass());

        $I->wait(5);

        // 5. When I click on the learner menu
        $I->click(\Page\Gamification::$learnerMenuItem);

        // 6. Then I see the "Go to Reward shop" link in the menu header
        $I->seeElement(\Page\Gamification::$rewardShopLink);

        // 7. And I see the "My Coins" counter
        $I->seeElement(\Page\Gamification::$myCoinsCounter);

        // Logout from the system
        $page = new \Page\Common($I);
        $page->logout();
    }

    /**
     * User going to the My Rewards
     *
     * @scenario User getting a gamification badge and seeing My Coins increasing
     *
     * @group DOCEBO-1273
     * @group DOCEBO-1273-s2
     *
     * @param AcceptanceTester $I
     *
     */
    public function userGoingToTheMyRewardsScenario2(AcceptanceTester $I) {
        // 1. Given I am an LMS user
        // 2. And Gamification app is active

        // Enable Gamification Plugin
        $this->gamificationHelper->enableGamificationApp($I);

        // Enable the rewards settings
        \PluginSettings::save('show_not_awarded', 'on', 'GamificationApp');
        \PluginSettings::save('enable_reward_shop', 'on', 'GamificationApp');

        // Verify is the Gamification plugin active
        $I->seePluginIsActive('GamificationApp');

        // Create test user
        $user = $this->userHelper->createUser();

        // 3. And I have 1000 gamification points
        $this->gamificationHelper->setUserCoins($user->idst, 1000);

        // 4. And I have badge B1 worth 100 points
        $badgeB1Id = $this->gamificationHelper->createBadge(100, 'B1', 'B1');

        // 5. And I am awarded badge B1
        $this->gamificationHelper->assignUserBadge($user->idst, $badgeB1Id);

        // Login in LMS
        $I->login(Yii::app()->user->getRelativeUsername($user->userid), $this->userHelper->getDefaultPass());


        $I->wait(5);

        // 6. When I click on the learner menu
        $I->click(\Page\Gamification::$learnerMenuItem);

        $I->wait(5);

        // And I see the "My Coins" counter
        // 7. Then I see the My Coins counter has value 1100
        $I->seeElement(\Page\Gamification::$myCoinsCounter);
        $I->see('1100', \Page\Gamification::$myCoinsCounter);

        // Logout from the system
        $page = new \Page\Common($I);
        $page->logout();
    }

    /**
     * User going to the My Rewards
     *
     * @scenario User visiting My Rewards history
     *
     * @group DOCEBO-1273
     * @group DOCEBO-1273-s3
     *
     * @param AcceptanceTester $I
     *
     */
    public function userGoingToTheMyRewardsScenario3(AcceptanceTester $I) {
        // Enable showing of the first name first, when the fullname is shown !!!
        \Settings::save('show_first_name_first', 'on');

        // 1. Given I am an LMS user
        // Create test user
        // 2. And my full name is "Josh Smith"
        $user = $this->userHelper->createUser(false, '*', 'John', 'Smith');


        // 3. And Gamification app is active
        // Enable Gamification Plugin
        $this->gamificationHelper->enableGamificationApp($I);

        // Verify is the Gamification plugin active
        $I->seePluginIsActive('GamificationApp');

        // 4. And the Rewards shop flag is active
        // Enable the rewards settings
        \PluginSettings::save('show_not_awarded', 'on', 'GamificationApp');
        \PluginSettings::save('enable_reward_shop', 'on', 'GamificationApp');

        // 5. And I have at least one Reward set visible
        // Prepare rewards & reward sets
        // Create Dummy rewards sets
        $this->gamificationHelper->createDummyRewardsSets(array(
            array(
                'name' => 'My Set',
                'filter' => 'all'
            ),
            array(
                'name' => 'My Set 2',
                'filter' => 'all'
            )
        ));

        // Create Dumm rewards
        $rewards = array(
            array(
                'coins'=>10,
                'availability'=>10,
                'translations' => array(
                    'english'=> array(
                        'name'=>'Reward 1',
                        'description'=>'Autogenerated Auto-Test Reward #1',
                    ))),
            array(
                'coins' => 100,
                'availability' => 2,
                'translations' => array(
                    'english' => array(
                        'name' => 'Reward 2',
                        'description' => 'Autogenerated Auto-Test Reward #2',
                    ))),
            array(
                'coins' => 1,
                'availability' => 5,
                'translations' => array(
                    'english' => array(
                        'name' =>'Reward 3',
                        'description' => 'Autogenerated Auto-Test Reward #3',
                    ))),
        );
        $dummyRewardIds = $this->gamificationHelper->createDummyRewards($rewards);

        // Get id of My Set
        $idMySet = $this->gamificationHelper->getSetIdByName('My Set');

        // Assign Reward 1, Reward 2 and Reward 3 to the My Set with id 1
        $this->gamificationHelper->assignDummyRewardToSet($dummyRewardIds[0], $idMySet);
        $this->gamificationHelper->assignDummyRewardToSet($dummyRewardIds[1], $idMySet);
        $this->gamificationHelper->assignDummyRewardToSet($dummyRewardIds[2], $idMySet);

        // Login in LMS
        $I->login(Yii::app()->user->getRelativeUsername($user->userid), $this->userHelper->getDefaultPass());

        $I->wait(5);

        // Prepare User Rewards requests; List of available statuses:
        // $STATUS_PENDING = 0;
        // $STATUS_APPROVED = 1;
        // $STATUS_REJECTED = 2;

        // 6. And I requested "Reward 1" which was approved
        $this->gamificationHelper->addRewardRequest($user->idst, $dummyRewardIds[0], 1);

        // 7. And I requested "Reward 2" which is still waiting for approval
        $this->gamificationHelper->addRewardRequest($user->idst, $dummyRewardIds[1], 0);

        // 8. And I requested "Reward 3" which has been rejected
        $this->gamificationHelper->addRewardRequest($user->idst, $dummyRewardIds[2], 2);

        // Click on the learner menu
        $I->click(\Page\Gamification::$learnerMenuItem);

        $I->wait(2);

        // 9. When I click on "View all badges & points"
        $I->click(\Page\Gamification::$viewAllBadgesPoints);

        $I->wait(5);

        // 10. Then I see a new tab "Rewards"(NOTE: and click it)
        $I->click(\Page\Gamification::$rewardsTab);

        $I->wait(3);

        // 11. And I see "Reward 1" with status "Approved" in the "My Rewards history" list
        $I->see('Reward 1', \Page\Gamification::$rewardsTabRow . '.name-'.$dummyRewardIds[0]);
        $I->see('Approved', \Page\Gamification::$rewardsTabRow . '.status-'.$dummyRewardIds[0] . ' div.request-status-approved');

        // 12. And I see "Reward 2" with status "Pending" in the "My Rewards history" list
        $I->see('Reward 2', \Page\Gamification::$rewardsTabRow . '.name-'.$dummyRewardIds[1]);
        $I->see('Pending', \Page\Gamification::$rewardsTabRow . '.status-'.$dummyRewardIds[1] . ' div.request-status-pending');

        // 13. And I see "Reward 3" with status "Rejected" in the "My Rewards history" list
        $I->see('Reward 3', \Page\Gamification::$rewardsTabRow . '.name-'.$dummyRewardIds[2]);
        $I->see('Rejected', \Page\Gamification::$rewardsTabRow . '.status-'.$dummyRewardIds[2] . ' div.request-status-rejected');

        // 14. And I click on the email icon for "Reward 1"
        $I->openModal(\Page\Gamification::$rewardsTabRow . '.message-'.$dummyRewardIds[0] . ' a', \Page\Gamification::$rewardsSendMessageModal);

        $I->wait(3);

        // 15. And I see a modal "Send Message"
        $I->seeElement(\Page\Gamification::$rewardsSendMessageModal);

        // 16. And I see the user field is pre-populated with "John Smith"
        $I->seeInField(\Page\Gamification::$rewardsSendMessageModal . ' input[name="name"]', 'John Smith');

        // 17. And I enter the message "Hello, I haven't received my reward yet"
        $I->fillTinyMceEditorById('message-textarea', "Hello, I haven't received my reward yet");


        // 18. And I click on "Send"
        $I->click(\Page\Gamification::$rewardsSendMessageModal . ' a.confirm');

        // 19. And the system sends an email to the superadministrator that approved the reward request #Not automatable
        // Skip this by now

        $I->wait(5);

        // 20. And I click on the email icon for "Reward 2"
        $I->openModal(\Page\Gamification::$rewardsTabRow . '.message-'.$dummyRewardIds[1] . ' a', \Page\Gamification::$rewardsSendMessageModal);

        $I->wait(3);

        // 21. And I see a modal "Send Message"
        $I->seeElement(\Page\Gamification::$rewardsSendMessageModal);

        // 22. And I enter the message "Hey, can anyone approve my reward request?!"
        $I->fillTinyMceEditorById('message-textarea', "Hey, can anyone approve my reward request?!");


        // 23.And I click on "Send"
        $I->click(\Page\Gamification::$rewardsSendMessageModal . ' a.confirm');

        // 24. And the system sends an email to all LMS superadministrators #Not automatable
        // Skip this by now

        $I->wait(5);

        // Logout from the system
        $page = new \Page\Common($I);
        $page->logout();
    }

    /**
     * Superadministrator configuring rewards related notifications
     *
     * @category GamificationRewards
     *
     * @scenario Superadministrator seeing notification type "User asked to redeem reward"
     *
     * @group DOCEBO-1416
     *
     * @param AcceptanceTester $I
     */
    public function seeNotificationsTypeUserAskedToRedeemReward(AcceptanceTester $I) {
        $params = array();
        
        $params['notification_type'] = 'UserAskedToRedeemReward';
        
        $params['shortcodes'] = array(
            "[first_name]",
            "[last_name]",
            "[username]",
            "[reward_name]",
            "[reward_request_date]",
            "[reward_coins]",
        );
        
        $params['userTypes'] = array(
            "Super-admin users"
        );
        
        $this->seeingNotificationTypeByParams($I, $params);
    }

    /**
     * Superadministrator configuring rewards related notifications
     *
     * @category GamificationRewards
     * 
     * @scenario Superadministrator seeing notification type "Superadministrator rejected reward request"
     *
     * @group DOCEBO-1416
     *
     * @param AcceptanceTester $I
     */
    
    public function seeingNotificationTypeSuperadministratorRejectedRewardRequest(AcceptanceTester $I) {
        $params = array();
        
        $params['notification_type'] = 'SuperadministratorRejectedRewardRequest';
        
        $params['shortcodes'] = array(
            "[first_name]",
            "[last_name]",
            "[username]",
            "[reward_name]",
            "[reward_request_date]",
            "[reward_coins]",
        );
        
        $params['userTypes'] = array(
            "Users",
            "Super-admin users"
        );
        
        $this->seeingNotificationTypeByParams($I, $params);
    }

    /**
     * Superadministrator configuring rewards related notifications
     *
     * @category GamificationRewards
     * 
     * @scenario Superadministrator seeing notification type "Superadministrator rejected reward request"
     *
     * @group DOCEBO-1416
     *
     * @param AcceptanceTester $I
     */
    
    public function seeingNotificationTypeSuperadministratorApprovedRewardRequest(AcceptanceTester $I) {
        $params = array();
        
        $params['notification_type'] = 'SuperadministratorApprovedRewardRequest';
        
        $params['shortcodes'] = array(
            "[first_name]",
            "[last_name]",
            "[username]",
            "[reward_name]",
            "[reward_request_date]",
            "[reward_coins]",
        );
        
        $params['userTypes'] = array(
            "Users",
            "Super-admin users"
        );
        
        $this->seeingNotificationTypeByParams($I, $params);
    }
    
    private function seeingNotificationTypeByParams(AcceptanceTester $I, $params = array()) {
        
        if(empty($params) || empty($params['notification_type']) || empty($params['shortcodes']) || empty($params['userTypes'])) {
            $I->fail('Attribute $params cannot be empty at \''.__CLASS__.'->'. __FUNCTION__ .'\' on line: ' . __LINE__ - 4);
        }
        
        // Enable the rewards settings
        \PluginSettings::save('enable_reward_shop', 'on', 'GamificationApp');

        // Create test superadministrator user
        $user = $this->userHelper->createUser(Yii::app()->user->getGodadminLevelLabel());

        // Login in LMS
        $I->login(Yii::app()->user->getRelativeUsername($user->userid), $this->userHelper->getDefaultPass());

        // Enable Gamification Plugin
        $this->gamificationHelper->enableGamificationApp($I);

        // Verify is the Gamification plugin active
        $I->seePluginIsActive('GamificationApp');
        
        // I am going on notification management page
        $I->amOnPage('/admin/index.php?r=notification/index');
        
        // I click on new notification button
        $I->click('a[data-dialog-title="New notification"]');
        
        // waiting untill the modal is fully loaded
        $I->wait(6);
        
        // i select the element
        $I->click('select[name=\'CoreNotification\[type\]\']');
        
        $I->selectOption('select[name=\'CoreNotification\[type\]\']', $params['notification_type']);

        $I->seeElementInDom('ul#notification_shortcodes_list li');
        
        foreach($params['shortcodes'] as $code) {
            $I->comment("Checking for shortcode: {$code}");
            $I->see($code, 'ul#notification_shortcodes_list > li');
        }

        $I->fillField('input#CoreNotification_from_name', 'AUTO TEST 1');

        $I->fillField('input#CoreNotification_from_email', 'test@auto-fake.com');

        $I->click('select[name=languages]');

        $I->selectOption('select[name=languages]', 'en');

        $I->fillField('input#tmp_subject', 'AUTO TEST 1');
        $I->executeJS("$('#tmp_subject').trigger('change');");

        $I->fillTinyMceEditorById('message-textarea', 'AUTO TEST 123');
        $I->executeJS("$('textarea').trigger('change');");

        $I->wait(2);

        $I->click('div.modal-footer > a.btn.btn-docebo.green.big.jwizard-nav');
        
        $I->wait(6);

        $I->dontSeeElement('.alert-error');
        
        $I->see('At the time of the event', 'form#edit-notification-schedule-form fieldset label:only-of-type');
        
        $I->click('div.modal-footer > a.btn.btn-docebo.green.big.jwizard-nav:nth-of-type(2)');
        
        $I->wait(8);
        
        $I->click('div.modal-footer > a.btn.btn-docebo.green.big.jwizard-nav:nth-of-type(2)');

        $I->wait(8);
        
        $nthChild = 0;
        foreach($params['userTypes'] as $key => $userType) {
            $I->see($userType, 'form#edit-notification-recipients-form > fieldset > div > div > div > div');
            $nthChild = $key + 1;
        }
        $nthChild++;
        $I->dontSeeElementInDom('form#edit-notification-recipients-form > fieldset > div > div > div > div:nth-child('.$nthChild.')');

        $I->amOnPage('/admin/index.php?r=notification/index');
        // Logout from the system

		$I->wantTo("Logout");
		$I->click(Page\Common::$logoutButton);
		$I->seeElement('.login');
    }

    /**
     * Superadministrator monitoring user reward requests
     *
     * @scenario Superadministrator opening Manage rewards requests
     *
     * @group DOCEBO-1274
     *
     * @param AcceptanceTester $I
     */
    public function openingRewardsRequestsPage(AcceptanceTester $I){
        $admin = $this->userHelper->createUser(Yii::app()->user->getGodadminLevelLabel());
        $userJohn = $this->userHelper->createUser(false, '*', 'John');
        $userSue = $this->userHelper->createUser(false, '*', 'Sue');
        $this->gamificationHelper->enableGamificationApp($I);
        \PluginSettings::save('enable_reward_shop', 'on', 'GamificationApp');
        $rewards = array(
            array(
                'translations' => array(
                    'english'   => array('name'=>'Apple Watch', 'description'=>'Apple Watch description'),
                ),
            ),array(
                'translations' => array(
                    'english'   => array('name'=>'Regular Watch', 'description'=>'Regular Watch description'),
                ),
            )
        );
        $rewardIds = $this->gamificationHelper->createDummyRewards($rewards);
        $this->gamificationHelper->createDummyRewardsRequests(array($userJohn->idst, $userSue->idst),$rewardIds);

        $I->login(Yii::app()->user->getRelativeUsername($admin->userid), $this->userHelper->getDefaultPass());

        $I->seePluginIsActive('GamificationApp');
        $I->waitForElementVisible(\Page\SideMenu::$menuAdmin, 2);
        $I->moveMouseOver(\Page\SideMenu::$menuAdmin);
        $I->click(\Page\SideMenu::$menuRewards);
        $I->seeCurrentUrlEquals('/'.\Page\Gamification::$rewardsHomePage);
        $I->wait(5);
        $I->click(\Page\Gamification::$rewardsRequestsManagementBtn);
        $I->wait(10);
//        $I->seeCurrentUrlEquals('/'.\Page\Gamification::$rewardsRequestsPage);
        $I->seeNumberOfElements(\Page\Gamification::$rewardsRequestsTableUserNameTD, 2);

        //John
        $I->seeElement(str_replace(array('{userName}','{rewardName}','{requestStatus}'),array('User John','Apple Watch','request-status-pending'),\Page\Gamification::$rewardsRequestsUserRewardStatusXPath));
        $I->click(str_replace(array('{userName}','{rewardName}'),array('User John','Apple Watch'),\Page\Gamification::$rewardsRequestsUserRewardApproveXPath));
        $I->wait(5);
        $I->seeElement(str_replace(array('{userName}','{rewardName}','{requestStatus}'),array('User John','Apple Watch','request-status-approved'),\Page\Gamification::$rewardsRequestsUserRewardStatusXPath));

        //Sue
        $I->seeElement(str_replace(array('{userName}','{rewardName}','{requestStatus}'),array('User Sue','Regular Watch','request-status-pending'),\Page\Gamification::$rewardsRequestsUserRewardStatusXPath));
        $I->click(str_replace(array('{userName}','{rewardName}'),array('User Sue','Regular Watch'),\Page\Gamification::$rewardsRequestsUserRewardRejectXPath));
        $I->wait(5);
        $I->seeElement(str_replace(array('{userName}','{rewardName}','{requestStatus}'),array('User Sue','Regular Watch','request-status-rejected'),\Page\Gamification::$rewardsRequestsUserRewardStatusXPath));

        //FILTERS
        $I->seeElement(\Page\Gamification::$rewardsRequestsFilterAll.'.checked');
        //Pending
        $I->click(\Page\Gamification::$rewardsRequestsFilterPending);
        $I->wait(5);
        $I->seeNumberOfElements(\Page\Gamification::$rewardsRequestsTableUserNameTD, 0);
        //Rejected
        $I->click(\Page\Gamification::$rewardsRequestsFilterRejected);
        $I->wait(5);
        $I->seeNumberOfElements(\Page\Gamification::$rewardsRequestsTableUserNameTD, 1);
        $I->seeElement(str_replace(array('{userName}','{rewardName}','{requestStatus}'),array('User Sue','Regular Watch','request-status-rejected'),\Page\Gamification::$rewardsRequestsUserRewardStatusXPath));
        //Search "Apple"
        $I->click(\Page\Gamification::$rewardsRequestsFilterAll);
        $I->wait(5);
        $I->fillField(\Page\Gamification::$rewardsRequestsAutocomplete, 'Apple');
        $I->pressKey(\Page\Gamification::$rewardsRequestsAutocomplete,WebDriverKeys::ENTER);
        $I->wait(5);
        $I->seeNumberOfElements(\Page\Gamification::$rewardsRequestsTableUserNameTD, 1);
        $I->seeElement(str_replace(array('{userName}','{rewardName}','{requestStatus}'),array('User John','Apple Watch','request-status-approved'),\Page\Gamification::$rewardsRequestsUserRewardStatusXPath));
        //Send email
        $I->fillField(\Page\Gamification::$rewardsRequestsAutocomplete, '');
        $I->pressKey(\Page\Gamification::$rewardsRequestsAutocomplete,WebDriverKeys::ENTER);
        $I->wait(5);
        $I->seeElement(str_replace(array('{userName}','{rewardName}','{requestStatus}'),array('User Sue','Regular Watch','request-status-rejected'),\Page\Gamification::$rewardsRequestsUserRewardStatusXPath));
        $I->click(str_replace(array('{userName}','{rewardName}'),array('User Sue','Regular Watch'),\Page\Gamification::$rewardsRequestsUserRewardSendEmailXPath));
        $I->wait(2);
        $I->seeElement(\Page\Gamification::$rewardsRequestsEmailDialog);
//        $I->seeElement(\Page\Gamification::$rewardsRequestsEmailDialogMessage);
        $I->fillTinyMceEditorById(\Page\Gamification::$rewardsRequestsEmailDialogMessage, "Hey Sue, I'm sorry but I can't send you the reward!");
        $I->click(\Page\Gamification::$rewardsRequestsEmailDialogSendBtn);

        // Logout from the system
        $page = new \Page\Common($I);
        $page->logout();
    }


    /**
     * Superadministrator making the reward visible for all sets
     *
     * @scenario Superadministrator making the reward visible for all sets
     *
     * @group DOCEBO-1270
     * @group DOCEBO-1270-s2
     *
     * @param AcceptanceTester $I
     */
    public function assignUnassignRewardsVisibleToAllSets(AcceptanceTester $I)
    {
        // 1. Given I am a superadministrator
        // Create test user
        $user = $this->userHelper->createUser(Yii::app()->user->getGodadminLevelLabel());

        // 2. And the Gamification app is ON
        // Enable the rewards settings
        \PluginSettings::save('show_not_awarded', 'on', 'GamificationApp');
        \PluginSettings::save('enable_reward_shop', 'on', 'GamificationApp');

        // Login in LMS
        $I->login(Yii::app()->user->getRelativeUsername($user->userid), $this->userHelper->getDefaultPass());

        // Enable Gamification Plugin
        $this->gamificationHelper->enableGamificationApp($I);

        // Verify is the Gamification plugin active
        $I->seePluginIsActive('GamificationApp');

        // Create Dummy rewards sets
        $this->gamificationHelper->createDummyRewardsSets(array(
            array(
                'name' => 'My Set',
                'filter' => 'all'
            ),
            array(
                'name' => 'My Set 2',
                'filter' => 'all'
            )
        ));

        // 3. And I have reward "Reward 3" with visibility to all sets.
        // Create Dummy rewards
        $rewards = array(
            array(
                'coins' => 1,
                'availability' => 5,
                'translations' => array(
                    'english' => array(
                        'name' =>'Reward 3',
                        'description' => 'Autogenerated Auto-Test Reward #3',
                    ))),
        );
        $dummyRewardIds = $this->gamificationHelper->createDummyRewards($rewards);

        // Get id of My Set
        $idMySet = $this->gamificationHelper->getSetIdByName('My Set');
        $idMySet2 = $this->gamificationHelper->getSetIdByName('My Set 2');

        // 4. When I open the Rewards set management page.
        // I'm on rewards set home page
        $I->amOnPage(\Page\Gamification::$rewardsSetHomePage);
        $I->dontSee('', '.text-error');

        $I->wait(2);

        // Is there the row with the My Set data (use id of the set - 1)
        $I->seeElement(\Page\Gamification::$rewardsSetAssignedRewardsCount . ".assign-rewards-" . $idMySet);

        // 5. Then the Reward 3 will be visible in all available sets.
        // Get assigned rewards count for My Set
        $rewardsCount = $I->grabTextFrom(\Page\Gamification::$rewardsSetAssignedRewardsCount . '.assign-rewards-' . $idMySet);
        \PHPUnit_Framework_Assert::assertNotFalse($rewardsCount !== 1, 'There should be one reward assigned to My Set');
        unset($rewardsCount);

        // Get assigned rewards count for My Set 2
        $rewardsCount = $I->grabTextFrom(\Page\Gamification::$rewardsSetAssignedRewardsCount . '.assign-rewards-' . $idMySet2);
        \PHPUnit_Framework_Assert::assertNotFalse($rewardsCount !== 1, 'There should be one reward assigned to My Set 2');

        // And I click on the rewards counter link
        $I->click(\Page\Gamification::$rewardsSetAssignedRewardsCount . '.assign-rewards-' . $idMySet);

        $I->wait(8);

        // And I see listed "Reward 3"
        $I->seeElement(\Page\Gamification::$rewardsSetAssignedRewardListItem.'[data-key="'.$dummyRewardIds[0].'"]');

        //And I click on the "Assign Rewards" button
        $I->openModal(\Page\Gamification::$rewardsSetAssignRewardsBtn, \Page\Gamification::$rewardsSetAssignRewardsModal);

        $I->wait(5);

        // 6. And it will not be possible to assign it again to a particular set.
        //And I see a modal window (with rewards not yet assigned to this set) - in this case no rewards to be shown
        //And I don't see a row for "Reward 3"
        $I->dontSeeElement(\Page\Gamification::$rewardsSetAssignRewardsModal.' '. \Page\Gamification::$rewardsSetAssignRewardsGrid . ' tr[class="sorted[]_'.$dummyRewardIds[0].'"] td.name');

        //And I click on "Close" button
        $I->click(\Page\Gamification::$rewardsSetAssignRewardsModal. ' a.close-dialog');

        $I->wait(3);

        // 7. And if i delete the reward from some set - the visibility will change to "all except that set"
        //And I select the dropdown "Choose an action"
        //And I select option "Unassign rewards"
        //And I check the "Reward 3" checkbox
        $I->checkStyledOption(\Page\Gamification::$rewardsSetAssignedRewardListView . ' #combo-list-item-checkbox-'.$dummyRewardIds[0]);

        $I->wait(3);
        $option = $I->grabTextFrom(\Page\Gamification::$rewardsSetUnassignMassAction);
        $I->selectOption(\Page\Gamification::$rewardsSetUnassignMassActionDropDown, $option);

        $I->wait(5);

        // Click confirm check box
        $I->checkOption('#deleteConfirmed');

        $I->wait(3);

        $I->click('a.confirm-button');

        $I->wait(5);

        // And I don't see listed "Reward 3"
        $I->dontSeeElement(\Page\Gamification::$rewardsSetAssignedRewardListItem.'[data-key="'.$dummyRewardIds[0].'"]');

        // 8. The visibility of "Reward 3" will be changed to "all except that set"
        // Get the reward model
        $rewardModel = \GamificationReward::model()->findByPk($dummyRewardIds[0]);
        \PHPUnit_Framework_Assert::assertNotFalse($rewardModel->association !== 'all', 'There "Reward 3" is still set to "all"');

        // Get the Reward set id, that the reward is assigned on
        $setIds = $this->gamificationHelper->getRewardSetListIds($dummyRewardIds[0]);

        $inSetsArray = in_array($idMySet, $setIds);

        // Verify, that Reward 3 doesn't belongs to My Set !!!
        \PHPUnit_Framework_Assert::assertNotFalse(!$inSetsArray, '"Reward 3" should not be assigned to "My Set"');

        // Logout from the system
        $page = new \Page\Common($I);
        $page->logout();
    }

    /**
     * Superadministrator adding Rewards Shop widget to dashboard layout
     *
     * @scenario Superadministrator adding rewards shop dashlet to layout
     *
     * @group DOCEBO-1415
     *
     * @param AcceptanceTester $I
     */
    public function addingRewardsShopDashlet(AcceptanceTester $I){
        $this->gamificationHelper->enableGamificationApp($I);

        \PluginSettings::save('enable_reward_shop', 'on', 'GamificationApp');

        $admin = $this->userHelper->createUser(Yii::app()->user->getGodadminLevelLabel());

        $layout = $this->dashboardHelper->createLayout(1,'Layout 1',true,true);
        $layoutId = $layout['id'];

        $I->login(Yii::app()->user->getRelativeUsername($admin->userid), $this->userHelper->getDefaultPass());
        $I->seePluginIsActive('GamificationApp');
        $I->amOnPage('/'.str_replace('{layoutId}',$layoutId,\Page\Dashboard::$dashboardLayoutManagementPageById));
        $I->wait(5);
        $I->click(\Page\Dashboard::$dashboardLayoutAddCellBtn);
        $I->wait(2);
        $I->seeElement(\Page\Dashboard::$listOfDashletTypes);
        $I->seeElement(\Page\Dashboard::$rewardsShopDashletBtn);
        $I->see('Shows the Gamification rewards shop inside the user dashboard for quick access',\Page\Dashboard::$rewardsShopDashletBtnDescription);
        $I->click(\Page\Dashboard::$rewardsShopDashletBtn);
        $I->click(\Page\Dashboard::$dahletSelectionNextBtn);
        $I->wait(5);
        $I->see('Rewards shop',\Page\Dashboard::$rewardsShopDashletModalTitle);
        $I->fillField(\Page\Dashboard::$rewardsShopDashletModalTitleInput, 'Exciting rewards');
        $I->seeElement(\Page\Dashboard::$rewardsShopDashletModalMaxItemsInput);
        $I->seeElement(\Page\Dashboard::$rewardsShopDashletModalRewardHistoryCheckbox.':checked');
        $I->seeElement(\Page\Dashboard::$rewardsShopDashletModalRewardShopLinkCheckbox.':checked');
        $I->click(\Page\Dashboard::$rewardsShopDashletModalConfirmBtn);
        $I->wait(10);
        $I->seeElement(\Page\Dashboard::$rewardsShopDashletInLayout);

        // Logout from the system
        $page = new \Page\Common($I);
        $page->logout();
    }
}