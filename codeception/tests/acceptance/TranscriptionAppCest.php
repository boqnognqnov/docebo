<?php
// Autoload the DoceboCest superclass
require_once "DoceboCest.php";

/**
 * Category of test cases for the login page
 */
class TranscriptionAppCest extends DoceboCest {

    /**
     * @var $userHelper \Helper\User
     */
    protected $userHelper;
    protected $pluginHelper;

    public function _inject(\Helper\User $user, \Helper\Plugin $pluginHelper){
        $this->userHelper = $user;
        $this->pluginHelper = $pluginHelper;
    }

    /**
     * Managing training institutes and courses for External Training
     *
     * @scenario Superadministrator enabling/accessing support for training institutes and courses management
     *
     * @group DOCEBO-1894
     *
     * @param AcceptanceTester $I
     */
    public function userCreatingATrainingRecordWithAdvancedSelection(AcceptanceTester $I) {
        
        $I->comment('Trying to set deafult setting.');
        Settings::save('transcripts_require_defined_list', 'off');


        $I->comment("Given I'm a Super Admin and I login");
        $U = $this->userHelper->createUser(Yii::app()->user->getGodadminLevelLabel());
        $I->login(Yii::app()->user->getRelativeUsername($U->userid), $this->userHelper->getDefaultPass());

        $I->comment("I am going to the External Training Settings page");
        $I->amOnPage('/lms/index.php?r=TranscriptsApp/TranscriptsApp/settings');

        $I->comment("I see the newly created setting called 'Transcripts Require Defined List' and this setting is not checked");
        $I->dontSeeCheckboxIsChecked('TranscriptsAppSettingsForm[transcripts_require_defined_list]');

        $I->comment("I am going to the External Training -> Activities List Page");
        $I->amOnPage('/admin/?r=TranscriptsApp/TranscriptsManagement/activitiesList');

        $I->comment("I see that the button for managing the courses and institutes is not in the DOM at all");
        $I->dontSeeElementInDOM('#manage-courses-and-institues-button');

        $I->comment("I am going to the External Training -> Settings page again to enable the previous mentioned setting");
        $I->amOnPage('/lms/?r=TranscriptsApp/TranscriptsApp/settings');

        $I->comment("I check the checkbox");
        $I->click('#external-training-settings > div:nth-child(3) > label');

        $I->comment("And submit the form");
        $I->click('input.btn.btn-docebo.green.big[type=submit][name="submit"]');

        $I->comment("I am waiting to load the page.");
        $I->wait(5);

        $I->comment("I am going to activities page");
        $I->amOnPage('/admin/?r=TranscriptsApp/TranscriptsManagement/activitiesList');

        $I->comment("And now when the option is enabled I can see the button the managing courses and institutes page");
        $I->seeElement('#manage-courses-and-institues-button');

        $I->comment("I click on that button");
        $I->click('#manage-courses-and-institues-button');

        $I->comment("And wait the page to load");
        $I->wait(5);

        $I->comment("I clarify that the URL is as I was redirected to.");
        $this->seeCurrentUrlEquals($I, '/admin/index.php?r=TranscriptsApp/TranscriptsManagement/manageCoursesAndInstitutes'); // 201604261055 The Automated tests are successful

        $I->comment("I can see the button for managing institutes page");
        $I->seeElement('#manage-institues-button');

        $I->comment("I click on that button");
        $I->click('#manage-institues-button');

        $I->comment("And wait the page to load");
        $I->wait(5);

        $I->comment("I clarify that the URL is as I was redirected to.");
        $this->seeCurrentUrlEquals($I, '/admin/index.php?r=TranscriptsApp/TranscriptsManagement/manageInstitutes'); // 201604261055 The Automated tests are successful

        $I->comment('And I click on "New institute"');
        $I->click('#create-new-institute');

        $I->comment('And I enter the name "Politecnico di Milano" in the Institute Name field');
        $I->waitForElement('#TranscriptsInstitute_institute_name');
        $I->fillField('#TranscriptsInstitute_institute_name', 'Politecnico di Milano');

        $I->comment('And I click on Confirm');
        $I->click('a.institute_confirm_button');

        $I->comment('And I see a new row for "Politecnico di Milano" is added to the list');
        $I->wait(6);
        $rowIndex = intval($I->executeJS('return $("div#transcripts-institute-management-grid > table > tbody > tr > td").filter(function() {return $(this).text() == "Politecnico di Milano";}).closest("tr").index();')) + 1;
        $I->see('Politecnico di Milano', "div#transcripts-institute-management-grid > table > tbody > tr:nth-child({$rowIndex}) > td:nth-child(1)");

        $I->comment('And I click again on "New institute"');
        $I->click('#create-new-institute');

        $I->comment('And I enter the name "Politecnico di Bali" in the Institute Name field');
        $I->waitForElement('#TranscriptsInstitute_institute_name');
        $I->fillField('#TranscriptsInstitute_institute_name', 'Politecnico di Bali');

        $I->comment('And I click on Confirm');
        $I->click('a.institute_confirm_button');

        $I->comment('And I click on the edit icon for "Politecnico di Bali"');
        $I->wait(6);
        $rowIndex = intval($I->executeJS('return $("div#transcripts-institute-management-grid > table > tbody > tr > td").filter(function() {return $(this).text() == "Politecnico di Bali";}).closest("tr").index();')) + 1;
        $I->click("div#transcripts-institute-management-grid > table > tbody > tr:nth-child({$rowIndex}) > td:nth-child(2) a.institute-action-edit");

        $I->comment('And I enter the value "Politecnico di Bari" in the Institute Name field');
        $I->waitForElement('#TranscriptsInstitute_institute_name');
        $I->fillField('#TranscriptsInstitute_institute_name', 'Politecnico di Bari');

        $I->comment('And I click on Confirm');
        $I->click('a.institute_confirm_button');

        $I->comment('And I see the value "Politecnico di Bari" is updated in the list');
        $I->wait(6);
        $rowIndex = intval($I->executeJS('return $("div#transcripts-institute-management-grid > table > tbody > tr > td").filter(function() {return $(this).text() == "Politecnico di Bari";}).closest("tr").index();')) + 1;
        $I->see('Politecnico di Bari', "div#transcripts-institute-management-grid > table > tbody > tr:nth-child({$rowIndex}) > td:nth-child(1)");

        $I->comment('And I click again on "New institute"');
        $I->click('#create-new-institute');

        $I->comment('And I enter the name "Temporary institute" in the Institute Name field');
        $I->waitForElement('#TranscriptsInstitute_institute_name');
        $I->fillField('#TranscriptsInstitute_institute_name', 'Temporary institute');

        $I->comment('And I click on Confirm');
        $I->click('a.institute_confirm_button');

        $I->comment('And I click on the Delete icon for "Temporary institute"');
        $I->wait(6);
        $rowIndex = intval($I->executeJS('return $("div#transcripts-institute-management-grid > table > tbody > tr > td").filter(function() {return $(this).text() == "Temporary institute";}).closest("tr").index();')) + 1;
        $I->click("div#transcripts-institute-management-grid > table > tbody > tr:nth-child({$rowIndex}) > td:nth-child(2) a.institute-action-delete");

        $I->comment('And I see the deletion confirmation modal');
        $I->waitForElementVisible('#form-delete-reward-set > div.row-fluid > div > label');
        $I->click('#form-delete-reward-set > div.row-fluid > div > label');

        $I->comment('And I confirm I want to proceed');
        $I->click('body > div.modal.delete-Institute > div.modal-footer > a.btn.btn-docebo.green.big.confirm-button');

        $I->comment('And I see the training institute "Temporary institute" is removed from the list');
        $I->wait(8);
        $rowIndex = intval($I->executeJS('return $("div#transcripts-institute-management-grid > table > tbody > tr > td").filter(function() {return $(this).text() == "Temporary institute";}).closest("tr").index();')) + 1;
        $I->dontSee('Temporary institute', 'div#transcripts-institute-management-grid > table > tbody > tr > td');

        $I->comment('And I click on "Back"');
        $I->click('#content > div:nth-child(1) > h3 > a');

        $I->comment('And I am redirected to the "Courses and institutes" page');
        $this->seeCurrentUrlEquals($I, '/admin/index.php?r=TranscriptsApp/TranscriptsManagement/manageCoursesAndInstitutes');

        $I->comment('And I click on "Create new course"');
        $I->click('#edit-course');
        $I->waitForElement('#TranscriptsCourse_course_name');

        $I->comment('And I enter "Software engineering" in the Course name field');
        $I->fillField('#TranscriptsCourse_course_name', 'Software engineering');

        $I->comment('And I select Elearning as course type');
        $I->click('label[for="course-type-elearning"]');

        $I->comment('And I click on Confirm');
        $I->click('body > div.modal.modal-edit-course > div.modal-footer > a.btn.btn-docebo.green.big');

        $I->comment('And I see a new row "Software engineering" is added to the list');
        $I->wait(6);
        $rowIndex = intval($I->executeJS('return $("div#transcripts-course-management-grid > table > tbody > tr > td").filter(function() {return $(this).text() == "Software engineering";}).closest("tr").index();')) + 1;
        $I->see('Software engineering', "#transcripts-course-management-grid > table > tbody > tr:nth-child({$rowIndex}) > td:nth-child(1)");

        $I->comment('And I see the new row has the value "Assign a training institute" in the "Training institute" column');
        $I->see('Assign a training institute', "#transcripts-course-management-grid > table > tbody > tr:nth-child({$rowIndex}) > td:nth-child(3)");

        $I->comment('And I click on the link');
        $I->click("#transcripts-course-management-grid > table > tbody > tr:nth-child({$rowIndex}) > td:nth-child(3) > a.institutesLink");

        $I->comment('And I see a dropdown is displayed with two values: "Politecnico di Milano" and "Politecnico di Bari"');
        $I->click("#transcripts-course-management-grid > table > tbody > tr:nth-child({$rowIndex}) > td:nth-child(3) > select.institutesLinkSelect");
        $I->see('Politecnico di Milano', "#transcripts-course-management-grid > table > tbody > tr:nth-child({$rowIndex}) > td:nth-child(3) > select.institutesLinkSelect > option:nth-child(2)");
        $I->see('Politecnico di Bari', "#transcripts-course-management-grid > table > tbody > tr:nth-child({$rowIndex}) > td:nth-child(3) > select.institutesLinkSelect > option:nth-child(3)");

        $I->comment('And I select "Politecnico di Bari"');
        $I->click("div#transcripts-course-management-grid > table > tbody > tr:nth-child({$rowIndex}) > td:nth-child(3) > select.institutesLinkSelect > option:nth-child(3)");

        $I->comment('And I see the column value is updated with the value "Politecnico di Bari"');
        $I->waitForElementVisible("div#transcripts-course-management-grid > table > tbody > tr:nth-child({$rowIndex}) > td:nth-child(3) > a.institutesLink");
        $I->see('Politecnico di Bari', "div#transcripts-course-management-grid > table > tbody > tr:nth-child({$rowIndex}) > td:nth-child(3) > a.institutesLink");

        $I->comment('And I click on "Create new course"');
        $I->click('#edit-course');
        $I->waitForElement('#TranscriptsCourse_course_name');

        $I->comment('And I enter "Software engineering wrong" in the Course name field');
        $I->fillField('#TranscriptsCourse_course_name', 'Software engineering wrong');

        $I->comment('And I select Classroom as course type');
        $I->click('label[for="course-type-classroom"]');

        $I->comment('And I click on Confirm');
        $I->click('body > div.modal.modal-edit-course > div.modal-footer > a.btn.btn-docebo.green.big');

        $I->comment('And I see a new row "Software engineering wrong" is added to the list');
        $I->wait(6);
        $rowIndex = intval($I->executeJS('return $("div#transcripts-course-management-grid > table > tbody > tr > td").filter(function() {return $(this).text() == "Software engineering wrong";}).closest("tr").index();')) + 1;
        $I->see('Software engineering wrong', "#transcripts-course-management-grid > table > tbody > tr:nth-child({$rowIndex}) > td:nth-child(1)");

        $I->comment('And I click on the edit icon for this new row');
        $I->click("#transcripts-course-management-grid > table > tbody > tr:nth-child({$rowIndex}) > td:nth-child(4) > a.course-action-edit");
        $I->waitForElement('#TranscriptsCourse_course_name');

        $I->comment('And I change the type to Elearning');
        $I->click('label[for="course-type-elearning"]');

        $I->comment('And I click on Confirm');
        $I->click('body > div.modal.modal-edit-course > div.modal-footer > a.btn.btn-docebo.green.big');
        $I->wait(6);

        $I->comment('And I click on the Delete icon for "Software engineering wrong"');
        $I->click("#transcripts-course-management-grid > table > tbody > tr:nth-child({$rowIndex}) > td:nth-child(4) > a.course-action-delete");

        $I->comment('And I see the deletion confirmation modal');
        $I->waitForElementVisible('#form-delete-reward-set');

        $I->comment('And I confirm I want to proceed');
        $I->click('#deleteConfirmed-styler');
        $I->click('body > div.modal.delete-course > div.modal-footer > a.btn.btn-docebo.green.big.confirm-button');

        $I->comment('And I see the training institute "Software engineering wrong" is removed from the list');
        $I->wait(6);
        $I->dontSee('Software engineering wrong', 'div#transcripts-course-management-grid > table > tbody > tr > td');

        $I->comment("Fast Logout");
        $I->amOnPage('/lms/index.php?r=site/logout');
    }

    /**
     * @before activatePluginTranscripts
     * @before login
     * @param AcceptanceTester $I
     */
    public function adminChangeOptionToRequireUsersChooseTrainingInstitute(AcceptanceTester $I){

        $I->goToPageUsingMenu(\Page\Apps\Transcripts\TranscriptsSettings::route());
        $I->dontSeeCheckboxIsChecked(\Page\Apps\Transcripts\TranscriptsSettings::$checkboxRequireUserSelectTrainingInstitute);
        $I->checkStyledOption(\Page\Apps\Transcripts\TranscriptsSettings::$checkboxRequireUserSelectTrainingInstitute);
        $I->seeCheckboxIsChecked(\Page\Apps\Transcripts\TranscriptsSettings::$checkboxRequireUserSelectTrainingInstitute);
        $I->click(\Page\Apps\Transcripts\TranscriptsSettings::$confirmChangesButton);

        $I->wait(2);
        $courseNames = array('C1', 'C2', 'C3');
        $institutesNames = array('T1', 'T2');
        $this->createCoursesAndInstitutesAndAssignThem($I, $courseNames, $institutesNames);
        $I->waitForElementVisible(\Page\Apps\Transcripts\ManageCoursesAndInstitutes::$loadingGrid);
        $I->waitForElementNotVisible(\Page\Apps\Transcripts\ManageCoursesAndInstitutes::$loadingGrid);
        $coursesWithInstitutesIDS = $this->getAllInstitutesAndRelatedCourses($I);
        $institutesIDS = array_keys($coursesWithInstitutesIDS);
        $I->goToPageUsingMenu(\Page\apps\Transcripts\TranscriptsManagement::route());
        $I->waitForElementVisible(\Page\apps\Transcripts\TranscriptsManagement::$newActivityButton);

        $I->openModal(\Page\apps\Transcripts\TranscriptsManagement::$newActivityButton);
        $I->seeElement(\Page\apps\Transcripts\TranscriptsManagement::$newActivityModalSelectInstitute);
        $I->seeElement(\Page\apps\Transcripts\TranscriptsManagement::$newActivityModalSelectCourse);
        $I->seeDropdownContainsAllValues(\Page\apps\Transcripts\TranscriptsManagement::$newActivityModalSelectInstitute, $institutesIDS);
        $I->seeDropdownContainsAllValues(\Page\apps\Transcripts\TranscriptsManagement::$newActivityModalSelectCourse, $coursesWithInstitutesIDS[$institutesIDS[0]]);
        $I->selectOption(\Page\apps\Transcripts\TranscriptsManagement::$newActivityModalSelectInstitute, $institutesIDS[1]);
        $I->seeDropdownContainsAllValues(\Page\apps\Transcripts\TranscriptsManagement::$newActivityModalSelectCourse, $coursesWithInstitutesIDS[$institutesIDS[1]]);
        $I->wait(2);
    }

    /**
     * @before activatePluginTranscripts
     * @before login
     * @param AcceptanceTester $I
     */
    public function adminChangeOptionToNOTRequireUsersChooseTrainingInstitute(AcceptanceTester $I){

        $isOptionChecked = Settings::get('transcripts_require_defined_list', null, true);
        if($isOptionChecked == 'on'){
            $I->goToPageUsingMenu(\Page\Apps\Transcripts\TranscriptsSettings::route());
            // disable the required checkbox
            $I->unCheckStyledOption(\Page\Apps\Transcripts\TranscriptsSettings::$checkboxRequireUserSelectTrainingInstitute);
            $I->click(\Page\Apps\Transcripts\TranscriptsSettings::$confirmChangesButton);
            $I->wait(2);
        }

        // go to create new activity
        $I->goToPageUsingMenu(\Page\apps\Transcripts\TranscriptsManagement::route());
        $I->waitForElementVisible(\Page\apps\Transcripts\TranscriptsManagement::$newActivityButton);

        $I->openModal(\Page\apps\Transcripts\TranscriptsManagement::$newActivityButton);
        $I->dontSeeElement(\Page\apps\Transcripts\TranscriptsManagement::$newActivityModalSelectInstitute);
        $I->dontSeeElement(\Page\apps\Transcripts\TranscriptsManagement::$newActivityModalSelectCourse);
        $I->wait(2);
    }

    /**
     * @before activatePluginTranscripts
     * @before login
     * @param AcceptanceTester $I
     */
    public function userCreateActivityWhenChoosingCourseIsRequired(AcceptanceTester $I){

        $I->goToPageUsingMenu(\Page\Apps\Transcripts\TranscriptsSettings::route());
        $I->dontSeeCheckboxIsChecked(\Page\Apps\Transcripts\TranscriptsSettings::$checkboxRequireUserSelectTrainingInstitute);
        $I->checkStyledOption(\Page\Apps\Transcripts\TranscriptsSettings::$checkboxRequireUserSelectTrainingInstitute);
        $I->checkStyledOption(\Page\Apps\Transcripts\TranscriptsSettings::$checkboxAllowUsersToAddActivity);
        $I->seeCheckboxIsChecked(\Page\Apps\Transcripts\TranscriptsSettings::$checkboxRequireUserSelectTrainingInstitute);
        $I->click(\Page\Apps\Transcripts\TranscriptsSettings::$confirmChangesButton);

        $I->wait(2);
        $this->createCoursesAndInstitutesAndAssignThem($I, array('C11', 'C22', 'C33'), array('T11', 'T22'));
        $coursesWithInstitutesIDS = $this->getAllInstitutesAndRelatedCourses($I);
        $institutesIDS = array_keys($coursesWithInstitutesIDS);
        $jonny = $I->haveFriend('jonny');
        $jonny->does(function(AcceptanceTester $I) use($coursesWithInstitutesIDS, $institutesIDS){
            $U = $this->userHelper->createUser();
            $I->login(Yii::app()->user->getRelativeUsername($U->userid), $this->userHelper->getDefaultPass());
            $I->goToPageUsingMenu(\Page\MyActivities::route());
            $I->click(\Page\MyActivities::$tabExternalTrainingLink);
            $I->waitForElementVisible(\Page\MyActivities::$new_activity);
            $I->openModal(\Page\MyActivities::$new_activity);
            $I->waitForElementVisible(\Page\apps\Transcripts\TranscriptsManagement::$newActivityModalSelectInstitute);
            $I->waitForElementVisible(\Page\apps\Transcripts\TranscriptsManagement::$newActivityModalSelectCourse);
            $I->seeDropdownContainsAllValues(\Page\apps\Transcripts\TranscriptsManagement::$newActivityModalSelectCourse, $coursesWithInstitutesIDS[$institutesIDS[0]]);
            $I->wait(2);
        });
    }

    /**
     * @before activatePluginTranscripts
     * @before login
     * @param AcceptanceTester $I
     */
    public function userCreateActivityWhenChoosingCourseIsNOTRequired(AcceptanceTester $I){

        $I->goToPageUsingMenu(\Page\Apps\Transcripts\TranscriptsSettings::route());
        $isUserAllowedToAddActivity = Settings::get('transcripts_allow_users', null, true);
        if($isUserAllowedToAddActivity == 'off')
            $I->checkStyledOption(\Page\Apps\Transcripts\TranscriptsSettings::$checkboxAllowUsersToAddActivity);

        $isOptionChecked = Settings::get('transcripts_require_defined_list', null, true);
        if($isOptionChecked == 'on'){
            // disable the required checkbox
            $I->unCheckStyledOption(\Page\Apps\Transcripts\TranscriptsSettings::$checkboxRequireUserSelectTrainingInstitute);
        }

        $I->click(\Page\Apps\Transcripts\TranscriptsSettings::$confirmChangesButton);
        $I->wait(2);
        $jonny = $I->haveFriend('jonny');
        $jonny->does(function(AcceptanceTester $I){
            $U = $this->userHelper->createUser();
            $I->login(Yii::app()->user->getRelativeUsername($U->userid), $this->userHelper->getDefaultPass());
            $I->goToPageUsingMenu(\Page\MyActivities::route());
            $I->click(\Page\MyActivities::$tabExternalTrainingLink);
            $I->waitForElementVisible(\Page\MyActivities::$new_activity);
            $I->openModal(\Page\MyActivities::$new_activity);
            $I->waitForElementVisible(\Page\apps\Transcripts\TranscriptsManagement::$newActivityModalInputInstitute);
            $I->waitForElementVisible(\Page\apps\Transcripts\TranscriptsManagement::$newActivityModalInputCourse);
            $I->wait(2);
        });
    }

    private function waitForJSResult($rowIndex, $jsFunc) {
        while($rowIndex == -1) {
            if($rowIndex == -1) {
                $rowIndex = intval($I->executeJS($jsFunc));
            } else {
                break;
            }
        }
        $rowIndex += 1;

        return $rowIndex;
    }

    private function seeCurrentUrlEquals(AcceptanceTester $I, $url) {
        $I->waitForJS('return document.readyState == "complete"');
        $I->seeCurrentUrlEquals($url);
    }

    private function createCoursesAndInstitutesAndAssignThem(AcceptanceTester $I, $courseNames, $institutesNames){
        // go to create courses and institutes
        $I->goToPageUsingMenu(\Page\Apps\Transcripts\TranscriptsManagement::route());
        $I->waitForElementVisible(\Page\apps\Transcripts\TranscriptsManagement::$manageCoursesInstitutesButton);
        $I->click(\Page\apps\Transcripts\TranscriptsManagement::$manageCoursesInstitutesButton);

        // creating new courses
        $this->createCourses($I, $courseNames);
        $coursesIds = Yii::app()->db->createCommand()->select('id')->from(TranscriptsCourse::model()->tableName())
            ->order('id DESC')->limit(count($courseNames))->queryColumn();

        // go to create some training institutes
        $this->createInstitutes($I, $institutesNames);
        $institutesIds = Yii::app()->db->createCommand()->select('id')->from(TranscriptsInstitute::model()->tableName())
            ->order('id DESC')->limit(count($institutesNames))->queryColumn();
        // go back to assign some institutes
        $I->click(\Page\Apps\Transcripts\ManageInstitutes::$backButton);
        $I->waitForElementVisible(\Page\Apps\Transcripts\ManageCoursesAndInstitutes::$createNewCourseButton);

        // search for course name and assign to institute
        $br = 0;
        foreach ($courseNames as $k => $courseName) {
            $I->fillField(\Page\Apps\Transcripts\ManageCoursesAndInstitutes::$searchFieldGrid, $courseName);
            $I->pressKey(\Page\Apps\Transcripts\ManageCoursesAndInstitutes::$searchFieldGrid, \Facebook\WebDriver\WebDriverKeys::ENTER);
            $I->waitForElementVisible(array('css'=>\Page\Apps\Transcripts\ManageCoursesAndInstitutes::$loadingGrid));
            $I->waitForElementNotVisible(array('css'=>\Page\Apps\Transcripts\ManageCoursesAndInstitutes::$loadingGrid));
            $I->click(\Page\Apps\Transcripts\ManageCoursesAndInstitutes::$assignInstituteLink);

            if(!array_key_exists($br, $institutesIds)) $br = 0;
            $targetId = $institutesIds[$br];
            $I->selectOption(\Page\Apps\Transcripts\ManageCoursesAndInstitutes::$assignInstituteDropdownVisible, $targetId);
            $br++;
        }
    }

    private function getAllInstitutesAndRelatedCourses(AcceptanceTester $I){
        $dataFromDb = Yii::app()->db->createCommand()
            ->select('tc.id AS c_id, ti.id AS i_id')->from(TranscriptsCourse::model()->tableName().' tc')
            ->join(TranscriptsInstitute::model()->tableName().' ti', 'tc.institute_id=ti.id')->queryAll();
        $result = array();
        foreach ($dataFromDb as $row) {
            $result[$row['i_id']][] = $row['c_id'];
        }
        return $result;
    }

    private function createCourses(AcceptanceTester $I, $names){
        $I->wait(2);
        foreach ($names as $name) {
            $I->openModal(\Page\Apps\Transcripts\ManageCoursesAndInstitutes::$createNewCourseButton);
            $I->fillField(\Page\Apps\Transcripts\ManageCoursesAndInstitutes::$courseNameField, $name);
            $I->click(\Page\Apps\Transcripts\ManageCoursesAndInstitutes::$confirmButtonModal);
            $I->wait(2);
        }
    }

    private function createInstitutes(AcceptanceTester $I, $names){
        $I->click(\Page\apps\Transcripts\ManageCoursesAndInstitutes::$manageInstitutesButton);
        foreach($names as $name){
            $I->openModal(\Page\Apps\Transcripts\ManageInstitutes::$createNewInstituteButton);
            $I->waitForElementVisible(\Page\Apps\Transcripts\ManageInstitutes::$instituteNameField);
            $I->fillField(\Page\Apps\Transcripts\ManageInstitutes::$instituteNameField, $name);
            $I->click(\Page\Apps\Transcripts\ManageInstitutes::$confirmButton);
            $I->wait(2);
        }
    }
}
