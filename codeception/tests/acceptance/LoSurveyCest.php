<?php

// Autoload the DoceboCest superclass
require_once "DoceboCest.php";

class LoSurveyCest extends  DoceboCest
{
    /**
     * @var Helper\Course
     */
    protected $courseHelper;

    /**
     * @var Helper\Lo
     */
    protected $loHelper;

    /**
     * @var Helper\User
     */
    protected $usersHelper;

    /**
     * @var Helper\Plugin
     */
    protected $pluginHelper;

    /**
     * @var Helper\apps\PowerUser
     */
    protected $puHelper;

    /**
     * Inject dependencies
     * @param \Helper\Course $courseHelper
     */
    protected function _inject(\Helper\Course $courseHelper, \Helper\LO $loHelper, \Helper\User $usersHelper, \Helper\Plugin $pluginHelper, \Helper\apps\PowerUser $puHelper) {
        $this->courseHelper = $courseHelper;
        $this->loHelper = $loHelper;
        $this->usersHelper = $usersHelper;
        $this->pluginHelper = $pluginHelper;
        $this->puHelper = $puHelper;
    }

    /**
     * @scenario Instructor managing a likert scale question in a survey
     *
     * @group DOCEBO-1695
     * @group DOCEBO-1696
     *
     * @param AcceptanceTester $I
     */

    public function instructorManagingALikertScaleQuestionInASurvey(AcceptanceTester $I){

        // Well in order to test the New Survey question types we need to create a Course with Survey LO
        $course = $this->courseHelper->createElearningCourseWithCode('C1');
        $user = $this->usersHelper->createUser();
        $this->courseHelper->assignUserAs(LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR, $user, $course);
        $survey = $this->loHelper->createSurvey($user->idst, $course->idCourse, 'S1');
        $page = new \Page\Course\LO\Poll\LikertScale($I);
        $question1 = "What color is Napoleon's white horse?";
        $question2 = "What color is the red sky?";
        $question3 = "What color is the blue sky?";
        $scale1 = "Green";
        $scale2 = "Blue";
        $scale3 = "Red";
        $scale4 = "White";

        // We assume that we already have Course And Test so let's get the Test's OrgChart ID
        $org = Yii::app()->db->createCommand()
            ->select('idOrg')
            ->from(LearningOrganization::model()->tableName())
            ->where(array('and', 'idResource = :lo' , 'objectType=:objectType'))
            ->queryScalar(array(':lo' => $survey->getPrimaryKey(), ':objectType' => LearningOrganization::OBJECT_TYPE_POLL));

        $I->login($user->getUserNameFormatted(), $this->usersHelper->getDefaultPass());
        // After the Survey is created let's visit the it's page
        $I->amOnPage($page::route(false, array('id_object' => $org, 'course_id' => $course->idCourse)));
        // Ok We are on the Survey Edit page, so let's click in the "ADD QUESTION" button
        $I->click($page::$pollTypes);
        $type = str_replace('{type}' , $page::TYPE_LIKERT_SCALE, $page::$templateType);
        // Does the "Likert scale" question type existing ?
        $I->seeElement($type);
        // If yes then let's click on it
        $I->click($type);
        // Well the click should redirect us on certain page "question/create"
        // Let's see if that is true
        $createUrl = $page::route('poll/question/create', array('id_poll' => $survey->id_poll, 'course_id' => $course->idCourse, 'question_type' => $page::TYPE_LIKERT_SCALE));
        $I->seeInCurrentUrl($createUrl);
        // Add question title
        $page->addQuestionTitle("Please answer the question");
        // Add new questions
        $page->addNewQuestion(array( $question1, $question2 ));
        // Add new Likers scale
        $page->addNewLikertScale(array($scale1, $scale2, $scale3));
        // Edit the first question and change it's value
        $page->editQuestionItem($question2, $question3);
        // I see that the value is now changed in the table
        $I->see($question3, $page::$question_table);
        // Edit the first Scale and change it's value
        $page->editScaleItem($scale1, $scale4);
        // I see that the value is now changed in the table
        $I->see($scale4, $page::$scale_table);
        // I delete the "Red" scale element
        $page->deleteScaleItem($scale3);
        // After it's delete we should not see it anymore
        $I->dontSee($scale3, $page::$scale_table);
        // Let's save everything
        $I->click($page::$save_changes);
        // Let's check if we had got back and ont Survey edit page
        $I->seeInCurrentUrl($page::route(false, array( 'course_id' => $course->idCourse, 'id_object' => $org )));
        // Let's wait till the table content appear
        $I->waitForElementNotVisible($page::$poll_questions_table_loading);
        // I see that the pool question table contains the newly created element
        $I->see(Yii::t('poll', '_QUEST_'.strtoupper(LearningPollquest::POLL_TYPE_LIKERT_SCALE)), $page::$poll_questions_table);
        // Let's click on the edit button of the survey question type
        $editQuestionType = str_replace('{text}' , Yii::t('poll', '_QUEST_'.strtoupper(LearningPollquest::POLL_TYPE_LIKERT_SCALE)), $page::$poll_question_table_edit_button_template);
        $I->click($editQuestionType);
        // And let's see if we are located on the likert scale edit page
        $editUrl = $page::route('poll/question/update', array('id_poll' => $survey->id_poll, 'course_id' => $course->idCourse));
        $I->seeInCurrentUrl($editUrl);
        // Let's see if the certain tables contain the proper values
        $I->see($question1, $page::$question_table);
        $I->see($question3, $page::$question_table);
        $I->see($scale2, $page::$scale_table);
        $I->see($scale4, $page::$scale_table);
        // Let's add one more question
        $page->addNewQuestion("What color is the red rose?");
        // And again to save eveything
        $I->click($page::$save_changes);
        // We should be redirected to Survey edit page
        $I->seeInCurrentUrl($page::route(false, array( 'course_id' => $course->idCourse, 'id_object' => $org)));
        // Let's create new Liker Scale question type
        $I->click($page::$pollTypes);
        $I->click($type);
        // The Url should be changed
        $I->seeInCurrentUrl($createUrl);
        // Let's see if in the liker scale table we keep the same scales
        $I->see($scale2, $page::$scale_table);
        $I->see($scale4, $page::$scale_table);
    }

    /**
     * @scenario User answering likert scale question inside a survey
     *
     * @group DOCEBO-1695
     * @group DOCEBO-1697
     *
     * @param AcceptanceTester $I
     */

    public function userAnsweringLikertScaleQuestionInsideSurvey(AcceptanceTester $I){

        // Well in order to test the New Survey question types we need to create a Course with Survey LO
        $user = $this->usersHelper->createUser();
        $course = $this->courseHelper->createElearningCourseWithCode('C1');
        $this->courseHelper->assignUserAs(false, $user, $course);
        $survey = $this->loHelper->createSurvey($user->idst, $course->idCourse, 'S1');
        $question = $this->loHelper->createSurveyQuestion($survey->getPrimaryKey(), LearningPollquest::POLL_TYPE_LIKERT_SCALE, "Q1");
        $questionAnswer1 = $this->loHelper->createSurveyQuestionAnswers($question->getPrimaryKey(), "Rate me 1", 1);
        $questionAnswer2 = $this->loHelper->createSurveyQuestionAnswers($question->getPrimaryKey(), "Rate me 2", 2);
        $likerScale1 = $this->loHelper->createSurveyLikerScale($survey->getPrimaryKey(), 'Bad', 1);
        $likerScale2 = $this->loHelper->createSurveyLikerScale($survey->getPrimaryKey(), 'Average', 2);
        $likerScale3 = $this->loHelper->createSurveyLikerScale($survey->getPrimaryKey(), 'Good', 3);
        $page = new \Page\Course\LO\Poll\LikertScale($I);
        // After all dummy data is created let's login
        $I->login($user->getUserNameFormatted(), $this->usersHelper->getDefaultPass());
        // After login let's visit the poll page
        $I->amOnPage($page::route('poll/player/play', array('id_poll' => $survey->getPrimaryKey(), 'course_id' => $course->getPrimaryKey())));
        // Check if the question contains two rows
        $page->checkRowQuestionScales($question->getPrimaryKey(), 2);
        // And it should contains 3 scales
        $page->checkQuestionContainScales($question->getPrimaryKey(), array('Bad', 'Average', 'Good'));
        // Let's select the question options
        $page->checkOnScaleQuestion($question->getPrimaryKey(), $questionAnswer1->getPrimaryKey(), $likerScale1->getPrimaryKey());
        $page->checkOnScaleQuestion($question->getPrimaryKey(), $questionAnswer2->getPrimaryKey(), $likerScale2->getPrimaryKey());
        // And then let's save the page
        $I->click($page::$play_scale_submit_form);
        // I see the "Survey completed" text
        $I->seeElement($page::$play_scale_completed);
    }

    /**
     * @scenario Admin seeing LO reports on survey with likert question
     *
     * @group DOCEBO-1695
     * @group DOCEBO-1699
     *
     * @param AcceptanceTester $I
     */

    public function adminSeeingLOReportsOnSurveyWithLikertQuestion(AcceptanceTester $I){
        $this->pluginHelper->activatePluginByCodeName('PowerUser');
        // Create 3 Dummy users - Admin, Johny, Lisa
        $admin = $this->usersHelper->createUser(Yii::app()->user->getAdminLevelLabel());
        $jony = $this->usersHelper->createUser(false, false, 'Johny', 'Acceptance');
        $lisa = $this->usersHelper->createUser(false, false, 'Lisa', 'Acceptance');
        // Assign the two users to that PU
        $this->puHelper->assignUsersToPU($admin, array($jony->idst, $lisa->idst));
        // Create Course and assign the users to that course
        $course = $this->courseHelper->createElearningCourseWithCode('C1');
        $this->courseHelper->assignUserAs(LearningCourseuser::$USER_SUBSCR_LEVEL_ADMIN, $admin, $course);
        $this->courseHelper->assignUserAs(false, $jony, $course);
        $this->courseHelper->assignUserAs(false, $lisa, $course);
        // Then create Survey in the course with questions and Likert scales
        $survey = $this->loHelper->createSurvey($admin->idst, $course->idCourse, 'S1');
        $question = $this->loHelper->createSurveyQuestion($survey->getPrimaryKey(), LearningPollquest::POLL_TYPE_LIKERT_SCALE, "Q1");
        $questionAnswer1 = $this->loHelper->createSurveyQuestionAnswers($question->getPrimaryKey(), "Rate me 1", 1);
        $questionAnswer2 = $this->loHelper->createSurveyQuestionAnswers($question->getPrimaryKey(), "Rate me 2", 2);
        $likerScale1 = $this->loHelper->createSurveyLikerScale($survey->getPrimaryKey(), 'Bad', 1);
        $likerScale2 = $this->loHelper->createSurveyLikerScale($survey->getPrimaryKey(), 'Average', 2);
        $likerScale3 = $this->loHelper->createSurveyLikerScale($survey->getPrimaryKey(), 'Good', 3);

        $page = new \Page\Player\Report($I);
        // We assume that we already have Course And Test so let's get the Test's OrgChart ID
        $org = Yii::app()->db->createCommand()
            ->select('idOrg')
            ->from(LearningOrganization::model()->tableName())
            ->where(array('and', 'idResource = :lo' , 'objectType=:objectType'))
            ->queryScalar(array(':lo' => $survey->getPrimaryKey(), ':objectType' => LearningOrganization::OBJECT_TYPE_POLL));

        // Now let's put some dummy data
        $johnyTrack = array(
            array(
                'id_quest' => $question->getPrimaryKey(),
                'id_answer' => $questionAnswer1->getPrimaryKey(),
                'more_info' => $likerScale1->getPrimaryKey(),
            ),
            array(
                'id_quest' => $question->getPrimaryKey(),
                'id_answer' => $questionAnswer2->getPrimaryKey(),
                'more_info' => $likerScale3->getPrimaryKey(),
            ),
        );
        $lisaTrack = array(
            array(
                'id_quest' => $question->getPrimaryKey(),
                'id_answer' => $questionAnswer1->getPrimaryKey(),
                'more_info' => $likerScale1->getPrimaryKey(),
            ),
            array(
                'id_quest' => $question->getPrimaryKey(),
                'id_answer' => $questionAnswer2->getPrimaryKey(),
                'more_info' => $likerScale2->getPrimaryKey(),
            ),
        );
        $this->loHelper->createFakeSurveyResults($org, $jony, $survey, $johnyTrack);
        $this->loHelper->createFakeSurveyResults($org, $lisa, $survey, $lisaTrack);

        $I->login($admin->getUserNameFormatted(), $this->usersHelper->getDefaultPass());
        // Go on the course report page
        $I->amOnPage($page::route('player/report',array('course_id' => $course->idCourse)));
        // Let's click on the training materials tab
        $I->click($page::$training_materials_tab);
        // Click on the test LO
        $element = str_replace('{attr}', 'organizationId=' .  $org, $page::$tm_lo_grid_element_tmpl_survey);
        $I->click($element);
        // Let's wait till the survey modal appear
        $I->waitForElementVisible($page::$user_modal_test_results);
        // I can see that the Liker scale table has two rows
        $tmpQuestion = str_replace('{question}', $questionAnswer1->getPrimaryKey(), $page::$player_report_survey_likert_scale_table_row);
        $tmpQuestion2 = str_replace('{question}', $questionAnswer2->getPrimaryKey(), $page::$player_report_survey_likert_scale_table_row);
        $I->seeElementInDOM($tmpQuestion);
        $I->seeElementInDOM($tmpQuestion2);
        // row 1 contains "Rate me 1" and the values "2" for the "Bad" column, "0" for the "Average" column, "0" for the "Good column"
        $tmpCodes = array('{question}', '{scale}');
        $rateMe1_Bad = str_replace($tmpCodes, array($questionAnswer1->getPrimaryKey(), $likerScale1->getPrimaryKey()), $page::$player_report_survey_likert_scale_table_row_scale);
        $rateMe1_Avg = str_replace($tmpCodes, array($questionAnswer1->getPrimaryKey(), $likerScale2->getPrimaryKey()), $page::$player_report_survey_likert_scale_table_row_scale);
        $rateMe1_Good = str_replace($tmpCodes, array($questionAnswer1->getPrimaryKey(), $likerScale3->getPrimaryKey()), $page::$player_report_survey_likert_scale_table_row_scale);
        $I->see('2', $rateMe1_Bad);
        $I->see('0', $rateMe1_Avg);
        $I->see('0', $rateMe1_Good);
        // row 2 contains "Rate me 2" and the values "0" for the "Bad" column, "1" for the "Average" column, "1" for the "Good column"
        $rateMe2_Bad = str_replace($tmpCodes, array($questionAnswer2->getPrimaryKey(), $likerScale1->getPrimaryKey()), $page::$player_report_survey_likert_scale_table_row_scale);
        $rateMe2_Avg = str_replace($tmpCodes, array($questionAnswer2->getPrimaryKey(), $likerScale2->getPrimaryKey()), $page::$player_report_survey_likert_scale_table_row_scale);
        $rateMe2_Good = str_replace($tmpCodes, array($questionAnswer2->getPrimaryKey(), $likerScale3->getPrimaryKey()), $page::$player_report_survey_likert_scale_table_row_scale);
        $I->see('0', $rateMe2_Bad);
        $I->see('1', $rateMe2_Avg);
        $I->see('1', $rateMe2_Good);
    }
}