<?php

// Autoload the DoceboCest superclass
require_once "DoceboCest.php";

/**
 * Class ExampleDoceboCest
 *
 * Put here all test cases belonging to the "example_docebo" category.
 */
class App7020ExpertAndChannelsCest extends DoceboCest {

	protected $userHelper;
	protected $orgChartHelper;
	protected $app7020Helper;
	protected $channelsManagementHelper;
	protected $expertsAndChannelsHelper;
	protected $defaultUserPass;

	public function _inject(\Helper\User $user, \Helper\Orgchart $orgChart, \Helper\App7020\App7020General $app7020General, \Helper\App7020\ExpertAndChannelsHelper $expertsAndChannelsHelper, \Helper\App7020\ChannelsManagementHelper $channelsManagementHelper) {
		$this->userHelper = $user;
		$this->orgChartHelper = $orgChart;
		$this->app7020Helper = $app7020General;
		$this->defaultUserPass = $this->userHelper->getDefaultPass();
		$this->channelsManagementHelper = $channelsManagementHelper;
		$this->expertsAndChannelsHelper = $expertsAndChannelsHelper;
	}

	/**
	 * The following code will execute before codeception starts running each test case.
	 * Put something here only if you need to add "before" actions, otherwise delete the method from this subclass
	 */
	public function _before(AcceptanceTester $I) {
		// Do not delete this to allow superclass to do its "before" actions
		parent::_before($I);

		// Add your actions here...
		$this->login($I);
	}

	/**
	 * The following code will execute after codeception runs each test case.
	 * Put something here only if you need to add "after" actions, otherwise delete the method from this subclass
	 */
	public function _after(AcceptanceTester $I) {
		// Do not delete this to allow superclass to do its "after" actions
		//parent::_after($I);
		// Add your actions here...
		// Logout after all
		//$I->logout();
	}

	public function scenarioAssetEditMeta(AcceptanceTester $I) {
		$firstAsset = App7020Assets::model()->find();
		$I->amOnPage(\Page\Common::route(\Page\App7020\App7020Routes::ROUTE_ASSET_EDIT . $firstAsset->id, array(), false, \Helper\App7020\App7020General::APP_NAME));
		$I->waitForElementVisible(".green.save", 30);
		$I->fillField("#App7020Assets_title", "Automatic Test Title");
		$I->fillField("#App7020Assets_description", "Automatic Test Description Automatic Test Description Automatic Test Description Automatic Test Description");
		$I->fillField("#tags", "tag1 , tag2 ,  tag3");
		$I->click(".green.save");
	}

}
