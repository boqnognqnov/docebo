<?php
// Autoload the DoceboCest superclass
require_once "DoceboCest.php";

/**
 * Class RegressionCest
 * @group regression
 */
class RegressionCest extends DoceboCest {

	/**
	 * @var Helper\User
	 */
	protected $usersHelper;

	/**
	 * Inject dependencies
	 * @param \Helper\User $usersHelper
	 */
	public function _inject(\Helper\User $usersHelper) {
		$this->usersHelper = $usersHelper;
	}

	/**=========================================================================
	 * There is an interesting problem here. The number of inputs can vary for
	 * each customer. There must be a way for .. "foreach" all fields or at least
	 * to fill them simultaneously. This is kind of achievable by using fill
	 * field, but for now we can't be sure which fields we are filling out.
	 *
	 * For now, the best found solution is to grab all of the html in the form
	 * and parse it for fields
	 * =========================================================================
	 *
	 * @param AcceptanceTester $I
	 */
	public function selfRegister(AcceptanceTester $I) {
		$I->amOnPage(\Page\Home::$URL);
		$I->openModal(\Page\Home::$modalSelfRegisterOpenLink);

		/* WORK IN PROGRESS! */
		//TODO: collect/parse form fields, populate, submit
	}

	/**
	 * =========================================================================
	 * For now it should be enough to visit the pages:
	 *  - My Course/Dashboard
	 *  - Catalogs
	 *  - Learning Plans
	 * and check for certain elements.
	 * =========================================================================
	 *
	 * **WorkInProgress**
	 *
	 * @before login
	 * @param AcceptanceTester $I
	 * @group testingNow
	 */
	public function userMainPages(AcceptanceTester $I) {
		$I->goToPageUsingMenu(\Page\Settings\LocalizationTool::route());
		$I->goToPageUsingMenu(\Page\Profile\MyCourses::route());
		$I->goToPageUsingMenu(\Page\Blog\Settings::route());
	}

}
