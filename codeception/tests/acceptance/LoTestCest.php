<?php
// Autoload the DoceboCest superclass
require_once "DoceboCest.php";

/**
 * Category of test cases for the LO test
 */
class LoTestCest extends DoceboCest {

	/**
	 * @var Helper\Course
	 */
	protected $courseHelper;

	/**
	 * @var Helper\Lo
	 */
	protected $loHelper;

	/**
	 * @var Helper\User
	 */
	protected $usersHelper;

	// Store Settings
	private $tmpData;

	/**
	 * Inject dependencies
	 * @param \Helper\Course $courseHelper
	 */
	protected function _inject(\Helper\Course $courseHelper, \Helper\Lo $loHelper, \Helper\User $usersHelper) {
		$this->courseHelper = $courseHelper;
		$this->loHelper = $loHelper;
		$this->usersHelper = $usersHelper;
	}

	/**
	 * @scenario Instructor creating a "fill in the blank" test question
	 *
	 * @group DOCEBO-1067
	 * @before login
	 *
	 * @param AcceptanceTester $I
	 */

	public function instructorCreatingFITBTestQuestion(AcceptanceTester $I){
		// Administrator or course instructor seeing the new question type
		$I->comment('Administrator or course instructor seeing the new question type');
		$this->_administratorOrCourseInstructorSeeingTheNewQuestionType($I);

		// Administrator or course instructor seeing the initial question creation form
		$I->comment('Administrator or course instructor seeing the initial question creation form');
		$this->_administratorOrCourseInstructorSeeingTheInitialQuestionCreationForm($I);

		// Administrator or course instructor adding a new first answer
		$I->comment('Administrator or course instructor adding a new first answer');
		$this->_administratorOrCourseInstructorAddingANewFirstAnswer($I);

		// Administrator or course instructor adding a new answer
		$I->comment('Administrator or course instructor adding a new answer');
		$this->_administratorOrCourseInstructorAddingANewAnswer($I);

		// Administrator or course instructor editing an existing answer
		$I->comment('Administrator or course instructor editing an existing answer');
		$this->_administratorOrCourseInstructorEditingAnExistingAnswer($I);

		// Administrator or course instructor deleting an existing answer
		$I->comment('Administrator or course instructor deleting an existing answer');
		$this->_administratorOrCourseInstructorDeletingAnExistingAnswer($I);

		// Administrator or course instructor creating a new FITB question with no answers
		$I->comment('Administrator or course instructor creating a new FITB question with no answers');
		$this->_administratorOrCourseInstructorCreatingANewFITBQuestionWithNoAnswers($I);

		// Administrator or course instructor creating a new FITB question with an incoherent set of answers
		$I->comment('Administrator or course instructor creating a new FITB question with an incoherent set of answers');
		$this->_administratorOrCourseInstructorCreatingANewFITBQuestionWithAnIncoherentSetOfAnswers($I);

		// Administrator or course instructor creating a new FITB question with a coherent set of answers
		$I->comment('Administrator or course instructor creating a new FITB question with a coherent set of answers');
		$this->_administratorOrCourseInstructorCreatingANewFITBQuestionWithACoherentSetOfAnswers($I);
	}


	private function _administratorOrCourseInstructorSeeingTheNewQuestionType(AcceptanceTester $I) {

		// Well in order to test the New FITB question types we need to create a Course with test LO
		$course = $this->courseHelper->createElearningCourseWithCode('C1');
		$test = $this->loHelper->createTest($course->idCourse, 'Test title');

		// We assume that we already have Course And Test so let's get the Test's OrgChart ID
		$org = Yii::app()->db->createCommand()
			->select('idOrg')
			->from(LearningOrganization::model()->tableName())
			->where(array('and', 'idResource = :idTest' , 'objectType=:objectType'))
			->queryScalar(array(':idTest' => $test->getPrimaryKey(), ':objectType' => LearningOrganization::OBJECT_TYPE_TEST));

		$this->tmpData = array(
			'idTest' => $test->getPrimaryKey(),
			'course_id' => $course->idCourse,
			'idOrg' => $org,
		);

		// After the Test is created let's visit the test's page
		$I->amOnPage(\Page\Course\LO\Test::createEditUrl(array('id_object' => $org, 'course_id' => $course->idCourse)));

		// Ok We are on the Test Edin page, so let's click in the "ADD QUESTION" button
		$I->click(\Page\Course\LO\Test::$testTypes);
		$type = str_replace('{type}' , \Page\Course\LO\Test::TYPE_FITB, \Page\Course\LO\Test::$templateType);
		// Does the "FITB" question type existing ?
		$I->seeElement($type);

	}

	public function _administratorOrCourseInstructorSeeingTheInitialQuestionCreationForm(AcceptanceTester $I) {
		// Ok the FITB Question type exists let'c click on that boy
		$type = str_replace('{type}' , \Page\Course\LO\Test::TYPE_FITB, \Page\Course\LO\Test::$templateType);
		$I->click($type);

		// Well the click should redirect us on recertain page "question/create"
		// Let's see if that is true
		$url = \Page\Course\LO\Test\Question::createCreateUrl(array('id_test' => $this->tmpData['idTest'], 'course_id' => $this->tmpData['course_id'], 'question_type' => \Page\Course\LO\Test::TYPE_FITB));
		$I->seeInCurrentUrl($url);

		// Well we are on the "question/create" page let's see if here we have empty list of answers
		$I->seeElement(\Page\Course\LO\Test\Question::$no_answers);

		// Let's see if the consider_correct checkbox is checked by default
		$I->dontSeeStyledCheckboxIsChecked(\Page\Course\LO\Test\Question::$consider_correct_answer);
	}

	public function _administratorOrCourseInstructorAddingANewFirstAnswer(AcceptanceTester $I) {
		// Click on the add answer button
		$I->click(\Page\Course\LO\Test\Question::$add_answer);

		// When we clicked the button should disappear let's see is this is correct
		$I->dontSeeElement(\Page\Course\LO\Test\Question::$add_answer);

		/// When we clicked the button we should now see the sub form for adding new answers
		$I->seeElement(\Page\Course\LO\Test\Question::$add_answer_sub_form);

		// On the page the checkbox for Answers are case sensiteve should be UNCHECKED by default
		$I->dontSeeStyledCheckboxIsChecked(\Page\Course\LO\Test\Question::$fitb_answers_case_sensitive);

		// Now both score_correct and score_incorect should have 0 as default value
		$I->seeElement(\Page\Course\LO\Test\Question::$new_answer_score_correct, array('value' => '0'));
		$I->seeElement(\Page\Course\LO\Test\Question::$new_answer_score_incorrect, array('value' => '0'));
	}

	public function _administratorOrCourseInstructorAddingANewAnswer(AcceptanceTester $I) {
		// We need to be sure that we can see the "Add answer" sub form
		$I->seeElement(\Page\Course\LO\Test\Question::$add_answer_sub_form);

		$I->fillField(\Page\Course\LO\Test\Question::$fitb_answers_input, "Choice 1");
		$I->pressKey(\Page\Course\LO\Test\Question::$fitb_answers_input, WebDriverKeys::ENTER);
		$I->fillField(\Page\Course\LO\Test\Question::$fitb_answers_input, "Choice 2");
		$I->pressKey(\Page\Course\LO\Test\Question::$fitb_answers_input, WebDriverKeys::ENTER);

		// And let's select the "Case Sensitive checkbox"
		$I->checkStyledOption(\Page\Course\LO\Test\Question::$fitb_answers_case_sensitive);

		// We are about to add new answer
		$I->click(\Page\Course\LO\Test\Question::$new_answer_add);

		// After we clicked on the "Add" button
		// we want to see if the answer goes at the answers table
		$I->seeElement(\Page\Course\LO\Test\Question::$answers_table_row);
		// Also the add answer sub form need to be cleared let's see if it is
		$I->seeElement(\Page\Course\LO\Test\Question::$fitb_answers_input, array('value' => ''));
		$I->dontSeeStyledCheckboxIsChecked(\Page\Course\LO\Test\Question::$fitb_answers_case_sensitive);
		$I->seeElement(\Page\Course\LO\Test\Question::$new_answer_score_correct, array('value' => '0'));
		$I->seeElement(\Page\Course\LO\Test\Question::$new_answer_score_incorrect, array('value' => '0'));
		$I->seeElement(\Page\Course\LO\Test\Question::$new_answer_comment, array('value' => ''));
	}

	public function _administratorOrCourseInstructorEditingAnExistingAnswer(AcceptanceTester $I) {
		// We want to check if "Answer 1" is in the answers list
		$I->seeElement(\Page\Course\LO\Test\Question::$answers_table_row);

		// Lets see if we have 2 Correct Answers for the first answer
		$I->see('2 '. Yii::t('test', 'choices'), \Page\Course\LO\Test\Question::$fitb_answers_count_column);

		// We need to check the answer sub form so let's click on the EDIT button
		$I->click(\Page\Course\LO\Test\Question::$answers_list_edit_first_answer);

		//Does the Correct answers contains "Choice 1" && "Choice 2"
		$I->seeFBKCompleteHasSelected(\Page\Course\LO\Test\Question::$fitb_answers_select, array("Choice 1", "Choice 2"));

		// The case sensitive checkbox should be checked
		$I->seeStyledCheckboxIsChecked(\Page\Course\LO\Test\Question::$fitb_answers_case_sensitive);

		$I->click(\Page\Course\LO\Test\Question::$mod_answer_undo);
	}

	public function _administratorOrCourseInstructorDeletingAnExistingAnswer(AcceptanceTester $I) {
		// We want to check if "Answer 1" is in the answers list
		$I->seeElement(\Page\Course\LO\Test\Question::$answers_table_row);

		// We want to delete the Answer from the Answer list
		$I->openModal(\Page\Course\LO\Test\Question::$answers_list_delete_first_answer, \Page\Course\LO\Test\Question::$answer_modal);

		// We click on the 'Confirm' button
		$I->click(\Page\Course\LO\test\Question::$answer_delete_confirm);

		// And now we should have the answer's list EMPTY
		$I->seeElement(\Page\Course\LO\Test\Question::$no_answers);
	}

	public function _administratorOrCourseInstructorCreatingANewFITBQuestionWithNoAnswers(AcceptanceTester $I) {
		// Let's wait for 2 second VERY IMPORTANT !!!
		$I->wait(2);

		// Let's enter the QUESTION TITLE
		$I->fillTinyMceEditorById(\Page\Course\LO\Test\Question::$question_title_textarea_id, 'Question Title');

		// Let's be sure that we don't have any answer entered so far
		$I->seeElement(\Page\Course\LO\Test\Question::$no_answers);

		// And we are about to submit the form
		$I->click(\Page\Course\LO\Test\Question::$question_save_changes);

		// Let's wait till the modal appear
		$I->waitForElement(\Page\Course\LO\Test\Question::$answer_modal);

		// When the modal is opeden we need to see this value
		$I->see(Yii::t('test', 'You should provide at least one answer for this type of question'));

		// And then closing the modal
		$I->click(\Page\Course\LO\Test\Question::$answer_modal_cancel);
	}

	public function _administratorOrCourseInstructorCreatingANewFITBQuestionWithAnIncoherentSetOfAnswers(AcceptanceTester $I) {
		// Let's wait for 2 second VERY IMPORTANT !!!
		$I->wait(2);

		// Let's enter the QUESTION TITLE
		$I->fillTinyMceEditorById(\Page\Course\LO\Test\Question::$question_title_textarea_id, 'Question Title [answ 1] and [answ 2]');

		// Click on the add answer button
		$I->click(\Page\Course\LO\Test\Question::$add_answer);

		// Let's add 2 Correct answers
		$I->fillField(\Page\Course\LO\Test\Question::$fitb_answers_input, "Choice 1");
		$I->pressKey(\Page\Course\LO\Test\Question::$fitb_answers_input, WebDriverKeys::ENTER);
		$I->fillField(\Page\Course\LO\Test\Question::$fitb_answers_input, "Choice 2");
		$I->pressKey(\Page\Course\LO\Test\Question::$fitb_answers_input, WebDriverKeys::ENTER);

		// We are about to add new answer
		$I->click(\Page\Course\LO\Test\Question::$new_answer_add);

		// And we are about to submit the form
		$I->click(\Page\Course\LO\Test\Question::$question_save_changes);

		// Let's wait till the modal appear
		$I->waitForElement(\Page\Course\LO\Test\Question::$answer_modal);

		// When the modal is opeden we need to see this value
		$I->see(Yii::t('test', 'You should provide answers for all the shortcodes used in your question title'));

		// And then closing the modal
		$I->click(\Page\Course\LO\Test\Question::$answer_modal_cancel);
	}

	public function _administratorOrCourseInstructorCreatingANewFITBQuestionWithACoherentSetOfAnswers(AcceptanceTester $I) {
		// Let's wait for 2 second VERY IMPORTANT !!!
		$I->wait(2);

		// Let's add more 2 Correct answers
		$I->fillField(\Page\Course\LO\Test\Question::$fitb_answers_input, "Choice 3");
		$I->pressKey(\Page\Course\LO\Test\Question::$fitb_answers_input, WebDriverKeys::ENTER);
		$I->fillField(\Page\Course\LO\Test\Question::$fitb_answers_input, "Choice 4");
		$I->pressKey(\Page\Course\LO\Test\Question::$fitb_answers_input, WebDriverKeys::ENTER);

		// We are about to add new answer
		$I->click(\Page\Course\LO\Test\Question::$new_answer_add);

		// And we are about to submit the form
		$I->click(\Page\Course\LO\Test\Question::$question_save_changes);

		// After the Question is created let's visit the test's page
		$I->seeInCurrentUrl(\Page\Course\LO\Test::createEditUrl(array('course_id' => $this->tmpData['course_id'], 'id_object' => $this->tmpData['idOrg'])));

		// Let's wait to see if the question will appear in the questions list
		$I->waitForElement(\Page\Course\LO\Test::$test_questions_table_row);

		// Well the Docebo success message should be in place
		$I->seeDoceboSuccessFlash();
	}

	/**
	 * @scenario Instructor editing a FITB test question
	 *
	 * @group DOCEBO-1009
	 * @before login
	 *
	 * @param AcceptanceTester $I
	 */
	public function instructorEditingFITBTestQuestion(AcceptanceTester $I){
		//Administrator or course instructor editing an existing FITB question
		$I->comment('Administrator or course instructor editing an existing FITB question');
		$this->_administratorOrCourseInstructorEditingAnExistingFITBQuestion($I);

		//Administrator or course instructor saving changes for an existing FITB question
		$I->comment('Administrator or course instructor saving changes for an existing FITB question');
		$this->_administratorOrCourseInstructorSavingChangesForAnExistingFITBQuestion($I);
	}


	public function _administratorOrCourseInstructorEditingAnExistingFITBQuestion(AcceptanceTester $I) {
		// Well in order to test the New FITB question types we need to create a Course with test LO
		$course = $this->courseHelper->createElearningCourseWithCode('C1');
		$test = $this->loHelper->createTest($course->idCourse, 'Test 1');
		$question = $this->loHelper->createTestQuestion($test->getPrimaryKey(), LearningTestquest::QUESTION_TYPE_FITB, "The [answ 1] is on the [answ 2]", array());
		$questionAnswer = $this->loHelper->createTestQuestionAnswers($question->getPrimaryKey(), "Choice 1", null, 1, 5, 5, array('case_sensitive' => false));
		$questionAnswer2 = $this->loHelper->createTestQuestionAnswers($question->getPrimaryKey(), "Choice 2", null, 2, 5, 5, array('case_sensitive' => false));

		// We assume that we already have Course And Test so let's get the Test's OrgChart ID
		$org = Yii::app()->db->createCommand()
			->select('idOrg')
			->from(LearningOrganization::model()->tableName())
			->where(array('and', 'idResource = :idTest' , 'objectType=:objectType'))
			->queryScalar(array(':idTest' => $test->getPrimaryKey(), ':objectType' => LearningOrganization::OBJECT_TYPE_TEST));


		$this->tmpData = array(
			'idTest' => $test->getPrimaryKey(),
			'course_id' => $course->idCourse,
			'idOrg' => $org,
			'id_question' => $question->getPrimaryKey()
		);

		// After the Test is created let's visit the test's page
		$I->amOnPage(\Page\Course\LO\Test::createEditUrl(array('id_object' => $org, 'course_id' => $course->idCourse)));

		// Let's wait to see if the question will appear in the questions list
		$I->waitForElement(\Page\Course\LO\Test::$test_questions_table_row, 10);

		// Let's click Edit on the first row
		$I->click(\Page\Course\LO\Test::$test_questions_table_edit_first_row);

		// Let's see if we are on the correct page
		$url = \Page\Course\LO\Test\Question::createUpdateUrl(array('id_test' => $this->tmpData['idTest'], 'course_id' => $this->tmpData['course_id'], 'id_question' => $question->getPrimaryKey()));
		$I->seeInCurrentUrl($url);

		// In the answers table should have 2 rows
		$I->seeNumberOfElements(\Page\Course\LO\Test\Question::$answers_table_row, 2);

		// The "Consider this question.." is UNchecked
		$I->dontSeeStyledCheckboxIsChecked(\Page\Course\LO\Test\Question::$consider_correct_answer);
	}

	public function _administratorOrCourseInstructorSavingChangesForAnExistingFITBQuestion(AcceptanceTester $I) {

		// Let's see if we are on the correct page
		$url = \Page\Course\LO\Test\Question::createUpdateUrl(array('id_test' => $this->tmpData['idTest'], 'course_id' => $this->tmpData['course_id'], 'id_question' => $this->tmpData['id_question']));
		$I->seeInCurrentUrl($url);

		// We are changing the title content
		$I->fillTinyMceEditorById(\Page\Course\LO\Test\Question::$question_title_textarea_id, "The [answ 1] is under the [answ 2]");

		// We are clicking on the save changes
		$I->click(\Page\Course\LO\Test\Question::$question_save_changes);

		// We need to be on the Test Edit page
		$I->seeInCurrentUrl(\Page\Course\LO\Test::createEditUrl(array('course_id' => $this->tmpData['course_id'], 'id_object' => $this->tmpData['idOrg'])));

		// Wait for the element to appear
		$I->waitForElement(\Page\Course\LO\Test::$test_questions_table_row);

		// I see that the Question has Changed it's title
		$I->see("The [answ 1] is under the [answ 2]", \Page\Course\LO\Test::$test_questions_table_row);

		// And I see the success message
		$I->seeDoceboSuccessFlash();
	}

	/**
	 * @scenario User answering a "fill in the blank" test question
	 *
	 * @group DOCEBO-1062
	 *
	 * @param AcceptanceTester $I
	 */
	public function userTakingFITBQuestion(AcceptanceTester $I){
		// Well in order to test the New FITB question types we need to create a Course with test LO
		$course = $this->courseHelper->createElearningCourseWithCode('C1');
		$test = $this->loHelper->createTest($course->idCourse, 'Test 1');
		$question = $this->loHelper->createTestQuestion($test->getPrimaryKey(), LearningTestquest::QUESTION_TYPE_FITB, "[answ 1] i'm [answ 2] and i'm from [answ 3]", array());
		$this->loHelper->createTestQuestionAnswers($question->getPrimaryKey(), "Choice 1", null, 1, 5, 5, array('case_sensitive' => false));
		$this->loHelper->createTestQuestionAnswers($question->getPrimaryKey(), "Choice 2", null, 2, 5, 5, array('case_sensitive' => false));
		$this->loHelper->createTestQuestionAnswers($question->getPrimaryKey(), "Choice 3", null, 3, 5, 5, array('case_sensitive' => false));

		// We assume that we already have Course And Test so let's get the Test's OrgChart ID
		$org = Yii::app()->db->createCommand()
			->select('idOrg')
			->from(LearningOrganization::model()->tableName())
			->where(array('and', 'idResource = :idTest' , 'objectType=:objectType'))
			->queryScalar(array(':idTest' => $test->getPrimaryKey(), ':objectType' => LearningOrganization::OBJECT_TYPE_TEST));
		// Create a tmp user
		$user = $this->usersHelper->createUser();
		// Let's assign that user to the test we created
		$this->courseHelper->assignUserAs(LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT, $user, $course);

		// Let's try to login with this created user
		$I->login(Yii::app()->user->getRelativeUsername($user->userid), $this->usersHelper->getDefaultPass());

		// After we logged in let's go to that course
		$I->amOnPage(Page\Player::createTrainingUrl(array('course_id' =>$course->idCourse)));

		// Wait till the course materials appears
		$I->waitForElement(\Page\Player::$launchpad, 10);

		// Click on the play course
		$I->click(Page\Player::$play);

		// Wait till the player arena appear
		$I->waitForElement(\Page\Player::$player_arena, 10);

		// Click on the begin TEST button
		$I->click(\Page\Player::$begin_test);

		// Let's wait till the test play screen appear
		$I->waitForElement(\Page\Player::$test_play_container, 10);

		// I see all three inputs
		$I->seeElement('input#question_'.$question->getPrimaryKey(). '_answ_1');
		$I->seeElement('input#question_'.$question->getPrimaryKey(). '_answ_2');
		$I->seeElement('input#question_'.$question->getPrimaryKey(). '_answ_3');
		//Click on save results
		$I->click(\Page\Player::$test_results);

		// And check if the see results screen appear
		$I->waitForElement(\Page\Player::$review_score);

		// If everything is ok we need to have those data into DB
		$idTrack = $I->grabFromDatabase(LearningTesttrack::model()->tableName(), 'idTrack', array(
			'idUser' => $user->idst,
			'idReference' => $org,
			'idTest' => $test->idTest
		));
		foreach($question->answers as $answer){
			$I->grabFromDatabase(LearningTesttrackAnswer::model()->tableName(), 'more_info', array(
				'idTrack' => $idTrack,
				'idQuest' => $question->idQuest,
				'idAnswer' => $answer->idAnswer
			));
		}
	}


	/**
	 * @scenario DOCEBO-1061
	 *
	 * @group DOCEBO-1061
	 * @before login
	 *
	 * @param AcceptanceTester $I
	 */
	public function adminSeeingReportingAboutFITBTestQuestion(AcceptanceTester $I){

		// Well in order to test the New FITB question types we need to create a Course with test LO
		$course = $this->courseHelper->createElearningCourseWithCode('C1');
		$test = $this->loHelper->createTest($course->idCourse, 'Test 1');
		$question = $this->loHelper->createTestQuestion($test->getPrimaryKey(), LearningTestquest::QUESTION_TYPE_FITB, "[answ 1] i'm [answ 2] and i'm from [answ 3]", array());
		$this->loHelper->createTestQuestionAnswers($question->getPrimaryKey(), "Choice 1", null, 1, 5, 5, array('case_sensitive' => false));
		$this->loHelper->createTestQuestionAnswers($question->getPrimaryKey(), "Choice 2", null, 2, 5, 5, array('case_sensitive' => false));
		$this->loHelper->createTestQuestionAnswers($question->getPrimaryKey(), "Choice 3", null, 3, 5, 5, array('case_sensitive' => false));
		// We assume that we already have Course And Test so let's get the Test's OrgChart ID
		$org = Yii::app()->db->createCommand()
			->select('idOrg')
			->from(LearningOrganization::model()->tableName())
			->where(array('and', 'idResource = :idTest' , 'objectType=:objectType'))
			->queryScalar(array(':idTest' => $test->getPrimaryKey(), ':objectType' => LearningOrganization::OBJECT_TYPE_TEST));
		// Create a tmp user
		$user = $this->usersHelper->createUser();
		// Let's assign that user to the test we created
		$this->courseHelper->assignUserAs(LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT, $user, $course);
		//We assigned the user let's make some fake test results
		$this->loHelper->createFakeTestResults($course, $user, $test, $org);

		$this->tmpData = array(
			'course' => $course,
			'idOrg' => $org,
			'question' => $question,
			'user' => $user,
			'test' => $test
		);

		$I->comment('Administrator seeing learner answers in course report');
		// Administrator seeing learner answers in course report
		$this->_administratorSeeingLearnerAnswersInCourseReport($I);

		// Administrator seeing answers breakdown for FITB question in course report
		$I->comment('Administrator seeing answers breakdown for FITB question in course report');
		$this->_administratorSeeingAnswersBreakdownForFITBQuestionInCourseReport($I);
	}

	public function _administratorSeeingLearnerAnswersInCourseReport(AcceptanceTester $I){

		// And let's visit the course report page
		$I->amOnPage(\Page\Player::createReportUrl(array('course_id' => $this->tmpData['course']->idCourse)));

		// Let's click on the training materials tab
		$I->click(\Page\Player\Report::$training_materials_tab);

		// Click on the test LO
		$element = str_replace('{attr}', 'organizationId=' .  $this->tmpData['org'], \Page\Player\Report::$tm_lo_grid_element_tmpl);
		$I->click($element);

		// Let's wait till the users test attempts table appear
		$I->waitForElement(\Page\Player\Report::$player_reports_test_users_table);

		// We want to see that the user's result are here listed in the table
		// The user were made one test attempts so the table should contain at least one row
		$row = str_replace('{n}', '1', \Page\Player\Report::$player_reports_test_users_table_row_tmpl);
		$I->seeElement($row);

		// OK the table have at least one record let's check if is the correct one
		$I->see( $this->tmpData['question']->title_quest,\Page\Player\Report::$player_reports_test_users_table);

		// Also we need to check if the listed record is FOR our user
		$I->see(Yii::app()->user->getRelativeUsername( $this->tmpData['user']->userid), \Page\Player\Report::$player_reports_test_users_table);
	}

	public function _administratorSeeingAnswersBreakdownForFITBQuestionInCourseReport(AcceptanceTester $I){
		 //We want to refresh the page in order to check th
		$I->amOnPage(Page\Player::createReportUrl(array('course_id' => $this->tmpData['course']->idCourse)));

		// Let's click on OUR user's report
		$row = str_replace('{attr}', 'user_id=' . $this->tmpData['user']->idst, \Page\Player\Report::$user_statistic_row_tmpl);
		$I->click($row);

		// For better review let's scroll to the bottom of the page
		$I->scrollToBottomOfPage();

		// Let's revire user's test attempt results
		$row = str_replace('{org}', $this->tmpData['idOrg'], \Page\Player\Report::$user_player_objects_row_tmp);
		$I->click($row);

		// Let's wait till the modal appear
		$I->waitForElement(\Page\Player\Report::$user_modal_test_results);

		// I see the list of answers in the modal
		$I->seeElement(\Page\Player\Report::$user_modal_view_answers);

		//Generate the currenct question title
		$tmpCodes = [];
		$tmpValues = [];
		foreach($this->tmpData['test']->questions as $key => $question){
			foreach($question->answers as $iKey => $answer){
				$tmpCodes[] = '[answ ' .($iKey+1).']';
				$tmpValues[] = $key . '_' . $iKey;
			}
		}
		$title = str_replace($tmpCodes, $tmpValues, $this->tmpData['question']->title_quest );
		// I see the question title placed in the modal
		$I->see($title, \Page\Player\Report::$user_modal_view_answers);
	}

}
