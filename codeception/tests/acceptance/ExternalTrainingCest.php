<?php
require_once "DoceboCest.php";

use Page\apps\Transcripts\TranscriptsManagement as ExternalTraining;
use Page\apps\Transcripts\AdditionalField as ExtTrainingAddFields;
use Page\ReportManagement as Report;

class ExternalTrainingCest extends DoceboCest
{
    /**
     * @var Helper\User
     */
    protected $usersHelper;

    /**
     * @var Helper\apps\ExternalTraining
     */
    protected $externalTraining;

    /**
     * @var Helper\Course
     */
    protected $courseHelper;

    /**
     * @var Helper\Plugin
     */
    protected $pluginHelper;

    // Store Settings
    private $tmpData;

    /**
     * Inject dependencies
     * @param \Helper\Course $courseHelper
     */
    protected function _inject(\Helper\User $usersHelper, \Helper\apps\ExternalTraining $externalTraining, \Helper\Course $courseHelper, \Helper\Plugin $pluginHelper) {
        $this->usersHelper = $usersHelper;
        $this->externalTraining = $externalTraining;
        $this->courseHelper = $courseHelper;
        $this->pluginHelper = $pluginHelper;
    }


    /**
     * @scenario Power User not seeing the Additional fields button
     *
     * @group DOCEBO-1042
     *
     * @param AcceptanceTester $I
     */
    public function powerUserNotSeeingTheAdditionalFieldsButton(AcceptanceTester $I) {
        // Activate the plugin first
        $this->pluginHelper->activatePluginByCodeName('Transcripts');
        // First check if the External Training Plugin is active
        $I->seePluginIsActive('TranscriptsApp');

        // Create TMP Super administrator
        $user = $this->usersHelper->createUser(Yii::app()->user->getAdminLevelLabel());

        // And login with HIM
        $I->login(Yii::app()->user->getRelativeUsername($user->userid), $this->usersHelper->getDefaultPass());

        // Let's visit the plugin management page
        $url = ExternalTraining::createActivitiesListUrl();
        $I->amOnPage($url);

        // I see the management buttons
        $I->seeElement(ExternalTraining::$main_actions);

        // We should see the new button to appear
        $I->dontSeeElement(ExternalTraining::$additional_fields_management_button);
    }

    /**
     * @scenario Superadministrator seeing the Additional fields button
     *
     * @group DOCEBO-1044
     * @before login
     *
     * @param AcceptanceTester $I
     */
    public function superadministratorSeeingTheAdditionalFieldsButton(AcceptanceTester $I) {
        // Activate the plugin first
        $this->pluginHelper->activatePluginByCodeName('Transcripts');
        // First check if the External Training Plugin is active
        $I->seePluginIsActive('TranscriptsApp');

        // Let's visit the plugin management page
        $url = ExternalTraining::createActivitiesListUrl();
        $I->amOnPage($url);

        // I see the management buttons
        $I->seeElement(ExternalTraining::$main_actions);

        // We should see the new button to appear
        $I->seeElement(ExternalTraining::$additional_fields_management_button);

        // Let's hover the button nad see if there is any message appeared
        $I->moveMouseOver(ExternalTraining::$additional_fields_management_button);

        // The info windows should appear
        $I->seeElement(ExternalTraining::$main_actions_info);

        // And inside the window we should see the following message
        $I->see(Yii::t('transcripts', 'Extend your default training record by creating and assigning brand new custom fields'), ExternalTraining::$main_actions_info);
    }

    /**
     *
     * @scenario FIELDS MANAGEMENT
     *
     * @group DOCEBO-1044
     * @before login
     *
     * @param AcceptanceTester $I
     */
    public function fieldsManagement(AcceptanceTester $I) {

        // Activate the plugin first
        $this->pluginHelper->activatePluginByCodeName('Transcripts');

        //Superadministrator clicking on the Additional fields button
        $I->comment("Superadministrator clicking on the Additional fields button");
        $this->_superadministratorClickingOnTheAdditionalFieldsButton($I);

        // Superadministrator creating Date type Additional Field
        $I->comment("Superadministrator creating Date type Additional Field");
        $this->_superadministratorCreatingDateTypeAdditionalField($I);

        // Superadministrator creating Dropdown type Additional Field
        $I->comment("Superadministrator creating Dropdown type Additional Field");
        $this->_superadministratorCreatingDropdownTypeAdditionalField($I);

        // Superadministrator editing an Additional Field of simple type
        $I->comment("Superadministrator editing an Additional Field of simple type");
        $this->_superadministratorEditingAnAdditionalFieldOfSimpleType($I);

        // Superadministrator editing an Additional Field of Complex type
        $I->comment("Superadministrator editing an Additional Field of Complex type");
        $this->_superadministratorEditingAnAdditionalFieldOfComplexType($I);

        // Superadministrator dragging an existing Additional Field
        $I->comment("Superadministrator dragging an existing Additional Field");
        $this->_superadministratorDraggingAnExistingAdditionalField($I);

        // Superadministrator clicking on delete on an existing Additional Field
        $I->comment("Superadministrator clicking on delete on an existing Additional Field");
        $this->_superadministratorClickingOnDeleteOnAnExistingAdditionalField($I);
    }

    public function _superadministratorClickingOnTheAdditionalFieldsButton(AcceptanceTester $I){
        // Let's visit the plugin management page
        $url = ExternalTraining::createActivitiesListUrl();
        $I->amOnPage($url);

        // We will click on the additional fields button
        $I->click(ExternalTraining::$additional_fields_management_button);

        // We should be redirected in the additional fields management screen
        $I->seeInCurrentUrl(ExtTrainingAddFields::createIndexUrl());

        // Let's wait till the page load and the element appear
        $I->waitForElement(ExtTrainingAddFields::$additional_fields_types_dropdown);

        // When the dropdown appear, he should contain the following values
        $types = TranscriptsField::getTranscriptsFieldsTypes();
        $I->seeDropdownContainsAllValues(ExtTrainingAddFields::$additional_fields_types_dropdown, $types);
    }

    public function _superadministratorCreatingDateTypeAdditionalField(AcceptanceTester $I){

        // Let's visit the plugin management page
        $url = ExtTrainingAddFields::createIndexUrl();
        $I->seeInCurrentUrl($url);

        // Let's select the date field
        $I->selectOption(ExtTrainingAddFields::$additional_fields_types_dropdown, TranscriptsField::TYPE_FIELD_DATE);

        // I click on the create new external field
        $I->openModal(ExtTrainingAddFields::$add_new_field);
        $I->waitForElementVisible(\Page\Common::$modalHeaderElement);

        // The modal for creating text field should appear and should contain certain header title
        $I->see(Yii::t('field', '_' . strtoupper(TranscriptsField::TYPE_FIELD_DATE)), ExtTrainingAddFields::$modalHeaderElement);

        //There should be a dropdown containing all System active languages
        $allLang = array_keys(CoreLangLanguage::getActiveLanguages());
        $I->seeDropdownContainsAllValues(ExtTrainingAddFields::$language_list, $allLang);

        // Fields the field name with some value
        $I->fillField(ExtTrainingAddFields::$default_language_input, "Date Field");

        // And then save changes
        $I->click(ExtTrainingAddFields::$modal_confirm);

        // Wait till the modal is closed and the grid is refreshed
        ExtTrainingAddFields::waitForModalToCloseAndGirdToLoad($I);

        // Filter the table so it will show only our item
        ExtTrainingAddFields::filterTable($I, "Date Field");

         // We should see in the grid the following data
        $I->see("Date Field", ExtTrainingAddFields::$fields_table);
        $I->see(Yii::t('field', '_' . strtoupper(TranscriptsField::TYPE_FIELD_DATE)), ExtTrainingAddFields::$fields_table);
    }

    public function _superadministratorCreatingDropdownTypeAdditionalField(AcceptanceTester $I){
        // Let's visit the plugin management page
        $url = ExtTrainingAddFields::createIndexUrl();
        $I->seeInCurrentUrl($url);

        // Let's select the date field
        $I->selectOption(ExtTrainingAddFields::$additional_fields_types_dropdown, TranscriptsField::TYPE_FIELD_DROPDOWN);

        // I click on the create new external field
        $I->openModal(ExtTrainingAddFields::$add_new_field);
        $I->waitForElementVisible(\Page\Common::$modalHeaderElement);

        // The modal for creating text field should appear and should contain certain header title
        $I->see(Yii::t('field', '_' . strtoupper(TranscriptsField::TYPE_FIELD_DROPDOWN)), ExtTrainingAddFields::$modalHeaderElement);

        //There should be a dropdown containing all System active languages
        $allLang = array_keys(CoreLangLanguage::getActiveLanguages());
        $I->seeDropdownContainsAllValues(ExtTrainingAddFields::$language_list, $allLang);

        // Fill the field name with the proper value
        $I->fillField(ExtTrainingAddFields::$default_language_input, "Coutry");

        // Add two values
        $lang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
        $item = str_replace(array('{lang}', '{n}'), array($lang, 1), ExtTrainingAddFields::$filed_type_dropdown_entry_template);
        $I->fillField($item, "Italy");
        $item2 = str_replace(array('{lang}', '{n}'), array($lang, 2), ExtTrainingAddFields::$filed_type_dropdown_entry_template);
        $I->fillField($item2, "Bulgaria");

        // Save Changes
        $I->click(ExtTrainingAddFields::$modal_confirm);

        // Wait till the modal is closed and the grid is refreshed
        ExtTrainingAddFields::waitForModalToCloseAndGirdToLoad($I);

        // Filter the table so it will show only our item
        ExtTrainingAddFields::filterTable($I, "Coutry");

        // We should see in the grid the following data
        $I->see("Coutry", ExtTrainingAddFields::$fields_table);
        $I->see(Yii::t('field', '_' . strtoupper(TranscriptsField::TYPE_FIELD_DROPDOWN)), ExtTrainingAddFields::$fields_table);
    }

    public function _superadministratorEditingAnAdditionalFieldOfSimpleType(AcceptanceTester $I){
        // Filter the table with certain value
        ExtTrainingAddFields::filterTable($I, "Date Field");

        // Let's click edit on that field
        $I->openModal(ExtTrainingAddFields::$fields_table_last_edit_button);
        $I->waitForElementVisible(\Page\Common::$modalHeaderElement);

        // The modal for creating text field should appear and should contain certain header title
        $I->see(Yii::t('field', '_' . strtoupper(TranscriptsField::TYPE_FIELD_DATE)), ExtTrainingAddFields::$modalHeaderElement);

        //There should be a dropdown containing all System active languages
        $allLang = array_keys(CoreLangLanguage::getActiveLanguages());
        $I->seeDropdownContainsAllValues(ExtTrainingAddFields::$language_list, $allLang);

        // I see that the field contains the old values
        $I->seeElement(ExtTrainingAddFields::$default_language_input, array("value" => "Date Field"));

        // Then I'm changing it
        $I->fillField(ExtTrainingAddFields::$default_language_input, "Date Field - new");

        // And saving all the changes
        $I->click(ExtTrainingAddFields::$modal_confirm);

        // Wait till the modal disappear
        ExtTrainingAddFields::waitForModalToCloseAndGirdToLoad($I);

        // Filter the table with certain value
        ExtTrainingAddFields::filterTable($I, "Date Field - new");

        // We should see in the grid the following data
        $I->see("Date Field - new", ExtTrainingAddFields::$fields_table);
        $I->see(Yii::t('field', '_' . strtoupper(TranscriptsField::TYPE_FIELD_DATE)), ExtTrainingAddFields::$fields_table);
    }

    public function _superadministratorEditingAnAdditionalFieldOfComplexType(AcceptanceTester $I){
        // Filter the table with certain value
        ExtTrainingAddFields::filterTable($I, "Coutry");

        // Let's click edit on that field
        $I->openModal(ExtTrainingAddFields::$fields_table_last_edit_button);
        $I->waitForElementVisible(\Page\Common::$modalHeaderElement);

        // The modal for creating text field should appear and should contain certain header title
        $I->see(Yii::t('field', '_' . strtoupper(TranscriptsField::TYPE_FIELD_DROPDOWN)), ExtTrainingAddFields::$modalHeaderElement);

        //There should be a dropdown containing all System active languages
        $allLang = array_keys(CoreLangLanguage::getActiveLanguages());
        $I->seeDropdownContainsAllValues(ExtTrainingAddFields::$language_list, $allLang);

        // I see that the field contains the old values
        $I->seeElement(ExtTrainingAddFields::$default_language_input, array("value" => "Coutry"));

        // Then I'm changing it
        $I->fillField(ExtTrainingAddFields::$default_language_input, "Coutry - new");

        // Let's see if we have two fields populated for this language
        $lang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
        $item = str_replace(array('{lang}', '{n}'), array($lang, 1), ExtTrainingAddFields::$filed_type_dropdown_entry_template);
        $I->seeElement($item);
        $item2 = str_replace(array('{lang}', '{n}'), array($lang, 2), ExtTrainingAddFields::$filed_type_dropdown_entry_template);
        $I->seeElement($item2);

        // Save all the changes
        $I->click(ExtTrainingAddFields::$modal_confirm);

        // Wait till the modal disappear
        ExtTrainingAddFields::waitForModalToCloseAndGirdToLoad($I);

        // Filter the table with certain value
        ExtTrainingAddFields::filterTable($I, "Coutry - new");

        // We should see in the grid the following data
        $I->see("Coutry - new", ExtTrainingAddFields::$fields_table);
        $I->see(Yii::t('field', '_' . strtoupper(TranscriptsField::TYPE_FIELD_DROPDOWN)), ExtTrainingAddFields::$fields_table);
    }

    public function _superadministratorDraggingAnExistingAdditionalField(AcceptanceTester $I){
        // Filter the table with certain value
        ExtTrainingAddFields::filterTable($I, "");

        // Let's build the items selectors
        $item = str_replace(array( '{n}'), array(1), ExtTrainingAddFields::$fields_table_move_button_template);
        $item2 = str_replace(array( '{n}'), array(2), ExtTrainingAddFields::$fields_table_move_button_template);
        // And change the places
        $I->dragAndDrop($item2, $item);

        // Wait 3 sec before reloading
        $I->wait(3);
        // Reload the page
        $I->reloadPage();

        // Wait till the grid loads
        ExtTrainingAddFields::waitForModalToCloseAndGirdToLoad($I);

        // Check if they keep the propper order
        $item = str_replace(array( '{n}'), array(1), ExtTrainingAddFields::$fields_table_row_template);
        $item2 = str_replace(array( '{n}'), array(2), ExtTrainingAddFields::$fields_table_row_template);
        $I->see("Coutry - new", $item);
        $I->see("Date Field - new", $item2);
    }

    public function _superadministratorClickingOnDeleteOnAnExistingAdditionalField(AcceptanceTester $I){
        // Click on the delete icon of the last element in the grid
        $I->openModal(ExtTrainingAddFields::$fields_table_last_delete_button);

        // I see a modal window titled "Delete Additional Field"
        $I->see(Yii::t('custom_fields', 'Delete Additional Field'), ExtTrainingAddFields::$modalHeaderElement);

        // The modal has the field name displayed
        $I->see(Yii::t('standard', '_FIELD_NAME') . ': "' . 'Date Field - new', ExtTrainingAddFields::$modalElement);

        // I see a checkbox with the label "Yes, I want to proceed!"
        $I->see(Yii::t('standard', 'Yes, I confirm I want to proceed'), ExtTrainingAddFields::$modalElement);

        // The checkbox is unchecked by default
        $I->dontSeeStyledCheckboxIsChecked(ExtTrainingAddFields::$delete_field_checkbox);

        // Initially I don't see the confirm button
        $I->dontSeeElement(ExtTrainingAddFields::$delete_field_confirm_button);

        // Mark the "Yes, I want to proceed!" checkbox
        $I->checkStyledOption(ExtTrainingAddFields::$delete_field_checkbox);

        // I see the "Yes" button is visible
        $I->seeElement(ExtTrainingAddFields::$delete_field_confirm_button);

        // I click on the Yes button
        $I->click(ExtTrainingAddFields::$delete_field_confirm_button);

        // Wait till the modal is closed and the grid is reloaded
        ExtTrainingAddFields::waitForModalToCloseAndGirdToLoad($I);

        // Then I should not see the element in the grid
        $I->dontSee("Date Field - new", ExtTrainingAddFields::$fields_table);
    }

    /**
     * @scenario User editing an external training record with additional fields for external training
     *
     * @group DOCEBO-1046
     * @group DOCEBO-1046-1
     *
     * @param AcceptanceTester $I
     */
    public function userEditingAnExternalTrainingRecordWithAdditionalFieldsForExternalTraining(AcceptanceTester $I) {

        // First check if the External Training Plugin is active
        $this->pluginHelper->activatePluginByCodeName('Transcripts');
        Settings::save('transcripts_allow_users', 'on');
        // Set custom date format
        Settings::save('date_format', 'custom');
        Settings::save('date_format_custom', 'en');

        // Create Normal user
        $user = $this->usersHelper->createUser();
        $course = $this->courseHelper->createElearningCourseWithCode('404');

        // Create External training additional fields
        $date = $this->externalTraining->createAdditionalField(TranscriptsField::TYPE_FIELD_DATE, "Field 1");
        $dd = $this->externalTraining->createAdditionalField(TranscriptsField::TYPE_FIELD_DROPDOWN, "Field 2", array("Option 1", "Option 2"));
        $text = $this->externalTraining->createAdditionalField(TranscriptsField::TYPE_FIELD_TEXTFIELD, "Field 3");
        // And login with HIM
        $I->login(Yii::app()->user->getRelativeUsername($user->userid), $this->usersHelper->getDefaultPass());

        $url = \Page\MyActivities::createIndexUrl(array('tab' => \Page\MyActivities::$tab_external_activities));
        $I->amOnPage($url);
        $this->tmpData = [
            'user' => $user,
            'course' => $course,
            'date' => $date,
            'dd' => $dd,
            'text' => $text,
        ];

        $this->_userCreatingEditingExternalTrainingRecordWithAdditionalFields($I);

    }

    public function _userCreatingEditingExternalTrainingRecordWithAdditionalFields(AcceptanceTester $I, $isAdmin = false) {

        // Wait till everything is loaded
        $I->waitForElement(\Page\MyActivities::$new_activity);
        // Let's click on the new activity button
        $I->openModal(\Page\MyActivities::$new_activity, \Page\MyActivities::$modal_edit_activity);
        $I->waitForElement(\Page\MyActivities::$external_training_course_name);

        if($isAdmin)
            $I->executeJS("$('" . ExternalTraining::$assign_user_input ."').val('".Yii::app()->user->getRelativeUsername($this->tmpData['user']->userid)."')");

        // Fill the course name since is required
        $I->fillField(\Page\MyActivities::$external_training_course_name, $this->tmpData['course']->name);

        // And the date to, it's requred too
        $courseToDateValue = Yii::app()->localtime->toLocalDate(date('Y-m-d', strtotime('tomorrow')));
        $I->fillField(\Page\MyActivities::$external_training_to_date, $courseToDateValue);

        // Manage Date field
        $dateField = str_replace(array('{id}'), array($this->tmpData['date']->id_field), \Page\MyActivities::$date_additional_field_template);
        $dateFiledValue =  Yii::app()->localtime->toLocalDate(date('Y-m-d', strtotime('today')));
        $I->fillField($dateField, $dateFiledValue);

        // Fill the dropdown with the second item
        $ddField = str_replace(array('{id}'), array($this->tmpData['dd']->id_field), \Page\MyActivities::$additional_field_template);
        $I->selectOption($ddField, "Option 2");

        // Check if the Filed 3 is on place
        $textField = str_replace(array('{id}'), array($this->tmpData['text']->id_field), \Page\MyActivities::$additional_field_template);
        $I->seeElement($textField);

        // Submit the modal form
        $I->click(\Page\MyActivities::$confirm_activity_modal);

        $I->wait(3);
        // Wait till the grid loads
        \Page\MyActivities::waitForModalToCloseAndGirdToLoad($I);

        // And we should se those into the listed table
        $I->see($this->tmpData['course']->name, \Page\MyActivities::$external_training_table_last_row);

        // Let's click on the edit of the element
        $I->click(\Page\MyActivities::$external_training_table_edit_last_row);

        // And wait till he modal appear
        $I->waitForElement(\Page\MyActivities::$modalHeaderElement);
        $I->waitForElement($dateField);

        // I see that the date field contain today's date
        $I->seeInField($dateField, $dateFiledValue);

        /// The drop down field should contain "Option 2"
        $I->seeOptionIsSelected($ddField, "Option 2");

        // Populate the last field with "Value 3"
        $I->fillField($textField, "Value 3");

        // Click on "save changes" in order to keep our changes
        $I->click(\Page\MyActivities::$confirm_activity_modal);

        $I->wait(3);
        // Wait till the grid loads
        \Page\MyActivities::waitForModalToCloseAndGirdToLoad($I);
    }

    /**
     * @scenario Power User creating/editing external training record with additional fields
     *
     * @group DOCEBO-1046
     * @group DOCEBO-1046-2
     *
     * @param AcceptanceTester $I
     */
    public function powerUserCreatingEditingExternalTrainingRecordWithAdditionalFields(AcceptanceTester $I) {
        // First check if the External Training Plugin is active
        $this->pluginHelper->activatePluginByCodeName('Transcripts');
        Settings::save('transcripts_allow_users', 'on');
        // Set custom date format
        Settings::save('date_format', 'custom');
        Settings::save('date_format_custom', 'en');

        // Create Normal user
        $user = $this->usersHelper->createUser(Yii::app()->user->getAdminLevelLabel());
        $course = $this->courseHelper->createElearningCourseWithCode('404');

        // Create External training additional fields
        $date = $this->externalTraining->createAdditionalField(TranscriptsField::TYPE_FIELD_DATE, "Field 1");
        $dd = $this->externalTraining->createAdditionalField(TranscriptsField::TYPE_FIELD_DROPDOWN, "Field 2", array("Option 1", "Option 2"));
        $text = $this->externalTraining->createAdditionalField(TranscriptsField::TYPE_FIELD_TEXTFIELD, "Field 3");
        // And login with HIM
        $I->login(Yii::app()->user->getRelativeUsername($user->userid), $this->usersHelper->getDefaultPass());

        $url = \Page\MyActivities::createIndexUrl(array('tab' => \Page\MyActivities::$tab_external_activities));
        $I->amOnPage($url);
        $this->tmpData = [
            'normalUser' => $user,
            'course' => $course,
            'date' => $date,
            'dd' => $dd,
            'text' => $text,
        ];

        $this->_userCreatingEditingExternalTrainingRecordWithAdditionalFields($I);
    }

    /**
     * @scenario  Administrator creating an External Training report with additional fields
     *
     * @group DOCEBO-1052
     * @before login
     *
     * @param AcceptanceTester $I
     */
    public function  administratorCreatingAnExternalTrainingReportWithAdditionalFields(AcceptanceTester $I) {
        // Activate TranscriptsApp PLUGIN
        $this->pluginHelper->activatePluginByCodeName('Transcripts');
        // Set custom date format
        Settings::save('date_format', 'custom');
        Settings::save('date_format_custom', 'en');
        // Create A normal user
        $user = $this->usersHelper->createUser();
        $course = $this->courseHelper->createElearningCourseWithCode('404');
        $this->courseHelper->assignUserAs(LearningCourseuser::$COURSE_USER_SUBSCRIBED, $user, $course);

        $af1 = $this->externalTraining->createAdditionalField(TranscriptsField::TYPE_FIELD_TEXTFIELD, 'Field 1');
        $af2 = $this->externalTraining->createAdditionalField(TranscriptsField::TYPE_FIELD_DATE, 'Field 2');

        $settings = array(
            'id_user' => $user->idst,
            'course_name' => $course->name,
            'course_type' => $course->course_type,
            'to_date' => date('Y-m-d'),
            'status' => TranscriptsRecord::STATUS_APPROVED,
            'additionalFields' => array(
                $af1->id_field => array(
                    'type' => TranscriptsField::TYPE_FIELD_TEXTFIELD,
                    'value' => 'ABC',
                ),
                $af2->id_field => array(
                    'type' => TranscriptsField::TYPE_FIELD_DATE,
                    'value' => '2015-02-01'
                )
            )
        );
        $this->externalTraining->createRecord($settings);

        // Go to the report page
        $I->amOnPage(Report::route('index'));

        // Click on new report
        $I->openModal(Report::$new_custom_report_button);

        // Fill the first
        Report::newReportFillStep1($I, LearningReportType::USERS_EXTERNAL_TRAINING, "Users - External Training");

        // Move to the next step
        Report::wizardNextStep($I);

        // Wait till the page reloiad it's content
        $I->wait(2);
        $I->waitForElement(Report::$new_custom_report_wizard_page_user_selector);

        // Click ON select all
        $I->click(Report::$new_custom_report_wizard_page_user_selector_select_all_users);
        $I->wait(2);
        // Move to the next step
        Report::wizardNextStep($I);

        // Wait till the page reloiad it's content
        $I->waitForElement(Report::$new_custom_report_wizard_field_page);

        $option = str_replace(array('{id}'), array($af1->id_field),Report::$report_ext_training_field_template);
        $I->checkStyledOption($option);
        $option = str_replace(array('{id}'), array($af2->id_field),Report::$report_ext_training_field_template);
        $I->checkStyledOption($option);

        // Move to the next step
        Report::wizardNextStep($I);
        // Wait till the modal is loaded
        $I->waitForElement(\Page\Common::$modalElement . \Page\Common::$modalLoaded);

        // Move to the next step
        Report::wizardNextStep($I);
        // Wait till the modal is loaded
        $I->waitForElement(\Page\Common::$modalElement . \Page\Common::$modalLoaded);

        // Click on save and show
        $I->click(Report::$new_custom_report_wizard_last_page_saveNShow);

        // Wait till the grid loads
        $I->waitForElement(Report::$view_report_grid);

        // See those into the page
        $I->see('Field 1');
        $I->see('Field 2');
        $I->see("ABC");
        $I->see(Yii::app()->localtime->toLocalDate('2015-02-01'));
    }

}