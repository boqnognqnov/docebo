<?php

/**
 * Base Docebo test case superclass
 *
 * It contains generic method to be reused by all test categories (reports, users management, courses management...)
 */
abstract class DoceboCest {

    /**
     * Helper for running something once for the whole cest
     */
    protected $alreadyRun = false;
    protected $userHelper;
    protected $pluginHelper;

    public function _inject(\Helper\User $user, \Helper\Plugin $pluginHelper){
        $this->userHelper = $user;
        $this->pluginHelper = $pluginHelper;
    }

    /**
     * The following code will execute before codeception starts running each test case.
     */
    public function _before(AcceptanceTester $I) {
        if(!$this->alreadyRun)
            $this->alreadyRun = true;
    }

    /**
     * The following code will execute after codeception runs each test case.
     */
    public function _after(AcceptanceTester $I) {
        // Check for errors on the page
        $I->amGoingTo('check for errors on page');
        $I->dontSeeElement(Page\Common::$siteError);
        $I->dontSee(Page\Common::$phpNotice, Page\Common::$phpNoticeText);
        $I->dontSeeElement(Page\Common::$callStack);

        if(!$this->alreadyRun)
            $this->alreadyRun = true;
    }
    
    protected function login(AcceptanceTester $I) {
        extract($this->userHelper->getDefaultSuperadminAccount());
        /* @var $username */
        /* @var $password */
        $I->login($username, $password);
    }

    protected function activatePluginTranscripts(){
        $this->pluginHelper->activatePluginByCodeName('Transcripts');
    }
}