<?php
// Autoload the DoceboCest superclass
require_once "DoceboCest.php";

/**
 * Category of test cases for the login page
 */
class LmsLoginCest extends DoceboCest {

	/**
	 * @var Helper\SelfRegistration
	 */
	protected $selfRegistrationHelper;

	/**
	 * @var Helper\Orgchart
	 */
	protected $orgchartHelper;

	/**
	 * @var Helper\User
	 */
	protected $usersHelper;

	/**
	 * @var Helper\apps\Multidomain
	 */
	protected $multidomainHelper;

	/**
	 * @var Helper\Plugin
	 */
	protected $pluginHelper;

	/**
	 * Inject dependencies
	 * @param \Helper\SelfRegistration $selfRegistrationHelper
	 */
	protected function _inject(
		\Helper\SelfRegistration $selfRegistrationHelper,
		\Helper\Orgchart $orgchartHelper,
		\Helper\User $userHelper,
		\Helper\apps\Multidomain $multidomainHelper,
		\Helper\Plugin $pluginHelper) {
		$this->selfRegistrationHelper = $selfRegistrationHelper;
		$this->orgchartHelper = $orgchartHelper;
		$this->usersHelper = $userHelper;
		$this->multidomainHelper = $multidomainHelper;
		$this->pluginHelper = $pluginHelper;
	}

	/**
	 * Bug: Branch code in self-registration should not work outside the Multidomain client branch
	 *
	 * @scenario User self-registers on main LMS domain and branch dropdown is displayed
	 *
	 * @group DOCEBO-652
	 *
	 * @param AcceptanceTester $I
	 */
	public function selfRegisterWithDropdownRegistrationCodes(AcceptanceTester $I) {
		$this->selfRegistrationHelper->haveRegistrationCodeWithDropdownSelection();
		$I->amOnPage(\Page\Home::$URL);
		$I->openModal(\Page\Home::$modalSelfRegisterOpenLink);
		$I->expectTo("find the \"Code\" dropdown populated with all branches with a non-empty code");
		$I->seeElement(\Page\Home::$registrationCodesDropdow);
		$I->seeDropdownContainsAllValues(\Page\Home::$registrationCodesDropdow, $this->orgchartHelper->getBranchesWithNonEmptyCodes());
	}

	/**
	 *
	 * @scenario Superadministrator configuring whitelisted domains globally
	 *
	 * @group DOCEBO-1401
	 * @group DOCEBO-1401-1
	 * @before login
	 *
	 * @param AcceptanceTester $I
	 */
	public function superadministratorConfiguringWhitelistedDomainsGlobally(AcceptanceTester $I) {
		$selfRegistration = new \Page\AdvancedSettings\SelfRegister($I);
		// Let' go to Advanced Settings menu
		$I->goToPageUsingMenu(\Page\AdvancedSettings::route());
		// Let's go to self registration tab
		$I->click(\Page\AdvancedSettings::$tab_self_registration);
		// Let's wait till the element appear and the and see if the element is CHECKED
		$regTypeSelf = str_replace('{n}', '0', $selfRegistration::$registration_type_tmp);
		$I->waitForElement($regTypeSelf);
		$I->seeStyledRadioIsChecked($regTypeSelf);
		// The allowed domains field should be empty
		$I->dontSeeElement($selfRegistration::$allowed_domains_first_element);

		// Add few domains
		$domains = array("@gmail.com", "@mybankbank.com", "@mycompany.com");
		$I->fillFCBKAutocomplete($selfRegistration::$allowed_domains_dom_id, $domains);

		// Remove on element from the page
		$I->removeFCBKAutocompleteElement($selfRegistration::$allowed_domains_dom_id, '@mycompany.com');

		// Save the settings on the page
		$selfRegistration->saveSettings();

		// Let's see if the field contains the two options
		$domains = array("@gmail.com", "@mybankbank.com");
		$I->seeFBKCompleteHasSelected("#" . $selfRegistration::$allowed_domains_dom_id, $domains);

		// Let's change the registration type
		$regTypeAdmin = str_replace('{n}', '2', $selfRegistration::$registration_type_tmp);
		$I->checkStyledRadio($regTypeAdmin);

		// We should see the element is disabled
		$I->seeElement($selfRegistration::$allowed_domains_blocked);
	}

	/**
	 *
	 * @scenario Superadministrator configuring whitelisted domains for a specific multidomain client
	 *
	 * @group DOCEBO-1401
	 * @group DOCEBO-1401-2
	 * @before login
	 *
	 * @param AcceptanceTester $I
	 */
	public function superadministratorConfiguringWhitelistedDomainsForASpecificMultidomainClient(AcceptanceTester $I) {
		// Let's set the self-registration global settings to "Free self-registration"
		Settings::save('register_type', 'self');
		$domains = array("@gmail.com", "@citibank.com", "@citi.com", "@mycompany.com");
		Settings::save('allowed_domains', CJSON::encode($domains));

		 // Let's activate the Multidomain
		$this->pluginHelper->activatePluginByCodeName('Multidomain');

		// We want to create Multidomain client but we will need a org chart so let's create it
		$node = $this->orgchartHelper->createNode(404, 'Domain Branch');
		$data = array(
			'name' => 'Forth client',
			'domain' => 'mydomain.com',
			'domain_type' => CoreMultidomain::DOMAINTYPE_CUSTOMDOMAIN,
			'org_chart' => $node->idOrg
		);

		$client = $this->multidomainHelper->createClient($data);

		$settings = new \Page\Apps\Multidomain\Settings($I);

		$I->amOnPage($settings::route(false, array('id' => $client->id)));

		// Let's go on the self registration tab
		$tab = str_replace('{tab}', 'self-registration', $settings::$tab_tmp);
		$I->click($tab);

		// Don't see the custom settings for self registration are enabled
		$I->dontSeeStyledCheckboxIsChecked($settings::$enable_self_registration_settings);

		$I->seeElement($settings::$self_registration_settings_disabled);

		$element = str_replace('{n}', CoreMultidomain::FREE_SELF_REGISTRATION_CODE, $settings::$self_registration_registration_type_tmp);
		$I->seeStyledCheckboxIsChecked($element);

		// The Allowed domains field contains the default values
		$I->seeFBKCompleteHasSelected("#" . $settings::$self_registration_allowed_domains, $domains);

		// Let's enable the custom settigns for self registration
		$I->checkStyledOption($settings::$enable_self_registration_settings);

		$I->wait(1);
		// Click on the Registration type "Only by admin"
		$element = str_replace('{n}', CoreMultidomain::REGISTRATION_BY_ADMIN_CODE, $settings::$self_registration_registration_type_tmp);
		$I->checkStyledRadio($element);

		// See that the allowed domains is disabled
		$I->seeElement($settings::$self_registration_allowed_domains_disabled);

		// Click on the Registration type "Moderated self-registration"
		$element = str_replace('{n}', CoreMultidomain::MODERATED_SELF_REGISTRATION_CODE, $settings::$self_registration_registration_type_tmp);
		$I->checkStyledRadio($element);
		$I->wait(1);

		// Remove two items from the allowed domains
		$I->removeFCBKAutocompleteElement($settings::$self_registration_allowed_domains, '@citi.com');
		$I->removeFCBKAutocompleteElement($settings::$self_registration_allowed_domains, '@citibank.com');

		// Add one items into the allowed domains
		$I->fillFCBKAutocomplete($settings::$self_registration_allowed_domains, "@mycompany2.com");

		// Let's save changes on the page
		$settings->saveChanges();

		$element = str_replace('{n}', CoreMultidomain::MODERATED_SELF_REGISTRATION_CODE, $settings::$self_registration_registration_type_tmp);
		$I->seeStyledCheckboxIsChecked($element);
		$domains = array( "@gmail.com", "@mycompany.com", "@mycompany2.com");
		$I->seeFBKCompleteHasSelected("#". $settings::$self_registration_allowed_domains, $domains);
	}

	/**
	 *
	 * @scenario User trying to self-register on main domain
	 *
	 * @group DOCEBO-1423
	 * @group DOCEBO-1423-1
	 *
	 * @param AcceptanceTester $I
	 */
	public function userTryingToSelfRegisterOnMainDomain(AcceptanceTester $I) {
		Settings::save('register_type', 'self');
		Settings::save('mail_sender', 'a@b.c');
		$domains = array("@docebo.com", "@docebo.info");
		Settings::save('allowed_domains', CJSON::encode($domains));

		// Go to the login page
		$I->amOnPage(\Page\Home::$URL);

		// Let's click on the register button on the page
		$I->openModal(\Page\Login::$register_button);

		// Fill the registration form
		$I->fillField(\Page\Login::$register_form_username, 'john.doe');
		$I->fillField(\Page\Login::$register_form_email, 'john.doe@yahoo.it');
		$I->fillField(\Page\Login::$register_form_password, 'pass7821');
		$I->fillField(\Page\Login::$register_form_re_password, 'pass7821');
		$I->checkOption(\Page\Login::$register_form_accept_checkbox);
		// Click on confirm button
		$I->click(\Page\Login::$register_form_confirm_button);

		$I->wait(1);
		// Then we should see that the email domain is not accepted
		$message = Yii::t('register', "You'r email address is not allowed for self-registration. Please, contact the platform administrator for further information.");
		$I->see($message, \Page\Login::$modalElement);
		// Change the email and again enter the password fields
		$I->fillField(\Page\Login::$register_form_email, 'john.doe@docebo.com');
		$I->fillField(\Page\Login::$register_form_password, 'pass7821');
		$I->fillField(\Page\Login::$register_form_re_password, 'pass7821');
		$I->checkOption(\Page\Login::$register_form_accept_checkbox);
		$I->click(\Page\Login::$register_form_confirm_button);

		$I->wait(2);
		$I->seeElement(\Page\Login::$register_form_successful_registration);
	}

	/**
	 *
	 * @scenario User trying to self-register on multidomain client
	 *
	 * @group DOCEBO-1423
	 * @group DOCEBO-1423-2
	 *
	 * @param AcceptanceTester $I
	 */
	public function userTryingToSelfRegisterOnMultidomainClient(AcceptanceTester $I) {
		// Let's set the self-registration global settings to "Free self-registration"
		Settings::save('register_type', 'self');
		Settings::save('mail_sender', 'a@b.c');
		$domains = array("@docebo.com", "@docebo.info");
		Settings::save('allowed_domains', CJSON::encode($domains));
		// Let's activate the Multidomain
		$this->pluginHelper->activatePluginByCodeName('Multidomain');

		// We want to create Multidomain client but we will need a org chart so let's create it
		$node = $this->orgchartHelper->createNode(404, 'Domain Branch');
		$data = array(
			'name' => 'My Domain',
			'domain' => 'mydomain.com',
			'domain_type' => CoreMultidomain::DOMAINTYPE_CUSTOMDOMAIN,
			'org_chart' => $node->idOrg,
			'enable_self_registration_settings' => 1,
			'register_type' => 0,
			'allowed_domains' => array("@docebo.com", "@docebo.info"),
		);

		$client = $this->multidomainHelper->createClient($data);
		// Go to the domain url
		$I->amOnUrl("http://mydomain.com");
		$I->openModal(\Page\Login::$register_button);

		// Fill the registration form
		$I->fillField(\Page\Login::$register_form_username, 'john.doe');
		$I->fillField(\Page\Login::$register_form_email, 'john.doe@yahoo.it');
		$I->fillField(\Page\Login::$register_form_password, 'pass7821');
		$I->fillField(\Page\Login::$register_form_re_password, 'pass7821');
		$I->checkOption(\Page\Login::$register_form_accept_checkbox);
		// Click on confirm button
		$I->click(\Page\Login::$register_form_confirm_button);

		$I->wait(1);
		// Then we should see that the email domain is not accepted
		$message = Yii::t('register', "You'r email address is not allowed for self-registration. Please, contact the platform administrator for further information.");
		$I->see($message, \Page\Login::$modalElement);
		// Change the email and again enter the password fields
		$I->fillField(\Page\Login::$register_form_email, 'john.doe@docebo.com');
		$I->fillField(\Page\Login::$register_form_password, 'pass7821');
		$I->fillField(\Page\Login::$register_form_re_password, 'pass7821');
		$I->checkOption(\Page\Login::$register_form_accept_checkbox);
		$I->click(\Page\Login::$register_form_confirm_button);

		$I->wait(2);
		$I->seeElement(\Page\Login::$register_form_successful_registration);
	}
}
