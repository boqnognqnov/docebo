<?php
// Autoload the DoceboCest superclass
require_once "DoceboCest.php";

/**
 * Category of test cases for the login page
 */
class ReportMangementCest extends DoceboCest {

    /**
     * @var $userHelper \Helper\User
     */
    protected $userHelper;

    /**
     * @var $orgChartHelper \Helper\Orgchart
     */
    protected $orgChartHelper;

    /**
     * @var $userHelper \Helper\Course
     */
    protected $courseHelper;

    public function _inject(\Helper\User $user, \Helper\Orgchart $orgChart, \Helper\Course $course){
        $this->userHelper = $user;
        $this->orgChartHelper = $orgChart;
        $this->courseHelper = $course;
    }
    
    /**
     * Enhance report filter for enrollment days
     *
     * @scenario Superadministrator configuring report filters
     *
     * @group DOCEBO-1483
     *
     * @param AcceptanceTester $I
     */
    public function superadministratorConfiguringReportFilters(AcceptanceTester $I) {
        $N = "28/02/2016 00:00";
        $N2 = "26/02/2016 00:00"; // $N-2


        $I->comment("Given I am a superadministrator");
        $admin = $this->userHelper->createUser(Yii::app()->user->getGodadminLevelLabel());
        $I->login(Yii::app()->user->getRelativeUsername($admin->userid), $this->userHelper->getDefaultPass());

        $I->comment("I create course 'DOCEBO-1483'");
        $course = $this->courseHelper->createElearningCourseWithCode('DOCEBO-1483');

        $I->comment('And user John was enrolled in course C1 N days ago (DOCEOBO-1483 = C1)');
        $john = $this->userHelper->createUser(Yii::app()->user->getUserLevelLabel());
        $this->courseHelper->assignUserAs(false, $john, $course);
        if($this->courseHelper->setEnrollmentDateOfUser($john, $course, $N)) {
            $I->comment("User ({$john->userid}) is enrolled for ({$N}) at course ({$course->name})");
        }

        $I->comment('And user Susan was enrolled in course C1 (N-2) days ago (DOCEOBO-1483 = C1)');
        $susan = $this->userHelper->createUser(Yii::app()->user->getUserLevelLabel());
        $this->courseHelper->assignUserAs(false, $susan, $course);
        if($this->courseHelper->setEnrollmentDateOfUser($susan, $course, $N2)) {
            $I->comment("User ({$susan->userid}) is enrolled for ({$N2}) at course ({$course->name})");
        }

        $I->comment("And I am in the reports management page");
        $I->amOnPage('/admin/index.php?r=reportManagement/index');

        $I->comment("And I've click on \"Create custom report\"");
        $I->click('form#report-management-form a.btn[rel="custom-report-wizard"]');
        $I->waitForElement('form#report-wizard-step-1-form');

        $I->comment("And I've selected \"Users Courses report\" (Default)");
        $I->click('form#report-wizard-step-1-form > div.types > div.row-fluid:nth-child(1) > label');
        $I->fillField('form#report-wizard-step-1-form input[name=filter_name]', 'DOCEBO-1483');
        $I->click('div.modal.report-wizard-step-1 div.modal-footer a.btn.jwizard-nav');
        $I->waitForElement('form#selector-users');

        $I->comment("And I've selected all users");
        $I->click('form#selector-users div#users div.mass-selector a.select-all');
        $I->wait(5);

        $I->click('div a.btn.jwizard-nav:nth-of-type(2)');
        $I->waitForElement('#courses-filter-styler');

        $I->comment("And I've selected all courses");
        $I->click('#courses-filter-styler');

        $I->comment("When I click on Next");
        $I->click('div a.btn.jwizard-nav:nth-of-type(2)');
        $I->waitForElement('form#report-wizard-step-4-form');

        $I->comment("Then I see a dropdown with options \"exactly\", \"less than\", \"more than\" in the \"Include only users enrolled\" report filter");
        $I->seeElement('select#timelapse');

        $I->comment("And I see the default value is \"more than\"");
        $I->makeScreenshot('should_have_dropdown');
        $I->seeElementInDOM('select#timelapse option', array('value' => 'more'));
        $I->seeElementInDOM('select#timelapse option', array('value' => 'less'));
        $I->seeElementInDOM('select#timelapse option', array('value' => 'exact'));
        $I->seeOptionIsSelected('select#timelapse', 'more than');

        $I->comment("And I select \"N\" days ago");
        $I->fillField('input#ReportFieldsForm_filters_days_from_course', 1);

        $I->comment("And I click on Next two times");
        $I->click('div a.btn.jwizard-nav:nth-of-type(2)');

        $I->waitForElement('form#report-wizard-order-form');

        $I->click('div a.btn.jwizard-nav:nth-of-type(2)');

        $I->comment("And I click on \"Save and view\"");
        $I->waitForElement('input[name=finish_wizard_show]');
        $I->click('input[name=finish_wizard_show]');

        $I->comment("And I see the report contains the enrollment record for John");
        $I->comment("And I see the report does not contain the enrollment record for Susan");
        $I->waitForElement('table.items tr td:first-child');
        $I->wait(4);
        $I->see(substr($john->userid, 1), 'table.items tr td:first-child');
        $I->dontSee(substr($susan->userid, 1), 'table.items tr td:first-child');

        $I->comment("Fast Logout");
        $I->amOnPage('/lms/index.php?r=site/logout');
    }
}
