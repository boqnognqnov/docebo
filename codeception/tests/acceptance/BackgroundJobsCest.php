<?php
// Autoload the DoceboCest superclass
require_once "DoceboCest.php";

/**
 * Class ExampleDoceboCest
 *
 * Put here all test cases belonging to the "example_docebo" category.
 */
class BackgroundJobCest extends DoceboCest {

	protected $usersHelper;
	protected $fileManager;

	/**
	 * Inject dependencies
	 * @param \Helper\User $usersHelper
	 */
	protected function _inject(\Helper\User $usersHelper, \Helper\FileManager $fileManager) {
		$this->usersHelper = $usersHelper;
		$this->fileManager = $fileManager;
	}
	/**
	 * The following code will execute before codeception starts running each test case.
	 * Put something here only if you need to add "before" actions, otherwise delete the method from this subclass
	 */
	public function _before(AcceptanceTester $I) {
		parent::_before($I);
	}

	/**
	 * The following code will execute after codeception runs each test case.
	 * Put something here only if you need to add "after" actions, otherwise delete the method from this subclass
	 */
	public function _after(AcceptanceTester $I) {
		// Do not delete this to allow superclass to do its "after" actions
		parent::_after($I);

		// Add your actions here...
	}

	private function loginGodAdmin(AcceptanceTester $I){
		extract($this->usersHelper->getDefaultSuperadminAccount());
		/* @var $username */
		/* @var $password */
		$I->login($username, $password);
	}

	private function loginPu(AcceptanceTester $I){
		$user = $this->usersHelper->createUser(Yii::app()->user->getAdminLevelLabel());
		$I->login(Yii::app()->user->getRelativeUsername($user->userid), $this->usersHelper->getDefaultPass());
	}

	private function loginCommonUser(AcceptanceTester $I){
		$user = $this->usersHelper->createUser();
		$I->login(Yii::app()->user->getRelativeUsername($user->userid), $this->usersHelper->getDefaultPass());
	}

	/**
	 * @scenario "Administrator trying to fire a dummy background job and receiving an error"
	 *
	 * @group DOCEBO-518
	 * @group DOCEBO-1141 - subtask
	 *
	 * @param AcceptanceTester $I
	 */
	public function startBackgroundJobByAdministratorExpectError(AcceptanceTester $I) {
		// TODO -now simulate the inactive state of the node.js server, should be removed, when the logic for
		// TODO -variable $checkIfNodeServerIsEnabled in \lms\protected\modules\inbox\components\InboxComponent.php is ready
		// in method onBeforeLogoutLink in order to
		PluginManager::deactivateAppByCodename('Notification');
		// first case login with godadmin account
		$this->loginGodAdmin($I);
		$I->startBackgroundJobWithError();
		$I->logout();

		PluginManager::deactivateAppByCodename('Notification');
		// second case login with poweruser account
		$this->loginPu($I);
		$I->startBackgroundJobWithError();
		$I->logout();
	}

	/**
	 * @scenario "Administrator firing a dummy background job and waiting for it to finish"
	 *
	 * @group DOCEBO-518
	 * @group DOCEBO-1141 - subtask
	 *
	 * @param AcceptanceTester $I
	 */
	public function startBackgroundJobExpectSuccess(AcceptanceTester $I){

		// first case login with godadmin account
		$this->loginGodAdmin($I);
		$this->createNotificationInboxMsg($I);
		$I->startBackgroundJobWithSuccess();
		$I->logout();

		// second case login with poweruser account
		$this->loginPu($I);
		$I->startBackgroundJobWithSuccess();
		$I->logout();
	}

	/**
	 * @scenario "Administrator firing a dummy background job and interrupting it"
	 *
	 * @group DOCEBO-518
	 * @group DOCEBO-1141 - subtask
	 *
	 * @param AcceptanceTester $I
	 */
	public function startBackgroundJobInterruptingIt(AcceptanceTester $I){

		// first case login with godadmin account
		$this->loginGodAdmin($I);
		$this->createNotificationInboxMsg($I);
		$I->startBackgroundJobWithSuccess(true);
		$I->logout();

		// second case login with poweruser account
		$this->loginPu($I);
		$I->startBackgroundJobWithSuccess(true);
		$I->logout();
	}

	/**
	 * @scenario "Poweruser opening "View all" page"
	 *
	 * @group DOCEBO-5181
	 * @group DOCEBO-1254 - subtask
	 *
	 * @param AcceptanceTester $I
	 */
	public function powerUserOpenViewAllPage(AcceptanceTester $I){

		// login with poweruser account
		$this->loginPu($I);
		$I->seeElement(\Page\Test2\BackgroundJobJob::$headerJobNotificationElementCounter, array('text' => 3));
		$I->openBackgroundTooltipAndClickOnViewAllJobs();

		$I->dontSeeElement(\Page\Test2\BackgroundJobsFull::$pageTabPaneElements);

		// only 3 total jobs, one should be active
		$I->seeNumberOfElements(\Page\Test2\BackgroundJobsFull::$pageSingleJobContainer, 3);
		$I->seeNumberOfElements(\Page\Test2\BackgroundJobsFull::$pageNotifyLink, 1);

		// click on the notify link to receive notification, when the job is finished
		$I->clickNotifyLinkPage();

		// wait for the job to finish
		$I->waitForElementChange(\Page\Test2\BackgroundJobsFull::$pageJobPercent.\Page\Test2\BackgroundJob::FIRST_OF_TYPE_CSS, function() use($I){
			return ($I->grabTextFrom(\Page\Test2\BackgroundJobsFull::$pageJobPercent.\Page\Test2\BackgroundJob::FIRST_OF_TYPE_CSS) == '100');
		}, 10);
		$I->dontSeeElement(\Page\Test2\BackgroundJobsFull::$pageNotificationStopLink.':first-of-type');
		$I->seeElement(\Page\Test2\BackgroundJobsFull::$pageJobErrors);
		$I->seeElement(\Page\Test2\BackgroundJobsFull::$pageJobErrorsLink, array('text' => 3));
		$I->clickToDownloadErrors();

		// todo file download check
		$filePath = Docebo::getUploadTmpUrl('file.csv');
		$numberOfErrors = $this->fileManager->getNewlineNumbers($filePath);
		PHPUnit_Framework_Assert::assertEquals(3, $numberOfErrors, 'Number of errors in csv file is different than 3!');

		$I->logout();
	}

	/**
	 * @scenario "Administrator interrupting job from "Background jobs" page"
	 *
	 * @group DOCEBO-5181
	 * @group DOCEBO-1254 - subtask
	 *
	 * @param AcceptanceTester $I
	 */
	public function godadminInterruptJob(AcceptanceTester $I){
		$this->loginGodAdmin($I);
		$I->adminSeeJobsAndInterruptTheOneWhichIsActive();
		$I->logout();

		$this->loginPu($I);
		$I->adminSeeJobsAndInterruptTheOneWhichIsActive();
		$I->logout();
	}

	/**
	 * @scenario "Administrator seeing all jobs in the "Background jobs" page"
	 *
	 * @group DOCEBO-5181
	 * @group DOCEBO-1254 - subtask
	 *
	 * @param AcceptanceTester $I
	 */
	public function godadminSeeAllJobs(AcceptanceTester $I){
		$this->loginGodAdmin($I);
		$I->seeElement(\Page\Test2\BackgroundJob::$headerJobNotificationElementCounter, array('text' => 9));
		$I->openBackgroundTooltipAndClickOnViewAllJobs();
		$I->seeElement(\Page\Test2\BackgroundJobsFull::$pageTabPaneElements);
		$I->click(\Page\Test2\BackgroundJobsFull::$pageTabPaneElementsFirstTab);
		$I->seeNumberOfElements(\Page\Test2\BackgroundJobsFull::$pageSingleJobContainer, 9);
		$I->click(\Page\Test2\BackgroundJobsFull::$pageTabPaneElementsSecondTab);
		$I->seeNumberOfElements(\Page\Test2\BackgroundJobsFull::$pageSingleJobContainer, 3);
		$I->logout();
	}
}
