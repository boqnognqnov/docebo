<?php
// Autoload the DoceboCest superclass
require_once "DoceboCest.php";

/**
 * Class ExampleDoceboCest
 *
 * Put here all test cases belonging to the "example_docebo" category.
 */
class ExampleDoceboCest extends DoceboCest {

	/**
	 * The following code will execute before codeception starts running each test case.
	 * Put something here only if you need to add "before" actions, otherwise delete the method from this subclass
	 */
	public function _before(AcceptanceTester $I) {
		// Do not delete this to allow superclass to do its "before" actions
		parent::_before($I);

		// Add your actions here...
	}

	/**
	 * The following code will execute after codeception runs each test case.
	 * Put something here only if you need to add "after" actions, otherwise delete the method from this subclass
	 */
	public function _after(AcceptanceTester $I) {
		// Do not delete this to allow superclass to do its "after" actions
		parent::_after($I);

		// Add your actions here...
	}

	/**
	 * Each method is a test case/scenario.
	 *
	 * @scenario "<Full scenario name as per the JIRA task>"
	 *
	 * @group <JIRA TASK ID HERE>
	 *
	 * @param AcceptanceTester $I
	 */
	public function login(AcceptanceTester $I) {
		// Implement your acceptance criteria's scenario here
		// using helper methods coming from the $I variable.
		// If you need new ones, please add them to the corresponding helper class so
		// that others case reuse them

		$I->amOnPage("/");
		// other test actions here ....
	}
}
