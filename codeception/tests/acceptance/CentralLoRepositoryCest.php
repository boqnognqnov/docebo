<?php

require_once "DoceboCest.php";

class CentralLoRepositoryCest extends DoceboCest
{
    /**
     * @var $userHelper \Helper\User
     */
    protected $userHelper;

    /**
     * @var $centralRepo \Helper\CentralRepository
     */
    protected $centralRepo;

    /**
     * @var $lo \Helper\Lo
     */
    protected $lo;

    /**
     * Inject dependencies
     * @param \Helper\SelfRegistration $selfRegistrationHelper
     */
    protected function _inject(\Helper\User $userHelper, \Helper\CentralRepository $centralRepo, \Helper\Lo $loHelper) {
        $this->usersHelper = $userHelper;
        $this->centralRepo = $centralRepo;
        $this->lo = $loHelper;
    }

    // Comment out all the OLD TESTS because now the Central LO category management is changed and the test wouldn't work
//
//    /**
//     * @scenario Superadministrator accessing central LO and building category tree
//     *
//     * @group DOCEBO-1143
//     * @group DOCEBO-1563
//     * @before login
//     *
//     * @param AcceptanceTester $I
//     */
//    public function superadministratorAccessingCentralLOAndBuildingCategoryTree(AcceptanceTester $I){
//        $page = new \Page\CentralRepository($I);
//        $I->goToPageUsingMenu($page::route($page::$URL, array(), false, 'lms'));
//        // Let's see that the tree contains the Docebo category
//        $category = 'Docebo';
//        $I->waitForText($category, 5 ,$page::$categoryTree);
//        // Add new category
//        $category1 = 'Business';
//        $page->addNewCategory($category1);
//        $page->checkCategoryContainsCategory(array(), $category1);
//        // Let's edit the previouse category
//        $category2 = 'Soft skills';
//        $page->editCategory($category1, $category2);
//        $page->checkCategoryContainsCategory(array(), $category2);
//        // Let's ADD one new category under the previouse one
//        $category3 = 'Special soft skills';
//        $page->addNewCategoryUnder($category2, $category3);
//        $page->checkCategoryContainsCategory(array($category2), $category3);
//        // Let's move the newly created category
//        $page->moveCategoryUnder($category, $category3);
//        $page->checkCategoryContainsCategory(array(), $category3);
//        // Let's delete the special soft skills category
//        $page->deleteCategory($category3);
//        // See that there is no more category3 in the list
//        $I->dontSee($category3, $page::$categoryTree);
//    }
//
//    /**
//     * @scenario Superadministrator filtering and searching training materials in the Central Repository
//     *
//     * @group DOCEBO-1729
//     * @group DOCEBO-1563
//     * @before login
//     *
//     * @param AcceptanceTester $I
//     */
//    public function superadministratorFilteringAndSearchingTrainingMaterialsInTheCentralRepository(AcceptanceTester $I){
//        // Let's Add some data into DB
//        $englishCourse = "English course";
//        $trainingCourse = "Training course";
//        $testDocebo = "Test Docebo";
//        $business   = $this->centralRepo->createCategory('Business');
//        $softSkills = $this->centralRepo->createCategory('Soft skills');
//        $node3      = $this->centralRepo->createCategory('Node 3');
//        $node4      = $this->centralRepo->createCategory('Node 4');
//        $this->centralRepo->createLO($englishCourse, LearningOrganization::OBJECT_TYPE_SCORMORG, $softSkills->getPrimaryKey());
//        $this->centralRepo->createLO($trainingCourse, LearningOrganization::OBJECT_TYPE_ELUCIDAT, null);
//        $this->centralRepo->createLO($testDocebo, LearningOrganization::OBJECT_TYPE_TEST, $node3->getPrimaryKey());
//
//
//        $page = new \Page\CentralRepository($I);
//        $I->goToPageUsingMenu($page::route($page::$URL, array(), false, 'admin'));
//        $category = 'Docebo';
//        $I->waitForText($category, 5,$page::$categoryTree);
//        $root = str_replace('{item}', "Docebo", $page::$categoryTreeRowSelectedTemplate);
//        $I->seeElement($root);
//        $I->dontSeeStyledCheckboxIsChecked($page::$loContainChildrenInput);
//        $I->see($trainingCourse, $page::$losGrid);
//        $I->checkStyledOption($page::$loContainChildrenInput);
//        // Wait till the grid load
//        $I->wait(2);
//        // Let's check the grid's content
//        $I->see($englishCourse, $page::$losGrid);
//        $I->see($trainingCourse, $page::$losGrid);
//        $I->see($testDocebo, $page::$losGrid);
//        //Let's change the filter typ
//        $I->selectOption($page::$loTypeFilterInput, LearningOrganization::OBJECT_TYPE_SCORMORG);
//        $I->wait(2);
//        $I->see($englishCourse, $page::$losGrid);
//        $I->dontSee($trainingCourse, $page::$losGrid);
//        $I->dontSee($testDocebo, $page::$losGrid);
//        // Change the options to show all the elements again
//        $I->selectOption($page::$loTypeFilterInput, "");
//        $I->wait(2);
//        $I->see($englishCourse, $page::$losGrid);
//        $I->see($trainingCourse, $page::$losGrid);
//        $I->see($testDocebo, $page::$losGrid);
//        $I->executeJS("$('".$page::$loSearchInput."').val('Course')");
//        $I->pressKey($page::$loSearchInput, WebDriverKeys::ENTER);
//        $I->wait(1);
//        $I->see($englishCourse, $page::$losGrid);
//        $I->see($trainingCourse, $page::$losGrid);
//        $I->dontSee($testDocebo, $page::$losGrid);
//        $changeCat = str_replace('{item}', 'Soft skills', $page::$categoryTreeRowTemplate);
//        $I->click($changeCat);
//        $I->wait(2);
//        $I->see($englishCourse, $page::$losGrid);
//        $I->dontSee($trainingCourse, $page::$losGrid);
//        $I->dontSee($testDocebo, $page::$losGrid);
//    }

    /**
     * Activate the Elucidat Plugin before, because we nee
     */
    public function activateElucidat()    {
        $this->centralRepo->activateElucidatPlugin();
    }

    /**
     * @scenario Superadministrator creating and then editing LO in central repository
     *
     * @group DOCEBO-1799
     * @group DOCEBO-1563
     * @before activateElucidat
     * @before login
     * @after logout
     *
     * @param AcceptanceTester $I
     */
    public function superadministratorCreatingAndThenEditingLOinCentralRepository(AcceptanceTester $I){
        $page = new \Page\CentralRepository($I);

        $video = 'Video';
        $videoName = 'My personal video';
        $videoShortDescription = 'This is my personal video';
        $videoAdditionalText = 'Additional info';
        $videoNewTitle = 'My correct personal video';
        $versionName = 'V1.1';
        $versionDesciption = 'This contains the latest recoding';

        $I->goToPageUsingMenu($page::route($page::$URL, array(), false, 'lms'));
        $I->click($page::$addLoButton . ' a');
        $page->seeAllLOs();
        $I->click($page::getLoCreateUrl(LearningOrganization::OBJECT_TYPE_VIDEO));
        $I->wait(5);
        $I->waitForText($video, 5,$page::$centralRepoContainer);
        $I->attachFile($page::$fileUploader, $page::$video1);
        $I->wait(5);
        $I->fillField($page::$objectTitle, $videoName);
        $I->click(array('link' => $videoAdditionalText));
        $I->fillField($page::$objectShortDescription, $videoShortDescription);
        $I->executeJS("var thumb = $('.sub-item'); $(thumb[0]).find('img').click()");
        $I->click($page::$addLoSubmitButton);
        $I->wait(5);
        $I->waitForText($page::$centralRepoTitle);
        $I->see($videoName);
        $I->click(array('link' => 'Edit'), 'div.grid-view');
        $I->wait(5);
        $I->waitForText($video, 5,$page::$centralRepoContainer);
        $I->fillField($page::$objectTitle, $videoNewTitle);
        $I->click($page::$addLoSubmitButton);
        $I->wait(5);
        $I->waitForText($page::$centralRepoTitle);
        $I->see($videoNewTitle);
        $I->click(array('link' => 'Edit'), 'div.grid-view');
        $I->wait(5);
        $I->waitForText($video, 5,$page::$centralRepoContainer);
        $I->attachFile($page::$fileUploader, $page::$video2);
        $I->wait(5);
        $I->waitForElementVisible($page::$versionControlElement);
        $I->seeStyledRadioIsChecked('#keep-version');
        $I->checkStyledRadio($page::$overrideVersionOption);
        $I->fillField($page::$newVersionName, $versionName);
        $I->fillField($page::$newVersionDescription, $versionDesciption);
        $I->click($page::$addLoSubmitButton);
        $I->wait(5);
        $I->waitForText($page::$centralRepoTitle);
    }

    protected function preparationForDOCEBO1851(){
        $objectTitle = 'OBJ1';
        $user = $this->usersHelper->createUser();
        /**
         * @var $user CoreUser
         */
        $object = $this->centralRepo->createLO($objectTitle, LearningOrganization::OBJECT_TYPE_VIDEO);
        $video = $this->lo->createVideo($objectTitle);
        $version1 = $this->centralRepo->createVersion($object->id_object, $video->id_video, LearningOrganization::OBJECT_TYPE_VIDEO, $user->idst);
        $video2 = $this->lo->createVideo($objectTitle);
        $version2 = $this->centralRepo->createVersion($object->id_object, $video2->id_video, LearningOrganization::OBJECT_TYPE_VIDEO, $user->idst, 2, 'V2');
        $course = $this->centralRepo->createCourse();

        $userSubscribe = $this->centralRepo->subscribeUserToCourse($course->idCourse, $user->idst);

        if($userSubscribe !== false){
            $pushToCourse = $this->centralRepo->pushToCourse($version2->id_resource, LearningOrganization::OBJECT_TYPE_VIDEO, $course->idCourse, $video2->title);

            if($pushToCourse !== false){
                $completeLo = $this->centralRepo->completeLO($pushToCourse->idOrg, $user->idst, $version2->id_resource);

                if($completeLo !== false){
                    return array(
                        'idCourse' => $course->idCourse,
                        'userName' => str_replace('/', '', $user->userid)
                    );
                }
            }
        }

        return false;
    }

    protected function preparationForDOCEBO1849(){
        $objectTitle = 'OBJ1';
        $objectTitle2 = 'OBJ2';
        $objectTitle3 = 'OBJ3';
        $courseTitle = 'C1';

        $user1 = $this->usersHelper->createUser();
        $user2 = $this->usersHelper->createUser();
        $user3 = $this->usersHelper->createUser();

        $object = $this->centralRepo->createLO($objectTitle, LearningOrganization::OBJECT_TYPE_VIDEO);
        $object2 = $this->centralRepo->createLO($objectTitle2, LearningOrganization::OBJECT_TYPE_VIDEO);
        $object3 = $this->centralRepo->createLO($objectTitle3, LearningOrganization::OBJECT_TYPE_VIDEO);

        $video1 = $this->lo->createVideo();
        $video2 = $this->lo->createVideo('Video 2');
        $video3 = $this->lo->createVideo('Video 3');
        $video4 = $this->lo->createVideo('Video 4');
        $video5 = $this->lo->createVideo('Video 5');

        $version1 = $this->centralRepo->createVersion($object->id_object, $video1->id_video, LearningOrganization::OBJECT_TYPE_VIDEO, $user1->idst);
        $version2 = $this->centralRepo->createVersion($object->id_object, $video2->id_video, LearningOrganization::OBJECT_TYPE_VIDEO, $user1->idst, 2, 'V2');

        $version2_1 = $this->centralRepo->createVersion($object2->id_object, $video3->id_video, LearningOrganization::OBJECT_TYPE_VIDEO, $user1->idst);
        $version3_1 = $this->centralRepo->createVersion($object3->id_object, $video4->id_video, LearningOrganization::OBJECT_TYPE_VIDEO, $user1->idst);
        $version3_2 = $this->centralRepo->createVersion($object3->id_object, $video5->id_video, LearningOrganization::OBJECT_TYPE_VIDEO, $user1->idst, 2, 'V2');

        $course = $this->centralRepo->createCourse();

        $this->centralRepo->subscribeUserToCourse($course->idCourse, $user1->idst);
        $this->centralRepo->subscribeUserToCourse($course->idCourse, $user2->idst);
        $this->centralRepo->subscribeUserToCourse($course->idCourse, $user3->idst);

        $lo = $this->centralRepo->pushToCourse($version1->id_resource, LearningOrganization::OBJECT_TYPE_VIDEO, $course->idCourse, $video1->title, $version1->id_object);

        $this->centralRepo->putLoInProgress($lo->idOrg, $user1->idst, $version1->id_resource);
        $this->centralRepo->putLoInProgress($lo->idOrg, $user2->idst, $version1->id_resource);
        $this->centralRepo->putLoInProgress($lo->idOrg, $user3->idst, $version1->id_resource);

        $data = array(
            'idCourse' => $course->idCourse,
            'object1' => $object->id_object,
            'object2' => $object2->id_object,
            'object3' => $object3->id_object,
            'loId' => $lo->idOrg,
            'idResource' => $version2->id_resource,
            'versionDatetime' => $version2->create_date
        );
        return $data;
    }

    /**
     * @scenario Administrator seeing version info inside training material statistics
     *
     * @group DOCEBO-1563
     * @group DOCEBO-1851
     *
     * @before login
     *
     * @param AcceptanceTester $I
     */
    public function administratorSeeingVersionInfoInsideTrainingMaterialStatistic(AcceptanceTester $I){
        $data = $this->preparationForDOCEBO1851();
        
        $page = new \Page\CentralRepository($I);

        $I->amOnUrl(Docebo::createAbsoluteLmsUrl($page::$courseStatisticsUrl, array(
            'course_id' => $data['idCourse']
        )));

        $I->waitForText($page::$courseReportHeader);
        $I->click(array('link' => $data['userName']));
        $I->waitForText($page::$userReportHeader . $data['userName']);
        $I->waitForText('V2');
    }

    /**
     *
     * @scenario Instructor importing one or more LOs from Central Repository
     *
     * @group DOCEBO-1563
     * @group DOCEBO-1849
     *
     * @before login
     *
     * @param AcceptanceTester $I
     */
    public function instructorImportingOneOrMoreLOsFromCentralRepository(AcceptanceTester $I){
        $data = $this->preparationForDOCEBO1849();

        $page = new \Page\CentralRepository($I);
        
        $I->amOnUrl(Docebo::createAbsoluteLmsUrl($page::$courseTrainingMaterialsUrl, array(
            'course_id' => $data['idCourse']
        )));

        $videoName = 'Video 1';
        $object2Name = 'OBJ2';
        $object3Name = 'OBJ3';

        $I->click($page::$addLoButtonCourse . ' a');
        $I->waitForElementVisible($page::$importCentralLo);
        $I->click($page::$importCentralLo);
        $I->seeElement($page::$usersSelector);
        $I->dontSee($videoName);
        $I->checkStyledOption($page::$gridViewItem . '0');
        $I->selectOption('#versions_' . $data['object2'], 1);
        $I->checkStyledOption($page::$gridViewItem . '1');
        $I->selectOption('#versions_' . $data['object3'], 2);
        $I->click(array('link' => 'NEXT'));
        $I->see($object2Name);
        $I->wait(2);
        $I->see($object3Name);
        $I->executeJS("$('li#" . $data['loId'] ." a.btn.dropdown-toggle.p-hover').click();");
        $I->executeJS("$('li#" . $data['loId'] ." a[data-id-object=" . $data['loId'] . "]').click();");
        $I->waitForElementVisible('.player-central-lo-shared-settings');
        $I->selectOption('#version', $data['idResource'] . '-' . LearningOrganization::OBJECT_TYPE_VIDEO);
        $I->click('input[type=submit]');
        $I->waitForElementVisible($page::$changeVersionModal);
        $I->click($page::$changeVersionModal . ' input[type=submit]');
        $I->waitForElementVisible($page::$alertSuccess);
        $I->see('V2 - ' . Yii::app()->localtime->toLocalDateTime($data['versionDatetime']));
    }
    
    private function prepareForDocebo1846($courseName){
        $object = $this->centralRepo->createLO('Scorm', LearningOrganization::OBJECT_TYPE_SCORMORG);
        $version1 = $this->centralRepo->createVersion($object->id_object, 1, LearningOrganization::OBJECT_TYPE_SCORMORG, 18064);
        $course = $this->centralRepo->createCourse($courseName);

        $lo = $this->centralRepo->pushToCourse($version1->id_resource, LearningOrganization::OBJECT_TYPE_SCORMORG, $course->idCourse, 'Scorm', $version1->id_object);
    }

    /**
     * @scenario Superadministrator sees the usage of a training material inside course
     *
     * @group DOCEBO-1563
     * @group DOCEBO-1846
     *
     * @before login
     *
     * @param AcceptanceTester $I
     */
    public function superadministratorSeesTheUsageOfaTrainingMaterialInsideCourse(AcceptanceTester $I){
        $courseName = 'English course';

        $this->prepareForDocebo1846($courseName);

        $page = new \Page\CentralRepository($I);

        $I->amOnUrl(Docebo::createAbsoluteLmsUrl($page::$URL));

        $I->see('1 course');
        $I->click(array('link' => '1 course'));
        $I->waitForElementVisible($page::$usageDialog);
        $I->see('Total: 1');
        $I->see($courseName);
    }


    /**
     * @scenario Preview training materials inside the Central Repository
     *
     * @group DOCEBO-1563
     * @group DOCEBO-1824
     *
     * @before login
     *
     * @param AcceptanceTester $I
     */
    public function previewTrainingMaterialsInsideTheCentralRepository(AcceptanceTester $I){

//        $page = new \Page\CentralRepository($I);
//
//        $I->amOnUrl(Docebo::createAbsoluteLmsUrl($page::$URL));
//
//        // Add the Main Element
//        $page->addAICC($page::$aicc1);
//
//        // Add Second Version of the AICC
//        $I->waitForElement($page::$losGrid);
//        $page->addAICCVersion(false, $page::$aicc2, $newVersion = true, "V1.1", "Second Version");
//
//        // Add One More Version of the AICC
//        $I->waitForElement($page::$losGrid);
//        $page->addAICCVersion(false, $page::$aicc3, $newVersion = true, "V2", "Last Version");
//
//        // Click on the first Element ... because we SHOULD have only 1 element for NOW
//        $I->openModal($page::$losGrid . " a.play-repo-object");
//
//        $I->waitForElement($page::$preview_object_versions);
//
//        // Check that the AICC has 3 versions
//        $I->seeDropdownContainsAllValues($page::$preview_object_versions, array("1", "2", "3") );
//
//        $I->seeOptionIsSelected($page::$preview_object_versions, "V2");
//
//        $I->seeElement($page::$preview_object_TOC);
//
//        $I->selectOption($page::$preview_object_versions, 2);
//        $I->wait(2);
//        $I->see("Description:");
//        $I->see("Second Version");
//
//        $I->selectOption($page::$preview_object_versions, 1);
//        $I->wait(2);
//        $I->dontSee("Description:");
    }

}