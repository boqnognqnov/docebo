-- MySQL dump 10.13  Distrib 5.6.24, for Win32 (x86)
--
-- Host: localhost    Database: docebo_cest
-- ------------------------------------------------------
-- Server version	5.6.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `app7020_answer`
--

DROP TABLE IF EXISTS `app7020_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idQuestion` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `content` text NOT NULL,
  `bestAnswer` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idUser` (`idUser`),
  KEY `idQuestion` (`idQuestion`),
  CONSTRAINT `app7020_answer_ibfk_1` FOREIGN KEY (`idQuestion`) REFERENCES `app7020_question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `app7020_answer_ibfk_2` FOREIGN KEY (`idUser`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_answer`
--

LOCK TABLES `app7020_answer` WRITE;
/*!40000 ALTER TABLE `app7020_answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_answer_like`
--

DROP TABLE IF EXISTS `app7020_answer_like`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_answer_like` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idAnswer` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idUser` (`idUser`),
  KEY `idAnswer` (`idAnswer`),
  CONSTRAINT `app7020_answer_like_ibfk_1` FOREIGN KEY (`idAnswer`) REFERENCES `app7020_answer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `app7020_answer_like_ibfk_2` FOREIGN KEY (`idUser`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_answer_like`
--

LOCK TABLES `app7020_answer_like` WRITE;
/*!40000 ALTER TABLE `app7020_answer_like` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_answer_like` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_content`
--

DROP TABLE IF EXISTS `app7020_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `filename` varchar(512) NOT NULL,
  `originalFilename` varchar(512) NOT NULL,
  `idThumbnail` int(11) DEFAULT NULL COMMENT 'Relation to CoreAssets. ',
  `contentType` int(11) DEFAULT '1',
  `idSource` int(11) NOT NULL DEFAULT '1' COMMENT 'This is relation field to all content sources. 1 - Web App, 2-Android App, 3 iOs Devices',
  `conversion_status` int(11) DEFAULT '0',
  `userId` int(11) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL COMMENT 'Duration on video content in seconds. If Content is not video this value by default is NULL ',
  `viewCounter` int(11) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `shownInList` tinyint(1) DEFAULT '0',
  `whishedStatus` int(11) DEFAULT NULL,
  `sns` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `UserId` (`userId`),
  KEY `contentType` (`contentType`),
  KEY `CountentSourceIndex` (`idSource`),
  KEY `idThumbnail` (`idThumbnail`),
  CONSTRAINT `ContentThumbnaiReference` FOREIGN KEY (`idThumbnail`) REFERENCES `core_asset` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `ContentUserReference` FOREIGN KEY (`userId`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_content`
--

LOCK TABLES `app7020_content` WRITE;
/*!40000 ALTER TABLE `app7020_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_content_history`
--

DROP TABLE IF EXISTS `app7020_content_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_content_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idContent` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `viewed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `index2` (`idContent`,`idUser`),
  KEY `app7020_content_history_idUser_fk_1` (`idUser`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_content_history`
--

LOCK TABLES `app7020_content_history` WRITE;
/*!40000 ALTER TABLE `app7020_content_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_content_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_content_published`
--

DROP TABLE IF EXISTS `app7020_content_published`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_content_published` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `idContent` int(11) NOT NULL,
  `datePublished` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `actionType` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `indexIdUser` (`idUser`),
  KEY `indexIdContent` (`idContent`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_content_published`
--

LOCK TABLES `app7020_content_published` WRITE;
/*!40000 ALTER TABLE `app7020_content_published` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_content_published` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_content_rating`
--

DROP TABLE IF EXISTS `app7020_content_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_content_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idContent` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index2` (`idContent`,`idUser`),
  KEY `app7020_content_rating_idUser_fk_1` (`idUser`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_content_rating`
--

LOCK TABLES `app7020_content_rating` WRITE;
/*!40000 ALTER TABLE `app7020_content_rating` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_content_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_content_reviews`
--

DROP TABLE IF EXISTS `app7020_content_reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_content_reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idContent` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `message` text,
  `systemMessage` int(11) NOT NULL DEFAULT '0',
  `idTooltip` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastUpdated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index2` (`idContent`,`idUser`),
  KEY `app7020_content_reviews_idUser_fk_1` (`idUser`),
  KEY `index_app7020_tooltips_idTooltip` (`idTooltip`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_content_reviews`
--

LOCK TABLES `app7020_content_reviews` WRITE;
/*!40000 ALTER TABLE `app7020_content_reviews` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_content_reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_content_thumbs`
--

DROP TABLE IF EXISTS `app7020_content_thumbs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_content_thumbs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idContent` int(11) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL COMMENT 'THE COUNT NUMBER ON IMAGE',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idContent` (`idContent`),
  CONSTRAINT `ContentRelation` FOREIGN KEY (`idContent`) REFERENCES `app7020_content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_content_thumbs`
--

LOCK TABLES `app7020_content_thumbs` WRITE;
/*!40000 ALTER TABLE `app7020_content_thumbs` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_content_thumbs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_custom_content_settings`
--

DROP TABLE IF EXISTS `app7020_custom_content_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_custom_content_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idContent` int(11) NOT NULL,
  `settingName` varchar(255) NOT NULL,
  `settingValue` text,
  PRIMARY KEY (`id`),
  KEY `app7020_custom_content_settings_fk1` (`idContent`),
  CONSTRAINT `app7020_custom_content_settings_fk1` FOREIGN KEY (`idContent`) REFERENCES `app7020_content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_custom_content_settings`
--

LOCK TABLES `app7020_custom_content_settings` WRITE;
/*!40000 ALTER TABLE `app7020_custom_content_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_custom_content_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_document_conversions`
--

DROP TABLE IF EXISTS `app7020_document_conversions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_document_conversions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idContent` int(11) NOT NULL,
  `idPipe` varchar(255) DEFAULT NULL,
  `urlPipe` varchar(255) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `expired` timestamp NULL DEFAULT NULL,
  `used` smallint(6) NOT NULL DEFAULT '0',
  `idUser` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `app7020_document_conversions_idUser_fk_1` (`idUser`),
  KEY `app7020_document_images_idContent_fk_1` (`idContent`),
  CONSTRAINT `app7020_document_conversions_idContent_fk_1` FOREIGN KEY (`idContent`) REFERENCES `app7020_content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `app7020_document_conversions_idUser_fk_1` FOREIGN KEY (`idUser`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `app7020_document_images_idContent_fk_1` FOREIGN KEY (`idContent`) REFERENCES `app7020_content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_document_conversions`
--

LOCK TABLES `app7020_document_conversions` WRITE;
/*!40000 ALTER TABLE `app7020_document_conversions` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_document_conversions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_document_images`
--

DROP TABLE IF EXISTS `app7020_document_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_document_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idContent` int(11) NOT NULL,
  `imageName` varchar(255) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_document_images`
--

LOCK TABLES `app7020_document_images` WRITE;
/*!40000 ALTER TABLE `app7020_document_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_document_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_document_tracking`
--

DROP TABLE IF EXISTS `app7020_document_tracking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_document_tracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `idContent` int(11) NOT NULL,
  `Page` int(11) DEFAULT '0',
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `app7020_document_tracking_idContent_fk_1` (`idContent`),
  KEY `app7020_document_tracking_idUser_fk_1` (`idUser`),
  CONSTRAINT `app7020_document_tracking_idContent_fk_1` FOREIGN KEY (`idContent`) REFERENCES `app7020_content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `app7020_document_tracking_idUser_fk_1` FOREIGN KEY (`idUser`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_document_tracking`
--

LOCK TABLES `app7020_document_tracking` WRITE;
/*!40000 ALTER TABLE `app7020_document_tracking` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_document_tracking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_experts`
--

DROP TABLE IF EXISTS `app7020_experts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_experts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `idAdmin` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `index2` (`idUser`,`idAdmin`),
  KEY `app7020_experts_fk_2` (`idAdmin`),
  CONSTRAINT `app7020_experts_fk_1` FOREIGN KEY (`idUser`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `app7020_experts_fk_2` FOREIGN KEY (`idAdmin`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_experts`
--

LOCK TABLES `app7020_experts` WRITE;
/*!40000 ALTER TABLE `app7020_experts` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_experts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_ignore_list`
--

DROP TABLE IF EXISTS `app7020_ignore_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_ignore_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `idObject` int(11) NOT NULL,
  `objectType` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index2` (`idUser`),
  CONSTRAINT `app7020_ignore_list_fk_1` FOREIGN KEY (`idUser`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_ignore_list`
--

LOCK TABLES `app7020_ignore_list` WRITE;
/*!40000 ALTER TABLE `app7020_ignore_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_ignore_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_invitations`
--

DROP TABLE IF EXISTS `app7020_invitations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_invitations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idInvited` int(11) NOT NULL,
  `idInviter` int(11) NOT NULL,
  `idContent` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `index2` (`idInvited`,`idInviter`,`idContent`),
  KEY `app7020_invitations_fk_2` (`idInviter`),
  KEY `app7020_invitations_fk_3` (`idContent`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_invitations`
--

LOCK TABLES `app7020_invitations` WRITE;
/*!40000 ALTER TABLE `app7020_invitations` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_invitations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_question`
--

DROP TABLE IF EXISTS `app7020_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `content` text,
  `idUser` int(11) NOT NULL,
  `idContent` int(11) DEFAULT NULL,
  `idLearningObject` int(11) DEFAULT NULL,
  `viewCounter` int(11) NOT NULL DEFAULT '0',
  `open` int(11) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idUser` (`idUser`),
  KEY `idContentIndex` (`idContent`),
  KEY `idLearningObjectIndex` (`idLearningObject`),
  CONSTRAINT `app7020_question_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `app7020_question_id_content` FOREIGN KEY (`idContent`) REFERENCES `app7020_content` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `app7020_question_id_learning_object` FOREIGN KEY (`idLearningObject`) REFERENCES `learning_organization` (`idOrg`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_question`
--

LOCK TABLES `app7020_question` WRITE;
/*!40000 ALTER TABLE `app7020_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_question_follow`
--

DROP TABLE IF EXISTS `app7020_question_follow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_question_follow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idQuestion` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idQuestion` (`idQuestion`),
  KEY `idUser` (`idUser`),
  CONSTRAINT `app7020_question_follow_ibfk_1` FOREIGN KEY (`idQuestion`) REFERENCES `app7020_question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `app7020_question_follow_ibfk_2` FOREIGN KEY (`idUser`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_question_follow`
--

LOCK TABLES `app7020_question_follow` WRITE;
/*!40000 ALTER TABLE `app7020_question_follow` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_question_follow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_question_history`
--

DROP TABLE IF EXISTS `app7020_question_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_question_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idQuestion` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `viewed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `index2` (`idQuestion`,`idUser`),
  KEY `app7020_idUser_fk_1` (`idUser`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_question_history`
--

LOCK TABLES `app7020_question_history` WRITE;
/*!40000 ALTER TABLE `app7020_question_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_question_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_question_topic`
--

DROP TABLE IF EXISTS `app7020_question_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_question_topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idTopic` int(11) NOT NULL,
  `idQuestion` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idQuestion` (`idQuestion`),
  KEY `idTopic` (`idTopic`),
  CONSTRAINT `app7020_question_topic_ibfk_1` FOREIGN KEY (`idTopic`) REFERENCES `app7020_topic_tree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `app7020_question_topic_ibfk_2` FOREIGN KEY (`idQuestion`) REFERENCES `app7020_question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_question_topic`
--

LOCK TABLES `app7020_question_topic` WRITE;
/*!40000 ALTER TABLE `app7020_question_topic` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_question_topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_questions_requests`
--

DROP TABLE IF EXISTS `app7020_questions_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_questions_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idQuestion` int(11) DEFAULT NULL,
  `idExpert` int(11) DEFAULT NULL,
  `idContent` int(11) DEFAULT NULL,
  `autoPublish` int(11) DEFAULT NULL,
  `autoInvite` int(11) DEFAULT NULL,
  `tooked` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index2` (`idQuestion`,`idExpert`,`idContent`),
  KEY `app7020_questions_requests_fk_2` (`idExpert`),
  KEY `app7020_questions_requests_fk_3` (`idContent`),
  CONSTRAINT `app7020_questions_requests_fk_1` FOREIGN KEY (`idQuestion`) REFERENCES `app7020_question` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `app7020_questions_requests_fk_2` FOREIGN KEY (`idExpert`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `app7020_questions_requests_fk_3` FOREIGN KEY (`idContent`) REFERENCES `app7020_content` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_questions_requests`
--

LOCK TABLES `app7020_questions_requests` WRITE;
/*!40000 ALTER TABLE `app7020_questions_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_questions_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_report`
--

DROP TABLE IF EXISTS `app7020_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `idContentType` int(11) NOT NULL,
  `idRelated` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `comment` text,
  PRIMARY KEY (`id`),
  KEY `idUser` (`idUser`),
  KEY `idContentType` (`idContentType`),
  KEY `idRelated` (`idRelated`),
  CONSTRAINT `app7020_report_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_report`
--

LOCK TABLES `app7020_report` WRITE;
/*!40000 ALTER TABLE `app7020_report` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_settings_peer_review`
--

DROP TABLE IF EXISTS `app7020_settings_peer_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_settings_peer_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idContent` int(11) NOT NULL,
  `enableCustomSettings` tinyint(1) NOT NULL DEFAULT '0',
  `inviteToWatch` tinyint(1) NOT NULL DEFAULT '0',
  `allowContributorAnswer` tinyint(1) NOT NULL DEFAULT '0',
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idContentIndex` (`idContent`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_settings_peer_review`
--

LOCK TABLES `app7020_settings_peer_review` WRITE;
/*!40000 ALTER TABLE `app7020_settings_peer_review` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_settings_peer_review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_share_custom_permissions`
--

DROP TABLE IF EXISTS `app7020_share_custom_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_share_custom_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idObject` int(11) NOT NULL,
  `objectType` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_share_custom_permissions`
--

LOCK TABLES `app7020_share_custom_permissions` WRITE;
/*!40000 ALTER TABLE `app7020_share_custom_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_share_custom_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_share_permissions`
--

DROP TABLE IF EXISTS `app7020_share_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_share_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_name` varchar(100) NOT NULL,
  `permission_value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_share_permissions`
--

LOCK TABLES `app7020_share_permissions` WRITE;
/*!40000 ALTER TABLE `app7020_share_permissions` DISABLE KEYS */;
INSERT INTO `app7020_share_permissions` VALUES (1,'contribute_permissions','contribute_all'),(2,'publish_permissions','contribute_manual_publish');
/*!40000 ALTER TABLE `app7020_share_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_tag`
--

DROP TABLE IF EXISTS `app7020_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tagText` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_tag`
--

LOCK TABLES `app7020_tag` WRITE;
/*!40000 ALTER TABLE `app7020_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_tag_link`
--

DROP TABLE IF EXISTS `app7020_tag_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_tag_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idTag` int(11) NOT NULL,
  `idContent` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_tag` (`idTag`,`idContent`),
  KEY `idTag` (`idTag`),
  KEY `idContent` (`idContent`),
  CONSTRAINT `app7020_tag_link_ibfk_1` FOREIGN KEY (`idTag`) REFERENCES `app7020_tag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `app7020_tag_link_ibfk_2` FOREIGN KEY (`idContent`) REFERENCES `app7020_content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_tag_link`
--

LOCK TABLES `app7020_tag_link` WRITE;
/*!40000 ALTER TABLE `app7020_tag_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_tag_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_tags_lo`
--

DROP TABLE IF EXISTS `app7020_tags_lo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_tags_lo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idTag` int(11) NOT NULL,
  `idLo` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index2` (`idTag`,`idLo`),
  KEY `app7020_tags_lo_fk_2` (`idLo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_tags_lo`
--

LOCK TABLES `app7020_tags_lo` WRITE;
/*!40000 ALTER TABLE `app7020_tags_lo` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_tags_lo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_temp_invitations`
--

DROP TABLE IF EXISTS `app7020_temp_invitations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_temp_invitations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idContent` int(11) NOT NULL,
  `invited` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `app7020_temp_invitations_fk1` (`idContent`),
  CONSTRAINT `app7020_temp_invitations_fk1` FOREIGN KEY (`idContent`) REFERENCES `app7020_content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_temp_invitations`
--

LOCK TABLES `app7020_temp_invitations` WRITE;
/*!40000 ALTER TABLE `app7020_temp_invitations` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_temp_invitations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_tooltips`
--

DROP TABLE IF EXISTS `app7020_tooltips`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_tooltips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idContent` int(11) NOT NULL COMMENT 'Id of app7020_content table. This field assign tooltop to any content',
  `idExpert` int(11) NOT NULL COMMENT 'relation to core_users. This filed is responsible for author of tooltip',
  `dateCreated` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `dateUpdated` timestamp NULL DEFAULT NULL,
  `tooltipRow` int(1) NOT NULL,
  `tooltipStyle` int(1) NOT NULL DEFAULT '1',
  `durationFrom` int(11) NOT NULL COMMENT 'When Flow player must CUE start anotation',
  `durationTo` int(11) NOT NULL COMMENT 'When Flow player must CUE End anotation',
  `text` text,
  PRIMARY KEY (`id`),
  KEY `index_app7020_tooltips_idContent` (`idContent`),
  KEY `index_app7020_tooltips_idExpert` (`idExpert`),
  CONSTRAINT `app7020_tooltipsExpert_ibfk_1` FOREIGN KEY (`idExpert`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `app7020_tooltips_ibfk_1` FOREIGN KEY (`idContent`) REFERENCES `app7020_content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_tooltips`
--

LOCK TABLES `app7020_tooltips` WRITE;
/*!40000 ALTER TABLE `app7020_tooltips` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_tooltips` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_topic_content`
--

DROP TABLE IF EXISTS `app7020_topic_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_topic_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idTopic` int(11) NOT NULL,
  `idContent` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_topic` (`idTopic`,`idContent`),
  KEY `idTopic` (`idTopic`),
  KEY `idContent` (`idContent`),
  CONSTRAINT `app7020_topic_content_ibfk_1` FOREIGN KEY (`idTopic`) REFERENCES `app7020_topic_tree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `app7020_topic_content_ibfk_2` FOREIGN KEY (`idContent`) REFERENCES `app7020_content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_topic_content`
--

LOCK TABLES `app7020_topic_content` WRITE;
/*!40000 ALTER TABLE `app7020_topic_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_topic_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_topic_expert`
--

DROP TABLE IF EXISTS `app7020_topic_expert`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_topic_expert` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idTopic` int(11) NOT NULL,
  `idExpert` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_expert` (`idTopic`,`idExpert`),
  KEY `idTopic` (`idTopic`),
  KEY `idExpert` (`idExpert`),
  CONSTRAINT `app7020_topic_expert_ibfk_1` FOREIGN KEY (`idTopic`) REFERENCES `app7020_topic_tree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `app7020_topic_expert_ibfk_2` FOREIGN KEY (`idExpert`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_topic_expert`
--

LOCK TABLES `app7020_topic_expert` WRITE;
/*!40000 ALTER TABLE `app7020_topic_expert` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_topic_expert` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_topic_translation`
--

DROP TABLE IF EXISTS `app7020_topic_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_topic_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idTopic` int(11) NOT NULL,
  `language` varchar(32) NOT NULL,
  `text` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_topic` (`idTopic`),
  CONSTRAINT `app7020_topic_translation_ibfk_1` FOREIGN KEY (`idTopic`) REFERENCES `app7020_topic_tree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_topic_translation`
--

LOCK TABLES `app7020_topic_translation` WRITE;
/*!40000 ALTER TABLE `app7020_topic_translation` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_topic_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_topic_tree`
--

DROP TABLE IF EXISTS `app7020_topic_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_topic_tree` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iLeft` int(11) NOT NULL,
  `iRight` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `subtree` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_topic_tree`
--

LOCK TABLES `app7020_topic_tree` WRITE;
/*!40000 ALTER TABLE `app7020_topic_tree` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_topic_tree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_topic_visibility`
--

DROP TABLE IF EXISTS `app7020_topic_visibility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_topic_visibility` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idTopic` int(11) NOT NULL,
  `idObject` int(11) NOT NULL,
  `objectType` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `app7020_topic_visibility_idTopic_fk_1` (`idTopic`),
  CONSTRAINT `app7020_topic_visibility_idTopic_fk_1` FOREIGN KEY (`idTopic`) REFERENCES `app7020_topic_tree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_topic_visibility`
--

LOCK TABLES `app7020_topic_visibility` WRITE;
/*!40000 ALTER TABLE `app7020_topic_visibility` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_topic_visibility` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app7020_topics_lo`
--

DROP TABLE IF EXISTS `app7020_topics_lo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app7020_topics_lo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idTopic` int(11) NOT NULL,
  `idLo` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index2` (`idTopic`,`idLo`),
  KEY `app7020_topics_lo_fk_2` (`idLo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app7020_topics_lo`
--

LOCK TABLES `app7020_topics_lo` WRITE;
/*!40000 ALTER TABLE `app7020_topics_lo` DISABLE KEYS */;
/*!40000 ALTER TABLE `app7020_topics_lo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `audit_trail_log`
--

DROP TABLE IF EXISTS `audit_trail_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_trail_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `action` varchar(255) NOT NULL,
  `target_course` int(11) DEFAULT '0',
  `id_target` varchar(255) DEFAULT '0',
  `target_user` int(11) DEFAULT '0',
  `json_data` text,
  `ip` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audit_trail_log`
--

LOCK TABLES `audit_trail_log` WRITE;
/*!40000 ALTER TABLE `audit_trail_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `audit_trail_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conference_adobeconnect_meeting`
--

DROP TABLE IF EXISTS `conference_adobeconnect_meeting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conference_adobeconnect_meeting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_room` int(11) NOT NULL COMMENT 'Id of the LMS room (local)',
  `folder_id` varchar(32) NOT NULL COMMENT 'Adobe Connect folder id',
  `created_by` varchar(128) NOT NULL COMMENT 'Adobe Connect user created this meeting',
  `server_url` varchar(512) NOT NULL COMMENT 'Adobe connect server url',
  `url_path` varchar(128) NOT NULL,
  `sco_id` varchar(32) NOT NULL COMMENT 'Adobe Connect''s meeting id',
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_room` (`id_room`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conference_adobeconnect_meeting`
--

LOCK TABLES `conference_adobeconnect_meeting` WRITE;
/*!40000 ALTER TABLE `conference_adobeconnect_meeting` DISABLE KEYS */;
/*!40000 ALTER TABLE `conference_adobeconnect_meeting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conference_adobeconnect_user`
--

DROP TABLE IF EXISTS `conference_adobeconnect_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conference_adobeconnect_user` (
  `id_user` int(10) unsigned NOT NULL,
  `email` varchar(255) NOT NULL,
  `pwd` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `display_uid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conference_adobeconnect_user`
--

LOCK TABLES `conference_adobeconnect_user` WRITE;
/*!40000 ALTER TABLE `conference_adobeconnect_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `conference_adobeconnect_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conference_bigbluebutton_meeting`
--

DROP TABLE IF EXISTS `conference_bigbluebutton_meeting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conference_bigbluebutton_meeting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_room` int(11) NOT NULL COMMENT 'Id of the LMS room (local)',
  `name` varchar(255) NOT NULL,
  `meeting_id` varchar(32) NOT NULL COMMENT 'BBB meeting Id',
  `attendee_pw` varchar(64) NOT NULL,
  `moderator_pw` varchar(64) NOT NULL,
  `logout_url` varchar(512) DEFAULT '',
  `duration` int(11) DEFAULT '0' COMMENT 'Duration in minutes',
  PRIMARY KEY (`id`),
  KEY `id_room` (`id_room`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conference_bigbluebutton_meeting`
--

LOCK TABLES `conference_bigbluebutton_meeting` WRITE;
/*!40000 ALTER TABLE `conference_bigbluebutton_meeting` DISABLE KEYS */;
/*!40000 ALTER TABLE `conference_bigbluebutton_meeting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conference_gotomeeting_meeting`
--

DROP TABLE IF EXISTS `conference_gotomeeting_meeting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conference_gotomeeting_meeting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_room` int(11) NOT NULL COMMENT 'Id of the LMS room (local)',
  `name` varchar(255) NOT NULL,
  `meeting_id` varchar(64) NOT NULL COMMENT 'The ID of the meeting at Gotomeeting server',
  `unique_meeting_id` varchar(64) NOT NULL COMMENT 'Another ID returned from G2M on creation',
  `join_url` varchar(512) NOT NULL,
  `conference_call_info` text NOT NULL,
  `logout_url` varchar(512) DEFAULT '',
  `max_participants` int(11) DEFAULT '-1',
  PRIMARY KEY (`id`),
  KEY `id_room` (`id_room`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conference_gotomeeting_meeting`
--

LOCK TABLES `conference_gotomeeting_meeting` WRITE;
/*!40000 ALTER TABLE `conference_gotomeeting_meeting` DISABLE KEYS */;
/*!40000 ALTER TABLE `conference_gotomeeting_meeting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conference_meetecho_meeting`
--

DROP TABLE IF EXISTS `conference_meetecho_meeting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conference_meetecho_meeting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_room` int(11) NOT NULL COMMENT 'Id of the LMS room (local)',
  `room_code` varchar(255) NOT NULL DEFAULT '' COMMENT 'Meetecho room code',
  `conference_mode` varchar(255) DEFAULT 'flash' COMMENT 'flash,html5',
  PRIMARY KEY (`id`),
  KEY `id_room` (`id_room`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conference_meetecho_meeting`
--

LOCK TABLES `conference_meetecho_meeting` WRITE;
/*!40000 ALTER TABLE `conference_meetecho_meeting` DISABLE KEYS */;
/*!40000 ALTER TABLE `conference_meetecho_meeting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conference_onsync_meeting`
--

DROP TABLE IF EXISTS `conference_onsync_meeting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conference_onsync_meeting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_room` int(11) NOT NULL COMMENT 'Id of the LMS room (local)',
  `name` varchar(255) NOT NULL,
  `session_id` bigint(20) unsigned NOT NULL COMMENT 'OnSync session id',
  `friendly_id` varchar(32) NOT NULL COMMENT 'Friendly string at the end of session URL',
  `logout_url` varchar(512) DEFAULT '',
  `max_participants` int(11) DEFAULT '-1',
  PRIMARY KEY (`id`),
  KEY `id_room` (`id_room`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conference_onsync_meeting`
--

LOCK TABLES `conference_onsync_meeting` WRITE;
/*!40000 ALTER TABLE `conference_onsync_meeting` DISABLE KEYS */;
/*!40000 ALTER TABLE `conference_onsync_meeting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conference_room`
--

DROP TABLE IF EXISTS `conference_room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conference_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCal` bigint(20) NOT NULL DEFAULT '0',
  `idCourse` int(11) NOT NULL DEFAULT '0',
  `idSt` int(11) NOT NULL DEFAULT '0',
  `accountId` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `room_type` varchar(255) DEFAULT NULL,
  `starttime` bigint(20) DEFAULT NULL,
  `endtime` bigint(20) DEFAULT NULL,
  `meetinghours` int(11) DEFAULT NULL,
  `meetingminutes` int(11) DEFAULT NULL,
  `maxparticipants` int(11) DEFAULT NULL,
  `bookable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_history` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idCourse` (`idCourse`),
  KEY `idSt` (`idSt`),
  CONSTRAINT `conference_room_ibfk_1` FOREIGN KEY (`idCourse`) REFERENCES `learning_course` (`idCourse`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `conference_room_ibfk_2` FOREIGN KEY (`idSt`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conference_room`
--

LOCK TABLES `conference_room` WRITE;
/*!40000 ALTER TABLE `conference_room` DISABLE KEYS */;
/*!40000 ALTER TABLE `conference_room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conference_teleskill_meeting`
--

DROP TABLE IF EXISTS `conference_teleskill_meeting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conference_teleskill_meeting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_room` int(11) NOT NULL COMMENT 'Id of the LMS room (local)',
  `meeting_id` bigint(20) unsigned NOT NULL COMMENT 'Teleskill room id',
  PRIMARY KEY (`id`),
  KEY `id_room` (`id_room`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conference_teleskill_meeting`
--

LOCK TABLES `conference_teleskill_meeting` WRITE;
/*!40000 ALTER TABLE `conference_teleskill_meeting` DISABLE KEYS */;
/*!40000 ALTER TABLE `conference_teleskill_meeting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conference_webex_meeting`
--

DROP TABLE IF EXISTS `conference_webex_meeting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conference_webex_meeting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_room` int(11) NOT NULL COMMENT 'Id of the LMS room (local)',
  `type` varchar(10) NOT NULL DEFAULT 'meeting' COMMENT 'meeting | event',
  `session_id` bigint(20) unsigned NOT NULL COMMENT 'WebEx session id',
  `host_calendar_url` varchar(512) DEFAULT NULL,
  `attendee_calendar_url` varchar(512) DEFAULT NULL,
  `guest_token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_room` (`id_room`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conference_webex_meeting`
--

LOCK TABLES `conference_webex_meeting` WRITE;
/*!40000 ALTER TABLE `conference_webex_meeting` DISABLE KEYS */;
/*!40000 ALTER TABLE `conference_webex_meeting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_admin_course`
--

DROP TABLE IF EXISTS `core_admin_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_admin_course` (
  `idst_user` int(11) NOT NULL DEFAULT '0',
  `type_of_entry` varchar(50) NOT NULL DEFAULT '',
  `id_entry` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idst_user`,`type_of_entry`,`id_entry`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_admin_course`
--

LOCK TABLES `core_admin_course` WRITE;
/*!40000 ALTER TABLE `core_admin_course` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_admin_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_admin_tree`
--

DROP TABLE IF EXISTS `core_admin_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_admin_tree` (
  `idst` varchar(11) NOT NULL DEFAULT '',
  `idstAdmin` varchar(11) NOT NULL DEFAULT '',
  PRIMARY KEY (`idst`,`idstAdmin`),
  KEY `idx_idst` (`idst`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_admin_tree`
--

LOCK TABLES `core_admin_tree` WRITE;
/*!40000 ALTER TABLE `core_admin_tree` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_admin_tree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_asset`
--

DROP TABLE IF EXISTS `core_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(512) NOT NULL COMMENT 'Name and extension, no paths',
  `original_filename` varchar(512) NOT NULL,
  `type` varchar(32) NOT NULL COMMENT 'General purpose or role of the file, like courselogo, playerbg, userfile',
  `scope` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=241 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_asset`
--

LOCK TABLES `core_asset` WRITE;
/*!40000 ALTER TABLE `core_asset` DISABLE KEYS */;
INSERT INTO `core_asset` VALUES (1,'05e9c268f4ee6d1f05e9b57001e04993.jpg','05e9c268f4ee6d1f05e9b57001e04993.jpg','courselogo',1),(2,'074ff9e102e8c71cbea541c55f48d71e.jpg','074ff9e102e8c71cbea541c55f48d71e.jpg','courselogo',1),(3,'0acfa0d2d5e03e63ebdb6d6c93c09ae0.jpg','0acfa0d2d5e03e63ebdb6d6c93c09ae0.jpg','courselogo',1),(4,'0c0cb39c7ab7eee05acc6fb46c4e90d4.jpg','0c0cb39c7ab7eee05acc6fb46c4e90d4.jpg','courselogo',1),(5,'0fc601937ad87d7bd71e2b2b67670d78.jpg','0fc601937ad87d7bd71e2b2b67670d78.jpg','courselogo',1),(6,'14a219c63c5fa7b18072e95857d43f60.jpg','14a219c63c5fa7b18072e95857d43f60.jpg','courselogo',1),(7,'15246ff1e481ab208123a5fe27d4e908.jpg','15246ff1e481ab208123a5fe27d4e908.jpg','courselogo',1),(8,'18ae54160a5b66484783d9b60225c554.jpg','18ae54160a5b66484783d9b60225c554.jpg','courselogo',1),(9,'1e6a243eb8d960cfca37b57fabf202f6.jpg','1e6a243eb8d960cfca37b57fabf202f6.jpg','courselogo',1),(10,'2f063671dc13fe79aa5f299a79e8dd54.jpg','2f063671dc13fe79aa5f299a79e8dd54.jpg','courselogo',1),(11,'2fd35098b2185165017500ba4c92aa7a.jpg','2fd35098b2185165017500ba4c92aa7a.jpg','courselogo',1),(12,'331eb16d0a56028d01a1c10d4763fe29.jpg','331eb16d0a56028d01a1c10d4763fe29.jpg','courselogo',1),(13,'36c3e5d154051413a73356470b870715.jpg','36c3e5d154051413a73356470b870715.jpg','courselogo',1),(14,'3e2a0bac0c67e045afcfe239aeda4b1c.jpg','3e2a0bac0c67e045afcfe239aeda4b1c.jpg','courselogo',1),(15,'3fcd2fc3fda3fc6c8b4dd303e1679de9.jpg','3fcd2fc3fda3fc6c8b4dd303e1679de9.jpg','courselogo',1),(16,'424263a13d1f43ed4909f2de846e29ae.jpg','424263a13d1f43ed4909f2de846e29ae.jpg','courselogo',1),(17,'464e35a1c5c09cbf8209fccdbf7357a9.jpg','464e35a1c5c09cbf8209fccdbf7357a9.jpg','courselogo',1),(18,'4a6743f3c907833217d881b4f312ec3a.jpg','4a6743f3c907833217d881b4f312ec3a.jpg','courselogo',1),(19,'51c1069a5e8a8ead77992bdba5c853a2.jpg','51c1069a5e8a8ead77992bdba5c853a2.jpg','courselogo',1),(20,'58d0ec1cc3be5b3d1ea819b0af6b1fd2.jpg','58d0ec1cc3be5b3d1ea819b0af6b1fd2.jpg','courselogo',1),(21,'642126007b5a714f198386c06714e713.jpg','642126007b5a714f198386c06714e713.jpg','courselogo',1),(22,'64e140dc634c5a57f8dd0c3916d3a5b0.jpg','64e140dc634c5a57f8dd0c3916d3a5b0.jpg','courselogo',1),(23,'678333db633dd77ce0662a095106b345.jpg','678333db633dd77ce0662a095106b345.jpg','courselogo',1),(24,'6a2556ba0ac6c697ac9c79dfcc213abb.jpg','6a2556ba0ac6c697ac9c79dfcc213abb.jpg','courselogo',1),(25,'72e878a920d4811776e0cb99bebea5cd.jpg','72e878a920d4811776e0cb99bebea5cd.jpg','courselogo',1),(26,'73212a0b686605f493b0196071728532.jpg','73212a0b686605f493b0196071728532.jpg','courselogo',1),(27,'766cb11e06f086f4b1bcd1e100b0f7b6.jpg','766cb11e06f086f4b1bcd1e100b0f7b6.jpg','courselogo',1),(28,'7f06697c2ab068d667e003f1ec4f7c81.jpg','7f06697c2ab068d667e003f1ec4f7c81.jpg','courselogo',1),(29,'81d165c2499ff0c849d80185cfabc5d0.jpg','81d165c2499ff0c849d80185cfabc5d0.jpg','courselogo',1),(30,'8660c1c750db9614745ae7642c22f052.jpg','8660c1c750db9614745ae7642c22f052.jpg','courselogo',1),(31,'89c19ddfcb271e265fba4421051cfe5e.jpg','89c19ddfcb271e265fba4421051cfe5e.jpg','courselogo',1),(32,'8d84442574131f5f70d0f1138116382d.jpg','8d84442574131f5f70d0f1138116382d.jpg','courselogo',1),(33,'94ae8b6a7d39859424bfc809ddd33220.jpg','94ae8b6a7d39859424bfc809ddd33220.jpg','courselogo',1),(34,'9d835f873fa2cd87e1c16ecc3f80b6cb.jpg','9d835f873fa2cd87e1c16ecc3f80b6cb.jpg','courselogo',1),(35,'9dc7f4e52aefb11eba38503d3cee7561.jpg','9dc7f4e52aefb11eba38503d3cee7561.jpg','courselogo',1),(36,'a930e369f344226308c926548e751d2e.jpg','a930e369f344226308c926548e751d2e.jpg','courselogo',1),(37,'a9c5385866ec28def77fee10a4887e03.jpg','a9c5385866ec28def77fee10a4887e03.jpg','courselogo',1),(38,'ac54f11aa0716a86c6d5ea86bcd550fe.jpg','ac54f11aa0716a86c6d5ea86bcd550fe.jpg','courselogo',1),(39,'aefab7ab501a8bd0e9eadadbca7c7045.jpg','aefab7ab501a8bd0e9eadadbca7c7045.jpg','courselogo',1),(40,'b8a2e5ca9d8be85f88621cb9e4dbc84c.jpg','b8a2e5ca9d8be85f88621cb9e4dbc84c.jpg','courselogo',1),(41,'bb291d5cd4aa6feaebae71514454c073.jpg','bb291d5cd4aa6feaebae71514454c073.jpg','courselogo',1),(42,'c31366f5515ea06c01440e025c51c1e9.jpg','c31366f5515ea06c01440e025c51c1e9.jpg','courselogo',1),(43,'c90b17828f34d0fb2662e749198e5cab.jpg','c90b17828f34d0fb2662e749198e5cab.jpg','courselogo',1),(44,'ca5bf3959413c2dd3f91d0a94ab1f792.jpg','ca5bf3959413c2dd3f91d0a94ab1f792.jpg','courselogo',1),(45,'ca9aebe0e3b85d26c1945be54d53465b.jpg','ca9aebe0e3b85d26c1945be54d53465b.jpg','courselogo',1),(46,'d45fc9d747c1ba5827bb0202c5cdd70d.jpg','d45fc9d747c1ba5827bb0202c5cdd70d.jpg','courselogo',1),(47,'d4d650725ee5600f7cd204138262226f.jpg','d4d650725ee5600f7cd204138262226f.jpg','courselogo',1),(48,'d5a5a0dd9fc92bffc16fb36376ddb8bc.jpg','d5a5a0dd9fc92bffc16fb36376ddb8bc.jpg','courselogo',1),(49,'d99f4707c3d47b4e03aeaf7a471d566d.jpg','d99f4707c3d47b4e03aeaf7a471d566d.jpg','courselogo',1),(50,'db02af3b0b1d488ef2206c9812e06eb4.jpg','db02af3b0b1d488ef2206c9812e06eb4.jpg','courselogo',1),(51,'db9023ba8d09b0073332b1e2b5602e56.jpg','db9023ba8d09b0073332b1e2b5602e56.jpg','courselogo',1),(52,'e517621fe187034c9a3736293d8c8c44.jpg','e517621fe187034c9a3736293d8c8c44.jpg','courselogo',1),(53,'eae0bae35b8c9dd17a4f0a5c6a353396.jpg','eae0bae35b8c9dd17a4f0a5c6a353396.jpg','courselogo',1),(54,'f08994bb10df0d6eff7d002cad4d6537.jpg','f08994bb10df0d6eff7d002cad4d6537.jpg','courselogo',1),(55,'f41b5d67c39e844caf2b20a72b8758f7.jpg','f41b5d67c39e844caf2b20a72b8758f7.jpg','courselogo',1),(56,'f5e6b58c1c036a69407c22216766e551.jpg','f5e6b58c1c036a69407c22216766e551.jpg','courselogo',1),(57,'f67425b1cff74faf8f3d40cead7d8309.jpg','f67425b1cff74faf8f3d40cead7d8309.jpg','courselogo',1),(58,'f6cbce4aa241fae87964fde51deaf697.jpg','f6cbce4aa241fae87964fde51deaf697.jpg','courselogo',1),(59,'f7498f00b34cd9894cfc93e72a5da8b3.jpg','f7498f00b34cd9894cfc93e72a5da8b3.jpg','courselogo',1),(60,'f7f19f3e7cac1c177947505d1aef1955.jpg','f7f19f3e7cac1c177947505d1aef1955.jpg','courselogo',1),(61,'f92f45934d1afbc230cc499050517313.jpg','f92f45934d1afbc230cc499050517313.jpg','courselogo',1),(62,'fe915f121014f8d61d4005422ae1a340.jpg','fe915f121014f8d61d4005422ae1a340.jpg','courselogo',1),(63,'00c8cee59cdc6324505af3f234809626.jpg','00c8cee59cdc6324505af3f234809626.jpg','loginbg',1),(64,'02f69e7828d41864ceaea3b63ddb2e2d.jpg','02f69e7828d41864ceaea3b63ddb2e2d.jpg','loginbg',1),(65,'1874afcd4fcb4594dcdfd02ddc21ccec.jpg','1874afcd4fcb4594dcdfd02ddc21ccec.jpg','loginbg',1),(66,'19893f3387b1e20b143db85df1f91cb7.jpg','19893f3387b1e20b143db85df1f91cb7.jpg','loginbg',1),(67,'1c689da5a36a6c12294e8be995d2a209.jpg','1c689da5a36a6c12294e8be995d2a209.jpg','loginbg',1),(68,'1c9731c85bfcec443f0cfc4070f2f575.jpg','1c9731c85bfcec443f0cfc4070f2f575.jpg','loginbg',1),(69,'1db4a1f67f3e498edbd18f0da981eea4.jpg','1db4a1f67f3e498edbd18f0da981eea4.jpg','loginbg',1),(70,'1e2c110ff351b3301642524bf6e2f583.jpg','1e2c110ff351b3301642524bf6e2f583.jpg','loginbg',1),(71,'1ef56104f8b3e6593a7eba406fef1cba.jpg','1ef56104f8b3e6593a7eba406fef1cba.jpg','loginbg',1),(72,'21be4f80bc91a3505546e10c8006b6aa.jpg','21be4f80bc91a3505546e10c8006b6aa.jpg','loginbg',1),(73,'2602192a925414ae7b6d9f0019522b83.jpg','2602192a925414ae7b6d9f0019522b83.jpg','loginbg',1),(74,'3090e43d289b41afc1cbe00472e50891.jpg','3090e43d289b41afc1cbe00472e50891.jpg','loginbg',1),(75,'3fd850e769589d0b047f76c8fa5847f2.jpg','3fd850e769589d0b047f76c8fa5847f2.jpg','loginbg',1),(76,'4ac0b70e7d8c2cc8cbfde1041c8e76f6.jpg','4ac0b70e7d8c2cc8cbfde1041c8e76f6.jpg','loginbg',1),(77,'4b6c6ceee17655897a92739ec7457e4c.jpg','4b6c6ceee17655897a92739ec7457e4c.jpg','loginbg',1),(78,'52a04982f97c1f71d08570f2ea1b5688.jpg','52a04982f97c1f71d08570f2ea1b5688.jpg','loginbg',1),(79,'661ce294292ebeebabf32a0363ca4289.jpg','661ce294292ebeebabf32a0363ca4289.jpg','loginbg',1),(80,'68d41baf50b31e45301711838c331832.jpg','68d41baf50b31e45301711838c331832.jpg','loginbg',1),(81,'6e172c7611d2b1a66790e49177e226f1.jpg','6e172c7611d2b1a66790e49177e226f1.jpg','loginbg',1),(82,'6f85027a5e6cd5fa1e258c00f7940251.jpg','6f85027a5e6cd5fa1e258c00f7940251.jpg','loginbg',1),(83,'73e851fb6885354053be6498936b7b3d.jpg','73e851fb6885354053be6498936b7b3d.jpg','loginbg',1),(84,'759a7c336d56e8f58558e3fe8fb4c37a.jpg','759a7c336d56e8f58558e3fe8fb4c37a.jpg','loginbg',1),(85,'7691749c30964167d548ce75f9ef93a2.jpg','7691749c30964167d548ce75f9ef93a2.jpg','loginbg',1),(86,'76f47e8e6ec6e71e4a43744d72c38109.jpg','76f47e8e6ec6e71e4a43744d72c38109.jpg','loginbg',1),(87,'7a82bf6d3fa6ba819da4286f576daa14.jpg','7a82bf6d3fa6ba819da4286f576daa14.jpg','loginbg',1),(88,'7a8b9dc0848b9fa18d8eee3dc2737501.jpg','7a8b9dc0848b9fa18d8eee3dc2737501.jpg','loginbg',1),(89,'7ae86d7e57ff4585f82471249d7f5894.jpg','7ae86d7e57ff4585f82471249d7f5894.jpg','loginbg',1),(90,'7cbce9936ce2f0b15d63b9b998ac0712.png','7cbce9936ce2f0b15d63b9b998ac0712.png','loginbg',1),(91,'8062335a0dffe90f097a449099f314b1.jpg','8062335a0dffe90f097a449099f314b1.jpg','loginbg',1),(92,'82d58c8cf3b110ee60b519b37c9b02e4.jpg','82d58c8cf3b110ee60b519b37c9b02e4.jpg','loginbg',1),(93,'8d13fa93605b4a0ab8a0c1f2bef1707c.jpg','8d13fa93605b4a0ab8a0c1f2bef1707c.jpg','loginbg',1),(94,'908264a30759ceb1675e17ea8ee9273c.jpg','908264a30759ceb1675e17ea8ee9273c.jpg','loginbg',1),(95,'9135a57226e8d7764a4babace30bd2ed.jpg','9135a57226e8d7764a4babace30bd2ed.jpg','loginbg',1),(96,'9143a1e7a50d57d20f80cbcf28464152.jpg','9143a1e7a50d57d20f80cbcf28464152.jpg','loginbg',1),(97,'927638b6b517eb47544af4f03e9093a5.jpg','927638b6b517eb47544af4f03e9093a5.jpg','loginbg',1),(98,'9ccc98b89af21b528db1b1c627b14ed9.jpg','9ccc98b89af21b528db1b1c627b14ed9.jpg','loginbg',1),(99,'a2ba36351e06991d8853ca1c869014b2.jpg','a2ba36351e06991d8853ca1c869014b2.jpg','loginbg',1),(100,'a8a2ae95ba40a430a45cc62bd4475929.jpg','a8a2ae95ba40a430a45cc62bd4475929.jpg','loginbg',1),(101,'a8c670d48a3001c98bed3374be7a851c.jpg','a8c670d48a3001c98bed3374be7a851c.jpg','loginbg',1),(102,'ac6375362fcb74250b92fde940f954b1.jpg','ac6375362fcb74250b92fde940f954b1.jpg','loginbg',1),(103,'b0768a93a67687225afee60202e954c8.jpg','b0768a93a67687225afee60202e954c8.jpg','loginbg',1),(104,'b5bb0dcfa5934ca6b1574f00b30bf314.jpg','b5bb0dcfa5934ca6b1574f00b30bf314.jpg','loginbg',1),(105,'b871997aabcb204dfcd5fad698e846d5.jpg','b871997aabcb204dfcd5fad698e846d5.jpg','loginbg',1),(106,'bb2790e81eae02c9ee224e424627fa69.jpg','bb2790e81eae02c9ee224e424627fa69.jpg','loginbg',1),(107,'bfc55b53a3a6c07ba5895f25db42be7e.jpg','bfc55b53a3a6c07ba5895f25db42be7e.jpg','loginbg',1),(108,'c6d93bd662241125d61ade0550468e85.jpg','c6d93bd662241125d61ade0550468e85.jpg','loginbg',1),(109,'c9710ee28f009ddb7ac21aad373b2f45.jpg','c9710ee28f009ddb7ac21aad373b2f45.jpg','loginbg',1),(110,'d09bb4980198295d654401017262eced.jpg','d09bb4980198295d654401017262eced.jpg','loginbg',1),(111,'d33d439f0aea0448008795fdbdee72e4.jpg','d33d439f0aea0448008795fdbdee72e4.jpg','loginbg',1),(112,'d5d87f951115011a7d3e87207c583ee0.jpg','d5d87f951115011a7d3e87207c583ee0.jpg','loginbg',1),(113,'d95c0379804c7dc193433ba5a12d72fd.jpg','d95c0379804c7dc193433ba5a12d72fd.jpg','loginbg',1),(114,'ead0982519a9ee1c7c4bc1d7bfa43fe9.jpg','ead0982519a9ee1c7c4bc1d7bfa43fe9.jpg','loginbg',1),(115,'ed43bba17bcc03155a0b21b3ec1b6642.jpg','ed43bba17bcc03155a0b21b3ec1b6642.jpg','loginbg',1),(116,'eded018fbacb82ee02e88677ef953794.jpg','eded018fbacb82ee02e88677ef953794.jpg','loginbg',1),(117,'eea4ec554689b93a2bedf137107deaf3.jpg','eea4ec554689b93a2bedf137107deaf3.jpg','loginbg',1),(118,'f64518915e73268f9b3597c3e830ab79.jpg','f64518915e73268f9b3597c3e830ab79.jpg','loginbg',1),(119,'0046ceec622f815397930dce6beec284.jpg','0046ceec622f815397930dce6beec284.jpg','playerbg',1),(120,'0fb172d4bcfaa4857d75cd4f6c65600c.jpg','0fb172d4bcfaa4857d75cd4f6c65600c.jpg','playerbg',1),(121,'153f2a213767cc394109a3d4fbba86a4.jpg','153f2a213767cc394109a3d4fbba86a4.jpg','playerbg',1),(122,'162bd5bee679f8c1cb794d667eb7b94b.jpg','162bd5bee679f8c1cb794d667eb7b94b.jpg','playerbg',1),(123,'1808e5b241a64333392c7abe0da38238.jpg','1808e5b241a64333392c7abe0da38238.jpg','playerbg',1),(124,'1fd2df9b6f0cf6c9286e638ec5d345f4.jpg','1fd2df9b6f0cf6c9286e638ec5d345f4.jpg','playerbg',1),(125,'2205bf0637a78a7a49fe5eb3edee41a2.jpg','2205bf0637a78a7a49fe5eb3edee41a2.jpg','playerbg',1),(126,'22ba040087fc20f66a2b5f50aec99dbf.jpg','22ba040087fc20f66a2b5f50aec99dbf.jpg','playerbg',1),(127,'2b400510040a89a75850bb3a578ae243.jpg','2b400510040a89a75850bb3a578ae243.jpg','playerbg',1),(128,'2cddeafda7554a9cdde1a723e540edd0.jpg','2cddeafda7554a9cdde1a723e540edd0.jpg','playerbg',1),(129,'31e8dde8fdab908410f2cbe5c373186d.jpg','31e8dde8fdab908410f2cbe5c373186d.jpg','playerbg',1),(130,'37db3d46aed3a62e7ed2a1c2e5829e72.jpg','37db3d46aed3a62e7ed2a1c2e5829e72.jpg','playerbg',1),(131,'38d1aa8d62d2eddac8d09961048030ef.jpg','38d1aa8d62d2eddac8d09961048030ef.jpg','playerbg',1),(132,'391b1a04c880b04cae2bdbb2daebf878.jpg','391b1a04c880b04cae2bdbb2daebf878.jpg','playerbg',1),(133,'3aa5d392e8960830e5c8679295f466c6.jpg','3aa5d392e8960830e5c8679295f466c6.jpg','playerbg',1),(134,'43ec8466eda39e24de19c6f831422bec.jpg','43ec8466eda39e24de19c6f831422bec.jpg','playerbg',1),(135,'482f8360814b58cb449a6e73f8e11b12.jpg','482f8360814b58cb449a6e73f8e11b12.jpg','playerbg',1),(136,'4a1b157fc384586c6ec3d39c1cd11743.jpg','4a1b157fc384586c6ec3d39c1cd11743.jpg','playerbg',1),(137,'5153b5ba44dfa70360211407319333f2.jpg','5153b5ba44dfa70360211407319333f2.jpg','playerbg',1),(138,'6568dcdd4c53dd45985fc4559e3a7e35.jpg','6568dcdd4c53dd45985fc4559e3a7e35.jpg','playerbg',1),(139,'69def9cc26193dd51acb12db49cc59e0.jpg','69def9cc26193dd51acb12db49cc59e0.jpg','playerbg',1),(140,'6ab8b83bcf99a7b9f4ca4adaaf65efff.jpg','6ab8b83bcf99a7b9f4ca4adaaf65efff.jpg','playerbg',1),(141,'6f931fe7558725aa8ecf61e05cb63bef.jpg','6f931fe7558725aa8ecf61e05cb63bef.jpg','playerbg',1),(142,'72021bb6f489a89b1db767302cd6ed5c.jpg','72021bb6f489a89b1db767302cd6ed5c.jpg','playerbg',1),(143,'73045741b3850dc279d0b4c2b2b8f5c7.jpg','73045741b3850dc279d0b4c2b2b8f5c7.jpg','playerbg',1),(144,'89129a012b9d37b23fc9ad7c43b8979a.jpg','89129a012b9d37b23fc9ad7c43b8979a.jpg','playerbg',1),(145,'8d50d2eab6d14454506879cb1716eaae.jpg','8d50d2eab6d14454506879cb1716eaae.jpg','playerbg',1),(146,'8ddd1770b153048d78a99fa4d3132c5b.jpg','8ddd1770b153048d78a99fa4d3132c5b.jpg','playerbg',1),(147,'905a309b9dafd6fc36666bfcb3d98a4c.jpg','905a309b9dafd6fc36666bfcb3d98a4c.jpg','playerbg',1),(148,'919bbe33b3cb26273abee9d09f9e174e.jpg','919bbe33b3cb26273abee9d09f9e174e.jpg','playerbg',1),(149,'943201765912f9556f3df2e2fe8e0128.jpg','943201765912f9556f3df2e2fe8e0128.jpg','playerbg',1),(150,'9649d99a33ea60fb0ef1210b30c07b57.jpg','9649d99a33ea60fb0ef1210b30c07b57.jpg','playerbg',1),(151,'98ceef5ce2cefabebd3fddc36929927b.jpg','98ceef5ce2cefabebd3fddc36929927b.jpg','playerbg',1),(152,'9b86f545cfcff9d5d51af4b6fbb8a832.jpg','9b86f545cfcff9d5d51af4b6fbb8a832.jpg','playerbg',1),(153,'9c0908cdf69a28afa2260bc1e44718bf.jpg','9c0908cdf69a28afa2260bc1e44718bf.jpg','playerbg',1),(154,'a0823587e2b6cd3634b672326cd23dbf.jpg','a0823587e2b6cd3634b672326cd23dbf.jpg','playerbg',1),(155,'a1d191785780ee927c399748355e6b5b.jpg','a1d191785780ee927c399748355e6b5b.jpg','playerbg',1),(156,'a9d1fdd9784f4d0a3d0a232ecbde4e9e.jpg','a9d1fdd9784f4d0a3d0a232ecbde4e9e.jpg','playerbg',1),(157,'aa8a6108f62ad740e2f874f016aa819d.jpg','aa8a6108f62ad740e2f874f016aa819d.jpg','playerbg',1),(158,'b0ad1697131b2e1903427da80145720c.jpg','b0ad1697131b2e1903427da80145720c.jpg','playerbg',1),(159,'b5fd1cdc5f461d944214c47067a6e21e.jpg','b5fd1cdc5f461d944214c47067a6e21e.jpg','playerbg',1),(160,'b781de03efcc4ee9c3aef0ca41fd2fa6.jpg','b781de03efcc4ee9c3aef0ca41fd2fa6.jpg','playerbg',1),(161,'b8405d8f36228a31a70450c76c6ba0cf.jpg','b8405d8f36228a31a70450c76c6ba0cf.jpg','playerbg',1),(162,'b98c6a19347293958d10520241653940.jpg','b98c6a19347293958d10520241653940.jpg','playerbg',1),(163,'ba5da25d4f41b0360cd57bab385cbecb.jpg','ba5da25d4f41b0360cd57bab385cbecb.jpg','playerbg',1),(164,'baba16ff4ca9982fda7bd71dc38fecff.jpg','baba16ff4ca9982fda7bd71dc38fecff.jpg','playerbg',1),(165,'bed014e32dc95835ef716e7291c701eb.jpg','bed014e32dc95835ef716e7291c701eb.jpg','playerbg',1),(166,'caf8b5c6fdd5a1412d10232b743610eb.jpg','caf8b5c6fdd5a1412d10232b743610eb.jpg','playerbg',1),(167,'cfcebd243cc5ac600c2f59ef998a2acb.jpg','cfcebd243cc5ac600c2f59ef998a2acb.jpg','playerbg',1),(168,'d20d60658dfa66ee86e5779ea5748623.jpg','d20d60658dfa66ee86e5779ea5748623.jpg','playerbg',1),(169,'df74dbd42b46df076dbb0f130b5b76a8.jpg','df74dbd42b46df076dbb0f130b5b76a8.jpg','playerbg',1),(170,'e3af5d9f22671b347484ba6ec7ea67ed.jpg','e3af5d9f22671b347484ba6ec7ea67ed.jpg','playerbg',1),(171,'e54a7d76b4f06bf4568d8267440ecde4.jpg','e54a7d76b4f06bf4568d8267440ecde4.jpg','playerbg',1),(172,'e66ea5ebd05b0649e189ab76d3caa457.jpg','e66ea5ebd05b0649e189ab76d3caa457.jpg','playerbg',1),(173,'e6a25d0401d00752491bcd2865cf4ff6.jpg','e6a25d0401d00752491bcd2865cf4ff6.jpg','playerbg',1),(174,'eb671a52c616e92a749ce4e40e89a6a1.jpg','eb671a52c616e92a749ce4e40e89a6a1.jpg','playerbg',1),(175,'f0f0413dcb3d7c9ce299e164bd01f504.jpg','f0f0413dcb3d7c9ce299e164bd01f504.jpg','playerbg',1),(176,'f31f7baa90a3c389eb8e810dbc8e106a.jpg','f31f7baa90a3c389eb8e810dbc8e106a.jpg','playerbg',1),(201,'0bdc1237ff6856be762ee4ee291a7efb.png','0bdc1237ff6856be762ee4ee291a7efb.png','badges',1),(202,'0ebfa5da59f27444420733d1293370af.png','0ebfa5da59f27444420733d1293370af.png','badges',1),(203,'1cf8a9360646a2719e62a636ec85ab5f.png','1cf8a9360646a2719e62a636ec85ab5f.png','badges',1),(204,'22a81a0e9c95ca0dcbaeac5b4ca20535.png','22a81a0e9c95ca0dcbaeac5b4ca20535.png','badges',1),(205,'233edce9bb4d62294b5b1471e6fda2e5.png','233edce9bb4d62294b5b1471e6fda2e5.png','badges',1),(206,'265f2d113f3f67f630b88ff30815101d.png','265f2d113f3f67f630b88ff30815101d.png','badges',1),(207,'2b454ec3196b0224570cbfd66230e38b.png','2b454ec3196b0224570cbfd66230e38b.png','badges',1),(208,'2befecbc05c5db26b247d38efceae413.png','2befecbc05c5db26b247d38efceae413.png','badges',1),(209,'31102794fcd5576b59d75f9281b5d389.png','31102794fcd5576b59d75f9281b5d389.png','badges',1),(210,'408766bd3f27252e8f7b6e8e678e407d.png','408766bd3f27252e8f7b6e8e678e407d.png','badges',1),(211,'46eda7ff5bc3059af91a14321011bb75.png','46eda7ff5bc3059af91a14321011bb75.png','badges',1),(212,'481bbaf2f1508b4499e14150b72dab79.png','481bbaf2f1508b4499e14150b72dab79.png','badges',1),(213,'505855d891ecbbf13069156ec8c796bd.png','505855d891ecbbf13069156ec8c796bd.png','badges',1),(214,'50b9b4a8af97b219021b6e63910a7c30.png','50b9b4a8af97b219021b6e63910a7c30.png','badges',1),(215,'514e92c50fa40c00dba316b993d265d9.png','514e92c50fa40c00dba316b993d265d9.png','badges',1),(216,'53be84d88cb07b62610f303d8341cc4e.png','53be84d88cb07b62610f303d8341cc4e.png','badges',1),(217,'5adc88def010c67249714736235ac500.png','5adc88def010c67249714736235ac500.png','badges',1),(218,'66fe16b0364de2c7c60bec0a80d56614.png','66fe16b0364de2c7c60bec0a80d56614.png','badges',1),(219,'6b00f92c40f6aacd722130eec232c694.png','6b00f92c40f6aacd722130eec232c694.png','badges',1),(220,'872e5b1bb1b188992d5c3b1da33486cb.png','872e5b1bb1b188992d5c3b1da33486cb.png','badges',1),(221,'8be2c066d0c438421559c81711c2d840.png','8be2c066d0c438421559c81711c2d840.png','badges',1),(222,'8d7ca561bd63815c443fff540959b237.png','8d7ca561bd63815c443fff540959b237.png','badges',1),(223,'983a3a6cfb97fb3c56012d5361a2f62e.png','983a3a6cfb97fb3c56012d5361a2f62e.png','badges',1),(224,'9d20ab1849c16c60b2eac995d5afd59b.png','9d20ab1849c16c60b2eac995d5afd59b.png','badges',1),(225,'a1e5bac5919792dee01bc981de324abe.png','a1e5bac5919792dee01bc981de324abe.png','badges',1),(226,'b0e2e2f935d9f17f30a257d22ea5b665.png','b0e2e2f935d9f17f30a257d22ea5b665.png','badges',1),(227,'b621b26e14f4a41dd4a24e35eac25608.png','b621b26e14f4a41dd4a24e35eac25608.png','badges',1),(228,'bb9b2395326a4dd2e91bb86031504c84.png','bb9b2395326a4dd2e91bb86031504c84.png','badges',1),(229,'d39e0ccf27f55f271a677e0b9d599284.png','d39e0ccf27f55f271a677e0b9d599284.png','badges',1),(230,'d4bd2b998038e9d904e75e12b671526e.png','d4bd2b998038e9d904e75e12b671526e.png','badges',1),(231,'d94f8ae514f41d7e046c774e1d9fee2a.png','d94f8ae514f41d7e046c774e1d9fee2a.png','badges',1),(232,'dc723de716b98119cf7d5c2fb416f496.png','dc723de716b98119cf7d5c2fb416f496.png','badges',1),(233,'e1a8a63a36fc934c37af509300bf6599.png','e1a8a63a36fc934c37af509300bf6599.png','badges',1),(234,'e817590e5003e3c139a771a9a93251cc.png','e817590e5003e3c139a771a9a93251cc.png','badges',1),(235,'ed4969e71178e4518c0529d8e6943a7c.png','ed4969e71178e4518c0529d8e6943a7c.png','badges',1),(236,'efdca680972281cadebca05579a2f204.png','efdca680972281cadebca05579a2f204.png','badges',1),(237,'f5208c8f9db1f5099d7e9a49a6bdfd80.png','f5208c8f9db1f5099d7e9a49a6bdfd80.png','badges',1),(238,'f62155850a5f9f3ac202ffae74860b1f.png','f62155850a5f9f3ac202ffae74860b1f.png','badges',1),(239,'f744da39cf372d649960c88a86288588.png','f744da39cf372d649960c88a86288588.png','badges',1),(240,'fb4d07061c9ef8f3a4d8b937fbd80b41.png','fb4d07061c9ef8f3a4d8b937fbd80b41.png','badges',1);
/*!40000 ALTER TABLE `core_asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_bug`
--

DROP TABLE IF EXISTS `core_bug`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_bug` (
  `bug_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('bug','feature') DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `file` varchar(255) DEFAULT NULL,
  `project` varchar(40) DEFAULT NULL,
  `status` varchar(8) DEFAULT NULL,
  `url` text,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_bug`
--

LOCK TABLES `core_bug` WRITE;
/*!40000 ALTER TABLE `core_bug` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_bug` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_calendar`
--

DROP TABLE IF EXISTS `core_calendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_calendar` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `class` varchar(30) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `private` varchar(2) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `type` bigint(20) DEFAULT NULL,
  `visibility_rules` tinytext,
  `_owner` int(11) DEFAULT NULL,
  `_day` smallint(2) DEFAULT NULL,
  `_month` smallint(2) DEFAULT NULL,
  `_year` smallint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_calendar`
--

LOCK TABLES `core_calendar` WRITE;
/*!40000 ALTER TABLE `core_calendar` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_calendar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_callout_read_log`
--

DROP TABLE IF EXISTS `core_callout_read_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_callout_read_log` (
  `idst` int(11) NOT NULL,
  `callout_id` bigint(20) NOT NULL,
  PRIMARY KEY (`idst`,`callout_id`),
  CONSTRAINT `FK_idst` FOREIGN KEY (`idst`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_callout_read_log`
--

LOCK TABLES `core_callout_read_log` WRITE;
/*!40000 ALTER TABLE `core_callout_read_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_callout_read_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_country`
--

DROP TABLE IF EXISTS `core_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_country` (
  `id_country` int(11) NOT NULL AUTO_INCREMENT,
  `name_country` varchar(64) NOT NULL DEFAULT '',
  `iso_code_country` varchar(3) NOT NULL DEFAULT '',
  `id_zone` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_country`),
  KEY `IDX_COUNTRIES_NAME` (`name_country`)
) ENGINE=InnoDB AUTO_INCREMENT=246 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_country`
--

LOCK TABLES `core_country` WRITE;
/*!40000 ALTER TABLE `core_country` DISABLE KEYS */;
INSERT INTO `core_country` VALUES (1,'AFGHANISTAN','AF',0),(2,'ALAND ISLANDS','AX',0),(3,'ALBANIA','AL',0),(4,'ALGERIA','DZ',0),(5,'AMERICAN SAMOA','AS',0),(6,'ANDORRA','AD',0),(7,'ANGOLA','AO',0),(8,'ANGUILLA','AI',0),(9,'ANTARCTICA','AQ',0),(10,'ANTIGUA AND BARBUDA','AG',0),(11,'ARGENTINA','AR',0),(12,'ARMENIA','AM',0),(13,'ARUBA','AW',0),(14,'AUSTRALIA','AU',0),(15,'AUSTRIA','AT',0),(16,'AZERBAIJAN','AZ',0),(17,'BAHAMAS','BS',0),(18,'BAHRAIN','BH',0),(19,'BANGLADESH','BD',0),(20,'BARBADOS','BB',0),(21,'BELARUS','BY',0),(22,'BELGIUM','BE',0),(23,'BELIZE','BZ',0),(24,'BENIN','BJ',0),(25,'BERMUDA','BM',0),(26,'BHUTAN','BT',0),(27,'BOLIVIA','BO',0),(28,'BOSNIA AND HERZEGOVINA','BA',0),(29,'BOTSWANA','BW',0),(30,'BOUVET ISLAND','BV',0),(31,'BRAZIL','BR',0),(32,'BRITISH INDIAN OCEAN TERRITORY','IO',0),(33,'BRUNEI DARUSSALAM','BN',0),(34,'BULGARIA','BG',0),(35,'BURKINA FASO','BF',0),(36,'BURUNDI','BI',0),(37,'CAMBODIA','KH',0),(38,'CAMEROON','CM',0),(39,'CANADA','CA',0),(40,'CAPE VERDE','CV',0),(41,'CAYMAN ISLANDS','KY',0),(42,'CENTRAL AFRICAN REPUBLIC','CF',0),(43,'CHAD','TD',0),(44,'CHILE','CL',0),(45,'CHINA','CN',0),(46,'CHRISTMAS ISLAND','CX',0),(47,'COCOS (KEELING) ISLANDS','CC',0),(48,'COLOMBIA','CO',0),(49,'COMOROS','KM',0),(50,'CONGO','CG',0),(51,'CONGO, THE DEMOCRATIC REPUBLIC OF THE','CD',0),(52,'COOK ISLANDS','CK',0),(53,'COSTA RICA','CR',0),(54,'IVORY COAST','CI',0),(55,'CROATIA','HR',0),(56,'CUBA','CU',0),(57,'CYPRUS','CY',0),(58,'CZECH REPUBLIC','CZ',0),(59,'DENMARK','DK',0),(60,'DJIBOUTI','DJ',0),(61,'DOMINICA','DM',0),(62,'DOMINICAN REPUBLIC','DO',0),(63,'ECUADOR','EC',0),(64,'EGYPT','EG',0),(65,'EL SALVADOR','SV',0),(66,'EQUATORIAL GUINEA','GQ',0),(67,'ERITREA','ER',0),(68,'ESTONIA','EE',0),(69,'ETHIOPIA','ET',0),(70,'FALKLAND ISLANDS (MALVINAS)','FK',0),(71,'FAROE ISLANDS','FO',0),(72,'FIJI','FJ',0),(73,'FINLAND','FI',0),(74,'FRANCE','FR',0),(75,'FRENCH GUIANA','GF',0),(76,'FRENCH POLYNESIA','PF',0),(77,'FRENCH SOUTHERN TERRITORIES','TF',0),(78,'GABON','GA',0),(79,'GAMBIA','GM',0),(80,'GEORGIA','GE',0),(81,'GERMANY','DE',0),(82,'GHANA','GH',0),(83,'GIBRALTAR','GI',0),(84,'GREECE','GR',0),(85,'GREENLAND','GL',0),(86,'GRENADA','GD',0),(87,'GUADELOUPE','GP',0),(88,'GUAM','GU',0),(89,'GUATEMALA','GT',0),(90,'GUERNSEY','GG',0),(91,'GUINEA','GN',0),(92,'GUINEA-BISSAU','GW',0),(93,'GUYANA','GY',0),(94,'HAITI','HT',0),(95,'HEARD ISLAND AND MCDONALD ISLANDS','HM',0),(96,'HONDURAS','HN',0),(97,'HONG KONG','HK',0),(98,'HUNGARY','HU',0),(99,'ICELAND','IS',0),(100,'INDIA','IN',0),(101,'INDONESIA','ID',0),(102,'IRAN','IR',0),(103,'IRAQ','IQ',0),(104,'IRELAND','IE',0),(105,'ISLE OF MAN','IM',0),(106,'ISRAEL','IL',0),(107,'ITALY','IT',0),(108,'JAMAICA','JM',0),(109,'JAPAN','JP',0),(110,'JERSEY','JE',0),(111,'JORDAN','JO',0),(112,'KAZAKHSTAN','KZ',0),(113,'KENYA','KE',0),(114,'KIRIBATI','KI',0),(115,'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF','KP',0),(116,'KOREA, REPUBLIC OF','KR',0),(117,'KUWAIT','KW',0),(118,'KYRGYZSTAN','KG',0),(119,'LAO PEOPLE\'S DEMOCRATIC REPUBLIC','LA',0),(120,'LATVIA','LV',0),(121,'LEBANON','LB',0),(122,'LESOTHO','LS',0),(123,'LIBERIA','LR',0),(124,'LIBYAN ARAB JAMAHIRIYA','LY',0),(125,'LIECHTENSTEIN','LI',0),(126,'LITHUANIA','LT',0),(127,'LUXEMBOURG','LU',0),(128,'MACAO','MO',0),(129,'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','MK',0),(130,'MADAGASCAR','MG',0),(131,'MALAWI','MW',0),(132,'MALAYSIA','MY',0),(133,'MALDIVES','MV',0),(134,'MALI','ML',0),(135,'MALTA','MT',0),(136,'MARSHALL ISLANDS','MH',0),(137,'MARTINIQUE','MQ',0),(138,'MAURITANIA','MR',0),(139,'MAURITIUS','MU',0),(140,'MAYOTTE','YT',0),(141,'MEXICO','MX',0),(142,'MICRONESIA, FEDERATED STATES OF','FM',0),(143,'MOLDOVA, REPUBLIC OF','MD',0),(144,'MONACO','MC',0),(145,'MONGOLIA','MN',0),(146,'MONTENEGRO','ME',0),(147,'MONTSERRAT','MS',0),(148,'MOROCCO','MA',0),(149,'MOZAMBIQUE','MZ',0),(150,'MYANMAR','MM',0),(151,'NAMIBIA','NA',0),(152,'NAURU','NR',0),(153,'NEPAL','NP',0),(154,'NETHERLANDS','NL',0),(155,'NETHERLANDS ANTILLES','AN',0),(156,'NEW CALEDONIA','NC',0),(157,'NEW ZEALAND','NZ',0),(158,'NICARAGUA','NI',0),(159,'NIGER','NE',0),(160,'NIGERIA','NG',0),(161,'NIUE','NU',0),(162,'NORFOLK ISLAND','NF',0),(163,'NORTHERN MARIANA ISLANDS','MP',0),(164,'NORWAY','NO',0),(165,'OMAN','OM',0),(166,'PAKISTAN','PK',0),(167,'PALAU','PW',0),(168,'PALESTINIAN TERRITORY, OCCUPIED','PS',0),(169,'PANAMA','PA',0),(170,'PAPUA NEW GUINEA','PG',0),(171,'PARAGUAY','PY',0),(172,'PERU','PE',0),(173,'PHILIPPINES','PH',0),(174,'PITCAIRN','PN',0),(175,'POLAND','PL',0),(176,'PORTUGAL','PT',0),(177,'PUERTO RICO','PR',0),(178,'QATAR','QA',0),(179,'Reunion','RE',0),(180,'ROMANIA','RO',0),(181,'RUSSIAN FEDERATION','RU',0),(182,'RWANDA','RW',0),(183,'SAINT HELENA','SH',0),(184,'SAINT KITTS AND NEVIS','KN',0),(185,'SAINT LUCIA','LC',0),(186,'SAINT PIERRE AND MIQUELON','PM',0),(187,'SAINT VINCENT AND THE GRENADINES','VC',0),(188,'SAMOA','WS',0),(189,'SAN MARINO','SM',0),(190,'SAO TOME AND PRINCIPE','ST',0),(191,'SAUDI ARABIA','SA',0),(192,'SENEGAL','SN',0),(193,'SERBIA','RS',0),(194,'SEYCHELLES','SC',0),(195,'SIERRA LEONE','SL',0),(196,'SINGAPORE','SG',0),(197,'SLOVAKIA','SK',0),(198,'SLOVENIA','SI',0),(199,'SOLOMON ISLANDS','SB',0),(200,'SOMALIA','SO',0),(201,'SOUTH AFRICA','ZA',0),(202,'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','GS',0),(203,'SPAIN','ES',0),(204,'SRI LANKA','LK',0),(205,'SUDAN','SD',0),(206,'SURINAME','SR',0),(207,'SVALBARD AND JAN MAYEN','SJ',0),(208,'SWAZILAND','SZ',0),(209,'SWEDEN','SE',0),(210,'SWITZERLAND','CH',0),(211,'SYRIAN ARAB REPUBLIC','SY',0),(212,'TAIWAN, PROVINCE OF CHINA','TW',0),(213,'TAJIKISTAN','TJ',0),(214,'TANZANIA, UNITED REPUBLIC OF','TZ',0),(215,'THAILAND','TH',0),(216,'TIMOR-LESTE','TL',0),(217,'TOGO','TG',0),(218,'TOKELAU','TK',0),(219,'TONGA','TO',0),(220,'TRINIDAD AND TOBAGO','TT',0),(221,'TUNISIA','TN',0),(222,'TURKEY','TR',0),(223,'TURKMENISTAN','TM',0),(224,'TURKS AND CAICOS ISLANDS','TC',0),(225,'TUVALU','TV',0),(226,'UGANDA','UG',0),(227,'UKRAINE','UA',0),(228,'UNITED ARAB EMIRATES','AE',0),(229,'UNITED KINGDOM','GB',0),(230,'UNITED STATES','US',0),(231,'UNITED STATES MINOR OUTLYING ISLANDS','UM',0),(232,'URUGUAY','UY',0),(233,'UZBEKISTAN','UZ',0),(234,'VANUATU','VU',0),(235,'VATICAN CITY STATE','VA',0),(236,'VENEZUELA','VE',0),(237,'VIET NAM','VN',0),(238,'VIRGIN ISLANDS, BRITISH','VG',0),(239,'VIRGIN ISLANDS, U.S.','VI',0),(240,'WALLIS AND FUTUNA','WF',0),(241,'WESTERN SAHARA','EH',0),(242,'YEMEN','YE',0),(243,'ZAMBIA','ZM',0),(244,'ZIMBABWE','ZW',0),(245,'SOUTH SUDAN','SS',0);
/*!40000 ALTER TABLE `core_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_deleted_user`
--

DROP TABLE IF EXISTS `core_deleted_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_deleted_user` (
  `id_deletion` int(11) NOT NULL AUTO_INCREMENT,
  `idst` int(11) NOT NULL DEFAULT '0',
  `userid` varchar(255) NOT NULL DEFAULT '',
  `firstname` varchar(255) NOT NULL DEFAULT '',
  `lastname` varchar(255) NOT NULL DEFAULT '',
  `pass` varchar(64) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `photo` varchar(255) NOT NULL DEFAULT '',
  `avatar` varchar(255) NOT NULL DEFAULT '',
  `signature` text NOT NULL,
  `level` int(11) NOT NULL DEFAULT '0',
  `lastenter` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `valid` tinyint(1) NOT NULL DEFAULT '0',
  `pwd_expire_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `register_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deletion_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_deletion`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_deleted_user`
--

LOCK TABLES `core_deleted_user` WRITE;
/*!40000 ALTER TABLE `core_deleted_user` DISABLE KEYS */;
INSERT INTO `core_deleted_user` VALUES (1,13007,'/AcceptanceTmpUser31da4e8a28','Acceptance','User','$2a$13$qBGHR6FFUHBsCe0XxaCTCOMJ5y.L/TOKJNzIPAOCxjPOis7FXyCla','acceptance68dd8d36dc@test.com','','','',0,'2016-02-04 09:54:42',1,'0000-00-00 00:00:00','2016-02-04 09:54:33','2016-02-04 10:01:57',13006);
/*!40000 ALTER TABLE `core_deleted_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_enroll_log`
--

DROP TABLE IF EXISTS `core_enroll_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_enroll_log` (
  `enroll_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`enroll_log_id`),
  KEY `rule_id` (`rule_id`),
  CONSTRAINT `core_enroll_log_ibfk_1` FOREIGN KEY (`rule_id`) REFERENCES `core_enroll_rule` (`rule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_enroll_log`
--

LOCK TABLES `core_enroll_log` WRITE;
/*!40000 ALTER TABLE `core_enroll_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_enroll_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_enroll_log_item`
--

DROP TABLE IF EXISTS `core_enroll_log_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_enroll_log_item` (
  `log_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `enroll_log_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'the user have been enrolled',
  `target_id` int(11) NOT NULL COMMENT 'a course or curricula',
  `rollback` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'is a rollback performed',
  PRIMARY KEY (`log_item_id`),
  KEY `enroll_log_id` (`enroll_log_id`),
  CONSTRAINT `core_enroll_log_item_ibfk_1` FOREIGN KEY (`enroll_log_id`) REFERENCES `core_enroll_log` (`enroll_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_enroll_log_item`
--

LOCK TABLES `core_enroll_log_item` WRITE;
/*!40000 ALTER TABLE `core_enroll_log_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_enroll_log_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_enroll_rule`
--

DROP TABLE IF EXISTS `core_enroll_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_enroll_rule` (
  `rule_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `rule_type` varchar(50) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_updated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`rule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_enroll_rule`
--

LOCK TABLES `core_enroll_rule` WRITE;
/*!40000 ALTER TABLE `core_enroll_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_enroll_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_enroll_rule_item`
--

DROP TABLE IF EXISTS `core_enroll_rule_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_enroll_rule_item` (
  `rule_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_id` int(11) NOT NULL,
  `item_type` varchar(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `selection_state` int(11) DEFAULT NULL,
  PRIMARY KEY (`rule_item_id`),
  KEY `rule_id` (`rule_id`),
  CONSTRAINT `core_enroll_rule_item_ibfk_1` FOREIGN KEY (`rule_id`) REFERENCES `core_enroll_rule` (`rule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_enroll_rule_item`
--

LOCK TABLES `core_enroll_rule_item` WRITE;
/*!40000 ALTER TABLE `core_enroll_rule_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_enroll_rule_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_field`
--

DROP TABLE IF EXISTS `core_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_field` (
  `idField` int(11) NOT NULL AUTO_INCREMENT,
  `id_common` int(11) NOT NULL DEFAULT '0',
  `type_field` varchar(255) NOT NULL DEFAULT '',
  `lang_code` varchar(255) NOT NULL DEFAULT '',
  `translation` varchar(255) NOT NULL DEFAULT '',
  `sequence` int(5) NOT NULL DEFAULT '0',
  `show_on_platform` varchar(255) NOT NULL DEFAULT 'framework,',
  `use_multilang` tinyint(1) NOT NULL DEFAULT '0',
  `mandatory` tinyint(1) NOT NULL,
  `invisible_to_user` tinyint(1) NOT NULL,
  PRIMARY KEY (`idField`),
  KEY `id_common` (`id_common`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_field`
--

LOCK TABLES `core_field` WRITE;
/*!40000 ALTER TABLE `core_field` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_field_son`
--

DROP TABLE IF EXISTS `core_field_son`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_field_son` (
  `idSon` int(11) NOT NULL AUTO_INCREMENT,
  `idField` int(11) NOT NULL DEFAULT '0',
  `id_common_son` int(11) NOT NULL DEFAULT '0',
  `lang_code` varchar(50) NOT NULL DEFAULT '',
  `translation` varchar(255) NOT NULL DEFAULT '',
  `sequence` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idSon`),
  KEY `idField` (`idField`),
  KEY `id_common_son` (`id_common_son`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_field_son`
--

LOCK TABLES `core_field_son` WRITE;
/*!40000 ALTER TABLE `core_field_son` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_field_son` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_field_type`
--

DROP TABLE IF EXISTS `core_field_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_field_type` (
  `type_field` varchar(255) NOT NULL DEFAULT '',
  `type_file` varchar(255) NOT NULL DEFAULT '',
  `type_class` varchar(255) NOT NULL DEFAULT '',
  `type_category` varchar(255) NOT NULL DEFAULT 'standard',
  PRIMARY KEY (`type_field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_field_type`
--

LOCK TABLES `core_field_type` WRITE;
/*!40000 ALTER TABLE `core_field_type` DISABLE KEYS */;
INSERT INTO `core_field_type` VALUES ('codicefiscale','class.cf.php','Field_Cf','standard'),('country','class.country.php','Field_Country','standard'),('date','class.date.php','Field_Date','standard'),('dropdown','class.dropdown.php','Field_Dropdown','standard'),('freetext','class.freetext.php','Field_Freetext','standard'),('textfield','class.textfield.php','Field_Textfield','standard'),('upload','class.upload.php','Field_Upload','standard'),('yesno','class.yesno.php','Field_Yesno','standard');
/*!40000 ALTER TABLE `core_field_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_field_userentry`
--

DROP TABLE IF EXISTS `core_field_userentry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_field_userentry` (
  `id_common` int(11) NOT NULL,
  `id_common_son` int(11) NOT NULL DEFAULT '0',
  `id_user` int(11) NOT NULL DEFAULT '0',
  `user_entry` text NOT NULL,
  `user_entry_extra` text,
  PRIMARY KEY (`id_common`,`id_common_son`,`id_user`),
  KEY `id_user` (`id_user`),
  KEY `id_common` (`id_common`),
  KEY `id_common_son` (`id_common_son`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_field_userentry`
--

LOCK TABLES `core_field_userentry` WRITE;
/*!40000 ALTER TABLE `core_field_userentry` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_field_userentry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_group`
--

DROP TABLE IF EXISTS `core_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_group` (
  `idst` int(11) NOT NULL DEFAULT '0',
  `groupid` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `hidden` enum('true','false') NOT NULL DEFAULT 'false',
  `type` enum('free','moderate','private','invisible','course','company') NOT NULL DEFAULT 'free',
  `show_on_platform` text NOT NULL,
  `assign_rules` int(1) DEFAULT '0',
  `assign_rules_logic_operator` varchar(3) DEFAULT 'AND',
  `additional_fields` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idst`),
  UNIQUE KEY `groupid` (`groupid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_group`
--

LOCK TABLES `core_group` WRITE;
/*!40000 ALTER TABLE `core_group` DISABLE KEYS */;
INSERT INTO `core_group` VALUES (1,'/oc_0','Root of organization chart','true','free','',0,'AND',NULL),(2,'/ocd_0','Root of organization chart and descendants','true','free','',0,'AND',NULL),(3,'/framework/level/godadmin','Group of godadmins','true','free','',0,'AND',NULL),(4,'/framework/level/admin','Group of administrators','true','free','',0,'AND',NULL),(6,'/framework/level/user','Group of normal users','true','free','',0,'AND',NULL),(12301,'/framework/adminrules/Country manager','Standard admin profile','true','free','',0,'AND',NULL);
/*!40000 ALTER TABLE `core_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_group_assign_rules`
--

DROP TABLE IF EXISTS `core_group_assign_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_group_assign_rules` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `idGroup` int(11) NOT NULL,
  `idFieldCommon` int(11) NOT NULL,
  `condition_type` varchar(32) NOT NULL,
  `condition_value` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_group_idx` (`idGroup`),
  KEY `fk_field_idx` (`idFieldCommon`),
  CONSTRAINT `fk_group` FOREIGN KEY (`idGroup`) REFERENCES `core_group` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_group_assign_rules`
--

LOCK TABLES `core_group_assign_rules` WRITE;
/*!40000 ALTER TABLE `core_group_assign_rules` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_group_assign_rules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_group_fields`
--

DROP TABLE IF EXISTS `core_group_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_group_fields` (
  `idst` int(11) NOT NULL DEFAULT '0',
  `id_field` int(11) NOT NULL DEFAULT '0',
  `mandatory` enum('true','false') NOT NULL DEFAULT 'false',
  `useraccess` enum('noaccess','readonly','readwrite') NOT NULL DEFAULT 'readonly',
  `user_inherit` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `user_modify` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`idst`,`id_field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_group_fields`
--

LOCK TABLES `core_group_fields` WRITE;
/*!40000 ALTER TABLE `core_group_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_group_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_group_members`
--

DROP TABLE IF EXISTS `core_group_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_group_members` (
  `idst` int(11) NOT NULL DEFAULT '0',
  `idstMember` int(11) NOT NULL DEFAULT '0',
  `filter` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`idst`,`idstMember`),
  KEY `idstMember` (`idstMember`),
  CONSTRAINT `core_group_members_ibfk_1` FOREIGN KEY (`idst`) REFERENCES `core_group` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_group_members`
--

LOCK TABLES `core_group_members` WRITE;
/*!40000 ALTER TABLE `core_group_members` DISABLE KEYS */;
INSERT INTO `core_group_members` VALUES (1,13006,''),(1,13008,''),(2,13006,''),(2,13008,''),(3,13006,''),(6,13008,'');
/*!40000 ALTER TABLE `core_group_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_https`
--

DROP TABLE IF EXISTS `core_https`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_https` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mode` int(11) DEFAULT NULL,
  `domain` varchar(255) NOT NULL,
  `certFile` varchar(255) DEFAULT NULL,
  `keyFile` varchar(255) DEFAULT NULL,
  `keyFileOrigin` varchar(32) DEFAULT NULL,
  `intermCaFile` varchar(255) DEFAULT NULL,
  `csrFile` varchar(255) DEFAULT NULL,
  `sslStatus` varchar(32) DEFAULT NULL,
  `sslInstallTime` timestamp NULL DEFAULT NULL,
  `sslExpireTime` timestamp NULL DEFAULT NULL,
  `isWildCard` tinyint(4) DEFAULT NULL,
  `sslStatusLastError` varchar(255) DEFAULT NULL,
  `csrGenerationStatus` varchar(32) DEFAULT NULL,
  `csrGenerationTime` timestamp NULL DEFAULT NULL,
  `csrGenerationLastMessage` varchar(1024) DEFAULT NULL,
  `csrCountryName` varchar(255) DEFAULT NULL,
  `csrOrgUnitName` varchar(255) DEFAULT NULL,
  `csrStateOrProvinceName` varchar(255) DEFAULT NULL,
  `csrCommonName` varchar(255) DEFAULT NULL,
  `csrLocalityName` varchar(255) DEFAULT NULL,
  `csrEmail` varchar(255) DEFAULT NULL,
  `csrOrgName` varchar(255) DEFAULT NULL,
  `idMultidomainClient` int(11) DEFAULT NULL,
  `httpsAgentToken` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idMultidomainClient` (`idMultidomainClient`),
  CONSTRAINT `core_https_ibfk_1` FOREIGN KEY (`idMultidomainClient`) REFERENCES `core_multidomain` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_https`
--

LOCK TABLES `core_https` WRITE;
/*!40000 ALTER TABLE `core_https` DISABLE KEYS */;
INSERT INTO `core_https` VALUES (1,0,'andre67empty.docebosaas.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ef041b41c31c84d48692c0c982f53a5f6552d095');
/*!40000 ALTER TABLE `core_https` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_ip_filter`
--

DROP TABLE IF EXISTS `core_ip_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_ip_filter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_ip_filter`
--

LOCK TABLES `core_ip_filter` WRITE;
/*!40000 ALTER TABLE `core_ip_filter` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_ip_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_job`
--

DROP TABLE IF EXISTS `core_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_job` (
  `id_job` int(11) NOT NULL AUTO_INCREMENT,
  `hash_id` varchar(64) NOT NULL,
  `name` varchar(128) NOT NULL,
  `id_author` int(11) DEFAULT NULL COMMENT 'User',
  `active` tinyint(4) DEFAULT '1' COMMENT '0/1',
  `is_running` tinyint(4) DEFAULT '0' COMMENT '0/1',
  `allow_multiple` int(11) DEFAULT '0' COMMENT '0/1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL,
  `handler_id` varchar(255) DEFAULT NULL COMMENT 'Job Handler ID or classFilePath',
  `type` varchar(32) NOT NULL COMMENT 'Job type (recurring, dailywork, onetime,...)',
  `rec_period` varchar(32) DEFAULT NULL,
  `rec_length` int(11) DEFAULT NULL COMMENT 'Period length, used for Months only (every Nth month)',
  `rec_minute` int(11) DEFAULT NULL,
  `rec_hour` int(11) DEFAULT NULL,
  `rec_timezone` varchar(50) DEFAULT NULL,
  `rec_day_of_week` int(11) DEFAULT NULL,
  `rec_day_of_month` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `cron_timing` varchar(100) DEFAULT NULL COMMENT 'Cron style string * * * * * , resulting from other recurring fields',
  `last_started` timestamp NULL DEFAULT NULL,
  `last_finished` timestamp NULL DEFAULT NULL,
  `last_status` varchar(32) DEFAULT NULL,
  `last_error_message` text,
  `params` longtext COMMENT 'JSON-encoded Job specific array or parameters',
  PRIMARY KEY (`id_job`),
  KEY `hash_id` (`hash_id`),
  KEY `id_author` (`id_author`),
  CONSTRAINT `core_job_ibfk_1` FOREIGN KEY (`id_author`) REFERENCES `core_user` (`idst`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_job`
--

LOCK TABLES `core_job` WRITE;
/*!40000 ALTER TABLE `core_job` DISABLE KEYS */;
INSERT INTO `core_job` VALUES (1,'22ee81bea2e1d28057189e7a4d4b87fbc6bf392c','cache_user_items_visibility_',13006,1,0,0,'2016-02-03 07:40:07',NULL,'elasticsearch.components.EsJobHandler','onetime','',1,0,0,'',1,1,'2016-02-03','0 0 * * *','2016-02-03 07:40:07','2016-02-03 07:40:07','success',NULL,'{\"task\":\"cache_user_items_visibility_\",\"users\":[\"13006\"]}'),(2,'371053c0aaf0d07ea4d84c7e4621990bcf0a6679','cache_user_items_visibility_',13006,1,0,0,'2016-02-03 09:37:55',NULL,'elasticsearch.components.EsJobHandler','onetime','',1,0,0,'',1,1,'2016-02-03','0 0 * * *','2016-02-03 09:37:55','2016-02-03 09:37:55','success',NULL,'{\"task\":\"cache_user_items_visibility_\",\"users\":[\"13006\"]}');
/*!40000 ALTER TABLE `core_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_job_log`
--

DROP TABLE IF EXISTS `core_job_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_job_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_job` int(11) DEFAULT NULL,
  `started` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `finished` timestamp NULL DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL COMMENT 'Success status (success, error, etc)',
  `message` text COMMENT 'Error message, other feedback',
  `json_job` text NOT NULL COMMENT 'JSON-ed job attributes',
  PRIMARY KEY (`id`),
  KEY `id_job` (`id_job`),
  CONSTRAINT `core_job_log_ibfk_1` FOREIGN KEY (`id_job`) REFERENCES `core_job` (`id_job`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_job_log`
--

LOCK TABLES `core_job_log` WRITE;
/*!40000 ALTER TABLE `core_job_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_job_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_lang_language`
--

DROP TABLE IF EXISTS `core_lang_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_lang_language` (
  `lang_code` varchar(50) NOT NULL DEFAULT '',
  `lang_description` varchar(255) NOT NULL DEFAULT '',
  `lang_browsercode` varchar(50) NOT NULL DEFAULT '',
  `lang_direction` enum('ltr','rtl') NOT NULL DEFAULT 'ltr',
  `lang_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`lang_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_lang_language`
--

LOCK TABLES `core_lang_language` WRITE;
/*!40000 ALTER TABLE `core_lang_language` DISABLE KEYS */;
INSERT INTO `core_lang_language` VALUES ('arabic','Arabic','ar','rtl',1),('bosnian','Bosnian','bs','ltr',1),('bulgarian','Bulgarian','bg','ltr',1),('croatian','Croatian','hr','ltr',1),('czech','Czech','cs','ltr',1),('danish','Dansk','da','ltr',1),('dutch','Dutch','nl','ltr',1),('english','English','en','ltr',1),('english_uk','English (UK)','en-gb','ltr',1),('farsi','Farsi','fa','rtl',1),('finnish','Finnish','fi','ltr',1),('french','French','fr','ltr',1),('german','German','de','ltr',1),('greek','Greek','el','ltr',1),('hebrew','Hebrew','he','rtl',1),('hungarian','Hungarian','hu','ltr',1),('indonesian','Indonesian','id','ltr',1),('italian','Italiano','it','ltr',1),('japanese','Japanese','ja','ltr',1),('korean','Korean','ko','ltr',1),('norwegian','Norwegian','no','ltr',1),('polish','Polish','pl','ltr',1),('portuguese','PortuguÃªs','pt','ltr',1),('portuguese-br','PortuguÃªs Brazil','pt-br','ltr',1),('romanian','Romanian','ro','ltr',1),('russian','Russian','ru','ltr',1),('simplified_chinese','ç®€ä½“ä¸­æ–‡','zh','ltr',1),('slovenian','Slovenian','sl','ltr',1),('spanish','Spanish','es','ltr',1),('swedish','Swedish','sv','ltr',1),('thai','à¹„à¸—à¸¢','th','ltr',1),('turkish','Turkish','tr','ltr',1),('ukrainian','Ukranian','uk','ltr',1);
/*!40000 ALTER TABLE `core_lang_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_lang_text`
--

DROP TABLE IF EXISTS `core_lang_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_lang_text` (
  `id_text` int(11) NOT NULL AUTO_INCREMENT,
  `text_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `text_module` varchar(50) NOT NULL DEFAULT '',
  `text_attributes` set('accessibility','sms','email') NOT NULL DEFAULT '',
  `context` text NOT NULL,
  PRIMARY KEY (`id_text`),
  UNIQUE KEY `text_key` (`text_key`,`text_module`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_lang_text`
--

LOCK TABLES `core_lang_text` WRITE;
/*!40000 ALTER TABLE `core_lang_text` DISABLE KEYS */;
INSERT INTO `core_lang_text` VALUES (1,'_LOGIN_MAIN_TITLE','login','','login'),(2,'_LOGIN_MAIN_CAPTION','login','','login');
/*!40000 ALTER TABLE `core_lang_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_lang_translation`
--

DROP TABLE IF EXISTS `core_lang_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_lang_translation` (
  `id_text` int(11) NOT NULL DEFAULT '0',
  `lang_code` varchar(50) NOT NULL DEFAULT '',
  `translation_text` text COMMENT 'The actual text of the translation.',
  `translation_status` varchar(50) NOT NULL DEFAULT 'To review' COMMENT 'Values: new, approved, translating and translated',
  `save_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `pid` varchar(20) DEFAULT NULL COMMENT 'PID returned from the translation API',
  PRIMARY KEY (`id_text`,`lang_code`),
  KEY `lang_code` (`lang_code`),
  KEY `id_text` (`id_text`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_lang_translation`
--

LOCK TABLES `core_lang_translation` WRITE;
/*!40000 ALTER TABLE `core_lang_translation` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_lang_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_menu_filter`
--

DROP TABLE IF EXISTS `core_menu_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_menu_filter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idMenu` int(11) DEFAULT NULL,
  `obj_type` varchar(20) DEFAULT '',
  `idResource` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_menu_filter`
--

LOCK TABLES `core_menu_filter` WRITE;
/*!40000 ALTER TABLE `core_menu_filter` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_menu_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_menu_item`
--

DROP TABLE IF EXISTS `core_menu_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_menu_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lev` int(11) NOT NULL,
  `iLeft` int(5) NOT NULL,
  `iRight` int(5) NOT NULL,
  `idParent` int(5) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `default_opt` varchar(255) DEFAULT NULL,
  `module_name` varchar(255) DEFAULT NULL,
  `is_custom` tinyint(1) DEFAULT '0',
  `visible` tinyint(1) DEFAULT '1',
  `active` tinyint(1) DEFAULT '1',
  `default` tinyint(1) DEFAULT '0',
  `options` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_menu_item`
--

LOCK TABLES `core_menu_item` WRITE;
/*!40000 ALTER TABLE `core_menu_item` DISABLE KEYS */;
INSERT INTO `core_menu_item` VALUES (1,1,1,16,0,'','root',NULL,NULL,0,1,1,0,NULL),(2,2,2,3,1,'menu-icon-mydashboard','site/index','[]','',0,1,1,1,NULL),(3,2,4,5,1,'my-courses','site/index','{\"opt\":\"fullmycourses\"}','',0,1,1,0,NULL),(4,2,6,7,1,'catalog','site/index','{\"opt\":\"catalog\"}','CoursecatalogApp',0,0,0,0,NULL),(5,2,8,9,1,'my-activities','myActivities/index','[]','',0,1,1,0,NULL),(6,2,10,11,1,'my-calendar','mycalendar/show','{\"sop\":\"unregistercourse\"}','',0,0,1,0,NULL),(7,2,12,13,1,'my-blog','MyBlogApp/MyBlogApp/index','[]','MyBlogApp',0,0,0,0,NULL),(8,2,14,15,1,'curricula','curricula/show','{\"sop\":\"unregistercourse\"}','CurriculaApp',0,0,0,0,NULL);
/*!40000 ALTER TABLE `core_menu_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_menu_translation`
--

DROP TABLE IF EXISTS `core_menu_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_menu_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idMenu` int(11) DEFAULT NULL,
  `lang_code` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_menu_translation`
--

LOCK TABLES `core_menu_translation` WRITE;
/*!40000 ALTER TABLE `core_menu_translation` DISABLE KEYS */;
INSERT INTO `core_menu_translation` VALUES (1,1,'arabic',''),(2,1,'bosnian',''),(3,1,'bulgarian',''),(4,1,'croatian',''),(5,1,'czech',''),(6,1,'danish',''),(7,1,'dutch',''),(8,1,'english','Menu'),(9,1,'farsi',''),(10,1,'finnish',''),(11,1,'french',''),(12,1,'german',''),(13,1,'greek',''),(14,1,'hebrew',''),(15,1,'hungarian',''),(16,1,'indonesian',''),(17,1,'italian',''),(18,1,'japanese',''),(19,1,'korean',''),(20,1,'norwegian',''),(21,1,'polish',''),(22,1,'portuguese',''),(23,1,'portuguese-br',''),(24,1,'romanian',''),(25,1,'russian',''),(26,1,'simplified_chinese',''),(27,1,'spanish',''),(28,1,'swedish',''),(29,1,'thai',''),(30,1,'turkish',''),(31,1,'ukrainian',''),(32,2,'arabic','Ù„ÙˆØ­Ø© Ø§Ù„ØªØ­ÙƒÙ…'),(33,2,'bosnian','Moja kontrolna ploÄ�a'),(34,2,'bulgarian','ÐœÐ¾ÐµÑ‚Ð¾ Ñ‚Ð°Ð±Ð»Ð¾ Ð·Ð°Ð´Ð°Ñ‡Ð¸'),(35,2,'croatian','Moja Kontrolna ploÄ�a'),(36,2,'czech','MÅ¯j Å™Ã­dÃ­cÃ­ panel'),(37,2,'danish','Mit Dashboard'),(38,2,'dutch','Mijn dashboard'),(39,2,'english','My Dashboard'),(40,2,'farsi','Ø¯Ø§Ø´Ø¨ÙˆØ±Ø¯ Ù…Ù†'),(41,2,'finnish','Oma koontinÃ¤yttÃ¶'),(42,2,'french','Mon tableau de bord'),(43,2,'german','Meine Ãœbersichtsseite'),(44,2,'greek','Î¤Î¿ Ï„Î±Î¼Ï€Î»ÏŒ Î¼Î¿Ï…'),(45,2,'hebrew','×œ×•×— ×”×‘×§×¨×” ×”×�×™×©×™ ×©×œ×™'),(46,2,'hungarian','IrÃ¡nyÃ­tÃ³pultom'),(47,2,'indonesian','Dasbor saya'),(48,2,'italian','La mia Dashboard'),(49,2,'japanese','ãƒžã‚¤ ãƒ€ãƒƒã‚·ãƒ¥ãƒœãƒ¼ãƒ‰'),(50,2,'korean','ë‚´ ëŒ€ì‹œë³´ë“œ'),(51,2,'norwegian','My Dashboard'),(52,2,'polish','MÃ³j panel'),(53,2,'portuguese','Meu Painel de Controlo'),(54,2,'portuguese-br','Meu Painel de Controle'),(55,2,'romanian','Tabloul meu de bord'),(56,2,'russian','ÐœÐ¾Ñ� Ð¿Ð°Ð½ÐµÐ»ÑŒ ÑƒÐ¿Ñ€Ð°Ð²Ð»ÐµÐ½Ð¸Ñ�'),(57,2,'simplified_chinese','æˆ‘çš„ä»ªè¡¨æ�¿'),(58,2,'spanish','Mi panel de control'),(59,2,'swedish','Min instrumentpanel'),(60,2,'thai','à¸«à¸™à¹‰à¸²à¸„à¸§à¸šà¸„à¸¸à¸¡à¸‚à¸­à¸‡à¸‰à¸±à¸™'),(61,2,'turkish','Panom'),(62,2,'ukrainian','ÐœÐ¾Ñ� ÐŸÐ°Ð½ÐµÐ»ÑŒ'),(63,3,'arabic','Ø¯ÙˆØ±Ø§ØªÙŠ'),(64,3,'bosnian','Moji kursevi'),(65,3,'bulgarian','ÐœÐ¾Ð¸Ñ‚Ðµ ÐºÑƒÑ€Ñ�Ð¾Ð²Ðµ'),(66,3,'croatian','Moji teÄ�ajevi'),(67,3,'czech','MÃ© kurzy'),(68,3,'danish','Mine kurser'),(69,3,'dutch','Mijn cursussen'),(70,3,'english','My Courses'),(71,3,'farsi','Ø¯ÙˆØ±Ù‡ Ù‡Ø§ÛŒ Ù…Ù†'),(72,3,'finnish','Kurssini'),(73,3,'french','Mes Cours'),(74,3,'german','Meine Kurse'),(75,3,'greek','Î¤Î± Î¼Î±Î¸Î®Î¼Î±Ï„Î¬ Î¼Î¿Ï…'),(76,3,'hebrew','×”×§×•×¨×¡×™×� ×©×œ×™'),(77,3,'hungarian','Az Ã©n tanfolyamaim'),(78,3,'indonesian','My Courses'),(79,3,'italian','I miei corsi'),(80,3,'japanese','ãƒžã‚¤ã‚³ãƒ¼ã‚¹'),(81,3,'korean','ë‚´ ê³¼ì •'),(82,3,'norwegian','Mine kurs'),(83,3,'polish','Moje kursy'),(84,3,'portuguese','Os Meus Cursos'),(85,3,'portuguese-br','Meus Cursos'),(86,3,'romanian','Cursurile mele'),(87,3,'russian','ÐœÐ¾Ð¸ ÐºÑƒÑ€Ñ�Ñ‹'),(88,3,'simplified_chinese','æˆ‘çš„è¯¾ç¨‹'),(89,3,'spanish','Mis cursos'),(90,3,'swedish','Mina kurser'),(91,3,'thai','à¸„à¸­à¸£à¹Œà¸ªà¸‚à¸­à¸‡à¸‰à¸±à¸™'),(92,3,'turkish','Derslerim'),(93,3,'ukrainian','ÐœÐ¾Ñ— ÐºÑƒÑ€Ñ�Ð¸'),(94,4,'arabic','ÙƒØªØ§Ù„ÙˆØ¬ Ø§Ù„Ø¯ÙˆØ±Ø§Øª'),(95,4,'bosnian','Katalog kurseva'),(96,4,'bulgarian','ÐšÐ°Ñ‚Ð°Ð»Ð¾Ð³ Ð½Ð° ÐºÑƒÑ€Ñ�Ð¾Ð²ÐµÑ‚Ðµ'),(97,4,'croatian','Katalog teÄ�ajeva'),(98,4,'czech','Katalog kurzÅ¯'),(99,4,'danish','Kursuskatalog'),(100,4,'dutch','Cursuscatalogus'),(101,4,'english','Courses Catalog'),(102,4,'farsi','Ú©Ø§ØªØ§Ù„ÙˆÚ¯ Ø¯ÙˆØ±Ù‡ Ù‡Ø§'),(103,4,'finnish','Kurssiluettelo'),(104,4,'french','Catalogue des cours'),(105,4,'german','Kurskatalog'),(106,4,'greek','ÎšÎ±Ï„Î¬Î»Î¿Î³Î¿Ï‚ Î¼Î±Î¸Î·Î¼Î¬Ï„Ï‰Î½'),(107,4,'hebrew','×§×˜×œ×•×’ ×§×•×¨×¡×™×�'),(108,4,'hungarian','Tanfolyami katalÃ³gus'),(109,4,'indonesian','Katalog Kursus'),(110,4,'italian','Catalogo Corsi'),(111,4,'japanese','ã‚³ãƒ¼ã‚¹ã‚«ã‚¿ãƒ­ã‚°'),(112,4,'korean','ê³¼ì • ì¹´íƒˆë¡œê·¸'),(113,4,'norwegian','Kurskatalog'),(114,4,'polish','Katalog kursÃ³w'),(115,4,'portuguese','CatÃ¡logo de cursos'),(116,4,'portuguese-br','CatÃ¡logo de Cursos'),(117,4,'romanian','Catalog de cursuri'),(118,4,'russian','ÐšÐ°Ñ‚Ð°Ð»Ð¾Ð³ ÐºÑƒÑ€Ñ�Ð¾Ð²'),(119,4,'simplified_chinese','è¯¾ç¨‹ç›®å½•'),(120,4,'spanish','CatÃ¡logo de cursos'),(121,4,'swedish','Kurskatalog'),(122,4,'thai','à¹�à¸„à¸•à¸•à¸²à¸¥à¹‡à¸­à¸�à¸«à¸¥à¸±à¸�à¸ªà¸¹à¸•à¸£'),(123,4,'turkish','Ders KataloÄŸu'),(124,4,'ukrainian','ÐšÐ°Ñ‚Ð°Ð»Ð¾Ð³ ÐºÑƒÑ€Ñ�Ñ–Ð²'),(125,5,'arabic','Ø£Ù†Ø´Ø·ØªÙŠ'),(126,5,'bosnian','Moje aktivnosti'),(127,5,'bulgarian','ÐœÐ¾Ð¸Ñ‚Ðµ Ð´ÐµÐ¹Ð½Ð¾Ñ�Ñ‚Ð¸'),(128,5,'croatian','Moje aktivnosti'),(129,5,'czech','Moje Ä�innosti'),(130,5,'danish','Mine Aktiviteter'),(131,5,'dutch','Mijn Activiteiten'),(132,5,'english','My Activities'),(133,5,'farsi','Ù�Ø¹Ø§Ù„ÛŒØªâ€ŒÙ‡Ø§ÛŒ Ù…Ù†'),(134,5,'finnish','Toimintoni'),(135,5,'french','Mes activitÃ©s'),(136,5,'german','Meine AktivitÃ¤ten'),(137,5,'greek','My Activities'),(138,5,'hebrew','×”×¤×¢×™×œ×•×™×•×ª ×©×œ×™'),(139,5,'hungarian','SajÃ¡t tevÃ©kenysÃ©geim'),(140,5,'indonesian','Kegiatan saya'),(141,5,'italian','Le mie attivitÃ '),(142,5,'japanese','ãƒžã‚¤ãƒ»ã‚¢ã‚¯ãƒ†ã‚£ãƒ“ãƒ†ã‚£'),(143,5,'korean','ë‚´ í™œë�™'),(144,5,'norwegian','Mine aktiviteter'),(145,5,'polish','Moja aktywnoÅ›Ä‡'),(146,5,'portuguese','As minhas atividades'),(147,5,'portuguese-br','My Activities'),(148,5,'romanian','ActivitÄƒÅ£ile mele'),(149,5,'russian','ÐœÐ¾Ð¸ Ð¾Ð¿ÐµÑ€Ð°Ñ†Ð¸Ð¸'),(150,5,'simplified_chinese','æˆ‘çš„æ´»åŠ¨'),(151,5,'spanish','Mis actividades'),(152,5,'swedish','Mina aktiviteter'),(153,5,'thai','à¸�à¸´à¸ˆà¸�à¸£à¸£à¸¡à¸‚à¸­à¸‡à¸‰à¸±à¸™'),(154,5,'turkish','Faaliyetlerim'),(155,5,'ukrainian','ÐœÐ¾Ñ� Ð´Ñ–Ñ�Ð»ÑŒÐ½Ñ–Ñ�Ñ‚ÑŒ'),(156,6,'arabic','ØªÙ‚ÙˆÙŠÙ…ÙŠ'),(157,6,'bosnian','Moj kalendar'),(158,6,'bulgarian','ÐœÐ¾Ñ�Ñ‚ ÐºÐ°Ð»ÐµÐ½Ð´Ð°Ñ€'),(159,6,'croatian','Moj kalendar'),(160,6,'czech','MÅ¯j kalendÃ¡Å™'),(161,6,'danish','Min kalender'),(162,6,'dutch','Mijn kalender'),(163,6,'english','My Calendar'),(164,6,'farsi','ØªÙ‚ÙˆÛŒÙ… Ù…Ù†'),(165,6,'finnish','Kalenterini'),(166,6,'french','Mon calendrier'),(167,6,'german','Mein Kalender'),(168,6,'greek','Î¤Î¿ Î—Î¼ÎµÏ�Î¿Î»ÏŒÎ³Î¹ÏŒ Î¼Î¿Ï…'),(169,6,'hebrew','×œ×•×— ×”×©× ×” ×©×œ×™'),(170,6,'hungarian','Az Ã‰n NaptÃ¡ram'),(171,6,'indonesian','Kalender Saya'),(172,6,'italian','Il mio calendario'),(173,6,'japanese','ãƒžã‚¤ ã‚«ãƒ¬ãƒ³ãƒ€ãƒ¼'),(174,6,'korean','ë‚´ ìº˜ë¦°ë�”'),(175,6,'norwegian','Min kalender'),(176,6,'polish','My Calendar'),(177,6,'portuguese','My Calendar'),(178,6,'portuguese-br','Meu calendÃ¡rio'),(179,6,'romanian','Calendarul meu'),(180,6,'russian','ÐœÐ¾Ð¹ ÐºÐ°Ð»ÐµÐ½Ð´Ð°Ñ€ÑŒ'),(181,6,'simplified_chinese','æˆ‘çš„æ—¥åŽ†'),(182,6,'spanish','Mi calendario'),(183,6,'swedish','Min kalender'),(184,6,'thai','à¸›à¸�à¸´à¸—à¸´à¸™à¸‚à¸­à¸‡à¸‰à¸±à¸™'),(185,6,'turkish','Takvimim'),(186,6,'ukrainian','ÐœÑ–Ð¹ ÐšÐ°Ð»ÐµÐ½Ð´Ð°Ñ€'),(187,7,'arabic','Ù…Ø¯ÙˆÙ†ØªÙŠ'),(188,7,'bosnian','Moj blog'),(189,7,'bulgarian','ÐœÐ¾Ñ�Ñ‚ Ð±Ð»Ð¾Ð³'),(190,7,'croatian','Moj blog'),(191,7,'czech','MÅ¯j blog'),(192,7,'danish','Min blog'),(193,7,'dutch','Mijn blog'),(194,7,'english','My Blog'),(195,7,'farsi','ÙˆØ¨Ù„Ø§Ú¯ Ù…Ù†'),(196,7,'finnish','Blogini'),(197,7,'french','Mon Blog'),(198,7,'german','Mein Blog'),(199,7,'greek','Î¤Î¿ Î¹ÏƒÏ„Î¿Î»ÏŒÎ³Î¹ÏŒ Î¼Î¿Ï…'),(200,7,'hebrew','×”×‘×œ×•×’ ×©×œ×™'),(201,7,'hungarian','SajÃ¡t Blog'),(202,7,'indonesian','Blog Saya'),(203,7,'italian','Il mio Blog'),(204,7,'japanese','ãƒžã‚¤ãƒ–ãƒ­ã‚°'),(205,7,'korean','ë‚´ ë¸”ë¡œê·¸'),(206,7,'norwegian','Min blogg'),(207,7,'polish','MÃ³j blog'),(208,7,'portuguese','O meu bloque'),(209,7,'portuguese-br','Meu blog'),(210,7,'romanian','Blogul meu'),(211,7,'russian','ÐœÐ¾Ð¸ Ð·Ð°Ð¿Ð¸Ñ�Ð¸'),(212,7,'simplified_chinese','æˆ‘çš„å�šå®¢'),(213,7,'spanish','Mi Blog'),(214,7,'swedish','Min Blogg'),(215,7,'thai','à¸šà¸¥à¹‡à¸­à¸�à¸‚à¸­à¸‡à¸‰à¸±à¸™'),(216,7,'turkish','Blogum'),(217,7,'ukrainian','ÐœÑ–Ð¹ Ð±Ð»Ð¾Ð³'),(218,8,'arabic','Ø§Ù„Ù…Ù†Ø§Ù‡Ø¬'),(219,8,'bosnian','Nastavni planovi'),(220,8,'bulgarian','Ð£Ñ‡ÐµÐ±Ð½Ð° Ð¿Ñ€Ð¾Ð³Ñ€Ð°Ð¼Ð°'),(221,8,'croatian','Nastavni planovi'),(222,8,'czech','Osnovy'),(223,8,'danish','Studieplaner'),(224,8,'dutch','Curricula'),(225,8,'english','Learning plan'),(226,8,'farsi','Ø¨Ø±Ù†Ø§Ù…Ù‡ Ù‡Ø§'),(227,8,'finnish','Opinto-ohjelmat'),(228,8,'french','Programmes'),(229,8,'german','LernplÃ¤ne'),(230,8,'greek','Î”Î¹Î´Î±ÎºÏ„Î¹ÎºÎ¬ Ï€Ï�Î¿Î³Ï�Î¬Î¼Î¼Î±Ï„Î±'),(231,8,'hebrew','×ª×•×›× ×™×•×ª ×œ×™×ž×•×“×™×�'),(232,8,'hungarian','Tanterv'),(233,8,'indonesian','Kurikulum'),(234,8,'italian','Percorsi formativi'),(235,8,'japanese','ã‚«ãƒªã‚­ãƒ¥ãƒ©ãƒ '),(236,8,'korean','ì»¤ë¦¬í�˜ëŸ¼'),(237,8,'norwegian','Fagkrets'),(238,8,'polish','Program'),(239,8,'portuguese','CurrÃ­culo'),(240,8,'portuguese-br','Trilhas de Aprendizado'),(241,8,'romanian','Curricula'),(242,8,'russian','ÐŸÑ€Ð¾Ð³Ñ€Ð°Ð¼Ð¼Ð° Ð¾Ð±ÑƒÑ‡ÐµÐ½Ð¸Ñ�'),(243,8,'simplified_chinese','è¯¾ç¨‹'),(244,8,'spanish','Curricula'),(245,8,'swedish','LÃ¤roplan'),(246,8,'thai','à¸«à¸¥à¸±à¸�à¸ªà¸¹à¸•à¸£'),(247,8,'turkish','MÃ¼fredatlar'),(248,8,'ukrainian','Ð�Ð°Ð²Ñ‡Ð°Ð»ÑŒÐ½Ð¸Ð¹ Ð¿Ð»Ð°Ð½');
/*!40000 ALTER TABLE `core_menu_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_multidomain`
--

DROP TABLE IF EXISTS `core_multidomain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_multidomain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `domain` varchar(256) NOT NULL,
  `domain_type` varchar(32) NOT NULL,
  `org_chart` int(11) DEFAULT NULL,
  `logo` int(11) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `pageTitle` varchar(255) DEFAULT NULL,
  `companyLogo` int(11) DEFAULT NULL,
  `favicon` int(11) DEFAULT NULL,
  `colorScheme` int(11) DEFAULT NULL,
  `signInPageLayoutEnabled` int(11) DEFAULT NULL,
  `signInPageLayoutOption` varchar(32) DEFAULT NULL,
  `signInPageImage` int(11) DEFAULT NULL,
  `signInPageImageAspect` varchar(16) DEFAULT NULL,
  `signInPageWebPages` text,
  `customCss` text,
  `coursePlayerEnabled` int(11) DEFAULT NULL,
  `coursePlayerViewLayout` varchar(32) DEFAULT NULL,
  `coursePlayerImage` int(11) DEFAULT NULL,
  `coursePlayerImageAspect` varchar(16) DEFAULT NULL,
  `coursePlayerHtmlCss` text,
  `whiteLabelEnabled` int(11) DEFAULT NULL,
  `whiteLabelPoweredByOption` varchar(32) DEFAULT NULL,
  `whiteLabelPoweredByCustomText` varchar(255) DEFAULT NULL,
  `whiteLabelContactOption` varchar(32) DEFAULT NULL,
  `whiteLabelContactCustomEmail` varchar(255) DEFAULT NULL,
  `replaceDoceboWordOption` int(11) DEFAULT NULL,
  `replaceDoceboCustomWord` varchar(255) DEFAULT NULL,
  `replaceDoceboCustomLink` varchar(1024) DEFAULT NULL,
  `ecommerceEnabled` int(11) DEFAULT NULL,
  `currencySymbol` varchar(8) DEFAULT NULL,
  `currency` varchar(8) DEFAULT NULL,
  `payPalEnabled` int(11) DEFAULT NULL,
  `payPalAccount` varchar(255) DEFAULT NULL,
  `payPalSandbox` int(11) DEFAULT NULL,
  `authorizeNetEnabled` int(11) DEFAULT NULL,
  `authorizeNetLoginId` varchar(255) DEFAULT NULL,
  `authorizeNetTransactionKey` varchar(512) DEFAULT NULL,
  `authorizeNetMD5Hash` varchar(512) DEFAULT NULL,
  `authorizeNetSandbox` int(11) DEFAULT NULL,
  `httpsEnabled` int(11) DEFAULT NULL,
  `adyen_enabled` tinyint(1) DEFAULT NULL,
  `adyen_merchant_account` varchar(255) DEFAULT NULL,
  `adyen_hmac_key` varchar(255) DEFAULT NULL,
  `adyen_skin_code` varchar(255) DEFAULT NULL,
  `adyen_single_page_hpp` tinyint(1) DEFAULT NULL,
  `adyen_sandbox` tinyint(1) DEFAULT NULL,
  `catalog_custom_settings` tinyint(1) DEFAULT '0',
  `catalog_use_categories_tree` tinyint(1) DEFAULT '0',
  `catalog_external` tinyint(1) DEFAULT '0',
  `catalog_external_selected_catalogs` text,
  `whiteLabelHeader` varchar(255) DEFAULT 'current_header',
  `whiteLabelHeaderExtUrl` varchar(2000) DEFAULT NULL,
  `whiteLabelHeaderExtHeight` int(11) DEFAULT '50',
  `whiteLabelFooterExtUrl` varchar(2000) DEFAULT NULL,
  `whiteLabelFooterExtHeight` int(11) DEFAULT '30',
  `whitelabelNamingSiteEnable` tinyint(4) DEFAULT '0',
  `signInMinimalType` varchar(255) DEFAULT 'color',
  `signInMinimalBackground` varchar(255) DEFAULT '#ffffff',
  `signInMinimalVideoFallbackImg` varchar(255) DEFAULT NULL,
  `stripe_enabled` int(11) NOT NULL,
  `stripe_public_key` varchar(255) NOT NULL,
  `stripe_private_key` varchar(255) NOT NULL,
  `stripe_account_email` varchar(255) NOT NULL,
  `stripe_sandbox` int(11) NOT NULL,
  `stripe_public_key_test` varchar(255) NOT NULL,
  `stripe_private_key_test` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `node` (`org_chart`),
  CONSTRAINT `core_multidomain_ibfk_1` FOREIGN KEY (`org_chart`) REFERENCES `core_org_chart_tree` (`idOrg`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_multidomain`
--

LOCK TABLES `core_multidomain` WRITE;
/*!40000 ALTER TABLE `core_multidomain` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_multidomain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_multidomain_signin_text`
--

DROP TABLE IF EXISTS `core_multidomain_signin_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_multidomain_signin_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_multidomain` int(11) NOT NULL,
  `lang_code` varchar(32) NOT NULL COMMENT 'english, italian,...',
  `title` varchar(512) DEFAULT NULL,
  `text` text,
  PRIMARY KEY (`id`),
  KEY `id_multidomain` (`id_multidomain`),
  CONSTRAINT `core_multidomain_signin_text_ibfk_1` FOREIGN KEY (`id_multidomain`) REFERENCES `core_multidomain` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_multidomain_signin_text`
--

LOCK TABLES `core_multidomain_signin_text` WRITE;
/*!40000 ALTER TABLE `core_multidomain_signin_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_multidomain_signin_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_multidomain_webpage`
--

DROP TABLE IF EXISTS `core_multidomain_webpage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_multidomain_webpage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_multidomain` int(11) NOT NULL,
  `id_webpage` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_webpage` (`id_webpage`),
  KEY `id_multidomain` (`id_multidomain`),
  CONSTRAINT `core_multidomain_webpage_ibfk_1` FOREIGN KEY (`id_multidomain`) REFERENCES `core_multidomain` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `core_multidomain_webpage_ibfk_2` FOREIGN KEY (`id_webpage`) REFERENCES `learning_webpage` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_multidomain_webpage`
--

LOCK TABLES `core_multidomain_webpage` WRITE;
/*!40000 ALTER TABLE `core_multidomain_webpage` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_multidomain_webpage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_newsletter`
--

DROP TABLE IF EXISTS `core_newsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_send` int(11) NOT NULL DEFAULT '0',
  `sub` varchar(255) NOT NULL DEFAULT '',
  `msg` text NOT NULL,
  `fromemail` varchar(255) NOT NULL DEFAULT '',
  `language` varchar(255) NOT NULL DEFAULT '',
  `tot` int(11) NOT NULL DEFAULT '0',
  `send_type` varchar(255) NOT NULL DEFAULT 'email',
  `stime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `file` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_newsletter`
--

LOCK TABLES `core_newsletter` WRITE;
/*!40000 ALTER TABLE `core_newsletter` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_newsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_newsletter_sendto`
--

DROP TABLE IF EXISTS `core_newsletter_sendto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_newsletter_sendto` (
  `id_send` int(11) NOT NULL DEFAULT '0',
  `idst` int(11) NOT NULL DEFAULT '0',
  `stime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `processed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_send`,`idst`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_newsletter_sendto`
--

LOCK TABLES `core_newsletter_sendto` WRITE;
/*!40000 ALTER TABLE `core_newsletter_sendto` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_newsletter_sendto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_notification`
--

DROP TABLE IF EXISTS `core_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(64) NOT NULL,
  `recipient` varchar(32) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `schedule_type` varchar(32) DEFAULT NULL COMMENT 'after, before, at',
  `schedule_time` int(11) DEFAULT NULL COMMENT 'Hour of the day to send notification',
  `from_name` varchar(128) NOT NULL,
  `from_email` varchar(128) NOT NULL,
  `schedule_shift_period` varchar(16) DEFAULT NULL,
  `schedule_shift_number` int(11) DEFAULT NULL,
  `job_handler_id` varchar(255) DEFAULT NULL,
  `id_job` int(11) DEFAULT NULL,
  `id_author` int(11) DEFAULT NULL,
  `ufilter_option` int(11) DEFAULT NULL,
  `cfilter_option` int(11) DEFAULT NULL,
  `notify_type` varchar(255) NOT NULL DEFAULT 'email',
  `puProfileId` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `id_job` (`id_job`),
  KEY `id_author` (`id_author`),
  CONSTRAINT `core_notification_ibfk_id_author` FOREIGN KEY (`id_author`) REFERENCES `core_user` (`idst`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `core_notification_ibfk_id_job` FOREIGN KEY (`id_job`) REFERENCES `core_job` (`id_job`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_notification`
--

LOCK TABLES `core_notification` WRITE;
/*!40000 ALTER TABLE `core_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_notification_assoc`
--

DROP TABLE IF EXISTS `core_notification_assoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_notification_assoc` (
  `id` bigint(20) NOT NULL COMMENT 'Notification ID',
  `idItem` int(11) NOT NULL COMMENT 'Course/Plan ID',
  `type` int(11) NOT NULL COMMENT 'Item Type (1: course, 2:plan, ...)',
  UNIQUE KEY `id` (`id`,`idItem`,`type`),
  CONSTRAINT `core_notification_assoc_ibfk_1` FOREIGN KEY (`id`) REFERENCES `core_notification` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_notification_assoc`
--

LOCK TABLES `core_notification_assoc` WRITE;
/*!40000 ALTER TABLE `core_notification_assoc` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_notification_assoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_notification_translation`
--

DROP TABLE IF EXISTS `core_notification_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_notification_translation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `notification_id` bigint(20) NOT NULL,
  `language` varchar(32) NOT NULL COMMENT 'Language like "en", "it", "bg" (browser codes) ',
  `subject` varchar(512) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `notification_id_language` (`notification_id`,`language`),
  CONSTRAINT `core_notification_translation_ibfk_1` FOREIGN KEY (`notification_id`) REFERENCES `core_notification` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_notification_translation`
--

LOCK TABLES `core_notification_translation` WRITE;
/*!40000 ALTER TABLE `core_notification_translation` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_notification_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_notification_user_filter`
--

DROP TABLE IF EXISTS `core_notification_user_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_notification_user_filter` (
  `id` bigint(20) NOT NULL,
  `idItem` int(11) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1: Group, 2: Branch',
  `selectionState` int(11) DEFAULT NULL COMMENT 'For branches only (1,2)',
  UNIQUE KEY `id` (`id`,`idItem`,`type`),
  CONSTRAINT `core_notification_user_filter_ibfk_1` FOREIGN KEY (`id`) REFERENCES `core_notification` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_notification_user_filter`
--

LOCK TABLES `core_notification_user_filter` WRITE;
/*!40000 ALTER TABLE `core_notification_user_filter` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_notification_user_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_org_chart`
--

DROP TABLE IF EXISTS `core_org_chart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_org_chart` (
  `id_dir` int(11) NOT NULL DEFAULT '0',
  `lang_code` varchar(50) NOT NULL DEFAULT '',
  `translation` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_dir`,`lang_code`),
  CONSTRAINT `core_org_chart_ibfk_1` FOREIGN KEY (`id_dir`) REFERENCES `core_org_chart_tree` (`idOrg`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_org_chart`
--

LOCK TABLES `core_org_chart` WRITE;
/*!40000 ALTER TABLE `core_org_chart` DISABLE KEYS */;
INSERT INTO `core_org_chart` VALUES (0,'arabic','Docebo'),(0,'bosnian','Docebo'),(0,'bulgarian','Docebo'),(0,'croatian','Docebo'),(0,'czech','Docebo'),(0,'danish','Docebo'),(0,'dutch','Docebo'),(0,'english','Docebo'),(0,'english_uk',''),(0,'farsi','Docebo'),(0,'finnish','Docebo'),(0,'french','Docebo'),(0,'german','Docebo'),(0,'greek','Docebo'),(0,'hebrew','Docebo'),(0,'hungarian','Docebo'),(0,'indonesian','Docebo'),(0,'italian','Docebo'),(0,'japanese','Docebo'),(0,'korean','Docebo'),(0,'norwegian','Docebo'),(0,'polish','Docebo'),(0,'portuguese','Docebo'),(0,'portuguese-br','Docebo'),(0,'romanian','Docebo'),(0,'russian','Docebo'),(0,'simplified_chinese','Docebo'),(0,'slovenian',''),(0,'spanish','Docebo'),(0,'swedish','Docebo'),(0,'thai','Docebo'),(0,'turkish','Docebo'),(0,'ukrainian','Docebo');
/*!40000 ALTER TABLE `core_org_chart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_org_chart_branding`
--

DROP TABLE IF EXISTS `core_org_chart_branding`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_org_chart_branding` (
  `id_org` int(11) NOT NULL,
  `id_template` int(11) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `login_img` varchar(255) DEFAULT NULL,
  `player_bg` varchar(255) DEFAULT NULL,
  `custom_url` varchar(255) DEFAULT NULL,
  `catalog_external` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_org`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_org_chart_branding`
--

LOCK TABLES `core_org_chart_branding` WRITE;
/*!40000 ALTER TABLE `core_org_chart_branding` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_org_chart_branding` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_org_chart_tree`
--

DROP TABLE IF EXISTS `core_org_chart_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_org_chart_tree` (
  `idOrg` int(11) NOT NULL AUTO_INCREMENT,
  `idParent` int(11) NOT NULL DEFAULT '0',
  `path` text NOT NULL,
  `lev` int(3) NOT NULL DEFAULT '0',
  `iLeft` int(5) NOT NULL DEFAULT '0',
  `iRight` int(5) NOT NULL DEFAULT '0',
  `code` varchar(255) DEFAULT NULL,
  `idst_oc` int(11) NOT NULL DEFAULT '0',
  `idst_ocd` int(11) NOT NULL DEFAULT '0',
  `associated_policy` int(11) unsigned DEFAULT NULL,
  `associated_template` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idOrg`),
  KEY `idx_idst_oc` (`idst_oc`),
  KEY `idx_idst_ocd` (`idst_ocd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_org_chart_tree`
--

LOCK TABLES `core_org_chart_tree` WRITE;
/*!40000 ALTER TABLE `core_org_chart_tree` DISABLE KEYS */;
INSERT INTO `core_org_chart_tree` VALUES (0,0,'',1,1,2,'',1,2,NULL,NULL);
/*!40000 ALTER TABLE `core_org_chart_tree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_password_history`
--

DROP TABLE IF EXISTS `core_password_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_password_history` (
  `idst_user` int(11) NOT NULL DEFAULT '0',
  `pwd_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `passw` varchar(100) NOT NULL DEFAULT '',
  `changed_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idst_user`,`pwd_date`),
  KEY `pwd_date` (`pwd_date`),
  KEY `changed_by` (`changed_by`),
  CONSTRAINT `core_password_history_ibfk_1` FOREIGN KEY (`idst_user`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `core_password_history_ibfk_2` FOREIGN KEY (`changed_by`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_password_history`
--

LOCK TABLES `core_password_history` WRITE;
/*!40000 ALTER TABLE `core_password_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_password_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_plugin`
--

DROP TABLE IF EXISTS `core_plugin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_plugin` (
  `plugin_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `plugin_name` varchar(255) NOT NULL,
  `plugin_title` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `settings` text,
  PRIMARY KEY (`plugin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_plugin`
--

LOCK TABLES `core_plugin` WRITE;
/*!40000 ALTER TABLE `core_plugin` DISABLE KEYS */;
INSERT INTO `core_plugin` VALUES (1,'mobile','Mobile plugin',1,NULL),(2,'userlimit','User limit',1,NULL),(3,'customlogo','Custom logo',1,NULL),(4,'cloudfront','Cloudfront plugin',1,NULL),(5,'saas_billing','Saas billing',1,NULL),(6,'api_enterprise','Enterprise api plugin',1,NULL),(7,'joyride','How to tour',1,NULL),(8,'marketplace','Marketplace',1,NULL),(9,'NotificationApp','NotificationApp',0,NULL),(10,'SchedulerApp','SchedulerApp',1,NULL),(11,'GamificationApp','GamificationApp',1,'{\"show_not_awarded\":\"off\",\"enable_reward_shop\":\"on\"}');
/*!40000 ALTER TABLE `core_plugin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_pwd_recover`
--

DROP TABLE IF EXISTS `core_pwd_recover`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_pwd_recover` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idst_user` int(11) NOT NULL DEFAULT '0',
  `random_code` varchar(255) NOT NULL DEFAULT '',
  `request_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `idst_user` (`idst_user`),
  CONSTRAINT `core_pwd_recover_ibfk_1` FOREIGN KEY (`idst_user`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_pwd_recover`
--

LOCK TABLES `core_pwd_recover` WRITE;
/*!40000 ALTER TABLE `core_pwd_recover` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_pwd_recover` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_rest_authentication`
--

DROP TABLE IF EXISTS `core_rest_authentication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_rest_authentication` (
  `id_user` int(11) NOT NULL DEFAULT '0',
  `user_level` int(11) NOT NULL DEFAULT '0',
  `token` varchar(255) NOT NULL DEFAULT '',
  `generation_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_enter_date` timestamp NULL DEFAULT NULL,
  `expiry_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_rest_authentication`
--

LOCK TABLES `core_rest_authentication` WRITE;
/*!40000 ALTER TABLE `core_rest_authentication` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_rest_authentication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_role`
--

DROP TABLE IF EXISTS `core_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_role` (
  `idst` int(11) NOT NULL DEFAULT '0',
  `roleid` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idst`),
  KEY `roleid` (`roleid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_role`
--

LOCK TABLES `core_role` WRITE;
/*!40000 ALTER TABLE `core_role` DISABLE KEYS */;
INSERT INTO `core_role` VALUES (7,'/framework/admin/adminmanager/mod',NULL),(8,'/framework/admin/adminmanager/view',NULL),(9,'/framework/admin/adminrules/view',NULL),(10,'/framework/admin/dashboard/view',NULL),(11,'/framework/admin/dashboard/view',NULL),(12,'/framework/admin/directory/approve_waiting_user',NULL),(13,'/framework/admin/event_manager/view_event_manager',NULL),(14,'/framework/admin/field_manager/add',NULL),(15,'/framework/admin/field_manager/del',NULL),(16,'/framework/admin/field_manager/mod',NULL),(17,'/framework/admin/field_manager/view',NULL),(18,'/framework/admin/groupmanagement/add',NULL),(19,'/framework/admin/groupmanagement/associate_user',NULL),(20,'/framework/admin/groupmanagement/del',NULL),(21,'/framework/admin/groupmanagement/mod',NULL),(22,'/framework/admin/groupmanagement/view',NULL),(23,'/framework/admin/iotask/view',NULL),(24,'/framework/admin/kb/mod',NULL),(25,'/framework/admin/kb/view',NULL),(26,'/framework/admin/lang/mod',NULL),(27,'/framework/admin/lang/view',NULL),(28,'/framework/admin/newsletter/view',NULL),(29,'/framework/admin/publicadminmanager/mod',NULL),(30,'/framework/admin/publicadminmanager/view',NULL),(31,'/framework/admin/usermanagement/add',NULL),(32,'/framework/admin/usermanagement/approve_waiting_user',NULL),(33,'/framework/admin/usermanagement/del',NULL),(34,'/framework/admin/usermanagement/mod',NULL),(35,'/framework/admin/usermanagement/view',NULL),(36,'/lms/admin/amanmenu/mod',NULL),(37,'/lms/admin/amanmenu/view',NULL),(38,'/lms/admin/catalogue/mod',NULL),(39,'/lms/admin/catalogue/view',NULL),(40,'/lms/admin/certificate/mod',NULL),(41,'/lms/admin/certificate/view',NULL),(42,'/lms/admin/classroom/view',NULL),(43,'/lms/admin/communication/mod',NULL),(44,'/lms/admin/communication/view',NULL),(45,'/lms/admin/course/add',NULL),(46,'/lms/admin/course/del',NULL),(47,'/lms/admin/course/mod',NULL),(48,'/lms/admin/course/moderate',NULL),(49,'/lms/admin/course/subscribe',NULL),(50,'/lms/admin/course/view',NULL),(51,'/lms/admin/coursepath/mod',NULL),(53,'/lms/admin/coursepath/subscribe',NULL),(54,'/lms/admin/coursepath/view',NULL),(55,'/lms/admin/enrollrules/view',NULL),(56,'/lms/admin/games/mod',NULL),(57,'/lms/admin/games/subscribe',NULL),(58,'/lms/admin/games/view',NULL),(59,'/lms/admin/internal_news/mod',NULL),(60,'/lms/admin/internal_news/view',NULL),(61,'/lms/admin/kb/view',NULL),(62,'/lms/admin/label/view',NULL),(63,'/lms/admin/middlearea/view',NULL),(64,'/lms/admin/news/mod',NULL),(65,'/lms/admin/news/view',NULL),(66,'/lms/admin/preassessment/mod',NULL),(67,'/lms/admin/preassessment/subscribe',NULL),(68,'/lms/admin/preassessment/view',NULL),(69,'/lms/admin/questcategory/mod',NULL),(70,'/lms/admin/questcategory/view',NULL),(71,'/lms/admin/report/mod',NULL),(72,'/lms/admin/report/view',NULL),(73,'/lms/admin/reservation/mod',NULL),(74,'/lms/admin/reservation/view',NULL),(75,'/lms/admin/timeperiods/mod',NULL),(76,'/lms/admin/timeperiods/view',NULL),(77,'/lms/admin/transaction/view',NULL),(78,'/lms/admin/webpages/mod',NULL),(79,'/lms/admin/webpages/view',NULL),(80,'/lms/course/public/course/view',NULL),(81,'/lms/course/public/course_autoregistration/view',NULL),(82,'/lms/course/public/coursecatalogue/view',NULL),(83,'/lms/course/public/message/send_all',NULL),(84,'/lms/course/public/message/view',NULL),(85,'/lms/course/public/mycertificate/view',NULL),(86,'/lms/course/public/mycompetences/view',NULL),(87,'/lms/course/public/mygroup/view',NULL),(88,'/lms/course/public/profile/mod',NULL),(89,'/lms/course/public/profile/view',NULL),(90,'/lms/course/public/tprofile/view',NULL),(91,'/lms/course/public/public_forum/view',NULL),(92,'/lms/course/public/public_forum/add',NULL),(93,'/lms/course/public/public_forum/del',NULL),(94,'/lms/course/public/public_forum/mod',NULL),(95,'/lms/course/public/public_forum/moderate',NULL),(96,'/lms/course/public/public_forum/upload',NULL),(97,'/lms/course/public/public_forum/write',NULL),(98,'/lms/course/public/pcourse/add',NULL),(99,'/lms/course/public/pcourse/del',NULL),(100,'/lms/course/public/pcourse/mod',NULL),(101,'/lms/course/public/pcourse/moderate',NULL),(102,'/lms/course/public/pcourse/subscribe',NULL),(103,'/lms/course/public/pcourse/view',NULL),(104,'/lms/course/public/public_newsletter_admin/view',NULL),(105,'/lms/course/public/public_report_admin/view',NULL),(106,'/lms/course/public/public_subscribe_admin/approve_waiting_user',NULL),(107,'/lms/course/public/public_subscribe_admin/createuser_org_chart',NULL),(108,'/lms/course/public/public_subscribe_admin/deluser_org_chart',NULL),(109,'/lms/course/public/public_subscribe_admin/edituser_org_chart',NULL),(110,'/lms/course/public/public_subscribe_admin/view_org_chart',NULL),(111,'/framework/admin/functionalroles/view',NULL),(112,'/framework/admin/functionalroles/mod',NULL),(113,'/framework/admin/functionalroles/associate_user',NULL),(114,'/framework/admin/competences/view',NULL),(115,'/framework/admin/competences/mod',NULL),(116,'/framework/admin/competences/associate_user',NULL),(117,'/framework/admin/publicadminrules/view',NULL),(118,'/framework/admin/code/view',NULL),(119,'/framework/admin/setting/view',NULL),(120,'/lms/admin/meta_certificate/view',NULL),(121,'/lms/admin/meta_certificate/mod',NULL),(122,'/framework/admin/usermanagement/mod_org',NULL),(123,'/lms/course/public/pusermanagement/view',NULL),(124,'/lms/course/public/pusermanagement/add',NULL),(125,'/lms/course/public/pusermanagement/mod',NULL),(126,'/lms/course/public/pusermanagement/del',NULL),(127,'/lms/course/public/pusermanagement/approve_waiting_user',NULL),(175,'/lms/course/private/advice/mod',NULL),(176,'/lms/course/private/advice/view',NULL),(177,'/lms/course/private/calendar/mod',NULL),(178,'/lms/course/private/calendar/personal',NULL),(179,'/lms/course/private/calendar/view',NULL),(180,'/lms/course/private/chat/view',NULL),(181,'/lms/course/private/conference/mod',NULL),(182,'/lms/course/private/conference/view',NULL),(183,'/lms/course/private/course/mod',NULL),(184,'/lms/course/private/course/view',NULL),(185,'/lms/course/private/course/view_info',NULL),(186,'/lms/course/private/coursereport/mod',NULL),(187,'/lms/course/private/coursereport/view',NULL),(188,'/lms/course/private/forum/add',NULL),(189,'/lms/course/private/forum/del',NULL),(190,'/lms/course/private/forum/mod',NULL),(191,'/lms/course/private/forum/moderate',NULL),(192,'/lms/course/private/forum/upload',NULL),(193,'/lms/course/private/forum/view',NULL),(194,'/lms/course/private/forum/write',NULL),(195,'/lms/course/private/gradebook/view',NULL),(196,'/lms/course/private/groups/mod',NULL),(197,'/lms/course/private/groups/subscribe',NULL),(198,'/lms/course/private/groups/view',NULL),(199,'/lms/course/private/htmlfront/mod',NULL),(200,'/lms/course/private/htmlfront/view',NULL),(201,'/lms/course/private/light_repo/mod',NULL),(202,'/lms/course/private/light_repo/view',NULL),(203,'/lms/course/private/manmenu/mod',NULL),(204,'/lms/course/private/manmenu/view',NULL),(205,'/lms/course/private/newsletter/view',NULL),(206,'/lms/course/private/notes/view',NULL),(207,'/lms/course/private/organization/view',NULL),(208,'/lms/course/private/project/add',NULL),(209,'/lms/course/private/project/del',NULL),(210,'/lms/course/private/project/mod',NULL),(211,'/lms/course/private/project/view',NULL),(212,'/lms/course/private/quest_bank/mod',NULL),(213,'/lms/course/private/quest_bank/view',NULL),(214,'/lms/course/private/reservation/mod',NULL),(215,'/lms/course/private/reservation/view',NULL),(216,'/lms/course/private/statistic/view',NULL),(217,'/lms/course/private/stats/view_course',NULL),(218,'/lms/course/private/stats/view_user',NULL),(219,'/lms/course/private/storage/home',NULL),(220,'/lms/course/private/storage/lesson',NULL),(221,'/lms/course/private/storage/public',NULL),(222,'/lms/course/private/storage/view',NULL),(223,'/lms/course/private/wiki/admin',NULL),(224,'/lms/course/private/wiki/edit',NULL),(225,'/lms/course/private/wiki/view',NULL),(226,'/lms/admin/location/view',NULL),(227,'/lms/admin/location/mod',NULL),(272,'/lms/course/private/coursecharts/view',''),(11553,'/framework/admin/usermanagement/associate_user',NULL),(11612,'/lms/course/public/pcertificate/view',NULL),(11613,'/lms/course/public/pcertificate/mod',NULL),(11757,'/lms/course/private/coursestats/view',''),(11835,'/lms/course/private/presence/view',''),(11836,'/framework/admin/privacypolicy/view',NULL),(12302,'/lms/course/public/mycalendar/view',NULL),(12304,'/framework/admin/gamificationapp/view',NULL),(12312,'/framework/admin/transcripts/view',NULL),(12313,'/framework/admin/transcripts/mod',NULL),(12369,'/lms/admin/classroomsessions/view',NULL),(12370,'/lms/admin/classroomsessions/mod',NULL),(12403,'/lms/admin/classroomsessions/add',NULL),(12412,'/lms/admin/webinarsessions/view',NULL),(12413,'/lms/admin/webinarsessions/mod',NULL),(12414,'/lms/admin/webinarsessions/add',NULL),(13003,'/framework/admin/catalogManagement/add',NULL),(13004,'/framework/admin/catalogManagement/view',NULL),(13005,'/framework/admin/catalogManagement/mod',NULL),(13006,'/framework/admin/catalogManagement/del',NULL), (13007,'enrollment/create',NULL), (13008,'enrollment/view',NULL), (13009,'enrollment/update',NULL), (13010,'enrollment/delete',NULL);
/*!40000 ALTER TABLE `core_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_role_members`
--

DROP TABLE IF EXISTS `core_role_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_role_members` (
  `idst` int(11) NOT NULL DEFAULT '0',
  `idstMember` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idst`,`idstMember`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_role_members`
--

LOCK TABLES `core_role_members` WRITE;
/*!40000 ALTER TABLE `core_role_members` DISABLE KEYS */;
INSERT INTO `core_role_members` VALUES (7,3),(8,3),(9,3),(10,3),(11,3),(12,3),(13,3),(14,3),(15,3),(16,3),(17,3),(18,3),(19,3),(20,3),(21,3),(22,3),(23,3),(24,3),(25,3),(26,3),(27,3),(28,3),(29,3),(30,3),(31,3),(31,12301),(32,3),(33,3),(34,3),(34,12301),(35,3),(35,12301),(36,3),(37,3),(38,3),(39,3),(40,3),(41,3),(42,3),(43,3),(44,3),(45,3),(45,12301),(46,3),(47,3),(47,12301),(48,3),(48,12301),(49,3),(49,12301),(50,3),(50,12301),(51,3),(52,3),(53,3),(54,3),(55,3),(56,3),(57,3),(58,3),(59,3),(60,3),(61,3),(62,3),(63,3),(64,3),(65,3),(66,3),(67,3),(68,3),(69,3),(70,3),(71,3),(71,12301),(72,3),(72,12301),(73,3),(74,3),(75,3),(76,3),(77,3),(78,3),(79,3),(80,1),(81,1),(82,1),(83,1),(84,1),(85,1),(86,1),(87,1),(88,1),(89,1),(90,1),(91,1),(92,3),(93,3),(94,3),(95,3),(96,1),(96,3),(97,1),(97,3),(111,3),(112,3),(113,3),(114,3),(115,3),(116,3),(117,3),(118,3),(119,3),(120,3),(121,3),(122,3),(175,301),(175,302),(175,303),(175,304),(176,301),(176,302),(176,303),(176,304),(176,305),(176,306),(176,307),(177,301),(177,302),(177,303),(177,304),(178,301),(178,302),(178,303),(178,304),(178,305),(179,301),(179,302),(179,303),(179,304),(179,305),(179,306),(179,307),(180,301),(180,302),(180,303),(180,304),(180,305),(181,301),(181,302),(181,303),(181,304),(182,301),(182,302),(182,303),(182,304),(182,305),(182,306),(182,307),(183,301),(183,302),(183,303),(183,304),(185,301),(185,302),(185,303),(185,304),(185,305),(185,306),(185,307),(186,301),(186,302),(186,303),(186,304),(186,10893),(186,10894),(186,10895),(187,301),(187,302),(187,303),(187,304),(187,10893),(187,10894),(187,10895),(187,10896),(187,10897),(187,10898),(187,10899),(188,301),(188,302),(188,303),(188,304),(189,301),(189,302),(189,303),(189,304),(190,301),(190,302),(190,303),(190,304),(191,301),(191,302),(191,303),(191,304),(192,301),(192,302),(192,303),(192,304),(192,305),(193,301),(193,302),(193,303),(193,304),(193,305),(193,306),(193,307),(194,301),(194,302),(194,303),(194,304),(194,305),(195,301),(195,302),(195,303),(195,304),(195,305),(195,10893),(195,10894),(195,10895),(195,10896),(195,10897),(195,10898),(195,10899),(196,301),(196,302),(196,303),(196,304),(197,301),(197,302),(197,303),(197,304),(198,301),(198,302),(198,303),(198,304),(201,301),(201,302),(201,303),(201,304),(202,301),(202,302),(202,303),(202,304),(202,305),(205,301),(205,302),(205,303),(205,304),(206,301),(206,302),(206,303),(206,304),(206,305),(206,306),(206,307),(207,301),(207,302),(207,303),(207,304),(207,305),(207,306),(207,307),(207,10893),(207,10894),(207,10895),(207,10896),(207,10897),(207,10898),(208,301),(208,302),(208,303),(208,304),(209,301),(209,302),(209,303),(209,304),(210,301),(210,302),(210,303),(210,304),(211,301),(211,302),(211,303),(211,304),(211,305),(211,306),(211,307),(216,301),(216,302),(216,303),(216,304),(216,10893),(216,10894),(216,10895),(216,10896),(217,301),(217,302),(217,303),(217,304),(217,10893),(217,10894),(217,10895),(217,10896),(218,301),(218,302),(218,303),(218,304),(218,10893),(218,10894),(218,10895),(218,10896),(219,301),(219,302),(219,10893),(219,10894),(220,301),(220,302),(220,303),(220,304),(220,10893),(220,10894),(220,10895),(221,301),(221,302),(221,10893),(221,10894),(222,301),(222,302),(222,303),(222,304),(222,10893),(222,10894),(222,10895),(223,301),(223,302),(223,303),(223,304),(224,301),(224,302),(224,303),(224,304),(224,305),(225,301),(225,302),(225,303),(225,304),(225,305),(225,306),(225,307),(226,3),(227,3),(272,301),(272,302),(272,303),(272,304),(272,307),(272,10893),(272,10894),(272,10895),(272,10896),(11553,3),(11612,3),(11613,3),(11757,301),(11757,302),(11757,303),(11757,304),(11757,10893),(11757,10894),(11757,10895),(11757,10896),(11835,301),(11835,302),(11835,303),(11835,304),(11836,3),(12302,1),(12304,3),(12312,3),(12313,3);
/*!40000 ALTER TABLE `core_role_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_rules`
--

DROP TABLE IF EXISTS `core_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_rules` (
  `id_rule` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `lang_code` varchar(255) NOT NULL DEFAULT '',
  `rule_type` varchar(10) NOT NULL DEFAULT '',
  `creation_date` date NOT NULL DEFAULT '0000-00-00',
  `rule_active` tinyint(1) NOT NULL DEFAULT '0',
  `course_list` text NOT NULL,
  PRIMARY KEY (`id_rule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_rules`
--

LOCK TABLES `core_rules` WRITE;
/*!40000 ALTER TABLE `core_rules` DISABLE KEYS */;
INSERT INTO `core_rules` VALUES (0,'','all','base','0000-00-00',1,'');
/*!40000 ALTER TABLE `core_rules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_rules_entity`
--

DROP TABLE IF EXISTS `core_rules_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_rules_entity` (
  `id_rule` int(11) NOT NULL DEFAULT '0',
  `id_entity` varchar(50) NOT NULL DEFAULT '',
  `course_list` text NOT NULL,
  PRIMARY KEY (`id_rule`,`id_entity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_rules_entity`
--

LOCK TABLES `core_rules_entity` WRITE;
/*!40000 ALTER TABLE `core_rules_entity` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_rules_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_rules_log`
--

DROP TABLE IF EXISTS `core_rules_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_rules_log` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `log_action` varchar(255) NOT NULL DEFAULT '',
  `log_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `applied` text NOT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_rules_log`
--

LOCK TABLES `core_rules_log` WRITE;
/*!40000 ALTER TABLE `core_rules_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_rules_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_running_operation`
--

DROP TABLE IF EXISTS `core_running_operation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_running_operation` (
  `id_operation` varchar(50) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `time_start` timestamp NULL DEFAULT NULL,
  `time_start_ms` int(11) DEFAULT NULL,
  `time_end` timestamp NULL DEFAULT NULL,
  `time_end_ms` int(11) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_operation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_running_operation`
--

LOCK TABLES `core_running_operation` WRITE;
/*!40000 ALTER TABLE `core_running_operation` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_running_operation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_scheme`
--

DROP TABLE IF EXISTS `core_scheme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_scheme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `is_custom` tinyint(1) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_scheme`
--

LOCK TABLES `core_scheme` WRITE;
/*!40000 ALTER TABLE `core_scheme` DISABLE KEYS */;
INSERT INTO `core_scheme` VALUES (1,'Scheme 1 - Docebo',1,1),(2,'Scheme 2 - Orange',1,0),(3,'Scheme 3 - Blue Grey',1,0),(4,'Scheme 4 - Dark Blue',1,0),(5,'Scheme 5 - Dark Gold',1,0),(6,'Scheme 6 - Grey Orange',1,0),(7,'Scheme 7 - Blue Violet',1,0),(8,'Scheme 8 - Light Green',1,0);
/*!40000 ALTER TABLE `core_scheme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_scheme_color`
--

DROP TABLE IF EXISTS `core_scheme_color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_scheme_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `color` varchar(7) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_scheme_color`
--

LOCK TABLES `core_scheme_color` WRITE;
/*!40000 ALTER TABLE `core_scheme_color` DISABLE KEYS */;
INSERT INTO `core_scheme_color` VALUES (1,'Menu & Headers','Mainly used in Menu Bar and popup headers','#333333'),(2,'Title text','Used for all the titles','#0465AC'),(3,'Text','Main text colour','#333333'),(4,'Hover item','Used for hover items','#666666'),(5,'Selected items','Used for selected items','#333333'),(6,'Action buttons','Used for buttons like confirm, save, add...','#333333'),(7,'Secondary buttons','Used for buttons like cancel, previous, go back, etc...','#333333'),(8,'Other buttons','Used for other buttons','#666666'),(9,'Charts 1','Mainly used in charts','#003D6B'),(10,'Charts 2','Mainly used in charts','#0465AC'),(11,'Charts 3','Mainly used in charts','#52A1DD'),(12,'Menu header background','Background color for menu header','#555555');
/*!40000 ALTER TABLE `core_scheme_color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_scheme_color_selection`
--

DROP TABLE IF EXISTS `core_scheme_color_selection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_scheme_color_selection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scheme_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `color` varchar(7) NOT NULL,
  PRIMARY KEY (`id`,`scheme_id`,`color_id`),
  KEY `scheme_id` (`scheme_id`),
  KEY `color_id` (`color_id`),
  CONSTRAINT `core_scheme_color_selection_ibfk_1` FOREIGN KEY (`scheme_id`) REFERENCES `core_scheme` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `core_scheme_color_selection_ibfk_2` FOREIGN KEY (`color_id`) REFERENCES `core_scheme_color` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_scheme_color_selection`
--

LOCK TABLES `core_scheme_color_selection` WRITE;
/*!40000 ALTER TABLE `core_scheme_color_selection` DISABLE KEYS */;
INSERT INTO `core_scheme_color_selection` VALUES (1,1,1,'#333333'),(2,1,2,'#333333'),(3,1,3,'#333333'),(4,1,4,'#666666'),(5,1,5,'#333333'),(6,1,6,'#5FBF5F'),(7,1,7,'#666666'),(8,1,8,'#333333'),(9,1,9,'#003D6A'),(10,1,10,'#0364AB'),(11,1,11,'#52A1DC'),(12,2,1,'#333333'),(13,2,2,'#333333'),(14,2,3,'#333333'),(15,2,4,'#F27C10'),(16,2,5,'#333333'),(17,2,6,'#F27C10'),(18,2,7,'#FFAB2E'),(19,2,8,'#333333'),(20,2,9,'#E7792C'),(21,2,10,'#F47F0E'),(22,2,11,'#FFAB2E'),(23,3,1,'#666666'),(24,3,2,'#333333'),(25,3,3,'#333333'),(26,3,4,'#58807F'),(27,3,5,'#666666'),(28,3,6,'#58807F'),(29,3,7,'#58807F'),(30,3,8,'#666666'),(31,3,9,'#666666'),(32,3,10,'#568380'),(33,3,11,'#BAC7B6'),(34,4,1,'#1A4266'),(35,4,2,'#333333'),(36,4,3,'#333333'),(37,4,4,'#0465AC'),(38,4,5,'#1A4266'),(39,4,6,'#E84C3D'),(40,4,7,'#0465AC'),(41,4,8,'#1A4266'),(42,4,9,'#0465AC'),(43,4,10,'#52A1DC'),(44,4,11,'#E84C3D'),(45,5,1,'#332E11'),(46,5,2,'#33333'),(47,5,3,'#333333'),(48,5,4,'#403B15'),(49,5,5,'#332E11'),(50,5,6,'#8C802E'),(51,5,7,'#403B15'),(52,5,8,'#332E11'),(53,5,9,'#403B15'),(54,5,10,'#4D4419'),(55,5,11,'#8B8031'),(56,6,1,'#333333'),(57,6,2,'#333333'),(58,6,3,'#333333'),(59,6,4,'#FFAB2E'),(60,6,5,'#333333'),(61,6,6,'#FFAB2E'),(62,6,7,'#808E8F'),(63,6,8,'#333333'),(64,6,9,'#333333'),(65,6,10,'#7E8C8D'),(66,6,11,'#95A5A5'),(67,7,1,'#3E618B'),(68,7,2,'#333333'),(69,7,3,'#333333'),(70,7,4,'#5CACE1'),(71,7,5,'#3E618B'),(72,7,6,'#AE7AC4'),(73,7,7,'#668CBB'),(74,7,8,'#3E618B'),(75,7,9,'#3D608A'),(76,7,10,'#668CBB'),(77,7,11,'#5BACE1'),(78,8,1,'#34495E'),(79,8,2,'#333333'),(80,8,3,'#333333'),(81,8,4,'#16A086'),(82,8,5,'#34495E'),(83,8,6,'#E67F22'),(84,8,7,'#16A086'),(85,8,8,'#34495E'),(86,8,9,'#14A085'),(87,8,10,'#57D78D'),(88,8,11,'#E67F22'),(89,1,12,'#555555'),(90,2,12,'#555555'),(91,3,12,'#555555'),(92,4,12,'#555555'),(93,5,12,'#555555'),(94,6,12,'#555555'),(95,7,12,'#555555'),(96,8,12,'#555555');
/*!40000 ALTER TABLE `core_scheme_color_selection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_setting`
--

DROP TABLE IF EXISTS `core_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_setting` (
  `param_name` varchar(255) NOT NULL DEFAULT '',
  `param_value` text NOT NULL,
  `value_type` varchar(25) NOT NULL DEFAULT 'string',
  `max_size` int(3) NOT NULL DEFAULT '255',
  `pack` varchar(25) NOT NULL DEFAULT 'main',
  `regroup` int(5) NOT NULL DEFAULT '0',
  `sequence` int(5) NOT NULL DEFAULT '0',
  `param_load` tinyint(1) NOT NULL DEFAULT '1',
  `hide_in_modify` tinyint(1) NOT NULL DEFAULT '0',
  `extra_info` text NOT NULL,
  PRIMARY KEY (`param_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_setting`
--

LOCK TABLES `core_setting` WRITE;
/*!40000 ALTER TABLE `core_setting` DISABLE KEYS */;
INSERT INTO `core_setting` VALUES ('accessibility','off','enum',255,'0',8,5,1,0,''),('admin_version','basic','string',255,'',0,0,1,1,''),('allow_any_length','off','share7020settings',255,'main',0,0,1,0,''),('allow_assets_dowload','on','share7020settings',3,'',0,0,1,1,''),('app7020_ask_button_lo','off','enum',255,'main',0,0,1,0,''),('app7020_limit_characters_question','0','int',255,'main',0,0,1,0,''),('authorize_enabled','off','enum',3,'0',5,3,1,1,''),('catalog_enabled','0','int',1,'0',0,0,1,1,''),('catalog_type','catalog','string',255,'0',0,0,1,1,''),('catalog_use_categories_tree','off','enum',3,'',0,0,1,1,''),('certificate_real_size','off','enum',3,'main',4,13,1,0,''),('common_admin_session','on','enum',3,'security',8,24,1,0,''),('company_logo','','string',255,'',0,0,1,1,''),('core_version','6.8','string',255,'0',1,0,1,1,''),('cost_matrix','[{\"10\":0,\"50\":3.98,\"100\":2.75,\"250\":2.13,\"500\":1.15,\"1000\":0.83,\"3500\":0.35,\"9999999\":0.16}]','string',65000,'0',0,0,1,1,''),('course_quota','0','string',255,'0',4,5,1,0,''),('credits_balance','0','string',255,'0',0,0,1,1,''),('currency_code','EUR','string',255,'',0,0,1,1,''),('currency_symbol','&euro;','string',10,'0',5,2,1,1,''),('customer_help_email','','string',255,'0',3,19,1,0,''),('customer_help_subj_pfx','','string',255,'0',3,20,1,0,''),('custom_css','','text',5000,'',0,0,1,1,''),('date_format','browser_code','string',255,'',0,0,1,1,''),('date_format_custom','','string',255,'',0,0,1,1,''),('date_format_custom_lang','it','string',255,'',0,0,1,1,''),('defaultTemplate','standard','template',255,'0',1,4,1,0,''),('default_color_scheme','1','colorscheme',255,'0',1,6,1,0,''),('default_language','english','language',255,'0',1,3,1,0,''),('disable_registration_email_confirmation','off','enum',3,'register',3,15,0,0,''),('documents_contribution','off','share7020settings',255,'main',0,0,1,0,''),('do_debug','off','enum',3,'debug',8,8,1,0,''),('elements_per_page','10','int',5,'advanced',0,0,1,0,''),('enable_answer_questions','off','enum',255,'main',0,0,1,0,''),('enable_email_verification','off','string',3,'',0,0,1,1,''),('enable_legacy_menu','off','string',3,'',0,0,1,1,''),('enable_user_billing','on','enum',3,'main',1,0,1,1,''),('expiration_date','2020-12-08 00:00:00','string',255,'0',0,0,1,1,''),('file_extensions','','share7020settings',255,'main',0,0,1,0,''),('file_upload_whitelist','zip,doc,xls,ppt,jpg,gif,png,txt,docx,pptx,xlsx,pdf,mp4,flv,ods,odt,odp,csv','string',65535,'security',8,25,1,1,''),('first_catalogue','off','enum',3,'0',4,2,1,1,''),('force_saas_https_redirect','1','int',5,'0',0,0,1,0,''),('forum_as_table','on','enum',3,'0',4,4,1,0,''),('free_users','5','string',255,'main',0,0,1,1,''),('google_stat_code','','textarea',65535,'0',10,2,1,0,''),('google_stat_in_lms','0','check',1,'0',10,1,1,0,''),('home_login_img','63','string',255,'',0,0,1,0,''),('home_login_img_position','tile','string',255,'',0,0,1,0,''),('home_login_show_catalog','off','enum',3,'0',4,2,1,1,''),('home_login_show_news','off','enum',3,'0',4,2,1,1,''),('hour_request_limit','48','int',2,'register',3,13,0,0,''),('hteditor','tinymce','hteditor',255,'0',1,8,1,0,''),('htmledit_image_admin','1','check',255,'0',8,1,1,0,''),('htmledit_image_godadmin','1','check',255,'0',8,0,1,0,''),('htmledit_image_user','1','check',255,'0',8,2,1,0,''),('invite_watch','on','share7020settings',255,'main',0,0,1,0,''),('kb_filter_by_user_access','on','enum',3,'main',4,10,1,0,''),('kb_show_uncategorized','on','enum',3,'main',4,11,1,0,''),('lang_check','off','enum',3,'debug',8,7,1,0,''),('lastfirst_mandatory','off','enum',3,'register',3,14,2,0,''),('ldap_port','389','string',5,'0',7,3,1,0,''),('ldap_server','192.168.0.1','string',255,'0',7,2,1,0,''),('ldap_used','off','enum',3,'0',7,1,1,0,''),('ldap_user_string','$user@domain2.domain1','string',255,'0',7,4,1,0,''),('legacy_api_authentication','on','enum',3,'0',9,0,1,0,''),('limit_questions','off','enum',255,'main',0,0,1,0,''),('login_layout','layout3','string',255,'',0,0,1,0,''),('mail_sender','','string',255,'register',3,12,0,0,''),('mandatory_code','off','enum',3,'register',3,18,1,0,''),('max_log_attempt','0','int',3,'0',3,4,0,0,''),('max_users','0','string',255,'0',0,0,1,1,''),('minimal_login_background','#ffffff','string',255,'',0,0,1,1,''),('minimal_login_type','color','string',255,'',0,0,1,1,''),('minimal_login_video_fallback_img','','string',255,'',0,0,1,1,''),('nl_sendpause','20','int',3,'newsletter',8,10,1,0,''),('nl_sendpercycle','200','int',4,'newsletter',8,9,1,0,''),('no_answer_in_poll','off','enum',3,'0',4,7,1,0,''),('no_answer_in_test','off','enum',3,'0',4,6,1,0,''),('on_catalogue_empty','on','enum',3,'0',4,3,1,1,''),('other_contribution','off','share7020settings',255,'main',0,0,1,0,''),('page_title','Docebo E-learning','string',255,'0',1,1,1,0,''),('pass_alfanumeric','off','enum',3,'password',3,6,1,0,''),('pass_change_first_login','off','enum',3,'password',3,8,1,0,''),('pass_max_time_valid','0','int',4,'password',3,9,1,0,''),('pass_min_char','6','int',2,'password',3,7,0,0,''),('pass_not_username','off','enum',3,'password',3,6,1,0,''),('pass_special_chars','no','enum',10,'password',3,0,1,0,''),('pathchat','chat/','string',255,'path',8,21,1,1,''),('pathcourse','course/','string',255,'path',8,11,1,1,''),('pathfield','field/','string',255,'path',8,12,1,1,''),('pathforum','forum/','string',255,'path',8,14,1,1,''),('pathlesson','item/','string',255,'path',8,15,1,1,''),('pathmessage','message/','string',255,'path',8,16,1,1,''),('pathphoto','photo/','string',255,'path',8,13,1,1,''),('pathprj','project/','string',255,'path',8,20,1,1,''),('pathscorm','scorm/','string',255,'path',8,17,1,1,''),('pathsponsor','sponsor/','string',255,'path',8,18,1,1,''),('pathtest','test/','string',255,'path',8,19,1,1,''),('paypal_currency','EUR','string',255,'0',5,1,1,1,''),('paypal_enabled','on','enum',3,'0',5,3,1,1,''),('paypal_mail','','string',255,'0',5,0,1,1,''),('paypal_sandbox','off','enum',3,'0',5,3,1,1,''),('privacy_policy','on','enum',3,'register',3,15,0,0,''),('profile_only_pwd','on','enum',3,'0',3,1,1,0,''),('register_deleted_user','off','enum',3,'0',3,3,1,0,''),('register_type','self','register_type',10,'register',3,11,0,0,''),('registration_code_type','tree_drop','registration_code_type',3,'register',3,17,1,0,''),('request_mandatory_fields_compilation','off','enum',3,'0',3,2,1,0,''),('rest_auth_code','','string',255,'api',9,4,1,0,''),('rest_auth_lifetime','60','int',3,'api',9,5,1,0,''),('rest_auth_method','1','rest_auth_sel_method',3,'api',9,3,1,0,''),('rest_auth_update','off','enum',3,'api',9,6,1,0,''),('return_path','','string',255,'main',0,0,1,0,''),('saas_installation_status','0','int',1,'',0,0,1,1,''),('save_log_attempt','no','save_log_attempt',255,'0',3,5,0,0,''),('sco_direct_play','on','enum',3,'0',8,3,1,0,''),('selected_color_scheme','1','string',255,'',0,0,1,1,''),('sender_event','','string',255,'0',1,6,1,0,''),('send_cc_for_system_emails','','string',255,'0',8,4,1,0,''),('session_ip_control','off','enum',3,'security',8,22,1,0,''),('sms_cell_num_field','1','field_select',5,'0',11,6,1,1,''),('sms_credit','0','string',20,'0',1,0,1,1,''),('sms_gateway','smsmarket','string',50,'0',11,0,1,1,''),('sms_gateway_host','193.254.241.47','string',15,'0',11,8,1,1,''),('sms_gateway_id','3','sel_sms_gateway',1,'0',11,7,1,1,''),('sms_gateway_pass','','string',255,'0',11,5,1,1,''),('sms_gateway_port','26','int',5,'0',11,9,1,1,''),('sms_gateway_user','','string',50,'0',11,4,1,1,''),('sms_international_prefix','+39','string',3,'0',11,1,1,1,''),('sms_sent_from','0','string',25,'0',11,2,1,1,''),('social_fb_active','off','enum',3,'main',12,0,1,1,''),('social_fb_api','','string',255,'main',12,1,1,1,''),('social_fb_secret','','string',255,'main',12,2,1,1,''),('social_google_active','off','enum',3,'main',12,9,1,1,''),('social_linkedin_access','','string',255,'main',12,7,1,1,''),('social_linkedin_active','off','enum',3,'main',12,6,1,1,''),('social_linkedin_secret','','string',255,'main',12,8,1,1,''),('social_twitter_active','off','enum',3,'main',12,3,1,1,''),('social_twitter_consumer','','string',255,'main',12,4,1,1,''),('social_twitter_secret','','string',255,'main',12,5,1,1,''),('sso_secret','','text',255,'0',9,1,1,0,''),('sso_token','off','enum',3,'0',9,0,1,0,''),('stop_concurrent_user','off','enum',3,'security',8,23,1,0,''),('subscription_begin_date','','string',255,'main',0,0,1,0,''),('tablist_mycourses','name,status','tablist_mycourses',255,'0',4,1,1,0,''),('template_domain','','textarea',65535,'0',8,8,1,0,''),('templ_use_field','0','id_field',11,'0',1,0,1,1,''),('test_force_multichoice','off','enum',3,'0',4,8,1,0,''),('timezone_allow_user_override','off','string',255,'',0,0,1,1,''),('timezone_default','Europe/Rome','string',255,'',0,0,1,1,''),('title_organigram_chart','Docebo','string',255,'0',1,0,1,1,''),('tos_last_approval_date','2015-11-27','string',255,'',0,0,1,1,''),('tracking','off','enum',3,'0',4,8,1,0,''),('trial_expiration_date','2020-12-08 00:00:00','string',255,'',0,0,1,1,''),('ttlSession','4000','int',5,'0',1,7,1,0,''),('ua_mobile','[\'android\',\'iphone\',\'ipod\',\'ipad\',\'opera mini\',\'blackberry\',\'webOS\']','string',65535,'0',8,26,1,0,''),('url','','string',255,'0',1,2,1,1,''),('userlimit_hellobar_closed','0','string',255,'',0,0,1,1,''),('user_pwd_history_length','3','int',3,'password',3,10,1,0,''),('user_quota','50','string',255,'0',8,6,1,0,''),('use_dashlets','on','enum',3,'0',3,21,1,0,''),('use_node_fields_visibility','off','enum',3,'0',3,21,1,0,''),('use_rest_api','off','enum',3,'api',9,2,1,0,''),('use_tag','off','enum',3,'0',4,5,1,0,''),('video_contribute','0','share7020settings',255,'main',0,0,1,0,''),('visuItem','25','int',3,'0',2,1,1,1,''),('visuNewsHomePage','3','int',5,'0',1,0,1,1,''),('welcome_use_feed','on','enum',3,'0',1,0,1,1,'');
/*!40000 ALTER TABLE `core_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_setting_group`
--

DROP TABLE IF EXISTS `core_setting_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_setting_group` (
  `path_name` varchar(255) NOT NULL DEFAULT '',
  `idst` int(11) NOT NULL DEFAULT '0',
  `value` text NOT NULL,
  PRIMARY KEY (`path_name`,`idst`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_setting_group`
--

LOCK TABLES `core_setting_group` WRITE;
/*!40000 ALTER TABLE `core_setting_group` DISABLE KEYS */;
INSERT INTO `core_setting_group` VALUES ('admin_rules.allow_only_student_enrollments',12301,'off'),('admin_rules.direct_course_subscribe',12301,'on'),('admin_rules.direct_user_insert',12301,'off');
/*!40000 ALTER TABLE `core_setting_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_setting_user`
--

DROP TABLE IF EXISTS `core_setting_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_setting_user` (
  `path_name` varchar(255) NOT NULL DEFAULT '',
  `id_user` int(11) NOT NULL DEFAULT '0',
  `value` text NOT NULL,
  PRIMARY KEY (`path_name`,`id_user`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `core_setting_user_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_setting_user`
--

LOCK TABLES `core_setting_user` WRITE;
/*!40000 ALTER TABLE `core_setting_user` DISABLE KEYS */;
INSERT INTO `core_setting_user` VALUES ('ui.language',12303,'english'),('ui.language',13006,'english');
/*!40000 ALTER TABLE `core_setting_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_setting_whitelist`
--

DROP TABLE IF EXISTS `core_setting_whitelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_setting_whitelist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) DEFAULT NULL,
  `whitelist` blob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_setting_whitelist`
--

LOCK TABLES `core_setting_whitelist` WRITE;
/*!40000 ALTER TABLE `core_setting_whitelist` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_setting_whitelist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_st`
--

DROP TABLE IF EXISTS `core_st`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_st` (
  `idst` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idst`)
) ENGINE=InnoDB AUTO_INCREMENT=13009 DEFAULT CHARSET=utf8 COMMENT='Security Tokens';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_st`
--

LOCK TABLES `core_st` WRITE;
/*!40000 ALTER TABLE `core_st` DISABLE KEYS */;
INSERT INTO `core_st` VALUES (1),(2),(3),(4),(6),(11831),(11832),(11833),(11834),(11835),(11836),(11837),(11838),(11839),(11840),(11841),(11842),(11843),(11844),(11845),(11846),(11847),(11848),(11849),(11850),(11851),(11852),(11853),(11854),(11855),(11856),(11857),(11858),(11859),(11860),(11861),(11862),(11863),(11864),(11865),(11866),(11867),(11868),(11869),(11870),(11871),(11872),(11873),(11874),(11875),(11876),(11877),(11878),(11879),(11880),(11881),(11882),(11883),(11884),(11885),(11886),(11887),(11888),(11889),(11890),(11891),(11892),(11893),(11894),(11895),(11896),(11897),(11898),(11899),(11900),(11901),(11902),(11903),(11904),(11905),(11906),(11907),(11908),(11909),(11910),(11911),(11912),(11913),(11914),(11915),(11916),(11917),(11918),(11919),(11920),(11921),(11922),(11923),(11924),(11925),(11926),(11927),(11928),(11929),(11930),(11931),(11932),(11933),(11934),(11935),(11936),(11937),(11938),(11939),(11940),(11941),(11942),(11943),(11944),(11945),(11946),(11947),(11948),(11949),(11950),(11951),(11952),(11953),(11954),(11955),(11956),(11957),(11958),(11959),(11960),(11961),(11962),(11963),(11964),(11965),(11966),(11967),(11968),(11969),(11970),(11971),(11972),(11973),(11974),(11975),(11976),(11977),(11978),(11979),(11980),(11981),(11982),(11983),(11984),(11985),(11986),(11987),(11988),(11989),(11990),(11991),(11992),(11993),(11994),(11995),(11996),(11997),(11998),(11999),(12000),(12001),(12002),(12003),(12004),(12005),(12006),(12007),(12008),(12009),(12010),(12011),(12012),(12013),(12014),(12015),(12016),(12017),(12018),(12019),(12020),(12021),(12022),(12023),(12024),(12025),(12026),(12027),(12028),(12029),(12030),(12031),(12032),(12033),(12034),(12035),(12036),(12037),(12038),(12039),(12040),(12041),(12042),(12043),(12044),(12045),(12046),(12047),(12049),(12050),(12051),(12052),(12053),(12054),(12055),(12056),(12057),(12058),(12059),(12060),(12061),(12062),(12063),(12064),(12065),(12066),(12067),(12068),(12069),(12070),(12071),(12072),(12073),(12074),(12075),(12076),(12077),(12078),(12079),(12080),(12081),(12082),(12083),(12084),(12085),(12086),(12087),(12088),(12089),(12090),(12091),(12092),(12093),(12094),(12095),(12096),(12097),(12098),(12099),(12100),(12101),(12102),(12103),(12104),(12105),(12106),(12107),(12108),(12109),(12110),(12111),(12112),(12113),(12114),(12115),(12116),(12117),(12118),(12119),(12120),(12121),(12122),(12123),(12124),(12125),(12126),(12127),(12128),(12129),(12130),(12131),(12132),(12133),(12134),(12135),(12136),(12137),(12138),(12139),(12140),(12141),(12142),(12143),(12144),(12145),(12146),(12147),(12148),(12149),(12150),(12151),(12152),(12153),(12154),(12155),(12156),(12157),(12158),(12159),(12160),(12161),(12162),(12163),(12164),(12165),(12166),(12167),(12168),(12169),(12170),(12171),(12172),(12173),(12174),(12175),(12176),(12177),(12178),(12179),(12180),(12181),(12182),(12183),(12184),(12185),(12186),(12187),(12188),(12189),(12190),(12191),(12192),(12193),(12194),(12195),(12196),(12197),(12198),(12199),(12200),(12201),(12202),(12203),(12204),(12205),(12206),(12207),(12208),(12209),(12210),(12211),(12212),(12213),(12214),(12215),(12216),(12217),(12218),(12219),(12220),(12221),(12222),(12223),(12224),(12225),(12226),(12227),(12228),(12229),(12230),(12231),(12232),(12233),(12234),(12235),(12236),(12237),(12238),(12239),(12240),(12241),(12242),(12243),(12244),(12245),(12246),(12247),(12248),(12249),(12250),(12251),(12252),(12253),(12254),(12255),(12256),(12257),(12258),(12259),(12260),(12261),(12262),(12263),(12264),(12265),(12266),(12267),(12268),(12269),(12270),(12271),(12272),(12273),(12274),(12275),(12276),(12277),(12278),(12279),(12280),(12281),(12282),(12283),(12284),(12285),(12286),(12287),(12288),(12289),(12290),(12291),(12292),(12293),(12294),(12295),(12296),(12297),(12298),(12299),(12300),(12301),(12302),(12303),(12304),(12305),(12412),(12413),(12414),(13000),(13001),(13002),(13003),(13004),(13005),(13006),(13007),(13008);
/*!40000 ALTER TABLE `core_st` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_templatecolor`
--

DROP TABLE IF EXISTS `core_templatecolor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_templatecolor` (
  `set_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `set_title` varchar(255) NOT NULL,
  `set_colors` text,
  `is_custom` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`set_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_templatecolor`
--

LOCK TABLES `core_templatecolor` WRITE;
/*!40000 ALTER TABLE `core_templatecolor` DISABLE KEYS */;
INSERT INTO `core_templatecolor` VALUES (1,'default','{\"mainBgColor\":\"#EFEFEF\",\"darkColor\":\"#03569D\",\"middleColor\":\"#338DCE\",\"lightColor\":\"#73B0DD\",\"titleColor\":\"#0A5C8E\",\"textColor\":\"#333333\"}',0,1),(2,'dark green','{\"mainBgColor\":\"#333333\",\"darkColor\":\"#333333\",\"middleColor\":\"#666666\",\"lightColor\":\"#999999\",\"titleColor\":\"#85B200\",\"textColor\":\"#333333\"}',0,0),(3,'green','{\"mainBgColor\":\"#EFEFEF\",\"darkColor\":\"#489148\",\"middleColor\":\"#61C261\",\"lightColor\":\"#94D694\",\"titleColor\":\"#489148\",\"textColor\":\"#333333\"}',0,0),(4,'orange','{\"mainBgColor\":\"#EFEFEF\",\"darkColor\":\"#FA6105\",\"middleColor\":\"#FC9252\",\"lightColor\":\"#FDBE99\",\"titleColor\":\"#FA6105\",\"textColor\":\"#333333\"}',0,0),(5,'purple','{\"mainBgColor\":\"#EFEFEF\",\"darkColor\":\"#5545AF\",\"middleColor\":\"#8478C9\",\"lightColor\":\"#BBB5E1\",\"titleColor\":\"#5545AF\",\"textColor\":\"#333333\"}',0,0),(6,'red','{\"mainBgColor\":\"#EFEFEF\",\"darkColor\":\"#C63737\",\"middleColor\":\"#D66B6B\",\"lightColor\":\"#E7A5A5\",\"titleColor\":\"#C63737\",\"textColor\":\"#333333\"}',0,0);
/*!40000 ALTER TABLE `core_templatecolor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_user`
--

DROP TABLE IF EXISTS `core_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_user` (
  `idst` int(11) NOT NULL DEFAULT '0',
  `userid` varchar(255) NOT NULL DEFAULT '',
  `firstname` varchar(255) NOT NULL DEFAULT '',
  `lastname` varchar(255) NOT NULL DEFAULT '',
  `pass` varchar(64) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `avatar` varchar(255) NOT NULL DEFAULT '',
  `signature` text NOT NULL,
  `level` int(11) NOT NULL DEFAULT '0',
  `lastenter` timestamp NULL DEFAULT NULL,
  `valid` tinyint(1) NOT NULL DEFAULT '1',
  `pwd_expire_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `force_change` tinyint(1) NOT NULL DEFAULT '0',
  `register_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `facebook_id` varchar(255) DEFAULT NULL,
  `twitter_id` varchar(255) DEFAULT NULL,
  `linkedin_id` varchar(255) DEFAULT NULL,
  `google_id` varchar(255) DEFAULT NULL,
  `privacy_policy` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `suspend_date` timestamp NULL DEFAULT NULL,
  `saas_expire_date` date DEFAULT NULL,
  `saas_status` varchar(30) DEFAULT NULL,
  `saas_helpdesk_user` varchar(120) DEFAULT NULL,
  `expiration` date DEFAULT NULL,
  `email_code` varchar(200) DEFAULT NULL,
  `requested_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `email_status` int(1) DEFAULT '0',
  `recent_search` text,
  PRIMARY KEY (`idst`),
  UNIQUE KEY `userid` (`userid`),
  UNIQUE KEY `facebook_id` (`facebook_id`),
  UNIQUE KEY `twitter_id` (`twitter_id`),
  UNIQUE KEY `linkedin_id` (`linkedin_id`),
  UNIQUE KEY `google_id` (`google_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_user`
--

LOCK TABLES `core_user` WRITE;
/*!40000 ALTER TABLE `core_user` DISABLE KEYS */;
INSERT INTO `core_user` VALUES (270,'/Anonymous','','','','','','',0,'0000-00-00 00:00:00',0,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00 00:00:00',0,NULL),(13006,'/staff.docebo','Staff','Docebo','$2a$13$Nbf3vbwotEnvqyFrzAVMouNQYC55yx18cFT.0hENVh22M7q6RIoxK','a@b.c','','',0,'2016-02-04 10:01:08',1,'0000-00-00 00:00:00',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00 00:00:00',0,NULL);
/*!40000 ALTER TABLE `core_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_user_billing`
--

DROP TABLE IF EXISTS `core_user_billing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_user_billing` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idst` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `bill_address1` varchar(255) NOT NULL,
  `bill_address2` varchar(255) NOT NULL,
  `bill_city` varchar(128) NOT NULL,
  `bill_state` varchar(64) NOT NULL COMMENT 'State, province, region',
  `bill_zip` varchar(32) NOT NULL COMMENT 'Zip, postal code',
  `bill_country_code` varchar(16) NOT NULL COMMENT 'Like US, IT, ...',
  `bill_company_name` varchar(128) DEFAULT NULL,
  `bill_vat_number` varchar(64) DEFAULT NULL COMMENT 'Like: GBHH5422JH765',
  `bill_stored` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'When this information is stored for the first time',
  `bill_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'When this information is updated last time',
  `bill_is_current` tinyint(4) DEFAULT NULL COMMENT 'Use as current billing information',
  `sha1_sum` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `bill_stored` (`bill_stored`),
  KEY `bill_updated` (`bill_updated`),
  KEY `idst` (`idst`),
  KEY `userid` (`userid`),
  KEY `sha1_sum` (`sha1_sum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds user billing information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_user_billing`
--

LOCK TABLES `core_user_billing` WRITE;
/*!40000 ALTER TABLE `core_user_billing` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_user_billing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_user_pu`
--

DROP TABLE IF EXISTS `core_user_pu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_user_pu` (
  `puser_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  UNIQUE KEY `puser_id` (`puser_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_user_pu`
--

LOCK TABLES `core_user_pu` WRITE;
/*!40000 ALTER TABLE `core_user_pu` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_user_pu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_user_pu_course`
--

DROP TABLE IF EXISTS `core_user_pu_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_user_pu_course` (
  `puser_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `total_seats` int(11) DEFAULT '0',
  `available_seats` int(11) DEFAULT '0',
  `extra_seats` int(11) DEFAULT '0',
  PRIMARY KEY (`puser_id`,`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_user_pu_course`
--

LOCK TABLES `core_user_pu_course` WRITE;
/*!40000 ALTER TABLE `core_user_pu_course` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_user_pu_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_user_pu_coursepath`
--

DROP TABLE IF EXISTS `core_user_pu_coursepath`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_user_pu_coursepath` (
  `puser_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  PRIMARY KEY (`puser_id`,`path_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_user_pu_coursepath`
--

LOCK TABLES `core_user_pu_coursepath` WRITE;
/*!40000 ALTER TABLE `core_user_pu_coursepath` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_user_pu_coursepath` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_user_pu_location`
--

DROP TABLE IF EXISTS `core_user_pu_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_user_pu_location` (
  `puser_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  PRIMARY KEY (`puser_id`,`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_user_pu_location`
--

LOCK TABLES `core_user_pu_location` WRITE;
/*!40000 ALTER TABLE `core_user_pu_location` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_user_pu_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_user_temp`
--

DROP TABLE IF EXISTS `core_user_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_user_temp` (
  `idst` int(11) NOT NULL DEFAULT '0',
  `userid` varchar(255) NOT NULL DEFAULT '',
  `firstname` varchar(100) NOT NULL DEFAULT '',
  `lastname` varchar(100) NOT NULL DEFAULT '',
  `pass` varchar(64) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `language` varchar(50) NOT NULL DEFAULT '',
  `request_on` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `random_code` varchar(255) NOT NULL DEFAULT '',
  `create_by_admin` int(11) NOT NULL DEFAULT '0',
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `facebook_id` varchar(255) DEFAULT NULL,
  `twitter_id` varchar(255) DEFAULT NULL,
  `linkedin_id` varchar(255) DEFAULT NULL,
  `google_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idst`),
  UNIQUE KEY `userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_user_temp`
--

LOCK TABLES `core_user_temp` WRITE;
/*!40000 ALTER TABLE `core_user_temp` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_user_temp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_assign`
--

DROP TABLE IF EXISTS `dashboard_assign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard_assign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_layout` int(11) NOT NULL,
  `item_type` varchar(32) NOT NULL COMMENT 'user level, user, group, branch, etc.',
  `id_item` varchar(128) NOT NULL COMMENT 'ID of the item of the given type',
  PRIMARY KEY (`id`),
  KEY `id_layout` (`id_layout`),
  CONSTRAINT `dashboard_assign_ibfk_1` FOREIGN KEY (`id_layout`) REFERENCES `dashboard_layout` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_assign`
--

LOCK TABLES `dashboard_assign` WRITE;
/*!40000 ALTER TABLE `dashboard_assign` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboard_assign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_cell`
--

DROP TABLE IF EXISTS `dashboard_cell`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard_cell` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row` int(11) NOT NULL,
  `position` int(11) DEFAULT NULL COMMENT 'Position inside the row',
  `width` int(11) NOT NULL COMMENT 'Width: 1..12',
  PRIMARY KEY (`id`),
  KEY `row` (`row`),
  CONSTRAINT `dashboard_cell_ibfk_1` FOREIGN KEY (`row`) REFERENCES `dashboard_row` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_cell`
--

LOCK TABLES `dashboard_cell` WRITE;
/*!40000 ALTER TABLE `dashboard_cell` DISABLE KEYS */;
INSERT INTO `dashboard_cell` VALUES (1,1,1,12);
/*!40000 ALTER TABLE `dashboard_cell` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_dashlet`
--

DROP TABLE IF EXISTS `dashboard_dashlet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard_dashlet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `handler` varchar(255) NOT NULL COMMENT 'PHP class name',
  `cell` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL COMMENT 'Position inside a cell',
  `height` int(11) DEFAULT NULL,
  `header` varchar(255) DEFAULT NULL,
  `params` text COMMENT 'JSON parameters',
  PRIMARY KEY (`id`),
  KEY `position` (`position`),
  KEY `cell` (`cell`),
  CONSTRAINT `dashboard_dashlet_ibfk_1` FOREIGN KEY (`cell`) REFERENCES `dashboard_cell` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_dashlet`
--

LOCK TABLES `dashboard_dashlet` WRITE;
/*!40000 ALTER TABLE `dashboard_dashlet` DISABLE KEYS */;
INSERT INTO `dashboard_dashlet` VALUES (1,'mydashboard.widgets.DashletMyCourses',1,2,0,'','{\"selected_sorting_fields\":[\"t.name\",\"enrollment.status\"],\"initial_status_filter\":\"all\",\"aggregate_labels\":false}');
/*!40000 ALTER TABLE `dashboard_dashlet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_layout`
--

DROP TABLE IF EXISTS `dashboard_layout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard_layout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `position` int(11) DEFAULT NULL,
  `default` int(1) DEFAULT NULL COMMENT 'Is this layout default',
  `ufilter_option` int(11) DEFAULT '1' COMMENT 'User filter: ALL (1) or selected (2)',
  `puprofiles_filter_option` int(11) DEFAULT '1',
  `is_fallback` int(1) DEFAULT NULL COMMENT 'Is this layout the ultimate fallback-default one',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_layout`
--

LOCK TABLES `dashboard_layout` WRITE;
/*!40000 ALTER TABLE `dashboard_layout` DISABLE KEYS */;
INSERT INTO `dashboard_layout` VALUES (1,'Standard layout',9999,0,1,1,1);
/*!40000 ALTER TABLE `dashboard_layout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_row`
--

DROP TABLE IF EXISTS `dashboard_row`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard_row` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_layout` int(11) NOT NULL,
  `position` int(11) DEFAULT NULL COMMENT 'Position inside layout',
  PRIMARY KEY (`id`),
  KEY `id_layout` (`id_layout`),
  CONSTRAINT `dashboard_row_ibfk_1` FOREIGN KEY (`id_layout`) REFERENCES `dashboard_layout` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_row`
--

LOCK TABLES `dashboard_row` WRITE;
/*!40000 ALTER TABLE `dashboard_row` DISABLE KEYS */;
INSERT INTO `dashboard_row` VALUES (1,1,1);
/*!40000 ALTER TABLE `dashboard_row` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecommerce_coupon`
--

DROP TABLE IF EXISTS `ecommerce_coupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecommerce_coupon` (
  `id_coupon` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) NOT NULL,
  `description` varchar(255) NOT NULL,
  `valid_from` timestamp NULL DEFAULT NULL,
  `valid_to` timestamp NULL DEFAULT NULL,
  `can_have_courses` tinyint(1) NOT NULL DEFAULT '0',
  `usage_count` int(4) NOT NULL DEFAULT '0',
  `discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discount_type` varchar(12) DEFAULT 'percent',
  `minimum_order` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id_coupon`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecommerce_coupon`
--

LOCK TABLES `ecommerce_coupon` WRITE;
/*!40000 ALTER TABLE `ecommerce_coupon` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecommerce_coupon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecommerce_coupon_courses`
--

DROP TABLE IF EXISTS `ecommerce_coupon_courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecommerce_coupon_courses` (
  `id_coupon` int(11) NOT NULL,
  `id_course` int(11) NOT NULL,
  PRIMARY KEY (`id_coupon`,`id_course`),
  KEY `fk_ecommerce_coupon_courses_learning_course1_idx` (`id_course`),
  CONSTRAINT `fk_ecommerce_coupon_courses_learning_coupon` FOREIGN KEY (`id_coupon`) REFERENCES `ecommerce_coupon` (`id_coupon`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ecommerce_coupon_courses_learning_course1` FOREIGN KEY (`id_course`) REFERENCES `learning_course` (`idCourse`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecommerce_coupon_courses`
--

LOCK TABLES `ecommerce_coupon_courses` WRITE;
/*!40000 ALTER TABLE `ecommerce_coupon_courses` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecommerce_coupon_courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecommerce_transaction`
--

DROP TABLE IF EXISTS `ecommerce_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecommerce_transaction` (
  `id_trans` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `location` varchar(10) NOT NULL DEFAULT '',
  `date_creation` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_activated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `paid` tinyint(1) NOT NULL DEFAULT '0',
  `note` varchar(255) DEFAULT NULL,
  `billing_info_id` bigint(20) DEFAULT NULL COMMENT 'Id of a billing info record',
  `payment_type` varchar(32) DEFAULT NULL COMMENT 'PayPal, WireCard, Wire...',
  `payment_txn_id` varchar(512) DEFAULT NULL COMMENT 'Some Unique ID received by payment processor',
  `payment_currency` varchar(6) NOT NULL,
  `raw_data_json` text COMMENT 'Consists the full transaction data in a JSON format',
  `receipt_id` varchar(255) DEFAULT NULL,
  `discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `id_coupon` int(11) DEFAULT NULL,
  `abandoned` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_trans`),
  KEY `id_user` (`id_user`),
  KEY `billing_info_id` (`billing_info_id`),
  KEY `payment_txn_id` (`payment_txn_id`(255)),
  KEY `receipt_id` (`receipt_id`),
  KEY `id_coupon` (`id_coupon`),
  CONSTRAINT `ecommerce_transaction_ibfk_1` FOREIGN KEY (`id_coupon`) REFERENCES `ecommerce_coupon` (`id_coupon`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecommerce_transaction`
--

LOCK TABLES `ecommerce_transaction` WRITE;
/*!40000 ALTER TABLE `ecommerce_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecommerce_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecommerce_transaction_info`
--

DROP TABLE IF EXISTS `ecommerce_transaction_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecommerce_transaction_info` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_trans` int(11) NOT NULL DEFAULT '0',
  `id_course` int(11) NOT NULL DEFAULT '0',
  `id_date` int(11) NOT NULL DEFAULT '0',
  `id_edition` int(11) NOT NULL DEFAULT '0',
  `code` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `price` varchar(255) NOT NULL DEFAULT '',
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `item_type` varchar(255) NOT NULL DEFAULT 'course',
  `id_path` int(11) DEFAULT NULL,
  `item_data_json` text,
  PRIMARY KEY (`item_id`),
  KEY `id_course` (`id_course`),
  KEY `core_transaction_info_ibfk_2` (`id_trans`),
  CONSTRAINT `trans1` FOREIGN KEY (`id_trans`) REFERENCES `ecommerce_transaction` (`id_trans`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecommerce_transaction_info`
--

LOCK TABLES `ecommerce_transaction_info` WRITE;
/*!40000 ALTER TABLE `ecommerce_transaction_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecommerce_transaction_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gamification_assigned_badges`
--

DROP TABLE IF EXISTS `gamification_assigned_badges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamification_assigned_badges` (
  `id_badge` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `issued_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `score` int(11) NOT NULL DEFAULT '0',
  `event_name` varchar(100) DEFAULT NULL,
  `event_key` varchar(255) DEFAULT NULL,
  `event_module` varchar(128) NOT NULL DEFAULT 'gamification',
  `event_params` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id_badge`,`id_user`,`issued_on`),
  KEY `fk_gamification_assigned_badges_core_user1_idx` (`id_user`),
  CONSTRAINT `fk_gamification_assigned_badges_core_user1` FOREIGN KEY (`id_user`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_gamification_assigned_badges_gamification_badge1` FOREIGN KEY (`id_badge`) REFERENCES `gamification_badge` (`id_badge`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamification_assigned_badges`
--

LOCK TABLES `gamification_assigned_badges` WRITE;
/*!40000 ALTER TABLE `gamification_assigned_badges` DISABLE KEYS */;
/*!40000 ALTER TABLE `gamification_assigned_badges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gamification_associated_sets`
--

DROP TABLE IF EXISTS `gamification_associated_sets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamification_associated_sets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_reward` int(11) NOT NULL,
  `id_set` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1__gamification_reward` (`id_reward`),
  KEY `FK2__gamification_rewards_set` (`id_set`),
  CONSTRAINT `FK1__gamification_reward` FOREIGN KEY (`id_reward`) REFERENCES `gamification_reward` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK2__gamification_rewards_set` FOREIGN KEY (`id_set`) REFERENCES `gamification_rewards_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamification_associated_sets`
--

LOCK TABLES `gamification_associated_sets` WRITE;
/*!40000 ALTER TABLE `gamification_associated_sets` DISABLE KEYS */;
/*!40000 ALTER TABLE `gamification_associated_sets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gamification_badge`
--

DROP TABLE IF EXISTS `gamification_badge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamification_badge` (
  `id_badge` int(11) NOT NULL AUTO_INCREMENT,
  `icon` varchar(255) NOT NULL,
  `score` int(11) NOT NULL,
  `event_name` varchar(100) DEFAULT NULL,
  `event_params` text,
  PRIMARY KEY (`id_badge`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamification_badge`
--

LOCK TABLES `gamification_badge` WRITE;
/*!40000 ALTER TABLE `gamification_badge` DISABLE KEYS */;
/*!40000 ALTER TABLE `gamification_badge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gamification_badge_translation`
--

DROP TABLE IF EXISTS `gamification_badge_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamification_badge_translation` (
  `id_badge` int(11) NOT NULL,
  `lang_code` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  PRIMARY KEY (`id_badge`,`lang_code`),
  KEY `fk_gamification_badge_translation_gamification_badge_idx` (`id_badge`),
  CONSTRAINT `fk_gamification_badge_translation_gamification_badge` FOREIGN KEY (`id_badge`) REFERENCES `gamification_badge` (`id_badge`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamification_badge_translation`
--

LOCK TABLES `gamification_badge_translation` WRITE;
/*!40000 ALTER TABLE `gamification_badge_translation` DISABLE KEYS */;
/*!40000 ALTER TABLE `gamification_badge_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gamification_contest`
--

DROP TABLE IF EXISTS `gamification_contest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamification_contest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_date` timestamp NULL DEFAULT NULL,
  `to_date` timestamp NULL DEFAULT NULL,
  `goal` varchar(255) DEFAULT NULL,
  `goal_settings` text,
  `filter` text,
  `timezone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamification_contest`
--

LOCK TABLES `gamification_contest` WRITE;
/*!40000 ALTER TABLE `gamification_contest` DISABLE KEYS */;
/*!40000 ALTER TABLE `gamification_contest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gamification_contest_chart`
--

DROP TABLE IF EXISTS `gamification_contest_chart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamification_contest_chart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_contest` int(11) NOT NULL,
  `rank` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `ranking_result` int(3) NOT NULL,
  `reward_data` text,
  PRIMARY KEY (`id`),
  KEY `fk_gamification_contest_chart` (`id_contest`),
  CONSTRAINT `fk_gamification_contest_chart` FOREIGN KEY (`id_contest`) REFERENCES `gamification_contest` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamification_contest_chart`
--

LOCK TABLES `gamification_contest_chart` WRITE;
/*!40000 ALTER TABLE `gamification_contest_chart` DISABLE KEYS */;
/*!40000 ALTER TABLE `gamification_contest_chart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gamification_contest_reward`
--

DROP TABLE IF EXISTS `gamification_contest_reward`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamification_contest_reward` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_contest` int(11) NOT NULL,
  `rank` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `id_item` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gamification_contest_reward` (`id_contest`),
  CONSTRAINT `fk_gamification_contest_reward` FOREIGN KEY (`id_contest`) REFERENCES `gamification_contest` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamification_contest_reward`
--

LOCK TABLES `gamification_contest_reward` WRITE;
/*!40000 ALTER TABLE `gamification_contest_reward` DISABLE KEYS */;
/*!40000 ALTER TABLE `gamification_contest_reward` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gamification_contest_translation`
--

DROP TABLE IF EXISTS `gamification_contest_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamification_contest_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_contest` int(11) NOT NULL,
  `lang_code` varchar(50) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `fk_gamification_contest_translation` (`id_contest`),
  CONSTRAINT `fk_gamification_contest` FOREIGN KEY (`id_contest`) REFERENCES `gamification_contest` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamification_contest_translation`
--

LOCK TABLES `gamification_contest_translation` WRITE;
/*!40000 ALTER TABLE `gamification_contest_translation` DISABLE KEYS */;
/*!40000 ALTER TABLE `gamification_contest_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gamification_leaderboard`
--

DROP TABLE IF EXISTS `gamification_leaderboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamification_leaderboard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filter` text COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `default` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamification_leaderboard`
--

LOCK TABLES `gamification_leaderboard` WRITE;
/*!40000 ALTER TABLE `gamification_leaderboard` DISABLE KEYS */;
INSERT INTO `gamification_leaderboard` VALUES (1,'all',1,1);
/*!40000 ALTER TABLE `gamification_leaderboard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gamification_leaderboard_translation`
--

DROP TABLE IF EXISTS `gamification_leaderboard_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamification_leaderboard_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_board` int(11) NOT NULL,
  `lang_code` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `leaderboard_idx` (`id_board`),
  CONSTRAINT `leaderboard` FOREIGN KEY (`id_board`) REFERENCES `gamification_leaderboard` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamification_leaderboard_translation`
--

LOCK TABLES `gamification_leaderboard_translation` WRITE;
/*!40000 ALTER TABLE `gamification_leaderboard_translation` DISABLE KEYS */;
INSERT INTO `gamification_leaderboard_translation` VALUES (1,1,'arabic','Default Leaderboard'),(2,1,'bosnian','Default Leaderboard'),(3,1,'bulgarian','Default Leaderboard'),(4,1,'croatian','Default Leaderboard'),(5,1,'czech','Default Leaderboard'),(6,1,'danish','Default Leaderboard'),(7,1,'dutch','Default Leaderboard'),(8,1,'english','Default Leaderboard'),(9,1,'english_uk','Default Leaderboard'),(10,1,'farsi','Default Leaderboard'),(11,1,'finnish','Default Leaderboard'),(12,1,'french','Default Leaderboard'),(13,1,'german','Default Leaderboard'),(14,1,'greek','Default Leaderboard'),(15,1,'hebrew','Default Leaderboard'),(16,1,'hungarian','Default Leaderboard'),(17,1,'indonesian','Default Leaderboard'),(18,1,'italian','Default Leaderboard'),(19,1,'japanese','Default Leaderboard'),(20,1,'korean','Default Leaderboard'),(21,1,'norwegian','Default Leaderboard'),(22,1,'polish','Default Leaderboard'),(23,1,'portuguese','Default Leaderboard'),(24,1,'portuguese-br','Default Leaderboard'),(25,1,'romanian','Default Leaderboard'),(26,1,'russian','Default Leaderboard'),(27,1,'simplified_chinese','Default Leaderboard'),(28,1,'slovenian','Default Leaderboard'),(29,1,'spanish','Default Leaderboard'),(30,1,'swedish','Default Leaderboard'),(31,1,'thai','Default Leaderboard'),(32,1,'turkish','Default Leaderboard'),(33,1,'ukrainian','Default Leaderboard');
/*!40000 ALTER TABLE `gamification_leaderboard_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gamification_reward`
--

DROP TABLE IF EXISTS `gamification_reward`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamification_reward` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coins` int(11) NOT NULL DEFAULT '0',
  `availability` int(11) NOT NULL DEFAULT '0',
  `picture` varchar(2000) DEFAULT NULL,
  `association` varchar(50) DEFAULT 'all',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamification_reward`
--

LOCK TABLES `gamification_reward` WRITE;
/*!40000 ALTER TABLE `gamification_reward` DISABLE KEYS */;
/*!40000 ALTER TABLE `gamification_reward` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gamification_reward_translation`
--

DROP TABLE IF EXISTS `gamification_reward_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamification_reward_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_reward` int(11) NOT NULL,
  `lang_code` varchar(50) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `FK__gamification_reward` (`id_reward`),
  CONSTRAINT `FK__gamification_reward` FOREIGN KEY (`id_reward`) REFERENCES `gamification_reward` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamification_reward_translation`
--

LOCK TABLES `gamification_reward_translation` WRITE;
/*!40000 ALTER TABLE `gamification_reward_translation` DISABLE KEYS */;
/*!40000 ALTER TABLE `gamification_reward_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gamification_rewards_set`
--

DROP TABLE IF EXISTS `gamification_rewards_set`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamification_rewards_set` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `filter` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamification_rewards_set`
--

LOCK TABLES `gamification_rewards_set` WRITE;
/*!40000 ALTER TABLE `gamification_rewards_set` DISABLE KEYS */;
/*!40000 ALTER TABLE `gamification_rewards_set` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gamification_user_rewards`
--

DROP TABLE IF EXISTS `gamification_user_rewards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamification_user_rewards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_reward` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `data` text,
  `date_created` timestamp NULL DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__core_user` (`id_user`),
  KEY `FK_gamification_user_rewards_gamification_reward` (`id_reward`),
  CONSTRAINT `FK__core_user` FOREIGN KEY (`id_user`) REFERENCES `core_user` (`idst`),
  CONSTRAINT `FK_gamification_user_rewards_gamification_reward` FOREIGN KEY (`id_reward`) REFERENCES `gamification_reward` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamification_user_rewards`
--

LOCK TABLES `gamification_user_rewards` WRITE;
/*!40000 ALTER TABLE `gamification_user_rewards` DISABLE KEYS */;
/*!40000 ALTER TABLE `gamification_user_rewards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gamification_user_wallet`
--

DROP TABLE IF EXISTS `gamification_user_wallet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamification_user_wallet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `coins` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK1__core_user` (`id_user`),
  CONSTRAINT `FK1__core_user` FOREIGN KEY (`id_user`) REFERENCES `core_user` (`idst`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamification_user_wallet`
--

LOCK TABLES `gamification_user_wallet` WRITE;
/*!40000 ALTER TABLE `gamification_user_wallet` DISABLE KEYS */;
/*!40000 ALTER TABLE `gamification_user_wallet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inbox_app_notification`
--

DROP TABLE IF EXISTS `inbox_app_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inbox_app_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `json_data` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inbox_app_notification`
--

LOCK TABLES `inbox_app_notification` WRITE;
/*!40000 ALTER TABLE `inbox_app_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `inbox_app_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inbox_app_notification_delivered`
--

DROP TABLE IF EXISTS `inbox_app_notification_delivered`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inbox_app_notification_delivered` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notification_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inbox_app_notification_delivered`
--

LOCK TABLES `inbox_app_notification_delivered` WRITE;
/*!40000 ALTER TABLE `inbox_app_notification_delivered` DISABLE KEYS */;
/*!40000 ALTER TABLE `inbox_app_notification_delivered` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_aicc_item`
--

DROP TABLE IF EXISTS `learning_aicc_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_aicc_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_package` int(11) NOT NULL,
  `system_id` varchar(255) NOT NULL,
  `parent` varchar(255) NOT NULL,
  `launch` text,
  `sorting` int(11) DEFAULT NULL,
  `item_type` varchar(16) DEFAULT NULL COMMENT 'au, objective, block, ...',
  `developer_id` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `command_line` text,
  `datafromlms` text,
  `mastery_score` float DEFAULT NULL,
  `max_score` float DEFAULT NULL,
  `max_time_allowed` varchar(16) DEFAULT NULL,
  `time_limit_action` varchar(255) DEFAULT NULL,
  `web_launch` text,
  `prerequisite` varchar(255) DEFAULT NULL,
  `completion` varchar(255) DEFAULT NULL,
  `system_vendor` varchar(32) DEFAULT NULL,
  `core_vendor` text,
  `au_password` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `nChild` int(11) NOT NULL DEFAULT '0',
  `nDescendant` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_package` (`id_package`),
  CONSTRAINT `learning_aicc_item_ibfk_1` FOREIGN KEY (`id_package`) REFERENCES `learning_aicc_package` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_aicc_item`
--

LOCK TABLES `learning_aicc_item` WRITE;
/*!40000 ALTER TABLE `learning_aicc_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_aicc_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_aicc_item_track`
--

DROP TABLE IF EXISTS `learning_aicc_item_track`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_aicc_item_track` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `idItem` int(11) NOT NULL,
  `timemodified` timestamp NULL DEFAULT NULL,
  `lesson_status` varchar(128) DEFAULT NULL,
  `lesson_location` varchar(255) DEFAULT NULL,
  `lesson_mode` varchar(32) DEFAULT NULL,
  `total_time` varchar(16) DEFAULT '0000:00:00.00',
  `score_min` float DEFAULT NULL,
  `score_max` float DEFAULT NULL,
  `score_raw` float DEFAULT NULL,
  `suspend_data` blob,
  `launch_data` blob,
  `comments` blob,
  `comments_from_lms` blob,
  `first_access` timestamp NULL DEFAULT NULL,
  `last_access` timestamp NULL DEFAULT NULL,
  `cmi_json` text,
  `nChildCompleted` int(11) NOT NULL DEFAULT '0',
  `nDescendantCompleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idUser` (`idUser`),
  KEY `idItem` (`idItem`),
  CONSTRAINT `learning_aicc_item_track_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `learning_aicc_item_track_ibfk_2` FOREIGN KEY (`idItem`) REFERENCES `learning_aicc_item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_aicc_item_track`
--

LOCK TABLES `learning_aicc_item_track` WRITE;
/*!40000 ALTER TABLE `learning_aicc_item_track` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_aicc_item_track` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_aicc_package`
--

DROP TABLE IF EXISTS `learning_aicc_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_aicc_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idReference` int(11) DEFAULT NULL COMMENT 'Learning Object ID',
  `lms_original_filename` varchar(255) NOT NULL,
  `lms_id_user` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `course_creator` varchar(255) DEFAULT NULL,
  `course_id` varchar(255) DEFAULT NULL COMMENT 'AICC package course ID',
  `course_system` varchar(255) DEFAULT NULL,
  `course_title` varchar(255) DEFAULT NULL,
  `level` varchar(8) DEFAULT NULL,
  `max_fields_cst` int(11) DEFAULT NULL,
  `total_aus` int(11) DEFAULT NULL,
  `total_blocks` int(11) DEFAULT NULL,
  `version` varchar(8) DEFAULT NULL,
  `max_normal` int(11) DEFAULT NULL,
  `course_description` text,
  PRIMARY KEY (`id`),
  KEY `idReference` (`idReference`),
  CONSTRAINT `learning_aicc_package_ibfk_1` FOREIGN KEY (`idReference`) REFERENCES `learning_organization` (`idOrg`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_aicc_package`
--

LOCK TABLES `learning_aicc_package` WRITE;
/*!40000 ALTER TABLE `learning_aicc_package` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_aicc_package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_aicc_package_track`
--

DROP TABLE IF EXISTS `learning_aicc_package_track`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_aicc_package_track` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_package` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `status` varchar(16) NOT NULL DEFAULT 'not attempted',
  `nChildCompleted` int(11) NOT NULL DEFAULT '0',
  `nDescendantCompleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idscorm_item` (`id_package`) USING BTREE,
  KEY `idUser` (`idUser`) USING BTREE,
  CONSTRAINT `learning_aicc_package_track_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `learning_aicc_package_track_ibfk_2` FOREIGN KEY (`id_package`) REFERENCES `learning_aicc_package` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_aicc_package_track`
--

LOCK TABLES `learning_aicc_package_track` WRITE;
/*!40000 ALTER TABLE `learning_aicc_package_track` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_aicc_package_track` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_aicc_session`
--

DROP TABLE IF EXISTS `learning_aicc_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_aicc_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `idItem` int(11) NOT NULL,
  `hapcSession` varchar(128) NOT NULL,
  `timecreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `timemodified` timestamp NULL DEFAULT NULL,
  `mode` varchar(128) DEFAULT NULL,
  `status` varchar(128) DEFAULT NULL,
  `lesson_status` varchar(128) DEFAULT NULL,
  `session_time` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idUser` (`idUser`),
  KEY `idItem` (`idItem`),
  CONSTRAINT `learning_aicc_session_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `learning_aicc_session_ibfk_2` FOREIGN KEY (`idItem`) REFERENCES `learning_aicc_item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_aicc_session`
--

LOCK TABLES `learning_aicc_session` WRITE;
/*!40000 ALTER TABLE `learning_aicc_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_aicc_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_authoring`
--

DROP TABLE IF EXISTS `learning_authoring`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_authoring` (
  `authoring_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `file_version` int(11) NOT NULL,
  `file_extension` varchar(255) NOT NULL,
  `slide_count` int(11) NOT NULL DEFAULT '0',
  `status_id` int(11) NOT NULL,
  `slide_number` tinyint(1) NOT NULL DEFAULT '0',
  `playbar` tinyint(1) NOT NULL DEFAULT '0',
  `fullscreen` tinyint(1) NOT NULL DEFAULT '0',
  `background_color` varchar(20) NOT NULL,
  `text_color` varchar(20) NOT NULL,
  `hover_color` varchar(20) NOT NULL,
  `copy_of` int(11) DEFAULT NULL,
  `pipeline_id` varchar(255) NOT NULL,
  PRIMARY KEY (`authoring_id`),
  KEY `fk_learning_authoring_status_id_idx` (`status_id`),
  CONSTRAINT `learning_authoring_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `learning_authoring_status` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_authoring`
--

LOCK TABLES `learning_authoring` WRITE;
/*!40000 ALTER TABLE `learning_authoring` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_authoring` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_authoring_status`
--

DROP TABLE IF EXISTS `learning_authoring_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_authoring_status` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_authoring_status`
--

LOCK TABLES `learning_authoring_status` WRITE;
/*!40000 ALTER TABLE `learning_authoring_status` DISABLE KEYS */;
INSERT INTO `learning_authoring_status` VALUES (1,'pending'),(2,'confirmed'),(3,'published');
/*!40000 ALTER TABLE `learning_authoring_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_calendar`
--

DROP TABLE IF EXISTS `learning_calendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_calendar` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `idCourse` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_calendar`
--

LOCK TABLES `learning_calendar` WRITE;
/*!40000 ALTER TABLE `learning_calendar` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_calendar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_catalogue`
--

DROP TABLE IF EXISTS `learning_catalogue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_catalogue` (
  `idCatalogue` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  PRIMARY KEY (`idCatalogue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_catalogue`
--

LOCK TABLES `learning_catalogue` WRITE;
/*!40000 ALTER TABLE `learning_catalogue` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_catalogue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_catalogue_entry`
--

DROP TABLE IF EXISTS `learning_catalogue_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_catalogue_entry` (
  `idCatalogue` int(11) NOT NULL DEFAULT '0',
  `idEntry` int(11) NOT NULL DEFAULT '0',
  `type_of_entry` enum('course','coursepath') NOT NULL DEFAULT 'course',
  PRIMARY KEY (`idCatalogue`,`idEntry`,`type_of_entry`),
  CONSTRAINT `learning_catalogue_entry_ibfk_1` FOREIGN KEY (`idCatalogue`) REFERENCES `learning_catalogue` (`idCatalogue`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_catalogue_entry`
--

LOCK TABLES `learning_catalogue_entry` WRITE;
/*!40000 ALTER TABLE `learning_catalogue_entry` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_catalogue_entry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_catalogue_member`
--

DROP TABLE IF EXISTS `learning_catalogue_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_catalogue_member` (
  `idCatalogue` int(11) NOT NULL DEFAULT '0',
  `idst_member` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idCatalogue`,`idst_member`),
  CONSTRAINT `learning_catalogue_member_ibfk_1` FOREIGN KEY (`idCatalogue`) REFERENCES `learning_catalogue` (`idCatalogue`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_catalogue_member`
--

LOCK TABLES `learning_catalogue_member` WRITE;
/*!40000 ALTER TABLE `learning_catalogue_member` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_catalogue_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_category`
--

DROP TABLE IF EXISTS `learning_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_category` (
  `idCategory` int(11) NOT NULL AUTO_INCREMENT,
  `lang_code` varchar(50) NOT NULL DEFAULT '',
  `translation` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`idCategory`,`lang_code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_category`
--

LOCK TABLES `learning_category` WRITE;
/*!40000 ALTER TABLE `learning_category` DISABLE KEYS */;
INSERT INTO `learning_category` VALUES (1,'arabic','Docebo'),(1,'bosnian','Docebo'),(1,'bulgarian','Docebo'),(1,'croatian','Docebo'),(1,'czech','Docebo'),(1,'danish','Docebo'),(1,'dutch','Docebo'),(1,'english','Docebo'),(1,'farsi','Docebo'),(1,'finnish','Docebo'),(1,'french','Docebo'),(1,'german','Docebo'),(1,'greek','Docebo'),(1,'hebrew','Docebo'),(1,'hungarian','Docebo'),(1,'indonesian','Docebo'),(1,'italian','Docebo'),(1,'japanese','Docebo'),(1,'korean','Docebo'),(1,'norwegian','Docebo'),(1,'polish','Docebo'),(1,'portuguese','Docebo'),(1,'portuguese-br','Docebo'),(1,'romanian','Docebo'),(1,'russian','Docebo'),(1,'simplified_chinese','Docebo'),(1,'spanish','Docebo'),(1,'swedish','Docebo'),(1,'thai','Docebo'),(1,'turkish','Docebo'),(1,'ukrainian','Docebo');
/*!40000 ALTER TABLE `learning_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_category_tree`
--

DROP TABLE IF EXISTS `learning_category_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_category_tree` (
  `idCategory` int(11) NOT NULL AUTO_INCREMENT,
  `lev` int(11) NOT NULL DEFAULT '0',
  `iLeft` int(5) NOT NULL DEFAULT '0',
  `iRight` int(5) NOT NULL DEFAULT '0',
  `soft_deadline` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`idCategory`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_category_tree`
--

LOCK TABLES `learning_category_tree` WRITE;
/*!40000 ALTER TABLE `learning_category_tree` DISABLE KEYS */;
INSERT INTO `learning_category_tree` VALUES (1,1,1,2,NULL);
/*!40000 ALTER TABLE `learning_category_tree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_certificate`
--

DROP TABLE IF EXISTS `learning_certificate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_certificate` (
  `id_certificate` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `base_language` varchar(255) NOT NULL DEFAULT '',
  `cert_structure` text NOT NULL,
  `orientation` enum('P','L') NOT NULL DEFAULT 'P',
  `bgimage` varchar(255) NOT NULL DEFAULT '',
  `meta` tinyint(1) NOT NULL DEFAULT '0',
  `user_release` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_certificate`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_certificate`
--

LOCK TABLES `learning_certificate` WRITE;
/*!40000 ALTER TABLE `learning_certificate` DISABLE KEYS */;
INSERT INTO `learning_certificate` VALUES (2,'0000','Certificate sample','Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br />','english','<table style=\"margin-left: auto; margin-right: auto;\" border=\"0\">\n<tbody>\n<tr>\n<td>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</td>\n<td style=\"text-align: center;\" colspan=\"2\"><span style=\"font-size: 65px;\"><br /><br /><br /><br /><br /><br /><br />This certificate is awarded to<strong><strong><br />[display_name]<br /></strong><br /><br /><br /></strong>In recognition of your completion of the training course<strong><br />[course_name]<br /><br /><br /><br /><br /><br /></strong></span></td>\n<td>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</td>\n</tr>\n<tr>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n<td style=\"text-align: right;\"><span style=\"line-height: 19px; font-size: small;\"><strong><br /></strong></span></td>\n<td><span style=\"font-size: x-large;\"><strong>[theacher_list]</strong></span><br /><span style=\"font-size: x-large;\">The Instructor/s</span></td>\n</tr>\n</tbody>\n</table>\n<br />','L','certificate_sample.jpg',0,1);
/*!40000 ALTER TABLE `learning_certificate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_certificate_assign`
--

DROP TABLE IF EXISTS `learning_certificate_assign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_certificate_assign` (
  `id_certificate` int(11) NOT NULL DEFAULT '0',
  `id_course` int(11) NOT NULL DEFAULT '0',
  `id_user` int(11) NOT NULL DEFAULT '0',
  `on_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cert_file` varchar(255) NOT NULL DEFAULT '',
  `cert_number` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_certificate`,`id_course`,`id_user`),
  UNIQUE KEY `cert_number_UNIQUE` (`cert_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_certificate_assign`
--

LOCK TABLES `learning_certificate_assign` WRITE;
/*!40000 ALTER TABLE `learning_certificate_assign` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_certificate_assign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_certificate_assign_cp`
--

DROP TABLE IF EXISTS `learning_certificate_assign_cp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_certificate_assign_cp` (
  `id_certificate` int(11) NOT NULL DEFAULT '0',
  `id_path` int(11) NOT NULL DEFAULT '0',
  `id_user` int(11) NOT NULL DEFAULT '0',
  `on_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cert_file` varchar(255) DEFAULT NULL,
  `cert_number` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_certificate`,`id_path`,`id_user`),
  UNIQUE KEY `cert_number_UNIQUE_CP` (`cert_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_certificate_assign_cp`
--

LOCK TABLES `learning_certificate_assign_cp` WRITE;
/*!40000 ALTER TABLE `learning_certificate_assign_cp` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_certificate_assign_cp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_certificate_course`
--

DROP TABLE IF EXISTS `learning_certificate_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_certificate_course` (
  `id_certificate` int(11) NOT NULL DEFAULT '0',
  `id_course` int(11) NOT NULL DEFAULT '0',
  `available_for_status` tinyint(1) NOT NULL DEFAULT '0',
  `point_required` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_certificate`,`id_course`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_certificate_course`
--

LOCK TABLES `learning_certificate_course` WRITE;
/*!40000 ALTER TABLE `learning_certificate_course` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_certificate_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_certificate_coursepath`
--

DROP TABLE IF EXISTS `learning_certificate_coursepath`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_certificate_coursepath` (
  `id_certificate` int(11) NOT NULL DEFAULT '0',
  `id_path` int(11) NOT NULL DEFAULT '0',
  `available_for_status` tinyint(1) NOT NULL DEFAULT '0',
  `point_required` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_certificate`,`id_path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_certificate_coursepath`
--

LOCK TABLES `learning_certificate_coursepath` WRITE;
/*!40000 ALTER TABLE `learning_certificate_coursepath` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_certificate_coursepath` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_certificate_meta`
--

DROP TABLE IF EXISTS `learning_certificate_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_certificate_meta` (
  `idMetaCertificate` int(11) NOT NULL AUTO_INCREMENT,
  `idCertificate` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idMetaCertificate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_certificate_meta`
--

LOCK TABLES `learning_certificate_meta` WRITE;
/*!40000 ALTER TABLE `learning_certificate_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_certificate_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_certificate_meta_assign`
--

DROP TABLE IF EXISTS `learning_certificate_meta_assign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_certificate_meta_assign` (
  `idUser` int(11) NOT NULL DEFAULT '0',
  `idMetaCertificate` int(11) NOT NULL DEFAULT '0',
  `idCertificate` int(11) NOT NULL DEFAULT '0',
  `on_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cert_file` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`idUser`,`idMetaCertificate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_certificate_meta_assign`
--

LOCK TABLES `learning_certificate_meta_assign` WRITE;
/*!40000 ALTER TABLE `learning_certificate_meta_assign` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_certificate_meta_assign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_certificate_meta_course`
--

DROP TABLE IF EXISTS `learning_certificate_meta_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_certificate_meta_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idMetaCertificate` int(11) NOT NULL DEFAULT '0',
  `idUser` int(11) NOT NULL DEFAULT '0',
  `idCourse` int(11) NOT NULL DEFAULT '0',
  `idCourseEdition` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_certificate_meta_course`
--

LOCK TABLES `learning_certificate_meta_course` WRITE;
/*!40000 ALTER TABLE `learning_certificate_meta_course` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_certificate_meta_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_certificate_tags`
--

DROP TABLE IF EXISTS `learning_certificate_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_certificate_tags` (
  `file_name` varchar(255) NOT NULL DEFAULT '',
  `class_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`file_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_certificate_tags`
--

LOCK TABLES `learning_certificate_tags` WRITE;
/*!40000 ALTER TABLE `learning_certificate_tags` DISABLE KEYS */;
INSERT INTO `learning_certificate_tags` VALUES ('certificate.course.php','CertificateSubs_Course'),('certificate.misc.php','CertificateSubs_Misc'),('certificate.user.php','CertificateSubs_User'),('certificate.userstat.php','CertificateSubs_UserStat');
/*!40000 ALTER TABLE `learning_certificate_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_commontrack`
--

DROP TABLE IF EXISTS `learning_commontrack`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_commontrack` (
  `idReference` int(11) NOT NULL DEFAULT '0',
  `idUser` int(11) NOT NULL DEFAULT '0',
  `idTrack` int(11) NOT NULL DEFAULT '0',
  `objectType` varchar(20) NOT NULL DEFAULT '',
  `dateAttempt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` varchar(20) NOT NULL DEFAULT '',
  `first_status` varchar(20) NOT NULL,
  `firstAttempt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `first_complete` timestamp NULL DEFAULT NULL,
  `last_complete` timestamp NULL DEFAULT NULL,
  `first_score` float NOT NULL,
  `score` float DEFAULT NULL,
  `score_max` float DEFAULT NULL,
  `total_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`idTrack`,`objectType`),
  KEY `idReference` (`idReference`),
  KEY `idUser` (`idUser`),
  KEY `idTrack` (`idTrack`),
  CONSTRAINT `learning_commontrack_ibfk_1` FOREIGN KEY (`idReference`) REFERENCES `learning_organization` (`idOrg`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `learning_commontrack_ibfk_2` FOREIGN KEY (`idUser`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_commontrack`
--

LOCK TABLES `learning_commontrack` WRITE;
/*!40000 ALTER TABLE `learning_commontrack` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_commontrack` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_conversion`
--

DROP TABLE IF EXISTS `learning_conversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_conversion` (
  `track_id` int(11) NOT NULL AUTO_INCREMENT,
  `authoring_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `completion_status` varchar(20) NOT NULL,
  `suspend_data` varchar(255) NOT NULL,
  PRIMARY KEY (`track_id`),
  KEY `fk_learning_conversion_authoring_id_idx` (`authoring_id`),
  KEY `fk_learning_conversion_user_id_idx` (`user_id`),
  CONSTRAINT `fk_learning_conversion_authoring_id` FOREIGN KEY (`authoring_id`) REFERENCES `learning_authoring` (`authoring_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_learning_conversion_user_id` FOREIGN KEY (`user_id`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_conversion`
--

LOCK TABLES `learning_conversion` WRITE;
/*!40000 ALTER TABLE `learning_conversion` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_conversion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_course`
--

DROP TABLE IF EXISTS `learning_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_course` (
  `idCourse` int(11) NOT NULL AUTO_INCREMENT,
  `mpidCourse` int(11) NOT NULL COMMENT 'The ID of the course from the Marketplace db table (column course_profile_id in mp_course_profile table)',
  `idCategory` int(11) NOT NULL DEFAULT '0',
  `code` varchar(50) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `lang_code` varchar(100) NOT NULL DEFAULT '',
  `status` int(1) NOT NULL DEFAULT '0',
  `level_show_user` int(11) NOT NULL DEFAULT '0',
  `subscribe_method` tinyint(1) NOT NULL DEFAULT '0',
  `linkSponsor` varchar(255) NOT NULL DEFAULT '',
  `imgSponsor` varchar(255) NOT NULL DEFAULT '',
  `img_course` varchar(255) NOT NULL DEFAULT '',
  `img_demothumb` varchar(255) NOT NULL,
  `img_material` varchar(255) NOT NULL DEFAULT '',
  `img_othermaterial` varchar(255) NOT NULL DEFAULT '',
  `player_bg` varchar(255) DEFAULT NULL,
  `player_bg_aspect` varchar(255) DEFAULT NULL COMMENT 'fill,tile',
  `course_demo` varchar(255) NOT NULL DEFAULT '',
  `mediumTime` int(10) unsigned NOT NULL DEFAULT '0',
  `permCloseLO` tinyint(1) NOT NULL DEFAULT '0',
  `userStatusOp` int(11) NOT NULL DEFAULT '0',
  `difficult` enum('veryeasy','easy','medium','difficult','verydifficult') NOT NULL DEFAULT 'medium',
  `show_progress` tinyint(1) NOT NULL DEFAULT '1',
  `show_time` tinyint(1) NOT NULL DEFAULT '0',
  `show_who_online` tinyint(1) NOT NULL DEFAULT '0',
  `show_extra_info` tinyint(1) NOT NULL DEFAULT '0',
  `show_rules` tinyint(1) NOT NULL DEFAULT '0',
  `date_begin` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `hour_begin` varchar(5) NOT NULL DEFAULT '',
  `hour_end` varchar(5) NOT NULL DEFAULT '',
  `valid_time` int(10) NOT NULL DEFAULT '0',
  `abs_max_subscriptions` int(11) DEFAULT '0',
  `max_num_subscribe` int(11) NOT NULL DEFAULT '0',
  `min_num_subscribe` int(11) NOT NULL DEFAULT '0',
  `max_sms_budget` double NOT NULL DEFAULT '0',
  `selling` tinyint(1) NOT NULL DEFAULT '0',
  `prize` varchar(255) NOT NULL DEFAULT '',
  `course_type` varchar(255) NOT NULL DEFAULT 'elearning',
  `policy_point` varchar(255) NOT NULL DEFAULT '',
  `point_to_all` int(10) NOT NULL DEFAULT '0',
  `course_edition` tinyint(1) NOT NULL DEFAULT '0',
  `classrooms` varchar(255) NOT NULL DEFAULT '',
  `certificates` varchar(255) NOT NULL DEFAULT '',
  `create_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `security_code` varchar(255) NOT NULL DEFAULT '',
  `imported_from_connection` varchar(255) DEFAULT NULL,
  `course_quota` varchar(255) NOT NULL DEFAULT '-1',
  `used_space` varchar(255) NOT NULL DEFAULT '0',
  `course_vote` double NOT NULL DEFAULT '0',
  `allow_overbooking` tinyint(1) NOT NULL DEFAULT '0',
  `can_subscribe` tinyint(1) NOT NULL DEFAULT '0',
  `enable_self_unenrollment` tinyint(1) DEFAULT '0',
  `sub_start_date` timestamp NULL DEFAULT NULL,
  `sub_end_date` timestamp NULL DEFAULT NULL,
  `advance` varchar(255) NOT NULL DEFAULT '',
  `autoregistration_code` varchar(255) NOT NULL DEFAULT '',
  `direct_play` tinyint(1) NOT NULL DEFAULT '0',
  `use_logo_in_courselist` tinyint(1) NOT NULL DEFAULT '0',
  `show_result` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `credits` decimal(10,2) NOT NULL DEFAULT '0.00',
  `auto_unsubscribe` tinyint(1) NOT NULL DEFAULT '0',
  `unsubscribe_date_limit` timestamp NULL DEFAULT NULL,
  `prerequisites_policy` tinyint(1) NOT NULL DEFAULT '1',
  `player_layout` varchar(255) DEFAULT NULL,
  `social_rating_settings` text,
  `social_sharing_settings` text,
  `resume_autoplay` tinyint(1) DEFAULT '-1',
  `enable_coaching` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `learner_assignment` tinyint(1) unsigned NOT NULL DEFAULT '3',
  `coaching_option_allow_start_course` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `coaching_option_allow_request_session_change` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `final_score_mode` varchar(255) DEFAULT NULL,
  `final_object` int(11) DEFAULT NULL,
  `soft_deadline` tinyint(1) DEFAULT NULL,
  `initial_score_mode` varchar(255) DEFAULT NULL,
  `initial_object` int(11) DEFAULT NULL,
  `deep_link` tinyint(1) NOT NULL DEFAULT '0',
  `id_publisher` int(11) DEFAULT NULL,
  `publish_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idCourse`),
  KEY `idCategory` (`idCategory`),
  KEY `learning_course_final_object` (`final_object`),
  KEY `fk_learning_course_id_publisher_idx` (`id_publisher`),
  CONSTRAINT `fk_learning_course_id_publisher` FOREIGN KEY (`id_publisher`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `learning_course_final_object` FOREIGN KEY (`final_object`) REFERENCES `learning_organization` (`idOrg`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_course`
--

LOCK TABLES `learning_course` WRITE;
/*!40000 ALTER TABLE `learning_course` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_course_catch_up`
--

DROP TABLE IF EXISTS `learning_course_catch_up`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_course_catch_up` (
  `id_path` int(11) NOT NULL,
  `id_course` int(11) NOT NULL,
  `id_catch_up` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_path`,`id_course`,`id_catch_up`),
  KEY `learning_course_catch_up_ibfk_2` (`id_catch_up`),
  KEY `learning_course_catch_up_ibfk_1` (`id_course`),
  CONSTRAINT `learning_course_catch_up_ibfk_1` FOREIGN KEY (`id_course`) REFERENCES `learning_course` (`idCourse`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `learning_course_catch_up_ibfk_2` FOREIGN KEY (`id_catch_up`) REFERENCES `learning_course` (`idCourse`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `learning_course_catch_up_ibfk_3` FOREIGN KEY (`id_path`) REFERENCES `learning_coursepath` (`id_path`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_course_catch_up`
--

LOCK TABLES `learning_course_catch_up` WRITE;
/*!40000 ALTER TABLE `learning_course_catch_up` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_course_catch_up` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_course_coaching_messages`
--

DROP TABLE IF EXISTS `learning_course_coaching_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_course_coaching_messages` (
  `idMessage` int(11) NOT NULL AUTO_INCREMENT,
  `idSession` int(11) NOT NULL,
  `idCoach` int(11) NOT NULL,
  `idLearner` int(11) NOT NULL,
  `message` text NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0',
  `sent_by_coach` tinyint(2) NOT NULL DEFAULT '0',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_edited` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`idMessage`),
  KEY `FK_Session` (`idSession`),
  KEY `FK_Coach` (`idCoach`),
  KEY `FK_Learner` (`idLearner`),
  CONSTRAINT `FK_Coach` FOREIGN KEY (`idCoach`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Learner` FOREIGN KEY (`idLearner`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Session` FOREIGN KEY (`idSession`) REFERENCES `learning_course_coaching_session` (`idSession`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This db table holds the messages between coaches and learner';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_course_coaching_messages`
--

LOCK TABLES `learning_course_coaching_messages` WRITE;
/*!40000 ALTER TABLE `learning_course_coaching_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_course_coaching_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_course_coaching_session`
--

DROP TABLE IF EXISTS `learning_course_coaching_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_course_coaching_session` (
  `idSession` int(11) NOT NULL AUTO_INCREMENT,
  `idCourse` int(11) NOT NULL DEFAULT '0',
  `idCoach` int(11) NOT NULL DEFAULT '0',
  `max_users` int(11) NOT NULL DEFAULT '0',
  `datetime_start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `datetime_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`idSession`),
  KEY `FK_idCourse` (`idCourse`),
  KEY `FK_idCoach` (`idCoach`),
  CONSTRAINT `FK_idCoach` FOREIGN KEY (`idCoach`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_idCourse` FOREIGN KEY (`idCourse`) REFERENCES `learning_course` (`idCourse`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This db table holds the course coaching sessions';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_course_coaching_session`
--

LOCK TABLES `learning_course_coaching_session` WRITE;
/*!40000 ALTER TABLE `learning_course_coaching_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_course_coaching_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_course_coaching_session_user`
--

DROP TABLE IF EXISTS `learning_course_coaching_session_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_course_coaching_session_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idSession` int(11) NOT NULL,
  `idUser` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idSession_idUser` (`idSession`,`idUser`),
  KEY `FK_idSession` (`idSession`),
  KEY `FK_idUser` (`idUser`),
  CONSTRAINT `FK_idSession` FOREIGN KEY (`idSession`) REFERENCES `learning_course_coaching_session` (`idSession`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_idUser` FOREIGN KEY (`idUser`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This db table holds the refference between ''learning_courseu';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_course_coaching_session_user`
--

LOCK TABLES `learning_course_coaching_session_user` WRITE;
/*!40000 ALTER TABLE `learning_course_coaching_session_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_course_coaching_session_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_course_coaching_webinar_session`
--

DROP TABLE IF EXISTS `learning_course_coaching_webinar_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_course_coaching_webinar_session` (
  `id_session` int(11) NOT NULL AUTO_INCREMENT,
  `id_coaching_session` int(11) NOT NULL,
  `id_tool_account` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `date_begin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `tool` varchar(255) NOT NULL DEFAULT 'custom',
  `custom_url` varchar(1024) DEFAULT NULL,
  `tool_params` text,
  PRIMARY KEY (`id_session`),
  KEY `FK_coaching_session_idx` (`id_coaching_session`),
  KEY `FK_webinar_tool_account_idx` (`id_tool_account`),
  CONSTRAINT `FK_coaching_session` FOREIGN KEY (`id_coaching_session`) REFERENCES `learning_course_coaching_session` (`idSession`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_webinar_tool_account` FOREIGN KEY (`id_tool_account`) REFERENCES `webinar_tool_account` (`id_account`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_course_coaching_webinar_session`
--

LOCK TABLES `learning_course_coaching_webinar_session` WRITE;
/*!40000 ALTER TABLE `learning_course_coaching_webinar_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_course_coaching_webinar_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_course_coaching_webinar_session_user`
--

DROP TABLE IF EXISTS `learning_course_coaching_webinar_session_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_course_coaching_webinar_session_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_session` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `joined` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_coaching_webinar_session_idx` (`id_session`),
  KEY `FK_coaching_webinar_user_id_idx` (`id_user`),
  CONSTRAINT `FK_coaching_webinar_session` FOREIGN KEY (`id_session`) REFERENCES `learning_course_coaching_webinar_session` (`id_session`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_coaching_webinar_user_id` FOREIGN KEY (`id_user`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_course_coaching_webinar_session_user`
--

LOCK TABLES `learning_course_coaching_webinar_session_user` WRITE;
/*!40000 ALTER TABLE `learning_course_coaching_webinar_session_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_course_coaching_webinar_session_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_course_field`
--

DROP TABLE IF EXISTS `learning_course_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_course_field` (
  `id_field` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(60) NOT NULL,
  `sequence` int(5) NOT NULL DEFAULT '0',
  `invisible_to_user` tinyint(1) NOT NULL DEFAULT '0',
  `settings` text,
  PRIMARY KEY (`id_field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_course_field`
--

LOCK TABLES `learning_course_field` WRITE;
/*!40000 ALTER TABLE `learning_course_field` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_course_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_course_field_dropdown`
--

DROP TABLE IF EXISTS `learning_course_field_dropdown`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_course_field_dropdown` (
  `id_option` int(11) NOT NULL AUTO_INCREMENT,
  `id_field` int(11) NOT NULL,
  `sequence` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_option`),
  KEY `FK_course_field_dropdown` (`id_field`),
  CONSTRAINT `FK_course_field_dropdown` FOREIGN KEY (`id_field`) REFERENCES `learning_course_field` (`id_field`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_course_field_dropdown`
--

LOCK TABLES `learning_course_field_dropdown` WRITE;
/*!40000 ALTER TABLE `learning_course_field_dropdown` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_course_field_dropdown` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_course_field_dropdown_translations`
--

DROP TABLE IF EXISTS `learning_course_field_dropdown_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_course_field_dropdown_translations` (
  `id_option` int(11) NOT NULL AUTO_INCREMENT,
  `lang_code` varchar(255) NOT NULL DEFAULT '',
  `translation` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_option`,`lang_code`),
  CONSTRAINT `FK_course_field_dropdown_translations` FOREIGN KEY (`id_option`) REFERENCES `learning_course_field_dropdown` (`id_option`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_course_field_dropdown_translations`
--

LOCK TABLES `learning_course_field_dropdown_translations` WRITE;
/*!40000 ALTER TABLE `learning_course_field_dropdown_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_course_field_dropdown_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_course_field_translation`
--

DROP TABLE IF EXISTS `learning_course_field_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_course_field_translation` (
  `id_field` int(11) NOT NULL,
  `lang_code` varchar(255) NOT NULL DEFAULT '',
  `translation` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_field`,`lang_code`),
  CONSTRAINT `FK_course_field_translation` FOREIGN KEY (`id_field`) REFERENCES `learning_course_field` (`id_field`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_course_field_translation`
--

LOCK TABLES `learning_course_field_translation` WRITE;
/*!40000 ALTER TABLE `learning_course_field_translation` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_course_field_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_course_field_value`
--

DROP TABLE IF EXISTS `learning_course_field_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_course_field_value` (
  `id_course` int(11) NOT NULL,
  PRIMARY KEY (`id_course`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_course_field_value`
--

LOCK TABLES `learning_course_field_value` WRITE;
/*!40000 ALTER TABLE `learning_course_field_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_course_field_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_course_file`
--

DROP TABLE IF EXISTS `learning_course_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_course_file` (
  `id_file` int(11) NOT NULL AUTO_INCREMENT,
  `id_course` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `path` varchar(255) NOT NULL DEFAULT '',
  `position` int(11) DEFAULT '0',
  `created` timestamp NULL DEFAULT NULL,
  `original_filename` varchar(255) NOT NULL,
  `idUser` int(11) DEFAULT NULL,
  `lev` int(5) DEFAULT NULL,
  `iLeft` int(5) DEFAULT NULL,
  `iRight` int(5) DEFAULT NULL,
  `idParent` int(5) DEFAULT NULL,
  `item_type` int(5) DEFAULT NULL,
  PRIMARY KEY (`id_file`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_course_file`
--

LOCK TABLES `learning_course_file` WRITE;
/*!40000 ALTER TABLE `learning_course_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_course_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_course_rating`
--

DROP TABLE IF EXISTS `learning_course_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_course_rating` (
  `idCourse` int(11) NOT NULL,
  `rate_count` int(11) NOT NULL DEFAULT '0',
  `rate_sum` int(11) NOT NULL DEFAULT '0',
  `rate_average` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`idCourse`),
  CONSTRAINT `fk_id_course` FOREIGN KEY (`idCourse`) REFERENCES `learning_course` (`idCourse`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_course_rating`
--

LOCK TABLES `learning_course_rating` WRITE;
/*!40000 ALTER TABLE `learning_course_rating` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_course_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_course_rating_vote`
--

DROP TABLE IF EXISTS `learning_course_rating_vote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_course_rating_vote` (
  `idCourse` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  PRIMARY KEY (`idCourse`,`idUser`),
  KEY `fk_course_rating_vote_id_user` (`idUser`),
  CONSTRAINT `fk_course_rating_vote_id_course` FOREIGN KEY (`idCourse`) REFERENCES `learning_course` (`idCourse`) ON DELETE CASCADE,
  CONSTRAINT `fk_course_rating_vote_id_user` FOREIGN KEY (`idUser`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_course_rating_vote`
--

LOCK TABLES `learning_course_rating_vote` WRITE;
/*!40000 ALTER TABLE `learning_course_rating_vote` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_course_rating_vote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_coursepath`
--

DROP TABLE IF EXISTS `learning_coursepath`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_coursepath` (
  `id_path` int(11) NOT NULL AUTO_INCREMENT,
  `path_code` varchar(255) NOT NULL DEFAULT '',
  `path_name` varchar(255) NOT NULL DEFAULT '',
  `path_descr` text NOT NULL,
  `subscribe_method` tinyint(1) NOT NULL DEFAULT '0',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `img` int(11) DEFAULT NULL,
  `is_selling` tinyint(1) DEFAULT '0',
  `purchase_type` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `visible_in_catalog` tinyint(1) DEFAULT '0',
  `credits` varchar(255) DEFAULT NULL,
  `days_valid` int(11) DEFAULT NULL,
  `catchup_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `catchup_limit` tinyint(1) NOT NULL DEFAULT '0',
  `deep_link` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_coursepath`
--

LOCK TABLES `learning_coursepath` WRITE;
/*!40000 ALTER TABLE `learning_coursepath` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_coursepath` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_coursepath_courses`
--

DROP TABLE IF EXISTS `learning_coursepath_courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_coursepath_courses` (
  `id_path` int(11) NOT NULL DEFAULT '0',
  `id_item` int(11) NOT NULL DEFAULT '0',
  `in_slot` int(11) NOT NULL DEFAULT '0',
  `prerequisites` text NOT NULL,
  `sequence` int(3) NOT NULL DEFAULT '0',
  `mode` varchar(10) NOT NULL DEFAULT 'all' COMMENT 'all=All courses required | {n}=at least {n} courses',
  `delay_time` int(3) DEFAULT NULL,
  `delay_time_unit` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id_path`,`id_item`,`in_slot`),
  KEY `id_item` (`id_item`),
  CONSTRAINT `learning_coursepath_courses_ibfk_1` FOREIGN KEY (`id_path`) REFERENCES `learning_coursepath` (`id_path`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `learning_coursepath_courses_ibfk_2` FOREIGN KEY (`id_item`) REFERENCES `learning_course` (`idCourse`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_coursepath_courses`
--

LOCK TABLES `learning_coursepath_courses` WRITE;
/*!40000 ALTER TABLE `learning_coursepath_courses` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_coursepath_courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_coursepath_user`
--

DROP TABLE IF EXISTS `learning_coursepath_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_coursepath_user` (
  `id_path` int(11) NOT NULL DEFAULT '0',
  `idUser` int(11) NOT NULL DEFAULT '0',
  `waiting` tinyint(1) NOT NULL DEFAULT '0',
  `course_completed` int(3) NOT NULL DEFAULT '0',
  `date_assign` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `subscribed_by` int(11) NOT NULL DEFAULT '0',
  `date_begin_validity` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end_validity` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `catchup_user_limit` int(11) DEFAULT NULL,
  `deeplinked_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_path`,`idUser`),
  KEY `idUser` (`idUser`),
  CONSTRAINT `learning_coursepath_user_ibfk_1` FOREIGN KEY (`id_path`) REFERENCES `learning_coursepath` (`id_path`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `learning_coursepath_user_ibfk_2` FOREIGN KEY (`idUser`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_coursepath_user`
--

LOCK TABLES `learning_coursepath_user` WRITE;
/*!40000 ALTER TABLE `learning_coursepath_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_coursepath_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_courseuser`
--

DROP TABLE IF EXISTS `learning_courseuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_courseuser` (
  `idUser` int(11) NOT NULL DEFAULT '0',
  `idCourse` int(11) NOT NULL DEFAULT '0',
  `edition_id` int(11) NOT NULL DEFAULT '0',
  `level` int(11) NOT NULL DEFAULT '0',
  `date_inscr` timestamp NULL DEFAULT NULL,
  `date_first_access` timestamp NULL DEFAULT NULL,
  `date_last_access` timestamp NULL DEFAULT NULL,
  `date_complete` timestamp NULL DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `waiting` tinyint(1) NOT NULL DEFAULT '0',
  `subscribed_by` int(11) NOT NULL DEFAULT '0',
  `rule_log` int(11) DEFAULT NULL,
  `score_given` float(5,2) DEFAULT NULL,
  `imported_from_connection` varchar(255) DEFAULT NULL,
  `absent` tinyint(1) NOT NULL DEFAULT '0',
  `cancelled_by` int(11) NOT NULL DEFAULT '0',
  `new_forum_post` int(11) NOT NULL DEFAULT '0',
  `date_begin_validity` timestamp NULL DEFAULT NULL,
  `date_expire_validity` timestamp NULL DEFAULT NULL,
  `requesting_unsubscribe` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `requesting_unsubscribe_date` timestamp NULL DEFAULT NULL,
  `initial_score_given` float(5,2) DEFAULT NULL,
  `deeplinked_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`idUser`,`idCourse`,`edition_id`),
  KEY `idCourse` (`idCourse`),
  KEY `idUser` (`idUser`),
  KEY `level` (`level`),
  CONSTRAINT `learning_courseuser_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `learning_courseuser_ibfk_2` FOREIGN KEY (`idCourse`) REFERENCES `learning_course` (`idCourse`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_courseuser`
--

LOCK TABLES `learning_courseuser` WRITE;
/*!40000 ALTER TABLE `learning_courseuser` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_courseuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_deliverable`
--

DROP TABLE IF EXISTS `learning_deliverable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_deliverable` (
  `id_deliverable` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `allow_multiupload` tinyint(1) DEFAULT NULL,
  `learner_policy` tinyint(1) DEFAULT NULL COMMENT '0/1',
  PRIMARY KEY (`id_deliverable`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_deliverable`
--

LOCK TABLES `learning_deliverable` WRITE;
/*!40000 ALTER TABLE `learning_deliverable` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_deliverable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_deliverable_object`
--

DROP TABLE IF EXISTS `learning_deliverable_object`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_deliverable_object` (
  `idObject` int(11) NOT NULL AUTO_INCREMENT,
  `id_deliverable` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` enum('upload','link') NOT NULL DEFAULT 'upload',
  `content` text NOT NULL COMMENT 'uploaded file name | video url',
  `evaluation_score` int(11) DEFAULT '0',
  `evaluation_status` tinyint(1) DEFAULT '0' COMMENT '-1/0/1 = rejected/pending/accepted',
  `evaluation_date` timestamp NULL DEFAULT NULL,
  `evaluation_comments` text,
  `evaluation_file` varchar(255) DEFAULT NULL,
  `evaluation_user_id` int(11) DEFAULT NULL,
  `student_comments` text,
  `allow_another_upload` tinyint(1) DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_reply` text,
  PRIMARY KEY (`idObject`),
  KEY `fk_learning_deliverable_object_learning_deliverable1_idx` (`id_deliverable`),
  KEY `fk_learning_deliverable_object_core_user1_idx` (`id_user`),
  CONSTRAINT `learning_deliverable_object_ibfk_1` FOREIGN KEY (`id_deliverable`) REFERENCES `learning_deliverable` (`id_deliverable`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `learning_deliverable_object_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_deliverable_object`
--

LOCK TABLES `learning_deliverable_object` WRITE;
/*!40000 ALTER TABLE `learning_deliverable_object` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_deliverable_object` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_elucidat`
--

DROP TABLE IF EXISTS `learning_elucidat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_elucidat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_org` int(11) NOT NULL,
  `last_status` varchar(32) DEFAULT NULL,
  `last_status_updated` timestamp NULL DEFAULT NULL,
  `last_successful_update` timestamp NULL DEFAULT NULL,
  `p_project_code` varchar(64) NOT NULL,
  `p_name` varchar(255) NOT NULL,
  `p_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `p_project_key` varchar(128) NOT NULL DEFAULT 'none',
  `r_release_code` varchar(64) NOT NULL,
  `r_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `r_description` varchar(2048) DEFAULT NULL,
  `r_version_number` varchar(16) DEFAULT NULL,
  `r_release_mode` varchar(32) DEFAULT NULL,
  `edit_url` varchar(2000) DEFAULT NULL,
  `keep_updated` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_idOrg` (`id_org`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_elucidat`
--

LOCK TABLES `learning_elucidat` WRITE;
/*!40000 ALTER TABLE `learning_elucidat` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_elucidat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_forum`
--

DROP TABLE IF EXISTS `learning_forum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_forum` (
  `idForum` int(11) NOT NULL AUTO_INCREMENT,
  `idCourse` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `num_thread` int(11) NOT NULL DEFAULT '0',
  `num_post` int(11) NOT NULL DEFAULT '0',
  `last_post` int(11) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `sequence` int(5) NOT NULL DEFAULT '0',
  `emoticons` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`idForum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_forum`
--

LOCK TABLES `learning_forum` WRITE;
/*!40000 ALTER TABLE `learning_forum` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_forum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_forum_timing`
--

DROP TABLE IF EXISTS `learning_forum_timing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_forum_timing` (
  `idUser` int(11) NOT NULL DEFAULT '0',
  `idThread` int(11) NOT NULL DEFAULT '0',
  `last_access` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`idUser`,`idThread`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_forum_timing`
--

LOCK TABLES `learning_forum_timing` WRITE;
/*!40000 ALTER TABLE `learning_forum_timing` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_forum_timing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_forummessage`
--

DROP TABLE IF EXISTS `learning_forummessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_forummessage` (
  `idMessage` int(11) NOT NULL AUTO_INCREMENT,
  `idThread` int(11) NOT NULL DEFAULT '0',
  `idCourse` int(11) NOT NULL DEFAULT '0',
  `answer_tree` text NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `textof` text NOT NULL,
  `posted` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `author` int(11) NOT NULL DEFAULT '0',
  `generator` tinyint(1) NOT NULL DEFAULT '0',
  `attach` varchar(255) NOT NULL DEFAULT '',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `modified_by_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `tone_sum` int(11) NOT NULL DEFAULT '0',
  `tone_count` int(11) NOT NULL DEFAULT '0',
  `idReply` int(11) DEFAULT NULL,
  `path` text,
  `original_filename` text,
  PRIMARY KEY (`idMessage`),
  KEY `idThread` (`idThread`),
  KEY `learning_forummessage_ibfk_2` (`author`),
  CONSTRAINT `learning_forummessage_ibfk_1` FOREIGN KEY (`idThread`) REFERENCES `learning_forumthread` (`idThread`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `learning_forummessage_ibfk_2` FOREIGN KEY (`author`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_forummessage`
--

LOCK TABLES `learning_forummessage` WRITE;
/*!40000 ALTER TABLE `learning_forummessage` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_forummessage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_forummessage_votes`
--

DROP TABLE IF EXISTS `learning_forummessage_votes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_forummessage_votes` (
  `id_message` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `type` enum('tone','helpful') NOT NULL,
  `value` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_message`,`id_user`,`type`),
  KEY `fk_learning_forummessage_votes_core_user1_idx` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_forummessage_votes`
--

LOCK TABLES `learning_forummessage_votes` WRITE;
/*!40000 ALTER TABLE `learning_forummessage_votes` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_forummessage_votes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_forumthread`
--

DROP TABLE IF EXISTS `learning_forumthread`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_forumthread` (
  `idThread` int(11) NOT NULL AUTO_INCREMENT,
  `id_edition` int(11) NOT NULL DEFAULT '0',
  `idForum` int(11) NOT NULL DEFAULT '0',
  `posted` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(255) NOT NULL DEFAULT '',
  `author` int(11) NOT NULL DEFAULT '0',
  `num_post` int(11) NOT NULL DEFAULT '0',
  `num_view` int(5) NOT NULL DEFAULT '0',
  `last_post` int(11) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `erased` tinyint(1) NOT NULL DEFAULT '0',
  `important` tinyint(1) NOT NULL DEFAULT '0',
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  `emoticons` varchar(255) NOT NULL DEFAULT '',
  `rilevantForum` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idThread`),
  KEY `idForum` (`idForum`),
  KEY `FK_user_id` (`author`),
  CONSTRAINT `FK_user_id` FOREIGN KEY (`author`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE,
  CONSTRAINT `learning_forumthread_ibfk_1` FOREIGN KEY (`idForum`) REFERENCES `learning_forum` (`idForum`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_forumthread`
--

LOCK TABLES `learning_forumthread` WRITE;
/*!40000 ALTER TABLE `learning_forumthread` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_forumthread` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_htmlpage`
--

DROP TABLE IF EXISTS `learning_htmlpage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_htmlpage` (
  `idPage` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL DEFAULT '',
  `textof` longtext NOT NULL,
  `author` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idPage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_htmlpage`
--

LOCK TABLES `learning_htmlpage` WRITE;
/*!40000 ALTER TABLE `learning_htmlpage` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_htmlpage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_label`
--

DROP TABLE IF EXISTS `learning_label`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_label` (
  `id_common_label` int(11) NOT NULL DEFAULT '0',
  `lang_code` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `color` varchar(255) NOT NULL DEFAULT '',
  `sequence` int(11) unsigned NOT NULL DEFAULT '0',
  `icon` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_common_label`,`lang_code`),
  KEY `id_common_label` (`id_common_label`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_label`
--

LOCK TABLES `learning_label` WRITE;
/*!40000 ALTER TABLE `learning_label` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_label` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_label_course`
--

DROP TABLE IF EXISTS `learning_label_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_label_course` (
  `id_common_label` int(11) NOT NULL DEFAULT '0',
  `id_course` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_common_label`,`id_course`),
  KEY `learning_label_course_ibfk_2` (`id_course`),
  CONSTRAINT `learning_label_course_ibfk_1` FOREIGN KEY (`id_common_label`) REFERENCES `learning_label` (`id_common_label`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `learning_label_course_ibfk_2` FOREIGN KEY (`id_course`) REFERENCES `learning_course` (`idCourse`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_label_course`
--

LOCK TABLES `learning_label_course` WRITE;
/*!40000 ALTER TABLE `learning_label_course` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_label_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_lo_types`
--

DROP TABLE IF EXISTS `learning_lo_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_lo_types` (
  `objectType` varchar(20) NOT NULL DEFAULT '',
  `className` varchar(20) NOT NULL DEFAULT '',
  `fileName` varchar(50) NOT NULL DEFAULT '',
  `classNameTrack` varchar(255) NOT NULL DEFAULT '',
  `fileNameTrack` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`objectType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_lo_types`
--

LOCK TABLES `learning_lo_types` WRITE;
/*!40000 ALTER TABLE `learning_lo_types` DISABLE KEYS */;
INSERT INTO `learning_lo_types` VALUES ('authoring','Learning_Authoring','learning.authoring.php','Track_Authoring','track.authoring.php'),('faq','Learning_Faq','learning.faq.php','Track_Faq','track.faq.php'),('glossary','Learning_Glossary','learning.glossary.php','Track_Glossary','track.glossary.php'),('htmlpage','Learning_Htmlpage','learning.htmlpage.php','Track_Htmlpage','track.htmlpage.php'),('item','Learning_Item','learning.item.php','Track_Item','track.item.php'),('link','Learning_Link','learning.link.php','Track_Link','track.link.php'),('poll','Learning_Poll','learning.poll.php','Track_Poll','track.poll.php'),('scormorg','Learning_ScormOrg','learning.scorm.php','Track_Scormorg','track.scorm.php'),('test','Learning_Test','learning.test.php','Track_Test','track.test.php'),('tincan','Learning_Tincan','learning.tincan.php','Track_Tincan','track.tincan.php'),('video','Learning_Video','learning.video.php','Track_Video','track.video.php');
/*!40000 ALTER TABLE `learning_lo_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_log_marketplace_course_login`
--

DROP TABLE IF EXISTS `learning_log_marketplace_course_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_log_marketplace_course_login` (
  `idLog` int(11) NOT NULL AUTO_INCREMENT,
  `mpCourseId` int(11) NOT NULL COMMENT 'Marketplace Course ID',
  `idCourse` int(11) NOT NULL COMMENT 'Lms Course ID',
  `idst` int(11) NOT NULL COMMENT 'User ID',
  `dateFirstAccess` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idLog`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Keeps track of the user first access in a mp course';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_log_marketplace_course_login`
--

LOCK TABLES `learning_log_marketplace_course_login` WRITE;
/*!40000 ALTER TABLE `learning_log_marketplace_course_login` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_log_marketplace_course_login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_materials_lesson`
--

DROP TABLE IF EXISTS `learning_materials_lesson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_materials_lesson` (
  `idLesson` int(11) NOT NULL AUTO_INCREMENT,
  `author` int(11) NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `path` varchar(255) NOT NULL DEFAULT '',
  `original_filename` varchar(255) NOT NULL,
  PRIMARY KEY (`idLesson`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_materials_lesson`
--

LOCK TABLES `learning_materials_lesson` WRITE;
/*!40000 ALTER TABLE `learning_materials_lesson` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_materials_lesson` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_materials_track`
--

DROP TABLE IF EXISTS `learning_materials_track`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_materials_track` (
  `idTrack` int(11) NOT NULL AUTO_INCREMENT,
  `idResource` int(11) NOT NULL DEFAULT '0',
  `idReference` int(11) NOT NULL DEFAULT '0',
  `idUser` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idTrack`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_materials_track`
--

LOCK TABLES `learning_materials_track` WRITE;
/*!40000 ALTER TABLE `learning_materials_track` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_materials_track` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_menucourse_main`
--

DROP TABLE IF EXISTS `learning_menucourse_main`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_menucourse_main` (
  `idMain` int(11) NOT NULL AUTO_INCREMENT,
  `idCourse` int(11) NOT NULL DEFAULT '0',
  `sequence` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`idMain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_menucourse_main`
--

LOCK TABLES `learning_menucourse_main` WRITE;
/*!40000 ALTER TABLE `learning_menucourse_main` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_menucourse_main` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_menucourse_under`
--

DROP TABLE IF EXISTS `learning_menucourse_under`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_menucourse_under` (
  `idCourse` int(11) NOT NULL DEFAULT '0',
  `idModule` int(11) NOT NULL DEFAULT '0',
  `idMain` int(11) NOT NULL DEFAULT '0',
  `sequence` int(3) NOT NULL DEFAULT '0',
  `my_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`idCourse`,`idModule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_menucourse_under`
--

LOCK TABLES `learning_menucourse_under` WRITE;
/*!40000 ALTER TABLE `learning_menucourse_under` DISABLE KEYS */;
INSERT INTO `learning_menucourse_under` VALUES (0,1,0,1,''),(0,2,0,2,''),(0,3,0,3,''),(0,4,0,4,''),(0,5,0,5,''),(0,6,0,6,''),(0,7,0,2,''),(0,8,0,8,''),(0,9,0,9,''),(0,32,0,4,''),(0,33,5,11,''),(0,34,0,3,''),(0,35,1,1,''),(0,36,1,2,''),(0,37,1,3,''),(0,38,1,4,''),(0,39,1,5,''),(0,47,0,1,''),(0,48,1,6,''),(0,49,0,12,'');
/*!40000 ALTER TABLE `learning_menucourse_under` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_middlearea`
--

DROP TABLE IF EXISTS `learning_middlearea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_middlearea` (
  `obj_index` varchar(255) NOT NULL DEFAULT '',
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  `idst_list` text NOT NULL,
  PRIMARY KEY (`obj_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_middlearea`
--

LOCK TABLES `learning_middlearea` WRITE;
/*!40000 ALTER TABLE `learning_middlearea` DISABLE KEYS */;
INSERT INTO `learning_middlearea` VALUES ('credits',1,'a:0:{}'),('mo_32',1,'a:0:{}'),('mo_33',1,'a:0:{}'),('mo_34',1,'a:0:{}'),('mo_47',0,'a:0:{}'),('mo_48',1,'a:0:{}'),('mo_7',1,'a:0:{}'),('mo_help',1,'a:0:{}'),('mo_message',1,'a:0:{}'),('news',1,'a:0:{}'),('tb_assessment',1,'a:0:{}'),('tb_catalog',1,'a:0:{}'),('tb_classroom',1,'a:0:{}'),('tb_communication',1,'a:0:{}'),('tb_coursepath',1,'a:0:{}'),('tb_games',1,'a:0:{}'),('tb_label',1,'a:0:{}'),('tb_videoconference',1,'a:0:{}'),('training_actv',0,'a:0:{}'),('user_details_full',1,'a:0:{}');
/*!40000 ALTER TABLE `learning_middlearea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_module`
--

DROP TABLE IF EXISTS `learning_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_module` (
  `idModule` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) NOT NULL DEFAULT '',
  `default_op` varchar(255) NOT NULL DEFAULT '',
  `default_name` varchar(255) NOT NULL DEFAULT '',
  `token_associated` varchar(100) NOT NULL DEFAULT '',
  `file_name` varchar(255) NOT NULL DEFAULT '',
  `class_name` varchar(255) NOT NULL DEFAULT '',
  `module_info` varchar(50) NOT NULL DEFAULT '',
  `mvc_path` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`idModule`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_module`
--

LOCK TABLES `learning_module` WRITE;
/*!40000 ALTER TABLE `learning_module` DISABLE KEYS */;
INSERT INTO `learning_module` VALUES (1,'course','','_MYCOURSES','view','','','all','elearning/show'),(3,'profile','profile','_PROFILE','view','class.profile.php','Module_Profile','_user',''),(10,'course','infocourse','_INFCOURSE','view_info','class.course.php','Module_Course','',''),(11,'advice','advice','_ADVICE','view','class.advice.php','Module_Advice','',''),(12,'storage','display','_STORAGE','view','class.storage.php','Module_Storage','',''),(13,'calendar','calendar','_CALENDAR','view','class.calendar.php','Module_Calendar','',''),(16,'reservation','reservation','_RESERVATION','view','class.reservation.php','Module_Reservation','',''),(17,'light_repo','repolist','_LIGHT_REPO','view','class.light_repo.php','Module_Light_Repo','',''),(18,'htmlfront','showhtml','_HTMLFRONT','view','class.htmlfront.php','Module_Htmlfront','',''),(19,'forum','forum','_FORUM','view','class.forum.php','Module_Forum','',''),(20,'wiki','main','_WIKI','view','class.wiki.php','Module_Wiki','',''),(21,'chat','chat','_CHAT','view','class.chat.php','Module_Chat','',''),(24,'groups','groups','_GROUPS','view','class.groups.php','Module_Groups','',''),(25,'organization','','_ORGANIZATION','view','','','','lms/coursedocs/show'),(26,'coursereport','coursereport','_COURSEREPORT','view','class.coursereport.php','Module_CourseReport','','coursereport/show'),(27,'newsletter','view','_NEWSLETTER','view','class.newsletter.php','Module_Newsletter','',''),(28,'manmenu','manmenu','_MAN_MENU','view','class.manmenu.php','Module_CourseManmenu','',''),(29,'statistic','statistic','_STAT','view','class.statistic.php','Module_Statistic','',''),(30,'stats','statuser','_STATUSER','view_user','class.stats.php','Module_Stats','',''),(31,'stats','statcourse','_STATCOURSE','view_course','class.stats.php','Module_Stats','',''),(33,'course_autoregistration','course_autoregistration','_COURSE_AUTOREGISTRATION','view','class.course_autoregistration.php','Module_Course_Autoregistration','all','course/autoreg'),(35,'pusermanagement','','_PUBLIC_USER_ADMIN','view','','','public_admin','lms/pusermanagement/show'),(36,'pcourse','','_PUBLIC_COURSE_ADMIN','view','','','public_admin','lms/pcourse/show'),(38,'public_report_admin','reportlist','_PUBLIC_REPORT_ADMIN','view','class.public_report_admin.php','Module_Public_Report_Admin','public_admin',''),(39,'public_newsletter_admin','newsletter','_PUBLIC_NEWSLETTER_ADMIN','view','class.public_newsletter_admin.php','Module_Public_Newsletter_Admin','public_admin',''),(40,'quest_bank','main','_QUEST_BANK','view','class.quest_bank.php','Module_QuestBank','',''),(41,'coursecharts','','_COURSECHART','view','','','','lms/statistics/general'),(42,'coursestats','show','_COURSESTATS','view','','','','coursestats/show'),(44,'pcertificate','certificate','_PUBLIC_CERTIFICATE_ADMIN','view','class.pcertificate.php','Module_Pcertificate','public_admin',''),(45,'presence','','_PRESENCE','view','','','','presence/presence'),(47,'course','','_CATALOGUE','view','','','all','lms/catalog/show'),(48,'course','','_COURSEPATH','view','','','all','curricula/show'),(49,'mycalendar','','My Calendar','view','','','all','mycalendar/show');
/*!40000 ALTER TABLE `learning_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_organization`
--

DROP TABLE IF EXISTS `learning_organization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_organization` (
  `idOrg` int(11) NOT NULL AUTO_INCREMENT,
  `idParent` int(11) NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `lev` int(10) NOT NULL DEFAULT '0',
  `iLeft` int(11) unsigned NOT NULL,
  `iRight` int(11) unsigned NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `objectType` varchar(20) NOT NULL DEFAULT '',
  `idResource` int(11) NOT NULL DEFAULT '0',
  `idCategory` int(11) NOT NULL DEFAULT '0',
  `idUser` int(11) NOT NULL DEFAULT '0',
  `idAuthor` int(11) NOT NULL DEFAULT '0',
  `mobile` tinyint(1) NOT NULL,
  `version` varchar(8) NOT NULL DEFAULT '',
  `difficult` enum('_VERYEASY','_EASY','_MEDIUM','_DIFFICULT','_VERYDIFFICULT') NOT NULL DEFAULT '_VERYEASY',
  `description` text NOT NULL,
  `language` varchar(50) NOT NULL DEFAULT '',
  `resource` varchar(255) NOT NULL DEFAULT '',
  `objective` text NOT NULL,
  `dateInsert` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `idCourse` int(11) NOT NULL DEFAULT '0',
  `prerequisites` varchar(255) NOT NULL DEFAULT '',
  `isTerminator` tinyint(4) NOT NULL DEFAULT '0',
  `idParam` int(11) NOT NULL DEFAULT '0',
  `visible` tinyint(4) NOT NULL DEFAULT '1',
  `milestone` enum('start','end','-') NOT NULL DEFAULT '-',
  `width` varchar(4) NOT NULL DEFAULT '',
  `height` varchar(4) NOT NULL DEFAULT '',
  `publish_from` timestamp NULL DEFAULT NULL,
  `publish_to` timestamp NULL DEFAULT NULL,
  `access` varchar(255) DEFAULT NULL,
  `publish_for` int(1) NOT NULL DEFAULT '0',
  `location` varchar(255) DEFAULT NULL,
  `params_json` text NOT NULL COMMENT 'Extra JSON encoded parameters',
  `self_prerequisite` varchar(64) NOT NULL,
  `short_description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idOrg`),
  KEY `idParent` (`idParent`),
  KEY `path` (`path`),
  KEY `idCourse` (`idCourse`),
  KEY `idResource` (`idResource`),
  CONSTRAINT `learning_organization_ibfk_1` FOREIGN KEY (`idCourse`) REFERENCES `learning_course` (`idCourse`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_organization`
--

LOCK TABLES `learning_organization` WRITE;
/*!40000 ALTER TABLE `learning_organization` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_organization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_poll`
--

DROP TABLE IF EXISTS `learning_poll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_poll` (
  `id_poll` int(11) NOT NULL AUTO_INCREMENT,
  `author` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  PRIMARY KEY (`id_poll`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_poll`
--

LOCK TABLES `learning_poll` WRITE;
/*!40000 ALTER TABLE `learning_poll` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_poll` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_pollquest`
--

DROP TABLE IF EXISTS `learning_pollquest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_pollquest` (
  `id_quest` int(11) NOT NULL AUTO_INCREMENT,
  `id_poll` int(11) NOT NULL DEFAULT '0',
  `id_category` int(11) NOT NULL DEFAULT '0',
  `type_quest` varchar(255) NOT NULL DEFAULT '',
  `title_quest` text NOT NULL,
  `sequence` int(5) NOT NULL DEFAULT '0',
  `page` int(11) NOT NULL DEFAULT '1',
  `settings` text,
  PRIMARY KEY (`id_quest`),
  KEY `fk_id_poll_learning_pollquest_idx` (`id_poll`),
  CONSTRAINT `fk_id_poll_learning_pollquest` FOREIGN KEY (`id_poll`) REFERENCES `learning_poll` (`id_poll`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_pollquest`
--

LOCK TABLES `learning_pollquest` WRITE;
/*!40000 ALTER TABLE `learning_pollquest` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_pollquest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_pollquest_extra`
--

DROP TABLE IF EXISTS `learning_pollquest_extra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_pollquest_extra` (
  `id_quest` int(11) NOT NULL DEFAULT '0',
  `id_answer` int(11) NOT NULL DEFAULT '0',
  `extra_info` text NOT NULL,
  PRIMARY KEY (`id_quest`,`id_answer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_pollquest_extra`
--

LOCK TABLES `learning_pollquest_extra` WRITE;
/*!40000 ALTER TABLE `learning_pollquest_extra` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_pollquest_extra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_pollquestanswer`
--

DROP TABLE IF EXISTS `learning_pollquestanswer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_pollquestanswer` (
  `id_answer` int(11) NOT NULL AUTO_INCREMENT,
  `id_quest` int(11) NOT NULL DEFAULT '0',
  `sequence` int(11) NOT NULL DEFAULT '0',
  `answer` text NOT NULL,
  PRIMARY KEY (`id_answer`),
  KEY `fk_id_quest_learning_pollquestanswer_idx` (`id_quest`),
  CONSTRAINT `fk_id_quest_learning_pollquestanswer` FOREIGN KEY (`id_quest`) REFERENCES `learning_pollquest` (`id_quest`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_pollquestanswer`
--

LOCK TABLES `learning_pollquestanswer` WRITE;
/*!40000 ALTER TABLE `learning_pollquestanswer` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_pollquestanswer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_polltrack`
--

DROP TABLE IF EXISTS `learning_polltrack`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_polltrack` (
  `id_track` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `id_reference` int(11) NOT NULL DEFAULT '0',
  `id_poll` int(11) NOT NULL DEFAULT '0',
  `date_attempt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('valid','not_complete') NOT NULL DEFAULT 'not_complete',
  PRIMARY KEY (`id_track`),
  KEY `fk_id_user_learning_polltrack_idx` (`id_user`),
  KEY `fk_id_poll_learning_polltrack_idx` (`id_poll`),
  CONSTRAINT `fk_id_poll_learning_polltrack` FOREIGN KEY (`id_poll`) REFERENCES `learning_poll` (`id_poll`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_id_user_learning_polltrack` FOREIGN KEY (`id_user`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_polltrack`
--

LOCK TABLES `learning_polltrack` WRITE;
/*!40000 ALTER TABLE `learning_polltrack` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_polltrack` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_polltrack_answer`
--

DROP TABLE IF EXISTS `learning_polltrack_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_polltrack_answer` (
  `id_track` int(11) NOT NULL DEFAULT '0',
  `id_quest` int(11) NOT NULL DEFAULT '0',
  `id_answer` int(11) NOT NULL DEFAULT '0',
  `more_info` longtext NOT NULL,
  PRIMARY KEY (`id_track`,`id_quest`,`id_answer`),
  KEY `fk_id_track_learning_polltrack_answer_idx` (`id_track`),
  CONSTRAINT `fk_id_track_learning_polltrack_answer` FOREIGN KEY (`id_track`) REFERENCES `learning_polltrack` (`id_track`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_polltrack_answer`
--

LOCK TABLES `learning_polltrack_answer` WRITE;
/*!40000 ALTER TABLE `learning_polltrack_answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_polltrack_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_quest_category`
--

DROP TABLE IF EXISTS `learning_quest_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_quest_category` (
  `idCategory` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `textof` text NOT NULL,
  `author` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idCategory`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_quest_category`
--

LOCK TABLES `learning_quest_category` WRITE;
/*!40000 ALTER TABLE `learning_quest_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_quest_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_report`
--

DROP TABLE IF EXISTS `learning_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_report` (
  `id_report` int(11) NOT NULL AUTO_INCREMENT,
  `report_name` varchar(255) NOT NULL DEFAULT '',
  `class_name` varchar(255) NOT NULL DEFAULT '',
  `file_name` varchar(255) NOT NULL DEFAULT '',
  `use_user_selection` enum('true','false') NOT NULL DEFAULT 'true',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_report`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_report`
--

LOCK TABLES `learning_report` WRITE;
/*!40000 ALTER TABLE `learning_report` DISABLE KEYS */;
INSERT INTO `learning_report` VALUES (2,'user_report','Report_User','class.report_user.php','true',1),(4,'courses_report','Report_Courses','class.report_courses.php','true',1),(5,'aggregate_report','Report_Aggregate','class.report_aggregate.php','true',1);
/*!40000 ALTER TABLE `learning_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_report_access`
--

DROP TABLE IF EXISTS `learning_report_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_report_access` (
  `id_report` int(11) NOT NULL AUTO_INCREMENT,
  `visibility_type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_report`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_report_access`
--

LOCK TABLES `learning_report_access` WRITE;
/*!40000 ALTER TABLE `learning_report_access` DISABLE KEYS */;
INSERT INTO `learning_report_access` VALUES (1,'public'),(2,'public'),(3,'public'),(4,'public'),(5,'public'),(6,'public');
/*!40000 ALTER TABLE `learning_report_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_report_access_members`
--

DROP TABLE IF EXISTS `learning_report_access_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_report_access_members` (
  `id_report` int(11) NOT NULL,
  `member_type` varchar(100) DEFAULT NULL,
  `member_id` int(11) NOT NULL,
  `select_state` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_report_access_members`
--

LOCK TABLES `learning_report_access_members` WRITE;
/*!40000 ALTER TABLE `learning_report_access_members` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_report_access_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_report_filter`
--

DROP TABLE IF EXISTS `learning_report_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_report_filter` (
  `id_filter` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `report_type_id` int(11) DEFAULT NULL,
  `author` int(11) unsigned NOT NULL DEFAULT '0',
  `creation_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `filter_name` varchar(255) NOT NULL DEFAULT '',
  `filter_data` mediumblob NOT NULL,
  `is_public` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `views` int(11) NOT NULL DEFAULT '0',
  `is_standard` tinyint(1) NOT NULL,
  `id_job` int(11) DEFAULT NULL COMMENT 'Scheduled JOB',
  PRIMARY KEY (`id_filter`),
  KEY `report_type_id` (`report_type_id`),
  KEY `id_job` (`id_job`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_report_filter`
--

LOCK TABLES `learning_report_filter` WRITE;
/*!40000 ALTER TABLE `learning_report_filter` DISABLE KEYS */;
INSERT INTO `learning_report_filter` VALUES (1,1,0,'0000-00-00 00:00:00','Users - Courses','{\"fields\":{\"user\":{\"userid\":\"1\",\"firstname\":\"1\",\"lastname\":\"1\",\"email\":\"1\",\"register_date\":\"1\",\"valid\":\"1\",\"suspend_date\":\"1\"},\"course\":{\"name\":\"1\",\"coursesCategory.translation\":\"1\",\"code\":\"1\",\"status\":\"1\",\"credits\":\"1\"},\"enrollment\":	{\"level\":\"1\",\"date_inscr\":\"1\",\"date_first_access\":\"1\",\"date_complete\":\"1\",\"status\":\"1\",\"number_of_sessions\":\"1\",\"total_time_in_course\":\"1\"}}}',1,0,1,NULL),(2,2,0,'0000-00-00 00:00:00','Users - Delay','{\"fields\":{\"user\":{\"userid\":\"1\",\"firstname\":\"1\",\"lastname\":\"1\",\"email\":\"1\",\"register_date\":\"1\",\"valid\":\"1\",\"suspend_date\":\"1\"},\"course\": {\"name\":\"1\",\"coursesCategory.translation\":\"1\",\"code\":\"1\",\"status\":\"1\",\"credits\":\"1\"}}}',1,0,1,NULL),(3,3,0,'0000-00-00 00:00:00','Users - Learning Objects','{\"fields\":{\"user\":{\"userid\":\"1\",\"firstname\":\"1\",\"lastname\":\"1\",\"email\":\"1\",\"register_date\":\"1\",\"valid\":\"1\",\"suspend_date\":\"1\",\"528\":\"1\"},\"course\":{\"name\":\"1\",\"coursesCategory.translation\":\"1\",\"code\":\"1\",\"status\":\"1\",\"credits\":\"1\"},\"learning_object\":{\"objectType\":\"1\",\"firstAttempt\":\"1\",\"dateAttempt\":\"1\",\"status\":\"1\",\"score\":\"1\"}}}',1,0,1,NULL),(4,4,0,'0000-00-00 00:00:00','Courses - Users','{\"fields\":{\"course\":{\"name\":\"1\",\"coursesCategory.translation\":\"1\",\"code\":\"1\",\"status\":\"1\",\"credits\":\"1\"},\"stat\":{\"total_subscribed_users\":\"1\",\"number_of_users_who_has_not_started_yet\":\"1\",\"number_of_users_in_progress\":\"1\",\"number_of_users_completed\":\"1\",\"total_time_spent_by_users_in_the_course\":\"1\",\"show_percents\":\"1\"}}}',1,0,1,NULL),(5,5,0,'0000-00-00 00:00:00','Groups - Courses','{\"fields\":{\"course\":{\"name\":\"1\",\"coursesCategory.translation\":\"1\",\"code\":\"1\",\"status\":\"1\",\"credits\":\"1\"},\"stat\":{\"total_subscribed_users\":\"1\",\"number_of_users_who_has_not_started_yet\":\"1\",\"number_of_users_in_progress\":\"1\",\"number_of_users_completed\":\"1\",\"total_time_spent_by_users_in_the_course\":\"1\",\"show_percents\":\"1\"},\"group\":{\"group_name\":\"1\",\"type\":\"1\",\"total_users_in_group\":\"1\",\"course_name\":\"1\"}},}',1,0,1,NULL),(6,10,0,'2013-07-17 09:18:10','Users - Learning Plans','{\"filters\":{\"subscription_status\":\"all\"},\"fields\":{\"user\":{\"userid\":\"1\",\"firstname\":\"1\",\"lastname\":\"1\",\"email\":\"1\",\"register_date\":\"1\",\"valid\":\"1\",\"suspend_date\":\"1\"},\"plans\":{\"plan_code\":\"1\",\"plan_credits\":\"1\"},\"plansUsers\":{\"plan_subDate\":\"1\",\"plan_compDate\":\"1\",\"plan_compStatus\":\"1\",\"plan_compPercent\":\"1\"}}}',1,0,1,NULL),(7,29,0,'0000-00-00 00:00:00','Users - Badges','{\"filters\":{\"date_from\":null,\"date_to\":null,\"custom_fields\":{\"32\":\"\",\"94\":\"\"}},\"fields\":{\"user\":{\"userid\":\"1\",\"firstname\":\"1\",\"lastname\":\"1\",\"email\":\"1\"},\"badges\":{\"icon\":\"1\",\"name\":\"1\",\"description\":\"1\",\"score\":\"1\"},\"assignment\":{\"issued_on\":\"1\",\"event_key\":\"1\"}},\"users\":[],\"plans\":[],\"groups\":[],\"courses\":[],\"orgchartnodes\":[],\"certifications\":[],\"badges\":[],\"reportId\":\"\",\"author\":\"0\",\"id_filter\":\"\",\"report_type_id\":\"29\",\"creation_date\":\"12/4/2015 11:36:12 am\",\"filter_name\":\"ub2\",\"is_public\":\"0\",\"is_standard\":\"0\",\"id_job\":null,\"order\":{\"orderBy\":\"user.userid\",\"type\":\"ASC\"}}',0,0,1,NULL);
/*!40000 ALTER TABLE `learning_report_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_report_type`
--

DROP TABLE IF EXISTS `learning_report_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_report_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(120) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_report_type`
--

LOCK TABLES `learning_report_type` WRITE;
/*!40000 ALTER TABLE `learning_report_type` DISABLE KEYS */;
INSERT INTO `learning_report_type` VALUES (1,'Users - Courses'),(2,'Users - Delay'),(3,'Users - Learning Objects'),(4,'Courses - Users'),(5,'Groups - Courses'),(6,'Ecommerce - Transactions'),(10,'Users - Learning Plans'),(20,'User - Session'),(26,'Certifications - Users'),(27,'Users - Certifications'),(28,'Users - External Training'),(29,'Users - Badges'),(30,'Users - Contests');
/*!40000 ALTER TABLE `learning_report_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_scorm_items`
--

DROP TABLE IF EXISTS `learning_scorm_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_scorm_items` (
  `idscorm_item` int(11) NOT NULL AUTO_INCREMENT,
  `idscorm_organization` int(11) NOT NULL DEFAULT '0',
  `idscorm_parentitem` int(11) DEFAULT NULL,
  `adlcp_prerequisites` varchar(200) DEFAULT NULL,
  `adlcp_maxtimeallowed` varchar(24) DEFAULT NULL,
  `adlcp_timelimitaction` varchar(24) DEFAULT NULL,
  `adlcp_datafromlms` text,
  `adlcp_masteryscore` varchar(200) DEFAULT NULL,
  `item_identifier` varchar(255) DEFAULT NULL,
  `identifierref` varchar(255) DEFAULT NULL,
  `idscorm_resource` int(11) DEFAULT NULL,
  `isvisible` set('true','false') DEFAULT 'true',
  `parameters` varchar(100) DEFAULT NULL,
  `title` varchar(100) NOT NULL DEFAULT '',
  `nChild` int(11) NOT NULL DEFAULT '0',
  `nDescendant` int(11) NOT NULL DEFAULT '0',
  `adlcp_completionthreshold` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`idscorm_item`),
  UNIQUE KEY `idscorm_organization` (`idscorm_organization`,`item_identifier`),
  KEY `idscorm_organization_2` (`idscorm_organization`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_scorm_items`
--

LOCK TABLES `learning_scorm_items` WRITE;
/*!40000 ALTER TABLE `learning_scorm_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_scorm_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_scorm_items_track`
--

DROP TABLE IF EXISTS `learning_scorm_items_track`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_scorm_items_track` (
  `idscorm_item_track` int(11) NOT NULL AUTO_INCREMENT,
  `idscorm_organization` int(11) NOT NULL DEFAULT '0',
  `idscorm_item` int(11) DEFAULT NULL,
  `idReference` int(11) NOT NULL DEFAULT '0',
  `idUser` int(11) NOT NULL DEFAULT '0',
  `idscorm_tracking` int(11) DEFAULT NULL,
  `status` varchar(16) NOT NULL DEFAULT 'not attempted',
  `nChild` int(11) NOT NULL DEFAULT '0',
  `nChildCompleted` int(11) NOT NULL DEFAULT '0',
  `nDescendant` int(11) NOT NULL DEFAULT '0',
  `nDescendantCompleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idscorm_item_track`),
  KEY `idscorm_organization` (`idscorm_organization`),
  KEY `idscorm_item` (`idscorm_item`),
  KEY `idUser` (`idUser`),
  KEY `idscorm_tracking` (`idscorm_tracking`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Join table 3 factor';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_scorm_items_track`
--

LOCK TABLES `learning_scorm_items_track` WRITE;
/*!40000 ALTER TABLE `learning_scorm_items_track` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_scorm_items_track` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_scorm_organizations`
--

DROP TABLE IF EXISTS `learning_scorm_organizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_scorm_organizations` (
  `idscorm_organization` int(11) NOT NULL AUTO_INCREMENT,
  `org_identifier` varchar(255) NOT NULL DEFAULT '',
  `idscorm_package` int(11) NOT NULL DEFAULT '0',
  `title` varchar(100) DEFAULT NULL,
  `nChild` int(11) NOT NULL DEFAULT '0',
  `nDescendant` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idscorm_organization`),
  KEY `idscorm_package` (`idscorm_package`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_scorm_organizations`
--

LOCK TABLES `learning_scorm_organizations` WRITE;
/*!40000 ALTER TABLE `learning_scorm_organizations` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_scorm_organizations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_scorm_package`
--

DROP TABLE IF EXISTS `learning_scorm_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_scorm_package` (
  `idscorm_package` int(11) NOT NULL AUTO_INCREMENT,
  `idpackage` varchar(255) NOT NULL DEFAULT '',
  `idProg` int(11) NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `location` varchar(255) DEFAULT NULL,
  `defaultOrg` varchar(255) NOT NULL DEFAULT '',
  `idUser` int(11) NOT NULL DEFAULT '0',
  `scormVersion` varchar(10) NOT NULL DEFAULT '1.2',
  PRIMARY KEY (`idscorm_package`),
  KEY `idUser` (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_scorm_package`
--

LOCK TABLES `learning_scorm_package` WRITE;
/*!40000 ALTER TABLE `learning_scorm_package` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_scorm_package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_scorm_resources`
--

DROP TABLE IF EXISTS `learning_scorm_resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_scorm_resources` (
  `idscorm_resource` int(11) NOT NULL AUTO_INCREMENT,
  `idsco` varchar(255) NOT NULL DEFAULT '',
  `idscorm_package` int(11) NOT NULL DEFAULT '0',
  `scormtype` set('sco','asset') DEFAULT NULL,
  `href` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idscorm_resource`),
  UNIQUE KEY `idsco_package_unique` (`idsco`,`idscorm_package`),
  KEY `idscorm_package` (`idscorm_package`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_scorm_resources`
--

LOCK TABLES `learning_scorm_resources` WRITE;
/*!40000 ALTER TABLE `learning_scorm_resources` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_scorm_resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_scorm_tracking`
--

DROP TABLE IF EXISTS `learning_scorm_tracking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_scorm_tracking` (
  `idscorm_tracking` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL DEFAULT '0',
  `idReference` int(11) NOT NULL DEFAULT '0',
  `idscorm_item` int(11) NOT NULL DEFAULT '0',
  `user_name` varchar(255) DEFAULT NULL,
  `lesson_location` varchar(255) DEFAULT NULL,
  `credit` varchar(24) DEFAULT NULL,
  `lesson_status` varchar(24) DEFAULT NULL,
  `entry` varchar(24) DEFAULT NULL,
  `score_raw` float DEFAULT NULL,
  `score_max` float DEFAULT NULL,
  `score_min` float DEFAULT NULL,
  `total_time` varchar(15) DEFAULT '0000:00:00.00',
  `lesson_mode` varchar(24) DEFAULT NULL,
  `exit` varchar(24) DEFAULT NULL,
  `session_time` varchar(15) DEFAULT NULL,
  `suspend_data` blob,
  `launch_data` blob,
  `comments` blob,
  `comments_from_lms` blob,
  `xmldata` longblob,
  `first_access` timestamp NULL DEFAULT NULL,
  `last_access` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idscorm_tracking`),
  UNIQUE KEY `Unique_tracking_usersco` (`idUser`,`idReference`,`idscorm_item`),
  KEY `idUser` (`idUser`),
  KEY `idscorm_resource` (`idReference`),
  KEY `idscorm_item` (`idscorm_item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_scorm_tracking`
--

LOCK TABLES `learning_scorm_tracking` WRITE;
/*!40000 ALTER TABLE `learning_scorm_tracking` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_scorm_tracking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_scorm_tracking_history`
--

DROP TABLE IF EXISTS `learning_scorm_tracking_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_scorm_tracking_history` (
  `idscorm_tracking` int(11) NOT NULL DEFAULT '0',
  `date_action` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `score_raw` float DEFAULT NULL,
  `score_max` float DEFAULT NULL,
  `session_time` varchar(15) DEFAULT NULL,
  `lesson_status` varchar(24) NOT NULL DEFAULT '',
  PRIMARY KEY (`idscorm_tracking`,`date_action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_scorm_tracking_history`
--

LOCK TABLES `learning_scorm_tracking_history` WRITE;
/*!40000 ALTER TABLE `learning_scorm_tracking_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_scorm_tracking_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_tc_activity`
--

DROP TABLE IF EXISTS `learning_tc_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_tc_activity` (
  `id_tc_activity` int(11) NOT NULL AUTO_INCREMENT,
  `id_tc_ap` int(11) NOT NULL,
  `id` varchar(2000) NOT NULL,
  `type` varchar(2000) NOT NULL,
  `name` varchar(512) NOT NULL,
  `description` text NOT NULL,
  `launch` varchar(255) NOT NULL,
  PRIMARY KEY (`id_tc_activity`),
  KEY `lms_tc_activity_lms_tc_ap` (`id_tc_ap`),
  CONSTRAINT `lms_tc_activity_lms_tc_ap` FOREIGN KEY (`id_tc_ap`) REFERENCES `learning_tc_ap` (`id_tc_ap`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_tc_activity`
--

LOCK TABLES `learning_tc_activity` WRITE;
/*!40000 ALTER TABLE `learning_tc_activity` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_tc_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_tc_ap`
--

DROP TABLE IF EXISTS `learning_tc_ap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_tc_ap` (
  `id_tc_ap` int(11) NOT NULL AUTO_INCREMENT,
  `registration` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `xapi_version` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id_tc_ap`),
  UNIQUE KEY `registration` (`registration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_tc_ap`
--

LOCK TABLES `learning_tc_ap` WRITE;
/*!40000 ALTER TABLE `learning_tc_ap` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_tc_ap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_tc_track`
--

DROP TABLE IF EXISTS `learning_tc_track`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_tc_track` (
  `idTrack` int(11) NOT NULL AUTO_INCREMENT,
  `idReference` int(11) NOT NULL,
  `idResource` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  PRIMARY KEY (`idTrack`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_tc_track`
--

LOCK TABLES `learning_tc_track` WRITE;
/*!40000 ALTER TABLE `learning_tc_track` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_tc_track` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_test`
--

DROP TABLE IF EXISTS `learning_test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_test` (
  `idTest` int(11) NOT NULL AUTO_INCREMENT,
  `author` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `point_type` tinyint(1) NOT NULL DEFAULT '0',
  `point_assignment` tinyint(1) NOT NULL DEFAULT '0',
  `show_users_avg` tinyint(1) NOT NULL DEFAULT '0',
  `point_required` double NOT NULL DEFAULT '0',
  `display_type` tinyint(1) NOT NULL DEFAULT '0',
  `order_type` tinyint(1) NOT NULL DEFAULT '0',
  `shuffle_answer` tinyint(1) NOT NULL DEFAULT '0',
  `question_random_number` int(4) NOT NULL DEFAULT '0',
  `save_keep` tinyint(1) NOT NULL DEFAULT '0',
  `mod_doanswer` tinyint(1) NOT NULL DEFAULT '1',
  `can_travel` tinyint(1) NOT NULL DEFAULT '1',
  `show_only_status` tinyint(1) NOT NULL DEFAULT '0',
  `show_score` tinyint(1) NOT NULL DEFAULT '1',
  `show_score_cat` tinyint(1) NOT NULL DEFAULT '0',
  `show_doanswer` tinyint(1) NOT NULL DEFAULT '0',
  `show_solution` tinyint(1) NOT NULL DEFAULT '0',
  `show_solution_date` date DEFAULT NULL,
  `time_dependent` tinyint(1) NOT NULL DEFAULT '0',
  `time_assigned` int(6) NOT NULL DEFAULT '0',
  `penality_test` tinyint(1) NOT NULL DEFAULT '0',
  `penality_time_test` double NOT NULL DEFAULT '0',
  `penality_quest` tinyint(1) NOT NULL DEFAULT '0',
  `penality_time_quest` double NOT NULL DEFAULT '0',
  `max_attempt` int(11) NOT NULL DEFAULT '0',
  `hide_info` tinyint(1) NOT NULL DEFAULT '0',
  `order_info` text NOT NULL,
  `use_suspension` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `suspension_num_attempts` int(10) unsigned NOT NULL DEFAULT '0',
  `suspension_num_hours` int(10) unsigned NOT NULL DEFAULT '0',
  `suspension_prerequisites` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mandatory_answer` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `score_max` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idTest`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_test`
--

LOCK TABLES `learning_test` WRITE;
/*!40000 ALTER TABLE `learning_test` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_test_feedback`
--

DROP TABLE IF EXISTS `learning_test_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_test_feedback` (
  `id_feedback` int(11) NOT NULL AUTO_INCREMENT,
  `id_test` int(11) NOT NULL,
  `from_score` double NOT NULL,
  `to_score` double NOT NULL,
  `feedback_text` text NOT NULL,
  PRIMARY KEY (`id_feedback`),
  KEY `id_test` (`id_test`),
  CONSTRAINT `learning_test_feedback_ibfk_1` FOREIGN KEY (`id_test`) REFERENCES `learning_test` (`idTest`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_test_feedback`
--

LOCK TABLES `learning_test_feedback` WRITE;
/*!40000 ALTER TABLE `learning_test_feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_test_feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_test_quest_rel`
--

DROP TABLE IF EXISTS `learning_test_quest_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_test_quest_rel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_test` int(11) NOT NULL,
  `id_question` int(11) NOT NULL,
  `sequence` int(5) NOT NULL DEFAULT '0',
  `page` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_test` (`id_test`),
  KEY `id_question` (`id_question`),
  CONSTRAINT `learning_test_quest_rel_ibfk_1` FOREIGN KEY (`id_test`) REFERENCES `learning_test` (`idTest`),
  CONSTRAINT `learning_test_quest_rel_ibfk_2` FOREIGN KEY (`id_question`) REFERENCES `learning_testquest` (`idQuest`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_test_quest_rel`
--

LOCK TABLES `learning_test_quest_rel` WRITE;
/*!40000 ALTER TABLE `learning_test_quest_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_test_quest_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_testquest`
--

DROP TABLE IF EXISTS `learning_testquest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_testquest` (
  `idQuest` int(11) NOT NULL AUTO_INCREMENT,
  `idCategory` int(11) NOT NULL DEFAULT '0',
  `type_quest` varchar(255) NOT NULL DEFAULT '',
  `title_quest` text NOT NULL,
  `difficult` int(1) NOT NULL DEFAULT '3',
  `time_assigned` int(5) NOT NULL DEFAULT '0',
  `shuffle` tinyint(1) NOT NULL DEFAULT '0',
  `is_bank` tinyint(1) NOT NULL DEFAULT '0',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idQuest`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_testquest`
--

LOCK TABLES `learning_testquest` WRITE;
/*!40000 ALTER TABLE `learning_testquest` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_testquest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_testquestanswer`
--

DROP TABLE IF EXISTS `learning_testquestanswer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_testquestanswer` (
  `idAnswer` int(11) NOT NULL AUTO_INCREMENT,
  `idQuest` int(11) NOT NULL DEFAULT '0',
  `sequence` int(11) NOT NULL DEFAULT '0',
  `is_correct` int(11) NOT NULL DEFAULT '0',
  `answer` text NOT NULL,
  `comment` text NOT NULL,
  `score_correct` double NOT NULL DEFAULT '0',
  `score_incorrect` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`idAnswer`),
  KEY `fk_id_quest_idx_learning_testquestanswer` (`idQuest`),
  CONSTRAINT `fk_id_quest_learning_testquestanswer` FOREIGN KEY (`idQuest`) REFERENCES `learning_testquest` (`idQuest`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_testquestanswer`
--

LOCK TABLES `learning_testquestanswer` WRITE;
/*!40000 ALTER TABLE `learning_testquestanswer` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_testquestanswer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_testquestanswer_associate`
--

DROP TABLE IF EXISTS `learning_testquestanswer_associate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_testquestanswer_associate` (
  `idAnswer` int(11) NOT NULL AUTO_INCREMENT,
  `idQuest` int(11) NOT NULL DEFAULT '0',
  `answer` text NOT NULL,
  PRIMARY KEY (`idAnswer`),
  KEY `idQiest` (`idQuest`),
  CONSTRAINT `learning_testquestanswer_associate_ibfk_1` FOREIGN KEY (`idQuest`) REFERENCES `learning_testquest` (`idQuest`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_testquestanswer_associate`
--

LOCK TABLES `learning_testquestanswer_associate` WRITE;
/*!40000 ALTER TABLE `learning_testquestanswer_associate` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_testquestanswer_associate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_testtrack`
--

DROP TABLE IF EXISTS `learning_testtrack`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_testtrack` (
  `idTrack` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL DEFAULT '0',
  `idReference` int(11) NOT NULL DEFAULT '0',
  `idTest` int(11) NOT NULL DEFAULT '0',
  `date_attempt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_attempt_mod` timestamp NULL DEFAULT NULL,
  `date_end_attempt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_page_seen` int(11) NOT NULL DEFAULT '0',
  `last_page_saved` int(11) NOT NULL DEFAULT '0',
  `number_of_save` int(11) NOT NULL DEFAULT '0',
  `number_of_attempt` int(11) NOT NULL DEFAULT '0',
  `score` double DEFAULT NULL,
  `bonus_score` double NOT NULL DEFAULT '0',
  `score_status` enum('valid','not_checked','not_passed','passed','not_complete','doing') NOT NULL DEFAULT 'not_complete',
  `comment` text NOT NULL,
  `attempts_for_suspension` int(10) unsigned NOT NULL DEFAULT '0',
  `suspended_until` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idTrack`),
  KEY `fk_id_user_idx_learning_testtrack` (`idUser`),
  KEY `fk_id_test_idx_learning_testtrack` (`idTest`),
  CONSTRAINT `fk_id_test_learning_testtrack` FOREIGN KEY (`idTest`) REFERENCES `learning_test` (`idTest`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_id_user_learning_testtrack` FOREIGN KEY (`idUser`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_testtrack`
--

LOCK TABLES `learning_testtrack` WRITE;
/*!40000 ALTER TABLE `learning_testtrack` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_testtrack` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_testtrack_answer`
--

DROP TABLE IF EXISTS `learning_testtrack_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_testtrack_answer` (
  `idTrack` int(11) NOT NULL DEFAULT '0',
  `idQuest` int(11) NOT NULL DEFAULT '0',
  `idAnswer` int(11) NOT NULL DEFAULT '0',
  `score_assigned` double NOT NULL DEFAULT '0',
  `more_info` longtext NOT NULL,
  `manual_assigned` tinyint(1) NOT NULL DEFAULT '0',
  `user_answer` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`idTrack`,`idQuest`,`idAnswer`),
  KEY `fk_id_track_idx_learning_testtrack_answer` (`idTrack`),
  KEY `idQuest` (`idQuest`),
  CONSTRAINT `fk_id_track_learning_testtrack_answer` FOREIGN KEY (`idTrack`) REFERENCES `learning_testtrack` (`idTrack`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `learning_testtrack_answer_ibfk_1` FOREIGN KEY (`idQuest`) REFERENCES `learning_testquest` (`idQuest`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_testtrack_answer`
--

LOCK TABLES `learning_testtrack_answer` WRITE;
/*!40000 ALTER TABLE `learning_testtrack_answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_testtrack_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_testtrack_page`
--

DROP TABLE IF EXISTS `learning_testtrack_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_testtrack_page` (
  `idTrack` int(11) NOT NULL DEFAULT '0',
  `page` int(3) NOT NULL DEFAULT '0',
  `display_from` timestamp NULL DEFAULT NULL,
  `display_to` timestamp NULL DEFAULT NULL,
  `accumulated` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idTrack`,`page`),
  KEY `fk_id_track_idx_learning_testtrack_page` (`idTrack`),
  CONSTRAINT `fk_id_track_learning_testtrack_page` FOREIGN KEY (`idTrack`) REFERENCES `learning_testtrack` (`idTrack`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_testtrack_page`
--

LOCK TABLES `learning_testtrack_page` WRITE;
/*!40000 ALTER TABLE `learning_testtrack_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_testtrack_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_testtrack_quest`
--

DROP TABLE IF EXISTS `learning_testtrack_quest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_testtrack_quest` (
  `idTrack` int(11) NOT NULL DEFAULT '0',
  `idQuest` int(11) NOT NULL DEFAULT '0',
  `page` int(11) NOT NULL DEFAULT '0',
  `quest_sequence` int(11) NOT NULL DEFAULT '0',
  `more_info` text,
  PRIMARY KEY (`idTrack`,`idQuest`),
  KEY `fk_id_track_idx_learning_testtrack_quest` (`idTrack`),
  CONSTRAINT `fk_id_track_learning_testtrack_quest` FOREIGN KEY (`idTrack`) REFERENCES `learning_testtrack` (`idTrack`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_testtrack_quest`
--

LOCK TABLES `learning_testtrack_quest` WRITE;
/*!40000 ALTER TABLE `learning_testtrack_quest` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_testtrack_quest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_testtrack_times`
--

DROP TABLE IF EXISTS `learning_testtrack_times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_testtrack_times` (
  `idTrack` int(11) NOT NULL DEFAULT '0',
  `idReference` int(11) NOT NULL DEFAULT '0',
  `idTest` int(11) NOT NULL DEFAULT '0',
  `date_attempt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `number_time` tinyint(4) NOT NULL DEFAULT '0',
  `score` double NOT NULL DEFAULT '0',
  `score_status` varchar(50) NOT NULL DEFAULT '',
  `date_begin` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `time` int(11) NOT NULL,
  PRIMARY KEY (`idTrack`,`number_time`,`idTest`),
  KEY `fk_id_track_idx_learning_testtrack_times` (`idTrack`),
  CONSTRAINT `fk_id_track_learning_testtrack_times` FOREIGN KEY (`idTrack`) REFERENCES `learning_testtrack` (`idTrack`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_testtrack_times`
--

LOCK TABLES `learning_testtrack_times` WRITE;
/*!40000 ALTER TABLE `learning_testtrack_times` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_testtrack_times` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_tracksession`
--

DROP TABLE IF EXISTS `learning_tracksession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_tracksession` (
  `idEnter` int(11) NOT NULL AUTO_INCREMENT,
  `idCourse` int(11) NOT NULL DEFAULT '0',
  `idUser` int(11) NOT NULL DEFAULT '0',
  `session_id` varchar(255) NOT NULL DEFAULT '',
  `enterTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `numOp` int(5) NOT NULL DEFAULT '0',
  `lastFunction` varchar(50) NOT NULL DEFAULT '',
  `lastOp` varchar(5) NOT NULL DEFAULT '',
  `lastTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip_address` varchar(40) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idEnter`),
  KEY `idCourse` (`idCourse`,`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_tracksession`
--

LOCK TABLES `learning_tracksession` WRITE;
/*!40000 ALTER TABLE `learning_tracksession` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_tracksession` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_userlimit_login_log`
--

DROP TABLE IF EXISTS `learning_userlimit_login_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_userlimit_login_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userIdst` int(11) NOT NULL,
  `first_period_login` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userIdst` (`userIdst`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_userlimit_login_log`
--

LOCK TABLES `learning_userlimit_login_log` WRITE;
/*!40000 ALTER TABLE `learning_userlimit_login_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_userlimit_login_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_video`
--

DROP TABLE IF EXISTS `learning_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_video` (
  `id_video` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `video_formats` text NOT NULL,
  `seekable` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_video`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_video`
--

LOCK TABLES `learning_video` WRITE;
/*!40000 ALTER TABLE `learning_video` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_video_subtitles`
--

DROP TABLE IF EXISTS `learning_video_subtitles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_video_subtitles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_video` int(11) NOT NULL,
  `lang_code` varchar(50) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `fallback` int(11) NOT NULL DEFAULT '0',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Subtitles_Unique_Per_Lang` (`id_video`,`lang_code`),
  CONSTRAINT `learning_video_subtitles_ibfk_1` FOREIGN KEY (`id_video`) REFERENCES `learning_video` (`id_video`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_video_subtitles`
--

LOCK TABLES `learning_video_subtitles` WRITE;
/*!40000 ALTER TABLE `learning_video_subtitles` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_video_subtitles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_video_track`
--

DROP TABLE IF EXISTS `learning_video_track`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_video_track` (
  `idTrack` int(11) NOT NULL AUTO_INCREMENT,
  `idReference` int(11) NOT NULL,
  `idResource` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `total_time` int(11) NOT NULL,
  `accesses` int(4) NOT NULL,
  `bookmark` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`idTrack`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_video_track`
--

LOCK TABLES `learning_video_track` WRITE;
/*!40000 ALTER TABLE `learning_video_track` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_video_track` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_webpage`
--

DROP TABLE IF EXISTS `learning_webpage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_webpage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sequence` int(3) NOT NULL,
  `publish` tinyint(1) NOT NULL,
  `in_home` tinyint(1) NOT NULL,
  `visible_nodes` varchar(10) NOT NULL DEFAULT 'all' COMMENT 'all | selection',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_webpage`
--

LOCK TABLES `learning_webpage` WRITE;
/*!40000 ALTER TABLE `learning_webpage` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_webpage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_webpage_domainbranding`
--

DROP TABLE IF EXISTS `learning_webpage_domainbranding`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_webpage_domainbranding` (
  `id_branding` int(11) NOT NULL,
  `id_page` int(11) NOT NULL,
  PRIMARY KEY (`id_branding`,`id_page`),
  KEY `fk_learning_webpage_domainbranding_webpage` (`id_page`),
  CONSTRAINT `fk_learning_webpage_domainbranding_branding` FOREIGN KEY (`id_branding`) REFERENCES `core_org_chart_branding` (`id_org`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_learning_webpage_domainbranding_webpage` FOREIGN KEY (`id_page`) REFERENCES `learning_webpage` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_webpage_domainbranding`
--

LOCK TABLES `learning_webpage_domainbranding` WRITE;
/*!40000 ALTER TABLE `learning_webpage_domainbranding` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_webpage_domainbranding` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learning_webpage_translation`
--

DROP TABLE IF EXISTS `learning_webpage_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learning_webpage_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `lang_code` varchar(30) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`,`page_id`,`lang_code`),
  KEY `page_id` (`page_id`),
  CONSTRAINT `learning_webpage_translation_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `learning_webpage` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learning_webpage_translation`
--

LOCK TABLES `learning_webpage_translation` WRITE;
/*!40000 ALTER TABLE `learning_webpage_translation` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_webpage_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lrs_activity`
--

DROP TABLE IF EXISTS `lrs_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lrs_activity` (
  `id_activity` int(11) NOT NULL AUTO_INCREMENT,
  `json_data` longtext NOT NULL,
  `hash_data` varchar(255) NOT NULL,
  `activity_id` varchar(2000) NOT NULL,
  `def_name_json` text,
  `def_description_json` text,
  `def_activity_type` varchar(2000) DEFAULT NULL,
  `def_interaction_type` varchar(200) DEFAULT NULL,
  `def_correct_pattern_json` text,
  `def_interaction_components` text,
  `def_extensions_json` varchar(255) DEFAULT NULL,
  `def_moreInfo` varchar(2000) DEFAULT NULL COMMENT '(since v1) IRL to a human-readable information about the Actiivty',
  `canonical_version` int(11) DEFAULT '0' COMMENT '(since v1)',
  `xapi_version_code` int(11) DEFAULT NULL COMMENT '(since v1) Track client version internal coding',
  PRIMARY KEY (`id_activity`),
  KEY `activity_id` (`activity_id`(255)),
  KEY `xapi_version_code` (`xapi_version_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lrs_activity`
--

LOCK TABLES `lrs_activity` WRITE;
/*!40000 ALTER TABLE `lrs_activity` DISABLE KEYS */;
/*!40000 ALTER TABLE `lrs_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lrs_agent`
--

DROP TABLE IF EXISTS `lrs_agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lrs_agent` (
  `id_agent` int(11) NOT NULL AUTO_INCREMENT,
  `json_data` longtext NOT NULL,
  `hash_data` varchar(255) NOT NULL,
  `objectType` varchar(64) NOT NULL,
  `name_json` text,
  `mbox_json` text,
  `mbox_sha1sum_json` text,
  `openid_json` text,
  `member` text,
  `givenName_json` text,
  `familyName_json` text,
  `firstName_json` text,
  `lastName_json` text,
  `account_json` text,
  `canonical_version` int(11) DEFAULT NULL COMMENT '(since v1)',
  `account_homePage` varchar(2000) DEFAULT NULL COMMENT '(since v1)',
  `account_name` varchar(255) DEFAULT NULL COMMENT '(since v1)',
  `xapi_version_code` int(11) DEFAULT NULL COMMENT '(since v1) Track client version internal coding',
  PRIMARY KEY (`id_agent`),
  KEY `xapi_version_code` (`xapi_version_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lrs_agent`
--

LOCK TABLES `lrs_agent` WRITE;
/*!40000 ALTER TABLE `lrs_agent` DISABLE KEYS */;
/*!40000 ALTER TABLE `lrs_agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lrs_context`
--

DROP TABLE IF EXISTS `lrs_context`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lrs_context` (
  `id_context` int(11) NOT NULL AUTO_INCREMENT,
  `json_data` longtext NOT NULL,
  `hash_data` varchar(255) NOT NULL,
  `id_parent` int(11) DEFAULT NULL,
  `id_grouping` int(11) DEFAULT NULL,
  `id_other` int(11) DEFAULT NULL,
  `registration_id` varchar(255) DEFAULT NULL,
  `id_instructor` int(11) DEFAULT NULL,
  `id_team` int(11) DEFAULT NULL,
  `revision` varchar(64) DEFAULT NULL,
  `platform` varchar(200) DEFAULT NULL,
  `language` varchar(8) DEFAULT NULL,
  `id_statement` int(11) DEFAULT NULL,
  `extensions_json` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_context`),
  KEY `id_parent` (`id_parent`),
  KEY `id_grouping` (`id_grouping`),
  KEY `id_other` (`id_other`),
  KEY `registration_id` (`registration_id`),
  KEY `id_instructor` (`id_instructor`),
  KEY `id_team` (`id_team`),
  KEY `id_statement` (`id_statement`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lrs_context`
--

LOCK TABLES `lrs_context` WRITE;
/*!40000 ALTER TABLE `lrs_context` DISABLE KEYS */;
/*!40000 ALTER TABLE `lrs_context` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lrs_context_activities`
--

DROP TABLE IF EXISTS `lrs_context_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lrs_context_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_context` int(11) NOT NULL,
  `id_activity` int(11) NOT NULL,
  `context_type` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_context` (`id_context`),
  KEY `id_activity` (`id_activity`),
  CONSTRAINT `lrs_context_activities_ibfk_1` FOREIGN KEY (`id_context`) REFERENCES `lrs_context` (`id_context`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lrs_context_activities_ibfk_2` FOREIGN KEY (`id_activity`) REFERENCES `lrs_activity` (`id_activity`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lrs_context_activities`
--

LOCK TABLES `lrs_context_activities` WRITE;
/*!40000 ALTER TABLE `lrs_context_activities` DISABLE KEYS */;
/*!40000 ALTER TABLE `lrs_context_activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lrs_profile`
--

DROP TABLE IF EXISTS `lrs_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lrs_profile` (
  `id_profile` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` varchar(255) NOT NULL,
  `id_activity` int(11) DEFAULT NULL,
  `id_actor` int(11) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `contents` text,
  `etag` varchar(128) DEFAULT NULL COMMENT '(since v1)',
  `content_type` varchar(255) DEFAULT NULL COMMENT '(since v1)',
  `xapi_version_code` int(11) DEFAULT NULL COMMENT '(since v1) Track client version internal coding',
  PRIMARY KEY (`id_profile`),
  KEY `xapi_version_code` (`xapi_version_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lrs_profile`
--

LOCK TABLES `lrs_profile` WRITE;
/*!40000 ALTER TABLE `lrs_profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `lrs_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lrs_registration_map`
--

DROP TABLE IF EXISTS `lrs_registration_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lrs_registration_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ap_registration` varchar(128) NOT NULL COMMENT 'Registration ID set in AP context',
  `lms_registration` varchar(128) NOT NULL COMMENT 'Corresponding LMS based registration ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lrs_registration_map`
--

LOCK TABLES `lrs_registration_map` WRITE;
/*!40000 ALTER TABLE `lrs_registration_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `lrs_registration_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lrs_result`
--

DROP TABLE IF EXISTS `lrs_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lrs_result` (
  `id_result` int(11) NOT NULL AUTO_INCREMENT,
  `json_data` longtext NOT NULL,
  `hash_data` varchar(255) NOT NULL,
  `simple_string` varchar(255) DEFAULT NULL,
  `success` int(11) DEFAULT NULL,
  `completion` int(11) DEFAULT NULL,
  `duration` varchar(64) DEFAULT NULL,
  `response_json` text,
  `score_scaled` double(10,4) DEFAULT NULL,
  `score_raw` double(10,4) DEFAULT NULL,
  `score_min` double(10,4) DEFAULT NULL,
  `score_max` double(10,4) DEFAULT NULL,
  `extensions_json` text COMMENT 'JSON result extensions',
  PRIMARY KEY (`id_result`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lrs_result`
--

LOCK TABLES `lrs_result` WRITE;
/*!40000 ALTER TABLE `lrs_result` DISABLE KEYS */;
/*!40000 ALTER TABLE `lrs_result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lrs_state`
--

DROP TABLE IF EXISTS `lrs_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lrs_state` (
  `id_state` int(11) NOT NULL AUTO_INCREMENT,
  `json_data` text NOT NULL,
  `hash_data` varchar(255) NOT NULL,
  `state_id` varchar(200) NOT NULL,
  `id_agent` int(11) DEFAULT NULL,
  `id_activity` int(11) DEFAULT NULL,
  `state` text NOT NULL,
  `registration_id` varchar(200) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `etag` varchar(128) DEFAULT NULL COMMENT '(since v1)',
  `content_type` varchar(255) DEFAULT NULL COMMENT '(since v1)',
  PRIMARY KEY (`id_state`),
  KEY `id_agent` (`id_agent`),
  KEY `id_activity` (`id_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lrs_state`
--

LOCK TABLES `lrs_state` WRITE;
/*!40000 ALTER TABLE `lrs_state` DISABLE KEYS */;
/*!40000 ALTER TABLE `lrs_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lrs_statement`
--

DROP TABLE IF EXISTS `lrs_statement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lrs_statement` (
  `id_statement` int(11) NOT NULL AUTO_INCREMENT,
  `json_data` longtext NOT NULL,
  `hash_data` varchar(255) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `id_actor` int(11) DEFAULT NULL,
  `id_authority` int(11) NOT NULL,
  `verb` varchar(2000) NOT NULL,
  `id_object` int(11) NOT NULL,
  `object_type` varchar(64) DEFAULT NULL,
  `id_result` int(11) DEFAULT NULL,
  `id_context` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stored` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `score` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  `authority_consumer_key` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `voided` tinyint(4) DEFAULT NULL,
  `version` varchar(16) DEFAULT NULL COMMENT '(since v1) Tracks xAPI version of the reporting client',
  `id_verb` int(11) DEFAULT NULL COMMENT '(since v1) Points to Verb model',
  `xapi_version_code` int(11) DEFAULT NULL COMMENT '(since v1) Track client version internal coding',
  PRIMARY KEY (`id_statement`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `id_actor` (`id_actor`),
  KEY `verb` (`verb`(255)),
  KEY `id_object` (`id_object`),
  KEY `id_result` (`id_result`),
  KEY `id_context` (`id_context`),
  KEY `id_user` (`id_user`),
  KEY `id_authority` (`id_authority`),
  KEY `id_verb` (`id_verb`),
  CONSTRAINT `lrs_statement_ibfk_1` FOREIGN KEY (`id_verb`) REFERENCES `lrs_verb` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lrs_statement`
--

LOCK TABLES `lrs_statement` WRITE;
/*!40000 ALTER TABLE `lrs_statement` DISABLE KEYS */;
/*!40000 ALTER TABLE `lrs_statement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lrs_statement_attachment`
--

DROP TABLE IF EXISTS `lrs_statement_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lrs_statement_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_statement` int(11) NOT NULL,
  `usageType` varchar(2000) NOT NULL COMMENT 'IRI',
  `contentType` varchar(256) NOT NULL COMMENT 'MIME type',
  `length` int(11) NOT NULL COMMENT 'The length of the attachment data in octets',
  `sha2` varchar(256) NOT NULL COMMENT 'The SHA-2 (SHA-256, SHA-384, SHA-512) hash of the attachment data',
  `fileUrl` varchar(2000) DEFAULT NULL COMMENT 'Where attachment can be retrieved from or its original location',
  `description` text COMMENT 'JSON, language map',
  `display` text NOT NULL COMMENT 'JSON, language map',
  `payloadContent` longblob COMMENT 'Content of the attachment, as an inline blob',
  `payloadFilePath` varchar(2048) NOT NULL COMMENT 'Content of the attachment, file path relative to storage collection',
  PRIMARY KEY (`id`),
  KEY `id_statement` (`id_statement`),
  CONSTRAINT `lrs_statement_attachment_ibfk_1` FOREIGN KEY (`id_statement`) REFERENCES `lrs_statement` (`id_statement`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lrs_statement_attachment`
--

LOCK TABLES `lrs_statement_attachment` WRITE;
/*!40000 ALTER TABLE `lrs_statement_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `lrs_statement_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lrs_verb`
--

DROP TABLE IF EXISTS `lrs_verb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lrs_verb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `verb_id` varchar(2000) NOT NULL COMMENT 'IRI',
  `display` text COMMENT 'JSON, Language map',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lrs_verb`
--

LOCK TABLES `lrs_verb` WRITE;
/*!40000 ALTER TABLE `lrs_verb` DISABLE KEYS */;
/*!40000 ALTER TABLE `lrs_verb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lt_classroom`
--

DROP TABLE IF EXISTS `lt_classroom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lt_classroom` (
  `id_classroom` int(11) NOT NULL AUTO_INCREMENT,
  `id_location` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `details` text,
  `seats` int(11) NOT NULL DEFAULT '0',
  `equipment` text,
  PRIMARY KEY (`id_classroom`),
  KEY `fk_learning_classroom_learning_location1_idx` (`id_location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lt_classroom`
--

LOCK TABLES `lt_classroom` WRITE;
/*!40000 ALTER TABLE `lt_classroom` DISABLE KEYS */;
/*!40000 ALTER TABLE `lt_classroom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lt_course_session`
--

DROP TABLE IF EXISTS `lt_course_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lt_course_session` (
  `id_session` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `max_enroll` int(11) NOT NULL,
  `min_enroll` int(11) DEFAULT '0',
  `score_base` int(11) DEFAULT '100',
  `other_info` text,
  `notify_enrolled` tinyint(1) DEFAULT '0',
  `date_begin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_subscription_date` timestamp NULL DEFAULT NULL,
  `total_hours` float NOT NULL DEFAULT '0',
  `evaluation_type` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_session`),
  KEY `fk_learning_course_session_learning_course1_idx` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lt_course_session`
--

LOCK TABLES `lt_course_session` WRITE;
/*!40000 ALTER TABLE `lt_course_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `lt_course_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lt_course_session_date`
--

DROP TABLE IF EXISTS `lt_course_session_date`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lt_course_session_date` (
  `id_session` int(11) NOT NULL,
  `day` date NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `time_begin` time NOT NULL,
  `time_end` time NOT NULL,
  `break_begin` time DEFAULT NULL,
  `break_end` time DEFAULT NULL,
  `timezone` varchar(255) NOT NULL,
  `id_location` int(11) NOT NULL,
  `id_classroom` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_session`,`day`),
  KEY `fk_learning_course_session_date_learning_location1_idx` (`id_location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lt_course_session_date`
--

LOCK TABLES `lt_course_session_date` WRITE;
/*!40000 ALTER TABLE `lt_course_session_date` DISABLE KEYS */;
/*!40000 ALTER TABLE `lt_course_session_date` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lt_course_session_date_attendance`
--

DROP TABLE IF EXISTS `lt_course_session_date_attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lt_course_session_date_attendance` (
  `id_session` int(11) NOT NULL,
  `day` date NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_session`,`day`,`id_user`),
  KEY `fk_learning_course_session_date_attendance_core_user1_idx` (`id_user`),
  KEY `fk_learning_course_session_date_attendance_learning_course__idx` (`id_session`,`day`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lt_course_session_date_attendance`
--

LOCK TABLES `lt_course_session_date_attendance` WRITE;
/*!40000 ALTER TABLE `lt_course_session_date_attendance` DISABLE KEYS */;
/*!40000 ALTER TABLE `lt_course_session_date_attendance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lt_courseuser_session`
--

DROP TABLE IF EXISTS `lt_courseuser_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lt_courseuser_session` (
  `id_session` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `evaluation_text` text,
  `evaluation_file` varchar(255) DEFAULT NULL,
  `evaluation_score` int(11) DEFAULT NULL,
  `evaluation_date` date DEFAULT NULL,
  `evaluation_status` tinyint(1) DEFAULT NULL,
  `evaluator_id` int(11) DEFAULT NULL,
  `attendance_hours` int(11) DEFAULT '0',
  `waiting` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_subscribed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_completed` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_session`,`id_user`),
  KEY `fk_learning_courseuser_session_core_user1_idx` (`id_user`),
  KEY `fk_learning_courseuser_session_core_user2_idx` (`evaluator_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lt_courseuser_session`
--

LOCK TABLES `lt_courseuser_session` WRITE;
/*!40000 ALTER TABLE `lt_courseuser_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `lt_courseuser_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lt_location`
--

DROP TABLE IF EXISTS `lt_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lt_location` (
  `id_location` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `id_country` int(11) NOT NULL,
  `telephone` varchar(45) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `reaching_info` text,
  `accomodations` text,
  `other_info` text,
  PRIMARY KEY (`id_location`),
  KEY `fk_learning_location_core_country1_idx` (`id_country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lt_location`
--

LOCK TABLES `lt_location` WRITE;
/*!40000 ALTER TABLE `lt_location` DISABLE KEYS */;
/*!40000 ALTER TABLE `lt_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lt_location_photo`
--

DROP TABLE IF EXISTS `lt_location_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lt_location_photo` (
  `id_photo` int(11) NOT NULL AUTO_INCREMENT,
  `file_path` varchar(255) NOT NULL,
  `order` int(11) DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  `id_location` int(11) NOT NULL,
  PRIMARY KEY (`id_photo`),
  KEY `fk_learning_location_photo_learning_location1_idx` (`id_location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lt_location_photo`
--

LOCK TABLES `lt_location_photo` WRITE;
/*!40000 ALTER TABLE `lt_location_photo` DISABLE KEYS */;
/*!40000 ALTER TABLE `lt_location_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mobile_pushnotification`
--

DROP TABLE IF EXISTS `mobile_pushnotification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_pushnotification` (
  `device_token` varchar(255) NOT NULL,
  `os` varchar(255) NOT NULL,
  `id_user` int(11) NOT NULL,
  `registration_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`device_token`,`os`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mobile_pushnotification`
--

LOCK TABLES `mobile_pushnotification` WRITE;
/*!40000 ALTER TABLE `mobile_pushnotification` DISABLE KEYS */;
/*!40000 ALTER TABLE `mobile_pushnotification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `access_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`access_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_authorization_codes`
--

DROP TABLE IF EXISTS `oauth_authorization_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_authorization_codes` (
  `authorization_code` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `redirect_uri` varchar(2000) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`authorization_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_authorization_codes`
--

LOCK TABLES `oauth_authorization_codes` WRITE;
/*!40000 ALTER TABLE `oauth_authorization_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_authorization_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `client_id` varchar(80) NOT NULL,
  `client_secret` varchar(80) NOT NULL,
  `redirect_uri` varchar(2000) DEFAULT NULL,
  `grant_types` varchar(80) DEFAULT NULL,
  `scope` varchar(100) DEFAULT NULL,
  `user_id` varchar(80) DEFAULT NULL,
  `app_name` varchar(255) NOT NULL,
  `app_description` tinytext,
  `app_icon` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `hidden` int(11) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_consumer_registry`
--

DROP TABLE IF EXISTS `oauth_consumer_registry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_consumer_registry` (
  `ocr_id` int(11) NOT NULL AUTO_INCREMENT,
  `ocr_usa_id_ref` int(11) DEFAULT NULL,
  `ocr_consumer_key` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ocr_consumer_secret` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ocr_signature_methods` varchar(255) NOT NULL DEFAULT 'HMAC-SHA1,PLAINTEXT',
  `ocr_server_uri` varchar(255) NOT NULL,
  `ocr_server_uri_host` varchar(128) NOT NULL,
  `ocr_server_uri_path` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ocr_request_token_uri` varchar(255) NOT NULL,
  `ocr_authorize_uri` varchar(255) NOT NULL,
  `ocr_access_token_uri` varchar(255) NOT NULL,
  `ocr_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ocr_id`),
  UNIQUE KEY `ocr_consumer_key` (`ocr_consumer_key`,`ocr_usa_id_ref`,`ocr_server_uri`),
  KEY `ocr_server_uri` (`ocr_server_uri`),
  KEY `ocr_server_uri_host` (`ocr_server_uri_host`,`ocr_server_uri_path`),
  KEY `ocr_usa_id_ref` (`ocr_usa_id_ref`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_consumer_registry`
--

LOCK TABLES `oauth_consumer_registry` WRITE;
/*!40000 ALTER TABLE `oauth_consumer_registry` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_consumer_registry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_consumer_token`
--

DROP TABLE IF EXISTS `oauth_consumer_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_consumer_token` (
  `oct_id` int(11) NOT NULL AUTO_INCREMENT,
  `oct_ocr_id_ref` int(11) NOT NULL,
  `oct_usa_id_ref` int(11) NOT NULL,
  `oct_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `oct_token` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `oct_token_secret` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `oct_token_type` enum('request','authorized','access') DEFAULT NULL,
  `oct_token_ttl` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `oct_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`oct_id`),
  UNIQUE KEY `oct_ocr_id_ref` (`oct_ocr_id_ref`,`oct_token`),
  UNIQUE KEY `oct_usa_id_ref` (`oct_usa_id_ref`,`oct_ocr_id_ref`,`oct_token_type`,`oct_name`),
  KEY `oct_token_ttl` (`oct_token_ttl`),
  CONSTRAINT `oauth_consumer_token_ibfk_1` FOREIGN KEY (`oct_ocr_id_ref`) REFERENCES `oauth_consumer_registry` (`ocr_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_consumer_token`
--

LOCK TABLES `oauth_consumer_token` WRITE;
/*!40000 ALTER TABLE `oauth_consumer_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_consumer_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_log`
--

DROP TABLE IF EXISTS `oauth_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_log` (
  `olg_id` int(11) NOT NULL AUTO_INCREMENT,
  `olg_osr_consumer_key` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `olg_ost_token` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `olg_ocr_consumer_key` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `olg_oct_token` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `olg_usa_id_ref` int(11) DEFAULT NULL,
  `olg_received` text NOT NULL,
  `olg_sent` text NOT NULL,
  `olg_base_string` text NOT NULL,
  `olg_notes` text NOT NULL,
  `olg_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `olg_remote_ip` bigint(20) NOT NULL,
  PRIMARY KEY (`olg_id`),
  KEY `olg_osr_consumer_key` (`olg_osr_consumer_key`,`olg_id`),
  KEY `olg_ost_token` (`olg_ost_token`,`olg_id`),
  KEY `olg_ocr_consumer_key` (`olg_ocr_consumer_key`,`olg_id`),
  KEY `olg_oct_token` (`olg_oct_token`,`olg_id`),
  KEY `olg_usa_id_ref` (`olg_usa_id_ref`,`olg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_log`
--

LOCK TABLES `oauth_log` WRITE;
/*!40000 ALTER TABLE `oauth_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `refresh_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`refresh_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_scopes`
--

DROP TABLE IF EXISTS `oauth_scopes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_scopes` (
  `scope` text,
  `is_default` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_scopes`
--

LOCK TABLES `oauth_scopes` WRITE;
/*!40000 ALTER TABLE `oauth_scopes` DISABLE KEYS */;
INSERT INTO `oauth_scopes` VALUES ('api',1);
/*!40000 ALTER TABLE `oauth_scopes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_server_nonce`
--

DROP TABLE IF EXISTS `oauth_server_nonce`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_server_nonce` (
  `osn_id` int(11) NOT NULL AUTO_INCREMENT,
  `osn_consumer_key` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `osn_token` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `osn_timestamp` bigint(20) NOT NULL,
  `osn_nonce` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`osn_id`),
  UNIQUE KEY `osn_consumer_key` (`osn_consumer_key`,`osn_token`,`osn_timestamp`,`osn_nonce`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_server_nonce`
--

LOCK TABLES `oauth_server_nonce` WRITE;
/*!40000 ALTER TABLE `oauth_server_nonce` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_server_nonce` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_server_registry`
--

DROP TABLE IF EXISTS `oauth_server_registry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_server_registry` (
  `osr_id` int(11) NOT NULL AUTO_INCREMENT,
  `osr_usa_id_ref` int(11) DEFAULT NULL,
  `osr_consumer_key` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `osr_consumer_secret` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `osr_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `osr_status` varchar(16) NOT NULL,
  `osr_requester_name` varchar(64) NOT NULL,
  `osr_requester_email` varchar(64) NOT NULL,
  `osr_callback_uri` varchar(255) NOT NULL,
  `osr_application_uri` varchar(255) NOT NULL,
  `osr_application_title` varchar(80) NOT NULL,
  `osr_application_descr` text NOT NULL,
  `osr_application_notes` text NOT NULL,
  `osr_application_type` varchar(20) NOT NULL,
  `osr_application_commercial` tinyint(1) NOT NULL DEFAULT '0',
  `osr_issue_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `osr_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`osr_id`),
  UNIQUE KEY `osr_consumer_key` (`osr_consumer_key`),
  KEY `osr_usa_id_ref` (`osr_usa_id_ref`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_server_registry`
--

LOCK TABLES `oauth_server_registry` WRITE;
/*!40000 ALTER TABLE `oauth_server_registry` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_server_registry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_server_token`
--

DROP TABLE IF EXISTS `oauth_server_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_server_token` (
  `ost_id` int(11) NOT NULL AUTO_INCREMENT,
  `ost_osr_id_ref` int(11) NOT NULL,
  `ost_usa_id_ref` int(11) NOT NULL,
  `ost_token` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ost_token_secret` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ost_token_type` enum('request','access') DEFAULT NULL,
  `ost_scope` text NOT NULL,
  `ost_authorized` tinyint(1) NOT NULL DEFAULT '0',
  `ost_referrer_host` varchar(128) NOT NULL DEFAULT '',
  `ost_token_ttl` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ost_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ost_verifier` char(10) DEFAULT NULL,
  `ost_callback_url` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`ost_id`),
  UNIQUE KEY `ost_token` (`ost_token`),
  KEY `ost_osr_id_ref` (`ost_osr_id_ref`),
  KEY `ost_token_ttl` (`ost_token_ttl`),
  CONSTRAINT `oauth_server_token_ibfk_1` FOREIGN KEY (`ost_osr_id_ref`) REFERENCES `oauth_server_registry` (`osr_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_server_token`
--

LOCK TABLES `oauth_server_token` WRITE;
/*!40000 ALTER TABLE `oauth_server_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_server_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player_baseblock`
--

DROP TABLE IF EXISTS `player_baseblock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player_baseblock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) DEFAULT NULL,
  `course_id` int(11) NOT NULL,
  `content_type` varchar(64) NOT NULL,
  `columns_span` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `params` text,
  PRIMARY KEY (`id`),
  KEY `idCourse` (`course_id`),
  CONSTRAINT `player_baseblock_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `learning_course` (`idCourse`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_baseblock`
--

LOCK TABLES `player_baseblock` WRITE;
/*!40000 ALTER TABLE `player_baseblock` DISABLE KEYS */;
/*!40000 ALTER TABLE `player_baseblock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `simplesaml_account`
--

DROP TABLE IF EXISTS `simplesaml_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `simplesaml_account` (
  `id_account` int(11) NOT NULL AUTO_INCREMENT,
  `id_multidomain` int(11) DEFAULT NULL,
  `federation_metadata` longtext NOT NULL,
  `idp` varchar(255) NOT NULL,
  `idp_metadata` longtext NOT NULL,
  `logout` tinyint(1) NOT NULL DEFAULT '0',
  `username_attribute` varchar(255) NOT NULL,
  `use_certificate` tinyint(1) NOT NULL DEFAULT '0',
  `privatekey_file` varchar(255) DEFAULT NULL,
  `certificate_file` varchar(255) DEFAULT NULL,
  `user_provisioning` tinyint(1) DEFAULT NULL,
  `user_provisioning_update_if_exists` tinyint(1) DEFAULT NULL,
  `saml_statements` text,
  PRIMARY KEY (`id_account`),
  KEY `simplesaml_id_multidomain` (`id_multidomain`),
  CONSTRAINT `simplesaml_account_ibfk_1` FOREIGN KEY (`id_multidomain`) REFERENCES `core_multidomain` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `simplesaml_account`
--

LOCK TABLES `simplesaml_account` WRITE;
/*!40000 ALTER TABLE `simplesaml_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `simplesaml_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_migration`
--

DROP TABLE IF EXISTS `tbl_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_migration`
--

LOCK TABLES `tbl_migration` WRITE;
/*!40000 ALTER TABLE `tbl_migration` DISABLE KEYS */;
INSERT INTO `tbl_migration` VALUES ('m000000_000000_base',1406294849),('m140105_182540_example_migration',1406294849),('m140506_101058_ALTER_mobile_pushnotification_ADD_registration_id',1406294849),('m140609_095413_change_core_user_billing_relation',1406294849),('m140612_143326_remove_coursepath_moderate_role',1406294849),('m140616_110650_ALTER_increase_tincan_activity_id_length',1406294849),('m140616_182629_fill_authoring_status_table',1406294849),('m140704_130758_ALTER_MULTIPLE_COLUMN_pass',1406294849),('m140708_082501_myblog',1406294849),('m140630_130236_upgrade_ecommerce_tables',1406315114),('m140630_130236_upgrade_webpages_tables',1406315115),('m140721_143407_core_version_update',1406315117),('m140723_094921_transcripts_permission',1406315117),('m140723_141010_ALTER_ecommerce_transaction_ADD_abandoned',1406315118),('m140723_142108_ALTER_core_org_chart_branding_ADD_catalog_external',1406315118),('m140814_105202_core_org_chart_tree_null_code',1410951323),('m140818_170458_INSERT_ecommerce_paypal_enabled_on',1410951323),('m140828_173314_add_original_filename_to_transcripts',1410951323),('m140910_115929_add_enable_self_unenrollment',1410951323),('m140911_173245_ALTER_lt_course_session_ADD_evaluation_type',1410951323),('m140926_093414_remove_use_advanced_form_setting',1415349632),('m141021_160000_upgrade_webex_table',1415349632),('m141117_121911_assign_users_to_root_branch_if_not_assigned',1416491636),('m141107_145922_CREATE_PU_ROLES_SESSION_MANAGEMENT',1417700306),('m141110_135326_ALTER_lt_course_session_ADD_COLUMN_created_by',1417700306),('m141209_102517_remove_learning_userlimit_login_log_foreign_key',1418316954),('m140725_130000_create_table_learning_course_rating',1418654990),('m140821_154800_INSERT_additional_fields_visibility_setting',1418654990),('m141010_122700_upgrade_learning_pollquest',1418654990),('m141015_141317_UPDATE_learning_course_ADD_COLUMN_player_layout',1418654990),('m141028_103528_ALTER_TABLE_learning_organization_ADD_COLUMN_short_description',1418654991),('m141103_115223_ADD_job_tables',1418654991),('m141109_214650_ALTER_report_table_add_job_field',1418654991),('m141119_133313_ALTER_modify_job_log_table',1418654991),('m141213_182156_MIGRATE_notifications_related',1418654992),('m141218_085945_add_create_pu_permission_sessions',1419237509),('m141223_110840_core_version_update_to_65',1428600385),('m150116_134208_course_social_settings',1428600387),('m150119_181818_adjust_modules',1428600387),('m150223_152256_alter_lt_course_session',1428600389),('m150224_120322_org_chart_indexes',1428600391),('m150225_225628_remove_smartphone_course',1428600391),('m150309_101251_alter_learning_testrack_quest',1428600393),('m150324_103703_job_time_zone',1428600396),('m150326_100605_ALTER_TABLE_lt_course_session_ADD_COLUMN_last_subscription_date',1428600397),('m141124_144923_ALTER_TABLE_learning_coursepath_ADD_COLUMN_img',1435344104),('m141125_142410_ALTER_TABLE_learning_coursepath_ADD_MULTIPLE_COLUMNS',1435344105),('m141203_155841_ALTER_TABLE_ecommerce_transaction_info_ADD_MULTIPLE_COLUMNS',1435344106),('m141204_103634_DROP_FK_BETWEEN_ecommerce_transaction_info_AND_learning_course',1435344106),('m141204_141353_ALTER_TABLE_ecommerce_transaction_info_ADD_COLUMN_item_data_json',1435344107),('m141210_112020_ALTER_TABLE_core_user_pu_course_ADD_COLUMN_total_seats',1435344107),('m141210_151511_ALTER_TABLE_core_user_pu_course_ADD_COLUMN_available_seats',1435344107),('m141212_105957_ALTER_TABLE_ecommerce_transaction_MODIFY_COLUMN_TYPE_item_type',1435344107),('m141215_120000_ALTER_learning_coursepath_courses',1435344108),('m141217_171303_ALTER_TABLE_learning_coursepath_ADD_COLUMN_visible_in_catalog',1435344108),('m141230_120000_ALTER_learning_deliverable',1435344108),('m150105_105810_create_tables_audit_trail',1435344110),('m150108_153033_ALTER_TABLE_learning_deliverable_object_ADD_COLUMN_user_reply',1435344110),('m150111_160531_core_version_update_to_66',1435344110),('m150111_193558_create_my_dashboard_tables',1435344112),('m150204_084132_webinars',1435344114),('m150208_163516_webinar_convert_settings_in_accounts',1435344114),('m150210_141315_CREATE_PU_ROLES_WEBINAR_MANAGEMENT',1435344114),('m150212_165356_ALTER_TABLE_learning_coursepath_ADD_COLUMNS_credits__days_valid',1435344115),('m150213_141857_ALTER_TABLE_learning_coursepath_user_ADD_COLUMNS_days_validity',1435344115),('m150225_084757_testquest_changes',1435344117),('m150304_160828_ALTER_TABLE_core_newsletter_COLUMN_send_type_FROM_ENUM_TO_VARCHAR',1435344117),('m150305_164856_ALTER_TABLE_core_notification_ADD_COLUMN_notify_type',1435344117),('m150305_200438_CREATE_multidomain_tables',1435344118),('m150311_035524_test_show_solution_date',1435344118),('m150311_120654_test_show_average_score',1435344119),('m150312_131839_test_score_max_calculation',1435344119),('m150318_101343_ALTER_TABLE_learning_label_ADD_COLUMN_icon',1435344119),('m150402_161930_ALTER_TABLE_core_newsletter_sendto_ADD_COLUMN_processed',1435344120),('m150409_133916_CREATE_TABLES_INBOX_RELATED',1435344120),('m150414_155745_activate_scheduler_app',1435344120),('m150416_075206_core_country_fix',1435344120),('m150427_110133_course_catch_ups',1435344121),('m150427_131119_add_catchup_settings_coursepath',1435344121),('m150427_152235_CREATE_NEW_CUSTOM_REPORT_TYPE_ecommerce_transactions',1435344121),('m150428_071531_core_version_update_to_661',1435344121),('m150430_191518_create_aicc_tables',1435344124),('m150504_153513_CREATE_TABLE_audit_trail_log',1435344124),('m150507_082232_learning_report_type_ADD_NEW_CUSTOM_REPORT_TYPE_audit_trail',1435344124),('m150508_083457_FIX_core_group_fields',1435344124),('m150510_134436_ADD_USE_DASHLETS_CORE_SETTING',1435344124),('m150511_141213_adyen_gateway_integration',1435344125),('m150512_083018_ALTER_conference_room_ADD_accountId',1435344126),('m150512_151617_ALTER_TABLE_learning_course_ADD_COLUMN_autoplay_resume',1435344126),('m150513_134520_ALTER_learning_report_type_CHANGE_audit_trail_related',1435344127),('m150514_152855_catchup_add_datetime',1435344127),('m150515_060459_ALTER_TABLE_learning_course_file_ADD_user_id',1435344127),('m150515_141011_FIX_active_users_counting',1435344127),('m150516_140458_create_simplesaml_multidomain_tables',1435344127),('m150518_085121_alter_inbox_notif_timestamp',1435344127),('m150519_062157_ALTER_learning_test_ADD_point_assignment',1435344127),('m150519_062223_FIX_learning_testquestanswer',1435344127),('m150519_160456_ALTER_TABLE_learning_coursepath_user_ADD_catchup_user_limit',1435344128),('m150521_082133_delete_old_full_catalog_dashlet',1435344128),('m150521_175845_FIX_org_branch_fields_assignment',1435344128),('m150528_121503_FIX_core_group_members',1435344128),('m150601_074200_delete_old_full_courses_dashlet',1435344128),('m150622_073130_FIX_core_notification_user_filter',1435344128),('m150622_122830_remove_emulate_ie8_setting',1435344128),('m150624_141543_fix_webinar_courseuser_completion',1435344128),('m150328_184038_CREATE_HTTPS_tables',1448624525),('m150522_113219_CUSTOM_MAIN_MENU',1448624526),('m150527_123832_ALTER_audit_trail_log_ADD_id_target',1448624526),('m150603_094515_INSERT_learning_report_type_INSERT_learning_report_filter',1448624526),('m150603_121206_FIX_core_https',1448624526),('m150605_065056_COACHING_related_tables',1448624526),('m150611_072100_ALTER_TABLE_core_group_ADD_COLUMNS_assign_rules_AND_assign_rules_logic_operator',1448624526),('m150611_090246_CREATE_TABLE_gore_group_assign_rules',1448624526),('m150616_075053_CALCULATE_COURSE_SCORE_TO_score_given_COLUMN',1448624526),('m150617_122756_APP7020_TABLES',1448624527),('m150623_110647_ALTER_TABLE_learning_course_ADD_COLUMNS_final_score_mode_AND_final_object',1448624527),('m150625_104413_ADDING_NEW_CUSTOM_REPORT_TYPE',1448624527),('m150626_130000_core_version_update_to_662',1448624527),('m150629_080550_FIX_learning_courseuser_for_classrooms',1448624527),('m150630_111013_FIX_learning_label_and_additional_fields',1448624527),('m150630_151142_alter_CORE_USER_PU_COURSE_add_extra_seats_column',1448624527),('m150701_094804_FIX_additional_mandatory_fields',1448624527),('m150701_145420_ALTER_TABLE_learning_course_ADD_COLUMN_soft_deadline',1448624527),('m150701_200000_core_version_update_to_663',1448624527),('m150707_124157_add_LEARNING_COURSE_COACHING_MESSAGES_table',1448624527),('m150708_130000_ALTER_TABLE_core_enroll_rule_item_ADD_COLUMN_selection_state',1448624527),('m150713_110138_ALTER_TABLE_learning_coursepath_courses_ADD_COLUMNS_delay_time_AND_delay_time_unit',1448624527),('m150714_192305_ALTER_TABLE_learning_category_tree_ADD_COLUMN_soft_deadline',1448624527),('m150715_074808_MIGRATE_FINAL_BOOKMARKS_TO_NEW_LOGIC',1448624527),('m150716_100915_DROP_PK_IN_core_pwd_recover',1448624527),('m150716_112829_FIX_core_group_members',1448624527),('m150717_194755_DROP_TABLE_old_audit_trail',1448624527),('m150722_094548_FIX_CUSTOM_MAIN_MENU',1448624527),('m150726_170042_CREATE_TABLES_oauth2_server',1448624527),('m150728_122033_ALTER_TABLE_core_job_CHANGE_COLUMN_params',1448624527),('m150729_081309_CREATE_learning_course_coaching_webinar_session_CREATE_learning_course_coaching_webinar_session_user',1448624527),('m150803_063519_FIX_core_enroll_rule_item',1448624527),('m150803_140952_CREATE_COURSE_CUSTOM_FIELDS_TABLES',1448624527),('m150807_070548_core_version_update_to_664',1448624528),('m150808_072108_ADD_COLUMNS_OAUTH2_CLIENTS',1448624528),('m150809_134018_XAPI_related_changes_initial_migration',1448624528),('m150810_084026_ALTER_TABLE_simplesaml_account_ADD_COLUMN_user_provisioning_AND_user_provisioning_update_if_exists',1448624528),('m150810_090437_ALTER_TABLE_simplesaml_account_ADD_COLUMN_saml_statements',1448624528),('m150811_085857_ALTER_TABLE_LEARNING_COURSE_FILE_ADDING_NESTED_SET_BEHAVIOR',1448624528),('m150817_102344_CREATE_TABLE_ipfilter',1448624528),('m150826_100000_ALTER_TABLE_webinar_session_change_foreign_key_settings',1448624528),('m150827_070349_ALTER_TABLE_CORE_NOTIFICATION_CHANGE_FOREIGN_KEY',1448624528),('m150901_070106_ALTER_TABLE_CORE_SETTING_ADD_RETURN_PATH',1448624528),('m150914_071418_APP7020_QUESTION_HISTORY',1448624528),('m150914_110007_APP7020_ALTER_ADD_QUESTION_CONTENT_RELATION',1448624528),('m150914_125618_APP7020_CONTENT_RATING',1448624528),('m150915_083552_ALTER_TABLE_core_admin_course_MODIFY_COLUMN_id_entry_TO_STRING',1448624528),('m150915_113549_ALTER_learning_video_ADD_seekable',1448624528),('m150915_122118_CREATE_learning_video_subtitles_table',1448624528),('m150915_134544_ALTER_learning_video_track_ADD_bookmark',1448624528),('m150916_074416_APP7020_CONTENT_HISTORY',1448624528),('m150917_140750_ALTER_TABLE_CORE_GROUP_ADD_NEW_ROLES',1448624528),('m150918_133836_APP7020_CONTENT_REVIEWS',1448624528),('m150921_210001_ELUCIDAT_create_table_lrs_registration_map',1448624528),('m150923_104143_ADD_SETTINGS_COLUMN_TO_COURSE_FIELDS_TABLE',1448624528),('m150925_102743_ALTER_TABLE_core_plugin_ADD_COLUMN_settings',1448624528),('m150929_105418_ELUCIDAT_RELATED_TABLES',1448624528),('m151001_111138_MIGRATE_LDAP_SETTINGS',1448624528),('m151007_073638_ALTER_core_multidomain_ADD_catalog_COLUMNS',1448624528),('m151007_115223_ALTER_APP7020_QUESTION_ADD_LO',1448624528),('m151008_140513_APP7020_CREATE_TABLE_LO_TAGS',1448624529),('m151008_140523_APP7020_CREATE_TABLE_LO_TOPICS',1448624529),('m151009_073438_APP7020_ADD_TABLE_PEER_REVIEW_SETTINGS',1448624529),('m151009_110556_APP7020_REMOVE_UNUSED_SETTINGS_FOR_PEER_REVUEW',1448624529),('m151013_010000_remove_7020_items_from_learner_menu',1448624529),('m151013_083520_FORUM_THREADS_ADD_FIRST_LEVEL_NESTED_REPLIES',1448624529),('m151013_120240_CREATE_APP7020_INVITE_TABLE',1448624529),('m151013_152449_APP7020_ADD_PUBLISHED_TABLE',1448624529),('m151015_151529_ALTER_TABLE_webinar_session_ADD_COLUMNS_join_in_advance_time_user_join_in_advance_time_teacher',1448624529),('m151021_142749_ALTER_core_multidomain_ADD_whitelabel_COLUMNS',1448624529),('m151023_114037_app7020_show_expert_button_core_settings',1448624529),('m151027_070000_ALTER_salesforce_add_use_sandbox',1448624529),('m151020_114934_ALTER_TABLE_LEARNING_COURSE_ADD_INITIAL_SCORE_COLUMNS',1454484176),('m151020_135606_MIGRATE_INITIAL_BOOKMARKS_TO_NEW_LOGIC',1454484176),('m151021_075406_CALCULATE_INITIAL_COURSE_SCORE_TO_initial_score_given_COLUMN',1454484176),('m151024_070000_core_version_update_to_68',1454484176),('m151026_162119_APP7020_EXPRETS_TABLE',1454484176),('m151027_100032_APP7020_EXPRETS_PATCH',1454484176),('m151028_094831_ALTER_TABLE_CORE_NOTIFICATION_ADD_COLUMN_PU_PROFILE_ID',1454484176),('m151028_123318_create_CORE_SETTING_WHITELIST',1454484176),('m151028_125434_coach_app_settings',1454484176),('m151028_153738_MOVE_CONFERENCE_TABLES_INTO_WEBINAR_SESSION',1454484176),('m151028_155411_coach_app_settings_patch',1454484176),('m151030_144700_ALTER_TABLE_learning_coursepath_ADD_COLUMN_deep_link',1454484177),('m151103_114248_ALTER_TABLE_learning_course_ADD_COLUMN_deep_link',1454484177),('m151104_130307_share_7020_settings_insert',1454484177),('m151104_144626_ALTER_TABLE_learning_coursepath_user_ADD_COLUMN_deeplinked_by',1454484177),('m151105_093234_APP7020_PERMISSIONS',1454484177),('m151105_101748_APP7020_CUSTOM_PERMISSIONS',1454484177),('m151105_153027_ALTER_TABLE_core_user_ADD_FIELD_expiration',1454484177),('m151106_115538_app7020_rename_permission_name',1454484177),('m151106_133437_Share7020App_insert_delete_core_settings',1454484177),('m151110_080159_ALTER_learning_courseuser_ADD_deeplinked_by',1454484177),('m151110_151632_APP7020_Questions_Requests',1454484177),('m151110_153057_APP7020_Ingore_list',1454484177),('m151110_164911_ALTER_TABLE_learning_forummessage_ADD_COLUMNS_path_original_filemane',1454484177),('m151113_124758_app7020_Requests_changes',1454484177),('m151114_140000_ALTER_core_org_chart_tree_FIX_PARENTS',1454484177),('m151116_081935_APP7020_ADD_TOOLTIPS_TABLE',1454484177),('m151117_063748_APP7020_ALTER_TOOLTIPS_ADD_COLUMN_TEXT',1454484177),('m151117_115226_learning_report_type_ADD_NEW_CUSTOM_REPORTS_certification_users_AND_users_certification',1454484177),('m151117_141604_learning_report_type_ADD_NEW_CUSTOM_REPORTS_user_external_training',1454484177),('m151118_144931_ALTER_TABLE_CORE_GROUP_ADD_COLUMN_ADDITIONAL_FIELDS',1454484177),('m151120_123009_INSERT_CORE_SETTING_NEW_SETTING_EMAIL_VERIFICATION',1454484177),('m151120_144658_ALTER_TABLE_CORE_USER_ADD_COLUMNS_EMAIL_CODE_REQUESTED_ON',1454484177),('m151125_081428_CREATE_TABLE_App7020_document_conversions',1454484177),('m151125_081512_CREATE_TABLE_App7020_document_images',1454484177),('m151125_115620_ADD_Minimal_Layout_Columns_CORE_MULTIDOMAIN',1454484177),('m151125_120059_APP7020_CREATE_TABLE_CONTENT_TO_LINKS',1454484177),('m151125_120341_ADD_Minimal_Layout_Columns_CORE_SETTING',1454484177),('m151125_144147_App7020_Alter_document_conversions_table',1454484177),('m151126_080727_stripe_multidomain_settings',1454484177),('m151127_094049_INSERT_CORE_SETTING_NEW_SETTING_DOCUMENTS_DOWLOAD',1454484177),('m151127_163057_ADD_users_badges_TO_learning_report_type',1454484177),('m151128_084707_elasicsearch_initial_mass_index',1454484178),('m151130_130000_CREATE_core_running_operation',1454484178),('m151201_074858_APP7020_CREATE_TABLE_document_tracking',1454484178),('m151202_100930_APP7020_DROP_TABLE_APP7020_CONTENT_LINKS',1454484178),('m151209_134635_APP7020_ALTER_ADD_NEW_FIELD_FOR_VIEW_IN_LIST',1454484178),('m151209_164526_ALTER_TABLE_core_user_ADD_COLUMN_recent_search',1454484178),('m151210_103849_CREATE_TABLE_LEARNING_REPORT_ACCESS_AND_MEMBERS',1454484178),('m151211_075111_stripe_sandbox_keys_pair',1454484178),('m151214_124421_App7020ContentPublished_add_column_actionType',1454484178),('m151216_161937_APP7020_CREATE_TABLE_topic_visibility',1454484178),('m151221_160305_APP7020_CONTENT_ADD_WHISHED_STATUS_FIELD',1454484178),('m151229_162029_Insert_CORE_SETTING_Column_ENABLE_LEGACY_MENU',1454484178),('m160104_114947_CREATE_TABLE_APP7020_CUSTOM_CONTENT_SETTINGS',1454484178),('m160107_154744_ALTER_TABLE_Learning_Course_ADD_COLUMNS_For_Publisher_And_Publish_Date',1454484179),('m160110_154008_ADD_pipeline_id_TABLE_learning_authoring',1454484179),('m160111_120647_add_div_to_title_question',1454484179),('m160112_075116_CREATE_TABLE_APP7020_TEMPORARY_INVITATIONS',1454484179),('m160115_093109_APP7020_ALTER_ADD_NEW_FIELD_SNS',1454484179),('m160118_142720_APP7020_ALTER_REMOVE_NEW_FIELD_SNS',1454484179),('m160118_154552_APP7020_ALTER_RIVERT_SNS_FLAG',1454484179),('m160121_133046_INSERT_core_lang_language_NEW_LANGUAGE',1454484179),('m160122_092057_ALTER_TABLE_CORE_LANG_TEXT_MODIFY_TEXT_KEY_BINARY',1454484179),('m160125_145201_INSERT_core_scheme_color_MENU_HEADER_BG',1454484179),('m160126_122257_course_deleted_notification_only_godadmin',1454579910),('m151126_150010_CREATE_TABLES_gamification_leaderboard_AND_gamification_leaderboard_translation',1454579911),('m151203_110518_CREATE_TABLES_gamification_contest_ADD_FOREIGN_KEYS',1454579911),('m151204_133214_ADD_STANDARD_REPORT_users_badges',1454579911),('m151209_135856_ADD_NEW_CUSTOM_REPORT_user_contest',1454579911),('m151216_132638_ALTER_TABLE_gamification_contest_chart_CHANGE_fk_gamification_contest_chart',1454579911),('m160121_120649_ALTER_TABLE_gamification_contest_ADD_NEW_COLUMN_timezone',1454579911),('m160203_100637_APP7020_DELETE_ASK_THE_EXPERT_BUTTON_SETTING',1455547135),('m160203_154310_APP7020_ALTER_QUESTION_REQUEST_CHANGE_DELETE_CRITERIA',1455547136),('m160203_162652_APP7020_APP7020_ALTER_QUESTION_REQUEST_CHANGE_DELETE_CRITERIA_QUESTION',1455547138),('m160204_092627_APP7020_ALTER_APP7020_QUESTIONS_REMOVE_IDCONTENT_FK_SET_TO_NULL_ON_DELETE',1455547139),('m160205_080000_elasicsearch_initial_mass_index_2',1455547140),('m160210_080000_create_view_and_function_org_assignments',1455547140),('m160211_114956_INSERT_default_translations_SLOVENIAN_ENGLISH_UK',1455547140),('m160202_142329_CREATE_GAMIFICATION_REWARDS_TABLES',1455547543),('m160215_125317_GAMIFICATION_CONVERT_BADGES_POINTS_TO_COINS',1455547543);
/*!40000 ALTER TABLE `tbl_migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `user_org_assignment`
--

DROP TABLE IF EXISTS `user_org_assignment`;
/*!50001 DROP VIEW IF EXISTS `user_org_assignment`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `user_org_assignment` AS SELECT 
 1 AS `idUser`,
 1 AS `idGroup`,
 1 AS `isUserGroup`,
 1 AS `idOrgDirectMemberOf`,
 1 AS `idOrgDirectMemberOrInChildOf`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `webinar_session`
--

DROP TABLE IF EXISTS `webinar_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webinar_session` (
  `id_session` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `id_tool_account` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `max_enroll` int(11) NOT NULL,
  `score_base` int(11) NOT NULL DEFAULT '100',
  `description` text,
  `date_begin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `tool` varchar(255) NOT NULL DEFAULT 'custom',
  `tool_params` text,
  `time_spent_to_complete` int(11) NOT NULL DEFAULT '0',
  `complete_on_registration_watched` tinyint(1) NOT NULL DEFAULT '0',
  `evaluation_type` varchar(45) NOT NULL DEFAULT 'joined_webinar',
  `created_by` int(11) DEFAULT NULL,
  `recording_file` varchar(255) DEFAULT NULL,
  `join_in_advance_time_user` int(11) DEFAULT '0',
  `join_in_advance_time_teacher` int(11) DEFAULT '0',
  PRIMARY KEY (`id_session`),
  KEY `tool_account` (`id_tool_account`),
  KEY `course_id` (`course_id`),
  CONSTRAINT `webinar_session_ibfk_1` FOREIGN KEY (`id_tool_account`) REFERENCES `webinar_tool_account` (`id_account`) ON DELETE SET NULL,
  CONSTRAINT `webinar_session_ibfk_2` FOREIGN KEY (`course_id`) REFERENCES `learning_course` (`idCourse`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webinar_session`
--

LOCK TABLES `webinar_session` WRITE;
/*!40000 ALTER TABLE `webinar_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `webinar_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webinar_session_user`
--

DROP TABLE IF EXISTS `webinar_session_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webinar_session_user` (
  `id_session` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `evaluation_text` text,
  `evaluation_file` varchar(255) DEFAULT NULL,
  `evaluation_score` int(11) DEFAULT NULL,
  `evaluation_date` date DEFAULT NULL,
  `evaluation_status` tinyint(1) DEFAULT NULL,
  `evaluator_id` int(11) DEFAULT NULL,
  `attendance_hours` int(11) NOT NULL DEFAULT '0',
  `waiting` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_subscribed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_completed` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `attended` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_session`,`id_user`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `webinar_session_user_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `core_user` (`idst`),
  CONSTRAINT `webinar_session_user_ibfk_2` FOREIGN KEY (`id_session`) REFERENCES `webinar_session` (`id_session`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webinar_session_user`
--

LOCK TABLES `webinar_session_user` WRITE;
/*!40000 ALTER TABLE `webinar_session_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `webinar_session_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webinar_tool_account`
--

DROP TABLE IF EXISTS `webinar_tool_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webinar_tool_account` (
  `id_account` int(11) NOT NULL AUTO_INCREMENT,
  `tool` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `settings` text NOT NULL,
  PRIMARY KEY (`id_account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webinar_tool_account`
--

LOCK TABLES `webinar_tool_account` WRITE;
/*!40000 ALTER TABLE `webinar_tool_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `webinar_tool_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `user_org_assignment`
--

/*!50001 DROP VIEW IF EXISTS `user_org_assignment`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `user_org_assignment` AS select `u`.`idst` AS `idUser`,`g`.`idst` AS `idGroup`,(`g`.`hidden` = 'false') AS `isUserGroup`,`tree`.`idOrg` AS `idOrgDirectMemberOf`,`tree2`.`idOrg` AS `idOrgDirectMemberOrInChildOf` from ((((`core_user` `u` join `core_group_members` `cgm` on((`cgm`.`idstMember` = `u`.`idst`))) left join `core_group` `g` on((`g`.`idst` = `cgm`.`idst`))) left join `core_org_chart_tree` `tree` on(((`tree`.`idst_oc` = `g`.`idst`) and (`g`.`groupid` like '/oc\\_%')))) left join `core_org_chart_tree` `tree2` on(((`tree2`.`iLeft` <= `tree`.`iLeft`) and (`tree2`.`iRight` >= `tree`.`iRight`)))) where ((isnull(`tree`.`idOrg`) and (`g`.`hidden` = 'false')) or ((`tree`.`idOrg` is not null) and (`tree`.`idOrg` <> `ROOT_ORG_ID`()))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-15 17:44:53
