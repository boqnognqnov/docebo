#!/bin/bash
#
# A little script to help transition from 6.9 to 7.0 platform configuration.
# Note please, ALL platform configuration keys gonna be shared by 7.0+ and Hydra!
#
# Prerequisites:
#
#    1. It is assumed you have good platform_main_config:6.9:devel or 6.9:live
#    2. You know the HOST and PORT of the Redis serving this LMS platform
#    3. You know the 7.0 common/config/platform_config_template.redis file and what every fields mean there
#
#  
# 1. This script must be executed in 6.9 environment, BEFORE switching the code to 7.0!
#    This is because some key fields required by 7.0 WILL be set by this script/procedure!
#    If you don't care interrupting the LMS though (local, development, test), you can do it in 7.0 environment.
#    If you want to run it in 7.0 env, you must change the platform version to 6.9 temporarily (in common/config/main.php)	
#
# 2. Manually copy platform_config_template.redis from !LMS 7.0! to <lms-root>/platform_config_template-7.0.redis
#
# 3. Set the CFGKEY to proper value
# 
# 4. Run this script (pick a random LMS, hosted on this cluster, all parameters are required):
#    ./upgrade-redis-platform-config-69-to-common.sh <lms.domain.com> <redis-host> <redis-port>
#
# 5. At the end, you have "platform_main_config:6.9:xxx" platform config transfered to "platform_main_config:common", having 6.9 values unchanged
#
# 6. Additionally, in the new "platform_main_config:common" you will find NEW fields, specific for 7.0.
#
# 7. The list of NEW fields is stored in diff.redis.
#
# 8. Manually edit that file (diff.redis) and set proper values for new fields
#
# 9. Manually Run this command to import diff.redis
#    $ redis-cli -h <redis-host> -p <redis-port> < diff.redis 
#
# 10. Now check out platform_main_config:common in Redis and see if everything is ok.
#     It must have exactly the same number of fields as many rows are in platform_config_template-7.0.redis.
#     Values from platform_main_config:6.9:xxx MUST be the same
#     Added fields must have values you set in diff.redis
#
# 11. NOW you can switch the LMS code to 7.0  (git checkout && git pull), if you done this in 6.9 env.
#     If you have done it in 7.0 env, change back main.php's platform version to 7.0 
#
#


# CHANGE ME!
CFGKEY="6.9:devel"    #devel, live ?

# --- NO CHANGES BELOW

DOMAIN=$1  
REDIS_HOST=$2
REDIS_PORT=$3

cd lms/protected
sh yiic-domain.sh $DOMAIN util exportPlatformConfig --key=$CFGKEY > ../../tmp.redis

cd ../../common/config
sed -i "s/${CFGKEY}/common/g" ../../tmp.redis
cd ../..
redis-cli -h $REDIS_HOST -p $REDIS_PORT < platform_config_template-7.0.redis
redis-cli -h $REDIS_HOST -p $REDIS_PORT < tmp.redis
#rm tmp.redis

cd lms/protected
sh yiic-domain.sh $DOMAIN util comparePlatformConfigs --keyFrom=$CFGKEY --keyTo=common --asHsets=1 > ../../diff.redis

# At this point you have diff.redis file in <lms-root> folder. Please edit it and at the end run the command:
# redis-cli < diff.redis




