Options -Indexes
Options +FollowSymLinks
DirectoryIndex index.php index.html index.htm

<Files favicon.ico>
    ErrorDocument 404 "The requested file favicon.ico was not found."
</Files>

<IfModule mod_rewrite.c>
    RewriteEngine on

    # hide directories that starts with a . (dot)
    #RewriteRule "(^|/)\." - [F]

    # Redirect to the www version of the site
    #RewriteCond %{HTTP_HOST} !^www\. [NC]
    #RewriteRule ^ http://www.%{HTTP_HOST}%{REQUEST_URI} [L,R=301]

	RewriteRule ^(.*)/webapp$ /$1/webapp/ [L,NC,R=301]

	RewriteCond %{REQUEST_URI} !^/addons
	RewriteCond %{REQUEST_URI} !^/docs
	RewriteCond %{REQUEST_URI} !^/admin
	RewriteCond %{REQUEST_URI} !^/api
	RewriteCond %{REQUEST_URI} !^/assets
	RewriteCond %{REQUEST_URI} !^/authoring
	RewriteCond %{REQUEST_URI} !^/common
	RewriteCond %{REQUEST_URI} !^/cron
	RewriteCond %{REQUEST_URI} !^/db

	# Uncomment the following line when you want to play SCORMs from local file system
	# Of ocurse, you have to grab the DCD code from http://git.docebo.info/root/docebo-content-delivery and clone it in /dcd
	# Additionally, see _LOCALPLAYER variable in dcd/scormapi_v60/js/api_12.js & api_2004.js
	RewriteCond %{REQUEST_URI} !^/dcd

	RewriteCond %{REQUEST_URI} !^/doceboCore
	RewriteCond %{REQUEST_URI} !^/doceboLms
	RewriteCond %{REQUEST_URI} !^/doceboScs
	RewriteCond %{REQUEST_URI} !^/files
	RewriteCond %{REQUEST_URI} !^/gapps
	RewriteCond %{REQUEST_URI} !^/i18n
	RewriteCond %{REQUEST_URI} !^/images
	RewriteCond %{REQUEST_URI} !^/lib
	RewriteCond %{REQUEST_URI} !^/lms
	RewriteCond %{REQUEST_URI} !^/app7020
	RewriteCond %{REQUEST_URI} !^/media
	RewriteCond %{REQUEST_URI} !^/mobile
	RewriteCond %{REQUEST_URI} !^/moxiemanager
	RewriteCond %{REQUEST_URI} !^/plugins
	RewriteCond %{REQUEST_URI} !^/custom_plugins
	RewriteCond %{REQUEST_URI} !^/tcapi
	RewriteCond %{REQUEST_URI} !^/templates
	RewriteCond %{REQUEST_URI} !^/themes
	RewriteCond %{REQUEST_URI} !^/tos
	RewriteCond %{REQUEST_URI} !^/widget
	RewriteCond %{REQUEST_URI} !^/yii
	RewriteCond %{REQUEST_URI} !^/hybridauth
	RewriteCond %{REQUEST_URI} !^/sso
	RewriteCond %{REQUEST_URI} !^/oauth2
	RewriteCond %{REQUEST_URI} !^/pens
	RewriteCond %{REQUEST_URI} !^/webapp
	RewriteCond %{REQUEST_URI} !^/cookiebaker
	RewriteCond %{REQUEST_URI} !^/simplesaml
	RewriteCond %{REQUEST_URI} !^$
	RewriteCond %{REQUEST_URI} !^/index.php
	RewriteCond %{REQUEST_URI} !^/i.php
	RewriteCond %{REQUEST_URI} !^/offline.php
	RewriteCond %{REQUEST_URI} !^/check_trial.php
    	RewriteCond %{REQUEST_URI} !^/check_rds_emea.php
    	RewriteCond %{REQUEST_URI} !^/check_rds_emea_2.php
    	RewriteCond %{REQUEST_URI} !^/check_rds_apac.php
    	RewriteCond %{REQUEST_URI} !^/check_rds_apac_2.php
	# Allow Apache Server status for diagnostic purposes
	# Please set IP filtering in /etc/apache2/mods-available/status.conf to prevent attacks
	RewriteCond %{REQUEST_URI} !^/server-status

	#RewriteCond %{QUERY_STRING} !domain
	RewriteCond %{QUERY_STRING} !(?:^|&)domain=
	RewriteRule ^([a-zA-Z0-9_-]+)/?(.*)$ $2?%{QUERY_STRING}&domain=$1 [L]

	# "mask" the hybridauth endpoint as a directory inside root (while it's really a Yii action)
	RewriteRule ^hybridauth/(.*)$ /lms/?r=site/hybridauthEndpoint [L,NC,QSA]

	# Make old Google apps redirect URLs work for backward compatibility
	RewriteCond %{QUERY_STRING} (^|&)r=lms/loginexternal/googleappsRedirect(&|$) [NC]
	RewriteCond %{QUERY_STRING} (^|&)code=(.*)(&|$) [NC]
	RewriteRule ^doceboLms/(.*) /lms/?r=GoogleappsApp/GoogleappsApp/googleappsRedirect&code=%2 [L,NC,R=302]

	# Make sure old SSO links work
	RewriteCond %{QUERY_STRING} (^|&)token=.*(&|$) [NC]
    RewriteRule ^doceboLms/(index\.php)$ /lms/?r=site/sso&%{QUERY_STRING} [L,NC,QSA,R=302]

	# Revwrite rule for SSO types
    RewriteRule ^sso/(.*)$ /lms/index.php?r=site/sso&sso_type=$1 [L,NC,QSA]

    # Revwrite rule for oauth2 endpoints
    RewriteRule ^oauth2/(.*)$ /lms/index.php?r=oauth/oauth2/$1&%{QUERY_STRING} [L,NC,QSA]

    # Rewrite rule for PENS requests
    RewriteRule ^pens$ /lms/index.php?r=pensServer/index&%{QUERY_STRING} [L,NC]

    # Revwrite rule for SSO types
    RewriteRule ^simplesaml/(.*)$ /lms/index.php?r=SimpleSamlApp/SimpleSamlApp/$1 [L,NC,QSA]
    RewriteRule ^([a-zA-Z0-9_-]+)/simplesaml/(.*)$ /lms/index.php?r=SimpleSamlApp/SimpleSamlApp/$2&domain=$1 [L,NC]
</IfModule>

<IfModule php5_module>
    php_value session.cookie_httponly true
</IfModule>

<FilesMatch "\.(htaccess|htpasswd|ini|php|fla|psd|log|sh)$">
    Order Allow,Deny
    Deny from all
</FilesMatch>

<FilesMatch "(check_trial\.php|check_rds_emea\.php|check_rds_emea_2\.php|check_rds_apac\.php|check_rds_apac_2\.php|offline\.php|i\.php|api_tester\.php|paypal_ipn_test\.php|index\.php|ajax\.adm_server\.php|ajax\.server\.php|translation\.php|paypal\.php|paypal_dsaas\.php|tasks\.php|tiny_mce_gzip\.php|index_tiny\.php|scormXmlTree\.php|soaplms\.php|scorm_page_body\.php|finish\.php|check\.php|text\.php|users\.php|rooms\.php|write\.php|keep_alive\.php|oauth2callback\.php)$">
    Order Allow,Deny
    Allow from all
</FilesMatch>
