<style>
<!--

html {
	margin: 0;
	padding: 0;
	height: 100%;
}

body {
	position: relative;
    font-family: 'Open Sans', sans-serif;
    color: #333333;
    background: #ffffff;
    overflow-y: scroll;
    font-size:13px;
    line-height: 13px;
	min-height: 100%;
	margin: 0;
	padding: 0;
}

h1,h2,h3 {
    font-size:24px;
    line-height: 11px;
}

p {
    font-size:20px;
}


.colored {
    color: #0465AC;
}


.boxed {
    width: 550px;
    height: 185px;
    border: 1px solid #CCCCCC;
    margin: 0 auto; 
}

-->
</style>


<div style="width: 100%; text-align: center;">


    <img src="/themes/spt/images/waiting_1.jpg">
    <br>

    <div class="boxed">
    <br>
    <h2>We're upgrading your E-Learning Platform</h2>
    <h2>to the <span class="colored">latest spectacular version</span></h2>
    
    <br>
    <p>This operation shouldn't take more than 5 minutes</p>
    <br>
    </div>
    

</div>